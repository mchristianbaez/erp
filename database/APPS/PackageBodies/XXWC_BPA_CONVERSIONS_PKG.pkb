create or replace package body           apps.XXWC_BPA_CONVERSIONS_PKG as

   /*************************************************************************************************   
   *    $Header XXWC_BPA_CONVERSIONS_PKG $                                                          *
   *   Module Name: XXWC_BPA_CONVERSIONS_PKG                                                        *
   *                                                                                                *
   *   PURPOSE:   This package is used for the data conversions for suppliers and supply chain data *
   *                                                                                                * 
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014  Lee Spitzer                Initial Version 20130917-00676             *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        08/05/2014  Lee Spitzer                TMS Ticket 20140726-00007                  *
   *                                                         Wave 1 of Vendor Cost Improvements     *
   *   1.2        09/08/2014  Lee Spitzer                TMS Ticket 20140909-00030                  *
   *                                                        Fix Address display on PO document for  *
   *                                                         0PURCHASING site                       *
   *   1.3        01/19/2015  Lee Spitzer                TMS Ticket 20140909-00029                  *
   *                                                        Create 1 SEE NOTES contact              *
   *                                                                                                *
   /************************************************************************************************/
 
G_EXCEPTION EXCEPTION;
g_vendor_site_code VARCHAR2(15) := 'PURCHASING';
g_count NUMBER DEFAULT 0;
g_1see_notes VARCHAR2(15) := '1SEE NOTES'; --added 1/19/2015 TMS Ticket 20140909-00029
g_see_notes VARCHAR2(20) := '****SEE NOTES****'; --added 1/19/2015 TMS Ticket 20140909-00029
G_PKG_NAME CONSTANT varchar2(30) := 'xxwc_bpa_conversions_pkg';

g_log_head    CONSTANT VARCHAR2(50) := 'po.plsql.'|| G_PKG_NAME || '.';

-- Debugging
g_debug_stmt BOOLEAN := PO_DEBUG.is_debug_stmt_on;
g_debug_unexp BOOLEAN := PO_DEBUG.is_debug_unexp_on;

 -- Added 1.1 8/6/2014 Error DEBUG Handling
  g_err_msg         VARCHAR2 (2000);
  g_err_callfrom    VARCHAR2 (175) := 'XXWC_BPA_CONVERSIONS_PKG';
  g_err_callpoint   VARCHAR2 (175) := 'START';
  g_distro_list     VARCHAR2 (80) := 'OracleDevelopmentGroup@hdsupply.com';
  g_module          VARCHAR2 (80);

   
   /*************************************************************************************************   
   *   Procedure add_purchasing_supplier_site                                                       *
   *   Purpose : Procedure adds the PURCHASING Vendor Site Code to existing Suppliers               *
   *                                                                                                *
   *                                                                                                *
   *     procedure add_purchasing_supplier_site                                                     *
   *             (p_org_id IN NUMBER,                                                               *
   *              p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        08/05/2014  Lee Spitzer                TMS Ticket 20140726-00007                  *
   *                                                         Wave 1 of Vendor Cost Improvements     *
   *                                                                                                *
   /************************************************************************************************/
   

    procedure add_purchasing_supplier_site 
                (p_org_id IN NUMBER,
                 p_vendor_id IN NUMBER) is
    
        --Cursor for Vendors that don't have g_vendor_site_code SITE
        Cursor c_vendor IS
        select distinct ass.vendor_id
        from   ap_suppliers ass,
               ap_supplier_sites_all assa
        where  not exists (select *
                       from   ap_supplier_sites_all assa1
                       where  assa1.vendor_id = ass.vendor_id
                       AND    assa1.vendor_site_code = g_vendor_site_code
                       AND    assa.vendor_id = assa1.vendor_id
                       AND    assa.org_id = assa1.org_id
                       and    sysdate  < nvl(assa1.inactive_date,sysdate+1)  
                       )
        and    ass.vendor_id = assa.vendor_id
        and    nvl(assa.purchasing_site_flag,'N') = 'Y'
        and    sysdate  < nvl(assa.inactive_date,sysdate+1)  
        and    assa.org_id = p_org_id
        and    ass.vendor_id = nvl(p_vendor_id, ass.vendor_id)
        ;
        
        --Return the first vendor site and create the g_vendor_site_code 
        Cursor c_vendor_site(p_vendor_id in NUMBER)  IS
        select distinct 
            vendor_id,
            org_id,
            purchasing_site_flag,
            pay_site_flag,
            primary_pay_site_flag,
            g_vendor_site_code vendor_site_code,
            address_line1,
            address_line2,
            address_line3, 
            city,
            state, zip,
            county,
            country,
            phone,
            fax,
            hold_unmatched_invoices_flag,
            accts_pay_code_combination_id,
            prepay_code_combination_id,
            payment_method_lookup_code,
            exclusive_payment_flag, 
            terms_id,
            pay_group_lookup_code,
            match_option,
            'NEW' status,
            distribution_set_id, 
            tax_reporting_site_flag,
            created_by, 
            creation_date,
            last_updated_by, 
            last_update_date,
            CASE WHEN address_style IS NULL AND country = 'US' THEN 'POSTAL_ADDR_US'
                 WHEN address_style IS NULL AND country != 'US' THEN 'POSTAL_ADDR_DEF'
                 WHEN address_style IS NOT NULL THEN address_style end address_style
            from  ap_supplier_sites_all assa
            where  vendor_id = p_vendor_id
            and    purchasing_site_flag = 'Y'
            AND    SYSDATE  < nvl(inactive_date,SYSDATE+1)  
            and    xxwc_bpa_conversions_pkg.get_default_vendor_site_id(p_vendor_id,null) is not null
            and    vendor_site_id = xxwc_bpa_conversions_pkg.get_default_vendor_site_id(p_vendor_id,null)
          UNION
          select distinct 
            vendor_id,
            org_id,
            purchasing_site_flag,
            pay_site_flag,
            primary_pay_site_flag,
            g_vendor_site_code vendor_site_code,
            address_line1,
            address_line2,
            address_line3, 
            city,
            state, zip,
            county,
            country,
            phone,
            fax,
            hold_unmatched_invoices_flag,
            accts_pay_code_combination_id,
            prepay_code_combination_id,
            payment_method_lookup_code,
            exclusive_payment_flag, 
            terms_id,
            pay_group_lookup_code,
            match_option,
            'NEW' status,
            distribution_set_id, 
            tax_reporting_site_flag,
            created_by, 
            creation_date,
            last_updated_by, 
            last_update_date,
            CASE WHEN address_style IS NULL AND country = 'US' THEN 'POSTAL_ADDR_US'
                 WHEN address_style IS NULL AND country != 'US' THEN 'POSTAL_ADDR_DEF'
                 WHEN address_style IS NOT NULL THEN address_style end address_style
            from  ap_supplier_sites_all assa
            where  vendor_id = p_vendor_id
            and    purchasing_site_flag = 'Y'
            AND    SYSDATE  < nvl(inactive_date,SYSDATE+1)  
            AND    xxwc_bpa_conversions_pkg.get_default_vendor_site_id(p_vendor_id,NULL) IS NULL
            and    rownum = 1;
    
    
            l_edi_count NUMBER;
            l_ece_tp_location_code VARCHAR2(60);
            l_SUPPLIER_NOTIF_METHOD VARCHAR2(25);
    
        BEGIN
        
        g_err_callpoint := 'add_purchasing_supplier_site';
            
            FOR r_vendor IN c_vendor 
            
                LOOP
                
                  --Check if EDI Vendor
                  BEGIN
                      select count(*)
                      into   l_edi_count
                      from   ap_supplier_sites_all assa
                      where  assa.ece_tp_location_code is not null
                      and    nvl(assa.purchasing_site_flag,'N') = 'Y'
                      AND    SYSDATE  < nvl(assa.inactive_date,SYSDATE+1)
                      and    assa.vendor_id = r_vendor.vendor_id; --8/6/2014 updated version 20140726-00007 1.1 missing vendor_id in loop
                  EXCEPTION
                    when others then
                        l_edi_count := 0;
                 end;
                 
                 
                  if l_edi_count > 0 then
                  
                      l_ece_tp_location_code := g_vendor_site_code;
                      
                  else
                  
                      l_ece_tp_location_code := NULL;
                      
                  end if;
                  
                  
                  BEGIN
                    SELECT distinct SUPPLIER_NOTIF_METHOD
                    INTO   l_SUPPLIER_NOTIF_METHOD
                    FROM   ap_supplier_sites_all assa
                    --where  assa.ece_tp_location_code is not null --Removed 08/19/2014
                    where  1=1 --Added 08/19/2014
                    AND    nvl(assa.purchasing_site_flag,'N') = 'Y'
                    AND    nvl(assa.supplier_notif_method,'NONE') != 'NONE'
                    AND    SYSDATE  < nvl(assa.inactive_date,SYSDATE+1)
                    and    assa.vendor_id = r_vendor.vendor_id; --8/6/2014 updated version 20140726-00007 1.1 missing vendor_id in loop
                  EXCEPTION
                    WHEN too_many_rows THEN
                          l_supplier_notif_method := 'FAX';
                    WHEN no_data_found THEN
                          l_supplier_notif_method := NULL;
                    WHEN others THEN
                        l_supplier_notif_method := NULL;
                 end;
                    
                    for r_vendor_site in c_vendor_site(r_vendor.vendor_id)
                    
                        LOOP
                    
                    
                              BEGIN
                              
                               g_count := g_count + 1;
                              
                                INSERT INTO ap_supplier_sites_int
                                           (vendor_site_interface_id,
                                            status,
                                            vendor_id,
                                            org_id,
                                            purchasing_site_flag,
                                            pay_site_flag,
                                            primary_pay_site_flag,
                                            vendor_site_code,
                                            address_line1,
                                            address_line2,
                                            address_line3, city,
                                            state, zip,
                                            county,
                                            country,
                                            phone,
                                            fax,
                                            hold_unmatched_invoices_flag,
                                            accts_pay_code_combination_id,
                                            prepay_code_combination_id,
                                            payment_method_lookup_code,
                                            exclusive_payment_flag, terms_id,
                                            pay_group_lookup_code,
                                            match_option,
                                            distribution_set_id, 
                                            tax_reporting_site_flag,
                                            created_by, 
                                            creation_date,
                                            last_updated_by, 
                                            last_update_date,
                                            ece_tp_location_code,
                                            address_style,
                                            supplier_notif_method,
                                            party_site_name
                                            )
                                        VALUES
                                           (ap_supplier_sites_int_s.NEXTVAL,
                                            r_vendor_site.status,
                                            r_vendor_site.vendor_id, --- Added by Thiru 9/Jan/2012
                                            r_vendor_site.org_id,
                                            r_vendor_site.purchasing_site_flag,
                                            r_vendor_site.pay_site_flag,
                                            r_vendor_site.primary_pay_site_flag,
                                            r_vendor_site.vendor_site_code,
                                            r_vendor_site.vendor_site_code, --Address Line 1 --r_vendor_site.address_line1,
                                            r_vendor_site.address_line1, --Address Line 2 --r_vendor_site.address_line2,
                                            r_vendor_site.address_line2,  --Adderss Line 3 --r_vendor_site.address_line3
                                            r_vendor_site.city,
                                            r_vendor_site.state, 
                                            r_vendor_site.zip,
                                            r_vendor_site.county,
                                            r_vendor_site.country,
                                            r_vendor_site.phone,
                                            r_vendor_site.fax,
                                            r_vendor_site.hold_unmatched_invoices_flag,
                                            r_vendor_site.accts_pay_code_combination_id,
                                            r_vendor_site.prepay_code_combination_id,
                                            r_vendor_site.payment_method_lookup_code,
                                            r_vendor_site.exclusive_payment_flag, 
                                            r_vendor_site.terms_id,
                                            r_vendor_site.pay_group_lookup_code,
                                            r_vendor_site.match_option,
                                            r_vendor_site.distribution_set_id, 
                                            r_vendor_site.tax_reporting_site_flag,
                                            -1, 
                                            SYSDATE,
                                            -1, 
                                            SYSDATE,
                                            l_ece_tp_location_code,
                                            r_vendor_site.address_style,
                                            l_supplier_notif_method,
                                            r_vendor_site.vendor_site_code);
                        
                        
                            EXCEPTION
                            
                                WHEN OTHERS THEN
                                
                                        fnd_file.put_line (fnd_file.output, 'Error inserting into ap_supplier_sites_int ' ||SQLCODE || SQLERRM); 
                            END;
                            
                            
                        
                        END LOOP;
                
                    
                    END LOOP;
                    
                    
            COMMIT;
        
        
              fnd_file.put_line (fnd_file.output, g_count || ' records inserted into ap_supplier_sites_int');
    
    EXCEPTION

    WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    
    END add_purchasing_supplier_site;
    
   /*************************************************************************************************   
   *   Procedure add_purchasing_supplier_site                                                       *
   *   Purpose : Procedure adds the Contact to new PURCHASING Site                                  *
   *                                                                                                *
   *                                                                                                *
   *     procedure add_purchasing_site_contacts                                                     *
   *             (p_org_id IN NUMBER,                                                               *
   *              p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        08/05/2014  Lee Spitzer                TMS Ticket 20140726-00007                  *
   *                                                         Wave 1 of Vendor Cost Improvements     *
   *                                                         Exclude non-EDI                        *
   /************************************************************************************************/
   
    
    procedure add_purchasing_site_contacts
                (p_org_id IN NUMBER,
                 p_vendor_id IN NUMBER) IS
    
    
        cursor c_vendor is
        
            --Find Vendors that have the g_vendor_site_code SITE
            select distinct ass.vendor_id, assa.vendor_site_id
            from   ap_suppliers ass,
                   ap_supplier_sites_all assa
            where  assa.vendor_site_code = g_vendor_site_code
            and    ass.vendor_id = assa.vendor_id
            and    nvl(assa.purchasing_site_flag,'N') = 'Y'
            and    sysdate  < nvl(assa.inactive_date,sysdate+1)  
            and    assa.org_id = p_org_id
            and    ass.vendor_id = nvl(p_vendor_id, ass.vendor_id);
            


    cursor c_vendor_site(p_vendor_id IN NUMBER, p_vendor_site_id in NUMBER) is
    
        --Add the non PURCHASNG SITES as contacts to both the old site and g_vendor_site_code Site
        SELECT assa.vendor_site_code last_name, assa.vendor_site_id, assa.vendor_id, assa.org_id, assa.party_site_id,
               assa.area_code, assa.phone, assa.fax_area_code, assa.fax, assa.email_address
        from   ap_supplier_sites_all assa
        where  assa.vendor_id = p_vendor_id
        and    assa.purchasing_site_flag = 'Y'
        AND    assa.vendor_site_id != p_vendor_site_id
        and    assa.org_id = p_org_id
        AND    SYSDATE  < nvl(assa.inactive_date,SYSDATE+1)
        AND    assa.vendor_site_code NOT IN ('NON EDI')  --Added 1.1 20140726-00007
        --Added Exists Statement 1.1 20140726-00007
        AND   EXISTS (SELECT  msso.VENDOR_SITE_ID
                                          FROM   --XXWC.XXWC_MRP_SR_ASSIGNMENTS_BK msa,
                                                 mrp_sr_assignments msa,
                                                 mrp_sourcing_rules msr,
                                                 mrp_sr_receipt_org msro,
                                                 mrp_sr_source_org msso
                                          -----------
                                          WHERE  msso.vendor_id = p_vendor_id
                                          AND    msso.vendor_site_id = assa.vendor_site_id
                                          --and    msa.organization_id = r_vendor_minimum.organization_id
                                          --and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                                          and    msr.sourcing_rule_id = msro.sourcing_rule_id
                                          and    msro.sr_receipt_id = msso.sr_receipt_id
                                          AND    msa.sourcing_rule_id = msr.sourcing_rule_id
                                          AND    msa.ASSIGNMENT_TYPE = 6)
        and    not exists (select *
                   from   po_vendor_contacts pvc
                   where  pvc.vendor_id = assa.vendor_id
                   and    pvc.last_name = assa.vendor_site_code
                   and    sysdate < nvl(pvc.inactive_date,sysdate+1) 
                   )
        union 
        SELECT assa.vendor_site_code last_name, assa1.vendor_site_id, assa.vendor_id, assa.org_id, assa1.party_site_id,
                assa.area_code, assa.phone, assa.fax_area_code, assa.fax, assa.email_address
        from   ap_supplier_sites_all assa,
               ap_supplier_sites_all assa1
        where  assa.vendor_id = p_vendor_id
        and    assa.purchasing_site_flag = 'Y'
        and    assa.vendor_site_id != p_vendor_site_id
        AND    assa.vendor_id = assa1.vendor_id
        and    assa.org_id = p_org_id
        and    sysdate  < nvl(assa.inactive_date,sysdate+1)  
        and    assa.org_id = assa1.org_id
        and    sysdate  < nvl(assa1.inactive_date,sysdate+1)  
        AND    assa1.vendor_site_id = p_vendor_site_id
        and    assa.vendor_site_code not in ('NON EDI')   --Added 1.1 20140726-00007
        --Added Exists Statement 1.1 20140726-00007
        AND   EXISTS (SELECT  msso.VENDOR_SITE_ID
                      FROM   --XXWC.XXWC_MRP_SR_ASSIGNMENTS_BK msa,
                              mrp_sr_assignments msa,
                              mrp_sourcing_rules msr,
                              mrp_sr_receipt_org msro,
                              mrp_sr_source_org msso
                      -----------
                      WHERE  msso.vendor_id = p_vendor_id
                      AND    msso.vendor_site_id = assa.vendor_site_id
                      --and    msa.organization_id = r_vendor_minimum.organization_id
                      --and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                      and    msr.sourcing_rule_id = msro.sourcing_rule_id
                      and    msro.sr_receipt_id = msso.sr_receipt_id
                      AND    msa.sourcing_rule_id = msr.sourcing_rule_id
                      AND    msa.ASSIGNMENT_TYPE = 6)
        and    not exists (select *
                   from   po_vendor_contacts pvc
                   where  pvc.vendor_id = assa.vendor_id
                   and    pvc.last_name = assa.vendor_site_code
                   and    sysdate  < nvl(pvc.inactive_date,sysdate+1)  
                   );

    
    BEGIN
    
     g_err_callpoint := 'add_purchasing_supplier_site';
   
        for r_vendor in c_vendor 
        
            loop
                
                for r_vendor_site in c_vendor_site(r_vendor.vendor_id, r_vendor.vendor_site_id)
                
                    loop
                    
                          BEGIN
                
                          g_count := g_count + 1;
                
                            Insert Into AP_SUP_SITE_CONTACT_INT
                                (vendor_contact_interface_id,
                                 vendor_id,
                                 vendor_site_id,
                                 last_name,
                                 org_id,
                                 status,
                                 party_site_id,
                                 area_code, 
                                 phone, 
                                 fax_area_code, 
                                 fax, 
                                 email_address)
                                VALUES
                                (AP_SUP_SITE_CONTACT_INT_S.nextval,
                                 r_vendor_site.vendor_id,
                                 r_vendor_site.vendor_site_id,
                                 r_vendor_site.last_name,
                                 r_vendor_site.org_id,
                                 'NEW',
                                 r_vendor_site.party_site_id,
                                 r_vendor_site.area_code, 
                                 r_vendor_site.phone, 
                                 r_vendor_site.fax_area_code, 
                                 r_vendor_site.fax, 
                                 r_vendor_site.email_address);
                                
                            EXCEPTION
                                WHEN OTHERS THEN    
                                fnd_file.put_line (fnd_file.output, 'Error inserting into AP_SUP_SITE_CONTACT_INT ' ||SQLCODE || SQLERRM); 
                            END;
                
                
                
                    end loop;
                
            
            end loop;

          fnd_file.put_line (fnd_file.output, g_count || ' inserted into AP_SUP_SITE_CONTACT_INT ' ||SQLCODE || SQLERRM); 

        COMMIT;
    
    EXCEPTION
      WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    END add_purchasing_site_contacts;

   
   /*************************************************************************************************   
   *   Procedure end_old_purchasing_sites                                                           *
   *   Purpose : This updates the inactive date on Purchaisng Sites that are not PURCHASING         *
   *                                                                                                *
   *                                                                                                *
   *     procedure end_old_purchasing_sites                                                         *
   *             (p_org_id IN NUMBER,                                                               *
   *              p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
   
   
   
    procedure end_old_purchasing_sites
                (p_org_id IN NUMBER,
                 p_vendor_id IN NUMBER) is
    
        --Cursor for Vendors that don't have g_vendor_site_code SITE
        Cursor c_vendor IS
        select distinct ass.vendor_id
        from   ap_suppliers ass,
               ap_supplier_sites_all assa
        where  exists (select *
                       from   ap_supplier_sites_all assa1
                       where  assa1.vendor_id = ass.vendor_id
                       AND    assa1.vendor_site_code = g_vendor_site_code
                       AND    assa.vendor_id = assa1.vendor_id
                       AND    assa.org_id = assa1.org_id
                       and    sysdate  < nvl(assa1.inactive_date,sysdate+1)  
                       )
        and    ass.vendor_id = assa.vendor_id
        and    nvl(assa.purchasing_site_flag,'N') = 'Y'
        and    sysdate  < nvl(assa.inactive_date,sysdate+1)  
        and    assa.org_id = p_org_id
        and    ass.vendor_id = nvl(p_vendor_id, ass.vendor_id)
        ;
        
    
    
        Cursor c_vendor_site(p_vendor_id in NUMBER)  IS
        select  
            vendor_id,
            vendor_site_id
            from   ap_supplier_sites_all
            where  vendor_id = p_vendor_id
            and    purchasing_site_flag = 'Y'
            and    sysdate  < nvl(inactive_date,sysdate+1)  
            and    vendor_site_code != g_vendor_site_code
            ;
    
    
        BEGIN
        
          g_err_callpoint := 'end_old_purchasing_sites';
            
            FOR r_vendor IN c_vendor 
            
                LOOP
                
                    
                    for r_vendor_site in c_vendor_site(r_vendor.vendor_id)
                    
                        LOOP
                    
                    
                              BEGIN
                              
                                    g_count := g_count + 1;
                              
                                    update ap_supplier_sites_all set --purchasing_site_flag = 'N',
                                                 inactive_date = sysdate + 60,
                                                 last_update_date = sysdate,
                                                 last_updated_by = fnd_global.user_id,
                                                 attribute13 = 'Y'
                                    where  vendor_id = r_vendor_site.vendor_id
                                    and    vendor_site_id = r_vendor_site.vendor_site_id;
                              
                               EXCEPTION 
                                WHEN OTHERS THEN
                                
                                        fnd_file.put_line (fnd_file.output, 'Error updating ap_supplier_sites_all ' ||SQLCODE || SQLERRM); 
                                        
                    
                                END;
                            
                            
                        
                        END LOOP;
                
                    
                    END LOOP;
                    
                    
            COMMIT;
        
        
        fnd_file.put_line (fnd_file.output, g_count ||' records updated on ap_supplier_sites_all ' ||SQLCODE || SQLERRM); 
    
    EXCEPTION
      WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    
    
    
    END end_old_purchasing_sites;

   
   /*************************************************************************************************   
   *   Procedure create_purchasing_site_sr                                                          *
   *   Purpose : Creates the Purchasing Sourcing Rules and Assignments                              *
   *                                                                                                *
   *                                                                                                *
   *     procedure add_purchasing_site_contacts                                                     *
   *             (p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
    
        procedure create_purchasing_site_sr
                (p_vendor_id in NUMBER) is
                
            
            cursor c_sr is
                select ass.segment1 sourcing_rule_name, --supplier name
                       ass.segment1||'_'||ass.vendor_name sourcing_rule_desc,
                       1 sourcing_rule_type,
                       1 status,
                       'CREATE' operation,
                       1 planning_active,
                       'Buy From' Source_Type,
                       ass.segment1 vendor_num,
                       assa.vendor_site_code 
                from   ap_suppliers ass,
                       ap_supplier_sites_all assa
                where  ass.vendor_id = assa.vendor_id
                and    assa.vendor_site_code = g_vendor_site_code
                and    ass.vendor_id = nvl(p_vendor_id, ass.vendor_id)
                and    not exists (select *
                                   from   mrp_sr_source_org msso
                                   where  msso.vendor_id = assa.vendor_id
                                   and    msso.vendor_site_id = assa.vendor_site_id);
                    
            
            cursor c_sr_assignments is
            /*    select 'WC_SOURCING' assignment_set_name,
                       'WC_SOURCING' assignment_set_desc,
                       'CREATE' operation,
                       6 assignment_type,
                       sr.segment1 item_num,
                       sr.organization_code organization_code,
                       1 source_rule_type,
                       ass.segment1||'_'||assa.vendor_site_code sourcing_rule_name,
                       'P' process_status
                from   ap_suppliers ass,
                       ap_supplier_sites_all assa,
                       (select distinct mp.organization_code, msib.segment1, msso.vendor_id 
                from   mrp_assignment_sets mas,
                       MRP_SR_ASSIGNMENTS msa,
                       mrp_sr_source_org msso,
                       mrp_sourcing_rules msr,
                       mrp_sr_receipt_org msro,
                       mtl_parameters mp,
                       mtl_system_items_b msib
                where  mas.assignment_set_id = msa.assignment_set_id
                and    msa.sourcing_rule_id = msr.sourcing_rule_id
                and    msr.sourcing_rule_id = msro.sourcing_rule_id
                and    msro.sr_receipt_id = msso.sr_receipt_id
                and    mp.organization_id = msa.organization_id
                and    msib.organization_id = msa.organization_id
                and    msib.inventory_item_id = msa.inventory_item_id) sr
                where  ass.vendor_id = assa.vendor_id
                and    assa.vendor_site_code = 'g_vendor_site_code'
                and    ass.vendor_id = sr.vendor_id
                and    ass.vendor_id = nvl(p_vendor_id,ass.vendor_id)
                and    not exists (select distinct mp.organization_code, msib.segment1, msso.vendor_id 
                from   mrp_assignment_sets mas,
                       MRP_SR_ASSIGNMENTS msa,
                       mrp_sr_source_org msso,
                       mrp_sourcing_rules msr,
                       mrp_sr_receipt_org msro,
                       mtl_parameters mp,
                       mtl_system_items_b msib
                where  mas.assignment_set_id = msa.assignment_set_id
                and    msa.sourcing_rule_id = msr.sourcing_rule_id
                and    msr.sourcing_rule_id = msro.sourcing_rule_id
                and    msro.sr_receipt_id = msso.sr_receipt_id
                and    mp.organization_id = msa.organization_id
                and    msib.organization_id = msa.organization_id
                and    msib.inventory_item_id = msa.inventory_item_id
                and    msso.vendor_site_id = assa.vendor_site_id
                and    msso.vendor_id = assa.vendor_id);

            */
            select sr.organization_code organization_code,
                       sr.segment1 item_number,
                       sr.sourcing_rule_name old_sourcing_rule,
                       ass.segment1 new_sourcing_rule
                from   ap_suppliers ass,
                       ap_supplier_sites_all assa,
                       (select mp.organization_code, msib.segment1, msso.vendor_id, msr.sourcing_rule_name 
                from   mrp_assignment_sets mas,
                       MRP_SR_ASSIGNMENTS msa,
                       mrp_sr_source_org msso,
                       mrp_sourcing_rules msr,
                       mrp_sr_receipt_org msro,
                       mtl_parameters mp,
                       mtl_system_items_b msib
                where  mas.assignment_set_id = msa.assignment_set_id
                and    msa.sourcing_rule_id = msr.sourcing_rule_id
                and    msr.sourcing_rule_id = msro.sourcing_rule_id
                and    msro.sr_receipt_id = msso.sr_receipt_id
                and    mp.organization_id = msa.organization_id
                and    msib.organization_id = msa.organization_id
                and    msib.inventory_item_id = msa.inventory_item_id) sr
                where  ass.vendor_id = assa.vendor_id
                and    assa.vendor_site_code = g_vendor_site_code
                and    ass.vendor_id = sr.vendor_id
                and    ass.vendor_id = nvl(p_vendor_id,ass.vendor_id)
                and    not exists (select distinct mp.organization_code, msib.segment1, msso.vendor_id 
                from   mrp_assignment_sets mas,
                       MRP_SR_ASSIGNMENTS msa,
                       mrp_sr_source_org msso,
                       mrp_sourcing_rules msr,
                       mrp_sr_receipt_org msro,
                       mtl_parameters mp,
                       mtl_system_items_b msib
                where  mas.assignment_set_id = msa.assignment_set_id
                and    msa.sourcing_rule_id = msr.sourcing_rule_id
                and    msr.sourcing_rule_id = msro.sourcing_rule_id
                and    msro.sr_receipt_id = msso.sr_receipt_id
                and    mp.organization_id = msa.organization_id
                and    msib.organization_id = msa.organization_id
                and    msib.inventory_item_id = msa.inventory_item_id
                and    msso.vendor_site_id = assa.vendor_site_id
                and    msso.vendor_id = assa.vendor_id);
            
        BEGIN
        
          g_err_callpoint := 'create_purchasing_site_sr';
              
               for r_sr in c_sr
               
                              
                    LOOP
                    
                         BEGIN
                         
                         g_count := g_count + 1;
                         
                            insert into xxwc_sourcing_rules_conv xxwcsr
                                (sourcing_rule_name,
                                 sourcing_rule_desc,
                                 sourcing_rule_type,
                                 status,
                                 operation,
                                 planning_active,
                                 vendor_num,
                                 vendor_site_code,
                                 source_type,
                                 process_status)
                               values
                                (r_sr.sourcing_rule_name,
                                 r_sr.sourcing_rule_desc,
                                 r_sr.sourcing_rule_type,
                                 r_sr.status,
                                 r_sr.operation,
                                 r_sr.planning_active,
                                 r_sr.vendor_num,
                                 r_sr.vendor_site_code,
                                 r_sr.source_type,
                                 'R');
                            EXCEPTION
                                 WHEN OTHERS THEN
                                 
                                 fnd_file.put_line (fnd_file.output, 'Error inserting into xxwc_sourcing_rules_conv ' ||SQLCODE || SQLERRM);       
                    
                            END;
                            
                    END LOOP;
                    
               
                COMMIT;
                

              fnd_file.put_line (fnd_file.output, g_count || ' inserting into xxwc_sourcing_rules_conv '); 
                     
                     
              g_count := 0;
              
                for r_sr_assignments in c_sr_assignments
                
                    LOOP
                    
                        BEGIN
                        
                        g_count := g_count + 1;
                        
                          --Removed inserting into sr assignments table and inserting into the web adi conversion table
                         /*insert into xxwc_sr_assingments_conv
                            (assignment_set_name,
                             assignment_set_desc,
                             operation,
                             assignment_type,
                             item_num,
                             organization_code,
                             source_rule_type,
                             sourcing_rule_name,
                             process_status
                             )
                            values
                            (r_sr_assignments.assignment_set_name,
                             r_sr_assignments.assignment_set_desc,
                             r_sr_assignments.operation,
                             r_sr_assignments.assignment_type,
                             r_sr_assignments.item_num,
                             r_sr_assignments.organization_code,
                             r_sr_assignments.source_rule_type,
                             r_sr_assignments.sourcing_rule_name,
                             'R'
                             );
                           EXCEPTION
                                 WHEN OTHERS THEN
                                       
                                 dbms_output.put_line('Error inserting into xxwc_sr_assignments_conv ' ||SQLCODE || SQLERRM); 
                    
                            END;
                                             
                        */
                            --Loading into Web SR Assignments ADI Upload Table
                             insert into XXWC.XXWCMRP_SRC_RULE_ASSGN_IFACE
                                         (organization_code,
                                          item_number,
                                          old_sourcing_rule,
                                          new_sourcing_rule)
                                        values
                                        ( r_sr_assignments.organization_code,
                                          r_sr_assignments.item_number,
                                          r_sr_assignments.old_sourcing_rule,
                                          r_sr_assignments.new_sourcing_rule);
                            
                            EXCEPTION
                            
                                 WHEN OTHERS THEN
                                 
                                 fnd_file.put_line (fnd_file.output, 'Error inserting into xxwcmrp_src_rule_assgn_iface ' ||SQLCODE || SQLERRM);       
                                 
                    
                            END;
                           
                    END LOOP;
          
          fnd_file.put_line (fnd_file.output, g_count || ' inserted into xxwcmrp_src_rule_assgn_iface ');       
                                 
                COMMIT;
        
      EXCEPTION
       WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    
    
        END create_purchasing_site_sr;

  
   /*************************************************************************************************   
   *   Function get_contact_id_from_legacy                                                          *
   *   Purpose : returns the new contact id from the legacy vendor site id                          *
   *                                                                                                *
   *                                                                                                *
   *     function  get_contact_id_from_legacy                                                       *
   *             (p_vendor_id IN NUMBER,                                                            *
   *              p_vendor_site_id IN NUMBER);                                                      *
   *                RETURN NUMBER                                                                   *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/

  function  get_contact_id_from_legacy
                (p_vendor_id in NUMBER,
                 p_vendor_site_id IN NUMBER)
                 RETURN NUMBER IS
                 
   
    l_vendor_contact_id NUMBER DEFAULT NULL;
    l_vendor_site_id NUMBER;
    l_vendor_site_code VARCHAR2(15); 
  
   BEGIN
    
    g_err_callpoint := 'get_contact_id_from_legacy';
   
        BEGIN
            select vendor_site_id
            into   l_vendor_site_id
            from   ap_supplier_sites_all
            where  vendor_site_code = g_vendor_site_code
            and    vendor_id = p_vendor_id;
        EXCEPTION
             WHEN OTHERS THEN
                    raise g_exception;
        END;
         
        
        BEGIN
            select vendor_site_code
            into   l_vendor_site_code
            from   ap_supplier_sites_all
            where  vendor_site_id = p_vendor_site_id
            and    vendor_id = p_vendor_id;
        EXCEPTION
             WHEN OTHERS THEN
                    raise g_exception;
        END;
        
        
        if l_vendor_site_id is not null and l_vendor_site_code is not null then
        
        
            BEGIN
                select vendor_contact_id
                into   l_vendor_contact_id
                from   po_vendor_contacts
                where  vendor_id = p_vendor_id
                and    vendor_site_id = l_vendor_site_id
                and    last_name = l_vendor_site_code;
            EXCEPTION
                 when others then
                        raise g_exception;
            END; 
               
        END IF;
        
        
       return l_vendor_contact_id;
         
         
   EXCEPTION
        WHEN g_exception THEN
                
              l_vendor_contact_id := NULL;
              
              RETURN l_vendor_contact_id;  
  
      WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    
    
   END;

    
   /*************************************************************************************************   
   *   Function get_legacy_id_from_contact_id                                                       *
   *   Purpose : returns the new old vendor site id from the new vendor contact id                  *
   *                                                                                                *
   *                                                                                                *
   *     function  get_legacy_id_from_contact_id                                                    *
   *             (p_vendor_id IN NUMBER,                                                            *
   *              p_vendor_site_id IN NUMBER,                                                       *
   *              p_vendor_contact_id IN NUMBER)                                                    *
   *                RETURN NUMBER             ;                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   

    function  get_legacy_id_from_contact_id
                (p_vendor_id in NUMBER,
                 p_vendor_site_id in NUMBER,
                 p_vendor_contact_id IN NUMBER)
                 RETURN NUMBER IS
                 
       l_vendor_site_id NUMBER;
       l_vendor_site_code VARCHAR2(15);
                 
    BEGIN
    
      g_err_callpoint := 'get_legacy_id_from_contact_id';
    
        BEGIN
            select substr(last_name,1,15)
            into   l_vendor_site_code
            from   po_vendor_contacts
            where  vendor_id = p_vendor_id
            and    vendor_site_id = p_vendor_site_id
            and    vendor_contact_id = p_vendor_contact_id;
        EXCEPTION
            when others then    
                 raise g_exception;
        END;
        
        
        BEGIN
        
            select vendor_site_id
            into   l_vendor_site_id
            from   ap_supplier_sites_all
            where  vendor_id = p_vendor_id
            and    vendor_site_code = l_vendor_site_code;
        
        EXCEPTION
            when others then
                raise g_exception;
        END;
        
    
    return l_vendor_site_id;
    
  EXCEPTION
        when g_exception then
                l_vendor_site_id := NULL;
                return l_vendor_site_id;
         
      WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

  END get_legacy_id_from_contact_id;
    

    
   /*************************************************************************************************   
   *   Function get_purchasing_ven_site_id                                                          *
   *   Purpose : returns the vendor site id based on the default global vendor site code            *
   *                                                                                                *
   *                                                                                                *
   *     function  get_purchasing_ven_site_id                                                       *
   *             (p_vendor_id IN NUMBER)                                                            *
   *                RETURN NUMBER             ;                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/


   function  get_purchasing_ven_site_id
                (p_vendor_id in NUMBER)
                 RETURN NUMBER IS
                 
      l_vendor_site_id NUMBER DEFAULT NULL;
      
      
   BEGIN
   
    g_err_callpoint := 'get_purchasing_ven_site_id';
   
        BEGIN
            select vendor_site_id
            into   l_vendor_site_id
            from   ap_supplier_sites_all
            where  vendor_id = p_vendor_id
            and    vendor_site_code = g_vendor_site_code;
         EXCEPTION
            when others then
                    l_vendor_site_id := NULL;
         END;
         
        RETURN l_vendor_site_id; 
  
  EXCEPTION
      WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    END;
  
    
    
   /*************************************************************************************************   
   *   Procedure update_vendor_minimum_contacts                                                     *
   *   Purpose : updates the XXWC PO Vendor Minimum table with with contact ids                     *
   *                                                                                                *
   *                                                                                                *
   *     Procedure  update_vendor_minimum_contacts                                                  *
   *             (p_vendor_id IN NUMBER);                                                            *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
 
   procedure update_vendor_minimum_contacts
                (p_vendor_id in NUMBER) IS
   
   
    cursor c_vendor_minimum(p_vendor_id in NUMBER, p_vendor_site_id in NUMBER) is
        select x1.rowid row_id,
               x1.vendor_id,
               x1.vendor_site_id old_vendor_site_id
        from   xxwc.xxwc_po_vendor_minimum x1
        where  vendor_id = p_vendor_id
        and    not exists (select *
                           from   xxwc.xxwc_po_vendor_minimum x2
                           where  x1.vendor_id = x2.vendor_id
                           and    x1.vendor_site_id = x2.vendor_site_id
                           and    x2.vendor_site_id = p_vendor_site_id);
        
   
   
    cursor c_vendor is
        select vendor_site_id, vendor_id
        from   ap_supplier_sites_all
        where  vendor_site_code = g_vendor_site_code
        and    vendor_id = nvl(p_vendor_id, vendor_id);
    
    
        l_vendor_contact_id NUMBER;
        
   BEGIN
     
     g_err_callpoint := 'update_vendor_minimum_contacts';
     
           for r_vendor in c_vendor
            
                LOOP
                
                    
                    for r_vendor_minimum in c_vendor_minimum (r_vendor.vendor_id, r_vendor.vendor_site_id)
                    
                        LOOP
                        
                           l_vendor_contact_id := XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(r_vendor_minimum.vendor_id, r_vendor_minimum.old_vendor_site_id);
                               
                                    BEGIN
                                      
                                      g_count := g_count + 1;
                                    
                                        update xxwc.xxwc_po_vendor_minimum
                                            set  vendor_site_id = r_vendor.vendor_site_id,
                                                 vendor_contact_id = l_vendor_contact_id,
                                                 last_update_date = sysdate,
                                                 last_updated_by = fnd_global.user_id
                                         where rowid = r_vendor_minimum.row_id
                                         and   vendor_id = r_vendor_minimum.vendor_id
                                         and   vendor_site_id = r_vendor_minimum.old_vendor_site_id;
                                         
                                    EXCEPTION
                                        WHEN others THEN
                                                ROLLBACK;
                                              fnd_file.put_line (fnd_file.output, ' error updating xxwc_po_vendor_minimum ' || SQLCODE || SQLERRM);
                                     END;
                                     
                                     
                                     COMMIT;
                                      
                           
                        
                        
                        END LOOP;
                    
                           
                
               fnd_file.put_line (fnd_file.output, g_count || ' updated xxwc_po_vendor_minimum ');
                              
                END LOOP;
                
    EXCEPTION
    
     WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    
   END update_vendor_minimum_contacts;

  
   /*************************************************************************************************   
   *   procedure update_vendor_minimum_default                                                      *
   *   Purpose : updates the vendor minimum default flag on the XXWC PO Vendor Minimum table        *
   *                                                                                                *
   *                                                                                                *
   *     procedure update_vendor_minimum_default                                                    *
   *             (p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
  
   
   procedure update_vendor_minimum_default
                (p_vendor_id in NUMBER) IS
   
   
    cursor c_vendor_minimum(p_vendor_id in NUMBER, p_vendor_site_id in NUMBER) is
        select x1.rowid row_id,
               x1.vendor_id,
               x1.vendor_site_id,
               x1.organization_id,
               x1.vendor_contact_id
        from   xxwc.xxwc_po_vendor_minimum x1
        where  vendor_id = p_vendor_id
        and    vendor_site_id = p_vendor_site_id;
        
    cursor c_vendor is
        select vendor_site_id, vendor_id
        from   ap_supplier_sites_all
        where  vendor_site_code = g_vendor_site_code
        and    vendor_id = nvl(p_vendor_id, vendor_id);
    
    
        l_default_flag VARCHAR2(1);
        
        l_default_vendor_site_id NUMBER;
        l_legacy_vendor_site_id NUMBER;
        
        
   BEGIN
   
    g_err_callpoint := 'update_vendor_minimum_default';  
    
           for r_vendor in c_vendor
            
                LOOP
                
                    
                    for r_vendor_minimum in c_vendor_minimum (r_vendor.vendor_id, r_vendor.vendor_site_id)
                    
                        LOOP
                        
                           l_default_vendor_site_id := XXWC_BPA_CONVERSIONS_PKG.GET_DEFAULT_VENDOR_SITE_ID(r_vendor_minimum.vendor_id,r_vendor_minimum.organization_id);
                           
                           l_legacy_vendor_site_id := XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id);
                           
                           
                           if l_default_vendor_site_id = l_legacy_vendor_site_id then
                           
                              l_default_flag := 'Y';
                              
                          else 
                          
                              l_default_flag := 'N';
                              
                          end if;
                           
                           /*
                                    BEGIN
                                        select 'Y'
                                        into   l_default_flag
                                        from   xxwc.xxwc_mrp_sr_assignments_bk msa,
                                               mrp_sourcing_rules msr,
                                               mrp_sr_receipt_org msro,
                                               mrp_sr_source_org msso
                                        where  msso.vendor_id = r_vendor_minimum.vendor_id
                                        and    msso.vendor_site_id = XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id)
                                        and    msa.organization_id = r_vendor_minimum.organization_id
                                        and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                                        and    msr.sourcing_rule_id = msro.sourcing_rule_id
                                        and    msro.sr_receipt_id = msso.sr_receipt_id
                                        and    msa.sourcing_rule_id = msr.sourcing_rule_id
                                        and    msa.ASSIGNMENT_TYPE = 6
                                        and    rownum = 1;
                                    EXCEPTION
                                        when no_data_found THEN
                                                l_default_flag := 'N';
                                        when others then
                                                l_default_flag := NULL;
                                    END;
                                        
                            */
                            
                            
                                    BEGIN
                                    
                                      g_count := g_count + 1;
                                    
                                        update xxwc.xxwc_po_vendor_minimum
                                            set  default_flag = l_default_flag,
                                                 last_update_date = sysdate,
                                                 last_updated_by = fnd_global.user_id
                                         where rowid = r_vendor_minimum.row_id
                                         and   vendor_id = r_vendor_minimum.vendor_id
                                         and   vendor_site_id = r_vendor_minimum.vendor_site_id
                                         and   vendor_contact_id = r_vendor_minimum.vendor_contact_id
                                         and   organization_id = r_vendor_minimum.organization_id;
                                    exception
                                        when others then
                                            ROLLBACK;
                                            fnd_file.put_line (fnd_file.output, ' error updating default flag on xxwc_po_vendor_minimum ' || SQLCODE || SQLERRM);
                                     END;
                                     
                                     COMMIT;
                                         
                                    
                        
                        END LOOP;
                    
                           
                
                              
                END LOOP;
                
                fnd_file.put_line (fnd_file.output, g_count || ' updated xxwc_po_vendor_minimum ');
   
   EXCEPTION
   
     WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

            
   
   END update_vendor_minimum_default;


   /*************************************************************************************************   
   *   procedure create_freight_burden_pur_site                                                     *
   *   Purpose - Creates a freight burden entry for the global vendor site code                     *
   *                                                                                                *
   *                                                                                                *
   *    procedure  update_vendor_minimum_default                                                    *
   *             (p_vendor_id IN NUMBER)                                                            *
   *                RETURN NUMBER             ;                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/

   
   procedure create_freight_burden_pur_site
                (p_vendor_id in NUMBER) IS
                
    cursor c_vendor_minimum(p_vendor_id in NUMBER, p_vendor_site_id in NUMBER) is
        select distinct --x1.rowid row_id,
               x1.vendor_id,
               p_vendor_site_id vendor_site_id,
               x1.organization_id,
               x1.freight_duty,
               x1.import_flag,
               x1.freight_per_lb,
               x1.source_organization_id,
               x1.freight_basis,
               x1.vendor_type
        from   xxwc.xxwc_po_freight_burden_tbl x1
        where  vendor_id = p_vendor_id
        and    not exists (select *
                           from   xxwc.xxwc_po_freight_burden_tbl x2
                           where  x1.vendor_id = x2.vendor_id
                           and    x1.vendor_site_id = x2.vendor_site_id
                           and    x2.vendor_site_id = p_vendor_site_id);
         
        
   
   
    cursor c_vendor is
        select vendor_site_id, vendor_id
        from   ap_supplier_sites_all
        where  vendor_site_code = g_vendor_site_code
        and    vendor_id = nvl(p_vendor_id, vendor_id);
    
    
        l_vendor_contact_id NUMBER;
        
   BEGIN
     
     g_err_callpoint := 'create_freight_burden_pur_site';
     
           for r_vendor in c_vendor
            
                LOOP
                
                    
                    for r_vendor_minimum in c_vendor_minimum (r_vendor.vendor_id, r_vendor.vendor_site_id)
                    
                        LOOP
                        
                           /*l_vendor_contact_id := XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(r_vendor_minimum.vendor_id, r_vendor_minimum.old_vendor_site_id);
                               
                                    BEGIN
                                    
                                        update xxwc.xxwc_po_freight_burden_tbl
                                            set  vendor_site_id = r_vendor.vendor_site_id,
                                                 --vendor_contact_id = l_vendor_contact_id,
                                                 last_update_date = sysdate,
                                                 last_updated_by = fnd_global.user_id
                                         where rowid = r_vendor_minimum.row_id
                                         and   vendor_id = r_vendor_minimum.vendor_id
                                         and   vendor_site_id = r_vendor_minimum.old_vendor_site_id;
                                         
                                    EXCEPTION
                                        WHEN others THEN
                                                dbms_output.put_line(SQLCODE||SQLERRM);
                                                rollback;
                                     END;
                                     
                                     
                                     COMMIT;
                                      
                           
                        */
                        
                                    BEGIN
                                    
                                      g_count := g_count + 1;
                                    
                                        insert into xxwc.xxwc_po_freight_burden_tbl
                                                (vendor_id,
                                                 vendor_site_id,
                                                 organization_id,
                                                 freight_duty,
                                                 import_flag,
                                                 freight_per_lb,
                                                 source_organization_id,
                                                 freight_basis,
                                                 vendor_type,
                                                 creation_date,
                                                 created_by,
                                                 last_update_date,
                                                 last_updated_by,
                                                 last_update_login)
                                             values
                                                (r_vendor_minimum.vendor_id,
                                                 r_vendor_minimum.vendor_site_id,
                                                 r_vendor_minimum.organization_id,
                                                 r_vendor_minimum.freight_duty,
                                                 r_vendor_minimum.import_flag,
                                                 r_vendor_minimum.freight_per_lb,
                                                 r_vendor_minimum.source_organization_id,
                                                 r_vendor_minimum.freight_basis,
                                                 r_vendor_minimum.vendor_type,
                                                 sysdate,
                                                 fnd_global.user_id,
                                                 sysdate,
                                                 fnd_global.user_id,
                                                 fnd_global.login_id);
                                             
                                    EXCEPTION
                                        WHEN others THEN
                                                fnd_file.put_line (fnd_file.output, ' error inserting into xxwc_po_freight_burden_tbl ' || SQLCODE || SQLERRM);
                                                rollback;
                                     END;
                                     
                                     
                                     COMMIT;
                        
                        
                        
                        END LOOP;
                    
                        
                        fnd_file.put_line (fnd_file.output, g_count || ' inserted into xxwc_po_freight_burden_tbl ');
                                                
                        
                              
                END LOOP;
    
    EXCEPTION
    
      WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    
                
     end  create_freight_burden_pur_site;


   /*************************************************************************************************   
   *   procedure launch_ccr                                                                         *
   *   Purpose - Main Concurrent program for XXWC BPA Price Zone Import                             *
   *                                                                                                *
   *   procedure launch_ccr (errbuf   OUT VARCHAR2                                                  *
   *                      ,retcode  OUT NUMBER                                                      *
   *                      ,p_org_id  IN NUMBER                                                      *
   *                      ,p_program IN VARCHAR2                                                    *
   *                      ,P_VENDOR_ID IN NUMBER                                                    *
   *                      ,p_vendor_site_code IN VARCHAR2);                                         *
   *                      ,p_vendor_param_list_id IN NUMBER); --Added Version 1.1 20140726-00007    *
   *                                                                                                *
   *                                                                                                *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        08/05/2014   Lee Spitzer              TMS Ticket 20140726-00007                   *
   *                                                       Upgrade program to use Supplier list     *
   *   1.3        01/19/2015  Lee Spitzer                TMS Ticket 20140909-00029                  *
   *                                                        Create 1 SEE NOTES contact              *
   *                                                                                                *
   /************************************************************************************************/

     procedure launch_ccr (errbuf   OUT VARCHAR2
                          ,retcode  OUT NUMBER
                          ,p_org_id IN NUMBER
                          ,p_program IN VARCHAR2
                          ,P_VENDOR_ID IN NUMBER
                          ,p_vendor_site_code IN VARCHAR2
                          ,p_vendor_param_list_id IN NUMBER) is
                          
      --TMS Ticket 20140726-00007 Cursor added to use for vendor list                    
      CURSOR c_supplier_list IS
        SELECT ass.vendor_id
        FROM   xxwc.xxwc_param_list_values xplv,
               ap_suppliers ass
        WHERE  xplv.list_id = p_vendor_param_list_id
        AND    xplv.list_value = ass.segment1
        AND    p_vendor_param_list_id IS NOT NULL
        UNION
        SELECT ass.vendor_id
        FROM   ap_suppliers ass
        WHERE  ass.vendor_id = nvl(p_vendor_id, ass.vendor_id)
        and    p_vendor_param_list_id IS NULL;

                          
     begin
     
     g_err_callpoint := 'launch_ccr';
     
     --Set the global vendor site code to be the parameter vendor site code otherwise it will take the default of PURCHASING
     IF P_VENDOR_SITE_CODE IS not NULL THEN
     
        g_vendor_site_code := p_vendor_site_code;
     
     END IF;
     
     --Added Fnd Put Line for concurrent program parameters --Added Version 1.1 20140726-00007
     fnd_file.put_line (fnd_file.LOG, 'p_org_id ' || p_org_id);
     fnd_file.put_line (fnd_file.LOG, 'p_program ' || p_program);
     fnd_file.put_line (fnd_file.LOG, 'p_vendor_id ' || p_vendor_id);
     fnd_file.put_line (fnd_file.LOG, 'p_vendor_site_code ' || p_vendor_site_code);
     fnd_file.put_line (fnd_file.LOG, 'p_vendor_param_list_id ' || p_vendor_param_list_id);                          
                          
     --Added Cursor Loop
     
     --Added Version 1.1 20140726-00007 logic that will call the api to update the list id
     
     if p_vendor_param_list_id is not null then
     
        fnd_file.put_line (fnd_file.LOG, 'Calling API to update List Id for list id ' || p_vendor_param_list_id);                          
      	xxwc_inv_prod_cloning.read_param_list (p_vendor_param_list_id);
     
     end if;
     
     
     
     FOR  r_supplier_list IN c_supplier_list 
     
      LOOP
     
     
        IF p_program = 'add_purchasing_site_contacts' THEN
          
          
              --add_purchasing_site_contacts(p_org_id, p_vendor_id); --Original Removed Version 1.1 20140726-00007
            
              add_purchasing_site_contacts(p_org_id, r_supplier_list.vendor_id); --Added Version 1.1 20140726-00007
              
            
        elsif p_program = 'add_purchasing_supplier_site' then
        
          
               --add_purchasing_supplier_site(p_org_id, p_vendor_id); --Original Removed Version 1.1 20140726-00007
          
                add_purchasing_supplier_site(p_org_id, r_supplier_list.vendor_id); --Added Version 1.1 20140726-00007
              
            
        elsif p_program = 'create_freight_burden_pur_site' then
        
               --create_freight_burden_pur_site(p_vendor_id); --Original Removed Version 1.1 20140726-00007
          
                create_freight_burden_pur_site(r_supplier_list.vendor_id); --Added Version 1.1 20140726-00007
              
        elsif p_program = 'create_purchasing_site_sr' then
  
               --create_purchasing_site_sr(p_vendor_id); --Original Removed Version 1.1 20140726-00007
  
               create_purchasing_site_sr(r_supplier_list.vendor_id);  --Added Version 1.1 20140726-00007
              
           
        elsif p_program = 'end_old_purchasing_sites' then
        
               --end_old_purchasing_sites(p_org_id, p_vendor_id); --Original Removed Version 1.1 20140726-00007 
          
               end_old_purchasing_sites(p_org_id, r_supplier_list.vendor_id);  --Added Version 1.1 20140726-00007
              
        elsif p_program = 'update_vendor_minimum_contacts' then
        
               --update_vendor_minimum_contacts(p_vendor_id);  --Original Removed Version 1.1 20140726-00007
              
               update_vendor_minimum_contacts(r_supplier_list.vendor_id);  --Added Version 1.1 20140726-00007
              
        elsif p_program = 'update_vendor_minimum_default' then
  
              --update_vendor_minimum_default(p_vendor_id); --Original Removed Version 1.1 20140726-00007
          
              update_vendor_minimum_default(r_supplier_list.vendor_id); --Added Version 1.1 20140726-00007
              
        
        elsif p_program = 'update_vendor_address' then
  
              --update_vendor_minimum_default(p_vendor_id); --Original Removed Version 1.1 20140726-00007
          
              update_vendor_address(p_org_id, r_supplier_list.vendor_id, p_vendor_site_code); --Added Version 1.1 20140726-00007
              
        --Added 1/19/2015 for 1 SEE NOTES
        elsif p_program = 'add_1_see_notes_contact' THEN
        
            add_1_see_notes_contact(p_org_id, r_supplier_list.vendor_id);
        
        --Added 1/19/2015 for 1 SEE NOTES
        elsif p_program = 'add_1_see_notes_supplier_site' THEN
        
          add_1_see_notes_supplier_site(p_org_id, r_supplier_list.vendor_id);
        
        else
        
            retcode := 2;
            
        end if;
          
    END LOOP;
    
    retcode := 0;
   
   EXCEPTION
   
     WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

       
   end launch_ccr;
                          
   /*************************************************************************************************   
   *   Function get_default_vendor_site_id                                                          *
   *   Purpose : Function used in the conversion process to determine which legacy vendor site      *
   *       had the most sourcing assignments                                                        *
   *                                                                                                *
   *                                                                                                *
   *     function  get_default_vendor_site_id                                                       *
   *             (p_vendor_id IN NUMBER,                                                            *
   *              p_organization_id IN NUMBER) --Can be NULL                                        *       
   *                RETURN NUMBER             ;                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        08/05/2014   Lee Spitzer              TMS Ticket 20140726-00007                   *
   *                                                       Filter only for stock items              *
   *                                                                                                *
   *                                                                                                *
   /************************************************************************************************/
     
      
      FUNCTION get_default_vendor_site_id
                        (p_vendor_id IN NUMBER
                        ,p_organization_id IN NUMBER
                        )  
          RETURN NUMBER IS
                        
                        
      
        l_vendor_site_id NUMBER;
        l_organization_id NUMBER;
        l_max_count NUMBER;
        l_max_count2 NUMBER;
        l_too_many_rows EXCEPTION;
      
      BEGIN
      
         g_err_callpoint :='get_default_vendor_site_id';
         
          
              if p_organization_id is not null then
          
                BEGIN
                                SELECT nvl(MAX(count_max),0)
                                into   l_max_count
                                FROM (
                                SELECT  msso.VENDOR_ID, msso.VENDOR_SITE_ID, msa.organization_id, count(msa.inventory_item_id) count_max
                                        FROM   --XXWC.XXWC_MRP_SR_ASSIGNMENTS_BK msa,
                                               mrp_sr_assignments msa,
                                               mrp_sourcing_rules msr,
                                               mrp_sr_receipt_org msro,
                                               mrp_sr_source_org msso
                                        --WHERE  msso.vendor_id = r_vendor_minimum.vendor_id
                                        --AND    msso.vendor_site_id = XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id)
                                        -----------
                                        WHERE  msso.vendor_id = p_vendor_id
                                        AND    msa.organization_id = p_organization_id
                                        --and    msa.organization_id = r_vendor_minimum.organization_id
                                        --and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                                        and    msr.sourcing_rule_id = msro.sourcing_rule_id
                                        and    msro.sr_receipt_id = msso.sr_receipt_id
                                        AND    msa.sourcing_rule_id = msr.sourcing_rule_id
                                        AND    msa.ASSIGNMENT_TYPE = 6
                                        --Added Exists Statement 1.1 - 20140726-00007 to include only stock items
                                        AND   EXISTS (
                                                      SELECT mcs.category_set_id, mcs.category_set_name, mck.concatenated_segments
                                                      FROM   mtl_category_sets mcs,
                                                             mtl_categories_kfv mck,
                                                             mtl_item_categories mic
                                                      WHERE  mcs.category_set_name = 'Sales Velocity'
                                                      AND    mcs.category_set_id =  mic.category_set_id
                                                      AND    mck.category_id = mic.category_id
                                                      AND    mck.concatenated_segments IN ('1','2','3','4','5','6','7','8','9')
                                                      AND    mic.inventory_item_id = msa.inventory_item_id
                                                      AND    mic.organization_id = msa.organization_id)
                                        --Added Exists Statement 1.1 - 20140726-00007 to not include NON EDI
                                        AND   EXISTS (
                                                          SELECT assa.vendor_site_code
                                                          FROM   ap_supplier_sites_all assa
                                                          WHERE  assa.vendor_id = msso.vendor_id
                                                          AND    assa.vendor_site_id = msso.vendor_site_id
                                                          AND    assa.vendor_site_code != 'NON EDI')
                                        and   xxwcinv_inv_aging_pkg.check_special(msa.organization_id, msa.inventory_item_id) = 'No' -- -Added 20140726-00007 to exclude specials
                                        group by msso.VENDOR_ID, msso.VENDOR_SITE_ID, msa.organization_id);
                            
                            EXCEPTION
                                WHEN too_many_rows THEN
                                    
                                    BEGIN
                                     
                                      SELECT nvl(MAX(count_max),0)
                                      INTO   l_max_count
                                      FROM (SELECT  msso.VENDOR_ID, msso.VENDOR_SITE_ID, msa.organization_id, count(msa.inventory_item_id) count_max
                                        FROM   --XXWC.XXWC_MRP_SR_ASSIGNMENTS_BK msa,
                                               mrp_sr_assignments msa,
                                               mrp_sourcing_rules msr,
                                               mrp_sr_receipt_org msro,
                                               mrp_sr_source_org msso
                                        --WHERE  msso.vendor_id = r_vendor_minimum.vendor_id
                                        --AND    msso.vendor_site_id = XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id)
                                        -----------
                                        WHERE  msso.vendor_id = p_vendor_id
                                        AND    msa.organization_id = p_organization_id
                                        --and    msa.organization_id = r_vendor_minimum.organization_id
                                        --and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                                        and    msr.sourcing_rule_id = msro.sourcing_rule_id
                                        and    msro.sr_receipt_id = msso.sr_receipt_id
                                        AND    msa.sourcing_rule_id = msr.sourcing_rule_id
                                        AND    msa.ASSIGNMENT_TYPE = 6
                                        --Added Exists Statement 1.1 - 20140726-00007 to include only stock items
                                        AND   EXISTS (
                                                      SELECT mcs.category_set_id, mcs.category_set_name, mck.concatenated_segments
                                                      FROM   mtl_category_sets mcs,
                                                             mtl_categories_kfv mck,
                                                             mtl_item_categories mic
                                                      WHERE  mcs.category_set_name = 'Sales Velocity'
                                                      AND    mcs.category_set_id =  mic.category_set_id
                                                      AND    mck.category_id = mic.category_id
                                                      AND    mck.concatenated_segments IN ('1','2','3','4','5','6','7','8','9')
                                                      AND    mic.inventory_item_id = msa.inventory_item_id
                                                      AND    mic.organization_id = msa.organization_id)
                                        --Added Exists Statement 1.1 - 20140726-00007 to not include NON EDI
                                        AND   EXISTS (
                                                          SELECT assa.vendor_site_code
                                                          FROM   ap_supplier_sites_all assa
                                                          WHERE  assa.vendor_id = msso.vendor_id
                                                          AND    assa.vendor_site_id = msso.vendor_site_id
                                                          AND    assa.vendor_site_code != 'NON EDI')
                                        AND   xxwcinv_inv_aging_pkg.check_special(msa.organization_id, msa.inventory_item_id) = 'No' -- -Added 20140726-00007 to exclude specials
                                        GROUP BY msso.VENDOR_ID, msso.VENDOR_SITE_ID, msa.organization_id)
                                        WHERE ROWNUM = 1 -- added rownum
                                        ;
                                    
                                    exception
                                        WHEN others THEN
                                              l_max_count := 0;
                                    END;
                                        
                                WHEN others THEN
                                        l_max_count := 0;
                                        
                            END;
                            
                            
                            
                            if l_max_count > 0 then
                            
                            
                                    BEGIN
                                        SELECT  msso.VENDOR_SITE_ID, msa.organization_id, count(msa.inventory_item_id) count_max
                                        INTO   l_vendor_site_id, l_organization_id, l_max_count2
                                        FROM   --XXWC.XXWC_MRP_SR_ASSIGNMENTS_BK msa,
                                               mrp_sr_assignments msa,
                                               mrp_sourcing_rules msr,
                                               mrp_sr_receipt_org msro,
                                               mrp_sr_source_org msso
                                        --WHERE  msso.vendor_id = r_vendor_minimum.vendor_id
                                        --AND    msso.vendor_site_id = XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id)
                                        -----------
                                        WHERE  msso.vendor_id = p_vendor_id
                                        and    msa.organization_Id = p_organization_id
                                        --and    msa.organization_id = r_vendor_minimum.organization_id
                                        --and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                                        and    msr.sourcing_rule_id = msro.sourcing_rule_id
                                        and    msro.sr_receipt_id = msso.sr_receipt_id
                                        AND    msa.sourcing_rule_id = msr.sourcing_rule_id
                                        AND    msa.ASSIGNMENT_TYPE = 6
                                        --Added Exists Statement 1.1 - 20140726-00007 to include only stock items
                                        AND   EXISTS (
                                                      SELECT mcs.category_set_id, mcs.category_set_name, mck.concatenated_segments
                                                      FROM   mtl_category_sets mcs,
                                                             mtl_categories_kfv mck,
                                                             mtl_item_categories mic
                                                      WHERE  mcs.category_set_name = 'Sales Velocity'
                                                      AND    mcs.category_set_id =  mic.category_set_id
                                                      AND    mck.category_id = mic.category_id
                                                      AND    mck.concatenated_segments IN ('1','2','3','4','5','6','7','8','9')
                                                      AND    mic.inventory_item_id = msa.inventory_item_id
                                                      AND    mic.organization_id = msa.organization_id)
                                        --Added Exists Statement 1.1 - 20140726-00007 to not include NON EDI
                                        AND   EXISTS (
                                                          SELECT assa.vendor_site_code
                                                          FROM   ap_supplier_sites_all assa
                                                          WHERE  assa.vendor_id = msso.vendor_id
                                                          AND    assa.vendor_site_id = msso.vendor_site_id
                                                          AND    assa.vendor_site_code != 'NON EDI')
                                        AND   xxwcinv_inv_aging_pkg.check_special(msa.organization_id, msa.inventory_item_id) = 'No' -- -Added 20140726-00007 to exclude specials
                                        GROUP BY msso.VENDOR_ID, msso.VENDOR_SITE_ID, msa.organization_id
                                        HAVING  count(msa.inventory_item_id) = l_max_count;
                                      
                                    EXCEPTION
                                    
                                      when too_many_rows then
                                      
                                        BEGIN
                                          
                                          select vendor_site_id, organization_id, count_max
                                          INTO   l_vendor_site_id, l_organization_id, l_max_count2
                                          FROM  (
                                          SELECT  msso.VENDOR_SITE_ID, msa.organization_id, count(msa.inventory_item_id) count_max
                                          FROM   --XXWC.XXWC_MRP_SR_ASSIGNMENTS_BK msa,
                                                 mrp_sr_assignments msa,
                                                 mrp_sourcing_rules msr,
                                                 mrp_sr_receipt_org msro,
                                                 mrp_sr_source_org msso
                                          --WHERE  msso.vendor_id = r_vendor_minimum.vendor_id
                                          --AND    msso.vendor_site_id = XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id)
                                          -----------
                                          WHERE  msso.vendor_id = p_vendor_id
                                          and    msa.organization_Id = p_organization_id
                                          --and    msa.organization_id = r_vendor_minimum.organization_id
                                          --and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                                          and    msr.sourcing_rule_id = msro.sourcing_rule_id
                                          and    msro.sr_receipt_id = msso.sr_receipt_id
                                          AND    msa.sourcing_rule_id = msr.sourcing_rule_id
                                          AND    msa.ASSIGNMENT_TYPE = 6
                                          --Added Exists Statement 1.1 - 20140726-00007 to include only stock items
                                          AND   EXISTS (
                                                        SELECT mcs.category_set_id, mcs.category_set_name, mck.concatenated_segments
                                                        FROM   mtl_category_sets mcs,
                                                               mtl_categories_kfv mck,
                                                               mtl_item_categories mic
                                                        WHERE  mcs.category_set_name = 'Sales Velocity'
                                                        AND    mcs.category_set_id =  mic.category_set_id
                                                        AND    mck.category_id = mic.category_id
                                                        AND    mck.concatenated_segments IN ('1','2','3','4','5','6','7','8','9')
                                                        AND    mic.inventory_item_id = msa.inventory_item_id
                                                        AND    mic.organization_id = msa.organization_id)
                                          --Added Exists Statement 1.1 - 20140726-00007 to not include NON EDI
                                          AND   EXISTS (
                                                            SELECT assa.vendor_site_code
                                                            FROM   ap_supplier_sites_all assa
                                                            WHERE  assa.vendor_id = msso.vendor_id
                                                            AND    assa.vendor_site_id = msso.vendor_site_id
                                                            AND    assa.vendor_site_code != 'NON EDI')
                                          AND   xxwcinv_inv_aging_pkg.check_special(msa.organization_id, msa.inventory_item_id) = 'No' -- -Added 20140726-00007 to exclude specials
                                          GROUP BY msso.VENDOR_ID, msso.VENDOR_SITE_ID, msa.organization_id
                                          HAVING  count(msa.inventory_item_id) = l_max_count)
                                          WHERE  ROWNUM = 1;
                                          
                                        exception
                                           WHEN others THEN
                                            l_organization_id := null;         
                                            l_vendor_site_Id := NULL;
                                            l_max_count2 := 0;

                                        end;
                                    
                                      WHEN others THEN
                                            
                                            l_organization_id := null;         
                                            l_vendor_site_Id := NULL;
                                            l_max_count2 := null;
                                    
                                    END;
                                      
                                      
                           else
                           
                              l_vendor_site_id := NULL;
                              l_organization_id := null;
                              
                            end if;
                                        

             ELSE
             
                          BEGIN
                                SELECT nvl(MAX(count_max),0)
                                into   l_max_count
                                FROM (
                                SELECT  msso.VENDOR_ID, msso.VENDOR_SITE_ID, count(msa.inventory_item_id) count_max
                                        FROM   --XXWC.XXWC_MRP_SR_ASSIGNMENTS_BK msa,
                                               mrp_sr_assignments msa,
                                               mrp_sourcing_rules msr,
                                               mrp_sr_receipt_org msro,
                                               mrp_sr_source_org msso
                                        --WHERE  msso.vendor_id = r_vendor_minimum.vendor_id
                                        --AND    msso.vendor_site_id = XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id)
                                        -----------
                                        WHERE  msso.vendor_id = p_vendor_id
                                        --and    msa.organization_id = r_vendor_minimum.organization_id
                                        --and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                                        and    msr.sourcing_rule_id = msro.sourcing_rule_id
                                        and    msro.sr_receipt_id = msso.sr_receipt_id
                                        AND    msa.sourcing_rule_id = msr.sourcing_rule_id
                                        AND    msa.ASSIGNMENT_TYPE = 6
                                        --Added Exists Statement 1.1 - 20140726-00007 to include only stock items
                                        AND   EXISTS (
                                                      SELECT mcs.category_set_id, mcs.category_set_name, mck.concatenated_segments
                                                      FROM   mtl_category_sets mcs,
                                                             mtl_categories_kfv mck,
                                                             mtl_item_categories mic
                                                      WHERE  mcs.category_set_name = 'Sales Velocity'
                                                      AND    mcs.category_set_id =  mic.category_set_id
                                                      AND    mck.category_id = mic.category_id
                                                      AND    mck.concatenated_segments IN ('1','2','3','4','5','6','7','8','9')
                                                      AND    mic.inventory_item_id = msa.inventory_item_id
                                                      AND    mic.organization_id = msa.organization_id)
                                        --Added Exists Statement 1.1 - 20140726-00007 to not include NON EDI
                                        AND   EXISTS (
                                                          SELECT assa.vendor_site_code
                                                          FROM   ap_supplier_sites_all assa
                                                          WHERE  assa.vendor_id = msso.vendor_id
                                                          AND    assa.vendor_site_id = msso.vendor_site_id
                                                          AND    assa.vendor_site_code != 'NON EDI')
                                        AND   xxwcinv_inv_aging_pkg.check_special(msa.organization_id, msa.inventory_item_id) = 'No' -- -Added 20140726-00007 to exclude specials
                                        group by msso.VENDOR_ID, msso.VENDOR_SITE_ID);
                            
                            EXCEPTION
                                WHEN too_many_rows THEN
                                    BEGIN
                                        SELECT nvl(MAX(count_max),0)
                                        into   l_max_count
                                        FROM (
                                        SELECT  msso.VENDOR_ID, msso.VENDOR_SITE_ID, count(msa.inventory_item_id) count_max
                                                FROM   --XXWC.XXWC_MRP_SR_ASSIGNMENTS_BK msa,
                                                       mrp_sr_assignments msa,
                                                       mrp_sourcing_rules msr,
                                                       mrp_sr_receipt_org msro,
                                                       mrp_sr_source_org msso
                                                --WHERE  msso.vendor_id = r_vendor_minimum.vendor_id
                                                --AND    msso.vendor_site_id = XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id)
                                                -----------
                                                WHERE  msso.vendor_id = p_vendor_id
                                                --and    msa.organization_id = r_vendor_minimum.organization_id
                                                --and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                                                and    msr.sourcing_rule_id = msro.sourcing_rule_id
                                                and    msro.sr_receipt_id = msso.sr_receipt_id
                                                AND    msa.sourcing_rule_id = msr.sourcing_rule_id
                                                AND    msa.ASSIGNMENT_TYPE = 6
                                                --Added Exists Statement 1.1 - 20140726-00007 to include only stock items
                                                AND   EXISTS (
                                                              SELECT mcs.category_set_id, mcs.category_set_name, mck.concatenated_segments
                                                              FROM   mtl_category_sets mcs,
                                                                     mtl_categories_kfv mck,
                                                                     mtl_item_categories mic
                                                              WHERE  mcs.category_set_name = 'Sales Velocity'
                                                              AND    mcs.category_set_id =  mic.category_set_id
                                                              AND    mck.category_id = mic.category_id
                                                              AND    mck.concatenated_segments IN ('1','2','3','4','5','6','7','8','9')
                                                              AND    mic.inventory_item_id = msa.inventory_item_id
                                                              AND    mic.organization_id = msa.organization_id)
                                                --Added Exists Statement 1.1 - 20140726-00007 to not include NON EDI
                                                AND   EXISTS (
                                                                  SELECT assa.vendor_site_code
                                                                  FROM   ap_supplier_sites_all assa
                                                                  WHERE  assa.vendor_id = msso.vendor_id
                                                                  AND    assa.vendor_site_id = msso.vendor_site_id
                                                                  AND    assa.vendor_site_code != 'NON EDI')
                                                AND   xxwcinv_inv_aging_pkg.check_special(msa.organization_id, msa.inventory_item_id) = 'No' -- -Added 20140726-00007 to exclude specials
                                                        GROUP BY msso.VENDOR_ID, msso.VENDOR_SITE_ID)
                                                        WHERE    ROWNUM = 1 --added rownum
                                                        ;
                                    
                                    EXCEPTION
                                        WHEN others THEN
                                                l_max_count := 0;
                                                
                                    END;
                          
                                WHEN others THEN
                                        l_max_count := 0;
                                        
                            END;
                            
                            
                            
                            if l_max_count > 0 then
                            
                            
                                    BEGIN
                                        SELECT  msso.VENDOR_SITE_ID, count(msa.inventory_item_id) count_max
                                        INTO   l_vendor_site_id, l_max_count2
                                        FROM   --XXWC.XXWC_MRP_SR_ASSIGNMENTS_BK msa,
                                               mrp_sr_assignments msa,
                                               mrp_sourcing_rules msr,
                                               mrp_sr_receipt_org msro,
                                               mrp_sr_source_org msso
                                        --WHERE  msso.vendor_id = r_vendor_minimum.vendor_id
                                        --AND    msso.vendor_site_id = XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id)
                                        -----------
                                        WHERE  msso.vendor_id = p_vendor_id
                                        --and    msa.organization_id = r_vendor_minimum.organization_id
                                        --and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                                        and    msr.sourcing_rule_id = msro.sourcing_rule_id
                                        and    msro.sr_receipt_id = msso.sr_receipt_id
                                        AND    msa.sourcing_rule_id = msr.sourcing_rule_id
                                        AND    msa.ASSIGNMENT_TYPE = 6
                                        --Added Exists Statement 1.1 - 20140726-00007 to include only stock items
                                        AND   EXISTS (
                                                      SELECT mcs.category_set_id, mcs.category_set_name, mck.concatenated_segments
                                                      FROM   mtl_category_sets mcs,
                                                             mtl_categories_kfv mck,
                                                             mtl_item_categories mic
                                                      WHERE  mcs.category_set_name = 'Sales Velocity'
                                                      AND    mcs.category_set_id =  mic.category_set_id
                                                      AND    mck.category_id = mic.category_id
                                                      AND    mck.concatenated_segments IN ('1','2','3','4','5','6','7','8','9')
                                                      AND    mic.inventory_item_id = msa.inventory_item_id
                                                      AND    mic.organization_id = msa.organization_id)
                                        --Added Exists Statement 1.1 - 20140726-00007 to not include NON EDI
                                        AND   EXISTS (
                                                          SELECT assa.vendor_site_code
                                                          FROM   ap_supplier_sites_all assa
                                                          WHERE  assa.vendor_id = msso.vendor_id
                                                          AND    assa.vendor_site_id = msso.vendor_site_id
                                                          AND    assa.vendor_site_code != 'NON EDI')
                                        AND   xxwcinv_inv_aging_pkg.check_special(msa.organization_id, msa.inventory_item_id) = 'No' -- -Added 20140726-00007 to exclude specials
                                        GROUP BY msso.VENDOR_ID, msso.VENDOR_SITE_ID
                                        HAVING  count(msa.inventory_item_id) = l_max_count;
                                      
                                    EXCEPTION
                                      
                                      WHEN too_many_rows THEN
                                      
                                       BEGIN
                                       
                                          SELECT vendor_site_id, count_max
                                          INTO   l_vendor_site_id, l_max_count2
                                          FROM   (
                                          SELECT  msso.VENDOR_SITE_ID, count(msa.inventory_item_id) count_max
                                          FROM   --XXWC.XXWC_MRP_SR_ASSIGNMENTS_BK msa,
                                                 mrp_sr_assignments msa,
                                                 mrp_sourcing_rules msr,
                                                 mrp_sr_receipt_org msro,
                                                 mrp_sr_source_org msso
                                          --WHERE  msso.vendor_id = r_vendor_minimum.vendor_id
                                          --AND    msso.vendor_site_id = XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id)
                                          -----------
                                          WHERE  msso.vendor_id = p_vendor_id
                                          --and    msa.organization_id = r_vendor_minimum.organization_id
                                          --and    r_vendor_minimum.vendor_contact_id = XXWC_BPA_CONVERSIONS_PKG.get_contact_id_from_legacy(msso.vendor_id, XXWC_BPA_CONVERSIONS_PKG.get_legacy_id_from_contact_id(r_vendor_minimum.vendor_id, r_vendor_minimum.vendor_site_id, r_vendor_minimum.vendor_contact_id))
                                          and    msr.sourcing_rule_id = msro.sourcing_rule_id
                                          and    msro.sr_receipt_id = msso.sr_receipt_id
                                          AND    msa.sourcing_rule_id = msr.sourcing_rule_id
                                          AND    msa.ASSIGNMENT_TYPE = 6
                                        --Added Exists Statement 1.1 - 20140726-00007 to include only stock items
                                        AND   EXISTS (
                                                      SELECT mcs.category_set_id, mcs.category_set_name, mck.concatenated_segments
                                                      FROM   mtl_category_sets mcs,
                                                             mtl_categories_kfv mck,
                                                             mtl_item_categories mic
                                                      WHERE  mcs.category_set_name = 'Sales Velocity'
                                                      AND    mcs.category_set_id =  mic.category_set_id
                                                      AND    mck.category_id = mic.category_id
                                                      AND    mck.concatenated_segments IN ('1','2','3','4','5','6','7','8','9')
                                                      AND    mic.inventory_item_id = msa.inventory_item_id
                                                      AND    mic.organization_id = msa.organization_id)
                                        --Added Exists Statement 1.1 - 20140726-00007 to not include NON EDI
                                        AND   EXISTS (
                                                          SELECT assa.vendor_site_code
                                                          FROM   ap_supplier_sites_all assa
                                                          WHERE  assa.vendor_id = msso.vendor_id
                                                          AND    assa.vendor_site_id = msso.vendor_site_id
                                                          AND    assa.vendor_site_code != 'NON EDI')
                                        AND   xxwcinv_inv_aging_pkg.check_special(msa.organization_id, msa.inventory_item_id) = 'No' -- -Added 20140726-00007 to exclude specials
                                          GROUP BY msso.VENDOR_ID, msso.VENDOR_SITE_ID
                                          HAVING  count(msa.inventory_item_id) = l_max_count)
                                          where  rownum = 1;
                                        exception
                                            WHEN others THEN
                                                  l_vendor_site_id := NULL;
                                                  l_max_count2 := NULL;
                                                  
                                        end; 
                                    
                                      WHEN others THEN
                                          
                                            l_vendor_site_Id := NULL;
                                            l_max_count2 := null;
                                    
                                    END;
                                      
                            else
                            
                                l_vendor_site_id := null;
                            
                            end if;
                            
             
            END IF;
            
           
           
               return l_vendor_site_id;
   
     EXCEPTION
     
        WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

           
      
      END get_default_vendor_site_id;
     
    /*************************************************************************************************   
   *   Procedure update_vendor_address                                                              *
   *   Purpose : Update Address Line 2 to Address Line 1 from Vendor Conversion; Address Line 1     *
   *       is a bogus address to help convert vendor site correctly                                 *
   *                                                                                                *
   *                                                                                                *
   *     procudure update_vendor_address                                                            *
   *             (p_org_id    IN NUMBER                                                             *
   *             ,p_vendor_id IN NUMBER                                                             *
   *             ,p_vendor_site_code IN VARCHAR2)                                                   *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.2        09/08/2014  Lee Spitzer                TMS Ticket 20140909-00030                  *
   *                                                        Fix Address display on PO document for  *
   *                                                         0PURCHASING site                       *
   *                                                                                                *
   *                                                                                                *
   /************************************************************************************************/
    
    PROCEDURE update_vendor_address
                (p_org_id    IN NUMBER
                ,p_vendor_id IN NUMBER
                ,p_vendor_site_code IN VARCHAR2) IS
    
      p_location_rec          HZ_LOCATION_V2PUB.LOCATION_REC_TYPE;
      p_object_version_number NUMBER;
      x_return_status         VARCHAR2(2000);
      x_msg_count             NUMBER;
      x_msg_data              VARCHAR2(2000);


      --Get Current Address Information
      CURSOR c_address IS
        SELECT hl.address1,
               hl.address2,
               hl.address3,
               hl.address4,
               hl.object_version_number,
               hl.location_id,
               sup.segment1,
               assa.vendor_site_code
        FROM   hz_locations hl,
               ap_suppliers sup,
               ap_supplier_sites_all assa
        WHERE  hl.location_id = assa.location_id
        AND    sup.vendor_id = assa.vendor_id
        AND    assa.org_id = p_org_id
        and    assa.vendor_site_code = p_vendor_site_code
        AND    assa.vendor_site_code = hl.address1
        and    sup.vendor_id = nvl(p_vendor_id, sup.vendor_id);
     

    BEGIN

      g_err_callpoint := 'update_vendor_address';
        

      FOR r_address IN c_address

        LOOP
  
          g_count := g_count + 1;
          
        -- Initializing the Mandatory API parameters
          p_location_rec.location_id := r_address.location_id;  -- HZ_LOCATIONS.LOCATION_ID
          p_location_rec.address1    := r_address.address2;
          p_object_version_number    := 1;
          
            IF r_address.address3 IS NULL THEN
            
              p_location_rec.address2    := FND_API.G_MISS_CHAR;
                
            else
            
              p_location_rec.address2  :=  r_address.address3;
              
            END IF;
            
        
           IF r_address.address4 IS NULL THEN
            
              p_location_rec.address3    := FND_API.G_MISS_CHAR;
                
            else
            
              p_location_rec.address3  :=  r_address.address4;
              
            END IF;
            
        
        hz_location_v2pub.update_location 
                    (
                     p_init_msg_list           => FND_API.G_TRUE,
                     p_location_rec            => p_location_rec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => x_return_status,
                     x_msg_count               => x_msg_count,
                     x_msg_data                => x_msg_data
                          ); 
        
        IF x_return_status = fnd_api.g_ret_sts_success THEN
            COMMIT;
            fnd_file.put_line(fnd_file.log, r_address.segment1 || ' ' || r_address.vendor_site_code || ' address updated successfully'); 
            
        ELSE
            FOR i IN 1 .. x_msg_count
            LOOP
              x_msg_data := oe_msg_pub.get( p_msg_index => i, p_encoded => 'F');
            END LOOP;
            fnd_file.put_line(fnd_file.log, r_address.segment1 || ' ' || r_address.vendor_site_code || ' update error. ' || x_msg_data); 
            
        END IF;
        
        END LOOP;
    
      fnd_file.put_line (fnd_file.output, g_count || ' updated site vendor address ');       
                                 
        
    EXCEPTION
    
       WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    END update_vendor_address;

   /*************************************************************************************************   
   *   Procedure add_1_see_notes_contact                                                            *
   *   Purpose : This adds 1 SEE NOTES Contact to the 0PURCHASING Site                              *
   *                                                                                                *
   *                                                                                                *
   *     procedure end_old_purchasing_sites                                                         *
   *             (p_org_id IN NUMBER,                                                               *
   *              p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.3        01/19/2015  Lee Spitzer                TMS Ticket 20140909-00029                  *
   *                                                        Create 1 SEE NOTES contact              *
   *                                                                                                *
   /************************************************************************************************/
   
  
    PROCEDURE add_1_see_notes_contact
                      (p_org_id IN NUMBER,
                       p_vendor_id IN NUMBER)
      IS
      
      
      cursor c_vendor is
        
            --Find Vendors that have the g_vendor_site_code SITE
            select distinct ass.vendor_id, assa.vendor_site_id
            from   ap_suppliers ass,
                   ap_supplier_sites_all assa
            where  assa.vendor_site_code = g_vendor_site_code
            and    ass.vendor_id = assa.vendor_id
            and    nvl(assa.purchasing_site_flag,'N') = 'Y'
            and    sysdate  < nvl(assa.inactive_date,sysdate+1)  
            and    assa.org_id = p_org_id
            and    ass.vendor_id = nvl(p_vendor_id, ass.vendor_id);
            


      cursor c_vendor_site(p_vendor_id IN NUMBER, p_vendor_site_id in NUMBER) is
    
        --Add the non PURCHASNG SITES as contacts to both the old site and g_vendor_site_code Site
        SELECT assa.vendor_site_code last_name, assa.vendor_site_id, assa.vendor_id, assa.org_id, assa.party_site_id,
               assa.area_code, assa.phone, assa.fax_area_code, assa.fax, assa.email_address
        FROM   ap_supplier_sites_all assa
        WHERE  assa.vendor_id = p_vendor_id
        AND    assa.vendor_site_code = g_1see_notes
        AND    assa.vendor_site_id != p_vendor_site_id
        and    assa.org_id = p_org_id
        AND    SYSDATE  < nvl(assa.inactive_date,SYSDATE+1)
        --Added Exists Statement 1.1 20140726-00007
        and    not exists (select *
                   from   po_vendor_contacts pvc
                   WHERE  pvc.vendor_id = assa.vendor_id
                   and    pvc.last_name = g_1see_notes
                   and    sysdate < nvl(pvc.inactive_date,sysdate+1) 
                   )
        UNION
        SELECT assa.vendor_site_code last_name, assa1.vendor_site_id, assa.vendor_id, assa.org_id, assa1.party_site_id,
                assa.area_code, assa.phone, assa.fax_area_code, assa.fax, assa.email_address
        from   ap_supplier_sites_all assa,
               ap_supplier_sites_all assa1
        WHERE  assa.vendor_id = p_vendor_id
        AND    assa.vendor_site_code = g_1see_notes
        and    assa.vendor_site_id != p_vendor_site_id
        AND    assa.vendor_id = assa1.vendor_id
        AND    assa.org_id = p_org_id
        and    sysdate  < nvl(assa.inactive_date,sysdate+1)  
        and    assa.org_id = assa1.org_id
        AND    SYSDATE  < nvl(assa1.inactive_date,SYSDATE+1)  
        AND    assa1.vendor_site_id = p_vendor_site_id
        --Added Exists Statement 1.1 20140726-00007
        and    not exists (select *
                   from   po_vendor_contacts pvc
                   WHERE  pvc.vendor_id = assa.vendor_id
                   AND    pvc.last_name = g_1see_notes
                   AND    SYSDATE  < nvl(pvc.inactive_date,SYSDATE+1)  
                   );

    BEGIN
    
     g_err_callpoint := 'add_1_see_notes_contact';
   
        for r_vendor in c_vendor 
        
            loop
                
                for r_vendor_site in c_vendor_site(r_vendor.vendor_id, r_vendor.vendor_site_id)
                
                    loop
                    
                          BEGIN
                
                          g_count := g_count + 1;
                
                            Insert Into AP_SUP_SITE_CONTACT_INT
                                (vendor_contact_interface_id,
                                 vendor_id,
                                 vendor_site_id,
                                 last_name,
                                 org_id,
                                 status,
                                 party_site_id,
                                 area_code, 
                                 phone, 
                                 fax_area_code, 
                                 fax, 
                                 email_address)
                                VALUES
                                (AP_SUP_SITE_CONTACT_INT_S.nextval,
                                 r_vendor_site.vendor_id,
                                 r_vendor_site.vendor_site_id,
                                 r_vendor_site.last_name,
                                 r_vendor_site.org_id,
                                 'NEW',
                                 r_vendor_site.party_site_id,
                                 r_vendor_site.area_code, 
                                 r_vendor_site.phone, 
                                 r_vendor_site.fax_area_code, 
                                 r_vendor_site.fax, 
                                 r_vendor_site.email_address);
                                
                            EXCEPTION
                                WHEN OTHERS THEN    
                                fnd_file.put_line (fnd_file.output, 'Error inserting into AP_SUP_SITE_CONTACT_INT ' ||SQLCODE || SQLERRM); 
                            END;
                
                
                
                    end loop;
                
            
            end loop;

          fnd_file.put_line (fnd_file.output, g_count || ' inserted into AP_SUP_SITE_CONTACT_INT ' ||SQLCODE || SQLERRM); 

        COMMIT;
    
    EXCEPTION
      WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);


    END add_1_see_notes_contact;
    
   /*************************************************************************************************   
   *   Procedure add_1_see_notes_site                                                               *
   *   Purpose : This adds 1 SEE NOTES Vendor Sitehe 0PURCHASING Site                               *
   *                                                                                                *
   *                                                                                                *
   *     procedure end_old_purchasing_sites                                                         *
   *             (p_org_id IN NUMBER,                                                               *
   *              p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.3        01/19/2015  Lee Spitzer                TMS Ticket 20140909-00029                  *
   *                                                        Create 1 SEE NOTES contact              *
   *                                                                                                *
   /************************************************************************************************/
   
  
    PROCEDURE add_1_see_notes_supplier_site
                      (p_org_id IN NUMBER,
                       p_vendor_id IN NUMBER)
      IS
      
        --Cursor for Vendors that don't have g_1see_notes SITE
        Cursor c_vendor IS
        select distinct ass.vendor_id
        from   ap_suppliers ass,
               ap_supplier_sites_all assa
        where  not exists (select *
                               FROM   ap_supplier_sites_all assa1
                               WHERE  assa1.vendor_id = assa.vendor_id
                               AND    assa1.vendor_site_code = assa.vendor_site_code
                               AND    assa1.vendor_site_code = g_1see_notes
                               AND    assa1.org_id = assa.org_id
                               AND    SYSDATE  < nvl(assa1.inactive_date,SYSDATE+1)  
                          )
        and    ass.vendor_id = assa.vendor_id
        and    nvl(assa.purchasing_site_flag,'N') = 'Y'
        and    sysdate  < nvl(assa.inactive_date,sysdate+1)  
        and    assa.org_id = p_org_id
        and    ass.vendor_id = nvl(p_vendor_id, ass.vendor_id)
        ;
        
        --Return the first vendor site and create the g_1see_notes 
        Cursor c_vendor_site(p_vendor_id in NUMBER)  IS
        select distinct 
            vendor_id,
            org_id,
            'N' purchasing_site_flag,
            pay_site_flag,
            primary_pay_site_flag,
            g_1see_notes vendor_site_code,
            g_see_notes address_line1,
            address_line2,
            address_line3, 
            city,
            state, 
            zip,
            county,
            country,
            phone,
            fax,
            hold_unmatched_invoices_flag,
            accts_pay_code_combination_id,
            prepay_code_combination_id,
            payment_method_lookup_code,
            exclusive_payment_flag, 
            terms_id,
            pay_group_lookup_code,
            match_option,
            'NEW' status,
            distribution_set_id, 
            tax_reporting_site_flag,
            created_by, 
            creation_date,
            last_updated_by, 
            last_update_date,
            CASE WHEN address_style IS NULL AND country = 'US' THEN 'POSTAL_ADDR_US'
                 WHEN address_style IS NULL AND country != 'US' THEN 'POSTAL_ADDR_DEF'
                 WHEN address_style IS NOT NULL THEN address_style end address_style
            from  ap_supplier_sites_all assa
            where  vendor_id = p_vendor_id
            and    purchasing_site_flag = 'Y'
            AND    SYSDATE  < nvl(inactive_date,SYSDATE+1)  
            AND    vendor_site_code = g_vendor_site_code
            AND    not exists (select *
                               FROM   ap_supplier_sites_all assa1
                               WHERE  assa1.vendor_id = assa.vendor_id
                               AND    assa1.vendor_site_code = g_1see_notes
                               AND    assa1.org_id = assa.org_id
                               AND    SYSDATE  < nvl(assa1.inactive_date,SYSDATE+1)  
                          );
            
          
            l_edi_count NUMBER;
            l_ece_tp_location_code VARCHAR2(60);
            l_SUPPLIER_NOTIF_METHOD VARCHAR2(25);
    
        BEGIN
        
        g_err_callpoint := 'add_1_see_notes_supplier_site';
        
            FOR r_vendor IN c_vendor 
            
                LOOP
                
                  --Check if EDI Vendor
                  BEGIN
                      select count(*)
                      into   l_edi_count
                      from   ap_supplier_sites_all assa
                      where  assa.ece_tp_location_code is not null
                      and    nvl(assa.purchasing_site_flag,'N') = 'Y'
                      AND    SYSDATE  < nvl(assa.inactive_date,SYSDATE+1)
                      and    assa.vendor_id = r_vendor.vendor_id; --8/6/2014 updated version 20140726-00007 1.1 missing vendor_id in loop
                  EXCEPTION
                    when others then
                        l_edi_count := 0;
                 end;
                 
                 
                  if l_edi_count > 0 then
                  
                      l_ece_tp_location_code := g_1see_notes;
                      
                  else
                  
                      l_ece_tp_location_code := NULL;
                      
                  end if;
                  
                  
                  BEGIN
                    SELECT distinct SUPPLIER_NOTIF_METHOD
                    INTO   l_SUPPLIER_NOTIF_METHOD
                    FROM   ap_supplier_sites_all assa
                    --where  assa.ece_tp_location_code is not null --Removed 08/19/2014
                    where  1=1 --Added 08/19/2014
                    AND    nvl(assa.purchasing_site_flag,'N') = 'Y'
                    AND    nvl(assa.supplier_notif_method,'NONE') != 'NONE'
                    AND    SYSDATE  < nvl(assa.inactive_date,SYSDATE+1)
                    and    assa.vendor_id = r_vendor.vendor_id; --8/6/2014 updated version 20140726-00007 1.1 missing vendor_id in loop
                  EXCEPTION
                    WHEN too_many_rows THEN
                          l_supplier_notif_method := 'FAX';
                    WHEN no_data_found THEN
                          l_supplier_notif_method := NULL;
                    WHEN others THEN
                        l_supplier_notif_method := NULL;
                 end;
                    
                    for r_vendor_site in c_vendor_site(r_vendor.vendor_id)
                    
                        LOOP
                    
                    
                              BEGIN
                              
                               g_count := g_count + 1;
                              
                                INSERT INTO ap_supplier_sites_int
                                           (vendor_site_interface_id,
                                            status,
                                            vendor_id,
                                            org_id,
                                            purchasing_site_flag,
                                            pay_site_flag,
                                            primary_pay_site_flag,
                                            vendor_site_code,
                                            address_line1,
                                            address_line2,
                                            address_line3, city,
                                            state, zip,
                                            county,
                                            country,
                                            phone,
                                            fax,
                                            hold_unmatched_invoices_flag,
                                            accts_pay_code_combination_id,
                                            prepay_code_combination_id,
                                            payment_method_lookup_code,
                                            exclusive_payment_flag, terms_id,
                                            pay_group_lookup_code,
                                            match_option,
                                            distribution_set_id, 
                                            tax_reporting_site_flag,
                                            created_by, 
                                            creation_date,
                                            last_updated_by, 
                                            last_update_date,
                                            ece_tp_location_code,
                                            address_style,
                                            supplier_notif_method,
                                            party_site_name,
                                            inactive_date
                                            )
                                        VALUES
                                           (ap_supplier_sites_int_s.NEXTVAL,
                                            r_vendor_site.status,
                                            r_vendor_site.vendor_id, --- Added by Thiru 9/Jan/2012
                                            r_vendor_site.org_id,
                                            r_vendor_site.purchasing_site_flag,
                                            r_vendor_site.pay_site_flag,
                                            r_vendor_site.primary_pay_site_flag,
                                            r_vendor_site.vendor_site_code,
                                            r_vendor_site.vendor_site_code, --Address Line 1 --r_vendor_site.address_line1,
                                            r_vendor_site.address_line1, --Address Line 2 --r_vendor_site.address_line2,
                                            r_vendor_site.address_line2, --Adderss Line 3 --r_vendor_site.address_line3
                                            r_vendor_site.city,
                                            r_vendor_site.state, 
                                            r_vendor_site.zip,
                                            r_vendor_site.county,
                                            r_vendor_site.country,
                                            r_vendor_site.phone,
                                            r_vendor_site.fax,
                                            r_vendor_site.hold_unmatched_invoices_flag,
                                            r_vendor_site.accts_pay_code_combination_id,
                                            r_vendor_site.prepay_code_combination_id,
                                            r_vendor_site.payment_method_lookup_code,
                                            r_vendor_site.exclusive_payment_flag, 
                                            r_vendor_site.terms_id,
                                            r_vendor_site.pay_group_lookup_code,
                                            r_vendor_site.match_option,
                                            r_vendor_site.distribution_set_id, 
                                            r_vendor_site.tax_reporting_site_flag,
                                            -1, 
                                            SYSDATE,
                                            -1, 
                                            SYSDATE,
                                            l_ece_tp_location_code,
                                            r_vendor_site.address_style,
                                            l_supplier_notif_method,
                                            r_vendor_site.vendor_site_code,
                                            trunc(sysdate+1));
                        
                        
                            EXCEPTION
                            
                                WHEN OTHERS THEN
                                
                                        fnd_file.put_line (fnd_file.output, 'Error inserting into ap_supplier_sites_int ' ||SQLCODE || SQLERRM); 
                            END;
                            
                            
                        
                        END LOOP;
                
                    
                    END LOOP;
                    
                    
            COMMIT;
        
        
              fnd_file.put_line (fnd_file.output, g_count || ' records inserted into ap_supplier_sites_int');
    
    EXCEPTION

    WHEN OTHERS THEN
    
         g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
              
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    end add_1_see_notes_supplier_site;
                       
end XXWC_BPA_CONVERSIONS_PKG;