CREATE OR REPLACE PACKAGE BODY APPS."XXCUS_APXCMMN_LOAD_PKG" IS
  /********************************************************************************
  
  File Name: HDS_APXCMMN_LOAD_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Populate Real Estate Location, FRULOC and related data
           from ORAFIN to APXCMMN
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/23/2009    Jason Sloan     Initial creation of the procedure
  2.0     07/28/2009    Jason Sloan     Added additional fields and changed the sql
                                        to match the LD's SQL rather than the
                                        PNLD_MASTER_VIEW Report
  3.0     09/04/2009    Jason Sloan     Broke FRU-LOC information into two tables
  3.1     03/16/2010    Luong Vu        Add Property start/end date, and Tenancy type
  3.2     01/07/2011    Luong Vu        Added COUNTRY_ABBR (request from Nancy Pahwa
                                        for ESMS change as part of PeopleSoft implementation.
  3.3     01/12/2011    Luong Vu        Remove Trunc/Repop
                                        Push data to apxcmmn.hds_fruloc_master_stg
                                        instead of apxcmmn.hds_fruloc_master
                                        Remove trunc/repop of contact data
  4.0     02/24/2012    Luong Vu        Modified program for R12
                                        Modified program to use local data tables  
                                        rather than using PNLD tables.       
  5.0     05/14/2012    Manny           Adding market cluster   
  5.1     09/28/2012    Luong Vu        Add loc_image to axpcmmn feed     
  5.2     04/09/2014    Manny           --RFC 40035  subquery issue.  change is on the view.      
  5.3     09/04/2014    Manny           --SR 520376 templates added                                                       
  ********************************************************************************/

  PROCEDURE create_pn_data_set(errbuf  OUT VARCHAR2
                              ,retcode OUT NUMBER) IS
  BEGIN
    NULL;
  
    --**************************************************************************--
  
    EXECUTE IMMEDIATE 'TRUNCATE TABLE  XXCUS.XXCUSPN_LD_GLDESC_TBL';
    INSERT INTO xxcus.xxcuspn_ld_gldesc_tbl
    /*   SELECT DISTINCT fvt1.description   product
                  ,locmap.system_cd   pos_system
                  ,locmap.creation_dt fru_create_date
                  ,fvt2.description   location
                  ,gcc.segment1
                  ,gcc.segment2
                  ,locmap.fru
                  ,locmap.lob_branch
     FROM applsys.fnd_flex_values     fv2
         ,applsys.fnd_flex_values_tl  fvt2
         ,applsys.fnd_flex_value_sets fvs2
         ,applsys.fnd_flex_values     fv1
         ,applsys.fnd_flex_values_tl  fvt1
         ,applsys.fnd_flex_value_sets fvs1
         ,gl.gl_code_combinations     gcc
         ,apps.xxcus_location_code_vw locmap
    WHERE gcc.segment2 = fv2.flex_value
      AND fv2.flex_value_id = fvt2.flex_value_id
      AND fv2.flex_value_set_id = fvs2.flex_value_set_id
      AND gcc.segment1 = fv1.flex_value
      AND fv1.flex_value_id = fvt1.flex_value_id
      AND fv1.flex_value_set_id = fvs1.flex_value_set_id
      AND fvs1.flex_value_set_name = 'XXCUS_GL_PRODUCT'
      AND fvs2.flex_value_set_name = 'XXCUS_GL_LOCATION'
         --AND gcc.enabled_flag = 'Y'
      AND gcc.segment2 = locmap.entrp_loc
         --AND locmap.ap_docum_flag = 'Y'
      AND gcc.segment1 = locmap.entrp_entity
      AND fvt1.description <> 'Conversion - DO NOT USE';*/
    
      SELECT DISTINCT fvt1.description    product
                     ,locmap.system_cd    pos_system
                     ,locmap.creation_dt  fru_create_date
                     ,fvt2.description    location
                     ,locmap.entrp_entity --.segment1
                     ,locmap.entrp_loc --.segment2
                     ,locmap.fru
                     ,locmap.lob_branch
        FROM applsys.fnd_flex_values     fv2
            ,applsys.fnd_flex_values_tl  fvt2
            ,applsys.fnd_flex_value_sets fvs2
            ,applsys.fnd_flex_values     fv1
            ,applsys.fnd_flex_values_tl  fvt1
            ,applsys.fnd_flex_value_sets fvs1
             -- ,gl.gl_code_combinations     gcc
            ,apps.xxcus_location_code_vw locmap
       WHERE locmap.entrp_loc = fv2.flex_value
         AND fv2.flex_value_id = fvt2.flex_value_id
         AND fv2.flex_value_set_id = fvs2.flex_value_set_id
         AND locmap.entrp_entity = fv1.flex_value
         AND fv1.flex_value_id = fvt1.flex_value_id
         AND fv1.flex_value_set_id = fvs1.flex_value_set_id
         AND fvs1.flex_value_set_name = 'XXCUS_GL_PRODUCT'
         AND fvs2.flex_value_set_name = 'XXCUS_GL_LOCATION'
            --AND gcc.enabled_flag = 'Y'
            --AND gcc.segment2 = locmap.entrp_loc
            --AND locmap.ap_docum_flag = 'Y'
            --AND gcc.segment1 = locmap.entrp_entity
         AND fvt1.description <> 'Conversion - DO NOT USE';
  
    COMMIT;
  
    --*************************************************************************************************--
  
    EXECUTE IMMEDIATE 'TRUNCATE TABLE   xxcus.xxcuspn_ld_opsover_tbl';
    INSERT INTO xxcus.xxcuspn_ld_opsover_tbl
      SELECT gldcr.product fru_lobname
            ,gldcr.location fru_location_name
            ,gldcr.leg_seg_one lob_br
            ,loc.space_type_lookup_code primary_ops_type
            ,loc_space.meaning operations_type
            ,NULL fru_type --placeholder
            ,CASE
               WHEN trunc(empsp.emp_assign_start_date) > trunc(SYSDATE) THEN
                'FUTURE'
               WHEN empsp.emp_assign_end_date IS NOT NULL THEN
                'INACTIVE'
               ELSE
                'ACTIVE'
             END ops_status
            ,prop.portfolio
            ,empsp.emp_space_assign_id
            ,empsp.location_id
            ,empsp.emp_assign_start_date
            ,empsp.cost_center_code
            ,empsp.allocated_area_pct
            ,empsp.allocated_area
            ,empsp.utilized_area
            ,empsp.attribute1 fru
            ,corp.lob_branch
            ,rollups.bu_id lob_name_bu -- v.1.2 New columns
            ,NULL --rollups.sbu_id lob_name -- v.1.2 New columns
            ,rollups.bu_desc bu_desc
            ,rollups.fps_id -- v.1.2 New columns
            ,rollups.fps_desc -- v.1.2 New columns
            ,loc_assign.meaning assignment_type
            ,loc_site.meaning site_type
            ,decode(loc.location_type_lookup_code, 'OFFICE', 'SECTION',
                    'SECTION', 'YARD', loc.location_type_lookup_code) location_type
            ,decode(loc.location_type_lookup_code, 'BUILDING', loc.building,
                    'FLOOR', loc.floor, 'OFFICE', loc.office) loc_desc
            ,loc1.attribute1 longitude
            ,loc1.attribute2 latitude
            ,prop.property_code location_code
            ,loc1.location_code location_code_lvl1
            ,loc2.location_code location_code_lvl2
            ,loc.location_code location_code_lvl3
            ,loc.attribute3 operating_hours
            ,loc.attribute4 walk_in
            ,loc.attribute5 customer_site
            ,loc.uom_code
            ,loc.rentable_area
            ,loc.usable_area
            ,addr.address_line1
            ,addr.address_line2
            ,addr.county
            ,addr.city
            ,addr.state
            ,addr.province
            ,addr.zip_code
            ,decode(addr.country, 'US', 'United States', 'CA', 'Canada',
                    addr.country) country --v1.3
            ,loc1.address_id
            ,loc_gross.gross_area
        FROM pn.pn_space_assign_emp_all empsp
            ,pn.pn_locations_all loc
            ,pn.pn_addresses_all addr
            ,pn.pn_properties_all prop
            ,(SELECT meaning
                    ,lookup_code
                FROM applsys.fnd_lookup_values
               WHERE lookup_type LIKE 'PN_SPACE_TYPE') loc_space
            ,xxcus.xxcuspn_ld_gldesc_tbl gldcr
            ,(SELECT loc2.location_id
                    ,loc2.parent_location_id
                    ,loc2.active_end_date
                    ,loc2.location_code
                    ,loc2.space_type_lookup_code
                FROM pn.pn_locations_all loc2) loc2
            ,(SELECT loc1.location_id
                    ,loc1.parent_location_id
                    ,address_id
                    ,loc1.property_id
                    ,loc1.attribute1
                    ,loc1.attribute2
                    ,loc1.location_code
                    ,loc1.active_end_date
                FROM pn.pn_locations_all loc1) loc1
            ,(SELECT fru
                    ,entrp_cc
                    ,lob_branch
                FROM apps.xxcus_location_code_vw) corp
            ,pn.pn_locations_all loc_gross
            ,(SELECT meaning
                    ,lookup_code
                FROM applsys.fnd_lookup_values
               WHERE lookup_type = 'PN_FUNCTION_TYPE') loc_site
            ,(SELECT meaning
                    ,lookup_code
                FROM applsys.fnd_lookup_values
               WHERE lookup_type = 'PN_STANDARD_TYPE') loc_assign
            ,apps.xxcuspn_ld_lob_rollup_vw rollups
       WHERE empsp.location_id = loc.location_id(+)
         AND loc1.address_id = addr.address_id(+)
         AND loc1.property_id = prop.property_id(+)
         AND empsp.cost_center_code = gldcr.segment2(+)
         AND loc_gross.location_code = loc1.location_code
         AND loc_gross.active_end_date = '31-DEC-4712'
         AND rollups.fps_id = gldcr.segment1
         AND loc.standard_type_lookup_code = loc_assign.lookup_code(+)
         AND loc.function_type_lookup_code = loc_site.lookup_code(+)
         AND loc.parent_location_id = loc2.location_id
         AND loc2.parent_location_id = loc1.location_id
         AND empsp.attribute1 = corp.fru
         AND loc.space_type_lookup_code = loc_space.lookup_code(+)
         AND gldcr.l_corp_id = empsp.attribute1
         AND loc.active_end_date = '31-DEC-4712'
         AND loc1.active_end_date = '31-DEC-4712'
         AND loc2.active_end_date = '31-DEC-4712'
         AND nvl(empsp.emp_assign_end_date,
                 to_date('31-DEC-4712', 'DD-MON-YYYY')) =
             (SELECT MAX(nvl(spc.emp_assign_end_date,
                             to_date('31-DEC-4712', 'DD-MON-YYYY')))
                FROM pn.pn_space_assign_emp_all spc
               WHERE spc.attribute1 = empsp.attribute1
                 AND spc.location_id = empsp.location_id);
  
    COMMIT;
  
    --*************************************************************************************************--
  
    -- site details
    EXECUTE IMMEDIATE 'TRUNCATE TABLE  xxcus.xxcuspn_ld_site_detail_tbl';
    INSERT INTO xxcus.xxcuspn_ld_site_detail_tbl
    
    --other lcoations addresses
      SELECT DISTINCT decode(prop.active_property, 'I', 'Inactive', 'Active') ops_status --placeholder
                     ,prop.property_code location_code
                     ,cbsanamer.metroarea metroarea
                     ,prop.portfolio lob
                     ,bldgs.bldgs
                     ,nvl(yrds.yrd_count, 0) yard_count
                     ,propsqft.sqft
                     ,tenured.meaning tenure
                     ,zoned.meaning timezone
                     ,cbsanamer.countyname
                     ,decode(prop.attribute3, NULL, 'English',
                             prop.attribute3) proplanguage --v1.3
                     ,decode(prop.attribute2, NULL, 'USD', prop.attribute2) currency --v1.3
                     ,round(nvl(acres.land_sqft, 0) / 43560, 1) acres
                     ,decode(addr.country, 'US', 'United States', 'CA',
                             'Canada', addr.country) country --v1.3
                     ,locs.attribute1 longitude --v1.1
                     ,locs.attribute2 latitude --v1.1
                     ,(SELECT lea.lease_num
                         FROM pn.pn_tenancies_all t
                             ,pn.pn_locations_all l
                             ,pn.pn_leases_all    lea
                        WHERE t.location_id = l.location_id
                          AND t.lease_id = lea.lease_id
                          AND l.location_code LIKE
                              prop.property_code || '%'
                          AND t.primary_flag = 'Y'
                          AND rownum < 2) realid
                     ,(SELECT lok.meaning
                         FROM pn.pn_tenancies_all       t
                             ,pn.pn_locations_all       l
                             ,pn.pn_leases_all          lea
                             ,applsys.fnd_lookup_values lok
                        WHERE t.location_id = l.location_id
                          AND t.lease_id = lea.lease_id
                          AND lok.lookup_code = lea.lease_type_code
                          AND lok.lookup_type = 'PN_LEASE_TYPE'
                          AND l.location_code LIKE
                              prop.property_code || '%'
                          AND t.primary_flag = 'Y'
                          AND rownum < 2) rer_type
        FROM pn.pn_properties_all prop
            ,(SELECT property_code
                    ,nvl(COUNT(property_code), 0) bldgs
                FROM pn.pn_locations_all  a
                    ,pn.pn_properties_all b
               WHERE b.property_id = a.property_id
                 AND a.location_type_lookup_code = 'BUILDING'
                 AND a.active_end_date = '31-DEC-4712'
               GROUP BY property_code) bldgs
            ,(SELECT SUM(gross_area) sqft
                    ,b.property_code
                FROM pn.pn_locations_all  a
                    ,pn.pn_properties_all b
               WHERE a.location_type_lookup_code = 'BUILDING'
                 AND b.property_id = a.property_id
                 AND a.active_end_date = '31-DEC-4712'
               GROUP BY b.property_code) propsqft
            ,(SELECT lookup_code
                    ,meaning
                FROM applsys.fnd_lookup_values
               WHERE lookup_type = 'PN_LEASED_OR_OWNED') tenured
            ,(SELECT lookup_code
                    ,meaning
                    ,lookup_type
                FROM applsys.fnd_lookup_values
               WHERE lookup_type = 'PN_ZONE_TYPE') zoned
            ,apps.xxcuspn_metro_area_zips_vw cbsanamer
            ,(SELECT prop.property_code
                    ,COUNT(prop.property_code) yrd_count
                FROM pn.pn_locations_all  lvl3
                    ,pn.pn_locations_all  lvl2
                    ,pn.pn_locations_all  lvl1
                    ,pn.pn_properties_all prop
               WHERE lvl3.location_type_lookup_code = 'SECTION'
                 AND lvl3.parent_location_id = lvl2.location_id
                 AND lvl2.parent_location_id = lvl1.location_id
                 AND lvl1.property_id(+) = prop.property_id
               GROUP BY prop.property_code) yrds
            ,(SELECT SUM(gross_area) land_sqft
                    ,b.property_code
                FROM pn.pn_locations_all  a
                    ,pn.pn_properties_all b
               WHERE a.location_type_lookup_code = 'LAND'
                 AND b.property_id = a.property_id
                 AND a.active_end_date = '31-DEC-4712'
               GROUP BY b.property_code) acres
            ,pn.pn_locations_all locs --v1.1
            ,pn.pn_addresses_all addr
       WHERE prop.property_code = bldgs.property_code(+)
         AND prop.zone = zoned.lookup_code(+)
         AND prop.property_code = propsqft.property_code
         AND prop.property_code = acres.property_code(+)
         AND prop.tenure = tenured.lookup_code
         AND prop.attribute1 = cbsanamer.zipcode(+)
         AND prop.property_code = yrds.property_code(+)
            --AND   locs.property_id = prop.property_id
         AND locs.location_code(+) = prop.property_code --v1.1
         AND locs.address_id = addr.address_id(+);
  
    COMMIT;
    --*************************************************************************************************--
  
    EXECUTE IMMEDIATE 'TRUNCATE TABLE    xxcus.xxcuspn_ld_financial_det_tbl ';
    INSERT INTO xxcus.xxcuspn_ld_financial_det_tbl
      SELECT empsp.attribute1 fru
            ,corp.entrp_entity lob_branch
            ,empsp.cost_center_code oracle_financial_location
            ,gldcr.segment1 product_segment_id
            ,NULL legal_entity
            ,gldcr.pos_system
            ,gldcr.fru_create_date fru_open_date
            ,NULL federal_tax_id
            ,gldcr.product segment_name
            ,empsp.emp_assign_start_date fru_close_date
            ,prop.property_code location_code
            ,loc1.location_code location_code_lvl1
            ,loc2.location_code location_code_lvl2
            ,loc.location_code location_code_lvl3
            ,CASE
               WHEN trunc(empsp.emp_assign_start_date) > trunc(SYSDATE) THEN
                'FUTURE'
               WHEN empsp.emp_assign_end_date IS NOT NULL THEN
                'INACTIVE'
               ELSE
                'ACTIVE'
             END ops_status
        FROM pn.pn_space_assign_emp_all empsp
            ,pn.pn_locations_all loc
            ,pn.pn_properties_all prop
            ,xxcus.xxcuspn_ld_gldesc_tbl gldcr
            ,(SELECT loc2.location_id
                    ,loc2.parent_location_id
                    ,loc2.active_end_date
                    ,loc2.location_code
                    ,loc2.space_type_lookup_code
                FROM pn.pn_locations_all loc2) loc2
            ,(SELECT loc1.location_id
                    ,loc1.parent_location_id
                    ,address_id
                    ,loc1.property_id
                    ,loc1.attribute1
                    ,loc1.attribute2
                    ,loc1.location_code
                    ,loc1.active_end_date
                FROM pn.pn_locations_all loc1) loc1
            ,(SELECT entrp_entity
                    ,entrp_loc
                    ,fru
                FROM apps.xxcus_location_code_vw) corp
       WHERE empsp.location_id = loc.location_id(+)
         AND loc1.property_id = prop.property_id(+)
         AND empsp.cost_center_code = gldcr.segment2(+)
         AND loc.parent_location_id = loc2.location_id
         AND loc2.parent_location_id = loc1.location_id
         AND empsp.attribute1 = corp.fru
         AND gldcr.l_corp_id = empsp.attribute1
         AND loc.active_end_date = '31-DEC-4712'
         AND loc1.active_end_date = '31-DEC-4712'
         AND loc2.active_end_date = '31-DEC-4712'
         AND nvl(empsp.emp_assign_end_date,
                 to_date('31-DEC-4712', 'DD-MON-YYYY')) =
             (SELECT MAX(nvl(spc.emp_assign_end_date,
                             to_date('31-DEC-4712', 'DD-MON-YYYY')))
                FROM pn.pn_space_assign_emp_all spc
               WHERE spc.attribute1 = empsp.attribute1
                 AND spc.location_id = empsp.location_id);
  
    COMMIT;
  
  
  END create_pn_data_set;


  /*******************************************************************************
  * Procedure:   UC4_AUTOADJUSTMENT
  * Description: This procedure is for loading the apxcmmn  objects.
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  5.3     09/04/2014    Manny           --sr 520376 fixed procedure name
                                          
  ********************************************************************************/



  PROCEDURE load_pnld_all(errbuf  OUT VARCHAR2
                         ,retcode OUT NUMBER) IS
  
    --Intialize Variables
    l_err_msg        VARCHAR2(2000);
    l_err_code       NUMBER;
    l_sec            VARCHAR2(150);
    l_procedure_name VARCHAR2(75) := 'XXCUS_APXCMMN_LOAD_PKG.LOAD_PNLD_ALL';  --5.3
  
  BEGIN
    --dbms_output.enable(1000000);
  
    l_sec := 'Refresh data tables';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    apps.xxcus_apxcmmn_load_pkg.create_pn_data_set(errbuf, retcode);
  
    ------------------------------------------------------------------------------
    --                   Refresh PNLD Master View                               --
    ------------------------------------------------------------------------------
  
    l_sec := 'Truncate the summary table in APXCMMN Master - PNLD_MASTER_STG.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    apxcmmn.hds_apxcmmn_ddl_pkg.truncate_pnld_master_view@apxprd_lnk.hsi.hughessupply.com;
  
    l_sec := 'Insert into PNLD Master STG.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    FOR c_pnld_master_view IN (SELECT *
                                 FROM apps.xxcus_pnld_master_vw)
    LOOP
    
      /*     INSERT INTO apxcmmn.pnld_master_VIEW@apxprd_lnk.hsi.hughessupply.com*/ --V3.3
      INSERT INTO apxcmmn.pnld_master_stg@apxprd_lnk.hsi.hughessupply.com --V3.3
      VALUES
        (c_pnld_master_view.fru
        ,c_pnld_master_view.segment_description
        ,c_pnld_master_view.fru_location_name
        ,c_pnld_master_view.bu_desc
        ,c_pnld_master_view.pos_system
        ,c_pnld_master_view.ops_nickname
        ,c_pnld_master_view.primary_ops_type
        ,c_pnld_master_view.operations_type
        ,c_pnld_master_view.fru_type
        ,c_pnld_master_view.ops_status
        ,c_pnld_master_view.fru_status
        ,c_pnld_master_view.lob_occupancy
        ,c_pnld_master_view.currency
        ,c_pnld_master_view.language
        ,c_pnld_master_view.primary_bld
        ,c_pnld_master_view.tenure
        ,c_pnld_master_view.timezone
        ,c_pnld_master_view.fru_open_date
        ,c_pnld_master_view.segment_id
        ,c_pnld_master_view.lob_name
        ,c_pnld_master_view.bu_desc
        ,c_pnld_master_view.emp_space_assign_id
        ,c_pnld_master_view.location_id
        ,c_pnld_master_view.emp_assign_start_date
        ,c_pnld_master_view.oracle_id
        ,c_pnld_master_view.allocated_area_pct
        ,c_pnld_master_view.allocated_area
        ,c_pnld_master_view.utilized_area
        ,c_pnld_master_view.lob_branch
        ,c_pnld_master_view.location_type
        ,c_pnld_master_view.loc_desc
        ,c_pnld_master_view.longitude
        ,c_pnld_master_view.latitude
        ,c_pnld_master_view.location_code
        ,c_pnld_master_view.location_code_lvl1
        ,c_pnld_master_view.location_code_lvl2
        ,c_pnld_master_view.location_code_lvl3
        ,c_pnld_master_view.loc_sqaure_feet
        ,c_pnld_master_view.loc_acre
        ,c_pnld_master_view.operating_hours
        ,c_pnld_master_view.walk_in
        ,c_pnld_master_view.customer_site
        ,c_pnld_master_view.signage
        ,c_pnld_master_view.operations_open_date
        ,c_pnld_master_view.operations_closed_date
        ,c_pnld_master_view.operations_square_footage
        ,c_pnld_master_view.operations_acreage
        ,c_pnld_master_view.uom_code
        ,c_pnld_master_view.rentable_area
        ,c_pnld_master_view.usable_area
        ,c_pnld_master_view.gross_area
        ,c_pnld_master_view.address_line1
        ,c_pnld_master_view.address_line2
        ,c_pnld_master_view.address_line3
        ,c_pnld_master_view.address_line4
        ,c_pnld_master_view.county
        ,c_pnld_master_view.city
        ,c_pnld_master_view.state
        ,c_pnld_master_view.province
        ,c_pnld_master_view.country
        ,c_pnld_master_view.zip_code
        ,c_pnld_master_view.address_id
        ,c_pnld_master_view.assignment_type
        ,c_pnld_master_view.site_type
        ,c_pnld_master_view.countyname
        ,c_pnld_master_view.metro_latitude
        ,c_pnld_master_view.metro_longitude
        ,c_pnld_master_view.metroarea
        ,c_pnld_master_view.contact_address_type
        ,c_pnld_master_view.remit_address
        ,c_pnld_master_view.mailing_address
        ,c_pnld_master_view.mailing_suite
        ,c_pnld_master_view.fru_count
        ,c_pnld_master_view.loc_count
        ,c_pnld_master_view.phone
        ,c_pnld_master_view.fax
        ,c_pnld_master_view.fed_tax_id
        ,c_pnld_master_view.company
        ,c_pnld_master_view.branch_manager
        ,c_pnld_master_view.emp_count
        ,c_pnld_master_view.location_open_date
        ,c_pnld_master_view.location_close_date --v3.1
        ,c_pnld_master_view.tenancy_type
        ,c_pnld_master_view.market_cluster); --v3.1
      COMMIT;
    
    END LOOP;
  
    l_sec := 'Calling apxcmmn.update_pnld_fru_data'; --v.3.3 Start
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    apxcmmn.hds_apxcmmn_ddl_pkg.update_pnld_fru_data@apxprd_lnk.hsi.hughessupply.com; --v3.3 End
    /*
        ------------------------------------------------------------------------------
        --                   Refresh POS/Branch System Code                         --
        ------------------------------------------------------------------------------
        l_sec := 'Truncate the summary table in APXCMMN POS.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
    
        apxcmmn.hds_apxcmmn_ddl_pkg.truncate_orafin_pos_view@apxprd_lnk.hsi.hughessupply.com;
    
        l_sec := 'Insert into the summary table in APXCMMN POS.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
    
        FOR c_pos_view IN (SELECT p.pos_system_code
                                 ,y.*
                             FROM apps.xxhsi_bif_branch_vw_prod y
                                 ,apps.xxhsi_branch_pos_vw      p
                            WHERE y.corp_id = p.corp_id(+))
        LOOP
    
          --INSERT INTO apxcmmn.hds_orafin_pos_branch_tbl@apxprd_lnk.hsi.hughessupply.com
          INSERT INTO apxcmmn.hds_orafin_pos_branch_stg@apxprd_lnk.hsi.hughessupply.com --v3.3
          VALUES
            (c_pos_view.pos_system_code
            ,c_pos_view.corp_id
            ,c_pos_view.product
            ,c_pos_view.location
            ,c_pos_view.cost_center
            ,c_pos_view.branch_name
            ,c_pos_view.product_desc
            ,c_pos_view.location_desc
            ,c_pos_view.cost_center_desc
            ,c_pos_view.leg_loc
            ,c_pos_view.creation_date
            ,c_pos_view.posting);
    
          COMMIT;
    
        END LOOP;
        --v3.3 Begin
        apxcmmn.hds_apxcmmn_ddl_pkg.update_orafin_pos_data@apxprd_lnk.hsi.hughessupply.com;
    
        l_sec := 'update_orafin_pos_data';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        --v3.3 End
    */
    ------------------------------------------------------------------------------
    --                   Refresh FRULOC TBL/VW                                  --
    ------------------------------------------------------------------------------
  
    l_sec := 'Truncate the summary table in APXCMMN Master - hds_fruloc_master_stg.'; --v3.3
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    apxcmmn.hds_apxcmmn_ddl_pkg.truncate_orafin_fruloc_view@apxprd_lnk.hsi.hughessupply.com;
  
  
    l_sec := 'Insert into summary table in APXCMMN Master.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    --dbms_output.put_line('outside of loop');
    FOR c_fruloc_master_view IN (SELECT *
                                   FROM apps.xxcus_fruloc_master_vw)
    LOOP
      --dbms_output.put_line('inside of loop');
      INSERT INTO apxcmmn.hds_fruloc_master_stg@apxprd_lnk.hsi.hughessupply.com --v3.3
      VALUES
        (c_fruloc_master_view.fruloc
        ,c_fruloc_master_view.fru
        ,c_fruloc_master_view.loc
        , --added
         --c_fruloc_master_view.int_use_only,   --Renamed PRIMARY_LOC
         c_fruloc_master_view.segment_description
        ,c_fruloc_master_view.fru_description
        , --Renamed FRU_LOCATION_NAME
         c_fruloc_master_view.bu_description
        ,c_fruloc_master_view.pos_system
        ,
         --c_fruloc_master_view.ops_nickname,
         c_fruloc_master_view.primary_ops_type
        ,c_fruloc_master_view.operations_type
        ,c_fruloc_master_view.assignment_type
        , --Renamed from FRU_TYPE
         c_fruloc_master_view.ops_status
        ,c_fruloc_master_view.emp_status
        , --Renamed from FRU_STATUS
         --c_fruloc_master_view.lob_occupancy,
         c_fruloc_master_view.currency
        ,c_fruloc_master_view.language
        ,c_fruloc_master_view.primary_bld
        ,c_fruloc_master_view.timezone
        ,c_fruloc_master_view.fru_open_date
        ,c_fruloc_master_view.fps_id
        ,c_fruloc_master_view.lob_name
        ,c_fruloc_master_view.fps_desc
        ,c_fruloc_master_view.emp_space_assign_id
        ,c_fruloc_master_view.location_id
        ,c_fruloc_master_view.emp_assign_start_date
        ,c_fruloc_master_view.property_id
        , --Renamed from COST_CENTER_CODE
         c_fruloc_master_view.allocated_area_pct
        ,c_fruloc_master_view.allocated_area
        ,c_fruloc_master_view.utilized_area
        ,c_fruloc_master_view.lob_branch
        ,c_fruloc_master_view.location_type
        ,c_fruloc_master_view.loc_desc
        ,c_fruloc_master_view.longitude
        ,c_fruloc_master_view.latitude
        ,c_fruloc_master_view.location_code
        ,c_fruloc_master_view.location_code_lvl1
        ,c_fruloc_master_view.location_code_lvl2
        ,c_fruloc_master_view.location_code_lvl3
        ,c_fruloc_master_view.loc_square_feet
        ,c_fruloc_master_view.loc_acre
        ,c_fruloc_master_view.operating_hours
        ,c_fruloc_master_view.walk_in
        ,c_fruloc_master_view.customer_site
        ,c_fruloc_master_view.signage
        ,c_fruloc_master_view.operations_open_date
        ,c_fruloc_master_view.operations_closed_date
        ,c_fruloc_master_view.operations_square_footage
        ,c_fruloc_master_view.operations_acreage
        ,c_fruloc_master_view.uom_code
        ,c_fruloc_master_view.rentable_area
        ,c_fruloc_master_view.usable_area
        ,
         --c_fruloc_master_view.gross_area,
         c_fruloc_master_view.address_line1
        ,c_fruloc_master_view.address_line2
        ,
         --c_fruloc_master_view.address_line3,
         --c_fruloc_master_view.address_line4,
         c_fruloc_master_view.county
        ,c_fruloc_master_view.city
        ,c_fruloc_master_view.state
        ,c_fruloc_master_view.province
        ,c_fruloc_master_view.country
        ,c_fruloc_master_view.zip_code
        ,c_fruloc_master_view.address_id
        ,c_fruloc_master_view.assignment_type_descr
        ,c_fruloc_master_view.site_type
        ,c_fruloc_master_view.fru_count
        ,c_fruloc_master_view.loc_count
        ,c_fruloc_master_view.phone
        ,c_fruloc_master_view.fax
        ,c_fruloc_master_view.fed_tax_id
        ,c_fruloc_master_view.branch_manager
        ,c_fruloc_master_view.emp_count
        ,c_fruloc_master_view.location_open_date
        , --v3.1  start
         c_fruloc_master_view.location_close_date
        ,c_fruloc_master_view.tenancy_type
        , --v3.1  end);
         c_fruloc_master_view.country_abbr --v3.2
        ,c_fruloc_master_view.market_cluster
        ,c_fruloc_master_view.loc_image --v5.1
         );
      COMMIT;
    
    END LOOP;
  
    l_sec := 'Truncate the summary table in APXCMMN FRULOC Single.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    apxcmmn.hds_apxcmmn_ddl_pkg.update_fruloc_data@apxprd_lnk.hsi.hughessupply.com; --v3.3
  
    ------------------------------------------------------------------------------
    --                   Refresh Location Contact data                          --
    ------------------------------------------------------------------------------
  
    l_sec := 'Truncate the summary table in APXCMMN FRULOC Contacts.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    apxcmmn.hds_apxcmmn_ddl_pkg.truncate_fruloc_contact_view@apxprd_lnk.hsi.hughessupply.com;
  
    FOR c_pnld_contact_view IN (SELECT *
                                  FROM apps.xxcuspn_fruloc_contact_vw)
    LOOP
    
      INSERT INTO apxcmmn.hds_loc_contact_stg@apxprd_lnk.hsi.hughessupply.com --v3.3
      VALUES
        (c_pnld_contact_view.location_code
        ,c_pnld_contact_view.location_code5
        ,c_pnld_contact_view.contact_address_type
        ,c_pnld_contact_view.address
        ,c_pnld_contact_view.suiteunit
        ,c_pnld_contact_view.stateprov
        ,c_pnld_contact_view.city
        ,c_pnld_contact_view.postal
        ,c_pnld_contact_view.company_site_id
        ,c_pnld_contact_view.company_id
        ,c_pnld_contact_view.company_number
        ,c_pnld_contact_view.comp_name
        ,c_pnld_contact_view.phone
        ,c_pnld_contact_view.fax);
    
      COMMIT;
    
    END LOOP;
  
    apxcmmn.hds_apxcmmn_ddl_pkg.update_loc_contact@apxprd_lnk.hsi.hughessupply.com; --v3.3
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    WHEN OTHERS THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
  END;
END;
/
