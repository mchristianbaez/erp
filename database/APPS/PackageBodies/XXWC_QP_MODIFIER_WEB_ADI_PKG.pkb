CREATE OR REPLACE PACKAGE BODY APPS.XXWC_QP_MODIFIER_WEB_ADI_PKG IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   ********************************************************************************************
   *   $Header xxwc_price_list_web_adi_pkg.pkb $
   *   Module Name: xxwc_price_list_web_adi_pkg.pkb
   *
   *   PURPOSE:   This package is used by the Price List Conversion
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   *   2.0        15-FEB-2013 Satish Upadhyayula        Satish U: 15-FEB-2013 :
   *                                                    TMS# 20130215-01073   UPdate Procedure Is modified
   *   2.1        22-MAR-2013 Satish Upadhyayula        TMS# 20130322-01259 :  Performance Tuning.
   *                                                    Modified API's Upload and Import
   *                                                    Upload API and Import API are modified
   *   2.2        08-Oct-2014 Shankar Hariharan         TMS# 20141001-00177 Multi-Org Canada Change
   * *******************************************************************************************/

   /*******************************************************************************************
   *   Procedure : LOG_MSG
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *   Parameter:
   *          IN
   *              p_debug_level    -- Debug Level
   *              p_mod_name       -- Module Name
   *              p_debug_msg      -- Debug Message
   * ********************************************************************************************/

   -- add debug message to log table and concurrent log file
   PROCEDURE LOG_MSG (p_debug_level   IN NUMBER,
                      p_mod_name      IN VARCHAR2,
                      p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
       -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      Fnd_File.Put_Line(Fnd_File.LOG, P_DEBUG_MSG);
      --COMMIT;
   END LOG_MSG;



   /*************************************************************************
    *   Procedure : Get_product_attr_value
    *
    *   PURPOSE:   This procedure is used to get the item number or the category
    *   Parameter:
    *          IN
    *              p_product_attribute  -- product_attribute
    *              p_product_attr_value -- product_attr_value
    * ************************************************************************/

   FUNCTION Get_product_attr_value (p_product_attribute    IN VARCHAR2,
                                    p_product_attr_value   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_return_value   VARCHAR2 (240) := NULL;
   BEGIN
      IF p_product_attribute = 'PRICING_ATTRIBUTE1' THEN
         SELECT   segment1
         INTO   l_return_value
         FROM   apps.mtl_system_items_b
         WHERE   inventory_item_id = TO_NUMBER (p_product_attr_value)
         AND ROWNUM = 1;
      ELSIF p_product_attribute = 'PRICING_ATTRIBUTE2'  THEN
         SELECT   category_concat_segs
         INTO   l_return_value
         FROM   apps.mtl_categories_v
         WHERE   category_id = TO_NUMBER (p_product_attr_value);
      ELSIF p_product_attribute = 'PRICING_ATTRIBUTE3'  THEN
         l_return_value := 'ALL';
      END IF;

      RETURN l_return_value;
   EXCEPTION
      WHEN OTHERS  THEN
         RETURN l_return_value;
   END Get_product_attr_value;

   /*************************************************************************
    *   Procedure : Get_Formula_name
    *
    *   PURPOSE:   This procedure is used to get formula name
    *   Parameter:
    *          IN
    *              p_formula_id  -- product_attribute
    * ************************************************************************/

   FUNCTION Get_Formula_name (p_formula_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_return_value   VARCHAR2 (300) := NULL;
   BEGIN
      SELECT   name
        INTO   l_return_value
        FROM   apps.qp_price_formulas_vl
       WHERE   price_formula_id = p_formula_id;

      RETURN l_return_value;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN l_return_value;
   END Get_Formula_name;

 /*  *************************************************************************
    *   Function: Get_Formula_ID
    *
    *   PURPOSE:   This procedure is used to get formula ID
    *   Parameter:
    *          IN
    *              p_formula_name
    * ************************************************************************/

   FUNCTION Get_Formula_id (p_formula_name IN VARCHAR2)
      RETURN NUMBER
   IS
      l_return_value   NUMBER(10) := NULL;
   BEGIN
      SELECT   Price_Formula_ID
        INTO   l_return_value
        FROM   apps.qp_price_formulas_vl
       WHERE   Name = p_formula_name;

      RETURN l_return_value;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN l_return_value;
   END Get_Formula_id;

   /*************************************************************************
    *   Procedure : VAL_MOD_LIST_NUMBER
    *
    *   PURPOSE:   This procedure is used to Validate Modifer List Name
    *   Parameter:
    *          IN
    *               P_List_Header_ID        --  Modifier List Header Id
    *               P_MODIFIER_LIST_NUMBER  -- Modifer List Name
    * ************************************************************************/

   PROCEDURE VAL_MOD_LIST_NUMBER ( P_List_Header_ID       In  Number,
                          P_MODIFIER_LIST_NUMBER IN  Varchar2,
                          x_Return_Status        OUT Varchar2 )  IS
      l_Count Number ;
   Begin
      X_Return_Status := G_Return_Success;
      Select Count(*)
      Into l_Count
      From apps.qp_list_headers
      Where Name = p_Modifier_List_Number
      And  List_Header_ID <> p_List_Header_ID ;

      If l_Count > 0 Then
         X_Return_Status := G_Return_Error;
      Else
         X_Return_Status := G_Return_Success;
      End If;

   End VAL_MOD_LIST_NUMBER;


   /*************************************************************************
    *   Procedure : Get_Lookup_Code
    *
    *   PURPOSE:   This Function will return Lookup Code for given Lookup TYpe and Meaning
    *   Parameter:
    *          IN
    *               p_Lookup_Type       --  Lookup Type
    *              p_Meaning            -- Lookup Meaning
    * ************************************************************************/

   FUNCTION Get_Lookup_Code( p_Lookup_Type   Varchar2,
                             p_Meaning       Varchar2 ) RETURN Varchar2 IS
      l_Lookup_Code QP_LOOKUPS.Lookup_Code%Type;
   BEGIN

      SELECT Lookup_Code
      INTO l_Lookup_Code
      FROM apps.qp_lookups
      WHERE Meaning     = p_Meaning
      AND lookup_type   = p_lookup_type ;

      RETURN l_Lookup_Code;

   EXCEPTION
      When NO_DATA_FOUND Then
         Return Null;
      When OTHERS Then
         Return Null;
   End Get_Lookup_Code;

    /*************************************************************************
    *   Procedure : Upload
    *
    *   PURPOSE:   This procedure is called from the WEDADI excel spreadsheet
    *   Parameter:
    *          IN
    *    P_LIST_HEADER_ID               NUMBER,
    *    P_MODIFIER_LIST_NAME           VARCHAR2,
    *    P_MODIFIER_LIST_NUMBER         VARCHAR2,
    *    P_LN_ORIG_SYS_LINE_REF         VARCHAR2,
    *    P_LN_LIST_LINE_ID              NUMBER,
    *    P_LN_LIST_LINE_NO              VARCHAR2,
    *    P_LN_MODIFIER_LEVEL            VARCHAR2,
    *    P_LN_LIST_LINE_TYPE            VARCHAR2,
    *    P_LN_START_DATE_ACTIVE         DATE,
    *    P_LN_END_DATE_ACTIVE           DATE,
    *    P_LN_AUTOMATIC_FLAG            VARCHAR2,
    *    P_LN_OVERRIDE_FLAG             VARCHAR2,
    *    P_LN_PRICING_PHASE             VARCHAR2,
    *    P_LN_PRODUCT_PRECEDENCE        NUMBER,
    *    P_LN_PRICE_BREAK_TYPE_CODE     VARCHAR2,
    *    P_LN_PRODUCT_UOM_CODE          VARCHAR2,
    *    P_LN_ARITHMETIC_OPERATOR_TYPE  VARCHAR2,
    *    P_LN_OPERAND                   NUMBER,
    *    P_LN_ACCRUAL_FLAG              VARCHAR2,
    *    P_PA_ORIG_SYS_PRICING_ATTR_REF VARCHAR2,
    *    P_PA_PRODUCT_ATTRIBUTE_CONTEXT VARCHAR2,
    *    P_PA_PRODUCT_ATTRIBUTE         VARCHAR2,
    *    P_PA_PRODUCT_ATTR_VALUE        VARCHAR2,
    *    P_PA_PRICING_ATTRIBUTE         VARCHAR2,
    *    P_PA_COMPARISON_OPERATOR_CODE  VARCHAR2,
    *    P_PA_PRICING_ATTRIBUTE_CONTEXT VARCHAR2,
    *    P_PA_PRICING_ATTR_VALUE_FROM   VARCHAR2,
    *    P_PA_PRICING_ATTR_VALUE_TO     VARCHAR2,
    *    P_QL_ORIG_SYS_QUALIFIER_REF    VARCHAR2,
    *    P_QL_QUALIFIER_GROUPING_NO     NUMBER,
    *    P_QL_QUALIFIER_CONTEXT         VARCHAR2,
    *    P_QL_QUALIFIER_ATTR_VALUE      VARCHAR2,
    *    P_QL_QUALIFIER_ATTR_VALUE_TO   VARCHAR2,
    *    P_QL_COMPARISON_OPERATOR_CODE  VARCHAR2,
    *    P_QL_QUALIFIER_PRECEDENCE      NUMBER );
    * ************************************************************************/

  -- Satish U : 03-JAN-2013 :  Ticket # 20130104-02019
  -- Made changes to  address issues of  Price Break Qty Overlap Issue
  -- Satish U: 15-FEB-2013 : TMS# 20130215-01073  : Made changes to address Date OVerlaop and Price Break Qty Overlap Issues.
  --  While validating Date Over Lap check shoul also check for Price Over lap too.
  -- Satish U # 20130322-01259 :  Performance Tuning.
  --  Comment the logic that validates Date Overlap/Price Break Overlap
   --- TMS # 20130322-01259  Satish U : Performance Improvement steps
  -- SatishU : 30-MAR_2013 :  TO improve performance we will be removing some of the columns
-- from WEB ADI Template  These columns will have Constant values for new List Lines and
-- for update we will be using G_MISS_CHAR, G_MISS_NUM, G_MISS_DATE values.
--  Here are the list of columns that are taken out.
-- P_LN_ORIG_SYS_LINE_REF, P_LN_LIST_LINE_NO, P_LN_MODIFIER_LEVEL, P_LN_LIST_LINE_TYPE
-- P_LN_AUTOMATIC_FLAG, P_LN_OVERRIDE_FLAG, P_LN_PRODUCT_PRECEDENCE, P_LN_INCLUDE_ON_RETURNS_FLAG
--
-- Following Columns are not removed
-- P_LN_LIST_LINE_ID, P_PA_PRICING_ATTRIBUTE_ID, P_LN_INCOMPATIBILITY_GRP_CODE

   PROCEDURE UPLOAD (P_LIST_HEADER_ID  NUMBER,
        P_MODIFIER_LIST_NAME           VARCHAR2,
        P_MODIFIER_LIST_NUMBER         VARCHAR2,
        P_CURRENCY_CODE                VARCHAR2,
        P_LIST_TYPE_CODE               VARCHAR2,
        P_START_DATE_ACTIVE            DATE ,
        P_END_DATE_ACTIVE              DATE,
        P_GLOBAL_FLAG                  VARCHAR2,
        P_AUTOMATIC_FLAG               VARCHAR2,
        P_LN_LIST_LINE_ID              NUMBER,
        P_LN_START_DATE_ACTIVE         DATE,
        P_LN_END_DATE_ACTIVE           DATE,
        P_LN_PRICE_BREAK_TYPE_CODE     VARCHAR2,
        P_LN_ARITHMETIC_OPERATOR_TYPE  VARCHAR2,
        P_LN_OPERAND                   NUMBER,
        P_LN_INCOMPATIBILITY_GRP_CODE  VARCHAR2,
        P_LN_FORMULA                   VARCHAR2,
        P_PA_PRICING_ATTRIBUTE_ID      NUMBER ,
        P_PA_PRODUCT_ATTRIBUTE_CONTEXT VARCHAR2 ,
        P_PA_PRODUCT_ATTRIBUTE         VARCHAR2,
        P_PA_PRODUCT_ATTR_VALUE_ITEM   VARCHAR2,
        P_PA_PRODUCT_ATTR_VALUE_CAT    VARCHAR2 ,
        P_PA_PRODUCT_UOM_CODE          VARCHAR2,
        P_PA_PRICING_ATTRIBUTE         VARCHAR2 ,
        P_PA_COMPARISON_OPERATOR_CODE  VARCHAR2,
        P_PA_PRICING_ATTRIBUTE_CONTEXT VARCHAR2 ,
        P_PA_PRICING_ATTR_VALUE_FROM   VARCHAR2,
        P_PA_PRICING_ATTR_VALUE_TO     VARCHAR2
        )
   IS
       -- Prameters that are removed,
        -- P_LN_ORIG_SYS_LINE_REF         VARCHAR2 Default NULL,
        -- P_LN_LIST_LINE_NO              VARCHAR2 Default NULL,
        -- P_LN_MODIFIER_LEVEL            VARCHAR2 Default NULL,
        -- P_LN_LIST_LINE_TYPE            VARCHAR2 Default NULL,
        --P_LN_AUTOMATIC_FLAG            VARCHAR2 Default NULL,
        --P_LN_OVERRIDE_FLAG             VARCHAR2 Default NULL,
        --P_LN_PRICING_PHASE             VARCHAR2 Default NULL,
        --P_LN_PRODUCT_PRECEDENCE        NUMBER   Default NULL,
        --P_LN_ACCRUAL_FLAG              VARCHAR2 Default NULL,
        --P_LN_PRICING_GROUP_SEQUENCE    NUMBER   Default NULL,
        --P_LN_INCLUDE_ON_RETURNS_FLAG   VARCHAR2 Default NULL,
        --P_LN_QUALIFICATION_IND         NUMBER   Default NULL,
        --P_PA_ORIG_SYS_PRICING_ATTR_REF VARCHAR2 Default NULL,
         --P_PA_EXCLUDER_FLAG             VARCHAR2 Default NULL,
        --P_PA_ATTRIBUTE_GROUPING_NO     VARCHAR2 Default NULL

      --  X_Batch_ID                Out  Number )
      l_mod_name                 VARCHAR2 (100)  := 'XXWC_QP_MODIFIER_WEB_ADI_PKG.UPLOAD :' ;
      l_return_status            VARCHAR2(1);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2(2000);
      l_LN_MODIFIER_LEVEL_CODE   QP_LIST_LINES.MODIFIER_LEVEL_CODE%TYPE;
      l_LN_LIST_LINE_TYPE_CODE   QP_LIST_LINES.LIST_LINE_TYPE_CODE%TYPE;
      l_LN_PRICING_PHASE_ID      QP_LIST_LINES.PRICING_PHASE_ID%TYPE;
      l_LN_ARITHMETIC_OPERATOR   QP_LIST_LINES.ARITHMETIC_OPERATOR%TYPE;
      l_LN_AUTOMATIC_FLAG        QP_LIST_LINES.AUTOMATIC_FLAG%TYPE;
      l_LN_LIST_LINE_NO          QP_LIST_LINES.LIST_LINE_NO%TYPE;
      l_LN_OVERRIDE_FLAG         QP_LIST_LINES.OVERRIDE_FLAG%TYPE;
      l_LN_PRODUCT_PRECEDENCE    QP_LIST_LINES.PRODUCT_PRECEDENCE%TYPE;
      l_LN_PRICING_GROUP_SEQUENCE  QP_LIST_LINES.PRICING_GROUP_SEQUENCE%TYPE;
      l_LN_INCLUDE_ON_RETURNS_FLAG  QP_LIST_LINES.INCLUDE_ON_RETURNS_FLAG%TYPE;
      l_AUTOMATIC_FLAG           QP_LIST_HEADERS_ALL.AUTOMATIC_FLAG%TYPE;


      --l_PA_PRICING_ATTRIBUTE_ID  QP_PRICING_ATTRIBUTES.PRICING_ATTRIBUTE_ID%TYPE;
      L_PA_PRODUCT_ATTR_VALUE    Varchar2(30) ;
      l_PA_PROUDCT_ATTRIBUTE     Varchar2(30) ;
      l_Primary_UOM_Code         Varchar2(30) ;


      L_LN_PRICE_BY_FORMULA_ID   Number ;
      l_Line_COunt               Number;

      C_STATUS_VALIDATE    Constant Varchar2(1) := 'V' ;
      C_STATUS_ERROR       Constant Varchar2(1) := 'E' ;
      C_START_DATE         Constant Date        :=  ADD_MONTHS(SYSDATE, -120) ;
      C_END_DATE           Constant Date        :=  ADD_MONTHS(SYSDATE, 120) ;
      l_file_val           Varchar2(60) ;
      l_UOM_Class_Item     Varchar2(30) ;
      l_UOM_Class_WEBADI   Varchar2(30) ;
      L_KNOWN_ERROR        Varchar2(1)  := 'N';


      l_PA_PRICING_ATTRIBUTE_CONTEXT   varchar2(30) ;

      l_UOM_Match                Varchar2(1) := 'N';
      l_Start_Date_Match         Varchar2(1) := 'N';
      l_End_Date_Match           Varchar2(1) := 'N';
      l_Dates_Overlap_Flag       Varchar2(1) := 'N';
      l_PriceBreak_Overlap_Flag  Varchar2(1) := 'N';
      l_From_Qty_Match           Varchar2(1) := 'N';
      l_To_Qty_Match             Varchar2(1) := 'N';
      l_LINE_ID_Match            Varchar2(1) := 'N';
      XXWC_EXCEPTION         Exception ;
      -- Satish U: 20-DEC-2011
      Cursor  Get_Mod_list_Cur ( p_List_Header_ID Number ) Is
         Select currency_code, list_type_code, source_system_code, pte_code
         From apps.qp_list_headers_all
         Where list_Header_ID = p_List_Header_ID  ;

      Cursor Get_Item_ID_Cur ( P_Item_Number Varchar2 ) IS
         Select Inventory_Item_ID
         From apps.Mtl_System_Items_B
         Where Segment1 = P_ITEM_NUMBER
         AND   Organization_ID = Fnd_profile.Value('XXWC_ITEM_MASTER_ORG')
         and Rownum = 1;

      Cursor Get_Cat_ID_Cur ( P_Category Varchar2 ) IS
         Select CATEGORY_ID
         From apps.XXWC_ITEM_CATEGORY_VW
         Where Item_Category = P_Category
         and Rownum = 1;

      -- CUrosr to get all the rows from stagign table  that match 'Modifier List Number', Item Number'
      -- Added Parameter P_LN_LIST_LINE_ID : Satish U : 04-DEC-2012
      Cursor Get_Modifier_Cur ( P_MODIFIER_LIST_NUMBER          Varchar2 ,
                                P_PA_PRODUCT_ATTRIBUTE_CONTEXT  Varchar2,
                                P_PA_PRODUCT_ATTRIBUTE          Varchar2,
                                P_PA_PRODUCT_ATTR_VALUE         Varchar2,
                                P_LN_LIST_LINE_ID               Number) Is
         Select qmod.ROWID, QMOD.*
         From apps.XXWC_QP_MODIFIER_INT  qmod
         Where MODIFIER_LIST_NUMBER         = P_MODIFIER_LIST_NUMBER
         And  PA_PRODUCT_ATTRIBUTE_CONTEXT  = P_PA_PRODUCT_ATTRIBUTE_CONTEXT
         And  PA_PRODUCT_ATTRIBUTE          = P_PA_PRODUCT_ATTRIBUTE
         And  PA_PRODUCT_ATTR_VALUE         = P_PA_PRODUCT_ATTR_VALUE
         AND  (P_LN_LIST_LINE_ID IS NULL OR P_LN_LIST_LINE_ID <> LN_LIST_LINE_ID )
         And  NVL(Status,'R') <> 'P';

       -- CUrosr to get all the rows from stagign table  that match 'Modifier List Number', Item Number'

      -- CUrosr to get all the rows from Base table  that match 'Modifier List Number', Item Number'
      -- Added Parameter P_LN_LIST_LINE_ID : Satish U : 04-DEC-2012
     Cursor Get_Modifier2_Cur ( P_MODIFIER_LIST_NUMBER          Varchar2 ,
                                P_PA_PRODUCT_ATTRIBUTE_CONTEXT  Varchar2,
                                P_PA_PRODUCT_ATTRIBUTE          Varchar2,
                                P_PA_PRODUCT_ATTR_VALUE_ITEM    Varchar2,
                                P_PA_PRODUCT_ATTR_VALUE_CAT     Varchar2,
                                P_LN_LIST_LINE_ID               Number) Is
         Select QMOD.*
         From apps.XXWC_QP_MODIFIERS_LIST_VW  qmod
         Where MODIFIER_LIST_NUMBER         = P_MODIFIER_LIST_NUMBER
         And  PA_PRODUCT_ATTRIBUTE_CONTEXT  = P_PA_PRODUCT_ATTRIBUTE_CONTEXT
         And  PA_PRODUCT_ATTRIBUTE          = P_PA_PRODUCT_ATTRIBUTE
         AND  (P_LN_LIST_LINE_ID IS NULL OR P_LN_LIST_LINE_ID <> LN_LIST_LINE_ID )
         AND ( (PA_PRODUCT_ATTRIBUTE        = 'Item Number'
           And  NVL(PA_PRODUCT_ATTR_VALUE_ITEM,'NULL')  = NVL(P_PA_PRODUCT_ATTR_VALUE_ITEM, 'NULL') ) Or
           ( PA_PRODUCT_ATTRIBUTE            = 'Item Category'
           And  NVL(PA_PRODUCT_ATTR_VALUE_CAT,'NULL')   = NVL(P_PA_PRODUCT_ATTR_VALUE_CAT,'NULL') )
           OR (PA_PRODUCT_ATTRIBUTE            = 'All Items' ));

     -- Satish U : 03-JAN-2013 : 20130104-02019
     -- Cursor to get From Qty and To Qty breaks for a given List Line Id from
     Cursor Get_Qty_Breaks_Cur (p_List_Line_ID Number ) Is
        Select   PA_PRICING_ATTR_VALUE_FROM, PA_PRICING_ATTR_VALUE_TO
        From apps.XXWC_QP_MODIFIER_INT
        Where List_Header_ID = P_LIST_Header_ID
        ANd LN_List_Line_Id = p_List_Line_ID ;

    C_NON_AUTOMATIC_MODIFIER  Varchar2(80) := 'ECOMM PRICE ZONE MODIFIER';

   BEGIN
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, 'At Begin');
      --initializing Return Status

      l_Return_Status := 'S' ;

      --Dbms_Output.Put_Line('01 : Begining  of the program');

      If G_Batch_ID is NUll  Then
         Select XXWC_QP_BATCH_S.Nextval
         Into G_Batch_ID
         From Dual;
         --X_Batch_ID  := G_Batch_ID ;
      End If;

      FND_MESSAGE.CLEAR;
      --/******************
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101 : p_Modifier_List_Name ='   || p_Modifier_List_Name);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '102 :p_Modifier_List_Number =' || p_Modifier_List_Number);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '103 :p_ln_list_line_id =' || p_LN_list_line_id);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '104 :p_pa_product_attribute =' || p_PA_product_attribute);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '105 :p_pa_product_attr_value_item =' || p_PA_product_attr_value_Item);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '106 :p_PA_product_uom_code =' || p_PA_product_uom_code);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '107: p_ln_operand =' || p_Ln_operand);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '108: p_ln_start_date_active =' || p_ln_start_date_active);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '109: p_ln_end_date_active =' || p_ln_end_date_active);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '110: p_list_Header_ID =' || p_List_Header_ID);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '111 :User_ID =' || fnd_global.user_id);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '112 :Resp_ID =' || fnd_global.resp_id);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '113: Resp_Appl_ID =' || fnd_global.Resp_Appl_id);
      -- **********************/
      fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                  resp_id        => fnd_global.resp_id,
                                  resp_appl_id   => fnd_global.resp_appl_id);

      SELECT   mo_global.get_current_org_id ()
      INTO g_org_id
      FROM DUAL;

      --Dbms_Output.Put_Line('03 : After Getting Current Org ID');
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '114: g_org_id =' || g_org_id);

      mo_global.set_policy_context ('S', g_org_id);


      --oe_msg_pub.initialize;  Satish U
      -- Validate  Modifer List Attributes

      VAL_MOD_LIST_NUMBER ( P_List_Header_ID       => P_LIST_HEADER_ID,
                          P_MODIFIER_LIST_NUMBER   => P_MODIFIER_LIST_NUMBER,
                          x_Return_Status          => l_Return_Status );
      --Dbms_Output.Put_Line(' 25 : Val Mod List Number :' || l_Return_Status);

      If l_Return_Status <> G_RETURN_SUCCESS Then
         l_msg_data  := 'Duplicate Modfier List Name';
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '115 Mod List Number Error =>' || l_msg_data);
         FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_DUPLICATE_MODIFER');
         -- 04-FEB-2012 :  To display Error Message in Spreadsheet
         -- 07-JUN-2012 : TO display error  message in Spreadsheet
         --l_MSG_Data :=  fnd_message.get();
         l_KNOWN_ERROR  := 'Y' ;
         --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
         Raise XXWC_EXCEPTION ;

      End If;

      -- TMS# 20130322-01259  Assign Value to l_AUTOMATIC_FLAG
      -- Satish U: 05-APR-2013 -- Automatic Flag is not passed through Spreadsheet
      -- Non Automatic Modifier should have Automatic Flag set to N
      -- Automatic_flag should be set to N if Modifier or Modifier List Line is expired
      IF P_END_DATE_ACTIVE IS NOT NULL AND P_END_DATE_ACTIVE <= SYSDATE THEN
         l_AUTOMATIC_FLAG := 'N';
      ELSE
         l_AUTOMATIC_FLAG := 'Y';
      ENd IF;
      -- Modifier List is Non Automatic Modifier Please Assign value 'N'
      IF P_MODIFIER_LIST_NAME = C_NON_AUTOMATIC_MODIFIER THEN
          l_AUTOMATIC_FLAG := 'N';
          l_LN_AUTOMATIC_FLAG := 'N';
      End IF;
      --TMS# 20130322-01259
      -- SatishU : 31-Mar-2013 : THis validation is removed as they are hard coded for new
      -- Records and existing records values will not be updated.

      /**************************Satish U: 31-MAR-2013 ****************************************

       -- Commented Validation of Modifier Level Code as this column as Constant value
       -- for new records and existing records value is not updated through WEB ADI interface.

      l_LN_MODIFIER_LEVEL_CODE := Get_Lookup_Code( p_Lookup_Type   => 'MODIFIER_LEVEL_CODE',
                                                   p_Meaning       => P_LN_MODIFIER_LEVEL ) ;
      If l_LN_MODIFIER_LEVEL_CODE Is NULL Then
         l_msg_data  := 'Invalid Modifier Level';
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '116 Mod Level Code Error => ' || l_msg_data);

         FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_INVALID_ENTITY');
         FND_MESSAGE.SET_TOKEN('ENTITY_NAME','Modifier Level');
         -- 04-FEB-2012 :  To display Error Message in Spreadsheet
         --l_MSG_Data :=  fnd_message.get();
         l_KNOWN_ERROR  := 'Y' ;
         Raise XXWC_EXCEPTION ;
         --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
      Else
         oe_debug_pub.add('116.1: Modifier Level Code Value is  :' || l_LN_MODIFIER_LEVEL_CODE );
      End If;

      --l_LN_LIST_LINE_TYPE_CODE
      -- Commented Validation of List Line TYpe Code as this column as Constant value
      -- for new records and existing records value is not updated through WEB ADI interface.

      l_LN_LIST_LINE_TYPE_CODE := Get_Lookup_Code( p_Lookup_Type   => 'LIST_LINE_TYPE_CODE',
                                                   p_Meaning       => P_LN_LIST_LINE_TYPE ) ;
      If l_LN_LIST_LINE_TYPE_CODE Is NULL Then
         l_msg_data  := 'Invalid List Line Type';
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '120 List Line Type Code Error => ' || l_msg_data);

         FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_INVALID_ENTITY');
         FND_MESSAGE.SET_TOKEN('ENTITY_NAME','List Line Type');
         -- 04-FEB-2012 :  To display Error Message in Spreadsheet
         --l_MSG_Data :=  fnd_message.get();
         l_KNOWN_ERROR  := 'Y' ;
         Raise XXWC_EXCEPTION ;
         --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
      Else
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Statement,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '121 List Line Type Code Found => ' || l_LN_LIST_LINE_TYPE_CODE);
         oe_debug_pub.add('111.1: Modifier List Line Type Code Value is  :' || l_LN_LIST_LINE_TYPE_CODE );
      End If;


      --l_LN_PRICING_PHASE_ID
      -- Commented Validation of Pricing Phase ID as this column as Constant value
      -- for new records and existing records value is not updated through WEB ADI interface.

      BEGIN
         SELECT Pricing_Phase_Id
         INTO l_LN_PRICING_PHASE_ID
         FROM QP_Pricing_Phases
         WHERE Name = P_LN_PRICING_PHASE ;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Statement,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '129 Pricing Phase Found => ' || l_LN_PRICING_PHASE_ID);
         oe_debug_pub.add('129.1: Pricing Phase ID is   :' || l_LN_PRICING_PHASE_ID );
      EXCEPTION
         When Others Then
            l_LN_PRICING_PHASE_ID := NULL;
            l_msg_data  := 'Invalid Pricing Phase' ;
            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '130 Pricing Phase Error => ' || l_MSG_Data);

            FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_INVALID_ENTITY');
            FND_MESSAGE.SET_TOKEN('ENTITY_NAME','Pricing Phase');
            -- 04-FEB-2012 :  To display Error Message in Spreadsheet
            --l_MSG_Data :=  fnd_message.get();
            l_KNOWN_ERROR  := 'Y' ;
            --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
            RAISE XXWC_EXCEPTION ;

      END;
      *******************************************************************/

      If P_LN_ARITHMETIC_OPERATOR_TYPE IS NOT NULL THEN
         -- l_LN_ARITHMETIC_OPERATOR
         l_LN_ARITHMETIC_OPERATOR := Get_Lookup_Code( p_Lookup_Type   => 'ARITHMETIC_OPERATOR',
                                                   p_Meaning       => P_LN_ARITHMETIC_OPERATOR_TYPE ) ;

         If l_LN_ARITHMETIC_OPERATOR Is NULL Then
            l_msg_data  := 'Invalid Arithmetic Operator' ;
            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '140 Arithmatic Operator Error => ' || l_MSG_Data);
            FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_INVALID_ENTITY');
            FND_MESSAGE.SET_TOKEN('ENTITY_NAME','Arithmetic Operator');
            -- 04-FEB-2012 :  To display Error Message in Spreadsheet
            --l_MSG_Data :=  fnd_message.get();
            l_KNOWN_ERROR  := 'Y' ;
            Raise XXWC_EXCEPTION ;
            --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
         Else
            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '142 Arithmatic Operator is Found => ' || l_LN_ARITHMETIC_OPERATOR);
         End If;
      End If;
      -- Satish U: 28-NOV-2011 :
      IF P_LN_START_DATE_ACTIVE is Not null And  P_LN_END_DATE_ACTIVE is Not null And P_LN_END_DATE_ACTIVE < P_LN_START_DATE_ACTIVE then

          l_msg_data  := 'End Date Should be greater then Start Date';
          XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '150 End Date is smaller then Start Date => ' ||l_msg_data);
          FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_DATE_PAIR_ERROR');
          --l_MSG_Data :=  fnd_message.get();
          l_KNOWN_ERROR  := 'Y' ;
          --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
          Raise XXWC_EXCEPTION ;

      END IF;

      -- Initialize Varialbes
      l_PA_PROUDCT_ATTRIBUTE   := NULL;
      L_PA_PRODUCT_ATTR_VALUE  := NULL;

      If P_PA_PRODUCT_ATTRIBUTE = 'Item Number' Then
         l_PA_PROUDCT_ATTRIBUTE   :=  'PRICING_ATTRIBUTE1'  ;
         FOr Item_Rec In Get_Item_ID_Cur ( P_PA_PRODUCT_ATTR_VALUE_ITEM ) Loop
            L_PA_PRODUCT_ATTR_VALUE := Item_Rec.Inventory_Item_ID ;
         ENd Loop ;
         If L_PA_PRODUCT_ATTR_VALUE IS NULL THEN
            -- This Product Attribute is New and not planned to be handled through WEB ADI
            FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_INVALID_ENTITY');
            FND_MESSAGE.SET_TOKEN('ENTITY_NAME','Item Number');
            -- 04-FEB-2012 :  To display Error Message in Spreadsheet
            --l_MSG_Data :=  fnd_message.get();
            l_KNOWN_ERROR  := 'Y' ;
            --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
            Raise XXWC_EXCEPTION ;
         END IF;

         -- Satishu : 02-MAY-2012 :  Check if UOM_CODE on spreadsheet belongs to the UOM_CLASS of the item's Primary UOM CODE
         -- Get UOM Class information for Item's Primary UOM Code -- Only when UOM COde is Not Null
         If P_PA_PRODUCT_UOM_CODE IS NOT NULL THEN
             Begin

                 Select  muom.UOM_CLASS , Primary_UOM_Code
                 Into l_UOM_Class_Item, l_Primary_UOM_Code
                 From apps.Mtl_System_Items_B msi,
                     apps.mtl_units_of_measure_tl muom
                 Where Inventory_Item_ID =  L_PA_PRODUCT_ATTR_VALUE
                 And Organization_ID = fnd_profile.Value('XXWC_ITEM_MASTER_ORG')
                 ANd msi.Primary_UOM_Code = muom.Uom_code;

                 XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '160 UOM Code is  => ' || l_Primary_UOM_Code);
                 XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '161 UOM Class Item is  => ' || l_UOM_Class_Item);


                 Select UOM_Class
                 Into l_UOM_CLass_WEBADI
                 From  apps.mtl_units_of_measure_tl muom
                 Where muom.Uom_CODE = P_PA_PRODUCT_UOM_CODE
                 AND  muom.UOM_CLass = l_UOM_Class_Item ;

             Exception
                When Others then
                   XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '162 UOM Class or UOM Code is not found  => ' );
                   FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_INVALID_UOM_CODE');
                   FND_MESSAGE.SET_TOKEN('UOM_CODE1',P_PA_PRODUCT_UOM_CODE);
                   FND_MESSAGE.SET_TOKEN('UOM_CODE2',l_Primary_UOM_Code);

                   -- 04-FEB-2012 :  To display Error Message in Spreadsheet
                   --l_MSG_Data :=  fnd_message.get();
                   l_KNOWN_ERROR  := 'Y' ;
                   --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
                   Raise XXWC_EXCEPTION ;
             End ;
         END IF;



      Elsif P_PA_PRODUCT_ATTRIBUTE = 'Item Category' Then
         l_PA_PROUDCT_ATTRIBUTE   :=  'PRICING_ATTRIBUTE2'  ;
         FOr Cat_Rec In Get_Cat_ID_Cur ( P_PA_PRODUCT_ATTR_VALUE_CAT ) Loop
            L_PA_PRODUCT_ATTR_VALUE := Cat_Rec.Category_ID ;
         ENd Loop ;

         If L_PA_PRODUCT_ATTR_VALUE IS NULL THEN
            -- This Product Attribute is New and not planned to be handled through WEB ADI
            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '163 Item Category is not found  => ' );
            FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_INVALID_ENTITY');
            FND_MESSAGE.SET_TOKEN('ENTITY_NAME','Item Category');
            -- 04-FEB-2012 :  To display Error Message in Spreadsheet
            --l_MSG_Data :=  fnd_message.get();
            l_KNOWN_ERROR  := 'Y' ;
            --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
            Raise XXWC_EXCEPTION ;
         END IF;
      Else
         -- This Product Attribute is New and not planned to be handled through WEB ADI
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '164 Product Attribute is not found  => ' );
         FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_INVALID_ENTITY');
         FND_MESSAGE.SET_TOKEN('ENTITY_NAME','Product Attribute');
         -- 04-FEB-2012 :  To display Error Message in Spreadsheet
         --l_MSG_Data :=  fnd_message.get();
         l_KNOWN_ERROR  := 'Y' ;
         --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
         Raise XXWC_EXCEPTION ;
      End IF;

      IF P_LN_FORMULA Is Not Null THen
         Begin
            SELECT Price_Formula_ID
            Into L_LN_PRICE_BY_FORMULA_ID
            FROM apps.QP_Price_Formulas_Vl
            WHERE Name = P_LN_FORMULA;
         End ;
         If L_LN_PRICE_BY_FORMULA_ID IS NULL THEN
            l_msg_data  := 'Invalid Pricing Formula' ;
            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '165 Pricing Formula is not found  => ' );
            -- 04-FEB-2012 :  To display Error Message in Spreadsheet
            FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_INVALID_ENTITY');
            FND_MESSAGE.SET_TOKEN('ENTITY_NAME','Pricing Formula');
            --l_MSG_Data :=  fnd_message.get();
            l_KNOWN_ERROR  := 'Y' ;
            --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
         END IF;
      End If;

      -- Satish U: 07-MAY-2012 :  Check if Pricing Attribute Values ( From And TO) are not Null and If Pricing Context is NULL  then
      -- assign default value 'VOLUME' to the context.
      IF ( P_PA_PRICING_ATTR_VALUE_FROM  IS NOT NULL
        AND P_PA_PRICING_ATTR_VALUE_TO IS NOT NULL ) AND  P_PA_PRICING_ATTRIBUTE_CONTEXT IS NULL THEN
        l_PA_PRICING_ATTRIBUTE_CONTEXT := 'VOLUME';
      ELSe
        l_PA_PRICING_ATTRIBUTE_CONTEXT := P_PA_PRICING_ATTRIBUTE_CONTEXT ;

      END IF;



      -- Before Inserting Staging Table Check Of Duplicates If exist then Throw Error.
      -- Initlialize Line Count Variable
      --TMS# 20130322-01259 Added P_LIST_HEader_ID to SQL QUery to use Index
      l_Line_Count := 0;
      Select Count(*)
      Into l_Line_Count
      From apps.XXWC_QP_MODIFIER_INT
      Where List_Header_ID = P_LIST_Header_ID
      AND LN_LIST_LINE_ID = P_LN_LIST_LINE_ID
      And  NVL(Status,'R') <> 'P';


      If L_Line_Count > 0 Then
         -- If So Delete the Modifier Line. Always override with latest line from Spreadsheet.

         Delete From apps.XXWC_QP_MODIFIER_INT
         Where LN_LIST_LINE_ID = P_LN_LIST_LINE_ID ;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '166 Deleting Duplicate Line for List Line ID  => ' || P_LN_LIST_LINE_ID  );
      End If;

      -- 05JUN2012 : Check if Line exists in interface table
      l_Line_Count := 0;
      /***************************Satish U : 22-MAR-2013 *********************************
      --- TMS # 20130322-01259  Performance Tuning : Commented following Logic in Upload API
      Select Count(*)
      Into l_Line_Count
      From XXWC_QP_MODIFIER_INT
      Where MODIFIER_LIST_NUMBER = P_MODIFIER_LIST_NUMBER
      And  PA_PRODUCT_ATTRIBUTE_CONTEXT  = P_PA_PRODUCT_ATTRIBUTE_CONTEXT
      And  PA_PRODUCT_ATTRIBUTE          = l_PA_PROUDCT_ATTRIBUTE
      And  PA_PRODUCT_ATTR_VALUE         = L_PA_PRODUCT_ATTR_VALUE
      And  NVL(Status,'R') <> 'P';

      If L_Line_Count > 0 Then  --  Duplicate Lines Check 100
         -- If So Delete the Modifier Line. Always override with latest line from Spreadsheet.
         -- SatishU : 06-JUL-2012 : Before Deleting Check if  Start Date and End Dates are overlapping and if UOM Code is same.
         -- IF UOM Code is different or if Start Date and End Date values  do not overlap then Do not Delete the Lines.

         For  Get_Mod_Rec In Get_Modifier_Cur (
                            P_MODIFIER_LIST_NUMBER          => P_MODIFIER_LIST_NUMBER ,
                            P_PA_PRODUCT_ATTRIBUTE_CONTEXT  => P_PA_PRODUCT_ATTRIBUTE_CONTEXT,
                            P_PA_PRODUCT_ATTRIBUTE          => l_PA_PROUDCT_ATTRIBUTE,
                            P_PA_PRODUCT_ATTR_VALUE         => l_PA_PRODUCT_ATTR_VALUE ,
                            P_LN_LIST_LINE_ID               => P_LN_LIST_LINE_ID) Loop
            l_UOM_Match               := 'N';
            l_Start_Date_Match        := 'N';
            l_End_Date_Match          := 'N';
            l_Dates_Overlap_Flag      := 'N';
            l_PriceBreak_Overlap_Flag := 'N';
            l_LINE_ID_Match           := 'N' ;
            l_From_Qty_Match           := 'N';
            l_To_Qty_Match             := 'N';

            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '167 PRoduct UOM Code is  => ' || P_PA_PRODUCT_UOM_CODE  );

            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '167.2 P_LN_LIST_LINE_ID  => ' || P_LN_LIST_LINE_ID  );
            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '167.3 LN_LIST_LINE_ID  => ' || Get_Mod_Rec.LN_LIST_LINE_ID  );
            -- Check if UOM Matches
            If NVL(Get_Mod_Rec.PA_PRODUCT_UOM_CODE,'NULL')  = NVL(P_PA_PRODUCT_UOM_CODE,'NULL') Then
              l_UOM_Match := 'Y';
            ENd If;

            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '168 PRoduct UOM Code Match Value  => ' || l_UOM_Match  );

            -- Check If Effectivity Dates Match
            If   ( NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) = NVL(P_LN_START_DATE_ACTIVE , C_END_DATE ))  Then
               l_Start_Date_Match := 'Y';
            End If;

            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '169 Start Date Active Match  => ' || l_Start_Date_Match  );
            -- Check If Effectivity Dates Match
            If   ( NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_START_DATE) = NVL(P_LN_END_DATE_ACTIVE , C_END_DATE ))  Then
               l_End_Date_Match := 'Y';
            End If;

            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '170 End Date Active Match  => ' || l_End_Date_Match  );
            -- Check if Dates Overlap
            IF  NVL(P_LN_START_DATE_ACTIVE, C_START_DATE)
                   Between  NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) And NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_END_DATE)  Then
                l_Dates_Overlap_Flag := 'Y';
            End If;

            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Statement,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '171 Dates Overlap Flag Value  => ' || l_Dates_Overlap_Flag  );

              -- Check if Dates Overlap
            IF  NVL(P_LN_END_DATE_ACTIVE, C_END_DATE)
                   Between  NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) And NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_END_DATE)  Then
                l_Dates_Overlap_Flag := 'Y';
            End If;

            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Statement,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '172 Dates Overlap Flag Value  => ' || l_Dates_Overlap_Flag  );
           -- Satish U: 05-DEC-2012 :  Old Logic for checkig overlap  20130104-02019
           --********************************************05-DEC-2012 **************************************
           -- -- Check if Price Breaks Overlap
           --  IF  (P_PA_PRICING_ATTR_VALUE_FROM IS NOT NULL) AND ( P_PA_PRICING_ATTR_VALUE_FROM
           --        Between  NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM, 1) And NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO, 999999))  Then
           --     l_PriceBreak_Overlap_Flag := 'Y';
           -- End If;

           -- ************************
            -- Added Following Logic to address Price Break Qty Overlap Issue : 20130104-02019

            Begin
                CASE
                   WHEN TO_NUMBER(NVL(P_PA_PRICING_ATTR_VALUE_FROM,1))  Between To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM ,1))
                      And To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999)) THEN
                      l_PriceBreak_Overlap_Flag := 'Y';
                      l_Msg_Data := '173.1 Price Break values Overlap with another line ';
                      Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                      --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                   WHEN To_Number(NVL(P_PA_PRICING_ATTR_VALUE_TO,99999))   Between To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM,1))
                      And To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999))  THEN
                      l_PriceBreak_Overlap_Flag := 'Y';
                      l_Msg_Data :=  '173.2 Price Break values overlap with another line =>' ;
                      Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                      --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                   ELSE
                     l_PriceBreak_Overlap_Flag := 'N';
                End CASE;
            Exception
                -- Satish U :  01/09/2023
               When Invalid_Number Then
                  CASE
                     WHEN NVL(P_PA_PRICING_ATTR_VALUE_FROM,1)  Between NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM ,1)
                        And NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999) THEN
                        l_PriceBreak_Overlap_Flag := 'Y';
                        l_Msg_Data := '173.4 Price Break values Overlap with another line =>' ;
                        Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                        --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                     WHEN NVL(P_PA_PRICING_ATTR_VALUE_TO,99999)   Between NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM,1)
                        And NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999)  THEN
                        l_PriceBreak_Overlap_Flag := 'Y';
                        l_Msg_Data :=  '173.5 Price Break values Overlap with another line =>' ;
                        Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                        --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                     ELSE
                        l_PriceBreak_Overlap_Flag := 'N';
                  End CASE;

               When Others Then
                  l_Msg_Data := Substr(SQLERRM, 1,200) ;
                  RAISE  XXWC_EXCEPTION ;
            End ;


            -- Check if List_Line_ID matches Too
            If NVL(P_LN_LIST_LINE_ID, -1) = NVL(Get_Mod_Rec.LN_LIST_LINE_ID, -2) Then
               l_LINE_ID_Match := 'Y' ;
            END IF ;

            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '175 Line Match Flag  => ' || l_LINE_ID_Match  );

             -- Check if From Qty Matches
            If NVL(P_PA_PRICING_ATTR_VALUE_FROM, -1) = NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM, -1) Then
               l_From_Qty_Match := 'Y' ;
            END IF ;

            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '176 From Qty Match Flag  => ' || l_From_Qty_Match  );

            -- Check if To Qty Matches
            If NVL(P_PA_PRICING_ATTR_VALUE_TO, -1) = NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO, -1) Then
               l_To_Qty_Match := 'Y' ;
            END IF ;

            XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '177 To Qty Match Flag  => ' || l_To_Qty_Match  );

            ----===================================================================---------------------------------------
            -- Validate if List_Line ID Matches.
            If l_LINE_ID_Match = 'Y' Then
               --AND l_PriceBreak_Overlap_Flag = 'N' AND l_Dates_Overlap_Flag = 'N' AND l_End_Date_Match = 'Y'
               --AND l_Start_Date_Match = 'Y' AND l_UOM_Match = 'Y' Then
               -- All the values match so Delete the Row from Interface Table.
               -- New Lines gets prcocessed and old line gets deleted.

               DELETE FROM XXWC_QP_MODIFIER_INT
               WHERE ROWID  = Get_Mod_Rec.ROWID ;

               XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '178 Deleted rows from Modifier Interface Table  => '   );

            ELSIF l_LINE_ID_Match = 'N'  AND l_UOM_Match = 'Y' Then
               -- Check if Other attributes Match
               --AND l_PriceBreak_Overlap_Flag = 'N' AND l_Dates_Overlap_Flag = 'N' AND l_End_Date_Match = 'Y'
               --AND l_Start_Date_Match = 'Y' AND l_UOM_Match = 'Y' Then
               -- Satish U: 03-JAN-2013 : 20130104-02019
               -- Defined following Error messages.
               -- 15-FEB-2013 : Satish U:  Compare Dates Overlap when Price Break Overlap Flag is Yes Too
               -- TMS# 20130215-01073
               If l_Dates_Overlap_Flag = 'Y' AND l_PriceBreak_Overlap_Flag = 'Y' AND ( l_Start_Date_Match = 'N' OR l_End_Date_Match = 'N') Then
                  XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '178: Duplicate Line can not insert into staging table. ');
                  FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_EFFECT_DATES_OVERLAP');
                  l_KNOWN_ERROR  := 'Y' ;
                  --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
                  Raise XXWC_EXCEPTION;
               Elsif l_PriceBreak_Overlap_Flag = 'Y' AND ( l_From_Qty_Match = 'N' OR l_To_Qty_Match = 'N') Then
                  XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '179: Duplicate Line can not insert into staging table. ');
                  FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_PRICEBREAK_QTY_OVERLAP');
                  l_KNOWN_ERROR  := 'Y' ;
                  --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
                  Raise XXWC_EXCEPTION;
               End If;
               -- Ever thing matches but The List Line ID
               IF P_LN_LIST_LINE_ID IS NULL AND Get_Mod_Rec.LN_LIST_LINE_ID Is Not Null
                  AND  l_Start_Date_Match = 'Y' AND l_End_Date_Match = 'Y'
                  AND  l_From_Qty_Match   = 'Y' AND l_To_Qty_Match = 'Y'
                  THEN
                     XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '180: Duplicate Line can not insert into staging table. ');
                     FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_DUPLICATE_LINE');
                     l_KNOWN_ERROR  := 'Y' ;
                     --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
                     Raise XXWC_EXCEPTION;
               -- Satish U: 20130104-02019  : Modified Following Logic
               Elsif P_LN_LIST_LINE_ID IS NULL AND Get_Mod_Rec.LN_LIST_LINE_ID Is Null
                   AND  l_Start_Date_Match = 'Y' AND l_End_Date_Match = 'Y'
                   AND  l_From_Qty_Match   = 'Y' AND l_To_Qty_Match = 'Y'
                   THEN
                   -- All the values match so Delete the Row from Interface Table.
                   -- Current row
                   DELETE FROM XXWC_QP_MODIFIER_INT
                   WHERE ROWID  = Get_Mod_Rec.ROWID ;
                   XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '180.1 Row deleted from XXWC_QP_MODIFIER_INT for Line ID => '  || P_LN_LIST_LINE_ID );

               End If;
            End If;
            --

         End Loop ;

      End If; -- --  Duplicate Lines Check 100
      --**********************************-- SatishU : 19-MAR-2013 *****************
      XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '180 PA Product Attribute  => '  || l_PA_PROUDCT_ATTRIBUTE );

      XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '181 PA Product Attribute Value => '  || L_PA_PRODUCT_ATTR_VALUE );

      -- Validate Current Modifier Line Data with existing Modifier line details in Base Table
      -- Added Parameter P_LN_LIST_LINE_ID : Satish U : 04-DEC-2012
      --****************satish U*********19-MAR-2013 *************************************************************
      For  Get_Mod_Rec In Get_Modifier2_Cur (
                                P_MODIFIER_LIST_NUMBER          => P_MODIFIER_LIST_NUMBER ,
                                P_PA_PRODUCT_ATTRIBUTE_CONTEXT  => P_PA_PRODUCT_ATTRIBUTE_CONTEXT,
                                P_PA_PRODUCT_ATTRIBUTE          => P_PA_PRODUCT_ATTRIBUTE,
                                P_PA_PRODUCT_ATTR_VALUE_ITEM    => P_PA_PRODUCT_ATTR_VALUE_ITEM ,
                                P_PA_PRODUCT_ATTR_VALUE_CAT     => P_PA_PRODUCT_ATTR_VALUE_CAT,
                                P_LN_LIST_LINE_ID               => P_LN_LIST_LINE_ID ) Loop
        l_UOM_Match               := 'N';
        l_Start_Date_Match        := 'N';
        l_End_Date_Match          := 'N';
        l_Dates_Overlap_Flag      := 'N';
        l_PriceBreak_Overlap_Flag := 'N';
        l_LINE_ID_Match           := 'N' ;
        l_From_Qty_Match           := 'N';
        l_To_Qty_Match             := 'N';

       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '182.0 List Line ID => '  || Get_Mod_Rec.LN_List_Line_ID );

        -- Check if UOM Matches
        If NVL(Get_Mod_Rec.PA_PRODUCT_UOM_CODE,'NULL')  = NVL(P_PA_PRODUCT_UOM_CODE,'NULL') Then
          l_UOM_Match := 'Y';
        ENd If;

        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '182.1 UOM Match Flag => '  || l_UOM_Match );

        -- Check If Effectivity Dates Match
        If   ( NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) = NVL(P_LN_START_DATE_ACTIVE , C_END_DATE ))  Then
           l_Start_Date_Match := 'Y';
        End If;

        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '183 Start Date Match Flag => '  || l_Start_Date_Match );
        -- Check If Effectivity Dates Match
        If   ( NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_START_DATE) = NVL(P_LN_END_DATE_ACTIVE , C_END_DATE ))  Then
           l_End_Date_Match := 'Y';
        End If;

        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '184 End Date Match Flag => '  || l_End_Date_Match );

        -- Check if Dates Overlap
        IF  NVL(P_LN_START_DATE_ACTIVE, C_START_DATE)
               Between  NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) And NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_END_DATE)  Then
            l_Dates_Overlap_Flag := 'Y';
        End If;

        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '185 Dates Overlap Flag  => '  || l_Dates_Overlap_Flag );


          -- Check if Dates Overlap
        IF  NVL(P_LN_END_DATE_ACTIVE, C_END_DATE)
               Between  NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) And NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_END_DATE)  Then
            l_Dates_Overlap_Flag := 'Y';
        End If;
        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                     P_DEBUG_MSG     => '186 Dates Overlap Flag  => '  || l_Dates_Overlap_Flag );
        -- Satish U : 03-JAN -2013 : 20130104-02019
        -- Modified Logic to compare Record values with Modifer List values in Oracle Modifier Tables.
        Begin
            CASE
               WHEN TO_NUMBER(NVL(P_PA_PRICING_ATTR_VALUE_FROM,1))  Between To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM ,1))
                  And To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999)) THEN
                  -- Check if User is updating records
                  For REc_Qty_Break in Get_Qty_Breaks_Cur (Get_Mod_Rec.LN_List_Line_ID ) Loop
                      IF TO_NUMBER(NVL(P_PA_PRICING_ATTR_VALUE_FROM,1))  Between To_Number(NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_FROM ,1))
                          And To_Number(NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_TO,99999)) THEN
                          l_PriceBreak_Overlap_Flag := 'Y';
                          l_Msg_Data := '187.1 Price Break values Overlap =>' ;
                          Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                      End If;
                  End Loop ;


                  --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
               WHEN To_Number(NVL(P_PA_PRICING_ATTR_VALUE_TO,99999))   Between To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM,1))
                  And To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999))  THEN
                  For REc_Qty_Break in Get_Qty_Breaks_Cur (Get_Mod_Rec.LN_List_Line_ID ) Loop
                      IF TO_NUMBER(NVL(P_PA_PRICING_ATTR_VALUE_TO,99999))  Between To_Number(NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_FROM ,1))
                          And To_Number(NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_TO,99999)) THEN
                          l_PriceBreak_Overlap_Flag := 'Y';
                          l_Msg_Data := '187.2 Price Break values Overlap =>' ;
                          Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                      End If;
                  End Loop ;

                  --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
               ELSE
                 l_PriceBreak_Overlap_Flag := 'N';
            End CASE;
        Exception
           When Others Then
              CASE
                 WHEN NVL(P_PA_PRICING_ATTR_VALUE_FROM,1)  Between NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM ,1)
                    And NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999) THEN
                    -- Check if User is updating records
                      For REc_Qty_Break in Get_Qty_Breaks_Cur (Get_Mod_Rec.LN_List_Line_ID ) Loop
                          IF NVL(P_PA_PRICING_ATTR_VALUE_FROM,1)  Between NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_FROM ,1)
                              And NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_TO,99999) THEN
                              l_PriceBreak_Overlap_Flag := 'Y';
                              l_Msg_Data := '187.3 Price Break values Overlap =>' ;
                              Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                          End If;
                      End Loop ;
                    --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                 WHEN NVL(P_PA_PRICING_ATTR_VALUE_TO,99999)   Between NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM,1)
                    And NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999)  THEN
                    For REc_Qty_Break in Get_Qty_Breaks_Cur (Get_Mod_Rec.LN_List_Line_ID ) Loop
                      IF NVL(P_PA_PRICING_ATTR_VALUE_TO,99999)  Between NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_FROM ,1)
                          And NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_TO,99999) THEN
                          l_PriceBreak_Overlap_Flag := 'Y';
                          l_Msg_Data := '187.4 Price Break values Overlap =>' ;
                          Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                      End If;
                  End Loop ;
                    --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                 ELSE
                    l_PriceBreak_Overlap_Flag := 'N';
            End CASE;
        End ;


        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Statement,
                                      p_mod_name      =>  l_Mod_Name,
                                      P_DEBUG_MSG     => '188.09 Price Break Overlap Flag Value  => ' || l_PriceBreak_Overlap_Flag  );


        -- Validate if List_Line ID Matches.
        If l_LINE_ID_Match = 'Y' Then
           -- Do nothing as this line will be udpated
           NULL;
        ELSIF l_LINE_ID_Match = 'N'  AND l_UOM_Match = 'Y' Then
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      =>  l_Mod_Name,
                                       P_DEBUG_MSG     => '191 UOM match  => '  );
           -- Check if Other attributes Match
           --AND l_PriceBreak_Overlap_Flag = 'N' AND l_Dates_Overlap_Flag = 'N' AND l_End_Date_Match = 'Y'
           --AND l_Start_Date_Match = 'Y' AND l_UOM_Match = 'Y' Then
           -- 14-FEB-2014  Quantities SHould Match When Dates Do not Match. TMS# 20130215-01073
           If l_Dates_Overlap_Flag = 'Y' AND ( l_Start_Date_Match = 'N' OR l_End_Date_Match = 'N')
                AND l_PriceBreak_Overlap_Flag = 'Y'  Then
              XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '192: Effective Dates Overlap, can not insert into staging table. ');
              FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_EFFECT_DATES_OVERLAP');
              l_KNOWN_ERROR  := 'Y' ;

              --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
              Raise XXWC_EXCEPTION;
           Elsif l_PriceBreak_Overlap_Flag = 'Y' AND ( l_From_Qty_Match = 'N' OR l_To_Qty_Match = 'N') Then
              XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '194: Duplicate Line can not insert into staging table. ');
              FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_PRICEBREAK_QTY_OVERLAP');
              l_KNOWN_ERROR  := 'Y' ;

              --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
              Raise XXWC_EXCEPTION;
           End If;
           -- Ever thing matches but The List Line ID
           ---IF P_LN_LIST_LINE_ID IS NULL AND Get_Mod_Rec.LN_LIST_LINE_ID Is Not Null
           IF  l_Start_Date_Match = 'Y' AND l_End_Date_Match = 'Y'
              AND  l_From_Qty_Match   = 'Y' AND l_To_Qty_Match = 'Y'
              THEN
                 XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '196: Modifier line with these details exist, Can not insert into staging table. ');
                 FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_DUPLICATE_LINE_BASE');
                 l_KNOWN_ERROR  := 'Y' ;
                 --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
                 Raise XXWC_EXCEPTION;
           End If;
        End If;
        --

     End Loop ;

     ********************************Satish U: 20130322-01259 End of Comments *********************/

    --***************satish U*********19-MAR-2013 *************************************************************
     XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '150: p_PA_PRODUCT_ATTRIBUTE_CONTEXT =' || p_PA_PRODUCT_ATTRIBUTE_CONTEXT);
     XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '151: p_PA_PROUDCT_ATTRIBUTE =' || l_PA_PROUDCT_ATTRIBUTE);
     XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '152: l_PA_PRODUCT_ATTR_VALUE =' || l_PA_PRODUCT_ATTR_VALUE);


      /*************Satish U : Commented this validation as this not required any more
      -- Satish U: 05-MAR-2012 : Check if  Override Flag is Required
      If P_LN_OVERRIDE_FLAG IS NULL Then
         FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_INVALID_ENTITY');
         FND_MESSAGE.SET_TOKEN('ENTITY_NAME','Override Flag');
         --l_MSG_Data :=  fnd_message.get();
         l_KNOWN_ERROR  := 'Y' ;
         --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
         Raise XXWC_EXCEPTION;
      End If;
      ****************************/
      -- TMS# 20130322-01259 : FOr exisitng Modifier List Lines deriving vlaue for columns that are removed from spreadsheet
      -- Assign Constant values for new Modifier List Lines
      -- Check if Modifier List Line is new
      If P_LN_LIST_LINE_ID Is NOT NULL Then
          Select
             LIST_LINE_TYPE_CODE,
             MODIFIER_LEVEL_CODE,
             LIST_LINE_NO,
             AUTOMATIC_FLAG,
             OVERRIDE_FLAG,
             PRICING_PHASE_ID,
             PRODUCT_PRECEDENCE,
             PRICING_GROUP_SEQUENCE,
             INCLUDE_ON_RETURNS_FLAG
          Into
             l_LN_LIST_LINE_TYPE_CODE,
             l_LN_MODIFIER_LEVEL_CODE,
             l_LN_LIST_LINE_NO,
             l_LN_AUTOMATIC_FLAG,
             l_LN_OVERRIDE_FLAG,
             l_LN_PRICING_PHASE_ID,
             l_LN_PRODUCT_PRECEDENCE,
             l_LN_PRICING_GROUP_SEQUENCE,
             l_LN_INCLUDE_ON_RETURNS_FLAG
          From apps.QP_List_Lines
          Where List_Line_ID = P_LN_List_Line_ID ;

      Else
          l_LN_LIST_LINE_TYPE_CODE := 'DIS';
          l_LN_MODIFIER_LEVEL_CODE := 'LINE';
          l_LN_LIST_LINE_NO  := NULL;
          l_LN_AUTOMATIC_FLAG := 'Y';
          l_LN_OVERRIDE_FLAG := 'N';
          l_LN_PRICING_PHASE_ID := 2;
          l_LN_PRODUCT_PRECEDENCE := 220;
          l_LN_PRICING_GROUP_SEQUENCE := NULL;
          l_LN_INCLUDE_ON_RETURNS_FLAG := 'Y';
      End IF;

      -- CHeck Expiration Date on Lines
      -- Satish U: 05-APR-2013  Automatic Flag logic on Modifier List Lines
      --TMS# 20130322-01259
      If P_LN_END_DATE_ACTIVE Is Not Null
          AND P_LN_END_DATE_ACTIVE >= SYSDATE
          AND P_MODIFIER_LIST_NAME <> C_NON_AUTOMATIC_MODIFIER THen
          l_LN_AUTOMATIC_FLAG := 'Y';
      Else
          l_LN_AUTOMATIC_FLAG := 'N';
      End If;

     --TMS# 20130322-01259  : Some of the columns are removed from Insert Statement as they are not used any more

      -- Insert Record Into Modifer Interface Table
      INSERT INTO apps.XXWC_QP_MODIFIER_INT (
         BATCH_ID                      ,
         LIST_HEADER_ID                ,
         MODIFIER_LIST_NAME            ,
         MODIFIER_LIST_NUMBER          ,
         CURRENCY_CODE                 ,
         LIST_TYPE_CODE                ,
         START_DATE_ACTIVE             ,
         END_DATE_ACTIVE               ,
         GLOBAL_FLAG                   ,
         AUTOMATIC_FLAG                ,
         --LN_ORIG_SYS_LINE_REF          ,
         LN_LIST_LINE_ID               ,
         LN_LIST_LINE_NO               ,
         --LN_MODIFIER_LEVEL             ,
         LN_MODIFIER_LEVEL_CODE        ,
         --LN_LIST_LINE_TYPE             ,
         LN_LIST_LINE_TYPE_CODE        ,
         LN_START_DATE_ACTIVE          ,
         LN_END_DATE_ACTIVE            ,
         LN_AUTOMATIC_FLAG             ,
         LN_OVERRIDE_FLAG              ,
         --LN_PRICING_PHASE              ,
         LN_PRICING_PHASE_ID           ,
         LN_PRODUCT_PRECEDENCE         ,
         LN_PRICE_BREAK_TYPE_CODE      ,
         PA_PRODUCT_UOM_CODE           ,
         LN_ARITHMETIC_OPERATOR_TYPE   ,
         LN_ARITHMETIC_OPERATOR        ,
         LN_OPERAND                    ,
         --LN_ACCRUAL_FLAG               ,
         LN_INCOMPATIBILITY_GRP_CODE   ,
         LN_PRICING_GROUP_SEQUENCE     ,
         LN_INCLUDE_ON_RETURNS_FLAG    ,
         LN_PRICE_BY_FORMULA_ID        ,
         PA_PRICING_ATTRIBUTE_ID       ,
         --PA_ORIG_SYS_PRICING_ATTR_REF  ,
         PA_PRODUCT_ATTRIBUTE_CONTEXT  ,
         PA_PRODUCT_ATTRIBUTE          ,
         PA_PRODUCT_ATTR_VALUE       ,
         PA_PRICING_ATTRIBUTE          ,
         PA_COMPARISON_OPERATOR_CODE   ,
         PA_PRICING_ATTRIBUTE_CONTEXT  ,
         PA_PRICING_ATTR_VALUE_FROM    ,
         PA_PRICING_ATTR_VALUE_TO      ,
         --PA_EXCLUDER_FLAG              ,
         --PA_ATTRIBUTE_GROUPING_NO      ,
         STATUS                        ,
         CREATED_BY                    ,
         CREATION_DATE                 ,
         LAST_UPDATED_BY               ,
         LAST_UPDATE_DATE              ,
         LAST_UPDATE_LOGIN )
      VALUES ( G_BATCH_ID  ,
         P_LIST_HEADER_ID               ,
         P_MODIFIER_LIST_NAME           ,
         P_MODIFIER_LIST_NUMBER         ,
         P_CURRENCY_CODE                ,
         P_LIST_TYPE_CODE               ,
         P_START_DATE_ACTIVE            ,
         P_END_DATE_ACTIVE              ,
         P_GLOBAL_FLAG                  ,
         P_AUTOMATIC_FLAG               ,
         --P_LN_ORIG_SYS_LINE_REF         ,
         P_LN_LIST_LINE_ID              ,
         l_LN_LIST_LINE_NO              ,
         --P_LN_MODIFIER_LEVEL            ,
         l_LN_MODIFIER_LEVEL_CODE       ,
         --P_LN_LIST_LINE_TYPE            ,
         l_LN_LIST_LINE_TYPE_CODE       ,
         P_LN_START_DATE_ACTIVE         ,
         P_LN_END_DATE_ACTIVE           ,
         l_LN_AUTOMATIC_FLAG            ,
         l_LN_OVERRIDE_FLAG             ,
         --P_LN_PRICING_PHASE             ,
         l_LN_PRICING_PHASE_ID          ,
         l_LN_PRODUCT_PRECEDENCE        ,
         P_LN_PRICE_BREAK_TYPE_CODE     ,
         P_PA_PRODUCT_UOM_CODE          ,
         P_LN_ARITHMETIC_OPERATOR_TYPE  ,
         l_LN_ARITHMETIC_OPERATOR       ,
         P_LN_OPERAND                   ,
         --P_LN_ACCRUAL_FLAG              ,
         P_LN_INCOMPATIBILITY_GRP_CODE  ,
         l_LN_PRICING_GROUP_SEQUENCE    ,
         l_LN_INCLUDE_ON_RETURNS_FLAG   ,
         L_LN_PRICE_BY_FORMULA_ID       ,
         P_PA_PRICING_ATTRIBUTE_ID       ,
         --P_PA_ORIG_SYS_PRICING_ATTR_REF ,
         P_PA_PRODUCT_ATTRIBUTE_CONTEXT ,
         l_PA_PROUDCT_ATTRIBUTE         ,
         L_PA_PRODUCT_ATTR_VALUE        ,
         P_PA_PRICING_ATTRIBUTE         ,
         P_PA_COMPARISON_OPERATOR_CODE  ,
         L_PA_PRICING_ATTRIBUTE_CONTEXT ,
         P_PA_PRICING_ATTR_VALUE_FROM   ,
         P_PA_PRICING_ATTR_VALUE_TO     ,
         --P_PA_EXCLUDER_FLAG              ,
         --P_PA_ATTRIBUTE_GROUPING_NO      ,
         C_STATUS_VALIDATE              ,
         fnd_global.user_id             ,
         Sysdate                        ,
         fnd_global.user_id             ,
         Sysdate                        ,
         fnd_global.login_id );

         Select Count(*)
         Into l_Line_Count
         From apps.XXWC_QP_MODIFIER_INT
         Where Batch_ID = G_BATCH_ID ;

        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '160 Successfully Inserted record in Modifier List Interface table  =>');
        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '161 Number of rows in XXWC_QP_MODIFIER_INT   =>' || l_Line_Count );
   Exception
      When XXWC_EXCEPTION Then
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '200 XXWC Exception  =>' || l_Msg_Data);
         FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_UPLOAD_ERROR');
         FND_MESSAGE.SET_TOKEN( 'ERROR_MSG',Substr(l_msg_data, 1,200));
         Raise;
      When Others THen
          If l_KNOWN_ERROR  = 'N' Then
              l_Msg_Data := SUBSTR(SQLERRM,1,200);
              FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_UPLOAD_ERROR');
              FND_MESSAGE.SET_TOKEN( 'ERROR_MSG',Substr(l_msg_data, 1,200));
              --l_MSG_Data :=  fnd_message.get();
          Else
             l_Msg_Data :=  SUBSTR(SQLERRM,200)  ;
          End If;
          -- 04-FEB-2012 :  To display Error Message in Spreadsheet

          XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => g_level_Error,
                                       p_mod_name      => l_Mod_Name,
                                       P_DEBUG_MSG     => '201 When Others Error Exception  =>' || l_Msg_Data);

          --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
          Raise;
   End Upload ;

  -- Satish U # 20130322-01259 :  Performance Tuning.
  --  Fine tune the logic that validates Date Overlap/Price Break Overlap

 PROCEDURE  IMPORT ( Errbuf            OUT NOCOPY VARCHAR2,
                     Retcode           OUT NOCOPY NUMBER,
                     P_Batch_ID        IN   Number ,
                     p_LIST_HEADER_ID  IN   Number  ) IS

      --- Define Local Variables here
      l_qualifier_rec            QP_QUALIFIER_RULES_PUB.QUALIFIERS_REC_TYPE     := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
      l_modifier_line_Rec        QP_MODIFIERS_PUB.MODIFIERS_REC_TYPE            := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_REC;
      l_pricing_att_rec          QP_MODIFIERS_PUB.PRICING_ATTR_REC_TYPE         := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_REC;
      l_error_message_list       ERROR_HANDLER.ERROR_TBL_TYPE;
      l_MODIFIER_LIST_rec        QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec    QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl            QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl        QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl           QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl       QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl         QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl     QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      lx_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
      lx_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      lx_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
      lx_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      lx_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      lx_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      lx_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      lx_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      l_qll_rec                  qp_list_lines%ROWTYPE;
      l_pra_rec                  qp_pricing_attributes%ROWTYPE;
      l_control_rec              QP_GLOBALS.Control_Rec_Type;
      l_mod_name                 VARCHAR2 (100)  := 'XXWC_QP_MODIFIER_WEB_ADI_PKG.IMPORT :' ;
      l_return_status            VARCHAR2(1);

      l_Msg_Data                 Varchar2(2000);
      l_Msg_Count                Number ;
      L_List_Header_ID           Number;
      l_List_Line_Id             Number ;
      l_Pricing_Attribute_ID     Number ;
      l_Qualifier_ID             Number ;
      i                          Number ;
      l_Line_COunt               Number ;

      C_STATUS_VALIDATE          CONSTANT   VARCHAR2(1) := 'V';
      C_STATUS_ERROR             CONSTANT   VARCHAR2(1) := 'E';
      C_STATUS_PROCESSED         CONSTANT   VARCHAR2(1) := 'P' ;


      -- Cursor to get the Modifier List
      Cursor  Get_Mod_List_Cur IS
         Select Distinct LIST_HEADER_ID , MODIFIER_LIST_NAME , MODIFIER_LIST_NUMBER , HDR_OPERATION , CURRENCY_CODE,
            LIST_TYPE_CODE , SOURCE_SYSTEM_CODE  , PTE_CODE   , START_DATE_ACTIVE, END_DATE_ACTIVE, GLOBAL_FLAG,
            AUTOMATIC_FLAG, COMMENTS
         From apps.XXWC_QP_MODIFIER_INT
         Where ( P_BATCH_ID IS NULL Or Batch_ID       = P_BATCH_ID )
         AND   (P_LIST_HEADER_ID IS NULL OR LIST_HEADER_ID = P_LIST_HEADER_ID )
         AND   Status         in ( C_STATUS_VALIDATE , C_STATUS_ERROR)
         --AND PA_PRODUCT_ATTRIBUTE = 'PRICING_ATTRIBUTE2'
         AND PA_PRODUCT_ATTR_VALUE IS NOT NULL ;


      -- Cursor to get the Modifier Lines
      Cursor  Get_Mod_Line_Cur (P_List_Header_ID Number ) IS
         Select Distinct
             LN_ORIG_SYS_LINE_REF          ,
             LN_LIST_LINE_ID               ,
             LN_LIST_LINE_NO               ,
             LN_MODIFIER_LEVEL             ,
             LN_MODIFIER_LEVEL_CODE        ,
             LN_LIST_LINE_TYPE             ,
             LN_LIST_LINE_TYPE_CODE        ,
             LN_START_DATE_ACTIVE          ,
             LN_END_DATE_ACTIVE            ,
             LN_AUTOMATIC_FLAG             ,
             LN_OVERRIDE_FLAG              ,
             LN_PRICING_PHASE              ,
             LN_PRICING_PHASE_ID           ,
             LN_PRODUCT_PRECEDENCE         ,
             LN_PRICE_BREAK_TYPE_CODE      ,
             --LN_PRODUCT_UOM_CODE           ,
             LN_ARITHMETIC_OPERATOR_TYPE   ,
             LN_ARITHMETIC_OPERATOR        ,
             LN_OPERAND                    ,
             LN_ACCRUAL_FLAG      ,
             LN_OPERATION     ,
             LN_INCOMPATIBILITY_GRP_CODE,
             LN_PRICING_GROUP_SEQUENCE ,
             LN_INCLUDE_ON_RETURNS_FLAG ,
             LN_PRICE_BY_FORMULA_ID
         From apps.XXWC_QP_MODIFIER_INT
         Where List_Header_ID  = P_List_Header_ID
         --AND PA_PRODUCT_ATTRIBUTE = 'PRICING_ATTRIBUTE2'
         And   NVL(Status,'NULL') <> C_STATUS_PROCESSED
         AND PA_PRODUCT_ATTR_VALUE IS NOT NULL
         Order By LN_LIST_LINE_ID ;

       -- Cursor to get Modofier Qualifiers
      Cursor  Get_HDr_Qual_Cur (P_List_Header_ID Number ) IS
         Select Distinct
             --LN_LIST_LINE_ID               ,
             QL_QUALIFIER_ID               ,
             QL_ORIG_SYS_QUALIFIER_REF     ,
             QL_QUALIFIER_GROUPING_NO      ,
             QL_QUALIFIER_CONTEXT          ,
             QL_QUALIFIER_ATTRIBUTE        ,  -- New column
             QL_QUALIFIER_ATTR_VALUE       ,
             QL_QUALIFIER_ATTR_VALUE_TO    ,
             QL_COMPARISON_OPERATOR_CODE   ,
             QL_QUALIFIER_PRECEDENCE    ,
             QL_OPERATION
         From apps.XXWC_QP_MODIFIER_INT
         Where List_Header_ID  = P_List_Header_ID
         --AND PA_PRODUCT_ATTRIBUTE = 'PRICING_ATTRIBUTE2'
         And   NVL(Status,'NULL') <> C_STATUS_PROCESSED
         AND PA_PRODUCT_ATTR_VALUE IS NOT NULL ;


         --AND   LN_List_Line_ID  = -1 ;


           -- Cursor to get Modofier Qualifiers
      Cursor  Get_Pricing_Attr_Cur (P_List_Header_ID Number,
                                    p_List_Line_ID   Number ) IS
         Select Distinct
            PA_PRICING_ATTRIBUTE_ID       ,
            PA_ORIG_SYS_PRICING_ATTR_REF ,
            PA_PRODUCT_ATTRIBUTE_CONTEXT ,
            PA_PRODUCT_ATTRIBUTE         ,
            PA_PRODUCT_ATTR_VALUE        ,
            PA_PRODUCT_UOM_CODE          ,
            PA_PRICING_ATTRIBUTE         ,
            PA_COMPARISON_OPERATOR_CODE  ,
            PA_PRICING_ATTRIBUTE_CONTEXT ,
            PA_PRICING_ATTR_VALUE_FROM   ,
            PA_PRICING_ATTR_VALUE_TO     ,
            PA_EXCLUDER_FLAG              ,
            PA_ATTRIBUTE_GROUPING_NO      ,
            PA_OPERATION
         From apps.XXWC_QP_MODIFIER_INT
         Where List_Header_ID  = P_List_Header_ID
         And   LN_List_Line_ID    = P_List_LIne_ID
         And   NVL(Status,'NULL') <> C_STATUS_PROCESSED ;


     Cursor  Get_New_Ln_Cur ( P_LIST_HEADER_ID   Number ) Is
        Select Distinct LIST_HEADER_ID , rowid
        --, LN_ORIG_SYS_LINE_REF
        From apps.XXWC_QP_MODIFIER_INT
        Where LN_LIST_LINE_ID is Null
        AND   LN_LIST_Line_NO is NULL
        And   NVL(Status,'NULL') <> C_STATUS_PROCESSED ;

    -- Satish U : 31-MAR-2013 G
    Cursor  Get_Upd_Ln_Cur ( P_LIST_HEADER_ID   Number ) Is
        Select Distinct LIST_HEADER_ID , rowid
        --, LN_ORIG_SYS_LINE_REF
        From apps.XXWC_QP_MODIFIER_INT
        Where LN_LIST_LINE_ID is NOT Null
        And   NVL(Status,'NULL') <> C_STATUS_PROCESSED ;


     Cursor  Get_New_PA_Cur ( P_LIST_HEADER_ID   Number ) Is
        Select Distinct LIST_HEADER_ID , LN_List_Line_ID
        --, rowid
        --PA_ORIG_SYS_PRICING_ATTR_REF
        From apps.XXWC_QP_MODIFIER_INT
        Where PA_PRICING_ATTRIBUTE_ID is Null
        And   NVL(Status,'NULL') <> C_STATUS_PROCESSED ;

      Cursor  Get_New_QL_Cur ( P_LIST_HEADER_ID   Number ) Is
        Select Distinct LIST_HEADER_ID , QL_QUALIFIER_ATTR_VALUE
        --, LN_List_Line_ID, rowid
        --QL_ORIG_SYS_QUALIFIER_REF
        From apps.XXWC_QP_MODIFIER_INT
        Where QL_QUALIFIER_ID is Null
        AND   QL_QUALIFIER_ATTRIBUTE is Not Null
        And   NVL(Status,'NULL') <> C_STATUS_PROCESSED ;

          l_err_callpoint                      VARCHAR2(175) := 'START';
          l_distro_list                        VARCHAR2(80) := 'OracleDevelopmentGroup@hdsupply.com';
          l_ln_Count  Number ;
          l_pa_Count  Number ;
          l_ql_Count  Number ;
          l_Row_Found_Flag    Varchar2(1) ;
          l_file_val  Varchar2(240);
          l_Rec_Count  Number;
          l_PA_Rec_Count Number ;

   Begin
      Fnd_File.Put_Line ( Fnd_File.LOG, '101 Begining of API =>' || l_mod_name);

      Fnd_File.Put_Line ( Fnd_File.LOG, '102 Batch Id  =>' || P_BATCH_ID);
      Fnd_File.Put_Line ( Fnd_File.LOG, '103 List Header Id   =>' || P_LIST_HEADER_ID );
      --x_Return_Status :=  G_RETURN_SUCCESS ;
      -- Update List Line Id FOr New Records
      fnd_profile.PUT('QP_PRICING_TRANSACTION_ENTITY', 'ORDFUL');
      fnd_profile.PUT('QP_SOURCE_SYSTEM_CODE', 'QP');
      oe_debug_pub.debug_on;
      oe_debug_pub.initialize;
      Fnd_File.Put_Line ( Fnd_File.LOG, '103.1 Set Debug Mode FILE   =>'  );
      l_file_val := OE_DEBUG_PUB.Set_Debug_Mode('FILE');
      oe_Debug_pub.setdebuglevel(5);
      Fnd_File.Put_Line ( Fnd_File.LOG, '104 Debug File   =>' || l_file_val );
     -- XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101 : Debug File Name ='   || l_file_val);

      oe_debug_pub.add('Debug Statement', 1);
      oe_debug_pub.add('Add other Debug Statement', 1);

      Fnd_File.Put_Line ( Fnd_File.LOG, '105 Use Id   =>' || fnd_global.user_id );
      Fnd_File.Put_Line ( Fnd_File.LOG, '106 Resp_ID    =>' || fnd_global.resp_id );
      Fnd_File.Put_Line ( Fnd_File.LOG, '107 Resp_Appl_ID   =>' || fnd_global.Resp_Appl_id );



      fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                  resp_id        => fnd_global.resp_id,
                                  resp_appl_id   => fnd_global.resp_appl_id);

      oe_debug_pub.add('108: Calling mo_global.get_current_org_id  API' );
      SELECT   mo_global.get_current_org_id ()
      INTO g_org_id
      FROM DUAL;

      --Dbms_Output.Put_Line('03 : After Getting Current Org ID');
      --XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, 'g_org_id =' || g_org_id);

      --mo_global.set_policy_context ('S', 162);
      oe_debug_pub.add('109: Setting Policy Context  for Org ID :' || g_org_Id );
      mo_global.set_policy_context ('S', g_org_id);
      Fnd_File.Put_Line ( Fnd_File.LOG, '110 Org_ID   =>' || g_org_Id );
      --Dbms_Output.Put_Line('01 : Begining  of the pr

      For New_Ln_Rec In Get_New_Ln_Cur ( P_LIST_HEADER_ID )  Loop

        SELECT QP_LIST_LINES_S.nextval
        INTO l_list_line_id
        FROM DUAL;
        Fnd_File.Put_Line ( Fnd_File.LOG, '111 Generating List Line ID for new lines   =>' || l_list_line_id );

        -- TMS# 20130322-01259   Assign Default Values for new Modifier List Lines
        -- These columns are removed from spreadsheet
        Update apps.XXWC_QP_MODIFIER_INT
           Set LN_List_LIne_ID  = l_List_Line_ID ,
               LN_List_Line_No  = To_Char(l_List_Line_ID) ,
               LN_Operation     = QP_GLOBALS.G_OPR_CREATE,
               LN_MODIFIER_LEVEL_CODE = 'LINE',
               LN_LIST_LINE_TYPE_CODE = 'DIS' ,
               LN_AUTOMATIC_FLAG      = 'Y',
               LN_PRICING_PHASE_ID    = 2,
               LN_PRICING_GROUP_SEQUENCE = 1,
               LN_OVERRIDE_FLAG        = 'N',
               LN_INCLUDE_ON_RETURNS_FLAG = 'Y',
             --  LN_QUALIFICATION_IND    = .
               LN_PRODUCT_PRECEDENCE   = 220
        Where List_Header_Id = New_Ln_Rec.List_Header_ID
        And   rowId = New_Ln_Rec.RowId;

       -- and   LN_ORIG_SYS_LINE_REF = New_Ln_Rec.LN_ORIG_SYS_LINE_REF ;

      End Loop ;


      -- Update Pricing Attribute ID  for new Pricing Attributes

      For New_PA_Rec In Get_New_PA_Cur(P_List_Header_ID ) Loop
         Select QP_PRICING_ATTRIBUTES_S.NextVal
         Into l_Pricing_Attribute_ID
         From Dual ;

         Fnd_File.Put_Line ( Fnd_File.LOG, '112 Generating Pricing Attribute ID for new lines   =>' || l_Pricing_Attribute_ID );

         Update apps.XXWC_QP_MODIFIER_INT
           Set PA_Pricing_Attribute_ID  = l_Pricing_Attribute_ID ,
               PA_Operation              = QP_GLOBALS.G_OPR_CREATE
         Where List_Header_Id = New_PA_Rec.List_Header_ID
         And   LN_List_Line_ID = New_PA_Rec.LN_List_Line_ID;


         --And   PA_ORIG_SYS_PRICING_ATTR_REF = New_PA_Rec.PA_ORIG_SYS_PRICING_ATTR_REF ;

      End Loop ;


      -- Update Qualifier Id for New Rows
      For New_QL_Rec In Get_New_QL_Cur(P_List_Header_ID ) Loop
         Select QP_QUALIFIERS_S.NextVal
         Into l_Qualifier_ID
         From Dual ;

         Fnd_File.Put_Line ( Fnd_File.LOG, '113 Generating QUalifier ID for new lines   =>' || l_Qualifier_ID );

         Update apps.XXWC_QP_MODIFIER_INT
           Set QL_Qualifier_ID  = l_Qualifier_ID ,
               QL_Operation     = QP_GLOBALS.G_OPR_CREATE
         Where List_Header_Id = New_QL_Rec.List_Header_ID
         And   QL_QUALIFIER_ATTR_VALUE = New_QL_REc.QL_QUALIFIER_ATTR_VALUE ;
         --And   RowID   = New_QL_Rec.RowId;
         --And   QL_ORIG_SYS_QUALIFIER_REF = New_QL_Rec.QL_ORIG_SYS_QUALIFIER_REF ;

      End Loop ;

      -- Commit the Transactions
      --Commit;

      For  Mod_List_Rec In Get_Mod_List_Cur Loop
         Begin
            -- INitialize  Records and Tables in the begining. 20-JAN-2012  Satish U
            Fnd_File.Put_Line ( Fnd_File.LOG, '114.001 Initializing Records  =>' );
            l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
            l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
            l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;
            l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
              /* Update  a Modifier header  */

             If Mod_List_Rec.List_Header_ID is NULL Then
                   -- Generate List_Header ID from Sequece

                   Select QP_LIST_HEADERS_B_S.NextVal
                   Into L_List_Header_ID
                   From Dual ;

                   l_MODIFIER_LIST_rec.List_Header_ID := l_List_Header_ID ;
                   l_MODIFIER_LIST_rec.operation      := QP_GLOBALS.G_OPR_CREATE ;


             Else
                -- CHeck if List Header Id exists in QL_LIST_HEADERS TABLE
                Select Count(*)
                INto l_Rec_Count
                From apps.qp_List_Headers
                Where List_Header_ID = Mod_List_Rec.List_Header_ID ;

                If l_Rec_Count > 0 Then
                   l_MODIFIER_LIST_rec.operation      := QP_GLOBALS.G_OPR_UPDATE ;

                Else
                     l_MODIFIER_LIST_rec.operation      := QP_GLOBALS.G_OPR_CREATE ;
                     l_MODIFIER_LIST_rec.SOURCE_SYSTEM_CODE := Mod_List_Rec.SOURCE_SYSTEM_CODE;
                     l_MODIFIER_LIST_rec.LIST_TYPE_CODE     := Mod_List_Rec.LIST_TYPE_CODE ;
                     l_MODIFIER_LIST_rec.PTE_CODE           := Mod_List_Rec.PTE_CODE ;
                     l_MODIFIER_LIST_rec.CURRENCY_CODE      := Mod_List_Rec.CURRENCY_CODE ;
                END If;
                l_MODIFIER_LIST_rec.List_Header_ID := Mod_List_Rec.List_Header_ID ;
             End If;

             Fnd_File.Put_Line ( Fnd_File.LOG, '114.002 Building Modifer List Header Record  =>' || Mod_List_Rec.List_Header_ID );
             l_MODIFIER_LIST_rec.Name           := Mod_List_Rec.MODIFIER_LIST_Number;
             l_MODIFIER_LIST_rec.Description    := Mod_List_Rec.MODIFIER_LIST_NAME ;
             -- Added New Columns to the Code 03-JAN-2011
             l_MODIFIER_LIST_rec.START_Date_Active := Mod_List_Rec.Start_Date_Active ;
             l_MODIFIER_LIST_rec.End_Date_Active   := Mod_List_Rec.End_Date_Active ;
             l_MODIFIER_LIST_rec.Comments          := Mod_List_Rec.Comments ;
             l_MODIFIER_LIST_rec.Automatic_Flag    := Mod_List_Rec.Automatic_Flag ;
             l_MODIFIER_LIST_rec.Global_Flag       := Mod_List_Rec.Global_Flag ;

             l_ql_Count := 0 ;
             IF l_MODIFIER_LIST_rec.operation      =  QP_GLOBALS.G_OPR_CREATE Then
                 For Qualifiers_Rec In Get_Hdr_Qual_Cur (P_List_Header_ID => Mod_List_Rec.List_Header_ID)  Loop

                     --Fnd_File.Put_Line ( Fnd_File.LOG, '114.02 Qualifier Record  =>' || Qualifiers_Rec.QL_QUALIFIER_ID );
                     l_ql_Count := l_ql_Count + 1;
                     Fnd_File.Put_Line ( Fnd_File.LOG, '114.02 Qualifier Record Coount =>' || l_ql_Count);

                     --Fnd_File.Put_Line ( Fnd_File.LOG, '114.1 Qualifier Record  =>' || Qualifiers_Rec.QL_QUALIFIER_ID );
                     l_qualifier_rec.LIST_HEADER_ID             :=  Mod_List_Rec.List_Header_ID;
                     l_qualifier_rec.LIST_LINE_ID               :=  -1 ; --Qualifiers_Rec.LN_LIST_LINE_ID  ;
                     l_qualifier_rec.QUALIFIER_ID               :=  Qualifiers_Rec.QL_QUALIFIER_ID ;
                     --l_QUALIFIERS_tbl(l_ql_Count).ORIG_SYS_QUALIFIER_REF     :=  Qualifiers_Rec.QL_ORIG_SYS_QUALIFIER_REF;
                     l_qualifier_rec.QUALIFIER_GROUPING_NO      :=  l_ql_Count ; --Qualifiers_Rec.QL_QUALIFIER_GROUPING_NO ;
                     l_qualifier_rec.QUALIFIER_CONTEXT          :=  Qualifiers_Rec.QL_QUALIFIER_CONTEXT ;
                     --Fnd_File.Put_Line ( Fnd_File.LOG, '114.2 Qualifier Record  =>' || Qualifiers_Rec.QL_QUALIFIER_ID );
                     l_qualifier_rec.QUALIFIER_ATTRIBUTE        :=  Qualifiers_Rec.QL_QUALIFIER_ATTRIBUTE ;
                     l_qualifier_rec.QUALIFIER_ATTR_VALUE       :=  Qualifiers_Rec.QL_QUALIFIER_ATTR_VALUE ;
                     l_qualifier_rec.QUALIFIER_ATTR_VALUE_TO    :=  Qualifiers_Rec.QL_QUALIFIER_ATTR_VALUE_TO ;
                     l_qualifier_rec.COMPARISON_OPERATOR_CODE   :=  Qualifiers_Rec.QL_COMPARISON_OPERATOR_CODE ;
                     Fnd_File.Put_Line ( Fnd_File.LOG, '114.300 Qualifier : Comparison Operator Codc ies   =>' || QUALIFIERS_Rec.QL_COMPARISON_OPERATOR_CODE );

                     --Fnd_File.Put_Line ( Fnd_File.LOG, '114.3 Qualifier Record  =>' || Qualifiers_Rec.QL_QUALIFIER_ID );
                     l_qualifier_rec.QUALIFIER_PRECEDENCE       :=  1; -- Qualifiers_Rec.QL_QUALIFIER_PRECEDENCE ;
                     l_qualifier_rec.Excluder_Flag              := 'N';

                     -- Check if Qulifier Record exists in Table
                     Select Count(*)
                     INto l_Rec_Count
                     From apps.qp_qualifiers
                     Where Qualifier_ID = Qualifiers_Rec.QL_Qualifier_ID ;

                     --If Qualifiers_Rec.QL_ORIG_SYS_QUALIFIER_REF  Is Not Null Then
                     --IF Qualifiers_Rec.QL_Qualifier_ID is NULL Then
                     IF l_Rec_Count = 0 THen
                        l_qualifier_rec.Operation   := QP_GLOBALS.G_OPR_CREATE;
                        Fnd_File.Put_Line ( Fnd_File.LOG, '114.301 Qualifier : Qualifier Record does not exist');
                        -- Check If List_Line Id is already generated
                     Else
                        l_qualifier_rec.Operation := QP_GLOBALS.G_OPR_UPDATE;

                     End If;

                     l_QUALIFIERS_tbl(l_ql_Count)  := l_qualifier_rec;

                     Fnd_File.Put_Line ( Fnd_File.LOG, '114.310 Qualifier : Comparison Operator Code   =>' || l_QUALIFIERS_tbl(l_ql_Count).COMPARISON_OPERATOR_CODE );
                  End Loop ;  -- Qualifiers
             End If;

             Fnd_File.Put_Line ( Fnd_File.LOG, '114.4 Number of Qualifier Record Records =>' || l_QL_Count );
             /* Create Or Update a Modifier line  */
             l_Ln_Count := 0 ;
             l_pa_Count := 0;
             For Mod_Line_Rec in Get_Mod_Line_Cur (Mod_List_Rec.List_Header_ID ) Loop
                l_Ln_Count :=  l_Ln_Count + 1;
                Fnd_File.Put_Line ( Fnd_File.LOG, '115.100 Modifer Line Record  =>' || Mod_Line_Rec.LN_LIST_LINE_ID );

                l_MODIFIERS_tbl(l_Ln_Count).LIST_HEADER_ID              := Mod_List_Rec.List_Header_ID;
                --l_MODIFIERS_tbl(l_Ln_Count).ORIG_SYS_LINE_REF           := Mod_Line_Rec.LN_ORIG_SYS_LINE_REF ;
                l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                := Mod_Line_Rec.LN_LIST_LINE_ID ;
                l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_NO                := NVL(Mod_Line_Rec.LN_LIST_LINE_NO, Mod_Line_Rec.LN_LIST_LINE_ID)  ;
                --l_MODIFIERS_tbl(l_Ln_Count).MODIFIER_LEVEL              := Mod_Line_Rec.LN_MODIFIER_LEVEL  ;
                --TMS# 20130322-01259 Satish U: 31-Mar-2013 Assign Values only if they are not NULL
                If Mod_Line_Rec.LN_MODIFIER_LEVEL_CODE IS NOT NULL Then
                   l_MODIFIERS_tbl(l_Ln_Count).MODIFIER_LEVEL_CODE         := Mod_Line_Rec.LN_MODIFIER_LEVEL_CODE ;
                End If;

                --l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_TYPE              := Mod_Line_Rec.LN_LIST_LINE_TYPE;
                -- TMS# 20130322-01259 Satish U: 31-Mar-2013 Assign Values only if they are not NULL
                If Mod_Line_Rec.LN_LIST_LINE_TYPE_CODE IS NOT NULL Then
                   l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_TYPE_CODE         := Mod_Line_Rec.LN_LIST_LINE_TYPE_CODE;

                End If;

                l_MODIFIERS_tbl(l_Ln_Count).START_DATE_ACTIVE           := Mod_Line_Rec.LN_START_DATE_ACTIVE;
                l_MODIFIERS_tbl(l_Ln_Count).END_DATE_ACTIVE             := Mod_Line_Rec.LN_END_DATE_ACTIVE;
                -- TMS# 20130322-01259 Satish U: 31-Mar-2013 Assign Values only if they are not NULL
                If Mod_Line_Rec.LN_AUTOMATIC_FLAG IS NOT NULL Then
                   l_MODIFIERS_tbl(l_Ln_Count).AUTOMATIC_FLAG              := Mod_Line_Rec.LN_AUTOMATIC_FLAG;
                End If;
                -- TMS# 20130322-01259 Satish U: 31-Mar-2013 Assign Values only if they are not NULL
                If Mod_Line_Rec.LN_OVERRIDE_FLAG  Is Not NULL Then
                   l_MODIFIERS_tbl(l_Ln_Count).OVERRIDE_FLAG               := Mod_Line_Rec.LN_OVERRIDE_FLAG;
                End If;

                --l_MODIFIERS_tbl(l_Ln_Count).PRICING_PHASE               := Mod_Line_Rec.LN_PRICING_PHASE;
                -- TMS# 20130322-01259 Satish U: 31-Mar-2013 Assign Values only if they are not NULL
                If Mod_Line_Rec.LN_PRICING_PHASE_ID Is Not Null Then
                   l_MODIFIERS_tbl(l_Ln_Count).PRICING_PHASE_ID            := Mod_Line_Rec.LN_PRICING_PHASE_ID;
                End If;
                -- TMS# 20130322-01259 Satish U: 31-Mar-2013 Assign Values only if they are not NULL
                If  Mod_Line_Rec.LN_PRODUCT_PRECEDENCE Is Not Null Then
                   l_MODIFIERS_tbl(l_Ln_Count).PRODUCT_PRECEDENCE          := Mod_Line_Rec.LN_PRODUCT_PRECEDENCE;
                End If;

                l_MODIFIERS_tbl(l_Ln_Count).PRICE_BREAK_TYPE_CODE       := Mod_Line_Rec.LN_PRICE_BREAK_TYPE_CODE;
                --l_MODIFIERS_tbl(l_Ln_Count).PRODUCT_UOM_CODE            := Mod_Line_Rec.LN_PRODUCT_UOM_CODE ;
                --l_MODIFIERS_tbl(l_Ln_Count).ARITHMETIC_OPERATOR_TYPE    := Mod_Line_Rec.LN_ARITHMETIC_OPERATOR_TYPE ;
                l_MODIFIERS_tbl(l_Ln_Count).ARITHMETIC_OPERATOR         := Mod_Line_Rec.LN_ARITHMETIC_OPERATOR ;
                l_MODIFIERS_tbl(l_Ln_Count).OPERAND                     := Mod_Line_Rec.LN_OPERAND ;
                l_MODIFIERS_tbl(l_Ln_Count).ACCRUAL_FLAG                := FND_API.G_MISS_CHAR ;--Mod_Line_Rec.LN_ACCRUAL_FLAG ;
                l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE    := Mod_Line_Rec.LN_INCOMPATIBILITY_GRP_CODE ;
                -- TMS# 20130322-01259 Satish U: 31-Mar-2013 Assign Values only if they are not NULL
                If Mod_Line_Rec.LN_PRICING_GROUP_SEQUENCE  Is Not Null Then
                   l_MODIFIERS_tbl(l_Ln_Count).PRICING_GROUP_SEQUENCE      := Mod_Line_Rec.LN_PRICING_GROUP_SEQUENCE ;
                End If;
                -- TMS# 20130322-01259 Satish U: 31-Mar-2013 Assign Values only if they are not NULL
                If Mod_Line_Rec.LN_INCLUDE_ON_RETURNS_FLAG Is Not Null Then
                   l_MODIFIERS_tbl(l_Ln_Count).INCLUDE_ON_RETURNS_FLAG     := Mod_Line_Rec.LN_INCLUDE_ON_RETURNS_FLAG ;
                End If;

                l_MODIFIERS_tbl(l_Ln_Count).PRICE_BY_FORMULA_ID         := Mod_Line_Rec.LN_PRICE_BY_FORMULA_ID ;
                --l_MODIFIERS_tbl(l_Ln_Count).ORIG_SYS_LINE_REF           :=   Mod_Line_Rec.LN_ORIG_SYS_LINE_REF ;
                -- Check if it is new line or updating old line
                --If Mod_Line_Rec.LN_ORIG_SYS_LINE_REF  Is Not Null Then
                -- Check if List Line ID exists
                Select Count(*)
                Into l_Rec_Count
                From apps.QP_List_Lines
                Where List_Line_ID = Mod_Line_Rec.LN_LIST_LINE_ID ;

                --If Mod_Line_Rec.LN_LIST_LINE_ID is NULL Then
                If l_Rec_Count = 0 THen
                  l_MODIFIERS_tbl(l_Ln_Count).operation  := QP_GLOBALS.G_OPR_CREATE;
                  Fnd_File.Put_Line ( Fnd_File.LOG, '115.110 Modifer Line Record  : Create Operation=>' );
                Elsif Mod_Line_Rec.LN_ORIG_SYS_LINE_REF  Is Null Then
                   l_MODIFIERS_tbl(l_Ln_Count).operation := QP_GLOBALS.G_OPR_UPDATE;

                End If;

                /* Create and Update Pricing Attributes ***/

                For Pricing_Attr_Rec In Get_Pricing_Attr_Cur (P_List_Header_ID => Mod_List_Rec.List_Header_ID,
                                                              p_List_Line_ID   => Mod_Line_Rec.LN_LIST_LINE_ID ) Loop
                    Fnd_File.Put_Line ( Fnd_File.LOG, '116 Pricing Attribute Record  =>' || Pricing_Attr_Rec.PA_PRICING_ATTRIBUTE_ID );
                    l_pa_Count := l_pa_Count + 1;
                    l_PRICING_ATTR_tbl(l_pa_Count).LIST_HEADER_ID            :=  Mod_List_Rec.List_Header_ID;
                    l_PRICING_ATTR_tbl(l_pa_Count).LIST_LINE_ID              :=  Mod_Line_Rec.LN_LIST_LINE_ID ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_ID      :=  Pricing_Attr_Rec.PA_PRICING_ATTRIBUTE_ID ;
                    --l_PRICING_ATTR_tbl(l_pa_Count).ORIG_SYS_PRICING_ATTR_REF :=  Pricing_Attr_Rec.PA_ORIG_SYS_PRICING_ATTR_REF;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE_CONTEXT :=  'ITEM'; --Pricing_Attr_Rec.PA_PRODUCT_ATTRIBUTE_CONTEXT ;
                    Fnd_File.Put_Line ( Fnd_File.LOG, '116.4 PROduct Attribute Context =>' || l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE_CONTEXT );
                    l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE         :=  Pricing_Attr_Rec.PA_PRODUCT_ATTRIBUTE ;
                    Fnd_File.Put_Line ( Fnd_File.LOG, '116.5 PROduct Attribute  =>' || l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE );

                    l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTR_VALUE        :=  Pricing_Attr_Rec.PA_PRODUCT_ATTR_VALUE ;

                    Fnd_File.Put_Line ( Fnd_File.LOG, '116.6 PROduct Attribute Value =>' || l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTR_VALUE);

                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE         :=  Pricing_Attr_Rec.PA_PRICING_ATTRIBUTE ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_UOM_CODE          :=  Pricing_Attr_Rec.PA_PRODUCT_UOM_CODE ;
                    l_PRICING_ATTR_tbl(l_pa_Count).COMPARISON_OPERATOR_CODE  :=  Pricing_Attr_Rec.PA_COMPARISON_OPERATOR_CODE ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PA_PRICING_ATTRIBUTE_CONTEXT ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_FROM   :=  Pricing_Attr_Rec.PA_PRICING_ATTR_VALUE_FROM ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_TO     :=  Pricing_Attr_Rec.PA_PRICING_ATTR_VALUE_TO ;

                    --Satish U: 03-MAY-2012 :  Derive PA_PRICING_ATTRIBUTE
                    If Pricing_Attr_Rec.PA_PRICING_ATTRIBUTE_CONTEXT  IS NOT NULL
                       AND Pricing_Attr_Rec.PA_PRICING_ATTRIBUTE  IS NOT NULL Then
                       Begin
                          Select Segment_Mapping_Column
                          INto l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE
                          From apps.qp_segments_v qs,
                                 apps.QP_PRC_CONTEXTS_V qpc
                          Where qs.prc_Context_ID = qpc.prc_context_ID
                          And qpc.Prc_Context_Code = Pricing_Attr_Rec.PA_PRICING_ATTRIBUTE_CONTEXT
                          and qpc.prc_context_type = 'PRICING_ATTRIBUTE'
                          and qs.User_Segment_Name = Pricing_Attr_Rec.PA_PRICING_ATTRIBUTE ;
                       Exception
                          When Others Then
                             Null;
                       End ;
                    End If;

                                        --JAN-24-2012
                    l_PRICING_ATTR_tbl(l_pa_Count).ATTRIBUTE_GROUPING_NO     :=  1; -- To_NUmber(Pricing_Attr_Rec.PA_ATTRIBUTE_GROUPING_NO) ;

                    -- Check if Pricing Attribute Record exists
                    Select Count(*)
                    Into l_Rec_Count
                    From apps.QP_Pricing_Attributes
                    Where PRicing_Attribute_ID = Pricing_Attr_Rec.PA_PRICING_ATTRIBUTE_ID ;


                    --If Pricing_Attr_Rec.PA_ORIG_SYS_PRICING_ATTR_REF  Is Not Null Then
                    --If Pricing_Attr_Rec.PA_PRICING_ATTRIBUTE_ID Is Null Then
                    IF l_Rec_Count = 0 THen
                       l_PRICING_ATTR_tbl(l_pa_Count).Operation   := QP_GLOBALS.G_OPR_CREATE;
                       l_PRICING_ATTR_tbl(l_pa_Count).EXCLUDER_FLAG   :=  'N';
                       l_PRICING_ATTR_tbl(l_pa_Count).ATTRIBUTE_GROUPING_NO     :=  l_pa_Count ;
                       --l_PRICING_ATTR_tbl(l_pa_Count).Modifiers_Index           := l_PA_COUNT;

                     Else
                        l_PRICING_ATTR_tbl(l_pa_Count).EXCLUDER_FLAG   :=  FND_API.G_MISS_CHAR;
                        l_PRICING_ATTR_tbl(l_pa_Count).Operation := QP_GLOBALS.G_OPR_UPDATE;

                        l_PRICING_ATTR_tbl(l_pa_Count).ATTRIBUTE_GROUPING_NO     :=  FND_API.G_MISS_NUM ;
                    End If;
                    l_PRICING_ATTR_tbl(l_pa_Count).MODIFIERS_index:=1;
                ENd Loop ;  -- Pricing Attributes
                /* Create Or Update  Qualifier line  */

             End Loop ;  -- Prcing Modifier Lines
             Fnd_File.Put_Line ( Fnd_File.LOG, '117.4 Number of Modifier Lines  Record Records =>' || l_pa_Count );
             -- Satis U: 20-MAR-2013 Commented Called to Validate_List_Lines API
             -- Satish U: 12-26-2012 : Validate Pricing Quantity and Start Date and End Dates for for Overlap or Duplicate values
             Validate_list_Lines (p_Header_Id      => l_MODIFIER_LIST_rec.List_Header_ID ,
                                  X_Return_Status  => l_Return_Status );

             If l_Return_Status  <>'S' Then
             -- If Return Status is Not Successful then Complete Concurrent Program in Warning.
               RetCode  := 1 ;
               Delete From apps.XXWC_QP_MODIFIER_INT
               Where List_Header_ID =  l_MODIFIER_LIST_rec.List_Header_ID  ;

               Fnd_File.Put_Line ( Fnd_File.LOG, '124.1 Process Modifiers Deleted From interface table =>' || l_MODIFIER_LIST_rec.NAME);
               Commit;
               Fnd_File.Put_Line ( Fnd_File.OUTPUT, 'Validating list lines failed for Modifier : ' || l_MODIFIER_LIST_rec.Name  );
             Else
                -- End Loop;  -- Modifier List Header s
                /* Call the Modifiers Public API to create the modifier header, modifier line, qualifier and a pricing attributes record */
                Fnd_File.Put_Line ( Fnd_File.LOG, '118 Before calling Process Modifiers API  =>' );
                oe_debug_pub.add('15: Before calling Process Modifiers API  =>');
                QP_Modifiers_PUB.Process_Modifiers
                   ( p_api_version_number    => 1.0
                   , p_init_msg_list         => FND_API.G_FALSE
                   , p_return_values         => FND_API.G_FALSE
                   , p_commit                => FND_API.G_TRUE
                   , x_return_status         => l_return_status
                   , x_msg_count             => l_msg_count
                   , x_msg_data              => l_msg_data
                   , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
                   , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
                   , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
                   , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
                   , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
                   , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
                   , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
                   , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
                   , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
                   , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
                   , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
                   , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );

                 Fnd_File.Put_Line ( Fnd_File.LOG, '119 After calling Process Modifiers API  =>' );

                 IF l_Return_Status =  G_RETURN_SUCCESS THen
                    Fnd_File.Put_Line ( Fnd_File.LOG, '120 Process Modifiers API Return Status is Success =>' );
                    Fnd_File.Put_Line ( Fnd_File.LOG, '121.2 Modifier List Header is  =>' || l_MODIFIER_LIST_rec.NAME);
                    Fnd_File.Put_Line ( Fnd_File.LOG, '122.2 Modifier List Header is  =>' || lx_MODIFIER_LIST_rec.List_Header_ID );
                    Fnd_File.Put_Line ( Fnd_File.OUTPUT, ' Successfully processed Modifier List =>' || l_MODIFIER_LIST_rec.NAME);
                    -- Delete Modifiers upon successfully imported.

                    Delete From apps.XXWC_QP_MODIFIER_INT
                    Where List_Header_ID =  l_MODIFIER_LIST_rec.List_Header_ID  ;

                    Fnd_File.Put_Line ( Fnd_File.LOG, '124.1 Process Modifiers Deleted From interface table =>' || l_MODIFIER_LIST_rec.NAME);
                    Commit;
                 ELSE
                    Fnd_File.Put_Line ( Fnd_File.LOG, '125 Process Modifiers API Return Status is =>' || l_Return_Status);
                    Fnd_File.Put_Line ( Fnd_File.LOG, '126 Process Modifiers API Msg Count is =>' || l_Msg_Count);
                    Fnd_File.Put_Line ( Fnd_File.OUTPUT, ' Failed to process Modifier List =>' || l_MODIFIER_LIST_rec.NAME);
                    FOR i IN 1 .. l_msg_count loop
                       l_msg_data := oe_msg_pub.get( p_msg_index => i, p_encoded => 'F'  );
                    End Loop ;
                    Fnd_File.Put_Line ( Fnd_File.LOG, '127 Process Modifiers API Error Message is =>' || l_Msg_Data);
                    Fnd_File.Put_Line ( Fnd_File.OUTPUT, 'Process Modifiers API Error Message is =>' || l_Msg_Data);


                    -- Satish U: 07-MAY-2012 : If Modifer does not originate from Conversion staging tables then Delete upon failure.
                    -- User will reload the transactions from spreadsheet .
                    -- TMS# 20130322-01259 Satish U: 31-Mar-2013  Delete Lines in all cases
                    Delete From apps.XXWC_QP_MODIFIER_INT
                    Where  List_Header_ID = l_MODIFIER_LIST_rec.List_Header_ID ;
                    /*******************
                    And Not Exists ( Select 1
                    From  XXWC_QP_MODIFIER_HDR_CNV HDR
                    Where List_Header_ID = l_MODIFIER_LIST_rec.List_Header_ID  ) ;
                    ***********************/
                    -- Check if Modifier List is from Conversion If So Update the  table else delete Modifier Information :
                    -- Satish U : 07-MAY-2012 :
                    /*******************************
                      Satish U : 23-MAR-2013 : SInce rows are deleted no need to update status
                    Update XXWC_QP_MODIFIER_INT
                       Set Status = C_STATUS_ERROR ,
                       Error_Message =  l_msg_data
                    Where List_Header_ID =  l_MODIFIER_LIST_rec.List_Header_ID
                    AND    Status <> C_STATUS_PROCESSED
                    And Exists ( Select 1
                       From  XXWC_QP_MODIFIER_HDR_CNV HDR
                       Where List_Header_ID = l_MODIFIER_LIST_rec.List_Header_ID  ) ;

                    Fnd_File.Put_Line ( Fnd_File.LOG, '128 Number of rows updated with status Error  =>' || SQL%ROWCOUNT);
                    *********************************/
                    RetCode  := 1 ;
                    Commit;
                 END IF;

                 -- Satish U: 22-MAR-2013

                 Begin
                    -- TMS# 20130322-01259 Satish U: 31-Mar-2013 Truncate Table
                    Select Count(*)
                    Into L_Line_Count
                    From  apps.XXWC_QP_MODIFIER_INT  ;

                    If l_Line_Count = 0 Then
                       Begin
                           Execute Immediate 'Truncate Table XXWC.XXWC_QP_MODIFIER_INT' ;
                           Fnd_File.Put_Line ( Fnd_File.LOG, '130 XXWC_QP_MODIFIER_INT Table is truncated');
                       Exception
                          When Others Then
                            l_Msg_Data := Substr(SQLERRM,200);
                            RetCode  := 1 ;
                            Fnd_File.Put_Line ( Fnd_File.LOG, '130.1 Table Truncation Error Message =>' || l_Msg_Data);

                       End ;

                       Null;
                    End If;
                 End ;

              End IF; --
        Exception
            When Others Then

               l_Msg_Data := Substr(SQLERRM,200);
               RetCode  := 1 ;
               Fnd_File.Put_Line ( Fnd_File.LOG, '131 Process Modifiers API Error Message is =>' || l_Msg_Data);
               /******************* Satish U: Following Update Statement is no more required
               Update XXWC_QP_MODIFIER_INT
                   Set Status = C_STATUS_ERROR ,
                       Error_Message =  l_msg_data
               Where List_Header_ID =  l_MODIFIER_LIST_rec.List_Header_ID ;
               ***************************/
        End ;
     End Loop;  -- Modifier List Header s
   EXCEPTION

      WHEN OTHERS       THEN

         l_Msg_Data := SUBSTR(SQLERRM,1,200);
          RetCode  := 1 ;
         Fnd_File.Put_Line ( Fnd_File.LOG, '141 Process Modifiers API Error Message is =>' || l_Msg_Data);
          --x_Return_Status := G_Return_Error;
          --x_Msg_Data      := l_Msg_Data ;
         Raise_Application_error (-20001, l_Msg_Data);
   End Import ;

   Function Get_Product_Attr_Code (
        p_attribute_name         IN VARCHAR2,
        p_attr_value             IN VARCHAR2 ) Return  Number IS

      l_item_name varchar2(240)      := NULL;
      l_Inventory_Item_ID             Number;
      l_Category_ID                   Number ;
      l_category_name varchar2(240)  := NULL;
      l_segment_name varchar2(240)   := NULL;
      l_organization_id Number  :=  FND_Profile.Value('XXWC_ITEM_MASTER_ORG');

   Begin
      IF (p_attribute_name = 'PRICING_ATTRIBUTE1') then
        select Inventory_Item_ID
        into l_Inventory_Item_ID
        from apps.mtl_system_items_vl
        where Segment1 = to_char(p_attr_value)
        and organization_id = l_organization_id
        and rownum=1;

        RETURN l_Inventory_Item_ID;

     ELSIF (p_attribute_name = 'PRICING_ATTRIBUTE2') then

        select category_ID
        into l_category_ID
        from apps.qp_item_categories_v
        where category_Name = to_char(p_attr_value)
        and rownum=1;

        RETURN l_category_ID;
      End IF;
   Exception
      When Others Then
            Return Null;
   ENd;

   Function Get_Product_Attr_Value (
        p_attribute_name         IN VARCHAR2,
        p_attr_Code              IN Number ) Return Varchar2 IS
      l_item_name varchar2(240)      := NULL;
      l_Inventory_Item_ID             Number;
      l_Category_ID                   Number ;
      l_category_name varchar2(240)  := NULL;
      l_segment_name varchar2(240)   := NULL;
      l_organization_id Number  :=  FND_Profile.Value('XXWC_ITEM_MASTER_ORG');
   Begin
      IF (p_attribute_name = 'PRICING_ATTRIBUTE1') then
        select Segment1
        into l_item_name
        from apps.mtl_system_items_vl
        where Inventory_Item_ID = p_attr_Code
        and organization_id = l_organization_id
        and rownum=1;

        RETURN l_item_name;

     ELSIF (p_attribute_name = 'PRICING_ATTRIBUTE2') then

        select category_Name
        into l_category_name
        from apps.qp_item_categories_v
        where category_ID = p_attr_Code
        and rownum=1;

        RETURN l_category_Name;
      End IF;
   Exception
      When Others Then
            Return Null;
   ENd;

   Procedure  Bulk_Import( Errbuf            OUT NOCOPY VARCHAR2,
                           Retcode           OUT NOCOPY NUMBER ) IS

      l_mod_name                 VARCHAR2 (100)  := 'XXWC_QP_MODIFIER_WEB_ADI_PKG.BULK_IMPORT :' ;
      l_return_status            VARCHAR2(1);

      l_Msg_Data                 Varchar2(2000);
      l_Msg_Count                Number ;
      l_Batch_ID                 Number ;
      l_Request_ID               Number := Fnd_Global.Conc_Request_ID;
      l_User_ID                  Number := Fnd_Global.User_ID ;
      l_List_Header_ID           Number ;
      l_List_Line_ID             Number ;
      l_Qualifier_ID             Number;
      l_Pricing_Attribute_ID     Number ;
      l_Login_ID                 Number ;
      L_PA_PRODUCT_ATTR_VALUE    Varchar2(30) ;
      l_PA_PROUDCT_ATTRIBUTE     Varchar2(30) ;
      l_Modifier_import_req_id   Number ;
      l_PA_PRICING_ATTRIBUTE     Varchar2(30) ;

      l_request_status               BOOLEAN;
      l_interval                     NUMBER := 30;
      l_max_wait                     NUMBER := 0;
      l_phase                        VARCHAR2(80);
      l_status                       VARCHAR2(80);
      l_dev_phase                    VARCHAR2(30);
      l_dev_status                   VARCHAR2(30);
      l_message                      VARCHAR2(240);
      l_Site_Use_ID                  Number;

      C_STATUS_VALIDATE          CONSTANT   VARCHAR2(1) := 'V';
      C_STATUS_ERROR             CONSTANT   VARCHAR2(1) := 'E';
      C_STATUS_PROCESSED         CONSTANT   VARCHAR2(1) := 'P' ;
      C_OPERATION                CONSTANT   VARCHAR2(30) := QP_GLOBALS.G_OPR_CREATE;
      l_err_callpoint            Varchar2(80) := 'Start';
      l_distro_list              VARCHAR2(80) := 'OracleDevelopmentGroup@hdsupply.com';
      -- Define a Cursor to read value from Modifer HDR and Modifer Lines Table.
      Cursor  Modifier_Cur IS
         Select DISTINCT
              HDR.ROWID      HDR_ROWID        ,
              HDR.MODIFIER_TYPE, -- Added Modifier Type - Gopi
              HDR.BATCH_ID           ,
              HDR.LIST_HEADER_ID       ,
              HDR.MODIFIER_LIST_NAME     ,
              HDR.MODIFIER_LIST_NUMBER   ,
              HDR.CURRENCY_CODE          ,
              HDR.LIST_TYPE_CODE         ,
              HDR.LIST_TYPE              ,
              HDR.SOURCE_SYSTEM_CODE     ,
              HDR.PTE_CODE               ,
              HDR.START_DATE_ACTIVE      ,
              HDR.END_DATE_ACTIVE        ,
              HDR.GLOBAL_FLAG             ,
              HDR.AUTOMATIC_FLAG           ,
              HDR.ACTIVE_FLAG              ,
              HDR.COMMENTS                 ,
              HDR.QL_QUALIFIER_ID           ,
              HDR.QL_ORIG_SYS_QUALIFIER_REF ,
              HDR.QL_QUALIFIER_GROUPING_NO  ,
              HDR.QL_QUALIFIER_CONTEXT      ,
              HDR.QL_QUALIFIER_ATTRIBUTE    ,
              HDR.QL_QUALIFIER_ATTR_VALUE       ,
              HDR.QL_QUALIFIER_ATTRIBUTE_VALUE ,
              HDR.QL_QUALIFIER_ATTR_VALUE_TO    ,
              HDR.QL_COMPARISON_OPERATOR_CODE   ,
              HDR.QL_QUALIFIER_PRECEDENCE ,
              HDR.QL_ACTIVE_FLAG          ,
              HDR.QL_LIST_TYPE_CODE        ,
              HDR.LEGACY_CUSTOMER_NUMBER    ,
              HDR.LEGACY_PARTY_SITE_NUMBER  ,
              HDR.STATUS,
              Line.ROWID   Line_ROWID ,
              Line.LN_ORIG_SYS_LINE_REF         ,
              Line.LN_LIST_LINE_ID               ,
              Line.LN_LIST_LINE_NO               ,
              Line.LN_MODIFIER_LEVEL    ,
              Line.LN_MODIFIER_LEVEL_CODE    ,
              Line.LN_LIST_LINE_TYPE       ,
              Line.LN_LIST_LINE_TYPE_CODE    ,
              Line.LN_START_DATE_ACTIVE ,
              Line.LN_END_DATE_ACTIVE      ,
              Line.LN_AUTOMATIC_FLAG      ,
              Line.LN_OVERRIDE_FLAG    ,
              Line.LN_PRICING_PHASE    ,
              Line.LN_PRICING_PHASE_ID     ,
              Line.LN_PRODUCT_PRECEDENCE    ,
              Line.LN_PRICE_BREAK_TYPE_CODE ,
              Line.LN_ARITHMETIC_OPERATOR_TYPE   ,
              Line.LN_ARITHMETIC_OPERATOR        ,
              Line.LN_OPERAND                  ,
              Line.LN_ACCRUAL_FLAG         ,
              Line.LN_INCOMPATIBILITY_GRP_CODE   ,
              Line.LN_PRICING_GROUP_SEQUENCE    ,
              Line.LN_INCLUDE_ON_RETURNS_FLAG    ,
              Line.LN_FORMULA         ,
              Line.LN_GENERATE_USING_FORMULA_ID  ,
              Line.LN_PRICE_BY_FORMULA_ID     ,
              Line.LN_QUALIFICATION_IND   ,
              Line.LN_MODIFIER_PARENT_INDEX ,
              Line.PA_PRICING_ATTRIBUTE_ID   ,
              Line.PA_ORIG_SYS_PRICING_ATTR_REF  ,
              Line.PA_PRODUCT_ATTRIBUTE_CONTEXT  ,
              Line.PA_PRODUCT_ATTRIBUTE          ,
              Line.ORACLE_PA_PRODUCT_ATTRIBUTE  ,
              Line.PA_PRODUCT_ATTR_VALUE        ,
              Line.ORACLE_PA_PRODUCT_ATTR_VALUE  ,
              Line.PA_PRODUCT_UOM_CODE           ,
              Line.PA_PRICING_ATTRIBUTE          ,
              Line.ORACLE_PA_PRICING_ATTRIBUTE  ,
              Line.PA_COMPARISON_OPERATOR_CODE   ,
              Line.PA_PRICING_ATTRIBUTE_CONTEXT  ,
              Line.PA_PRICING_ATTR_VALUE_FROM   ,
              Line.PA_PRICING_ATTR_VALUE_TO      ,
              Line.PA_EXCLUDER_FLAG              ,
              Line.PA_ATTRIBUTE_GROUPING_NO      ,
              Line.PA_PRICING_PHASE_ID           ,
              Line.PA_QUALIFICATION_IND
         From apps.XXWC_QP_MODIFIER_LINE_CNV Line,
              apps.XXWC_QP_MODIFIER_HDR_CNV HDR
         Where Line.MODIFIER_LIST_NUMBER  = HDR.MODIFIER_LIST_NUMBER
         AND   Line.List_Header_Id        = HDR.List_Header_ID
         AND   HDR.Status  = 'V'
         AND   Line.Status = 'V' ;



       Cursor Get_List_Hdr_Cur Is (
            Select Distinct MODIFIER_LIST_NUMBER
            From apps.XXWC_QP_MODIFIER_HDR_CNV
            Where List_Header_ID IS NULL );

       Cursor Get_Item_ID_Cur ( P_Item_Number Varchar2 ) IS
         Select Inventory_Item_ID
         From apps.Mtl_System_Items_B
         Where Segment1 = P_ITEM_NUMBER
         AND   Organization_ID = Fnd_profile.Value('XXWC_ITEM_MASTER_ORG')
         and Rownum = 1;

      Cursor Get_Cat_ID_Cur ( P_Category Varchar2 ) IS
         Select CATEGORY_ID
         From apps.XXWC_ITEM_CATEGORY_VW
         Where Item_Category = P_Category
         and Rownum = 1;

      Cursor Get_Site_User_Cur  ( p_Party_Site_Number  Varchar2 ) Is
            SELECT   hcsu.Site_USe_ID
            FROM apps.hz_cust_acct_sites_all hcas,
                 apps.hz_cust_site_uses_all hcsu,
                 apps.hz_party_Sites hps
            WHERE  hps.Party_Site_number = p_Party_Site_Number
            and    hps.party_site_Id     = hcas.party_site_Id
            And    hcsu.CUST_ACCT_SITE_ID = hcas.CUST_ACCT_SITE_ID
            AND    hcsu.site_use_code = 'SHIP_TO'
            And    hcsu.Status = 'A' ;

      Cursor  Get_Cust_Cur (P_Customer_Number  Varchar2 ) IS
          Select Cust_Account_ID
          From   apps.HZ_CUST_ACCOUNTS_ALL
          WHERE  Account_Number = p_Customer_Number ;


      -- Get list of New Qualifier Records
      Cursor  Get_New_QL_Cur  Is
        Select Distinct LIST_HEADER_ID , RowID
        --, LN_List_Line_ID, rowid
        --QL_ORIG_SYS_QUALIFIER_REF
        From  apps.XXWC_QP_MODIFIER_HDR_CNV
        Where QL_QUALIFIER_ID is Null
        AND   QL_QUALIFIER_ATTRIBUTE is Not Null
        And   NVL(Status,'NULL') <> C_STATUS_PROCESSED ;

      -- Get List of New lines Records
      Cursor  Get_New_Ln_Cur  Is
        Select Distinct LIST_HEADER_ID , rowid
        From  apps.XXWC_QP_MODIFIER_LINE_CNV
        Where LN_LIST_LINE_ID is Null
        AND   LN_LIST_Line_NO is NULL
        And   NVL(Status,'NULL') <> C_STATUS_PROCESSED ;

     -- Get List of New Pricing Attribute Lines
      Cursor  Get_New_PA_Cur  Is
        Select Distinct LIST_HEADER_ID , LN_List_Line_ID
        --, rowid
        --PA_ORIG_SYS_PRICING_ATTR_REF
        From apps.XXWC_QP_MODIFIER_LINE_CNV
        Where PA_PRICING_ATTRIBUTE_ID is Null
        And   NVL(Status,'NULL') <> C_STATUS_PROCESSED ;

     -- Cursor To Get the List of All the Header Records Where Customer INformation exists
     Cursor  Mod_HDr_Cur IS
         Select
              HDR.ROWID      HDR_ROWID        ,
              HDR.BATCH_ID           ,
              HDR.LIST_HEADER_ID       ,
              HDR.MODIFIER_LIST_NAME     ,
              HDR.MODIFIER_LIST_NUMBER   ,
              HDR.CURRENCY_CODE          ,
              HDR.LIST_TYPE_CODE         ,
              HDR.LIST_TYPE              ,
              HDR.SOURCE_SYSTEM_CODE     ,
              HDR.PTE_CODE               ,
              HDR.START_DATE_ACTIVE      ,
              HDR.END_DATE_ACTIVE        ,
              HDR.GLOBAL_FLAG             ,
              HDR.AUTOMATIC_FLAG           ,
              HDR.ACTIVE_FLAG              ,
              HDR.COMMENTS                 ,
              HDR.QL_QUALIFIER_ID           ,
              HDR.QL_ORIG_SYS_QUALIFIER_REF ,
              HDR.QL_QUALIFIER_GROUPING_NO  ,
              HDR.QL_QUALIFIER_CONTEXT      ,
              HDR.QL_QUALIFIER_ATTRIBUTE    ,
              HDR.QL_QUALIFIER_ATTR_VALUE       ,
              HDR.QL_QUALIFIER_ATTRIBUTE_VALUE ,
              HDR.QL_QUALIFIER_ATTR_VALUE_TO    ,
              HDR.QL_COMPARISON_OPERATOR_CODE   ,
              HDR.QL_QUALIFIER_PRECEDENCE ,
              HDR.QL_ACTIVE_FLAG          ,
              HDR.QL_LIST_TYPE_CODE        ,
              HDR.LEGACY_CUSTOMER_NUMBER    ,
              HDR.LEGACY_PARTY_SITE_NUMBER  ,
              HDR.STATUS
        From apps.XXWC_QP_MODIFIER_HDR_CNV HDR
        Where   NVL(HDR.Status,'R') <> 'P'  ;

      --- Get All The Lines and Populate
      -- Cursor To Get the List of All the Header Records Where Customer INformation exists
       Cursor  Mod_Line_Cur IS
          Select DISTINCT Line.ROWID   Line_ROWID ,
              Line.List_Header_ID   ,
              Line.LN_ORIG_SYS_LINE_REF         ,
              Line.LN_LIST_LINE_ID               ,
              Line.LN_LIST_LINE_NO               ,
              Line.LN_MODIFIER_LEVEL    ,
              Line.LN_MODIFIER_LEVEL_CODE    ,
              Line.LN_LIST_LINE_TYPE       ,
              Line.LN_LIST_LINE_TYPE_CODE    ,
              Line.LN_START_DATE_ACTIVE ,
              Line.LN_END_DATE_ACTIVE      ,
              Line.LN_AUTOMATIC_FLAG      ,
              Line.LN_OVERRIDE_FLAG    ,
              Line.LN_PRICING_PHASE    ,
              Line.LN_PRICING_PHASE_ID     ,
              Line.LN_PRODUCT_PRECEDENCE    ,
              Line.LN_PRICE_BREAK_TYPE_CODE ,
              Line.LN_ARITHMETIC_OPERATOR_TYPE   ,
              Line.LN_ARITHMETIC_OPERATOR        ,
              Line.LN_OPERAND                  ,
              Line.LN_ACCRUAL_FLAG         ,
              Line.LN_INCOMPATIBILITY_GRP_CODE   ,
              Line.LN_PRICING_GROUP_SEQUENCE    ,
              Line.LN_INCLUDE_ON_RETURNS_FLAG    ,
              Line.LN_FORMULA         ,
              Line.LN_GENERATE_USING_FORMULA_ID  ,
              Line.LN_PRICE_BY_FORMULA_ID     ,
              Line.LN_QUALIFICATION_IND   ,
              Line.LN_MODIFIER_PARENT_INDEX ,
              Line.PA_PRICING_ATTRIBUTE_ID   ,
              Line.PA_ORIG_SYS_PRICING_ATTR_REF  ,
              Line.PA_PRODUCT_ATTRIBUTE_CONTEXT  ,
              Line.PA_PRODUCT_ATTRIBUTE          ,
              Line.ORACLE_PA_PRODUCT_ATTRIBUTE ,
              Line.PA_PRODUCT_ATTR_VALUE        ,
              Line.ORACLE_PA_PRODUCT_ATTR_VALUE ,
              Line.PA_PRODUCT_UOM_CODE           ,
              Line.PA_PRICING_ATTRIBUTE          ,
              Line.ORACLE_PA_PRICING_ATTRIBUTE   ,
              Line.PA_COMPARISON_OPERATOR_CODE   ,
              Line.PA_PRICING_ATTRIBUTE_CONTEXT  ,
              Line.PA_PRICING_ATTR_VALUE_FROM   ,
              Line.PA_PRICING_ATTR_VALUE_TO      ,
              Line.PA_EXCLUDER_FLAG              ,
              Line.PA_ATTRIBUTE_GROUPING_NO      ,
              Line.PA_PRICING_PHASE_ID           ,
              Line.PA_QUALIFICATION_IND
         From apps.XXWC_QP_MODIFIER_LINE_CNV Line,
              apps.XXWC_QP_MODIFIER_HDR_CNV HDR
         Where Line.MODIFIER_LIST_NUMBER  = HDR.MODIFIER_LIST_NUMBER
         AND   Line.List_Header_Id        = HDR.List_Header_ID
         And   NVL(HDR.Status,'R') <> 'P'
         And   NVL(Line.Status , 'R') <> 'P' ;

   Begin
      Fnd_File.Put_Line ( Fnd_File.LOG, '101 Begining of API =>' || l_mod_name);
      fnd_profile.PUT('QP_PRICING_TRANSACTION_ENTITY', 'ORDFUL');
      fnd_profile.PUT('QP_SOURCE_SYSTEM_CODE', 'QP');

      fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                  resp_id        => fnd_global.resp_id,
                                  resp_appl_id   => fnd_global.resp_appl_id);

      SELECT   mo_global.get_current_org_id ()
      INTO g_org_id
      FROM DUAL;

      mo_global.set_policy_context ('S', g_org_id);



      -- Generate List_HEader ID
      For HDR_REc In Get_List_Hdr_Cur Loop
         Select QP_LIST_HEADERS_B_S.NextVal
         Into l_List_Header_ID
         From Dual ;

         If l_Batch_ID is Null Then
            Select XXWC_QP_BATCH_S.NextVal
            Into l_Batch_ID
             From Dual ;
         End If ;

         Update apps.XXWC_QP_MODIFIER_HDR_CNV
            Set  List_Header_ID   = l_List_Header_ID ,
                 Batch_ID         = l_Batch_ID,
                 Request_ID       = l_Request_ID,
                 Creation_Date    = Sysdate,
                 Created_By       = l_User_ID,
                 Last_Updated_By  = l_User_ID,
                 Last_Update_Date = Sysdate,
                 Status           = 'R'
         Where MODIFIER_LIST_NUMBER = HDR_Rec.MODIFIER_LIST_NUMBER ;

         Fnd_File.Put_Line ( Fnd_File.LOG, '102 Number of HDR Records Updated with List Header ID =>' || SQL%ROWCOUNT);

         Update apps.XXWC_QP_MODIFIER_LINE_CNV
            Set  List_Header_ID   = l_List_Header_ID,
                 Request_ID       = l_Request_ID,
                 Creation_Date    = Sysdate,
                 Created_By       = l_User_ID,
                 Last_Updated_By  = l_User_ID,
                 Last_Update_Date = Sysdate,
                 Status           = 'R'
         Where MODIFIER_LIST_NUMBER = HDR_Rec.MODIFIER_LIST_NUMBER ;

         Fnd_File.Put_Line ( Fnd_File.LOG, '103 Number of LINE Records Updated with List Header ID =>' || SQL%ROWCOUNT);
      ENd Loop ;

      -- Generate QL_QUalifier ID
      -- Update Qualifier Id for New Rows
      For New_QL_Rec In Get_New_QL_Cur Loop
         Select QP_QUALIFIERS_S.NextVal
         Into l_Qualifier_ID
         From Dual ;

         Fnd_File.Put_Line ( Fnd_File.LOG, '113 Generating QUalifier ID for new lines   =>' || l_Qualifier_ID );

         Update apps.XXWC_QP_MODIFIER_HDR_CNV
           Set QL_Qualifier_ID  = l_Qualifier_ID ,
               Status           = 'R'
         Where  RowID   = New_QL_Rec.RowId;

      End Loop ;

      -- Generate List_Line_ID
      For New_Ln_Rec In Get_New_Ln_Cur   Loop

        SELECT QP_LIST_LINES_S.nextval
        INTO l_list_line_id
        FROM DUAL;
        Fnd_File.Put_Line ( Fnd_File.LOG, '111 Generating List Line ID for new lines   =>' || l_list_line_id );

        Update apps.XXWC_QP_MODIFIER_LINE_CNV
           Set LN_List_LIne_ID  = l_List_Line_ID ,
               LN_List_Line_No  = To_Char(l_List_Line_ID) ,
               Status           = 'R'
        Where List_Header_Id = New_Ln_Rec.List_Header_ID
        And   rowId = New_Ln_Rec.RowId;

       -- and   LN_ORIG_SYS_LINE_REF = New_Ln_Rec.LN_ORIG_SYS_LINE_REF ;

      End Loop ;

      -- Update Pricing Attribute ID  for new Pricing Attributes

      For New_PA_Rec In Get_New_PA_Cur Loop
         Select QP_PRICING_ATTRIBUTES_S.NextVal
         Into l_Pricing_Attribute_ID
         From Dual ;

         Fnd_File.Put_Line ( Fnd_File.LOG, '112 Generating Pricing Attribute ID for new lines   =>' || l_Pricing_Attribute_ID );

         Update apps.XXWC_QP_MODIFIER_LINE_CNV
           Set PA_Pricing_Attribute_ID  = l_Pricing_Attribute_ID ,
               Status                   = 'R'
         Where List_Header_Id = New_PA_Rec.List_Header_ID
         And   LN_List_Line_ID = New_PA_Rec.LN_List_Line_ID ;



      End Loop ;

      -- Loop Through The Header Records and generate Ship To Site USE ID for qualifiers
      For Hdr_Rec In Mod_HDr_Cur Loop
         l_Status  := NULL;
         l_Message := NULL;
         --- Get Site USe ID
         IF NVL(Hdr_Rec.QL_QUALIFIER_CONTEXT,'NULL') = 'CUSTOMER' THEN
            IF (UPPER(Hdr_Rec.QL_QUALIFIER_ATTRIBUTE) = 'CUSTOMER' OR UPPER(Hdr_Rec.QL_QUALIFIER_ATTRIBUTE) = 'QUALIFIER_ATTRIBUTE32') THEN  -- Gopi Damuluri
                Hdr_Rec.QL_QUALIFIER_ATTRIBUTE := 'QUALIFIER_ATTRIBUTE32' ;
                For Cust_REc In Get_Cust_Cur (Hdr_Rec.QL_QUALIFIER_ATTR_VALUE) Loop
                    Hdr_Rec.QL_QUALIFIER_ATTRIBUTE_VALUE := Cust_REc.Cust_Account_ID ;
                End Loop;

                If Hdr_Rec.QL_QUALIFIER_ATTRIBUTE_VALUE Is NULL THEN
                   l_Status := 'E' ;
                   l_Message := 'Can not find Oracle Customer' ;
                Else
                   l_Status := 'V' ;
                   l_Message := NULL;
                ENd IF;

            ELSIF UPPER(Hdr_Rec.QL_QUALIFIER_ATTRIBUTE) = 'SHIP TO' THEN

                Hdr_Rec.QL_QUALIFIER_ATTRIBUTE := 'QUALIFIER_ATTRIBUTE11' ;
                For Site_REc In Get_Site_User_Cur(Hdr_Rec.QL_QUALIFIER_ATTR_VALUE) Loop
                    Hdr_Rec.QL_QUALIFIER_ATTRIBUTE_VALUE := Site_Rec.Site_Use_ID ;
                End Loop;

                If Hdr_Rec.QL_QUALIFIER_ATTRIBUTE_VALUE Is NULL THEN
                   l_Status := 'E' ;
                   l_Message := 'Can not find Oracle Customer Ship TO Party Site Number' ;
                Else
                   l_Status := 'V' ;
                   l_Message := NULL;
                ENd IF;
            END IF;
         ELSIF NVL(Hdr_Rec.QL_QUALIFIER_CONTEXT,'CUSTOMER') <> 'CUSTOMER' Then
            l_Status  := 'E' ;
            l_Message := 'Qualifier Context is not supported' ;
         ELSIF Hdr_Rec.QL_QUALIFIER_CONTEXT IS NULL THen
             l_Status := 'V' ;
             l_Message := NULL;
         End If;


         Update apps.XXWC_QP_MODIFIER_HDR_CNV
           Set Status                        = l_Status ,
               Error_Message                 = l_Message,
               QL_QUALIFIER_ATTRIBUTE        = Hdr_Rec.QL_QUALIFIER_ATTRIBUTE,
               QL_QUALIFIER_ATTRIBUTE_VALUE  = Hdr_Rec.QL_QUALIFIER_ATTRIBUTE_VALUE
         Where  List_Header_ID = Hdr_Rec.List_Header_ID
           AND QL_QUALIFIER_ATTR_VALUE = Hdr_Rec.QL_QUALIFIER_ATTR_VALUE;

         If NVL(l_Status,'E')  <> 'V' Then
            Update apps.XXWC_QP_MODIFIER_LINE_CNV
               Set Status           = NVL(l_Status,'E') ,
                   Error_Message    = 'Header Record validation failed'
            Where  List_Header_ID = Hdr_Rec.List_Header_ID ;

         End If;

      End Loop;

      -- Generate Values for Item, ITem Category and Formula If any one of them failes then
      -- Whole Modifier List should not be processed

      --------------------------------
      For Line_Rec In Mod_Line_Cur  Loop
         l_Status  := Null;
         l_Message := Null;
         --After Processing All Attributes of a Record , Insert into  Staging Table.
         If Line_Rec.PA_PRODUCT_ATTRIBUTE = 'Item Number' Then
            Line_Rec.ORACLE_PA_PRODUCT_ATTRIBUTE   :=  'PRICING_ATTRIBUTE1'  ;
            FOr Item_Rec In Get_Item_ID_Cur ( Line_Rec.PA_PRODUCT_ATTR_VALUE) Loop
               Line_Rec.ORACLE_PA_PRODUCT_ATTR_VALUE := Item_Rec.Inventory_Item_ID ;
            ENd Loop ;

            IF Line_Rec.ORACLE_PA_PRODUCT_ATTR_VALUE IS NULL THEN
                l_Status := 'E' ;
                l_Message := 'Invalid Item Number';
            ELSE
               l_Status := 'V' ;
               l_Message := NULL;
            END IF;

         Elsif Line_Rec.PA_PRODUCT_ATTRIBUTE = 'Item Category' Then
            Line_Rec.ORACLE_PA_PRODUCT_ATTRIBUTE  :=  'PRICING_ATTRIBUTE2'  ;
            FOr Cat_Rec In Get_Cat_ID_Cur ( Line_Rec.PA_PRODUCT_ATTR_VALUE ) Loop
                Line_Rec.ORACLE_PA_PRODUCT_ATTR_VALUE := Cat_Rec.Category_ID ;
            ENd Loop ;

            IF Line_Rec.ORACLE_PA_PRODUCT_ATTR_VALUE IS NULL THEN
                l_Status := 'E' ;
                l_Message := 'Invalid Item Category';
            ELSE
               l_Status := 'V' ;
               l_Message := NULL;
            END IF;
         Else
            l_Status := 'E' ;
            l_Message := 'Invalid Product Attribute';
         End IF;
         -- Validate Pricing Attribute iF value is provided
         If Line_Rec.PA_PRICING_ATTRIBUTE = 'Item Quantity' Then
            Line_Rec.ORACLE_PA_PRICING_ATTRIBUTE := 'PRICING_ATTRIBUTE10' ;
            Line_Rec.PA_PRICING_ATTRIBUTE_CONTEXT := 'VOLUME' ;
            If NVL(l_Status,'V') <> 'E' Then
               l_Status := 'V' ;
               l_Message := NULL;
            ENd If;
         ELSIF  Line_Rec.PA_PRICING_ATTRIBUTE IS NOT NULL
            AND Line_Rec.PA_PRICING_ATTRIBUTE <> 'Item Quantity' THEN
            l_Status := 'E' ;
            l_Message := l_Message || '-Invalid Pricing Attribute ';
         End If;

         -- Derive LN_ARITHMETIC_OPERATOR TYPE
         IF Line_Rec.LN_ARITHMETIC_OPERATOR_TYPE = 'Amount' Then
            Line_Rec.LN_ARITHMETIC_OPERATOR := 'AMT';
            If NVL(l_Status,'V') <> 'E' Then
               l_Status := 'V' ;
               l_Message := NULL;
            ENd If;
         Elsif Line_Rec.LN_ARITHMETIC_OPERATOR_TYPE  =  'Lumsum' THen
            Line_Rec.LN_ARITHMETIC_OPERATOR := 'LUMSUM';
            If NVL(l_Status,'V') <> 'E' Then
               l_Status := 'V' ;
               l_Message := NULL;
            ENd If;
         Elsif Line_Rec.LN_ARITHMETIC_OPERATOR_TYPE  = 'New Price' THen
            Line_Rec.LN_ARITHMETIC_OPERATOR := 'NEWPRICE';
            If NVL(l_Status,'V') <> 'E' Then
               l_Status := 'V' ;
               l_Message := NULL;
            ENd If;
         Elsif Line_Rec.LN_ARITHMETIC_OPERATOR_TYPE  = 'Percent' Then
            Line_Rec.LN_ARITHMETIC_OPERATOR := '%';
            If NVL(l_Status,'V') <> 'E' Then
               l_Status := 'V' ;
               l_Message := NULL;
            ENd If;
         Elsif Line_Rec.LN_ARITHMETIC_OPERATOR_TYPE IS NOT NULL Then
            l_Status := 'E' ;
            l_Message :=  l_Message || '-Invalid Arithmetic Operator Type ';
         End If;

         -- Satish U: 06-MAR-2012  -- Derive Forumla ID
         IF Line_Rec.LN_FORMULA Is Not Null THen
             Begin
                SELECT Price_Formula_ID
                Into Line_Rec.LN_PRICE_BY_FORMULA_ID
                FROM apps.QP_Price_Formulas_Vl
                WHERE Name = Line_Rec.LN_FORMULA;
             End ;
             If Line_Rec.LN_PRICE_BY_FORMULA_ID IS NULL THEN
                l_msg_data  := 'Invalid Pricing Formula' ;
                l_Status    := 'E' ;
             END IF;
         End If;


         -----------------------------Update Line Status -- IF Errors Then Update HDR Status too
         Update apps.XXWC_QP_MODIFIER_LINE_CNV
           Set Status                       = NVL(l_Status,'E') ,
               Error_Message                = l_Message ,
               ORACLE_PA_PRODUCT_ATTRIBUTE  = Line_Rec.ORACLE_PA_PRODUCT_ATTRIBUTE,
               ORACLE_PA_PRODUCT_ATTR_VALUE = Line_Rec.ORACLE_PA_PRODUCT_ATTR_VALUE,
               ORACLE_PA_PRICING_ATTRIBUTE  = Line_Rec.ORACLE_PA_PRICING_ATTRIBUTE,
               PA_PRICING_ATTRIBUTE_CONTEXT = Line_Rec.PA_PRICING_ATTRIBUTE_CONTEXT,
               LN_ARITHMETIC_OPERATOR       = Line_Rec.LN_ARITHMETIC_OPERATOR,
               LN_PRICE_BY_FORMULA_ID       = Line_Rec.LN_PRICE_BY_FORMULA_ID
         Where  List_Header_ID = Line_Rec.List_Header_ID
         ANd    LN_LIST_LINE_ID   = Line_Rec.LN_List_Line_ID;
         --- Satish U: 30 JAN 2012 : THis is No More required

      End Loop;

      --------------------------------
      --Update All The Lines to error status for a one line in Error status
      -- Satish U: 30-JAN-2012 : Following Statements are no more required


      Commit;
      --------------------------------
      For Modifier_Rec In Modifier_Cur Loop
         Begin
             l_PA_PRICING_ATTRIBUTE := NULL;
             L_PA_PRODUCT_ATTR_VALUE := NULL;
             l_PA_PRICING_ATTRIBUTE  := NULL;



             INSERT INTO apps.XXWC_QP_MODIFIER_INT (
                 BATCH_ID                      ,
                 LIST_HEADER_ID                ,
                 MODIFIER_TYPE                 ,  -- Added Modifier Type - Gopi
                 MODIFIER_LIST_NAME            ,
                 MODIFIER_LIST_NUMBER          ,
                 CURRENCY_CODE                 ,
                 LIST_TYPE_CODE                ,
                 START_DATE_ACTIVE             ,
                 END_DATE_ACTIVE               ,
                 GLOBAL_FLAG                   ,
                 AUTOMATIC_FLAG                ,
                 SOURCE_SYSTEM_CODE           ,
                 PTE_CODE                     ,
                 COMMENTS                     ,
                 QL_QUALIFIER_ID           ,
                 QL_ORIG_SYS_QUALIFIER_REF ,
                 QL_QUALIFIER_GROUPING_NO  ,
                 QL_QUALIFIER_CONTEXT      ,
                 QL_QUALIFIER_ATTRIBUTE    ,
                 QL_QUALIFIER_ATTR_VALUE       ,
                 QL_QUALIFIER_ATTR_VALUE_TO    ,
                 QL_COMPARISON_OPERATOR_CODE   ,
                 QL_QUALIFIER_PRECEDENCE ,
                 QL_ACTIVE_FLAG          ,
                 QL_LIST_TYPE_CODE        ,
                 LN_ORIG_SYS_LINE_REF          ,
                 LN_LIST_LINE_ID               ,
                 LN_LIST_LINE_NO               ,
                 LN_MODIFIER_LEVEL             ,
                 LN_MODIFIER_LEVEL_CODE        ,
                 LN_LIST_LINE_TYPE             ,
                 LN_LIST_LINE_TYPE_CODE        ,
                 LN_START_DATE_ACTIVE          ,
                 LN_END_DATE_ACTIVE            ,
                 LN_AUTOMATIC_FLAG             ,
                 LN_OVERRIDE_FLAG              ,
                 LN_PRICING_PHASE              ,
                 LN_PRICING_PHASE_ID           ,
                 LN_PRODUCT_PRECEDENCE         ,
                 LN_PRICE_BREAK_TYPE_CODE      ,
                 PA_PRODUCT_UOM_CODE           ,
                 LN_ARITHMETIC_OPERATOR_TYPE   ,
                 LN_ARITHMETIC_OPERATOR        ,
                 LN_OPERAND                    ,
                 LN_ACCRUAL_FLAG               ,
                 LN_INCOMPATIBILITY_GRP_CODE   ,
                 LN_PRICING_GROUP_SEQUENCE     ,
                 LN_INCLUDE_ON_RETURNS_FLAG    ,
                 LN_PRICE_BY_FORMULA_ID        ,
                 PA_PRICING_ATTRIBUTE_ID       ,
                 PA_ORIG_SYS_PRICING_ATTR_REF  ,
                 PA_PRODUCT_ATTRIBUTE_CONTEXT  ,
                 PA_PRODUCT_ATTRIBUTE          ,
                 PA_PRODUCT_ATTR_VALUE       ,
                 PA_PRICING_ATTRIBUTE          ,
                 PA_COMPARISON_OPERATOR_CODE   ,
                 PA_PRICING_ATTRIBUTE_CONTEXT  ,
                 PA_PRICING_ATTR_VALUE_FROM    ,
                 PA_PRICING_ATTR_VALUE_TO      ,
                 PA_EXCLUDER_FLAG              ,
                 PA_ATTRIBUTE_GROUPING_NO      ,
                 HDR_OPERATION                ,
                 LN_OPERATION                 ,
                 QL_OPERATION                 ,
                 PA_OPERATION                 ,
                 STATUS                        ,
                 CREATED_BY                    ,
                 CREATION_DATE                 ,
                 LAST_UPDATED_BY               ,
                 LAST_UPDATE_DATE              ,
                 LAST_UPDATE_LOGIN )
             VALUES ( Modifier_Rec.BATCH_ID  ,
                Modifier_Rec.LIST_HEADER_ID               ,
                Modifier_Rec.MODIFIER_TYPE                ,  -- Added Modifier Type - Gopi
                Modifier_Rec.MODIFIER_LIST_NAME           ,
                Modifier_Rec.MODIFIER_LIST_NUMBER         ,
                Modifier_Rec.CURRENCY_CODE                ,
                Modifier_Rec.LIST_TYPE_CODE               ,
                Modifier_Rec.START_DATE_ACTIVE            ,
                Modifier_Rec.END_DATE_ACTIVE              ,
                Modifier_Rec.GLOBAL_FLAG                  ,
                Modifier_Rec.AUTOMATIC_FLAG               ,
                Modifier_Rec.SOURCE_SYSTEM_CODE           ,
                Modifier_Rec.PTE_CODE                     ,
                Modifier_Rec.COMMENTS                     ,
                Modifier_Rec.QL_QUALIFIER_ID           ,
                Modifier_Rec.QL_ORIG_SYS_QUALIFIER_REF ,
                Modifier_Rec.QL_QUALIFIER_GROUPING_NO  ,
                Modifier_Rec.QL_QUALIFIER_CONTEXT      ,
                Modifier_Rec.QL_QUALIFIER_ATTRIBUTE    ,
                Modifier_Rec.QL_QUALIFIER_ATTRIBUTE_VALUE ,
                Modifier_Rec.QL_QUALIFIER_ATTR_VALUE_TO    ,
                Modifier_Rec.QL_COMPARISON_OPERATOR_CODE   ,
                Modifier_Rec.QL_QUALIFIER_PRECEDENCE ,
                Modifier_Rec.QL_ACTIVE_FLAG          ,
                Modifier_Rec.QL_LIST_TYPE_CODE        ,
                Modifier_Rec.LN_ORIG_SYS_LINE_REF         ,
                Modifier_Rec.LN_LIST_LINE_ID              ,
                Modifier_Rec.LN_LIST_LINE_NO              ,
                Modifier_Rec.LN_MODIFIER_LEVEL            ,
                Modifier_Rec.LN_MODIFIER_LEVEL_CODE       ,
                Modifier_Rec.LN_LIST_LINE_TYPE            ,
                Modifier_Rec.LN_LIST_LINE_TYPE_CODE       ,
                Modifier_Rec.LN_START_DATE_ACTIVE         ,
                Modifier_Rec.LN_END_DATE_ACTIVE           ,
                Modifier_Rec.LN_AUTOMATIC_FLAG            ,
                Modifier_Rec.LN_OVERRIDE_FLAG             ,
                Modifier_Rec.LN_PRICING_PHASE             ,
                Modifier_Rec.LN_PRICING_PHASE_ID          ,
                Modifier_Rec.LN_PRODUCT_PRECEDENCE        ,
                Modifier_Rec.LN_PRICE_BREAK_TYPE_CODE     ,
                Modifier_Rec.PA_PRODUCT_UOM_CODE          ,
                Modifier_Rec.LN_ARITHMETIC_OPERATOR_TYPE  ,
                Modifier_Rec.LN_ARITHMETIC_OPERATOR       ,
                Modifier_Rec.LN_OPERAND                   ,
                Modifier_Rec.LN_ACCRUAL_FLAG              ,
                Modifier_Rec.LN_INCOMPATIBILITY_GRP_CODE  ,
                Modifier_Rec.LN_PRICING_GROUP_SEQUENCE    ,
                Modifier_Rec.LN_INCLUDE_ON_RETURNS_FLAG   ,
                Modifier_Rec.LN_PRICE_BY_FORMULA_ID       ,
                Modifier_Rec.PA_PRICING_ATTRIBUTE_ID       ,
                Modifier_Rec.PA_ORIG_SYS_PRICING_ATTR_REF ,
                Modifier_Rec.PA_PRODUCT_ATTRIBUTE_CONTEXT ,
                Modifier_Rec.ORACLE_PA_PRODUCT_ATTRIBUTE         ,
                Modifier_Rec.ORACLE_PA_PRODUCT_ATTR_VALUE        ,
                Modifier_Rec.ORACLE_PA_PRICING_ATTRIBUTE         ,
                Modifier_Rec.PA_COMPARISON_OPERATOR_CODE  ,
                Modifier_Rec.PA_PRICING_ATTRIBUTE_CONTEXT ,
                Modifier_Rec.PA_PRICING_ATTR_VALUE_FROM   ,
                Modifier_Rec.PA_PRICING_ATTR_VALUE_TO     ,
                Modifier_Rec.PA_EXCLUDER_FLAG              ,
                Modifier_Rec.PA_ATTRIBUTE_GROUPING_NO      ,
                C_OPERATION,   -- HDR_OPERATIONS
                C_OPERATION,   -- LN_OPERATIONS
                C_OPERATION,   -- PA_OPERATIONS
                C_OPERATION,   -- QL_OPERATIONS
                C_STATUS_VALIDATE              ,
                l_user_id             ,
                Sysdate                        ,
                l_user_id             ,
                Sysdate                  ,
                l_login_id );
               -- Once Rows Are Successfully Processed Update the Status as Success.
                Update apps.XXWC_QP_MODIFIER_LINE_CNV Line
                    Set Status = C_STATUS_PROCESSED ,
                        ERROR_MESSAGE = Null
                Where Rowid = Modifier_Rec.Line_Rowid ;

                Update  apps.XXWC_QP_MODIFIER_HDR_CNV HDR
                Set Status = C_STATUS_PROCESSED ,
                    ERROR_MESSAGE = Null
                Where Rowid     = Modifier_Rec.Hdr_RowId ;
           Exception
             When Others Then
                RetCode  := 1 ;
                l_Msg_Data := SQLERRM;
                Update apps.XXWC_QP_MODIFIER_LINE_CNV Line
                    Set Status = C_STATUS_ERROR ,
                        ERROR_MESSAGE = Substr( ERROR_MESSAGE || l_Msg_Data, 1, 2000)
                Where Rowid = Modifier_Rec.Line_Rowid ;

                Update  apps.XXWC_QP_MODIFIER_HDR_CNV HDR
                Set Status = C_STATUS_ERROR ,
                    ERROR_MESSAGE = Substr(ERROR_MESSAGE || l_Msg_Data, 1, 2000)
                Where Rowid     = Modifier_Rec.Hdr_RowId ;


                --Where MODIFIER_LIST_NUMBER = Modifier_Rec.MODIFIER_LIST_NUMBER ;
                Fnd_File.Put_Line ( Fnd_File.LOG, '280 When Others Exception in side the Loop =>' || Modifier_Rec.MODIFIER_LIST_NUMBER);
           End;
      End Loop;

      -- Call Concurrent Program to
      Fnd_File.put_line(Fnd_File.LOG,'Submitting Modifer List Import program' );
      l_Modifier_import_req_id  := 0;
      l_Modifier_import_req_id := Fnd_Request.SUBMIT_REQUEST
            ('XXWC'                         --application IN VARCHAR2 DEFAULT NULL
            ,'XXWC_MODIFIER_LIST_IMPORT'                       --program     IN VARCHAR2 DEFAULT NULL
            ,'XXWC Modifier List Import Program'   --description IN VARCHAR2 DEFAULT NULL
            ,NULL                           --start_time  IN VARCHAR2 DEFAULT NULL
            ,FALSE                          --sub_request IN BOOLEAN  DEFAULT FALSE
            ,NULL                      --P_Batch ID
            ,NULL      );                      --P_List_Header_ID

      Fnd_File.put_line(Fnd_File.LOG,'Request Id for the Modifier List Import program is:'||l_Modifier_import_req_id);
      COMMIT;
      --
      -- Checking the Request Id wether its successfully submitted or not
      --
     IF l_Modifier_import_req_id = 0 THEN  -- For Checking the Oracle Import Request Id
        Fnd_File.put_line(Fnd_File.LOG,'Modifier List Import Program failed. Please review the log file for the error message' );
        retcode := 1;
     ELSE
        Fnd_File.put_line(Fnd_File.LOG, 'Checking if the Order import is complete');
        l_request_status := Fnd_Concurrent.WAIT_FOR_REQUEST
             (request_id  => l_Modifier_import_req_id,
              INTERVAL    => l_interval,
              max_wait    => l_max_wait,
              phase       => l_phase,
              status      => l_status,
              dev_phase   => l_dev_phase,
              dev_status  => l_dev_status,
              message     => l_message   );
        Fnd_File.put_line(Fnd_File.LOG,'Development Phase for Modifier List Import is:'||l_dev_phase );
        Fnd_File.put_line(Fnd_File.LOG,'Development Phase for Modifier List Import is:'||l_dev_status );


          IF l_request_status THEN   -- Checking the Return Status for Order Import
             IF l_dev_phase  = 'COMPLETE' AND
                l_dev_status = 'NORMAL' THEN
                Fnd_File.put_line(Fnd_File.LOG,'Modifer List Import is Completed normally' );
             Else
                 Fnd_File.put_line(Fnd_File.LOG,'Modifer List Import program did not complete successfully' );
             End If;

          End If;
      End If;
   EXCEPTION
      When Others Then

         Fnd_File.Put_Line ( Fnd_File.LOG, '299 When Others Exception =>' || l_Msg_Data);
         l_err_callpoint := '210 :  When Others Exception :' ;
         l_Return_Status := 'U';
         Fnd_File.Put_Line ( Fnd_File.LOG, 'WHen Others Exception :' || SQLERRM);

         errbuf := SQLERRM;
         retcode := 2;
         ROLLBACK;

         FND_FILE.Put_Line(FND_FILE.Log, SQLERRM);
         FND_FILE.Put_Line(FND_FILE.Output, SQLERRM);

         XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API(p_called_from      => G_MODULE_NAME,
                                             p_calling           => l_err_callpoint,
                                             p_ora_error_msg     => SQLERRM,
                                             p_error_desc        => 'When Others Exception XXWC_QP_MODIFIER_WEB_ADI_PKG.BULK_IMPORT',
                                             p_distribution_list => l_distro_list,
                                             p_module            => 'OZF');
   End  Bulk_Import ;

  -- Procedure to validate List Lines  values.
  -- Satish U # 20130322-01259 :  Performance Tuning.
  --  Fine tune the logic that validates Date Overlap/Price Break Overlap

   Procedure Validate_list_Lines (p_Header_Id      In NUMBER ,
                                  X_Return_Status  OUT Varchar2 ) Is

      l_Line_Count               Number ;
      l_UOM_Match                Varchar2(1) := 'N';
      l_Start_Date_Match         Varchar2(1) := 'N';
      l_End_Date_Match           Varchar2(1) := 'N';
      l_Dates_Overlap_Flag       Varchar2(1) := 'N';
      l_PriceBreak_Overlap_Flag  Varchar2(1) := 'N';
      l_From_Qty_Match           Varchar2(1) := 'N';
      l_To_Qty_Match             Varchar2(1) := 'N';
      l_LINE_ID_Match            Varchar2(1) := 'N';
      l_Mod_name                 Varchar2(80) := 'VALIDATE_LIST_LINES';
      l_Msg_Data                 Varchar2(240) ;
      L_KNOWN_ERROR              Varchar2(1)  := 'N';
      l_PA_PRODUCT_ATTR_VALUE_ITEM     Varchar2(30);
      L_PA_PRODUCT_ATTR_VALUE_CAT      Varchar2(30) ;
      l_PA_PRODUCT_ATTR_VALUE          Varchar2(30);
      l_Item_Description               Varchar2(80) ;

      C_STATUS_VALIDATE          CONSTANT   VARCHAR2(1) := 'V';
      C_STATUS_ERROR             CONSTANT   VARCHAR2(1) := 'E';
      C_STATUS_PROCESSED         CONSTANT   VARCHAR2(1) := 'P' ;
      C_STATUS_VALIDATE          Constant   Varchar2(1) := 'V' ;
      C_STATUS_ERROR             Constant   Varchar2(1) := 'E' ;
      C_START_DATE               Constant   Date        :=  ADD_MONTHS(SYSDATE, -120) ;
      C_END_DATE                 Constant    Date        :=  ADD_MONTHS(SYSDATE, 120) ;

      XXWC_EXCEPTION             Exception ;

       -- Cursor to get the Modifier Lines
      Cursor  Get_Mod_Line_Cur (P_List_Header_ID Number ) IS
         Select --Distinct
             LIST_HEADER_ID                ,
             MODIFIER_LIST_NUMBER          ,
             LN_ORIG_SYS_LINE_REF          ,
             LN_LIST_LINE_ID               ,
             LN_LIST_LINE_NO               ,
             LN_MODIFIER_LEVEL             ,
             LN_MODIFIER_LEVEL_CODE        ,
             LN_LIST_LINE_TYPE             ,
             LN_LIST_LINE_TYPE_CODE        ,
             LN_START_DATE_ACTIVE          ,
             LN_END_DATE_ACTIVE            ,
             LN_AUTOMATIC_FLAG             ,
             LN_OVERRIDE_FLAG              ,
             LN_PRICING_PHASE              ,
             LN_PRICING_PHASE_ID           ,
             LN_PRODUCT_PRECEDENCE         ,
             LN_PRICE_BREAK_TYPE_CODE      ,
             --LN_PRODUCT_UOM_CODE           ,
             LN_ARITHMETIC_OPERATOR_TYPE   ,
             LN_ARITHMETIC_OPERATOR        ,
             LN_OPERAND                    ,
             LN_ACCRUAL_FLAG      ,
             LN_OPERATION     ,
             LN_INCOMPATIBILITY_GRP_CODE,
             LN_PRICING_GROUP_SEQUENCE ,
             LN_INCLUDE_ON_RETURNS_FLAG ,
             LN_PRICE_BY_FORMULA_ID  ,
             PA_PRODUCT_ATTRIBUTE ,
             PA_PRODUCT_ATTR_VALUE,
             PA_PRODUCT_ATTRIBUTE_CONTEXT,
             PA_PRODUCT_UOM_CODE,
             PA_PRICING_ATTR_VALUE_FROM,
             PA_PRICING_ATTR_VALUE_TO
         From apps.XXWC_QP_MODIFIER_INT
         Where List_Header_ID  = P_List_Header_ID
         --AND PA_PRODUCT_ATTRIBUTE = 'PRICING_ATTRIBUTE2'
         And   NVL(Status,'NULL') <> C_STATUS_PROCESSED
         AND PA_PRODUCT_ATTR_VALUE IS NOT NULL
         Order By LN_LIST_LINE_ID ;

      -- CUrosr to get all the rows from stagign table  that match 'Modifier List Number', Item Number'
      -- Added Parameter P_LN_LIST_LINE_ID : Satish U : 04-DEC-2012
      -- SatishU : 21-MAR-2013 : Added P_LIST_HEADER_ID and LN_END_DATE_ACTIVE
      Cursor Get_Modifier_Cur ( P_MODIFIER_LIST_NUMBER          Varchar2 ,
                                P_PA_PRODUCT_ATTRIBUTE_CONTEXT  Varchar2,
                                P_PA_PRODUCT_ATTRIBUTE          Varchar2,
                                P_PA_PRODUCT_ATTR_VALUE         Varchar2,
                                P_LN_LIST_LINE_ID               Number,
                                P_LIST_HEADER_ID                Number,
                                P_LN_END_DATE_ACTIVE            Date) Is
         Select qmod.ROWID, QMOD.*
         From apps.XXWC_QP_MODIFIER_INT  qmod
         Where MODIFIER_LIST_NUMBER         = P_MODIFIER_LIST_NUMBER
         and   List_Header_Id               = P_LIST_HEADER_ID
         And  PA_PRODUCT_ATTRIBUTE_CONTEXT  = P_PA_PRODUCT_ATTRIBUTE_CONTEXT
         And  PA_PRODUCT_ATTRIBUTE          = P_PA_PRODUCT_ATTRIBUTE
         And  PA_PRODUCT_ATTR_VALUE         = P_PA_PRODUCT_ATTR_VALUE
         AND  NVL(LN_END_DATE_ACTIVE, SYSDATE) >= P_LN_END_DATE_ACTIVE
         AND  (P_LN_LIST_LINE_ID IS NULL OR P_LN_LIST_LINE_ID <> LN_LIST_LINE_ID )
         And  NVL(Status,'R') <> 'P';

       -- CUrosr to get all the rows from stagign table  that match 'Modifier List Number', Item Number'

      -- CUrosr to get all the rows from Base table  that match 'Modifier List Number', Item Number'
      -- Added Parameter P_LN_LIST_LINE_ID : Satish U : 04-DEC-2012
      -- Commented Cursor Definition Will be using New Cursor Definition
      -- -- Satish U # 20130322-01259 :  Performance Tuning.  Using a Global Temp Table
     /****************************Satish U: 22-MAR-2013 *************************
     Cursor Get_Modifier2_Cur ( P_MODIFIER_LIST_NUMBER          Varchar2 ,
                                P_PA_PRODUCT_ATTRIBUTE_CONTEXT  Varchar2,
                                P_PA_PRODUCT_ATTRIBUTE          Varchar2,
                                P_PA_PRODUCT_ATTR_VALUE_ITEM    Varchar2,
                                P_PA_PRODUCT_ATTR_VALUE_CAT     Varchar2,
                                P_LN_LIST_LINE_ID               Number,
                                P_LIST_HEADER_ID                Number,
                                p_LN_END_DATE_ACTIVE            DATE) Is
         Select QMOD.*
         From XXWC_QP_MODIFIERS_LIST_VW  qmod
         Where MODIFIER_LIST_NUMBER  = P_MODIFIER_LIST_NUMBER
         and   List_Header_ID        = P_LIST_HEADER_ID
         And  PA_PRODUCT_ATTRIBUTE_CONTEXT  = P_PA_PRODUCT_ATTRIBUTE_CONTEXT
         And  PA_PRODUCT_ATTRIBUTE          = P_PA_PRODUCT_ATTRIBUTE
         and  NVL(LN_END_DATE_ACTIVE, SYSDATE) >= P_LN_END_DATE_ACTIVE
        -- AND  (P_LN_LIST_LINE_ID IS NULL OR P_LN_LIST_LINE_ID <> LN_LIST_LINE_ID )
         AND ( (PA_PRODUCT_ATTRIBUTE        = 'Item Number'
           And  NVL(PA_PRODUCT_ATTR_VALUE_ITEM,'NULL')  = NVL(P_PA_PRODUCT_ATTR_VALUE_ITEM, 'NULL') ) Or
           ( PA_PRODUCT_ATTRIBUTE            = 'Item Category'
           And  NVL(PA_PRODUCT_ATTR_VALUE_CAT,'NULL')   = NVL(P_PA_PRODUCT_ATTR_VALUE_CAT,'NULL') )
           OR (PA_PRODUCT_ATTRIBUTE            = 'All Items' ));
       **************************************************************************/
     -- Satish U # 20130322-01259 :  Performance Tuning.  Cursor will get informration from
     -- Global Temp Table
     Cursor Get_Modifier2_Cur ( P_MODIFIER_LIST_NUMBER          Varchar2 ,
                                P_PA_PRODUCT_ATTRIBUTE_CONTEXT  Varchar2,
                                P_PA_PRODUCT_ATTRIBUTE          Varchar2,
                                P_PA_PRODUCT_ATTR_VALUE_ITEM    Varchar2,
                                P_PA_PRODUCT_ATTR_VALUE_CAT     Varchar2,
                                P_LN_LIST_LINE_ID               Number,
                                P_LIST_HEADER_ID                Number,
                                p_LN_END_DATE_ACTIVE            DATE) Is
         Select QMOD.*
         From apps.XXWC_QP_MODIFIER_INT_GT  qmod
         Where MODIFIER_LIST_NUMBER  = P_MODIFIER_LIST_NUMBER
         and   List_Header_ID        = P_LIST_HEADER_ID
         And  PA_PRODUCT_ATTRIBUTE_CONTEXT  = P_PA_PRODUCT_ATTRIBUTE_CONTEXT
         And  PA_PRODUCT_ATTRIBUTE          = P_PA_PRODUCT_ATTRIBUTE
         --and  NVL(LN_END_DATE_ACTIVE, SYSDATE) >= P_LN_END_DATE_ACTIVE
         AND   P_LN_LIST_LINE_ID <> LN_LIST_LINE_ID
         AND ( (PA_PRODUCT_ATTRIBUTE        = 'Item Number'
           And  NVL(PA_PRODUCT_ATTR_VALUE,'NULL')  = NVL(P_PA_PRODUCT_ATTR_VALUE_ITEM, 'NULL') ) Or
           ( PA_PRODUCT_ATTRIBUTE            = 'Item Category'
           And  NVL(PA_PRODUCT_ATTR_VALUE,'NULL')   = NVL(P_PA_PRODUCT_ATTR_VALUE_CAT,'NULL') )
           OR (PA_PRODUCT_ATTRIBUTE            = 'All Items' ));



     -- Cursor to get From Qty and To Qty breaks for a given List Line Id from
     Cursor Get_Qty_Breaks_Cur (p_List_Line_ID Number ) Is
        Select   PA_PRICING_ATTR_VALUE_FROM, PA_PRICING_ATTR_VALUE_TO
        From apps.XXWC_QP_MODIFIER_INT
        Where LN_List_Line_Id = p_List_Line_ID ;

     -- Satish U : 22-MAR-2013  # 20130322-01259 :  Performance Tuning.
     -- CUrosr to get all the rows from Base table  that match 'Modifier List Number', Item Number'

     Cursor Get_Modifier3_Cur ( P_LIST_HEADER_ID          Number  ) IS
         Select QMOD.*
         From apps.XXWC_QP_MODIFIERS_LIST_VW  qmod
         Where List_Header_ID   = P_LIST_HEADER_ID
         And LN_LIST_LINE_ID    = LN_LIST_LINE_ID
         And  NVL(LN_END_DATE_ACTIVE, SYSDATE +1 ) >= SYSDATE
         AND PA_PRODUCT_ATTRIBUTE   In ( 'Item Number' , 'Item Category', 'All Items' ) ;

   Begin
      -- Initialize the Return Status to Success
      -- Satish U # 20130322-01259 :  Performance Tuning.
      X_Return_Status :=  G_RETURN_SUCCESS ;
      -- Check if Rows Exist in Global Temp Table if not insert Records
      Begin
         Select Count(*)
         Into l_Line_Count
         From apps.XXWC_QP_MODIFIER_INT_GT
         Where LIST_HEADER_ID = P_HEADER_ID  ;

         If l_Line_Count = 0 Then
           -- Satish U # 20130322-01259 :  Performance Tuning.
           Fnd_File.Put_Line ( Fnd_File.LOG, '500 Begining of API =>' || l_mod_name);

           For Mod_Rec In Get_Modifier3_Cur ( P_LIST_HEADER_ID   =>  P_HEADER_ID )  LOOP
              Begin
                   -- Insert Records Into Global Temp Table
                   If Mod_Rec.PA_PRODUCT_ATTRIBUTE =  'Item Category' Then
                      l_PA_PRODUCT_ATTR_VALUE := Mod_Rec.PA_PRODUCT_ATTR_VALUE_CAT ;
                   Else
                      l_PA_PRODUCT_ATTR_VALUE := Mod_Rec.PA_PRODUCT_ATTR_VALUE_ITEM ;
                   End IF;
                   Insert Into apps.XXWC_QP_MODIFIER_INT_GT (
                      ---BATCH_ID                      ,
                     LIST_HEADER_ID                ,
                     MODIFIER_LIST_NAME            ,
                     MODIFIER_LIST_NUMBER          ,
                     CURRENCY_CODE                 ,
                     LIST_TYPE_CODE                ,
                     START_DATE_ACTIVE             ,
                     END_DATE_ACTIVE               ,
                     GLOBAL_FLAG                   ,
                     AUTOMATIC_FLAG                ,
                     LN_ORIG_SYS_LINE_REF          ,
                     LN_LIST_LINE_ID               ,
                     LN_LIST_LINE_NO               ,
                     LN_MODIFIER_LEVEL             ,
                   --  LN_MODIFIER_LEVEL_CODE        ,
                     LN_LIST_LINE_TYPE             ,
                   --  LN_LIST_LINE_TYPE_CODE        ,
                     LN_START_DATE_ACTIVE          ,
                     LN_END_DATE_ACTIVE            ,
                     LN_AUTOMATIC_FLAG             ,
                     LN_OVERRIDE_FLAG              ,
                     LN_PRICING_PHASE              ,
                  --   LN_PRICING_PHASE_ID           ,
                     LN_PRODUCT_PRECEDENCE         ,
                     LN_PRICE_BREAK_TYPE_CODE      ,
                     PA_PRODUCT_UOM_CODE           ,
                     LN_ARITHMETIC_OPERATOR_TYPE   ,
                    -- LN_ARITHMETIC_OPERATOR        ,
                     LN_OPERAND                    ,
                     LN_ACCRUAL_FLAG               ,
                     LN_INCOMPATIBILITY_GRP_CODE   ,
                     LN_PRICING_GROUP_SEQUENCE     ,
                     LN_INCLUDE_ON_RETURNS_FLAG    ,
                   --  LN_PRICE_BY_FORMULA_ID        ,
                     PA_PRICING_ATTRIBUTE_ID       ,
                     PA_ORIG_SYS_PRICING_ATTR_REF  ,
                     PA_PRODUCT_ATTRIBUTE_CONTEXT  ,
                     PA_PRODUCT_ATTRIBUTE     ,
                     PA_PRODUCT_ATTR_VALUE       ,
                     PA_PRICING_ATTRIBUTE          ,
                     PA_COMPARISON_OPERATOR_CODE   ,
                     PA_PRICING_ATTRIBUTE_CONTEXT  ,
                     PA_PRICING_ATTR_VALUE_FROM    ,
                     PA_PRICING_ATTR_VALUE_TO      ,
                     PA_EXCLUDER_FLAG              ,
                     PA_ATTRIBUTE_GROUPING_NO      )
                  VALUES (
                     Mod_Rec.LIST_HEADER_ID        ,
                     Mod_Rec.MODIFIER_LIST_NAME    ,
                     Mod_Rec.MODIFIER_LIST_NUMBER  ,
                     Mod_Rec.CURRENCY_CODE         ,
                     Mod_Rec.LIST_TYPE_CODE        ,
                     Mod_Rec.START_DATE_ACTIVE     ,
                     Mod_Rec.END_DATE_ACTIVE       ,
                     Mod_Rec.GLOBAL_FLAG           ,
                     Mod_Rec.AUTOMATIC_FLAG        ,
                     Mod_Rec.LN_ORIG_SYS_LINE_REF  ,
                     Mod_Rec.LN_LIST_LINE_ID       ,
                     Mod_Rec.LN_LIST_LINE_NO       ,
                     Mod_Rec.LN_MODIFIER_LEVEL     ,
                   --  Mod_Rec.LN_MODIFIER_LEVEL_CODE        ,
                     Mod_Rec.LN_LIST_LINE_TYPE             ,
                    -- Mod_Rec.LN_LIST_LINE_TYPE_CODE        ,
                     Mod_Rec.LN_START_DATE_ACTIVE          ,
                     Mod_Rec.LN_END_DATE_ACTIVE            ,
                     Mod_Rec.LN_AUTOMATIC_FLAG             ,
                     Mod_Rec.LN_OVERRIDE_FLAG              ,
                     Mod_Rec.LN_PRICING_PHASE              ,
                    -- Mod_Rec.LN_PRICING_PHASE_ID           ,
                     Mod_Rec.LN_PRODUCT_PRECEDENCE         ,
                     Mod_Rec.LN_PRICE_BREAK_TYPE_CODE      ,
                     Mod_Rec.PA_PRODUCT_UOM_CODE           ,
                     Mod_Rec.LN_ARITHMETIC_OPERATOR_TYPE   ,
                    -- Mod_Rec.LN_ARITHMETIC_OPERATOR        ,
                     Mod_Rec.LN_OPERAND                    ,
                     Mod_Rec.LN_ACCRUAL_FLAG               ,
                     Mod_Rec.LN_INCOMPATIBILITY_GRP_CODE   ,
                     Mod_Rec.LN_PRICING_GROUP_SEQUENCE     ,
                     Mod_Rec.LN_INCLUDE_ON_RETURNS_FLAG    ,
                   --  Mod_Rec.LN_PRICE_BY_FORMULA_ID        ,
                     Mod_Rec.PA_PRICING_ATTRIBUTE_ID       ,
                     Mod_Rec.PA_ORIG_SYS_PRICING_ATTR_REF  ,
                     Mod_Rec.PA_PRODUCT_ATTRIBUTE_CONTEXT  ,
                     Mod_Rec.PA_PRODUCT_ATTRIBUTE          ,
                     l_PA_PRODUCT_ATTR_VALUE         ,
                     Mod_Rec.PA_PRICING_ATTRIBUTE          ,
                     Mod_Rec.PA_COMPARISON_OPERATOR_CODE   ,
                     Mod_Rec.PA_PRICING_ATTRIBUTE_CONTEXT  ,
                     Mod_Rec.PA_PRICING_ATTR_VALUE_FROM    ,
                     Mod_Rec.PA_PRICING_ATTR_VALUE_TO      ,
                     Mod_Rec.PA_EXCLUDER_FLAG              ,
                     Mod_Rec.PA_ATTRIBUTE_GROUPING_NO
                     );
              Exception
                 When Others Then
                    X_Return_Status := G_RETURN_UNEXP_ERROR;
                    Fnd_File.Put_Line ( Fnd_File.LOG, '500.2 When Others Exception : While Inserting Records into Global Temp Table ' );
                    Fnd_File.Put_Line ( Fnd_File.LOG, '500.3 Modifier List Line No. : ' || Mod_Rec.LN_LIST_LINE_NO);
                    Fnd_File.Put_Line ( Fnd_File.LOG, '500.3 Error Message : ' || Substr(SqlErrM, 1,250));
              End;
           End Loop ;

           Select Count(*)
           Into l_Line_Count
           From apps.XXWC_QP_MODIFIER_INT_GT
           Where LIST_HEADER_ID = P_HEADER_ID  ;

           Fnd_File.Put_Line ( Fnd_File.LOG, '500.1 Number of Rows populated into Global Temp Table =>' || l_Line_Count);
         End If;
      End ;
      Fnd_File.Put_Line ( Fnd_File.LOG, '500.10 Validating rows in Modifier Staging table ');
      --Fnd_File.Put_Line ( Fnd_File.OUTPUT, '500.10 Validating rows in Modifier Staging table ');
      -- Go through the loop and validate Modifer List Lines for Overlapping quantities
      For Line_Rec in Get_Mod_Line_Cur (P_List_Header_ID => P_Header_ID )  Loop
         Begin
             --Fnd_File.Put_Line ( Fnd_File.LOG, '501 Inside Loop First Cursor Get_Mod_line_Cur =>' );
             -- 05JUN2012 : Check if Line exists in interface table
             l_Line_Count := 0;
             -- Satish U: Added List-Header_ID and ln_End_Date_Active to sql query
             --SatishU: 21-MAR-2013
             Select Count(*)
             Into l_Line_Count
             From apps.XXWC_QP_MODIFIER_INT
             Where MODIFIER_LIST_NUMBER         = Line_Rec.MODIFIER_LIST_NUMBER
             and  list_header_Id                = Line_Rec.List_Header_ID
             And  PA_PRODUCT_ATTRIBUTE_CONTEXT  = Line_Rec.PA_PRODUCT_ATTRIBUTE_CONTEXT
             And  PA_PRODUCT_ATTRIBUTE          = Line_Rec.PA_PRODUCT_ATTRIBUTE
             And  PA_PRODUCT_ATTR_VALUE         = Line_Rec.PA_PRODUCT_ATTR_VALUE
             AND  List_Header_ID                = Line_Rec.List_Header_ID
             AND  NVL(LN_END_DATE_ACTIVE, SYSDATE) >= Line_Rec.LN_END_DATE_ACTIVE
             And  NVL(Status,'R') <> 'P';

             l_Item_Description :=  Get_Product_Attr_Value (
                   p_attribute_name   => Line_Rec.PA_PRODUCT_ATTRIBUTE,
                   p_attr_Code        => Line_Rec.PA_PRODUCT_ATTR_VALUE );

             If   Line_Rec.PA_PRODUCT_ATTRIBUTE =  'PRICING_ATTRIBUTE1' Then
                l_Item_Description := 'Item Number : ' || l_Item_Description ;
             Else
                l_Item_Description := 'Item Category :' || l_Item_Description ;
             End If;


             If L_Line_Count > 1 Then  --  Duplicate Lines Check 100
                 Fnd_File.Put_Line ( Fnd_File.LOG, '502 Line Count is =>' || l_Line_Count );
                 -- SatishU : 06-JUL-2012 : Check if  Start Date and End Dates are overlapping and if UOM Code is same.
                 -- IF UOM Code is different or if Start Date and End Date values  do not overlap then Do not Delete the Lines.

                 For  Get_Mod_Rec In Get_Modifier_Cur (
                                    P_MODIFIER_LIST_NUMBER          => Line_Rec.MODIFIER_LIST_NUMBER ,
                                    P_PA_PRODUCT_ATTRIBUTE_CONTEXT  => Line_Rec.PA_PRODUCT_ATTRIBUTE_CONTEXT,
                                    P_PA_PRODUCT_ATTRIBUTE          => Line_Rec.PA_PRODUCT_ATTRIBUTE,
                                    P_PA_PRODUCT_ATTR_VALUE         => Line_Rec.PA_PRODUCT_ATTR_VALUE ,
                                    P_LN_LIST_LINE_ID               => Line_Rec.LN_LIST_LINE_ID,
                                    P_LIST_HEADER_ID                => Line_Rec.List_Header_ID,
                                    P_LN_END_DATE_ACTIVE            => Line_Rec.LN_End_Date_Active) Loop
                    Begin
                        l_UOM_Match               := 'N';
                        l_Start_Date_Match        := 'N';
                        l_End_Date_Match          := 'N';
                        l_Dates_Overlap_Flag      := 'N';
                        l_PriceBreak_Overlap_Flag := 'N';
                        l_LINE_ID_Match           := 'N' ;
                        l_From_Qty_Match          := 'N';
                        l_To_Qty_Match            := 'N';
                        Fnd_File.Put_Line ( Fnd_File.LOG, '503 Inside Second Loop : Get Modifier Cursor  =>' || l_Line_Count );
                        -- Check if UOM Matches
                        If NVL(Get_Mod_Rec.PA_PRODUCT_UOM_CODE,'NULL')  = NVL(Line_Rec.PA_PRODUCT_UOM_CODE,'NULL') Then
                           l_UOM_Match := 'Y';
                        ENd If;

                         -- Check If Effectivity Dates Match
                        If   ( NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) = NVL(Line_Rec.LN_START_DATE_ACTIVE , C_END_DATE ))  Then
                           l_Start_Date_Match := 'Y';
                        End If;


                        -- Check If Effectivity Dates Match
                        If   ( NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_START_DATE) = NVL(Line_Rec.LN_END_DATE_ACTIVE , C_END_DATE ))  Then
                           l_End_Date_Match := 'Y';
                        End If;

                        -- Check if Dates Overlap
                        IF  NVL(Line_Rec.LN_START_DATE_ACTIVE, C_START_DATE)
                               Between  NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) And NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_END_DATE)  Then
                            l_Dates_Overlap_Flag := 'Y';
                        End If;

                        -- Check if Dates Overlap
                        IF  NVL(Line_Rec.LN_END_DATE_ACTIVE, C_END_DATE)
                               Between  NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) And NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_END_DATE)  Then
                            l_Dates_Overlap_Flag := 'Y';
                        End If;

                        --IF  (Line_Rec.PA_PRICING_ATTR_VALUE_FROM IS NOT NULL) AND (Line_Rec.PA_PRICING_ATTR_VALUE_FROM
                        --   Between  NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM, 1) And NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO, 999999))  Then
                        --   l_PriceBreak_Overlap_Flag := 'Y';
                        --End If;

                        --Satish U: 12-13-2012 : Added P_PA_PRICING_ATTR_VALUE_FROM != 0
                        Begin
                            CASE
                               WHEN TO_NUMBER(NVL(Line_Rec.PA_PRICING_ATTR_VALUE_FROM,1))  Between To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM ,1))
                                  And To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999)) THEN
                                  l_PriceBreak_Overlap_Flag := 'Y';
                                  l_Msg_Data := '504.1 Price Break Overlap Flag is Set Y =>' ;
                                  Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                                  --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                               WHEN To_Number(NVL(Line_Rec.PA_PRICING_ATTR_VALUE_TO,99999))   Between To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM,1))
                                  And To_Number(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999))  THEN
                                  l_PriceBreak_Overlap_Flag := 'Y';
                                  l_Msg_Data :=  '505.1 Price Break Overlap Flag is Set Y =>' ;
                                  Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                                  --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                               ELSE
                                 l_PriceBreak_Overlap_Flag := 'N';
                            End CASE;
                        Exception
                           When INVALID_NUMBER Then
                              CASE
                                 WHEN NVL(Line_Rec.PA_PRICING_ATTR_VALUE_FROM,1)  Between NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM ,1)
                                    And NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999) THEN
                                    l_PriceBreak_Overlap_Flag := 'Y';
                                    l_Msg_Data := '504.2 Price Break Overlap Flag is Set Y =>' ;
                                    Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                                    --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                                 WHEN NVL(Line_Rec.PA_PRICING_ATTR_VALUE_TO,99999)   Between NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM,1)
                                    And NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999)  THEN
                                    l_PriceBreak_Overlap_Flag := 'Y';
                                    l_Msg_Data :=  '505.2 Price Break Overlap Flag is Set Y =>' ;
                                    Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                                    --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                                 ELSE
                                    l_PriceBreak_Overlap_Flag := 'N';
                              End CASE;
                           When Others Then
                              l_Msg_Data := Substr(SQLERRM, 1,200) ;
                              RAISE XXWC_EXCEPTION ;
                        End ;

                        -- Check if List_Line_ID matches Too
                        If NVL(Line_Rec.LN_LIST_LINE_ID, -1) = NVL(Get_Mod_Rec.LN_LIST_LINE_ID, -1) Then
                           l_LINE_ID_Match := 'Y' ;
                        END IF ;

                         -- Check if From Qty Matches
                        If NVL(Line_Rec.PA_PRICING_ATTR_VALUE_FROM, -1) = NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM, -1) Then
                           l_From_Qty_Match := 'Y' ;
                        END IF ;

                        -- Check if To Qty Matches
                        If NVL(Line_Rec.PA_PRICING_ATTR_VALUE_TO, -1) = NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO, -1) Then
                           l_To_Qty_Match := 'Y' ;
                        END IF ;

                        ----===================================================================---------------------------------------
                        -- Validate if List_Line ID Matches.
                        If l_LINE_ID_Match = 'Y' Then
                           -- Do nothing as this line will be udpated
                           NULL;
                           Fnd_File.Put_Line ( Fnd_File.LOG, '506 Line ID Matches Do Nothing =>'  );
                        ELSIF l_LINE_ID_Match = 'N'  AND l_UOM_Match = 'Y' Then
                           -- Check if Other attributes Match
                           --AND l_PriceBreak_Overlap_Flag = 'N' AND l_Dates_Overlap_Flag = 'N' AND l_End_Date_Match = 'Y'
                           --AND l_Start_Date_Match = 'Y' AND l_UOM_Match = 'Y' Then
                           If l_Dates_Overlap_Flag = 'Y' AND ( l_Start_Date_Match = 'N' OR l_End_Date_Match = 'N') Then
                              l_Msg_Data     := 'Modifier List Line :' || Line_rec.LN_LIST_LINE_NO || l_Item_Description ||
                                  ' Start Date , End Date values overlap with another List Line .' || Get_Mod_Rec.LN_List_Line_NO ;
                              Fnd_File.put_line(Fnd_File.LOG, l_Msg_Data );
                              Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                              l_KNOWN_ERROR  := 'Y' ;
                              --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
                              Fnd_File.Put_Line ( Fnd_File.LOG, '510 Raise XXWC Exception =>'  );
                              Raise XXWC_EXCEPTION;
                           Elsif l_PriceBreak_Overlap_Flag = 'Y' AND ( l_From_Qty_Match = 'N' OR l_To_Qty_Match = 'N') Then
                              l_Msg_Data     := 'Modifier List Line :' || Line_rec.LN_LIST_LINE_NO || l_Item_Description ||
                                  ' From Qty and To Qty overlap with another List Line .' || Get_Mod_Rec.LN_List_Line_NO ;
                              Fnd_File.put_line(Fnd_File.LOG, l_Msg_Data );
                              Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                              l_KNOWN_ERROR  := 'Y' ;
                              --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
                              Fnd_File.Put_Line ( Fnd_File.LOG, '511 Raise XXWC Exception =>'  );
                              Raise XXWC_EXCEPTION;
                           End If;
                           -- Ever thing matches but The List Line ID
                           ---IF P_LN_LIST_LINE_ID IS NULL AND Get_Mod_Rec.LN_LIST_LINE_ID Is Not Null
                           IF  l_Start_Date_Match = 'Y' AND l_End_Date_Match = 'Y'
                              AND  l_From_Qty_Match   = 'Y' AND l_To_Qty_Match = 'Y'
                              THEN
                                 l_Msg_Data     := 'Modifier List Line :' || Line_rec.LN_LIST_LINE_NO || l_Item_Description ||
                                  ' Start Date , End Date, From Qty and To Qty Match with another List Line .' || Get_Mod_Rec.LN_List_Line_NO ;
                                 Fnd_File.put_line(Fnd_File.LOG, l_Msg_Data );
                                 Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                                 l_KNOWN_ERROR  := 'Y' ;
                                 --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
                                 Fnd_File.Put_Line ( Fnd_File.LOG, '512 Raise XXWC Exception =>'  );
                                 Raise XXWC_EXCEPTION;
                           End If;
                        End If;
                   Exception
                      When XXWC_EXCEPTION Then
                         Fnd_File.Put_Line ( Fnd_File.LOG, '512.10 XXWC Exception Raised=>'  );
                         X_Return_Status := G_Return_Error;
                      When OThers Then
                         l_Msg_Data     := 'While Processing Modifier List Line :' || Line_rec.LN_LIST_LINE_NO || l_Item_Description ;
                         Fnd_File.Put_Line ( Fnd_File.LOG, '512.11 When Others Exception Raised=>' || l_Msg_Data  );
                         Fnd_File.Put_Line ( Fnd_File.LOG, '512.12 SQL Error Message=>' || Substr(sqlerrm , 1,250));
                         Fnd_File.put_line(Fnd_File.OUTPUT, 'Error ' || l_Msg_Data ||Substr(sqlerrm , 1,250) );
                         X_Return_Status := G_Return_Error;
                   End;
                End Loop ;  -- Get_Mod_Line_Cur
             End If;
             -- Validate Current Modifier Line Data with existing Modifier line details in Base Table
             -- Added Parameter P_LN_LIST_LINE_ID : Satish U : 04-DEC-2012
             If Line_rec.PA_PRODUCT_ATTRIBUTE_CONTEXT = 'ITEM' Then
                l_PA_PRODUCT_ATTR_VALUE_ITEM     := Line_rec.PA_PRODUCT_ATTR_VALUE ;
                L_PA_PRODUCT_ATTR_VALUE_CAT      := NULL ;
             Else
                l_PA_PRODUCT_ATTR_VALUE_ITEM     := NULL ;
                L_PA_PRODUCT_ATTR_VALUE_CAT      := Line_rec.PA_PRODUCT_ATTR_VALUE ;
             End If;

             l_Line_Count := 0;
             -- Satish U: Added List-Header_ID and ln_End_Date_Active to sql query
             --SatishU: 21-MAR-2013
             --PA_PRODUCT_ATTRIBUTE   In ( 'Item Number' , 'Item Category', 'All Items' ) ;
             Select Count(*)
             Into l_Line_Count
             From apps.XXWC_QP_MODIFIER_INT_GT
             Where MODIFIER_LIST_NUMBER         = Line_Rec.MODIFIER_LIST_NUMBER
             and  list_header_Id                = Line_Rec.List_Header_ID
             And  PA_PRODUCT_ATTRIBUTE_CONTEXT  = Line_Rec.PA_PRODUCT_ATTRIBUTE_CONTEXT
             AND PA_PRODUCT_ATTRIBUTE   In ( 'Item Number' , 'Item Category', 'All Items' )
             --AND( (PA_PRODUCT_ATTRIBUTE   = 'Item Number' AND
             --  AND  PA_PRODUCT_ATTR_VALUE  = l_PA_PRODUCT_ATTR_VALUE_ITEM )  Or
             --  ( PA_PRODUCT_ATTRIBUTE   = 'Item Category' AND
             --  AND  PA_PRODUCT_ATTR_VALUE    = L_PA_PRODUCT_ATTR_VALUE_CAT ) )
             And  PA_PRODUCT_ATTR_VALUE         = Line_Rec.PA_PRODUCT_ATTR_VALUE
             AND  List_Header_ID                = Line_Rec.List_Header_ID
             AND  NVL(LN_END_DATE_ACTIVE, SYSDATE) >= Line_Rec.LN_END_DATE_ACTIVE ;


             Fnd_File.Put_Line ( Fnd_File.LOG, '502 Line Count is =>' || l_Line_Count );
             If L_Line_Count > 1 Then  --  Duplicate Lines Check 100
                 Fnd_File.Put_Line ( Fnd_File.LOG, '502.10 Validating rows in Modifier Base table ');
                 --Fnd_File.Put_Line ( Fnd_File.OUTPUT, '502.10 Validating rows in Modifier Base table ');
                 For  Get_Mod_Rec In Get_Modifier2_Cur (
                                        P_MODIFIER_LIST_NUMBER          => Line_rec.MODIFIER_LIST_NUMBER ,
                                        P_PA_PRODUCT_ATTRIBUTE_CONTEXT  => Line_rec.PA_PRODUCT_ATTRIBUTE_CONTEXT,
                                        P_PA_PRODUCT_ATTRIBUTE          => Line_rec.PA_PRODUCT_ATTRIBUTE,
                                        P_PA_PRODUCT_ATTR_VALUE_ITEM    => l_PA_PRODUCT_ATTR_VALUE_ITEM,
                                        P_PA_PRODUCT_ATTR_VALUE_CAT     => L_PA_PRODUCT_ATTR_VALUE_CAT,
                                        P_LN_LIST_LINE_ID               => Line_rec.LN_LIST_LINE_ID ,
                                        P_LIST_HEADER_ID                => Line_Rec.List_Header_ID,
                                        P_LN_END_DATE_ACTIVE            => Line_Rec.Ln_End_Date_Active) Loop
                    Begin
                        l_UOM_Match               := 'N';
                        l_Start_Date_Match        := 'N';
                        l_End_Date_Match          := 'N';
                        l_Dates_Overlap_Flag      := 'N';
                        l_PriceBreak_Overlap_Flag := 'N';
                        l_LINE_ID_Match           := 'N' ;
                        l_From_Qty_Match          := 'N';
                        l_To_Qty_Match            := 'N';


                        Fnd_File.Put_Line ( Fnd_File.LOG, '520 Inside Third Loop Get_MOdifier2_Cur=>'  );
                        -- Check if UOM Matches
                        If NVL(Get_Mod_Rec.PA_PRODUCT_UOM_CODE,'NULL')  = NVL(Line_rec.PA_PRODUCT_UOM_CODE,'NULL') Then
                          l_UOM_Match := 'Y';
                        ENd If;

                        -- Check If Effectivity Dates Match
                        If   ( NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) = NVL(Line_rec.LN_START_DATE_ACTIVE , C_END_DATE ))  Then
                           l_Start_Date_Match := 'Y';
                        End If;

                         -- Check If Effectivity Dates Match
                        If   ( NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_START_DATE) = NVL(Line_Rec.LN_END_DATE_ACTIVE , C_END_DATE ))  Then
                           l_End_Date_Match := 'Y';
                        End If;

                        -- Check if Dates Overlap
                        IF  NVL(Line_Rec.LN_START_DATE_ACTIVE, C_START_DATE)
                               Between  NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) And NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_END_DATE)  Then
                            l_Dates_Overlap_Flag := 'Y';
                        End If;

                        -- Check if Dates Overlap
                        IF  NVL(Line_Rec.LN_END_DATE_ACTIVE, C_END_DATE)
                               Between  NVL(Get_Mod_Rec.LN_START_DATE_ACTIVE, C_START_DATE) And NVL(Get_Mod_Rec.LN_END_DATE_ACTIVE, C_END_DATE)  Then
                            l_Dates_Overlap_Flag := 'Y';
                        End If;
                        Begin
                            CASE
                              WHEN TO_NUMBER(NVL(Line_Rec.PA_PRICING_ATTR_VALUE_FROM,1))   Between TO_NUMBER(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM,1))
                                       And TO_NUMBER(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999))  THEN
                                  For REc_Qty_Break in Get_Qty_Breaks_Cur (Get_Mod_Rec.LN_List_Line_ID ) Loop
                                      IF TO_NUMBER(NVL(Line_Rec.PA_PRICING_ATTR_VALUE_FROM,1))  Between To_Number(NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_FROM ,1))
                                          And To_Number(NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_TO,99999)) THEN
                                          l_PriceBreak_Overlap_Flag := 'Y';
                                          l_Msg_Data := '520.1 Price Break values Overlap =>' ;
                                          Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                                      End If;
                                  End Loop ;

                                  --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );

                               WHEN TO_NUMBER(NVL(Line_Rec.PA_PRICING_ATTR_VALUE_TO,99999))   Between TO_NUMBER(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM,1))
                                       And TO_NUMBER(NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999))  THEN

                                  For REc_Qty_Break in Get_Qty_Breaks_Cur (Get_Mod_Rec.LN_List_Line_ID ) Loop
                                      IF TO_NUMBER(NVL(Line_Rec.PA_PRICING_ATTR_VALUE_TO,99999))  Between To_Number(NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_FROM ,1))
                                          And To_Number(NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_TO,99999)) THEN
                                          l_PriceBreak_Overlap_Flag := 'Y';
                                          l_Msg_Data := '520.2 Price Break values Overlap =>' ;
                                          Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                                      End If;
                                  End Loop ;

                                  --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                               ELSE
                                  l_PriceBreak_Overlap_Flag := 'N';
                            End CASE;
                        Exception
                           When Others Then
                              CASE
                                  WHEN NVL(Line_Rec.PA_PRICING_ATTR_VALUE_FROM,1)   Between NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM,1)
                                           And NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999)  THEN
                                      For REc_Qty_Break in Get_Qty_Breaks_Cur (Get_Mod_Rec.LN_List_Line_ID ) Loop
                                          IF NVL(Line_Rec.PA_PRICING_ATTR_VALUE_FROM,1)  Between NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_FROM ,1)
                                              And NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_TO,99999) THEN
                                              l_PriceBreak_Overlap_Flag := 'Y';
                                              l_Msg_Data := '520.3 Price Break values Overlap =>' ;
                                              Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                                          End If;
                                      End Loop ;

                                      --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );

                                   WHEN NVL(Line_Rec.PA_PRICING_ATTR_VALUE_TO,99999)   Between NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM,1)
                                           And NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO,99999)  THEN

                                       For REc_Qty_Break in Get_Qty_Breaks_Cur (Get_Mod_Rec.LN_List_Line_ID ) Loop
                                          IF NVL(Line_Rec.PA_PRICING_ATTR_VALUE_TO,99999)  Between NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_FROM ,1)
                                              And NVL(REc_Qty_Break.PA_PRICING_ATTR_VALUE_TO,99999) THEN
                                              l_PriceBreak_Overlap_Flag := 'Y';
                                              l_Msg_Data := '520.4 Price Break values Overlap =>' ;
                                              Fnd_File.Put_Line ( Fnd_File.LOG, l_Msg_Data  );
                                          End If;
                                      End Loop ;

                                      --Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                                   ELSE
                                      l_PriceBreak_Overlap_Flag := 'N';
                                End CASE;

                        End ;

                        -- Check if List_Line_ID matches Too
                        If NVL(LINE_Rec.LN_LIST_LINE_ID, -1) = NVL(Get_Mod_Rec.LN_LIST_LINE_ID, -1) Then
                           l_LINE_ID_Match := 'Y' ;
                        END IF ;

                         -- Check if From Qty Matches
                        If NVL(Line_Rec.PA_PRICING_ATTR_VALUE_FROM, -1) = NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_FROM, -1) Then
                           l_From_Qty_Match := 'Y' ;
                        END IF ;

                         -- Check if To Qty Matches
                        If NVL(Line_Rec.PA_PRICING_ATTR_VALUE_TO, -1) = NVL(Get_Mod_Rec.PA_PRICING_ATTR_VALUE_TO, -1) Then
                           l_To_Qty_Match := 'Y' ;
                        END IF ;

                        -- Validate if List_Line ID Matches.
                        If l_LINE_ID_Match = 'Y' Then
                           -- Do nothing as this line will be udpated
                           NULL;
                        ELSIF l_LINE_ID_Match = 'N'  AND l_UOM_Match = 'Y' Then
                           If l_Dates_Overlap_Flag = 'Y' AND ( l_Start_Date_Match = 'N' OR l_End_Date_Match = 'N') Then
                              l_Msg_Data     := 'Modifier List Line :' || Line_rec.LN_LIST_LINE_NO || l_Item_Description ||
                                      ' Start Date , End Date values overlap with another List Line .' || Get_Mod_Rec.LN_List_Line_NO ;
                              Fnd_File.put_line(Fnd_File.LOG, l_Msg_Data );
                              Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                              FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_EFFECT_DATES_OVERLAP');
                              l_KNOWN_ERROR  := 'Y' ;
                              Fnd_File.Put_Line ( Fnd_File.LOG, '530 Raise XXWC Exception=>'  );

                              Raise XXWC_EXCEPTION;
                           Elsif l_PriceBreak_Overlap_Flag = 'Y' AND ( l_From_Qty_Match = 'N' OR l_To_Qty_Match = 'N') Then
                              l_Msg_Data     := 'Modifier List Line :' || Line_rec.LN_LIST_LINE_NO || l_Item_Description ||
                                      ' Price Break quantity values overlap with another List Line .' || Get_Mod_Rec.LN_List_Line_NO ;
                              Fnd_File.put_line(Fnd_File.LOG, l_Msg_Data );
                              Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
                              FND_MESSAGE.SET_NAME('XXWC','XXWC_QP_PRICEBREAK_QTY_OVERLAP');
                              l_KNOWN_ERROR  := 'Y' ;

                              --RAISE_APPLICATION_ERROR( -20000, l_MSG_Data);
                              Fnd_File.Put_Line ( Fnd_File.LOG, '531 Raise XXWC Exception=>'  );
                              Raise XXWC_EXCEPTION;
                           End If;
                        END IF;
                    Exception
                       When XXWC_EXCEPTION Then
                         Fnd_File.Put_Line ( Fnd_File.LOG, '512.10 XXWC Exception Raised=>'  );
                         X_Return_Status := G_RETURN_ERROR;
                       When OThers Then
                          l_Msg_Data     := 'While Processing Modifier List Line :' || Line_rec.LN_LIST_LINE_NO || l_Item_Description  ;
                          Fnd_File.Put_Line ( Fnd_File.LOG, '512.11 When Others Exception Raised=>' || l_Msg_Data  );
                          Fnd_File.Put_Line ( Fnd_File.LOG, '512.12 SQL Error Message=>' || Substr(sqlerrm , 1,250));
                          Fnd_File.put_line(Fnd_File.OUTPUT, 'Error ' || l_Msg_Data ||Substr(sqlerrm , 1,250) );
                          X_Return_Status := G_RETURN_UNEXP_ERROR;
                    End;
                 End Loop ;
             End IF; -- Line Count > 1

      Exception
         When XXWC_EXCEPTION  Then
              X_Return_Status := G_RETURN_ERROR;
              Fnd_File.Put_Line ( Fnd_File.LOG, '532 Raise XXWC Exception=>'  );
         When Others then
              l_Msg_Data := Substr(SQLERRM,1,200);
              Fnd_File.put_line(Fnd_File.LOG, l_Msg_Data );
              Fnd_File.put_line(Fnd_File.OUTPUT, l_Msg_Data );
              X_Return_Status := G_RETURN_UNEXP_ERROR;
              Fnd_File.Put_Line ( Fnd_File.LOG, '533 Raise XXWC Exception=>'  );
      End;
      End Loop ;
   End Validate_list_Lines;

END XXWC_QP_MODIFIER_WEB_ADI_PKG;
/