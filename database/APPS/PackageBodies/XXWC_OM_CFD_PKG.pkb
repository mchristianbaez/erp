CREATE OR REPLACE PACKAGE BODY xxwc_om_cfd_pkg
/********************************************************************************
FILE NAME: APPS.XXWCAP_INV_INT_PKG.pkb

PROGRAM TYPE: PL/SQL Package Body

PURPOSE: APIs for Order Management Customer Facing Documents.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/08/2013    Gopi Damuluri    Initial version.
1.1     02/18/2013    Gopi Damuluri    Added a new function - secondary_tax
1.2     06/05/2013    Ram Talluri      Added secondary_tax_wc_quote - TMS #20130529-01469
1.3     07/18/2013    Gopi Damuluri    TMS# 20130618-01287
                                       * Time stamp based on the branch's timezone
1.4     08/27/2013    Gopi Damuluri    Resolved 1% Lumber tax issue for Non-CA Orders - TMS #20130827-00521
1.5     12/20/2013    Gopi Damuluri    TMS# 20131010-00323
                                       -- Display Country of Origin(COO) 
                                       -- GSA TAA Compliance
1.6     01/07/2014    Gopi Damuluri    TMS# 20130715-00484 Included exception handling.
1.7     10/01/2014    Gopi Damuluri    TMS# 20141001-00061 Multi Org Changes
1.8     01/08/2015    Raghavendra S    TMS#20141210-00130 OM - GSA Customer Message for Non Compliant Items
                                        COO should be pick from PDH tables instead of Item Master Table
1.9     06/23/2015    Pattabhi Avula   TMS#20150414-00147  -- GSA Customer Message changed as per the
                                       User's request 
1.10    02/14/2018    Pattabhi Avula   TMS#20170314-00267 Auth Buyer CFD Enhancement
1.11    04/12/2018    Naveen Kalidindi TMS# 20171011-00160,20180723-00096 - Prop65 Oracle Compliance Changes.
1.12    08/08/2018    Naveen Kalidindi TMS# 20180723-00096 - Prop65 Performance improvement.
1.13    08/09/2018    Rakesh Patel     TMS# 20180723-00096 -Prop 65 Oracle Compliance changes - CFD, e-Comm
1.14    08/17/2018    Naveen Kalidindi TMS# 20180817-00033 -Fix Prop65 replace command
********************************************************************************/
 IS

  l_package_name VARCHAR2(100) := 'XXWC_OM_CFD_PKG';
  l_distro_list  VARCHAR2(100) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  -- ********************************************************************************
  -- Function to get GSC Notification for a given Order Line
  -- *******************************************************************************
  FUNCTION notify_gsa(p_order_header_id IN NUMBER
                     ,p_order_line_id   IN NUMBER) RETURN VARCHAR2 IS
    l_mod_attr10 VARCHAR2(240);
    --     l_cust_class      VARCHAR2(30);
    l_ret_message    VARCHAR2(250);
    l_qp_gsa_count   NUMBER;
    l_gsa_flag       VARCHAR2(1); -- Version# 1.5
    l_hp_gsa_flag    VARCHAR2(1); -- Version# 1.8
    l_territory_code VARCHAR2(50); -- Version# 1.5
    l_taa_count      NUMBER; -- Version# 1.5
  
  BEGIN
    BEGIN
    
      /*
            SELECT al.lookup_code
              INTO l_cust_class
              FROM apps.oe_order_headers_all oeh
                 , hz_cust_accounts_all      hca
                 , ar_lookups                al
             WHERE 1 = 1
               AND oeh.header_id       = p_order_header_id
               AND hca.cust_account_id = oeh.sold_to_org_id
               AND al.lookup_type      = 'CUSTOMER CLASS'
               AND al.lookup_code      = hca.customer_class_code;
      */
    
    
      SELECT COUNT(1)
        INTO l_qp_gsa_count
        FROM apps.qp_list_headers      qph
            ,apps.oe_order_headers     oeh
            ,apps.oe_order_lines       oel
            ,apps.oe_price_adjustments oep
       WHERE 1 = 1
         AND qph.active_flag = 'Y'
         AND oeh.header_id = oel.header_id
         AND oep.list_header_id = qph.list_header_id
         AND oep.header_id = oeh.header_id
         AND oep.line_id = oel.line_id
         AND oel.line_id = p_order_line_id
         AND oeh.header_id = p_order_header_id
         AND qph.attribute10 = 'GSA';
    
      -- Version# 1.5 > Start
      l_gsa_flag    := 'N';
      l_hp_gsa_flag := 'N'; -- added for V 1.8
    
      --- Commented by Raghavendra for V 1.8
      /* SELECT NVL(hcsu.gsa_indicator, 'N')
       INTO l_gsa_flag
       FROM apps.oe_order_headers oeh
          , hz_cust_site_uses     hcsu
      WHERE 1 = 1
        AND oeh.header_id       = p_order_header_id
        AND hcsu.site_use_id    = oeh.invoice_to_org_id;*/
    
      --- Added by Raghavendra for V 1.8
    
      SELECT nvl(hp.gsa_indicator_flag
                ,'N')
            ,nvl(hcsu.gsa_indicator
                ,'N')
        INTO l_hp_gsa_flag
            ,l_gsa_flag
        FROM apps.oe_order_headers   oeh
            ,apps.hz_parties         hp
            ,apps.hz_cust_accounts   hca
            ,apps.hz_cust_acct_sites hcas
            ,apps.hz_cust_site_uses  hcsu
       WHERE 1 = 1
         AND hp.party_id = hca.party_id
         AND hcas.cust_account_id = hca.cust_account_id
         AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
         AND oeh.header_id = p_order_header_id
         AND hcsu.site_use_id = oeh.invoice_to_org_id;
    
      SELECT COUNT(1)
        INTO l_taa_count
        FROM oe_order_lines ool
             -- , mtl_system_items_b             msib comment for v 1.8     
            ,ego_mtl_sy_items_ext_b pdh_emsi -- Added for V 1.8
            ,fnd_lookup_values      flv
       WHERE 1 = 1
         AND ool.line_id = p_order_line_id
         AND pdh_emsi.inventory_item_id = ool.inventory_item_id
         AND pdh_emsi.organization_id = 222
         AND flv.lookup_type = 'XXWC_TERRITORIES'
         AND flv.lookup_code = pdh_emsi.c_ext_attr2 -- Added for V 1.8
         AND pdh_emsi.attr_group_id = 861 -- Added for V 1.8
         AND flv.description = 'Y';
      -- Version# 1.5 < End
    
    EXCEPTION
      WHEN no_data_found THEN
        l_qp_gsa_count := 0;
        l_gsa_flag     := 'N';
        l_hp_gsa_flag  := 'N'; -- Added for V 1.8
    END;
  
    dbms_output.put_line('l_hp_gsa_flag - ' || l_hp_gsa_flag);
    dbms_output.put_line('l_gsa_flag - ' || l_gsa_flag);
    dbms_output.put_line('l_qp_gsa_count - ' || l_qp_gsa_count);
    dbms_output.put_line('l_taa_count - ' || l_taa_count);
  
    -- Version# 1.5 > Start
    IF (l_gsa_flag = 'Y' OR l_hp_gsa_flag = 'Y') THEN
      -- Added for OR l_hp_gsa_flag='Y' for V 1.8
      IF l_taa_count = 0
         AND l_qp_gsa_count > 0 THEN
        l_ret_message := 'ITEM IS NOT TAA COMPLIANT';
      
      ELSIF l_taa_count = 0
            AND l_qp_gsa_count = 0 THEN
        --  l_ret_message := 'ITEM IS NOT TAA COMPLIANT AND IS NOT GSA LISTED';  -- Commented as as part of V 1.9 TMS#20150414-00147
        l_ret_message := 'Item is not TAA compliant and is not contracted on GSA Advantage but under GSA Schedule'; -- Added as as part of V 1.9 TMS#20150414-00147
      
      ELSIF l_taa_count > 0
            AND l_qp_gsa_count = 0 THEN
        --  l_ret_message := 'ITEM IS NOT GSA LISTED';  -- Commented as as part of V 1.9 TMS#20150414-00147
        l_ret_message := 'Item is not contracted on GSA Advantage but under GSA Schedule'; -- Added as as part of V 1.9 TMS#20150414-00147
      
      ELSIF l_taa_count > 0
            AND l_qp_gsa_count > 0 THEN
        l_ret_message := '';
      END IF;
    ELSIF l_gsa_flag = 'N' THEN
      l_ret_message := '';
    END IF;
    -- Version# 1.5 < End
  
    RETURN l_ret_message;
  
  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_OM_CFD_PKG.NOTIFY_GSA'
                                          ,p_calling           => 'OTHERS exception'
                                          ,p_request_id        => NULL
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running XXWC_OM_CFD_PKG.NOTIFY_GSA procedure with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OM');
    
      l_ret_message := '';
      RETURN l_ret_message;
  END notify_gsa;

  -- ********************************************************************************
  -- Procedure to get GSC Notification for a given Order
  -- *******************************************************************************
  PROCEDURE notify_gsa_hdr(p_errbuf          OUT VARCHAR2
                          ,p_retcode         OUT VARCHAR2
                          ,p_order_header_id IN NUMBER DEFAULT NULL) IS
  
    -- Version# 1.5 Start > 
    CURSOR cur_order_hdr IS
      SELECT DISTINCT oel.header_id
                     ,oel.invoice_to_org_id
        FROM oe_order_lines oel
       WHERE 1 = 1
         AND oel.line_type_id IN (1002
                                 ,1005)
         AND trunc(oel.creation_date) LIKE trunc(SYSDATE)
         AND oel.flow_status_code NOT IN ('CLOSED'
                                         ,'CANCELLED')
         AND (p_order_header_id IS NULL OR oel.header_id = p_order_header_id)
         AND oel.line_category_code = 'ORDER'
         AND oel.cancelled_flag = 'N'
         AND oel.open_flag = 'Y';
  
    CURSOR cur_order_lines(p_order_hdr_id NUMBER) IS
      SELECT oel.line_id
            ,oel.invoice_to_org_id
            ,oel.rowid
        FROM oe_order_lines oel
       WHERE 1 = 1
         AND oel.line_type_id IN (1002
                                 ,1005)
         AND trunc(oel.creation_date) LIKE trunc(SYSDATE)
         AND oel.flow_status_code NOT IN ('CLOSED'
                                         ,'CANCELLED')
         AND oel.header_id = p_order_hdr_id
         AND oel.line_category_code = 'ORDER'
         AND oel.cancelled_flag = 'N'
         AND oel.open_flag = 'Y';
    -- Version# 1.5 End <
  
    --     l_cust_class      VARCHAR2(30);     -- Version# 1.5
    l_ret_message  VARCHAR2(250); -- Version# 1.5
    l_qp_gsa_count NUMBER; -- Version# 1.5
    l_taa_count    NUMBER; -- Version# 1.5
    l_gsa_flag     VARCHAR2(1); -- Version# 1.5
    l_hp_gsa_flag  VARCHAR2(1); -- Version# 1.8
  
  BEGIN
  
    FOR rec_hdr IN cur_order_hdr LOOP
    
      /*
             l_cust_class := NULL;
             SELECT al.lookup_code
               INTO l_cust_class
               FROM hz_cust_accounts_all      hca
                  , ar_lookups                al
              WHERE 1 = 1
                AND hca.cust_account_id = rec_hdr.sold_to_org_id
                AND al.lookup_type      = 'CUSTOMER CLASS'
                AND al.lookup_code      = hca.customer_class_code
                AND al.enabled_flag     = 'Y'
                AND SYSDATE between al.start_date_active AND NVL(al.end_date_active,SYSDATE + 1);
      */
      l_gsa_flag    := 'N';
      l_hp_gsa_flag := 'N';
      BEGIN
      
        --- Commented by Raghavendra for V 1.8
        /*
        SELECT NVL(hcsu.gsa_indicator, 'N')
          INTO l_gsa_flag
          FROM hz_cust_site_uses     hcsu
         WHERE 1 = 1
           AND hcsu.site_use_id = rec_hdr.invoice_to_org_id;*/
      
        SELECT nvl(hp.gsa_indicator_flag
                  ,'N')
              ,nvl(hcsu.gsa_indicator
                  ,'N')
          INTO l_hp_gsa_flag
              ,l_gsa_flag
          FROM apps.hz_parties         hp
              ,apps.hz_cust_accounts   hca
              ,apps.hz_cust_acct_sites hcas
              ,apps.hz_cust_site_uses  hcsu
         WHERE hp.party_id = hca.party_id
           AND hcas.cust_account_id = hca.cust_account_id
           AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
           AND hcsu.site_use_id = rec_hdr.invoice_to_org_id;
      EXCEPTION
        WHEN OTHERS THEN
          l_gsa_flag    := 'N';
          l_hp_gsa_flag := 'N';
      END;
    
      IF (l_gsa_flag = 'Y' OR l_hp_gsa_flag = 'Y') THEN
        FOR rec_lines IN cur_order_lines(rec_hdr.header_id) LOOP
        
          l_qp_gsa_count := 0;
          l_taa_count    := 0;
          l_ret_message  := NULL;
        
          BEGIN
            SELECT COUNT(1)
              INTO l_qp_gsa_count
              FROM apps.qp_list_headers      qph
                  ,apps.oe_price_adjustments oep
             WHERE 1 = 1
               AND oep.header_id = rec_hdr.header_id
               AND oep.line_id = rec_lines.line_id
               AND qph.list_header_id = oep.list_header_id
               AND qph.active_flag = 'Y'
               AND qph.attribute10 = 'GSA';
          EXCEPTION
            WHEN OTHERS THEN
              l_qp_gsa_count := 0;
          END;
        
          BEGIN
            SELECT COUNT(1)
              INTO l_taa_count
              FROM oe_order_lines ool
                   --, mtl_system_items_b             msib Comment for V 1.8
                  ,ego_mtl_sy_items_ext_b pdh_emsi -- Added for V 1.8
                  ,fnd_lookup_values      flv
             WHERE 1 = 1
               AND ool.line_id = rec_lines.line_id
               AND ool.attribute14 IS NULL
               AND pdh_emsi.inventory_item_id = ool.inventory_item_id
               AND pdh_emsi.organization_id = 222
               AND flv.lookup_type = 'XXWC_TERRITORIES'
               AND flv.lookup_code = pdh_emsi.c_ext_attr2 -- -- Added for V 1.8
               AND pdh_emsi.attr_group_id = 861 -- -- Added for V 1.8
               AND flv.description = 'Y';
          EXCEPTION
            WHEN OTHERS THEN
              l_taa_count := 0;
          END;
        
          IF p_order_header_id IS NOT NULL THEN
            fnd_file.put_line(fnd_file.log
                             ,'***************************');
            fnd_file.put_line(fnd_file.log
                             ,'line_id - ' || rec_lines.line_id);
            fnd_file.put_line(fnd_file.log
                             ,'l_hp_gsa_flag - ' || l_hp_gsa_flag);
            fnd_file.put_line(fnd_file.log
                             ,'l_gsa_flag - ' || l_gsa_flag);
            fnd_file.put_line(fnd_file.log
                             ,'l_qp_gsa_count - ' || l_qp_gsa_count);
            fnd_file.put_line(fnd_file.log
                             ,'l_taa_count - ' || l_taa_count);
            fnd_file.put_line(fnd_file.log
                             ,'***************************');
          END IF;
        
          -- Version# 1.5 > Start
          IF (l_gsa_flag = 'Y' OR l_hp_gsa_flag = 'Y') THEN
            -- Added for OR l_hp_gsa_flag='Y' for V 1.8 
            IF l_taa_count = 0
               AND l_qp_gsa_count > 0 THEN
              l_ret_message := 'ITEM IS NOT TAA COMPLIANT';
            
            ELSIF l_taa_count = 0
                  AND l_qp_gsa_count = 0 THEN
              --  l_ret_message := 'ITEM IS NOT TAA COMPLIANT AND IS NOT GSA LISTED';  -- Commented as as part of V 1.9 TMS#20150414-00147
              l_ret_message := 'Item is not TAA compliant and is not contracted on GSA Advantage but under GSA Schedule'; -- Added as as part of V 1.9 TMS#20150414-00147
            
            ELSIF l_taa_count > 0
                  AND l_qp_gsa_count = 0 THEN
              -- l_ret_message := 'ITEM IS NOT GSA LISTED';  -- Commented as as part of V 1.9 TMS#20150414-00147
              l_ret_message := 'Item is not contracted on GSA Advantage but under GSA Schedule'; -- Added as as part of V 1.9 TMS#20150414-00147
            
            ELSIF l_taa_count > 0
                  AND l_qp_gsa_count > 0 THEN
              l_ret_message := '';
            END IF;
          ELSIF l_gsa_flag = 'N' THEN
            l_ret_message := '';
          END IF;
          -- Version# 1.5 < End
        
          IF l_ret_message IS NOT NULL THEN
            BEGIN
              -- Version# 1.6
              UPDATE oe_order_lines
                 SET attribute14 = l_ret_message
               WHERE line_id = rec_lines.line_id;
              -- Version# 1.6 > Start
            EXCEPTION
              WHEN OTHERS THEN
                NULL;
            END;
            -- Version# 1.6 < End
          END IF;
        
        END LOOP; -- cur_order_lines
      END IF;
    END LOOP; -- cur_order_hdr
  
    /*
          UPDATE oe_order_lines
             SET attribute14 = l_ret_message
           WHERE line_id IN (SELECT oel.line_id
                               FROM apps.oe_order_headers oeh
                                  , apps.oe_order_lines   oel
                                  , hz_cust_accounts      hca
                                  , ar_lookups            al
                              WHERE 1 = 1
                                AND oeh.header_id       = oel.header_id
                                AND al.lookup_type      = 'CUSTOMER CLASS'
                                AND al.lookup_code      = hca.customer_class_code
                                AND al.lookup_code      = 'FEDERAL'
                                AND hca.cust_account_id = oeh.sold_to_org_id
                                AND oel.creation_date like SYSDATE
                                AND oel.line_category_code = 'ORDER'
                                AND oel.cancelled_flag     = 'N'
                                AND oel.open_flag          = 'Y'
                                AND oel.flow_status_code NOT IN ('CLOSED' , 'CANCELLED')
                                AND (p_order_header_id IS NULL OR oel.header_id = p_order_header_id)
                                AND oel.line_type_id IN (1002, 1005)
                                AND NOT EXISTS (SELECT '1'
                                                  FROM apps.qp_list_headers      qph
                                                     , apps.oe_price_Adjustments oep
                                                 WHERE 1 = 1
                                                   AND oep.list_header_id        = qph.list_header_id
                                                   AND NVL(qph.attribute10,'-1') = 'GSA'
                                                   AND oep.line_id               = oel.line_id
                                                   AND oep.header_id             = oeh.header_id
                                                   AND qph.ACTIVE_FLAG           ='Y')
                                );
    */
  EXCEPTION
    WHEN OTHERS THEN
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_OM_CFD_PKG.NOTIFY_GSA_HDR'
                                          ,p_calling           => 'OTHERS exception'
                                          ,p_request_id        => NULL
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running XXWC_OM_CFD_PKG.NOTIFY_GSA_HDR procedure with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OM');
    
  END notify_gsa_hdr;

  -- ********************************************************************************
  -- Function to get VendorPartNumber for a given InventoryItem
  -- *******************************************************************************
  FUNCTION vendor_part_num(p_inventory_item_id IN NUMBER) RETURN VARCHAR2 IS
    l_vendor_part_num VARCHAR2(255);
  BEGIN
    SELECT cross_reference
      INTO l_vendor_part_num
      FROM mtl_cross_references
     WHERE 1 = 1
       AND cross_reference_type = 'VENDOR'
       AND inventory_item_id = p_inventory_item_id
       AND rownum = 1;
  
    RETURN l_vendor_part_num;
  
  EXCEPTION
    WHEN OTHERS THEN
      l_vendor_part_num := '';
      RETURN l_vendor_part_num;
  END vendor_part_num;

  -- ********************************************************************************
  -- Function to get Unit_Selling_Price with 5 decimals
  -- *******************************************************************************
  FUNCTION unit_selling_price(p_unit_selling_price IN NUMBER) RETURN NUMBER IS
    l_unit_selling_price NUMBER;
  BEGIN
    SELECT round(p_unit_selling_price
                ,5)
      INTO l_unit_selling_price
      FROM dual;
  
    RETURN l_unit_selling_price;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN p_unit_selling_price;
  END unit_selling_price;

  -- ********************************************************************************
  -- Function to derive Secondary Tax
  -- *******************************************************************************
  FUNCTION secondary_tax(p_order_header_id  IN NUMBER
                        ,p_st_site_use_id   IN NUMBER
                        ,p_ship_from_org_id IN NUMBER
                        ,p_organization_id  IN NUMBER
                        ,p_delivery_id      IN NUMBER) RETURN NUMBER IS
  
    /*
    -- This code is commented as LineLevel Org Tax Calculation is not working.
          CURSOR cur_ord_line
              IS
          SELECT ool.line_id
               , ool.ship_from_org_id
               , ool.shipping_method_code
               , NVL (ool.shipping_quantity,0) *unit_selling_price line_amt
               , TO_NUMBER(flv.tag/100) tax_percent
            FROM oe_order_lines_all ool
               , mtl_system_items_b msib
               , fnd_lookup_values  flv
           WHERE ool.inventory_item_id = msib.inventory_item_id
             AND msib.organization_id  = 222
             AND flv.lookup_type       = 'XXWC_TAX_CODE_MAPPING'
             AND flv.description       = msib.attribute22
             AND ool.header_id         = p_order_header_id;
    */
    l_h_branch_state VARCHAR2(2);
    l_l_branch_state VARCHAR2(2);
    l_st_state       VARCHAR2(2);
    l_err_msg        VARCHAR2(5000);
    l_sec_tax_amt    NUMBER := 0;
    l_program_err EXCEPTION;
  BEGIN
    -----------------------------------------------------------
    -- Derive Ship-To state
    -----------------------------------------------------------
    BEGIN
      SELECT decode(hl.state
                   ,NULL
                   ,hl.province || ', '
                   ,hl.state)
        INTO l_st_state
        FROM hz_cust_site_uses  hcsu
            ,hz_cust_acct_sites hcas
            ,hz_party_sites     hps
            ,hz_locations       hl
       WHERE 1 = 1
         AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
         AND hcas.party_site_id = hps.party_site_id
         AND hps.location_id = hl.location_id(+)
         AND site_use_code = 'SHIP_TO'
         AND hcsu.site_use_id = p_st_site_use_id;
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Ship-To state';
        RAISE l_program_err;
    END;
  
    --   IF l_st_state != 'CA' THEN
    --     RAISE l_program_err;
    --   END IF;
  
    -----------------------------------------------------------
    -- Derive OrderHeader Branch State
    -----------------------------------------------------------
    BEGIN
      SELECT region_2
        INTO l_h_branch_state
        FROM hr_all_organization_units hou
            ,hr_locations_all          hl
       WHERE hou.organization_id = p_ship_from_org_id
         AND hou.location_id = hl.location_id;
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving OrderHeader Branch state';
        RAISE l_program_err;
    END;
  
    IF l_h_branch_state = 'CA' THEN
      IF p_delivery_id IS NULL THEN
        SELECT SUM(least(nvl(ool.shipping_quantity
                            ,ool.ordered_quantity)
                        ,nvl(ool.attribute11
                            ,9999999999)) * ool.unit_selling_price * (to_number(flv_tax.tag) / 100) *
                   decode(line_category_code
                         ,'RETURN'
                         ,-1
                         ,1))
          INTO l_sec_tax_amt
          FROM oe_order_lines     ool
              ,mtl_system_items_b msib
              ,fnd_lookup_values  flv_tax
         WHERE ool.inventory_item_id = msib.inventory_item_id
           AND msib.organization_id = 222
           AND flv_tax.lookup_type = 'XXWC_TAX_CODE_MAPPING'
           AND flv_tax.description = msib.attribute22
           AND ool.header_id = p_order_header_id
           AND ool.flow_status_code NOT IN ('PRE-BILLING_ACCEPTANCE'
                                           ,'CLOSED'
                                           ,'CANCELLED'
                                           ,'SHIPPED'
                                           ,'INVOICE_HOLD')
           AND nvl(ool.user_item_description
                  ,'ZZZZ') NOT IN ('BACKORDERED') -- Version# 1.5
           AND ool.ship_from_org_id = nvl(p_organization_id
                                         ,ool.ship_from_org_id)
           AND NOT EXISTS
         (SELECT '1'
                  FROM xxwc_wsh_shipping_stg xwss
                 WHERE xwss.header_id = ool.header_id
                   AND xwss.line_id = ool.line_id
                   AND xwss.ship_from_org_id = nvl(p_organization_id
                                                  ,xwss.ship_from_org_id));
      ELSE
        SELECT SUM(least(nvl(ool.shipping_quantity
                            ,ool.ordered_quantity)
                        ,nvl(ool.attribute11
                            ,9999999999)) * ool.unit_selling_price * (to_number(flv_tax.tag) / 100) *
                   decode(line_category_code
                         ,'RETURN'
                         ,-1
                         ,1))
          INTO l_sec_tax_amt
          FROM oe_order_lines     ool
              ,mtl_system_items_b msib
              ,fnd_lookup_values  flv_tax
         WHERE ool.inventory_item_id = msib.inventory_item_id
           AND msib.organization_id = 222
           AND flv_tax.lookup_type = 'XXWC_TAX_CODE_MAPPING'
           AND flv_tax.description = msib.attribute22
           AND ool.header_id = p_order_header_id
           AND ool.flow_status_code NOT IN ('PRE-BILLING_ACCEPTANCE'
                                           ,'CLOSED'
                                           ,'CANCELLED'
                                           ,'SHIPPED'
                                           ,'INVOICE_HOLD')
           AND nvl(ool.user_item_description
                  ,'ZZZZ') NOT IN ('BACKORDERED') -- Version# 1.5
           AND ool.ship_from_org_id = nvl(p_organization_id
                                         ,ool.ship_from_org_id)
           AND EXISTS
         (SELECT '1'
                  FROM xxwc_wsh_shipping_stg xwss
                 WHERE xwss.header_id = ool.header_id
                   AND xwss.line_id = ool.line_id
                   AND xwss.ship_from_org_id = nvl(p_organization_id
                                                  ,xwss.ship_from_org_id)
                   AND xwss.delivery_id = p_delivery_id);
      END IF;
    ELSE
      IF l_st_state = 'CA' THEN
        -- Version# 1.4
        IF p_delivery_id IS NULL THEN
          SELECT SUM(least(nvl(ool.shipping_quantity
                              ,ool.ordered_quantity)
                          ,nvl(ool.attribute11
                              ,9999999999)) * ool.unit_selling_price *
                     (to_number(flv_tax.tag) / 100) * decode(line_category_code
                                                            ,'RETURN'
                                                            ,-1
                                                            ,1))
            INTO l_sec_tax_amt
            FROM oe_order_lines     ool
                ,mtl_system_items_b msib
                ,fnd_lookup_values  flv_tax
                ,fnd_lookup_values  flv_shp
           WHERE ool.inventory_item_id = msib.inventory_item_id
             AND msib.organization_id = 222
             AND flv_tax.lookup_type = 'XXWC_TAX_CODE_MAPPING'
             AND flv_tax.description = msib.attribute22
             AND flv_shp.lookup_type = 'XXWC_SHIP_METHOD_MAPPING'
             AND flv_shp.meaning = ool.shipping_method_code
             AND ool.header_id = p_order_header_id
             AND nvl(ool.user_item_description
                    ,'ZZZZ') NOT IN ('BACKORDERED') -- Version# 1.5
             AND ool.flow_status_code NOT IN ('PRE-BILLING_ACCEPTANCE'
                                             ,'CLOSED'
                                             ,'CANCELLED'
                                             ,'SHIPPED'
                                             ,'INVOICE_HOLD')
             AND ool.ship_from_org_id = nvl(p_organization_id
                                           ,ool.ship_from_org_id)
             AND NOT EXISTS
           (SELECT '1'
                    FROM xxwc_wsh_shipping_stg xwss
                   WHERE xwss.header_id = ool.header_id
                     AND xwss.line_id = ool.line_id
                     AND xwss.ship_from_org_id = nvl(p_organization_id
                                                    ,xwss.ship_from_org_id));
        ELSE
          SELECT SUM(least(nvl(ool.shipping_quantity
                              ,ool.ordered_quantity)
                          ,nvl(ool.attribute11
                              ,9999999999)) * ool.unit_selling_price *
                     (to_number(flv_tax.tag) / 100) * decode(line_category_code
                                                            ,'RETURN'
                                                            ,-1
                                                            ,1))
            INTO l_sec_tax_amt
            FROM oe_order_lines     ool
                ,mtl_system_items_b msib
                ,fnd_lookup_values  flv_tax
                ,fnd_lookup_values  flv_shp
           WHERE ool.inventory_item_id = msib.inventory_item_id
             AND msib.organization_id = 222
             AND flv_tax.lookup_type = 'XXWC_TAX_CODE_MAPPING'
             AND flv_tax.description = msib.attribute22
             AND flv_shp.lookup_type = 'XXWC_SHIP_METHOD_MAPPING'
             AND flv_shp.meaning = ool.shipping_method_code
             AND ool.header_id = p_order_header_id
             AND nvl(ool.user_item_description
                    ,'ZZZZ') NOT IN ('BACKORDERED') -- Version# 1.5
             AND ool.flow_status_code NOT IN ('PRE-BILLING_ACCEPTANCE'
                                             ,'CLOSED'
                                             ,'CANCELLED'
                                             ,'SHIPPED'
                                             ,'INVOICE_HOLD')
             AND ool.ship_from_org_id = nvl(p_organization_id
                                           ,ool.ship_from_org_id)
             AND EXISTS
           (SELECT '1'
                    FROM xxwc_wsh_shipping_stg xwss
                   WHERE xwss.header_id = ool.header_id
                     AND xwss.line_id = ool.line_id
                     AND xwss.ship_from_org_id = nvl(p_organization_id
                                                    ,xwss.ship_from_org_id)
                     AND xwss.delivery_id = p_delivery_id);
        END IF;
      END IF; -- l_st_state = 'CA'       -- Version# 1.4
    END IF;
  
    /*
    -- This code is commented as LineLevel Org Tax Calculation is not working.
        -----------------------------------------------------------
        -- Derive OrderLine Branch State
        -----------------------------------------------------------
        BEGIN
          SELECT region_2
            INTO l_l_branch_state
            FROM hr_all_organization_units hou, hr_locations_all hl
           WHERE  hou.organization_id = p_l_ship_from_org_id
             AND hou.location_id = hl.location_id;
        EXCEPTION
        WHEN OTHERS THEN
          l_err_msg := 'Error deriving OrderLine Branch state';
          RAISE l_program_err;
        END;
    */
  
    RETURN l_sec_tax_amt;
  
  EXCEPTION
    WHEN l_program_err THEN
      RETURN 0;
    WHEN OTHERS THEN
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package_name
                                          ,p_calling           => 'LumberTax calculation - WHEN OTHERS Exception'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'LumberTax calculation - WHEN OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OM');
      RETURN 0;
  END secondary_tax;

  -- ********************************************************************************
  -- Function to derive Secondary Tax for White cap custom Quote.--Ram Talluri TMS #20130529-01469
  -- *******************************************************************************
  FUNCTION secondary_tax_wc_quote(p_order_header_id  IN NUMBER
                                 ,p_st_site_use_id   IN NUMBER
                                 ,p_ship_from_org_id IN NUMBER
                                 ,p_organization_id  IN NUMBER) RETURN NUMBER IS
  
    l_h_branch_state VARCHAR2(2);
    l_l_branch_state VARCHAR2(2);
    l_st_state       VARCHAR2(2);
    l_err_msg        VARCHAR2(5000);
    l_sec_tax_amt    NUMBER := 0;
    l_program_err EXCEPTION;
  BEGIN
    -----------------------------------------------------------
    -- Derive Ship-To state
    -----------------------------------------------------------
    BEGIN
      SELECT decode(hl.state
                   ,NULL
                   ,hl.province || ', '
                   ,hl.state)
        INTO l_st_state
        FROM hz_cust_site_uses  hcsu
            ,hz_cust_acct_sites hcas
            ,hz_party_sites     hps
            ,hz_locations       hl
       WHERE 1 = 1
         AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
         AND hcas.party_site_id = hps.party_site_id
         AND hps.location_id = hl.location_id(+)
         AND site_use_code = 'SHIP_TO'
         AND hcsu.site_use_id = p_st_site_use_id;
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Ship-To state';
        RAISE l_program_err;
    END;
  
    ---------------------------------------------------------
    -- Derive OrderHeader Branch State
    -----------------------------------------------------------
    BEGIN
      SELECT region_2
        INTO l_h_branch_state
        FROM hr_all_organization_units hou
            ,hr_locations_all          hl
       WHERE hou.organization_id = p_ship_from_org_id
         AND hou.location_id = hl.location_id;
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving OrderHeader Branch state';
        RAISE l_program_err;
    END;
  
    IF l_h_branch_state = 'CA' THEN
    
      SELECT SUM(nvl(xoql.line_quantity
                    ,0) * xoql.gm_selling_price * (to_number(flv_tax.tag) / 100))
        INTO l_sec_tax_amt
        FROM xxwc_om_quote_lines   xoql
            ,xxwc_om_quote_headers xoqh
            ,mtl_system_items_b    msib
            ,fnd_lookup_values     flv_tax
       WHERE xoql.inventory_item_id = msib.inventory_item_id
         AND xoqh.quote_number = xoql.quote_number
         AND msib.organization_id = 222
         AND flv_tax.lookup_type = 'XXWC_TAX_CODE_MAPPING'
         AND flv_tax.description = msib.attribute22
         AND xoql.quote_number = p_order_header_id
         AND xoqh.organization_id = nvl(p_organization_id
                                       ,xoqh.organization_id);
    
    ELSE
      IF l_st_state = 'CA' THEN
        -- Version# 1.4
        SELECT SUM(nvl(xoql.line_quantity
                      ,0) * xoql.gm_selling_price * (to_number(flv_tax.tag) / 100))
          INTO l_sec_tax_amt
          FROM xxwc_om_quote_lines   xoql
              ,xxwc_om_quote_headers xoqh
              ,mtl_system_items_b    msib
              ,fnd_lookup_values     flv_tax
              ,fnd_lookup_values     flv_shp
         WHERE xoql.inventory_item_id = msib.inventory_item_id
           AND xoqh.quote_number = xoql.quote_number
           AND msib.organization_id = 222
           AND flv_tax.lookup_type = 'XXWC_TAX_CODE_MAPPING'
           AND flv_tax.description = msib.attribute22
           AND flv_shp.lookup_type = 'XXWC_SHIP_METHOD_MAPPING'
           AND flv_shp.meaning = xoqh.shipping_method
           AND xoqh.quote_number = p_order_header_id
           AND xoqh.organization_id = nvl(p_organization_id
                                         ,xoqh.organization_id);
      END IF; -- l_st_state = 'CA'       -- Version# 1.4
    END IF;
  
    RETURN l_sec_tax_amt;
  
  EXCEPTION
    WHEN l_program_err THEN
      RETURN 0;
    WHEN OTHERS THEN
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package_name
                                          ,p_calling           => 'White Cap custom quote LumberTax calculation - WHEN OTHERS Exception'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'White Cap custom quote LumberTax calculation - WHEN OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OM');
      RETURN 0;
  END secondary_tax_wc_quote;

  -- Version 1.3 > Start
  -- ********************************************************************************
  -- Function to get date in Branch TimeZone
  -- *******************************************************************************
  FUNCTION get_branch_dts(p_branch_name IN VARCHAR2) RETURN VARCHAR2 IS
    l_branch_dts VARCHAR2(50);
  
  BEGIN
    SELECT to_char(new_time(SYSDATE
                           ,'EDT'
                           ,decode(timezone_code
                                  ,'America/Chicago'
                                  ,'CDT'
                                  ,'America/Denver'
                                  ,'MDT'
                                  ,'America/Los_Angeles'
                                  ,'PDT'
                                  ,'Pacific/Honolulu'
                                  ,'HST'
                                  ,'America/New_York'
                                  ,'EDT'
                                  ,'EDT'))
                  ,'MM/DD/YYYY HH:MI AM') || ' ' ||
           nvl(upper(substr(REPLACE(timezone_display
                                   ,' Time'
                                   ,'')
                           ,14))
              ,'EASTERN')
      INTO l_branch_dts
      FROM hr_locations_v
     WHERE location_code = p_branch_name;
  
    l_branch_dts := l_branch_dts;
    RETURN l_branch_dts;
  EXCEPTION
    WHEN OTHERS THEN
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package_name
                                          ,p_calling           => 'White Cap GET_BRANCH_DTS - WHEN OTHERS Exception'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'White Cap GET_BRANCH_DTS - WHEN OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OM');
      RETURN 0;
  END get_branch_dts;
  -- Version 1.3 < End

  -- Version# 1.5 > Start
  FUNCTION get_country_of_origin(p_inv_to_org_id IN NUMBER
                                ,p_inv_item_id   IN NUMBER) RETURN VARCHAR2 IS
    l_coo_code   VARCHAR2(240);
    l_coo_length NUMBER;
    l_gsa_flag   VARCHAR2(1) := 'N';
  BEGIN
  
    SELECT nvl(hcsu.gsa_indicator
              ,'N')
      INTO l_gsa_flag
      FROM hz_cust_site_uses hcsu
     WHERE 1 = 1
       AND hcsu.site_use_id = p_inv_to_org_id;
  
    IF l_gsa_flag = 'Y' THEN
      SELECT msib.attribute10
            ,length(nvl(msib.attribute10
                       ,' '))
        INTO l_coo_code
            ,l_coo_length
        FROM mtl_system_items_b msib
       WHERE 1 = 1
         AND msib.inventory_item_id = p_inv_item_id
         AND msib.organization_id = 222;
    
      IF l_coo_length = 2 THEN
        RETURN l_coo_code;
      ELSE
        l_coo_code := '';
        RETURN l_coo_code;
      END IF;
    ELSE
      l_coo_code := '';
      RETURN l_coo_code;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
    
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package_name
                                          ,p_calling           => 'White Cap GET_COUNTRY_OF_ORIGIN - WHEN OTHERS Exception'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'White Cap GET_COUNTRY_OF_ORIGIN - WHEN OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OM');
    
      l_coo_code := '';
      RETURN l_coo_code;
  END get_country_of_origin;
  -- Version# 1.5 < End

  -- Version 1.10 > Start
  /********************************************************************************
  FILE NAME: APPS.get_auth_buyer
  
  PROGRAM TYPE: Function
  
  PURPOSE: Fetching the Authorized buyer details.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.10    02/14/2018    Pattabhi Avula  TMS#20170314-00267 Auth Buyer CFD 
                                        Enhancement - Initial version
  ********************************************************************************/
  FUNCTION get_auth_buyer(p_auth_buyer_flag IN VARCHAR2
                         ,p_auth_buyer_id   IN NUMBER) RETURN VARCHAR2 IS
    l_auth_buyer hz_parties.party_name%TYPE := NULL;
  
  BEGIN
    IF p_auth_buyer_flag = 'Y' THEN
      SELECT party.party_name
        INTO l_auth_buyer
        FROM hz_parties             party
            ,hz_cust_account_roles  acct_role
            ,hz_relationships       rel
            ,hz_cust_accounts       role_acct
            ,hz_role_responsibility cont_role
       WHERE party.party_id = rel.subject_id
         AND acct_role.party_id = rel.party_id
         AND acct_role.role_type = 'CONTACT'
         AND acct_role.cust_account_id = role_acct.cust_account_id
         AND role_acct.party_id = rel.object_id
         AND acct_role.cust_account_role_id = cont_role.cust_account_role_id
         AND cont_role.responsibility_type = 'AUTH_BUYER'
         AND acct_role.cust_account_role_id = p_auth_buyer_id
         AND rownum = 1;
    ELSE
      l_auth_buyer := '';
    END IF;
  
    RETURN l_auth_buyer;
  
  EXCEPTION
    WHEN no_data_found THEN
      l_auth_buyer := '';
    WHEN OTHERS THEN
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package_name
                                          ,p_calling           => 'White Cap GET_AUTH_BUYER - WHEN OTHERS Exception'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'White Cap GET_AUTH_BUYER - WHEN OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OM');
      l_auth_buyer := '';
      RETURN l_auth_buyer;
  END get_auth_buyer;
  -- Version 1.10 < End


  /********************************************************************************
  FILE NAME: APPS.get_prop65_message
  
  PROGRAM TYPE: Function
  
  PURPOSE: Getting Prop65 Message
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------  -----------------------------------------
  1.11    04/12/2018    Naveen Kalidindi TMS 20171011-00160 | Prop 65 Oracle 
                                         Compliance changes - Initial version
  1.13    09/08/2018    Rakesh Patel     TMS#20180723-00096 -Prop 65 Oracle 
                                         Compliance changes - CFD, e-Comm
  ********************************************************************************/
  FUNCTION get_prop65_message(p_inventory_item_id IN NUMBER
                             ,p_remove_warning    IN VARCHAR2 DEFAULT 'N'
                             ,p_remove_link       IN VARCHAR2 DEFAULT 'N') RETURN VARCHAR2 IS

    l_prop65_msg VARCHAR2(2000) DEFAULT NULL;

    -- cursor to get the detail from custom table
    CURSOR c_ca_prop65 IS
      SELECT xiea.item_number
            ,NVL(TRIM(xiea.prop_65_flag),'N') prop_65_flag
            ,TRIM(xiea.prop_65_cancer) prop_65_cancer
            ,TRIM(xiea.prop_65_reproductive) prop_65_reproductive
            ,TRIM(xiea.prop_65_short_form) prop_65_short_form
            ,TRIM(xiea.prop_65_wood_dust_msg) prop65_cancer_wood_dust
            ,TRIM(prop_65_display_message) prop_65_display_message
            ,TRIM(prop_65_message) prop_65_message
            ,TRIM(prop_65_cancer_msg) prop_65_cancer_msg
            ,TRIM(prop_65_reproductive_msg) prop_65_reproductive_msg  
            ,TRIM(prop_65_short_form_msg)  prop_65_short_form_msg      
        FROM xxwc.xxwc_item_ehs_attributes xiea
       WHERE 1 = 1
         AND xiea.inventory_item_id = p_inventory_item_id
         AND xiea.organization_id = 222;		
         
	rc_ca_prop65 c_ca_prop65%ROWTYPE;
	
  BEGIN
    OPEN c_ca_prop65;
    FETCH c_ca_prop65
      INTO rc_ca_prop65;
    CLOSE c_ca_prop65;
  
    -- Prop65 Eligible --
    IF rc_ca_prop65.prop_65_flag = 'Y' THEN
  	  -- if Short Form EHS DFF provided --
      IF rc_ca_prop65.prop_65_short_form IS NOT NULL THEN
        l_prop65_msg := REPLACE(rc_ca_prop65.prop_65_short_form_msg,'<PROP65_SHORT_FORM>',rc_ca_prop65.prop_65_short_form);
      ELSE
        IF rc_ca_prop65.prop_65_cancer IS NOT NULL THEN
        
          IF rc_ca_prop65.prop_65_cancer = 'Wood Dust' THEN -- When Cancer EHS DFF is set as Wood Dust 
            --XXWC_PROP65_CANCER_WOOD_DUST in messages
            l_prop65_msg := rc_ca_prop65.prop65_cancer_wood_dust;

          ELSIF rc_ca_prop65.prop_65_reproductive IS NOT NULL THEN -- When Both Cancer and Reproductive EHS DFFs have values 
            --XXWC_PROP65_MESSAGE in messages
            l_prop65_msg := REPLACE(rc_ca_prop65.prop_65_message,'<PROP65_CANCER_MSG>',rc_ca_prop65.prop_65_cancer);
            
            l_prop65_msg := REPLACE(l_prop65_msg,'<PROP65_REP_MSG>',rc_ca_prop65.prop_65_reproductive);

          ELSE -- When Only Cancer EHS DFF has value
            --XXWC_PROP65_CANCER in messages
            l_prop65_msg := REPLACE(rc_ca_prop65.prop_65_cancer_msg,'<PROP65_CANCER_MSG>',rc_ca_prop65.prop_65_cancer);
          END IF;
          
        ELSIF rc_ca_prop65.prop_65_reproductive IS NOT NULL THEN-- When Only Reproductive EHS DFF has Value 
          --XXWC_PROP65_REPRODUCTIVE in messages
          l_prop65_msg := REPLACE(rc_ca_prop65.prop_65_reproductive_msg,'<PROP65_REP_MSG>',rc_ca_prop65.prop_65_reproductive);
        END IF;
      END IF;
    END IF;
    RETURN l_prop65_msg;
  
  EXCEPTION
    WHEN OTHERS THEN
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package_name
                                          ,p_calling           => 'White Cap GET_PROP65_MESSAGE - WHEN OTHERS Exception'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'White Cap GET_PROP65_MESSAGE - WHEN OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OM');
    
      RETURN NULL;
    
  
  END get_prop65_message;

  /********************************************************************************
  FILE NAME: APPS.get_prop65_message_ecomm
  
  PROGRAM TYPE: Function
  
  PURPOSE: Getting Prop65 Message E-Commerce extract
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------  -----------------------------------------
  1.12    04/12/2018    Naveen Kalidindi TMS 20180723-00096 | Prop 65 Oracle 
                                         Compliance changes - Ecommerce extract Only
  ********************************************************************************/
  FUNCTION get_prop65_message_ecomm(p_inventory_item_id IN NUMBER) RETURN VARCHAR2 IS
	--
    l_prop65_msg_ecomm VARCHAR2(2000) DEFAULT NULL;
    --								 
  BEGIN
    --
    -- l_prop65_msg_ecomm := REPLACE(get_prop65_message(p_inventory_item_id,'N','Y'),' For more information go to www.P65Warnings.ca.gov','');
    l_prop65_msg_ecomm := REPLACE(get_prop65_message(p_inventory_item_id,'N','Y'),' www.P65Warnings.ca.gov','');
	
	IF l_prop65_msg_ecomm IS NOT NULL THEN
		l_prop65_msg_ecomm := 'WARNING: '||l_prop65_msg_ecomm;
	END IF;
	  
    RETURN l_prop65_msg_ecomm;
  
  EXCEPTION
    WHEN OTHERS THEN
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package_name
                                          ,p_calling           => 'White Cap GET_PROP65_MESSAGE_ECOMM - WHEN OTHERS Exception'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'White Cap GET_PROP65_MESSAGE_ECOMM - WHEN OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OM');
    
      RETURN NULL;
    
  
  END get_prop65_message_ecomm;									 
  
  -- Version# 1.11 < End

END xxwc_om_cfd_pkg;
/