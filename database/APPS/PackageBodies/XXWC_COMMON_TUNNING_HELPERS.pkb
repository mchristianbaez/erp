/* Formatted on 05-Apr-2014 12:53:25 (QP5 v5.206) */
-- Start of DDL Script for Package Body APPS.XXWC_COMMON_TUNNING_HELPERS
-- Generated 05-Apr-2014 12:53:24 from APPS@EBIZPRD

CREATE OR REPLACE PACKAGE BODY apps.xxwc_common_tunning_helpers
IS
    --
    -- To modify this template, edit file PKGBODY.TXT in TEMPLATE
    -- directory of SQL Navigator
    --
    -- Purpose: Briefly explain the functionality of the package body
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  ------------------------------------------
    -- Enter procedure, function bodies as shown below
    -- will insert logging or error message into custom table to watch the performance evolution;
    
/*    
     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.1        06/16/2014  Manjula Chellappan    Modified the procedure 'elapsed_time'
                                                    to write log conditionally based on the lookup 
                                                    XXWC_COMMON_INTERFACE_LOG
      1.2      04/19/2016   Neha Saini              TMS#20160407-00183  removing parallelism.
    --******************************************************
    */

    l_errbuf   CLOB;

    PROCEDURE elapsed_time (p_interface_name VARCHAR2, p_called_from VARCHAR2, p_start IN OUT NUMBER)
    IS
        v_sec_elapsed      NUMBER;
        gr_start           NUMBER := DBMS_UTILITY.get_time;
        v_message          VARCHAR2 (512);
        v_found            NUMBER;
        v_interface_name   VARCHAR2 (254) := NVL (p_interface_name, 'Unknown');
        g_start            NUMBER;
        l_write_log        VARCHAR2(1):= 'Y';
    BEGIN
        BEGIN
            SELECT 1
              INTO v_found
              FROM all_objects
             WHERE object_name = 'XXWC_INTERFACES##LOG' AND owner = 'XXWC';
        EXCEPTION
            WHEN OTHERS
            THEN
                EXECUTE IMMEDIATE '
     create table xxwc.XXWC_INTERFACES##LOG (INTERFACE_NAME VARCHAR2(254),
     insert_date date, log varchar2(512), elapsed_time number)';

                EXECUTE IMMEDIATE 'alter table xxwc.XXWC_INTERFACES##LOG nologging';
        END;

        v_message := SUBSTR (p_called_from, 1, 512);
        g_start := NVL (p_start, gr_start);
        v_sec_elapsed := (gr_start - g_start) / 100;
        v_interface_name := NVL (SUBSTR (p_interface_name, 1, 254), 'Unknown');
        
        BEGIN
                
            SELECT 'N'
              INTO l_write_log              
              FROM fnd_lookup_values
             WHERE lookup_type = 'XXWC_COMMON_INTERFACE_LOG'
               AND lookup_code = upper(v_interface_name)
               AND tag = 'N';
               
        EXCEPTION WHEN OTHERS THEN
               l_write_log := 'Y';
        
        END;
        
IF l_write_log = 'Y' THEN

        EXECUTE IMMEDIATE 'INSERT INTO xxwc.XXWC_INTERFACES##LOG
             VALUES (:V_INTERFACE_NAME,:v_sysdate,:v_message,:v_time)'
            USING v_interface_name
                 ,SYSDATE
                 ,v_message
                 ,v_sec_elapsed;

        COMMIT;
        p_start := gr_start;
        
        END IF;
        
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            write_log ('error executing procedure elapsed_time ' || l_errbuf);
            RAISE;
    END;

    --******************************************************
    --*************************************************************
    -- table name should include schema name
    --******************************************************
    --*************************************************************
    --will add column group_number with values from 1 .. p_tuning_factor to the table and create bitmap index
    --to devide the table into chunks
    --*************************************************************
    FUNCTION add_tuning_parameter (p_schema_name VARCHAR2, p_table_name VARCHAR2, p_tuning_factor NUMBER)
        RETURN NUMBER
    IS
        v_string                 CLOB;
        v_tuning_factor          NUMBER;
        v_index_name             VARCHAR2 (124);
        v_index_name_no_schema   VARCHAR2 (124);
        v_schema_name            VARCHAR2 (164) := TRIM (UPPER (p_schema_name));
        v_table_name             VARCHAR2 (164);
        v_full_table_name        VARCHAR2 (164);
    BEGIN
        v_table_name := REPLACE (UPPER (p_table_name), v_schema_name || '.', '');
        v_full_table_name := v_schema_name || '.' || v_table_name;
        v_index_name := v_full_table_name || 'b1';
        v_index_name_no_schema := v_table_name || 'b1';

        IF p_tuning_factor IS NULL
        THEN
            v_tuning_factor := 70;
        ELSE
            v_tuning_factor := p_tuning_factor;
        END IF;

        v_string := 'ALTER  TABLE ' || v_full_table_name || ' ADD(group_number NUMBER,line_number_r NUMBER)';

        EXECUTE IMMEDIATE v_string;

        v_string :=
               'DECLARE
    n_loop   NUMBER := 0;

BEGIN
    FOR r IN (SELECT ROWID v_rowid FROM '
            || v_full_table_name
            || ' )
    LOOP
        n_loop := n_loop + 1;

        UPDATE  '
            || v_full_table_name
            || '
           SET line_number_r = n_loop
         WHERE ROWID = r.v_rowid;
    END LOOP;

    COMMIT;
END;';

        EXECUTE IMMEDIATE v_string;

        v_string :=
               'DECLARE
    v_count          NUMBER := 0;
    v_count6         NUMBER := 0;
    v_group_number   NUMBER := 1;

BEGIN
    SELECT MAX (line_number_r) INTO v_count FROM  '
            || v_full_table_name
            || ' ;

    v_count6 := ROUND (v_count / :tuning_factor);

    FOR r IN (SELECT ROWID v_rowid, line_number_r FROM  '
            || v_full_table_name
            || ' )
    LOOP
        UPDATE  '
            || v_full_table_name
            || '
           SET group_number = v_group_number
         WHERE ROWID = r.v_rowid;

        IF MOD (r.line_number_r, v_count6) = 0
        THEN
            v_group_number := v_group_number + 1;
        END IF;
    END LOOP;

    COMMIT;
END;';

        EXECUTE IMMEDIATE v_string USING v_tuning_factor;

        v_tuning_factor := v_tuning_factor + 1;

        BEGIN
            EXECUTE IMMEDIATE 'drop index ' || v_index_name;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        EXECUTE IMMEDIATE
               'create bitmap index '
            || v_index_name
            || ' on '
            || v_full_table_name
            || '(group_number) tablespace '
            || v_schema_name
            || '_IDX';

        DBMS_STATS.gather_index_stats (ownname => v_schema_name, indname => v_index_name_no_schema);

        RETURN v_tuning_factor;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            write_log ('error executing procedure xxwc_add_tuning_parameter ' || l_errbuf);
            RAISE;
    END;

    --*************************************************************
    --will remove tuning columns from the temp table
    --*************************************************************
    PROCEDURE remove_tuning_factor_columns (p_schema_name VARCHAR2, p_table_name VARCHAR2)
    IS
        v_schema_name   VARCHAR2 (164) := TRIM (UPPER (p_schema_name));
        v_table_name    VARCHAR2 (164);
    BEGIN
        v_table_name := REPLACE (UPPER (p_table_name), v_schema_name || '.', '');

        FOR r IN (SELECT *
                    FROM dba_tab_columns
                   WHERE column_name = 'GROUP_NUMBER' AND table_name = v_table_name AND owner = v_schema_name)
        LOOP
            EXECUTE IMMEDIATE 'ALTER TABLE ' || v_schema_name || '.' || v_table_name || ' DROP COLUMN  group_number';

            EXECUTE IMMEDIATE 'ALTER TABLE ' || v_schema_name || '.' || v_table_name || ' DROP COLUMN line_number_r';

            format_temp_table (v_schema_name, v_table_name);
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            write_log ('error executing procedure remove_tuning_factor_columns ' || l_errbuf);
            RAISE;
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --************************************************************
    PROCEDURE drop_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2)
    IS
        v_ddl_string   VARCHAR2 (1024);
    BEGIN
        FOR r
            IN (SELECT object_name, owner
                  FROM all_objects
                 WHERE     object_name = UPPER (TRIM (p_table_name))
                       AND object_type = 'TABLE'
                       AND owner = UPPER (TRIM (p_owner)))
        LOOP
            v_ddl_string := 'drop table ' || r.owner || '.' || r.object_name || ' purge';

            EXECUTE IMMEDIATE v_ddl_string;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            write_log ('error executing procedure drop_temp_table ' || l_errbuf);
            RAISE;
    END;

    --*************************************************************
    --  Find the most free tablespace that start with XX%. This is
    -- to avoid using XXWC if it is not the right place to store this
    -- temporary data. This will most likely find and use the XXEIS
    -- tablespace.
    FUNCTION running_tablespace
        RETURN VARCHAR2
    IS
        v_tablespacename   VARCHAR2 (124);
        v_insert_date      DATE;
    BEGIN
        --TMS 20140224-00021  changed by Rasikha to calculate every 2 days,02/24/2014
        FOR r IN (SELECT MIN (insert_date) insert_date FROM xxwc.xxwc_free_space)
        LOOP
            v_insert_date := r.insert_date;
        END LOOP;

        IF NVL (v_insert_date, SYSDATE - 5) < TRUNC (SYSDATE) - 2
        THEN
            EXECUTE IMMEDIATE 'truncate table xxwc.xxwc_free_space';

            INSERT INTO xxwc.xxwc_free_space
                SELECT df.tablespace_name tablespace
                      ,df.total_space total_space
                      ,fs.free_space free_space
                      ,df.total_space_mb total_space_mb
                      , (df.total_space_mb - fs.free_space_mb) used_space_mb
                      ,fs.free_space_mb free_space_mb
                      ,ROUND (100 * (fs.free_space / df.total_space), 2) pct_free
                      ,TRUNC (SYSDATE) insert_date
                  FROM (  SELECT tablespace_name, SUM (bytes) total_space, ROUND (SUM (bytes) / 1048576) total_space_mb
                            FROM dba_data_files
                        GROUP BY tablespace_name) df
                      ,(  SELECT tablespace_name, SUM (bytes) free_space, ROUND (SUM (bytes) / 1048576) free_space_mb
                            FROM dba_free_space
                        GROUP BY tablespace_name) fs
                 WHERE df.tablespace_name = fs.tablespace_name(+) AND df.tablespace_name LIKE 'XX%';

            COMMIT;
        END IF;

        SELECT tablespace
          INTO v_tablespacename
          FROM (  SELECT tablespace
                        ,total_space
                        ,free_space
                        ,total_space_mb
                        ,used_space_mb
                        ,free_space_mb
                        ,pct_free
                        ,insert_date
                    FROM xxwc.xxwc_free_space
                ORDER BY free_space_mb DESC)
         WHERE ROWNUM = 1;

        RETURN v_tablespacename;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            write_log ('error executing procedure running_tablespace ' || l_errbuf);
            RAISE;
    END;

    --*************************************************************
    --*************************************************************
   /* REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
   --   1.2      04/19/2016   Neha Saini              TMS#20160407-00183  removing parallelism.
    --**************************************************************/
    --      Make the table perform better for this extract
    PROCEDURE alter_table_temp (p_owner VARCHAR2, p_table_name VARCHAR2)
    IS
        v_execute_string   CLOB;
        v_tablespace       VARCHAR2 (124);
    BEGIN
        v_tablespace := running_tablespace;

        FOR r
            IN (SELECT object_name
                  FROM all_objects
                 WHERE     object_name = UPPER (TRIM (p_table_name))
                       AND object_type = 'TABLE'
                       AND owner = UPPER (TRIM (p_owner)))
        LOOP
            v_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_table_name || ' NOLOGGING';

            EXECUTE IMMEDIATE v_execute_string;

            v_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_table_name || ' MOVE TABLESPACE ' || v_tablespace;

            EXECUTE IMMEDIATE v_execute_string;

            -- Increases parallelism on this table, helps with RAC environment.
            -- May change this to a forumla based on the environment
            v_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_table_name || ' NOPARALLEL '; --' parallel 32'; --commented for ver 1.2 

            EXECUTE IMMEDIATE v_execute_string;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            write_log ('error executing ' || v_execute_string || ' ' || l_errbuf);
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************

    --*************************************************************
    --******helper function

    FUNCTION create_copy_table (p_owner VARCHAR2, p_table_name VARCHAR2, p_new_table_name VARCHAR2)
        RETURN NUMBER
    IS
        v_ind   NUMBER := 0;
    BEGIN
        drop_temp_table (p_owner, p_new_table_name);

        FOR rt IN (SELECT table_name
                     FROM all_tables
                    WHERE table_name = UPPER (p_table_name) AND owner = UPPER (p_owner) AND ROWNUM = 1)
        LOOP
            EXECUTE IMMEDIATE
                   ' CREATE TABLE '
                || p_owner
                || '.'
                || p_new_table_name
                || ' AS SELECT * FROM '
                || p_owner
                || '.'
                || p_table_name
                || ' WHERE 1=2';

            alter_table_temp (p_owner, p_new_table_name);
            v_ind := 1;
        END LOOP;

        RETURN v_ind;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            write_log ('error executing create_copy_table ' || l_errbuf);
            RAISE;
    END;

    --*************************************************************
    --*************************************************************

    --*************************************************************
    --*************************************************************
    -- helper function
    PROCEDURE create_table_from_other_table (p_owner VARCHAR2, p_old_table_name VARCHAR2, p_new_table_name VARCHAR2)
    IS
    BEGIN
        DECLARE
            vr   NUMBER := 0;
        BEGIN
            drop_temp_table (p_owner, p_new_table_name);
            vr := create_copy_table (p_owner, p_old_table_name, p_new_table_name);
            -- FUNCTION create_copy_table (p_owner VARCHAR2, p_table_name VARCHAR2, p_new_table_name VARCHAR2)
            alter_table_temp (p_owner, p_new_table_name);
        END;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            write_log ('error executing table_create_from_old ' || l_errbuf);
            RAISE;
    END;

    --*************************************************************
    -- Enter further code below as specified in the Package spec.
    --******************************************************
    --*************************************************************
    PROCEDURE format_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2)
    IS
        -- this procedure will reformat the table file
        -- to avoid aging(fragments)
        v_buffer_table_name   VARCHAR2 (112) := SUBSTR (p_table_name, 1, 28) || 'b#';
    BEGIN
        create_table_from_other_table (p_owner, p_table_name, v_buffer_table_name);

        EXECUTE IMMEDIATE
               'insert /*+append */ into '
            || p_owner
            || '.'
            || v_buffer_table_name
            || ' select * from  '
            || p_owner
            || '.'
            || p_table_name;

        COMMIT;
        drop_temp_table (p_owner, p_table_name);
        create_table_from_other_table (p_owner, v_buffer_table_name, p_table_name);

        EXECUTE IMMEDIATE
               'insert /*+append */ into '
            || p_owner
            || '.'
            || p_table_name
            || ' select * from  '
            || p_owner
            || '.'
            || v_buffer_table_name;

        COMMIT;
        drop_temp_table (p_owner, v_buffer_table_name);
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            write_log ('error executing format_temp_table  ' || l_errbuf);
            RAISE;
    END;

    --******************************************************
    --*************************************************************
    --**********************************************************************
    PROCEDURE write_log (p_text IN VARCHAR2)
    IS
    BEGIN
        fnd_file.put_line (fnd_file.LOG, p_text);

        DBMS_OUTPUT.put_line (p_text);
    END;

    --*****************************************************************************************************************
    --*************************************************************

    --*************************************************************
    PROCEDURE deleteduplicates (p_table_name     IN VARCHAR2
                               ,p_column_name    IN VARCHAR2
                               ,p_where_clause   IN VARCHAR2 DEFAULT NULL)
    IS
        v_cursorid       INTEGER;
        v_cursorid_del   INTEGER;
        v_selectstmt     CLOB;
        v_updatestmt     CLOB;
        v_rowid          ROWID;
        v_dummy          INTEGER;
        v_dummy_del      INTEGER;
        vcounter         INTEGER := 0;
        v_table_name     VARCHAR2 (56);
        v_column_name    CLOB;
        v_where_clause   CLOB;
    BEGIN
        /*
         *This procedure will delete duplicate line
         *from table (p_table_name),
         * duplication by  p_column_names which is
         * actually list of columns delimited by comma;
         * for example you can call

         *DeleteDuplicates('general_invoice','invoice_number,invoice_date'  ); or
         *DeleteDuplicates('general_invoice','invoice_number,invoice_date',' real_invoice_date >='4-Jun-2002'')

         *This will delete duplicate lines duplicated by columns:
         * invoice_number,invoice_date from table
         *general_invoice  where real_invoice_date >='4-Jun-2012'
         *
         *
         */
        v_table_name := p_table_name;
        v_column_name := p_column_name;

        IF p_where_clause IS NULL
        THEN
            v_where_clause := ' 1= 1';
        ELSE
            v_where_clause := p_where_clause;
        END IF;

        v_cursorid := DBMS_SQL.open_cursor;
        v_cursorid_del := DBMS_SQL.open_cursor;
        v_selectstmt :=
               'SELECT rowid   FROM '
            || v_table_name
            || ' WHERE rowid IN   (SELECT min(rowid)
         FROM '
            || v_table_name
            || ' where '
            || v_where_clause
            || ' GROUP BY '
            || v_column_name
            || '       HAVING COUNT(*) > 1) and '
            || v_where_clause;

        v_updatestmt := 'DELETE FROM  ' || v_table_name || ' WHERE ROWID =  :V_ROWID';

        DBMS_SQL.parse (v_cursorid, v_selectstmt, DBMS_SQL.v7);
        DBMS_SQL.parse (v_cursorid_del, v_updatestmt, DBMS_SQL.v7);
        DBMS_SQL.define_column (v_cursorid
                               ,1
                               ,v_rowid
                               ,18);
        v_dummy := DBMS_SQL.execute (v_cursorid);

        LOOP
            IF DBMS_SQL.fetch_rows (v_cursorid) = 0
            THEN
                EXIT;
            END IF;

            DBMS_SQL.COLUMN_VALUE (v_cursorid, 1, v_rowid);

            DBMS_SQL.bind_variable (v_cursorid_del, ':V_ROWID', v_rowid);
            v_dummy_del := DBMS_SQL.execute (v_cursorid_del);
            vcounter := vcounter + 1;
        END LOOP;

        COMMIT;
        DBMS_SQL.close_cursor (v_cursorid);
        DBMS_SQL.close_cursor (v_cursorid_del);

        IF vcounter > 0
        --it means that dbms_sql.fetch_rows(v_cursorid) <> 0
        THEN
            --call this procedure again to delete all but one row
            deleteduplicates (v_table_name, v_column_name, v_where_clause);
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            write_log ('Error ' || TO_CHAR (SQLCODE) || ': ' || SQLERRM);
            ROLLBACK;
    END deleteduplicates;

    --*******************************************************
    --will remove control chars and nonascii
    FUNCTION remove_control_nonascii (p_in_string VARCHAR2)
        RETURN VARCHAR2
    IS
        v_return   VARCHAR2 (1024);
    BEGIN
        v_return := p_in_string;

        v_return :=
            REGEXP_REPLACE (ASCIISTR (REGEXP_REPLACE (p_in_string, '[[:cntrl:]]', NULL)), '\\[[:xdigit:]]{4}', '');

        RETURN v_return;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN v_return;
    END;

    --*******************************************************
    --will remove control chars
    FUNCTION remove_control (p_in_string VARCHAR2)
        RETURN VARCHAR2
    IS
        v_return   VARCHAR2 (1024);
    BEGIN
        v_return := p_in_string;

        v_return := REGEXP_REPLACE (p_in_string, '[[:cntrl:]]', NULL);

        RETURN v_return;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN v_return;
    END;

    --*******************************************************
    --will remove nonascii chars
    FUNCTION remove_nonascii (p_in_string VARCHAR2)
        RETURN VARCHAR2
    IS
        v_return   VARCHAR2 (1024);
    BEGIN
        v_return := p_in_string;

        v_return := REGEXP_REPLACE (ASCIISTR (p_in_string), '\\[[:xdigit:]]{4}', '');

        RETURN v_return;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN v_return;
    END;
END;
/

-- Grants for Package Body
GRANT EXECUTE ON apps.xxwc_common_tunning_helpers TO xxwc_dev_admin
/
GRANT EXECUTE ON apps.xxwc_common_tunning_helpers TO interface_prism
/
GRANT EXECUTE ON apps.xxwc_common_tunning_helpers TO xxeis
/
GRANT EXECUTE ON apps.xxwc_common_tunning_helpers TO interface_xxcus
/

-- End of DDL Script for Package Body APPS.XXWC_COMMON_TUNNING_HELPERS
