CREATE OR REPLACE PACKAGE BODY APPS.XXWCMSC_MSC_PUB_SAFETY_STK_PKG AS
/* $Header: MSCXPSSB.pls 120.4 2006/05/23 10:47:23 pragarwa noship $ */

/************************************************************************************************************************************************
 * Publish_Safety_Stocks : create MTL safety stock from MRP safety stocks                                                                       *
 *                                                                                                                                              *
 *   REVISIONS:                                                                                                                                 *
 *   Ver        Date        Author                     Description                                                                              *
 *   ---------  ----------  ---------------         ----------------------------------------------------------------------------------          *
 *   1.1        01/30/2013                          Pull Processing Lead Time instead of Safety Stock Bucket Days                               *
 *                                                  and remove MRP Planned % Restriction                                                        *  
 *   1.2        08/14/2013                          Remove round order quantity logic and always round                                          *
 *                                                  safety stock calculation to 2 decimals - TMS Ticket 20130814-02113                          *
 *   1.3        02/06/2014                          Parameter for log file - TMS Ticket 20140110-00053                                          *                            
 *   1.4        12/16/2016  P.Vamshidhar            TMS#20160920-00081  - Spike in EBSPRD 'XXWC Generate Safety Stocks - Inventory’ program     *
 *                                                                                                                                              *                            
 ************************************************************************************************************************************************/

Procedure Publish_Safety_Stocks(p_errbuf                  OUT NOCOPY VARCHAR2,
                                p_retcode                 OUT NOCOPY NUMBER,   -- Bug 4560452
                                p_plan_id                 IN NUMBER,
                                p_org_code                IN VARCHAR2,
                                p_planner_code            IN VARCHAR2,
                                p_abc_class               IN VARCHAR2,
                                p_item_id                 IN NUMBER,
                                p_planning_gp             IN VARCHAR2,
                                p_project_id              IN NUMBER,
                                p_task_id                 IN NUMBER,
                                p_item_category           IN VARCHAR2,
                                p_horizon_start           IN VARCHAR2,
                                p_horizon_end             IN VARCHAR2,
                                p_overwrite               IN VARCHAR2
                               ) Is

  p_org_id                             NUMBER;
  p_inst_code                          VARCHAR2(3);
  p_sr_instance_id                     NUMBER;
  p_category_set_id                    NUMBER;
  p_category_name                      VARCHAR2(240);
  p_designator                         VARCHAR2(10);
  l_version                            NUMBER;
  l_user_id                            NUMBER;
  l_user_name                          VARCHAR2(100);
  l_resp_name                          VARCHAR2(30);
  l_application_name                   VARCHAR2(50);
  l_item_name                          VARCHAR2(255);
  l_log_message                        VARCHAR2(1000);
  l_supp_name                          VARCHAR2(100);
  l_supp_site                          VARCHAR2(30);
  l_records_exist                      NUMBER;
  l_cursor1                            NUMBER;
  l_cursor2                            NUMBER;
  l_language                           VARCHAR2(30);
  l_language_code                      VARCHAR2(4);

  l_horizon_start                      DATE;  --canonical date
  l_horizon_end                        DATE;  --canonical date

  t_pub                                COMPANYNAMELIST;
  t_pub_id                             NUMBERLIST;
  t_pub_site                           COMPANYSITELIST;
  t_pub_site_id                        NUMBERLIST;
  t_customer                           COMPANYNAMELIST;
  t_customer_id                        NUMBERLIST;
  t_customer_site                      COMPANYSITELIST;
  t_customer_site_id                   NUMBERLIST;
  t_org_id                             NUMBERLIST;
  t_sr_instance_id                     NUMBERLIST;
  t_item_id                            NUMBERLIST;
  t_base_item_id                       NUMBERLIST;
  t_qty                                NUMBERLIST;
  t_pub_ot                             NUMBERLIST;
  t_bkt_type                           NUMBERLIST;
  t_posting_party_id                   NUMBERLIST;
  t_item_name                          ITEMNAMELIST;
  t_item_desc                          ITEMDESCLIST;
  t_base_item_name                     ITEMNAMELIST;
  t_pub_ot_desc                        FNDMEANINGLIST;
  t_proj_NUMBER                        NUMBERLIST;
  t_task_NUMBER                        NUMBERLIST;
  t_planning_gp                        PLANNINGGROUPLIST;
  t_bkt_type_desc                      FNDMEANINGLIST;
  t_posting_party_name                 COMPANYNAMELIST;
  t_uom_code                           ITEMUOMLIST;
  t_planner_code                       PLANNERCODELIST;
  t_key_date                           DATELIST;
  t_ship_date                          DATELIST;
  t_receipt_date                       DATELIST;
  t_supp                               COMPANYNAMELIST;
  t_supp_id                            NUMBERLIST;
  t_supp_site                          COMPANYSITELIST;
  t_supp_site_id                       NUMBERLIST;
  t_master_item_name                   ITEMNAMELIST;
  t_master_item_desc                   ITEMDESCLIST;
  t_supp_item_name                     ITEMNAMELIST;
  t_supp_item_desc                     ITEMDESCLIST;
  t_type                               NUMBERLIST;

  t_days_in_bkt                        NUMBERLIST;
  t_bucket_type                        NUMBERLIST;
  t_period_start_date                  DATELIST;
  t_bucket_index                       NUMBERLIST;
  t_bucket_start                       DATELIST;
  t_bucket_end                         DATELIST;
 
  t_ss_code                            NUMBERLIST;
  t_ss_percent                         NUMBERLIST;
  t_ss_days                            NUMBERLIST;
  t_org_code                           ITEMNAMELIST;
  t_msi_item_id                        NUMBERLIST;
  
  b_bkt_index                          NUMBERLIST;
  b_bkt_start_date                     DATELIST;
  b_bkt_end_date                       DATELIST;
  b_bkt_type                           NUMBERLIST;

  i_bkt_index                          NUMBERLIST;
  i_bkt_start_date                     DATELIST;
  i_bkt_end_date                       DATELIST;
  i_bkt_type                           NUMBERLIST;

  i                                    NUMBER;
  l_bucket_end_date                    MSC_PLAN_BUCKETS.bkt_end_date%type;
  l_bucket_type                        MSC_PLAN_BUCKETS.bucket_type%type;
  l_supply                             NUMBER;
  l_demand                             NUMBER;
  l_scrap_demand                       NUMBER;
  l_exp_lot                            NUMBER;
  l_onhand                             NUMBER;
  l_expired_qty                        NUMBER;
  l_pab_total                          NUMBER;

  
  Cursor Safety_Stock_C(p_plan_id             NUMBER,
                        p_org_id              NUMBER,
                        p_sr_instance_id      NUMBER,
                        p_horizon_start       DATE,
                        p_horizon_end         DATE,
                        p_planner_code        VARCHAR2,
                        p_abc_class           VARCHAR2,
                        p_item_id             NUMBER,
                        p_planning_gp         VARCHAR2,
                        p_project_id          NUMBER,
                        p_task_id             NUMBER,
                        p_item_category       VARCHAR2) IS
    Select Distinct 
           MST.sr_instance_id,
           MST.organization_id,
           ITEM.base_item_id,
           ITEM.inventory_item_id,
           MST.period_start_date,
           MPB.bkt_start_date,
           MPB.bkt_end_date,
           MPB.days_in_bkt,
           MPB.bucket_type,
           MPB.bucket_index,
           NULL company_id,
           NULL company_name,
           NULL company_site_id,
           NULL company_site_name,
           ITEM.item_name,
           ITEM.description,
           NULL,  --base_item_name
           ITEM.uom_code,
           ITEM.planner_code, --Bug 4424426
           NULL, --mst.planning_group,
           NULL, --mst.project_id,
           NULL, --mst.task_id,
           MSI.inventory_item_id msi_item_id,
           MSI.mrp_safety_stock_code,
					 MSI.mrp_safety_stock_percent,
           MSI.safety_stock_bucket_days,
           MP.organization_code,
           Sum(MST.safety_stock_quantity)
      From MSC_SAFETY_STOCKS MST,
           MSC_PLAN_BUCKETS MPB,
           MSC_PLAN_ORGANIZATIONS_V OV,
           MSC_SYSTEM_ITEMS ITEM,
           MSC_PLANS P,
           MTL_SYSTEM_ITEMS_B MSI,
           MTL_PARAMETERS MP,
           MTL_ITEM_CATEGORIES MIC,
           MTL_CATEGORIES_KFV MC,
           MTL_CATEGORY_SETS MCS
     WHERE p.plan_id = mst.plan_id
       And mst.plan_id = p_plan_id
       And mst.organization_id = nvl(p_org_id, mst.organization_id)
       And mst.sr_instance_id = nvl(p_sr_instance_id, mst.sr_instance_id)
       And item.inventory_item_id = nvl(p_item_id, item.inventory_item_id)
       And mst.plan_id = item.plan_id
       And mst.sr_instance_id = item.sr_instance_id
       And mst.organization_id = item.organization_id
       And mst.inventory_item_id = item.inventory_item_id
       And ITEM.organization_id = MSI.organization_id
       And ITEM.sr_inventory_item_id = MSI.inventory_item_id
       And MSI.organization_id = MP.organization_id
--and mst.safety_stock_quantity > 0
       And NVL(item.planner_code,'-99') = NVL(p_planner_code, NVL(item.planner_code,'-99'))
       And NVL(item.abc_class_name,'-99') = NVL(p_abc_class, NVL(item.abc_class_name,'-99'))
       And MST.plan_id = ov.plan_id                         -- Bug# 3913477
       And MST.organization_id =ov.planned_organization
       And MST.sr_instance_id = ov.sr_instance_id
       And MST.plan_id = mpb.plan_id
       And MST.sr_instance_id = mpb.sr_instance_id
       And OV.plan_id = mpb.plan_id
       And MPB.curr_flag = 1
       And MSI.inventory_item_id = MIC.inventory_item_id
       And MSI.organization_id = MIC.organization_id
       And MIC.category_id = MC.category_id
       And MIC.category_set_id = MCS.category_set_id
       And MCS.category_set_name = 'Inventory Category'
       And MC.concatenated_segments = Nvl(p_item_category, MC.concatenated_segments)
       And Trunc(MST.period_start_date) between trunc(mpb.bkt_start_date) and trunc(mpb.bkt_end_date)
       And Nvl(MST.planning_group, '-99') = Nvl(p_planning_gp, Nvl(MST.planning_group, '-99'))
       And Nvl(MST.project_id,-99) = Nvl(p_project_id, Nvl(MST.project_id,-99))
       And Nvl(MST.task_id, -99) = Nvl(p_task_id, Nvl(MST.task_id, -99))
       And P.plan_completion_date Is Not NULL
       And Trunc(MST.period_start_date) Between Nvl(Trunc(P.plan_start_date),Trunc(MST.period_start_date)) 
                                            And Nvl(Trunc(p_horizon_end),Trunc(MST.period_start_date))
     Group By            MST.sr_instance_id,
           MST.organization_id,
           ITEM.base_item_id,
           ITEM.inventory_item_id,
           MST.period_start_date,
           MPB.bkt_start_date,
           MPB.bkt_end_date,
           MPB.days_in_bkt,
           MPB.bucket_type,
           MPB.bucket_index,
           NULL,
           NULL,
           NULL,
           NULL,
           ITEM.item_name,
           ITEM.description,
           NULL,  --base_item_name
           ITEM.uom_code,
           ITEM.planner_code, --Bug 4424426
           NULL,
           NULL,
           NULL,
           MSI.inventory_item_id,
           MSI.mrp_safety_stock_code,
					 MSI.mrp_safety_stock_percent,
           MSI.safety_stock_bucket_days,
           MP.organization_code
     Order By MST.organization_id,
              MSI.segment1,
              MST.period_start_date;


  l_cnt                                NUMBER := 0;

----------------------------------------------------------------
-- begin
-----------------------------------------------------------------

Begin

  t_pub                       := COMPANYNAMELIST();
  t_pub_id                    := NUMBERLIST();
  t_pub_site                  := COMPANYSITELIST();
  t_pub_site_id               := NUMBERLIST();
  t_customer                  := COMPANYNAMELIST();
  t_customer_id               := NUMBERLIST();
  t_customer_site             := COMPANYSITELIST();
  t_customer_site_id          := NUMBERLIST();
  t_item_name                 := ITEMNAMELIST();
  t_item_desc                 := ITEMDESCLIST();
  t_base_item_name            := ITEMNAMELIST();
  t_supp                      := COMPANYNAMELIST();
  t_supp_id                   := NUMBERLIST();
  t_supp_site                 := COMPANYSITELIST();
  t_supp_site_id              := NUMBERLIST();
  t_master_item_name          := ITEMNAMELIST();
  t_master_item_desc          := ITEMDESCLIST();
  t_supp_item_name            := ITEMNAMELIST();
  t_supp_item_desc            := ITEMDESCLIST();
  t_type                      := NUMBERLIST();
  b_bkt_index                 := NUMBERLIST();
  b_bkt_type                  := NUMBERLIST();
  
  i_bkt_index                 := NUMBERLIST();
  i_bkt_type                  := NUMBERLIST();


  If FND_GLOBAL.conc_request_id > 0 Then
    p_retcode := 0 ;      -- Bug 4560452
    p_errbuf  := NULL ;

    l_user_id := FND_GLOBAL.user_id;
    l_user_name := FND_GLOBAL.user_name;
  End If;

  If l_user_id Is NULL Then
    l_language_code := 'US';
  Else
    l_language := fnd_preference.get(UPPER(l_user_name),'WF','LANGUAGE');
    If l_language Is Not NULL Then
      SELECT language_code
      INTO   l_language_code
      FROM   fnd_languages
      WHERE  nls_language = l_language;
    ELSE
      l_language_code := 'US';
    End If;
  End If;


--dbms_output.put_line('At 1');
  Select compile_designator
    Into p_designator
    From MSC_PLANS
   Where plan_id = p_plan_id;

--dbms_output.put_line('Designator : ' || p_designator);

  If p_org_code Is Not NULL Then
    p_inst_code := substr(p_org_code,1,instr(p_org_code,':')-1);
    --dbms_output.put_line('p_inst_code := ' || p_inst_code);
    begin
    select instance_id
    into   p_sr_instance_id
    from   MSC_APPS_INSTANCES
    where  instance_code = p_inst_code;
    --dbms_output.put_line('p_sr_instance_id := ' || p_sr_instance_id);

    select sr_tp_id
    into   p_org_id
    from   MSC_TRADING_PARTNERS
    where  organization_code = p_org_code and
           sr_instance_id = p_sr_instance_id and
           partner_type = 3 and
           company_id is NULL;
    --dbms_output.put_line('p_org_id := ' || p_org_id);
    Exception
      when others then
        p_sr_instance_id := NULL;
        p_org_id := NULL;
    End;
  Else
    p_org_id := NULL;
    p_sr_instance_id := NULL;
  End If;

  --------------------------------------------------------------------------
  -- set the standard date as canonical date
  --------------------------------------------------------------------------
-- Bug 4549069
  If (p_horizon_start is NULL) Then
    l_horizon_start := SYSDATE;
  Else
    l_horizon_start := FND_DATE.Canonical_To_Date(p_horizon_start);
  End If;

  If (p_horizon_end is NULL) then
    l_horizon_end := sysdate +365;
  Else
    l_horizon_end := FND_DATE.Canonical_To_Date(p_horizon_end);
  End If;


  log_message('l_horizon_start: '||l_horizon_start);
  log_message('l_horizon_end: '||l_horizon_end);


  l_log_message := get_message('MSC','MSC_PUB_SS',l_language_code) || ' ' || fnd_global.local_chr(10) ||
    get_message('MSC','MSC_X_PUB_PLAN',l_language_code) || ': ' || p_designator || fnd_global.local_chr(10);


  if (l_horizon_start > l_horizon_end) THEN
    l_log_message := l_log_message || get_message('MSC','MSC_X_PUB_START_DATE',l_language_code) || ' ' || l_horizon_start || fnd_global.local_chr(10);
    l_log_message := l_log_message || get_message('MSC','MSC_X_PUB_END_DATE',l_language_code) || ' ' || l_horizon_end || fnd_global.local_chr(10);
    l_log_message := l_log_message || get_message('MSC','MSC_X_PUB_DATE_MISMATCH',l_language_code) || ' ' || fnd_global.local_chr(10);
    log_message(l_log_message);
    RETURN;
  End If;

  IF p_org_code IS NOT NULL THEN
    l_log_message := l_log_message || get_message('MSC','MSC_X_PUB_ORG',l_language_code) || ': ' || p_org_code || fnd_global.local_chr(10);
  END IF;


  IF p_item_id IS NOT NULL THEN
    SELECT item_name
      INTO l_item_name
      FROM msc_items
      WHERE inventory_item_id = p_item_id;
    l_log_message := l_log_message || get_message('MSC','MSC_X_PUB_ITEM',l_language_code) || ': ' || l_item_name || fnd_global.local_chr(10);
  End If;

  IF p_planner_code IS NOT NULL THEN
    l_log_message := l_log_message || get_message('MSC','MSC_X_PUB_PLANNER',l_language_code) || ': ' || p_planner_code || fnd_global.local_chr(10);
  END IF;

  IF p_planning_gp IS NOT NULL THEN
    l_log_message := l_log_message || get_message('MSC','MSC_X_PUB_PLAN_GP',l_language_code) || ': ' || p_planning_gp || fnd_global.local_chr(10);
  END IF;

  IF p_project_id IS NOT NULL THEN
    l_log_message := l_log_message || get_message('MSC','MSC_X_PUB_PROJ_NUM',l_language_code) || ': ' || p_project_id || fnd_global.local_chr(10);
  END IF;

  IF p_task_id IS NOT NULL THEN
    l_log_message := l_log_message || get_message('MSC','MSC_X_PUB_TASK_NUM',l_language_code) || ': ' || p_task_id || fnd_global.local_chr(10);
  END IF;

  IF p_abc_class IS NOT NULL THEN
    l_log_message := l_log_message || get_message('MSC','MSC_X_PUB_ABC_CLASS',l_language_code) || ': ' || p_abc_class || fnd_global.local_chr(10);
  END IF;
  Log_Message(l_log_message);


  FND_FILE.Put_Line(FND_FILE.LOG, 'Deleting old safety stock matching the filter criteria');

--dbms_output.put_line('At 2');

  Open Safety_Stock_C(p_plan_id
                     ,p_org_id
                     ,p_sr_instance_id
                     ,l_horizon_start
                     ,l_horizon_end
                     ,p_planner_code
                     ,p_abc_class
                     ,p_item_id
                     ,p_planning_gp
                     ,p_project_id
                     ,p_task_id
                     ,p_item_category);

  Fetch Safety_Stock_C BULK COLLECT Into t_sr_instance_id,
                                         t_org_id,
                                         t_base_item_id,
                                         t_item_id,
                                         t_period_start_date,
                                         t_bucket_start,
                                         t_bucket_end,
                                         t_days_in_bkt,
                                         t_bucket_type,
                                         t_bucket_index,
                                         t_pub_id,
                                         t_pub,
                                         t_pub_site_id,
                                         t_pub_site,
                                         t_item_name,
                                         t_item_desc,
                                         t_base_item_name,
                                         t_uom_code,
                                         t_planner_code,
                                         t_planning_gp,
                                         t_proj_NUMBER,
                                         t_task_NUMBER,
                                         t_msi_item_id,
                                         t_ss_code,
                                         t_ss_percent,
                                         t_ss_days,
                                         t_org_code,
                                         t_qty;
  Close Safety_Stock_C;

  --dbms_output.put_line('At 4');

  FND_FILE.Put_Line(FND_FILE.Output, To_Char(SYSDATE,'DD-MON-YYYY HH24:MI')||' '||
                                     Lpad('XXWC Publish Safety Stock',40));
  FND_FILE.Put_Line(FND_FILE.Output, ' ');
  FND_FILE.Put_Line(FND_FILE.Output, 'Org'||' '||
                                     Rpad('Item Number',40)||' '||
                                     Rpad('Description',80)||' '||
                                     'Bucket Date'||' '||
                                     'Quantity'
                   );
  FND_FILE.Put_Line(FND_FILE.Output, '---'||' '||
                                     Rpad('----',40,'-')||' '||
                                     Rpad('-----',80,'-')||' '||
                                     '-----------'||' '||
                                     '--------'
                   );

  IF t_org_id IS NOT NULL AND t_org_id.COUNT > 0 Then

    --dbms_output.put_line ('Records fetched by cursor ss := ' || t_org_id.COUNT);
    FND_FILE.Put_Line(FND_FILE.LOG, 'Records fetched by safety stock cursor : ' || t_org_id.COUNT);
    --dbms_output.put_line('At 5');
      
    For j in 1..t_org_id.COUNT Loop

      l_cnt := l_cnt + 1;
        
      If p_overwrite = 'Y' Then 
        Delete From MTL_SAFETY_STOCKS
         Where organization_id = t_org_id(j)
           And inventory_item_id = t_msi_item_id(j)
           And effectivity_date >= t_period_start_date(j);

        -- populate Inventory safety stock table with same information
        MTL_SAFETY_STOCKS_PKG.Insert_Safety_Stocks(org_id     => t_org_id(j)
                                                  ,item_id    => t_msi_item_id(j)
                                                  ,ss_code    => 1           -- 1=User-defined quantity 2=User-defined percentage 3=Mean absolute deviation (MAD)
                                                  ,forc_name  => NULL
                                                  ,ss_percent => t_ss_percent(j)
                                                  ,srv_level  => NULL
                                                  ,ss_date    => t_period_start_date(j)
                                                  ,ss_qty     => t_qty(j)
                                                  ,login_id   => FND_GLOBAL.Login_Id
                                                  ,user_id    => FND_GLOBAL.user_id
                                                  );
      End If;
        
      FND_FILE.Put_Line(FND_FILE.Output, t_org_code(j)||' '||
                                         Rpad(t_item_name(j),40)||' '||
                                         Rpad(Substr(t_item_desc(j),1,80),80)||' '||
                                         To_Char(t_period_start_date(j),'DD-MON-YYYY')||' '||
                                         To_Char(t_qty(j),'99,990.00')
                       );
    End Loop;
  End If;

  COMMIT;

  FND_FILE.Put_Line(FND_FILE.Output, ' ');
  FND_FILE.Put_Line(FND_FILE.Output, l_cnt||' records processed');
  FND_FILE.Put_Line(FND_FILE.Log, l_cnt||' records processed');

Exception
  When OTHERS Then
  --dbms_output.put_line('Error in publish safety stock proc: ' ||sqlerrm);
    FND_FILE.Put_Line(FND_FILE.LOG, 'Error in publish safety stock procedure: ' ||sqlerrm);
    FND_FILE.Put_Line(FND_FILE.LOG, 'Cannot Publish Safety Stock data ');
    p_retcode :=  2 ;    -- Bug 4560452
    p_errbuf  := SQLERRM ;

End Publish_Safety_Stocks;

/**************************************************************************************************************************************************************/
/* Generate_Safety_Stock : create MTL safety stock from MRP forecast                                                                                          */
/*Version 1.1       Updated 1/30/2013 - Pull Processing Lead Time instead of Safety Stock Bucket Days and remove MRP Planned % Restriction                    */
/*Version 1.2       Updated 8/14/2013 - Remove round order quantity logic and always round safety stock calculation to 2 decimals - TMS Ticket 20130814-02113 */
/*Version 1.3       Updated 2/6/2014 - Parameter for log file - TMS Ticket 20140110-00053                                                                     */
/*Version 1.4       Updated 12/16/2016  - P.Vamshidhar - TMS#20160920-00081  - Spike in EBSPRD 'XXWC Generate Safety Stocks - Inventory’ program              */	
/**************************************************************************************************************************************************************/
Procedure Generate_Safety_Stock(p_errbuf            OUT NOCOPY VARCHAR2,
                                p_retcode           OUT NOCOPY NUMBER,
                                p_org_id            IN  NUMBER,
                                p_forecast_set      IN  VARCHAR2,
                                p_item_category     IN  VARCHAR2,
                                p_horizon_date      IN  VARCHAR2,
                                p_overwrite         IN  VARCHAR2,
                                p_write_log         IN  VARCHAR2,
                                p_write_output      IN  VARCHAR2) Is

  -- retrieve all items within organization that is eliglible for MRP Planning
  Cursor C_Items Is
    Select MSI.organization_id,
           MP.organization_code,
           MSI.inventory_item_id,
           MSI.segment1,
           MSI.description,
           --Nvl(MSI.safety_stock_bucket_days, 0) safety_stock_bucket_days, --removed 1/30/2013 by Lee Spitzer to use processing lead time
           Nvl(MSI.full_lead_time,0) safety_stock_bucket_days, --updated 1/30/2013 --Lee Spitzer to full processing lead time 
           Decode(MSI.mrp_safety_stock_percent,NULL, 0, MSI.mrp_safety_stock_percent/100) mrp_safety_stock_percent,
           MSI.rounding_control_type,            -- 1=round-up CEIL  2=leave as 2 decimal
           MP.calendar_code
      From MTL_SYSTEM_ITEMS_B MSI,
           MTL_PARAMETERS MP,
           MTL_ITEM_CATEGORIES MIC,
           MTL_CATEGORIES_KFV MC,
           MTL_CATEGORY_SETS MCS
     --Where MSI.mrp_safety_stock_code = 2  removed condition on 1/30/2013           -- MRP Planned % updated 1/30/2013 --
       WHERE MSI.organization_id = p_org_id
       And MSI.organization_id = MP.organization_id
       And MSI.organization_id = MIC.organization_id
       And MSI.inventory_item_id = MIC.inventory_item_id
       And MIC.category_set_id = MCS.category_set_id
       And MCS.category_set_name = 'Inventory Category'
       And MIC.category_id = MC.category_id
       And MC.concatenated_segments = Nvl(p_item_category, MC.concatenated_segments)
       And Exists (Select 'x' From MRP_FORECAST_DESIGNATORS MFD, MRP_FORECAST_ITEMS MFI
                    Where MFD.organization_id = p_org_id
                      And MFD.forecast_set = p_forecast_set
                      And MFD.organization_id = MFI.organization_id
                      And MFD.forecast_designator = MFI.forecast_designator
                      And MFI.inventory_item_id = MSI.inventory_item_id)
     Order By MSI.organization_id,
              MSI.segment1;

  -- retrieve calendar periods 
  Cursor C_Calendar_Dates(p_horizon_date DATE, p_calendar_code  VARCHAR2) Is
    Select D1.period_start_date,
           Min(D2.period_start_date-1) period_end_date
      From BOM_PERIOD_START_DATES D1,
           BOM_PERIOD_START_DATES D2
     Where D1.period_start_date Between (Select Max(period_start_date) From BOM_PERIOD_START_DATES D3 -- derive start date of current period
                                          Where D3.period_start_Date < Trunc(SYSDATE)
                                            And D3.calendar_code = D1.calendar_code
                                        ) 
                    And p_horizon_date
       And D2.period_start_date > D1.period_start_date
       and D1.calendar_code = p_calendar_code
       and D1.calendar_code = D2.calendar_code
      Group By D1.period_start_date
      Order BY D1.period_start_date;

  -- retrieve forecast data for organization / forecast set / item / date range
  Cursor C_Forecasts(p_item_id NUMBER, p_start_date DATE, p_end_date DATE) Is
    Select Nvl(Sum(original_forecast_quantity),0)
      From MRP_FORECAST_DESIGNATORS MF,
           MRP_FORECAST_DATES FD
     Where MF.organization_id = p_org_id
       And MF.forecast_set = p_forecast_set
       And MF.organization_id = FD.organization_id
       And MF.forecast_designator = FD.forecast_designator
       And FD.inventory_item_id = p_item_id
       And FD.forecast_date Between p_start_date and p_end_date;

  l_horizon_date                       DATE := FND_DATE.Canonical_To_Date(p_horizon_date);

  l_forecast_quantity                  NUMBER;
  l_ss_quantity                        NUMBER;

  l_cnt                                NUMBER := 0;

  l_write_log         VARCHAR2(1) := nvl(p_write_log,'N');
  l_write_output      VARCHAR2(1) := nvl(p_write_output,'N');

Begin

  p_errbuf := 'Success';
  p_retcode := 0;
  
  IF l_write_log = 'Y' THEN --added 2/6/2014 TMS Ticket 20140110-00053
    -- output program parameters
    Log_Message('P_ORG_ID = '||p_org_id);
    Log_Message('P_FORECAST_SET = '||p_forecast_set);
    Log_Message('P_ITEM_CATEGORY = '||p_item_category);
    Log_Message('P_HORIZON_DATE = '||p_horizon_date||'    '||To_Char(l_horizon_date,'DD-MON-YYYY'));
    Log_Message('P_OVERWRITE = '||p_overwrite);
  END IF; --added 2/6/2014 TMS Ticket 20140110-00053
  
  IF l_write_output = 'Y' THEN --added 2/6/2014 TMS Ticket 20140110-00053
  -- output report headers
    FND_FILE.Put_Line(FND_FILE.Output, To_Char(SYSDATE,'DD-MON-YYYY HH24:MI')||' '||
                                       Lpad('XXWC Generate Safety Stock',40));
    FND_FILE.Put_Line(FND_FILE.Output, ' ');
    FND_FILE.Put_Line(FND_FILE.Output, 'Forecast Set: '||p_forecast_set);
  END IF; --added 2/6/2014 TMS Ticket 20140110-00053
  IF p_item_category IS NOT NULL THEN
    IF l_write_output = 'Y' THEN --added 2/6/2014 TMS Ticket 20140110-00053
      FND_FILE.Put_Line(FND_FILE.Output, 'Item Category: '||p_item_category);
    END IF; --added 2/6/2014 TMS Ticket 20140110-00053
  End If;

  IF l_write_output = 'Y' THEN --added 2/6/2014 TMS Ticket 20140110-00053  
    FND_FILE.Put_Line(FND_FILE.Output, 'Horizon Date:'||To_Char(l_horizon_date,'DD-MON-YYYY'));
    FND_FILE.Put_Line(FND_FILE.Output, 'Overwrite Flag:'||p_overwrite);
    FND_FILE.Put_Line(FND_FILE.Output, ' ');
    FND_FILE.Put_Line(FND_FILE.Output, 'Org'||' '||
                                       Rpad('Item Number',40)||' '||
                                       Rpad('Description',80)||' '||
                                       'Bucket Date'||' '||
                                       'Quantity'
                     );
    FND_FILE.Put_Line(FND_FILE.Output, '---'||' '||
                                       Rpad('----',40,'-')||' '||
                                       Rpad('-----',80,'-')||' '||
                                       '-----------'||' '||
                                       '--------'
                     );
    -- loop for all items
  END IF; --added 2/6/2014 TMS Ticket 20140110-00053
  
  FOR R_Item IN C_Items Loop

    -- Added below block in Rev 1.4 by Vamshi. -- Begin
    For R_Date In C_Calendar_Dates(l_horizon_date, R_Item.calendar_code) Loop
       
      If p_overwrite = 'Y' Then 
        Delete From MTL_SAFETY_STOCKS
         Where organization_id = p_org_id
           And inventory_item_id = R_Item.inventory_item_id
           And effectivity_date >= R_Date.period_start_date;         
      End If;
	  
    End Loop;  
	COMMIT; 
	FND_FILE.PUT_LINE(FND_FILE.LOG,' Delete from MTL_SAFETY_STOCKS '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
      -- Added above block in Rev 1.4 by Vamshi. -- End
  
    -- loop for calendar period dates within range  
    For R_Date In C_Calendar_Dates(l_horizon_date, R_Item.calendar_code) Loop

      -- retrieve forecast quantity    
      Open C_Forecasts(R_Item.inventory_item_id, R_Date.period_start_date, R_Date.period_end_date);
      Fetch C_Forecasts Into l_forecast_quantity;
      Close C_Forecasts;
      
      /*Added new rounding logic on 8/14/2013 per TMS Ticket 20130814-02113 to always round to 2 decimals reguardless of the round order quantities setting of the item */ 
      If Nvl(l_forecast_quantity,0) > 0 Then
          l_ss_quantity := Round((l_forecast_quantity / 28) * R_Item.safety_stock_bucket_days * R_Item.mrp_safety_stock_percent,2);
      Else
        l_ss_quantity := 0;
      End If;
      
      IF l_write_log = 'Y' THEN --added 2/6/2014 TMS Ticket 20140110-00053

        Log_Message('Item: '||R_Item.segment1||
                    ' Period Date: '||Greatest(Trunc(SYSDATE), R_Date.period_start_date)||
                    ' Forecast Qty: '||l_forecast_quantity||
                    ' SS qty: '||l_ss_quantity);
      END IF; --added 2/6/2014 TMS Ticket 20140110-00053
    
      l_cnt := l_cnt + 1;
        
      If p_overwrite = 'Y' Then 
        /* Below block commented in Rev 1.4 by Vamshi
		 Delete From MTL_SAFETY_STOCKS
         Where organization_id = p_org_id
           And inventory_item_id = R_Item.inventory_item_id
           And effectivity_date >= R_Date.period_start_date;
         */
        -- populate Inventory safety stock table with same information
        MTL_SAFETY_STOCKS_PKG.Insert_Safety_Stocks(org_id     => p_org_id
                                                  ,item_id    => R_Item.inventory_item_id
                                                  ,ss_code    => 1           -- 1=User-defined quantity 2=User-defined percentage 3=Mean absolute deviation (MAD)
                                                  ,forc_name  => NULL
                                                  ,ss_percent => R_Item.mrp_safety_stock_percent
                                                  ,srv_level  => NULL
                                                  ,ss_date    => Greatest(Trunc(SYSDATE), R_Date.period_start_date)   -- handles first ss period date
                                                  ,ss_qty     => l_ss_quantity
                                                  ,login_id   => FND_GLOBAL.Login_Id
                                                  ,user_id    => FND_GLOBAL.user_id
                                                  );
      End If;

      IF l_write_output = 'Y' THEN --added 2/6/2014 TMS Ticket 20140110-00053
 
        -- output safet stock details
        FND_FILE.Put_Line(FND_FILE.Output, R_Item.organization_code||' '||
                                           Rpad(R_Item.segment1,40)||' '||
                                           Rpad(Substr(R_Item.description,1,80),80)||' '||
                                           To_Char(Greatest(Trunc(SYSDATE), R_Date.period_start_date),'DD-MON-YYYY')||' '||
                                           To_Char(l_ss_quantity,'99,990.00')
                         );
      
      END IF; --added 2/6/2014 TMS Ticket 20140110-00053
      
    End Loop;  -- date loop

  End Loop;  -- item loop


  IF l_write_output = 'Y' THEN --added 2/6/2014 TMS Ticket 20140110-00053

    FND_FILE.Put_Line(FND_FILE.Output, ' ');
    FND_FILE.Put_Line(FND_FILE.Output, l_cnt||' records processed');

  END IF; --added 2/6/2014 TMS Ticket 20140110-00053

  IF l_write_log = 'Y' THEN --added 2/6/2014 TMS Ticket 20140110-00053

    Log_Message(l_cnt||' records processed');

  END IF; --added 2/6/2014 TMS Ticket 20140110-00053


Exception
  When OTHERS Then
    ROLLBACK;
    IF l_write_log = 'Y' THEN --added 2/6/2014 TMS Ticket 20140110-00053
      Log_Message('Error in generate safety stock procedure: ' ||sqlerrm);
      Log_Message('Cannot generate Safety Stock data ');
    END IF; --added 2/6/2014 TMS Ticket 20140110-00053
    p_retcode :=  2 ;    -- Bug 4560452
    p_errbuf  := SQLERRM ;
End Generate_Safety_Stock;

/***************************************************************************************/
/* Log_Message : outputs message to concurrent request log file or to screen           */
/***************************************************************************************/
Procedure Log_Message(p_string IN VARCHAR2) IS
Begin
  IF fnd_global.conc_request_id > 0 Then
    FND_FILE.Put_Line(FND_FILE.LOG, p_string);
  Else
    DBMS_OUTPUT.Put_Line(p_string);
  End If;
End Log_Message;

/***************************************************************************************/
/* Get_Message : retrived message from FND_NEW_MESSAGES                                */
/***************************************************************************************/
Function Get_Message(p_app   IN VARCHAR2,
                     p_name  IN VARCHAR2,
                     p_lang  IN VARCHAR2
                    ) RETURN VARCHAR2 IS
  msg VARCHAR2(2000);

  CURSOR C1(app_name VARCHAR2, msg_name VARCHAR2, lang VARCHAR2) Is
    Select M.message_text
      From FND_NEW_MESSAGES M,
           FND_APPLICATION A
     Where M.message_name = msg_name 
       And M.language_code = lang
       And A.application_short_name = app_name
       And M.application_id = A.application_id;

Begin

  msg := NULL;

  Open C1(p_app, p_name, p_lang);
  Fetch C1 INTO msg;
  If (C1%NOTFOUND) Then
    msg := p_name;
  End If;
  Close C1;
  Return msg;
End Get_Message;

END XXWCMSC_MSC_PUB_SAFETY_STK_PKG;
/
