create or replace package body APPS.XXWC_MS_MASTER_ADMIN_PKG is
/*************************************************************************
  $Header XXWC_MS_MASTER_ADMIN_PKG.pks $
  Module Name: XXWC_MS_MASTER_ADMIN_PKG

  PURPOSE: To support the data extract for Master Administration Application

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        21-August-2015  Pahwa, Nancy    Initial Version TMS # 20150901-00129 

**************************************************************************/
  /*************************************************************************
    *   Function Name: get_user_name
    *
    *************************************************************************
    *   Purpose       : To get user information from HR source
    *   Parameter:
    *       IN
    *            nt_id        -User Id
    * ***********************************************************************/

  function get_user_name(nt_id in varchar2) return varchar2 is
    user_name varchar2(200);
  begin
    begin
      select first_name || ' ' || last_name
        into user_name
    -- from  apxcmmn.hr_ad_all_users_vw@APEX_XPEPRD.HSI.HUGHESSUPPLY.COM
       from XXWC_HDS_USER_PULL_MV
       where employee_id = nt_id;
    exception
      when no_data_found then
        user_name := 'NOT FOUND';
    end;
    RETURN(user_name);
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-20001,
                              'XXWC_WC_MASTER_ADMIN_PKG.get_user_name: ' || SQLERRM);

  end;
    /*************************************************************************
    *   Function Name: get_responsibility_name
    *
    *************************************************************************
    *   Purpose       : To get name of a responsibility
    *   Parameter:
    *       IN
    *            p_resp_id        , p_app_id
    * ***********************************************************************/
  function get_responsibility_name(p_resp_id in number, p_app_id in number)
    return varchar2 is
    l_responsibility_name varchar2(250);
  begin
    begin
      select responsibility_name
        into l_responsibility_name
        from XXWC.XXWC_MS_RESPONSIBILITY_TBL
       where RESPONSIBILITY_ID = p_resp_id
         and app_id = p_app_id;
    exception
      when no_data_found then
        l_responsibility_name := 'NOT FOUND';
    end;
    RETURN(l_responsibility_name);
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-20001,
                              'XXWC_WC_MASTER_ADMIN_PKG.get_responsibility_name: ' ||
                              SQLERRM);

  end;
   /*************************************************************************
    *   Function Name: get_role_name
    *
    *************************************************************************
    *   Purpose       : To get name of a responsibility
    *   Parameter:
    *       IN
    *            p_role_id
    * ***********************************************************************/
  function get_role_name(p_role_id in number) return varchar2 is
    l_role_name varchar2(250);
  begin
    begin
      select role_name
        into l_role_name
        from XXWC.XXWC_MS_ROLES_TBL
       where role_id = p_role_id;

    exception
      when no_data_found then
        l_role_name := 'NOT FOUND';
    end;
    RETURN(l_role_name);
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-20001,
                              'XXWC_WC_MASTER_ADMIN_PKG.get_role_name: ' || SQLERRM);

  end;
end XXWC_MS_MASTER_ADMIN_PKG;
/