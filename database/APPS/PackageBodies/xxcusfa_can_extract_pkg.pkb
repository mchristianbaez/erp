CREATE OR REPLACE PACKAGE BODY APPS.xxcusfa_can_extract_pkg IS

  /********************************************************************************
  
  File Name: XXCUSGL_CAN_EXTRACT_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send Oracle Fixed Assets journals to HDS Canada (SX.e)
           in Canadian currency
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/29/2010    Kathy Poling    Initial creation of the procedure
                                        Service Ticket
  1.1     10/04/2011    Luong Vu        Project resumed.  Modified program for R12
                                        production instance (Rebate).
  
  ********************************************************************************/
  PROCEDURE canada_fa_extract(errbuf        OUT VARCHAR2
                             ,retcode       OUT NUMBER
                             ,p_lob         IN VARCHAR2
                             ,p_period_name IN VARCHAR2) IS
  
    --Intialize Variables
    l_period_name    VARCHAR2(20);
    l_file_period    VARCHAR2(20);
    l_err_msg        VARCHAR2(2000);
    l_err_code       NUMBER;
    l_file_name      VARCHAR2(150);
    l_file_name_temp VARCHAR2(150);
    l_file_dir       VARCHAR2(100);
    l_file_handle    utl_file.file_type;
    l_sid            VARCHAR2(10);
    l_sec            VARCHAR2(150);
    l_amount         VARCHAR2(150);
    l_je_source      VARCHAR2(10) := 'Assets';
    l_ledger_id      NUMBER := 2041;
    l_procedure_name VARCHAR2(75) := 'XXCUSGL_CAN_EXTRACT_PKG.CANADA_FA_EXTRACT';
    l_distro_list    fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    pl_dflt_email    fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';
    pl_email         fnd_user.email_address%TYPE;
    pl_instance      VARCHAR2(100);
    pl_sender        VARCHAR2(100);
    pl_errorstatus   NUMBER;
    p_host           VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage  VARCHAR2(4000);
    --
  BEGIN
  
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
  
    --pl_sender     := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --l_file_dir    := '/xx_iface/' || l_sid || '/outbound/uc4';
    l_file_dir    := 'ORACLE_INT_LITEMOR_FA_EXTRACT'; --Version 1.3
    l_period_name := p_period_name;
    l_file_period := REPLACE(p_period_name, '-', '');
  
    FOR c_rec IN (SELECT DISTINCT cc.segment1
                                 ,jh.je_category
                                 ,jh.je_source
                                 ,jh.currency_code
                                 ,jl.period_name
                                 ,jh.ledger_id
                    FROM gl.gl_je_headers        jh
                        ,gl.gl_je_lines          jl
                        ,gl.gl_code_combinations cc
                   WHERE jh.je_header_id = jl.je_header_id
                     AND jl.code_combination_id = cc.code_combination_id
                     AND jh.je_source = l_je_source
                     AND jh.period_name = p_period_name
                     AND jh.ledger_id = l_ledger_id)
    LOOP
    
      --Set the file name and open the file
      l_file_name_temp := 'TEMP_' || c_rec.je_category || p_lob || '_' ||
                          l_file_period || '.csv';
    
      l_file_name := 'CAN_FA_' || c_rec.je_category || p_lob || '_' ||
                     l_file_period || '.csv';
    
      fnd_file.put_line(fnd_file.log,
                        'Filename generated : ' || l_file_name);
      fnd_file.put_line(fnd_file.output,
                        'Filename generated : ' || l_file_name);
      dbms_output.put_line('before fopen');
      l_file_handle := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
      dbms_output.put_line('you are here');
      -- Get extract information begin
      FOR c_gl IN (SELECT jh.je_category
                         ,jh.je_source
                         ,jh.currency_code
                         ,jh.actual_flag
                         ,jl.je_header_id
                         ,jl.je_line_num
                         ,jl.ledger_id
                         ,jl.code_combination_id
                         ,jl.period_name
                         ,jl.accounted_dr
                         ,jl.accounted_cr
                         ,jl.description
                         ,cc.segment1
                         ,cc.segment2
                         ,cc.segment3
                         ,cc.segment4
                         ,cc.segment5
                     FROM gl.gl_je_headers        jh
                         ,gl.gl_je_lines          jl
                         ,gl.gl_code_combinations cc
                    WHERE jh.je_header_id = jl.je_header_id
                      AND jl.code_combination_id = cc.code_combination_id
                      AND jh.period_name = p_period_name
                      AND jh.ledger_id = c_rec.ledger_id
                      AND cc.segment1 = c_rec.segment1
                      AND jh.je_category = c_rec.je_category
                      AND jh.je_source = l_je_source)
      LOOP
      
        IF c_gl.accounted_dr IS NULL THEN
          l_amount := '-' || c_gl.accounted_cr;
        ELSE
          l_amount := c_gl.accounted_dr;
        END IF;
      
        --Write detail rows
        utl_file.put_line(l_file_handle,
                          c_gl.segment2 || ',' || c_gl.segment3 || ',' ||
                           c_gl.segment4 || ',' || c_gl.segment5 || ',' ||
                           l_amount || ',' || c_gl.description || ',' || ',' || ',' || ',' || ',' || ',' || ',' || ',' || ',' || ',' || ',');
      
      END LOOP;
      dbms_output.put_line('after for loop');
      --Close the file
      l_sec := 'Closing the File; ';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      utl_file.fclose(l_file_handle);
      utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                       l_file_name);
    
    
      COMMIT;
    END LOOP;
    utl_file.fclose(l_file_handle);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Account Extract Process in GL_CAN_EXTRACT_PKG.GL_EXTRACT_CANADA package with Program Error Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running GL Extract Process in GL_CAN_EXTRACT_PKG.GL_EXTRACT_CANADA package with OTHERS Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
  END canada_fa_extract;

END xxcusfa_can_extract_pkg;
/
