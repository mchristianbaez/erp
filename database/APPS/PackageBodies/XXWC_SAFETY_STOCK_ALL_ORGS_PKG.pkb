CREATE OR REPLACE PACKAGE BODY APPS.xxwc_safety_stock_all_orgs_pkg
AS
/******************************************************************************
   $Header XXWC_SAFETY_STOCK_ALL_ORGS_PKG$
   NAME:       XXWC_SAFETY_STOCK_ALL_ORGS_PKG

   PURPOSE:    Automate Generate Safety Stock program for all organizations - TMS# 20131120-00056


   REVISIONS:
   Ver        Date         Author            Description
   ---------  -----------  ---------------   -------------------------------------
   1.0        23-APR-2013  Dheeresh Chintala  Initial version TMS# 20131120-00056
   1.1        03-SEP-2014  Manjula Chellappan Added parameter Horizon_date in Main procedure
					      for TMS # 20140903-00150   

******************************************************************************/

   -- Global variables
   g_application     fnd_application.application_short_name%TYPE    := 'XXWC';
   g_cp_short_code   fnd_concurrent_programs.concurrent_program_name%TYPE
                                              := 'XXWC_GENERATE_SAFETY_STOCK';
   --XXWC Generate Safety Stocks - Inventory
   g_dflt_email      fnd_user.email_address%TYPE
                                        := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE main (
      retcode          OUT      NUMBER,
      errbuf           OUT      VARCHAR2,
      p_region         IN       VARCHAR2,
--Added the parameter by Manjula on 03-Sep-14 for TMS # 20140903-00150      
      p_horizon_date   IN       VARCHAR2
   )
/*************************************************************************
        Procedure : Main

        PURPOSE:   Automate Generate Safety Stock program for all organizations - TMS# 20131120-00056
        Parameter:
        REVISIONS:
   Ver        Date         Author            Description
   ---------  -----------  ---------------   ------------------------------------
   1.0        23-APR-2013  Dheeresh Chintala  Initial version TMS# 20131120-00056
   1.1        03-SEP-2014  Manjula Chellappan Added parameter Horizon_date for TMS # 20140903-00150   


 ************************************************************************/   
   IS
      CURSOR cur_inv_orgs (n_org_id IN NUMBER)
      IS
         SELECT   ood.organization_id, ood.organization_code,
                  ood.organization_name, ood.set_of_books_id,
                  ood.chart_of_accounts_id, ood.legal_entity,
                  mp.attribute9 region
             FROM org_organization_definitions ood, mtl_parameters mp
            WHERE 1 = 1
              AND ood.operating_unit = n_org_id
              AND ood.disable_date IS NULL
              AND ood.organization_code NOT IN ('WCC', 'MST')
              --WC Corporate Organization, WC Item Master
              AND ood.organization_id = mp.organization_id
              AND mp.attribute9 = NVL (p_region, mp.attribute9)
         ORDER BY ood.organization_code ASC;

      CURSOR c_location (p_organization_id IN NUMBER)
      IS
         SELECT location_id
           FROM hr_all_organization_units
          WHERE organization_id = p_organization_id
         --AND    p_supply_type = 2
         UNION
         SELECT TO_NUMBER (attribute15) location_id
           FROM mtl_parameters
          WHERE organization_id = p_organization_id;

      --AND    p_supply_type = 1;
      TYPE l_inv_orgs_tbl IS TABLE OF cur_inv_orgs%ROWTYPE
         INDEX BY BINARY_INTEGER;

      l_inv_orgs_rec          l_inv_orgs_tbl;
      l_request_data          VARCHAR2 (20)  := '';
      l_request_id            NUMBER         := 0;
      l_req_id                NUMBER         := 0;
      l_org_id                NUMBER         := 0;
      l_user_id               NUMBER         := 0;
      l_forecast_designator   VARCHAR2 (10);
      l_category_set_id       NUMBER;
      l_structure_id          NUMBER;
      l_message               VARCHAR2 (200);
      l_exception             EXCEPTION;
      l_req_approval          VARCHAR2 (1);
      l_sec                   VARCHAR2 (100);

   BEGIN
      l_sec := 'Start of Procedure MAIN';
      l_request_data := fnd_conc_global.request_data;
      l_req_id := fnd_global.conc_request_id;
      -- l_date := to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');


      BEGIN
         SELECT category_set_id, structure_id
           INTO l_category_set_id, l_structure_id
           FROM mtl_category_sets
          WHERE category_set_name = 'Inventory Category';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_message :=
               SUBSTR (   'Could not find Inventory Category Set'
                       || SQLCODE
                       || SQLERRM,
                       1,
                       200
                      );
            RAISE l_exception;
      END;

      IF l_request_data IS NULL
      THEN
         l_request_data := '1';
         l_org_id := mo_global.get_current_org_id;
         l_user_id := fnd_global.user_id;
         fnd_file.put_line (fnd_file.LOG, '');
         fnd_file.put_line (fnd_file.LOG, 'l_org_id =' || l_org_id);
         fnd_file.put_line (fnd_file.LOG, 'l_user_id =' || l_user_id);
         fnd_file.put_line (fnd_file.LOG, '');
         l_sec := 'Before opening the cursor - CUR_INV_ORGS';

         OPEN cur_inv_orgs (l_org_id);

         FETCH cur_inv_orgs
         BULK COLLECT INTO l_inv_orgs_rec;

         CLOSE cur_inv_orgs;

         IF l_inv_orgs_rec.COUNT = 0
         THEN
            fnd_file.put_line
               (fnd_file.LOG,
                'No active inventory organizations found for operating unit WhiteCap'
               );
         ELSE
            l_sec :=
                    'Trigger Generate Safety Stock program for every Inv Org';

                 --wc_inv_orgs_rec.count >0
            -- We will kick off for every single inventory orgs and not wait to check how many are running at a time
            FOR indx IN 1 .. l_inv_orgs_rec.COUNT
            LOOP
               BEGIN
                  SELECT forecast_designator
                    INTO l_forecast_designator
                    FROM mrp.mrp_forecast_designators
                   WHERE 'FS' || l_inv_orgs_rec (indx).organization_code =
                                                           forecast_designator
                     AND organization_id =
                                         l_inv_orgs_rec (indx).organization_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_forecast_designator := NULL;
               END;

               -- for r_location in c_location(wc_inv_orgs_rec(Indx).organization_id) loop
               fnd_file.put_line (fnd_file.LOG,
                                     'org ='
                                  || l_inv_orgs_rec (indx).organization_name
                                 );

               IF l_forecast_designator IS NOT NULL
               THEN
                  l_sec :=
                        'Before submitting the concurrent program - '
                     || g_cp_short_code;
                  l_request_id :=
                     fnd_request.submit_request
                           (application      => g_application,               --
                            program          => g_cp_short_code,             --
                            description      => '',
                            start_time       => '',
                            sub_request      => TRUE,
                            argument1        => l_inv_orgs_rec (indx).organization_id,
--Organization Id                                                                                  ,
                            argument2        => l_forecast_designator,
                            --Forecast                                                                  ,
                            argument3        => NULL,
                            --Item Category                                                 ,
--updated the parameter by Manjula on 03-Sep-14 for TMS # 20140903-00150                            
                            argument4        => p_horizon_date,
                            --Horizon End Date                                                          ,
                            argument5        => 'Y',
                            --Third Sort                                                ,
                            argument6        => 'N',
                            --Items From                                                ,
                            argument7        => 'N'                 --Items To
                           );
               ELSE
                  l_request_id := 0;
               END IF;

               IF l_request_id > 0
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Submitted XXWC Generate Safety Stocks - Inventory for Org ='
                      || l_inv_orgs_rec (indx).organization_name
                      || ', Request Id ='
                      || l_request_id
                     );
               ELSE
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Failed to submit XXWC Generate Safety Stocks - Inventory for Org ='
                      || l_inv_orgs_rec (indx).organization_name
                     );
               END IF;

               fnd_conc_global.set_req_globals (conc_status       => 'PAUSED',
                                                request_data      => l_request_data
                                               );
               l_request_data := TO_CHAR (TO_NUMBER (l_request_data) + 1);
            --  END loop;
            END LOOP;
         END IF;                       -- checking if wc_inv_orgs_rec.count =0

         l_sec :=
               'After submitting the concurrent program - ' || g_cp_short_code;
         COMMIT;
         retcode := 0;
         errbuf :=
            'Child request[s] completed. Exit XXWC Generate Safety Stocks - Inventory For All Orgs';
         fnd_file.put_line (fnd_file.LOG, errbuf);
      ELSE
         retcode := 1;
         errbuf :=
            'Child requests not submitted becuase no active orgs. Exit XXWC Generate Safety Stocks - Inventory For All Orgs';
         fnd_file.put_line (fnd_file.LOG, errbuf);
      END IF;
   EXCEPTION
      WHEN l_exception
      THEN
         fnd_file.put_line
            (fnd_file.LOG,
                'Error in APPS.XXWC_SAFETY_STOCK_ALL_ORGS_PKG count not find Inventory Category Set ='
             || SQLCODE
             || SQLERRM
            );
      WHEN OTHERS
      THEN
         fnd_file.put_line
            (fnd_file.LOG,
                'Error in caller apps.APPS.XXWC_SAFETY_STOCK_ALL_ORGS_PKG.main ='
             || SQLERRM
            );
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => 'XXWC_SAFETY_STOCK_ALL_ORGS_PKG.MAIN',
             p_calling                => l_sec,
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SUBSTR (SQLERRM, 1, 2000),
             p_error_desc             => 'Error in XXWC Generate Safety Stocks - Inventory For All Orgs procedure, When Others Exception',
             p_distribution_list      => g_dflt_email,
             p_module                 => 'XXWC'
            );
         fnd_file.put_line
              (fnd_file.LOG,
                  'Error in caller apps.XXWC_SAFETY_STOCK_ALL_ORGS_PKG.MAIN ='
               || SQLERRM
              );
         ROLLBACK;
   END main;
END xxwc_safety_stock_all_orgs_pkg;
/
