CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_DEFAULT_RULE_PKG
/*************************************************************************
 Copyright (c) 2014 HD Supply
 All rights reserved.
**************************************************************************
  $Header XXWC_OM_DEFAULT_RULE_PKG $
  Module Name: XXWC_OM_DEFAULT_RULE_PKG.pkb

  PURPOSE: To Customize Defaulting Rules in Oracle.

  REVISIONS:
  Ver        Date        Author              Description
  ---------  ----------  ---------------     -------------------------
  1.0        03/05/2014  Gopi Damuluri       Initial Version
                                             TMS# 20140124-00027
                                             
  2.0        07/31/2014  Gopi Damuluri      TMS # 20140730-00271 (Restirct warehouse change 
                                              when the ship to changed)
  3.0         8/5/2014      Ram Talluri       TMS #20140805-00024 -Added function Get_subinv_iso_line-to return correct subinventory for ISO line.
**************************************************************************/
AS

--------------------------------------------------------------------------------
-- Procedure to derive Shipping Method of Original Line for Return Order Line
--------------------------------------------------------------------------------
-- Version   Date             Developer        Description
-- -------   -----            ----------       ---------------
--  1.0      05-Mar-2014      Gopi Damuluri    Initial Version
--------------------------------------------------------------------------------  
FUNCTION Get_Return_Ship_Code( p_database_object_name     IN  VARCHAR2
                              ,p_attribute_code           IN  VARCHAR2)
         RETURN VARCHAR2
IS
  l_ret_ship_code  VARCHAR2(30);
BEGIN 
   SELECT shipping_method_code
     INTO l_ret_ship_code 
     FROM apps.oe_order_lines_all 
    WHERE line_id = ONT_LINE_DEF_HDLR.G_RECORD.REFERENCE_LINE_ID;

RETURN l_ret_ship_code ;

EXCEPTION
WHEN OTHERS THEN
  RETURN NULL;
END Get_Return_Ship_Code;


--------------------------------------------------------------------------------
-- Procedure to validate Ship From Org when ShipTo Address is modified
--------------------------------------------------------------------------------
-- Version   Date        Developer        Description
-- -------   -----       ----------       ---------------
-- 2.0       07/31/2014  Gopi Damuluri    TMS # 20140730-00271 (Restirct warehouse change
--                                        when the ship to changed)
--------------------------------------------------------------------------------  
FUNCTION Get_Ship_From_Org( p_database_object_name     IN  VARCHAR2
                          , p_attribute_code           IN  VARCHAR2)
         RETURN NUMBER
IS
  l_ship_from_org  NUMBER;
BEGIN 
   SELECT mp.organization_id
     INTO l_ship_from_org 
     FROM apps.oe_order_headers_all  ooh
        , apps.mtl_parameters        mp
    WHERE ooh.header_id            = ONT_HEADER_DEF_HDLR.G_RECORD.HEADER_ID
      AND ooh.ship_from_org_id     = mp.organization_id;

RETURN l_ship_from_org ;

EXCEPTION
WHEN OTHERS THEN
  RETURN NULL;
END Get_Ship_From_Org;
/**************************************************************************
 FUNCTION NAME:Get_subinv_iso_line
  PURPOSE: To get applicable subinventory for internal order line.

  REVISIONS:
  Ver        Date        Author              Description
  ---------  ----------  ---------------     -------------------------
  3.0        8/5/2014      Ram Talluri       TMS #20140805-00024 -Added function Get_subinv_iso_line-to return correct subinventory for ISO line.                                                                                        
**************************************************************************/
FUNCTION Get_subinv_iso_line( p_database_object_name     IN  VARCHAR2
                              ,p_attribute_code           IN  VARCHAR2)
         RETURN VARCHAR2
IS
  l_subinv_code  VARCHAR2(30);
BEGIN 
   SELECT subinventory_code
     INTO l_subinv_code
     FROM apps.mtl_item_sub_defaults
    WHERE inventory_item_id = ONT_LINE_DEF_HDLR.G_RECORD.INVENTORY_ITEM_ID
      AND organization_id = ONT_LINE_DEF_HDLR.G_RECORD.SHIP_FROM_ORG_ID
      AND default_type=1;--default shipping subiventory of item.

RETURN l_subinv_code ;

EXCEPTION
WHEN OTHERS THEN
  l_subinv_code:='General';
  RETURN l_subinv_code;
END Get_subinv_iso_line;

END XXWC_OM_DEFAULT_RULE_PKG;
/