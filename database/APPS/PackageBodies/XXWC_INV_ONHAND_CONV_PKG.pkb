CREATE OR REPLACE PACKAGE BODY xxwc_inv_onhand_conv_pkg
IS
  /***************************************************************************
  *    script name: xxwc_inv_onhand_conv_pkg.pkb
  *
  *    interface / conversion name: Item On-hand conversion.
  *
  *    functional purpose: convert On-hand Qty using Interface
  *
  *    history:
  *
  *    version    date              author             description
  *****************************************************************
  *    1.0        04-SEP-2011      k.Tavva             Initial development.
  *    1.10       04-MAR-2013      S. Spivey           BOD update should only effect acct alias receipts
  *    1.11       05-APR-2018      Ashwin Sridhar      Added for TMS#20180404-00049--AH Harries Inv PUBD Conversion
  ****************************************************************/
PROCEDURE print_debug(
    p_print_str IN VARCHAR2)
IS
BEGIN
  --  IF g_debug = 'Y' THEN
  fnd_file.put_line (fnd_file.LOG, p_print_str);
  -- END IF;
  DBMS_OUTPUT.put_line (p_print_str);
END print_debug;
PROCEDURE validations
IS
  CURSOR c_onhadbal_stg
  IS
    SELECT ROWID,
      xobc.*
    FROM xxwc_onhand_bal_cnv xobc
    WHERE NVL (status, 'R') IN ('R','E'); --onhand qty
  l_errmsg                 VARCHAR2 (4000);
  l_item_error             VARCHAR2 (2) := 'N';
  l_item_err_details       VARCHAR2 (3000);
  l_transaction_type_id    NUMBER;
  l_organization_id        NUMBER;
  l_inventory_item_id      NUMBER;
  l_uom                    NUMBER;
  l_uom_code               VARCHAR2 (5);
  l_count                  NUMBER := 0;
  l_srl_num_control        NUMBER;
  l_lotcontrol             NUMBER;
  l_trans_action_id        NUMBER;
  l_trans_source_type_id   NUMBER;
  l_accts_pay_code_comb_id VARCHAR2 (30);
  l_processing_stg         NUMBER := 0;
  l_error_stg              NUMBER := 0;
  l_validated_stg          NUMBER := 0;
  l_exist                  VARCHAR2(1);
BEGIN
  SELECT COUNT (1)
  INTO l_processing_stg
  FROM xxwc_onhand_bal_cnv xobc
  WHERE NVL (status, 'R') IN ('R','E');
  UPDATE xxwc_onhand_bal_cnv a
  SET transaction_uom =
    (SELECT b.primary_uom_code
    FROM mtl_system_items b,
      mtl_parameters c
    WHERE a.item_segment    = b.segment1
    AND b.organization_id   = c.organization_id
    AND a.organization_code = c.organization_code
    )
  WHERE transaction_uom IS NULL;
  UPDATE xxwc_onhand_bal_cnv
  SET transaction_uom = DECODE (transaction_uom, 'PAR', 'PR', 'ROL', 'RL', 'PK', 'PKG', 'LB', 'LBS', 'PAL', 'PL', 'CAS', 'CS', transaction_uom);
  COMMIT;
  FOR r_onhadbal_stg IN c_onhadbal_stg
  LOOP
    l_errmsg            := NULL;
    l_item_error        := NULL;
    l_organization_id   := NULL;
    l_inventory_item_id := NULL;
    --Organization Validation
    IF r_onhadbal_stg.organization_code IS NOT NULL THEN
      BEGIN
        SELECT organization_id
        INTO l_organization_id
        FROM mtl_parameters
        WHERE organization_code = r_onhadbal_stg.organization_code;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || 'Invalid Organization Code';
        l_item_err_details := '\' || ' ' || ' Organization Code is not valid';
        print_debug (l_item_err_details);
      WHEN OTHERS THEN
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
        l_item_err_details := '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
        print_debug (l_item_err_details);
      END;
    ELSE
      l_item_error       := 'Y';
      l_errmsg           := l_errmsg || '\' || 'Organization Code is NULL';
      l_item_err_details := '\' || ' ' || 'Organization Code cannot be Null';
      print_debug (l_item_err_details);
    END IF;
	
    --Item Validation
    IF r_onhadbal_stg.item_segment IS NOT NULL THEN
      BEGIN
        SELECT inventory_item_id,
          primary_uom_code --count(1)
        INTO l_inventory_item_id,
          l_uom_code
        FROM mtl_system_items_b
        WHERE UPPER (segment1) = UPPER (r_onhadbal_stg.item_segment)
        AND organization_id    = l_organization_id
          --l_organization_id
        AND enabled_flag = 'Y'
        AND SYSDATE BETWEEN NVL (start_date_active, SYSDATE) AND NVL (end_date_active, SYSDATE);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || 'Inventory Item Id doesnt exist';
        l_item_err_details := '\' || ' ' || 'Inventory Item Id cannot be Null';
        print_debug (l_item_err_details);
      WHEN OTHERS THEN
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
        l_item_err_details := '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
        print_debug (l_item_err_details);
      END;
    ELSE
      l_item_error       := 'Y';
      l_errmsg           := l_errmsg || '\' || 'Inventory Item Id is NULL';
      l_item_err_details := '\' || ' ' || 'Inventory Item Id cannot be Null';
      print_debug (l_item_err_details);
    END IF;
    --UOM Validation
    IF r_onhadbal_stg.transaction_uom IS NOT NULL THEN
      BEGIN
        SELECT COUNT (1)
        INTO l_uom
        FROM mtl_units_of_measure
        WHERE UPPER (uom_code) = UPPER (r_onhadbal_stg.transaction_uom);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || 'Item UOM doesnt exist';
        l_item_err_details := '\' || ' ' || 'Item  UOM doesnt exist';
        print_debug (l_item_err_details);
      WHEN OTHERS THEN
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
        l_item_err_details := '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
        print_debug (l_item_err_details);
      END;
    ELSE
      l_item_error       := 'Y';
      l_errmsg           := l_errmsg || '\' || 'Item UOM is NULL';
      l_item_err_details := '\' || ' ' || 'Item UOM is NULL';
      print_debug (l_item_err_details);
    END IF;
    l_lotcontrol           := 0;
    l_srl_num_control      := 0;
    IF l_inventory_item_id IS NOT NULL THEN
      SELECT lot_control_code,
        serial_number_control_code
      INTO l_lotcontrol,
        l_srl_num_control
      FROM mtl_system_items_b msb
      WHERE inventory_item_id = l_inventory_item_id
      AND organization_id     = l_organization_id;
    END IF;
    IF l_lotcontrol = 2 THEN
      BEGIN
        SELECT 'x'
        INTO l_exist
        FROM xxwc_onhand_lot_cnv
        WHERE organization_code=r_onhadbal_stg.organization_code
        AND item_segment       =r_onhadbal_stg.item_segment
        AND rownum             =1;
      EXCEPTION
      WHEN OTHERS THEN
        IF r_onhadbal_stg.transaction_quantity != 0 THEN -- If Onhand Qty != 0 for a lot controlled item, and the item does not exist in Lot File then report error. -- Gopi Damuluri
          l_item_error                         := 'Y';
          l_errmsg                             := l_errmsg || '\' || 'Lot Information do not exist';
        END IF;
      END;
    END IF;
    /* Commented by Shankar 22-Feb-2012 as Serial information will not be available
    at the time of on hand conversion */
    /*
    IF l_srl_num_control = 5 then
    begin
    select 'x'
    into l_exist
    from xxwc_onhand_serial_cnv
    where organization_code=r_onhadbal_stg.organization_code
    and item_segment=r_onhadbal_stg.item_segment
    and rownum=1;
    exception
    when others then
    l_item_error := 'Y';
    l_errmsg :=
    l_errmsg
    || '\'
    || 'Serial Information do not exist';
    end;
    END IF;
    */
    --Transaction Qty Validation
    IF r_onhadbal_stg.transaction_quantity >= 0 THEN
      NULL;
    ELSE
      l_item_error       := 'Y';
      l_errmsg           := l_errmsg || '\' || 'TRANSACTION QUANTITY Does Not Exist';
      l_item_err_details := '\' || ' ' || 'TRANSACTION QUANTITY Does Not Exist';
      print_debug (l_item_err_details);
    END IF;
    -- Sub Inventory Code Validation
    --================================================================================
    --        IF r_onhadbal_stg.subinventory_code IS NOT NULL
    --        THEN
    --            BEGIN
    --                SELECT COUNT (1)
    --                   INTO l_count
    --                   FROM mtl_secondary_inventories
    --                  WHERE UPPER (secondary_inventory_name) =
    --                                               UPPER (r_onhadbal_stg.subinventory_code)
    --                    AND organization_id = l_organization_id;
    --            EXCEPTION
    --                WHEN NO_DATA_FOUND
    --                THEN
    --                    l_item_error := 'Y';
    --                    l_errmsg :=
    --                               l_errmsg || '\' || 'Subinventory Code doesnt exist';
    --                    l_item_err_details :=
    --                            '\'
    --                        || ' '
    --                        || 'Sub Inventory Code '
    --                        || r_onhadbal_stg.subinventory_code
    --                        || ' does not exist for Inventory Organization -> '
    --                        || r_onhadbal_stg.organization_id;
    --                    print_debug (l_item_err_details);
    --                WHEN OTHERS
    --                THEN
    --                    l_item_error := 'Y';
    --                    l_errmsg := l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
    --                    l_item_err_details :=
    --                                    '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
    --                    print_debug (l_item_err_details);
    --            END;
    --        ELSE
    --            l_item_error := 'Y';
    --            l_errmsg := l_errmsg || '\' || 'Subinventory Code is NULL';
    --            l_item_err_details :=
    --                                   '\' || ' ' || 'Subinventory Code cannot be Null';
    --            print_debug (l_item_err_details);
    --        END IF;
    --Transaction Cost for Asset Subinventory Validation
    IF UPPER (r_onhadbal_stg.subinventory_code) = UPPER ('Asset Subinventory') THEN
      IF r_onhadbal_stg.transaction_cost        > 0 THEN
        NULL;
      ELSE
        l_item_error       := 'Y';
        l_errmsg           := l_errmsg || '\' || 'Transaction Cost cannot be NULL';
        l_item_err_details := '\' || ' ' || 'Transaction Cost cannot be Null';
        print_debug (l_item_err_details);
      END IF;
    END IF;
    --print_debug ('Updating Staging Tables');
    --Processing Error Information
    IF NVL (l_item_error, 'N') = 'Y' THEN
      -- Update Item Staging Table with error flag and error message
      UPDATE xxwc_onhand_bal_cnv
      SET status      = 'E',
        error_message = SUBSTR (l_errmsg, 1, 2000)
      WHERE ROWID     = r_onhadbal_stg.ROWID;
    ELSE
      UPDATE xxwc_onhand_bal_cnv
      SET status  = 'V'
      WHERE ROWID = r_onhadbal_stg.ROWID;
    END IF;
    COMMIT;
  END LOOP;
  print_debug ('Number of Records Processed: ' || l_processing_stg);
  SELECT COUNT (1)
  INTO l_error_stg
  FROM xxwc_onhand_bal_cnv xobc
  WHERE NVL (status, 'E') = 'E';
  print_debug ('Number of Records Errored out : ' || l_error_stg);
  SELECT COUNT (1)
  INTO l_validated_stg
  FROM xxwc_onhand_bal_cnv xobc
  WHERE status = 'V';
  print_debug ('Number of Records Validated ' || l_validated_stg);
  --display the processed information
END validations;
PROCEDURE process_statements
IS
  CURSOR c_validrec_stg
  IS
    SELECT ROWID,
      organization_id,
      xxonhand.transaction_quantity transaction_quantity,
      transaction_uom,
      transaction_cost,
      xxonhand.status status,
      organization_code,
      item_segment,
      subinventory_code,
      transaction_typename,
      accts_code_combination,
      error_message,
      xxonhand.source_code transaction_source,
      transaction_type,
      locator_name,
      lot_number,
      to_serial_number,
      fm_serial_number,
      lot_expiration_date,
      transaction_date,
      xxonhand.transaction_reference transaction_reference,
      primary_quantity,
      origination_date,
      how_priced
    FROM xxwc_onhand_bal_cnv xxonhand
    WHERE xxonhand.status = 'V'
    ORDER BY organization_code,
      item_segment,
      NVL(transaction_date,sysdate);
  l_transaction_type_id    NUMBER;
  l_0_transaction_type_id  NUMBER;
  l_organization_id        NUMBER;
  l_inventory_item_id      NUMBER;
  l_lotcontrol             NUMBER;
  l_srl_num_control        NUMBER;
  l_trans_action_id        NUMBER;
  l_trans_source_type_id   NUMBER;
  l_flow_schedule          VARCHAR2 (1) := 'Y';
  l_scheduled_flag         NUMBER       := 2;
  l_transaction_mode       NUMBER       := 3;
  l_process_flag           NUMBER       := 1;
  l_lock_mode              NUMBER       := 2;
  l_accts_pay_code_comb_id VARCHAR2 (30);
  l_interfaceprocess_count NUMBER := 0;
  l_interfaceerror_count   NUMBER := 0;
  l_interfacerec_count     NUMBER := 0;
  l_errormsg               VARCHAR2 (1000);
  l_organization_code      VARCHAR2 (10);
  l_transaction_source_id  NUMBER;
  l_count                  NUMBER;
  l_total_qty              NUMBER;
  l_bal_qty                NUMBER;
  l_trx_qty                NUMBER;
  l_mat_account            NUMBER;
  l_cost_group_id          NUMBER;
  l_lot_number             VARCHAR2(80); -- Added by Gopi Damuluri
  l_exist                  VARCHAR2(1);  -- Added by Gopi Damuluri
BEGIN
  --    --Deleting all the interface information
  --    delete from MTL_TRANSACTIONS_INTERFACE;
  --    delete from MTL_TRANSACTION_LOTS_INTERFACE;
  --    delete from MTL_SERIAL_NUMBERS_INTERFACE;
  --    commit;
  SELECT COUNT (1)
  INTO l_interfaceprocess_count
  FROM xxwc_onhand_bal_cnv xxobc
  WHERE xxobc.status = 'V';
  print_debug ('Before Insertion Process.....!');
  FOR r_validrec_stg IN c_validrec_stg
  LOOP
    l_errormsg                        := NULL;
    r_validrec_stg.transaction_source := 'CONVERSION';
    l_mat_account                     := NULL;
    l_cost_group_id                   := NULL;
    /*
    BEGIN
    SELECT transaction_type_id
    INTO l_0_transaction_type_id
    FROM mtl_transaction_types
    WHERE description = 'Update average cost information';
    WHEN OTHERS THEN
    NULL;
    END;
    */
    IF NVL (r_validrec_stg.transaction_quantity, 0) = 0 THEN
      l_transaction_type_id                        := 80; -- For Zero Onhand Quantity     -- Average cost update
      SELECT material_account,
        default_cost_group_id
      INTO l_mat_account,
        l_cost_group_id
      FROM mtl_parameters
      WHERE organization_code = r_validrec_stg.organization_code;
    ELSE
      l_transaction_type_id := 41; -- Account alias receipt
    END IF;
    print_debug ('Before Insertion Loop....!');
    print_debug ('Transaction Type.....!');
    print_debug ('Org Code....!');
    --getting organization id
    SELECT organization_id
    INTO l_organization_id
    FROM org_organization_definitions
    WHERE organization_code                 = r_validrec_stg.organization_code
    AND NVL (disable_date, TRUNC(SYSDATE)) >= TRUNC (SYSDATE);
    print_debug ('Transaction Source....!');
    --getting transaction source and transaction id
    SELECT transaction_source_type_id,
      transaction_action_id
    INTO l_trans_source_type_id,
      l_trans_action_id
    FROM mtl_transaction_types
    WHERE transaction_type_id = l_transaction_type_id;
    print_debug ('Item Id....!');
    --getting inventory item id
    SELECT inventory_item_id
    INTO l_inventory_item_id
    FROM mtl_system_items_b
    WHERE segment1      = r_validrec_stg.item_segment
    AND organization_id = l_organization_id
    AND enabled_flag    = 'Y'
    AND SYSDATE BETWEEN NVL (start_date_active, SYSDATE) AND NVL (end_date_active, SYSDATE);
    print_debug ('GL Code Comination ....!');
    SELECT disposition_id,
      distribution_account
    INTO l_transaction_source_id,
      l_accts_pay_code_comb_id
    FROM mtl_generic_dispositions
    WHERE organization_id = l_organization_id -- (SELECT organization_id
      -- FROM org_organization_definitions
      -- WHERE organization_code =
      -- r_validrec_stg.organization_code)
    AND segment1 = 'CONVERSION';
    print_debug ('Inserting into mtl_transactions_interface table');
    IF NVL (r_validrec_stg.transaction_quantity, 0) = 0 THEN
      INSERT
      INTO mtl_transactions_interface
        (
          source_code,
          process_flag,
          transaction_mode,
          transaction_uom,
          transaction_type_id,
          flow_schedule,
          scheduled_flag,
          created_by,
          last_updated_by,
          organization_id,
          subinventory_code,
          transaction_date,
          new_average_cost,
          transaction_reference,
          inventory_item_id,
          source_header_id,
          source_line_id,
          transaction_quantity,
          transaction_source_id,
          distribution_account_id,
          locator_name,
          lock_flag,
          transaction_action_id,
          transaction_source_type_id,
          creation_date,
          last_update_date,
          transaction_interface_id,
          transaction_header_id,
          material_account,
          cost_group_id,
          ATTRIBUTE15 --born on date
        )
        VALUES
        (
          r_validrec_stg.transaction_source,
          l_process_flag,
          l_transaction_mode,
          r_validrec_stg.transaction_uom,
          l_transaction_type_id,
          l_flow_schedule,
          l_scheduled_flag,
          fnd_profile.VALUE ('USER_ID'), --,0
          fnd_profile.VALUE ('USER_ID'), --,-1
          l_organization_id,
          NVL (r_validrec_stg.subinventory_code, 'General'),
          sysdate,                                                                                                                                                                                    --add_months(last_day(SYSDATE), -1)-7,
          DECODE(r_validrec_stg.how_priced, 'E',r_validrec_stg.transaction_cost, 'C',r_validrec_stg.transaction_cost/100, 'M',r_validrec_stg.transaction_cost/1000, r_validrec_stg.transaction_cost), -- new_average_cost
          r_validrec_stg.transaction_reference,
          l_inventory_item_id,
          99, --p_source_header_id
          98, --p_source_line_id
          NVL (r_validrec_stg.transaction_quantity, 0),
          l_transaction_source_id,
          l_accts_pay_code_comb_id,
          NVL (r_validrec_stg.locator_name, ''),
          l_lock_mode, --add
          l_trans_action_id,
          l_trans_source_type_id,
          SYSDATE,
          SYSDATE,
          mtl_material_transactions_s.NEXTVAL, --l_txn_int_id -- transaction_interface_id
          TO_CHAR (r_validrec_stg.organization_id)
          || TO_CHAR (SYSDATE, 'ddmmyyyy'),
          l_mat_account,
          l_cost_group_id,
          r_validrec_stg.transaction_date);
    ELSE
      INSERT
      INTO mtl_transactions_interface
        (
          source_code,
          process_flag,
          transaction_mode,
          transaction_uom,
          transaction_type_id,
          flow_schedule,
          scheduled_flag,
          created_by,
          last_updated_by,
          organization_id,
          subinventory_code,
          transaction_date,
          transaction_cost,
          transaction_reference,
          inventory_item_id,
          source_header_id,
          source_line_id,
          transaction_quantity,
          transaction_source_id,
          distribution_account_id,
          locator_name,
          lock_flag,
          transaction_action_id,
          transaction_source_type_id,
          creation_date,
          last_update_date,
          transaction_interface_id,
          transaction_header_id,
          attribute15 --born on date
        )
        VALUES
        (
          r_validrec_stg.transaction_source,
          l_process_flag,
          l_transaction_mode,
          r_validrec_stg.transaction_uom,
          l_transaction_type_id,
          l_flow_schedule,
          l_scheduled_flag,
          fnd_profile.VALUE ('USER_ID'), --,0
          fnd_profile.VALUE ('USER_ID'), --,-1
          l_organization_id,
          NVL (r_validrec_stg.subinventory_code, 'General'),
          sysdate,                                                                                                                                                                                    --add_months(last_day(SYSDATE), -1)-7,
          DECODE(r_validrec_stg.how_priced, 'E',r_validrec_stg.transaction_cost, 'C',r_validrec_stg.transaction_cost/100, 'M',r_validrec_stg.transaction_cost/1000, r_validrec_stg.transaction_cost), -- transaction_cost
          r_validrec_stg.transaction_reference,
          l_inventory_item_id,
          99, --p_source_header_id
          98, --p_source_line_id
          NVL (r_validrec_stg.transaction_quantity, 0),
          l_transaction_source_id,
          l_accts_pay_code_comb_id,
          NVL (r_validrec_stg.locator_name, ''),
          l_lock_mode, --add
          l_trans_action_id,
          l_trans_source_type_id,
          SYSDATE,
          SYSDATE,
          mtl_material_transactions_s.NEXTVAL, --l_txn_int_id -- transaction_interface_id
          TO_CHAR (r_validrec_stg.organization_id)
          || TO_CHAR (SYSDATE, 'ddmmyyyy'),
          r_validrec_stg.transaction_date);
    END IF;
    print_debug ('Lot Or Serial Control Verification');
    print_debug (l_inventory_item_id || l_organization_id);
    SELECT lot_control_code,
      serial_number_control_code
    INTO l_lotcontrol,
      l_srl_num_control
    FROM mtl_system_items_b msb
    WHERE inventory_item_id = l_inventory_item_id
    AND organization_id     = l_organization_id;
    print_debug ('Lot Or Serial Control Verification');
    IF l_lotcontrol = 2 THEN -- for full control if 1 no control     --need to derive
      print_debug ( 'Inserting into mtl_transaction_lots_interface table');
      l_total_qty := 0;
      l_trx_qty   := 0;
      l_bal_qty   := 0;
      -- Check if the Item exists in the LotControl File
      -- Added by Gopi Damuluri > Start
      BEGIN
        SELECT 'x'
        INTO l_exist
        FROM xxwc_onhand_lot_cnv
        WHERE organization_code=r_validrec_stg.organization_code
        AND item_segment       =r_validrec_stg.item_segment
        AND rownum             =1;
      EXCEPTION
      WHEN OTHERS THEN
        IF r_validrec_stg.transaction_quantity = 0 THEN -- Use Default Lot - LOT0 for Zero OnHand Quantity Lot Transactions
          INSERT
          INTO mtl_transaction_lots_interface
            (
              transaction_interface_id,
              lot_number,
              transaction_quantity,
              last_update_date,
              last_updated_by,
              creation_date,
              created_by,
              --product_code,
              last_update_login,
              product_transaction_id,
              --primary_quantity,
              lot_expiration_date
            )
            VALUES
            (
              mtl_material_transactions_s.CURRVAL, --transaction_interface_id
              'LOT0',                              --r_validrec_stg.lot_number -- lot_number
              0,                                   --lot_rec.transaction_quantity,
              SYSDATE,
              0, --userid
              SYSDATE,
              -1,                                  --userid
              0,                                   --userid
              mtl_material_transactions_s.CURRVAL, -- product_transaction_id
              SYSDATE + 1
            );
        END IF;
      END;
      -- Added by Gopi Damuluri < End
      FOR lot_rec IN
      (SELECT a.rowid,
          a.transaction_quantity,
          a.lot_expiration_date,
          a.balance_quantity,
          a.lot_number -- Gopi Damuluri
        FROM xxwc_onhand_lot_cnv a
        WHERE a.organization_code=r_validrec_stg.organization_code
        AND a.item_segment       =r_validrec_stg.item_segment
        AND a.status            IS NULL
        ORDER BY a.lot_expiration_date ASC
      )
      LOOP
        IF l_total_qty                                                                 + lot_rec.balance_quantity > r_validrec_stg.transaction_quantity THEN
          l_trx_qty                              := r_validrec_stg.transaction_quantity-l_total_qty;
          l_bal_qty                              := lot_rec.balance_quantity           - l_trx_qty;
        ELSE
          l_trx_qty := lot_rec.balance_quantity;
          l_bal_qty := 0;
        END IF;
        l_total_qty := l_total_qty+l_trx_qty;
        --l_total_qty := l_total_qty + lot_rec.transaction_quantity;
        l_lot_number := 'L' || NVL(lot_rec.lot_number,xxwc_onhand_lot_s.NEXTVAL); -- Use PRISM lot_number provided in the file.If no value is provided then use the default sequence. -- Gopi Damuluri
        INSERT
        INTO mtl_transaction_lots_interface
          (
            transaction_interface_id,
            lot_number,
            transaction_quantity,
            last_update_date,
            last_updated_by,
            creation_date,
            created_by,
            --product_code,
            last_update_login,
            product_transaction_id,
            --primary_quantity,
            lot_expiration_date
          )
          VALUES
          (
            mtl_material_transactions_s.CURRVAL, --transaction_interface_id
            'L'
            || NVL(lot_rec.lot_number,xxwc_onhand_lot_s.NEXTVAL), --r_validrec_stg.lot_number -- lot_number
            l_trx_qty,                                            --lot_rec.transaction_quantity,
            SYSDATE,
            0, --userid
            SYSDATE,
            -1,                                  --userid
            0,                                   --userid
            mtl_material_transactions_s.CURRVAL, -- transaction_interface_id
            --r_validrec_stg.primary_quantity,
            lot_rec.lot_expiration_date
          );
        IF l_bal_qty = 0 THEN
          UPDATE xxwc_onhand_lot_cnv
          SET status        ='P',
            balance_quantity=0
          WHERE rowid       =lot_rec.rowid;
        ELSE
          UPDATE xxwc_onhand_lot_cnv
          SET balance_Quantity=l_bal_qty
          WHERE rowid         =lot_rec.rowid;
        END IF;
        IF l_total_qty = r_validrec_stg.transaction_quantity THEN
          EXIT;
        END IF;
      END LOOP;
    ELSE
      NULL;
    END IF;
    -- Commented by Shankar
    /*
    IF l_srl_num_control = 5                                  --derive
    THEN
    print_debug (
    'Inserting into mtl_serial_numbers_interface table');
    l_count := 0;                                               --
    --WHILE l_count <= r_validrec_stg.transaction_quantity
    for serial_rec in (select a.rowid,a.serial_number, a.transaction_quantity
    from xxwc_onhand_serial_cnv a
    where a.organization_code=r_validrec_stg.organization_code
    and a.item_segment=r_validrec_stg.item_segment
    and a.status is null
    order by a.serial_number asc)
    --and a.rownum <r_validrec_stg.transaction_quantity+1)
    LOOP
    l_count := l_count +1;
    INSERT INTO mtl_serial_numbers_interface (transaction_interface_id,
    source_code,
    source_line_id,
    process_flag,
    fm_serial_number,
    to_serial_number,
    created_by,
    last_updated_by,
    creation_date,
    last_update_date,
    origination_date)
    VALUES   (mtl_material_transactions_s.CURRVAL, --l_txn_int_id
    r_validrec_stg.transaction_source, --l_source_code
    98,                         --p_source_line_id
    l_process_flag, --r_validrec_stg.fm_serial_number,
    serial_rec.serial_number,--xxwc_onhand_fm_serial_s.NEXTVAL,
    NULL, --r_validrec_stg.to_serial_number,xxwc_onhand_fm_serial_s.currval
    0,             --,FND_PROFILE.VALUE('USER_ID')
    -1,            --,FND_PROFILE.VALUE('USER_ID')
    SYSDATE,
    SYSDATE,
    r_validrec_stg.origination_date);
    --l_count := l_count + 1;
    update xxwc_onhand_serial_cnv
    set status='P'
    where rowid=serial_rec.rowid;
    IF l_count = r_validrec_stg.transaction_quantity then
    exit;
    END IF;
    END LOOP;
    ELSE
    NULL;
    END IF;
    */
    -- Update Item Staging Table with process status
    IF l_errormsg IS NOT NULL THEN
      --ROLLBACK;
      --print_debug ('Updating the Staging Table with status E');
      UPDATE xxwc_onhand_bal_cnv
      SET status      = 'E',
        error_message = l_errormsg
      WHERE ROWID     = r_validrec_stg.ROWID;
      --COMMIT;
    ELSE
      -- print_debug ('Updating the Staging Table with status P');
      UPDATE xxwc_onhand_bal_cnv
      SET status  = 'P'
      WHERE ROWID = r_validrec_stg.ROWID;
      --COMMIT;
    END IF;
    COMMIT;
    --- FND_FILE.PUT_LINE(FND_FILE.OUTPUT, 'One record inserted into interface table -> mtl_transactions_interface');
  END LOOP;
EXCEPTION
WHEN OTHERS THEN
  print_debug (SQLCODE || '  ' || SQLERRM);
  print_debug ('Exception Part....!');
  --processing information
  print_debug ( 'Number Of Records being Processed: ' || l_interfaceprocess_count);
  SELECT COUNT (1)
  INTO l_interfaceerror_count
  FROM xxwc_onhand_bal_cnv
  WHERE status = 'E';
  print_debug('Number Of Records with Error in Interface Table: ' || l_interfaceerror_count);
  SELECT COUNT (1)
  INTO l_interfacerec_count
  FROM xxwc_onhand_bal_cnv
  WHERE status = 'P';
  print_debug('Number Of Valid Records in Interface Table: ' || l_interfacerec_count);
END process_statements;
PROCEDURE itemonhand_conv_proc(
    errbuf OUT VARCHAR2,
    retcode OUT VARCHAR2,
    p_validate_only IN VARCHAR2)
IS
  l_count        NUMBER;
  l_count_valid  NUMBER;
  v_errorcode    NUMBER;
  v_errormessage VARCHAR2 (240);
  CURSOR c_master
  IS
    SELECT DISTINCT organization_code
    FROM xxwc_onhand_bal_cnv
    WHERE NVL (status, 'R') = 'R';
  CURSOR c_items ( p_org VARCHAR2)
  IS
    SELECT *
    FROM xxwc_onhand_bal_cnv
    WHERE UPPER (organization_code) = UPPER (p_org)
    AND NVL (status, 'R')           = 'R';
BEGIN
  print_debug ('Performing .........!');
  SELECT COUNT (1)
  INTO l_count
  FROM xxwc_onhand_bal_cnv
  WHERE NVL (status, 'R') IN ('R','E');
  IF l_count               > 0 THEN
    print_debug ('Performing Validations');
    xxwc_inv_onhand_conv_pkg.validations;
  ELSE
    print_debug ('No Records To Process');
  END IF;
  IF NVL(p_validate_only,'Y') = 'N' THEN
    SELECT COUNT (1)
    INTO l_count_valid
    FROM xxwc_onhand_bal_cnv
    WHERE status     = 'V';
    IF l_count_valid > 0 THEN
      print_debug ('Performing Insertion');
      xxwc_inv_onhand_conv_pkg.process_statements;
    ELSE
      print_debug ('No Records To Insert');
    END IF;
  END IF;
END itemonhand_conv_proc;
PROCEDURE update_born_on_date(
    errbuf OUT VARCHAR2 ,
    retcode OUT VARCHAR2 ,
    i_trx_date        IN VARCHAR2 ,
    i_organization_id IN NUMBER)
IS
  l_trx_date DATE;
  ln_count NUMBER:=0; --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
  CURSOR c1
  IS
    SELECT organization_id ,
      transaction_id ,
      TO_DATE (NVL (attribute15, TO_CHAR (transaction_date - 366, 'DD-MON-RR')), 'DD-MON-RR') trx_date
    FROM mtl_material_transactions a
    WHERE TRUNC (creation_date) = TRUNC (l_trx_date)
    AND organization_id         =NVL(i_organization_id, organization_id)
      --and attribute15 is not null
    AND transaction_type_id = 41 -- Account alias receipt    -- rev 1.10  4-MAR-2013
    AND EXISTS
      (SELECT 'x'
      FROM mtl_onhand_quantities_detail b
      WHERE b.create_transaction_id = a.transaction_id
      );
BEGIN

fnd_file.put_line(fnd_file.output,'This is the output file'); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'-------------------------'); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Execution Start Time '||SYSTIMESTAMP); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049

fnd_file.put_line(fnd_file.output,'Starting the loop...'); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Parameter Transaction Date...'||i_trx_date); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Parameter Inventory Organization ID...'||i_organization_id); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Starting the loop...'); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049

  l_trx_date := FND_DATE.canonical_to_date(i_trx_date);
  FOR c1_rec IN c1
  LOOP
  
    ln_count:=ln_count+1;
  
    UPDATE mtl_onhand_quantities_detail
    SET orig_date_received      = c1_rec.trx_date
    WHERE create_transaction_id = c1_rec.transaction_id;
  END LOOP;
 
fnd_file.put_line(fnd_file.output,'Loop completed...'); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Count of records updated in table mtl_onhand_quantities_detail is...'||ln_count); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Execution End Time '||SYSTIMESTAMP); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049

END update_born_on_date;
END xxwc_inv_onhand_conv_pkg;
/
