create or replace PACKAGE BODY           XXWC_CC_MOBILE_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_CC_MOBILE_PKG $                                                                                                         *
   *   Module Name: XXWC_CC_MOBILE_PKG                                                                                                      *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA                                                                            *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Count                                                                   *
   *   1.1        02-MAR-2016  Lee Spitzer               TMS#20160309-00091 - RF Enhancements                                               *
   *   1.2        17-MAR-2016  Lee Spitzer               TMS Ticket Number 20150810-00022 - RF - Cyclecount Research Report
   *   1.2        03-MAY-2016  Lee Spitzer               TMS Ticket Number 20151210-00014 - RF - PI Enhancements
 *****************************************************************************************************************************************/

  
  
  PROCEDURE DEBUG_LOG (P_MESSAGE   VARCHAR2)
  IS
   
    
       PRAGMA AUTONOMOUS_TRANSACTION;
       
       BEGIN
    
        IF g_debug = 'Y' THEN
      
          FND_LOG.STRING (G_LOG_LEVEL, upper(G_PACKAGE||'.'||G_CALL_FROM) ||' '|| G_CALL_POINT, P_MESSAGE);
         
          COMMIT;
        
        END IF;
  
  EXCEPTION
  
    WHEN others THEN
        
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in generating debug log ';
    
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

   
  END DEBUG_LOG;

                       
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_YES_NO_LOV                                                                                                             *
   *                                                                                                                                        *
   *   PURPOSE:  This procedure is used to get the Yes or No List of Values                                                                 *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Count                                                                   *
   *****************************************************************************************************************************************/
 
    PROCEDURE GET_YES_NO_LOV  ( x_default                   OUT    NOCOPY t_genref --0
                              , p_description               IN     VARCHAR2
                              ) IS
                                    
     BEGIN
     
            g_call_from := 'GET_YES_NO_LOV';
            g_call_point := 'Start';
            
            
            OPEN x_default FOR
              SELECT  lookup_code,
                      trim(meaning),
                      trim(meaning)
              FROM   mfg_lookups
              WHERE  lookup_type = 'SYS_YES_NO'
              AND    nvl(enabled_flag,'N') = 'Y'
              AND    SYSDATE BETWEEN nvl(start_date_active, SYSDATE-1) AND nvl(end_date_active, SYSDATE+1)
              AND    meaning LIKE nvl(p_description, meaning);
     
    EXCEPTION
  
    WHEN others THEN
        close x_default;
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in getting yes no type lov ';
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

     
     END GET_YES_NO_LOV;
     
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_WC_COUNT_LIST_SEQUENCE_LOV                                                                                             *
   *                                                                                                                                        *
   *   PURPOSE:  This procedure is used to get the Count List Sequence List of Values                                                       *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Count                                                                   *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_WC_COUNT_LIST_SEQUENCE_LOV  (  x_default                   OUT    NOCOPY t_genref --0
                                              ,  p_organization_id          IN      VARCHAR2
                                              ,  p_cycle_count_header_id    IN      VARCHAR2
                                              ,  p_wc_count_list_sequence   IN      VARCHAR2
                                              ,  p_subinventory_code        IN      VARCHAR2
                                              ,  p_locator_id               IN      VARCHAR2
                                              ,  p_sort_by                  IN      VARCHAR2
                                              --,  p_inventory_item_id        IN      VARCHAR2
                                              ) IS
                                              
      l_organization_id NUMBER DEFAULT NULL;
      l_cycle_count_header_id NUMBER DEFAULT NULL;
      l_inventory_item_id NUMBER DEFAULT NULL;
      l_locator_id  NUMBER DEFAULT NULL;
      l_wc_count_list_sequence NUMBER DEFAULT NULL;
      l_sort_by NUMBER DEFAULT NULL;
                                              
    BEGIN
            g_call_from := 'GET_WC_COUNT_LIST_SEQUENCE_LOV';
            g_call_point := 'Start';
            
            
            g_call_point := 'Convert organization_id to number';
            
            IF p_organization_id IS NOT NULL THEN
            
              BEGIN
                SELECT to_number(p_organization_id)
                INTO   l_organization_id
                FROM   dual;
              EXCEPTION
                  WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Could not convert organization_id to number ' || g_sqlcode || g_sqlerrm;
                      raise g_exception;
              END;
              
            END IF;
            

            g_call_point := 'Convert cycle_count_header_id to number';
            
            IF p_cycle_count_header_id IS NOT NULL THEN
            
              BEGIN
                SELECT to_number(p_cycle_count_header_id)
                INTO   l_cycle_count_header_id
                FROM   dual;
              EXCEPTION
                  WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Could not convert cycle_count_header_id to number ' || g_sqlcode || g_sqlerrm;
                      raise g_exception;
              END;

            END IF;
            
            IF p_wc_count_list_sequence IS NOT NULL THEN
            
            g_call_point := 'Convert wc count list sequence to number';
            
              IF p_wc_count_list_sequence = '%' THEN
                l_wc_count_list_sequence := NULL;
            
              ELSE
              
                  BEGIN
                    SELECT to_number(p_wc_count_list_sequence)
                    INTO   l_wc_count_list_sequence
                    FROM   dual;
                  EXCEPTION
                      WHEN others THEN
                          g_sqlcode := SQLCODE;
                          g_sqlerrm := SQLERRM;
                          g_message := 'Could not convert wc count list sequence to number' || g_sqlcode || g_sqlerrm;
                          raise g_exception;
                  END;
            
              END IF;
            
            ELSE--ELSE IF p_wc_count_list_seqence = '%' THEN
            
              l_wc_count_list_sequence := NULL;
            
            END IF;
            
            
            g_call_point := 'Convert locator_id to number';
            
            IF p_locator_id IS NOT NULL THEN
            
              BEGIN
                SELECT to_number(p_locator_id)
                INTO   l_locator_id
                FROM   dual;
              EXCEPTION
                  WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Could not convert locator_id to number ' || g_sqlcode || g_sqlerrm;
                      raise g_exception;
              END;
            
            END IF;
            
            g_call_point := 'Convert sort by';
            
            IF p_sort_by IS NOT NULL THEN
            
              BEGIN
                SELECT to_number(p_sort_by)
                INTO   l_sort_by
                FROM   dual;
              EXCEPTION
                  WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Could not convert sort by to number ' || g_sqlcode || g_sqlerrm;
                      raise g_exception;
              END;

            END IF;

         IF l_sort_by = 0 THEN 
         
          BEGIN          
            OPEN x_default FOR
              SELECT  xmccev.wc_count_list_sequence
                     ,xmccev.item_number
                     ,xmccev.item_description
                     ,xmccev.inventory_item_id
                     ,xmccev.subinventory
                     ,xmccev.stock_locator
                     ,xmccev.inventory_location_id
                     ---
                     ,xmccev.revision
                     ,xmccev.primary_uom_code
                     ,xmccev.cycle_count_entry_id
                     ,xmccev.count_list_sequence
                     ,xmccev.cost_group_id
                     ,xmccev.entry_status_code
                     ,decode(xmccev.entry_status_code,
                              1,'No',
                              3,'Yes') recount_dispaly
              FROM   xxwc_mtl_cycle_count_entries_v xmccev
              WHERE  xmccev.organization_id = nvl(l_organization_id, xmccev.organization_id)
              AND    xmccev.cycle_count_header_id = nvl(l_cycle_count_header_id, xmccev.cycle_count_header_id)
              AND    xmccev.WC_COUNT_LIST_SEQUENCE = nvl(l_wc_count_list_sequence, xmccev.WC_COUNT_LIST_SEQUENCE)
              AND    xmccev.entry_status_code = 1
              AND    xmccev.subinventory = nvl(p_subinventory_code, xmccev.subinventory)
              AND    xmccev.inventory_location_id = nvl(l_locator_id, xmccev.inventory_location_id)
              order by xmccev.WC_STOCK_LOCATOR,
                       xmccev.ITEM_NUMBER,
                       xmccev.SUBINVENTORY,
                       xmccev.LOT_NUMBER;
              
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting query for wc count list sequence ';
                raise g_exception;
                
           END;
          
      ELSIF l_sort_by = 1 THEN
      
          BEGIN          
            OPEN x_default FOR
              SELECT  xmccev.wc_count_list_sequence
                     ,xmccev.item_number
                     ,xmccev.item_description
                     ,xmccev.inventory_item_id
                     ,xmccev.subinventory
                     ,xmccev.stock_locator
                     ,xmccev.inventory_location_id
                     ---
                     ,xmccev.revision
                     ,xmccev.primary_uom_code
                     ,xmccev.cycle_count_entry_id
                     ,xmccev.count_list_sequence
                     ,xmccev.cost_group_id
                     ,xmccev.entry_status_code
                     ,decode(xmccev.entry_status_code,
                              1,'No',
                              3,'Yes') recount_dispaly
              FROM   xxwc_mtl_cycle_count_entries_v xmccev
              WHERE  xmccev.organization_id = nvl(l_organization_id, xmccev.organization_id)
              AND    xmccev.cycle_count_header_id = nvl(l_cycle_count_header_id, xmccev.cycle_count_header_id)
              AND    xmccev.WC_COUNT_LIST_SEQUENCE = nvl(l_wc_count_list_sequence, xmccev.WC_COUNT_LIST_SEQUENCE)
              AND    xmccev.entry_status_code = 3
              AND    xmccev.subinventory = nvl(p_subinventory_code, xmccev.subinventory)
              AND    xmccev.inventory_location_id = nvl(l_locator_id, xmccev.inventory_location_id)
              ORDER BY abs(xmccev.ADJUSTMENT_AMOUNT) DESC, 
                       xmccev.ITEM_NUMBER,
                       xmccev.SUBINVENTORY,
                       xmccev.LOT_NUMBER,
                       xmccev.WC_STOCK_LOCATOR;
              
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting query for wc count list sequence ';
                raise g_exception;
                
          END;
      
      ELSIF l_sort_by = 9 THEN
      
          BEGIN          
            OPEN x_default FOR
              SELECT  xmccev.wc_count_list_sequence
                     ,xmccev.item_number
                     ,xmccev.item_description
                     ,xmccev.inventory_item_id
                     ,xmccev.subinventory
                     ,xmccev.stock_locator
                     ,xmccev.inventory_location_id
                     ---
                     ,xmccev.revision
                     ,xmccev.primary_uom_code
                     ,xmccev.cycle_count_entry_id
                     ,xmccev.count_list_sequence
                     ,xmccev.cost_group_id
                     ,xmccev.entry_status_code
                     ,decode(xmccev.entry_status_code,
                              1,'No',
                              3,'Yes') recount_dispaly
              FROM   xxwc_mtl_cycle_count_entries_v xmccev
              WHERE  xmccev.organization_id = nvl(l_organization_id, xmccev.organization_id)
              AND    xmccev.cycle_count_header_id = nvl(l_cycle_count_header_id, xmccev.cycle_count_header_id)
              AND    xmccev.WC_COUNT_LIST_SEQUENCE = nvl(l_wc_count_list_sequence, xmccev.WC_COUNT_LIST_SEQUENCE)
              AND    xmccev.entry_status_code = 1
              AND    xmccev.subinventory = nvl(p_subinventory_code, xmccev.subinventory)
              AND    xmccev.inventory_location_id = nvl(l_locator_id, xmccev.inventory_location_id)
              AND NOT EXISTS 
                            (SELECT * 
                             FROM   xxwc.xxwc_cc_entry_table xcet 
                             WHERE  xcet.inventory_item_id = xmccev.inventory_item_id 
                             AND    xcet.organization_id = xmccev.organization_id 
                             AND    xcet.process_flag = 1 
                             AND    xcet.cycle_count_header_id = xmccev.cycle_count_header_id 
                             AND    nvl(xcet.wc_count_list_sequence,-1) = nvl(xmccev.wc_count_list_sequence,-1))
              ORDER BY xmccev.WC_STOCK_LOCATOR,
                       xmccev.ITEM_NUMBER,
                       xmccev.SUBINVENTORY,
                       xmccev.LOT_NUMBER;
              
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting query for wc count list sequence ';
                raise g_exception;
                
          END;
          
    
    ELSIF l_sort_by = 10 THEN
      
          BEGIN          
            OPEN x_default FOR
              SELECT  xmccev.wc_count_list_sequence
                     ,xmccev.item_number
                     ,xmccev.item_description
                     ,xmccev.inventory_item_id
                     ,xmccev.subinventory
                     ,xmccev.stock_locator
                     ,xmccev.inventory_location_id
                     ---
                     ,xmccev.revision
                     ,xmccev.primary_uom_code
                     ,xmccev.cycle_count_entry_id
                     ,xmccev.count_list_sequence
                     ,xmccev.cost_group_id
                     ,xmccev.entry_status_code
                     ,decode(xmccev.entry_status_code,
                              1,'No',
                              3,'Yes') recount_dispaly
              FROM   xxwc_mtl_cycle_count_entries_v xmccev
              WHERE  xmccev.organization_id = nvl(l_organization_id, xmccev.organization_id)
              AND    xmccev.cycle_count_header_id = nvl(l_cycle_count_header_id, xmccev.cycle_count_header_id)
              AND    xmccev.WC_COUNT_LIST_SEQUENCE = nvl(l_wc_count_list_sequence, xmccev.WC_COUNT_LIST_SEQUENCE)
              AND    xmccev.entry_status_code = 3
              AND    xmccev.subinventory = nvl(p_subinventory_code, xmccev.subinventory)
              AND    xmccev.inventory_location_id = nvl(l_locator_id, xmccev.inventory_location_id)
              AND    NOT EXISTS 
                               (SELECT * 
                                FROM   xxwc.xxwc_cc_entry_table xcet 
                                WHERE  xcet.inventory_item_id = xmccev.inventory_item_id 
                                AND    xcet.organization_id = xmccev.organization_id 
                                AND    xcet.process_flag = 1 
                                AND    xcet.cycle_count_header_id = xmccev.cycle_count_header_id 
                                AND    nvl(xcet.wc_count_list_sequence,-1) = nvl(xmccev.wc_count_list_sequence,-1))    
              ORDER BY abs(xmccev.ADJUSTMENT_AMOUNT) DESC, 
                       xmccev.ITEM_NUMBER,
                       xmccev.SUBINVENTORY,
                       xmccev.LOT_NUMBER,
                       xmccev.WC_STOCK_LOCATOR;
              
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting query for wc count list sequence ';
                raise g_exception;
                
          END;

    ELSE
      
          BEGIN          
            OPEN x_default FOR
              SELECT  xmccev.wc_count_list_sequence
                     ,xmccev.item_number
                     ,xmccev.item_description
                     ,xmccev.inventory_item_id
                     ,xmccev.subinventory
                     ,xmccev.stock_locator
                     ,xmccev.inventory_location_id
                     ---
                     ,xmccev.revision
                     ,xmccev.primary_uom_code
                     ,xmccev.cycle_count_entry_id
                     ,xmccev.count_list_sequence
                     ,xmccev.cost_group_id
                     ,xmccev.entry_status_code
                     ,decode(xmccev.entry_status_code,
                              1,'No',
                              3,'Yes') recount_dispaly
              FROM   xxwc_mtl_cycle_count_entries_v xmccev
              WHERE  xmccev.organization_id = nvl(l_organization_id, xmccev.organization_id)
              AND    xmccev.cycle_count_header_id = nvl(l_cycle_count_header_id, xmccev.cycle_count_header_id)
              AND    xmccev.WC_COUNT_LIST_SEQUENCE = nvl(l_wc_count_list_sequence, xmccev.WC_COUNT_LIST_SEQUENCE)
              AND    xmccev.entry_status_code in (1,3)
              AND    xmccev.subinventory = nvl(p_subinventory_code, xmccev.subinventory)
              AND    xmccev.inventory_location_id = nvl(l_locator_id, xmccev.inventory_location_id)
              ORDER BY DECODE (l_sort_by,
                                  2, xmccev.wc_count_list_sequence,
                                  3, xmccev.item_number,
                                  4, xmccev.lot_number,
                                  5, xmccev.subinventory,
                                  6, xmccev.wc_stock_locator,
                                  7, xmccev.adjustment_quantity,
                                  8, xmccev.adjustment_amount);
                                  
              
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting query for wc count list sequence ';
                raise g_exception;
                
          END;
          
    END IF;
    
    EXCEPTION
    
    WHEN g_exception THEN
         close x_default;
           g_message := 'Error in getting yes no type lov ';
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

  
    WHEN others THEN
       close x_default;
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in getting yes no type lov ';
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

      
    
    END  GET_WC_COUNT_LIST_SEQUENCE_LOV;


   /*****************************************************************************************************************************************
   *   PROCEDURE GET_WC_SORT_BY_LOV                                                                                                         *
   *                                                                                                                                        *
   *   PURPOSE:  This procedure is used to get the Sort List of Values                                                                      *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    
    PROCEDURE GET_WC_SORT_BY_LOV  ( x_default       OUT NOCOPY t_genref
                                  , p_column_number IN VARCHAR2
                                  ) IS
    
    BEGIN
    
            g_call_from := 'GET_WC_SORT_BY_LOV';
            g_call_point := 'Start';
            
             OPEN x_default FOR
                  SELECT column_number, column_name
                  FROM   (
                          select 0 column_number, 'INITIAL_COUNT'  column_name from dual
                          union
                          select 9 column_number, 'INITIAL COUNT BUT NOT ENTERED' column_name from dual
                          union
                          select 10 column_number, 'RECOUNT BUT NOT ENTERED' column_name from dual
                          union
                          select 1 column_number, 'RECOUNT' column_name from dual
                          union
                          SELECT DECODE(COLUMN_NAME,
                                        'WC_COUNT_LIST_SEQUENCE',2,
                                        'ITEM_NUMBER',3,
                                        'LOT_NUMBER',4,
                                        'SUBINVENTORY',5,
                                        'WC_STOCK_LOCATOR',6,
                                        'ADJUSTMENT_QUANTITY',7,
                                        'ADJUSTMENT_AMOUNT',8) column_number,
                                        column_name
                          FROM   ALL_TAB_COLUMNS
                          WHERE  TABLE_NAME = 'XXWC_MTL_CYCLE_COUNT_ENTRIES_V'
                          AND    COLUMN_NAME IN ('WC_STOCK_LOCATOR','WC_COUNT_LIST_SEQUENCE','ITEM_NUMBER','SUBINVENTORY','LOT_NUMBER','ADJUSTMENT_QUANTITY','ADJUSTMENT_AMOUNT'))
                  where  column_number like nvl(p_column_number,column_number);
    EXCEPTION
    
     WHEN others THEN
       close x_default;
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in getting yes no type lov ';
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

      
    END GET_WC_SORT_BY_LOV;
    
   /*****************************************************************************************************************************************
   *   FUNCTION GET_DEFAULT_CC_HEADER_ID                                                                                                    *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_DEFAULT_CC_HEADER_ID  ( p_organization_id IN NUMBER
                                       )
        RETURN NUMBER IS
        
    
      l_cycle_count_header_id NUMBER;
    
    BEGIN
    
        g_call_from := 'GET_DEFAULT_CC_HEADER_ID';
        g_call_point := 'Start';
    
    
        debug_log('p_organization_id ' || p_organization_id);
        
        
        BEGIN
          select cycle_count_header_id
          into   l_cycle_count_header_id
          from   mtl_cycle_count_headers
          where  organization_id = p_organization_id
          and    nvl(disable_date, sysdate+1) > sysdate;
        EXCEPTION
          when no_data_found then
                l_cycle_count_header_id := 0;
          when too_many_rows then
                l_cycle_count_header_id := -1;
          when others then
                l_cycle_count_header_id := -2;
        END;
    
    
      return l_cycle_count_header_id;
    
    
    EXCEPTION
    
      WHEN others THEN
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in GET_DEFAULT_CC_HEADER_ID ';
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

    
    END GET_DEFAULT_CC_HEADER_ID;
    
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_WC_ITEM_LOV                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for items and upc code                                                                                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
     *                                                     RF - Cycle Counting                                                                *
     *   1.1        03-MAY-2016  Lee Spitzer               TMS Ticket Number 20151210-00014 - RF - PI Enhancements
     *****************************************************************************************************************************************/


    PROCEDURE GET_WC_ITEM_LOV ( x_item             OUT    NOCOPY t_genref --0
                              , p_organization_id  IN     VARCHAR2
                              , p_item             IN     VARCHAR2
                              , p_cycle_count_header_id IN VARCHAR2
                              , p_entry_status_code IN VARCHAR2
                              , p_wc_count_list_sequence IN VARCHAR2
                              , p_wc_locator        IN VARCHAR2
                              , p_subinventory            IN VARCHAR2 --Added 05/03/2016 TMS Ticket 20151210-00014
                              , p_doctype                 IN VARCHAR2 --Added 05/03/2016 TMS Ticket 20151210-00014
                             )
      IS
      
      l_organization_id NUMBER;
      l_cycle_count_header_id NUMBER;
      l_entry_status_code NUMBER;
   
   BEGIN               

      g_call_from := 'GET_WC_ITEM_LOV';
      g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_organization_id ' || p_organization_id);
              debug_log('p_item ' || p_item);
              debug_log('p_cycle_count_header_id ' || p_cycle_count_header_id);
              debug_log('p_entry_status_code ' || p_entry_status_code);
              debug_log('p_wc_count_list_sequence ' || p_wc_count_list_sequence);
              debug_log('p_wc_locator ' || p_wc_locator);
              debug_log('p_subinventory ' || p_subinventory); --Added 05/03/2016 TMS Ticket 20151210-00014
              debug_log('p_doctype ' || p_doctype); --Added 05/03/2016 TMS Ticket 20151210-00014                
        END IF;
    
      g_call_point := 'Convert organization_id to number';
      
      BEGIN
        SELECT to_number(p_organization_id)
        INTO   l_organization_id
        FROM   dual;
      EXCEPTION
        WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting organization_id';
              debug_log(g_message);
              raise g_exception;
      END;
    
    g_call_point := 'Convert cycle_count_header_id to number';
      
      BEGIN
        SELECT to_number(p_cycle_count_header_id)
        INTO   l_cycle_count_header_id
        FROM   dual;
      EXCEPTION
        WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting cycle_count_header_id';
              debug_log(g_message);
              raise g_exception;
      END;
    
    g_call_point := 'Convert entry_status_code to number';
      
      BEGIN
        SELECT to_number(p_entry_status_code)
        INTO   l_entry_status_code
        FROM   dual;
      EXCEPTION
        WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting entry_status_code';
              debug_log(g_message);
              raise g_exception;
      END;

         --Added 5/12/2015 to prevent open cursors
        if x_item%isOPEN then
           debug_log('closing x_item cursor');
            close x_item;
        
        end if;
        
        IF l_entry_status_code != 3 THEN 
          IF p_subinventory IS NULL AND p_doctype = 'PHYSICAL' THEN-- Added 05/03/2016 TMS Ticket 20151210-00014
            OPEN x_item FOR
              SELECT  item.inventory_item_id             --1
                    , item.concatenated_segments         --2
                    , item.primary_uom_code              --3
                    , item.description                   --4
                    , item.lot_control_code              --5
                    , item.serial_number_control_code    --6
                    , item.shelf_life_days               --7
                    , item.min_minmax_quantity           --8
                    , item.max_minmax_quantity           --9
                    , item.sv                           --10
                    , item.wc_count_list_sequence       --11
                    , item.subinventory                 --12
                    , item.stock_locator                --13
                    , item.wc_stock_locator             --14
                    , item.locator_id                   --15
                    , item.cycle_count_entry_id         --16
                    , item.lot_number                   --17
                    , item.count_list_sequence          --18
                    , item.adjustment_amount            --19
                    , item.cost_group_id                --20
                    , item.quantity                     --21
                    --, to_char(to_date(upper(item.count_due_date),'DD-MON-YYYY'),'DD-MON-YYYY')     --22
                    , to_char(item.count_due_date,'DD-MON-YYYY') --22
                    , item.count_quantity_current       --23
                    , item.snap_shot_quantity           --24
              FROM ( 
                      SELECT  msi.inventory_item_id             --1
                            , msi.concatenated_segments         --2
                            , msi.primary_uom_code              --3
                            , msi.description                   --4
                            , msi.lot_control_code              --5
                            , msi.serial_number_control_code    --6
                            , msi.shelf_life_days               --7
                            , msi.min_minmax_quantity           --8
                            , msi.max_minmax_quantity           --9
                            , xxwc_cc_mobile_pkg.get_sv_value(l_organization_id, msi.inventory_item_id) sv --10
                            , mcce.wc_count_list_sequence       --11
                            , mcce.subinventory                 --12
                            , mcce.stock_locator                --13
                            , mcce.wc_stock_locator             --14
                            , mcce.locator_id                   --15
                            , mcce.cycle_count_entry_id         --16
                            , mcce.lot_number                   --17
                            , mcce.count_list_sequence          --18
                            , mcce.adjustment_amount            --19
                            , mcce.cost_group_id                --20
                            , xxwc_cc_mobile_pkg.get_cc_value(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.wc_count_list_sequence, mcce.count_list_sequence, l_organization_id, msi.inventory_item_id) quantity --21
                            , mcce.count_due_date               --22
                            , mcce.count_quantity_current       --23
                            , xxwc_cc_mobile_pkg.GET_SS_OH_QTY(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.organization_id, mcce.inventory_item_id, mcce.subinventory, mcce.lot_number) snap_shot_quantity
                      FROM   mtl_system_items_kfv msi,
                             xxwc_mtl_cycle_count_entries_v mcce
                      WHERE  msi.organization_id = l_organization_id
                      --AND    msi.concatenated_segments like nvl(p_item , msi.concatenated_segments) --removed 4/20/2015
                      AND    msi.concatenated_segments like nvl(upper(p_item), msi.concatenated_segments) --added 4/20/2015
                      AND    mcce.organization_id = msi.organization_id
                      AND    mcce.cycle_count_header_id = l_cycle_count_header_id
                      AND    mcce.inventory_item_id = msi.inventory_item_id
                      AND    mcce.entry_status_code = l_entry_status_code
                      --AND    mcce.wc_count_list_sequence = nvl(p_wc_count_list_sequence, mcce.wc_count_list_sequence)
                      AND    nvl(mcce.wc_count_list_sequence,-1) = nvl(p_wc_count_list_sequence, nvl(mcce.wc_count_list_sequence,-1))
                      --AND    (mcce.wc_stock_locator = nvl(p_wc_locator, mcce.wc_stock_locator) --Removed 05/03/2016 TMS Ticket 20151210-00014
                      AND    mcce.wc_stock_locator LIKE nvl(p_wc_locator||'%', mcce.wc_stock_locator) --Added 05/03/2016 TMS Ticket 20151210-00014
                      --OR     mcce.inventory_location_id is null)  --Removed 05/03/2016 TMS Ticket 20151210-00014
                      UNION
                      SELECT  msi.inventory_item_id
                            , msi.concatenated_segments
                            , msi.primary_uom_code
                            , msi.description
                            , msi.lot_control_code
                            , msi.serial_number_control_code
                            , msi.shelf_life_days
                            , msi.min_minmax_quantity
                            , msi.max_minmax_quantity
                            , xxwc_cc_mobile_pkg.get_sv_value(l_organization_id, msi.inventory_item_id) sv
                            , mcce.wc_count_list_sequence
                            , mcce.subinventory
                            , mcce.stock_locator
                            , mcce.wc_stock_locator
                            , mcce.locator_id
                            , mcce.cycle_count_entry_id
                            , mcce.lot_number
                            , mcce.count_list_sequence
                            , mcce.adjustment_amount
                            , mcce.cost_group_id
                            , xxwc_cc_mobile_pkg.get_cc_value(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.wc_count_list_sequence, mcce.count_list_sequence, l_organization_id, msi.inventory_item_id) quantity
                            , mcce.count_due_date --22
                            , mcce.count_quantity_current --23
                            , xxwc_cc_mobile_pkg.GET_SS_OH_QTY(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.organization_id, mcce.inventory_item_id, mcce.subinventory, mcce.lot_number) snap_shot_quantity
                      FROM   mtl_system_items_kfv msi,
                             xxwc_mtl_cycle_count_entries_v mcce
                      WHERE  msi.organization_id = l_organization_id
                      AND    mcce.organization_id = msi.organization_id
                      AND    mcce.cycle_count_header_id = p_cycle_count_header_id
                      AND    mcce.inventory_item_id = msi.inventory_item_id
                      AND    mcce.entry_status_code = l_entry_status_code
                      --AND    (mcce.wc_stock_locator = nvl(p_wc_locator, mcce.wc_stock_locator) --Removed 05/03/2016 TMS Ticket 20151210-00014
                      AND    mcce.wc_stock_locator LIKE nvl(p_wc_locator||'%', mcce.wc_stock_locator) --Added 05/03/2016 TMS Ticket 20151210-00014
                      --OR     mcce.inventory_location_id is null)  --Removed 05/03/2016 TMS Ticket 20151210-00014
                      AND    EXISTS (SELECT mcr.inventory_item_id
                                     FROM   mtl_cross_references mcr
                                     WHERE  1=1--msi.organization_id = fnd_profile.VALUE('XXWC_ITEM_MASTER_ORG')
                                     --AND mcr.cross_reference_type = fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE') removed 5/12/2015
                                     AND mcr.organization_id IS NULL                                        
                                     AND mcr.inventory_item_id = msi.inventory_item_id
                                     --AND mcr.cross_reference like nvl(p_item , mcr.cross_reference)) --removed 4/20/2015
                                     AND mcr.cross_reference = upper(p_item)
                                     AND  exists (select mcrt.cross_reference_type --added exists cross reference type 5/12/2015 to include multiple cross reference types
                                                  from   mtl_cross_reference_types mcrt
                                                  where  nvl(mcrt.attribute1,'N') = 'Y'
                                                  AND    mcrt.cross_reference_type = mcr.cross_reference_type)
                                      )) item--added 4/20/2015
                      order by decode(item.quantity,
                                      null, 'N',
                                      'Y'), item.wc_stock_locator, item.concatenated_segments, item.subinventory, item.lot_number
                      ;
               ELSIF p_subinventory IS NOT NULL  AND p_doctype = 'PHYSICAL' THEN-- Added 05/03/2016 TMS Ticket 20151210-00014
                OPEN x_item FOR
                  SELECT  item.inventory_item_id             --1
                        , item.concatenated_segments         --2
                        , item.primary_uom_code              --3
                        , item.description                   --4
                        , item.lot_control_code              --5
                        , item.serial_number_control_code    --6
                        , item.shelf_life_days               --7
                        , item.min_minmax_quantity           --8
                        , item.max_minmax_quantity           --9
                        , item.sv                           --10
                        , item.wc_count_list_sequence       --11
                        , item.subinventory                 --12
                        , item.stock_locator                --13
                        , item.wc_stock_locator             --14
                        , item.locator_id                   --15
                        , item.cycle_count_entry_id         --16
                        , item.lot_number                   --17
                        , item.count_list_sequence          --18
                        , item.adjustment_amount            --19
                        , item.cost_group_id                --20
                        , item.quantity                     --21
                        --, to_char(to_date(upper(item.count_due_date),'DD-MON-YYYY'),'DD-MON-YYYY')     --22
                        , to_char(item.count_due_date,'DD-MON-YYYY') --22
                        , item.count_quantity_current       --23
                        , item.snap_shot_quantity           --24
                  FROM ( 
                          SELECT  msi.inventory_item_id             --1
                                , msi.concatenated_segments         --2
                                , msi.primary_uom_code              --3
                                , msi.description                   --4
                                , msi.lot_control_code              --5
                                , msi.serial_number_control_code    --6
                                , msi.shelf_life_days               --7
                                , msi.min_minmax_quantity           --8
                                , msi.max_minmax_quantity           --9
                                , xxwc_cc_mobile_pkg.get_sv_value(l_organization_id, msi.inventory_item_id) sv --10
                                , mcce.wc_count_list_sequence       --11
                                , mcce.subinventory                 --12
                                , mcce.stock_locator                --13
                                , mcce.wc_stock_locator             --14
                                , mcce.locator_id                   --15
                                , mcce.cycle_count_entry_id         --16
                                , mcce.lot_number                   --17
                                , mcce.count_list_sequence          --18
                                , mcce.adjustment_amount            --19
                                , mcce.cost_group_id                --20
                                , xxwc_cc_mobile_pkg.get_cc_value(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.wc_count_list_sequence, mcce.count_list_sequence, l_organization_id, msi.inventory_item_id) quantity --21
                                , mcce.count_due_date               --22
                                , mcce.count_quantity_current       --23
                                , xxwc_cc_mobile_pkg.GET_SS_OH_QTY(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.organization_id, mcce.inventory_item_id, mcce.subinventory, mcce.lot_number) snap_shot_quantity
                          FROM   mtl_system_items_kfv msi,
                                 xxwc_mtl_cycle_count_entries_v mcce
                          WHERE  msi.organization_id = l_organization_id
                          --AND    msi.concatenated_segments like nvl(p_item , msi.concatenated_segments) --removed 4/20/2015
                          AND    msi.concatenated_segments like nvl(upper(p_item), msi.concatenated_segments) --added 4/20/2015
                          AND    mcce.organization_id = msi.organization_id
                          AND    mcce.cycle_count_header_id = l_cycle_count_header_id
                          AND    mcce.inventory_item_id = msi.inventory_item_id
                          AND    mcce.entry_status_code = l_entry_status_code
                          --AND    mcce.wc_count_list_sequence = nvl(p_wc_count_list_sequence, mcce.wc_count_list_sequence)
                          AND    nvl(mcce.wc_count_list_sequence,-1) = nvl(p_wc_count_list_sequence, nvl(mcce.wc_count_list_sequence,-1))
                          --AND    (mcce.wc_stock_locator = nvl(p_wc_locator, mcce.wc_stock_locator) --Removed 05/03/2016 TMS Ticket 20151210-00014
                          AND    mcce.subinventory = nvl(p_subinventory, mcce.subinventory) --Added 05/03/2016 TMS Ticket 20151210-00014
                          UNION
                          SELECT  msi.inventory_item_id
                                , msi.concatenated_segments
                                , msi.primary_uom_code
                                , msi.description
                                , msi.lot_control_code
                                , msi.serial_number_control_code
                                , msi.shelf_life_days
                                , msi.min_minmax_quantity
                                , msi.max_minmax_quantity
                                , xxwc_cc_mobile_pkg.get_sv_value(l_organization_id, msi.inventory_item_id) sv
                                , mcce.wc_count_list_sequence
                                , mcce.subinventory
                                , mcce.stock_locator
                                , mcce.wc_stock_locator
                                , mcce.locator_id
                                , mcce.cycle_count_entry_id
                                , mcce.lot_number
                                , mcce.count_list_sequence
                                , mcce.adjustment_amount
                                , mcce.cost_group_id
                                , xxwc_cc_mobile_pkg.get_cc_value(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.wc_count_list_sequence, mcce.count_list_sequence, l_organization_id, msi.inventory_item_id) quantity
                                , mcce.count_due_date --22
                                , mcce.count_quantity_current --23
                                , xxwc_cc_mobile_pkg.GET_SS_OH_QTY(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.organization_id, mcce.inventory_item_id, mcce.subinventory, mcce.lot_number) snap_shot_quantity
                          FROM   mtl_system_items_kfv msi,
                                 xxwc_mtl_cycle_count_entries_v mcce
                          WHERE  msi.organization_id = l_organization_id
                          AND    mcce.organization_id = msi.organization_id
                          AND    mcce.cycle_count_header_id = p_cycle_count_header_id
                          AND    mcce.inventory_item_id = msi.inventory_item_id
                          AND    mcce.entry_status_code = l_entry_status_code
                          --AND    (mcce.wc_stock_locator = nvl(p_wc_locator, mcce.wc_stock_locator) --Removed 05/03/2016 TMS Ticket 20151210-00014
                          --OR     mcce.inventory_location_id is null)  --Removed 05/03/2016 TMS Ticket 20151210-00014
                          AND    mcce.subinventory = nvl(p_subinventory, mcce.subinventory) --Added 05/03/2016 TMS Ticket 20151210-00014
                          AND    EXISTS (SELECT mcr.inventory_item_id
                                         FROM   mtl_cross_references mcr
                                         WHERE  1=1--msi.organization_id = fnd_profile.VALUE('XXWC_ITEM_MASTER_ORG')
                                         --AND mcr.cross_reference_type = fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE') removed 5/12/2015
                                         AND mcr.organization_id IS NULL                                        
                                         AND mcr.inventory_item_id = msi.inventory_item_id
                                         --AND mcr.cross_reference like nvl(p_item , mcr.cross_reference)) --removed 4/20/2015
                                         AND mcr.cross_reference = upper(p_item)
                                         AND  exists (select mcrt.cross_reference_type --added exists cross reference type 5/12/2015 to include multiple cross reference types
                                                      from   mtl_cross_reference_types mcrt
                                                      where  nvl(mcrt.attribute1,'N') = 'Y'
                                                      AND    mcrt.cross_reference_type = mcr.cross_reference_type)
                                          )) item--added 4/20/2015
                          order by decode(item.quantity,
                                          null, 'N',
                                          'Y'), item.wc_stock_locator, item.concatenated_segments, item.subinventory, item.lot_number
                          ;
                ELSIF p_doctype = 'CYCLE' THEN-- Added 05/03/2016 TMS Ticket 20151210-00014
                OPEN x_item FOR
                  SELECT  item.inventory_item_id             --1
                        , item.concatenated_segments         --2
                        , item.primary_uom_code              --3
                        , item.description                   --4
                        , item.lot_control_code              --5
                        , item.serial_number_control_code    --6
                        , item.shelf_life_days               --7
                        , item.min_minmax_quantity           --8
                        , item.max_minmax_quantity           --9
                        , item.sv                           --10
                        , item.wc_count_list_sequence       --11
                        , item.subinventory                 --12
                        , item.stock_locator                --13
                        , item.wc_stock_locator             --14
                        , item.locator_id                   --15
                        , item.cycle_count_entry_id         --16
                        , item.lot_number                   --17
                        , item.count_list_sequence          --18
                        , item.adjustment_amount            --19
                        , item.cost_group_id                --20
                        , item.quantity                     --21
                        --, to_char(to_date(upper(item.count_due_date),'DD-MON-YYYY'),'DD-MON-YYYY')     --22
                        , to_char(item.count_due_date,'DD-MON-YYYY') --22
                        , item.count_quantity_current       --23
                        , item.snap_shot_quantity           --24
                  FROM ( 
                          SELECT  msi.inventory_item_id             --1
                                , msi.concatenated_segments         --2
                                , msi.primary_uom_code              --3
                                , msi.description                   --4
                                , msi.lot_control_code              --5
                                , msi.serial_number_control_code    --6
                                , msi.shelf_life_days               --7
                                , msi.min_minmax_quantity           --8
                                , msi.max_minmax_quantity           --9
                                , xxwc_cc_mobile_pkg.get_sv_value(l_organization_id, msi.inventory_item_id) sv --10
                                , mcce.wc_count_list_sequence       --11
                                , mcce.subinventory                 --12
                                , mcce.stock_locator                --13
                                , mcce.wc_stock_locator             --14
                                , mcce.locator_id                   --15
                                , mcce.cycle_count_entry_id         --16
                                , mcce.lot_number                   --17
                                , mcce.count_list_sequence          --18
                                , mcce.adjustment_amount            --19
                                , mcce.cost_group_id                --20
                                , xxwc_cc_mobile_pkg.get_cc_value(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.wc_count_list_sequence, mcce.count_list_sequence, l_organization_id, msi.inventory_item_id) quantity --21
                                , mcce.count_due_date               --22
                                , mcce.count_quantity_current       --23
                                , xxwc_cc_mobile_pkg.GET_SS_OH_QTY(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.organization_id, mcce.inventory_item_id, mcce.subinventory, mcce.lot_number) snap_shot_quantity
                          FROM   mtl_system_items_kfv msi,
                                 xxwc_mtl_cycle_count_entries_v mcce
                          WHERE  msi.organization_id = l_organization_id
                          --AND    msi.concatenated_segments like nvl(p_item , msi.concatenated_segments) --removed 4/20/2015
                          AND    msi.concatenated_segments like nvl(upper(p_item), msi.concatenated_segments) --added 4/20/2015
                          AND    mcce.organization_id = msi.organization_id
                          AND    mcce.cycle_count_header_id = l_cycle_count_header_id
                          AND    mcce.inventory_item_id = msi.inventory_item_id
                          AND    mcce.entry_status_code = l_entry_status_code
                          --AND    mcce.wc_count_list_sequence = nvl(p_wc_count_list_sequence, mcce.wc_count_list_sequence)
                          AND    nvl(mcce.wc_count_list_sequence,-1) = nvl(p_wc_count_list_sequence, nvl(mcce.wc_count_list_sequence,-1))
                          AND    (mcce.wc_stock_locator = nvl(p_wc_locator, mcce.wc_stock_locator)
                          OR     mcce.inventory_location_id is null)
                      UNION
                          SELECT  msi.inventory_item_id
                                , msi.concatenated_segments
                                , msi.primary_uom_code
                                , msi.description
                                , msi.lot_control_code
                                , msi.serial_number_control_code
                                , msi.shelf_life_days
                                , msi.min_minmax_quantity
                                , msi.max_minmax_quantity
                                , xxwc_cc_mobile_pkg.get_sv_value(l_organization_id, msi.inventory_item_id) sv
                                , mcce.wc_count_list_sequence
                                , mcce.subinventory
                                , mcce.stock_locator
                                , mcce.wc_stock_locator
                                , mcce.locator_id
                                , mcce.cycle_count_entry_id
                                , mcce.lot_number
                                , mcce.count_list_sequence
                                , mcce.adjustment_amount
                                , mcce.cost_group_id
                                , xxwc_cc_mobile_pkg.get_cc_value(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.wc_count_list_sequence, mcce.count_list_sequence, l_organization_id, msi.inventory_item_id) quantity
                                , mcce.count_due_date --22
                                , mcce.count_quantity_current --23
                                , xxwc_cc_mobile_pkg.GET_SS_OH_QTY(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.organization_id, mcce.inventory_item_id, mcce.subinventory, mcce.lot_number) snap_shot_quantity
                          FROM   mtl_system_items_kfv msi,
                                 xxwc_mtl_cycle_count_entries_v mcce
                          WHERE  msi.organization_id = l_organization_id
                          AND    mcce.organization_id = msi.organization_id
                          AND    mcce.cycle_count_header_id = p_cycle_count_header_id
                          AND    mcce.inventory_item_id = msi.inventory_item_id
                          AND    mcce.entry_status_code = l_entry_status_code
                          AND    nvl(mcce.wc_count_list_sequence,-1) = nvl(p_wc_count_list_sequence, nvl(mcce.wc_count_list_sequence,-1))
                          AND    (mcce.wc_stock_locator = nvl(p_wc_locator, mcce.wc_stock_locator)
                          OR     mcce.inventory_location_id IS NULL)
                          AND    EXISTS (SELECT mcr.inventory_item_id
                                         FROM   mtl_cross_references mcr
                                         WHERE  1=1--msi.organization_id = fnd_profile.VALUE('XXWC_ITEM_MASTER_ORG')
                                         --AND mcr.cross_reference_type = fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE') removed 5/12/2015
                                         AND mcr.organization_id IS NULL                                        
                                         AND mcr.inventory_item_id = msi.inventory_item_id
                                         --AND mcr.cross_reference like nvl(p_item , mcr.cross_reference)) --removed 4/20/2015
                                         AND mcr.cross_reference = upper(p_item)
                                         AND  exists (select mcrt.cross_reference_type --added exists cross reference type 5/12/2015 to include multiple cross reference types
                                                      from   mtl_cross_reference_types mcrt
                                                      where  nvl(mcrt.attribute1,'N') = 'Y'
                                                      AND    mcrt.cross_reference_type = mcr.cross_reference_type)
                                          )) item--added 4/20/2015
                          order by decode(item.quantity,
                                          null, 'N',
                                          'Y'), item.wc_stock_locator, item.concatenated_segments, item.subinventory, item.lot_number
                          ;
                END IF;
          ELSE
            
            OPEN x_item FOR
              SELECT  item.inventory_item_id             --1
                    , item.concatenated_segments         --2
                    , item.primary_uom_code              --3
                    , item.description                   --4
                    , item.lot_control_code              --5
                    , item.serial_number_control_code    --6
                    , item.shelf_life_days               --7
                    , item.min_minmax_quantity           --8
                    , item.max_minmax_quantity           --9
                    , item.sv                           --10
                    , item.wc_count_list_sequence       --11
                    , item.subinventory                 --12
                    , item.stock_locator                --13
                    , item.wc_stock_locator             --14
                    , item.locator_id                   --15
                    , item.cycle_count_entry_id         --16
                    , item.lot_number                   --17
                    , item.count_list_sequence          --18
                    , item.adjustment_amount            --19
                    , item.cost_group_id                --20
                    , item.quantity                     --21
                    --, to_char(to_date(upper(item.count_due_date),'DD-MON-YYYY'),'DD-MON-YYYY')     --22
                    , to_char(item.count_due_date,'DD-MON-YYYY') --22
                    , item.count_quantity_current       --23
                    , item.snap_shot_quantity           --24
              FROM ( 
                      SELECT  msi.inventory_item_id             --1
                            , msi.concatenated_segments         --2
                            , msi.primary_uom_code              --3
                            , msi.description                   --4
                            , msi.lot_control_code              --5
                            , msi.serial_number_control_code    --6
                            , msi.shelf_life_days               --7
                            , msi.min_minmax_quantity           --8
                            , msi.max_minmax_quantity           --9
                            , xxwc_cc_mobile_pkg.get_sv_value(l_organization_id, msi.inventory_item_id) sv --10
                            , mcce.wc_count_list_sequence       --11
                            , mcce.subinventory                 --12
                            , mcce.stock_locator                --13
                            , mcce.wc_stock_locator             --14
                            , mcce.locator_id                   --15
                            , mcce.cycle_count_entry_id         --16
                            , mcce.lot_number                   --17
                            , mcce.count_list_sequence          --18
                            , mcce.adjustment_amount            --19
                            , mcce.cost_group_id                --20
                            , xxwc_cc_mobile_pkg.get_cc_value(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.wc_count_list_sequence, mcce.count_list_sequence, l_organization_id, msi.inventory_item_id) quantity --21
                            , mcce.count_due_date               --22
                            , mcce.count_quantity_current       --23
                            , xxwc_cc_mobile_pkg.GET_SS_OH_QTY(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.organization_id, mcce.inventory_item_id, mcce.subinventory, mcce.lot_number) snap_shot_quantity
                      FROM   mtl_system_items_kfv msi,
                             xxwc_mtl_cycle_count_entries_v mcce
                      WHERE  msi.organization_id = l_organization_id
                      --AND    msi.concatenated_segments like nvl(p_item , msi.concatenated_segments) --removed 4/20/2015
                      AND    msi.concatenated_segments like nvl(upper(p_item), msi.concatenated_segments) --added 4/20/2015
                      AND    mcce.organization_id = msi.organization_id
                      AND    mcce.cycle_count_header_id = l_cycle_count_header_id
                      AND    mcce.inventory_item_id = msi.inventory_item_id
                      AND    mcce.entry_status_code = l_entry_status_code
                      AND    (mcce.wc_stock_locator = nvl(p_wc_locator, mcce.wc_stock_locator) 
                      OR     mcce.inventory_location_id is null) 
                      UNION
                      SELECT  msi.inventory_item_id
                            , msi.concatenated_segments
                            , msi.primary_uom_code
                            , msi.description
                            , msi.lot_control_code
                            , msi.serial_number_control_code
                            , msi.shelf_life_days
                            , msi.min_minmax_quantity
                            , msi.max_minmax_quantity
                            , xxwc_cc_mobile_pkg.get_sv_value(l_organization_id, msi.inventory_item_id) sv
                            , mcce.wc_count_list_sequence
                            , mcce.subinventory
                            , mcce.stock_locator
                            , mcce.wc_stock_locator
                            , mcce.locator_id
                            , mcce.cycle_count_entry_id
                            , mcce.lot_number
                            , mcce.count_list_sequence
                            , mcce.adjustment_amount        
                            , mcce.cost_group_id
                            , xxwc_cc_mobile_pkg.get_cc_value(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.wc_count_list_sequence, mcce.count_list_sequence, l_organization_id, msi.inventory_item_id) quantity
                            , mcce.count_due_date --22
                            , mcce.count_quantity_current --23
                            , xxwc_cc_mobile_pkg.GET_SS_OH_QTY(mcce.cycle_count_header_id, mcce.cycle_count_entry_id, mcce.organization_id, mcce.inventory_item_id, mcce.subinventory, mcce.lot_number) snap_shot_quantity
                      FROM   mtl_system_items_kfv msi,
                             xxwc_mtl_cycle_count_entries_v mcce
                      WHERE  msi.organization_id = l_organization_id
                      AND    mcce.organization_id = msi.organization_id
                      AND    mcce.cycle_count_header_id = p_cycle_count_header_id
                      AND    mcce.inventory_item_id = msi.inventory_item_id
                      AND    mcce.entry_status_code = l_entry_status_code
                      AND    (mcce.wc_stock_locator = nvl(p_wc_locator, mcce.wc_stock_locator) --Removed 05/03/2016 TMS Ticket 20151210-00014
                      OR     mcce.inventory_location_id is null)  --Removed 05/03/2016 TMS Ticket 20151210-00014
                      AND    EXISTS (SELECT mcr.inventory_item_id
                                     FROM   mtl_cross_references mcr
                                     WHERE  1=1--msi.organization_id = fnd_profile.VALUE('XXWC_ITEM_MASTER_ORG')
                                     --AND mcr.cross_reference_type = fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE') removed 5/12/2015
                                     AND mcr.organization_id IS NULL                                        
                                     AND mcr.inventory_item_id = msi.inventory_item_id
                                     --AND mcr.cross_reference like nvl(p_item , mcr.cross_reference)) --removed 4/20/2015
                                     AND mcr.cross_reference = upper(p_item)
                                     AND  exists (select mcrt.cross_reference_type --added exists cross reference type 5/12/2015 to include multiple cross reference types
                                                  from   mtl_cross_reference_types mcrt
                                                  where  nvl(mcrt.attribute1,'N') = 'Y'
                                                  AND    mcrt.cross_reference_type = mcr.cross_reference_type)
                                      )) item--added 4/20/2015
                      order by   decode(item.quantity,
                                      null, 'N',
                                      'Y'), abs(item.adjustment_amount) desc, item.concatenated_segments, item.subinventory, item.lot_number, item.wc_stock_locator
                      ;
                
              
                  END IF;
           

    EXCEPTION
        WHEN g_exception THEN

            --Added 5/12/2015 to prevent open cursors
            if x_item%isOPEN then
               debug_log('closing x_item cursor');
                close x_item;
            
            end if;

            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          
          
          
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others ' ||g_sqlcode || g_sqlerrm;
            
            
             --Added 5/12/2015 to prevent open cursors
              if x_item%isOPEN then
                 debug_log('closing x_item cursor');
                  close x_item;
              
              end if;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
                
     
    END GET_WC_ITEM_LOV;
    
   /*****************************************************************************************************************************************
   *   FUNCTION GET_SV_VALUE                                                                                                                *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_SV_VALUE  ( p_organization_id IN NUMBER
                           , p_inventory_item_id IN NUMBER
                           )
        RETURN VARCHAR2
      
    IS
        
    
      l_sales_velocity VARCHAR2(80);
    
    BEGIN
    
        g_call_from := 'GET_SV_VALUE';
        g_call_point := 'Start';
    
    
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        
        BEGIN
           
          select mck.concatenated_segments
          into   l_sales_velocity
          from   mtl_item_categories mic,
                 mtl_categories_kfv mck
          where  mic.organization_id = p_organization_id
          and    mic.inventory_item_id = p_inventory_item_id
          and    mic.category_id = mck.category_id
          and    exists (
                          select *
                          from   mtl_category_sets mcs
                          where  mcs.category_set_name = 'Sales Velocity'
                          and    mcs.category_set_id = mic.category_set_id);
          EXCEPTION
            WHEN others THEN
                l_sales_velocity := '';
          END;
          
      return l_sales_velocity;
    
    
    EXCEPTION
    
    
      WHEN others THEN
    
        return '';
    
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in GET_DEFAULT_CC_HEADER_ID ';
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

    END GET_SV_VALUE;
    
    
        
   /*****************************************************************************************************************************************
   *   FUNCTION PROCESS_CC_ENTRIES_TBL                                                                                                      *
   *                                                                                                                                        *
   *   PURPOSE:  Process Cycle Entries Table - inserts and updates into XXWC.XXWC_CC_ENTRIES_TABLE                                          *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              

        
    PROCEDURE PROCESS_CC_ENTRIES_TBL ( p_cycle_count_header_id  IN VARCHAR2 --1
                                     , p_cycle_count_entry_id   IN VARCHAR2 --2
                                     , p_wc_count_list_sequence IN VARCHAR2 --3
                                     , p_count_list_sequence    IN VARCHAR2 --4
                                     , p_organization_id        IN VARCHAR2 --5
                                     , p_inventory_item_id      IN VARCHAR2 --6
                                     , p_revision               IN VARCHAR2 --7
                                     , p_quantity               IN VARCHAR2 --8
                                     , p_primary_uom_code       IN VARCHAR2 --9
                                     , p_subinventory           IN VARCHAR2 --10
                                     , p_locator_id             IN VARCHAR2 --11
                                     , p_lot_number             IN VARCHAR2 --12
                                     , p_cost_group_id          IN VARCHAR2 --13
                                     , p_user_id                IN VARCHAR2 --14
                                     , x_return                 OUT NUMBER --15
                                     , x_message                OUT VARCHAR2 --16
                                     ) 
        
          
      IS
    
    
    l_cycle_count_header_id  NUMBER;
    l_cycle_count_entry_id   NUMBER;
    l_wc_count_list_sequence NUMBER;
    l_count_list_sequence    NUMBER;
    l_organization_id        NUMBER;
    l_inventory_item_id      NUMBER;
    l_quantity               NUMBER;
    l_quantity_check         NUMBER;
    l_locator_id             NUMBER;
    l_cost_group_id          NUMBER;
    l_user_id                NUMBER;
    l_employee_id            NUMBER;
    l_exists                 number;
		l_qty                    number;
		l_sum_qty                number;
		
		l_status							 	 VARCHAR2(1);
		l_status_message				 VARCHAR2(50);
		
		x_interface_id					 NUMBER;
  	x_status                 VARCHAR2(1);
    x_error_code             VARCHAR2(240);
    x_msg_count              NUMBER;
    x_msg                    VARCHAR2(2000);
    
BEGIN
	
        g_call_from := 'PROCESS_CC_ENTRIES_TBL';
        g_call_point := 'Start';
        
        
        debug_log('p_cycle_count_header_id ' || p_cycle_count_header_id);
        debug_log('p_cycle_count_entry_id ' ||  p_cycle_count_entry_id);
        debug_log('p_wc_count_list_sequence ' || p_wc_count_list_sequence);
        debug_log('p_count_list_sequence ' || p_count_list_sequence);   
        debug_log('p_organization_id ' || p_organization_id);       
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_revision ' || p_revision);         
        debug_log('p_quantity ' || p_quantity);         
        debug_log('p_primary_uom_code ' || p_primary_uom_code); 
        debug_log('p_subinventory ' || p_subinventory );     
        debug_log('p_locator_id ' || p_locator_id);
        debug_log('p_lot_number ' || p_lot_number);
        debug_log('p_cost_group_id ' || p_cost_group_id);    
        debug_log('p_user_id ' || p_user_id);
	
	
 
    g_call_point := 'convert p_cycle_count_header_id to number ';
    
    BEGIN
      select to_number(p_cycle_count_header_id)
      into   l_cycle_count_header_id
      from   dual;
    EXCEPTION
      WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error converting to number';
          raise g_exception;
    END;
  
    g_call_point := 'convert p_cycle_count_entry_id to number ';
    
    BEGIN
      select to_number(p_cycle_count_entry_id)
      into   l_cycle_count_entry_id
      from   dual;
    EXCEPTION
      WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error converting to number';
          raise g_exception;
    END;
  
    g_call_point := 'convert p_wc_count_list_sequence to number ';
    
    BEGIN
      select to_number(p_wc_count_list_sequence)
      into   l_wc_count_list_sequence
      from   dual;
    EXCEPTION
      WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error converting to number';
          raise g_exception;
    END;
    
    g_call_point := 'convert p_count_list_sequence to number ';
    
    BEGIN
      select to_number(p_count_list_sequence)
      into   l_count_list_sequence
      from   dual;
    EXCEPTION
      WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error converting to number';
          raise g_exception;
    END;
    
    g_call_point := 'convert p_organization_id to number ';
    
    BEGIN
      select to_number(p_organization_id)
      into   l_organization_id
      from   dual;
    EXCEPTION
      WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error converting to number';
          raise g_exception;
    END;
    
    g_call_point := 'convert p_inventory_item_id to number ';
    
    BEGIN
      select to_number(p_inventory_item_id)
      into   l_inventory_item_id
      from   dual;
    EXCEPTION
      WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error converting to number';
          raise g_exception;
    END;
    
    g_call_point := 'convert p_quantity to number ';
    
    BEGIN
      select to_number(p_quantity)
      into   l_quantity
      from   dual;
    EXCEPTION
      WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error converting to number';
          raise g_exception;
    END;
    
    g_call_point := 'convert p_locator_id to number ';
    
    BEGIN
      select to_number(p_locator_id)
      into   l_locator_id
      from   dual;
    EXCEPTION
      WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error converting to number';
          raise g_exception;
    END;
    
    g_call_point := 'convert p_cost_group_id to number ';
    
    BEGIN
      select to_number(p_cost_group_id)
      into   l_cost_group_id
      from   dual;
    EXCEPTION
      WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error converting to number';
          raise g_exception;
    END;

    g_call_point := 'convert p_user_id to number ';
    
    BEGIN
      select to_number(p_user_id)
      into   l_user_id
      from   dual;
    EXCEPTION
      WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Error converting to number';
          raise g_exception;
    END;    
    
    
        debug_log('l_cycle_count_header_id ' || l_cycle_count_header_id);
        debug_log('l_cycle_count_entry_id ' ||  l_cycle_count_entry_id);
        debug_log('l_wc_count_list_sequence ' || l_wc_count_list_sequence);
        debug_log('l_count_list_sequence ' || l_count_list_sequence);   
        debug_log('l_organization_id ' || l_organization_id);       
        debug_log('l_inventory_item_id ' || l_inventory_item_id);
        debug_log('l_quantity ' || l_quantity);         
        debug_log('l_locator_id ' || l_locator_id);       
        debug_log('l_cost_group_id ' || l_cost_group_id);    
        debug_log('l_user_id ' || l_user_id);
	

 g_call_point := 'get employee id from fnd_user ';
   
  
	BEGIN
		select employee_id
		INTO   l_employee_id
		from   fnd_user
    where  user_id = l_user_id;
	EXCEPTION
			WHEN OTHERS THEN
					 g_sqlcode := SQLCODE;
           g_sqlerrm := SQLERRM;
           g_message := 'Error getting employee_id';
           raise g_exception;
	END;

 
 g_call_point := 'get count on entries table ';
   
  
	BEGIN
		select nvl(count(*),0)
		INTO   l_exists
		from   XXWC.XXWC_CC_ENTRY_TABLE 
		where  cycle_count_header_id = l_cycle_count_header_id
		and    nvl(cycle_count_entry_id,-1)  = nvl(l_cycle_count_entry_id,-1)
		and    nvl(wc_count_list_sequence,-1) = nvl(l_wc_count_list_sequence,-1)
		and    nvl(count_list_sequence,-1)   = nvl(l_count_list_sequence,-1)
		and    inventory_item_id		= l_inventory_item_id
		and    organization_id			= l_organization_id
		AND    process_flag = 1;
		
	EXCEPTION
			WHEN OTHERS THEN
					 l_exists := 0;
	END;
	
	
    debug_log('l_exists ' || l_exists);
  
	if l_exists > 0 then  

          g_call_point := 'checking quantity on cycle count table';
   
        
        	BEGIN
            select quantity
            INTO   l_quantity_check
            from   XXWC.XXWC_CC_ENTRY_TABLE 
            where  cycle_count_header_id = l_cycle_count_header_id
            and    nvl(cycle_count_entry_id,-1)  = nvl(l_cycle_count_entry_id,-1)
            and    nvl(wc_count_list_sequence,-1) = nvl(l_wc_count_list_sequence,-1)
            and    nvl(count_list_sequence,-1)   = nvl(l_count_list_sequence,-1)
            and    inventory_item_id		= l_inventory_item_id
            and    organization_id			= l_organization_id
            AND    process_flag = 1;
            
          EXCEPTION
              WHEN OTHERS THEN
                   l_quantity_check := NULL;
          END;

          debug_log('l_quantity_check ' || l_quantity_check);
          
        
				if l_quantity is not null AND l_quantity != l_quantity_check THEN
					
            g_call_point := 'updating cycle count table';
   
            begin
						
						update xxwc.xxwc_cc_entry_table set quantity = nvl(l_quantity,0), 
																								process_flag = 1,
																								last_update_date  = SYSDATE,
																								last_updated_by = FND_GLOBAL.USER_ID,
																								employee_id	= FND_GLOBAL.EMPLOYEE_ID
						where  cycle_count_header_id = l_cycle_count_header_id
						and    nvl(cycle_count_entry_id,-1)  = nvl(l_cycle_count_entry_id,-1)
						and    nvl(wc_count_list_sequence,-1) = nvl(l_wc_count_list_sequence,-1)
						and    nvl(count_list_sequence,-1)   = nvl(l_count_list_sequence,-1)
						and    inventory_item_id		= l_inventory_item_id
						and    organization_id			= l_organization_id
						;
            
          EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error updating XXWC.XXWC_CC_ENTRIES_TABLE';
              raise g_exception;
          END;
        
						commit;

				ELSIF l_quantity is null THEN
							
            g_call_point := 'deleting cycle count table';
   
        BEGIN  
						DELETE xxwc.xxwc_cc_entry_table
						where  cycle_count_header_id = l_cycle_count_header_id
						and    nvl(cycle_count_entry_id,-1)  = nvl(l_cycle_count_entry_id,-1)
						and    nvl(wc_count_list_sequence,-1) = nvl(l_wc_count_list_sequence,-1)
						and    nvl(count_list_sequence,-1)   = nvl(l_count_list_sequence,-1)
						and    inventory_item_id		= l_inventory_item_id
						and    organization_id			= l_organization_id
						AND    process_flag = 1;
        EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error deleting XXWC.XXWC_CC_ENTRIES_TABLE';
              raise g_exception;
        END;
            
						commit;
						
						
				END IF;		


	else
		
		
			if l_quantity is not null then
						
      
       g_call_point := 'inserting into cycle count table';
		
      BEGIN
      
        insert into xxwc.xxwc_cc_entry_table
          (creation_date
          ,created_by
          ,last_update_date
          ,last_updated_by
          ,employee_id
          ,cycle_count_header_id  
          ,cycle_count_entry_id   
          ,wc_count_list_sequence 
          ,count_list_sequence    
          ,organization_id
          ,inventory_item_id
          ,revision
          ,quantity         
          ,primary_uom_code
          ,subinventory
          ,lot_number
          ,locator_id
          ,cost_group_id      
          ,process_flag           
          )
          VALUES
          (SYSDATE
          ,l_user_id 
          ,SYSDATE
          ,l_user_id
          ,l_employee_id
          ,l_CYCLE_COUNT_HEADER_ID
          ,l_CYCLE_COUNT_ENTRY_ID
          ,l_WC_COUNT_LIST_SEQUENCE
          ,l_COUNT_LIST_SEQUENCE
          ,l_ORGANIZATION_ID
          ,l_INVENTORY_ITEM_ID
          ,p_revision
          ,l_QUANTITY
          ,p_PRIMARY_UOM_CODE
          ,p_SUBINVENTORY
          ,p_LOT_NUMBER
          ,l_LOCATOR_ID
          ,l_COST_GROUP_ID
          ,1);
        EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error inserting XXWC.XXWC_CC_ENTRIES_TABLE';
              raise g_exception;
        END;

     		commit;
        
			END IF;
			
    END IF;
    
    
      x_return := 0;
      x_message := 'Success';
    
    
    EXCEPTION

   WHEN g_exception THEN
   
      x_return := 1;
      x_message := g_message;
      
      debug_log (g_message);
      
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

    
      WHEN others THEN
      
      
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'When others in ' || g_package||'.'||g_call_from  || ' ' || g_call_point || ' ' || g_sqlcode || ' ' || g_sqlerrm;
      
        x_return := 1;
        x_message := g_message;
      
        debug_log (g_message);
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


    
    END PROCESS_CC_ENTRIES_TBL;
    
    
   /*****************************************************************************************************************************************
   *   FUNCTION GET_CC_VALUE                                                                                                                *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_CC_VALUE  ( p_cycle_count_header_id  IN NUMBER --1
                           , p_cycle_count_entry_id   IN NUMBER --2
                           , p_wc_count_list_sequence IN NUMBER --3
                           , p_count_list_sequence    IN NUMBER --4
                           , p_organization_id IN NUMBER
                           , p_inventory_item_id IN NUMBER
                           )
        RETURN VARCHAR2 IS
    
    l_quantity_number NUMBER;  
    l_quantity VARCHAR2(2000); 
        
    BEGIN
    
        g_call_from := 'GET_CC_VALUE';
        g_call_point := 'Start';
    
        debug_log('p_cycle_count_header_id ' || p_cycle_count_header_id);
        debug_log('p_cycle_count_entry_id ' || p_cycle_count_entry_id);
        debug_log('p_wc_count_list_sequence ' || p_wc_count_list_sequence);
        debug_log('p_count_list_sequence ' || p_count_list_sequence);
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);

    
    BEGIN
            select quantity
            INTO   l_quantity_number
            from   XXWC.XXWC_CC_ENTRY_TABLE 
            where  cycle_count_header_id = p_cycle_count_header_id
            and    nvl(cycle_count_entry_id,-1)  = nvl(p_cycle_count_entry_id,-1)
            and    nvl(wc_count_list_sequence,-1) = nvl(p_wc_count_list_sequence,-1)
            and    nvl(count_list_sequence,-1)   = nvl(p_count_list_sequence,-1)
            and    inventory_item_id		= p_inventory_item_id
            and    organization_id			= p_organization_id
            AND    process_flag = 1;
            
          EXCEPTION
              WHEN OTHERS THEN
                   l_quantity_number := null;
          END;
          
        debug_log('l_quantity_number ' || l_quantity_number);
        
        g_call_point := 'Convert l_quantity_number to character';
        
        begin
          select nvl(to_char(l_quantity_number),'BLANK')
          into   l_quantity
          from dual;
        EXCEPTION
              WHEN OTHERS THEN
                   l_quantity := 'BLANK';
        END;
          
          
         debug_log('l_quantity ' || l_quantity);
         
      return nvl(l_quantity,'BLANK');

    EXCEPTION
    
      WHEN others THEN
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in GET_CC_VALUE ';
        
        return '';
        
        debug_log('l_quantity ' || l_quantity);
        
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


    END GET_CC_VALUE;
    

   /*****************************************************************************************************************************************
   *   FUNCTION PROCESS_API                                                                                                                 *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
    
    
    PROCEDURE PROCESS_API ( p_organization_id       IN VARCHAR2 --1
                          , p_cycle_count_header_id IN VARCHAR2 --2
                          , p_entry_status_code     IN VARCHAR2 --3
                          , p_user_id               IN VARCHAR2 --4
                          , x_return                OUT NUMBER --5
                          , x_message               OUT VARCHAR2) --6
        
        IS	
  
    x_interface_id					 NUMBER;
  	x_status                 VARCHAR2(1);
    x_error_code             VARCHAR2(240);
    x_msg_count              NUMBER;
    x_msg                    VARCHAR2(2000);
    
    l_message								 VARCHAR2(2000);
  
  	currRec 								 INTEGER;  	
		l_count_list_sequence 	 NUMBER;
		l_status								 VARCHAR2(1);
		l_check									 NUMBER; --Added TMS Ticket 20130715-00437
		
		cursor c_items is
		 select   xcet.cycle_count_header_id
					   ,xcet.cycle_count_entry_id
					   ,xcet.count_list_sequence
					   ,xcet.organization_id
					   ,xcet.inventory_item_id
					   ,xcet.revision
					   ,xcet.primary_uom_code
					   ,xcet.subinventory
					   ,xcet.lot_number
					   ,xcet.locator_id
					   ,xcet.cost_group_id
					   ,xcet.process_flag
					   ,xcet.message
					   ,sum(xcet.quantity) sum_quantity
			from 	 XXWC.XXWC_CC_ENTRY_TABLE xcet
			where  xcet.process_flag = 1					 
			and    p_organization_id = xcet.organization_id
			and    p_cycle_count_header_id = xcet.cycle_count_header_id
      and    exists (select *
                     from   mtl_cycle_count_entries mcce
                     where  p_entry_status_code = mcce.entry_status_code
                     and    xcet.organization_id = mcce.organization_id
                     and    xcet.cycle_count_header_id = mcce.cycle_count_header_id)
			group  by 
						  --employee_id --Removed TMS Ticket 20130715-00437
					    xcet.cycle_count_header_id
					   ,xcet.cycle_count_entry_id
					   --,wc_count_list_sequence
					   ,xcet.count_list_sequence
					   ,xcet.organization_id
					   ,xcet.inventory_item_id
					   ,xcet.revision
					   ,xcet.primary_uom_code
					   ,xcet.subinventory
					   ,xcet.lot_number
					   ,xcet.locator_id
					   ,xcet.cost_group_id
					   ,xcet.process_flag
					   ,xcet.message
					   ;
					   
		l_exists number;
		l_exists_lock  number;
		l_batch_id number;
		l_return  NUMBER DEFAULT 0;

          l_user_id             NUMBER;
          l_resp_id             NUMBER;
          l_resp_appl_id        NUMBER;

			
  BEGIN
	
        g_call_from := 'PROCESS_API';
        g_call_point := 'Start';
    
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_cycle_count_header_id ' || p_cycle_count_header_id);
        debug_log('p_entry_status_code ' || p_entry_status_code);
        debug_log('p_user_id ' || p_user_id);
      
        g_call_point := 'Getting next batch id';
        
        
       MO_GLOBAL.INIT('INV');
       
          
       l_user_id := fnd_profile.VALUE('USER_ID');
       l_resp_id := fnd_profile.VALUE('RESP_ID');
       l_resp_appl_id := fnd_profile.VALUE('RESP_APPL_ID');
            
       debug_log('l_user_id ' || l_user_id);
       debug_log('l_resp_id ' || l_resp_id);
       debug_log('l_resp_appl_id ' || l_resp_appl_id);
          
       fnd_global.apps_initialize(l_user_id,l_resp_id,l_resp_appl_id); --Required for Multi-Org or expected receipts report will error
      
 
        
        BEGIN
          select XXWC.XXWC_CC_BATCH_S.nextval into l_batch_id from dual;
        EXCEPTION
          WHEN OTHERS THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error getting next batch id ';
              raise g_exception;
              
        END;

        debug_log('l_batch_id ' || l_batch_id);
        
        g_call_point := 'Starting loop';
				
				for r_items in c_items 
			
          loop
				
				
            XXWC_CC_ENTRIES_PKG.process_api
            (p_cycle_count_header_id  => r_items.cycle_count_header_id
            ,p_cycle_count_entry_id   => r_items.cycle_count_entry_id
            ,p_count_list_sequence    => r_items.count_list_sequence 
            ,p_organization_id        => r_items.organization_id
            ,p_inventory_item_id      => r_items.inventory_item_id
            ,p_revision               => r_items.revision
            ,p_count_quantity         => r_items.sum_quantity
            ,p_count_uom              => r_items.primary_uom_code
            ,p_subinventory           => r_items.subinventory
            ,p_lot_number             => r_items.lot_number
            ,p_locator_id             => r_items.locator_id
            ,p_cost_group_id          => r_items.cost_group_id
            ,p_user_id                => FND_GLOBAL.USER_ID
            ,p_employee_id            => FND_GLOBAL.EMPLOYEE_ID
            ,p_date                   => sysdate
            ,x_interface_id						=> x_interface_id
            ,x_status                 => x_status
            ,x_error_code             => x_error_code
            ,x_msg_count              => x_msg_count
            ,x_msg                    => x_msg
            );
          
				
            debug_log('x_interface_id ' || x_interface_id);
            debug_log('x_status ' || x_status);
            debug_log('x_error_code ' || x_error_code);
            debug_log('x_msg_count ' || x_msg_count);
            debug_log('x_msg ' || x_msg);
        
            if x_status != 'S' then
              l_return := 1; --set the return to 3
              l_status := x_status;
              
              update XXWC.XXWC_CC_ENTRY_TABLE set process_flag = 3, 
                                                  return_status = x_status, 
                                                  interface_id = x_interface_id, 
                                                  error_code = x_error_code,
                                                  message_count = x_msg_count,
                                                  message = x_msg, 
                                                  last_update_date = sysdate,
                                                  batch_id = l_batch_id
              where  cycle_count_header_id = r_items.cycle_count_header_id
              and    nvl(cycle_count_entry_id,-1) = nvl(r_items.cycle_count_entry_id,-1)
              and    organization_id = r_items.organization_id
              and    inventory_item_id = r_items.inventory_item_id;
              
            /*	fnd_message.set_string(:XXWC_CC_ENTRIES_VW.ITEM_NUMBER || ' sequence ' || l_count_list_sequence || ' returned an error return code of ' || x_status || ', ' || x_msg);
              fnd_message.show;
           */	
            
            else
            
            
              update XXWC.XXWC_CC_ENTRY_TABLE set process_flag = 2, 
                                                  return_status = x_status, 
                                                  interface_id = x_interface_id, 
                                                  message = 'Success', 
                                                  last_update_date = sysdate,
                                                  batch_id = l_batch_id
              where  cycle_count_header_id = r_items.cycle_count_header_id
              and    nvl(cycle_count_entry_id,-1) = nvl(r_items.cycle_count_entry_id,-1)
              and    organization_id = r_items.organization_id
              and    inventory_item_id = r_items.inventory_item_id;
            
          
            end if;		
				
				COMMIT;
						
			end loop;
	
      x_return := l_return;
 			x_message := 'All records processed sucessfully. Exiting form.';
 
 
    EXCEPTION

   WHEN g_exception THEN
   
      x_return := 1;
      x_message := g_message;
      
      debug_log (g_message);
      
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

    
      WHEN others THEN
      
      
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'When others in ' || g_package||'.'||g_call_from  || ' ' || g_call_point || ' ' || g_sqlcode || ' ' || g_sqlerrm;
      
        x_return := 1;
        x_message := g_message;
      
        debug_log (g_message);
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

  END PROCESS_API;
  
  
   /*****************************************************************************************************************************************
   *   FUNCTION GET_SS_OH_QTY                                                                                                               *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_SS_OH_QTY  ( p_cycle_count_header_id  IN NUMBER    --1
                            , p_cycle_count_entry_id   IN NUMBER    --2
                            , p_organization_id        IN NUMBER    --3
                            , p_inventory_item_id      IN NUMBER    --4
                            , p_subinventory_code      IN VARCHAR2  --5
                            , p_lot_number             IN VARCHAR2  --6
                           )
        RETURN NUMBER

        
    IS
      
      l_quantity NUMBER DEFAULT 0;
   
   BEGIN
   
        g_call_from := 'GET_SS_OH_QTY';
        g_call_point := 'Start';
    
        debug_log('p_cycle_count_header_id ' || p_cycle_count_header_id);
        debug_log('p_cycle_count_entry_id ' || p_cycle_count_entry_id);
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_lot_number ' || p_lot_number);

        BEGIN
        
            SELECT sum(nvl(primary_transaction_quantity,0))
            INTO   l_quantity
            FROM   XXWC.XXWC_CC_QUANTITIES_DETAIL_TBL
            WHERE  inventory_item_id = p_inventory_item_id
            AND    organization_id = 	p_organization_id
            AND    cycle_count_header_id = p_cycle_count_header_id
            AND    cycle_count_entry_id = p_cycle_count_entry_id
            AND    subinventory_code = p_subinventory_code
            and    nvl(lot_number,'-99999') = nvl(p_lot_number,'-99999');

      EXCEPTION
            when others then
                  l_quantity := 0;
      END;
      
      
    return l_quantity;
    
   
    EXCEPTION
    
     WHEN others THEN
      
       return l_quantity;
      
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'When others in ' || g_package||'.'||g_call_from  || ' ' || g_call_point || ' ' || g_sqlcode || ' ' || g_sqlerrm;
      
        debug_log (g_message);
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

    
    END GET_SS_OH_QTY;
 
 
    /*****************************************************************************************************************************************
   *   FUNCTION GET_TOTAL_COUNT_QTY                                                                                                         *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the default cycle count header id for the organization                                        *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_TOTAL_COUNT_QTY  ( p_cycle_count_header_id  IN NUMBER    --1
                                  , p_cycle_count_entry_id   IN NUMBER    --2
                                  , p_organization_id        IN NUMBER    --3
                                  , p_inventory_item_id      IN NUMBER    --4
                                  , p_subinventory_code      IN VARCHAR2  --5
                                  , p_lot_number             IN VARCHAR2  --6
                                  )
        RETURN NUMBER 
        
    IS
          
    l_quantity NUMBER DEFAULT 0;
   
   BEGIN
   
        g_call_from := 'GET_TOTAL_COUNT_QTY';
        g_call_point := 'Start';
    
        debug_log('p_cycle_count_header_id ' || p_cycle_count_header_id);
        debug_log('p_cycle_count_entry_id ' || p_cycle_count_entry_id);
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_lot_number ' || p_lot_number);

        BEGIN
        
            SELECT sum(nvl(primary_quantity,0))
            INTO   l_quantity
            FROM   XXWC_MTL_CC_ENTRIES_SUM_V
            WHERE  inventory_item_id = p_inventory_item_id
            AND    organization_id = 	p_organization_id
            AND    cycle_count_header_id = p_cycle_count_header_id
            AND    cycle_count_entry_id = p_cycle_count_entry_id
            AND    subinventory = p_subinventory_code
            and    nvl(lot_number,'-99999') = nvl(p_lot_number,'-99999');

      EXCEPTION
            when others then
                  l_quantity := 0;
      END;
      
      
    return l_quantity;
    
   
    EXCEPTION
    
     WHEN others THEN
      
       return l_quantity;
      
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'When others in ' || g_package||'.'||g_call_from  || ' ' || g_call_point || ' ' || g_sqlcode || ' ' || g_sqlerrm;
      
        debug_log (g_message);
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


      
    END GET_TOTAL_COUNT_QTY;
    
    
   /*****************************************************************************************************************************************
   *   FUNCTION GET_OPEN_COUNT_QTY                                                                                                          *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the number of open counts                                                                     *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_OPEN_COUNT_QTY  ( p_cycle_count_header_id  IN NUMBER    --1
                                 , p_organization_id        IN NUMBER    --2
                                 , p_entry_status_code      IN NUMBER    --3)
                                 )
        RETURN NUMBER
        
       IS
          
    l_quantity NUMBER DEFAULT 0;
   
   BEGIN
   
        g_call_from := 'GET_OPEN_COUNT_QTY';
        g_call_point := 'Start';
    
        debug_log('p_cycle_count_header_id ' || p_cycle_count_header_id);
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_entry_status_code ' || p_entry_status_code);

        BEGIN
        
            SELECT nvl(count(mcce.entry_statuS_code),0)
            INTO   l_quantity
            FROM   mtl_cycle_count_entries mcce
            WHERE  mcce.organization_id = 	p_organization_id
            AND    mcce.cycle_count_header_id = p_cycle_count_header_id
            AND    mcce.entry_status_code = p_entry_status_code
            ;
            
      EXCEPTION
            when others then
                  l_quantity := 0;
      END;
      
    
    debug_log('l_quantity ' || l_quantity);
      
    return l_quantity;
    
   
    EXCEPTION
    
     WHEN others THEN
      
       return l_quantity;
      
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'When others in ' || g_package||'.'||g_call_from  || ' ' || g_call_point || ' ' || g_sqlcode || ' ' || g_sqlerrm;
      
        debug_log (g_message);
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);



    END GET_OPEN_COUNT_QTY;
    
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_WC_BIN_LOC                                                                                                             *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get the branch bin assignments for an item organization combination                             *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *   1.1        02-MAR-2016  Lee Spitzer               TMS Ticket 20160309-00091 Mobile Stocking                                          *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_BIN_LOC           ( x_locators               OUT    NOCOPY t_genref --0
                                    , p_organization_id        IN     NUMBER    --1
                                    , p_subinventory_code      IN     VARCHAR2  --2
                                    , p_restrict_locators_code IN     NUMBER    --3
                                    , p_inventory_item_id      IN     VARCHAR2    --4
                                    , p_concatenated_segments  IN     VARCHAR2  --5
                                    , p_project_id             IN     NUMBER    --6
                                    , p_task_id                IN     NUMBER    --7
                                    , p_hide_prefix            IN     VARCHAR2 --8
                                    , p_cycle_count_header_id  IN     VARCHAR2  --9
                                    , p_entry_status_code      IN     VARCHAR2  --10
                                    --, p_alias                  IN     VARCHAR2
                                    ) IS

    l_concatenated_segments VARCHAR2(40);--Added 3/2/2016 TMS 20160309-00091
         
        
    BEGIN

      g_call_from := 'GET_WC_BIN_LOC';
      g_call_point := 'Start';
      
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_restrict_locators_code  ' || p_restrict_locators_code);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_concatenated_segments ' || p_concatenated_segments);
        debug_log('p_project_id ' || p_project_id);
        debug_log('p_task_id ' || p_task_id);
        debug_log('p_hide_prefix ' || p_hide_prefix);
        debug_log('p_cycle_count_header_id ' || p_cycle_count_header_id);
        debug_log('p_entry_status_code ' || p_entry_status_code);
        --debug_log('p_alias ' || p_alias);
        
        
                                   --Added 5/12/2015 to prevent open cursors
        if x_locators%isOPEN then
           debug_log('closing x_locators cursor');
            close x_locators;
        
        end if;

     BEGIN --Added 3/2/2016 TMS 20160309-00091
          SELECT upper(p_concatenated_segments)  --Added 3/2/2016 TMS 20160309-00091
          INTO   l_concatenated_segments  --Added 3/2/2016 TMS 20160309-00091
          FROM   dual;  --Added 3/2/2016 TMS 20160309-00091
        EXCEPTION  --Added 3/2/2016 TMS 20160309-00091
          WHEN others THEN  --Added 3/2/2016 TMS 20160309-00091
            g_sqlcode := SQLCODE;  --Added 3/2/2016 TMS 20160309-00091
            g_sqlerrm := SQLERRM;  --Added 3/2/2016 TMS 20160309-00091
            g_message := 'Could not upper ' || p_concatenated_segments;  --Added 3/2/2016 TMS 20160309-00091
            raise g_exception; --Added 3/2/2016 TMS 20160309-00091
        END; --Added 3/2/2016 TMS 20160309-00091
        
        
        debug_log('l_concatenated_segments ' || l_concatenated_segments); --Added 3/2/2016 TMS 20160309-00091

            --Hide Prefix and Locator Restrict is on
                         OPEN x_locators FOR
              --Primary Bin Query
              SELECT DISTINCT
                         loc.wc_locator
                       , NULL wc_prefix--, loc.wc_pre_fix
                       , NULL inventory_location_id--, loc.inventory_location_id
                       , NULL concatenated_segments--, loc.concatenated_segments
                       , NULL description--, loc.description
                       , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            --, substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            --, wil.inventory_location_id
                            --, wil.locator_segments concatenated_segments
                            --, wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     AND exists (select * --count_due_date, to_date(count_due_date)
                                 from   xxwc_mtl_cycle_count_entries_v mcce1
                                 where  mcce1.organization_id = p_organization_id
                                 and    mcce1.cycle_count_header_id = p_cycle_count_header_id
                                 and    mcce1.entry_status_code = p_entry_status_code
                                 and    wil.inventory_location_id = mcce1.inventory_location_id)
                     --AND wil.segment1 like nvl(p_concatenated_segments,wil.segment1)) loc
                     --AND substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) = nvl(p_concatenated_segments,substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)))) loc --Removed 3/2/2016 TMS 20160309-00091
                     --AND substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) = nvl(l_concatenated_segments,substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)))) loc --Added 3/2/2016 TMS 20160309-00091  --Removed 20151210-00014 
                     AND substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) like nvl(l_concatenated_segments,substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)))) loc --Added 3/2/2016 TMS 20160309-00091 --Added 20151210-00014 
                     order by loc.wc_locator;
    
    
    EXCEPTION
    
      WHEN others THEN
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
         if x_locators%isOPEN then
           debug_log('closing x_locators cursor');
            close x_locators;
        
        end if;

   

            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


    
    END  GET_BIN_LOC;
    

   /*****************************************************************************************************************************************
   *   FUNCTION GET_COST_GROUP_ID                                                                                                           *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the number of open counts                                                                     *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_COST_GROUP_ID   ( p_organization_id        IN NUMBER    --1
                                 )
        RETURN NUMBER 
      
        
    IS
    
      l_cost_group_id NUMBER DEFAULT -1;
      
    
    BEGIN
    
        g_call_from := 'GET_COST_GROUP_ID';
        g_call_point := 'Start';
    
        debug_log('p_organization_id ' || p_organization_id);
        
    
    	BEGIN
				
				select default_cost_group_id
				into   l_cost_group_id
				from   mtl_parameters
				where  organization_id = p_organization_id
				;
								
        EXCEPTION
            WHEN OTHERS THEN
                l_cost_group_id := -1;
                
        END;
        
        debug_log('l_cost_group_id ' || l_cost_group_id);
		
      RETURN l_cost_group_id;
    
   
    EXCEPTION
    
     WHEN others THEN
      
       return l_cost_group_id;
      
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'When others in ' || g_package||'.'||g_call_from  || ' ' || g_call_point || ' ' || g_sqlcode || ' ' || g_sqlerrm;
      
        debug_log (g_message);
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);



    END GET_COST_GROUP_ID;
    
    
       /*****************************************************************************************************************************************
   *   FUNCTION GET_ITEM_OPEN_COUNT_QTY                                                                                                     *
   *                                                                                                                                        *
   *   PURPOSE:  This function is used to get the number of open counts                                                                     *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150302-00173                                                          *
   *                                                     RF - Cycle Counting                                                                *
   *****************************************************************************************************************************************/
                                              
    FUNCTION GET_ITEM_OPEN_COUNT_QTY  ( p_cycle_count_header_id  IN NUMBER    --1
                                      , p_organization_id        IN NUMBER    --2
                                      , p_inventory_item_id      IN NUMBER    --3)
                                      )
        RETURN NUMBER
        
        IS
    
    l_quantity_mcce NUMBER DEFAULT 0;
    l_quantity_xcet NUMBER DEFAULT 0;
    l_quantity NUMBER DEFAULT 0;
   
   BEGIN
   
        g_call_from := 'GET_ITEM_OPEN_COUNT_QTY';
        g_call_point := 'Start';
    
        debug_log('p_cycle_count_header_id ' || p_cycle_count_header_id);
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);

        BEGIN
        
            SELECT nvl(count(mcce.entry_status_code),0)
            INTO   l_quantity_mcce
            FROM   mtl_cycle_count_entries mcce
            WHERE  mcce.organization_id = 	p_organization_id
            AND    mcce.cycle_count_header_id = p_cycle_count_header_id
            AND    mcce.inventory_item_id = p_inventory_item_id
            AND    mcce.entry_status_code in (1,3)
            ;
            
      EXCEPTION
            when others then
                  l_quantity_mcce := 0;
      END;
      
      debug_log('l_quantity_mcce ' || l_quantity_mcce);
      
      
        BEGIN
        
            SELECT nvl(count(xcet.inventory_item_id),0)
            INTO   l_quantity_xcet
            FROM   xxwc.xxwc_cc_entry_table xcet
            where  xcet.organization_id = p_organization_id
            and    xcet.inventory_item_id = p_inventory_item_id
            and    xcet.cycle_count_entry_id is null --New Counts
            and    xcet.process_flag = 1
            ;
            
      EXCEPTION
            when others then
                  l_quantity_xcet := 0;
      END;
      
      

      debug_log('l_quantity_xcet ' || l_quantity_xcet);
    
      l_quantity := l_quantity_xcet + l_quantity_mcce;
      
    debug_log('l_quantity ' || l_quantity);
      
    return l_quantity;
    
   
    EXCEPTION
    
     WHEN others THEN
      
       return l_quantity;
      
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'When others in ' || g_package||'.'||g_call_from  || ' ' || g_call_point || ' ' || g_sqlcode || ' ' || g_sqlerrm;
      
        debug_log (g_message);
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);



    END GET_ITEM_OPEN_COUNT_QTY;
    
    /*****************************************************************************************************************************************
    *   PROCEDURE PROCESS_CC_RESEARCH_CCR                                                                                                    *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to launch the Cycle Count Research Report                                                          *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        17-MAR-2016  Lee Spitzer               TMS Ticket 20150810-00022  RF - Cyclecount Research Report                         *
    *                                                                                                                                        *
    *****************************************************************************************************************************************/
  
    PROCEDURE PROCESS_CC_RESEARCH_CCR ( p_organization_id       IN NUMBER
                                      , p_cycle_count_header_id IN NUMBER
                                      , x_return                OUT NUMBER
                                      , x_message               OUT VARCHAR2
                                      ) IS
        
        l_copies              NUMBER DEFAULT 1;
        l_status_flag         VARCHAR2(1);
        l_printer             VARCHAR2 (30);
        l_set_print_options   BOOLEAN;
        l_set_mode            BOOLEAN;
        xml_layout            BOOLEAN;
        x_request_id          NUMBER;
        l_user_id             NUMBER;
        l_resp_id             NUMBER;
        l_resp_appl_id        NUMBER;
        
    BEGIN
   
        g_call_from := 'PROCESS_CC_RESEARCH_CCR';
        g_call_point := 'Start';
    
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_cycle_count_header_id ' || p_cycle_count_header_id);
        
        l_user_id := fnd_profile.VALUE('USER_ID');
        l_resp_id := fnd_profile.VALUE('RESP_ID');
        l_resp_appl_id := fnd_profile.VALUE('RESP_APPL_ID');

        debug_log('l_user_id ' || l_user_id);
        debug_log('l_resp_id ' || l_resp_id);
        debug_log('l_resp_appl_id ' || l_resp_appl_id);

        fnd_global.apps_initialize(l_user_id,l_resp_id,l_resp_appl_id); --Required for Multi-Org or expected receipts report will error

            l_printer := fnd_profile.value('PRINTER');

            if l_printer = 'noprint' then

                l_copies := 0;

            else

                l_copies := 1;

            end if;


            debug_log('l_printer ' || l_printer);
            debug_log('l_copies ' || l_copies);


            l_set_print_options :=
              fnd_request.set_print_options (printer               => l_printer,
                                             style                 => NULL,
                                             copies                => l_copies,
                                             save_output           => TRUE,
                                             print_together        => 'N',
                                             validate_printer      => 'RESOLVE'
                                            );
            xml_layout :=
                fnd_request.add_layout ('XXWC',
                                        'XXWC_CYLE_CNT_RES',
                                        'en',
                                        'US',
                                        'PDF'
                                       );

           x_request_id :=
                fnd_request.submit_request
                                       (application      => 'XXWC',
                                        PROGRAM          => 'XXWC_CYLE_CNT_RES',
                                        description      => 'XXWC Cycle Count Research Report',
                                        sub_request      => FALSE,
                                        argument1        => p_organization_id,
                                        argument2        => p_cycle_count_header_id
                                        );
        COMMIT;


        x_return := 0;
        x_message := 'Success Request ' || x_request_id || ' to printer ' || l_printer;

    EXCEPTION

      WHEN g_exception THEN
      

        x_return := 2;
        x_message := 'Error ' || g_message;

           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);


      WHEN others THEN
        
        x_return := 2;
        x_message := 'Error ' || g_message;

           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);
  END PROCESS_CC_RESEARCH_CCR;

END XXWC_CC_MOBILE_PKG;
/