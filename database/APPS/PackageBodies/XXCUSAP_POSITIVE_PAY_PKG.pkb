CREATE OR REPLACE PACKAGE BODY APPS."XXCUSAP_POSITIVE_PAY_PKG" AS

  /***************************************************************************************************
  **  This package extracts Positive pay information for a bank account.  This process accepts two **
  **  parameters Bank Account and Re-select flag as inputs.  Process selects both Voided and non   **
  **  voided information and using utl_file writes to a file.  After writing each record to the    **
  **  file positive_pay_status in ap_checks table is updated to be marked appropriately as sent.   **
  **  Then the java mail package is called which sends the file created as an attachement to the   **
  **  person submitting the request.  If the email id is missing in the fnd_user table it sends to **
  **  a default email id.                            **
  ***************************************************************************************************
  **  Date        Version         Developer            Description
  ** -------      --------        -----------          -----------------------
  **  01.10.12    2.0             Luong Vu             Migrate from 11i to R12. 
  **                                                   Changes required were relating to bank tables
  **  10.24.13    2.1             Luong Vu             Removed CRLF characters, and add overflow for
                                                       vendor name. 
      1.16.14     2.2             Luong Vu             Fixed vendor name for segment 62.  It was 
                                                       vendorname1 --> vendorname2
  ***************************************************************************************************/

  PROCEDURE main(p_errbuf         OUT VARCHAR2
                ,p_retcode        OUT NUMBER
                ,p_chk_stock_name IN VARCHAR2
                ,p_bank_name      IN VARCHAR2
                ,p_reselect_flg   IN VARCHAR2
                ,p_from_chk_date  IN DATE DEFAULT NULL
                ,p_to_chk_date    IN DATE DEFAULT NULL) IS
  
    l_bank_account_id NUMBER;
    l_reselect_flag   VARCHAR2(3);
    l_chk_stock_name  VARCHAR2(50) := p_chk_stock_name;
    l_from_chk_date   DATE := p_from_chk_date;
    l_to_chk_date     DATE := p_to_chk_date;
  
  BEGIN
    -- Get bank account ID 
    SELECT cba.bank_account_id
      INTO l_bank_account_id
      FROM ce.ce_bank_accounts cba
     WHERE cba.bank_account_name = p_bank_name;
    dbms_output.put_line('bank accnt id: ' || l_bank_account_id);
    IF p_reselect_flg = 'Yes' THEN
      l_reselect_flag := 'Y';
    ELSE
      l_reselect_flag := 'N';
    END IF;
  
    /*    apps.xxcusap_positive_pay_pkg.extract_data(p_check_stock_name => l_chk_stock_name,
    p_bank_account_id => l_bank_account_id,
    p_reselect_flag => l_reselect_flag,
    p_from_check_date => l_from_chk_date,
    p_to_check_date => l_to_chk_date);*/
  
    apps.xxcusap_positive_pay_pkg.extract_data(p_errbuf, p_retcode,
                                               l_chk_stock_name,
                                               l_bank_account_id,
                                               l_reselect_flag,
                                               l_from_chk_date,
                                               l_to_chk_date);
  
  END main;


  PROCEDURE get_bank_name(p_bank_account_id    IN ap_bank_accounts_all.bank_account_id%TYPE
                         ,p_account_name       IN OUT ap_bank_accounts_all.bank_account_name%TYPE
                         ,p_account_number     IN OUT ap_bank_accounts_all.bank_account_num%TYPE
                         ,p_3digit_bank_number IN OUT ap_bank_accounts_all.attribute1%TYPE) IS
    CURSOR cu_bank_name IS
      SELECT bank_account_name
            ,bank_account_num
            ,attribute1
        FROM ce.ce_bank_accounts
       WHERE bank_account_id = p_bank_account_id;
  
  BEGIN
    OPEN cu_bank_name;
    FETCH cu_bank_name
      INTO p_account_name
          ,p_account_number
          ,p_3digit_bank_number;
  
    CLOSE cu_bank_name;
  
  END get_bank_name;

  PROCEDURE extract_data(p_errbuf           OUT VARCHAR2
                        ,p_retcode          OUT NUMBER
                        ,p_check_stock_name IN VARCHAR2
                        ,p_bank_account_id  IN NUMBER
                        ,p_reselect_flag    IN VARCHAR2
                        ,p_from_check_date  IN DATE DEFAULT NULL
                        ,p_to_check_date    IN DATE DEFAULT NULL) IS
  
    l_file_name_temp VARCHAR2(150);
    l_file_name      VARCHAR2(150);
    l_file_dir       VARCHAR2(100);
    l_bank_acct_name VARCHAR2(100);
    l_ven_name_len   NUMBER;
    l_trx_code1      VARCHAR2(3) DEFAULT '*61';
    l_trx_code2      VARCHAR2(3) DEFAULT '*62';
    --crfl             VARCHAR2(2) := chr(13);      --v2.1
  
    CURSOR cu_positive_pay(p_reselect_flag IN VARCHAR2) IS
      SELECT decode(ac.status_lookup_code, 'VOIDED', '*26', '*10') transaction_code
            ,lpad(to_char(aba.bank_account_num), 10, '0') bank_account_number
            ,lpad(to_char(ac.check_number), 10, '0') serial_number
            ,lpad(to_char(ac.amount * 100), 11, '0') check_amount
            ,ac.amount amount
            ,lpad(to_char(ac.check_date, 'MMDDYY'), 6, ' ') check_issue_date
            ,rpad(' ', 15, ' ') additional_data --v2.1
            ,rpad(' ', 22, ' ') value_data
            ,rpad(' ', 6, ' ') value_data6
             --,rpad(substr(ac.vendor_name, 1, 15), 15, ' ') additional_data
            ,rpad(substr(ac.vendor_name, 1, 40), 40, ' ') vendor_name1
            ,rpad(substr(ac.vendor_name, 41, 40), 40, ' ') vendor_name2
            ,ac.vendor_name --v2.1
            ,decode(ac.status_lookup_code, 'VOIDED', 'V', '') void_indicator
            ,lpad(' ', 25, ' ') filler
            ,'111111111' funding_source
            ,ac.rowid ap_checks_rowid
            ,positive_pay_status_code
        FROM apps.ap_checks_all      ac
            ,ce.ce_payment_documents acs
            ,ce.ce_bank_accounts     aba
       WHERE ac.payment_method_code = 'CHECK'
         AND ac.payment_document_id = acs.payment_document_id
         AND acs.internal_bank_account_id = aba.bank_account_id
         AND p_reselect_flag = 'Y'
         AND ac.check_date BETWEEN p_from_check_date AND p_to_check_date
         AND nvl(acs.internal_bank_account_id, 0) = p_bank_account_id
         AND positive_pay_status_code IN ('UNSENT', 'SENT AS NEGOTIABLE')
         AND ac.payment_document_id IN
             (SELECT payment_document_id
                FROM ce.ce_payment_documents
               WHERE payment_document_name = p_check_stock_name)
         AND status_lookup_code IN
             ('NEGOTIABLE', 'CLEARED', 'ISSUED', 'CLEARED BUT UNACCOUNTED',
              'RECONCILED', 'RECONCILED UNACCOUNTED')
      UNION
      
      SELECT decode(ac.status_lookup_code, 'VOIDED', '*26', '*10') transaction_code
            ,lpad(to_char(aba.bank_account_num), 10, '0') bank_account_number
            ,lpad(to_char(ac.check_number), 10, '0') serial_number
            ,lpad(to_char(ac.amount * 100), 11, '0') check_amount
            ,ac.amount amount
            ,lpad(to_char(ac.check_date, 'MMDDYY'), 6, '0') check_issue_date
            ,rpad(' ', 15, ' ') additional_data --v2.1
            ,rpad(' ', 22, ' ') value_data
            ,rpad(' ', 6, ' ') value_data6
             --,rpad(substr(ac.vendor_name, 1, 15), 15, ' ') additional_data
            ,rpad(substr(ac.vendor_name, 1, 40), 40, ' ') vendor_name1
            ,rpad(substr(ac.vendor_name, 41, 40), 40, ' ') vendor_name2
            ,ac.vendor_name
            ,decode(ac.status_lookup_code, 'VOIDED', 'V', '') void_indicator
            ,lpad(' ', 25, ' ') filler
            ,'111111111' funding_source
            ,ac.rowid ap_checks_rowid
            ,positive_pay_status_code
        FROM apps.ap_checks_all      ac
            ,ce.ce_payment_documents acs
            ,ce.ce_bank_accounts     aba
       WHERE ac.payment_method_code = 'CHECK'
         AND ac.payment_document_id = acs.payment_document_id
         AND acs.internal_bank_account_id = aba.bank_account_id
         AND p_reselect_flag = 'N'
         AND nvl(acs.internal_bank_account_id, 0) = p_bank_account_id
         AND positive_pay_status_code = 'UNSENT'
         AND ac.payment_document_id IN
             (SELECT payment_document_id
                FROM ce.ce_payment_documents
               WHERE payment_document_name = p_check_stock_name)
         AND status_lookup_code IN
             ('NEGOTIABLE', 'CLEARED', 'ISSUED', 'CLEARED BUT UNACCOUNTED',
              'RECONCILED', 'RECONCILED UNACCOUNTED');
  
  
    /*    CURSOR cu_get_email_id(p_user_id IN NUMBER) IS
    SELECT email_address
      FROM fnd_user
     WHERE user_id = p_user_id;*/
  
    pl_string             VARCHAR2(240);
    pl_out_file           utl_file.file_type;
    pl_user_name          fnd_user.user_name%TYPE;
    pl_user_id            fnd_user.user_id%TYPE;
    pl_dflt_email         fnd_user.email_address%TYPE;
    pl_email              fnd_user.email_address%TYPE;
    pl_errormessage       VARCHAR2(4000);
    pl_errorstatus        NUMBER;
    pl_bank_acct_name     ce_bank_accounts.bank_account_name%TYPE;
    pl_bank_acct_num      ce_bank_accounts.bank_account_num%TYPE;
    pl_3digit_bank_number ce_bank_accounts.attribute1%TYPE;
    pl_amt_total          NUMBER := 0;
    pl_record_total       NUMBER := 0;
    pl_instance           VARCHAR2(100);
    pl_sender             VARCHAR2(100);
    pl_rowid              p_rowid_plsql_tbl;
    pl_cnt                NUMBER := 0;
    pl_curr_date          DATE;
    l_test                VARCHAR2(100);
  
  BEGIN
  
    pl_rowid.delete;
  
    FOR c IN cu_positive_pay('N')
    LOOP
      dbms_output.put_line(c.additional_data);
    END LOOP;
  
    BEGIN
      SELECT NAME
        INTO pl_instance
        FROM v$database;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    pl_sender := 'Oracle.Applications_' || pl_instance ||
                 '@hughessupply.com';
  
    /*    fnd_profile.get('USER_ID', pl_user_id);
    fnd_profile.get('USERNAME', pl_user_name);
    
    --fnd_profile.get('XXHSI_POSITIVE_PAY_EMAIL_ADDR', pl_dflt_email);
    
    fnd_file.put_line(fnd_file.log,
                      'Default email id is : ' || pl_dflt_email);*/
  
    /*    OPEN cu_get_email_id(pl_user_id);
    FETCH cu_get_email_id
      INTO pl_email;
    CLOSE cu_get_email_id;*/
  
    l_file_dir := 'AP_POSITIVE_PAY';
  
    get_bank_name(p_bank_account_id, pl_bank_acct_name, pl_bank_acct_num,
                  pl_3digit_bank_number);
  
    l_bank_acct_name := REPLACE(pl_bank_acct_name, ' ', '_');
    pl_curr_date     := SYSDATE;
  
    l_file_name_temp := 'TEMP_' || p_file_name || l_bank_acct_name || '_' ||
                        to_char(pl_curr_date, 'mmddyyyy_hh24_mi_ss') ||
                        '.txt';
    l_file_name      := p_file_name || l_bank_acct_name || '_' ||
                        to_char(pl_curr_date, 'mmddyyyy_hh24_mi_ss') ||
                        '.txt';
    pl_out_file      := utl_file.fopen(l_file_dir, l_file_name_temp, 'W');
  
    IF utl_file.is_open(pl_out_file) THEN
    
      -- print header record
      /*   pl_string := 'ID=OFT0164 BID=''HUGHES CP 0968''' || crfl;
      fnd_file.put_line(fnd_file.output, pl_string);
      utl_file.put_line(pl_out_file, pl_string);*/
    
      pl_string := '*00' || lpad(pl_bank_acct_num, 10, '0') -- || LPAD('0', 10, '0')
                   || lpad(pl_3digit_bank_number, 13, '0') ||
                   to_char(SYSDATE, 'MMDDYY');
      fnd_file.put_line(fnd_file.output, pl_string);
      utl_file.put_line(pl_out_file, pl_string);
    
      FOR i IN cu_positive_pay(p_reselect_flag)
      LOOP
        l_ven_name_len := length(i.vendor_name); --v2.1
      
        IF l_ven_name_len > 40 THEN
          pl_string := i.transaction_code || i.bank_account_number ||
                       i.serial_number || i.check_amount ||
                       i.additional_data || i.funding_source ||
                       i.value_data || l_trx_code1 || i.bank_account_number ||
                       i.serial_number || i.check_amount || i.vendor_name1 ||
                       i.value_data6 || l_trx_code2 ||
                       i.bank_account_number || i.serial_number ||
                       i.check_amount || i.vendor_name2 || i.value_data6;
        ELSE
          pl_string := i.transaction_code || i.bank_account_number ||
                       i.serial_number || i.check_amount ||
                       i.additional_data || i.funding_source ||
                       i.value_data || l_trx_code1 || i.bank_account_number ||
                       i.serial_number || i.check_amount || i.vendor_name1 ||
                       i.value_data6;
        END IF;
        fnd_file.put_line(fnd_file.output, pl_string);
        utl_file.put_line(pl_out_file, pl_string);
        pl_amt_total    := pl_amt_total + i.amount;
        pl_record_total := pl_record_total + 1;
      
        pl_cnt := pl_cnt + 1;
        pl_rowid(pl_cnt).p_rowid := i.ap_checks_rowid;
      
      END LOOP;
    
      pl_string := '*98' || lpad(pl_bank_acct_num, 10, '0') ||
                   lpad('9999999998', 10, '0') || '01' ||
                   lpad((pl_record_total + 2), 9, '0') ||
                   lpad(pl_amt_total * 100, 11, '0');
      utl_file.put_line(pl_out_file, pl_string);
      fnd_file.put_line(fnd_file.output, pl_string);
    
      pl_string := '*99' || lpad(pl_bank_acct_num, 10, '0') ||
                   lpad('9', 10, '9') || '01' ||
                   lpad((pl_record_total + 3), 9, '0') ||
                   lpad(pl_amt_total * 100, 11, '0');
      utl_file.put_line(pl_out_file, pl_string);
      fnd_file.put_line(fnd_file.output, pl_string);
    
      utl_file.fclose(pl_out_file);
    
      utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                       l_file_name);
    
    END IF;
  
    FOR i IN 1 .. pl_rowid.count
    LOOP
      UPDATE ap.ap_checks_all
         SET positive_pay_status_code = decode(status_lookup_code,
                                               'NEGOTIABLE',
                                               'SENT AS NEGOTIABLE', 'ISSUED',
                                               'SENT AS NEGOTIABLE', 'CLEARED',
                                               'SENT AS NEGOTIABLE',
                                               'CLEARED BUT UNACCOUNTED',
                                               'SENT AS NEGOTIABLE',
                                               'RECONCILED',
                                               'SENT AS NEGOTIABLE',
                                               'RECONCILED UNACCOUNTED',
                                               'SENT AS NEGOTIABLE', 'VOIDED',
                                               'SENT AS VOIDED', 'SPOILED',
                                               'SENT AS VOIDED', 'SET UP',
                                               'SENT AS VOIDED', 'OVERFLOW',
                                               'SENT AS VOIDED')
            ,last_update_date         = pl_curr_date
            ,last_updated_by          = fnd_global.user_id
       WHERE ROWID = pl_rowid(i).p_rowid
         AND status_lookup_code IN
             ('NEGOTIABLE', 'ISSUED', 'CLEARED', 'CLEARED BUT UNACCOUNTED',
              'RECONCILED', 'RECONCILED UNACCOUNTED', 'VOIDED', 'SPOILED',
              'SET UP', 'OVERFLOW');
      COMMIT;
    
    END LOOP;
  
  EXCEPTION
    WHEN utl_file.invalid_path THEN
      fnd_file.put_line(fnd_file.log, 'invalid_path');
      p_retcode := 2;
    WHEN utl_file.invalid_mode THEN
      fnd_file.put_line(fnd_file.log, 'invalid_mode');
      p_retcode := 2;
    WHEN utl_file.invalid_filehandle THEN
      fnd_file.put_line(fnd_file.log, 'invalid_filehandle');
      p_retcode := 2;
    WHEN utl_file.invalid_operation THEN
      fnd_file.put_line(fnd_file.log, 'invalid_operation');
      p_retcode := 2;
    WHEN utl_file.read_error THEN
      fnd_file.put_line(fnd_file.log, 'read_error');
      p_retcode := 2;
    WHEN utl_file.write_error THEN
      fnd_file.put_line(fnd_file.log, 'write_error');
      p_retcode := 2;
    WHEN utl_file.internal_error THEN
      fnd_file.put_line(fnd_file.log, 'internal_error');
      p_retcode := 2;
    WHEN OTHERS THEN
      ROLLBACK;
      p_retcode := 2;
      utl_file.fclose(pl_out_file);
      fnd_file.put_line(fnd_file.log, SQLERRM);
    
  END extract_data;

END xxcusap_positive_pay_pkg;
/
