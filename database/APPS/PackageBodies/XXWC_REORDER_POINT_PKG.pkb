CREATE OR REPLACE package body APPS.XXWC_REORDER_POINT_PKG as

/******************************************************************************
   NAME:       XXWC_REORDER_POINT_PKG 

   PURPOSE:    Automate Whitecap Reorder Point Planning for all organizations - TMS Ticket 20121217-00830
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        23-APR-2013  Lee Spitzer       1. Create the PL/SQL Package
   1.1        10-AUG-2013  Lee Spitzer       2. Updated PL/SQL Package to loop through Req Import Request via Batch_ID TMS Ticket 20130618-01280 Added a Loop to submit request via batch_id to group reqs by vendor and org for planning
   1.2        30-JUL-2014  Lee Spitzer       3. 20140730-00181  Create wrapper program for requisition Import @SC create wrapper program to run requisition import for all orgs. ... 
   1.3        06-NOV-2014  Pattabhi Avula    4. Canada Changes Made as per TMS# 20141002-00072
******************************************************************************/


 -- Global variables
 G_Application    Fnd_Application.Application_Short_Name%TYPE :='XXWC';
 G_Cp_Short_Code  Fnd_Concurrent_Programs.Concurrent_Program_Name%Type :='XXWC_MRPRPROP_TEXT'; --Whitecap Reorder Point Planning Report
 
 /*Added because it was missing from custom package on Version 1.2*/
  g_exception       EXCEPTION;   
  g_err_msg         VARCHAR2 (2000);
  g_err_callfrom    VARCHAR2 (175) := 'XXWC_REORDER_POINT_PKG';
  g_err_callpoint   VARCHAR2 (175) := 'START';
  g_distro_list     VARCHAR2 (80) := 'OracleDevelopmentGroup@hdsupply.com';
  g_module          VARCHAR2 (80);


/******************************************************************************
   NAME:       Main

   PURPOSE:    Launch Requisition Import for All Orgs after Min-Max and Reorder Point Planning
               This will pass the organization_id in the batch id parameter
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        23-APR-2013  Lee Spitzer       1. Create the PL/SQL Package
   1.1        10-AUG-2013  Lee Spitzer       2. Updated PL/SQL Package to loop through Req Import Request via Batch_ID TMS Ticket 20130618-01280 Added a Loop to submit request via batch_id to group reqs by vendor and org for planning
   1.2        30-JUL-2014  Lee Spitzer       2. 20140730-00181  Create wrapper program for requisition Import 
                                                        @SC create wrapper program to run requisition import for all orgs. ... 
                                                Removing the req import logic in this procedure and moved it to a new procedure PLANNING_REQUSITION_IMPORT

******************************************************************************/

  procedure main (
    retcode               out number
   ,errbuf                out VARCHAR2
   ,p_region              IN VARCHAR2
   ,p_supply_date         IN VARCHAR2
   ,p_demand_date         IN VARCHAR2
   ,p_restock             IN NUMBER
   ,p_update_min_max      IN NUMBER
   ,p_supply_type         IN NUMBER
  ) is
    
    CURSOR wc_inv_orgs (n_org_id IN NUMBER) IS
      SELECT ood.organization_id,
             ood.organization_code,
             ood.organization_name,
             ood.set_of_books_id,
             ood.chart_of_accounts_id,
             ood.legal_entity,
             mp.attribute9 region
        FROM org_organization_definitions ood,
             mtl_parameters mp
       WHERE 1 = 1         
         AND ood.operating_unit = n_org_id 
         AND ood.disable_date IS NULL
         --  AND ood.organization_code NOT IN ('WCC', 'MST') --WC Corporate Organization, WC Item Master
		 AND ood.organization_code NOT IN (fnd_profile.value('XXWC_CORPORATE_ORGANIZATION'), 'MST')  -- Replaced the WCC hard code value by profile on 07-11-2014 by Pattabhi for TMS# 20141002-00064
         AND ood.organization_id = mp.organization_id
         AND mp.attribute9 = nvl(p_region,mp.attribute9)
    ORDER BY ood.organization_code ASC;
    
   CURSOR c_location (p_organization_id IN NUMBER) IS 
      SELECT location_id
      FROM   hr_all_organization_units
      WHERE  organization_id = p_organization_id
      AND    p_supply_type = 2
      UNION
      select to_number(attribute15) location_id
      FROM   mtl_parameters
      WHERE  organization_id = p_organization_id
      AND    p_supply_type = 1;
      
    type wc_inv_orgs_tbl is table of wc_inv_orgs%rowtype index by binary_integer;
    wc_inv_orgs_rec wc_inv_orgs_tbl;        
  
   l_request_data VARCHAR2(20) :='';
   l_request_id   NUMBER :=0;
   l_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   l_user_id NUMBER :=0;
   
   l_forecast_designator VARCHAR2(10);
    
   l_category_set_id NUMBER;
   l_structure_id NUMBER;
   l_message VARCHAR2(200);
   l_exception EXCEPTION;
   l_req_approval varchar2(1);

   
  begin  

    g_err_callpoint := '010 - MAIN';

  
   l_request_data :=fnd_conc_global.request_data;
   
   l_req_id :=fnd_global.conc_request_id;
 
  BEGIN
   
    SELECT category_set_id, structure_id
    into   l_category_set_id, l_structure_id
    FROM   mtl_category_sets
    where  category_set_name = 'Inventory Category';
   
   exception
   
      WHEN others THEN
          l_message := substr('Could not find Inventory Category Set' || SQLCODE || SQLERRM,1,200);
          raise l_exception;
   END;
   
   
   if l_request_data is null then     
       
       l_request_data :='1';
  
       l_org_id :=mo_global.get_current_org_id;
      
       l_user_id :=fnd_global.user_id; 
       
       
       fnd_file.put_line(fnd_file.log, '');              
       fnd_file.put_line(fnd_file.log, 'l_org_id ='||l_org_id);
       fnd_file.put_line(fnd_file.log, 'l_user_id ='||l_user_id);
       fnd_file.put_line(fnd_file.log, '');      
      
      Open wc_inv_orgs(l_org_id);
      Fetch wc_inv_orgs Bulk Collect Into wc_inv_orgs_rec;
      Close wc_inv_orgs;    
      
      If wc_inv_orgs_rec.count =0 Then
      
       fnd_file.put_line(fnd_file.log,'No active inventory organizations found for operating unit WhiteCap');
       
      Else --wc_inv_orgs_rec.count >0 
      
         -- We will kick off for every single inventory orgs and not wait to check how many are running at a time
          for indx in 1 .. wc_inv_orgs_rec.count loop  
          
              BEGIN
              
                SELECT forecast_designator
                INTO   l_forecast_designator
                FROM   mrp.mrp_forecast_designators
                WHERE  'FCST'||wc_inv_orgs_rec(Indx).organization_code = forecast_designator
                AND    organization_id = wc_inv_orgs_rec(Indx).organization_id;
              EXCEPTION
                  WHEN OTHERS THEN
                  
                      l_forecast_designator := NULL;
              
              END;
          
              
              for r_location in c_location(wc_inv_orgs_rec(Indx).organization_id) loop
               
               fnd_file.put_line(fnd_file.log, 'org ='||wc_inv_orgs_rec(indx).organization_name);
              
              
              IF l_forecast_designator IS NOT NULL THEN
              
                l_request_id :=fnd_request.submit_request
                      (application      =>G_Application
                      ,PROGRAM          =>G_Cp_Short_Code
                      ,description      =>''
                      ,start_time       =>''
                      ,sub_request      =>TRUE
                      ,argument1        =>wc_inv_orgs_rec(Indx).organization_id --Organization Id
                      ,argument2        =>1 --Item Selection
                      ,argument3        =>p_demand_date --Demand Cutoff Date
                      ,argument4        =>p_supply_date --Supply Cutoff Date
                      ,argument5        =>p_restock --Restock
                      ,argument6        =>r_location.location_id --Default Delivery To
                      ,argument7        =>l_forecast_designator --Forecast
                      ,argument8        =>1 --First Sort
                      ,argument9        =>0 --Second Sort
                      ,argument10       =>0 --Third Sort
                      ,argument11       =>NULL --Items From
                      ,argument12       =>NULL --Items To
                      ,argument13       =>NULL --Planners From
                      ,argument14       =>NULL --Planners To
                      ,argument15       =>NULL --Buyers From
                      ,argument16       =>NULL --Buyers To
                      ,argument17       =>1100000062 --Category Set
                      ,argument18       =>101 --Category Structure
                      ,argument19       =>NULL --Categories From
                      ,argument20       =>NULL --Categories To
                      ,argument21       =>NULL --ABC Assignment Group
                      ,argument22       =>NULL --ABC Class
                      ,argument23       =>1 --Include PO Supply
                      ,argument24       =>1 --Include WIP Supply
                      ,argument25       =>1 --Include Interface Supply
                      ,argument26       =>2 --Include Non-nettable
                      ,argument27       =>2 --Display Item Description
                      ,argument28       =>2 --Display Additional Information
                      ,argument29       =>l_user_id --User ID
                      ,argument30       =>p_update_min_max --Update Min-Max Quantities
                      ,argument31       =>p_supply_type --Source Type
                      ); 
              
              ELSE
              
                  l_request_id := 0;
              
              END IF;
                  
                  /*    
                  IF l_request_id >0 THEN 
                    fnd_file.put_line(fnd_file.log, 'Submitted Whitecap Reorder Point Planning for organization ='
                                                     ||wc_inv_orgs_rec(indx).organization_name
                                                     ||', Request Id ='
                                                     ||l_request_id
                                                    );
                  ELSE
                    fnd_file.put_line(fnd_file.log, 'Failed to submit Whitecap Reorder Point Planning report for organization ='
                                                     ||wc_inv_orgs_rec(indx).organization_name                                                     
                                                    );
                  end if;     
                  */
                  
                 fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>l_request_data);
           
                 l_request_data :=to_char(to_number(l_request_data)+1);      
          
            END loop;
            
          end loop;     
                              
      End If; -- checking if wc_inv_orgs_rec.count =0      
 
    COMMIT;
    
    /* Removing -  1.2        30-JUL-2014  Lee Spitzer       2. 20140730-00181  Create wrapper program for requisition Import.  This will be called by another concurrent request program in the
                                                                  in the procedure PLANNING_REQUISITION_IMPORT
   
     if p_restock = 1 THEN
     
      --If the Source Type is Supplier = 2 or 3, then initiate req approval
      IF p_supply_type != 1 THEN
      
        l_req_approval := 'Y';
        
      ELSE
      
      --If the Source Type is Inventory = 1 then No
      
        l_req_approval := 'N';
      
      END IF;
      
      
            
      for r_req_iface in wc_inv_orgs(l_org_id)  --Added 8/10/2013 TMS Ticket 20130618-01280

      
      loop  --Added 8/10/2013 TMS Ticket 20130618-01280 
       
      l_request_id :=fnd_request.submit_request
              (
               application      =>'PO'
              ,PROGRAM          =>'REQIMPORT'
              ,description      =>'Requisition Import'
              ,start_time       =>''
              ,sub_request      => TRUE
              ,argument1        => 'INV' --Import Source
              ,argument2        => r_req_iface.organization_id--NULL  --Batch Added 8/10/2013 TMS Ticket 20130618-01280 
              ,argument3        => 'VENDOR' --Group By               
              ,argument4        => NULL --Last Requisition Number
              ,argument5        => 'N' --Multiple Distributions
              ,argument6        => l_req_approval --Initiate Req Approval After Reqimport                                                                          
              ); 
     
          COMMIT;
      
      end loop; --Added 8/10/2013 TMS Ticket 20130618-01280 
      
     END IF;
    
   */ 
      retcode :=0;
      errbuf :='Child request[s] completed. Exit XXWC Reorder Point Planning For All Orgs';
      fnd_file.put_line(fnd_file.log,errbuf);
      
    ELSE
    
      retcode :=1;
      errbuf :='Child requests not submitted becuase no active orgs. Exit XXWC Reorder Point Planning For All Orgs';
      fnd_file.put_line(fnd_file.log,errbuf);

    END IF;
    
  exception
   WHEN l_exception THEN
      fnd_file.put_line(fnd_file.log, 'Error in XXWC_REORDER_POINT_PKG count not find Inventory Category Set ='||sqlcode || sqlerrm);
   WHEN others THEN
    fnd_file.put_line(fnd_file.log, 'Error in caller apps.XXWC_REORDER_POINT_PKG.main ='||sqlerrm);
    
    g_err_callpoint := '020 - MAIN';
    
          g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
            --dbms_output.put_line(l_fulltext);
  
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'When Others Error in call ' || g_err_callfrom || '.' || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);
    
  
  end main;
  
  
/******************************************************************************
   NAME:       PLANNING_REQUISITION_IMPORT

   PURPOSE:    Launch Requisition Import for All Orgs after Min-Max and Reorder Point Planning
               This will pass the organization_id in the batch id parameter
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.2        30-JUL-2014  Lee Spitzer       2. 20140730-00181  Create wrapper program for requisition Import 
                                                        @SC create wrapper program to run requisition import for all orgs. ... 

******************************************************************************/
  
  PROCEDURE PLANNING_REQUSITION_IMPORT
   (retcode               OUT NUMBER
   ,errbuf                OUT VARCHAR2
   ,p_supply_type         IN NUMBER
  ) IS
   
   
   CURSOR wc_inv_orgs (n_org_id IN NUMBER) IS
      SELECT ood.organization_id,
             ood.organization_code
        FROM org_organization_definitions ood,
             mtl_parameters mp
       WHERE 1 = 1         
         AND ood.operating_unit = n_org_id 
         AND ood.disable_date IS NULL
         -- AND ood.organization_code NOT IN ('WCC', 'MST') --WC Corporate Organization, WC Item Master
		 AND ood.organization_code NOT IN (fnd_profile.value('XXWC_CORPORATE_ORGANIZATION'), 'MST')  -- Replaced the WCC hard code value by profile on 07-11-2014 by Pattabhi for TMS# 20141002-00064
         AND ood.organization_id = mp.organization_id
    ORDER BY ood.organization_code ASC;


   l_request_data VARCHAR2(20) :='';
   l_request_id   NUMBER :=0;
   l_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   l_user_id NUMBER :=0;
   l_req_approval varchar2(1);
    
    
    BEGIN
    
      g_err_callpoint := '010 - PLANNING_REQUSITION_IMPORT';
    
       l_request_data :=fnd_conc_global.request_data;
   
       l_req_id :=fnd_global.conc_request_id;
 
      IF l_request_data is null then     
       
         l_request_data :='1';
         
         l_org_id :=mo_global.get_current_org_id;
      
         l_user_id :=fnd_global.user_id; 
       
       
       fnd_file.put_line(fnd_file.LOG, '');              
       fnd_file.put_line(fnd_file.LOG, 'p_supply_type =' || p_supply_type); 
       fnd_file.put_line(fnd_file.log, 'l_org_id ='||l_org_id);
       fnd_file.put_line(fnd_file.log, 'l_user_id ='||l_user_id);
       fnd_file.put_line(fnd_file.LOG, '');      

    
       --If the Source Type is Supplier = 2 or 3, then initiate req approval
      IF p_supply_type != 1 THEN
      
        l_req_approval := 'Y';
        
      ELSE
      
      --If the Source Type is Inventory = 1 then No
      
        l_req_approval := 'N';
      
      END IF;
      
      
      for r_req_iface in wc_inv_orgs(l_org_id)  

      
      loop 
       
        BEGIN
        
      
         l_request_id :=fnd_request.submit_request
              (
               application      =>'PO'
              ,PROGRAM          =>'REQIMPORT'
              ,description      =>'Requisition Import for Org ' || r_req_iface.organization_code
              ,start_time       =>''
              ,sub_request      => TRUE
              ,argument1        => 'INV' --Import Source
              ,argument2        => r_req_iface.organization_id --Batch 
              ,argument3        => 'VENDOR' --Group By               
              ,argument4        => NULL --Last Requisition Number
              ,argument5        => 'N' --Multiple Distributions
              ,argument6        => l_req_approval --Initiate Req Approval After Reqimport                                                                          
              ); 
      
          EXCEPTION
              WHEN others THEN
                  g_err_callpoint := '020 - PLANNING_REQUSITION_IMPORT - Launching Req Import';
                  raise g_exception;
         
          END;
          
           fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>l_request_data);
           
           l_request_data :=to_char(to_number(l_request_data)+1);      
          
          COMMIT;
          
      end loop;  
      
    
      retcode :=0;
      errbuf :='Child request[s] completed. Exit XXWC Requisition Import for Nightly Planning Run';
      fnd_file.put_line(fnd_file.log,errbuf);
      
    ELSE
    
      retcode :=1;
      errbuf :='Child requests not submitted becuase no active orgs. Exit XXWC Requisition Import for Nightly Planning Run';
      fnd_file.put_line(fnd_file.log,errbuf);

    END IF;
    
  exception
  WHEN g_exception THEN
  
    fnd_file.put_line(fnd_file.LOG, 'Error in caller apps.XXWC_REORDER_POINT_PKG.PLANNING_REQUSITION_IMPORT ='||sqlerrm);
    ROLLBACK;

          g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
            --dbms_output.put_line(l_fulltext);
  
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'G Exception Error in call ' || g_err_callfrom ||'.'||g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

    
  
   WHEN others THEN

    fnd_file.put_line(fnd_file.LOG, 'Error in caller apps.XXWC_REORDER_POINT_PKG.PLANNING_REQUSITION_IMPORT ='||sqlerrm);
    ROLLBACK;

        g_err_callpoint := '040 - PLANNING_REQUSITION_IMPORT - Launching Req Import';
    
          g_err_msg := 'Error in ' || g_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
            --dbms_output.put_line(l_fulltext);
  
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'When Others Error in call ' || g_err_callfrom || '.' || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);
    
    END PLANNING_REQUSITION_IMPORT;
  
end XXWC_REORDER_POINT_PKG;
/
