CREATE OR REPLACE PACKAGE BODY APPS.xxwc_po_doc_control_pkg
AS
   /******************************************************************************
      NAME:       apps.xxwc_po_doc_control_pkg
      PURPOSE:    Package to perform actions on Purchasing documents.

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        12/10/2013      shariharan    Initial Version
      1.1        12/27/2013      shariharan    BPA Ticket Task ID: 20130815-00900
	  1.2        11/11/2014  Pattabhi Avula    TMS# 20141001-00254 Canada Changes Made
   ******************************************************************************/
   PROCEDURE cancel_bpa_lines (errbuf              OUT VARCHAR2,
                               retcode             OUT VARCHAR2,
                               i_po_header_id   IN     NUMBER)
   IS
      l_return_status   VARCHAR2 (1);
      l_msg_data        VARCHAR2 (2000);
      out_message       VARCHAR2 (2000);
      l_msg_index_OUT   NUMBER;
	  l_org_id          NUMBER(5):=mo_global.get_current_org_id;  -- Added by Pattabhi on 11/10/2014 for TMS# 20141001-00254 Canada Changes

      CURSOR c1
      IS
         SELECT a.item_id,
                a.po_Line_id,
                a.po_header_id,
                h.segment1,
                a.line_num,
                a.item_description
           FROM apps.po_lines a, apps.po_headers h
          WHERE     h.po_header_id = i_po_header_id
                AND a.po_header_id = h.po_header_id
                AND h.type_lookup_code = 'BLANKET'
                --AND a.item_id=3067819
                AND NVL (h.closed_code, 'OPEN') = 'OPEN'
                AND NVL (a.closed_code, 'OPEN') = 'OPEN'
                AND a.cancel_flag = 'N'
                AND EXISTS
                       (  SELECT item_id
                            FROM apps.po_lines b
                           WHERE     b.po_header_id = h.po_header_id
                                 AND b.cancel_flag = 'N'
                                 AND a.item_id = b.item_id
                                 AND NVL (b.closed_code, 'OPEN') = 'OPEN'
                        GROUP BY item_id
                          HAVING COUNT (1) > 1)
                AND a.po_line_id <>
                       (SELECT MAX (po_line_id)
                          FROM apps.po_lines c
                         WHERE     c.po_header_id = h.po_header_id
                               AND c.item_id = a.item_id
                               AND NVL (c.closed_code, 'OPEN') = 'OPEN'
                               AND c.cancel_flag = 'N');
   BEGIN
      apps.mo_global.init ('PO');

      FOR c1_rec IN c1
      LOOP
         l_msg_data := null;
         --call the Cancel API for PO
         apps.PO_Document_Control_PUB.control_document (
            p_api_version        => 1.0,
            p_init_msg_list      => FND_API.G_TRUE,
            p_commit             => FND_API.G_TRUE,
            x_return_status      => l_return_status,
            p_doc_type           => 'PA',
            p_doc_subtype        => 'BLANKET',
            p_doc_id             => c1_rec.po_header_id,
            p_doc_num            => c1_rec.segment1,
            p_release_id         => NULL,
            p_release_num        => NULL,
            p_doc_line_id        => c1_rec.po_line_id,
            p_doc_line_num       => c1_rec.line_num,
            p_doc_line_loc_id    => NULL,
            p_doc_shipment_num   => NULL,
            p_action             => 'CANCEL',
            p_action_date        => SYSDATE,
            p_cancel_reason      => 'Duplicate Line Process',
            p_cancel_reqs_flag   => 'N',
            p_print_flag         => 'N',
            p_note_to_vendor     => NULL,
            p_use_gldate         => 'N',
            p_org_id             => l_org_id);  -- Removed 162 and l_org_id is added by Pattabhi on 11/10/2014 for TMS# 20141001-00254 Canada Changes

         -- Get any messages returned by the Cancel API
         IF l_return_status <> 'E'
         THEN
            FND_FILE.Put_Line (
               FND_FILE.Output,'BPA-'
               ||  c1_rec.segment1
               || ' Line - '
               || c1_rec.line_num
               || ' - '
               || c1_rec.item_description
               || ' cancelled successfully.');
         ELSE
            FOR i IN 1 .. FND_MSG_PUB.Count_Msg
            LOOP
               apps.FND_MSG_PUB.Get (p_msg_index       => i,
                                     p_encoded         => 'F',
                                     p_data            => out_message,
                                     p_msg_index_OUT   => l_msg_index_OUT);
               l_msg_data := substr(l_msg_data || ':' || out_message,1,2000);
            END LOOP;

            FND_FILE.Put_Line (
               FND_FILE.Output,'BPA-'
               || c1_rec.segment1
               || ' Line - '
               || c1_rec.line_num
               || ' - '
               || c1_rec.item_description
               || ' cancel error '
               || l_msg_data);
         END IF;
      END LOOP;
   END cancel_bpa_lines;
   
   
 FUNCTION get_price_break(i_header_id in number,i_ship_to_org_id in number, i_line_id in number, i_line_location_id in number,i_qty in number,i_price in number) 
 return varchar    
 is
 l_return_value varchar2(2000);
 begin
  for c1_rec in (select shipment_num,quantity,price_override
                   from apps.po_line_locations a
                  where nvl(closed_code,'OPEN')='OPEN'
                    and po_line_id=i_line_id
                    --and quantity is not null
                    and nvl(quantity,i_qty+1) > i_qty
                    and price_override< i_price
                    and sysdate between nvl(start_date,sysdate-1) and nvl(end_date,sysdate+1)
                    and (ship_to_organization_id=i_ship_to_org_id or ship_to_organization_id is null)
                    and nvl(cancel_flag,'N')='N'
                    order by shipment_num)
       loop
         l_return_value := l_return_value||'ShipNum:'||c1_rec.shipment_num||' Qty:'||c1_rec.quantity||' Price:$'||c1_rec.price_override||chr(10);       
       end loop;   
       /*
       IF l_return_value is not null then
        l_return_value := 'ShipNum:::Qty:::Price'||chr(10)||l_return_value;
       END IF;*/
       return(l_return_value);
 end get_price_break;

   FUNCTION get_pz_price_break(i_vendor_id in number, i_vendor_site_id in number, i_header_id in number,i_ship_to_org_id in number, i_inventory_item_id in number, i_qty in number,i_price in number)
    return varchar2 is
 
    l_return_value varchar2(2000);
    l_price_zone number;
    l_vendor_id NUMBER;
    l_vendor_site_id NUMBER;
 
   begin    
   
     
        begin
        
            select nvl(price_zone,0)
            into   l_price_zone
            from   xxwc.xxwc_vendor_price_zone_tbl
            where  vendor_id = i_vendor_id
            and    vendor_site_id = i_vendor_site_id
            and    organization_id = i_ship_to_org_id;
        exception
            when others then
                    l_price_zone :=0;
        end;

     for c1_rec in (select price_zone_quantity, price_zone_quantity_price
                    from   apps.XXWC_BPA_PRICE_ZONE_BREAKS_TBL
                    where  po_header_id = i_header_id
                    and    inventory_item_id = i_inventory_item_id
                    and    price_zone = l_price_zone
                    and    nvl(price_zone_quantity,i_qty+1) > = i_qty
                    order by price_zone_quantity asc)
       loop
     l_return_value := l_return_value||'Qty:'||c1_rec.price_zone_quantity||' Price:$:'||c1_rec.price_zone_quantity_price||chr(10);       
       end loop;   
       /*
       IF l_return_value is not null then
        l_return_value := 'ShipNum:::Qty:::Price'||chr(10)||l_return_value;
       END IF;*/
       return(l_return_value);
     
   
   end get_pz_price_break;
   
END xxwc_po_doc_control_pkg;
/
