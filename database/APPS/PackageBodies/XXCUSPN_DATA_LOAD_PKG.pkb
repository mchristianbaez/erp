CREATE OR REPLACE PACKAGE BODY APPS."XXCUSPN_DATA_LOAD_PKG" AS

  --
  -- DEBUG Variables
  --

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(175) DEFAULT 'XXCUSPN_DATA_LOAD_PKG';
  l_err_callpoint VARCHAR2(175) DEFAULT 'START';
  --  l_distro_list   VARCHAR2(175) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  l_distro_list VARCHAR2(175) DEFAULT 'luong.vu@hdsupply.com';

  program_error EXCEPTION;

  l_req_id             NUMBER NULL;
  v_phase              VARCHAR2(50);
  v_status             VARCHAR2(50);
  v_dev_status         VARCHAR2(50);
  v_dev_phase          VARCHAR2(50);
  v_message            VARCHAR2(250);
  v_error_message      VARCHAR2(3000);
  l_statement          VARCHAR2(9000);
  l_can_submit_request BOOLEAN := TRUE;
  l_globalset          VARCHAR2(100);
  l_sec                VARCHAR2(255);
  g_org_id             NUMBER DEFAULT 163; --HD Supply Corp USD - Org
  l_batch              VARCHAR2(50);

  PROCEDURE load_land(errbuf  OUT VARCHAR2
                     ,retcode OUT NUMBER) IS
  
    l_loc_id        NUMBER;
    l_count_process NUMBER;
    l_loc           VARCHAR2(25);
  
  BEGIN
    dbms_output.enable(1000000);
  
    l_err_callpoint := 'Aquiring batch from existing Sequence for';
  
    -- DEBUG NAME
    BEGIN
      SELECT 'ADMIN_' || to_char(apps.xxcuspn_admin_batch_s.nextval)
        INTO l_batch
        FROM dual;
    
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Error in ' || l_err_callpoint;
        RAISE program_error;
    END;
  
  
    -- Process a new LAND location
  
    l_err_callpoint := 'Process a new LAND location';
  
    FOR c_cur IN (SELECT location_code
                        ,substr(building, 0, 30) building
                        ,lease_or_owned
                         --     ,address_id         (NEED DATA UPDATE TO MAP OUT)
                        ,uom_code
                        ,max_capacity
                        ,optimum_capacity
                         --      ,status
                        ,attribute_category
                        ,attribute1
                        ,attribute2
                        ,attribute4
                        ,attribute8
                         --    ,org_id
                        ,property_id
                        ,CLASS
                        ,gross_area
                        ,active_start_date
                        ,active_end_date
                        ,occupancy_status_code
                        ,assignable_emp
                        ,assignable_cc
                        ,assignable_cust
                        ,location_alias
                    FROM xxcus.xxcuspn_land_tmp)
    LOOP
    
      l_err_callpoint := 'Check if location already exist';
    
      BEGIN
        SELECT location_code
          INTO l_loc
          FROM pn.pn_locations_all loc
         WHERE c_cur.location_code = loc.location_code;
      
      EXCEPTION
        WHEN no_data_found THEN
          l_loc := '0';
      END;
    
      dbms_output.put_line('location_code exist: ' || l_loc);
    
      l_err_callpoint := 'Get new location_id';
    
      BEGIN
        --        IF l_entry_type = 'A' THEN
        SELECT pn_locations_s.nextval
          INTO l_loc_id
          FROM dual;
      
        /*        ELSE
                  SELECT location_id
                    INTO l_loc_id
                    FROM pn.pn_locations_all loc
                   WHERE c_cur.location_code = loc.location_code;
                END IF;
        */
      EXCEPTION
        WHEN OTHERS THEN
          l_sec := 'Error in ' || l_err_callpoint;
          RAISE program_error;
      END;
    
      INSERT INTO pn.pn_locations_itf
        (batch_name
        ,entry_type
        ,location_id
        ,location_type_lookup_code
        ,location_code
        ,building
        ,lease_or_owned
        ,uom_code
        ,max_capacity
        ,optimum_capacity
        ,attribute_category
        ,attribute1
        ,attribute2
        ,attribute4
        ,attribute8
        ,property_id
        ,CLASS
        ,gross_area
        ,active_start_date
        ,active_end_date
         --    ,assignable_area
        ,SOURCE
        ,creation_date
        ,last_update_date
        ,location_alias)
      VALUES
        (l_batch
        ,'A' --l_entry_type
        ,l_loc_id
        ,'LAND'
        ,c_cur.location_code
        ,c_cur.building
        ,c_cur.lease_or_owned
        ,c_cur.uom_code
        ,c_cur.max_capacity
        ,c_cur.optimum_capacity
        ,c_cur.attribute_category
        ,c_cur.attribute1
        ,c_cur.attribute2
        ,c_cur.attribute4
        ,c_cur.attribute8
        ,c_cur.property_id
        ,c_cur.class
        ,c_cur.gross_area
        ,c_cur.active_start_date
        ,c_cur.active_end_date
        ,'SHEET'
        ,SYSDATE
        ,SYSDATE
        ,c_cur.location_alias);
    
    END LOOP;
    COMMIT;
  
    --START  LOCATION IMPORT--
  
    -- get count of processes
    SELECT COUNT(*)
      INTO l_count_process
      FROM pn.pn_locations_itf
     WHERE batch_name = l_batch;
  
    IF l_count_process > 0 THEN
      --Run the concurrent job for imports for locations
      BEGIN
        -- DEBUG NAME
        l_err_callpoint := 'step 3: Running concurrent job for locations import';
        -- DEBUG NAME
      
        l_can_submit_request := xxcus_misc_pkg.set_responsibility('OPNINTERFACE',
                                                                  'Property Manager');
        IF l_can_submit_request THEN
          l_globalset := 'Global Variables are set.';
        
        ELSE
        
          l_globalset := 'Global Variables are not set.';
          l_sec       := 'Global Variables are not set for the Responsibility of xxcus_CON and the User of Conversion.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE program_error;
        END IF;
      
        fnd_file.put_line(fnd_file.log, l_globalset);
        fnd_file.put_line(fnd_file.output, l_globalset);
        --  END OF Setup parameters for running FND JOBS!
      
        BEGIN
        
          l_req_id := fnd_request.submit_request('PN', 'PNIMPCAD',
                                                 'PNIMPCAD - Import From CAD',
                                                 NULL, FALSE, l_batch, 'L',
                                                 g_org_id);
        
          COMMIT;
        
          IF (l_req_id != 0) THEN
            IF fnd_concurrent.wait_for_request(l_req_id, 6, 15000, v_phase,
                                               v_status, v_dev_phase,
                                               v_dev_status, v_message) THEN
              v_error_message := chr(10) || 'ReqID=' || l_req_id ||
                                 ' DPhase ' || v_dev_phase || ' DStatus ' ||
                                 v_dev_status || chr(10) || ' MSG - ' ||
                                 v_message;
              -- Error Returned
              IF v_dev_phase != 'COMPLETE' THEN
                --v1.12
                -- OR v_dev_status != 'NORMAL' THEN
              
                l_statement := 'An error occured in the running of the Location import' ||
                               v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_statement);
                fnd_file.put_line(fnd_file.output, l_statement);
                --
                xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                                     p_calling => l_err_callpoint,
                                                     p_ora_error_msg => SQLERRM,
                                                     p_error_desc => 'Location Interface completed with Error.  Check records for batch ' ||
                                                                      l_batch,
                                                     p_distribution_list => l_distro_list,
                                                     p_module => 'PN',
                                                     p_request_id => l_req_id);
              END IF;
              -- Then Success!
            ELSE
              l_statement := 'An error occured running the Location import' ||
                             v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_statement);
              fnd_file.put_line(fnd_file.output, l_statement);
              RAISE program_error;
            END IF;
          
          ELSE
            l_statement := 'An error occured when trying to submitting the Location import';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          END IF;
          fnd_file.put_line(fnd_file.output,
                            'Processed ' || to_char(l_count_process) ||
                             ' records.');
        
        END;
      END;
    END IF;
  
    -- Update address_id
  
    FOR c_cur IN (SELECT location_code
                        ,TRIM(address_id) address_id
                    FROM xxcus.xxcuspn_land_tmp)
    LOOP
      UPDATE pn.pn_locations_all
         SET address_id = c_cur.address_id
       WHERE location_code = c_cur.address_id;
    END LOOP;
    COMMIT;
  END load_land;

  --***********************************************************************************
  --                         LOAD BUILDING FEATURES                                  --
  --***********************************************************************************

  PROCEDURE load_bldg_feature(errbuf  OUT VARCHAR2
                             ,retcode OUT NUMBER) IS
  
    l_feature_id    NUMBER DEFAULT NULL;
    l_count_process NUMBER;
    l_type          VARCHAR2(25);
    l_rowid         VARCHAR2(1000) DEFAULT NULL;
    l_update_by     NUMBER DEFAULT 1829;
  
  BEGIN
    dbms_output.enable(1000000);
  
    -- Process building feature
  
    l_err_callpoint := 'Begin';
  
    FOR c_cur IN (SELECT *
                    FROM xxcus.xxcuspn_location_features_tmp)
    LOOP
    
      l_count_process := 1;
    
      l_err_callpoint := 'Check if building feature already exist';
    
      BEGIN
        SELECT 'UPDATE'
          INTO l_type
          FROM pn.pn_location_features_all f
         WHERE f.location_feature_id = c_cur.location_feature_id;
      
      EXCEPTION
        WHEN no_data_found THEN
          l_type := 'NEW';
      END;
    
      dbms_output.put_line('Feature_id ' || c_cur.location_feature_id ||
                           ' row type: ' || l_type);
    
      IF l_type = 'NEW' THEN
        l_err_callpoint := 'Add new building feature';
      
        apps.pnt_location_features_pkg.insert_row(x_rowid => l_rowid,
                                                  x_org_id => c_cur.org_id,
                                                  x_location_feature_id => l_feature_id,
                                                  x_last_update_date => SYSDATE,
                                                  x_last_updated_by => l_update_by,
                                                  x_creation_date => SYSDATE,
                                                  x_created_by => l_update_by,
                                                  x_last_update_login => c_cur.last_update_login,
                                                  x_location_id => c_cur.location_id,
                                                  x_location_feature_lookup_code => c_cur.location_feature_lookup_code,
                                                  x_description => c_cur.description,
                                                  x_quantity => c_cur.quantity,
                                                  x_feature_size => c_cur.feature_size,
                                                  x_uom_code => c_cur.uom_code,
                                                  x_condition_lookup_code => c_cur.condition_lookup_code,
                                                  x_attribute_category => c_cur.attribute_category,
                                                  x_attribute1 => c_cur.attribute1,
                                                  x_attribute2 => c_cur.attribute2,
                                                  x_attribute3 => c_cur.attribute3,
                                                  x_attribute4 => c_cur.attribute4,
                                                  x_attribute5 => c_cur.attribute5,
                                                  x_attribute6 => c_cur.attribute6,
                                                  x_attribute7 => c_cur.attribute7,
                                                  x_attribute8 => c_cur.attribute8,
                                                  x_attribute9 => c_cur.attribute9,
                                                  x_attribute10 => c_cur.attribute10,
                                                  x_attribute11 => c_cur.attribute11,
                                                  x_attribute12 => c_cur.attribute12,
                                                  x_attribute13 => c_cur.attribute13,
                                                  x_attribute14 => c_cur.attribute14,
                                                  x_attribute15 => c_cur.attribute15);
        COMMIT;
      ELSE
        l_err_callpoint := 'update building feature';
        apps.pnt_location_features_pkg.update_row(x_rowid => l_rowid,
                                                  x_location_feature_id => c_cur.location_feature_id,
                                                  x_last_update_date => c_cur.last_update_date,
                                                  x_last_updated_by => c_cur.last_updated_by,
                                                  x_creation_date => c_cur.creation_date,
                                                  x_created_by => c_cur.created_by,
                                                  x_last_update_login => c_cur.last_update_login,
                                                  x_location_id => c_cur.location_id,
                                                  x_location_feature_lookup_code => c_cur.location_feature_lookup_code,
                                                  x_description => c_cur.description,
                                                  x_quantity => c_cur.quantity,
                                                  x_feature_size => c_cur.feature_size,
                                                  x_uom_code => c_cur.uom_code,
                                                  x_condition_lookup_code => c_cur.condition_lookup_code,
                                                  x_attribute_category => c_cur.attribute_category,
                                                  x_attribute1 => c_cur.attribute1,
                                                  x_attribute2 => c_cur.attribute2,
                                                  x_attribute3 => c_cur.attribute3,
                                                  x_attribute4 => c_cur.attribute4,
                                                  x_attribute5 => c_cur.attribute5,
                                                  x_attribute6 => c_cur.attribute6,
                                                  x_attribute7 => c_cur.attribute7,
                                                  x_attribute8 => c_cur.attribute8,
                                                  x_attribute9 => c_cur.attribute9,
                                                  x_attribute10 => c_cur.attribute10,
                                                  x_attribute11 => c_cur.attribute11,
                                                  x_attribute12 => c_cur.attribute12,
                                                  x_attribute13 => c_cur.attribute13,
                                                  x_attribute14 => c_cur.attribute14,
                                                  x_attribute15 => c_cur.attribute15);
        COMMIT;
      END IF;
    
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Location Interface completed with Error.  Check records for batch ' ||
                                                            l_batch,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN',
                                           p_request_id => l_req_id);
    
  END load_bldg_feature;


  --***********************************************************************************
  --                         LOAD PARCEL                                             --
  --***********************************************************************************

  PROCEDURE load_parcel(errbuf  OUT VARCHAR2
                       ,retcode OUT NUMBER) IS
  
    l_loc_id        NUMBER;
    l_count_process NUMBER;
    l_entry_type    VARCHAR2(1);
    l_loc_type      VARCHAR2(25) := 'PARCEL';
    l_creation_date DATE;
  
  BEGIN
    dbms_output.enable(1000000);
  
    l_err_callpoint := 'Aquiring batch from existing Sequence for';
  
    BEGIN
      SELECT 'ADMIN_' || to_char(xxcuspn_admin_batch_s.nextval)
        INTO l_batch
        FROM dual;
    
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Error in ' || l_err_callpoint;
        RAISE program_error;
    END;
  
  
    -- Process a new parcel location
  
    l_err_callpoint := 'Process a new parcel location';
  
    FOR c_cur IN (SELECT DISTINCT *
                    FROM xxcus.xxcuspn_parcel_tmp)
    LOOP
      dbms_output.put_line('location code: ' || c_cur.location_code);
    
      l_err_callpoint := 'Check if parcel already exist';
    
      BEGIN
        /*        SELECT 'U'
                  INTO l_entry_type
                  FROM pn.pn_locations_all loc
                 WHERE c_cur.location_code = loc.location_code;
        */
        SELECT DISTINCT 'U'
          INTO l_entry_type
          FROM pn.pn_locations_all loc
         WHERE c_cur.location_code = loc.location_code;
      
      EXCEPTION
        WHEN no_data_found THEN
          l_entry_type := 'A';
      END;
    
      l_err_callpoint := 'Get location_id';
    
      BEGIN
        IF l_entry_type = 'A' THEN
          SELECT pn_locations_s.nextval
            INTO l_loc_id
            FROM dual;
        
          l_creation_date := SYSDATE;
        
        ELSE
          SELECT DISTINCT location_id
                         ,creation_date
            INTO l_loc_id
                ,l_creation_date
            FROM xxcus.xxcuspn_parcel_tmp
           WHERE location_code = c_cur.location_code;
        END IF;
      
      EXCEPTION
        WHEN OTHERS THEN
          l_sec := 'Error in ' || l_err_callpoint;
          RAISE program_error;
      END;
    
      dbms_output.put_line('processing location: ' || c_cur.location_code ||
                           'ENTRY TYPE: ' || l_entry_type);
    
      INSERT INTO pn.pn_locations_itf
        (batch_name
        ,entry_type
        ,location_id
        ,location_type_lookup_code
        ,location_code
        ,building
        ,lease_or_owned
        ,floor
        ,office
        ,uom_code
        ,max_capacity
        ,optimum_capacity
        ,rentable_area
        ,usable_area
        ,allocate_cost_center_code
        ,parent_location_id
        ,last_update_date
        ,last_update_login
        ,created_by
        ,creation_date
        ,last_updated_by
        ,attribute_category
        ,attribute1
        ,attribute2
        ,attribute3
        ,attribute4
        ,attribute5
        ,attribute6
        ,attribute7
        ,attribute8
        ,attribute9
        ,attribute10
        ,attribute11
        ,attribute12
        ,attribute13
        ,attribute14
        ,attribute15
        ,property_id
        ,CLASS
        ,gross_area
        ,active_start_date
        ,active_end_date
        ,SOURCE
        ,location_alias)
      VALUES
        (l_batch
        ,l_entry_type
        ,TRIM(l_loc_id)
        ,l_loc_type
        ,c_cur.location_code
        ,c_cur.building
        ,c_cur.lease_or_owned
        ,c_cur.floor
        ,c_cur.office
        ,c_cur.uom_code
        ,c_cur.max_capacity
        ,c_cur.optimum_capacity
        ,c_cur.rentable_area
        ,c_cur.usable_area
        ,c_cur.allocate_cost_center_code
        ,c_cur.parent_location_id
        ,SYSDATE --last_update_date
        ,c_cur.last_update_login
        ,c_cur.created_by
        ,l_creation_date
        ,c_cur.last_updated_by
        ,c_cur.attribute_category
        ,c_cur.attribute1
        ,c_cur.attribute2
        ,c_cur.attribute3
        ,c_cur.attribute4
        ,c_cur.attribute5
        ,c_cur.attribute6
        ,c_cur.attribute7
        ,c_cur.attribute8
        ,c_cur.attribute9
        ,c_cur.attribute10
        ,c_cur.attribute11
        ,c_cur.attribute12
        ,c_cur.attribute13
        ,c_cur.attribute14
        ,c_cur.attribute15
        ,c_cur.property_id
        ,c_cur.class
        ,c_cur.gross_area
        ,c_cur.active_start_date
        ,c_cur.active_end_date
        ,'SHEET' -- source
        ,c_cur.location_alias);
    
    END LOOP;
    COMMIT;
  
    --START  LOCATION IMPORT--
  
    -- get count of processes
    SELECT COUNT(*)
      INTO l_count_process
      FROM pn.pn_locations_itf
     WHERE batch_name = l_batch;
  
    IF l_count_process > 0 THEN
      --Run the concurrent job for imports for locations
      BEGIN
        -- DEBUG NAME
        l_err_callpoint := 'step 3: Running concurrent job for locations import';
        -- DEBUG NAME
      
        l_can_submit_request := xxcus_misc_pkg.set_responsibility('OPNINTERFACE',
                                                                  'Property Manager');
        IF l_can_submit_request THEN
          l_globalset := 'Global Variables are set.';
        
        ELSE
        
          l_globalset := 'Global Variables are not set.';
          l_sec       := 'Global Variables are not set for the Responsibility of xxcus_CON and the User of Conversion.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE program_error;
        END IF;
      
        fnd_file.put_line(fnd_file.log, l_globalset);
        fnd_file.put_line(fnd_file.output, l_globalset);
        --  END OF Setup parameters for running FND JOBS!
      
        BEGIN
        
          l_req_id := fnd_request.submit_request('PN', 'PNIMPCAD',
                                                 'PNIMPCAD - Import From CAD',
                                                 NULL, FALSE, l_batch, 'L',
                                                 g_org_id);
        
          COMMIT;
        
          IF (l_req_id != 0) THEN
            IF fnd_concurrent.wait_for_request(l_req_id, 6, 15000, v_phase,
                                               v_status, v_dev_phase,
                                               v_dev_status, v_message) THEN
              v_error_message := chr(10) || 'ReqID=' || l_req_id ||
                                 ' DPhase ' || v_dev_phase || ' DStatus ' ||
                                 v_dev_status || chr(10) || ' MSG - ' ||
                                 v_message;
              -- Error Returned
              IF v_dev_phase != 'COMPLETE' THEN
                --v1.12
                -- OR v_dev_status != 'NORMAL' THEN
              
                l_statement := 'An error occured in the running of the Location import' ||
                               v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_statement);
                fnd_file.put_line(fnd_file.output, l_statement);
                --
                xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                                     p_calling => l_err_callpoint,
                                                     p_ora_error_msg => SQLERRM,
                                                     p_error_desc => 'Location Interface completed with Error.  Check records for batch ' ||
                                                                      l_batch,
                                                     p_distribution_list => l_distro_list,
                                                     p_module => 'PN',
                                                     p_request_id => l_req_id);
              END IF;
              -- Then Success!
            ELSE
              l_statement := 'An error occured running the Location import' ||
                             v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_statement);
              fnd_file.put_line(fnd_file.output, l_statement);
              RAISE program_error;
            END IF;
          
          ELSE
            l_statement := 'An error occured when trying to submitting the Location import';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          END IF;
          fnd_file.put_line(fnd_file.output,
                            'Processed ' || to_char(l_count_process) ||
                             ' records.');
        
        END;
      END;
    END IF;
  
  END load_parcel;

END xxcuspn_data_load_pkg;
/
