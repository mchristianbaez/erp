CREATE OR REPLACE PACKAGE BODY APPS.xxwc_po_req_qty_update_pkg
--//============================================================================
--//
--// Object Name         :: xxwc_po_req_qty_update_pkg
--//
--// Object Type         :: Package Specification
--//
--// Object Description  :: This is used to fix Zero Requisition Qty.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     02/09/2014    Initial Build - TMS#20140904-00170
--// 2.0     Mahesh Kudle     21/10/2014    TMS#20141009-00149 Fix IR/ISO quantity discrepancies,
--//                                        Changes to cursor c_req
--//3.0     Vijaysrinivasan   11/05/2014    TMS#20141002-00050 Multi org changes 
--//============================================================================
AS

g_dflt_email            VARCHAR2(50) := 'HDSOracleDevelopers@hdsupply.com';

PROCEDURE   write_log   (p_message in varchar2)
IS
BEGIN
    dbms_output.put_line (p_message);
    fnd_file.put_line(fnd_file.log, p_message);
EXCEPTION
WHEN OTHERS THEN
    null;
END write_log;

PROCEDURE   write_output   (p_message in varchar2)
IS
BEGIN
    dbms_output.put_line (p_message);
    fnd_file.put_line(fnd_file.output, p_message);
EXCEPTION
WHEN OTHERS THEN
    null;
END write_output;

--//============================================================================
--//
--// Object Name         :: xxwc_req_zero_qty
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This is used to fix Zero Requisition Qty.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     02/09/2014    Initial Build - TMS#20140904-00170
--// 2.0     Mahesh Kudle     21/10/2014    TMS#20141009-00149 Fix IR/ISO quantity discrepancies,
--//                                        Changes to cursor c_req
--//============================================================================
PROCEDURE xxwc_req_zero_qty (retcode      OUT VARCHAR2
                            ,errmsg       OUT VARCHAR2
                            ,p_req_number IN VARCHAR2
                            ,p_days       IN NUMBER)
IS

    l_conc_req_id    NUMBER := fnd_global.conc_request_id;
    l_login_id       NUMBER := fnd_global.login_id;
    l_count          NUMBER;
    l_err_callfrom   VARCHAR2(100) ;
    l_err_callpoint  VARCHAR2(100) ;
    lv_hdr           VARCHAR2(2000);
    l_err_msg        VARCHAR2(2000);
      
 CURSOR c_req is
  SELECT req_number,
         requisition_header_id,
         requisition_line_id,
         distribution_id,
         line_num,
         req_ln_qty,
         req_dist_qty,
         quantity_delivered,
         order_number,
         line_number,
         so_ln_qty
  FROM (SELECT prha.segment1 req_number,
               prha.requisition_header_id,
               prla.requisition_line_id,
               prda.distribution_id,
               prla.line_num,
               prla.quantity req_ln_qty,
               prda.req_line_quantity req_dist_qty,
               prla.quantity_delivered,
               oha.order_number,
               ola.line_number,
               SUM (NVL (ola.shipped_quantity, ola.ordered_quantity)) so_ln_qty
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
         FROM  apps.po_requisition_headers prha,
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
               apps.po_requisition_lines   prla,
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
               apps.po_req_distributions   prda,
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
               apps.oe_order_headers       oha,
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
               apps.oe_order_lines         ola
         WHERE prha.requisition_header_id  = prla.requisition_header_id
         AND   prla.requisition_line_id    = prda.requisition_line_id
         AND   oha.orig_sys_document_ref   = prha.segment1
         AND   ola.orig_sys_document_ref   = prha.segment1
         AND   ola.source_document_line_id = prla.requisition_line_id
         AND   ola.inventory_item_id       = prla.item_id
         AND   oha.header_id               = ola.header_id
         AND   oha.order_type_id           = 1011                          -- Internal Order
         AND   ola.line_type_id            = 1012                          -- Internal line
         AND   ola.flow_status_code        <> 'CANCELLED'
         AND   NVL (prla.cancel_flag, 'N') = 'N'
         AND   NVL (prha.cancel_flag, 'N') = 'N'
         AND   prha.authorization_status   = 'APPROVED'
         AND   prha.transferred_to_oe_flag = 'Y'
         AND   prla.transferred_to_oe_flag = 'Y'
         AND   prha.type_lookup_code       = 'INTERNAL'
         --AND   prla.quantity             = 0                             -- V2.0 Commented by Mahesh for TMS#20141009-00149 on 10/21/2014
         AND   prha.segment1               = NVL(p_req_number,prha.segment1)   
         AND   TRUNC(ola.last_update_date) >= TRUNC(SYSDATE)- p_days
         GROUP BY prha.segment1,
               prha.requisition_header_id,
               prla.requisition_line_id,
               prda.distribution_id,
               prla.line_num,
               prla.quantity,
               prda.req_line_quantity,
                 prla.quantity_delivered,
               oha.order_number,
               ola.line_number)
   WHERE (req_ln_qty <> so_ln_qty OR req_dist_qty <> so_ln_qty);           -- V2.0 Added by Mahesh for TMS#20141009-00149 on 10/21/2014

  BEGIN

    l_err_callfrom := 'XXWC_PO_REQ_QTY_UPDATE_PKG.XXWC_REQ_ZERO_QTY';
    l_err_callpoint:= 'XXWC_REQ_ZERO_QTY';
    
    write_output('                            XXWC PO Requisition Zero Qty Update                                 ');
    write_output('================================================================================================');
    write_output(' ');

    lv_hdr := 'Requisition Number  Requisition Line Num  Requisition Line Id  SO Qty  Req Ln Qty  Req Dist Qty';
    write_output(lv_hdr);
    
    lv_hdr := '------------------  --------------------  -------------------  ------  ----------  ------------';
    write_output(lv_hdr);
    
    l_count := 0;
    
    FOR i in c_req    
    LOOP        
        l_count := l_count + 1;
        lv_hdr := rpad(i.req_number,19,' ')||' '||rpad(i.line_num,21,'  ')||' '||rpad(i.requisition_line_id,20,'  ')||' '||rpad(i.so_ln_qty,6,' ')||'  '||rpad(i.req_ln_qty,10,' ')||'  '||rpad(i.req_dist_qty,13,' ');
        
        write_output(lv_hdr);
        
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
        UPDATE  apps.po_requisition_lines
        SET     quantity              = i.so_ln_qty
        WHERE   requisition_line_id   = i.requisition_line_id 
        AND     requisition_header_id = i.requisition_header_id;
        --AND     quantity              = 0;                            -- V2.0 Commented by Mahesh for TMS#20141009-00149 on 10/21/2014
        
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
        UPDATE  apps.po_req_distributions
        SET     req_line_quantity   = i.so_ln_qty
        WHERE   requisition_line_id = i.requisition_line_id 
        AND     distribution_id     = i.distribution_id; 
        --AND     req_line_quantity   = 0;                              -- V2.0 Commented by Mahesh for TMS#20141009-00149 on 10/21/2014        
        
    END LOOP;
   
    COMMIT;
   
    write_output(' ');
    write_output('================================================================================================');
    write_output(' ');
    write_output('Number of Requisitions updated = ' ||l_count);

EXCEPTION
WHEN OTHERS THEN    
    RETCODE  := 2;
    l_err_msg  := ' ERROR ' || SQLCODE ||  substr(SQLERRM, 1, 2000);
    
    XXCUS_error_pkg.XXCUS_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => l_conc_req_id
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => l_err_msg
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'APPS'); 
END xxwc_req_zero_qty;
        
END xxwc_po_req_qty_update_pkg;
/

