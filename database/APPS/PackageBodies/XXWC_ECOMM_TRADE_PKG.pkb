CREATE OR REPLACE PACKAGE BODY APPS.XXWC_ECOMM_TRADE_PKG
AS
   /**************************************************************************************************
    Copyright (c) HD Supply Group
    All rights reserved.
   ***************************************************************************************************
     $Header XXWC_ECOMM_TRADE_PKG $
     Module Name: XXWC_ECOMM_TRADE_PKG.pks

     PURPOSE:

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        05/23/2017  Pattabhi Avula             Initial Version(TMS#20160915-00028)

    *************************************************************************************************/

   g_err_callfrom         VARCHAR2 (100) := 'XXWC_ECOMM_TRADE_PKG';
   g_distro_list          VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';   
   g_errbuf               VARCHAR2 (1000);
   g_retcode              VARCHAR2 (10) := 0;
   

   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_ECOMM_TRADE_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_ECOMM_TRADE_PKG with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');
   END write_error;
   
/*************************************************************************************************
     Procedure : generate_csv_files

     PURPOSE:   This procedure will pull the trader and traders data

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        05/23/2017  Pattabhi Avula             Initial Version(TMS#20160915-00028)

   *************************************************************************************************/

PROCEDURE generate_csv_files (errbuf            OUT VARCHAR2,
                              retcode           OUT VARCHAR2)
   IS
 l_filename             VARCHAR2 (240):='TraderPriceZone.csv';  -- For trader file
 l_filename1            VARCHAR2 (240):='TradePriceZone.csv';   -- For trade file
 v_file                 UTL_FILE.file_type;                     -- For trader file
 v_file1                UTL_FILE.file_type;                     -- For trade file
 l_outbound_directory   VARCHAR2 (80) := 'XXWC_ECOMM_TRADE_PRIM_DIR';
 
   -- Trader file cursor 
   CURSOR Cur_trader
    IS
	 SELECT od.organization_code || '|' || flv.description trader
       FROM qp_list_headers h,
            qp_qualifiers_v q,
            fnd_lookup_values flv,
            mtl_parameters od
      WHERE h.list_header_id = q.list_header_id
        AND flv.lookup_type = 'XXWC_TRADER_PRICE'
        AND flv.enabled_flag = 'Y'
        AND flv.meaning = h.name
        AND od.organization_id = TO_NUMBER (q.qualifier_attr_value)
        AND NVL (q.end_date_active, TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
        AND EXISTS
              (SELECT 1
                 FROM apps.xxcus_fruloc_master_vw l
                WHERE l.lob_name = 'WC'
                  AND l.operations_type IN
                         ('Wholesale Outlet', 'Distribution Center')
                  AND l.ops_status = 'ACTIVE'
                  AND l.FRU = od.attribute10);

    -- Trade file cursor 
   CURSOR Cur_trade
    IS
	 SELECT organization_code || '|' || 'XXWC_NATIONAL_TRADE_PRICE' trade
       FROM mtl_parameters mp
      WHERE     1 = 1
        AND EXISTS
              (SELECT 1
                 FROM apps.XXCUS_FRULOC_MASTER_VW l
                WHERE l.lob_name = 'WC'
                  AND l.operations_type IN
                         ('Wholesale Outlet', 'Distribution Center')
                  AND l.ops_status = 'ACTIVE'
                  AND l.FRU = mp.attribute10
                  AND lob_branch=mp.organization_code
                );
-- Directory checking variables
l_prim_dir          VARCHAR2(100) := NULL;				
l_arch_dir          VARCHAR2(100) := NULL;
l_db_name           VARCHAR2 (20) := NULL;
l_path              VARCHAR2 (240) := NULL;
l_path_arc          VARCHAR2 (240) := NULL;
l_sql               VARCHAR2 (240) := NULL;
l_sql_arc           VARCHAR2 (240) := NULL;
				
BEGIN
  write_log ('Begin the package execution');
  
  BEGIN
   SELECT LOWER (name) INTO l_db_name FROM v$database;
   
   l_path    := '/xx_iface/'||l_db_name ||'/outbound/wcs/traders';
   l_path_arc:= '/xx_iface/'||l_db_name ||'/outbound/wcs/traders/archive';

   l_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_ECOMM_TRADE_PRIM_DIR as'
      || ' '
      || ''''
      || l_path
      || '''';
	  
	  l_sql_arc :=
         'CREATE OR REPLACE DIRECTORY XXWC_ECOMM_TRADE_ARCH_DIR as'
      || ' '
      || ''''
      || l_path_arc
      || '''';
	  
    write_log ('Checking the primary directory exists or not');
	BEGIN
	SELECT directory_name
	  INTO l_prim_dir
	  FROM dba_directories 
	 WHERE directory_name=l_outbound_directory;
	EXCEPTION
      WHEN NO_DATA_FOUND THEN
	  l_prim_dir:=NULL;
	  WHEN others THEN
	  write_log ('Error while checking the primary directory');
	  END;
	  
	 write_log ('Checking the archive directory exists or not');
	 BEGIN
	 SELECT directory_name
	  INTO l_arch_dir
	  FROM dba_directories 
	 WHERE directory_name='XXWC_ECOMM_TRADE_ARCH_DIR';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
	  l_arch_dir:=NULL;
	  WHEN others THEN
	  write_log ('Error while checking the archive directory');
	  END;
	  
	IF l_prim_dir IS NULL
	 THEN
	 -- BEGIN 	  
    	write_log ('Creating the primary directory directories');
	    EXECUTE IMMEDIATE l_sql;
	--  END;
    END IF;

	IF l_arch_dir IS NULL
	 THEN
	 -- BEGIN 	  
    	write_log ('Creating the archive directory');
	    EXECUTE IMMEDIATE l_sql_arc;
	 -- END;
    END IF;
 
  EXCEPTION
    WHEN OTHERS
        THEN
    write_log ('Error in Directory creation and details are: '||SQLERRM);
  END; 
  
  BEGIN 
      --Open the file handler
	  write_log ('Before trader file opening '||SQLERRM);
      v_file:=UTL_FILE.FOPEN (l_outbound_directory,l_filename,'w');        
      write_log ('After trader file opening '||SQLERRM);     
      --Write the file header
      write_log (' Before trader file Writing');
		 
    FOR cur_trdr_rec IN Cur_trader
      LOOP         
         UTL_FILE.PUT_LINE (v_file,cur_trdr_rec.trader);
    END LOOP;

      write_log (' Closing the the trader file ...');
	  
      --Closing the file handler
      UTL_FILE.fclose (v_file);
    COMMIT;
  EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File Path is invalid.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The open_mode parameter in FOPEN is invalid.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File handle is invalid..';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File could not be opened or operated on as requested';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (v_file);
         errbuf :=
            'Operating system error occurred during the write operation';
         retcode := '2';
         write_error (errbuf); 
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (v_file);
         write_log ('Unspecified PL/SQL error.');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The requested operation failed because the file is open.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The filename parameter is invalid.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Permission to access to the file location is denied.';
         retcode := '2';
         write_error (errbuf);
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Error Msg :' || SQLERRM;
         retcode := '2';
         write_error (errbuf);
   END;  -- Trader file  logic ended
   
   -- Generating the Trade file
     BEGIN 
        --Open the file handler
        v_file1 :=
           UTL_FILE.fopen (l_outbound_directory,l_filename1,'w');
             
        --Write the file header
        write_log (' Before trade file Writing');
	  	 
      FOR cur_trd_rec IN Cur_trade
        LOOP
           UTL_FILE.put_line (v_file1,cur_trd_rec.trade);
      END LOOP;
     
        write_log (' Closing the the trade file ...');
	    
        --Closing the file handler
        UTL_FILE.fclose (v_file1);
      COMMIT;
    EXCEPTION
        WHEN UTL_FILE.invalid_path
        THEN
           UTL_FILE.fclose (v_file1);
           errbuf := 'File Path is invalid.';
           retcode := '2';
           write_error (errbuf);
        WHEN UTL_FILE.invalid_mode
        THEN
           UTL_FILE.fclose (v_file1);
           errbuf := 'The open_mode parameter in FOPEN is invalid.';
           retcode := '2';
           write_error (errbuf);
        WHEN UTL_FILE.invalid_filehandle
        THEN
           UTL_FILE.fclose (v_file1);
           errbuf := 'File handle is invalid..';
           retcode := '2';
           write_error (errbuf);
        WHEN UTL_FILE.invalid_operation
        THEN
           UTL_FILE.fclose (v_file1);
           errbuf := 'File could not be opened or operated on as requested';
           retcode := '2';
           write_error (errbuf);
        WHEN UTL_FILE.write_error
        THEN
           UTL_FILE.fclose (v_file1);
           errbuf :=
              'Operating system error occurred during the write operation';
           retcode := '2';
           write_error (errbuf);
        WHEN UTL_FILE.internal_error
        THEN
           UTL_FILE.fclose (v_file1);
           write_log ('Unspecified PL/SQL error.');
        WHEN UTL_FILE.file_open
        THEN
           UTL_FILE.fclose (v_file1);
           errbuf := 'The requested operation failed because the file is open.';
           retcode := '2';
           write_error (errbuf);
        WHEN UTL_FILE.invalid_filename
        THEN
           UTL_FILE.fclose (v_file1);
           errbuf := 'The filename parameter is invalid.';
           retcode := '2';
           write_error (errbuf);
        WHEN UTL_FILE.access_denied
        THEN
           UTL_FILE.fclose (v_file1);
           errbuf := 'Permission to access to the file location is denied.';
           retcode := '2';
           write_error (errbuf);
        WHEN OTHERS
        THEN
           UTL_FILE.fclose (v_file1);
           errbuf := 'Error Msg :' || SQLERRM;
           retcode := '2';
           write_error (errbuf);
     END;  -- Trade file  logic ended
 EXCEPTION
    WHEN OTHERS
        THEN
    errbuf := 'Error Msg :' || SQLERRM;
    retcode := '2';
    write_error (errbuf);
 END generate_csv_files; -- End procedure
 END XXWC_ECOMM_TRADE_PKG;    -- End package
 /