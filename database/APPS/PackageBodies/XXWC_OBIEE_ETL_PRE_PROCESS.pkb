CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OBIEE_ETL_PRE_PROCESS
IS
   /*************************************************************************
     $Header XXWC_OBIEE_ETL_PRE_PROCESS.pkb $
     Module Name: XXWC_OBIEE_ETL_PRE_PROCESS

     PURPOSE: Package for Loading Inventory Dimension and Inventory Snapshot

     TMS Task Id :  20140425-00055

     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    01-May-2014  Rasikha Galimova      Initial Version
     1.1    15-May-2014  Manjula Chellappan    Added the objects related to Inventory Snapshot
     1.2    27-May-2014  Manjula Chellappan    Added procedure submit_inv_dimension
     1.3    21-Jul-2014  Manjula Chellappan    Added procedure populate_special_item_vendor
                                               TMS# : 20140716-00161
                                               Updated the Index name XXWC_OE_ORDER_LINES_LUD1
                                               to XXWC_OE_ORDER_LN_LUD1 and schema from XXWC to ONT
                                               TMS# : 20140609-00203
     1.4    22-Aug-14    Manjula Chellappan    TMS# : 20140822-00104 Modified to remove the duplicates deletion from 
                                               xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##g
     1.5   14-Oct-14     Manjula Chellappan    TMS# 20141014-00253 -   Fix scheduling bugs in XXWC_OBIEE_ETL_PRE_PROCESS                                        
   **************************************************************************/



   g_start         NUMBER;
   g_errbuf        CLOB;
   g_retcode       NUMBER := 0;
   pl_dflt_email   fnd_user.email_address%TYPE
                      := 'HDSOracleDevelopers@hdsupply.com';



   --Version 1.1
   g_error_msg     VARCHAR2 (4000);
   g_owner         VARCHAR2 (10) := 'XXWC';

   --Version 1.1



   PROCEDURE drop_table (p_owner VARCHAR2, p_table_name VARCHAR2)
   /*************************************************************************
     $Header XXWC_OBIEE_ETL_PRE_PROCESS.drop_table $

     PURPOSE: Procedure Dropping the temporary tables


     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    01-May-2014  Rasikha Galimova       Initial Version

   **************************************************************************/

   IS
      l_ddl_string   VARCHAR2 (1024);
      l_retcode      NUMBER;
      l_sec          VARCHAR2 (500);
   BEGIN
      FOR cur_table
         IN (SELECT object_name, owner
               FROM all_objects
              WHERE     object_name = UPPER (TRIM (p_table_name))
                    AND object_type = 'TABLE'
                    AND owner = UPPER (TRIM (p_owner)))
      LOOP
         l_ddl_string :=
               'drop table '
            || cur_table.owner
            || '.'
            || cur_table.object_name
            || ' purge';

         EXECUTE IMMEDIATE l_ddl_string;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;

   --*******************************************************************
   --*******************************************************************
   --*******************************************************************


   PROCEDURE alter_table_temp (p_owner VARCHAR2, p_table_name VARCHAR2)
   /*************************************************************************
     $Header XXWC_OBIEE_ETL_PRE_PROCESS.alter_table_temp $

     PURPOSE: Procedure to alter the temporary Table for Parallel run

     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    01-May-2014  Rasikha Galimova       Initial Version
     1.1    29-May-2014  Manjula Chellappan        Modified to return g_retcode value

   **************************************************************************/
   IS
      l_execute_string   CLOB;
      l_max_parallel     NUMBER;
      l_procedure_name   VARCHAR2 (100)
                            := 'XXWC_OBIEE_ETL_PRE_PROCESS.alter_table_temp';
      l_sec              VARCHAR2 (500);
      l_retcode          NUMBER;
      l_database         VARCHAR2 (50);
   --  v_tablespace       VARCHAR2 (124);
   BEGIN
      --   v_tablespace := running_tablespace;

      FOR curr_table
         IN (SELECT object_name
               FROM all_objects
              WHERE     object_name = UPPER (TRIM (p_table_name))
                    AND object_type = 'TABLE'
                    AND owner = UPPER (TRIM (p_owner)))
      LOOP
         l_execute_string :=
            'ALTER TABLE ' || p_owner || '.' || p_table_name || ' NOLOGGING';

         EXECUTE IMMEDIATE l_execute_string;

         --  l_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_table_name || ' MOVE TABLESPACE ' || v_tablespace;

         --   EXECUTE IMMEDIATE l_execute_string;

         --For an Oracle RAC configuration, DOP = parallel_threads_per_cpu x CPU_COUNT x instance_count

         /* -- Removing the parallel threads, Per Paul Jackson / HotSOS
                  BEGIN
                     SELECT   (SELECT NVL (VALUE, 1)
                                 FROM v$parameter
                                WHERE name = 'parallel_threads_per_cpu')
                            * (SELECT NVL (VALUE, 1)
                                 FROM v$parameter
                                WHERE name = 'cpu_count')
                       INTO l_max_parallel
                       FROM DUAL;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_max_parallel := 32;
                  END;
         */

         -- Hard Code max parallel = 1.

         l_max_parallel := 1;


         l_execute_string :=
               'ALTER TABLE '
            || p_owner
            || '.'
            || p_table_name
            || ' parallel '
            || l_max_parallel;

         EXECUTE IMMEDIATE l_execute_string;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_retcode := 2;

         g_errbuf :=
               ' Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

         DBMS_OUTPUT.put_line (
            'error executing ' || l_execute_string || ' ' || g_errbuf);

         g_error_msg := g_error_msg || ' ' || g_errbuf;

         l_sec := 'Procss alter_table_temp# for ' || p_table_name;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_database || ' xxwc_obiee_etl_pre_process',
            p_calling             => l_sec,
            p_request_id          => -1,
            p_ora_error_msg       => SUBSTR (g_errbuf, 1, 500),
            p_error_desc          =>    l_database
                                     || ' Error running xxwc_obiee_etl_pre_process',
            p_distribution_list   => pl_dflt_email,
            p_module              => 'OBIEE');
   END;

   --*******************************************************************
   --*******************************************************************
   --*******************************************************************
   PROCEDURE create_copy_table (p_owner             VARCHAR2,
                                p_table_name        VARCHAR2,
                                p_new_table_name    VARCHAR2)
   /*************************************************************************
     $Header XXWC_OBIEE_ETL_PRE_PROCESS.create_copy_table $

     PURPOSE: Procedure Dropping the temporary tables

     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    01-May-2014  Rasikha Galimova       Initial Version
     1.1    29-May-2014  Manjula Chellappan        Modified to return g_retcode value

   **************************************************************************/
   IS
      l_ind              NUMBER := 0;
      l_procedure_name   VARCHAR2 (100)
                            := 'XXWC_OBIEE_ETL_PRE_PROCESS.CREATE_COPY_TABLE';
      l_retcode          NUMBER;
      l_sec              VARCHAR2 (500);
      l_database         VARCHAR2 (50);
   BEGIN
      drop_table (p_owner, p_new_table_name);

      FOR cur_table
         IN (SELECT table_name
               FROM all_tables
              WHERE     table_name = UPPER (p_table_name)
                    AND owner = UPPER (p_owner)
                    AND ROWNUM = 1)
      LOOP
         l_sec := ' Process CREATE_COPY_TABLE for ' || p_table_name;

         EXECUTE IMMEDIATE
               ' CREATE TABLE '
            || p_owner
            || '.'
            || p_new_table_name
            || ' AS SELECT * FROM '
            || p_owner
            || '.'
            || p_table_name
            || ' WHERE 1=2';

         l_sec := ' Process alter_table_temp for ' || p_new_table_name;

         alter_table_temp (p_owner, p_new_table_name);
         l_ind := 1;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         SELECT name
           INTO l_database
           FROM v$database
          WHERE ROWNUM = 1;

         g_retcode := 2;
         g_errbuf :=
               ' Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

         DBMS_OUTPUT.put_line (
            'error executing create_copy_table ' || g_errbuf);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_database || ' xxwc_obiee_etl_pre_process',
            p_calling             => l_sec,
            p_request_id          => -1,
            p_ora_error_msg       => SUBSTR (g_errbuf, 1, 500),
            p_error_desc          =>    l_database
                                     || ' Error running xxwc_obiee_etl_pre_process',
            p_distribution_list   => pl_dflt_email,
            p_module              => 'OBIEE');
         RAISE;
   END;


   --*******************************************************************
   --*******************************************************************
   --*******************************************************************
   --*******************************************************************
   --*******************************************************************
   -- AR-TODO: This one seems important. It needs a description of what it does
   -- and how it is executed. UC4? Toad? Not yet known?

   -- AR-TODO: This should have a more specific name. What is the driver for?
   -- Is this the equivalent of the APPS.XXWC_OBIEE_ETL_PRE_PROCESS.Populate_ETL_INV_STAGE() procedure?
   PROCEDURE populate_etl_inv_stage (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   /*************************************************************************
      $Header XXWC_OBIEE_ETL_PRE_PROCESS.populate_etl_inv_stage $

      PURPOSE: Procedure To Populate Inventory Dimension staging table

      REVISIONS:
      Ver        Date         Author                Description
      -----  -----------  ------------------        ----------------
      1.0    01-May-2014  Rasikha Galimova           Initial Version
      1.1    29-May-2014  Manjula Chellappan        Modified to return retcode value
      1.4    22-Aug-14    Manjula Chellappan        TMS# : 20140822-00104 Modified to remove the 
                                                    duplicates deletion from xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##g

    **************************************************************************/
   IS
      l_database         VARCHAR2 (64);
      l_procedure_name   VARCHAR2 (100)
         := 'XXWC_OBIEE_ETL_PRE_PROCESS.POPULATE_ETL_INV_STAGE';
      l_sec              VARCHAR2 (500);
      l_retcode          NUMBER;
   BEGIN
      /*
         CREATE TABLE xxwc.obiee_etl_inv_stage
     (
         inventory_item_id        NUMBER
        ,organization_id          NUMBER
        ,safety_qty               NUMBER
        ,prim_bin_loc             VARCHAR2 (128 BYTE)
        ,vendor_name              VARCHAR2 (128 BYTE)
        ,velosity_item_category   VARCHAR2 (128 BYTE)
        ,item_qp_list_price       NUMBER
        ,item_cost                NUMBER
        ,ast_pay_site             VARCHAR2 (128 BYTE)
        ,org_id                   NUMBER
        ,on_ord_qty               NUMBER
        ,qoh                      NUMBER
        ,item_type                VARCHAR2 (512 BYTE)
        ,vendor_num               VARCHAR2 (128 BYTE)
        ,item_number              VARCHAR2 (128 BYTE)
     )*/
      xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                'start driver',
                                                g_start);

      fnd_file.put_line (fnd_file.LOG, 'Start POPULATE_ETL_INV_STAGE');

      l_sec := ' PROP INDEX xxwc.OBIEE_ETL_INV_STAGE_N1';

      BEGIN
         EXECUTE IMMEDIATE 'DROP INDEX xxwc.OBIEE_ETL_INV_STAGE_N1';
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      l_sec := ' truncate table XXWC.OBIEE_ETL_INV_STAGE';

      EXECUTE IMMEDIATE ' truncate table XXWC.OBIEE_ETL_INV_STAGE';

      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'Table XXWC.OBIEE_ETL_INV_STAGE truncated',
         g_start);

      fnd_file.put_line (fnd_file.LOG,
                         'Table XXWC.OBIEE_ETL_INV_STAGE truncated');

      l_sec := 'Start populate_inv_data_from_eisr';

      apps.xxwc_obiee_etl_pre_process.alter_table_temp (
         'XXWC',
         'OBIEE_ETL_INV_STAGE');

      fnd_file.put_line (fnd_file.LOG, 'Start populate_inv_data_from_eisr');

      l_sec := ' Start populate_inv_data_from_eisr';

      apps.xxwc_obiee_etl_pre_process.populate_inv_data_from_eisr;

      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after populate_inv_data_from_eisr',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End populate_inv_data_from_eisr');

      fnd_file.put_line (fnd_file.LOG, 'Start get_inv_last_pay_site');

      l_sec := ' Start get_inv_last_pay_site';

      apps.xxwc_obiee_etl_pre_process.get_inv_last_pay_site;
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_inv_last_pay_site',
         g_start);
      fnd_file.put_line (fnd_file.LOG, 'End get_inv_last_pay_site');

-- Modified by Manjula on 22-Aug-14 for TMS# 20140822-00104 to load the distinct data into xxwc.obiee_etl_inv_stage 


 --     EXECUTE IMMEDIATE
  --       'INSERT /*+append*/
  --    INTO  xxwc.obiee_etl_inv_stage  SELECT * FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##g';


      EXECUTE IMMEDIATE 
        'CREATE INDEX xxwc.OBIEE_ETL_INV_STAGE_C7G1VN_N1 ON xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##G (inventory_item_id, organization_id)';


      EXECUTE IMMEDIATE
         'INSERT /*+append*/
      INTO  xxwc.obiee_etl_inv_stage ( SELECT * FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##g a
        WHERE rowid in (select max(rowid) from XXWC.OBIEE_ETL_INV_STAGE_C7G1VN##G 
     WHERE inventory_item_id = a.inventory_item_id and organization_id = a.organization_id))';

      COMMIT;

      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'Load xxwc.obiee_etl_inv_stage',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'xxwc.obiee_etl_inv_stage ');



      -- drop_table ('XXWC', 'OBIEE_ETL_INV_STAGE_C7G1VN##G');

      EXECUTE IMMEDIATE
         'CREATE INDEX xxwc.OBIEE_ETL_INV_STAGE_N1 ON xxwc.obiee_etl_inv_stage
              (
          INVENTORY_ITEM_ID,
                ORGANIZATION_ID
              ) TABLESPACE XXWC_IDX';

      --Added the below procedure call by Manjula on 21-Jul-14 for TMS # 20140716-00161


      l_sec := ' Update Special Items Vendor details from Latest PO';

      XXWC_OBIEE_ETL_PRE_PROCESS.populate_Special_item_vendor;

      DBMS_STATS.gather_index_stats (ownname   => 'XXWC',
                                     indname   => 'OBIEE_ETL_INV_STAGE_N1');
      -- drop_table ('XXWC', 'OBIEE_ETL_GET_PAY_SITE28DG8##M');
      -- drop_table ('XXWC', 'OBIEE_ETL_GET_PAY_SITE28DG89##');
      -- drop_table ('XXWC', 'OBIEE_ETL_GET_PAY_SITE28DG8##Z');
      -- drop_table ('XXWC', 'OBIEE_ETL_ITEM_TYPE_PZQVGR##');
      -- drop_table ('XXWC', 'OBIEE_ETL_GET_PAY_SITE28DG89##X');
      -- drop_table ('XXWC', 'OBIEE_ETL_INV_STAGE_C7G1VN##');
      xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                'end, success.',
                                                g_start);

      IF g_retcode = 2
      THEN
         retcode := 2;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         /* look for log messages:
         SELECT *
           FROM xxwc.xxwc_interfaces##log
          WHERE interface_name = 'OBIEE'
       ORDER BY insert_date DESC;*/
         retcode := 2;

         SELECT name
           INTO l_database
           FROM v$database
          WHERE ROWNUM = 1;

         g_errbuf :=
               'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (g_errbuf);
         xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                   SUBSTR (g_errbuf, 1, 500),
                                                   g_start);

         fnd_file.put_line (fnd_file.LOG, SUBSTR (g_errbuf, 1, 500));


         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_database || ' xxwc_obiee_etl_pre_process',
            p_calling             => l_sec,
            p_request_id          => -1,
            p_ora_error_msg       => SUBSTR (g_errbuf, 1, 500),
            p_error_desc          =>    l_database
                                     || ' Error running xxwc_obiee_etl_pre_process',
            p_distribution_list   => pl_dflt_email,
            p_module              => 'OBIEE');
   END;

   --*******************************************************************
   --*******************************************************************
   --*******************************************************************
   --*******************************************************************
   --*******************************************************************
   PROCEDURE populate_inv_data_from_eisr
   /*************************************************************************
      $Header XXWC_OBIEE_ETL_PRE_PROCESS.populate_inv_data_from_eisr $

      PURPOSE: Procedure to populate inventory data from EISR table

      REVISIONS:
      Ver        Date         Author                Description
      -----  -----------  ------------------        ----------------
      1.0    01-May-2014  Rasikha Galimova          Initial Version
      1.1    29-May-2014  Manjula Chellappan        Modified to return g_retcode value

    **************************************************************************/
   IS
      l_procedure_name   VARCHAR2 (100)
         := 'XXWC_OBIEE_ETL_PRE_PROCESS.POPULATE_INV_DATA_FROM_EISR';
      l_sec              VARCHAR2 (500);
      l_retcode          NUMBER;
      l_database         VARCHAR2 (50);
   BEGIN
      l_sec := ' Start Drop table OBIEE_ETL_INV_STAGE_C7G1VN##';


      drop_table ('XXWC', 'OBIEE_ETL_INV_STAGE_C7G1VN##');

      l_sec := ' Start Create table OBIEE_ETL_INV_STAGE_C7G1VN##';

      EXECUTE IMMEDIATE
         ' CREATE TABLE xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##
                            (
                                inventory_item_id        NUMBER
                               ,organization_id          NUMBER
                               ,safety_qty               NUMBER
                               ,prim_bin_loc             VARCHAR2 (128 BYTE)
                               ,vendor_name              VARCHAR2 (128 BYTE)
                               ,velosity_item_category   VARCHAR2 (128 BYTE)
                               ,item_qp_list_price       NUMBER
                               ,item_cost                NUMBER
                               ,ast_pay_site             VARCHAR2 (128 BYTE)
                               ,org_id                   NUMBER
                               ,on_ord_qty               NUMBER
                               ,qoh                      NUMBER
                               ,item_type                VARCHAR2 (512 BYTE)
                               ,VENDOR_NUM  VARCHAR2 (128 BYTE)
                              , item_number VARCHAR2 (128 BYTE)
                            )';

      l_sec := ' Start alter_table_temp OBIEE_ETL_INV_STAGE_C7G1VN##';

      apps.xxwc_obiee_etl_pre_process.alter_table_temp (
         'XXWC',
         'OBIEE_ETL_INV_STAGE_C7G1VN##');

      l_sec := ' Start Inser into OBIEE_ETL_INV_STAGE_C7G1VN##';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/
                            INTO  xxwc.OBIEE_ETL_INV_STAGE_C7G1VN## (organization_id
                                       ,inventory_item_id
                                       ,org_id
                                       ,safety_qty
                                       ,on_ord_qty
                                       ,prim_bin_loc
                                       ,vendor_name
                                       ,item_cost
                                        ,VENDOR_NUM
                                        ,item_number)
                            SELECT organization_id
                                  ,inventory_item_id
                                  ,162
                                  ,ss
                                  ,on_ord
                                  ,bin_loc
                                  ,vendor_name
                                  ,aver_cost
                                   ,VENDOR_NUM
                                    ,item_number
                              FROM xxeis.eis_xxwc_po_isr_tab';

      COMMIT;

      l_sec := ' Start Drop table OBIEE_ETL_SALES_VEL_5PC9Y0##';

      drop_table ('XXWC', 'OBIEE_ETL_SALES_VEL_5PC9Y0##');

      l_sec := ' Start Create table OBIEE_ETL_SALES_VEL_5PC9Y0##';

      EXECUTE IMMEDIATE
         'CREATE TABLE XXWC.OBIEE_ETL_SALES_VEL_5PC9Y0##
                            AS
                                SELECT mic.segment1, mic.inventory_item_id, mic.organization_id
                                  FROM mtl_item_categories_v mic
                                 WHERE category_set_name = ''Sales Velocity''
                                 and 1=2';

      l_sec := ' Start Alter table OBIEE_ETL_SALES_VEL_5PC9Y0##';

      EXECUTE IMMEDIATE
         'alter  TABLE xxwc.OBIEE_ETL_SALES_VEL_5PC9Y0## nologging parallel 1'; -- Modified parallel 32 to 1 per Paul Jackson

      l_sec := ' Start Insert into table OBIEE_ETL_SALES_VEL_5PC9Y0##';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/
                          INTO  xxwc.OBIEE_ETL_SALES_VEL_5PC9Y0##
                        SELECT mic.segment1, mic.inventory_item_id, mic.organization_id
                          FROM mtl_item_categories_v mic
                         WHERE category_set_name = ''Sales Velocity''';

      COMMIT;

      l_sec := ' Start Copy Table OBIEE_ETL_INV_STAGE_C7G1VN##';

      apps.xxwc_obiee_etl_pre_process.create_copy_table (
         'XXWC',
         'OBIEE_ETL_INV_STAGE_C7G1VN##',
         'OBIEE_ETL_INV_STAGE_C7G1VN##Z');

      l_sec := ' Start Insert into Table OBIEE_ETL_INV_STAGE_C7G1VN##z';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/
          INTO  xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##z
            SELECT a.inventory_item_id
                  ,a.organization_id
                  ,a.safety_qty
                  ,a.prim_bin_loc
                  ,a.vendor_name
                  ,b.segment1
                  ,a.item_qp_list_price
                  ,a.item_cost
                  ,a.ast_pay_site
                  ,a.org_id
                  ,a.on_ord_qty
                  ,a.qoh
                  ,a.item_type
                  ,a.vendor_num
                  ,a.item_number
              FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN## a, xxwc.OBIEE_ETL_SALES_VEL_5PC9Y0## b
             WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

      COMMIT;
      xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                'after obiee_sales_velocity',
                                                g_start);

      l_sec := ' Start Drop Table OBIEE_ETL_SALES_VEL_5PC9Y0##';

      drop_table ('XXWC', 'OBIEE_ETL_SALES_VEL_5PC9Y0##');

      l_sec := ' Start Drop Table OBIEE_ITEM_L_PRICE_H8HR##';

      drop_table ('XXWC', 'OBIEE_ITEM_L_PRICE_H8HR##');

      l_sec := ' Start Insert into Table xxwc.OBIEE_ITEM_L_PRICE_H8HR##';

      EXECUTE IMMEDIATE
         'CREATE TABLE xxwc.OBIEE_ITEM_L_PRICE_H8HR##
            AS

            SELECT qll.operand price, qpa.product_attr_value inventory_item_id
              FROM qp_list_headers_b qlhb
                  ,qp_list_headers_tl qlht
                  ,qp_list_lines qll
                  ,qp_pricing_attributes qpa
             WHERE qlhb.pte_code = ''
            ORDFUL''
                       AND qlhb.list_type_code = ''PRL''
                       AND NVL (qlhb.active_flag, ''N'') = ''Y''
                       AND NVL (TRUNC (qlhb.start_date_active), TRUNC (SYSDATE)) <= TRUNC (SYSDATE)
                       AND NVL (TRUNC (qlhb.end_date_active), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
                       AND qlhb.list_header_id = qlht.list_header_id
                       AND qlhb.list_header_id = qll.list_header_id
                       AND qll.list_line_type_code = ''PLL''
                       AND qll.modifier_level_code = ''LINE''
                       AND NVL (TRUNC (qll.start_date_active), TRUNC (SYSDATE)) <= TRUNC (SYSDATE)
                       AND NVL (TRUNC (qll.end_date_active), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
                       -- Pricing Attributes
                       AND qll.list_line_id = qpa.list_line_id
                       AND qll.list_header_id = qpa.list_header_id
                       AND qpa.product_attribute_context = ''ITEM''
                       AND qpa.product_attribute = ''PRICING_ATTRIBUTE1''
                       AND 1 = 2';

      l_sec := ' Start Alter Table OBIEE_ITEM_L_PRICE_H8HR##';

      apps.xxwc_obiee_etl_pre_process.alter_table_temp (
         'XXWC',
         'OBIEE_ITEM_L_PRICE_H8HR##');

      l_sec := ' Start Insert into Table OBIEE_ITEM_L_PRICE_H8HR##';


      EXECUTE IMMEDIATE
         'INSERT /*+append*/
          INTO  xxwc.OBIEE_ITEM_L_PRICE_H8HR## SELECT /*+rule*/ qll.operand price, qpa.product_attr_value inventory_item_id
          FROM qp_list_headers_b qlhb
              ,qp_list_headers_tl qlht
              ,qp_list_lines qll
              ,qp_pricing_attributes qpa
         WHERE     qlhb.pte_code = ''ORDFUL''
               AND qlhb.list_type_code = ''PRL''
               AND NVL (qlhb.active_flag, ''N'') = ''Y''
               AND NVL (TRUNC (qlhb.start_date_active), TRUNC (SYSDATE)) <= TRUNC (SYSDATE)
               AND NVL (TRUNC (qlhb.end_date_active), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
               AND qlhb.list_header_id = qlht.list_header_id
               AND qlhb.list_header_id = qll.list_header_id
               AND qll.list_line_type_code = ''PLL''
               AND qll.modifier_level_code = ''LINE''
               AND NVL (TRUNC (qll.start_date_active), TRUNC (SYSDATE)) <= TRUNC (SYSDATE)
               AND NVL (TRUNC (qll.end_date_active), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
               -- Pricing Attributes
               AND qll.list_line_id = qpa.list_line_id
               AND qll.list_header_id = qpa.list_header_id
               AND qpa.product_attribute_context = ''ITEM''
               AND qpa.product_attribute = ''PRICING_ATTRIBUTE1''';

      COMMIT;

      -- This deletes ALL rows where they part of are duplicates. This is on purpose because the older
      -- code was doing a select INTO variables and would throw an exception whenever more than
      -- one row was returned. This emulates that functionality. Per Rasikha, the rows which
      -- were part of duplicates are handled elsewhere in the code.

      l_sec := ' Start Delete from Table OBIEE_ITEM_L_PRICE_H8HR##';

      EXECUTE IMMEDIATE
         '  DELETE FROM xxwc.OBIEE_ITEM_L_PRICE_H8HR##
                          WHERE inventory_item_id IN (  SELECT inventory_item_id
                                                          FROM xxwc.OBIEE_ITEM_L_PRICE_H8HR##
                                                      GROUP BY inventory_item_id
                                                        HAVING COUNT (inventory_item_id) > 1)';

      COMMIT;

      l_sec := ' Start Copy Table OBIEE_ETL_INV_STAGE_C7G1VN##Z ';

      apps.xxwc_obiee_etl_pre_process.create_copy_table (
         'XXWC',
         'OBIEE_ETL_INV_STAGE_C7G1VN##Z',
         'OBIEE_ETL_INV_STAGE_C7G1VN##');

      fnd_file.put_line (fnd_file.LOG, 'Start ITEM_LIST_PRICE');

      l_sec := ' Start Insert into Table OBIEE_ETL_INV_STAGE_C7G1VN## ';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/
                              INTO  xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##
                            SELECT a.inventory_item_id
                                  ,a.organization_id
                                  ,a.safety_qty
                                  ,a.prim_bin_loc
                                  ,a.vendor_name
                                  ,A.velosity_item_category
                                  ,NVL( B.PRICE,20000)--a.item_qp_list_price
                                  ,a.item_cost
                                  ,a.ast_pay_site
                                  ,a.org_id
                                  ,a.on_ord_qty
                                  ,a.qoh
                                    ,a.item_type
                                   ,a.vendor_num
                              ,a.item_number
                              FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##Z  a, xxwc.OBIEE_ITEM_L_PRICE_H8HR## b
                             WHERE a.inventory_item_id  = b.inventory_item_id (+)';

      COMMIT;

      --drop_table ('XXWC', 'OBIEE_ITEM_L_PRICE_H8HR##');
      xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                'after ITEM_LIST_PRICE',
                                                g_start);

      fnd_file.put_line (fnd_file.LOG, 'End ITEM_LIST_PRICE');
   EXCEPTION
      WHEN OTHERS
      THEN
         SELECT name
           INTO l_database
           FROM v$database
          WHERE ROWNUM = 1;

         g_retcode := 2;

         g_errbuf :=
               'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (g_errbuf);

         xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                   SUBSTR (g_errbuf, 1, 500),
                                                   g_start);

         fnd_file.put_line (fnd_file.LOG, SUBSTR (g_errbuf, 1, 500));

         l_sec := 'Process POPULATE_INV_DATA_FROM_EISR ';
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_database || 'xxwc_obiee_etl_pre_process',
            p_calling             => l_sec,
            p_request_id          => -1,
            p_ora_error_msg       => SUBSTR (g_errbuf, 1, 500),
            p_error_desc          =>    l_database
                                     || 'Error running xxwc_obiee_etl_pre_process',
            p_distribution_list   => pl_dflt_email,
            p_module              => 'OBIEE');
   END;

   --**************************************************************
   --**************************************************************
   --**************************************************************
   --**************************************************************
   PROCEDURE get_inv_last_pay_site
   /*************************************************************************
     $Header XXWC_OBIEE_ETL_PRE_PROCESS.get_inv_last_pay_site $

     PURPOSE: Procedure to populate inventory data from EISR table

     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    01-May-2014  Rasikha Galimova       Initial Version
     1.1    29-May-2014  Manjula Chellappan     Modified to return g_retcode value
     1.4    22-Aug-14    Manjula Chellappan    TMS# : 20140822-00104 Modified to remove the 
                                               duplicates deletion from xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##g

   **************************************************************************/
   IS
      l_procedure_name   VARCHAR2 (100)
         := 'XXWC_OBIEE_ETL_PRE_PROCESS.GET_INV_LAST_PAY_SITE';
      l_sec              VARCHAR2 (500);
      l_database         VARCHAR2 (50);
   BEGIN
      l_sec := ' Start Drop Table OBIEE_ETL_ITEM_TYPE_PZQVGR## ';

      drop_table ('XXWC', 'OBIEE_ETL_ITEM_TYPE_PZQVGR##');

      l_sec := ' Start Create Table OBIEE_ETL_ITEM_TYPE_PZQVGR## ';

      EXECUTE IMMEDIATE
         'CREATE TABLE XXWC.OBIEE_ETL_ITEM_TYPE_PZQVGR##
                        (
                            item_type           VARCHAR2 (224)
                           ,inventory_item_id   NUMBER
                           ,organization_id     NUMBER
                           ,source_type number
                           ,source_organization_id number
                        )';

      l_sec := ' Start Alter Table OBIEE_ETL_ITEM_TYPE_PZQVGR## ';

      apps.xxwc_obiee_etl_pre_process.alter_table_temp (
         'XXWC',
         'OBIEE_ETL_ITEM_TYPE_PZQVGR##');

      EXECUTE IMMEDIATE
         'ALTER TABLE XXWC.OBIEE_ETL_ITEM_TYPE_PZQVGR##   MOVE TABLESPACE XXEIS_DATA';

      fnd_file.put_line (fnd_file.LOG, ' Start OBIEE_ETL_INV_ITEM_type');

      l_sec := ' Start Insert into OBIEE_ETL_ITEM_TYPE_PZQVGR##';

      EXECUTE IMMEDIATE
         'BEGIN
                            FOR cur_org IN (SELECT  organization_id FROM xxeis.wc_organizations##)
                            LOOP
                                INSERT /*+append*/
                                      INTO  xxwc.OBIEE_ETL_ITEM_TYPE_PZQVGR##
                                    SELECT /*+rule*/ item_type
                                          ,inventory_item_id
                                          ,organization_id
                                          ,source_type
                                          ,source_organization_id
                                      FROM mtl_system_items_b
                                     WHERE organization_id = cur_org.organization_id
                                     AND (ITEM_TYPE=''SPECIAL'' OR segment1 LIKE ''SP/%'');

                                COMMIT;
                            END LOOP;
                          END;';

      --*******************************************************************

      fnd_file.put_line (fnd_file.LOG, ' End OBIEE_ETL_INV_ITEM_type');

      l_sec := ' Start Drop table OBIEE_ETL_GET_PAY_SITE28DG89##';

      drop_table ('XXWC', 'OBIEE_ETL_GET_PAY_SITE28DG89##');
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after OBIEE_ETL_INV_ITEM_type',
         g_start);

      l_sec := ' Start create table OBIEE_ETL_GET_PAY_SITE28DG89##';

      EXECUTE IMMEDIATE
         'CREATE TABLE xxwc.OBIEE_ETL_GET_PAY_SITE28DG89##
                            AS

                            SELECT pla.po_header_id, plla.ship_to_organization_id, pla.item_id
                              FROM po_lines_all pla, po_line_locations_all plla
                             WHERE NVL (pla.cancel_flag, ''N'') = ''N''
                                       AND pla.org_id = 162
                                      --   and     pla.item_id = p_inventory_item_id
                                       AND pla.po_line_id = plla.po_line_id
                                       AND NVL (plla.cancel_flag, ''N'') = ''N''
                                       AND 1 = 2';

      l_sec := ' Start Alter table OBIEE_ETL_GET_PAY_SITE28DG89##';

      apps.xxwc_obiee_etl_pre_process.alter_table_temp (
         'XXWC',
         'OBIEE_ETL_GET_PAY_SITE28DG89##');

      EXECUTE IMMEDIATE
         'ALTER TABLE XXWC.OBIEE_ETL_GET_PAY_SITE28DG89## MOVE TABLESPACE XXEIS_DATA';

      l_sec := ' Start Insert into table OBIEE_ETL_GET_PAY_SITE28DG89##';

      EXECUTE IMMEDIATE
         '
      BEGIN
        FOR cur_pay_site IN (SELECT  organization_id FROM xxeis.wc_organizations##)
        LOOP INSERT /*+append*/
              INTO  xxwc.OBIEE_ETL_GET_PAY_SITE28DG89##
           SELECT /*+rule*/ pla.po_header_id, plla.ship_to_organization_id, pla.item_id
              FROM po_lines_all pla, po_line_locations_all plla, xxwc.OBIEE_ETL_ITEM_TYPE_PZQVGR## H
             WHERE     NVL (pla.cancel_flag, ''N'') = ''N''
                   AND pla.org_id = 162
                   AND pla.po_line_id = plla.po_line_id
                   AND NVL (plla.cancel_flag, ''N'') = ''N''
                   AND plla.ship_to_organization_id=H.ORGANIZATION_ID
                   AND H.ORGANIZATION_ID =cur_pay_site.ORGANIZATION_ID
                   AND pla.item_id=H.INVENTORY_ITEM_ID;
                    COMMIT;
        END LOOP;
      END;';

      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_special_last_pay_site1',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End get_special_last_pay_site1');

      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_special_last_pay_site2',
         g_start);

      l_sec := ' Start copy table OBIEE_ETL_GET_PAY_SITE28DG89##';

      apps.xxwc_obiee_etl_pre_process.create_copy_table (
         'XXWC',
         'OBIEE_ETL_GET_PAY_SITE28DG89##',
         'OBIEE_ETL_GET_PAY_SITE28DG8##Z');

      l_sec := ' Start Insert into table XXWC.OBIEE_ETL_GET_PAY_SITE28DG8##Z';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/ INTO XXWC.OBIEE_ETL_GET_PAY_SITE28DG8##Z
                           SELECT MAX (po_header_id), ship_to_organization_id, item_id
                           FROM xxwc.OBIEE_ETL_GET_PAY_SITE28DG89##
                          GROUP BY ship_to_organization_id, item_id';

      COMMIT;
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_special_last_pay_site3',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End get_special_last_pay_site3');

      l_sec := ' Start Drop table XXWC.OBIEE_ETL_GET_PAY_SITE28DG89##';

      drop_table ('XXWC', 'OBIEE_ETL_GET_PAY_SITE28DG89##');

      l_sec := ' Start Create table XXWC.OBIEE_ETL_GET_PAY_SITE28DG89##';

      EXECUTE IMMEDIATE
         'CREATE TABLE XXWC.OBIEE_ETL_GET_PAY_SITE28DG89##
                        (
                            pay_site_code           VARCHAR2 (224)
                             ,organization_id     NUMBER
                           , inventory_item_id   NUMBER

                        )';

      l_sec := ' Start Alter table XXWC.OBIEE_ETL_GET_PAY_SITE28DG89##';

      apps.xxwc_obiee_etl_pre_process.alter_table_temp (
         'XXWC',
         'OBIEE_ETL_GET_PAY_SITE28DG89##');

      EXECUTE IMMEDIATE
         'ALTER TABLE XXWC.OBIEE_ETL_GET_PAY_SITE28DG89##   MOVE TABLESPACE XXEIS_DATA';

      l_sec := ' Start insert into table XXWC.OBIEE_ETL_GET_PAY_SITE28DG89##';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/
              INTO  xxwc.OBIEE_ETL_GET_PAY_SITE28DG89##
            SELECT/*+rule*/ (asa.segment1 || ''-'' || assa.vendor_site_id) pay_site_code, z.ship_to_organization_id, z.item_id
              FROM po_headers_all pha
                  ,ap_suppliers asa
                  ,ap_supplier_sites_all assa
                  ,xxwc.OBIEE_ETL_GET_PAY_SITE28DG8##Z z
             WHERE     pha.po_header_id = z.po_header_id
                   AND pha.vendor_id = asa.vendor_id
                   AND asa.vendor_id = assa.vendor_id
                   AND assa.org_id = 162
                   AND NVL (assa.pay_site_flag, ''N'') = ''Y''';

      COMMIT;
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_special_last_pay_site4',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End get_special_last_pay_site4');

      --*******************************************************
      --PROCESS ALL OTHER ITEMS

      l_sec := ' Start Drop table OBIEE_ETL_GET_PAY_SITE28DG8##M';

      drop_table ('XXWC', 'OBIEE_ETL_GET_PAY_SITE28DG8##M');

      l_sec := ' Start create table OBIEE_ETL_GET_PAY_SITE28DG8##M';

      EXECUTE IMMEDIATE
         'CREATE TABLE XXWC.OBIEE_ETL_GET_PAY_SITE28DG8##M
                        (
                            pay_site_code           VARCHAR2 (224)
                             ,organization_id     NUMBER
                           , inventory_item_id   NUMBER
                           ,primary_pay_site_flag VARCHAR2 (22)

                        )';

      l_sec := ' Start Alter table OBIEE_ETL_GET_PAY_SITE28DG8##M';

      apps.xxwc_obiee_etl_pre_process.alter_table_temp (
         'XXWC',
         'OBIEE_ETL_GET_PAY_SITE28DG8##M');

      EXECUTE IMMEDIATE
         'ALTER TABLE XXWC.OBIEE_ETL_GET_PAY_SITE28DG8##M   MOVE TABLESPACE XXEIS_DATA';

      l_sec := ' Start Insert into table OBIEE_ETL_GET_PAY_SITE28DG8##M';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/
                              INTO  xxwc.OBIEE_ETL_GET_PAY_SITE28DG8##M
                          SELECT /*+rule*/(asa.segment1 || ''-'' || assa.vendor_site_id) site_identifier
                              ,a.organization_id
                               ,a.inventory_item_id
                              ,NVL (assa.pay_site_flag, ''N'') primary_pay_site_flag
                          FROM ap_suppliers asa, ap_supplier_sites_all assa, xxeis.eis_xxwc_po_isr_tab a
                         WHERE     asa.segment1 = a.vendor_num
                               AND asa.vendor_id = assa.vendor_id
                               AND assa.org_id = 162
                               AND NVL (assa.pay_site_flag, ''N'') = ''Y''
                               AND a.vendor_num IS NOT NULL';

      COMMIT;
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_special_last_pay_site_m1',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End get_special_last_pay_site_m1');

      -------

      l_sec := ' Start copy table OBIEE_ETL_INV_STAGE_C7G1VN##';

      apps.xxwc_obiee_etl_pre_process.create_copy_table (
         'XXWC',
         'OBIEE_ETL_INV_STAGE_C7G1VN##',
         'OBIEE_ETL_INV_STAGE_C7G1VN##V');

      l_sec := ' Start Insert into table OBIEE_ETL_INV_STAGE_C7G1VN##V';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/ into xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##v
        SELECT a.inventory_item_id
              ,a.organization_id
              ,a.safety_qty
              ,a.prim_bin_loc
              ,a.vendor_name
              ,a.velosity_item_category
              ,a.item_qp_list_price
              ,a.item_cost
              ,b.pay_site_code ast_pay_site
              ,a.org_id
              ,a.on_ord_qty
              ,a.qoh
              ,a.item_type
              ,a.vendor_num
              ,a.item_number
          FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN## a, xxwc.OBIEE_ETL_GET_PAY_SITE28DG89## b
         WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

      COMMIT;
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_special_last_pay_site_m2',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End get_special_last_pay_site_m2');

      l_sec := ' Start Copy table OBIEE_ETL_INV_STAGE_C7G1VN##P';

      apps.xxwc_obiee_etl_pre_process.create_copy_table (
         'XXWC',
         'OBIEE_ETL_INV_STAGE_C7G1VN##',
         'OBIEE_ETL_INV_STAGE_C7G1VN##P');

      l_sec := ' Start Insert into table OBIEE_ETL_INV_STAGE_C7G1VN##P';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/ into xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##P
                            SELECT a.inventory_item_id
                                  ,a.organization_id
                                  ,a.safety_qty
                                  ,a.prim_bin_loc
                                  ,a.vendor_name
                                  ,a.velosity_item_category
                                  ,a.item_qp_list_price
                                  ,a.item_cost
                                  ,a.ast_pay_site
                                  ,a.org_id
                                  ,a.on_ord_qty
                                  ,a.qoh
                                  ,a.item_type
                                  ,a.vendor_num
                                  ,a.item_number
                              FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##V a
                             WHERE a.ast_pay_site is null';

      COMMIT;
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_special_last_pay_site_m3',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End get_special_last_pay_site_m3');

      l_sec := ' Start Delete from table OBIEE_ETL_GET_PAY_SITE28DG8##M';

      EXECUTE IMMEDIATE
         'DELETE
          FROM xxwc.OBIEE_ETL_GET_PAY_SITE28DG8##M
         WHERE (inventory_item_id, organization_id)
         IN (  SELECT inventory_item_id, organization_id
                    FROM xxwc.OBIEE_ETL_GET_PAY_SITE28DG8##M
                GROUP BY inventory_item_id, organization_id
                  HAVING COUNT (inventory_item_id) > 1)
                  AND pay_site_code=''6410-41680''';

      COMMIT;

      l_sec := ' Start Copy Table OBIEE_ETL_INV_STAGE_C7G1VN##Y';

      apps.xxwc_obiee_etl_pre_process.create_copy_table (
         'XXWC',
         'OBIEE_ETL_INV_STAGE_C7G1VN##',
         'OBIEE_ETL_INV_STAGE_C7G1VN##Y');

      l_sec := ' Start insert into Table OBIEE_ETL_INV_STAGE_C7G1VN##Y';


      EXECUTE IMMEDIATE
         'INSERT /*+append*/ into xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##Y
            SELECT a.inventory_item_id
                  ,a.organization_id
                  ,a.safety_qty
                  ,a.prim_bin_loc
                  ,a.vendor_name
                  ,a.velosity_item_category
                  ,a.item_qp_list_price
                  ,a.item_cost
                  ,b.pay_site_code ast_pay_site
                  ,a.org_id
                  ,a.on_ord_qty
                  ,a.qoh
                  ,a.item_type
                  ,a.vendor_num
                  ,a.item_number
              FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##P a, xxwc.OBIEE_ETL_GET_PAY_SITE28DG8##M b
             WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

      COMMIT;
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_special_last_pay_site_m4',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End get_special_last_pay_site_m4');

      l_sec := ' Start copy Table OBIEE_ETL_INV_STAGE_C7G1VN##P';


      apps.xxwc_obiee_etl_pre_process.create_copy_table (
         'XXWC',
         'OBIEE_ETL_INV_STAGE_C7G1VN##',
         'OBIEE_ETL_INV_STAGE_C7G1VN##P');

      l_sec := ' Start insert into Table OBIEE_ETL_INV_STAGE_C7G1VN##P';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/ into xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##P

                        SELECT a.inventory_item_id
                              ,a.organization_id
                              ,a.safety_qty
                              ,a.prim_bin_loc
                              ,a.vendor_name
                              ,a.velosity_item_category
                              ,a.item_qp_list_price
                              ,a.item_cost
                              ,a.ast_pay_site
                              ,a.org_id
                              ,a.on_ord_qty
                              ,a.qoh
                              ,a.item_type
                              ,a.vendor_num
                              ,a.item_number
                          FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##v a
                         WHERE a.ast_pay_site IS NOT NULL';

      COMMIT;
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_special_last_pay_site_m5',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End get_special_last_pay_site_m5');

      l_sec := ' Start copy  Table OBIEE_ETL_INV_STAGE_C7G1VN##G';

      apps.xxwc_obiee_etl_pre_process.create_copy_table (
         'XXWC',
         'OBIEE_ETL_INV_STAGE_C7G1VN##P',
         'OBIEE_ETL_INV_STAGE_C7G1VN##G');

      l_sec := ' Start insert into  Table OBIEE_ETL_INV_STAGE_C7G1VN##G';

      EXECUTE IMMEDIATE
         'INSERT /*+append*/ into xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##G
                        SELECT * FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##P
                        UNION ALL
                        SELECT * FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##y';

      COMMIT;
      --drop_table ('XXWC', 'OBIEE_ETL_INV_STAGE_C7G1VN##V');
      --drop_table ('XXWC', 'OBIEE_ETL_INV_STAGE_C7G1VN##P');
      --drop_table ('XXWC', 'OBIEE_ETL_INV_STAGE_C7G1VN##Y');
      --drop_table ('XXWC','OBIEE_ETL_INV_STAGE_C7G1VN##G');
      --drop_table ('XXWC', 'OBIEE_ETL_INV_STAGE_C7G1VN##Z');
      --drop_table ('XXWC','OBIEE_ETL_INV_STAGE_C7G1VN##');
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'after get_special_last_pay_site_m6',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End get_special_last_pay_site_m6');


-- commented the below section by Manjula on 22-Aug-14 for TMS# 20140822-00104 to avoid the deletion of duplicate records
-- New code is added to the main procedure to select the distinct records

/*

  delete_dups_in_driver_table;



      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'delete_dups_in_driver_table',
         g_start);

      fnd_file.put_line (fnd_file.LOG, 'End delete_dups_in_driver_table');

*/


   EXCEPTION
      WHEN OTHERS
      THEN
         SELECT name
           INTO l_database
           FROM v$database
          WHERE ROWNUM = 1;

         g_retcode := 2;
         g_errbuf :=
               'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || 'Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (g_errbuf);
         xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                   SUBSTR (g_errbuf, 1, 500),
                                                   g_start);

         fnd_file.put_line (fnd_file.LOG, SUBSTR (g_errbuf, 1, 500));


         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_database || 'xxwc_obiee_etl_pre_process',
            p_calling             => l_sec,
            p_request_id          => -1,
            p_ora_error_msg       => SUBSTR (g_errbuf, 1, 500),
            p_error_desc          =>    l_database
                                     || 'Error running xxwc_obiee_etl_pre_process',
            p_distribution_list   => pl_dflt_email,
            p_module              => 'OBIEE');
   END;


   -----****************************************************************
   -----****************************************************************
   -----****************************************************************
   -----****************************************************************
   -- AR-TODO: Rename this so it is clear that this is for the Inventory process.
   PROCEDURE delete_dups_in_driver_table
   /*************************************************************************
     $Header XXWC_OBIEE_ETL_PRE_PROCESS.delete_dups_in_driver_table $

     PURPOSE: Procedure to populate inventory data from EISR table

     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    01-May-2014  Rasikha Galimova       Initial Version
     1.1    29-May-2014  Manjula Chellappan        Modified to return g_retcode value

   **************************************************************************/
   IS
      l_procedure_name   VARCHAR2 (100)
         := 'XXWC_OBIEE_ETL_PRE_PROCESS.DELETE_DUPS_IN_DRIVER_TABLE';
      l_database         VARCHAR2 (50);
      l_sec              VARCHAR2 (500);
   BEGIN
      l_sec := ' Start delete from Table OBIEE_ETL_INV_STAGE_C7G1VN##G';

      EXECUTE IMMEDIATE
         '
DELETE FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##G
      WHERE     (inventory_item_id, organization_id) IN (  SELECT inventory_item_id, organization_id
                                                             FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##G
                                                         GROUP BY inventory_item_id, organization_id
                                                           HAVING COUNT (inventory_item_id) > 1)
            AND ast_pay_site = ''6410-41680''';

      COMMIT;

      l_sec := ' Start drop table OBIEE_ETL_INV_STAGE_89UYT##';

      drop_table ('XXWC', 'OBIEE_ETL_INV_STAGE_89UYT##');

      l_sec := ' Start insert into table OBIEE_ETL_INV_STAGE_89UYT##';

      EXECUTE IMMEDIATE
         'CREATE TABLE xxwc.OBIEE_ETL_INV_STAGE_89UYT##
            AS
                SELECT ROWID vrowid, inventory_item_id, organization_id
                  FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##G
                 WHERE (inventory_item_id, organization_id)
                    IN (  SELECT inventory_item_id, organization_id
                            FROM xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##G
                        GROUP BY inventory_item_id, organization_id
                          HAVING COUNT (inventory_item_id) > 1)';

      l_sec := ' Start Delete duplicates from OBIEE_ETL_INV_STAGE_C7G1VN##G';

   xxwc_common_tunning_helpers.deleteduplicates (
         'xxwc.OBIEE_ETL_INV_STAGE_C7G1VN##G ',
         'inventory_item_id, organization_id ',
         'rowid in ( select vrowid from xxwc.obiee_etl_inv_stage_89uyt##)');
      -- drop_table ('XXWC', 'OBIEE_ETL_INV_STAGE_89UYT##');


      fnd_file.put_line (fnd_file.LOG, 'End deleteduplicates');
   EXCEPTION
      WHEN OTHERS
      THEN
         g_retcode := 2;

         g_errbuf :=
               'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || 'Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (g_errbuf);
         xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                   SUBSTR (g_errbuf, 1, 500),
                                                   g_start);
         fnd_file.put_line (fnd_file.LOG, SUBSTR (g_errbuf, 1, 500));


         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_database || 'xxwc_obiee_etl_pre_process',
            p_calling             => l_sec,
            p_request_id          => -1,
            p_ora_error_msg       => SUBSTR (g_errbuf, 1, 500),
            p_error_desc          =>    l_database
                                     || 'Error running xxwc_obiee_etl_pre_process',
            p_distribution_list   => pl_dflt_email,
            p_module              => 'OBIEE');
   END;


   -- Enter further code below as specified in the Package spec.

   --*******************************************************************
   --*******************************************************************
   --*******************************************************************
   --*******************************************************************
   --*******************************************************************
   -- Is this the one that is supposed to be named Populate_ETL_CUST_TRX_STAGE()
   -- or is it just a part of that procedure?
   PROCEDURE populate_obiee_etl_pre_oel
   /*************************************************************************
     $Header XXWC_OBIEE_ETL_PRE_PROCESS.populate_obiee_etl_pre_oel $

     PURPOSE: Procedure to Preprocess Order liness Data

     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    01-May-2014  Rasikha Galimova       Initial Version
     1.1    29-May-2014  Manjula Chellappan     Modified to return g_retcode value
     1.2    31-Jul-2014  Manjula Chellappan     Updated the Index name XXWC_OE_ORDER_LINES_LUD1
                             to XXWC_OE_ORDER_LN_LUD1 and schema from XXWC to ONT
                                                TMS# : 20140609-00203

   **************************************************************************/
   IS
      l_table_exists             VARCHAR2 (1) := 'N';
      l_trunc_last_update_date   DATE;
      l_execute_string           CLOB;
      l_delta_exists             VARCHAR2 (1) := 'N';
      l_procedure_name           VARCHAR2 (100)
         := 'XXWC_OBIEE_ETL_PRE_PROCESS.POPULATE_OBIEE_ETL_PRE_OEL';
      l_database                 VARCHAR2 (50);
      l_sec                      VARCHAR2 (500);

      -- AR-TODO: An embedded procedure? A little strange but seems fine. Please add a comment
      -- explaining what this is for and why it's here.
      PROCEDURE rebuild_index
      IS
      BEGIN
         BEGIN
            l_sec := ' Start Drop index OBIEE_ETL_PRE_ETL_OL_STAGE_N1';

            EXECUTE IMMEDIATE 'drop index XXWC.OBIEE_ETL_PRE_ETL_OL_STAGE_N1';
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         BEGIN
            l_sec := ' Start Drop index OBIEE_ETL_PRE_ETL_OL_STAGE_N2';

            EXECUTE IMMEDIATE 'drop index XXWC.OBIEE_ETL_PRE_ETL_OL_STAGE_N2';
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         BEGIN
            l_sec := ' Start Create index OBIEE_ETL_PRE_ETL_OL_STAGE_N1';

            EXECUTE IMMEDIATE
               'CREATE INDEX XXWC.OBIEE_ETL_PRE_ETL_OL_STAGE_N1 ON xxwc.obiee_etl_pre_etl_ol_stage
                 (
             line_id
                 ) TABLESPACE XXWC_IDX';

            DBMS_STATS.gather_index_stats (
               ownname   => 'XXWC',
               indname   => 'OBIEE_ETL_PRE_ETL_OL_STAGE_N1');
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         BEGIN
            l_sec := ' Start Create index OBIEE_ETL_PRE_ETL_OL_STAGE_N2';

            EXECUTE IMMEDIATE
               'CREATE INDEX XXWC.OBIEE_ETL_PRE_ETL_OL_STAGE_N2 ON xxwc.obiee_etl_pre_etl_ol_stage
                 (
             trunc_last_update_date
                 ) TABLESPACE XXWC_IDX';

            DBMS_STATS.gather_index_stats (
               ownname   => 'XXWC',
               indname   => 'OBIEE_ETL_PRE_ETL_OL_STAGE_N2');
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END;
   BEGIN
      l_sec := ' Start process xxwc_oe_order_lines_index';
      
      
--   Below line updated for Version 1.2 by Manjula on 31-Jul-14 for TMS 20140609-00203

--    apps.xxwc_oe_order_lines_index ('XXWC', 'XXWC_OE_ORDER_LINES_LUD1');

      apps.xxwc_oe_order_lines_index ('ONT', 'XXWC_OE_ORDER_LN_LUD1');      

      --*************************************************
      --REPOPULATE THE TABLE AGAIN TO HAVE last_update_date FROM ORDER_LINES_TABLE
      --*************************************************
      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'start populate_obiee_etl_pre_oel',
         g_start);
      fnd_file.put_line (fnd_file.LOG, 'Start populate_obiee_etl_pre_oel');

      FOR cur_obj
         IN (SELECT 'Y'
               FROM all_objects
              -- AR-TODO: Is this table the one that is supposed to be named xxwc.obiee_etl_cust_trx_stage or
              -- is this something else?
              WHERE object_name = UPPER ('obiee_etl_pre_etl_ol_stage'))
      LOOP
         l_table_exists := 'Y';
      END LOOP;

      IF l_table_exists = 'N'
      THEN
         drop_table ('XXWC', 'OBIEE_ETL_PRE_ETL_OL_STAGE');

         l_sec := ' Start Create table obiee_etl_pre_etl_ol_stage';

         EXECUTE IMMEDIATE
            '
            CREATE TABLE xxwc.obiee_etl_pre_etl_ol_stage
            AS
                SELECT oel.line_id
                      ,oel.shipping_method_code
                      ,oel.pricing_date
                      ,oel.actual_shipment_date
                      ,oel.cust_po_number
                      ,oel.ordered_quantity
                      ,oel.unit_list_price
                      ,oel.source_type_code
                      ,oel.unit_cost
                      ,oel.attribute7
                      ,oel.header_id
                      ,oel.ship_from_org_id
                      ,TO_NUMBER (TO_CHAR (TRUNC (oel.last_update_date), ''J'')) last_date_j
                      ,TRUNC (oel.last_update_date) trunc_last_update_date
                  FROM oe_order_lines_all oel
                 WHERE 1 = 2';

         l_sec := ' Start Alter table obiee_etl_pre_etl_ol_stage';

         apps.xxwc_obiee_etl_pre_process.alter_table_temp (
            'XXWC',
            'OBIEE_ETL_PRE_ETL_OL_STAGE');

         l_sec :=
            ' Start Alter table obiee_etl_pre_etl_ol_stage move tablespace';

         EXECUTE IMMEDIATE
            'ALTER TABLE  xxwc.obiee_etl_pre_etl_ol_stage MOVE TABLESPACE XXEIS_DATA';

         l_sec := ' Start insert into table obiee_etl_pre_etl_ol_stage ';

         FOR cur_last_upd_date
            IN (SELECT DISTINCT TRUNC (oel.last_update_date) update_date
                  FROM oe_order_lines_all oel)
         LOOP
         
-- Below line updated for Version 1.2 by Manjula on 31-Jul-14 for TMS 20140609-00203         
            l_execute_string :=
               'INSERT /*+ append */
                          INTO  xxwc.obiee_etl_pre_etl_ol_stage
                          SELECT /*+ INDEX(OEL XXWC_OE_ORDER_LN_LUD1) rule */ oel.line_id
                              ,oel.shipping_method_code
                              ,oel.pricing_date
                              ,oel.actual_shipment_date
                              ,oel.cust_po_number
                              ,oel.ordered_quantity
                              ,oel.unit_list_price
                              ,oel.source_type_code
                              ,oel.unit_cost
                              ,oel.attribute7
                              ,oel.header_id
                              ,oel.ship_from_org_id
                              ,TO_NUMBER (TO_CHAR (TRUNC (oel.last_update_date), ''J''))
                              ,TRUNC (oel.last_update_date)
                          FROM oe_order_lines_all oel
                      where TRUNC(oel.LAST_UPDATE_DATE)=:update_date ';

            EXECUTE IMMEDIATE l_execute_string
               USING cur_last_upd_date.update_date;

            COMMIT;
            xxwc_common_tunning_helpers.elapsed_time (
               'OBIEE',
                  'populate_obiee_etl_pre_oel date='
               || cur_last_upd_date.update_date,
               g_start);

            fnd_file.put_line (fnd_file.LOG,
                               'End populate_obiee_etl_pre_oel');
         END LOOP;

         rebuild_index;
         xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                   'after rebuild_index',
                                                   g_start);

         fnd_file.put_line (fnd_file.LOG, 'End rebuild_index');
      --*************************************************
      --END REPOPULATE THE TABLE AGAIN TO HAVE last_update_date FROM ORDER_LINES_TABLE
      --*************************************************
      ELSE
         l_sec := ' Start Process xxwc_oe_order_lines_index ';


-- Below line updated for Version 1.2 by Manjula on 31-Jul-14 for TMS 20140609-00203

--       apps.xxwc_oe_order_lines_index ('XXWC', 'XXWC_OE_ORDER_LINES_LUD1');
         apps.xxwc_oe_order_lines_index ('ONT', 'XXWC_OE_ORDER_LN_LUD1');
         

         EXECUTE IMMEDIATE
            'SELECT MAX(TRUNC_LAST_UPDATE_DATE)  FROM XXWC.obiee_etl_pre_etl_ol_stage'
            INTO l_trunc_last_update_date;

         l_sec := ' Start xxwc_obiee_etl_pre_process.create_copy_table ';


         apps.xxwc_obiee_etl_pre_process.create_copy_table (
            'XXWC',
            'OBIEE_ETL_PRE_ETL_OL_STAGE',
            'OBIEE_ETL_PRE_ETL_OL##');

         l_sec := ' Start Insert into xxwc.OBIEE_ETL_PRE_ETL_OL## ';

         FOR cur_last_upd_date
            IN (SELECT DISTINCT TRUNC (oel.last_update_date) update_date
                  FROM oe_order_lines_all oel
                 WHERE TRUNC (oel.last_update_date) >=
                          l_trunc_last_update_date)
         LOOP
            l_delta_exists := 'Y';
            
-- Below line updated for Version 1.2 by Manjula on 31-Jul-14 for TMS 20140609-00203

            l_execute_string :=
               'INSERT /*+ append */
                      INTO  xxwc.OBIEE_ETL_PRE_ETL_OL##
                      SELECT /*+ INDEX(OEL XXWC_OE_ORDER_LN_LUD1) rule */oel.line_id
                          ,oel.shipping_method_code
                          ,oel.pricing_date
                          ,oel.actual_shipment_date
                          ,oel.cust_po_number
                          ,oel.ordered_quantity
                          ,oel.unit_list_price
                          ,oel.source_type_code
                          ,oel.unit_cost
                          ,oel.attribute7
                          ,oel.header_id
                          ,oel.ship_from_org_id
                          ,TO_NUMBER (TO_CHAR (TRUNC (oel.last_update_date), ''J''))
                          ,TRUNC (oel.last_update_date)
                      FROM oe_order_lines_all oel
                  where TRUNC(oel.LAST_UPDATE_DATE)=:update_date ';

            EXECUTE IMMEDIATE l_execute_string
               USING cur_last_upd_date.update_date;

            COMMIT;
            xxwc_common_tunning_helpers.elapsed_time (
               'OBIEE',
                  'populate_obiee_etl_pre_oel date='
               || cur_last_upd_date.update_date,
               g_start);

            fnd_file.put_line (fnd_file.LOG,
                               'End populate_obiee_etl_pre_oel');
         END LOOP;

         IF l_delta_exists = 'Y'
         THEN
            l_sec := ' Start Merge into xxwc.obiee_etl_pre_etl_ol_stage ';

            EXECUTE IMMEDIATE
               'MERGE INTO xxwc.obiee_etl_pre_etl_ol_stage a
                 USING xxwc.obiee_etl_pre_etl_ol## b
                    ON (a.line_id = b.line_id)
            WHEN MATCHED
            THEN
                UPDATE SET a.shipping_method_code = b.shipping_method_code
                          ,a.pricing_date = b.pricing_date
                          ,a.actual_shipment_date = b.actual_shipment_date
                          ,a.cust_po_number = b.cust_po_number
                          ,a.ordered_quantity = b.ordered_quantity
                          ,a.unit_list_price = b.unit_list_price
                          ,a.source_type_code = b.source_type_code
                          ,a.unit_cost = b.unit_cost
                          ,a.attribute7 = b.attribute7
                          ,a.header_id = b.header_id
                          ,a.ship_from_org_id = b.ship_from_org_id
                          ,a.last_date_j = b.last_date_j
                          ,a.trunc_last_update_date = b.trunc_last_update_date
            WHEN NOT MATCHED
            THEN
                INSERT     (line_id
                           ,shipping_method_code
                           ,pricing_date
                           ,actual_shipment_date
                           ,cust_po_number
                           ,ordered_quantity
                           ,unit_list_price
                           ,source_type_code
                           ,unit_cost
                           ,attribute7
                           ,header_id
                           ,ship_from_org_id
                           ,last_date_j
                           ,trunc_last_update_date)
                    VALUES (b.line_id
                           ,b.shipping_method_code
                           ,b.pricing_date
                           ,b.actual_shipment_date
                           ,b.cust_po_number
                           ,b.ordered_quantity
                           ,b.unit_list_price
                           ,b.source_type_code
                           ,b.unit_cost
                           ,b.attribute7
                           ,b.header_id
                           ,b.ship_from_org_id
                           ,b.last_date_j
                           ,b.trunc_last_update_date)';

            COMMIT;
            -- drop_table ('XXWC', 'OBIEE_ETL_PRE_ETL_OL##');
            xxwc_common_tunning_helpers.elapsed_time (
               'OBIEE',
               'after merge  populate_obiee_etl_pre_oel',
               g_start);

            fnd_file.put_line (fnd_file.LOG,
                               'End get_special_last_pay_site1');
            rebuild_index;

            xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                      'after rebuild_index',
                                                      g_start);
         END IF;
      END IF;

      xxwc_common_tunning_helpers.elapsed_time (
         'OBIEE',
         'success populate_obiee_etl_pre_oel',
         g_start);
      fnd_file.put_line (fnd_file.LOG, 'End populate_obiee_etl_pre_oel');

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- AR-TODO: This doesn't look like it is handling errors properly according to our standards.
         /* look for log messages:
         SELECT *
           FROM xxwc.xxwc_interfaces##log
          WHERE interface_name = 'OBIEE'
       ORDER BY insert_date DESC;*/


         DBMS_OUTPUT.put_line (l_execute_string);

         SELECT name
           INTO l_database
           FROM v$database
          WHERE ROWNUM = 1;

         g_retcode := 2;
         g_errbuf :=
               'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (g_errbuf);
         xxwc_common_tunning_helpers.elapsed_time ('OBIEE',
                                                   SUBSTR (g_errbuf, 1, 500),
                                                   g_start);

         fnd_file.put_line (fnd_file.LOG, SUBSTR (g_errbuf, 1, 500));

         l_sec := 'Process POPULATE_OBIEE_ETL_PRE_OEL ';
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_database || ' xxwc_obiee_etl_pre_process',
            p_calling             => l_sec,
            p_request_id          => -1,
            p_ora_error_msg       => SUBSTR (g_errbuf, 1, 500),
            p_error_desc          =>    l_database
                                     || ' Error running xxwc_obiee_etl_pre_process',
            p_distribution_list   => pl_dflt_email,
            p_module              => 'OBIEE');
   END;



   /******************************************************************
    Version 1.1
   *******************************************************************/



   /*---------------------------------------
   Main Procedure to load inventory snapshot
   ------------------------------------------*/


   PROCEDURE load_snapshot_main (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   /*************************************************************************
     $Header XXWC_OBIEE_ETL_PRE_PROCESS.Load_snapshot_main $

     PURPOSE: Main Procedure to load inventory snapshot

     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    15-May-2014  Manjula Chellappan    Initial Version

   **************************************************************************/

   IS
      l_retcode   NUMBER := 0;
   --0 for sucess and 2 for failure


   BEGIN
      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process Main : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_snapshot_main',
         g_start);

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 1 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_item',
         g_start);

      --Procedure Call

      load_inv_item (l_retcode);


      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 1 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_item '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);

         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 1 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_item ',
            g_start);
      END IF;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 2 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_po_open',
         g_start);

      --Procedure Call

      load_po_open (l_retcode);


      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 2 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_po_open '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);


         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 2 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_po_open',
            g_start);
      END IF;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 3 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_onhand',
         g_start);

      --Procedure Call

      load_inv_onhand (l_retcode);

      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 3 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_onhand '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);

         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 3 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_onhand',
            g_start);
      END IF;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 4 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_vndrcons',
         g_start);

      --Procedure Call

      load_inv_vndrcons (l_retcode);

      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 4 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_vndrcons '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);

         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 4 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_vndrcons',
            g_start);
      END IF;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 5 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_category',
         g_start);

      --Procedure Call

      load_inv_category (l_retcode);

      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 5 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_category '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);


         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 5 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_category',
            g_start);
      END IF;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 6 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_so_backorder',
         g_start);

      --Procedure Call

      load_so_backorder (l_retcode);

      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 6 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_so_backorder '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);

         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 6 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_so_backorder',
            g_start);
      END IF;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 7 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit',
         g_start);

      --Procedure Call

      load_inv_intransit (l_retcode);

      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 7 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);

         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 7 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit',
            g_start);
      END IF;



      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 8 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_cyclecount',
         g_start);

      --Procedure Call

      load_inv_cyclecount (l_retcode);

      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 8 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_cyclecount '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);

         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 8 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_cyclecount',
            g_start);
      END IF;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 9 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_receipt',
         g_start);

      --Procedure Call

      load_inv_receipt (l_retcode);

      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 9 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_receipt '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);

         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 9 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_receipt',
            g_start);
      END IF;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 10 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_so_sales',
         g_start);

      --Procedure Call

      load_so_sales (l_retcode);

      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 10 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_so_sales '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);

         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 10 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_so_sales',
            g_start);
      END IF;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'INV SNAPSHOT',
         'Process 11 : Start XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_snapshot',
         g_start);

      --Procedure Call

      load_inv_snapshot (l_retcode);

      IF l_retcode = 2
      THEN
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
               'Process 11 : Error XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_snapshot '
            || SUBSTR (g_error_msg, 1, 350),
            g_start);

         GOTO LastStep;
      ELSE
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process 11 : End XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_snapshot',
            g_start);

         apps.xxwc_common_tunning_helpers.elapsed_time (
            'INV SNAPSHOT',
            'Process Main : End XXWC_OBIEE_ETL_PRE_PROCESS.load_snapshot_main',
            g_start);
      END IF;


     <<LastStep>>
      drop_table ('XXWC', 'OBIEE_ETL_INV_ITEM##');
      drop_table ('XXWC', 'OBIEE_ETL_PO_OPEN##');
      drop_table ('XXWC', 'OBIEE_ETL_INV_ONHAND##');
      drop_table ('XXWC', 'OBIEE_ETL_INV_VNDRCONS##');
      drop_table ('XXWC', 'OBIEE_ETL_INV_CATEGORY##');
      drop_table ('XXWC', 'OBIEE_ETL_SO_BACKORDER##');
      drop_table ('XXWC', 'OBIEE_ETL_INV_INTRA1##');
      drop_table ('XXWC', 'OBIEE_ETL_INV_INTRA2##');
      drop_table ('XXWC', 'OBIEE_ETL_INV_INTRA##');
      drop_table ('XXWC', 'OBIEE_ETL_INV_COUNT##');
      drop_table ('XXWC', 'OBIEE_ETL_INV_RECEIPT##');
      drop_table ('XXWC', 'OBIEE_ETL_SO_SALES##');

      IF l_retcode = 2
      THEN
         retcode := 2;
      END IF;
   END load_snapshot_main;

   /*---------------------------------------
   Procedure to load item list with organization
   ------------------------------------------*/

   PROCEDURE load_inv_item (p_retcode OUT NUMBER)
   /*************************************************************************
     $Header XXWC_OBIEE_ETL_PRE_PROCESS.Load_inv_item $

     PURPOSE: Procedure to load item list with organization

     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    15-May-2014  Manjula Chellappan    Initial Version

   **************************************************************************/
   IS
      l_drop_table       VARCHAR2 (400) := 'DROP TABLE xxwc.OBIEE_ETL_INV_ITEM##';

      l_create_table     VARCHAR2 (4000)
         := ' CREATE TABLE  xxwc.OBIEE_ETL_INV_ITEM## AS
     SELECT /*+ Parallel (8) */ inventory_item_id, organization_id FROM mtl_system_items_b';

      l_create_index     VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_INV_ITEM##_n1 
                                on xxwc.OBIEE_ETL_INV_ITEM## (inventory_item_id,organization_id)';

      l_error_msg        VARCHAR2 (4000)
                            := ' Error Loading xxwc.OBIEE_ETL_INV_ITEM## : ';
      l_success_msg      VARCHAR2 (400)
                            := ' Loading xxwc.OBIEE_ETL_INV_ITEM## Completed ';

      l_drop_msg         VARCHAR2 (400)
                            := ' xxwc.OBIEE_ETL_INV_ITEM## dropped ';

      l_Procedure_name   VARCHAR2 (100)
                            := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_INV_ITEM';


      l_table_name       VARCHAR2 (100) := 'OBIEE_ETL_INV_ITEM##';

      l_table_count      NUMBER := 0;

      l_sec              VARCHAR2 (200);
   BEGIN
      BEGIN
         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;

      l_sec :=
         'Process 1 : Creating Table at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_item ';

      EXECUTE IMMEDIATE l_create_table;

      l_sec :=
         'Process 1 : Creating Index at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_item ';

      EXECUTE IMMEDIATE l_create_index;

      COMMIT;


      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || CHR (10) || SQLERRM;
         g_error_msg := g_error_msg || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


         send_mail (l_procedure_name,
                    l_sec,
                    NULL,
                    l_error_msg,
                    p_retcode);
   END;

   /*---------------------------------------
   Procedure to load Open PO Qty
   ------------------------------------------*/

   PROCEDURE load_po_open (p_retcode OUT NUMBER)
   /*************************************************************************
     $Header XXWC_OBIEE_ETL_PRE_PROCESS.load_po_open $

     PURPOSE: Procedure to load Open PO Qty

     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    15-May-2014  Manjula Chellappan    Initial Version

   **************************************************************************/
   IS
      l_drop_table       VARCHAR2 (400) := 'DROP TABLE xxwc.OBIEE_ETL_PO_OPEN##';

      l_create_table     VARCHAR2 (4000)
         := 'CREATE TABLE  xxwc.OBIEE_ETL_PO_OPEN## AS
      SELECT /*+ Parallel (8) */ pll.item_id inventory_item_id,
              plla.ship_to_organization_id organization_id ,
                SUM (
                     plla.quantity
                   - NVL (plla.quantity_received, 0)
                   - NVL (plla.quantity_cancelled, 0)) open_po_qty 
           FROM po_lines_all pll, po_line_locations_all plla
          WHERE     NVL (pll.cancel_flag, ''N'') = ''N''
                AND NVL (pll.closed_flag, ''N'') = ''N''
                AND pll.po_line_id = plla.po_line_id
                AND pll.po_header_id = plla.po_header_id
                AND NVL (plla.cancel_flag, ''N'') = ''N''
                AND NVL (plla.closed_flag, ''N'') = ''N''
                AND pll.item_id IS NOT NULL
                AND plla.ship_to_organization_id IS NOT NULL
       GROUP BY pll.item_id, plla.ship_to_organization_id';

      l_create_index     VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_PO_OPEN##_n1 
                                on xxwc.OBIEE_ETL_PO_OPEN## (inventory_item_id,organization_id)';

      l_error_msg        VARCHAR2 (4000)
                            := ' Error Loading xxwc.OBIEE_ETL_PO_OPEN## : ';
      l_success_msg      VARCHAR2 (400)
                            := ' Loading xxwc.OBIEE_ETL_PO_OPEN## Completed ';

      l_drop_msg         VARCHAR2 (400) := ' xxwc.OBIEE_ETL_PO_OPEN## dropped ';

      l_Procedure_name   VARCHAR2 (100)
                            := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_PO_OPEN';

      l_table_name       VARCHAR2 (100) := 'OBIEE_ETL_PO_OPEN##';

      l_table_count      NUMBER := 0;
      l_sec              VARCHAR2 (200);
   BEGIN
      BEGIN
         l_sec :=
            'Process 1 : checking Table Exists at XXWC_OBIEE_ETL_PRE_PROCESS.load_po_open ';

         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;


      l_sec :=
         'Process 1 : Creating Table at XXWC_OBIEE_ETL_PRE_PROCESS.load_po_open ';

      EXECUTE IMMEDIATE l_create_table;

      l_sec :=
         'Process 1 : Creating Index at XXWC_OBIEE_ETL_PRE_PROCESS.load_po_open ';

      EXECUTE IMMEDIATE l_create_index;

      COMMIT;
      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || CHR (10) || SQLERRM;
         g_error_msg := g_error_msg || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

         send_mail (l_procedure_name,
                    l_sec,
                    NULL,
                    l_error_msg,
                    p_retcode);
   END;



   /*---------------------------------------
   Procedure to load Inventory Onhand
   ------------------------------------------*/

   PROCEDURE load_inv_onhand (p_retcode OUT NUMBER)
   /*************************************************************************
      $Header XXWC_OBIEE_ETL_PRE_PROCESS.Load_inv_onhand $

      PURPOSE: Procedure to load Inventory Onhand

      REVISIONS:
      Ver        Date         Author                Description
      -----  -----------  ------------------    ----------------
      1.0    15-May-2014  Manjula Chellappan    Initial Version

    **************************************************************************/
   IS
      l_drop_table       VARCHAR2 (400)
                            := 'DROP TABLE xxwc.OBIEE_ETL_INV_ONHAND##';

      l_create_table     VARCHAR2 (4000)
         := 'CREATE TABLE xxwc.OBIEE_ETL_INV_ONHAND## AS
     SELECT /*+ Append */ inventory_item_id,
             a.organization_id,
             NVL(b.secondary_inventory_name,''X'') subinventory,
             SUM (primary_transaction_quantity) total_qoh ,
             SUM (
                DECODE (b.reservable_type, 1, primary_transaction_quantity, 0))
                total_rqoh ,
             SUM (primary_transaction_quantity) total_atr
        FROM mtl_onhand_quantities_detail a, mtl_secondary_inventories b
       WHERE     1 = 1
             AND a.subinventory_code = b.secondary_inventory_name
             AND a.organization_id = b.organization_id
    GROUP BY inventory_item_id, a.organization_id, b.secondary_inventory_name';

      l_error_msg        VARCHAR2 (4000)
                            := ' Error Loading xxwc.OBIEE_ETL_INV_ONHAND## : ';
      l_success_msg      VARCHAR2 (400)
         := ' Loading xxwc.OBIEE_ETL_INV_ONHAND## Completed ';

      l_drop_msg         VARCHAR2 (400)
                            := ' xxwc.OBIEE_ETL_INV_ONHAND## dropped ';

      l_Procedure_name   VARCHAR2 (100)
                            := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_INV_ONHAND';

      l_table_name       VARCHAR2 (100) := 'OBIEE_ETL_INV_ONHAND##';

      l_table_count      NUMBER := 0;

      l_create_index     VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_INV_ONHAND##_N1
                                on xxwc.OBIEE_ETL_INV_ONHAND## (inventory_item_id,organization_id,subinventory)';
      l_sec              VARCHAR2 (200);
   BEGIN
      BEGIN
         l_sec :=
            'Process 1 : checking table exist at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_onhand ';

         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;

      l_sec :=
         'Process 1 : Creating table  at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_onhand ';

      EXECUTE IMMEDIATE l_create_table;

      l_sec :=
         'Process 1 : Creating Index  at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_onhand ';

      EXECUTE IMMEDIATE l_create_index;


      COMMIT;
      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || CHR (10) || SQLERRM;
         g_error_msg := g_error_msg || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

         send_mail (l_procedure_name,
                    l_sec,
                    NULL,
                    l_error_msg,
                    p_retcode);
   END;



   /*---------------------------------------
   Procedure to load Vendor Consigned Qty
   ------------------------------------------*/

   PROCEDURE load_inv_vndrcons (p_retcode OUT NUMBER)
   /*************************************************************************
      $Header XXWC_OBIEE_ETL_PRE_PROCESS.Load_inv_vndrcons $

      PURPOSE: Procedure to load Vendor Consigned Quantity

      REVISIONS:
      Ver        Date         Author                Description
      -----  -----------  ------------------    ----------------
      1.0    15-May-2014  Manjula Chellappan    Initial Version

    **************************************************************************/
   IS
      l_drop_table       VARCHAR2 (400)
                            := 'DROP TABLE xxwc.OBIEE_ETL_INV_VNDRCONS##';

      l_create_table     VARCHAR2 (4000)
         := 'CREATE TABLE  xxwc.OBIEE_ETL_INV_VNDRCONS## AS
       SELECT inventory_item_id,
                organization_id,
                SUM (primary_transaction_quantity) vendor_consigned_qty
           FROM mtl_onhand_quantities_detail
          WHERE 1 = 1 AND NVL (is_consigned, 2) != 2
       GROUP BY inventory_item_id, organization_id';

      l_error_msg        VARCHAR2 (4000)
         := ' Error Loading xxwc.OBIEE_ETL_INV_VNDRCONS## : ';

      l_success_msg      VARCHAR2 (4000)
         := ' Loading xxwc.OBIEE_ETL_INV_VNDRCONS## Completed ';

      l_drop_msg         VARCHAR2 (400)
                            := ' xxwc.OBIEE_ETL_INV_VNDRCONS## dropped ';

      l_Procedure_name   VARCHAR2 (100)
                            := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_INV_VNDRCONS';

      l_table_name       VARCHAR2 (100) := 'OBIEE_ETL_INV_VNDRCONS##';

      l_table_count      NUMBER := 0;

      l_create_index     VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_INV_VNDRCONS##_N
                                on xxwc.OBIEE_ETL_INV_VNDRCONS## (inventory_item_id,organization_id)';

      l_sec              VARCHAR2 (200);
   BEGIN
      BEGIN
         l_sec :=
            'Process 4 : Checking table exist at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_vndrcons ';

         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;


      l_sec :=
         'Process 4 : Creating table  at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_vndrcons ';

      EXECUTE IMMEDIATE l_create_table;

      l_sec :=
         'Process 4 : Creating index  at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_vndrcons ';

      EXECUTE IMMEDIATE l_create_index;

      COMMIT;

      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || CHR (10) || SQLERRM;
         g_error_msg := g_error_msg || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));



         send_mail (l_procedure_name,
                    l_sec,
                    NULL,
                    l_error_msg,
                    p_retcode);
   END;

   /*---------------------------------------
   Procedure to load sales Velocity category
   ------------------------------------------*/

   PROCEDURE load_inv_category (p_retcode OUT NUMBER)
   /*************************************************************************
      $Header XXWC_OBIEE_ETL_PRE_PROCESS.Load_inv_category $

      PURPOSE: Procedure to load Inventory Category

      REVISIONS:
      Ver        Date         Author                Description
      -----  -----------  ------------------    ----------------
      1.0    15-May-2014  Manjula Chellappan    Initial Version

    **************************************************************************/
   IS
      l_drop_table       VARCHAR2 (400)
                            := 'DROP TABLE xxwc.OBIEE_ETL_INV_CATEGORY##';

      l_create_table     VARCHAR2 (4000)
         := 'CREATE TABLE  xxwc.OBIEE_ETL_INV_CATEGORY## AS
    SELECT /*+ Parallel (8) */ a.inventory_item_id,
            a.organization_id,
            a.category_set_id,
            c.category_set_name category_set,
            a.category_id,
            b.segment1,
            b.segment2,
            b.segment3,
            b.segment4,
               b.segment1
            || DECODE (b.segment2, NULL, NULL, ''.'')
            || b.segment2
            || DECODE (b.segment3, NULL, NULL, ''.'')
            || b.segment3
            || DECODE (b.segment4, NULL, NULL, ''.'')
            || b.segment4
               Concat_category
       FROM mtl_item_categories a, mtl_Categories b, mtl_category_sets c
      WHERE     a.category_id = b.category_id
            AND a.category_set_id = c.category_set_id';

      l_error_msg        VARCHAR2 (4000)
         := ' Error Loading xxwc.OBIEE_ETL_INV_CATEGORY## : ';

      l_success_msg      VARCHAR2 (4000)
         := ' Loading xxwc.OBIEE_ETL_INV_CATEGORY## Completed ';

      l_drop_msg         VARCHAR2 (400)
                            := ' xxwc.OBIEE_ETL_INV_CATEGORY## dropped ';

      l_Procedure_name   VARCHAR2 (100)
                            := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_INV_CATEGORY';

      l_table_name       VARCHAR2 (100) := 'OBIEE_ETL_INV_CATEGORY##';

      l_table_count      NUMBER := 0;

      l_create_index     VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_INV_CATEGORY##_N1
                                on xxwc.OBIEE_ETL_INV_CATEGORY## (inventory_item_id,organization_id,category_set_id,category_id)';

      l_sec              VARCHAR2 (200);
   BEGIN
      BEGIN
         l_sec :=
            'Process 5 : Checking table exist at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_category ';

         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;

      l_sec :=
         'Process 5 : creating table at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_category ';

      EXECUTE IMMEDIATE l_create_table;

      l_sec :=
         'Process 5 : creating index at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_category ';

      EXECUTE IMMEDIATE l_create_index;


      COMMIT;

      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || CHR (10) || SQLERRM;
         g_error_msg := g_error_msg || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));



         send_mail (l_procedure_name,
                    l_sec,
                    NULL,
                    l_error_msg,
                    p_retcode);
   END;

   /*---------------------------------------
   Procedure to load Backorder Qty
   ------------------------------------------*/


   PROCEDURE load_so_backorder (p_retcode OUT NUMBER)
   /*************************************************************************
      $Header XXWC_OBIEE_ETL_PRE_PROCESS.Load_so_backorder $

      PURPOSE: Procedure to load SO Bakorder quantity

      REVISIONS:
      Ver        Date         Author                Description
      -----  -----------  ------------------    ----------------
      1.0    15-May-2014  Manjula Chellappan    Initial Version

    **************************************************************************/
   IS
      l_drop_table       VARCHAR2 (400)
                            := 'DROP TABLE xxwc.OBIEE_ETL_SO_BACKORDER##';

      l_create_table     VARCHAR2 (4000)
         := 'CREATE TABLE xxwc.OBIEE_ETL_SO_BACKORDER## AS
     SELECT oola.inventory_item_id,
             oola.ship_from_org_id organization_id,
             SUM (
                  oola.ordered_quantity
                - NVL (oola.shipped_quantity, 0)
                - NVL (oola.cancelled_quantity, 0))
                backorder_qty
        FROM oe_order_lines_all oola, wsh_delivery_details wdd
       WHERE     1 = 1
             AND oola.line_category_code = ''ORDER''
             AND NVL (oola.cancelled_flag, ''N'') = ''N''
             AND NVL (oola.booked_flag, ''N'') = ''Y''
             AND oola.line_id = wdd.source_line_id
             AND wdd.source_code = ''OE''
             AND wdd.released_status = ''B''
    GROUP BY oola.inventory_item_id, oola.ship_from_org_id';

      l_error_msg        VARCHAR2 (4000)
         := ' Error Loading xxwc.OBIEE_ETL_SO_BACKORDER## : ';

      l_success_msg      VARCHAR2 (4000)
         := ' Loading xxwc.OBIEE_ETL_SO_BACKORDER## Completed ';

      l_drop_msg         VARCHAR2 (400)
                            := ' xxwc.OBIEE_ETL_SO_BACKORDER## dropped ';

      l_Procedure_name   VARCHAR2 (100)
                            := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_SO_BACKORDER';

      l_table_name       VARCHAR2 (100) := 'OBIEE_ETL_SO_BACKORDER##';

      l_table_count      NUMBER := 0;

      l_create_index     VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_SO_BACKORDER##_N1
                                on xxwc.OBIEE_ETL_SO_BACKORDER## (inventory_item_id,organization_id)';

      l_sec              VARCHAR2 (200);
   BEGIN
      BEGIN
         l_sec :=
            'Process 6 : Checking table exist at XXWC_OBIEE_ETL_PRE_PROCESS.load_so_backorder';

         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;

      l_sec :=
         'Process 6 : Creating table  at XXWC_OBIEE_ETL_PRE_PROCESS.load_so_backorder';

      EXECUTE IMMEDIATE l_create_table;

      l_sec :=
         'Process 6 : Creating Index  at XXWC_OBIEE_ETL_PRE_PROCESS.load_so_backorder';

      EXECUTE IMMEDIATE l_create_index;


      COMMIT;

      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || CHR (10) || SQLERRM;
         g_error_msg := g_error_msg || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


         send_mail (l_procedure_name,
                    l_sec,
                    NULL,
                    l_error_msg,
                    p_retcode);
   END;

   /*---------------------------------------
   Procedure to load Intransit Qty
   ------------------------------------------*/

   PROCEDURE load_inv_intransit (p_retcode OUT NUMBER)
   /*************************************************************************
       $Header XXWC_OBIEE_ETL_PRE_PROCESS.Load_inv_intransit $

       PURPOSE: Procedure to load Inventory Intransit quantity

       REVISIONS:
       Ver        Date         Author                Description
       -----  -----------  ------------------    ----------------
       1.0    15-May-2014  Manjula Chellappan    Initial Version

     **************************************************************************/
   IS
      CURSOR cur_org
      IS
           SELECT DISTINCT organization_id
             FROM mtl_system_items_b
         ORDER BY organization_id;

      l_organization_id   NUMBER;

      l_drop_table1       VARCHAR2 (400)
                             := 'DROP TABLE xxwc.OBIEE_ETL_INV_INTRA1##';

      l_drop_table2       VARCHAR2 (4000)
                             := 'DROP TABLE xxwc.OBIEE_ETL_INV_INTRA2##';

      l_drop_table        VARCHAR2 (4000)
                             := 'DROP TABLE xxwc.OBIEE_ETL_INV_INTRA##';


      l_create_table1     VARCHAR2 (4000)
         := 'CREATE TABLE xxwc.OBIEE_ETL_INV_INTRA1## AS
            SELECT  mp_from.organization_code ship_from_branch_num,
                 mp_from.organization_id ship_from_org_id,
                 mp_to.organization_code ship_to_branch_num,
                 mp_to.organization_id ship_to_org_id,
                 rsl.shipment_line_status_code line_status,
                 rsh.creation_date ordered_date,
                 rsh.shipped_date,
                 msib.segment1 sku_number,
                 msib.inventory_item_id,
                 rsl.quantity_shipped ordered_qty,
                 rsl.quantity_shipped shipped_qty,
                 0 cancelled_qty,
                 rsl.quantity_shipped requested_qty,
                 NVL (rsl.quantity_received, 0) received_qty,
                 rsh.shipment_num po_ref_num
            FROM rcv_shipment_headers rsh,
                 rcv_shipment_lines rsl,
                 mtl_parameters mp_from,
                 org_organization_definitions ood_from,
                 mtl_parameters mp_to,
                 org_organization_definitions ood_to,
                 mtl_system_items_b msib,
                 hr_operating_units hou
           WHERE     rsh.receipt_source_code = ''INVENTORY''
                 AND rsh.shipment_header_id = rsl.shipment_header_id
                 AND rsl.source_document_code = ''INVENTORY''
                 AND rsl.from_organization_id = mp_from.organization_id
                 AND mp_from.organization_id = ood_from.organization_id
                 AND rsl.to_organization_id = mp_to.organization_id
                 AND mp_to.organization_id = ood_to.organization_id
                 AND rsl.item_id = msib.inventory_item_id
                 AND rsl.to_organization_id = msib.organization_id
                 AND ood_from.operating_unit = hou.organization_id';
      --AND rsl.to_organization_id = :l_organization_id';

      l_create_table2     VARCHAR2 (4000)
         := 'CREATE TABLE xxwc.OBIEE_ETL_INV_INTRA2## AS
          SELECT /*+ Parallel (8) */  mp_from.organization_code ship_from_branch_num,
                 mp_from.organization_id ship_from_org_id,
                 mp_to.organization_code ship_to_branch_num,
                 mp_to.organization_id ship_to_org_id,
                 oola.flow_status_code line_status,
                 ooha.creation_date order_date,
                 oola.actual_shipment_date shipped_date,
                 msib.segment1 sku_number,
                 msib.inventory_item_id,
                 NVL (oola.ordered_quantity, 0) ordered_qty,
                 NVL (oola.shipped_quantity, 0) shipped_qty,
                 NVL (oola.cancelled_quantity, 0) cancelled_qty,
                 req_ln.quantity requested_qty,
                 NVL (req_ln.quantity_received, 0) received_qty,
                 req_hdr.segment1 po_ref_num
            FROM po_requisition_headers_all req_hdr,
                 po_requisition_lines_all req_ln,
                 oe_order_headers_all ooha,
                 oe_order_sources oos,
                 oe_order_lines_all oola,
                 mtl_parameters mp_from,
                 org_organization_definitions ood_from,
                 mtl_parameters mp_to,
                 org_organization_definitions ood_to,
                 mtl_system_items_b msib,
                 hr_operating_units hou
           WHERE     req_hdr.type_lookup_code = ''INTERNAL''
                 AND req_hdr.requisition_header_id =
                        req_ln.requisition_header_id
                 AND req_hdr.requisition_header_id = ooha.source_document_id(+)
                 AND ooha.order_source_id = oos.order_source_id(+)
                 AND oos.NAME(+) = ''Internal''
                 AND req_ln.requisition_header_id = oola.source_document_id(+)
                 AND req_ln.requisition_line_id =
                        oola.source_document_line_id(+)
                 AND NVL (oola.cancelled_flag(+), ''N'') = ''N''
                 AND NVL (oola.booked_flag(+), ''N'') = ''Y''
                 AND req_ln.source_organization_id = mp_from.organization_id
                 AND mp_from.organization_id = ood_from.organization_id
                 AND req_ln.destination_organization_id = mp_to.organization_id
                 AND mp_to.organization_id = ood_to.organization_id
                 AND req_ln.item_id = msib.inventory_item_id
                 AND req_ln.destination_organization_id = msib.organization_id
                 AND ood_from.operating_unit = hou.organization_id';

      --AND mp_to.organization_id = :l_organization_id';

      l_create_table      VARCHAR2 (4000)
         := 'CREATE TABLE xxwc.OBIEE_ETL_INV_INTRA## AS 
           SELECT x.inventory_item_id,
                x.ship_to_org_id organization_id ,
                SUM (x.ordered_qty - x.shipped_qty - x.cancelled_qty) intransit_qty
               FROM (SELECT /*+ Parallel (8) */
                * FROM xxwc.OBIEE_ETL_INV_INTRA1##
            UNION
             SELECT * FROM xxwc.OBIEE_ETL_INV_INTRA2##) x
                GROUP BY inventory_item_id, ship_to_org_id';


      l_error1_msg        VARCHAR2 (4000)
         := 'Error Loading xxwc.OBIEE_ETL_INV_INTRA1## : ';
      l_error2_msg        VARCHAR2 (4000)
         := 'Error Loading xxwc.OBIEE_ETL_INV_INTRA2## : ';
      l_error_msg         VARCHAR2 (4000)
                             := 'Error Loading xxwc.OBIEE_ETL_INV_INTRA## : ';

      l_success1_msg      VARCHAR2 (4000)
         := ' Loading xxwc.OBIEE_ETL_INV_INTRA1## Completed ';

      l_success2_msg      VARCHAR2 (4000)
         := ' Loading xxwc.OBIEE_ETL_INV_INTRA2## Completed ';

      l_success_msg       VARCHAR2 (4000)
         := ' Loading xxwc.OBIEE_ETL_INV_INTRA## Completed ';

      l_drop1_msg         VARCHAR2 (400)
                             := ' xxwc.OBIEE_ETL_INV_INTRA1## dropped ';

      l_drop2_msg         VARCHAR2 (400)
                             := ' xxwc.OBIEE_ETL_INV_INTRA2## dropped ';

      l_drop_msg          VARCHAR2 (400)
                             := ' xxwc.OBIEE_ETL_INV_INTRA## dropped ';

      l_Procedure_name    VARCHAR2 (100)
         := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_INV_INTRANSIT';


      l_table1_name       VARCHAR2 (100) := 'OBIEE_ETL_INV_INTRA1##';

      l_table1_count      NUMBER := 0;

      l_create_index1     VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_INV_INTRA1##_N1
                                on xxwc.OBIEE_ETL_INV_INTRA1## (inventory_item_id,ship_to_org_id)';

      l_table2_name       VARCHAR2 (100) := 'OBIEE_ETL_INV_INTRA2##';

      l_table2_count      NUMBER := 0;

      l_create_index2     VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_INV_INTRA2##_N1
                                on xxwc.OBIEE_ETL_INV_INTRA2## (inventory_item_id,ship_to_org_id)';

      l_table_name        VARCHAR2 (100) := 'OBIEE_ETL_INV_INTRA##';

      l_table_count       NUMBER := 0;

      l_create_index      VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_INV_INTRA##_N1
                                on xxwc.OBIEE_ETL_INV_INTRA## (inventory_item_id,organization_id)';
      l_sec1              VARCHAR2 (200);

      l_sec2              VARCHAR2 (200);

      l_sec               VARCHAR2 (200);
   BEGIN
      BEGIN
         BEGIN
            l_sec1 :=
               'Process 7a : Checking table exist at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit ';

            SELECT COUNT (*)
              INTO l_table1_count
              FROM all_tables
             WHERE table_name = l_table1_name AND owner = g_owner;

            IF l_table1_count = 1
            THEN
               EXECUTE IMMEDIATE l_drop_table1;

               DBMS_OUTPUT.put_line (
                     l_drop1_msg
                  || ' at '
                  || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
               fnd_file.put_line (
                  fnd_file.LOG,
                     l_drop1_msg
                  || ' at '
                  || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            END IF;
         END;

         l_sec1 :=
            'Process 7a : creating Table at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit ';

         EXECUTE IMMEDIATE l_create_table1;

         l_sec1 :=
            'Process 7a : creating Index at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit ';

         EXECUTE IMMEDIATE l_create_index1;

         COMMIT;

         DBMS_OUTPUT.put_line (
               l_success1_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_success1_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error1_msg := l_error1_msg || CHR (10) || SQLERRM;
            g_error_msg := g_error_msg || CHR (10) || l_error1_msg;

            DBMS_OUTPUT.put_line (
                  l_error1_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_error1_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


            send_mail (l_procedure_name,
                       l_sec,
                       NULL,
                       l_error_msg,
                       p_retcode);
      END;



      BEGIN
         BEGIN
            l_sec2 :=
               'Process 7b : Checking table exist at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit ';

            SELECT COUNT (*)
              INTO l_table2_count
              FROM all_tables
             WHERE table_name = l_table2_name AND owner = g_owner;

            IF l_table2_count = 1
            THEN
               EXECUTE IMMEDIATE l_drop_table2;

               DBMS_OUTPUT.put_line (
                     l_drop2_msg
                  || ' at '
                  || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
               fnd_file.put_line (
                  fnd_file.LOG,
                     l_drop2_msg
                  || ' at '
                  || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            END IF;
         END;

         l_sec2 :=
            'Process 7b : Creating table at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit ';

         EXECUTE IMMEDIATE l_create_table2;

         l_sec2 :=
            'Process 7b : Creating index at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit ';

         EXECUTE IMMEDIATE l_create_index2;

         COMMIT;

         DBMS_OUTPUT.put_line (
               l_success2_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_success2_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error2_msg := l_error2_msg || CHR (10) || SQLERRM;
            g_error_msg := g_error_msg || CHR (10) || l_error2_msg;

            DBMS_OUTPUT.put_line (
                  l_error2_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_error2_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


            send_mail (l_procedure_name,
                       l_sec,
                       NULL,
                       l_error_msg,
                       p_retcode);
      END;

      BEGIN
         BEGIN
            l_sec :=
               'Process 7 : Checking Table Exist at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit ';

            SELECT COUNT (*)
              INTO l_table_count
              FROM all_tables
             WHERE table_name = l_table_name AND owner = g_owner;

            IF l_table_count = 1
            THEN
               EXECUTE IMMEDIATE l_drop_table;

               DBMS_OUTPUT.put_line (
                     l_drop_msg
                  || ' at '
                  || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
               fnd_file.put_line (
                  fnd_file.LOG,
                     l_drop_msg
                  || ' at '
                  || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            END IF;
         END;

         l_sec :=
            'Process 7 : Creating table at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit ';

         EXECUTE IMMEDIATE l_create_table;

         l_sec :=
            'Process 7 : Creating index at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_intransit ';

         EXECUTE IMMEDIATE l_create_index;


         COMMIT;

         DBMS_OUTPUT.put_line (
               l_success_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_success_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_msg := l_error_msg || CHR (10) || SQLERRM;

            g_error_msg := g_error_msg || CHR (10) || l_error_msg;

            DBMS_OUTPUT.put_line (
                  l_error_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_error_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

            send_mail (l_procedure_name,
                       l_sec,
                       NULL,
                       l_error_msg,
                       p_retcode);
      END;
   END;

   /*---------------------------------------
   Procedure to load Cycle count
   ------------------------------------------*/

   PROCEDURE load_inv_cyclecount (p_retcode OUT NUMBER)
   /*************************************************************************
       $Header XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_cyclecount $

       PURPOSE: Procedure to load Inventory Cycle count

       REVISIONS:
       Ver        Date         Author                Description
       -----  -----------  ------------------    ----------------
       1.0    15-May-2014  Manjula Chellappan    Initial Version

     **************************************************************************/
   IS
      l_drop_table       VARCHAR2 (400)
                            := 'DROP TABLE xxwc.OBIEE_ETL_INV_COUNT##';

      l_create_table     VARCHAR2 (4000)
         := 'CREATE TABLE  xxwc.OBIEE_ETL_INV_COUNT## AS 
     SELECT inventory_item_id,
       organization_id,
       mcce.count_quantity_current quantity,
       (count_date_current) counted_date,
              (SELECT /*+ Parallel (8) */ user_name
          FROM fnd_user
         WHERE employee_id = counted_by_employee_id_current) counted_by
  FROM mtl_cycle_count_entries mcce
 WHERE     1 = 1
       AND mcce.cycle_count_entry_id =
              (SELECT /*+ Parallel (8) */ MAX (mcce2.cycle_count_entry_id)
                 FROM mtl_cycle_count_entries mcce2
                WHERE     mcce2.inventory_item_id = mcce.inventory_item_id
                      AND mcce2.organization_id = mcce.organization_id)';

      l_error_msg        VARCHAR2 (4000)
                            := 'Error Loading xxwc.OBIEE_ETL_INV_COUNT## : ';

      l_success_msg      VARCHAR2 (4000)
                            := ' Loading xxwc.OBIEE_ETL_INV_COUNT## Completed ';

      l_drop_msg         VARCHAR2 (400)
                            := ' xxwc.OBIEE_ETL_INV_COUNT## dropped ';

      l_Procedure_name   VARCHAR2 (100)
         := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_INV_CYCLECOUNT';


      l_table_name       VARCHAR2 (100) := 'OBIEE_ETL_INV_COUNT##';

      l_table_count      NUMBER := 0;

      l_create_index     VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_INV_COUNT##_N1
                                on xxwc.OBIEE_ETL_INV_COUNT## (inventory_item_id,organization_id)';
      l_sec              VARCHAR2 (200);
   BEGIN
      BEGIN
         l_sec :=
            'Process 8 : Checking Table exist at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_cyclecount ';

         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;

      l_sec :=
         'Process 8 : creating Table at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_cyclecount ';

      EXECUTE IMMEDIATE l_create_table;

      l_sec :=
         'Process 8 : creating index at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_cyclecount ';

      EXECUTE IMMEDIATE l_create_index;


      COMMIT;

      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || CHR (10) || SQLERRM;

         g_error_msg := g_error_msg || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

         send_mail (l_procedure_name,
                    l_sec,
                    NULL,
                    l_error_msg,
                    p_retcode);
   END;

   /*---------------------------------------
   Procedure to load Reciepts
   ------------------------------------------*/

   PROCEDURE load_inv_receipt (p_retcode OUT NUMBER)
   /*************************************************************************
        $Header XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_receipt $

        PURPOSE: Procedure to load Inventory Receipts

        REVISIONS:
        Ver        Date         Author                Description
        -----  -----------  ------------------    ----------------
        1.0    15-May-2014  Manjula Chellappan    Initial Version

      **************************************************************************/
   IS
      l_drop_table       VARCHAR2 (400)
                            := 'DROP TABLE xxwc.OBIEE_ETL_INV_RECEIPT##';

      l_create_table     VARCHAR2 (4000)
         := 'CREATE TABLE  xxwc.OBIEE_ETL_INV_RECEIPT## AS
      (  SELECT rsl.item_id inventory_item_id,
                rsl.to_organization_id organization_id ,
                rt.source_document_code source_code ,
                DECODE (rt.source_document_code,
                        ''REQ'', ''Intransit'',
                        ''INVENTORY'', ''Intransit'',
                        ''PO'', ''PO'',
                        ''RMA'', ''RMA'')
                   receipt_type,
                MAX (rt.transaction_date) receipt_date
           FROM rcv_transactions rt, rcv_shipment_lines rsl
          WHERE     rt.transaction_type = ''RECEIVE''
                AND rt.shipment_line_id = rsl.shipment_line_id
       GROUP BY rsl.item_id, rsl.to_organization_id, rt.source_document_code)';

      l_error_msg        VARCHAR2 (4000)
                            := 'Error Loading xxwc.OBIEE_ETL_INV_RECEIPT## : ';

      l_success_msg      VARCHAR2 (4000)
         := ' Loading xxwc.OBIEE_ETL_INV_RECEIPT## Completed ';

      l_drop_msg         VARCHAR2 (400)
                            := ' xxwc.OBIEE_ETL_INV_RECEIPT## dropped ';

      l_Procedure_name   VARCHAR2 (100)
                            := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_INV_RECEIPT';

      l_table_name       VARCHAR2 (100) := 'OBIEE_ETL_INV_RECEIPT##';

      l_table_count      NUMBER := 0;

      l_create_index     VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_INV_RECEIPT##_N1
                                on xxwc.OBIEE_ETL_INV_RECEIPT## (inventory_item_id,organization_id)';
      l_sec              VARCHAR2 (200);
   BEGIN
      BEGIN
         l_sec :=
            'Process 9 : Checking table exists at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_receipt ';

         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;

      l_sec :=
         'Process 9 : creating table at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_receipt ';


      EXECUTE IMMEDIATE l_create_table;

      l_sec :=
         'Process 9 : creating index at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_receipt ';

      EXECUTE IMMEDIATE l_create_index;


      COMMIT;

      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || CHR (10) || SQLERRM;
         g_error_msg := g_error_msg || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


         send_mail (l_procedure_name,
                    l_sec,
                    NULL,
                    l_error_msg,
                    p_retcode);
   END;

   /*---------------------------------------
   Procedure to load Sales
   ------------------------------------------*/
   PROCEDURE load_so_sales (p_retcode OUT NUMBER)
   /*************************************************************************
        $Header XXWC_OBIEE_ETL_PRE_PROCESS.load_so_sales $

        PURPOSE: Procedure to load Sales Order Details

        REVISIONS:
        Ver        Date         Author                Description
        -----  -----------  ------------------    ----------------
        1.0    15-May-2014  Manjula Chellappan    Initial Version

      **************************************************************************/
   IS
      CURSOR cur_org
      IS
           SELECT DISTINCT organization_id
             FROM mtl_system_items_b
         ORDER BY organization_id;

      l_organization_id   NUMBER;

      l_drop_table        VARCHAR2 (400)
                             := 'DROP TABLE xxwc.OBIEE_ETL_SO_SALES##';

      l_create_table      VARCHAR2 (4000)
         := 'CREATE TABLE  xxwc.OBIEE_ETL_SO_SALES## AS 
          SELECT /*+ Parallel (8) */ inventory_item_id,
              ship_from_org_id organization_id,
              actual_shipment_date last_sales_date,
              order_Source_id,  ''N'' cancelled_flag
            FROM
              (SELECT /*+ Parallel (8) */ inventory_item_id,
                ship_from_org_id ,
                actual_shipment_date ,
                order_Source_id,
                cancelled_flag,
                ROW_NUMBER () OVER (PARTITION BY oola.inventory_item_id, oola.ship_from_org_id,order_Source_id
                ORDER BY oola.actual_shipment_date DESC) AS rn
              FROM oe_order_lines_all oola WHERE actual_shipment_date IS NOT NULL 
              ) a
            WHERE rn                  =1';

      l_error_msg         VARCHAR2 (4000)
                             := 'Error Loading xxwc.OBIEE_ETL_SO_SALES## : ';

      l_success_msg       VARCHAR2 (4000)
         := ' Loading xxwc.OBIEE_ETL_SO_SALES## Completed ';

      l_drop_msg          VARCHAR2 (400)
                             := ' xxwc.OBIEE_ETL_SO_SALES## dropped ';

      l_Procedure_name    VARCHAR2 (100)
                             := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_SO_SALES';

      l_table_name        VARCHAR2 (100) := 'OBIEE_ETL_SO_SALES##';

      l_table_count       NUMBER := 0;

      l_create_index      VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_SO_SALES##_N1
                                on xxwc.OBIEE_ETL_SO_SALES## (inventory_item_id,organization_id,order_source_id , cancelled_flag)';
      l_sec               VARCHAR2 (200);
   BEGIN
      BEGIN
         l_sec :=
            'Process 10 : Checking Table exist XXWC_OBIEE_ETL_PRE_PROCESS.load_so_sales ';

         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;

      l_sec :=
         'Process 10 : Creating Table XXWC_OBIEE_ETL_PRE_PROCESS.load_so_sales ';

      EXECUTE IMMEDIATE l_create_table;

      l_sec :=
         'Process 10 : Creating index XXWC_OBIEE_ETL_PRE_PROCESS.load_so_sales ';

      EXECUTE IMMEDIATE l_create_index;


      COMMIT;

      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || CHR (10) || SQLERRM;
         g_error_msg := g_error_msg || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

         send_mail (l_procedure_name,
                    l_sec,
                    NULL,
                    l_error_msg,
                    p_retcode);
   END;

   /*---------------------------------------
   Procedure to load inventory snaphot by consolidating the individul loads
   ------------------------------------------*/

   PROCEDURE load_inv_snapshot (p_retcode OUT NUMBER)
   /*************************************************************************
        $Header XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_snapshot $

        PURPOSE: Procedure to load inventory snaphot by consolidating the individul loads

        REVISIONS:
        Ver        Date         Author                Description
        -----  -----------  ------------------    ----------------
        1.0    15-May-2014  Manjula Chellappan    Initial Version

      **************************************************************************/
   IS
      CURSOR cur_org
      IS
           SELECT DISTINCT organization_id
             FROM mtl_system_items_b
         ORDER BY organization_id;

      l_organization_id   NUMBER;

      l_drop_table        VARCHAR2 (400)
                             := 'DROP TABLE xxwc.OBIEE_ETL_INV_SNAPSHOT_TBL';


      l_error_msg         VARCHAR2 (4000)
         := 'Error Loading xxwc.OBIEE_ETL_INV_SNAPSHOT_TBL ';

      l_success_msg       VARCHAR2 (4000)
         := ' Loading xxwc.OBIEE_ETL_INV_SNAPSHOT_TBL Completed ';

      l_drop_msg          VARCHAR2 (400)
                             := ' xxwc.OBIEE_ETL_INV_SNAPSHOT_TBL dropped ';

      l_Procedure_name    VARCHAR2 (100)
         := 'OBIEE_ETL_INV_SNAPSHOT_PKG.LOAD_INV_SNAPSHOT';

      l_table_name        VARCHAR2 (100) := 'OBIEE_ETL_INV_SNAPSHOT_TBL';

      l_table_count       NUMBER := 0;

      l_create_index      VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_INV_SNAPSHOT_TBL_N1
                                on xxwc.OBIEE_ETL_INV_SNAPSHOT_TBL (inventory_item_id,organization_id)';

      l_sec               VARCHAR2 (200);
   BEGIN
      BEGIN
         l_sec :=
            'Process 11 : Checking table exist at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_snapshot ';

         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;

      l_sec :=
         'Process 11 : Creating table at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_snapshot ';

      EXECUTE IMMEDIATE
         'CREATE TABLE xxwc.OBIEE_ETL_INV_SNAPSHOT_TBL AS 
   SELECT  a.inventory_item_id,
           a.organization_id,
           NVL((SELECT  open_po_qty
              FROM xxwc.OBIEE_ETL_PO_OPEN##
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id),0)
              open_po_qty,
           NVL((SELECT  vendor_consigned_qty
              FROM xxwc.OBIEE_ETL_INV_VNDRCONS##
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id),0)
              vendor_consigned_qty,
           NVL((SELECT  SUM (total_atr)
              FROM xxwc.OBIEE_ETL_INV_ONHAND## b, mtl_system_items_b c
             WHERE     b.inventory_item_id = c.inventory_item_id
                   AND b.organization_id = c.organization_id
                   AND b.inventory_item_id = a.inventory_item_id
                   AND b.organization_id = a.organization_id
                   AND c.reservable_type=1),0)
              available_qty,
           NVL((SELECT  SUM (total_qoh)
              FROM xxwc.OBIEE_ETL_INV_ONHAND##
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id
                   AND subinventory = ''Damage RTV''),0)
              defective_qty,
           NVL((SELECT  DECODE (ABS (SUM (total_qoh)),
                           SUM (total_qoh), 0,
                           SUM (total_qoh))
              FROM xxwc.OBIEE_ETL_INV_ONHAND##
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id),0)
              overship_qty,
           NVL((SELECT   SUM(total_qoh)
              FROM xxwc.OBIEE_ETL_INV_ONHAND## b,
                   (SELECT  *
                      FROM xxwc.OBIEE_ETL_INV_CATEGORY##
                     WHERE category_set = ''Sales Velocity'') c
             WHERE     1 = 1
                   AND b.inventory_item_id = a.inventory_item_id
                   AND b.organization_id = a.organization_id
                   AND b.inventory_item_id = c.inventory_item_id
                   AND b.organization_id = c.organization_id
                   AND NVL (c.segment1, ''X'') <> ''N''
                   AND b.subinventory = ''General''),0)
              stock_qty,
           NVL((SELECT  SUM(backorder_qty)
              FROM xxwc.OBIEE_ETL_SO_BACKORDER##
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id),0)
              svc_qty,
           NVL((SELECT  intransit_qty
              FROM xxwc.OBIEE_ETL_INV_INTRA##
             WHERE     inventory_item_id = a.inventory_item_id
                       AND organization_id = a.organization_id),0)
              transit_qty,
           NVL((SELECT  quantity
              FROM xxwc.OBIEE_ETL_INV_COUNT##
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id),0)
              last_count_qty,
           (SELECT  counted_date
              FROM xxwc.OBIEE_ETL_INV_COUNT##
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id)
              last_count_date,
           (SELECT  counted_by
              FROM xxwc.OBIEE_ETL_INV_COUNT##
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id)
              last_counted_by,
           (SELECT  MAX(receipt_date)
              FROM xxwc.OBIEE_ETL_INV_RECEIPT##
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id
                   AND receipt_type =''PO'')
              last_po_receipt_date,
           (SELECT  MAX (last_sales_date)
              FROM xxwc.OBIEE_ETL_SO_SALES## b, oe_order_sources c
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id
                   AND b.order_source_id = c.order_source_id AND c.name = ''Internal'')
             last_trans_out_date,
           (SELECT  MAX(receipt_date)
              FROM xxwc.OBIEE_ETL_INV_RECEIPT##
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id
                   AND receipt_type =''Intransit'')last_trans_in_date,
           (SELECT  MAX (last_sales_date)
              FROM xxwc.OBIEE_ETL_SO_SALES## b
             WHERE     inventory_item_id = a.inventory_item_id
                   AND organization_id = a.organization_id)
              last_sales_date
      FROM xxwc.OBIEE_ETL_INV_ITEM## a';

      l_sec :=
         'Process 11 : Creating Index at XXWC_OBIEE_ETL_PRE_PROCESS.load_inv_snapshot ';

      EXECUTE IMMEDIATE l_create_index;

      COMMIT;

      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || CHR (10) || SQLERRM;
         g_error_msg := g_error_msg || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

         send_mail (l_procedure_name,
                    l_sec,
                    NULL,
                    l_error_msg,
                    p_retcode);
   END;

   /*---------------------------------------
  Procedure to submit the program from UC4
  ------------------------------------------*/
   PROCEDURE submit_inv_snapshot (errbuf                     OUT VARCHAR2,
                                  retcode                    OUT NUMBER,
                                  p_user_name             IN     VARCHAR2,
                                  p_responsibility_name   IN     VARCHAR2)
   /*************************************************************************
        $Header XXWC_OBIEE_ETL_PRE_PROCESS.submit_inv_snapshot $

        PURPOSE: Procedure to submit the program from UC4

        REVISIONS:
        Ver        Date         Author                Description
        -----  -----------  ------------------    ----------------
        1.0    15-May-2014  Manjula Chellappan    Initial Version
        1.5   14-Oct-2014   Manjula Chellappan    TMS# 20141014-00253 -   Fix scheduling bugs in XXWC_OBIEE_ETL_PRE_PROCESS 

      **************************************************************************/
   IS
      l_package               VARCHAR2 (50)
                                 := 'OBIEE_ETL_INV_SNAPSHOT_PKG.SUBMIT_INV_SNAPSHOT';
      l_distribution_list     VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';

      l_req_id                NUMBER NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      v_supplier_id           NUMBER;
      v_rec_cnt               NUMBER := 0;
      v_interval              NUMBER := 30;                      -- In seconds
      v_max_time              NUMBER := 15000;                   -- In seconds
      l_message               VARCHAR2 (150);
      l_errormessage          VARCHAR2 (3000);
      pl_errorstatus          NUMBER;
      l_can_submit_request    BOOLEAN := TRUE;
      l_globalset             VARCHAR2 (100);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_sec                   VARCHAR2 (255);
      l_statement             VARCHAR2 (9000);
      l_user                  fnd_user.user_id%TYPE;
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_program               VARCHAR2 (30) := 'XXWC_OBIEE_ETL_INV_SNAPSHOT'; /* Concurrent Program short name */
      l_application           VARCHAR2 (30) := 'XXWC';
   BEGIN
      retcode := 0;



      /*---------------------------------------
      Derive User Id
      ------------------------------------------*/

      l_sec := 'Start Getting User Id ';

      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM applsys.fnd_user
          WHERE     user_name = UPPER (p_user_name)
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName - ' || p_user_name || ' not defined in Oracle';
            DBMS_OUTPUT.put_line (
                  l_err_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            DBMS_OUTPUT.put_line (
                  l_err_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            RAISE PROGRAM_ERROR;
      END;



      /*---------------------------------------
      Derive Responsibility Id and Responsibility Application Id
      ------------------------------------------*/

      l_sec := 'Start Getting Responsibility Id ';

      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM apps.fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility - '
               || p_responsibility_name
               || ' not defined in Oracle';
            DBMS_OUTPUT.put_line (
                  l_err_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for ResponsibilityName - '
               || p_responsibility_name;
            DBMS_OUTPUT.put_line (
                  l_err_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            RAISE PROGRAM_ERROR;
      END;



      /*---------------------------------------
      Apps Initialize
      ------------------------------------------*/

      l_sec := 'Start App Initialize ';


      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);

      l_sec := 'Start Submit concurrent Request ' || l_program;

 -- Modiffied by Manjula Chellappan  TMS# 20141014-00253 -  modified the timestamp format to 24
      l_req_id :=
         fnd_request.submit_request (
            application   => l_application,
            program       => l_program,
            description   => NULL,
            start_time    => TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'),
            sub_request   => FALSE);
      COMMIT;

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id,
                                             v_interval,
                                             v_max_time,
                                             v_phase,
                                             v_status,
                                             v_dev_phase,
                                             v_dev_status,
                                             v_message)
         THEN
            v_error_message :=
                  CHR (10)
               || 'ReqID='
               || l_req_id
               || ' DPhase '
               || v_dev_phase
               || ' DStatus '
               || v_dev_status
               || CHR (10)
               || ' MSG - '
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'Error occured in the running of XXWC OBIEE ETL Inventory Snapshot'
                  || v_error_message
                  || '.';
               fnd_file.put_line (fnd_file.LOG, l_statement);
               fnd_file.put_line (fnd_file.output, l_statement);
               DBMS_OUTPUT.put_line (l_statement);
               RAISE PROGRAM_ERROR;
            END IF;
         END IF;
      ELSE
         l_statement :=
            'Error occured when submitting XXWC OBIEE ETL Inventory Snapshot';
         fnd_file.put_line (fnd_file.LOG, l_statement);
         fnd_file.put_line (fnd_file.output, l_statement);
         DBMS_OUTPUT.put_line (l_statement);
         RAISE PROGRAM_ERROR;
      END IF;

      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
      DBMS_OUTPUT.put_line (
         'Responsibility Name:  ' || p_responsibility_name);
      DBMS_OUTPUT.put_line ('User Name:  ' || p_user_name);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         retcode := 2;
         l_sec := 'WHEN program_error Exception';
         DBMS_OUTPUT.put_line (l_sec);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_package,
            p_calling             => l_sec,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => l_distribution_list,
            p_module              => 'INV');
      WHEN OTHERS
      THEN
         retcode := 2;
         l_sec := ' WHEN OTHERS Exception';
         DBMS_OUTPUT.put_line (l_sec);
         fnd_file.put_line (fnd_file.LOG, 'Error Message = ' || SQLERRM);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_package,
            p_calling             => l_sec,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => l_distribution_list,
            p_module              => 'INV');
   END;


   /*---------------------------------------
     Procedure to submit the program from UC4-- Version 1.1
     ------------------------------------------*/
   PROCEDURE submit_inv_dimension (errbuf                     OUT VARCHAR2,
                                   retcode                    OUT NUMBER,
                                   p_user_name             IN     VARCHAR2,
                                   p_responsibility_name   IN     VARCHAR2)
   /*************************************************************************
        $Header XXWC_OBIEE_ETL_PRE_PROCESS.submit_inv_dimension $

        PURPOSE: Procedure to submit the program from UC4

        REVISIONS:
        Ver        Date         Author                Description
        -----  -----------  ------------------    ----------------
        1.0    27-May-2014  Manjula Chellappan    Initial Version
        1.5   14-Oct-2014   Manjula Chellappan    TMS# 20141014-00253 -   Fix scheduling bugs in XXWC_OBIEE_ETL_PRE_PROCESS     

      **************************************************************************/
   IS
      l_package               VARCHAR2 (50)
                                 := 'OBIEE_ETL_INV_SNAPSHOT_PKG.SUBMIT_INV_DIMENSION';
      l_distribution_list     VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';

      l_req_id                NUMBER NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      v_supplier_id           NUMBER;
      v_rec_cnt               NUMBER := 0;
      v_interval              NUMBER := 30;                      -- In seconds
      v_max_time              NUMBER := 15000;                   -- In seconds
      l_message               VARCHAR2 (150);
      l_errormessage          VARCHAR2 (3000);
      pl_errorstatus          NUMBER;
      l_can_submit_request    BOOLEAN := TRUE;
      l_globalset             VARCHAR2 (100);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_sec                   VARCHAR2 (255);
      l_statement             VARCHAR2 (9000);
      l_user                  fnd_user.user_id%TYPE;
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_program               VARCHAR2 (30) := 'XXWC_OBIEE_ETL_INV_DIMENSION'; /* Concurrent Program short name */
      l_application           VARCHAR2 (30) := 'XXWC';
   BEGIN
      retcode := 0;



      /*---------------------------------------
      Derive User Id
      ------------------------------------------*/

      l_sec := 'Start Getting User Id ';

      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM applsys.fnd_user
          WHERE     user_name = UPPER (p_user_name)
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName - ' || p_user_name || ' not defined in Oracle';
            DBMS_OUTPUT.put_line (
                  l_err_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            DBMS_OUTPUT.put_line (
                  l_err_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            RAISE PROGRAM_ERROR;
      END;



      /*---------------------------------------
      Derive Responsibility Id and Responsibility Application Id
      ------------------------------------------*/

      l_sec := 'Start Getting Responsibility Id ';

      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM apps.fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility - '
               || p_responsibility_name
               || ' not defined in Oracle';
            DBMS_OUTPUT.put_line (
                  l_err_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for ResponsibilityName - '
               || p_responsibility_name;
            DBMS_OUTPUT.put_line (
                  l_err_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            RAISE PROGRAM_ERROR;
      END;



      /*---------------------------------------
      Apps Initialize
      ------------------------------------------*/

      l_sec := 'Start App Initialize ';


      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);

      l_sec := 'Start Submit concurrent Request ' || l_program;

-- Modiffied by Manjula Chellappan  TMS# 20141014-00253 -  modified the timestamp format to 24   
      l_req_id :=
         fnd_request.submit_request (
            application   => l_application,
            program       => l_program,
            description   => NULL,
            start_time    => TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'),
            sub_request   => FALSE);
      COMMIT;

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id,
                                             v_interval,
                                             v_max_time,
                                             v_phase,
                                             v_status,
                                             v_dev_phase,
                                             v_dev_status,
                                             v_message)
         THEN
            v_error_message :=
                  CHR (10)
               || 'ReqID='
               || l_req_id
               || ' DPhase '
               || v_dev_phase
               || ' DStatus '
               || v_dev_status
               || CHR (10)
               || ' MSG - '
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'Error occured in the running of XXWC OBIEE ETL Inventory Dimension'
                  || v_error_message
                  || '.';
               fnd_file.put_line (fnd_file.LOG, l_statement);
               fnd_file.put_line (fnd_file.output, l_statement);
               DBMS_OUTPUT.put_line (l_statement);
               RAISE PROGRAM_ERROR;
            END IF;
         END IF;
      ELSE
         l_statement :=
            'Error occured when submitting XXWC OBIEE ETL Inventory Dimension';
         fnd_file.put_line (fnd_file.LOG, l_statement);
         fnd_file.put_line (fnd_file.output, l_statement);
         DBMS_OUTPUT.put_line (l_statement);
         RAISE PROGRAM_ERROR;
      END IF;

      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
      DBMS_OUTPUT.put_line (
         'Responsibility Name:  ' || p_responsibility_name);
      DBMS_OUTPUT.put_line ('User Name:  ' || p_user_name);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         retcode := 2;
         l_sec := 'WHEN program_error Exception';
         DBMS_OUTPUT.put_line (l_sec);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_package,
            p_calling             => l_sec,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => l_distribution_list,
            p_module              => 'INV');
      WHEN OTHERS
      THEN
         retcode := 2;
         l_sec := ' WHEN OTHERS Exception';
         DBMS_OUTPUT.put_line (l_sec);
         fnd_file.put_line (fnd_file.LOG, 'Error Message = ' || SQLERRM);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_package,
            p_calling             => l_sec,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => l_distribution_list,
            p_module              => 'INV');
   END;


   /*---------------------------------------
   Procedure to send the error Mail
   ------------------------------------------*/

   PROCEDURE send_mail (p_procedure_name   IN     VARCHAR2,
                        p_section          IN     VARCHAR2,
                        p_request_id       IN     NUMBER,
                        p_error_msg        IN     VARCHAR2,
                        p_retcode             OUT NUMBER)
   /*************************************************************************
        $Header XXWC_OBIEE_ETL_PRE_PROCESS.send_mail $

        PURPOSE: Procedure to submit the Error Email from Exception

        REVISIONS:
        Ver        Date         Author                Description
        -----  -----------  ------------------    ----------------
        1.0    15-May-2014  Manjula Chellappan    Initial Version

      **************************************************************************/
   IS
      l_sec                 VARCHAR2 (100) := p_section;
      l_procedure_name      VARCHAR2 (50) := p_procedure_name;
      l_request_id          NUMBER := p_request_id;
      l_error_msg           VARCHAR2 (240) := p_error_msg;

      l_distribution_list   VARCHAR2 (75)
                               DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      p_retcode := 2;

      DBMS_OUTPUT.put_line (l_sec);
      fnd_file.put_line (fnd_file.LOG, 'Error Message = ' || SQLERRM);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_procedure_name,
         p_calling             => l_sec,
         p_request_id          => l_request_id,
         p_ora_error_msg       => SUBSTR (
                                       ' Error_Stack...'
                                    || DBMS_UTILITY.format_error_stack ()
                                    || ' Error_Backtrace...'
                                    || DBMS_UTILITY.format_error_backtrace (),
                                    1,
                                    2000),
         p_error_desc          => l_error_msg,
         p_distribution_list   => l_distribution_list,
         p_module              => 'INV');
   END;


   /*---------------------------------------
   Procedure to populate to vendor for Special Items
   ------------------------------------------*/


   PROCEDURE populate_Special_item_vendor
   /*************************************************************************
        $Header XXWC_OBIEE_ETL_PRE_PROCESS.populate_Special_item_vendor $

        PURPOSE: Procedure to load Special Item Vendor Details
        TMS : 20140716-00161

        REVISIONS:
        Ver        Date         Author                Description
        -----  -----------  ------------------    ----------------
        1.4    21-Jul-2014  Manjula Chellappan    Initial Version

      **************************************************************************/
   IS
      l_drop_table           VARCHAR2 (400)
                                := 'DROP TABLE xxwc.OBIEE_ETL_PO_VENDORS##';

      l_create_table         VARCHAR2 (4000)
         := 'CREATE TABLE xxwc.OBIEE_ETL_PO_VENDORS## AS
               SELECT * from (SELECT pla.po_header_id,
                             plla.ship_to_organization_id organization_id ,
                             pha.vendor_id,
                             pla.item_id inventory_item_id,
                             pla.org_id,
                             NVL (pla.cancel_flag, ''N'') cancel_flag,
                             aps.segment1 vendor_num,
                             aps.vendor_name,
                             ROW_NUMBER ()
                             OVER (
                                PARTITION BY pla.item_id,
                                             plla.ship_to_organization_id,
                                             NVL (pla.cancel_flag, ''N'')
                                ORDER BY pla.po_header_id DESC)
                                AS rn
                        FROM po_lines_all pla,
                             po_line_locations_all plla,
                             po_headers_all pha,
                             ap_suppliers aps
                       WHERE     pla.po_line_id = plla.po_line_id
                         AND pla.po_header_id = pha.po_header_id
                         AND pha.vendor_id = aps.vendor_id
                         AND NVL (pla.cancel_flag, ''N'') = ''N'')
                     WHERE rn = 1';

      l_error_msg            VARCHAR2 (4000)
                                := 'Error Loading xxwc.OBIEE_ETL_PO_VENDORS## : ';

      l_success_msg          VARCHAR2 (4000)
         := ' Loading xxwc.OBIEE_ETL_PO_VENDORS## Completed ';

      l_drop_msg             VARCHAR2 (400)
                                := ' xxwc.OBIEE_ETL_PO_VENDORS## dropped ';

      l_Procedure_name       VARCHAR2 (100)
         := 'OBIEE_ETL_INV_SNAPSHOT_PKG.POPULATE_SPECIAL_ITEM_VENDOR';

      l_table_name           VARCHAR2 (100) := 'OBIEE_ETL_PO_VENDORS##';

      l_table_count          NUMBER := 0;

      l_create_index         VARCHAR2 (4000)
         := 'CREATE INDEX xxwc.OBIEE_ETL_PO_VENDORS##_N1
                                on xxwc.OBIEE_ETL_PO_VENDORS## (inventory_item_id,organization_id)';
      l_sec                  VARCHAR2 (200);

      l_update_vendor_num    VARCHAR2 (4000)
         := 'UPDATE xxwc.obiee_etl_inv_stage a
              SET vendor_num =
                (SELECT vendor_num
                FROM xxwc.OBIEE_ETL_PO_VENDORS##
                WHERE inventory_item_id = a.inventory_item_id
                AND organization_id     = a.organization_id
                )
              WHERE vendor_num IS NULL';

      l_update_vendor_name   VARCHAR2 (4000)
         := 'UPDATE xxwc.obiee_etl_inv_stage a
              SET vendor_name =
                (SELECT vendor_name
                FROM xxwc.OBIEE_ETL_PO_VENDORS##
                WHERE inventory_item_id = a.inventory_item_id
                AND organization_id     = a.organization_id
                )
              WHERE vendor_num IS NOT NULL
              AND vendor_name IS NULL';
   BEGIN
      BEGIN
         l_sec :=
            'Checking table exists at XXWC_OBIEE_ETL_PRE_PROCESS.populate_special_item_vendor ';

         SELECT COUNT (*)
           INTO l_table_count
           FROM all_tables
          WHERE table_name = l_table_name AND owner = g_owner;

         IF l_table_count = 1
         THEN
            EXECUTE IMMEDIATE l_drop_table;

            DBMS_OUTPUT.put_line (
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
            fnd_file.put_line (
               fnd_file.LOG,
                  l_drop_msg
               || ' at '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         END IF;
      END;

      l_sec :=
         'creating table at XXWC_OBIEE_ETL_PRE_PROCESS.populate_special_item_vendor ';


      EXECUTE IMMEDIATE l_create_table;

      l_sec :=
         'creating index at XXWC_OBIEE_ETL_PRE_PROCESS.populate_special_item_vendor ';

      EXECUTE IMMEDIATE l_create_index;


      COMMIT;

      l_sec :=
         'Updating Vendor Number at XXWC_OBIEE_ETL_PRE_PROCESS.populate_special_item_vendor ';

      EXECUTE IMMEDIATE l_update_vendor_num;

      DBMS_OUTPUT.put_line (
            'Vendor Num updated'
         || ' at '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
            'Vendor Num updated'
         || ' at '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


      l_sec :=
         'Updating Vendor Name at XXWC_OBIEE_ETL_PRE_PROCESS.populate_special_item_vendor ';


      EXECUTE IMMEDIATE l_update_vendor_name;

      DBMS_OUTPUT.put_line (
            'Vendor Name updated'
         || ' at '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
            'Vendor Name updated'
         || ' at '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         g_retcode := 2;

         l_error_msg := l_error_msg || CHR (10) || l_sec || CHR (10) || SQLERRM;

         g_errbuf := g_errbuf || CHR (10) || l_error_msg;

         DBMS_OUTPUT.put_line (
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


END populate_Special_item_vendor;

END XXWC_OBIEE_ETL_PRE_PROCESS;
/
SHO ERR
/

-- UC4 execution
GRANT EXECUTE ON APPS.XXWC_OBIEE_ETL_PRE_PROCESS TO interface_prism;