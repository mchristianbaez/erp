CREATE OR REPLACE PACKAGE BODY APPS.xxwc_bom_conversion_pkg
AS
/********************************************************************************
FILE NAME: APPS.XXWC_BOM_CONVERSION_PKG.pkg

PROGRAM TYPE: PL/SQL Package Body

PURPOSE: Validates and loads BOM and Routing data from staging to interface tables.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     08/24/2012    Gopi Damuluri    Initial version.
1.1     02/25/2013    Gopi Damuluri    Modified validate_bom procedure to add 
                                       "Validate BOM Component Items"
                                       create master-org BOMs
********************************************************************************/

validate_only    EXCEPTION;

/*************************************************************************
*   PROCEDURE Name: validate_bom
*
*   PURPOSE:   To validate BOM Staging data
* ***************************************************************************/
PROCEDURE validate_bom(p_errbuf    OUT  VARCHAR2,
                       p_retcode   OUT  NUMBER) IS
CURSOR cur_bom IS
SELECT DISTINCT xxstg.item_number   item_number
     , xxstg.warehouse              warehouse
  FROM xxwc.xxwc_bom_conv_tbl       xxstg
 WHERE 1                      = 1
   AND xxstg.status           IN ('N', 'E');

CURSOR cur_bom_comp IS
SELECT DISTINCT xxstg.component   
     , xxstg.warehouse              warehouse
  FROM xxwc.xxwc_bom_conv_tbl       xxstg
 WHERE 1                      = 1
   AND xxstg.status           IN ('N', 'E');

  l_val_flag    VARCHAR2(1);
BEGIN

   -----------------------------------------------------
   -- Validate BOM Items
   -----------------------------------------------------
   FOR rec_bom IN cur_bom LOOP
     BEGIN
       SELECT '1'
         INTO l_val_flag
         FROM mtl_system_items_b           item
            , org_organization_definitions ood
        WHERE 1                      = 1
          AND item.segment1          = TRIM(rec_bom.item_number)
          AND ood.organization_code  = TRIM(rec_bom.warehouse)
          AND item.organization_id   = ood.organization_id
          AND item.bom_enabled_flag  = 'Y';

       UPDATE xxwc.xxwc_bom_conv_tbl       xxstg
          SET status          = 'V'
        WHERE 1               = 1
          AND item_number     = rec_bom.item_number
          AND xxstg.warehouse = rec_bom.warehouse;

     EXCEPTION
     WHEN NO_DATA_FOUND THEN
       -- fnd_file.put_line(fnd_file.log,' Item does not exist. ItemNumber - '||rec_bom.item_number||' Warehouse - '||rec_bom.warehouse);
       UPDATE xxwc.xxwc_bom_conv_tbl       xxstg
          SET status          = 'E'
            , error_message   = 'Invalid Item - '||rec_bom.item_number
        WHERE 1               = 1
          AND item_number     = rec_bom.item_number
          AND xxstg.warehouse = rec_bom.warehouse;
     WHEN OTHERS THEN
       -- fnd_file.put_line(fnd_file.log,' Unidentified error. ItemNumber - '||rec_bom.item_number||' Warehouse - '||rec_bom.warehouse||' .Error - '||SQLERRM);
       UPDATE xxwc.xxwc_bom_conv_tbl       xxstg
          SET status          = 'E'
            , error_message   = 'Unidentified Error'
        WHERE 1               = 1
          AND item_number     = rec_bom.item_number
          AND xxstg.warehouse = rec_bom.warehouse;
     END;
   END LOOP;

   -----------------------------------------------------
   -- Validate BOM Component Items
   -----------------------------------------------------
   FOR rec_bom IN cur_bom_comp LOOP
     BEGIN
       SELECT '1'
         INTO l_val_flag
         FROM mtl_system_items_b           item
            , org_organization_definitions ood
        WHERE 1                      = 1
          AND item.segment1          = TRIM(rec_bom.component)
          AND ood.organization_code  = TRIM(rec_bom.warehouse)
          AND item.organization_id   = ood.organization_id
          AND item.bom_enabled_flag  = 'Y';

       UPDATE xxwc.xxwc_bom_conv_tbl       xxstg
          SET status          = 'V'
        WHERE 1               = 1
          AND component       = rec_bom.component
          AND xxstg.warehouse = rec_bom.warehouse;

     EXCEPTION
     WHEN NO_DATA_FOUND THEN
       -- fnd_file.put_line(fnd_file.log,' Item does not exist. ItemNumber - '||rec_bom.item_number||' Warehouse - '||rec_bom.warehouse);
       UPDATE xxwc.xxwc_bom_conv_tbl       xxstg
          SET status          = 'E'
            , error_message   = 'Invalid Component - '||rec_bom.component
        WHERE 1               = 1
          AND component     = rec_bom.component
          AND xxstg.warehouse = rec_bom.warehouse;
     WHEN OTHERS THEN
       -- fnd_file.put_line(fnd_file.log,' Unidentified error. ItemNumber - '||rec_bom.item_number||' Warehouse - '||rec_bom.warehouse||' .Error - '||SQLERRM);
       UPDATE xxwc.xxwc_bom_conv_tbl       xxstg
          SET status          = 'E'
            , error_message   = 'Unidentified Error - Components'
        WHERE 1               = 1
          AND component     = rec_bom.component
          AND xxstg.warehouse = rec_bom.warehouse;
     END;
   END LOOP;

   COMMIT;
EXCEPTION
WHEN OTHERS THEN
  fnd_file.put_line(fnd_file.log,' Error In validate_bom '||SQLERRM);
END validate_bom;

/*************************************************************************
*   PROCEDURE Name: bom_conversion
*
*   PURPOSE:   To validate BOM Staging data and load the same into
*              Interface table.
* ***************************************************************************/
PROCEDURE bom_conversion(p_errbuf        OUT  VARCHAR2,
                         p_retcode       OUT  NUMBER,
                         p_validate_only  IN  VARCHAR2) IS

CURSOR get_all_boms IS
SELECT DISTINCT xxstg.item_number   item_number
     , xxstg.warehouse              warehouse
     , item.inventory_item_id       inventory_item_id
     , item.organization_id         organization_id
  FROM xxwc.xxwc_bom_conv_tbl       xxstg
     , mtl_system_items_b           item
     , org_organization_definitions ood
 WHERE item.segment1          = RTRIM(xxstg.item_number)
   AND item.organization_id   = ood.organization_id
   AND ood.organization_code  = xxstg.warehouse
   AND item.bom_enabled_flag  = 'Y'
   AND xxstg.status           = 'V';

CURSOR get_components (p_item_number VARCHAR2, p_warehouse  VARCHAR2) IS
SELECT DISTINCT msi.inventory_item_id
     , xxstg.quantity quantity
     , xxstg.component
     , xxstg.item_seq
  FROM xxwc.xxwc_bom_conv_tbl       xxstg
     , mtl_system_items_b           msi
     , org_organization_definitions ood
WHERE RTRIM(item_number)    = p_item_number
  AND xxstg.warehouse       = p_warehouse
  AND ood.organization_code = xxstg.warehouse
  AND msi.bom_enabled_flag  = 'Y'
  AND msi.organization_id   = ood.organization_id
  AND msi.segment1          = xxstg.component
  AND xxstg.status          = 'V'
ORDER BY  xxstg.component;

vnCounter        NUMBER;
vdDate           DATE;
vnUserId         NUMBER;

BEGIN
   --START OF PROGRAM
   vdDate   :=SYSDATE;
   vnUserId:=Fnd_Global.user_id;

 --  Before running the Interface remove the comments of below two lines
 --  EXECUTE IMMEDIATE 'truncate table bom.BOM_BILL_OF_MTLS_INTERFACE';
 --  EXECUTE IMMEDIATE 'truncate table bom.BOM_INVENTORY_COMPS_INTERFACE';

 -- Validate staging table BOM data.
 validate_bom(p_errbuf
            , p_retcode);

 IF p_validate_only = 'Y' THEN
   RAISE validate_only;
 END IF;

   FOR c1 IN get_all_BOMs
   LOOP

       -- begin   rev 1.1     02/25/2013
       -------------------------------------------------------------------
       -- Inserting into BOM_BILL_OF_MTLS_INTERFACE table for Master Org 
       -------------------------------------------------------------------
       BEGIN
       INSERT INTO BOM_BILL_OF_MTLS_INTERFACE (
       ASSEMBLY_ITEM_ID,
       ORGANIZATION_ID,
       ALTERNATE_BOM_DESIGNATOR,
       LAST_UPDATE_DATE,
       LAST_UPDATED_BY,
       CREATION_DATE,
       CREATED_BY,
       LAST_UPDATE_LOGIN,
       COMMON_ASSEMBLY_ITEM_ID,
       SPECIFIC_ASSEMBLY_COMMENT,
       PENDING_FROM_ECN,
       ASSEMBLY_TYPE,
       COMMON_BILL_SEQUENCE_ID,
       BILL_SEQUENCE_ID,
       REQUEST_ID,
       PROGRAM_APPLICATION_ID,
       PROGRAM_ID,
       PROGRAM_UPDATE_DATE,
       DEMAND_SOURCE_LINE,
       SET_ID,
       COMMON_ORGANIZATION_ID,
       DEMAND_SOURCE_TYPE,
       DEMAND_SOURCE_HEADER_ID,
       TRANSACTION_ID,
       PROCESS_FLAG,
       ORGANIZATION_CODE,
       COMMON_ORG_CODE,
       ITEM_NUMBER,
       COMMON_ITEM_NUMBER,
       NEXT_EXPLODE_DATE,
       REVISION,
       TRANSACTION_TYPE,
       DELETE_GROUP_NAME,
       DG_DESCRIPTION,
       ORIGINAL_SYSTEM_REFERENCE,
       ENABLE_ATTRS_UPDATE,
       IMPLEMENTATION_DATE)
    VALUES
    (  c1.inventory_item_id,       --ASSEMBLY_ITEM_ID,
       222,                        --ORGANIZATION_ID,
       NULL,                       --ALTERNATE_BOM_DESIGNATOR,
       vdDate,                     --LAST_UPDATE_DATE,
       vnUserId,                   --LAST_UPDATED_BY,
       vdDate,                     --CREATION_DATE,
       vnUserId,                   --CREATED_BY,
       NULL,                       --LAST_UPDATE_LOGIN,
       NULL,                       --COMMON_ASSEMBLY_ITEM_ID,
       NULL,                       --SPECIFIC_ASSEMBLY_COMMENT,
       NULL,                       --PENDING_FROM_ECN,
       1,                          --ASSEMBLY_TYPE, (STANDARD)
       NULL,                       --COMMON_BILL_SEQUENCE_ID,
       NULL,                       --BILL_SEQUENCE_ID,
       NULL,                       --REQUEST_ID,
       NULL,                       --PROGRAM_APPLICATION_ID,
       NULL,                       --PROGRAM_ID,
       NULL,                       --PROGRAM_UPDATE_DATE,
       NULL,                       --DEMAND_SOURCE_LINE,
       NULL,                       --SET_ID,
       NULL,                       --COMMON_ORGANIZATION_ID,
       NULL,                       --DEMAND_SOURCE_TYPE,
       NULL,                       --DEMAND_SOURCE_HEADER_ID,
       NULL,                       --TRANSACTION_ID,
       1,                          --PROCESS_FLAG,
       NULL,                       --ORGANIZATION_CODE,
       NULL,                       --COMMON_ORG_CODE,
       NULL,                       --ITEM_NUMBER,
       NULL,                       --COMMON_ITEM_NUMBER,
       NULL,                       --NEXT_EXPLODE_DATE,
       NULL,                       --REVISION,
       'CREATE',                   --TRANSACTION_TYPE,
       NULL,                       --DELETE_GROUP_NAME,
       NULL,                       --DG_DESCRIPTION,
       NULL,                       --ORIGINAL_SYSTEM_REFERENCE,
       'Y',                        --ENABLE_ATTRS_UPDATE
       NULL);                      --IMPLEMENTATION_DATE);
       -- end   rev 1.1     02/25/2013

       ------------------------------------------------------------
       -- Inserting into BOM_BILL_OF_MTLS_INTERFACE table for Inv Org
       ------------------------------------------------------------
       INSERT INTO BOM_BILL_OF_MTLS_INTERFACE (
       ASSEMBLY_ITEM_ID,
       ORGANIZATION_ID,
       ALTERNATE_BOM_DESIGNATOR,
       LAST_UPDATE_DATE,
       LAST_UPDATED_BY,
       CREATION_DATE,
       CREATED_BY,
       LAST_UPDATE_LOGIN,
       COMMON_ASSEMBLY_ITEM_ID,
       SPECIFIC_ASSEMBLY_COMMENT,
       PENDING_FROM_ECN,
       ASSEMBLY_TYPE,
       COMMON_BILL_SEQUENCE_ID,
       BILL_SEQUENCE_ID,
       REQUEST_ID,
       PROGRAM_APPLICATION_ID,
       PROGRAM_ID,
       PROGRAM_UPDATE_DATE,
       DEMAND_SOURCE_LINE,
       SET_ID,
       COMMON_ORGANIZATION_ID,
       DEMAND_SOURCE_TYPE,
       DEMAND_SOURCE_HEADER_ID,
       TRANSACTION_ID,
       PROCESS_FLAG,
       ORGANIZATION_CODE,
       COMMON_ORG_CODE,
       ITEM_NUMBER,
       COMMON_ITEM_NUMBER,
       NEXT_EXPLODE_DATE,
       REVISION,
       TRANSACTION_TYPE,
       DELETE_GROUP_NAME,
       DG_DESCRIPTION,
       ORIGINAL_SYSTEM_REFERENCE,
       IMPLEMENTATION_DATE)
    VALUES
    (  c1.inventory_item_id,       --ASSEMBLY_ITEM_ID,
       c1.organization_id,         --ORGANIZATION_ID,
       NULL,                       --ALTERNATE_BOM_DESIGNATOR,
       vdDate,                     --LAST_UPDATE_DATE,
       vnUserId,                   --LAST_UPDATED_BY,
       vdDate,                     --CREATION_DATE,
       vnUserId,                   --CREATED_BY,
       NULL,                       --LAST_UPDATE_LOGIN,
       NULL,                       --COMMON_ASSEMBLY_ITEM_ID,
       NULL,                       --SPECIFIC_ASSEMBLY_COMMENT,
       NULL,                       --PENDING_FROM_ECN,
       1,                          --ASSEMBLY_TYPE, (STANDARD)
       NULL,                       --COMMON_BILL_SEQUENCE_ID,
       NULL,                       --BILL_SEQUENCE_ID,
       NULL,                       --REQUEST_ID,
       NULL,                       --PROGRAM_APPLICATION_ID,
       NULL,                       --PROGRAM_ID,
       NULL,                       --PROGRAM_UPDATE_DATE,
       NULL,                       --DEMAND_SOURCE_LINE,
       NULL,                       --SET_ID,
       NULL,                       --COMMON_ORGANIZATION_ID,
       NULL,                       --DEMAND_SOURCE_TYPE,
       NULL,                       --DEMAND_SOURCE_HEADER_ID,
       NULL,                       --TRANSACTION_ID,
       1,                          --PROCESS_FLAG,
       NULL,                       --ORGANIZATION_CODE,
       NULL,                       --COMMON_ORG_CODE,
       NULL,                       --ITEM_NUMBER,
       NULL,                       --COMMON_ITEM_NUMBER,
       NULL,                       --NEXT_EXPLODE_DATE,
       NULL,                       --REVISION,
       'CREATE',                   --TRANSACTION_TYPE,
       NULL,                       --DELETE_GROUP_NAME,
       NULL,                       --DG_DESCRIPTION,
       NULL,                       --ORIGINAL_SYSTEM_REFERENCE,
       NULL);                      --IMPLEMENTATION_DATE);

      vnCounter:=0;
      FOR c2 IN get_components(c1.item_number, c1.warehouse)
      LOOP

         BEGIN
         IF c2.inventory_item_id <> c1.inventory_item_id THEN

         vnCounter:=vnCounter+1;
          -- begin   rev 1.1     02/25/2013
          ------------------------------------------------------------
          -- Inserting into BOM_INVENTORY_COMPS_INTERFACE table for Master Org
          ------------------------------------------------------------
           INSERT INTO BOM_INVENTORY_COMPS_INTERFACE
          (OPERATION_SEQ_NUM,
           COMPONENT_ITEM_ID,
           LAST_UPDATE_DATE,
           LAST_UPDATED_BY,
           CREATION_DATE,
           CREATED_BY,
           LAST_UPDATE_LOGIN,
           ITEM_NUM,
           COMPONENT_QUANTITY,
           COMPONENT_YIELD_FACTOR,
           COMPONENT_REMARKS,
           EFFECTIVITY_DATE,
           CHANGE_NOTICE,
           IMPLEMENTATION_DATE,
           DISABLE_DATE,
           PLANNING_FACTOR,
           QUANTITY_RELATED,
           SO_BASIS,
           OPTIONAL,
           MUTUALLY_EXCLUSIVE_OPTIONS,
           INCLUDE_IN_COST_ROLLUP,
           CHECK_ATP,
           SHIPPING_ALLOWED,
           REQUIRED_TO_SHIP,
           REQUIRED_FOR_REVENUE,
           INCLUDE_ON_SHIP_DOCS,
           LOW_QUANTITY,
           HIGH_QUANTITY,
           ACD_TYPE,
           OLD_COMPONENT_SEQUENCE_ID,
           COMPONENT_SEQUENCE_ID,
           BILL_SEQUENCE_ID,
           REQUEST_ID,
           PROGRAM_APPLICATION_ID,
           PROGRAM_ID,
           PROGRAM_UPDATE_DATE,
           WIP_SUPPLY_TYPE,
           SUPPLY_SUBINVENTORY,
           SUPPLY_LOCATOR_ID,
           REVISED_ITEM_SEQUENCE_ID,
           MODEL_COMP_SEQ_ID,
           ASSEMBLY_ITEM_ID,
           ALTERNATE_BOM_DESIGNATOR,
           ORGANIZATION_ID,
           ORGANIZATION_CODE,
           COMPONENT_ITEM_NUMBER,
           ASSEMBLY_ITEM_NUMBER,
           REVISED_ITEM_NUMBER,
           LOCATION_NAME,
           REFERENCE_DESIGNATOR,
           SUBSTITUTE_COMP_ID,
           SUBSTITUTE_COMP_NUMBER,
           TRANSACTION_ID,
           PROCESS_FLAG,
           BOM_ITEM_TYPE,
           OPERATION_LEAD_TIME_PERCENT,
           COST_FACTOR,
           INCLUDE_ON_BILL_DOCS,
           PICK_COMPONENTS,
           DDF_CONTEXT1,
           DDF_CONTEXT2,
           NEW_OPERATION_SEQ_NUM,
           OLD_OPERATION_SEQ_NUM,
           NEW_EFFECTIVITY_DATE,
           OLD_EFFECTIVITY_DATE,
           ASSEMBLY_TYPE,
           INTERFACE_ENTITY_TYPE,
           TRANSACTION_TYPE,
           BOM_INVENTORY_COMPS_IFCE_KEY,
           ENG_REVISED_ITEMS_IFCE_KEY,
           ENG_CHANGES_IFCE_KEY,
           TO_END_ITEM_UNIT_NUMBER,
           FROM_END_ITEM_UNIT_NUMBER,
           NEW_FROM_END_ITEM_UNIT_NUMBER,
           DELETE_GROUP_NAME,
           DG_DESCRIPTION,
           ORIGINAL_SYSTEM_REFERENCE,
           ENFORCE_INT_REQUIREMENTS,
           OPTIONAL_ON_MODEL,
           PARENT_BILL_SEQ_ID,
           PLAN_LEVEL,
           AUTO_REQUEST_MATERIAL,
           SUGGESTED_VENDOR_NAME,
           UNIT_PRICE,
           NEW_REVISED_ITEM_REVISION)
        VALUES
         (
           10,                              --OPERATION_SEQ_NUM,
           c2.inventory_item_id,            --COMPONENT_ITEM_ID,
           vdDate,                          --LAST_UPDATE_DATE,
           vnUserId,                        --LAST_UPDATED_BY,
           vdDate,                          --CREATION_DATE,
           vnUserId,                        --CREATED_BY,
           NULL,                            --LAST_UPDATE_LOGIN,
           10*vnCounter,                    --ITEM_NUM,
           c2.QUANTITY,                     --QUANTITY,
           NULL,                            --COMPONENT_YIELD_FACTOR,
           NULL,                            --COMPONENT_REMARKS,
           TRUNC(SYSDATE),                  --EFFECTIVITY_DATE,
           NULL,                            --CHANGE_NOTICE,
           NULL,                            --IMPLEMENTATION_DATE,
           NULL,                            --DISABLE_DATE,
           NULL,                            --PLANNING_FACTOR,
           NULL,                            --QUANTITY_RELATED,
           NULL,                            --SO_BASIS,
           2 ,                              --OPTIONAL, -- NO
           2,                               --MUTUALLY_EXCLUSIVE_OPTIONS, -- NO
           NULL,                            --INCLUDE_IN_COST_ROLLUP,
           NULL,                            --CHECK_ATP,
           NULL,                            --SHIPPING_ALLOWED,
           NULL,                            --REQUIRED_TO_SHIP,
           NULL,                            --REQUIRED_FOR_REVENUE,
           NULL,                            --INCLUDE_ON_SHIP_DOCS,
           NULL,                            --LOW_QUANTITY,
           NULL,                            --HIGH_QUANTITY,
           NULL,                            --ACD_TYPE,
           NULL,                            --OLD_COMPONENT_SEQUENCE_ID,
           NULL,                            --COMPONENT_SEQUENCE_ID,
           NULL,                            --BILL_SEQUENCE_ID,
           NULL,                            --REQUEST_ID,
           NULL,                            --PROGRAM_APPLICATION_ID,
           NULL,                            --PROGRAM_ID,
           NULL,                            --PROGRAM_UPDATE_DATE,
           2,                               --WIP_SUPPLY_TYPE,  -- ????? -- Assembly Pull
           'General',                       --SUPPLY_SUBINVENTORY, ?????
           NULL,                            --SUPPLY_LOCATOR_ID,
           NULL,                            --REVISED_ITEM_SEQUENCE_ID,
           NULL,                            --MODEL_COMP_SEQ_ID,
           c1.inventory_item_id,            --ASSEMBLY_ITEM_ID,
           NULL,                            --ALTERNATE_BOM_DESIGNATOR,
           222,                             --ORGANIZATION_ID,
           NULL,                            --ORGANIZATION_CODE,
           NULL,                            --COMPONENT_ITEM_NUMBER,
           NULL,                            --ASSEMBLY_ITEM_NUMBER,
           NULL,                            --REVISED_ITEM_NUMBER,
           NULL,                            --LOCATION_NAME,
           NULL,                            --REFERENCE_DESIGNATOR,
           NULL,                            --SUBSTITUTE_COMP_ID,
           NULL,                            --SUBSTITUTE_COMP_NUMBER,
           NULL,                            --TRANSACTION_ID,
           1,                               --PROCESS_FLAG,
           NULL,                            --BOM_ITEM_TYPE,
           NULL,                            --OPERATION_LEAD_TIME_PERCENT,
           NULL,                            --COST_FACTOR,
           NULL,                            --INCLUDE_ON_BILL_DOCS,
           NULL,                            --PICK_COMPONENTS,
           NULL,                            --DDF_CONTEXT1,
           NULL,                            --DDF_CONTEXT2,
           NULL,                            --NEW_OPERATION_SEQ_NUM,
           NULL,                            --OLD_OPERATION_SEQ_NUM,
           NULL,                            --NEW_EFFECTIVITY_DATE,
           NULL,                            --OLD_EFFECTIVITY_DATE,
           1,                               --ASSEMBLY_TYPE, (STANDARD) ?????
           NULL,                            --INTERFACE_ENTITY_TYPE,
           'CREATE',                        --TRANSACTION_TYPE,
           NULL,                            --BOM_INVENTORY_COMPS_IFCE_KEY,
           NULL,                            --ENG_REVISED_ITEMS_IFCE_KEY,
           NULL,                            --ENG_CHANGES_IFCE_KEY,
           NULL,                            --TO_END_ITEM_UNIT_NUMBER,
           NULL,                            --FROM_END_ITEM_UNIT_NUMBER,
           NULL,                            --NEW_FROM_END_ITEM_UNIT_NUMBER,
           NULL,                            --DELETE_GROUP_NAME,
           NULL,                            --DG_DESCRIPTION,
           NULL,                            --ORIGINAL_SYSTEM_REFERENCE,
           NULL,                            --ENFORCE_INT_REQUIREMENTS,
           NULL,                            --OPTIONAL_ON_MODEL,
           NULL,                            --PARENT_BILL_SEQ_ID,
           NULL,                            --PLAN_LEVEL,
           NULL,                            --AUTO_REQUEST_MATERIAL,
           NULL,                            --SUGGESTED_VENDOR_NAME,
           NULL,                            --UNIT_PRICE,
           NULL                             --NEW_REVISED_ITEM_REVISION
           );
       -- end   rev 1.1     02/25/2013

          ------------------------------------------------------------
          -- Inserting into BOM_INVENTORY_COMPS_INTERFACE table for Inv Org
          ------------------------------------------------------------
           INSERT INTO BOM_INVENTORY_COMPS_INTERFACE
          (OPERATION_SEQ_NUM,
           COMPONENT_ITEM_ID,
           LAST_UPDATE_DATE,
           LAST_UPDATED_BY,
           CREATION_DATE,
           CREATED_BY,
           LAST_UPDATE_LOGIN,
           ITEM_NUM,
           COMPONENT_QUANTITY,
           COMPONENT_YIELD_FACTOR,
           COMPONENT_REMARKS,
           EFFECTIVITY_DATE,
           CHANGE_NOTICE,
           IMPLEMENTATION_DATE,
           DISABLE_DATE,
           PLANNING_FACTOR,
           QUANTITY_RELATED,
           SO_BASIS,
           OPTIONAL,
           MUTUALLY_EXCLUSIVE_OPTIONS,
           INCLUDE_IN_COST_ROLLUP,
           CHECK_ATP,
           SHIPPING_ALLOWED,
           REQUIRED_TO_SHIP,
           REQUIRED_FOR_REVENUE,
           INCLUDE_ON_SHIP_DOCS,
           LOW_QUANTITY,
           HIGH_QUANTITY,
           ACD_TYPE,
           OLD_COMPONENT_SEQUENCE_ID,
           COMPONENT_SEQUENCE_ID,
           BILL_SEQUENCE_ID,
           REQUEST_ID,
           PROGRAM_APPLICATION_ID,
           PROGRAM_ID,
           PROGRAM_UPDATE_DATE,
           WIP_SUPPLY_TYPE,
           SUPPLY_SUBINVENTORY,
           SUPPLY_LOCATOR_ID,
           REVISED_ITEM_SEQUENCE_ID,
           MODEL_COMP_SEQ_ID,
           ASSEMBLY_ITEM_ID,
           ALTERNATE_BOM_DESIGNATOR,
           ORGANIZATION_ID,
           ORGANIZATION_CODE,
           COMPONENT_ITEM_NUMBER,
           ASSEMBLY_ITEM_NUMBER,
           REVISED_ITEM_NUMBER,
           LOCATION_NAME,
           REFERENCE_DESIGNATOR,
           SUBSTITUTE_COMP_ID,
           SUBSTITUTE_COMP_NUMBER,
           TRANSACTION_ID,
           PROCESS_FLAG,
           BOM_ITEM_TYPE,
           OPERATION_LEAD_TIME_PERCENT,
           COST_FACTOR,
           INCLUDE_ON_BILL_DOCS,
           PICK_COMPONENTS,
           DDF_CONTEXT1,
           DDF_CONTEXT2,
           NEW_OPERATION_SEQ_NUM,
           OLD_OPERATION_SEQ_NUM,
           NEW_EFFECTIVITY_DATE,
           OLD_EFFECTIVITY_DATE,
           ASSEMBLY_TYPE,
           INTERFACE_ENTITY_TYPE,
           TRANSACTION_TYPE,
           BOM_INVENTORY_COMPS_IFCE_KEY,
           ENG_REVISED_ITEMS_IFCE_KEY,
           ENG_CHANGES_IFCE_KEY,
           TO_END_ITEM_UNIT_NUMBER,
           FROM_END_ITEM_UNIT_NUMBER,
           NEW_FROM_END_ITEM_UNIT_NUMBER,
           DELETE_GROUP_NAME,
           DG_DESCRIPTION,
           ORIGINAL_SYSTEM_REFERENCE,
           ENFORCE_INT_REQUIREMENTS,
           OPTIONAL_ON_MODEL,
           PARENT_BILL_SEQ_ID,
           PLAN_LEVEL,
           AUTO_REQUEST_MATERIAL,
           SUGGESTED_VENDOR_NAME,
           UNIT_PRICE,
           NEW_REVISED_ITEM_REVISION)
        VALUES
         (
           10,                              --OPERATION_SEQ_NUM,
           c2.inventory_item_id,            --COMPONENT_ITEM_ID,
           vdDate,                          --LAST_UPDATE_DATE,
           vnUserId,                        --LAST_UPDATED_BY,
           vdDate,                          --CREATION_DATE,
           vnUserId,                        --CREATED_BY,
           NULL,                            --LAST_UPDATE_LOGIN,
           10*vnCounter,                    --ITEM_NUM,
           c2.QUANTITY,                     --QUANTITY,
           NULL,                            --COMPONENT_YIELD_FACTOR,
           NULL,                            --COMPONENT_REMARKS,
           TRUNC(SYSDATE),                  --EFFECTIVITY_DATE,
           NULL,                            --CHANGE_NOTICE,
           NULL,                            --IMPLEMENTATION_DATE,
           NULL,                            --DISABLE_DATE,
           NULL,                            --PLANNING_FACTOR,
           NULL,                            --QUANTITY_RELATED,
           NULL,                            --SO_BASIS,
           2 ,                              --OPTIONAL, -- NO
           2,                               --MUTUALLY_EXCLUSIVE_OPTIONS, -- NO
           NULL,                            --INCLUDE_IN_COST_ROLLUP,
           NULL,                            --CHECK_ATP,
           NULL,                            --SHIPPING_ALLOWED,
           NULL,                            --REQUIRED_TO_SHIP,
           NULL,                            --REQUIRED_FOR_REVENUE,
           NULL,                            --INCLUDE_ON_SHIP_DOCS,
           NULL,                            --LOW_QUANTITY,
           NULL,                            --HIGH_QUANTITY,
           NULL,                            --ACD_TYPE,
           NULL,                            --OLD_COMPONENT_SEQUENCE_ID,
           NULL,                            --COMPONENT_SEQUENCE_ID,
           NULL,                            --BILL_SEQUENCE_ID,
           NULL,                            --REQUEST_ID,
           NULL,                            --PROGRAM_APPLICATION_ID,
           NULL,                            --PROGRAM_ID,
           NULL,                            --PROGRAM_UPDATE_DATE,
           2,                               --WIP_SUPPLY_TYPE,  -- ????? -- Assembly Pull
           'General',                       --SUPPLY_SUBINVENTORY, ?????
           NULL,                            --SUPPLY_LOCATOR_ID,
           NULL,                            --REVISED_ITEM_SEQUENCE_ID,
           NULL,                            --MODEL_COMP_SEQ_ID,
           c1.inventory_item_id,            --ASSEMBLY_ITEM_ID,
           NULL,                            --ALTERNATE_BOM_DESIGNATOR,
           c1.organization_id,              --ORGANIZATION_ID,
           NULL,                            --ORGANIZATION_CODE,
           NULL,                            --COMPONENT_ITEM_NUMBER,
           NULL,                            --ASSEMBLY_ITEM_NUMBER,
           NULL,                            --REVISED_ITEM_NUMBER,
           NULL,                            --LOCATION_NAME,
           NULL,                            --REFERENCE_DESIGNATOR,
           NULL,                            --SUBSTITUTE_COMP_ID,
           NULL,                            --SUBSTITUTE_COMP_NUMBER,
           NULL,                            --TRANSACTION_ID,
           1,                               --PROCESS_FLAG,
           NULL,                            --BOM_ITEM_TYPE,
           NULL,                            --OPERATION_LEAD_TIME_PERCENT,
           NULL,                            --COST_FACTOR,
           NULL,                            --INCLUDE_ON_BILL_DOCS,
           NULL,                            --PICK_COMPONENTS,
           NULL,                            --DDF_CONTEXT1,
           NULL,                            --DDF_CONTEXT2,
           NULL,                            --NEW_OPERATION_SEQ_NUM,
           NULL,                            --OLD_OPERATION_SEQ_NUM,
           NULL,                            --NEW_EFFECTIVITY_DATE,
           NULL,                            --OLD_EFFECTIVITY_DATE,
           1,                               --ASSEMBLY_TYPE, (STANDARD) ?????
           NULL,                            --INTERFACE_ENTITY_TYPE,
           'CREATE',                        --TRANSACTION_TYPE,
           NULL,                            --BOM_INVENTORY_COMPS_IFCE_KEY,
           NULL,                            --ENG_REVISED_ITEMS_IFCE_KEY,
           NULL,                            --ENG_CHANGES_IFCE_KEY,
           NULL,                            --TO_END_ITEM_UNIT_NUMBER,
           NULL,                            --FROM_END_ITEM_UNIT_NUMBER,
           NULL,                            --NEW_FROM_END_ITEM_UNIT_NUMBER,
           NULL,                            --DELETE_GROUP_NAME,
           NULL,                            --DG_DESCRIPTION,
           NULL,                            --ORIGINAL_SYSTEM_REFERENCE,
           NULL,                            --ENFORCE_INT_REQUIREMENTS,
           NULL,                            --OPTIONAL_ON_MODEL,
           NULL,                            --PARENT_BILL_SEQ_ID,
           NULL,                            --PLAN_LEVEL,
           NULL,                            --AUTO_REQUEST_MATERIAL,
           NULL,                            --SUGGESTED_VENDOR_NAME,
           NULL,                            --UNIT_PRICE,
           NULL                             --NEW_REVISED_ITEM_REVISION
           );

         END IF;
         EXCEPTION
         WHEN OTHERS THEN
           fnd_file.put_line(fnd_file.log,'Error inserting into Components Interface '||SQLERRM);
         END;
        END LOOP;  --get_components
       EXCEPTION
       WHEN OTHERS THEN
       fnd_file.put_line(fnd_file.log,'Error inserting into Bill of Materials Interface '||SQLERRM);
      END;

      UPDATE xxwc.xxwc_bom_conv_tbl       xxstg
         SET status = 'P'
       WHERE 1               = 1
         AND item_number     = c1.item_number
         AND xxstg.warehouse = c1.warehouse;
   END LOOP; --get_all_BOM's
  COMMIT;

  ----------------------------------------------------
  -- Routing Conversion
  ----------------------------------------------------
  routing_conversion (p_errbuf,
                      p_retcode,
                      NULL);

EXCEPTION
WHEN validate_only THEN
  NULL;
WHEN OTHERS THEN
ROLLBACK;
fnd_file.put_line(fnd_file.log,' Error In BOM_CONVERSION '||SQLERRM);
END bom_conversion;

/*************************************************************************
*   PROCEDURE Name: routing_conversion
*
*   PURPOSE:   To validate Routing Staging data and load the same into
*              Interface table.
* ***************************************************************************/
PROCEDURE routing_conversion (p_errbuf        OUT VARCHAR2,
                              p_retcode       OUT NUMBER,
                              p_mfg_org_code  IN VARCHAR2) IS

--Routing Cursor
CURSOR cur_rtg IS
SELECT DISTINCT msi.inventory_item_id
     , msi.organization_id
     , rt.item_number   item_number
     , rt.warehouse              warehouse
  FROM xxwc.xxwc_bom_conv_tbl       rt,
       mtl_system_items_b           msi,
       org_organization_definitions ood
WHERE 1                     = 1
  AND msi.segment1          = RTRIM(rt.item_number)
  AND msi.organization_id   = ood.organization_id
  AND ood.organization_code = NVL(p_mfg_org_code, ood.organization_code)
  AND rt.warehouse          = ood.organization_code
  AND rt.status             IN ('V', 'P');

vdDate DATE;
vnUserId NUMBER;
vopseq NUMBER :=100;
vrouseq NUMBER:=100;


BEGIN
--START OF PROGRAM
   vdDate   :=SYSDATE;
   vnUserId:=Fnd_Global.user_id;
  -- EXECUTE IMMEDIATE 'truncate table bom.BOM_OP_ROUTINGS_INTERFACE'; --Header
  -- EXECUTE IMMEDIATE 'truncate table bom.BOM_OP_SEQUENCES_INTERFACE';--Operation's
  -- EXECUTE IMMEDIATE 'truncate table bom.BOM_OP_RESOURCES_INTERFACE';--Operation resource
-- Routing Header
FOR REC_RTG IN CUR_RTG
LOOP
  --For ROUTING_SEQUENCE_ID
  vrouseq := vrouseq + 10;
  vopseq  := 10;
  ------------------------------------------------------------
  -- Inserting into BOM_OP_ROUTINGS_INTERFACE table
  ------------------------------------------------------------
  INSERT INTO BOM_OP_ROUTINGS_INTERFACE(ROUTING_SEQUENCE_ID,
      ASSEMBLY_ITEM_ID,
      ORGANIZATION_ID,
      ALTERNATE_ROUTING_DESIGNATOR,
      LAST_UPDATE_DATE,
      LAST_UPDATED_BY,
      CREATION_DATE,
      CREATED_BY,
      LAST_UPDATE_LOGIN,
      ROUTING_TYPE,
      COMMON_ASSEMBLY_ITEM_ID,
      COMMON_ROUTING_SEQUENCE_ID,
      ROUTING_COMMENT,
      COMPLETION_SUBINVENTORY,
      COMPLETION_LOCATOR_ID,
      ATTRIBUTE_CATEGORY,
      ATTRIBUTE1,
      ATTRIBUTE2,
      ATTRIBUTE3,
      ATTRIBUTE4,
      ATTRIBUTE5,
      ATTRIBUTE6,
      ATTRIBUTE7,
      ATTRIBUTE8,
      ATTRIBUTE9,
      ATTRIBUTE10,
      ATTRIBUTE11,
      ATTRIBUTE12,
      ATTRIBUTE13,
      ATTRIBUTE14,
      ATTRIBUTE15,
      REQUEST_ID,
      PROGRAM_APPLICATION_ID,
      PROGRAM_ID,
      PROGRAM_UPDATE_DATE,
      DEMAND_SOURCE_LINE,
      SET_ID,
      PROCESS_REVISION,
      DEMAND_SOURCE_TYPE,
      DEMAND_SOURCE_HEADER_ID,
      ORGANIZATION_CODE,
      ASSEMBLY_ITEM_NUMBER,
      COMMON_ITEM_NUMBER,
      LOCATION_NAME,
      TRANSACTION_ID,
      PROCESS_FLAG,
      TRANSACTION_TYPE,
      LINE_ID,
      LINE_CODE,
      MIXED_MODEL_MAP_FLAG,
      PRIORITY,
      CFM_ROUTING_FLAG,
      TOTAL_PRODUCT_CYCLE_TIME,
      CTP_FLAG,
      ORIGINAL_SYSTEM_REFERENCE,
      SERIALIZATION_START_OP,
      DELETE_GROUP_NAME,
      DG_DESCRIPTION)
  VALUES (  BOM.BOM_OPERATIONAL_ROUTINGS_S.nextval,      --ROUTING_SEQUENCE_ID,
      rec_rtg.inventory_item_id,      --ASSEMBLY_ITEM_ID,
      rec_rtg.organization_id,        --ORGANIZATION_ID,
      NULL,                           --ALTERNATE_ROUTING_DESIGNATOR,
      vddate,                         --LAST_UPDATE_DATE,
      vnUserId,                       --LAST_UPDATED_BY,
      vddate,                         --CREATION_DATE,
      vnUserId,                       --CREATED_BY,
      NULL,                           --LAST_UPDATE_LOGIN,
      1,                              --ROUTING_TYPE(1-Manu.,2-Engg.)
      NULL,                           --COMMON_ASSEMBLY_ITEM_ID,
      NULL,                           --COMMON_ROUTING_SEQUENCE_ID,
      NULL,                           --ROUTING_COMMENT,
      'General',                      --COMPLETION_SUBINVENTORY,
      NULL,                           --COMPLETION_LOCATOR_ID,
      NULL,                           --ATTRIBUTE_CATEGORY,
      NULL,                           --ATTRIBUTE1,
      NULL,                           --ATTRIBUTE2,
      NULL,                           --ATTRIBUTE3,
      NULL,                           --ATTRIBUTE4,
      NULL,                           --ATTRIBUTE5,
      NULL,                           --ATTRIBUTE6,
      NULL,                           --ATTRIBUTE7,
      NULL,                           --ATTRIBUTE8,
      NULL,                           --ATTRIBUTE9,
      NULL,                           --ATTRIBUTE10,
      NULL,                           --ATTRIBUTE11,
      NULL,                           --ATTRIBUTE12,
      NULL,                           --ATTRIBUTE13,
      NULL,                           --ATTRIBUTE14,
      NULL,                           --ATTRIBUTE15,
      NULL,                           --REQUEST_ID,
      NULL,                           --PROGRAM_APPLICATION_ID,
      NULL,                           --PROGRAM_ID,
      NULL,                           --PROGRAM_UPDATE_DATE,
      NULL,                           --DEMAND_SOURCE_LINE,
      NULL,                           --SET_ID,
      0,                              --PROCESS_REVISION,
      NULL,                           --DEMAND_SOURCE_TYPE,
      NULL,                           --DEMAND_SOURCE_HEADER_ID,
      NULL,                           --ORGANIZATION_CODE,
      NULL,                           --ASSEMBLY_ITEM_NUMBER,
      NULL,                           --COMMON_ITEM_NUMBER,
      NULL,                           --LOCATION_NAME,
      NULL,                           --TRANSACTION_ID,
      1,                              --PROCESS_FLAG,
      'CREATE',                       --TRANSACTION_TYPE,
      NULL,                           --LINE_ID,
      NULL,                           --LINE_CODE,
      NULL,                           --MIXED_MODEL_MAP_FLAG,
      NULL,                           --PRIORITY,
      NULL,                           --CFM_ROUTING_FLAG,
      NULL,                           --TOTAL_PRODUCT_CYCLE_TIME,
      NULL,                           --CTP_FLAG,
      NULL,                           --ORIGINAL_SYSTEM_REFERENCE,
      NULL,                           --SERIALIZATION_START_OP,
      NULL,                           --DELETE_GROUP_NAME,
      NULL                            --DG_DESCRIPTION
    );

  -- Operation Table
  BEGIN
  ------------------------------------------------------------
  -- Inserting into BOM_OP_SEQUENCES_INTERFACE table
  ------------------------------------------------------------
  INSERT INTO BOM_OP_SEQUENCES_INTERFACE (OPERATION_SEQUENCE_ID,
      ROUTING_SEQUENCE_ID,
      OPERATION_SEQ_NUM,
      LAST_UPDATE_DATE,
      LAST_UPDATED_BY,
      CREATION_DATE,
      CREATED_BY,
      LAST_UPDATE_LOGIN,
      STANDARD_OPERATION_ID,
      DEPARTMENT_ID,
      OPERATION_LEAD_TIME_PERCENT,
      RUN_TIME_OVERLAP_PERCENT,
      MINIMUM_TRANSFER_QUANTITY,
      COUNT_POINT_TYPE,
      OPERATION_DESCRIPTION,
      EFFECTIVITY_DATE,
      CHANGE_NOTICE,
      IMPLEMENTATION_DATE,
      DISABLE_DATE,
      BACKFLUSH_FLAG,
      OPTION_DEPENDENT_FLAG,
      ATTRIBUTE_CATEGORY,
      ATTRIBUTE1,
      ATTRIBUTE2,
      ATTRIBUTE3,
      ATTRIBUTE4,
      ATTRIBUTE5,
      ATTRIBUTE6,
      ATTRIBUTE7,
      ATTRIBUTE8,
      ATTRIBUTE9,
      ATTRIBUTE10,
      ATTRIBUTE11,
      ATTRIBUTE12,
      ATTRIBUTE13,
      ATTRIBUTE14,
      ATTRIBUTE15,
      REQUEST_ID,
      PROGRAM_APPLICATION_ID,
      PROGRAM_ID,
      PROGRAM_UPDATE_DATE,
      MODEL_OP_SEQ_ID,
      ASSEMBLY_ITEM_ID,
      ORGANIZATION_ID,
      ALTERNATE_ROUTING_DESIGNATOR,
      ORGANIZATION_CODE,
      ASSEMBLY_ITEM_NUMBER,
      DEPARTMENT_CODE,
      OPERATION_CODE,
      RESOURCE_ID1,
      RESOURCE_ID2,
      RESOURCE_ID3,
      RESOURCE_CODE1,
      RESOURCE_CODE2,
      RESOURCE_CODE3,
      INSTRUCTION_CODE1,
      INSTRUCTION_CODE2,
      INSTRUCTION_CODE3,
      TRANSACTION_ID,
      PROCESS_FLAG,
      TRANSACTION_TYPE,
      NEW_OPERATION_SEQ_NUM,
      NEW_EFFECTIVITY_DATE,
      ASSEMBLY_TYPE,
      OPERATION_TYPE,
      REFERENCE_FLAG,
      PROCESS_OP_SEQ_ID,
      LINE_OP_SEQ_ID,
      YIELD,
      CUMULATIVE_YIELD,
      REVERSE_CUMULATIVE_YIELD,
      LABOR_TIME_CALC,
      MACHINE_TIME_CALC,
      TOTAL_TIME_CALC,
      LABOR_TIME_USER,
      MACHINE_TIME_USER,
      TOTAL_TIME_USER,
      NET_PLANNING_PERCENT,
      INCLUDE_IN_ROLLUP,
      OPERATION_YIELD_ENABLED,
      PROCESS_SEQ_NUMBER,
      PROCESS_CODE,
      LINE_OP_SEQ_NUMBER,
      LINE_OP_CODE,
      ORIGINAL_SYSTEM_REFERENCE,
      SHUTDOWN_TYPE,
      LONG_DESCRIPTION,
      DELETE_GROUP_NAME,
      DG_DESCRIPTION,
      NEW_ROUTING_REVISION,
      ACD_TYPE,
      OLD_START_EFFECTIVE_DATE,
      CANCEL_COMMENTS,
      ENG_CHANGES_IFCE_KEY,
      ENG_REVISED_ITEMS_IFCE_KEY,
      BOM_REV_OP_IFCE_KEY,
      NEW_REVISED_ITEM_REVISION)
    VALUES (BOM.BOM_OPERATION_SEQUENCES_S.nextval,     --OPERATION_SEQUENCE_ID,
      BOM.BOM_OPERATIONAL_ROUTINGS_S.currval,          --ROUTING_SEQUENCE_ID,
      vopseq,                     --OPERATION_SEQ_NUM,
      vddate,                     --LAST_UPDATE_DATE,
      vnUserId,                   --LAST_UPDATED_BY,
      vddate,                     --CREATION_DATE,
      vnUserId,                   --CREATED_BY,
      NULL,                       --LAST_UPDATE_LOGIN,
      NULL,                       --STANDARD_OPERATION_ID,
      NULL,                       --DEPARTMENT_ID,
      NULL,                       --OPERATION_LEAD_TIME_PERCENT,
      NULL,                       --RUN_TIME_OVERLAP_PERCENT,
      NULL,                       --MINIMUM_TRANSFER_QUANTITY,
      NULL,                       --COUNT_POINT_TYPE,
      NULL,                       --OPERATION_DESCRIPTION,
      TRUNC(SYSDATE),             --EFFECTIVITY_DATE
      NULL,                       --CHANGE_NOTICE,
      NULL,                       --IMPLEMENTATION_DATE,
      NULL,                       --DISABLE_DATE,
      NULL,                       --BACKFLUSH_FLAG,
      NULL,                       --OPTION_DEPENDENT_FLAG,
      NULL,                       --ATTRIBUTE_CATEGORY,
      NULL,                       --ATTRIBUTE1,
      NULL,                       --ATTRIBUTE2,
      NULL,                       --ATTRIBUTE3,
      NULL,                       --ATTRIBUTE4,
      NULL,                       --ATTRIBUTE5,
      NULL,                       --ATTRIBUTE6,
      NULL,                       --ATTRIBUTE7,
      NULL,                       --ATTRIBUTE8,
      NULL,                       --ATTRIBUTE9,
      NULL,                       --ATTRIBUTE10,
      NULL,                       --ATTRIBUTE11,
      NULL,                       --ATTRIBUTE12,
      NULL,                       --ATTRIBUTE13,
      NULL,                       --ATTRIBUTE14,
      NULL,                       --ATTRIBUTE15,
      NULL,                       --REQUEST_ID,
      NULL,                       --PROGRAM_APPLICATION_ID,
      NULL,                       --PROGRAM_ID,
      NULL,                       --PROGRAM_UPDATE_DATE,
      NULL,                       --MODEL_OP_SEQ_ID,
      rec_rtg.inventory_item_id,   --ASSEMBLY_ITEM_ID,
      rec_rtg.organization_id,     --ORGANIZATION_ID,
      NULL,                       --ALTERNATE_ROUTING_DESIGNATOR,
      NULL,                       --ORGANIZATION_CODE,
      NULL,                       --ASSEMBLY_ITEM_NUMBER,
      'GEN LABOR',                 --DEPARTMENT_CODE,
      NULL,                       --OPERATION_CODE,
      NULL,                       --RESOURCE_ID1,
      NULL,                       --RESOURCE_ID2,
      NULL,                       --RESOURCE_ID3,
      NULL,                       --RESOURCE_CODE1,
      NULL,                       --RESOURCE_CODE2,
      NULL,                       --RESOURCE_CODE3,
      NULL,                       --INSTRUCTION_CODE1,
      NULL,                       --INSTRUCTION_CODE2,
      NULL,                       --INSTRUCTION_CODE3,
      NULL,                       --TRANSACTION_ID,
      1,                         --PROCESS_FLAG,
      'CREATE',                    --TRANSACTION_TYPE,
      NULL,                       --NEW_OPERATION_SEQ_NUM,
      NULL,                       --NEW_EFFECTIVITY_DATE,
      NULL,                       --ASSEMBLY_TYPE,
      1,                       --OPERATION_TYPE,
      NULL,                       --REFERENCE_FLAG,
      NULL,                       --PROCESS_OP_SEQ_ID,
      NULL,                       --LINE_OP_SEQ_ID,
      NULL,                       --YIELD,
      NULL,                       --CUMULATIVE_YIELD,
      NULL,                       --REVERSE_CUMULATIVE_YIELD,
      NULL,                       --LABOR_TIME_CALC,
      NULL,                       --MACHINE_TIME_CALC,
      NULL,                       --TOTAL_TIME_CALC,
      NULL,                       --LABOR_TIME_USER,
      NULL,                       --MACHINE_TIME_USER,
      NULL,                       --TOTAL_TIME_USER,
      NULL,                       --NET_PLANNING_PERCENT,
      NULL,                       --INCLUDE_IN_ROLLUP,
      NULL,                       --OPERATION_YIELD_ENABLED,
      NULL,                       --PROCESS_SEQ_NUMBER,
      NULL,                       --PROCESS_CODE,
      NULL,                       --LINE_OP_SEQ_NUMBER,
      NULL,                       --LINE_OP_CODE,
      NULL,                       --ORIGINAL_SYSTEM_REFERENCE,
      NULL,                       --SHUTDOWN_TYPE,
      NULL,                       --LONG_DESCRIPTION,
      NULL,                       --DELETE_GROUP_NAME,
      NULL,                       --DG_DESCRIPTION,
      NULL,                       --NEW_ROUTING_REVISION,
      NULL,                       --ACD_TYPE,
      NULL,                       --OLD_START_EFFECTIVE_DATE,
      NULL,                       --CANCEL_COMMENTS,
      NULL,                       --ENG_CHANGES_IFCE_KEY,
      NULL,                       --ENG_REVISED_ITEMS_IFCE_KEY,
      NULL,                       --BOM_REV_OP_IFCE_KEY,
      NULL                       --NEW_REVISED_ITEM_REVISION
      );
  EXCEPTION
    WHEN OTHERS THEN
    fnd_file.put_line(fnd_file.log,'Error inserting into Operation Interface '||SQLERRM);
  END;

  -- Operation Resource
  ------------------------------------------------------------
  -- Inserting into BOM_OP_RESOURCES_INTERFACE table
  ------------------------------------------------------------
  BEGIN
    INSERT INTO BOM_OP_RESOURCES_INTERFACE (OPERATION_SEQUENCE_ID,
      RESOURCE_SEQ_NUM,
      RESOURCE_ID,
      ACTIVITY_ID,
      STANDARD_RATE_FLAG,
      ASSIGNED_UNITS,
      USAGE_RATE_OR_AMOUNT,
      USAGE_RATE_OR_AMOUNT_INVERSE,
      BASIS_TYPE,
      SCHEDULE_FLAG,
      LAST_UPDATE_DATE,
      LAST_UPDATED_BY,
      CREATION_DATE,
      CREATED_BY,
      LAST_UPDATE_LOGIN,
      RESOURCE_OFFSET_PERCENT,
      ASSEMBLY_ITEM_ID,
      ALTERNATE_ROUTING_DESIGNATOR,
      ORGANIZATION_ID,
      OPERATION_SEQ_NUM,
      EFFECTIVITY_DATE,
      ROUTING_SEQUENCE_ID,
      ORGANIZATION_CODE,
      ASSEMBLY_ITEM_NUMBER,
      RESOURCE_CODE,
      ACTIVITY,
      TRANSACTION_ID,
      PROCESS_FLAG,
      TRANSACTION_TYPE)
          VALUES
      (BOM.BOM_OPERATION_SEQUENCES_S.currval,                      --OPERATION_SEQUENCE_ID,
      10,                           --RESOURCE_SEQ_NUM
      NULL,                         --RESOURCE_ID,
      NULL,                         --ACTIVITY_ID,
      NULL,                         --STANDARD_RATE_FLAG,
      NULL,                         --ASSIGNED_UNITS,
      1,                            --USAGE_RATE_OR_AMOUNT,
      1,                            --USAGE_RATE_OR_AMOUNT_INVERSE,
      NULL,                         --BASIS_TYPE,
      NULL,                         --SCHEDULE_FLAG,
             vddate,                        --LAST_UPDATE_DATE,
      vnUserId,                     --LAST_UPDATED_BY,
      vddate,                        --CREATION_DATE,
      vnUserId,                     --CREATED_BY,
      NULL,                        --LAST_UPDATE_LOGIN,
      NULL,                        --RESOURCE_OFFSET_PERCENT,
      rec_rtg.inventory_item_id,    --ASSEMBLY_ITEM_ID,
      NULL,                         --ALTERNATE_ROUTING_DESIGNATOR,
      rec_rtg.organization_id,      --ORGANIZATION_ID,
      vopseq,                           --OPERATION_SEQ_NUM,
      TRUNC(SYSDATE),               --EFFECTIVITY_DATE
      BOM.BOM_OPERATIONAL_ROUTINGS_S.currval,                      --ROUTING_SEQUENCE_ID,
      NULL,                         --ORGANIZATION_CODE,
      NULL,                         --ASSEMBLY_ITEM_NUMBER,
      'FAB LABOR',                  --RESOURCE_CODE,
      NULL,                         --ACTIVITY,
      NULL,                         --TRANSACTION_ID,
      1,                            --PROCESS_FLAG,
      'CREATE');
  EXCEPTION
    WHEN OTHERS THEN
    fnd_file.put_line(fnd_file.log,'Error inserting into Operation Resource Interface '||SQLERRM);
  END;

  ------------------------------------------------------------
  -- Update if the record is successfully inserted in
  -- interface tables.
  ------------------------------------------------------------
  UPDATE xxwc.xxwc_bom_conv_tbl       xxstg
          SET status = 'S'
  WHERE 1               = 1
    AND item_number     = rec_rtg.item_number
    AND xxstg.warehouse = rec_rtg.warehouse;
END LOOP; -- Routing Header

COMMIT;
EXCEPTION
WHEN validate_only THEN
  NULL;
WHEN OTHERS THEN
   ROLLBACK;
   fnd_file.put_line(fnd_file.log,' Error In routing_conversion '||SQLERRM);
END routing_conversion;

END xxwc_bom_conversion_pkg;
/