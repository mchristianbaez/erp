/* Formatted on 2014/07/22 22:24 (Formatter Plus v4.8.8) */
CREATE OR REPLACE PACKAGE BODY apps.xxwc_cycle_count_sch_all_orgs
AS
   /******************************************************************************
      $Header XXWC_CYCLE_COUNT_SCH_ALL_ORGS $
      NAME:       XXWC_CYCLE_COUNT_SCH_ALL_ORGS
      PURPOSE:    Automate Cycle Count Scheduler program to run for all organizations

      REVISIONS:
      Ver        Date        Author                    Description
      ---------  ----------  ---------------           ------------------------------------
      1.0        6/24/2014   Dheeresh Chintala          Initial Version TMS# 20131120-00056
	  1.1        11/11/2014  Raghavendra syamanaboina   TMS# 20141002-00066 - Canada Multi org Changes
   ******************************************************************************/

   /*************************************************************************
        Procedure : Main

        PURPOSE:   This procedure spawns XXWC Cycle Count Scheduler program for each org
        Parameter:

       REVISIONS:
      Ver        Date        Author            Description
      ---------  ----------  ---------------   ------------------------------------
      1.0        6/24/2014   Dheeresh Chintala Initial Version TMS# 20131120-00056
      ************************************************************************/

   -- Global variables
   g_application     fnd_application.application_short_name%TYPE   := 'XXWC';
   g_cp_short_code   VARCHAR2 (60)             := 'XXWC_CYCLECOUNT_SCHEDULER';
                                                -- XXWC Cycle Count Scheduler
   g_dflt_email      fnd_user.email_address%TYPE
                                        := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE main (retcode OUT NUMBER, errbuf OUT VARCHAR2)
   IS
      CURSOR cur_inv_orgs (c_org_id IN NUMBER)
      IS
         SELECT   organization_id, organization_code, organization_name,
                  set_of_books_id, chart_of_accounts_id, legal_entity
             FROM org_organization_definitions
            WHERE 1 = 1
              AND operating_unit = c_org_id
              AND disable_date IS NULL
              AND organization_code NOT IN (fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'), 'MST') -- modified by Raghavendra 'WCC' replaced with Profile Value
                                   --WC Corporate Organization, WC Item Master
         --AND rownum <3    --testing for upto 7 organizations
         ORDER BY organization_code ASC;

         /*********************************************
         -- Below cursor can be used if we want add additional parameter P_region and run this program a any particualr region
         /*********************************************/
         /*    CURSOR cur_inv_orgs (n_org_id IN NUMBER) IS
            SELECT ood.organization_id,
                   ood.organization_code,
                   ood.organization_name,
                   ood.set_of_books_id,
                   ood.chart_of_accounts_id,
                   ood.legal_entity,
                   mp.attribute9 region
              FROM org_organization_definitions ood,
                   mtl_parameters mp
             WHERE 1 = 1
               AND ood.operating_unit = n_org_id
               AND ood.disable_date IS NULL
               AND ood.organization_code NOT IN ('WCC', 'MST') --WC Corporate Organization, WC Item Master
               AND ood.organization_id = mp.organization_id
               AND mp.attribute9 = nvl(p_region,mp.attribute9)
          ORDER BY ood.organization_code ASC;
      */
      TYPE l_inv_orgs_tbl IS TABLE OF cur_inv_orgs%ROWTYPE
         INDEX BY BINARY_INTEGER;

      l_inv_orgs_rec     l_inv_orgs_tbl;
      l_request_data     VARCHAR2 (20)                 := '';
      l_request_id       NUMBER                        := 0;
      l_org_id           NUMBER                        := 0;
      l_user_id          NUMBER                        := 0;
      l_email            fnd_user.email_address%TYPE   := NULL;
      l_cp_long_name     VARCHAR2 (100)                := NULL;
      l_cp_phase         VARCHAR2 (40)                 := NULL;
      l_cp_status        VARCHAR2 (40)                 := NULL;
      l_child_requests   VARCHAR2 (240)                := NULL;
      v_request_id       NUMBER;
      output_file_id     UTL_FILE.file_type;
      v_path             VARCHAR2 (80);
      p_period_id        NUMBER                        := 0;
      l_sec              VARCHAR2 (100);
   BEGIN
      l_sec := 'Start of Procedure MAIN';
      l_request_data := fnd_conc_global.request_data;

      IF l_request_data IS NULL
      THEN
         l_request_data := '1';
         l_org_id := mo_global.get_current_org_id;
         l_user_id := fnd_global.user_id;
         fnd_file.put_line (fnd_file.LOG, '');
         fnd_file.put_line (fnd_file.LOG, 'l_org_id =' || l_org_id);
         fnd_file.put_line (fnd_file.LOG, 'n_user_id =' || l_user_id);
         fnd_file.put_line (fnd_file.LOG, '');
         l_sec := 'Before opening the cursor - CUR_INV_ORGS';

         OPEN cur_inv_orgs (l_org_id);

         FETCH cur_inv_orgs
         BULK COLLECT INTO l_inv_orgs_rec;

         CLOSE cur_inv_orgs;

         IF l_inv_orgs_rec.COUNT = 0
         THEN
            fnd_file.put_line
               (fnd_file.LOG,
                'No active inventory organizations found for operating unit WhiteCap'
               );
         ELSE                                        --l_inv_orgs_rec.count >0
            l_sec := 'Trigger Cycle Count Scheduler for every Inv Org';

            -- We will kick off for every single inventory orgs and not wait to check how many are running at a time
            FOR indx IN 1 .. l_inv_orgs_rec.COUNT
            LOOP
               fnd_file.put_line (fnd_file.LOG,
                                     'Org IDs - '
                                  || l_inv_orgs_rec (indx).organization_id
                                 );
               l_sec :=
                     'Before submitting the concurrent program - '
                  || g_cp_short_code;
               l_request_id :=
                  fnd_request.submit_request
                            (application      => g_application,
                             program          => g_cp_short_code,
                             description      => '',
                             start_time       => '',
                             sub_request      => TRUE,
                             argument1        => l_inv_orgs_rec (indx).organization_id
                            );
               COMMIT;
               l_sec :=
                     'After submitting the concurrent program - '
                  || g_cp_short_code;
               fnd_file.put_line (fnd_file.LOG,
                                  'Request_id: ' || v_request_id);

               IF l_request_id > 0
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Submitted XXWC Cycle Count Scheduler for organization ='
                      || l_inv_orgs_rec (indx).organization_name
                      || ', Request Id ='
                      || l_request_id
                     );
               ELSE
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Failed to submit XXWC Cycle Count Scheduler for organization ='
                      || l_inv_orgs_rec (indx).organization_name
                     );
                  fnd_file.put_line (fnd_file.LOG,
                                     SQLCODE || ' Error :' || SQLERRM
                                    );
               END IF;

               fnd_conc_global.set_req_globals (conc_status       => 'PAUSED',
                                                request_data      => l_request_data
                                               );
               l_request_data := TO_CHAR (TO_NUMBER (l_request_data) + 1);
            END LOOP;
         END IF;                        -- checking if l_inv_orgs_rec.count =0
      /*********************
      Start Comment

         else

              Begin
                  select a.user_concurrent_program_name
                        ,c.email_address
                  into   v_cp_long_name
                        ,v_email
                  from fnd_amp_requests_v a
                      ,fnd_concurrent_programs d
                      ,fnd_user c
                  where 1 =1
                    and a.request_id =l_req_id
                    and a.concurrent_program_id =d.concurrent_program_id
                    and a.requested_by =c.user_id;

              Exception
               When Others Then
                 Null;
              End;

            retcode :=0;
            errbuf :='Child request[s] completed. Exit XXWC Cycle Count Scheduler -Master';
            fnd_file.put_line(fnd_file.log,errbuf);
            --return;
      **********************/
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => 'XXWC_CYCLE_COUNT_SCH_ALL_ORGS.MAIN',
             p_calling                => l_sec,
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SUBSTR (SQLERRM, 1, 2000),
             p_error_desc             => 'Error in XXWC Cycle Count Scheduler -Main procedure, When Others Exception',
             p_distribution_list      => g_dflt_email,
             p_module                 => 'XXWC'
            );
         fnd_file.put_line
            (fnd_file.LOG,
                'Error in caller apps.XXWC_INV_CYCLE_COUNT_PKG.SUBMIT_SCHEDULER ='
             || SQLERRM
            );
   -- rollback;
   END main;
END xxwc_cycle_count_sch_all_orgs;
/