CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_EXP_CORRECT_PKG
AS
   /*************************************************************************
     $Header XXWC_INV_EXP_CORRECT_PKG.pkb $
     Module Name: XXWC_INV_EXP_CORRECT_PKG

     PURPOSE: This Package will create the Reversal Miscelleneious
                 transaction receipts
     ESMS Task Id :   20140717-00061

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        07/21/2014  Pattabhi Avula        Initial Version
     

   **************************************************************************/
   g_start               NUMBER;
   g_program_status      BOOLEAN;
   g_retcode             VARCHAR2(1);
   g_distribution_list   fnd_user.email_address%TYPE
                            := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE create_correction_txn (
      errbuff                 OUT  NOCOPY   VARCHAR2,
      retcode                 OUT  NOCOPY VARCHAR2,
      p_requisition_num       IN     po_requisition_headers_all.segment1%TYPE,
      p_iso_num               IN     oe_order_headers_all.order_number%TYPE,
      p_dest_org              IN     mtl_parameters.organization_code%TYPE,
      p_item_num              IN     mtl_system_items_b.segment1%TYPE     
                                   )
   /*************************************************************************
     $Header create_correction_txn $
     Module Name: create_correction_txn

     PURPOSE: Procedure to do Miscellaneous Transaction Receipt creation
              for Internal IR's
     ESMS Task Id :  20140717-00061

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        07/21/2014  Pattabhi Avula        Initial Version
          
   **************************************************************************/
   IS
      l_transaction_type_name        VARCHAR2 (50) := 'Miscellaneous Recpt(RG Update)';
      l_sec                          VARCHAR2 (100);
      l_transaction_type_id          NUMBER;
      l_transaction_source_type_id   NUMBER;
      l_transaction_source_id        NUMBER;
      l_transaction_source_code      VARCHAR2 (40);
      l_process_flag                 NUMBER := 1;
      l_transaction_mode             NUMBER := 2;
      l_lock_flag                    NUMBER := 3;
      l_sysdate                      DATE := SYSDATE;
      l_result                       BOOLEAN;
      l_start                        BOOLEAN;
      l_err_code                     VARCHAR2 (100);
      l_err_desc                     VARCHAR2 (4000);
      l_err_msg                      VARCHAR2 (4000);
      l_subinventory_code            VARCHAR2 (10):='General';
      l_code_combination_id          NUMBER;
      l_item_exp_code                VARCHAR2 (10);
      l_req_header_id                NUMBER;
      l_user_id                      NUMBER :=FND_GLOBAL.USER_ID;
      l_count                        NUMBER:=0;
      
    
      
      
      CURSOR cur_misc_txns
       IS
         SELECT   prh.segment1 req_num,
                  prh.requisition_header_id,         
                  prl.destination_type_code,
                  prl.line_num,
                  prl.destination_organization_id,                  
                  prl.item_id,
                  prl.requisition_line_id,
                  prl.unit_price,
                  primary_uom_code uom, 
                  mp_to.organization_code  dest_branch,                  
                  mp_to.expense_account,
                  msi.segment1 item_num,
                  prl.quantity_delivered req_del_qty,
                  oeh.order_number                  
        FROM      apps.po_requisition_headers_all prh,
                  apps.po_requisition_lines_all prl,
                  apps.PO_REQ_DISTRIBUTIONS_all prdl,
                  apps.oe_order_lines_all oel,
                  apps.oe_order_headers_all oeh,
                  apps.mtl_system_items_b msi,
                  apps.mtl_parameters mp_from,
                  apps.mtl_parameters mp_to
        WHERE     prh.requisition_header_id = prl.requisition_header_id
        AND       prl.requisition_line_id = oel.SOURCE_DOCUMENT_LINE_ID
        AND       prl.requisition_line_id = prdl.requisition_line_id 
        AND       oel.header_id = oeh.header_id
        AND       prh.TYPE_LOOKUP_CODE = 'INTERNAL'
        AND       NVL(prl.cancel_flag,'N') = 'N'
        AND       prl.item_id = msi.inventory_item_id
        AND       msi.organization_id = prl.destination_organization_id 
        AND       prl.destination_organization_id = mp_to.organization_id
        AND       prl.source_organization_id = mp_from.organization_id
        AND       prh.segment1 = p_requisition_num
        AND       oeh.order_number=p_iso_num
        AND       mp_to.organization_code=p_dest_org
        AND       msi.segment1=p_item_num;
        
                
  BEGIN
  

   /*=========================================================================
        Main Program block begins
    =========================================================================*/
           BEGIN
             l_sec := 'Get Transaction Type';
             
           SELECT  transaction_type_id, transaction_source_type_id
             INTO    l_transaction_type_id, l_transaction_source_type_id
             FROM    mtl_transaction_types
             WHERE   transaction_type_name = l_transaction_type_name;    
           EXCEPTION
             WHEN OTHERS
             THEN
               l_err_msg := l_err_msg || ' ' || l_sec || ' ' || SQLERRM;
               DBMS_OUTPUT.PUT_LINE ('Exception : ' || l_err_msg);
           END;    
          -- FND_FILE.PUT_LINE (FND_FILE.LOG,'Getting the Transaction type id and source type id is:'||l_transaction_type_id||' '||l_transaction_source_type_id);
            
           /*===============================================================
                If Item type as Expense then Inserting into interfac table
            ================================================================*/
            --FND_FILE.PUT_LINE (FND_FILE.LOG,'Inserting data into Interface table');
              
            FOR rec_misc_txn IN cur_misc_txns
            LOOP
              l_req_header_id:=rec_misc_txn.requisition_header_id;
              
               IF rec_misc_txn.destination_type_code<>'EXPENSE' THEN
                 FND_FILE.PUT_LINE (FND_FILE.LOG,'Entered IR line is not an Expense Type:'||p_requisition_num||' '||'and line Num:'||rec_misc_txn.line_num);
                 l_err_msg := l_err_msg || ' ' || l_sec || ' ' || SQLERRM;
                 DBMS_OUTPUT.PUT_LINE ('Exception : ' || l_err_msg);
                 g_retcode:= '2';
                 retcode:= g_retcode;
                 g_program_Status := FND_CONCURRENT.SET_COMPLETION_STATUS('ERROR','Selected IR/Item/ISO line is not an Expense Type');
               ELSE 
               
                 BEGIN
                   SELECT COUNT(*)
                   INTO   l_count
                   FROM   mtl_material_transactions
                   WHERE  inventory_item_id=rec_misc_txn.item_id
                   AND    organization_id=rec_misc_txn.destination_organization_id
                   AND    transaction_quantity=rec_misc_txn.req_del_qty
                   AND    attribute7=rec_misc_txn.req_num;
                 EXCEPTION
                  WHEN OTHERS THEN
                  l_err_msg := l_err_msg || ' ' || l_sec || ' ' || SQLERRM;
                  DBMS_OUTPUT.PUT_LINE ('Exception : ' || l_err_msg);
                 END;
			   END IF;
                
                        IF l_count <>0 THEN
                          FND_FILE.PUT_LINE (FND_FILE.LOG,'Correction has been done earlier for the selected IR/Item/Quantity:'||p_requisition_num||' '||'and line Num:'||rec_misc_txn.line_num);
                          l_err_msg := l_err_msg || ' ' || l_sec || ' ' || SQLERRM;
                          DBMS_OUTPUT.PUT_LINE ('Exception : ' || l_err_msg);
                          g_retcode:= '2';
                          retcode:= g_retcode;
                          g_program_Status := FND_CONCURRENT.SET_COMPLETION_STATUS('ERROR','Correction has been done earlier for the selected IR/Item/Quantity');
                        ELSE
                        
                         BEGIN
                           INSERT
                              INTO mtl_transactions_interface 
                                     (transaction_header_id,
                                       transaction_type_id,
                                       transaction_source_type_id,
                                       transaction_source_id,
                                       source_code,
                                       process_flag,
                                       transaction_mode,
                                       lock_flag,
                                       created_by,
                                       creation_date,
                                       last_updated_by,
                                       last_update_date,
                                       source_header_id,
                                       source_line_id,
                                       inventory_item_id,
                                       organization_id,  
                                       transaction_quantity,
                                       transaction_uom,
                                       transaction_date,
                                       transaction_cost,
                                       subinventory_code,
                                       distribution_account_id,
                                       attribute8
                                      )                                                       
                               VALUES (l_req_header_id,
                                       l_transaction_type_id,
                                       l_transaction_source_type_id,
                                       NULL,
                                       rec_misc_txn.destination_type_code,
                                       l_process_flag,
                                       l_transaction_mode,
                                       l_lock_flag,
                                       l_user_id,
                                       l_sysdate,
                                       l_user_id,
                                       l_sysdate,
                                       rec_misc_txn.requisition_header_id,
                                       rec_misc_txn.requisition_line_id,
                                       rec_misc_txn.item_id,
                                       rec_misc_txn.destination_organization_id,  
                                       rec_misc_txn.req_del_qty,
                                       rec_misc_txn.uom,
                                       l_sysdate,
                                       rec_misc_txn.unit_price,
                                       l_subinventory_code,
                                       rec_misc_txn.expense_account,
                                       rec_misc_txn.req_num
                                      ); 
                                                        
                         EXCEPTION
                              WHEN OTHERS
                              THEN
                              l_err_msg := l_err_msg || ' ' || l_sec || ' ' || SQLERRM;
                              DBMS_OUTPUT.PUT_LINE ('Exception : ' || l_err_msg);
                         END;
                  
                         FND_FILE.PUT_LINE (FND_FILE.LOG,'Data Successfully have been Inserted into Interface table');
                        END IF;
            END LOOP;
              
              
               COMMIT;
             
             -- FND_FILE.PUT_LINE (FND_FILE.LOG,'Loop header id getting at line -265 is:'||l_req_header_id);
            /*===============================================================
                       Calling API to Process Misc Transaction
            ================================================================*/
           -- FND_FILE.PUT_LINE (FND_FILE.LOG,'Calling API to Process the Interface data');  
            BEGIN
                  l_sec := 'Process Miscellaneous Transaction';
                             
                  l_result :=
                     mtl_online_transaction_pub.process_online (l_req_header_id,
                                                                NULL,
                                                                l_err_code,
                                                                l_err_desc);
                  DBMS_OUTPUT.PUT_LINE ('Err code ' || l_err_code);
                  DBMS_OUTPUT.PUT_LINE ('Err desc ' || l_err_desc);
              END;
              /*===============================================================
                            Checking the API Result
                ================================================================*/
           -- FND_FILE.PUT_LINE (FND_FILE.LOG,'Checking the API Result:');
                                                
              IF l_result = TRUE
               THEN
                  l_sec := 'Misc Transaction Success';
                FND_FILE.PUT_LINE (FND_FILE.LOG,'Miscellaneous Receipt have been created Successfully for selected IR/Item/ISO');                                            
              ELSE
              g_retcode:= '2';
              retcode:= g_retcode;
              g_program_Status := FND_CONCURRENT.SET_COMPLETION_STATUS('ERROR','See log file for detail');
              
                  l_err_msg :=
                        l_err_desc
                     || ' '
                     || l_err_msg
                     || ' '
                     || l_sec
                     || ' '
                     || SQLERRM;
                  DBMS_OUTPUT.PUT_LINE ('Exception : ' || l_err_msg);
                  
                  FND_FILE.PUT_LINE (FND_FILE.LOG,'Miscellaneous Receipt creation has not Successful for selected IR/Item/ISO');
                  FND_FILE.PUT_LINE (FND_FILE.LOG,'API Result Error Message is:'||' '||l_err_msg);
              END IF;
              l_sec := 'Misc Transaction not Success';
              
                 xxcus_error_pkg.xxcus_error_main_api (
                 p_called_from         => 'XXWC_INV_EXP_CORRECT_PKG.create_correction_txn',
                 p_calling             => l_sec,
                 p_request_id          => NULL,
                 p_ora_error_msg       => SUBSTR (
                                           ' Error_Stack...'
                                          || DBMS_UTILITY.format_error_stack ()
                                          || ' Error_Backtrace...'
                                          || DBMS_UTILITY.format_error_backtrace (),
                                          1,
                                          2000),
                 p_error_desc          => SUBSTR (l_err_desc,
                                                  1,
                                                  240),
                 p_distribution_list   => g_distribution_list,
                 p_module              => 'INV');
   END create_correction_txn;
 END XXWC_INV_EXP_CORRECT_PKG;
/
