CREATE OR REPLACE
PACKAGE BODY APPS.XXWC_EGO_METADATA_UPLOAD_PKG AS
/********************************************************************************
File Name				: XXWC_EGO_METADATA_UPLOAD_PKG
PROGRAM TYPE			: PL/SQL Package body
PURPOSE					: Procedures and functions for uploading the AG metadata load via Web ADI to EBS Default Interface tables
-- Dependencies Objecs 	: EGO_ATTR_GROUPS_INTERFACE --> EGO_ATTR_GROUPS_V
						  EGO_ATTR_GROUPS_DL_INTERFACE --> EGO_ATTR_GROUPS_DL_V
						  EGO_ATTR_GROUP_COLS_INTF --> EGO_ATTRS_V
						  EGO_ATTR_GRPS_ASSOC_INTERFACE --> EGO_OBJ_ATTR_GRP_ASSOCS_V
						  EGO_PAGES_INTERFACE --> EGO_PAGES_V
						  EGO_PAGE_ENTRIES_INTERFACE --> EGO_PAGE_ENTRIES_V
--Pre Processing		: Related VS and ICCs should be already populated in Interface tables or should be available in EBS main tables

-- Post Processing 		: Call the CP: EGO Import Metadata(EGOIMDCP - N, Y, Y, 1111, N) to load the data from Interface table to Main tables

-- Error Verification Table : mtl_interface_errors

HISTORY		:
==================================================================================
VERSION DATE          AUTHOR(S)       		  DESCRIPTION
------- -----------   ----------------------  -----------------------------------------
1.0     07-JUN-2013   Rajasekar Gunasekaran	  Initial Version
1.1     12-JUN-2013   Rajasekar Gunasekaran	  Updated with unit testing Fixes and few enhancements
1.2     17-JUN-2013   Rajasekar Gunasekaran	  removed the trimming from __ to _ for AG internal name,  attr internal name and item page internal name
1.3     10-Nov-2014   Maharajan Shunmugam         TMS# 20141001-00259 Canada Multi Org changes 
*********************************************************************************/

	FUNCTION LOAD_AG_TO_INTRF 
	(
	--AG Data Params
	P_AG_TYPE					VARCHAR2 DEFAULT 'EGO_ITEMMGMT_GROUP'
	,P_AG_NAME					VARCHAR2-- not null
	,P_AG_DISP_NAME				VARCHAR2-- not null
	,P_AG_DESCRIPTION			VARCHAR2 DEFAULT NULL
	,P_AG_MULTI_ROW_FLAG		VARCHAR2 DEFAULT 'N' -- Y/N ego_ef_multi_row
	,P_AG_VARIANT_FLAG			VARCHAR2 DEFAULT 'N' -- Y/N
	,P_AG_NUM_OF_ROWS 			NUMBER DEFAULT NULL-- null for SR
	,P_AG_NUM_OF_COLS			NUMBER DEFAULT 2 
	--AG Data Level Params
	,P_AG_DATA_LEVEL_NAME		VARCHAR2 DEFAULT 'ITEM_LEVEL' -- EGO_EF_DATA_LEVEL = ITEM_LEVEL, ITEM_ORG, COMPONENTS_LEVEL
	,P_AG_USER_VIEW_PRIV_NAME	VARCHAR2 DEFAULT NULL
	,P_AG_USER_EDIT_PRIV_NAME	VARCHAR2 DEFAULT NULL
	,P_AG_PRE_EVENT_FLAG		VARCHAR2 DEFAULT 'N' -- Y/N ego_yes_no
	,P_AG_POST_EVENT_FLAG		VARCHAR2 DEFAULT 'N'-- Y/N ego_yes_no
	-- ICC Level Params
	,P_ICC_NAME					VARCHAR2 -- not null
	,P_ICC_PAGE_NAME			VARCHAR2 DEFAULT NULL
	,P_ICC_PAGE_SEQ				NUMBER
	,P_ICC_PAGE_AG_SEQ			NUMBER
	--Attribute Params
	,P_ATTR_INTERNAL_NAME		VARCHAR2-- not null
	,P_ATTR_DISPLAY_NAME		VARCHAR2-- not null
	,P_ATTR_DESCRIPTION			VARCHAR2 DEFAULT NULL
	,P_ATTR_SEQUENCE			NUMBER
	,P_ATTR_DB_COLUMN_NAME 		VARCHAR2 -- not null --C_EXT_ATTR1 to 40, N_EXT_ATTR1 to 20, D_EXT_ATTR1 to 10, TL_EXT_ATTR1 to 40
	,P_ATTR_DATA_TYPE			VARCHAR2 DEFAULT 'C'--  C-Char, X-Standard Date, Y- Standard DateTime, A- Translatable Text, N- Number
	,P_ATTR_UNIQUE_KEY_FLAG		VARCHAR2 DEFAULT 'N'--Flag to indicate if this attribute is a part of Unique Key in case of Multi Row Attribute group
	,P_ATTR_INFO_1				VARCHAR2 DEFAULT NULL--URL incase of attribute type Dynamic URL
	,P_ATTR_UOM_CLASS			VARCHAR2 DEFAULT NULL-- Unit Of Measure in case of Number Attribute
	,P_ATTR_ENABLED_FLAG		VARCHAR2 DEFAULT 'Y' -- Y/N - Flag to indicate if the descriptive flexfield segment is enabled ego_ef_enabled_flag
	,P_ATTR_REQUIRED_FLAG		VARCHAR2 DEFAULT 'N' --Y/N - Flag to indicate whether a value must be entered for this segment
	,P_ATTR_DISPLAY_CODE		VARCHAR2 DEFAULT 'T' -- Values correspond to the lookup_type = EGO_EF_DISPLAY_TYPE Allowed values are 
													/* C - Checkbox,D - Dynamic URL,H - Hidden,L - Text Area,R - Radio Group,S - Static URL,T - Text field"*/
	,P_ATTR_DEFAULT_VALUE		VARCHAR2 DEFAULT NULL
	,P_ATTR_FLEX_VALUE_SET_NAME VARCHAR2 DEFAULT NULL
	--general Params
	,P_TRANSACTION_TYPE			VARCHAR2 DEFAULT 'SYNC' -- CREATE, UPDATE, SYNC, DELETE
	,P_INT_NAME_AUTO_GEN_VAL	VARCHAR2 DEFAULT NULL -- if internal names are not provided and this val is not null, then it will auto generate it
	) 
	RETURN VARCHAR2
	IS
/*	
SET_PROCESS_ID --Unique identifier for the subset of data. Each of the lines that are newly uploaded in the interface table, need to be given a unique ID, that will be used during the Concurrent program request submission. It is suggested to retrieve the values from EGO_IPI_DATASET_ID_S.
	DEFAULTING --Indicates the behavior of the Data Level across Style and SKU Items. (Takes values , D, I or NULL

	--,P_ATTR_CONTROL_LEVEL--Used to determine whether all the attribute properties of the attribute are updateble or not
	--,P_ATTR_VIEW_IN_HIERARCHY_CODE--A-Determines the extent to which this Attribute is viewable in whatever hierarchy is defined for the customer object (e.g., the Item or Project or Change). Only used by some customer objects.
	--,P_ATTR_EDIT_IN_HIERARCHY_CODE --A-Determines the extent to which this Attribute is editable in whatever hierarchy is defined for the customer object (e.g., the Item or Project or Change). Only used by some customer objects.
	--,P_ATTR_CUSTOMIZATION_LEVEL--A-Determines if the attribute can be customized
	--,P_ATTR_READ_ONLY_FLAG--Indicates Attribute should be read only in AG region or not.
	--,P_ATTR_SECURITY_ENABLED_FLAG
	--,P_ATTR_ATTRIBUTE_CODE
	--,P_ATTR_MAXIMUM_DESCRIPTION_LEN--The maximum size of the segment value description in flexfield window
	--,P_ATTR_CONCATENATION_DESCRIPTION_LEN--The number of characters in the segment value description
	--,P_ATTR_TRANSACTION_ID--Unique identifier for the line. It is suggested to retrieve the value for this from mtl_system_items_interface_s.
	--,P_ATTR_PROCESS_STATUS--Indicates the status of the line. Valid values are : 1 (To be processed), 3 (Error), 7 (Success). 

	TRANSACTION_ID -- -- SELECT mtl_system_items_interface_s.NEXTVAL FROM DUAL 

*/	

	--Declare and Assign the formatted value
	L_AG_TYPE					VARCHAR2(100) := TRIM(REPLACE(P_AG_TYPE,'"',''));
	--L_AG_NAME					VARCHAR2(100) := REPLACE(REPLACE(TRIM(REPLACE(REPLACE(P_AG_NAME,'"',''),'  ',' ')),' ','_'),'__','_');
	L_AG_NAME					VARCHAR2(100) := REPLACE(TRIM(REPLACE(REPLACE(P_AG_NAME,'"',''),'  ',' ')),' ','_'); -- <Raj 17-jun> removed the replace() for __ to _
	L_AG_DISP_NAME				VARCHAR2(100) := TRIM(REPLACE(REPLACE(P_AG_DISP_NAME,'"',''),'  ',' '));
	L_AG_DESCRIPTION			VARCHAR2(1000) := TRIM(REPLACE(REPLACE(P_AG_DESCRIPTION,'"',''),'  ',' '));
	L_AG_MULTI_ROW_FLAG			VARCHAR2(10) := TRIM(REPLACE(P_AG_MULTI_ROW_FLAG,'"',''));
	L_AG_VARIANT_FLAG			VARCHAR2(10) := TRIM(REPLACE(P_AG_VARIANT_FLAG,'"',''));
	L_AG_NUM_OF_ROWS 			NUMBER := P_AG_NUM_OF_ROWS;
	L_AG_NUM_OF_COLS			NUMBER := P_AG_NUM_OF_COLS;
	--AG Data Level Params
	L_AG_DATA_LEVEL_NAME		VARCHAR2(50) := TRIM(REPLACE(P_AG_DATA_LEVEL_NAME,'"',''));
	L_AG_USER_VIEW_PRIV_NAME	VARCHAR2(100) := TRIM(REPLACE(P_AG_USER_VIEW_PRIV_NAME,'"',''));
	L_AG_USER_EDIT_PRIV_NAME	VARCHAR2(100) := TRIM(REPLACE(P_AG_USER_EDIT_PRIV_NAME,'"',''));
	L_AG_PRE_EVENT_FLAG			VARCHAR2(10) := TRIM(REPLACE(P_AG_PRE_EVENT_FLAG,'"',''));
	L_AG_POST_EVENT_FLAG		VARCHAR2(10) := TRIM(REPLACE(P_AG_POST_EVENT_FLAG,'"',''));
	-- ICC Level Params
	L_ICC_NAME					VARCHAR2(100) := TRIM(REPLACE(REPLACE(P_ICC_NAME,'"',''),'  ',' '));
	L_ICC_PAGE_NAME				VARCHAR2(200) := TRIM(REPLACE(P_ICC_PAGE_NAME,'"',''));
	L_ICC_PAGE_INT_NAME			VARCHAR2(200) := NULL; --<Raj 17-Jun> added for maintaining the page internal name
	L_ICC_PAGE_SEQ				NUMBER := P_ICC_PAGE_SEQ;
	L_ICC_PAGE_AG_SEQ			NUMBER := P_ICC_PAGE_AG_SEQ;
	--Attribute Params
	--L_ATTR_INTERNAL_NAME		VARCHAR2(100) := REPLACE(REPLACE(TRIM(REPLACE(REPLACE(P_ATTR_INTERNAL_NAME,'"',''),'  ',' ')),' ','_'),'__','_');
	L_ATTR_INTERNAL_NAME		VARCHAR2(100) := REPLACE(TRIM(REPLACE(REPLACE(P_ATTR_INTERNAL_NAME,'"',''),'  ',' ')),' ','_');-- <Raj 17-jun> removed the replace() for __ to _
	L_ATTR_DISPLAY_NAME			VARCHAR2(100) := TRIM(REPLACE(REPLACE(P_ATTR_DISPLAY_NAME,'"',''),'  ',' '));
	L_ATTR_DESCRIPTION			VARCHAR2(1000) := TRIM(REPLACE(REPLACE(P_ATTR_DESCRIPTION,'"',''),'  ',' '));
	L_ATTR_SEQUENCE				NUMBER := P_ATTR_SEQUENCE;
	L_ATTR_DB_COLUMN_NAME 		VARCHAR2(200) := TRIM(REPLACE(P_ATTR_DB_COLUMN_NAME,'"',''));
	L_ATTR_DATA_TYPE			VARCHAR2(20) := TRIM(REPLACE(P_ATTR_DATA_TYPE,'"',''));
	L_ATTR_UNIQUE_KEY_FLAG		VARCHAR2(10) := TRIM(REPLACE(P_ATTR_UNIQUE_KEY_FLAG,'"',''));
	L_ATTR_INFO_1				VARCHAR2(100) := TRIM(REPLACE(P_ATTR_INFO_1,'"',''));
	L_ATTR_UOM_CLASS			VARCHAR2(100) := TRIM(REPLACE(P_ATTR_UOM_CLASS,'"',''));
	L_ATTR_ENABLED_FLAG			VARCHAR2(10) := TRIM(REPLACE(P_ATTR_ENABLED_FLAG,'"',''));
	L_ATTR_REQUIRED_FLAG		VARCHAR2(10) := TRIM(REPLACE(P_ATTR_REQUIRED_FLAG,'"',''));
	L_ATTR_DISPLAY_CODE			VARCHAR2(30) := TRIM(REPLACE(P_ATTR_DISPLAY_CODE,'"',''));
	L_ATTR_DEFAULT_VALUE		VARCHAR2(1000) := TRIM(REPLACE(P_ATTR_DEFAULT_VALUE,'"',''));
	L_ATTR_VS_NAME 				VARCHAR2(100) := TRIM(REPLACE(P_ATTR_FLEX_VALUE_SET_NAME,'"',''));
	--general Params
	L_TRANSACTION_TYPE			VARCHAR2(10) := TRIM(REPLACE(P_TRANSACTION_TYPE,'"',''));
	L_INT_NAME_AUTO_GEN_VAL  	VARCHAR2(10) := TRIM(REPLACE(P_INT_NAME_AUTO_GEN_VAL,'"',''));
	
	
	L_SET_PROCESS_ID NUMBER := 1111;
	L_PROCESS_STATUS NUMBER := 1;
	L_TRANSACTION_ID NUMBER;
	L_APPLICATION_ID NUMBER := 431;
	
	l_error_message   VARCHAR2(4000) := '';
	l_raise_error	VARCHAR2(2) := 'N';
	l_validation_num NUMBER := 0;
	
        BEGIN
	
	/*.............................. validation code for AG IN params .............START................*/
	
		--1. validation for AG Disp name mandatory Check
		IF (L_AG_DISP_NAME IS NULL) THEN
			l_validation_num := l_validation_num + 1;
			l_error_message := l_error_message || l_validation_num||' Attribute Group Display Name is Mandatory. ' || CHR(10);
			l_raise_error := 'Y';
		END IF;

		--2. validation for AG Internal name mandatory Check
		IF (L_AG_NAME IS NULL AND L_INT_NAME_AUTO_GEN_VAL IS NULL) THEN
			l_validation_num := l_validation_num + 1;
			l_error_message := l_error_message || l_validation_num||' Either Attribute Group Internal Name should be provided or the Auto Generation Prefix should be there for the row: '||L_AG_DISP_NAME|| CHR(10);
			l_raise_error := 'Y';
		END IF;

		--3. validation for AG Disp name Length Check
		IF (LENGTH(L_AG_DISP_NAME) > 40) THEN
			l_validation_num := l_validation_num + 1;		
			l_error_message := l_error_message || l_validation_num||'. Attribute Group Display Name: '||L_AG_DISP_NAME||' should not be greater than 40 char. '|| CHR(10);
			l_raise_error := 'Y';
		END IF;

		--4. validation for AG name Length Check
		IF (L_AG_NAME IS NULL AND L_INT_NAME_AUTO_GEN_VAL IS NOT NULL) THEN
			IF (LENGTH(L_AG_DISP_NAME) + LENGTH(L_INT_NAME_AUTO_GEN_VAL) > 27) THEN
				l_validation_num := l_validation_num + 1;		
				l_error_message := l_error_message || l_validation_num||'. The combination of Ag Disp name and Prefix should not be greater than 27 char'|| CHR(10);
				l_raise_error := 'Y';
			END IF;
		ELSIF (L_AG_NAME IS NOT NULL AND LENGTH(L_AG_NAME) > 30) THEN
			l_validation_num := l_validation_num + 1;		
			l_error_message := l_error_message || l_validation_num||'. The AG Internal name should not be greater than 30 char'|| CHR(10);
			l_raise_error := 'Y';		
		END IF;	
		


	   -- 5. Validation for ICC name to associate the AG
		IF (L_ICC_NAME IS NULL) THEN
			l_validation_num := l_validation_num + 1;		
			l_error_message := l_error_message || l_validation_num||'. ICC Name is Mandatory for association. '|| CHR(10);
			l_raise_error := 'Y';
		END IF;

	   -- 6. Validation for Attribute Display name mandatory check
		IF (L_ATTR_DISPLAY_NAME IS NULL) THEN
			l_validation_num := l_validation_num + 1;		
			l_error_message := l_error_message || l_validation_num||'. Attribute Display name is Mandatory.  '|| CHR(10);
			l_raise_error := 'Y';
		END IF;

	   -- 7. Validation for Attribute display name length check
		IF (LENGTH(L_ATTR_DISPLAY_NAME) > 80) THEN
			l_validation_num := l_validation_num + 1;		
			l_error_message := l_error_message || l_validation_num||'. Attribute Display Name: '||L_ATTR_DISPLAY_NAME||' should not be greater than 80 char. '|| CHR(10);
			l_raise_error := 'Y';
		END IF;	 

		--8. validation for Attribute Internal name mandatory Check
		IF (L_ATTR_INTERNAL_NAME IS NULL AND L_INT_NAME_AUTO_GEN_VAL IS NULL) THEN
			l_validation_num := l_validation_num + 1;		
			l_error_message := l_error_message || l_validation_num||'. Either Attribute Internal Name should be provided or the Auto Generation Prefix should be there for the row: '||L_ATTR_DISPLAY_NAME|| CHR(10);
			l_raise_error := 'Y';
		END IF;		

		--9. validation for Attribute internal name Length Check
		IF (L_ATTR_INTERNAL_NAME IS NULL AND L_INT_NAME_AUTO_GEN_VAL IS NOT NULL) THEN
			IF (LENGTH(L_ATTR_DISPLAY_NAME) + LENGTH(L_INT_NAME_AUTO_GEN_VAL) > 25) THEN
				l_validation_num := l_validation_num + 1;		
				l_error_message := l_error_message || l_validation_num||'. The combination of Attribute Disp name and Prefix should not be greater than 25 char'|| CHR(10);
				l_raise_error := 'Y';
			END IF;
		ELSIF (L_ATTR_INTERNAL_NAME IS NOT NULL AND LENGTH(L_ATTR_INTERNAL_NAME) > 30) THEN
			l_validation_num := l_validation_num + 1;		
			l_error_message := l_error_message || l_validation_num||'. The Attribute Internal name should not be greater than 30 char'|| CHR(10);
			l_raise_error := 'Y';		
		END IF;	
		
		
	   -- 10. Validation for ICC Page name to associate the AG
		IF (L_ICC_PAGE_NAME IS NULL AND L_INT_NAME_AUTO_GEN_VAL IS NULL) THEN
			l_validation_num := l_validation_num + 1;		
			l_error_message := l_error_message || l_validation_num||'. Either ICC Page Name should be provided or the Auto Generation Prefix should be there for the row: '||L_AG_DISP_NAME|| CHR(10);
			l_raise_error := 'Y';
		END IF;		
		
	/*.............................. validation code for AG IN params .............END................*/
/*-------------------------------------------------------------------------------------------------------- */
	
	-- raise error on any validation failure
	IF l_raise_error = 'Y' THEN
		RETURN l_error_message;
	END IF;
	
	/*.............................. Generate the Mandatory Values .............START................*/

		--generating the transaction id, which will be used in all the interface tables
		SELECT mtl_system_items_interface_s.NEXTVAL INTO L_TRANSACTION_ID FROM DUAL;

		-- generate the Internal Names using the prefix 
		IF (L_INT_NAME_AUTO_GEN_VAL IS NOT NULL) THEN
			
			--generating the AG internal name
			IF(L_AG_NAME IS NULL) THEN
				L_AG_NAME := SUBSTR(REPLACE(L_INT_NAME_AUTO_GEN_VAL ||'_'||REPLACE(REPLACE(regexp_replace(UPPER(L_AG_DISP_NAME), '( *[[:punct:]])', '_'),' ','_'),'___','_'),'__','_'),1,27) || '_AG';	--<Raj 17-Jun> replaced the punct with _ and added 1 more replace for ___ to _
			END IF;
			
			--generating the Attribute internal name
			IF(L_ATTR_INTERNAL_NAME IS NULL) THEN
				L_ATTR_INTERNAL_NAME := SUBSTR(REPLACE(L_INT_NAME_AUTO_GEN_VAL ||'_'|| REPLACE(REPLACE(regexp_replace(UPPER(L_ATTR_DISPLAY_NAME), '( *[[:punct:]])', '_'),' ','_'),'___','_'),'__','_'),1,25) || '_ATTR';	--<Raj 17-Jun> replaced the punct with _ and added 1 more replace for ___ to _
			END IF;
	
			 -- Validation for ICC name availability in EBS
			IF (L_ICC_NAME IS NOT NULL) THEN
			
			BEGIN
				SELECT SEGMENT1 INTO L_ICC_NAME from APPS.MTL_ITEM_CATALOG_GROUPS_V WHERE UPPER(SEGMENT1) = UPPER(L_ICC_NAME);
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				-- insert into ICC interface table-------$$$$$$$$$$$$$$$$$$$$$$--------------------
				
				/* BEGIN
					INSERT INTO mtl_item_cat_grps_interface (
																item_catalog_group_id,
																item_catalog_name,
																inactive_date,
																summary_flag,
																enabled_flag,
																start_date_active,
																end_date_active,
																description,
																segment1,
																parent_catalog_group_name,
																item_creation_allowed_flag,
																set_process_id,
																transaction_id,
																process_status,
																transaction_type,
																request_id,
																program_application_id,
																program_id,
																program_update_date,
																created_by,
																creation_date,
																last_updated_by,
																last_update_date,
																last_update_login
							   )
					  VALUES   (
								   NULL,
								   TRIM (UPPER (SUBSTR (L_ICC_NAME, 1, 40))),
								   NULL,
								   'N',
								   'Y',
								   SYSDATE,
								   NULL,
								   TRIM (L_ICC_NAME),
								   TRIM (UPPER (SUBSTR (L_ICC_NAME, 1, 40))),
								   NVL (
									  TRIM (
										 UPPER (SUBSTR (c_icc.parent_name, 1, 40))
									  ),
									  'WHITE CAP MST'
								   ),
								   DECODE (c_icc.leaf, 'X', 'Y', 'N'),
								   1,
								   mtl_system_items_interface_s.NEXTVAL,
								   1,
								   'CREATE',
								   NULL,
								   431,
								   NULL,
								   SYSDATE,
								   fnd_profile.VALUE ('USER_ID'),
								   SYSDATE,
								   fnd_profile.VALUE ('USER_ID'),
								   SYSDATE,
								   fnd_profile.VALUE ('LOGIN_ID')
							   );
				 EXCEPTION
					WHEN OTHERS
					THEN
					   fnd_file.put_line (
						  fnd_file.LOG,
							 'Error while inserting the ICC interface table for ICC '
						  || c_icc.child_name
						  || ' error: '
						  || SQLERRM
					   );
				 END; --end of icc insert
		 */
				--as of now raise error
				l_validation_num := l_validation_num + 1;
				l_error_message := l_error_message ||l_validation_num || '. ICC Name is not valid. ';
				l_raise_error := 'Y';
				RETURN l_error_message;
			END;

			END IF;	-- end if of icc check

			--generating the Item Page internal and disp name
			IF(L_ICC_PAGE_NAME IS NULL) THEN
				--<Raj 17-Jun> replaced the punct with _ and added 1 more replace for ___ to _
				L_ICC_PAGE_INT_NAME := SUBSTR(REPLACE(L_INT_NAME_AUTO_GEN_VAL || '_' ||REPLACE(REPLACE(regexp_replace(UPPER(L_ICC_NAME), '( *[[:punct:]])', '_'),' ','_'),'___','_'),'__','_'),1,146) || '_PG';	
				
				--<Raj 17-Jun> generating the Item Page Display name from ICC name
				L_ICC_PAGE_NAME := SUBSTR(INITCAP(L_ICC_NAME),1,240) ;
				
			ELSE --<Raj 17-Jun> generating the internal name from page display name
				--L_ICC_PAGE_INT_NAME :=SUBSTR(REPLACE(L_INT_NAME_AUTO_GEN_VAL || '_' ||REPLACE(REPLACE(regexp_replace(UPPER(L_ICC_PAGE_NAME), '( *[[:punct:]])', '_'),' ','_'),'___','_'),'__','_'),1,146) || '_PG';	
				
				-- only for HDS white cap, changed the logic to follow the old way  
				L_ICC_PAGE_INT_NAME :=SUBSTR(REPLACE(L_INT_NAME_AUTO_GEN_VAL || '_' ||regexp_replace(UPPER(L_ICC_PAGE_NAME), '( *[[:punct:]])', '_'),' ','_'),1,146) || '_PG';	
			END IF;
			
	
			
		END IF;-- END IF of generate the Internal Names using the prefix 
		
		--generate the Attribute Data Type
		IF L_ATTR_DATA_TYPE IS NOT NULL THEN
			SELECT DECODE (L_ATTR_DATA_TYPE,'Numeric','N','Number','N','Long','A','Translatable Text','A','String','C','Char','C','Date','X','Standard Date','X','DateTime','Y','Standard DateTime','Y','C') INTO L_ATTR_DATA_TYPE from DUAL;
		END IF;
		
		--generate the next item page to ICC asociation sequence number based on the current seq
		IF (L_ICC_PAGE_SEQ IS NULL OR L_ICC_PAGE_SEQ <= 0) THEN
			SELECT MAX(next_seq)+10 INTO L_ICC_PAGE_SEQ FROM 
			   (
				SELECT NVL(MAX(page.sequence),0) next_seq 
				FROM EGO_PAGES_V page, APPS.MTL_ITEM_CATALOG_GROUPS_V icc
					WHERE icc.item_catalog_group_id = page.classification_code
						AND page.OBJECT_NAME = 'EGO_ITEM' 
						AND page.DATA_LEVEL_INT_NAME = L_AG_DATA_LEVEL_NAME --'ITEM_LEVEL'
						AND UPPER(icc.SEGMENT1) = UPPER(L_ICC_NAME)
				UNION
				SELECT NVL(MAX(sequence),0) next_seq
				FROM APPS.EGO_PAGES_INTERFACE
				WHERE data_level = L_AG_DATA_LEVEL_NAME
				AND SET_PROCESS_ID = L_SET_PROCESS_ID
				AND PROCESS_STATUS in (1,2)
				AND UPPER(classification_name) = UPPER(L_ICC_NAME)
			   );
		
		END IF;
		
		--generate the next Item page AG association sequence number based on the current seq
		IF (L_ICC_PAGE_AG_SEQ IS NULL OR L_ICC_PAGE_AG_SEQ <= 0) THEN
			SELECT MAX(next_seq)+10 INTO L_ICC_PAGE_AG_SEQ FROM 
			   (
				SELECT NVL(MAX(page.sequence),0) next_seq 
				FROM EGO_PAGE_ENTRIES_V page, APPS.MTL_ITEM_CATALOG_GROUPS_V icc
					WHERE icc.item_catalog_group_id = page.classification_code
						AND page.OBJECT_DISPLAY_NAME = 'Item' 
						AND page.DATA_LEVEL_INT_NAME = L_AG_DATA_LEVEL_NAME --'ITEM_LEVEL'
						AND UPPER(icc.SEGMENT1) = UPPER(L_ICC_NAME)
						AND UPPER(page.page_internal_name) = UPPER(L_ICC_PAGE_INT_NAME)--'TechSpecs'
				UNION
				SELECT NVL(MAX(sequence),0) next_seq
				FROM APPS.EGO_PAGE_ENTRIES_INTERFACE
				WHERE OLD_ATTR_GROUP_NAME = L_AG_NAME
				AND SET_PROCESS_ID = L_SET_PROCESS_ID
				AND PROCESS_STATUS in (1,2)
				AND UPPER(classification_name) = UPPER(L_ICC_NAME)
				AND UPPER(internal_name) = UPPER(L_ICC_PAGE_INT_NAME)
			   );
		
		END IF;
		
		--generate the next attr sequence number based on the current seq
		IF (L_ATTR_SEQUENCE IS NULL OR L_ATTR_SEQUENCE <= 0) THEN
		
			SELECT MAX(next_seq)+10 INTO L_ATTR_SEQUENCE FROM 
			   (
				SELECT NVL(MAX(attr.sequence),0) next_seq 
				FROM APPS.EGO_ATTRS_V attr, APPS.EGO_ATTR_GROUPS_V ag
					WHERE ag.attr_group_name = attr.attr_group_name
						AND ag.attr_group_type = attr.attr_group_type
                        AND attr.attr_group_type = L_AG_TYPE --'EGO_ITEMMGMT_GROUP'
						AND ag.attr_group_name = L_AG_NAME --'Recovery_Recovery'
				UNION
				SELECT NVL(MAX(sequence),0) next_seq
				FROM APPS.EGO_ATTR_GROUP_COLS_INTF
				WHERE SET_PROCESS_ID = L_SET_PROCESS_ID
				AND PROCESS_STATUS in (1,2)
                AND attr_group_type = L_AG_TYPE--'EGO_ITEMMGMT_GROUP'
				AND attr_group_name = L_AG_NAME --'Recovery_Recovery'
			   );
                           
		END IF;
		
		
		--generate the next attr column number based on the data type 
		--data Type = C-Char, Y-Date, X- DateTime, A- TL cols ,N- Number
		-- Available Columns = C_EXT_ATTR1 to 40, N_EXT_ATTR1 to 20, D_EXT_ATTR1 to 10, TL_EXT_ATTR1 to 40
		IF (L_ATTR_DB_COLUMN_NAME IS NULL) THEN
		
			--generate DB column name
			L_ATTR_DB_COLUMN_NAME := generate_db_column_name(L_AG_NAME,L_AG_TYPE,L_ATTR_DATA_TYPE,L_SET_PROCESS_ID);
			--L_ATTR_DB_COLUMN_NAME := 'C_EXT_ATTR1';
            IF  L_ATTR_DB_COLUMN_NAME LIKE 'ERROR%' THEN
				l_validation_num := l_validation_num + 1;
				l_error_message := l_error_message || l_validation_num||'. Error while generating the DB Column Name. ' || L_ATTR_DB_COLUMN_NAME;
				RETURN l_error_message;
			END IF;
			
		END IF;
		
		

	/*..............................  Generate the Mandatory Values .............END................*/
/*-------------------------------------------------------------------------------------------------------- */



	/*.............................. INSERT into EBS interface tables .............START................*/
		
		 -- To load Attribute groups
      --truncating the interface table before metadata import
      --EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GROUPS_INTERFACE';
      --EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GROUPS_DL_INTERFACE';
	  
	   BEGIN
            INSERT INTO EGO_ATTR_GROUPS_INTERFACE (SET_PROCESS_ID,
                                                   ATTR_GROUP_ID,
                                                   APPLICATION_ID,
                                                   ATTR_GROUP_TYPE,
                                                   ATTR_GROUP_NAME,
                                                   ATTR_GROUP_DISP_NAME,
                                                   DESCRIPTION,
                                                   MULTI_ROW,
                                                   VARIANT,
                                                   NUM_OF_ROWS,
                                                   NUM_OF_COLS,
                                                   OWNING_PARTY_ID,
                                                   OWNING_PARTY,
                                                   TRANSACTION_ID,
                                                   PROCESS_STATUS,
                                                   TRANSACTION_TYPE,
                                                   REQUEST_ID,
                                                   PROGRAM_APPLICATION_ID,
                                                   PROGRAM_ID,
                                                   PROGRAM_UPDATE_DATE,
                                                   CREATED_BY,
                                                   CREATION_DATE,
                                                   LAST_UPDATED_BY,
                                                   LAST_UPDATE_DATE,
                                                   LAST_UPDATE_LOGIN)
			  SELECT L_SET_PROCESS_ID,
                        NULL,
                        L_APPLICATION_ID,
                        NVL(L_AG_TYPE,'EGO_ITEMMGMT_GROUP'),
                        L_AG_NAME,
                        INITCAP(L_AG_DISP_NAME),
                        NVL(L_AG_DESCRIPTION,INITCAP(L_AG_DISP_NAME)),
                        DECODE (L_AG_MULTI_ROW_FLAG, 'No', 'N', 'Yes','Y','Y','Y','N'),
                        DECODE (L_AG_VARIANT_FLAG, 'No', 'N', 'Yes','Y','Y','Y','N'),
                        L_AG_NUM_OF_ROWS,
                        NVL(L_AG_NUM_OF_COLS,2),
                        -100, --OWNING_PARTY_ID
                        NULL, --OWNING_PARTY
                        L_TRANSACTION_ID,
                        L_PROCESS_STATUS,
                        NVL(L_TRANSACTION_TYPE,'SYNC'),
                        NULL,
                        L_APPLICATION_ID,
                        NULL,
                        NULL,
                       -- fnd_profile.VALUE ('USER_ID'),                                      --commented and added below by Maha for ver 1.3
                        fnd_global.user_id,
                        SYSDATE,
                        --fnd_profile.VALUE ('USER_ID'),
                         fnd_global.user_id,
                        SYSDATE,
                       -- fnd_profile.VALUE ('LOGIN_ID')
                        fnd_global.login_id
				FROM DUAL
				WHERE NOT EXISTS 
						( SELECT 1 FROM EGO_ATTR_GROUPS_INTERFACE 
							WHERE ATTR_GROUP_TYPE = L_AG_TYPE AND ATTR_GROUP_NAME = L_AG_NAME
							AND SET_PROCESS_ID = L_SET_PROCESS_ID
							AND PROCESS_STATUS IN (1,2)
						
						);
				--);

			-- inserting into Data level AG interface table
            INSERT INTO EGO_ATTR_GROUPS_DL_INTERFACE (
                                                         SET_PROCESS_ID,
                                                         ATTR_GROUP_ID,
                                                         APPLICATION_ID,
                                                         ATTR_GROUP_TYPE,
                                                         ATTR_GROUP_NAME,
                                                         DATA_LEVEL_ID,
                                                         DATA_LEVEL_NAME,
                                                         DEFAULTING,
                                                         DEFAULTING_NAME,
                                                         VIEW_PRIVILEGE_ID,
                                                         VIEW_PRIVILEGE_NAME,
                                                         USER_VIEW_PRIV_NAME,
                                                         EDIT_PRIVILEGE_ID,
                                                         EDIT_PRIVILEGE_NAME,
                                                         USER_EDIT_PRIV_NAME,
                                                         PRE_BUSINESS_EVENT_FLAG,
                                                         BUSINESS_EVENT_FLAG,
                                                         TRANSACTION_ID,
                                                         PROCESS_STATUS,
                                                         TRANSACTION_TYPE,
                                                         REQUEST_ID,
                                                         PROGRAM_APPLICATION_ID,
                                                         PROGRAM_ID,
                                                         PROGRAM_UPDATE_DATE,
                                                         CREATED_BY,
                                                         CREATION_DATE,
                                                         LAST_UPDATED_BY,
                                                         LAST_UPDATE_DATE,
                                                         LAST_UPDATE_LOGIN
                       )
              SELECT L_SET_PROCESS_ID,
                        NULL,
                        L_APPLICATION_ID,
                        NVL(L_AG_TYPE,'EGO_ITEMMGMT_GROUP'),
                        L_AG_NAME,
                        NULL,
                        NVL(L_AG_DATA_LEVEL_NAME,'ITEM_LEVEL'),
                        'D', --DEFAULTING
                        NULL,
                        NULL,
                        NULL,
                        L_AG_USER_VIEW_PRIV_NAME,
                        NULL,
                        NULL,
                        L_AG_USER_EDIT_PRIV_NAME,
                        DECODE (L_AG_PRE_EVENT_FLAG, 'No', 'N', 'Yes','Y','Y','Y','N'),
                        DECODE (L_AG_POST_EVENT_FLAG, 'No', 'N', 'Yes','Y','Y','Y','N'),
                        L_TRANSACTION_ID,
                        L_PROCESS_STATUS,
                        NVL(L_TRANSACTION_TYPE,'SYNC'),
                        NULL,
                        L_APPLICATION_ID,
                        NULL,
                        NULL,
                        --fnd_profile.VALUE ('USER_ID'),                                              --commented and added below by Maha for ver 1.3
 			fnd_global.user_id,
                        SYSDATE,
                       -- fnd_profile.VALUE ('USER_ID'),
 			fnd_global.user_id,
                        SYSDATE,
                       -- fnd_profile.VALUE ('LOGIN_ID')
 			fnd_global.login_id
				FROM DUAL
				WHERE NOT EXISTS 
						( SELECT 1 FROM EGO_ATTR_GROUPS_DL_INTERFACE 
							WHERE ATTR_GROUP_TYPE = L_AG_TYPE AND ATTR_GROUP_NAME = L_AG_NAME
							AND DATA_LEVEL_NAME = L_AG_DATA_LEVEL_NAME
							AND SET_PROCESS_ID = L_SET_PROCESS_ID
							AND PROCESS_STATUS IN (1,2)						
						);

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
			l_error_message := SUBSTR ( SQLERRM || ' Error while inserting the Attribute Group name for: '|| L_AG_NAME, 1,2000);
            fnd_file.put_line (fnd_file.LOG,l_error_message);
         END;--end of AG insert
		 
		 
		 --to load attributes in attribute group
      --truncating the interface table before metadata import
      --EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GROUP_COLS_INTF';
    BEGIN
            INSERT INTO EGO_ATTR_GROUP_COLS_INTF (
                                                     SET_PROCESS_ID,
                                                     ATTR_ID,
                                                     APPLICATION_ID,
                                                     ATTR_GROUP_TYPE,
                                                     ATTR_GROUP_ID,
                                                     ATTR_GROUP_NAME,
                                                     INTERNAL_NAME,
                                                     DISPLAY_NAME,
                                                     DESCRIPTION,
                                                     SEQUENCE,
                                                     APPLICATION_COLUMN_NAME,
                                                     DATA_TYPE,
                                                     SEARCH_FLAG,
                                                     UNIQUE_KEY_FLAG,
                                                     INFO_1,
                                                     UOM_CLASS,
                                                     CONTROL_LEVEL,
                                                     VIEW_IN_HIERARCHY_CODE,
                                                     EDIT_IN_HIERARCHY_CODE,
                                                     CUSTOMIZATION_LEVEL,
                                                     READ_ONLY_FLAG,
                                                     ENABLED_FLAG,
                                                     REQUIRED_FLAG,
                                                     SECURITY_ENABLED_FLAG,
                                                     DISPLAY_CODE,
                                                     DEFAULT_VALUE,
                                                     ATTRIBUTE_CODE,
                                                     MAXIMUM_DESCRIPTION_LEN,
                                                     CONCATENATION_DESCRIPTION_LEN,
                                                     FLEX_VALUE_SET_ID,
                                                     FLEX_VALUE_SET_NAME,
                                                     TRANSACTION_ID,
                                                     PROCESS_STATUS,
                                                     TRANSACTION_TYPE,
                                                     REQUEST_ID,
                                                     PROGRAM_APPLICATION_ID,
                                                     PROGRAM_ID,
                                                     PROGRAM_UPDATE_DATE,
                                                     CREATED_BY,
                                                     CREATION_DATE,
                                                     LAST_UPDATED_BY,
                                                     LAST_UPDATE_DATE,
                                                     LAST_UPDATE_LOGIN
                       )
              SELECT 
                        L_SET_PROCESS_ID,
                        NULL,
			L_APPLICATION_ID,
                        NVL(L_AG_TYPE,'EGO_ITEMMGMT_GROUP'),
                        NULL,
                        L_AG_NAME,
                        L_ATTR_INTERNAL_NAME,
                        L_ATTR_DISPLAY_NAME,
                        NVL(L_ATTR_DESCRIPTION,L_ATTR_DISPLAY_NAME),
                        L_ATTR_SEQUENCE,
                        L_ATTR_DB_COLUMN_NAME,
                        L_ATTR_DATA_TYPE,--DECODE (L_ATTR_DATA_TYPE,'Numeric','N','Number','N','Long','A','Translatable Text','A','String','C','Char','C','Date','X','Standard Date','X','DateTime','Y','Standard DateTime','Y',L_ATTR_DATA_TYPE),
                        'N', --SEARCH_FLAG
                        NVL(DECODE(L_ATTR_UNIQUE_KEY_FLAG,'Yes','Y','No','N',L_ATTR_UNIQUE_KEY_FLAG),DECODE(L_AG_MULTI_ROW_FLAG,'Yes','Y','Y','Y','N')),
                        L_ATTR_INFO_1,
                        DECODE(L_ATTR_DATA_TYPE,'N',L_ATTR_UOM_CLASS,NULL), -- Unit Of Measure in case of Number Attribute, else NULL
                        1,--CONTROL_LEVEL
                        'A',
                        'A',
                        'A',
                        'N',
                        NVL(L_ATTR_ENABLED_FLAG,'Y'),
                        NVL(L_ATTR_REQUIRED_FLAG,'N'),
                        NULL,
                        NVL(L_ATTR_DISPLAY_CODE,'T'),
                        L_ATTR_DEFAULT_VALUE,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        L_ATTR_VS_NAME,
                        L_TRANSACTION_ID,
                        L_PROCESS_STATUS,
                        NVL(L_TRANSACTION_TYPE,'SYNC'),
                        NULL,
                        L_APPLICATION_ID,
                        NULL,
                        NULL,
                       -- fnd_profile.VALUE ('USER_ID'),                           --commented and added below by Maha for ver 1.3
                        fnd_global.user_id,
                        SYSDATE,
                        --fnd_profile.VALUE ('USER_ID'),
                        fnd_global.user_id,
                        SYSDATE,
                        --fnd_profile.VALUE ('LOGIN_ID')
                        fnd_global.login_id
				FROM DUAL
				WHERE NOT EXISTS 
						( SELECT 1 FROM EGO_ATTR_GROUP_COLS_INTF 
							WHERE ATTR_GROUP_TYPE = L_AG_TYPE AND ATTR_GROUP_NAME = L_AG_NAME
							AND INTERNAL_NAME = L_ATTR_INTERNAL_NAME
							AND SET_PROCESS_ID = L_SET_PROCESS_ID
							AND PROCESS_STATUS IN (1,2)						
						);

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
				l_error_message := SUBSTR ( SQLERRM || ' Error while inserting the Attribute name for: '|| P_ATTR_INTERNAL_NAME,1,2000);
				fnd_file.put_line (fnd_file.LOG,l_error_message);
         END;--end of Attr insert
		
		
		 -- to populate AG ICC association
		--truncating the interface table before metadata import
		--EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GRPS_ASSOC_INTERFACE';
	   BEGIN
            INSERT INTO EGO_ATTR_GRPS_ASSOC_INTERFACE (
                                                          ITEM_CATALOG_GROUP_ID,
                                                          ITEM_CATALOG_NAME,
                                                          DATA_LEVEL,
                                                          DATA_LEVEL_ID,
                                                          ATTR_GROUP_NAME,
                                                          ATTR_GROUP_ID,
                                                          SET_PROCESS_ID,
                                                          TRANSACTION_ID,
                                                          PROCESS_STATUS,
                                                          TRANSACTION_TYPE,
                                                          REQUEST_ID,
                                                          PROGRAM_APPLICATION_ID,
                                                          PROGRAM_ID,
                                                          PROGRAM_UPDATE_DATE,
                                                          CREATED_BY,
                                                          CREATION_DATE,
                                                          LAST_UPDATED_BY,
                                                          LAST_UPDATE_DATE,
                                                          LAST_UPDATE_LOGIN,
                                                          ASSOCIATION_ID
                       )
              SELECT    NULL,
                        L_ICC_NAME,
                        NULL, -- nullifying this column as per oracle suggestion NVL(L_AG_DATA_LEVEL_NAME,'ITEM_LEVEL'),
                        NULL,
                        L_AG_NAME,
                        NULL,
                        L_SET_PROCESS_ID,
                        L_TRANSACTION_ID,
                        L_PROCESS_STATUS,
                        DECODE(L_TRANSACTION_TYPE,'SYNC','CREATE',L_TRANSACTION_TYPE),--NVL(L_TRANSACTION_TYPE,'SYNC'),
                        NULL,
                        L_APPLICATION_ID,
                        NULL,
                        NULL,
                        --fnd_profile.VALUE ('USER_ID'),                           --commented and added below by Maha for ver 1.3
                        fnd_global.user_id,
                        SYSDATE,
                        --fnd_profile.VALUE ('USER_ID'),
			fnd_global.user_id,
                        SYSDATE,
                        --fnd_profile.VALUE ('LOGIN_ID'),
			  fnd_global.login_id,
                        NULL
				FROM DUAL
				WHERE NOT EXISTS 
						( SELECT 1 FROM EGO_ATTR_GRPS_ASSOC_INTERFACE 
							WHERE ITEM_CATALOG_NAME = L_ICC_NAME 
							AND ATTR_GROUP_NAME = L_AG_NAME
							AND SET_PROCESS_ID = L_SET_PROCESS_ID
							AND PROCESS_STATUS IN (1,2)						
						);					

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
				l_error_message := SUBSTR ( SQLERRM || ' Error while inserting the Attribute group association to ICC for: '|| P_AG_NAME,1,2000);
				fnd_file.put_line (fnd_file.LOG,l_error_message);
         END;

      -- to create the ICC Pages
      --truncating the interface table before metadata import
      --EXECUTE IMMEDIATE 'truncate table ego.EGO_PAGES_INTERFACE';

     -- EXECUTE IMMEDIATE 'truncate table ego.EGO_PAGE_ENTRIES_INTERFACE';
		BEGIN
            INSERT INTO EGO_PAGES_INTERFACE (
                                                SET_PROCESS_ID,
                                                PAGE_ID,
                                                DISPLAY_NAME,
                                                INTERNAL_NAME,--internal name of the page
                                                DESCRIPTION,
                                                CLASSIFICATION_CODE,
                                                CLASSIFICATION_NAME,
                                                DATA_LEVEL,
                                                SEQUENCE,
                                                TRANSACTION_ID,
                                                PROCESS_STATUS,
                                                TRANSACTION_TYPE,
                                                REQUEST_ID,
                                                PROGRAM_APPLICATION_ID,
                                                PROGRAM_ID,
                                                PROGRAM_UPDATE_DATE,
                                                CREATED_BY,
                                                CREATION_DATE,
                                                LAST_UPDATED_BY,
                                                LAST_UPDATE_DATE,
                                                LAST_UPDATE_LOGIN
                       )
              SELECT
						L_SET_PROCESS_ID,
                        NULL,
                        L_ICC_PAGE_NAME, --<Raj 17-Jun> changed the ICC page name variable
                        L_ICC_PAGE_INT_NAME,--<Raj 17-Jun> changed the ICC page internal name variable
                        L_ICC_PAGE_NAME,--<Raj 17-Jun> changed the ICC page name variable for desc
                        NULL,
                        L_ICC_NAME,
                        NVL(L_AG_DATA_LEVEL_NAME,'ITEM_LEVEL'),
                        L_ICC_PAGE_SEQ, --seq number for page
                        L_TRANSACTION_ID,
                        L_PROCESS_STATUS,
						NVL(L_TRANSACTION_TYPE,'SYNC'),
                        NULL,
                        L_APPLICATION_ID,
                        NULL,
                        NULL,
                       -- fnd_profile.VALUE ('USER_ID'),                             --commented and added below by Maha for ver 1.3
			fnd_global.user_id,
                        SYSDATE,
                      --  fnd_profile.VALUE ('USER_ID'),
			fnd_global.user_id,
                        SYSDATE,
                       -- fnd_profile.VALUE ('LOGIN_ID')
			fnd_global.login_id
				FROM DUAL
				WHERE NOT EXISTS 
						( SELECT 1 FROM EGO_PAGES_INTERFACE 
							WHERE CLASSIFICATION_NAME = L_ICC_NAME 
							AND INTERNAL_NAME = L_ICC_PAGE_INT_NAME--<Raj 17-Jun> changed the ICC page internal name variable
							AND DATA_LEVEL = L_AG_DATA_LEVEL_NAME
							AND SET_PROCESS_ID = L_SET_PROCESS_ID
							AND PROCESS_STATUS IN (1,2)						
						);
         EXCEPTION
            WHEN OTHERS
            THEN
				l_error_message := SUBSTR ( SQLERRM || ' Error while inserting the Item Pages for ICC:  '|| P_ICC_NAME,1,2000);
				fnd_file.put_line (fnd_file.LOG,l_error_message);

         END; -- end of Item Page insert for ICC
		 
		 BEGIN
			INSERT INTO EGO_PAGE_ENTRIES_INTERFACE (
                                                    SET_PROCESS_ID,
                                                    PAGE_ID,
                                                    INTERNAL_NAME,--internal name of the page
                                                    OLD_ASSOCIATION_ID,
                                                    NEW_ASSOCIATION_ID,
                                                    OLD_ATTR_GROUP_ID,
                                                    NEW_ATTR_GROUP_ID,
                                                    OLD_ATTR_GROUP_NAME,--internal name of AG
                                                    NEW_ATTR_GROUP_NAME,
                                                    SEQUENCE,
                                                    CLASSIFICATION_CODE,
                                                    CLASSIFICATION_NAME,
                                                    TRANSACTION_ID,
                                                    PROCESS_STATUS,
                                                    TRANSACTION_TYPE,
                                                    REQUEST_ID,
                                                    PROGRAM_APPLICATION_ID,
                                                    PROGRAM_ID,
                                                    PROGRAM_UPDATE_DATE,
                                                    CREATION_DATE,
                                                    CREATED_BY,
                                                    LAST_UPDATED_BY,
                                                    LAST_UPDATE_DATE,
                                                    LAST_UPDATE_LOGIN
                    )
           SELECT
						L_SET_PROCESS_ID,
                        NULL,
                        L_ICC_PAGE_INT_NAME,--<Raj 17-Jun> changed the ICC page internal name variable
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        L_AG_NAME, --old ag
                        NULL,--L_AG_NAME, -- new ag
                        L_ICC_PAGE_AG_SEQ, -- page to AG association seq
                        NULL,
                        L_ICC_NAME, --classification name
                        L_TRANSACTION_ID,
                        L_PROCESS_STATUS,
						DECODE(L_TRANSACTION_TYPE,'SYNC','CREATE',L_TRANSACTION_TYPE),--NVL(L_TRANSACTION_TYPE,'SYNC'),
                        NULL,
                        L_APPLICATION_ID,
                        NULL,
                        NULL,
                        SYSDATE,
                        --fnd_profile.VALUE ('USER_ID'),                          --commented and added below by Maha for ver 1.3
			fnd_global.user_id,
                       -- fnd_profile.VALUE ('USER_ID'),
			fnd_global.user_id,
                        SYSDATE,
                        --fnd_profile.VALUE ('LOGIN_ID')
			fnd_global.login_id
				FROM DUAL
				WHERE NOT EXISTS 
						( SELECT 1 FROM EGO_PAGE_ENTRIES_INTERFACE 
							WHERE CLASSIFICATION_NAME = L_ICC_NAME 
							AND INTERNAL_NAME = L_ICC_PAGE_INT_NAME--<Raj 17-Jun> changed the ICC page internal name variable
							AND OLD_ATTR_GROUP_NAME = L_AG_NAME
							AND SET_PROCESS_ID = L_SET_PROCESS_ID
							AND PROCESS_STATUS IN (1,2)						
						);
        EXCEPTION
            WHEN OTHERS
            THEN
				l_error_message := SUBSTR ( SQLERRM || ' Error while inserting the Item Pages to AG Association for ICC:  '|| P_ICC_NAME,1,2000);
				fnd_file.put_line (fnd_file.LOG,l_error_message);
               
         END; -- end of Item Page to AG association insert for ICC					
		
		COMMIT;
		
		IF(LENGTH(l_error_message) < 2) THEN
			l_error_message := NULL;
		END IF;
	    fnd_file.put_line (fnd_file.LOG,'End of Program - Inserted all the required data to Interface tables');
		
		RETURN l_error_message;
    EXCEPTION
    WHEN OTHERS THEN
	   l_error_message := SUBSTR ( l_error_message || ' --- ' || SQLERRM || ' Details: '||dbms_utility.format_error_backtrace(),1,4000);
	   fnd_file.put_line (fnd_file.LOG,l_error_message);
	   RETURN l_error_message;
    END LOAD_AG_TO_INTRF;
	
	/*************************************************************************
   *   Function : generate_db_column_name
   *
   *   PURPOSE:   This Function is used to generate DB column name
   *   Parameter:
   *          IN
   *              	L_AG_NAME      		-- Ag Name
					L_AG_TYPE			- AG Type
					L_ATTR_DATA_TYPE	- attribute data type
					L_SET_PROCESS_ID - set process id or batch id
   * 			  
   * 		  RETURNS:
   *			  l_db_column_name 		-- DB column name
   *			
   
    HISTORY
   ===============================================================================
    VERSION DATE          AUTHOR(S)       		  DESCRIPTION
    ------- -----------   ----------------------  ----------------------------------
	1.0     12-JUN-2013   Rajasekar Gunasekaran	  Initial Version
   **********************************************************************************/	
	
	FUNCTION generate_db_column_name(L_AG_NAME VARCHAR2,L_AG_TYPE VARCHAR2,L_ATTR_DATA_TYPE VARCHAR2,L_SET_PROCESS_ID NUMBER) RETURN VARCHAR2
	
	IS
	
		l_db_column_name VARCHAR2(200);
		l_next_db_col NUMBER;
	BEGIN
	
		IF (L_ATTR_DATA_TYPE = 'C') THEN
			SELECT MAX(next_db_col)+1 INTO l_next_db_col FROM 
					   (
						SELECT NVL(MAX(TO_NUMBER(SUBSTR(database_column,INSTR(database_column,'ATTR')+4))),0) next_db_col 
						FROM APPS.EGO_ATTRS_V attr, APPS.EGO_ATTR_GROUPS_V ag
							WHERE ag.attr_group_name = attr.attr_group_name
								AND ag.attr_group_type = attr.attr_group_type
								AND attr.attr_group_type = L_AG_TYPE --'EGO_ITEMMGMT_GROUP'
								AND ag.attr_group_name = L_AG_NAME --'Recovery_Recovery'
								AND database_column like 'C_EXT_ATTR%'
						UNION
						SELECT NVL(MAX(TO_NUMBER(SUBSTR(application_column_name,INSTR(application_column_name,'ATTR')+4))),0) next_db_col
						FROM APPS.EGO_ATTR_GROUP_COLS_INTF
						WHERE SET_PROCESS_ID = L_SET_PROCESS_ID
						AND PROCESS_STATUS in (1,2)
						AND attr_group_type = L_AG_TYPE--'EGO_ITEMMGMT_GROUP'
						AND attr_group_name = L_AG_NAME --'Recovery_Recovery'
						AND application_column_name like 'C_EXT_ATTR%'
					   );
					   
			l_db_column_name := 'C_EXT_ATTR'||to_char(l_next_db_col);
			
			IF (SUBSTR(l_db_column_name,INSTR(l_db_column_name,'ATTR')+4) > 40 ) THEN
				l_db_column_name := 'ERROR : Char DB column name should not be > 40 ';
			END IF;
			
		ELSIF(L_ATTR_DATA_TYPE = 'N') THEN
		
			SELECT MAX(next_db_col)+1 INTO l_next_db_col FROM 
					   (
						SELECT NVL(MAX(TO_NUMBER(SUBSTR(database_column,INSTR(database_column,'ATTR')+4))),0) next_db_col 
						FROM APPS.EGO_ATTRS_V attr, APPS.EGO_ATTR_GROUPS_V ag
							WHERE ag.attr_group_name = attr.attr_group_name
								AND ag.attr_group_type = attr.attr_group_type
								AND attr.attr_group_type = L_AG_TYPE --'EGO_ITEMMGMT_GROUP'
								AND ag.attr_group_name = L_AG_NAME --'Recovery_Recovery'
								AND database_column like 'N_EXT_ATTR%'
						UNION
						SELECT NVL(MAX(TO_NUMBER(SUBSTR(application_column_name,INSTR(application_column_name,'ATTR')+4))),0) next_db_col
						FROM APPS.EGO_ATTR_GROUP_COLS_INTF
						WHERE SET_PROCESS_ID = L_SET_PROCESS_ID
						AND PROCESS_STATUS in (1,2)
						AND attr_group_type = L_AG_TYPE--'EGO_ITEMMGMT_GROUP'
						AND attr_group_name = L_AG_NAME --'Recovery_Recovery'
						AND application_column_name like 'N_EXT_ATTR%'
					   );
			l_db_column_name := 'N_EXT_ATTR'||to_char(l_next_db_col);					   
			
			IF (SUBSTR(l_db_column_name,INSTR(l_db_column_name,'ATTR')+4) > 20 ) THEN
				l_db_column_name := 'ERROR : Number DB column name should not be > 20 ';
			END IF;	
			
		ELSIF(L_ATTR_DATA_TYPE = 'A') THEN
		
			SELECT MAX(next_db_col)+1 INTO l_next_db_col FROM 
					   (
						SELECT NVL(MAX(TO_NUMBER(SUBSTR(database_column,INSTR(database_column,'ATTR')+4))),0) next_db_col 
						FROM APPS.EGO_ATTRS_V attr, APPS.EGO_ATTR_GROUPS_V ag
							WHERE ag.attr_group_name = attr.attr_group_name
								AND ag.attr_group_type = attr.attr_group_type
								AND attr.attr_group_type = L_AG_TYPE --'EGO_ITEMMGMT_GROUP'
								AND ag.attr_group_name = L_AG_NAME --'Recovery_Recovery'
								AND database_column like 'TL_EXT_ATTR%'
						UNION
						SELECT NVL(MAX(TO_NUMBER(SUBSTR(application_column_name,INSTR(application_column_name,'ATTR')+4))),0) next_db_col
						FROM APPS.EGO_ATTR_GROUP_COLS_INTF
						WHERE SET_PROCESS_ID = L_SET_PROCESS_ID
						AND PROCESS_STATUS in (1,2)
						AND attr_group_type = L_AG_TYPE--'EGO_ITEMMGMT_GROUP'
						AND attr_group_name = L_AG_NAME --'Recovery_Recovery'
						AND application_column_name like 'TL_EXT_ATTR%'
					   );
			l_db_column_name := 'TL_EXT_ATTR'||to_char(l_next_db_col);					   
								   
			IF (SUBSTR(l_db_column_name,INSTR(l_db_column_name,'ATTR')+4) > 40 ) THEN
				l_db_column_name := 'ERROR  : TL DB column name should not be > 40 ';
			END IF;						
		
		ELSIF(L_ATTR_DATA_TYPE IN ('X','Y')) THEN
			SELECT MAX(next_db_col)+1 INTO l_next_db_col FROM 
					   (
						SELECT NVL(MAX(TO_NUMBER(SUBSTR(database_column,INSTR(database_column,'ATTR')+4))),0) next_db_col 
						FROM APPS.EGO_ATTRS_V attr, APPS.EGO_ATTR_GROUPS_V ag
							WHERE ag.attr_group_name = attr.attr_group_name
								AND ag.attr_group_type = attr.attr_group_type
								AND attr.attr_group_type = L_AG_TYPE --'EGO_ITEMMGMT_GROUP'
								AND ag.attr_group_name = L_AG_NAME --'Recovery_Recovery'
								AND database_column like 'D_EXT_ATTR%'
						UNION
						SELECT NVL(MAX(TO_NUMBER(SUBSTR(application_column_name,INSTR(application_column_name,'ATTR')+4))),0) next_db_col
						FROM APPS.EGO_ATTR_GROUP_COLS_INTF
						WHERE SET_PROCESS_ID = L_SET_PROCESS_ID
						AND PROCESS_STATUS in (1,2)
						AND attr_group_type = L_AG_TYPE--'EGO_ITEMMGMT_GROUP'
						AND attr_group_name = L_AG_NAME --'Recovery_Recovery'
						AND application_column_name like 'D_EXT_ATTR%'
					   );	
			l_db_column_name := 'D_EXT_ATTR'||to_char(l_next_db_col);					   
			
			IF (SUBSTR(l_db_column_name,INSTR(l_db_column_name,'ATTR')+4) > 10 ) THEN
				l_db_column_name := 'ERROR  : Date DB column name should not be > 10 ';
			END IF;	
			
		END IF; -- end if of data type check
		
		RETURN l_db_column_name;
	EXCEPTION
	WHEN OTHERS THEN
            --raise_application_error(SUBSTR ( SQLERRM || ' Details: '||dbms_utility.format_error_backtrace(),  1,  2000));
          RETURN SUBSTR ( 'ERROR'||SQLERRM || ' Details: '||dbms_utility.format_error_backtrace(),  1,  200);
	END generate_db_column_name;
	
	
	/*************************************************************************
   *   Procedure : submit_job
   *
   *   PURPOSE:   This procedure is used to call the EGO Import Metadata(EGOIMDCP)
   *   Parameter:
   *          IN
   *              p_user_name      		-- User name to initilize the request
   * 			  
   * 		  OUT:
   *			  errbuf 		-- contains the error details
   *			  retcode		-- 0 = Success, 1 = Warning, 2 = ERROR
   
    HISTORY
   ===============================================================================
    VERSION DATE          AUTHOR(S)       		  DESCRIPTION
    ------- -----------   ----------------------  ----------------------------------
	1.0     07-JUN-2013   Rajasekar Gunasekaran	  Initial Version
   **********************************************************************************/
   PROCEDURE submit_job (
      errbuf                  OUT NOCOPY VARCHAR2,
      retcode                 OUT NOCOPY VARCHAR2,
      p_user_name             IN         VARCHAR2
   )
   IS
   
   l_req_id 	NUMBER;
   l_phase 	VARCHAR2(10);
   l_status 	VARCHAR2(10);
   l_dev_phase 	VARCHAR2(10);
   l_dev_status VARCHAR2(10);
   l_message 	VARCHAR2(1000);
   l_error_message	VARCHAR2(1000);
   
   l_user_id 		NUMBER ;
   l_responsibility_id 	NUMBER;
   l_resp_application_id NUMBER;
   l_resp_name CONSTANT VARCHAR(40) := 'Development Manager';
   l_batch_id NUMBER := 1111;
   l_request_status boolean;
   
   BEGIN
	
    retcode := 0;
    errbuf :=  'Program Successfully Completed';
	
	 -- Deriving Ids from initalization variables
     -- to get the user id 
	 BEGIN
		l_user_id := Apps.FND_LOAD_UTIL.OWNER_ID(p_user_name); 
		--l_user_id := to_number(p_user_name);
		
	 EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_message := 'UserName ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_error_message := 'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

	  -- to get the resp id and application id
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE responsibility_name = l_resp_name
            AND SYSDATE BETWEEN start_date AND NVL (end_date,
                                                    TRUNC (SYSDATE) + 1
                                                   );
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_message := 'Responsibility '
               || l_resp_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_error_message :=
                  'Error deriving Responsibility_id for '
               || l_resp_name;
            RAISE PROGRAM_ERROR;
      END;	
		
        -- Environment Initialization
        apps.fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id
                                 );
	
      
	  --DBMS_OUTPUT.put_line('Submitting the Concurrent Program : EGO Import Metadata');
	  fnd_file.put_line (fnd_file.LOG,'Submitting the Concurrent Program : EGO Import Metadata');
       -- Calling the CP API to submit the CP :EGO Import Metadata            
        l_req_id :=
            apps.fnd_request.submit_request (application      => 'EGO',
                                        program          => 'EGOIMDCP',
                                        argument1        => 'N',
                                        argument2        => 'Y',
                                        argument3        => 'Y',
                                        argument4        => l_batch_id,
                                        argument5        => 'N'
                                       );
        COMMIT;
        --DBMS_OUTPUT.put_line('Concurrent Program Request Submitted Successfully. Request ID: ' || l_req_id);
		fnd_file.put_line (fnd_file.LOG,'Concurrent Program Request Submitted Successfully. Request ID: ' || l_req_id);
		
         IF (l_req_id > 0) THEN
            l_request_status :=  apps.fnd_concurrent.wait_for_request
                                       (request_id 	=> l_req_id,
                                       -- interval 	=> 60,
                                       -- max_wait 	=> 90,
                                        phase 		=> l_phase,
                                        status 		=> l_status,
                                        dev_phase	=> l_dev_phase,
                                        dev_status	=> l_dev_status,
                                        message 	=> l_message
                                       );
									   
              IF UPPER(l_status) = 'WARNING' OR UPPER(l_dev_status) = 'WARNING' THEN
                  l_error_message :=
                        'EBS Conc Prog Completed with warning '
                     || l_message;
					--DBMS_OUTPUT.put_line (l_error_message);
					fnd_file.put_line (fnd_file.LOG,l_error_message);
					retcode := 1;
                  --RAISE PROGRAM_ERROR;
               ELSIF UPPER(l_status) = 'ERROR' OR UPPER(l_dev_status) = 'ERROR' THEN
                  l_error_message :=
                        'EBS Conc Prog Completed with Error '
                     || l_message;
					--DBMS_OUTPUT.put_line (l_error_message);	
					fnd_file.put_line (fnd_file.LOG,l_error_message);					
					retcode := 2;
					RAISE PROGRAM_ERROR;
               ELSE -- else of l_status check
					retcode := 0;
					--DBMS_OUTPUT.put_line ('EBS Conc Program Successfully Completed');
					fnd_file.put_line (fnd_file.LOG,l_error_message);				   
              END IF; -- end of l_status check
		 
	ELSE	-- else of IF (l_req_id > 0)	 
            l_error_message := 'EBS Conc Program not initated';
            --DBMS_OUTPUT.put_line (l_error_message);
			fnd_file.put_line (fnd_file.LOG,l_error_message);
            RAISE PROGRAM_ERROR;
         END IF;
	

	
  EXCEPTION
  WHEN PROGRAM_ERROR THEN
        retcode := 2;
	errbuf := SUBSTR ( l_error_message || SQLCODE  || ' ERROR : ' || SQLERRM || ' Details: '||dbms_utility.format_error_backtrace(),  1,  2000);
	--DBMS_OUTPUT.put_line (errbuf);
	fnd_file.put_line (fnd_file.LOG,errbuf);
  WHEN OTHERS THEN
        retcode := 2;
	errbuf := SUBSTR ( l_error_message || SQLCODE  || ' ERROR : ' || SQLERRM || ' Details: '||dbms_utility.format_error_backtrace(),  1,  2000);
	--DBMS_OUTPUT.put_line (errbuf);
	fnd_file.put_line (fnd_file.LOG,errbuf);
  END submit_job;
  
  
  	/*************************************************************************
   *   Function : submit_metadata_cp
   *
   *   PURPOSE:   This Function is used to call the EGO Import Metadata(EGOIMDCP)
   *   Parameter:
				p_user_name - user name
   *          returns
   *			  errbuf 		-- contains the error details
   *			
   
    HISTORY
   ===============================================================================
    VERSION DATE          AUTHOR(S)       		  DESCRIPTION
    ------- -----------   ----------------------  ----------------------------------
	1.0     07-JUN-2013   Rajasekar Gunasekaran	  Initial Version
   **********************************************************************************/
   FUNCTION submit_metadata_cp(p_user_name VARCHAR2) RETURN VARCHAR2
   IS
   
   l_req_id 	NUMBER;
   l_phase 	VARCHAR2(10);
   l_status 	VARCHAR2(10);
   l_dev_phase 	VARCHAR2(10);
   l_dev_status VARCHAR2(10);
   l_message 	VARCHAR2(1000);
   l_error_message	VARCHAR2(2000) := NULL;
   
   l_user_id 		NUMBER ;
   l_responsibility_id 	NUMBER;
   l_resp_application_id NUMBER;
   l_user_name VARCHAR(40);
   l_resp_name CONSTANT VARCHAR(40) := 'Development Manager';
   l_batch_id NUMBER := 1111; --set_process_id or data_set_id or batch_id
   l_request_status boolean;
   
   BEGIN
	
   l_error_message :=  NULL;
	
	 -- Deriving Ids from initalization variables
     -- to get the user id 
	 BEGIN
		l_user_id := Apps.FND_LOAD_UTIL.OWNER_ID(p_user_name); 
		--l_user_id := to_number(l_user_name);
		
	 EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_message := 'UserName ' || l_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_error_message := 'Error deriving user_id for UserName - ' || l_user_name;
            RAISE PROGRAM_ERROR;
      END;

	  -- to get the resp id and application id
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE responsibility_name = l_resp_name
            AND SYSDATE BETWEEN start_date AND NVL (end_date,
                                                    TRUNC (SYSDATE) + 1
                                                   );
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_message := 'Responsibility '
               || l_resp_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_error_message :=
                  'Error deriving Responsibility_id for '
               || l_resp_name;
            RAISE PROGRAM_ERROR;
      END;	
		
        -- Environment Initialization
        apps.fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id
                                 );
	
      
	  --DBMS_OUTPUT.put_line('Submitting the Concurrent Program : EGO Import Metadata');
	  fnd_file.put_line (fnd_file.LOG,'Submitting the Concurrent Program : EGO Import Metadata');
       -- Calling the CP API to submit the CP :EGO Import Metadata            
        l_req_id :=
            apps.fnd_request.submit_request (application      => 'EGO',
                                        program          => 'EGOIMDCP',
                                        argument1        => 'N',
                                        argument2        => 'Y',
                                        argument3        => 'Y',
                                        argument4        => l_batch_id,
                                        argument5        => 'N'
                                       );
        COMMIT;
        --DBMS_OUTPUT.put_line('Concurrent Program Request Submitted Successfully. Request ID: ' || l_req_id);
		fnd_file.put_line (fnd_file.LOG,'Concurrent Program Request Submitted Successfully. Request ID: ' || l_req_id);
		
         IF (l_req_id > 0) THEN
            l_request_status :=  apps.fnd_concurrent.wait_for_request
                                       (request_id 	=> l_req_id,
                                       -- interval 	=> 60,
                                       -- max_wait 	=> 90,
                                        phase 		=> l_phase,
                                        status 		=> l_status,
                                        dev_phase	=> l_dev_phase,
                                        dev_status	=> l_dev_status,
                                        message 	=> l_message
                                       );
									   
              IF UPPER(l_status) = 'WARNING' OR UPPER(l_dev_status) = 'WARNING' THEN
                  l_error_message :=
                        'EBS Conc Prog Completed with warning '
                     || l_message;
					--DBMS_OUTPUT.put_line (l_error_message);
					fnd_file.put_line (fnd_file.LOG,l_error_message);
					--retcode := 1;
                  --RAISE PROGRAM_ERROR;
               ELSIF UPPER(l_status) = 'ERROR' OR UPPER(l_dev_status) = 'ERROR' THEN
                  l_error_message :=
                        'EBS Conc Prog Completed with Error '
                     || l_message;
					--DBMS_OUTPUT.put_line (l_error_message);	
					fnd_file.put_line (fnd_file.LOG,l_error_message);					
					--retcode := 2;
					RAISE PROGRAM_ERROR;
               ELSE -- else of l_status check
					--retcode := 0;
					--DBMS_OUTPUT.put_line ('EBS Conc Program Successfully Completed');
					fnd_file.put_line (fnd_file.LOG,'EBS Conc Program Successfully Completed');				   
              END IF; -- end of l_status check
		 
		ELSE	-- else of IF (l_req_id > 0)	 
            l_error_message := 'EBS Conc Program not initated';
            --DBMS_OUTPUT.put_line (l_error_message);
			fnd_file.put_line (fnd_file.LOG,l_error_message);
            RAISE PROGRAM_ERROR;
         END IF;
	
	return l_error_message;
	
  EXCEPTION
  WHEN PROGRAM_ERROR THEN
        --retcode := 2;
	l_error_message := SUBSTR ( l_error_message || SQLCODE  || ' ERROR : ' || SQLERRM || ' Details: '||dbms_utility.format_error_backtrace(),  1,  2000);
	--DBMS_OUTPUT.put_line (errbuf);
	fnd_file.put_line (fnd_file.LOG,l_error_message);
	return l_error_message;
  WHEN OTHERS THEN
        --retcode := 2;
	l_error_message := SUBSTR ( l_error_message || SQLCODE  || ' ERROR : ' || SQLERRM || ' Details: '||dbms_utility.format_error_backtrace(),  1,  2000);
	--DBMS_OUTPUT.put_line (errbuf);
	fnd_file.put_line (fnd_file.LOG,l_error_message);
	return l_error_message;
  END submit_metadata_cp;

END XXWC_EGO_METADATA_UPLOAD_PKG;
