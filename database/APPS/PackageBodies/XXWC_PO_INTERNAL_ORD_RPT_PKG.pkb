SET DEFINE OFF 
CREATE OR REPLACE PACKAGE BODY APPS.xxwc_po_internal_ord_rpt_pkg
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_po_internal_ord_rpt_pkg $
     Module Name: xxwc_po_internal_ord_rpt_pkg.pks

     PURPOSE:   

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/08/2012  Consuelo Gonzalez      Initial Version
     2.0        09/04/2012  Consuelo Gonzalez      TMS 20130904-00914: Fix for UOM Conversion Error
     3.0        11/22/2013  Consuelo Gonzalez      TMS 20131120-00029: Removing reservable field and 
                                                   adding On-Order
     4.0        11/05/2014  Vijaysrinivasan        TMS#20141002-00050 Multi org changes 
	 5.0        04/23/2015  P.Vamshidhar           TMS#20150422-00155 - WC Internal Req Source Avail Report
	                                               (While caliculating on-hand, no need to consider non-nettable sub-inventories).
     6.0        06/01/2015  Pattabhi Avula         TMS#20150513-00241 - Added Nettable condition should be checked for Available 
	                                               quantity, Subinventory name has hard coded as General
     7.0        12/11/2015  Manjula Chellappan     TMS#20151211-00099 - XXWC Internal Requisition Source Availability Report- Available qty is incorrect.
                                                    SET DEFINE OFF added to suppress the &	 
   **************************************************************************/

   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      dbms_output.put_line('Log: '||p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2
                          , p_where   IN VARCHAR2)
   IS
      l_req_id          NUMBER        := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_PO_INTERNAL_ORD_RPT_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                                   DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => l_err_callfrom,
          p_calling                => p_where, --l_err_callpoint,
          p_request_id             => l_req_id,
          p_ora_error_msg          => SUBSTR (p_debug_msg, 1, 2000),
          p_error_desc             => 'Error running xxwc_po_internal_ord_rpt_pkg with PROGRAM ERROR: ',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );
   END write_error;

   /*************************************************************************
     Procedure : gen_open_invoice_file

     PURPOSE:   This procedure creates file for the open invoices to
                Bill Trust
     Parameter:

   ************************************************************************/
   PROCEDURE gen_internal_ord_rpt (
      errbuf             OUT      VARCHAR2,
      retcode            OUT      VARCHAR2,
      p_org_code         IN       VARCHAR2,
      p_req_number       IN       VARCHAR2
   )
 /**************************************************************************     
     Module Name: xxwc_po_internal_ord_rpt_pkg.gen_internal_ord_rpt

     PURPOSE:   Used in the Internal Requisition source availability Report

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/08/2012  Consuelo Gonzalez      Initial Version
     2.0        09/04/2012  Consuelo Gonzalez      TMS 20130904-00914: Fix for UOM Conversion Error
     3.0        11/22/2013  Consuelo Gonzalez      TMS 20131120-00029: Removing reservable field and 
                                                   adding On-Order
     4.0        11/05/2014  Vijaysrinivasan        TMS#20141002-00050 Multi org changes 
	 5.0        04/23/2015  P.Vamshidhar           TMS#20150422-00155 - WC Internal Req Source Avail Report
	                                               (While caliculating on-hand, no need to consider non-nettable sub-inventories).
     6.0        06/01/2015  Pattabhi Avula         TMS#20150513-00241 - Added Nettable condition should be checked for Available 
	                                               quantity, Subinventory name has hard coded as General
     7.0        12/11/2015  Manjula Chellappan     TMS#20151211-00099 - XXWC Internal Requisition Source Availability Report- Available qty is incorrect.
                                                    SET DEFINE OFF added to suppress the &	 
   **************************************************************************/   
   IS
      CURSOR cur_incomp_int_orders
      IS
         SELECT   hou.organization_id operating_unit_id
                 , hou.name operating_unit_name
                 , mp_from.organization_code ship_from_branch_num
                 , TRIM(SUBSTR (ood_from.organization_name
                              , INSTR (ood_from.organization_name
                                     , '-'
                                     , 1
                                     , 1)
                                + 1))
                      ship_from_branch_name
                 , mp_to.organization_code ship_to_branch_num
                 , TRIM (
                      SUBSTR (ood_to.organization_name, INSTR (ood_to.organization_name
                                                             , '-'
                                                             , 1
                                                             , 1)
                                                        + 1)
                   )
                      ship_to_branch_name
                 , req_hdr.authorization_status
                 , req_hdr.segment1 requisition_num
                 , req_ln.line_num
                 , msib.segment1 sku_number
                 , replace(NVL (req_ln.item_description, msib.description),'&','&amp;') sku_description
                 , req_ln.unit_meas_lookup_code po_uom
                 , req_ln.quantity ordered_quantity
                 , req_ln.quantity_received
                 , (SELECT SUM(NVL(xxwc_ont_routines_pkg.get_onhand (req_ln.item_id
                                                   , req_ln.source_organization_id
                                                   , mtsi.secondary_inventory_name
                                                   , 'H'
                                                   , NULL),0)) FROM apps.mtl_secondary_inventories mtsi
                                                   where mtsi.organization_id = req_ln.source_organization_id
                                                   and 	availability_type=1 --V6.0 TMS#20150513-00241  added by pattabhi on 01-Jun-2015
												   and  inventory_atp_code=1
												   and  secondary_inventory_name='General') --V6.0 TMS#20150513-00241  added by pattabhi on 01-Jun-2015
					qoh_source_org							   
				 /*  Commented and added above code by Vamshi 5.0  - TMS#20150422-00155 
                 , xxwc_ont_routines_pkg.get_onhand (req_ln.item_id
                                                   , req_ln.source_organization_id
                                                   , NULL
                                                   , 'H'
                                                   , NULL) 
                      qoh_source_org */
					  -- Starts V6.0 TMS#20150513-00241  added by pattabhi on 04-Jun-2015
				 , (SELECT SUM(NVL(xxwc_ont_routines_pkg.get_onhand (req_ln.item_id
                                                   , req_ln.source_organization_id
                                                   , mtsi.secondary_inventory_name
                                                   , 'H'
                                                   , NULL),0)) FROM apps.mtl_secondary_inventories mtsi
                                                   where mtsi.organization_id = req_ln.source_organization_id
                                                   and  availability_type='1')												   
					qoh_tot_on_hand_qty	
					-- Ends V6.0 TMS#20150513-00241  added by pattabhi on 04-Jun-2015						   	  
                 , xxwc_ont_routines_pkg.get_onhand (req_ln.item_id
                                                   , req_ln.source_organization_id
                                                   , NULL
                                                   , 'R'
                                                   , NULL)
                      atr_source_org
                 , xxwc_ont_routines_pkg.get_onhand (req_ln.item_id
                                                   , req_ln.source_organization_id
                                                   , NULL
                                                   , 'T'
                                                   , NULL)
                      att_source_org
                 , ((SELECT   NVL (
                                     SUM(l.ordered_quantity
                                         * po_uom_s.po_uom_convert (
                                                                  -- 09/04/2013 CG: TMS 20130904-00914: Updated to handle UOM Conversion
                                                                  -- l.order_quantity_uom
                                                                  -- , msib.primary_uom_code
                                                                  um.unit_of_measure
                                                                  , msib.primary_unit_of_measure
                                                                  , l.inventory_item_id))
                                   , 0
                                  )
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
                           FROM   oe_order_lines l, 
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
                                  oe_order_headers h,
                                  -- 09/04/2013 CG: TMS 20130904-00914: Added to handle UOM Conversion
                                  mtl_units_of_measure_vl um
                          WHERE   l.header_id = h.header_id
                                  AND h.flow_status_code NOT IN
                                           ('ENTERED', 'CLOSED', 'CANCELLED')
                                  AND l.flow_status_code = 'AWAITING_SHIPPING'
                                  AND l.ship_from_org_id = req_ln.source_organization_id
                                  AND l.inventory_item_id = msib.inventory_item_id
								  AND l.subinventory ='General' -- Added for ver 7.0
                                  -- 09/04/2013 CG: TMS 20130904-00914: Added to handle UOM Conversion
                                  AND l.order_quantity_uom = um.uom_code)) open_sales_orders
                 , msib.min_minmax_quantity min_quantity_source_org
                 , msib.max_minmax_quantity max_quantity_source_org         
                 , to_number(msib.attribute20) AMU_source_org
                 , cst_cost_api.get_item_cost
                                             (1.0,
                                              msib.inventory_item_id,
                                              req_ln.source_organization_id
                                             ) avg_cost_source_org
                 , (xxwc_mv_routines_pkg.get_aged_inventory_rcvd (msib.inventory_item_id
                                                                , req_ln.source_organization_id
                                                                , NULL
                                                                , NULL
                                                                , 90)) oh_over_ninety_source_org                                   
                 , (xxwc_mv_routines_pkg.get_aged_inventory_rcvd (msib.inventory_item_id
                                                                , req_ln.source_organization_id
                                                                , NULL
                                                                , NULL
                                                                , 270)) oh_over_twoseventy_source_org
                 , NVL (req_hdr.transferred_to_oe_flag, 'N') transferred_to_oe
                 , ooha.order_number so_number
                 , (oola.line_number || '.' || oola.shipment_number) so_line_number
                 , oola.flow_status_code so_line_status
                 , req_hdr.requisition_header_id
                 , req_ln.requisition_line_id
                 , req_ln.source_organization_id from_org_id
                 , req_ln.destination_organization_id to_org_id
                 -- 11/22/2013 CG: TMS 20131120-00029: Adding On Order Function Call
                 , xxwc_inv_ais_pkg.get_onorder_qty (req_ln.source_organization_id
                                                     , msib.inventory_item_id) on_order
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
            FROM   po_requisition_headers req_hdr
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
                 , po_requisition_lines req_ln
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
                 , oe_order_headers ooha
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
                 , oe_order_sources oos
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/05/2014 
                 , oe_order_lines oola
                 , mtl_parameters mp_from
                 , org_organization_definitions ood_from
                 , mtl_parameters mp_to
                 , org_organization_definitions ood_to
                 , mtl_system_items_b msib
                 , hr_operating_units hou
           WHERE       req_hdr.type_lookup_code = 'INTERNAL'
                   AND req_hdr.authorization_status = 'INCOMPLETE' -- NOT IN ('APPROVED', 'CANCELLED')
                   AND req_hdr.requisition_header_id = req_ln.requisition_header_id
                   AND req_ln.requisition_header_id = oola.source_document_id(+)
                   AND req_ln.requisition_line_id = oola.source_document_line_id(+)
                   AND oola.header_id = ooha.header_id(+)
                   AND ooha.order_source_id = oos.order_source_id(+)
                   AND oos.name(+) = 'Internal'
                   AND req_ln.source_organization_id = mp_from.organization_id
                   AND mp_from.organization_id = ood_from.organization_id
                   AND req_ln.destination_organization_id = mp_to.organization_id
                   AND mp_to.organization_id = ood_to.organization_id
                   AND req_ln.item_id = msib.inventory_item_id
                   --AND req_ln.destination_organization_id = msib.organization_id
                   AND req_ln.source_organization_id = msib.organization_id
                   AND ood_from.operating_unit = hou.organization_id
                   AND ood_to.organization_code = p_org_code
                   AND req_hdr.segment1 = NVL(p_req_number, req_hdr.segment1)                   
        ORDER BY   TO_NUMBER (req_hdr.segment1), req_ln.line_num;
    
      v_count       number;
      
   BEGIN
      write_log ('Begining Report Generation for Internal Orders ');
      write_log ('========================================================');
      write_log ('  ');
      --Initialize the Out Parameter
      errbuf := '';
      retcode := '0';
      
      -- Displaying parameters passed in the concurrent program log file
      write_log ('Parameters');
      write_log (RPAD ('Organization Code: ', 25, ' ') || p_org_code);
      write_log (RPAD ('Internal Req Number: ', 25, ' ') || p_req_number);
      
      fnd_file.put_line (fnd_file.output,'<?xml version="1.0"?>');
      fnd_file.put_line (fnd_file.output,'<XXWC_PO_INTERNAL_ORD_RPT>');
      fnd_file.put_line (fnd_file.output,'<ORGANIZATION>'||p_org_code||'</ORGANIZATION>');
      fnd_file.put_line (fnd_file.output,'<REQ_NUMBER>'||p_req_number||'</REQ_NUMBER>');
      fnd_file.put_line (fnd_file.output,'<LIST_G_INTERNAL_ORDERS>');
      
      v_count := 0;
      FOR c0 IN cur_incomp_int_orders
      LOOP
         EXIT WHEN cur_incomp_int_orders%NOTFOUND;
         
         fnd_file.put_line (fnd_file.output,'<INTERNAL_ORDERS>');
         
         fnd_file.put_line (fnd_file.output,'<OPERATING_UNIT_ID>'||c0.operating_unit_id||'</OPERATING_UNIT_ID>');
         fnd_file.put_line (fnd_file.output,'<OPERATING_UNIT_NAME>'||C0.operating_unit_name||'</OPERATING_UNIT_NAME>');
         fnd_file.put_line (fnd_file.output,'<SHIP_FROM_BRANCH_NUM>'||c0.ship_from_branch_num||'</SHIP_FROM_BRANCH_NUM>');
         fnd_file.put_line (fnd_file.output,'<SHIP_FROM_BRANCH_NAME>'||c0.ship_from_branch_name||'</SHIP_FROM_BRANCH_NAME>');
         fnd_file.put_line (fnd_file.output,'<SHIP_TO_BRANCH_NUM>'||c0.ship_to_branch_num||'</SHIP_TO_BRANCH_NUM>');
         fnd_file.put_line (fnd_file.output,'<SHIP_TO_BRANCH_NAME>'||c0.ship_to_branch_name||'</SHIP_TO_BRANCH_NAME>');
         fnd_file.put_line (fnd_file.output,'<REQ_STATUS>'||c0.authorization_status||'</REQ_STATUS>');
         fnd_file.put_line (fnd_file.output,'<REQ_NUMBER>'||c0.requisition_num||'</REQ_NUMBER>');
         fnd_file.put_line (fnd_file.output,'<REQ_LINE_NUMBER>'||c0.line_num||'</REQ_LINE_NUMBER>');
         fnd_file.put_line (fnd_file.output,'<SKU_NUMBER>'||c0.sku_number||'</SKU_NUMBER>');
         fnd_file.put_line (fnd_file.output,'<SKU_DESCRIPTION>'||c0.sku_description||'</SKU_DESCRIPTION>');
         fnd_file.put_line (fnd_file.output,'<PO_LINE_UOM>'||c0.po_uom||'</PO_LINE_UOM>');
         fnd_file.put_line (fnd_file.output,'<QUANTITY_ORDERED>'||c0.ordered_quantity||'</QUANTITY_ORDERED>');
         fnd_file.put_line (fnd_file.output,'<QUANTITY_RECEIVED>'||c0.quantity_received||'</QUANTITY_RECEIVED>');
         fnd_file.put_line (fnd_file.output,'<QOH_SOURCE_ORG>'||c0.qoh_source_org||'</QOH_SOURCE_ORG>');
		 fnd_file.put_line (fnd_file.output,'<QOH_TOTAL_ON_HAND_QTY>'||c0.qoh_tot_on_hand_qty||'</QOH_TOTAL_ON_HAND_QTY>'); -- Version 6.0 TMS#20150513-00241
         fnd_file.put_line (fnd_file.output,'<OPEN_SO_SOURCE_ORG>'||c0.open_sales_orders||'</OPEN_SO_SOURCE_ORG>');
         fnd_file.put_line (fnd_file.output,'<QTY_AVAILABLE_SOURCE_ORG>'||(c0.qoh_source_org - c0.open_sales_orders)||'</QTY_AVAILABLE_SOURCE_ORG>');
		 fnd_file.put_line (fnd_file.output,'<QOH_TOT_ON_HAND_QTY>'||(c0.qoh_tot_on_hand_qty - c0.open_sales_orders)||'</QOH_TOT_ON_HAND_QTY>'); -- Version 6.0 TMS#20150513-00241
         -- 11/22/2013 CG: TMS 20131120-00029: Adding On Order at source org
         fnd_file.put_line (fnd_file.output,'<QTY_ON_ORDER_SOURCE_ORG>'||c0.on_order||'</QTY_ON_ORDER_SOURCE_ORG>');
         -- 11/22/2013 CG: TMS 20131120-00029
         fnd_file.put_line (fnd_file.output,'<ATR_SOURCE_ORG>'||c0.atr_source_org||'</ATR_SOURCE_ORG>');
         fnd_file.put_line (fnd_file.output,'<ATT_SOURCE_ORG>'||c0.att_source_org||'</ATT_SOURCE_ORG>');
         fnd_file.put_line (fnd_file.output,'<MIN_QTY_SOURCE_ORG>'||c0.min_quantity_source_org||'</MIN_QTY_SOURCE_ORG>');
         fnd_file.put_line (fnd_file.output,'<MAX_QTY_SOURCE_ORG>'||c0.max_quantity_source_org||'</MAX_QTY_SOURCE_ORG>');
         fnd_file.put_line (fnd_file.output,'<AMU_SOURCE_ORG>'||c0.AMU_source_org||'</AMU_SOURCE_ORG>');
         fnd_file.put_line (fnd_file.output,'<AVG_COST_SOURCE_ORG>'||c0.avg_cost_source_org||'</AVG_COST_SOURCE_ORG>');
         fnd_file.put_line (fnd_file.output,'<QOH_OVER_NINETY_SOURCE_ORG>'||c0.oh_over_ninety_source_org||'</QOH_OVER_NINETY_SOURCE_ORG>');
         fnd_file.put_line (fnd_file.output,'<QOH_OVER_TWOSEVENTY_SOURCE_ORG>'||c0.oh_over_twoseventy_source_org||'</QOH_OVER_TWOSEVENTY_SOURCE_ORG>');
         fnd_file.put_line (fnd_file.output,'<TRANSFERRED_TO_OE_FLAG>'||c0.transferred_to_oe||'</TRANSFERRED_TO_OE_FLAG>');
         fnd_file.put_line (fnd_file.output,'<SO_NUMBER>'||c0.so_number||'</SO_NUMBER>');
         fnd_file.put_line (fnd_file.output,'<SO_LINE_NUMBER>'||c0.so_line_number||'</SO_LINE_NUMBER>');
         fnd_file.put_line (fnd_file.output,'<SO_LINE_STATUS>'||c0.so_line_status||'</SO_LINE_STATUS>');
         fnd_file.put_line (fnd_file.output,'<REQUSIITION_HEADER_ID>'||c0.requisition_header_id ||'</REQUSIITION_HEADER_ID>');
         fnd_file.put_line (fnd_file.output,'<REQUISITION_LINE_ID>'||c0.requisition_line_id ||'</REQUISITION_LINE_ID>');
         fnd_file.put_line (fnd_file.output,'<FROM_ORG_ID>'||c0.from_org_id||'</FROM_ORG_ID>');
         fnd_file.put_line (fnd_file.output,'<TO_ORG_ID>'||c0.to_org_id||'</TO_ORG_ID>');
         
         fnd_file.put_line (fnd_file.output,'</INTERNAL_ORDERS>');
         v_count := v_count + 1;
                           
      END LOOP;

      fnd_file.put_line (fnd_file.output,'</LIST_G_INTERNAL_ORDERS>');
      fnd_file.put_line (fnd_file.output,'<REQ_COUNT>'||v_count||'</REQ_COUNT>');
      fnd_file.put_line (fnd_file.output,'</XXWC_PO_INTERNAL_ORD_RPT>');
       
      -- Control numbers in log file for concurrent program
      write_log ('  ');
      write_log ('Report generation complete');
      
   EXCEPTION      
      WHEN OTHERS
      THEN
         errbuf := 'Error Msg :' || SQLERRM;
         retcode := '2';
         write_error (errbuf, 'ln 212');
         write_log (errbuf);
   END gen_internal_ord_rpt;

END xxwc_po_internal_ord_rpt_pkg;
/
