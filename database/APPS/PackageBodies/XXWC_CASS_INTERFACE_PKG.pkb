CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CASS_INTERFACE_PKG
AS
   /*************************************************************************
     $Header APPS.XXWC_CASS_INTERFACE_PKG $
     Module Name: APPS.XXWC_CASS_INTERFACE_PKG.pkb

     PURPOSE   :This package populate datafor CASS interface from file provided by CASS
                into staging table and then to Gl interface after validating.
                Journal import program will finally import these transaction to GL in oracle.

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
   **************************************************************************/

   /*************************************************************************
   --Global variables for the Package
   *************************************************************************/
   g_email_distro_list   VARCHAR2 (500)
                            DEFAULT 'HDS.OracleGLSupport@hdsupply.com ';
   g_error_message       VARCHAR2 (5000) := NULL;
   g_request_id          NUMBER := fnd_global.conc_request_id;
   g_exception           EXCEPTION;
   g_prog_exception      EXCEPTION;

   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter: IN
                p_debug_msg      -- Debug Message
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
   ************************************************************************/
   --add message to concurrent log file
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   /*************************************************************************
    Procedure : send_email

    PURPOSE:   This procedure is to send email to stake holder in case of error
    Parameter: IN
               p_error_msg      -- Error Message
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
  ************************************************************************/
   --add message to concurrent log file
   PROCEDURE send_email (p_error_msg IN VARCHAR2, p_subject VARCHAR2)
   IS
      l_subject       VARCHAR2 (400);
      l_body_header   VARCHAR2 (4000);
      l_body_detail   VARCHAR2 (4000);
      l_body_footer   VARCHAR2 (4000);
      l_body          VARCHAR2 (32000);
      l_sender        VARCHAR2 (100) := 'no-reply@whitecap.net';
      l_hostport      VARCHAR2 (20) := '25';
      l_host          VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
      l_instance      VARCHAR2 (50);
   BEGIN
      g_email_distro_list := 'HDS.OracleGLSupport@hdsupply.com';

      ----------------------------------------------------------------------------------
      -- Derive Oracle Instance
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT SUBSTR (UPPER (instance_name), 1, 7)
           INTO l_instance
           FROM v$instance;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_instance := NULL;
      END;

      ------------------------------------------
      -- Send Email Notification
      ------------------------------------------
      l_subject := l_instance || ' ' || p_subject;
      l_body_header :=
         '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
      l_body_detail :=
            '</BR> <BR> '
         || p_error_msg
         || ' </BR> <BR>'
         || p_subject
         || '</BR>';
      l_body_footer :=
            '<BR></BR> Request ID : '
         || g_request_id
         || ' <BR></BR> <BR> Please see the log file for more information. </BR>';
      l_body := l_body_header || l_body_detail || CHR (10) || l_body_footer;

      IF l_instance IN ('EBSDEV')
      THEN
         g_email_distro_list := 'neha.saini@hdsupply.com';
      ELSIF l_instance IN ('EBSQA')
      THEN
         g_email_distro_list := 'Soji.Adeboye@hdsupply.com';
      END IF;

      xxcus_misc_pkg.html_email (p_to              => g_email_distro_list,
                                 p_from            => l_sender,
                                 p_text            => 'CASS Interface Notification:',
                                 p_subject         => l_subject,
                                 p_html            => l_body,
                                 p_smtp_hostname   => l_host,
                                 p_smtp_portnum    => l_hostport);
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, p_error_msg || SQLERRM);
         DBMS_OUTPUT.put_line (p_error_msg || SQLERRM);
   END send_email;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_CASS_INTERFACE_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_CASS_INTERFACE_PKG with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'GL');
   END write_error;

   /*************************************************************************
    Procedure : insert_into_gl_interface

   PURPOSE:   This procedure insert_into_gl_interface
   Parameter: In
               p_gl_int_rec  APPS.GL_INTERFACE%ROWTYPE
        REVISIONS:
      Ver        Date        Author             Description
      ---------  ----------  ---------------   -------------------------
       1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
 ************************************************************************/
   --insert_into_gl_interface
   PROCEDURE insert_into_gl_interface (
      p_gl_int_rec   IN APPS.GL_INTERFACE%ROWTYPE)
   IS
      l_location   VARCHAR2 (4000);
   BEGIN
      l_location := 'insert_into_gl_interface started';
      write_log (l_location);
      g_error_message := NULL;
      l_location := 'insert_statement started';
      write_log (l_location);
      write_log ('testing ' || p_gl_int_rec.STATUS);

      INSERT INTO gl_interface (STATUS,
                                LEDGER_ID,
                                ACCOUNTING_DATE,
                                CURRENCY_CODE,
                                DATE_CREATED,
                                CREATED_BY,
                                ACTUAL_FLAG,
                                USER_JE_CATEGORY_NAME,
                                USER_JE_SOURCE_NAME,
                                CURRENCY_CONVERSION_DATE,
                                ENCUMBRANCE_TYPE_ID,
                                BUDGET_VERSION_ID,
                                USER_CURRENCY_CONVERSION_TYPE,
                                CURRENCY_CONVERSION_RATE,
                                AVERAGE_JOURNAL_FLAG,
                                ORIGINATING_BAL_SEG_VALUE,
                                SEGMENT1,
                                SEGMENT2,
                                SEGMENT3,
                                SEGMENT4,
                                SEGMENT5,
                                SEGMENT6,
                                SEGMENT7,
                                SEGMENT8,
                                SEGMENT9,
                                SEGMENT10,
                                SEGMENT11,
                                SEGMENT12,
                                SEGMENT13,
                                SEGMENT14,
                                SEGMENT15,
                                SEGMENT16,
                                SEGMENT17,
                                SEGMENT18,
                                SEGMENT19,
                                SEGMENT20,
                                SEGMENT21,
                                SEGMENT22,
                                SEGMENT23,
                                SEGMENT24,
                                SEGMENT25,
                                SEGMENT26,
                                SEGMENT27,
                                SEGMENT28,
                                SEGMENT29,
                                SEGMENT30,
                                ENTERED_DR,
                                ENTERED_CR,
                                ACCOUNTED_DR,
                                ACCOUNTED_CR,
                                TRANSACTION_DATE,
                                REFERENCE1,
                                REFERENCE2,
                                REFERENCE3,
                                REFERENCE4,
                                REFERENCE5,
                                REFERENCE6,
                                REFERENCE7,
                                REFERENCE8,
                                REFERENCE9,
                                REFERENCE10,
                                REFERENCE11,
                                REFERENCE12,
                                REFERENCE13,
                                REFERENCE14,
                                REFERENCE15,
                                REFERENCE16,
                                REFERENCE17,
                                REFERENCE18,
                                REFERENCE19,
                                REFERENCE20,
                                REFERENCE21,
                                REFERENCE22,
                                REFERENCE23,
                                REFERENCE24,
                                REFERENCE25,
                                REFERENCE26,
                                REFERENCE27,
                                REFERENCE28,
                                REFERENCE29,
                                REFERENCE30,
                                JE_BATCH_ID,
                                PERIOD_NAME,
                                JE_HEADER_ID,
                                JE_LINE_NUM,
                                CHART_OF_ACCOUNTS_ID,
                                FUNCTIONAL_CURRENCY_CODE,
                                CODE_COMBINATION_ID,
                                DATE_CREATED_IN_GL,
                                WARNING_CODE,
                                STATUS_DESCRIPTION,
                                STAT_AMOUNT,
                                GROUP_ID,
                                REQUEST_ID,
                                SUBLEDGER_DOC_SEQUENCE_ID,
                                SUBLEDGER_DOC_SEQUENCE_VALUE,
                                ATTRIBUTE1,
                                ATTRIBUTE2,
                                GL_SL_LINK_ID,
                                GL_SL_LINK_TABLE,
                                ATTRIBUTE3,
                                ATTRIBUTE4,
                                ATTRIBUTE5,
                                ATTRIBUTE6,
                                ATTRIBUTE7,
                                ATTRIBUTE8,
                                ATTRIBUTE9,
                                ATTRIBUTE10,
                                ATTRIBUTE11,
                                ATTRIBUTE12,
                                ATTRIBUTE13,
                                ATTRIBUTE14,
                                ATTRIBUTE15,
                                ATTRIBUTE16,
                                ATTRIBUTE17,
                                ATTRIBUTE18,
                                ATTRIBUTE19,
                                ATTRIBUTE20,
                                CONTEXT,
                                CONTEXT2,
                                INVOICE_DATE,
                                TAX_CODE,
                                INVOICE_IDENTIFIER,
                                INVOICE_AMOUNT,
                                CONTEXT3,
                                USSGL_TRANSACTION_CODE,
                                DESCR_FLEX_ERROR_MESSAGE,
                                JGZZ_RECON_REF,
                                REFERENCE_DATE,
                                SET_OF_BOOKS_ID,
                                BALANCING_SEGMENT_VALUE,
                                MANAGEMENT_SEGMENT_VALUE,
                                FUNDS_RESERVED_FLAG,
                                CODE_COMBINATION_ID_INTERIM)
           VALUES (p_gl_int_rec.STATUS,                              -- STATUS
                   p_gl_int_rec.LEDGER_ID,                         --LEDGER_ID
                   p_gl_int_rec.ACCOUNTING_DATE,             --ACCOUNTING_DATE
                   p_gl_int_rec.CURRENCY_CODE,                 --CURRENCY_CODE
                   p_gl_int_rec.DATE_CREATED,                   --DATE_CREATED
                   p_gl_int_rec.CREATED_BY,                       --CREATED_BY
                   p_gl_int_rec.ACTUAL_FLAG,                     --ACTUAL_FLAG
                   p_gl_int_rec.USER_JE_CATEGORY_NAME, --USER_JE_CATEGORY_NAME
                   p_gl_int_rec.USER_JE_SOURCE_NAME,    -- USER_JE_SOURCE_NAME
                   p_gl_int_rec.CURRENCY_CONVERSION_DATE, --CURRENCY_CONVERSION_DATE
                   p_gl_int_rec.ENCUMBRANCE_TYPE_ID,     --ENCUMBRANCE_TYPE_ID
                   p_gl_int_rec.BUDGET_VERSION_ID,        -- BUDGET_VERSION_ID
                   p_gl_int_rec.USER_CURRENCY_CONVERSION_TYPE, -- USER_CURRENCY_CONVERSION_TYPE
                   p_gl_int_rec.CURRENCY_CONVERSION_RATE, --CURRENCY_CONVERSION_RATE
                   p_gl_int_rec.AVERAGE_JOURNAL_FLAG,   --AVERAGE_JOURNAL_FLAG
                   p_gl_int_rec.ORIGINATING_BAL_SEG_VALUE, --ORIGINATING_BAL_SEG_VALUE
                   p_gl_int_rec.SEGMENT1,                           --SEGMENT1
                   p_gl_int_rec.SEGMENT2,                           --SEGMENT2
                   p_gl_int_rec.SEGMENT3,                           --SEGMENT3
                   p_gl_int_rec.SEGMENT4,                           --SEGMENT4
                   p_gl_int_rec.SEGMENT5,                           --SEGMENT5
                   p_gl_int_rec.SEGMENT6,                           --SEGMENT6
                   p_gl_int_rec.SEGMENT7,                           --SEGMENT7
                   p_gl_int_rec.SEGMENT8,                          -- SEGMENT8
                   p_gl_int_rec.SEGMENT9,                           --SEGMENT9
                   p_gl_int_rec.SEGMENT10,                         --SEGMENT10
                   p_gl_int_rec.SEGMENT11,                         --SEGMENT11
                   p_gl_int_rec.SEGMENT12,                         --SEGMENT12
                   p_gl_int_rec.SEGMENT13,                         --SEGMENT13
                   p_gl_int_rec.SEGMENT14,                         --SEGMENT14
                   p_gl_int_rec.SEGMENT15,                         --SEGMENT15
                   p_gl_int_rec.SEGMENT16,                         --SEGMENT16
                   p_gl_int_rec.SEGMENT17,                         --SEGMENT17
                   p_gl_int_rec.SEGMENT18,                         --SEGMENT18
                   p_gl_int_rec.SEGMENT19,                         --SEGMENT19
                   p_gl_int_rec.SEGMENT20,                         --SEGMENT20
                   p_gl_int_rec.SEGMENT21,                         --SEGMENT21
                   p_gl_int_rec.SEGMENT22,                         --SEGMENT22
                   p_gl_int_rec.SEGMENT23,                         --SEGMENT23
                   p_gl_int_rec.SEGMENT24,                         --SEGMENT24
                   p_gl_int_rec.SEGMENT25,                         --SEGMENT25
                   p_gl_int_rec.SEGMENT26,                         --SEGMENT26
                   p_gl_int_rec.SEGMENT27,                         --SEGMENT27
                   p_gl_int_rec.SEGMENT28,                         --SEGMENT28
                   p_gl_int_rec.SEGMENT29,                         --SEGMENT29
                   p_gl_int_rec.SEGMENT30,                         --SEGMENT30
                   p_gl_int_rec.ENTERED_DR,                       --ENTERED_DR
                   p_gl_int_rec.ENTERED_CR,                       --ENTERED_CR
                   p_gl_int_rec.ACCOUNTED_DR,                   --ACCOUNTED_DR
                   p_gl_int_rec.ACCOUNTED_CR,                   --ACCOUNTED_CR
                   p_gl_int_rec.TRANSACTION_DATE,           --TRANSACTION_DATE
                   p_gl_int_rec.REFERENCE1,                       --REFERENCE1
                   p_gl_int_rec.REFERENCE2,                       --REFERENCE2
                   p_gl_int_rec.REFERENCE3,                       --REFERENCE3
                   p_gl_int_rec.REFERENCE4,                       --REFERENCE4
                   p_gl_int_rec.REFERENCE5,                       --REFERENCE5
                   p_gl_int_rec.REFERENCE6,                       --REFERENCE6
                   p_gl_int_rec.REFERENCE7,                       --REFERENCE7
                   p_gl_int_rec.REFERENCE8,                       --REFERENCE8
                   p_gl_int_rec.REFERENCE9,                       --REFERENCE9
                   p_gl_int_rec.REFERENCE10,                     --REFERENCE10
                   p_gl_int_rec.REFERENCE11,                     --REFERENCE11
                   p_gl_int_rec.REFERENCE12,                     --REFERENCE12
                   p_gl_int_rec.REFERENCE13,                     --REFERENCE13
                   p_gl_int_rec.REFERENCE14,                     --REFERENCE14
                   p_gl_int_rec.REFERENCE15,                     --REFERENCE15
                   p_gl_int_rec.REFERENCE16,                     --REFERENCE16
                   p_gl_int_rec.REFERENCE17,                     --REFERENCE17
                   p_gl_int_rec.REFERENCE18,                     --REFERENCE18
                   p_gl_int_rec.REFERENCE19,                     --REFERENCE19
                   p_gl_int_rec.REFERENCE20,                     --REFERENCE20
                   p_gl_int_rec.REFERENCE21,                     --REFERENCE21
                   p_gl_int_rec.REFERENCE22,                     --REFERENCE22
                   p_gl_int_rec.REFERENCE23,                     --REFERENCE23
                   p_gl_int_rec.REFERENCE24,                     --REFERENCE24
                   p_gl_int_rec.REFERENCE25,                     --REFERENCE25
                   p_gl_int_rec.REFERENCE26,                     --REFERENCE26
                   p_gl_int_rec.REFERENCE27,                     --REFERENCE27
                   p_gl_int_rec.REFERENCE28,                     --REFERENCE28
                   p_gl_int_rec.REFERENCE29,                     --REFERENCE29
                   p_gl_int_rec.REFERENCE30,                     --REFERENCE30
                   p_gl_int_rec.JE_BATCH_ID,                     --JE_BATCH_ID
                   p_gl_int_rec.PERIOD_NAME,                     --PERIOD_NAME
                   p_gl_int_rec.JE_HEADER_ID,                   --JE_HEADER_ID
                   p_gl_int_rec.JE_LINE_NUM,                     --JE_LINE_NUM
                   p_gl_int_rec.CHART_OF_ACCOUNTS_ID,   --CHART_OF_ACCOUNTS_ID
                   p_gl_int_rec.FUNCTIONAL_CURRENCY_CODE, --FUNCTIONAL_CURRENCY_CODE
                   p_gl_int_rec.CODE_COMBINATION_ID,     --CODE_COMBINATION_ID
                   p_gl_int_rec.DATE_CREATED_IN_GL,       --DATE_CREATED_IN_GL
                   p_gl_int_rec.WARNING_CODE,                   --WARNING_CODE
                   p_gl_int_rec.STATUS_DESCRIPTION,       --STATUS_DESCRIPTION
                   p_gl_int_rec.STAT_AMOUNT,                     --STAT_AMOUNT
                   p_gl_int_rec.GROUP_ID,                           --GROUP_ID
                   p_gl_int_rec.REQUEST_ID,                       --REQUEST_ID
                   p_gl_int_rec.SUBLEDGER_DOC_SEQUENCE_ID, --SUBLEDGER_DOC_SEQUENCE_ID
                   p_gl_int_rec.SUBLEDGER_DOC_SEQUENCE_VALUE, --SUBLEDGER_DOC_SEQUENCE_VALUE
                   p_gl_int_rec.ATTRIBUTE1,                       --ATTRIBUTE1
                   p_gl_int_rec.ATTRIBUTE2,                       --ATTRIBUTE2
                   p_gl_int_rec.GL_SL_LINK_ID,                 --GL_SL_LINK_ID
                   p_gl_int_rec.GL_SL_LINK_TABLE,           --GL_SL_LINK_TABLE
                   p_gl_int_rec.ATTRIBUTE3,                       --ATTRIBUTE3
                   p_gl_int_rec.ATTRIBUTE4,                       --ATTRIBUTE4
                   p_gl_int_rec.ATTRIBUTE5,                       --ATTRIBUTE5
                   p_gl_int_rec.ATTRIBUTE6,                       --ATTRIBUTE6
                   p_gl_int_rec.ATTRIBUTE7,                       --ATTRIBUTE7
                   p_gl_int_rec.ATTRIBUTE8,                      -- ATTRIBUTE8
                   p_gl_int_rec.ATTRIBUTE9,                       --ATTRIBUTE9
                   p_gl_int_rec.ATTRIBUTE10,                     --ATTRIBUTE10
                   p_gl_int_rec.ATTRIBUTE11,                     --ATTRIBUTE11
                   p_gl_int_rec.ATTRIBUTE12,                     --ATTRIBUTE12
                   p_gl_int_rec.ATTRIBUTE13,                     --ATTRIBUTE13
                   p_gl_int_rec.ATTRIBUTE14,                     --ATTRIBUTE14
                   p_gl_int_rec.ATTRIBUTE15,                     --ATTRIBUTE15
                   p_gl_int_rec.ATTRIBUTE16,                     --ATTRIBUTE16
                   p_gl_int_rec.ATTRIBUTE17,                     --ATTRIBUTE17
                   p_gl_int_rec.ATTRIBUTE18,                     --ATTRIBUTE18
                   p_gl_int_rec.ATTRIBUTE19,                     --ATTRIBUTE19
                   p_gl_int_rec.ATTRIBUTE20,                     --ATTRIBUTE20
                   p_gl_int_rec.CONTEXT,                             --CONTEXT
                   p_gl_int_rec.CONTEXT2,                           --CONTEXT2
                   p_gl_int_rec.INVOICE_DATE,                   --INVOICE_DATE
                   p_gl_int_rec.TAX_CODE,                           --TAX_CODE
                   p_gl_int_rec.INVOICE_IDENTIFIER,       --INVOICE_IDENTIFIER
                   p_gl_int_rec.INVOICE_AMOUNT,               --INVOICE_AMOUNT
                   p_gl_int_rec.CONTEXT3,                           --CONTEXT3
                   p_gl_int_rec.USSGL_TRANSACTION_CODE, --USSGL_TRANSACTION_CODE
                   p_gl_int_rec.DESCR_FLEX_ERROR_MESSAGE, --DESCR_FLEX_ERROR_MESSAGE
                   p_gl_int_rec.JGZZ_RECON_REF,               --JGZZ_RECON_REF
                   p_gl_int_rec.REFERENCE_DATE,               --REFERENCE_DATE
                   p_gl_int_rec.SET_OF_BOOKS_ID,             --SET_OF_BOOKS_ID
                   p_gl_int_rec.BALANCING_SEGMENT_VALUE, --BALANCING_SEGMENT_VALUE
                   p_gl_int_rec.MANAGEMENT_SEGMENT_VALUE, -- MANAGEMENT_SEGMENT_VALUE
                   p_gl_int_rec.FUNDS_RESERVED_FLAG,    -- FUNDS_RESERVED_FLAG
                   p_gl_int_rec.CODE_COMBINATION_ID_INTERIM --CODE_COMBINATION_ID_INTERIM
                                                           );

      l_location := SQL%ROWCOUNT || ' records inserted into GL_INTERFACE';
      write_log (l_location);
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_error_message :=
               'Failed in  insert_into_gl_interface at '
            || l_location
            || ' with Error '
            || SQLERRM;
   END insert_into_gl_interface;

   /*************************************************************************
      Procedure : populate_accrual_staging_table

     PURPOSE:   This procedure is to read file and transfer data into staging table
     Parameter: NONE
          REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
   ************************************************************************/

   --Process Accrual file
   PROCEDURE populate_accrual_staging_table
   IS
      l_loc   VARCHAR2 (500);
   BEGIN
      l_loc := 'started populate_accrual_staging_table: ';
      write_log (l_loc);
      g_error_message := NULL;

      l_loc := 'Start inserting into staging Table';
      write_log (l_loc);

      INSERT INTO XXWC.XXWC_CASS_GL_ACCRUAL
         (SELECT TRIM (NOTIFICATION_DATE),
                 TRIM (CARRIER_NAME),
                 TRIM (SHIP_DATE),
                 TRIM (INVOICE_DATE),
                 TRIM (CARRIER_PRO_NUMBER),
                 TRIM (SHIPMENT_NUMBER),
                 TRIM (SHIPMENT_MODE),
                 TRIM (MOVEMENT_TYPE),
                 TRIM (ORIGIN_NAME),
                 TRIM (ORIGIN_CITY),
                 TRIM (ORIGIN_STATE),
                 TRIM (ORIGIN_POSTAL_CODE),
                 TRIM (ORIGIN_CC),
                 TRIM (DESTINATION_NAME),
                 TRIM (DESTINATION_CITY),
                 TRIM (DESTINATION_STATE),
                 TRIM (DESTINATION_POSTAL_CODE),
                 TRIM (DESTINATION_CC),
                 TRIM (GL_ACCOUNT_CODE),
                 TRIM (GL_STRING),
                 TRIM (BILL_TO_FRU),                                     --GL1
                 TRIM (GL2),
                 TRIM (BILLED_AMOUNT),
                 TRIM (PAID_AMOUNT),
                 TRIM (GST),
                 TRIM (HST),
                 TRIM (QST),
                 TRIM (FSC),
                 TRIM (LINE_HAUL),
                 TRIM (OTHER),
                 TRIM (FISCAL_MONTH),
                 TRIM (AUDIT_MATCH),
                 TRIM (SHIPMENT_ESTIMATE),
                 TRIM (HDS_LINE_HAUL),
                 TRIM (HDS_FSC),
                 TRIM (HDS_ACCESSORIALS),
                 TRIM (CUSTOMER_NAME),
                 TRIM (SALES_ORDER_NUMBER),
                 TRIM (VENDOR_NAME),
                 TRIM (PURCHASE_ORDER_NUMBER),
                 TRIM (SENDER_OR_RECEIVER),
                 TRIM (RECEIVING_DATE),
                 TRIM (PRO_RECEIVED_DATE),
                 TRIM (SCHEDULED_PAY_DATE),
                 TRIM (INVOICE_DUE_DATE),
                 TRIM (SHIPMENT_NUMBER),
                 TRIM (BILLED_WEIGHT),
                 TRIM (WEIGHT_UOM),
                 TRIM (BILLED_CUBE_WEIGHT),
                 TRIM (CUBE_UOM),
                 TRIM (LENGTH),
                 TRIM (WIDTH),
                 TRIM (HEIGHT),
                 TRIM (FUNDS_TYPE),
                 TRIM (BILL_NAME),
                 TRIM (BILL_TO_FRU),
                 TRIM (CONSIGNEE_NAME),
                 TRIM (NO_OF_PIECES),
                 TRIM (QTY_UOM),
                 TRIM (TARGET_ERP_SYSTEM),
                 TRIM (CARRIER_STATUS),
                 XXWC.XXWC_CASS_INBACC_INTF_SEQUENCE.NEXTVAL record_id,
                 SYSDATE,
                 1230,
                 'NEW'
            FROM XXWC.XXWC_CASS_ACCR_INTF_EXT_TBL a);

      l_loc := 'no of rows inserted ' || SQL%ROWCOUNT;
      write_log (l_loc);
      COMMIT;



      l_loc := 'populate_accrual_staging_table successfully completed';
      write_log (l_loc);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_error_message :=
               'Failed in populate_accrual_staging_table:  at '
            || l_loc
            || ' with Error '
            || SQLERRM;
   END populate_accrual_staging_table;

   /*************************************************************************
      Procedure : populate_accrual_staging_table

     PURPOSE:   This procedure is to read file and transfer data into staging table
     Parameter: NONE
          REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
   ************************************************************************/

   --Process Detail file
   PROCEDURE populate_detail_staging_table
   IS
      l_loc   VARCHAR2 (500);
   BEGIN
      l_loc := 'started populate_detail_staging_table: ';
      write_log (l_loc);

      g_error_message := NULL;

      l_loc := 'start inserting into staging  tables';
      write_log (l_loc);

      INSERT INTO XXWC.XXWC_CASS_GL_DETAIL
         (SELECT TRIM (NOTIFICATION_DATE),
                 TRIM (CARRIER_NAME),
                 TRIM (SHIP_DATE),
                 TRIM (INVOICE_DATE),
                 TRIM (CARRIER_PRO_NUMBER),
                 TRIM (SHIPMENT_NUMBER),
                 TRIM (SHIPMENT_MODE),
                 TRIM (MOVEMENT_TYPE),
                 TRIM (ORIGIN_NAME),
                 TRIM (ORIGIN_CITY),
                 TRIM (ORIGIN_STATE),
                 TRIM (ORIGIN_POSTAL_CODE),
                 TRIM (ORIGIN_CC),
                 TRIM (DESTINATION_NAME),
                 TRIM (DESTINATION_CITY),
                 TRIM (DESTINATION_STATE),
                 TRIM (DESTINATION_POSTAL_CODE),
                 TRIM (DESTINATION_CC),
                 TRIM (GL_ACCOUNT_CODE),
                 TRIM (GL_STRING),
                 TRIM (BILL_TO_FRU),                                     --GL1
                 TRIM (GL2),
                 TRIM (BILLED_AMOUNT),
                 TRIM (PAID_AMOUNT),
                 TRIM (GST),
                 TRIM (HST),
                 TRIM (QST),
                 TRIM (FSC),
                 TRIM (LINE_HAUL),
                 TRIM (OTHER),
                 TRIM (FISCAL_MONTH),
                 TRIM (AUDIT_MATCH),
                 TRIM (SHIPMENT_ESTIMATE),
                 TRIM (HDS_LINE_HAUL),
                 TRIM (HDS_FSC),
                 TRIM (HDS_ACCESSORIALS),
                 TRIM (CUSTOMER_NAME),
                 TRIM (SALES_ORDER_NUMBER),
                 TRIM (VENDOR_NAME),
                 TRIM (PURCHASE_ORDER_NUMBER),
                 TRIM (SENDER_OR_RECEIVER),
                 TRIM (RECEIVING_DATE),
                 TRIM (PRO_RECEIVED_DATE),
                 TRIM (SCHEDULED_PAY_DATE),
                 TRIM (INVOICE_DUE_DATE),
                 TRIM (SHIPMENT_NUMBER),
                 TRIM (BILLED_WEIGHT),
                 TRIM (WEIGHT_UOM),
                 TRIM (BILLED_CUBE_WEIGHT),
                 TRIM (CUBE_UOM),
                 TRIM (LENGTH),
                 TRIM (WIDTH),
                 TRIM (HEIGHT),
                 TRIM (FUNDS_TYPE),
                 TRIM (BILL_NAME),
                 TRIM (BILL_TO_FRU),
                 TRIM (CONSIGNEE_NAME),
                 TRIM (NO_OF_PIECES),
                 TRIM (QTY_UOM),
                 TRIM (TARGET_ERP_SYSTEM),
                 TRIM (CARRIER_STATUS),
                 XXWC.XXWC_CASS_INBDET_INTF_SEQUENCE.NEXTVAL,
                 SYSDATE,
                 1230,
                 'NEW'
            FROM XXWC.XXWC_CASS_DET_INTF_EXT_TBL a);

      l_loc := 'no of rows inserted ' || SQL%ROWCOUNT;
      write_log (l_loc);
      COMMIT;


      l_loc := 'completed populate_detail_staging_table successfully: ';
      write_log (l_loc);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_error_message :=
               ' Failed in populate_detail_staging_table: at '
            || l_loc
            || ' with Error '
            || SQLERRM;
   END populate_detail_staging_table;

   /*************************************************************************
         Procedure : transfer_to_gl

        PURPOSE:   This procedure is to transfer_to_gl
        Parameter: NONE
             REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
      ************************************************************************/

   --transfer_to_gl
   PROCEDURE transfer_to_gl_intf_detail
   IS
      l_count               NUMBER;
      l_loc                 VARCHAR2 (500);
      l_entered_cr          GL_INTERFACE.ENTERED_CR%TYPE;
      l_entered_dr          GL_INTERFACE.ENTERED_DR%TYPE;
      l_gl_int_rec          GL_INTERFACE%ROWTYPE := NULL;
      l_gl_int_contra_rec   GL_INTERFACE%ROWTYPE := NULL;
      l_segment2            GL_INTERFACE.segment2%TYPE := NULL;
      l_segment1            GL_INTERFACE.segment1%TYPE := NULL;
      l_segment3            GL_INTERFACE.segment1%TYPE := NULL;
      l_segment4            GL_INTERFACE.segment1%TYPE := NULL;
      l_segment5            GL_INTERFACE.segment1%TYPE := NULL;
      l_segment6            GL_INTERFACE.segment1%TYPE := NULL;
      l_segment7            GL_INTERFACE.segment1%TYPE := NULL;
      l_reference10         VARCHAR2 (10000);
   BEGIN
      l_loc := 'started transfer_to_gl for detail file';
      write_log (l_loc);

      g_error_message := NULL;

      FOR rec IN (SELECT * FROM XXWC.XXWC_CASS_GL_DETAIL)
      LOOP
         -- resetting the global record type
         l_gl_int_rec := NULL;

         -- Populate record type for gl_interface
         l_loc := 'Getting Entered_CR and Entered_DR value';
         write_log (l_loc);

         IF rec.PAID_AMOUNT > 0
         THEN
            l_entered_cr := 0;
            l_entered_dr := rec.PAID_AMOUNT;
         ELSIF rec.PAID_AMOUNT < 0
         THEN
            l_entered_cr := rec.PAID_AMOUNT;
            l_entered_dr := 0;
         END IF;

         l_loc :=
               'Getting Entered_CR and Entered_DR value: '
            || l_entered_dr
            || ' '
            || l_entered_cr;
         write_log (l_loc);

         l_reference10 :=
               rec.CARRIER_NAME
            || '|'
            || rec.CARRIER_PRO_NUMBER
            || '|'
            || rec.BILL_OF_LADING_No;

         IF     rec.BILL_OF_LADING_NO IS NULL
            AND rec.CARRIER_PRO_NUMBER IS NULL
            AND rec.CARRIER_NAME IS NULL
         THEN
            l_reference10 := NULL;
         END IF;

         l_loc := 'Getting Reference 10 value ' || l_reference10;
         write_log (l_loc);



         BEGIN
            SELECT COUNT (1) INTO l_count FROM xxcus.xxcus_location_code_tbl;

            SELECT ENTRP_LOC, ENTRP_ENTITY
              INTO l_segment2, l_segment1
              FROM apps.xxcus_location_code_vw
             WHERE FRU = TRIM (rec.GL1);
         EXCEPTION
            WHEN OTHERS
            THEN
               g_error_message :=
                     'Failed in transfer_to_gl_intf_detail : at  '
                  || l_loc
                  || '  Error: '
                  || SQLERRM;
               RAISE g_exception;
         END;

         l_loc := 'Getting segment 1 and segment 2 from location code table';
         write_log (l_loc);
         l_loc := 'l_segment2,l_segment1 ' || l_segment2 || ' ' || l_segment1;
         write_log (l_loc);
         l_gl_int_rec.STATUS := 'NEW';
         l_gl_int_rec.LEDGER_ID := 2061;
         l_gl_int_rec.GROUP_ID := 10007;
         l_gl_int_rec.REFERENCE7 := NULL;
         l_gl_int_rec.ACCOUNTED_DR := l_entered_dr;
         l_gl_int_rec.ACCOUNTED_CR := l_entered_cr;
         l_gl_int_rec.SET_OF_BOOKS_ID := -1;
         l_gl_int_rec.ACCOUNTING_DATE := SYSDATE;
         l_gl_int_rec.CURRENCY_CODE := 'USD';
         l_gl_int_rec.DATE_CREATED := SYSDATE;
         l_gl_int_rec.CREATED_BY := 1230;
         l_gl_int_rec.ACTUAL_FLAG := 'A';
         l_gl_int_rec.USER_JE_CATEGORY_NAME := 'Freight';
         l_gl_int_rec.USER_JE_SOURCE_NAME := 'CASS';
         l_gl_int_rec.ENTERED_CR := l_entered_cr;
         l_gl_int_rec.ENTERED_DR := l_entered_dr;
         l_gl_int_rec.REFERENCE10 := l_reference10;
         l_gl_int_rec.SEGMENT1 := l_segment1;
         l_gl_int_rec.SEGMENT2 := l_segment2;
         l_gl_int_rec.SEGMENT3 := '0000';
         l_gl_int_rec.SEGMENT4 := rec.gl_account_code;
         l_gl_int_rec.SEGMENT5 := '00000';
         l_gl_int_rec.SEGMENT6 := '00000';
         l_gl_int_rec.SEGMENT7 := '00000';
         l_gl_int_rec.ATTRIBUTE1 := rec.record_id;         --unique identifier

         l_loc :=
            'Now calling insert_into_gl_interface to insert record in gl_interface';
         write_log (l_loc);
         insert_into_gl_interface (p_gl_int_rec => l_gl_int_rec);

         IF g_error_message IS NOT NULL
         THEN
            RAISE g_exception;
         END IF;

         l_loc := 'Now calling insert_into_gl_interface for counter entry';
         write_log (l_loc);
         l_loc := 'Getting ENTERED_CR and ENTERED_DR value for counter enrty';
         write_log (l_loc);

         IF l_entered_cr = 0
         THEN
            l_entered_cr := rec.PAID_AMOUNT;
            l_entered_dr := 0;
         ELSIF l_entered_dr = 0
         THEN
            l_entered_cr := 0;
            l_entered_dr := rec.PAID_AMOUNT;
         END IF;

         l_loc :=
            'Getting SEGMENT 1 AND SEGMENT 2,3,4,5,6,7  for counter entry';

         IF l_segment1 IN ('48', '53')
         THEN
            l_segment1 := '48';
            l_segment2 := 'Z0186';
            l_segment3 := '0000';
            l_segment4 := '930118';
            l_segment5 := '00000';
            l_segment6 := '00000';
            l_segment7 := '00000';
         ELSIF l_segment1 IN ('0W')
         THEN
            l_segment1 := '0W';
            l_segment2 := 'BW080';
            l_segment3 := '0000';
            l_segment4 := '930118';
            l_segment5 := '00000';
            l_segment6 := '00000';
            l_segment7 := '00000';
         ELSE
            l_segment1 := l_segment1;
            l_segment2 := l_segment2;
            l_segment3 := '0000';
            l_segment4 := rec.gl_account_code;
            l_segment5 := '00000';
            l_segment6 := '00000';
            l_segment7 := '00000';
         END IF;


         l_loc := 'populating contra enerty record';

         write_log (l_loc);

         l_gl_int_contra_rec.STATUS := 'NEW';
         l_gl_int_contra_rec.LEDGER_ID := 2061;
         l_gl_int_contra_rec.GROUP_ID := 10007;
         l_gl_int_contra_rec.REFERENCE7 := NULL;
         l_gl_int_contra_rec.ACCOUNTED_DR := l_entered_dr;
         l_gl_int_contra_rec.ACCOUNTED_CR := l_entered_cr;
         l_gl_int_contra_rec.SET_OF_BOOKS_ID := -1;
         l_gl_int_contra_rec.ACCOUNTING_DATE := SYSDATE;
         l_gl_int_contra_rec.CURRENCY_CODE := 'USD';
         l_gl_int_contra_rec.DATE_CREATED := SYSDATE;
         l_gl_int_contra_rec.CREATED_BY := 1230;
         l_gl_int_contra_rec.ACTUAL_FLAG := 'A';
         l_gl_int_contra_rec.USER_JE_CATEGORY_NAME := 'Freight';
         l_gl_int_contra_rec.USER_JE_SOURCE_NAME := 'CASS';
         l_gl_int_contra_rec.ENTERED_CR := l_entered_cr;
         l_gl_int_contra_rec.ENTERED_DR := l_entered_dr;
         l_gl_int_contra_rec.REFERENCE10 := l_reference10;
         l_gl_int_contra_rec.SEGMENT1 := l_segment1;
         l_gl_int_contra_rec.SEGMENT2 := l_segment2;
         l_gl_int_contra_rec.SEGMENT3 := l_segment3;
         l_gl_int_contra_rec.SEGMENT4 := l_segment4;
         l_gl_int_contra_rec.SEGMENT5 := l_segment5;
         l_gl_int_contra_rec.SEGMENT6 := l_segment6;
         l_gl_int_contra_rec.SEGMENT7 := l_segment7;
         l_gl_int_contra_rec.ATTRIBUTE1 := rec.record_id;  --unique identifier

         l_loc := 'Now insert into GL_INTERFACE';
         write_log (l_loc);
         insert_into_gl_interface (p_gl_int_rec => l_gl_int_contra_rec);

         IF g_error_message IS NOT NULL
         THEN
            RAISE g_exception;
         END IF;
      END LOOP;


      l_loc := 'completed transfer_to_gl successfully: ';
      write_log (l_loc);
   EXCEPTION
      WHEN g_exception
      THEN
         g_error_message :=
               'Error in transfer_to_gl_intf_detail: at '
            || l_loc
            || ' - '
            || g_error_message;
      WHEN OTHERS
      THEN
         g_error_message :=
               g_error_message
            || 'Failed  in transfer_to_gl_intf_detail at '
            || l_loc
            || ' with Error '
            || SQLERRM;
   END transfer_to_gl_intf_detail;

   /*************************************************************************
         Procedure : transfer_to_gl_intf_accrual

        PURPOSE:   This procedure is to transfer_to_gl_intf_accrual
        Parameter: NONE
             REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
      ************************************************************************/

   --transfer_to_gl
   PROCEDURE transfer_to_gl_intf_accrual
   IS
      l_loc                 VARCHAR2 (500);
      l_next_open_period    APPS.GL_PERIODS.PERIOD_NAME%TYPE;
      l_entered_cr          GL_INTERFACE.ENTERED_CR%TYPE;
      l_entered_dr          GL_INTERFACE.ENTERED_DR%TYPE;
      l_gl_int_rec          GL_INTERFACE%ROWTYPE := NULL;
      l_gl_int_contra_rec   GL_INTERFACE%ROWTYPE := NULL;
      l_segment2            GL_INTERFACE.segment2%TYPE := NULL;
      l_segment1            GL_INTERFACE.segment1%TYPE := NULL;
      l_segment3            GL_INTERFACE.segment1%TYPE := NULL;
      l_segment4            GL_INTERFACE.segment1%TYPE := NULL;
      l_segment5            GL_INTERFACE.segment1%TYPE := NULL;
      l_segment6            GL_INTERFACE.segment1%TYPE := NULL;
      l_segment7            GL_INTERFACE.segment1%TYPE := NULL;
      l_reference10         VARCHAR2 (10000);
   BEGIN
      l_loc := 'started transfer_to_gl_intf_accrual: ';
      write_log (l_loc);
      g_error_message := NULL;

      l_loc := 'getting next open period';
      write_log (l_loc);

      BEGIN
         SELECT period_name
           INTO l_next_open_period
           FROM apps.gl_periods
          WHERE TRUNC (SYSDATE) BETWEEN TRUNC (start_date)
                                    AND TRUNC (end_date);
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
               'Error while getting next open period ' || SQLERRM;
            RAISE g_exception;
      END;


      l_loc := 'l_next_open_period: ' || l_next_open_period;
      write_log (l_loc);

      FOR rec IN (SELECT * FROM XXWC.XXWC_CASS_GL_ACCRUAL)
      LOOP
         l_loc := 'Getting REFERENCE 10 value';

         l_reference10 :=
               rec.CARRIER_NAME
            || '|'
            || rec.CARRIER_PRO_NUMBER
            || '|'
            || rec.BILL_OF_LADING_NO;

         IF     rec.BILL_OF_LADING_NO IS NULL
            AND rec.CARRIER_PRO_NUMBER IS NULL
            AND rec.CARRIER_NAME IS NULL
         THEN
            l_reference10 := NULL;
         END IF;

         l_loc := 'l_reference10 ' || l_reference10;
         write_log (l_loc);



         IF rec.PAID_AMOUNT > 0
         THEN
            l_entered_cr := 0;
            l_entered_dr := rec.PAID_AMOUNT;
         ELSIF rec.PAID_AMOUNT < 0
         THEN
            l_entered_cr := rec.PAID_AMOUNT;
            l_entered_dr := 0;
         END IF;

         l_loc := 'Getting ENTERED_CR  value ' || l_entered_cr;
         write_log (l_loc);
         l_loc := 'Getting ENTERED_DR value ' || l_entered_dr;
         write_log (l_loc);


         l_loc := 'Getting SEGMENT 1 AND SEGMENT 2 from location code table ';
         write_log (l_loc);

         BEGIN
            SELECT ENTRP_LOC, ENTRP_ENTITY
              INTO l_segment2, l_segment1
              FROM xxcus.xxcus_location_code_tbl
             WHERE FRU = rec.GL1;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_error_message :=
                     'Error while getting segment 1 and segment 2 -product and location FRU value '
                  || SQLERRM;
               RAISE g_exception;
         END;

         l_loc := 'l_segment2,l_segment1 ' || l_segment2 || ' ' || l_segment1;
         write_log (l_loc);
         l_gl_int_rec.STATUS := 'NEW';
         l_gl_int_rec.LEDGER_ID := 2061;
         l_gl_int_rec.GROUP_ID := 10007;
         l_gl_int_rec.ACCOUNTED_DR := l_entered_dr;
         l_gl_int_rec.ACCOUNTED_CR := l_entered_cr;
         l_gl_int_rec.SET_OF_BOOKS_ID := -1;
         l_gl_int_rec.ACCOUNTING_DATE := SYSDATE;
         l_gl_int_rec.CURRENCY_CODE := 'USD';
         l_gl_int_rec.DATE_CREATED := SYSDATE;
         l_gl_int_rec.CREATED_BY := 1230;
         l_gl_int_rec.ACTUAL_FLAG := 'A';
         l_gl_int_rec.USER_JE_CATEGORY_NAME := 'Freight Accrual';
         l_gl_int_rec.USER_JE_SOURCE_NAME := 'CASS';
         l_gl_int_rec.ENTERED_CR := l_entered_cr;          --need to calculate
         l_gl_int_rec.ENTERED_DR := l_entered_dr;          --need to calculate
         l_gl_int_rec.REFERENCE7 := 'Yes';
         l_gl_int_rec.REFERENCE8 := l_next_open_period;
         l_gl_int_rec.REFERENCE10 := l_reference10;
         l_gl_int_rec.SEGMENT1 := l_segment1;              --need to calculate
         l_gl_int_rec.SEGMENT2 := l_segment2;              --need to calculate
         l_gl_int_rec.SEGMENT3 := '0000';
         l_gl_int_rec.SEGMENT4 := rec.gl_account_code;
         l_gl_int_rec.SEGMENT5 := '00000';
         l_gl_int_rec.SEGMENT6 := '00000';
         l_gl_int_rec.SEGMENT7 := '00000';
         l_gl_int_rec.ATTRIBUTE1 := rec.record_id;         --unique identifier


         l_loc := 'Now calling insert_into_gl_interface';
         write_log (l_loc);
         insert_into_gl_interface (p_gl_int_rec => l_gl_int_rec);

         IF g_error_message IS NOT NULL
         THEN
            RAISE g_exception;
         END IF;

         l_loc := 'Getting ENTERED_CR AND ENTERED_DR value for contra entry';
         write_log (l_loc);

         IF l_entered_cr = 0
         THEN
            l_entered_cr := rec.PAID_AMOUNT;
            l_entered_dr := 0;
         ELSIF l_entered_dr = 0
         THEN
            l_entered_cr := 0;
            l_entered_dr := rec.PAID_AMOUNT;
         END IF;


         l_loc :=
            'Getting SEGMENT 1 AND SEGMENT 2,3,4,5,6,7  for counter entry';
         write_log (l_loc);

         IF l_segment1 IN ('48', '53')
         THEN
            l_segment1 := '48';
            l_segment2 := 'Z0186';
            l_segment3 := '0000';
            l_segment4 := '212100';
            l_segment5 := '00000';
            l_segment6 := '00000';
            l_segment7 := '00000';
         ELSIF l_segment1 IN ('0W')
         THEN
            l_segment1 := '0W';
            l_segment2 := 'BW080';
            l_segment3 := '0000';
            l_segment4 := '212100';      --need to check from soji what value?
            l_segment5 := '00000';
            l_segment6 := '00000';
            l_segment7 := '00000';
         ELSE
            l_segment1 := l_segment1;
            l_segment2 := l_segment2;
            l_segment3 := '0000';
            l_segment4 := rec.gl_account_code;
            l_segment5 := '00000';
            l_segment6 := '00000';
            l_segment7 := '00000';
         END IF;



         l_gl_int_contra_rec.STATUS := 'NEW';
         l_gl_int_contra_rec.LEDGER_ID := 2061;
         l_gl_int_contra_rec.GROUP_ID := 10007;
         l_gl_int_contra_rec.ACCOUNTED_DR := l_entered_dr;
         l_gl_int_contra_rec.ACCOUNTED_CR := l_entered_cr;
         l_gl_int_contra_rec.SET_OF_BOOKS_ID := -1;
         l_gl_int_contra_rec.ACCOUNTING_DATE := SYSDATE;
         l_gl_int_contra_rec.CURRENCY_CODE := 'USD';
         l_gl_int_contra_rec.DATE_CREATED := SYSDATE;
         l_gl_int_contra_rec.CREATED_BY := 1230;
         l_gl_int_contra_rec.ACTUAL_FLAG := 'A';
         l_gl_int_contra_rec.USER_JE_CATEGORY_NAME := 'Freight Accrual';
         l_gl_int_contra_rec.USER_JE_SOURCE_NAME := 'CASS';
         l_gl_int_contra_rec.ENTERED_CR := l_entered_cr;   --need to calculate
         l_gl_int_contra_rec.ENTERED_DR := l_entered_dr;   --need to calculate
         l_gl_int_contra_rec.REFERENCE7 := 'Yes';
         l_gl_int_contra_rec.REFERENCE8 := l_next_open_period;
         l_gl_int_contra_rec.REFERENCE10 := l_reference10;

         l_gl_int_contra_rec.SEGMENT1 := l_segment1;       --need to calculate
         l_gl_int_contra_rec.SEGMENT2 := l_segment2;       --need to calculate
         l_gl_int_contra_rec.SEGMENT3 := l_segment3;
         l_gl_int_contra_rec.SEGMENT4 := l_segment4;
         l_gl_int_contra_rec.SEGMENT5 := l_segment5;
         l_gl_int_contra_rec.SEGMENT6 := l_segment6;
         l_gl_int_contra_rec.SEGMENT7 := l_segment7;
         l_gl_int_contra_rec.ATTRIBUTE1 := rec.record_id; --unique identifier for contra


         l_loc := 'Now calling insert_into_gl_interface for contra entry';
         write_log (l_loc);
         insert_into_gl_interface (p_gl_int_rec => l_gl_int_contra_rec);

         IF g_error_message IS NOT NULL
         THEN
            RAISE g_exception;
         END IF;
      END LOOP;

      l_loc := 'completed transfer_to_gl_intf_accrual successfully: ';
      write_log (l_loc);
   EXCEPTION
      WHEN g_exception
      THEN
         g_error_message :=
               'Error in transfer_to_gl_intf_accrual: at '
            || l_loc
            || ' - '
            || g_error_message;
      WHEN OTHERS
      THEN
         g_error_message :=
            'Error: ' || g_error_message || 'at ' || l_loc || '-' || SQLERRM;
   END transfer_to_gl_intf_accrual;

   /*************************************************************************
     Procedure : main_accrual

     PURPOSE:   This package populate data for CASS interface from accrual file provided by CASS
                into staging table and then to Gl interface after validating.
                Journal import program will finally import these transaction in oracle.
                gl_je_headers and gl_je_lines.
     PARAMETERS:  IN None
                  OUT errbuf, retcode mandatory for cncurrent program
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
   ************************************************************************/
   PROCEDURE main_accrual (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_location   VARCHAR2 (5000);
   BEGIN
      g_error_message := NULL;
      l_location := ' starting main_accrual';
      write_log (l_location);

      l_location := 'Initialize the Out Parameter';
      write_log (l_location);
      errbuf := NULL;
      retcode := '0';


      l_location := ' File type is Accrual process ACCRUAL';

      l_location := 'calling populate_accrual_staging_table';
      populate_accrual_staging_table ();

      IF g_error_message IS NOT NULL
      THEN
         RAISE g_prog_exception;
      END IF;

      l_location := 'calling transfer_to_gl_intf_accrual';

      transfer_to_gl_intf_accrual ();

      IF g_error_message IS NOT NULL
      THEN
         RAISE g_prog_exception;
      END IF;


      l_location := ' main_accrual END of the Program';

      retcode := '0';
      write_log ('main_accrual Program Successfully completed');
      send_email (g_error_message,
                  'Cass Interface: from file to GL to GL Interface Completed Successfully');
   EXCEPTION
      WHEN g_prog_exception
      THEN
         errbuf := g_error_message;
         retcode := '2';

         write_error (g_error_message);
         write_log (g_error_message);
         send_email (g_error_message, 'Cass Interface: from file to GL to GL Interface Failed.');
      WHEN OTHERS
      THEN
         g_error_message := 'Error Msg :' || SQLERRM;
         errbuf := g_error_message;
         retcode := '2';

         write_error (g_error_message);
         write_log (g_error_message);
         send_email (g_error_message, 'Cass Interface: from file to GL to GL Interface Failed.');
   END main_accrual;

   /*************************************************************************
   Procedure : main_detail

   PURPOSE:   This package populate data for CASS interface from detail file provided by CASS
              into staging table and then to Gl interface after validating.
              Journal import program will finally import these transaction in oracle.
              gl_je_headers and gl_je_lines.
   PARAMETERS: IN None
               OUT errbuf, retcode mandatory for cncurrent program
   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------
    1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016
 ************************************************************************/
   PROCEDURE main_detail (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_location   VARCHAR2 (5000);
   BEGIN
      g_error_message := NULL;
      l_location := ' starting   main_detail : ';
      write_log (l_location);

      l_location := 'Initialize the Out Parameter';
      write_log (l_location);
      errbuf := NULL;
      retcode := '0';


      l_location := 'File type is Detail process DETAIL';
      populate_detail_staging_table ();

      IF g_error_message IS NOT NULL
      THEN
         RAISE g_prog_exception;
      END IF;

      l_location := 'populating GL interface table ';

      transfer_to_gl_intf_detail ();

      IF g_error_message IS NOT NULL
      THEN
         RAISE g_prog_exception;
      END IF;


      l_location := 'main_detail :  END of the Program';

      retcode := '0';
      write_log ('  main_detail : Program Successfully completed');
      send_email (g_error_message,
                  'Cass Interface: from file to GL Interface Completed Successfully');
   EXCEPTION
      WHEN g_prog_exception
      THEN
         errbuf := g_error_message;
         retcode := '2';
         send_email (g_error_message, 'Cass Interface: from file to GL Interface Failed');
         write_error (g_error_message);
         write_log (g_error_message);
      WHEN OTHERS
      THEN
         g_error_message := 'Error Msg :' || SQLERRM;
         errbuf := g_error_message;
         retcode := '2';
         send_email (g_error_message, 'Cass Interface: from file to GL Interface Failed');
         write_error (g_error_message);
         write_log (g_error_message);
   END main_detail;
END XXWC_CASS_INTERFACE_PKG;
/