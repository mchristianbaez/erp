/* Formatted on 5/25/2013 8:50:41 AM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_NPB_DBG_PKG
IS
   /**************************************************************************
    File Name:XXWC_EGO_NPB_DBG_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Captures debug information on the NPB form code
    HISTORY
    -- Description   : Called from the trigger XXWC_ENG_NIR_TRG

    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     07-Aug-2012    Santhosh Louis    Initial creation of the package
   **************************************************************************/
   PROCEDURE DEBUG_PROC (param_name VARCHAR2, param_value VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      IF p_profile_option_value IS NULL
      THEN
         SELECT FND_PROFILE.VALUE ('XXWC_EGO_NPB_DEBUG_PROFILE')
           INTO p_profile_option_value
           FROM DUAL;
      END IF;

      IF p_profile_option_value = 'Y'
      THEN
         INSERT INTO XXWC_EGO_NPB_DEBUG (ID,
                                         PARAMETER_NAME,
                                         PARAMETER_VALUE,
                                         CREATION_DATE)
              VALUES (XXWC_EGO_NPB_DEBUG_S.NEXTVAL,
                      param_name,
                      param_value,
                      CURRENT_TIMESTAMP);

         COMMIT;
      END IF;
   END DEBUG_PROC;                                                -- Procedure
END;
/