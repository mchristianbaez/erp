CREATE OR REPLACE PACKAGE BODY apps.xxwc_qp_modifier_iface_pkg
as
/*************************************************************************
 Copyright (c) 2013 Lucidity Consulting Group
 All rights reserved.
**************************************************************************
  $Header xxwc_qp_modifier_iface_pkg$
  Module Name: xxwc_qp_modifier_iface_pkg.pkb

  PURPOSE:   This package is used for modifier upload interface

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        05/31/2013  Consuelo Gonzalez      Initial Version
  2.0        08/12/2013  Consuelo Gonzalez      TMS 20130809-01306: Changes to make
                                                standalone by piggy backing on the 
                                                pricelist upload form. Updates to 
                                                status and error handling during API Call 
  3.0        10/29/2013  Consuelo Gonzalez      TMS 20131013-00041: Bug fix to inherit active
                                                flag from modifier
  4.0        11/23/2015  Gopi Damuluri          TMS# 20151123-00124
                                                Pricing QP Maintenance Tool changes - Modifier Header upload
  5.0        02/17/2016  Rakesh Patel           TMS 20151217-00023: XXWC QP Modifier List Interface 
                                                program enahancement                                                      
  6.0        10/25/2016  Gopi Damuluri          TMS# 20160922-00037 - Matrix Pricing revert to Standard functionality
  7.0        4/12/2018   Nancy Pahwa            TMS 20180216-00104 - Added the concurrent submit request procedure to 
                                                 call the conversion program after modifier upload process
**************************************************************************/

PROCEDURE   write_log   (p_message in varchar2)
IS
BEGIN
    dbms_output.put_line (p_message);
    fnd_file.put_line(fnd_file.log, p_message);
EXCEPTION
WHEN OTHERS THEN
    null;
END write_log;

PROCEDURE   write_output   (p_message in varchar2)
IS
BEGIN
    dbms_output.put_line (p_message);
    fnd_file.put_line(fnd_file.output, p_message);
EXCEPTION
WHEN OTHERS THEN
    null;
END write_output;

/*************************************************************************
 PROCEDURE Name: xxwc_qp_modifier_iface

 PURPOSE:   To load Modifiers into Oracle.

 REVISIONS:
 Ver        Date        Author            Description
 ---------  ----------  ---------------   -------------------------
 4.0        11/23/2015  Gopi Damuluri     TMS# 20151123-00124
                                          Pricing QP Maintenance Tool changes - Modifier Header upload
 5.0        02/17/2016  Rakesh Patel      TMS 20151217-00023: XXWC QP Modifier List Interface 
                                           program enahancement
 6.0        10/25/2016  Gopi Damuluri     TMS# 20160922-00037 - Matrix Pricing revert to Standard functionality
 7.0        4/12/2018   Nancy Pahwa            TMS 20180216-00104 - Added the concurrent submit request procedure to 
                                                 call the conversion program after modifier upload
***************************************************************************/
procedure   xxwc_qp_modifier_iface (RETCODE         OUT NUMBER
                                   , ERRMSG         OUT VARCHAR2
                                   , P_FILE_NAME    IN VARCHAR2
                                   , P_FILE_ID      IN NUMBER)
IS

    l_error_message2 clob;
    l_start_time    number;  
    l_end_time      number;
    l_conc_req_id   number := fnd_global.conc_request_id;
    l_login_id      number := fnd_global.login_id;
    l_commit_size   number := 1500;
    l_commit_cnt    number;
    
    l_list_header_id            number;
    l_list_line_id              number;
    l_product_attribute_code    varchar2(240);
    l_product_attribute_id      number;
    l_primary_uom_code          varchar2(3);
    l_application_method        varchar2(30);
    l_formula_id                number;
    l_line_value                number;
    l_prod_precedence           number;    
    l_incompatibility           varchar2(80);
    l_status                    varchar2(50);
    l_error_message             varchar2(4000);   
    l_price_by_formula_id       number;
    l_min_start_date            date; --7.0
 
    l_qp_modifiers_rec_type       xxwc_qp_modifier_iface_pkg.XXWC_QP_MODIFIERS_REC_TYPE := xxwc_qp_modifier_iface_pkg.G_MISS_XXWC_QP_MODIFIERS_REC;
    l_qp_modifiers_tbl_type       xxwc_qp_modifier_iface_pkg.XXWC_QP_MODIFIERS_TBL_TYPE := xxwc_qp_modifier_iface_pkg.G_MISS_XXWC_QP_MODIFIERS_TBL;
    
    l_cnt                           NUMBER;
    i                               NUMBER;
    j                               NUMBER;
    k                               NUMBER;     
    l_api_version_number            NUMBER         := 1.0;
    l_init_msg_list                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_return_values                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_commit                        VARCHAR2(10)   := FND_API.G_TRUE;
    l_x_return_status               VARCHAR2(255)  := 'S';
    l_x_msg_count                   NUMBER;
    l_x_msg_data                    VARCHAR2(255);
    g_user_id                       NUMBER;
    l_modifier_rec                QP_MODIFIERS_PUB.Modifier_List_Rec_Type          := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
    l_modifier_val_rec            QP_MODIFIERS_PUB.MODIFIER_LIST_VAL_REC_TYPE      := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_VAL_REC;
    l_modifier_line               QP_MODIFIERS_PUB.MODIFIERS_REC_TYPE    := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_REC;
    l_modifier_line_tbl           QP_MODIFIERS_PUB.MODIFIERS_TBL_TYPE     := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL; 
    l_modifier_line_val_tbl       QP_MODIFIERS_PUB.MODIFIERS_VAL_TBL_TYPE := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_VAL_TBL;
    l_qualifiers_tbl                QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE     := QP_Qualifier_Rules_PUB.G_MISS_QUALIFIERS_TBL;
    l_qualifiers_val_tbl            QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE := QP_Qualifier_Rules_PUB.G_MISS_QUALIFIERS_VAL_TBL;
    l_pricing_att_rec               QP_MODIFIERS_PUB.PRICING_ATTR_REC_TYPE := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_REC;
    l_pricing_attr_tbl              QP_MODIFIERS_PUB.PRICING_ATTR_TBL_TYPE        := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
    l_pricing_attr_val_tbl_type     QP_MODIFIERS_PUB.PRICING_ATTR_VAL_TBL_TYPE    := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_VAL_TBL;
    l_x_modifier_rec              QP_MODIFIERS_PUB.MODIFIER_LIST_REC_TYPE;
    l_x_modifier_val_rec          QP_MODIFIERS_PUB.MODIFIER_LIST_VAL_REC_TYPE;
    l_x_modifier_line_tbl         QP_MODIFIERS_PUB.MODIFIERS_TBL_TYPE;
    l_x_modifier_line_val_tbl     QP_MODIFIERS_PUB.MODIFIERS_VAL_TBL_TYPE;
    l_x_qualifiers_tbl              QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE;
    l_x_qualifiers_val_tbl          QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE;
    l_x_pricing_attr_tbl            QP_MODIFIERS_PUB.PRICING_ATTR_TBL_TYPE;
    l_x_pricing_attr_val_tbl        QP_MODIFIERS_PUB.PRICING_ATTR_VAL_TBL_TYPE;
    
    l_error_message_list            ERROR_HANDLER.ERROR_TBL_TYPE;
    gpr_msg_data                    VARCHAR2(2000);

    l_err_msg                       VARCHAR2(2000);  -- Version# 6.0
    l_ret_code                      NUMBER;          -- Version# 6.0

    cursor cur_load is
            select  MODIFIER_NAME,
                    LIST_LINE_ID,
                    to_date(replace(START_DATE,'''',null),'DD-MON-YYYY') start_date,
                    to_date(replace(END_DATE,'''',null),'DD-MON-YYYY') end_date,
                    PRODUCT_ATTRIBUTE,
                    replace(PRODUCT_VALUE,'''',null) product_value, 
                    APPLICATION_METHOD,
                    LINE_VALUE,
                    INCOMPATIBILITY,
                    PRICE_BREAK_TYPE, 
                    NULL PRODUCT_UOM,
                    PRICE_BREAK_VALUE_FROM,
                    PRICE_BREAK_VALUE_TO,
                    NULL LIST_HEADER_ID,
                    NULL PRODUCT_ATTRIBUTE_CODE,
                    NULL PRODUCT_ATTR_VALUE_ID, 
                    NULL FORMULA_ID, 
                    NULL PROD_PRECEDENCE,
                    NULL STATUS, --'NEW' STATUS, 
                    NULL ERR_MESSAGE,
                    l_conc_req_id REQUEST_ID
            from    xxwc_qp_modifiers_ext_tbl;
    cursor cur_validate is 
            select  x1.rowid rid
                    , x1.*
                    , qlh.attribute10 -- Version# 4.0
            from    XXWC.XXWC_QP_MODIFIERS_STG x1
               , qp_list_headers_all qlh -- Version# 4.0
            where   x1.request_id = l_conc_req_id
            and     qlh.name = x1.modifier_name --qlh.list_header_id = x1.list_header_id -- Version# 4.0
            and     x1.status is null;    

    cursor  cur_valid_header is
            select  distinct(x1.list_header_id)
                    , x1.modifier_name
            from    XXWC.XXWC_QP_MODIFIERS_STG x1
            where   x1.request_id = l_conc_req_id
            and     x1.status != 'ERROR';
            
    cursor  cur_valid_lines (p_list_header_id number) is
            select  x1.rowid rid
                    , x1.*
            from    XXWC.XXWC_QP_MODIFIERS_STG x1
            where   x1.list_header_id = p_list_header_id
            and     x1.request_id = l_conc_req_id
            and     x1.status != 'ERROR';

    cursor cur_errors is
            select  x1.rowid rid
                    , x1.*
            from    XXWC.XXWC_QP_MODIFIERS_STG x1
            where   x1.request_id = l_conc_req_id
            -- 08/12/2013 CG: TMS 20130809-01306: Changed to pull validation errors and API Error Records
            -- and     x1.status = 'ERROR';
            and     x1.status IN ('ERROR','API_ERROR');
            
-- Version# 6.0 > Start
    cursor cur_item_cat_load is
            select /*+ INDEX(XXWC_QP_MODIFIERS_STG_N3) */ DISTINCT x1.modifier_name
            from    XXWC.XXWC_QP_MODIFIERS_STG x1
            where   x1.request_id = l_conc_req_id
            and     x1.product_attribute = 'Item Number'
            and     x1.status = 'LOADED';    
-- Version# 6.0 < End
            
    -- 08/12/2013 CG: TMS 20130809-01306
    l_file_id       number :=  P_FILE_ID;    
    l_file          BLOB;
    l_file_name     VARCHAR2 (256);
    x               INTEGER := 1; -- Row counter
    y               INTEGER := 1; -- Column Counter
    is_file_open    INTEGER;
    l_hdr           INTEGER;
    l_line          INTEGER;

    l_comma_count   NUMBER;
    l_offset        NUMBER := 1;
    l_eol_pos       NUMBER := 32767;
    l_amount        NUMBER;
    l_file_len      NUMBER := DBMS_LOB.getlength (l_file);
    l_buffer        RAW (30000);
    
    -- New File for DB Server
    l_filehandle             UTL_FILE.file_type;
    
    l_msg_data      VARCHAR2 (4000);
    -- 08/12/2013 CG: TMS 20130809-01306

BEGIN

/*TMS: 20140414-00019  Multi Decimal Rounding, Toby Rave, changed from % to NEW PRICE */ 
    BEGIN
        l_price_by_formula_id := NULL;
        SELECT Price_Formula_Id
          INTO l_price_by_formula_id
          FROM QP_PRICE_FORMULAS
         WHERE UPPER(NAME) = 'PERCENT DISCOUNT' AND ROWNUM = 1;
       EXCEPTION
        WHEN OTHERS THEN
        l_price_by_formula_id := NULL;
       END;   
    
    write_log('Starting Pricing Modifier Interface for file: '||P_FILE_NAME);
    write_log('---------------------------------------------------------------');
    
    if P_FILE_NAME is not null then
    
        l_commit_size := 0;
        begin        
            select  /*sysdate
                    , to_char(sysdate, 'HH24MI') time_stamp
                    ,*/ (case when to_number(to_char(sysdate, 'HH24MI')) between 600 and 2200
                            then 1 --'Daytime'
                            else 1500 ---'Evening'
                       end) time_frame
            into    l_commit_size
            from    dual;
        exception
        when others then
            l_commit_size := 1000;
        end;
    
        write_log('Batch Commit Sizes for Update run: '||l_commit_size);
        write_log(' ');
        write_log('******');
        write_log(' ');
    
        -- 08/12/2013 CG: TMS 20130809-01306: Added to make interface standalone. Using Price List maintenance from for standalone processing
        write_log('0. Transferring file from FND Table to DB Server Location...');
        l_Start_time := DBMS_UTILITY.get_time;
        write_log ('l_Start_time: '||l_Start_time);
        IF P_FILE_ID IS NOT NULL THEN
            BEGIN
                SELECT  file_data
                        , file_name
                INTO    l_file
                        , l_file_name
                FROM    fnd_lobs
                WHERE   file_id = l_file_id; 
            EXCEPTION
            WHEN OTHERS THEN
                l_file := null;
                l_file_name := null;
                write_log ('File Not Found. Error: '||SQLERRM);
                l_msg_data := substr(SQLERRM, 1, 4000);
                errmsg := l_msg_data;
                retcode := '2';  
                raise;
            END;

            is_file_open := DBMS_LOB.ISOPEN (l_file);
            IF is_file_open = 1
            THEN
                DBMS_LOB.close (l_file);
            END IF;

            BEGIN
                DBMS_LOB.open (l_file, DBMS_LOB.lob_readonly);
                is_file_open        := DBMS_LOB.ISOPEN (l_file);
            EXCEPTION
            WHEN OTHERS  THEN
                write_log ('Could not open file. Error: '||SQLERRM);
                l_msg_data := substr(SQLERRM, 1, 4000);
                errmsg := l_msg_data;
                retcode := '2';  
                raise;
            END;

            -- New DB Server File
            l_filehandle := UTL_FILE.fopen (    LOCATION       => 'XXWC_QP_MODIFIERS_DIR',
                                                filename       => l_file_name,
                                                open_mode      => 'w',
                                                max_linesize   => 32767
                                            );
            
            l_file_len  := DBMS_LOB.getlength (l_file);
            write_log ('File Name '||l_file_name||' and length ' ||l_file_len||'. Offset: '||l_offset);
            write_log ('Starting File read and parsing from LOB...');
            
            WHILE (l_offset < l_file_len)
            LOOP
                -- Finding EOL position  for new line/line feed ascii 0A
                l_eol_pos      := DBMS_LOB.INSTR (l_file, '0A', 1, x);
                -- write_log ('l_eol_pos: '||l_eol_pos);
                
                IF x = 1 THEN
                    l_amount     := l_eol_pos - 1;
                ELSE
                    l_amount     := l_eol_pos - l_offset;
                END IF;
                
                -- Reading record from LOB file
                DBMS_LOB.read (l_file, l_amount, l_offset, l_buffer);
                
                -- Writing to new file on Server
                UTL_FILE.put_line (l_filehandle, UTL_RAW.cast_to_varchar2 (l_buffer));
                
                l_offset := l_offset + l_amount + 1; -- Increasing offset for read. Starting position of next read
                x        := x + 1;
            END LOOP;
            
            UTL_FILE.fclose (l_filehandle);
            DBMS_LOB.close (l_file);
            
            write_log ('File Row Count: '||x);
            
            begin
                update  XXWC.XXWC_QP_MAINT_TOOL_STG
                set     status = 'Processed'
                        , last_update_date = sysdate
                        , last_updated_by = fnd_global.user_id
                        , last_update_login = l_login_id 
                where   file_id = l_file_id
                and     processing_request_id = l_conc_req_id;
            exception
            when others then
                null;
            end;
            
        END IF;
        write_log('0. File transfer to DB Server Location complete...');
        l_end_time := DBMS_UTILITY.get_time;
        write_log ('l_end_time : '||l_end_time );
        write_log ('File Transfer processing time: '||to_char(l_end_time-l_start_time));
        commit;
        write_log(' ');
        write_log('******');
        write_log(' ');
        -- 08/12/2013 CG: TMS 20130809-01306
        ------------------------------------------------------------------------

        write_log('1. Starting file read and upload to staging...');
        l_Start_time := DBMS_UTILITY.get_time;
        write_log ('l_Start_time: '||l_Start_time);

        execute immediate ( 'alter table xxwc_qp_modifiers_ext_tbl location ('||''''||P_FILE_NAME||''''||')');

        OPEN cur_load;
        LOOP
            FETCH cur_load BULK COLLECT INTO l_qp_modifiers_tbl_type;
            --FETCH cur_load BULK COLLECT INTO l_qp_modifiers_tbl_type LIMIT 1500;

            FORALL n IN 1..l_qp_modifiers_tbl_type.COUNT
            INSERT INTO XXWC.XXWC_QP_MODIFIERS_STG
            VALUES l_qp_modifiers_tbl_type(n);

            EXIT WHEN cur_load%NOTFOUND;
        END LOOP;
        CLOSE cur_load;
       --7.0 
        begin
        select min(h.creation_date)
               into l_min_start_date
        from XXWC.XXWC_QP_MODIFIERS_STG s, apps.qp_list_headers_all h
        where h.NAME = s.modifier_name
            --  and s.list_header_id = h.LIST_HEADER_ID
              and s.request_id = l_conc_req_id;
        end;
       --7.0
        execute immediate ( 'alter table xxwc_qp_modifiers_ext_tbl location ('||''''||'dummy.csv'||''''||')');

        commit;

        l_end_time := DBMS_UTILITY.get_time;
        write_log ('l_end_time : '||l_end_time );
        write_log ('File read/insert processing time: '||to_char(l_end_time-l_start_time));
        
        ------------------------------------------------------------------------
        write_log(' ');
        write_log('******');
        write_log(' ');
        write_log('2. Starting Data Validation...');
        
        l_Start_time := DBMS_UTILITY.get_time;
        write_log ('l_Start_time: '||l_Start_time);
        
        l_commit_cnt := 0;
        for c1 in cur_validate
        loop
            exit when cur_validate%notfound;
        
            l_list_header_id            := null;
            l_list_line_id              := null;
            l_product_attribute_code    := null;
            l_product_attribute_id      := null;
            l_primary_uom_code          := null;
            l_application_method        := null;
            l_prod_precedence           := null;
            l_formula_id                := null;
            l_line_value                := null;
            l_incompatibility           := null;
            l_status                    := null;
            l_error_message             := null;
            
            -- Validating Modifier Name
            begin
                select  list_header_id
                into    l_list_header_id
                from    qp_list_headers
                where   name = c1.modifier_name
                and     list_type_code = 'DLT'
                and     nvl(start_date_active, trunc(sysdate)) <= trunc(sysdate)
                and     nvl(end_date_active, trunc(sysdate+1)) > trunc(sysdate);
            exception
            when no_data_found then
                write_log('Invalid modifier '||c1.modifier_name);
                l_status := 'ERROR';  
                l_error_message := l_error_message||'.Invalid_Modifier';
            when others then
                write_log('Other modifier name validation for: '||c1.modifier_name||'. Error: '||SQLERRM);
                l_status := 'ERROR';  
                l_error_message := l_error_message||'.Other_Modifier_Error';
            end;
            
            --Validating Modifier List Line Id
            if c1.list_line_id is not null then
                begin
                    select  list_line_id 
                    into    l_list_line_id
                    from    qp_list_lines
                    where   list_line_id = c1.list_line_id
                    and     list_header_id = l_list_header_id
                    and     list_line_type_code = 'DIS';
                    
                    if nvl(l_status, 'Unk') != 'ERROR' then
                        l_status := 'UPDATE';
                    end if;
                exception
                when no_data_found then
                    write_log('Invalid list_line_id '||c1.modifier_name||' - '||c1.list_line_id);
                    l_status := 'ERROR';  
                    l_error_message := l_error_message||'.Invalid_List_line'; 
                when others then
                    write_log('Other list_line_id validation for: '||c1.modifier_name||' - '||c1.list_line_id||'. Error: '||SQLERRM);
                    l_status := 'ERROR';  
                    l_error_message := l_error_message||'.Other_Line_Line_Error';
                end;            
            else
                -- New modifier line being added, only if modifier validated ok
                if nvl(l_status, 'Unk') != 'ERROR' then
                    l_status := 'CREATE';
                end if;            
            end if;
            
            -- Validating Product Attribute
            if upper(c1.product_attribute) = 'ITEM NUMBER' then            
                l_product_attribute_code := 'PRICING_ATTRIBUTE1'; 
                l_prod_precedence        := 220;               
            elsif upper(c1.product_attribute) = 'ITEM CATEGORY' then
                l_product_attribute_code := 'PRICING_ATTRIBUTE2';
                l_prod_precedence        := 290;
            elsif upper(c1.product_attribute) = 'PRICING CATEGORY' then
                l_product_attribute_code := 'PRICING_ATTRIBUTE27';
                l_prod_precedence        := 250;
            else
                write_log('Invalid Product Attribute '||c1.modifier_name||' - '||c1.list_line_id||' - '||c1.product_attribute);
                l_status := 'ERROR';  
                l_error_message := l_error_message||'.Invalid_Product_Attribute';
            end if;
            
            -- Validating Product Attr Value
            if upper(c1.product_attribute) = 'ITEM NUMBER' then
            
                begin
                    select  msib.inventory_item_id
                            , msib.primary_uom_code
                    into    l_product_attribute_id 
                            , l_primary_uom_code
                    from    mtl_system_items_b msib
                            , mtl_parameters mp
                    where   msib.segment1 = c1.product_value
                    and     msib.organization_id = mp.organization_id
                    and     mp.organization_code = 'MST';
                exception
                when no_data_found then
                    l_product_attribute_id      := null;
                    l_primary_uom_code          := null;
                    write_log('Invalid Product Attr Value'||c1.modifier_name||' - '||c1.list_line_id||' - '||c1.product_value);
                    l_status := 'ERROR';  
                    l_error_message := l_error_message||'.Invalid_Product_Attr_Value';
                when others then
                    l_product_attribute_id      := null;
                    l_primary_uom_code          := null;
                    write_log('Other Product Attr Value error'||c1.modifier_name||' - '||c1.list_line_id||' - '||c1.product_value||'. Error: '||SQLERRM);
                    l_status := 'ERROR';  
                    l_error_message := l_error_message||'.Other_Product_Attr_Value_Error';
                end;    
            
            elsif upper(c1.product_attribute) in ('ITEM CATEGORY', 'PRICING CATEGORY') then
                begin
                    select  to_char(category_id)
                    into    l_product_attribute_id 
                    from    mtl_categories_v
                    where   category_concat_segs = c1.product_value
                    and     enabled_flag = 'Y'
                    and     structure_name in ('Pricing Category','Item Categories')
                    and     rownum = 1;
                    
                    l_primary_uom_code := 'EA'; --nvl(c1.product_uom, 'EA');
                exception
                when no_data_found then
                    l_product_attribute_id      := null;
                    l_primary_uom_code          := null;
                    write_log('Invalid Product Attr Value'||c1.modifier_name||' - '||c1.list_line_id||' - '||c1.product_value);
                    l_status := 'ERROR';  
                    l_error_message := l_error_message||'.Invalid_Product_Attr_Value';
                when others then
                    l_product_attribute_id      := null;
                    l_primary_uom_code          := null;
                    write_log('Other Product Attr Value error'||c1.modifier_name||' - '||c1.list_line_id||' - '||c1.product_value||'. Error: '||SQLERRM);
                    l_status := 'ERROR';  
                    l_error_message := l_error_message||'.Other_Product_Attr_Value_Error';
                end;    
            else
                null; -- Already cover invalid product attribute, so not checking attribute value
            end if;
        
            -- Validating Application Method and line value
            if UPPER(c1.application_method) in ('NEW PRICE', 'AMOUNT', 'PERCENT','COST PLUS') then
                select  (case when UPPER(c1.application_method) = 'NEW PRICE'
                                then 'NEWPRICE'
                              when UPPER(c1.application_method) = 'AMOUNT'
                                then 'AMT'
                              when UPPER(c1.application_method) = 'PERCENT'
                                then 'NEWPRICE' /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, changed from % to NEW PRICE */
                              when UPPER(c1.application_method) = 'COST PLUS'
                                then 'NEWPRICE'
                              else  null
                         end)
                into    l_application_method
                from    dual;
               
                if UPPER(c1.application_method) = 'COST PLUS' then
                    l_formula_id := 7030;
                    l_line_value := c1.line_value/100;
                /*TMS: 20140414-00019  Multi Decimal Rounding, Toby Rave, changed from % to NEW PRICE */     
                elsif UPPER(c1.application_method) = 'PERCENT' then
                l_formula_id := l_price_by_formula_id ; 
                l_line_value := ((100 - c1.line_value) / 100) ;
                else
                    l_formula_id := null;
                    l_line_value := c1.line_value;
                end if;
                
                              
            else
                write_log('Invalid Application Method'||c1.modifier_name||' - '||c1.list_line_id||' - '||c1.application_method);
                l_status := 'ERROR';  
                l_error_message := l_error_message||'.Invalid_Application_Method';
            end if;
            
            -- Validating Price Break Type
            if c1.price_break_type is not null and upper(c1.price_break_type) = 'POINT' then
            
                -- Validating Price Break Range
                if c1.price_break_value_from >= c1.price_break_value_to then
                    write_log('Invalid Price Break Range'||c1.modifier_name||' - '||c1.list_line_id);
                    l_status := 'ERROR';  
                    l_error_message := l_error_message||'.Invalid_PB_Range';  
                else
                    for c2 in (select  x2.list_line_id
                                        , x2.price_break_value_from
                                        , x2.price_break_value_to
                                from    XXWC.XXWC_QP_MODIFIERS_STG x2
                                where   x2.request_id = l_conc_req_id
                                and     x2.list_line_id != c1.list_line_id
                                and     upper(x2.modifier_name) = upper(c1.modifier_name)
                                and     upper(x2.price_break_type) = upper(c1.price_break_type)
                                and     upper(x2.product_attribute) = upper(c1.product_attribute)
                                and     upper(x2.product_value) = upper(c1.product_value)
                                order by x2.price_break_value_from)
                    loop
                        
                        if (
                            (c1.price_break_value_from between c2.price_break_value_from and c2.price_break_value_to)
                            OR
                            (c1.price_break_value_to between c2.price_break_value_from and c2.price_break_value_to)
                           )
                        then
                            write_log('Overlap Price Break Range'||c1.modifier_name||' - '||c1.list_line_id||'. Comparing to list_line_id '||c2.list_line_id);
                            l_status := 'ERROR';  
                            l_error_message := l_error_message||'.Overlap_PB_Range';
                        end if;
                    
                    end loop;
                end if;
                
            elsif c1.price_break_type is not null and upper(c1.price_break_type) != 'POINT' then
                write_log('Invalid Price Break '||c1.modifier_name||' - '||c1.list_line_id||' - '||c1.price_break_type);
                l_status := 'ERROR';  
                l_error_message := l_error_message||'.Invalid_Price_Break';
            else
                null; -- No op, it's not a price break
            end if;
            
            -- Validating Incompatibility
            IF c1.attribute10 = 'Segmented Price' THEN -- Version# 4.0
               -- l_incompatibility := 'EXCL'; -- Version# 4.0
               l_incompatibility := 'LVL 1'; -- Version# 6.0
            ELSE -- Version# 4.0
            if upper(c1.incompatibility) = 'BEST PRICE WINS' then
                l_incompatibility := 'LVL 1';
            elsif upper(c1.incompatibility) = 'EXCLUSIVE' then
                l_incompatibility := 'EXCL';
            elsif c1.incompatibility is null and l_status = 'CREATE' then
                l_incompatibility := 'LVL 1';
            else
                begin
                    select  incompatibility_grp_code
                    into    l_incompatibility
                    from    qp_list_lines
                    where   list_line_id = c1.list_line_id;
                exception
                when others then
                    write_log('Invalid Incompatibility '||c1.modifier_name||' - '||c1.list_line_id||' - '||c1.incompatibility);
                    l_incompatibility := null;
                    l_status := 'ERROR';  
                    l_error_message := l_error_message||'.Invalid_Incompatibility';
                end; 
            end if;
            END IF; -- Version# 4.0
            
            -- Update Staging table
            begin
                update  XXWC.XXWC_QP_MODIFIERS_STG
                set     application_method          = l_application_method
                        , line_value                = l_line_value
                        , product_uom               = NULL --l_primary_uom_code -- Version# 4.0
                        , list_header_id            = l_list_header_id
                        , product_attribute_code    = l_product_attribute_code
                        , product_attr_value_id     = l_product_attribute_id
                        , formula_id                = l_formula_id
                        , prod_precedence           = l_prod_precedence
                        , incompatibility           = nvl(l_incompatibility, c1.incompatibility)
                        , status                    = l_status
                        , err_message               = substr(l_error_message, 1, 4000)
                where   rowid = c1.rid;
                
                l_commit_cnt := l_commit_cnt + 1;
            exception
            when others then
                write_log('Could not update staging for '||c1.modifier_name||' - '||c1.list_line_id||'. Error: '||SQLERRM); 
            end;
        
            if l_commit_cnt >= l_commit_size then
                commit;
                l_commit_cnt := 0;
            end if;

        end loop;
        
        commit;
        
        l_end_time := DBMS_UTILITY.get_time;
        write_log ('l_end_time : '||l_end_time );
        write_log ('Validation time: '||to_char(l_end_time-l_start_time));
        
        ------------------------------------------------------------------------
        write_log(' ');
        write_log('******');
        write_log(' ');
        write_log('3. Starting Update and API Calls...');

        
        -- 06/05/2013 CG: Removed from Code Review
        /*mo_global.set_policy_context ('S', fnd_global.org_id);
        --mo_global.init ('ONT');
        fnd_global.apps_initialize(user_id        => fnd_global.user_id,
                                   resp_id        => fnd_global.resp_id,
                                   resp_appl_id   => fnd_global.resp_appl_id);*/
        
        g_user_id := fnd_global.user_id;
        l_Start_time := DBMS_UTILITY.get_time;
        write_log ('l_Start_time: '||l_Start_time);
        
        l_commit_cnt := 0;
        for c2 in cur_valid_header
        loop
            exit when cur_valid_header%notfound;
            
            --write_log ('Inside Header Loop '||c2.list_header_id);
            
            l_modifier_rec := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
            
            l_modifier_rec.list_header_id     := c2.list_header_id;
            begin
                select  nvl(description,FND_API.G_MISS_CHAR)
                        -- 10/29/2013 CG: TMS 20131013-00041: Updated to inherit original active flag
                        , nvl(active_flag,FND_API.G_MISS_CHAR)
                into    l_modifier_rec.description
                        -- 10/29/2013 CG: TMS 20131013-00041: Updated to inherit original active flag
                        , l_modifier_rec.active_flag
                from    qp_list_headers
                where   list_header_id = c2.list_header_id
                and     rownum = 1;
            exception
            when others then
                l_modifier_rec.description := FND_API.G_MISS_CHAR;
                -- 10/29/2013 CG: TMS 20131013-00041: Updated to inherit original active flag
                l_modifier_rec.active_flag := 'Y';
            end;
            
            l_modifier_rec.list_type_code     := 'DLT';
            l_modifier_rec.currency_code      := 'USD';
            l_modifier_rec.rounding_factor    := -5;
            -- 10/29/2013 CG: TMS 20131013-00041: Updated to inherit original active flag
            -- l_modifier_rec.active_flag        := 'Y';
            l_modifier_rec.source_system_code := 'QP';
            l_modifier_rec.pte_code           := 'ORDFUL';
            l_modifier_rec.description        := FND_API.G_MISS_CHAR;
            l_modifier_rec.last_updated_by    := g_user_id;
            l_modifier_rec.last_update_date   := SYSDATE; 
            l_modifier_rec.operation          := QP_GLOBALS.G_OPR_UPDATE;
        
            i := 1;
            l_modifier_line_tbl      := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
            l_pricing_attr_tbl       := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
            for c3 in cur_valid_lines (c2.list_header_id)
            loop
                exit when cur_valid_lines%notfound;
                
                --write_log ('Inside line Loop '||c3.list_line_id);
                
                l_modifier_line_tbl(i).list_header_id               := l_modifier_rec.list_header_id;
                l_modifier_line_tbl(i).operand                      := trim(c3.line_value);
                l_modifier_line_tbl(i).arithmetic_operator          := trim(c3.application_method);
                l_modifier_line_tbl(i).price_by_formula_id          := trim(c3.formula_id);
                l_modifier_line_tbl(i).product_precedence           := trim(c3.prod_precedence);
                l_modifier_line_tbl(i).incompatibility_grp_code     := trim(c3.incompatibility);
                l_modifier_line_tbl(i).start_date_active            := c3.start_date;
                l_modifier_line_tbl(i).end_date_active              := c3.end_date;
                
                    
                l_pricing_attr_tbl(i).product_attr_value                := trim(to_char(c3.product_attr_value_id));
                l_pricing_attr_tbl(i).product_uom_code                  := trim(c3.product_uom);
                l_pricing_attr_tbl(i).product_attribute_context         := 'ITEM';
                l_pricing_attr_tbl(i).product_attribute                 := trim(c3.product_attribute_code);
                    
                if upper(c3.price_break_type) = 'POINT' then
                    l_modifier_line_tbl(i).price_break_type_code        := trim(upper(c3.price_break_type));
                    
                    l_pricing_attr_tbl(i).pricing_attribute_context     := 'VOLUME';
                    l_pricing_attr_tbl(i).pricing_attribute             := 'PRICING_ATTRIBUTE10';
                    l_pricing_attr_tbl(i).comparison_operator_code      := 'BETWEEN';
                    l_pricing_attr_tbl(i).pricing_attr_value_from       := trim(c3.price_break_value_from);
                    l_pricing_attr_tbl(i).pricing_attr_value_to         := trim(c3.price_break_value_to);
                end if;
                
                l_modifier_line_tbl(i).pricing_phase_id            := 2;
                l_modifier_line_tbl(i).list_line_type_code         := 'DIS';
                l_modifier_line_tbl(i).automatic_flag              := 'Y';
                l_modifier_line_tbl(i).pricing_group_sequence      := 1;
                l_modifier_line_tbl(i).modifier_level_code         := 'LINE';
                l_modifier_line_tbl(i).accrual_flag                := 'N';
                l_modifier_line_tbl(i).include_on_returns_flag     := 'Y' ;
                l_modifier_line_tbl(i).context                     := '162';
                l_modifier_line_tbl(i).last_updated_by             := g_user_id;
                l_modifier_line_tbl(i).last_update_date            := sysdate;
                l_modifier_line_tbl(i).last_update_login           := l_login_id;
                                                
                l_pricing_attr_tbl(i).list_header_id            := l_modifier_rec.list_header_id;
                l_pricing_attr_tbl(i).pricing_phase_id          := 2;
                l_pricing_attr_tbl(i).last_updated_by           := g_user_id;
                l_pricing_attr_tbl(i).last_update_date          := sysdate;
                l_pricing_attr_tbl(i).last_update_login         := l_login_id;
                l_pricing_attr_tbl(i).modifiers_index           := i;
                
                if c3.status = 'UPDATE' then
                    
                    select  qll.list_line_id
                            , qpa.list_line_id
                            , qpa.pricing_attribute_id 
                    into    l_modifier_line_tbl(i).list_line_id
                            , l_pricing_attr_tbl(i).list_line_id
                            , l_pricing_attr_tbl(i).pricing_attribute_id
                    from    qp_list_lines qll
                            , qp_pricing_attributes qpa
                    where   qll.list_header_id = l_modifier_rec.list_header_id
                    and     qll.list_line_id = c3.list_line_id
                    and     qll.list_header_id = qpa.list_header_id
                    and     qll.qualification_ind = qpa.qualification_ind
                    and     qll.pricing_phase_id = qpa.pricing_phase_id
                    and     qll.list_line_id = qpa.list_line_id
                    and     NVL(qpa.excluder_flag, 'N') = 'N' ; -- Version# 5.0
                   
                    --write_log ('Looking for QPA records');
                
                    l_modifier_line_tbl(i).operation                 := QP_GLOBALS.G_OPR_UPDATE;
                    l_pricing_attr_tbl(i).operation                 := QP_GLOBALS.G_OPR_UPDATE;
                
                elsif c3.status = 'CREATE' then
                
                    l_modifier_line_tbl(i).created_by                := g_user_id;
                    l_modifier_line_tbl(i).creation_date             := sysdate;
                    l_modifier_line_tbl(i).start_date_active         := trunc(sysdate);
                    
                    l_pricing_attr_tbl(i).created_by                := g_user_id;
                    l_pricing_attr_tbl(i).creation_date             := sysdate;                
                    l_pricing_attr_tbl(i).pricing_phase_id          := 1;
                    
                    l_pricing_attr_tbl(i).list_line_id              := FND_API.G_MISS_NUM;
                
                    SELECT qp_pricing_attributes_s.nextval
                    INTO   l_pricing_attr_tbl(i).pricing_attribute_id
                    FROM   dual;
                    
                    l_modifier_line_tbl(i).operation                 := QP_GLOBALS.G_OPR_CREATE;
                    l_pricing_attr_tbl(i).operation                 := QP_GLOBALS.G_OPR_CREATE;
                else
                    null;
                end if;
                
                if c3.status in ('CREATE','UPDATE') then 
                    --l_pricing_attr_tbl(i)       := l_pricing_att_rec;
                    --l_modifier_line_tbl(i)      := l_modifier_line;
                    i := i + 1;
                    
                    -- 08/12/2013 CG: TMS 20130809-01306: Added to handle statuses by batches
                    begin
                        update  XXWC.XXWC_QP_MODIFIERS_STG
                        set     status = 'IN_PROC'
                        where   rowid = c3.rid
                        and     list_header_id = c2.list_header_id
                        and     request_id = l_conc_req_id;
                    exception
                    when others then
                        null;
                    end;
                    -- 08/12/2013 CG: TMS 20130809-01306: Added to handle statuses by batches
                    
                else
                    write_log('Invalid status, not loading to API. Modifier/Line '||c3.modifier_name||' - '||c3.list_line_id||'. Status: '||c3.status);
                end if;
                
                IF(i >= l_commit_size) THEN
                    j := i - 1;
                    write_log ( 'Adding ' || j || ' price lines to modifier');
                
                    qp_modifiers_pub.process_modifiers  ( p_api_version_number      => l_api_version_number,
                                                          p_init_msg_list           => l_init_msg_list,
                                                          p_return_values           => l_return_values,
                                                          p_commit                  => l_commit,
                                                          x_return_status           => l_x_return_status,
                                                          x_msg_count               => l_x_msg_count,
                                                          x_msg_data                => l_x_msg_data,
                                                          p_MODIFIER_LIST_rec       => l_modifier_rec,
                                                          p_MODIFIER_LIST_val_rec   => l_modifier_val_rec,
                                                          p_MODIFIERS_tbl           => l_modifier_line_tbl,
                                                          p_MODIFIERS_val_tbl       => l_modifier_line_val_tbl,
                                                          p_QUALIFIERS_tbl          => l_qualifiers_tbl,
                                                          p_QUALIFIERS_val_tbl      => l_qualifiers_val_tbl,
                                                          p_PRICING_ATTR_tbl        => l_pricing_attr_tbl,
                                                          p_PRICING_ATTR_val_tbl    => l_pricing_attr_val_tbl_type,
                                                          x_MODIFIER_LIST_rec       => l_x_modifier_rec,
                                                          x_MODIFIER_LIST_val_rec   => l_x_modifier_val_rec,
                                                          x_MODIFIERS_tbl           => l_x_modifier_line_tbl,
                                                          x_MODIFIERS_val_tbl       => l_x_modifier_line_val_tbl,
                                                          x_QUALIFIERS_tbl          => l_x_qualifiers_tbl, 
                                                          x_QUALIFIERS_val_tbl      => l_x_qualifiers_val_tbl,
                                                          x_PRICING_ATTR_tbl        => l_x_pricing_attr_tbl,
                                                          x_PRICING_ATTR_val_tbl    => l_x_pricing_attr_val_tbl);
                        
                    write_log ('Return status = ' || l_x_return_status);
                      
                    IF(l_x_return_status <> 'S') THEN
                        --  Error Processing
                        write_log ('  Message Count       = ' || l_x_msg_count);
                      
                        FOR K IN 1 .. l_x_msg_count LOOP 
                          gpr_msg_data := oe_msg_pub.get(p_msg_index => k, 
                                                         p_encoded => 'F'); 
                          write_log (TO_CHAR(i)||' ERR TEXT  '|| gpr_msg_data);        
                        END LOOP;
                        
                        -- 08/12/2013 CG: TMS 20130809-01306: Added to handle statuses by batches and not stop entire processing if one batch fails
                        -- RETURN;
                        begin
                            update  XXWC.XXWC_QP_MODIFIERS_STG
                            set     status = 'API_ERROR'
                            where   list_header_id = c2.list_header_id
                            and     request_id = l_conc_req_id
                            and     status = 'IN_PROC';
                        exception
                        when others then
                            null;
                        end;
                        -- 08/12/2013 CG: TMS 20130809-01306: Added to handle statuses by batches
                        
                    ELSE
                        -- 08/12/2013 CG: TMS 20130809-01306: Added to handle statuses by batches and not stop entire processing if one batch fails
                        begin
                            update  XXWC.XXWC_QP_MODIFIERS_STG
                            set     status = 'LOADED'
                            where   list_header_id = c2.list_header_id
                            and     request_id = l_conc_req_id
                            and     status = 'IN_PROC';
                        exception
                        when others then
                            null;
                        end;
                        -- 08/12/2013 CG: TMS 20130809-01306: Added to handle statuses by batches
                        
                        -- null;
                        /*Write_log ('Loaded');
                        Write_log ('************************');
                        Write_log ('List Line Table Type:');
                        Write_log (' ');
                        IF l_x_price_list_line_tbl.count > 0 THEN
                            FOR k in 1 .. l_x_price_list_line_tbl.count LOOP
                                Write_log ('Record = '|| k || 'Return Status = '|| l_x_price_list_line_tbl(k).return_status);
                                Write_log ('Record = '|| k || 'list_header_id = '|| l_x_price_list_line_tbl(k).list_header_id);
                                Write_log ('Record = '|| k || 'list_line_id = '|| l_x_price_list_line_tbl(k).list_line_id);
                                Write_log (' ');
                            END LOOP;
                        END IF;
                        
                        Write_log ('************************');
                        Write_log ('Pricing Attribute Table Type:');
                        Write_log (' ');
                        IF l_x_pricing_attr_tbl.count > 0 THEN
                            FOR k in 1 .. l_x_pricing_attr_tbl.count LOOP
                                Write_log ('Record = '|| k || 'Return Status = '|| l_x_pricing_attr_tbl(k).return_status);
                                Write_log ('Record = '|| k || 'list_header_id = '|| l_x_pricing_attr_tbl(k).list_header_id);
                                Write_log ('Record = '|| k || 'list_line_id = '|| l_x_pricing_attr_tbl(k).list_line_id);
                                Write_log ('Record = '|| k || 'product_attribute = '|| l_x_pricing_attr_tbl(k).product_attribute);
                                Write_log ('Record = '|| k || 'product_attr_value = '|| l_x_pricing_attr_tbl(k).product_attr_value);
                                Write_log (' ');                        
                            end loop;
                        END IF;*/                                    
                        
                    END IF;
                      
                    l_modifier_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
                    COMMIT; 
                        
                    i := 1;
                    l_pricing_attr_tbl       := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
                    l_modifier_line_tbl    := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
                END IF;
                
            end loop;
        
            IF(i NOT IN (0, 1)) THEN
                j := i - 1;
                write_log ('Adding ' || j || ' price lines to modifier');
            
                qp_modifiers_pub.process_modifiers  ( p_api_version_number      => l_api_version_number,
                                                      p_init_msg_list           => l_init_msg_list,
                                                      p_return_values           => l_return_values,
                                                      p_commit                  => l_commit,
                                                      x_return_status           => l_x_return_status,
                                                      x_msg_count               => l_x_msg_count,
                                                      x_msg_data                => l_x_msg_data,
                                                      p_MODIFIER_LIST_rec       => l_modifier_rec,
                                                      p_MODIFIER_LIST_val_rec   => l_modifier_val_rec,
                                                      p_MODIFIERS_tbl           => l_modifier_line_tbl,
                                                      p_MODIFIERS_val_tbl       => l_modifier_line_val_tbl,
                                                      p_QUALIFIERS_tbl          => l_qualifiers_tbl,
                                                      p_QUALIFIERS_val_tbl      => l_qualifiers_val_tbl,
                                                      p_PRICING_ATTR_tbl        => l_pricing_attr_tbl,
                                                      p_PRICING_ATTR_val_tbl    => l_pricing_attr_val_tbl_type,
                                                      x_MODIFIER_LIST_rec       => l_x_modifier_rec,
                                                      x_MODIFIER_LIST_val_rec   => l_x_modifier_val_rec,
                                                      x_MODIFIERS_tbl           => l_x_modifier_line_tbl,
                                                      x_MODIFIERS_val_tbl       => l_x_modifier_line_val_tbl,
                                                      x_QUALIFIERS_tbl          => l_x_qualifiers_tbl, 
                                                      x_QUALIFIERS_val_tbl      => l_x_qualifiers_val_tbl,
                                                      x_PRICING_ATTR_tbl        => l_x_pricing_attr_tbl,
                                                      x_PRICING_ATTR_val_tbl    => l_x_pricing_attr_val_tbl);
                  
                write_log ('Return status = ' || l_x_return_status);
                
                IF(l_x_return_status <> 'S') THEN
                    --  Error Processing
                    write_log ('  Message Count       = ' || l_x_msg_count);
              
                    FOR K IN 1 .. l_x_msg_count 
                    LOOP 
                        gpr_msg_data := oe_msg_pub.get(p_msg_index => k, p_encoded => 'F'); 
                        write_log (TO_CHAR(i)||' ERR TEXT  '|| gpr_msg_data);        
                    END LOOP;
                    
                    -- 08/12/2013 CG: TMS 20130809-01306: Added to handle statuses by batches and not stop entire processing if one batch fails
                    -- RETURN;
                    begin
                        update  XXWC.XXWC_QP_MODIFIERS_STG
                        set     status = 'API_ERROR'
                        where   list_header_id = c2.list_header_id
                        and     request_id = l_conc_req_id
                        and     status = 'IN_PROC';
                    exception
                    when others then
                        null;
                    end;
                    -- 08/12/2013 CG: TMS 20130809-01306: Added to handle statuses by batches
                
                ELSE
                    -- 08/12/2013 CG: TMS 20130809-01306: Added to handle statuses by batches and not stop entire processing if one batch fails
                    begin
                        update  XXWC.XXWC_QP_MODIFIERS_STG
                        set     status = 'LOADED'
                        where   list_header_id = c2.list_header_id
                        and     request_id = l_conc_req_id
                        and     status = 'IN_PROC';
                    exception
                    when others then
                        null;
                    end;
                    -- 08/12/2013 CG: TMS 20130809-01306: Added to handle statuses by batches
                    
                    -- null;                       
                    /*Write_log ('Loaded');
                    Write_log ('************************');
                    Write_log ('List Line Table Type:');
                    Write_log (' ');
                    IF l_x_price_list_line_tbl.count > 0 THEN
                        FOR k in 1 .. l_x_price_list_line_tbl.count LOOP
                            Write_log ('Record = '|| k || 'Return Status = '|| l_x_price_list_line_tbl(k).return_status);
                            Write_log ('Record = '|| k || 'list_header_id = '|| l_x_price_list_line_tbl(k).list_header_id);
                            Write_log ('Record = '|| k || 'list_line_id = '|| l_x_price_list_line_tbl(k).list_line_id);
                            Write_log (' ');
                        END LOOP;
                    END IF;
                        
                    Write_log ('************************');
                    Write_log ('Pricing Attribute Table Type:');
                    Write_log (' ');
                    IF l_x_pricing_attr_tbl.count > 0 THEN
                        FOR k in 1 .. l_x_pricing_attr_tbl.count LOOP
                            Write_log ('Record = '|| k || 'Return Status = '|| l_x_pricing_attr_tbl(k).return_status);
                            Write_log ('Record = '|| k || 'list_header_id = '|| l_x_pricing_attr_tbl(k).list_header_id);
                            Write_log ('Record = '|| k || 'list_line_id = '|| l_x_pricing_attr_tbl(k).list_line_id);
                            Write_log ('Record = '|| k || 'product_attribute = '|| l_x_pricing_attr_tbl(k).product_attribute);
                            Write_log ('Record = '|| k || 'product_attr_value = '|| l_x_pricing_attr_tbl(k).product_attr_value);
                            Write_log ( ' ');                        
                        end loop;
                    END IF;*/
                END IF;
                
                l_modifier_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
                
                COMMIT;

            END IF;
            
            -- 08/12/2013 CG: TMS 20130809-01306: Handled status update after API Calls
            /*IF (l_x_return_status = 'S') THEN
                -- updating loading pricelist lines
                update  XXWC.XXWC_QP_MODIFIERS_STG
                set     status = 'LOADED'
                where   list_header_id = c2.list_header_id
                and     request_id = l_conc_req_id
                and     status != 'ERROR';
                
            END IF;*/
            -- 08/12/2013 CG: TMS 20130809-01306
        
        end loop;
        
        commit;
        l_end_time := DBMS_UTILITY.get_time;
        write_log ('l_end_time : '||l_end_time );
        write_log ('QP_Modifiers_Pub processing time: '||to_char(l_end_time-l_start_time));
        
        ------------------------------------------------------------------------
        write_log(' ');
        write_log('******');
        write_log(' ');
        write_log('4. Generating error report in output...');
        
        l_Start_time := DBMS_UTILITY.get_time;
        write_log ('l_Start_time: '||l_Start_time);
        
        write_output ('<?xml version="1.0"?>');
        write_output ('<XXWC_QP_MODIFIER_IFACE_ERROR_RPT>');
        write_output ('<P_FILE_NAME>'||P_FILE_NAME||'</P_FILE_NAME>');
        write_output ('<LIST_G_MODIFIER_LINES>');
        
        l_commit_cnt := 0;
        for c4 in cur_errors
        loop
            exit when cur_errors%notfound;
            
            write_output ('<MODIFIER_LINES>');
            
            write_output ('<MODIFIER_NAME>'|| c4.MODIFIER_NAME ||'</MODIFIER_NAME>');
            write_output ('<LIST_LINE_ID>'|| c4.LIST_LINE_ID ||'</LIST_LINE_ID>');
            write_output ('<START_DATE>'|| c4.START_DATE ||'</START_DATE>');
            write_output ('<END_DATE>'|| c4.END_DATE ||'</END_DATE>');
            write_output ('<PRODUCT_ATTRIBUTE>'|| c4.PRODUCT_ATTRIBUTE ||'</PRODUCT_ATTRIBUTE>');
            write_output ('<PRODUCT_VALUE>'|| c4.PRODUCT_VALUE ||'</PRODUCT_VALUE>');
            write_output ('<APPLICATION_METHOD>'|| c4.APPLICATION_METHOD ||'</APPLICATION_METHOD>');
            write_output ('<LINE_VALUE>'|| c4.LINE_VALUE ||'</LINE_VALUE>');
            write_output ('<INCOMPATIBILITY>'|| c4.INCOMPATIBILITY ||'</INCOMPATIBILITY>');
            write_output ('<PRICE_BREAK_TYPE>'|| c4.PRICE_BREAK_TYPE ||'</PRICE_BREAK_TYPE>');
            write_output ('<PRODUCT_UOM>'|| c4.PRODUCT_UOM  ||'</PRODUCT_UOM>');
            write_output ('<PRICE_BREAK_VALUE_FROM>'|| c4.PRICE_BREAK_VALUE_FROM ||'</PRICE_BREAK_VALUE_FROM>');
            write_output ('<PRICE_BREAK_VALUE_TO>'|| c4.PRICE_BREAK_VALUE_TO ||'</PRICE_BREAK_VALUE_TO>');
            write_output ('<STATUS>'|| c4.STATUS ||'</STATUS>');
            write_output ('<ERR_MESSAGE>'|| c4.ERR_MESSAGE ||'</ERR_MESSAGE>');
            
            write_output ('</MODIFIER_LINES>');
            
            l_commit_cnt := l_commit_cnt + 1;
        end loop;
        
        write_output ('</LIST_G_MODIFIER_LINES>');
        write_output ('<COUNT>'||l_commit_cnt||'</COUNT>');
        write_output ('</XXWC_QP_MODIFIER_IFACE_ERROR_RPT>');
        
        l_end_time := DBMS_UTILITY.get_time;
        write_log ('l_end_time : '||l_end_time );
        write_log ('Output Report Generation time: '||to_char(l_end_time-l_start_time));

-- Version# 6.0 > Start
FOR c5 IN cur_item_cat_load LOOP
    fnd_file.put_line (fnd_file.LOG, ' #### > BEGIN ##### ');
    xxwc_pricing_segment_dcom_pkg.mtrx_item_cat_excl(l_err_msg
               , l_ret_code
               , c5.modifier_name);
    fnd_file.put_line (fnd_file.LOG, ' #### < END ##### ');
END LOOP;            
-- Version# 6.0 < End
    
    else
        write_log('File name cant be null...');
        ERRMSG := ('Error: Null file name for upload');
        RETCODE := '2'; 
    end if;
    
    commit; 
 submit_request(l_min_start_date); --7.0
EXCEPTION
WHEN OTHERS THEN
    UTL_FILE.fclose (l_filehandle);
    DBMS_LOB.close (l_file);

    l_error_message2 := 'xxwc_qp_modifier_iface '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_qp_modifier_iface_pkg.xxwc_qp_modifier_iface'
       ,p_calling             => 'xxwc_qp_modifier_iface_pkg.xxwc_qp_modifier_iface'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running Pricing Modifier Interface'
       ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
       ,p_module              => 'QP');

    write_log('Error in main xxwc_qp_modifier_iface. Error: '||l_error_message2);
    
    ERRMSG := l_error_message2;
    RETCODE := '2';
END xxwc_qp_modifier_iface;
 PROCEDURE submit_request(l_start_date in date) --7.0
   /*************************************************************************
 PROCEDURE Name: submit_request

 PURPOSE:   To submit the conversion program automatically after the modifier program.

 REVISIONS:
 Ver        Date        Author            Description
 ---------  ----------  ---------------   -------------------------
 7.0        4/12/2018   Nancy Pahwa            TMS 20180216-00104 - Added the concurrent submit request procedure to 
                                                 call the conversion program after modifier upload
***************************************************************************/
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      ln_request_id   NUMBER;
   BEGIN
      ln_request_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_OM_PRICING_CONV',
            description   => 'Matrix Conversion Program ',
            start_time    => '',
            argument1     => TO_CHAR (nvl(l_start_date,trunc(sysdate)), 'YYYY/MM/DD'), --7.0
            argument2     => TO_CHAR (SYSDATE, 'YYYY/MM/DD'),
            sub_request   => FALSE);
      COMMIT;
      FND_FILE.PUT_LINE (FND_FILE.log, 'Child Request id:' || ln_request_id);
   END;
   
end xxwc_qp_modifier_iface_pkg;
/