CREATE OR REPLACE 
PACKAGE BODY      APPS.XXWC_INV_MONTH_END_REPORTS_PKG AS

/******************************************************************************
   NAME:       XXWC_INV_MONTH_END_REPORTS_PKG

   PURPOSE:    Process Interorg Shipments That Were Received Short And Intorg Transfer It Back to the Shipping Organization
   

   REVISIONS:
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        13-MAR-2013  Lee Spitzer       1. Create the PL/SQL Package
   2.0        11/11/2014   Raghavendra S        TMS# 20141001-00253 - Canada Multi org Changes
******************************************************************************/


  /*************************************************************************
 *   Procedure : Write_Log
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/
   -- add debug message to log table and concurrent log file
   PROCEDURE Write_Log (p_debug_level   IN NUMBER,
                        p_mod_name      IN VARCHAR2,
                        p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;
      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      --DBMS_OUTPUT.put_line (P_DEBUG_MSG);
      COMMIT;
   END Write_Log;

  /*************************************************************************
 *   Procedure : Write_Error
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_Error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT G_PACKAGE_NAME ;
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START' ;
      l_distro_list VARCHAR2 (75)
            DEFAULT 'HDSOracleDevelopers@hdsupply.com'  ;
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running '|| G_PACKAGE_NAME ||' with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'INV'
      );
   END Write_Error;

   /*************************************************************************
    *   Procedure : Write_output
    *
    *   PURPOSE:   This procedure logs message into concurrent output
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;

   /*************************************************************************
    *   Procedure : Write_log_output
    *
    *   PURPOSE:   This procedure logs message into concurrent logfile
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/

   PROCEDURE Write_log_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
   END write_log_output;

   /*************************************************************************
    *   Procedure : ALL_INVENTORY_VALUES_CCR
    *
    *   PURPOSE:   This procedure executes ALL_INVENTORY_VALUES_CCR
    *
    *   Parameter:
    *          retbuf                         OUT VARCHAR2
    *         ,retcode                        OUT NUMBER
    *         ,p_date                         IN VARCHAR2)
    * ************************************************************************/
    
  PROCEDURE  ALL_INVENTORY_VALUES_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2)
    IS
              
              L_REQUEST_ID      NUMBER DEFAULT FND_GLOBAL.CONC_REQUEST_ID;
              L_DATE            DATE;
              L_CATEGORY_SET_ID NUMBER := 1100000043; --Inventory Category Sets
              L_MSG_COUNT       NUMBER;
              L_MSG_DATA        VARCHAR2(8000);
              L_RETURN_STATUS   VARCHAR2(1);
              L_ORGANIZATION_ID NUMBER;
              L_COST_TYPE_ID    NUMBER;
              L_RETURN          VARCHAR2(1);

        CURSOR C_ORG IS 
          SELECT organization_id, primary_cost_method cost_type_id
          FROM   mtl_parameters
          WHERE organization_code NOT IN (fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'), 'MST') --WC Corporate Organization, WC Item Master;  
          ;
          
          CURSOR c_totals (p_request_id in NUMBER)IS
          SELECT organization_code, 
               sum(stk_value) subinventory_value, 
               sum(int_value) intransit_value, 
               sum(rcv_value) receiving_value,  
               sum(ext_value) extended_value
          FROM XXWC.xxwc_all_inventories_value_tmp
          WHERE request_id = p_request_id
          GROUP BY organization_code
          ORDER BY organization_code;


      CURSOR c_details (p_request_id in NUMBER) IS
      SELECT organization_code, 
             segment1 item_number,
             stk_qty  subinventory_quantity,
             stk_value subinventory_value,
             int_qty  intransit_quantity,
             int_value intransit_value,
             rcv_qty receiving_quantity,
             rcv_value receiving_value,
             stk_value + int_value + rcv_value extended_value
        FROM XXWC.xxwc_all_inventories_value_tmp x
        WHERE request_id = p_request_id
        ORDER BY organization_code;

    BEGIN
  
    G_NAME := 'ALL_INVENTORY_VALUES_CCR';
    G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;

    write_log_output('******************************************************************');
    write_log_output('Starting Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    write_log_output(G_PROGRAM_NAME ||' Parameters');
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME ||' P_DATE = ' || P_DATE);
    
    l_date := FND_DATE.CANONICAL_TO_DATE(p_date);   --Change the p_date parameter from a varchar2 to a date 
 
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME ||' L_DATE = ' || L_DATE);
    
      FOR R_ORG IN C_ORG LOOP
  
        CST_Inventory_PUB.Calculate_InventoryValue
          (
                p_api_version => 1.0,
                p_init_msg_list => CST_Utility_PUB.Get_False,
                p_organization_id => r_org.organization_id,
                p_onhand_value => 1,
                p_intransit_value => 1,
                p_receiving_value => 1,
                p_valuation_date => l_date,
                p_cost_type_id => r_org.cost_type_id,
                p_item_from => NULL,
                p_item_to => NULL,
                p_category_set_id => l_category_set_id,
                p_category_from => NULL,
                p_category_to => NULL,
                p_cost_group_from => NULL,
                p_cost_group_to => NULL,
                p_subinventory_from => NULL,
                p_subinventory_to => NULL,
                p_qty_by_revision => NULL,
                p_zero_cost_only => NULL,
                p_zero_qty => NULL,
                p_expense_item => NULL,
                p_expense_sub => NULL,
                p_unvalued_txns => NULL,
                p_receipt => 1,
                p_shipment => 1,
                p_own => 1,
                p_detail => NULL,
                x_return_status => l_return_status,
                x_msg_count => l_msg_count,
                x_msg_data => l_msg_data
              );

    Write_log_output ('Organization id ' || r_org.organization_id);
    Write_log_output ('l_return_status ' || l_return_status);
    Write_log_output ('l_msg_count ' || l_msg_count);
    Write_log_output ('l_msg_data ' || l_msg_data);
    
    
      BEGIN
        
        INSERT INTO XXWC.xxwc_all_inventories_value_tmp 
                (creation_date,
                 created_by,
                 last_update_date,
                 last_updated_by,
                 request_id,
                 cutoff_date,
                 organization_code,
                 segment1,
                 organization_id,
                 inventory_item_id,
                 qty_source,
                 uom_code,
                 stk_qty,
                 int_qty,
                 rcv_qty,
                 stk_value,
                 int_value,
                 rcv_value,
                 ext_value)
                 --Inventory On-Hand Query
                SELECT sysdate,
                       g_user_id,
                       SYSDATE,
                       g_user_id,
                       l_request_id,
                       l_date,
                       organization_code,
                   segment1,
                   organization_id,
                   inventory_item_id,
                   qty_source,
                   uom_code,
                   sum(nvl(stk_qty,0)),
                   sum(nvl(int_qty,0)),
                   sum(nvl(rcv_qty,0)),
                   sum(nvl(stk_value,0)),
                   sum(nvl(int_value,0)),
                   sum(nvl(rcv_value,0f)),
                   round(sum(nvl(stk_value,0)) + sum(nvl(int_value,0)) + sum(nvl(rcv_value,0f)),2)
            FROM (
            SELECT mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   ciqt.qty_source,
                   MSIB.primary_uom_code UOM_CODE,
                   ROUND(SUM(CIQT.rollback_qty),2) STK_QTY,
                   0 INT_QTY,
                   0 RCV_QTY,
                   ROUND(
                     SUM(CIQT.rollback_qty) *
                     DECODE(msi.asset_inventory,1,CICT.item_cost,0)
                   ,2) STK_VALUE,
                   0 INT_VALUE,
                   0 RCV_VALUE
            FROM   mtl_system_items_b msib,
                   mtl_parameters mp,
                   cst_inv_cost_temp cict,
                   cst_inv_qty_temp ciqt,
                   mtl_secondary_inventories msi
            WHERE  CIQT.qty_source IN (3,4,5)
            AND    CICT.cost_source IN (1,2)
            AND    mp.organization_id = msib.organization_id
            AND    msib.organization_id = cict.organization_id
            AND    msib.inventory_item_id = cict.inventory_item_id
            AND    mp.primary_cost_method = cict.cost_type_id
            AND    msib.inventory_item_id = ciqt.inventory_item_id
            AND    msib.organization_id = ciqt.organization_id
            AND    cict.cost_type_id = ciqt.cost_type_id
            AND    cict.cost_group_id = ciqt.cost_group_id
            AND    ciqt.subinventory_code = msi.secondary_inventory_name
            AND    ciqt.organization_id = msi.organization_id
            AND    ciqt.cost_group_id = msi.default_cost_group_id
            --GROUP BY
            GROUP BY
                   mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   ciqt.qty_source,
                   MSIB.primary_uom_code,
                   DECODE(msi.asset_inventory,1,CICT.item_cost,0)
            HAVING SUM(CIQT.rollback_qty) <> 0
            --Order BY
            --ORDER BY
            --      MSIB.segment1,
            --      MP.organization_code
            UNION ALL
            --Intransit On-Hand Query
            SELECT mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   ciqt.qty_source,
                   MSIB.primary_uom_code UOM_CODE,
                   0 STK_QTY,
                   ROUND(SUM(CIQT.rollback_qty),2) INT_QTY,
                   0 RCV_QTY,
                   0 STK_VALUE,
                   ROUND((SUM(CIQT.rollback_qty) * CICT.item_cost),2) INT_VALUE,
                   0 RCV_VALUE
            FROM   mtl_system_items_b msib,
                   mtl_parameters mp,
                   cst_inv_cost_temp cict,
                   cst_inv_qty_temp ciqt
            WHERE  CIQT.qty_source IN (6,7,8)
            AND    CICT.cost_source IN (1,2)
            AND    mp.organization_id = msib.organization_id
            AND    msib.organization_id = cict.organization_id
            AND    msib.inventory_item_id = cict.inventory_item_id
            AND    mp.primary_cost_method = cict.cost_type_id
            AND    msib.inventory_item_id = ciqt.inventory_item_id
            AND    msib.organization_id = ciqt.organization_id
            AND    cict.cost_type_id = ciqt.cost_type_id
            AND    cict.cost_group_id = ciqt.cost_group_id
            --GROUP BY
            GROUP BY
                   mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   ciqt.qty_source,
                   MSIB.primary_uom_code,
                   CICT.item_cost
            --ORDER BY
            --ORDER BY mp.organization_code,
            --         msib.segment1
            HAVING SUM(CIQT.rollback_qty) <> 0
            UNION ALL
            --Receiving On-Hand Query
            SELECT mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   ciqt.qty_source,
                   MSIB.primary_uom_code UOM_CODE,
                   0 STK_QTY,
                   0 INT_QTY,
                   ROUND(SUM(CIQT.rollback_qty),2) RCV_QTY,
                   0 STK_VALUE,
                   0 INT_VALUE,
                   ROUND((SUM(CIQT.rollback_qty) * CICT.item_cost),2) RCV_VALUE
            FROM   mtl_system_items_b msib,
                   mtl_parameters mp,
                   cst_inv_cost_temp cict,
                   cst_inv_qty_temp ciqt
            WHERE  CIQT.qty_source in (9,10)
            AND     CICT.cost_source IN (3,4)
            AND     CICT.organization_id = CIQT.organization_id
            AND     CICT.inventory_item_id = CIQT.inventory_item_id
            AND     CICT.rcv_transaction_id = CIQT.rcv_transaction_id
            AND     MSIB.organization_id = CIQT.organization_id
            AND     MSIB.inventory_item_id = CIQT.inventory_item_id
            AND     MSIB.organization_id = mp.organization_id
            --GROUP BY
            GROUP BY
                   mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   ciqt.qty_source,
                   MSIB.primary_uom_code,
                   CICT.item_cost
            HAVING SUM(CIQT.rollback_qty) <> 0
            )
            GROUP BY organization_code,
                   segment1,
                   organization_id,
                   inventory_item_id,
                   qty_source,
                   uom_code
              ORDER BY organization_code, segment1;

      EXCEPTION
          WHEN OTHERS THEN
            write_log_output('Could not insert');
      END;
  
      
        execute immediate 'truncate table BOM.CST_INV_QTY_TEMP';
        
        execute immediate 'truncate table BOM.CST_INV_COST_TEMP';
        
        execute immediate 'truncate table BOM.cst_item_list_temp';
        
        execute immediate 'truncate table BOM.cst_cg_list_temp';
        
        execute immediate 'truncate table BOM.cst_sub_list_temp';


      COMMIT;

    END LOOP;
    
    write_output('ALL INVENTORIES VALUES');
    write_output('AS OF DATE ' || l_date);
    write_output(' ');
    write_output('TOTALS');
    write_output(' ');
    write_output('ORGANIZATION_CODE'||'|'||
                 'SUBINVENTORY_VALUE'||'|'||
                 'INTRANSIT_VALUE'||'|'||
                 'RECEIVING_VALUE'||'|'||
                 'EXTENDED_VALUE');
                 
    FOR r_totals IN c_totals (l_request_id)
      
        LOOP
        
          write_output(r_totals.organization_code||'|'||
                       r_totals.subinventory_value||'|'||
                       r_totals.intransit_value||'|'||
                       r_totals.receiving_value||'|'||
                       r_totals.extended_value);
              
        
        END loop;
    
    write_output(' ');
    write_output('DETAILS');
    write_output(' ');
    write_output('ORGANIZATION_CODE'||'|'||
                 'ITEM_NUMBER'||'|'||
                 'SUBINVENTORY_QUANTITY'||'|'||
                 'SUBINVENTORY_VALUE'||'|'||
                 'INTRANSIT_QUANTITY'||'|'||
                 'INTRANSIT_VALUE'||'|'||
                 'RECEIVING_QUANTITY'||'|'||
                 'RECEIVING_VALUE'||'|'||
                 'EXTENDED_VALUE');
                 
    FOR r_details IN c_details (l_request_id)
      
        LOOP
        
          write_output(r_details.organization_code||'|'||
                       r_details.item_number||'|'||
                       r_details.subinventory_quantity||'|'||
                       r_details.subinventory_value||'|'||
                       r_details.intransit_quantity||'|'||
                       r_details.intransit_value||'|'||
                       r_details.receiving_quantity||'|'||
                       r_details.receiving_value||'|'||
                       r_details.extended_value);
    
      END LOOP;
      
   RETCODE := 0;
   
   EXCEPTION
   
      WHEN OTHERS THEN
            
            G_MESSAGE := (G_PROGRAM_NAME || ' when others error');
            RETCODE := 2;
      
   END ALL_INVENTORY_VALUES_CCR;        

   /*************************************************************************
    *   Procedure : PERIOD_RECON_CCR
    *
    *   PURPOSE:   This procedure executes PERIOD_RECON_CCR
    *
    *   Parameter:
    *          retbuf                         OUT VARCHAR2
    *         ,retcode                        OUT NUMBER
    *         ,p_period                       IN VARCHAR2)
    * ************************************************************************/
   
   PROCEDURE  PERIOD_RECON_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_period                       IN VARCHAR2)
    IS
    
              L_REQUEST_ID      NUMBER DEFAULT FND_GLOBAL.CONC_REQUEST_ID;
              L_DATE            DATE;
              L_CATEGORY_SET_ID NUMBER := 1100000043; --Inventory Category Sets
              L_MSG_COUNT       NUMBER;
              L_MSG_DATA        VARCHAR2(8000);
              L_RETURN_STATUS   VARCHAR2(1);
              L_ORGANIZATION_ID NUMBER;
              L_COST_TYPE_ID    NUMBER;
              L_RETURN          VARCHAR2(1);

              CURSOR C_ORG IS 
                  SELECT oap.organization_id,
                         oap.acct_period_id,
                         NVL(summarized_flag,'N') summarized_flag,
                         DECODE(open_flag,'N',0,1) open_flag,
                         schedule_close_date
                  FROM   org_acct_periods oap,
                         mtl_parameters mp
                  WHERE  oap.organization_id = mp.organization_id
                  AND    mp.organization_code NOT IN (fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'), 'MST')
                  AND    oap.period_name = p_period
                  ;

          cursor c_totals (p_request_id in number) is
            SELECT organization_code,
                   sum(accounted_value) accounted_value,
                   sum(onhand_value) onhand_value,
                   sum(variance_value) variance_value
            FROM   XXWC.XXWC_PERIOD_RECON_TEMP
            WHERE  request_id = p_request_id
            GROUP BY organization_code
            ORDER BY ORGANIZATION_CODE;

          cursor c_details (p_request_id in number) is
            SELECT organization_code,
                   segment1,
                   accounted_value,
                   onhand_value,
                   variance_value
            FROM   XXWC.XXWC_PERIOD_RECON_TEMP
            WHERE  request_id = p_request_id
            ORDER BY ORGANIZATION_CODE, SEGMENT1
            ;
    
    BEGIN
    
        G_NAME := 'PERIOD_RECON_CCR';
        G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;
    
        write_log_output('******************************************************************');
        write_log_output('Starting Program '||G_PROGRAM_NAME);
        write_log_output('******************************************************************');
        write_log_output(G_PROGRAM_NAME ||' Parameters');
        write_log_output('--------------------------------------------');
        write_log_output(G_PROGRAM_NAME ||' P_PERIOD = ' || P_PERIOD);
        
        
         FOR R_ORG IN C_ORG LOOP
  
          IF r_org.open_flag = 1 then
  
            CST_AccountingPeriod_PUB.Summarize_Period(
              p_api_version   => 1.0,
              p_org_id        => r_org.organization_id,
              p_period_id     => r_org.acct_period_id,
              p_to_date       => r_org.schedule_close_date,
              p_user_id       => NULL, --user id
              p_login_id      => NULL, --login id
              p_simulation    => r_org.open_flag,
              x_return_status => l_return_status,
              x_msg_data      => l_msg_data);
            
            write_log_output('organization_id ' || r_org.organization_id);  
            write_log_output('l_return_status ' || l_return_status);
            write_log_output('l_msg_count ' || l_msg_count);
            write_log_output('l_msg_data ' || l_msg_data);  
          
          
                IF l_return_status in (2,3) THEN
                
                    BEGIN
                  
                      INSERT INTO XXWC.XXWC_PERIOD_RECON_TEMP
                        (creation_date      
                        ,created_by          
                        ,last_update_date    
                        ,last_updated_by     
                        ,request_id          
                        ,period         
                        ,organization_code   
                        ,segment1            
                        ,organization_id     
                        ,inventory_item_id
                        ,accounted_value     
                        ,onhand_value        
                        ,variance_value)
                       SELECT 
                           SYSDATE,
                           g_user_id,
                           SYSDATE,
                           g_user_id,
                           l_request_id,
                           p_period,
                           mp.organization_code,
                           msib.segment1,
                           mp.organization_id,
                           msib.inventory_item_id,
                           round(nvl(sum(cpcs.accounted_value),0),2) accounted_value,
                           round(nvl(sum(cpcs.rollback_value),0),2) onhand_value,
                           round(nvl(sum(cpcs.rollback_value),0) - nvl(sum(cpcs.accounted_value),0),2) variance_value
                        FROM   mtl_parameters mp,
                               mtl_system_items_b msib,
                               cst_per_close_summary_temp cpcs
                        WHERE  mp.organization_id = msib.organization_id
                        AND    msib.inventory_item_id = cpcs.inventory_item_id
                        AND    mp.default_cost_group_id = cpcs.cost_group_id
                        ---Parameters
                        AND    mp.organization_id = r_org.organization_id
                        --Group By
                        GROUP BY mp.organization_code,
                                 MSIB.segment1,
                                 mp.organization_id,
                                 msib.inventory_item_id
                        --Order By
                        ORDER BY mp.organization_code,
                                 msib.segment1;
                      
                      EXCEPTION
                          WHEN OTHERS THEN
                              write_log_output ('Could not insert ' || SQLCODE||SQLERRM);
                      END;      
                      
                        
                    ELSE         
                         g_message := ('Return status for API CST_AccountingPeriod_PUB.Summarize_Period ' || l_return);
                          raise g_exception;
                          
                    END IF;
                        
          
            EXECUTE IMMEDIATE 'truncate table BOM.CST_INV_QTY_TEMP';
        
            EXECUTE IMMEDIATE 'truncate table BOM.CST_INV_COST_TEMP';
        
            EXECUTE IMMEDIATE 'truncate table BOM.cst_item_list_temp';
        
            EXECUTE IMMEDIATE 'truncate table BOM.cst_cg_list_temp';
        
            EXECUTE IMMEDIATE 'truncate table BOM.cst_sub_list_temp';
          
            EXECUTE IMMEDIATE 'truncate table BOM.cst_per_close_summary_temp';
        
            COMMIT;
          
          ELSE
          
            BEGIN
          
              INSERT INTO XXWC.XXWC_PERIOD_RECON_TEMP
                (creation_date      
                ,created_by          
                ,last_update_date    
                ,last_updated_by     
                ,request_id          
                ,period         
                ,organization_code   
                ,segment1            
                ,organization_id     
                ,inventory_item_id
                ,accounted_value     
                ,onhand_value        
                ,variance_value)
               SELECT 
                   SYSDATE,
                   g_user_id,
                   SYSDATE,
                   g_user_id,
                   l_request_id,
                   p_period,
                   mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   round(nvl(sum(cpcs.accounted_value),0),2) accounted_value,
                   round(nvl(sum(cpcs.rollback_value),0),2) onhand_value,
                   round(nvl(sum(cpcs.rollback_value),0) - nvl(sum(cpcs.accounted_value),0),2) variance_value
                   --round(nvl(sum(cpcs.rollback_value),0),2) - round(nvl(sum(cpcs.accounted_value),0),2) variance_value
                FROM   mtl_parameters mp,
                       mtl_system_items_b msib,
                       cst_period_close_summary cpcs
                WHERE  mp.organization_id = msib.organization_id
                AND    msib.organization_id = cpcs.organization_id
                AND    msib.inventory_item_id = cpcs.inventory_item_id
                AND    mp.default_cost_group_id = cpcs.cost_group_id
                ---Parameters
                AND    mp.organization_id = r_org.organization_id
                AND    cpcs.acct_period_id = r_org.acct_period_id
                --Group By
                GROUP BY mp.organization_code,
                         MSIB.segment1,
                         mp.organization_id,
                         msib.inventory_item_id
                --Order By
                ORDER BY mp.organization_code,
                         msib.segment1;
              
              EXCEPTION
                  WHEN OTHERS THEN
                      g_message := ('Could not insert ' || SQLCODE||SQLERRM);
                      raise g_exception;
              END;      
               
            
          END IF;
          
        END LOOP;

    
    write_output('PERIOD CLOSE RECONCILATION');
    write_output('PERIOD ' || p_period);
    write_output(' ');
    write_output('TOTALS');
    write_output(' ');
    write_output('ORGANIZATION_CODE'||'|'||
                 'ACCOUNTED_VALUE'||'|'||
                 'ONHAND_VALUE'||'|'||
                 'VARIANCE_VALUE');
                 
    FOR r_totals IN c_totals (l_request_id)
      
        LOOP
        
          write_output(r_totals.organization_code||'|'||
                       r_totals.accounted_value||'|'||
                       r_totals.onhand_value||'|'||
                       r_totals.variance_value);
              
        
        END loop;
    
    
    write_output(' ');
    write_output('DETAILS');
    write_output(' ');
    write_output('ORGANIZATION_CODE'||'|'||
                 'ITEM_NUMBER'||'|'||
                 'ACCOUNTED_VALUE'||'|'||
                 'ONHAND_VALUE'||'|'||
                 'VARIANCE_VALUE');
                 
    FOR r_details IN c_details (l_request_id)
      
        LOOP
        
          write_output(r_details.organization_code||'|'||
                       r_details.segment1||'|'||
                       r_details.accounted_value||'|'||
                       r_details.onhand_value||'|'||
                       r_details.variance_value);
                        
      END LOOP;
      
   RETCODE := 0;
   
      
    EXCEPTION
    
    WHEN G_EXCEPTION THEN
    
            write_log_output  (g_message);
            RETCODE := 2;
     
     
     WHEN OTHERS THEN
            
            G_MESSAGE := (G_PROGRAM_NAME || ' when others error');
            write_log_output (g_message);
            RETCODE := 2;
        
    END PERIOD_RECON_CCR;
    
    /*************************************************************************
    *   Procedure : RECEIVING_VALUES_CCR
    *
    *   PURPOSE:   This procedure executes RECEIVING_VALUES_CCR
    *
    *   Parameter:
    *          retbuf                         OUT VARCHAR2
    *         ,retcode                        OUT NUMBER
    *         ,p_date                         IN VARCHAR2)
    * ************************************************************************/

    PROCEDURE  RECEIVING_VALUES_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2)
    IS
              
              L_REQUEST_ID      NUMBER DEFAULT FND_GLOBAL.CONC_REQUEST_ID;
              L_DATE            DATE;
              L_CATEGORY_SET_ID NUMBER := 1100000043; --Inventory Category Sets
              L_MSG_COUNT       NUMBER;
              L_MSG_DATA        VARCHAR2(8000);
              L_RETURN_STATUS   VARCHAR2(1);
              L_ORGANIZATION_ID NUMBER;
              L_COST_TYPE_ID    NUMBER;
              L_RETURN          VARCHAR2(1);

        CURSOR C_ORG IS 
          SELECT organization_id, primary_cost_method cost_type_id
          FROM   mtl_parameters
          WHERE organization_code NOT IN (fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'), 'MST') --WC Corporate Organization, WC Item Master;  
          ;
          
          CURSOR c_totals (p_request_id in NUMBER)IS
          SELECT organization_code, 
               sum(rcv_value) receiving_value
          FROM XXWC.xxwc_receiving_value_temp
          WHERE request_id = p_request_id
          GROUP BY organization_code
          ORDER BY organization_code;


      CURSOR c_details (p_request_id in NUMBER) IS
      SELECT organization_code, 
             segment1 item_number,
             rcv_qty receiving_quantity,
             rcv_value receiving_value
        FROM XXWC.xxwc_receiving_value_temp x
        WHERE request_id = p_request_id
        ORDER BY organization_code;

    BEGIN
  
    G_NAME := 'RECEIVING_VALUES_CCR';
    G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;

    write_log_output('******************************************************************');
    write_log_output('Starting Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    write_log_output(G_PROGRAM_NAME ||' Parameters');
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME ||' P_DATE = ' || P_DATE);
    
    l_date := FND_DATE.CANONICAL_TO_DATE(p_date);   --Change the p_date parameter from a varchar2 to a date 
 
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME ||' L_DATE = ' || L_DATE);
    
      FOR R_ORG IN C_ORG LOOP
  
        CST_Inventory_PUB.Calculate_InventoryValue
          (
                p_api_version => 1.0,
                p_init_msg_list => CST_Utility_PUB.Get_False,
                p_organization_id => r_org.organization_id,
                p_onhand_value => 0,
                p_intransit_value => 0,
                p_receiving_value => 1,
                p_valuation_date => l_date,
                p_cost_type_id => r_org.cost_type_id,
                p_item_from => NULL,
                p_item_to => NULL,
                p_category_set_id => l_category_set_id,
                p_category_from => NULL,
                p_category_to => NULL,
                p_cost_group_from => NULL,
                p_cost_group_to => NULL,
                p_subinventory_from => NULL,
                p_subinventory_to => NULL,
                p_qty_by_revision => NULL,
                p_zero_cost_only => NULL,
                p_zero_qty => NULL,
                p_expense_item => NULL,
                p_expense_sub => NULL,
                p_unvalued_txns => NULL,
                p_receipt => NULL,
                p_shipment => NULL,
                p_own => 1,
                p_detail => NULL,
                p_cost_enabled_only => 0,
                p_one_time_item => 2,
                p_include_period_end => 2,
                x_return_status => l_return_status,
                x_msg_count => l_msg_count,
                x_msg_data => l_msg_data
              );

    Write_log_output ('Organization id ' || r_org.organization_id);
    Write_log_output ('l_return_status ' || l_return_status);
    Write_log_output ('l_msg_count ' || l_msg_count);
    Write_log_output ('l_msg_data ' || l_msg_data);
    
    
      BEGIN
        
        INSERT INTO XXWC.xxwc_receiving_value_temp 
                (creation_date,
                 created_by,
                 last_update_date,
                 last_updated_by,
                 request_id,
                 cutoff_date,
                 organization_code,
                 segment1,
                 organization_id,
                 inventory_item_id,
                 qty_source,
                 uom_code,
                 rcv_qty,
                 rcv_value)
                 --Inventory On-Hand Query
                SELECT sysdate,
                       g_user_id,
                       SYSDATE,
                       g_user_id,
                       l_request_id,
                       l_date,
                       organization_code,
                   segment1,
                   organization_id,
                   inventory_item_id,
                   qty_source,
                   uom_code,
                   sum(nvl(rcv_qty,0)),
                   sum(nvl(rcv_value,0f))
            FROM (
            --Receiving On-Hand Query
            SELECT mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   ciqt.qty_source,
                   MSIB.primary_uom_code UOM_CODE,
                   ROUND(SUM(CIQT.rollback_qty),2) RCV_QTY,
                   ROUND((SUM(CIQT.rollback_qty) * CICT.item_cost),2) RCV_VALUE
            FROM   mtl_system_items_b msib,
                   mtl_parameters mp,
                   cst_inv_cost_temp cict,
                   cst_inv_qty_temp ciqt
            WHERE  CIQT.qty_source in (9,10)
            AND     CICT.cost_source IN (3,4)
            AND     CICT.organization_id = CIQT.organization_id
            AND     CICT.inventory_item_id = CIQT.inventory_item_id
            AND     CICT.rcv_transaction_id = CIQT.rcv_transaction_id
            AND     MSIB.organization_id = CIQT.organization_id
            AND     MSIB.inventory_item_id = CIQT.inventory_item_id
            AND     MSIB.organization_id = mp.organization_id
            --GROUP BY
            GROUP BY
                   mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   ciqt.qty_source,
                   MSIB.primary_uom_code,
                   CICT.item_cost
            HAVING SUM(CIQT.rollback_qty) <> 0
            )
            GROUP BY organization_code,
                   segment1,
                   organization_id,
                   inventory_item_id,
                   qty_source,
                   uom_code
              ORDER BY organization_code, segment1;

      EXCEPTION
          WHEN OTHERS THEN
            write_log_output('Could not insert');
      END;
  
      
        execute immediate 'truncate table BOM.CST_INV_QTY_TEMP';
        
        execute immediate 'truncate table BOM.CST_INV_COST_TEMP';
        
        execute immediate 'truncate table BOM.cst_item_list_temp';
        
        execute immediate 'truncate table BOM.cst_cg_list_temp';
        
        execute immediate 'truncate table BOM.cst_sub_list_temp';


      COMMIT;

    END LOOP;
    
    write_output('RECEIVING VALUES');
    write_output('AS OF DATE ' || l_date);
    write_output(' ');
    write_output('TOTALS');
    write_output(' ');
    write_output('ORGANIZATION_CODE'||'|'||
                 'RECEIVING_VALUE');
                 
    FOR r_totals IN c_totals (l_request_id)
      
        LOOP
        
          write_output(r_totals.organization_code||'|'||
                       r_totals.receiving_value);
                       
        
        END loop;
    
    write_output(' ');
    write_output('DETAILS');
    write_output(' ');
    write_output('ORGANIZATION_CODE'||'|'||
                 'ITEM_NUMBER'||'|'||
                 'RECEIVING_QUANTITY'||'|'||
                 'RECEIVING_VALUE');
                 
    FOR r_details IN c_details (l_request_id)
      
        LOOP
        
          write_output(r_details.organization_code||'|'||
                       r_details.item_number||'|'||
                       r_details.receiving_quantity||'|'||
                       r_details.receiving_value);
                       
      END LOOP;
      
   RETCODE := 0;
   
   EXCEPTION
   
      WHEN OTHERS THEN
            
            G_MESSAGE := (G_PROGRAM_NAME || ' when others error');
            RETCODE := 2;
      
   END RECEIVING_VALUES_CCR;        
  
  
    /*************************************************************************
    *   Procedure : INTRANSIT_VALUES_CCR
    *
    *   PURPOSE:   This procedure executes INTRANSIT_VALUES_CCR
    *
    *   Parameter:
    *          retbuf                         OUT VARCHAR2
    *         ,retcode                        OUT NUMBER
    *         ,p_date                         IN VARCHAR2)
    * ************************************************************************/

   PROCEDURE  INTRANSIT_VALUES_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2)
    IS
              
              L_REQUEST_ID      NUMBER DEFAULT FND_GLOBAL.CONC_REQUEST_ID;
              L_DATE            DATE;
              L_CATEGORY_SET_ID NUMBER := 1100000043; --Inventory Category Sets
              L_MSG_COUNT       NUMBER;
              L_MSG_DATA        VARCHAR2(8000);
              L_RETURN_STATUS   VARCHAR2(1);
              L_ORGANIZATION_ID NUMBER;
              L_COST_TYPE_ID    NUMBER;
              L_RETURN          VARCHAR2(1);

        CURSOR C_ORG IS 
          SELECT organization_id, primary_cost_method cost_type_id
          FROM   mtl_parameters
          WHERE organization_code NOT IN (fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'), 'MST') --WC Corporate Organization, WC Item Master;  
          ;
          
          CURSOR c_totals (p_request_id in NUMBER)IS
          SELECT organization_code, 
               sum(int_value) intransit_value
          FROM XXWC.xxwc_intransit_value_temp
          WHERE request_id = p_request_id
          GROUP BY organization_code
          ORDER BY organization_code;


      CURSOR c_details (p_request_id in NUMBER) IS
      SELECT organization_code, 
             segment1 item_number,
             int_qty  intransit_quantity,
             int_value intransit_value
        FROM XXWC.xxwc_intransit_value_temp x
        WHERE request_id = p_request_id
        ORDER BY organization_code;

    BEGIN
  
    G_NAME := 'INTRANSIT_VALUES_CCR';
    G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;

    write_log_output('******************************************************************');
    write_log_output('Starting Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    write_log_output(G_PROGRAM_NAME ||' Parameters');
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME ||' P_DATE = ' || P_DATE);
    
    l_date := FND_DATE.CANONICAL_TO_DATE(p_date);   --Change the p_date parameter from a varchar2 to a date 
 
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME ||' L_DATE = ' || L_DATE);
    
      FOR R_ORG IN C_ORG LOOP
  
        CST_Inventory_PUB.Calculate_InventoryValue
          (
                p_api_version => 1.0,
                p_init_msg_list => CST_Utility_PUB.Get_False,
                p_organization_id => r_org.organization_id,
                p_onhand_value => 0,
                p_intransit_value => 1,
                p_receiving_value => 0,
                p_valuation_date => l_date,
                p_cost_type_id => r_org.cost_type_id,
                p_item_from => NULL,
                p_item_to => NULL,
                p_category_set_id => l_category_set_id,
                p_category_from => NULL,
                p_category_to => NULL,
                p_cost_group_from => NULL,
                p_cost_group_to => NULL,
                p_subinventory_from => NULL,
                p_subinventory_to => NULL,
                p_qty_by_revision => NULL,
                p_zero_cost_only => NULL,
                p_zero_qty => NULL,
                p_expense_item => NULL,
                p_expense_sub => NULL,
                p_unvalued_txns => NULL,
                p_receipt => 1,
                p_shipment => 1,
                p_own => 1,
                p_detail => 1,
                x_return_status => l_return_status,
                x_msg_count => l_msg_count,
                x_msg_data => l_msg_data
              );

    Write_log_output ('Organization id ' || r_org.organization_id);
    Write_log_output ('l_return_status ' || l_return_status);
    Write_log_output ('l_msg_count ' || l_msg_count);
    Write_log_output ('l_msg_data ' || l_msg_data);
    
    
      BEGIN
        
        INSERT INTO XXWC.xxwc_intransit_value_temp 
                (creation_date,
                 created_by,
                 last_update_date,
                 last_updated_by,
                 request_id,
                 cutoff_date,
                 organization_code,
                 segment1,
                 organization_id,
                 inventory_item_id,
                 qty_source,
                 uom_code,
                 int_qty,
                 int_value)
                 --Inventory On-Hand Query
                SELECT sysdate,
                       g_user_id,
                       SYSDATE,
                       g_user_id,
                       l_request_id,
                       l_date,
                       organization_code,
                   segment1,
                   organization_id,
                   inventory_item_id,
                   qty_source,
                   uom_code,
                   sum(nvl(int_qty,0)),
                   sum(nvl(int_value,0))
            FROM (
            --Intransit On-Hand Query
            SELECT mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   ciqt.qty_source,
                   MSIB.primary_uom_code UOM_CODE,
                   ROUND(SUM(CIQT.rollback_qty),2) INT_QTY,
                   ROUND((SUM(CIQT.rollback_qty) * CICT.item_cost),2) INT_VALUE
             FROM   mtl_system_items_b msib,
                   mtl_parameters mp,
                   cst_inv_cost_temp cict,
                   cst_inv_qty_temp ciqt
            WHERE  CIQT.qty_source IN (6,7,8)
            AND    CICT.cost_source IN (1,2)
            AND    mp.organization_id = msib.organization_id
            AND    msib.organization_id = cict.organization_id
            AND    msib.inventory_item_id = cict.inventory_item_id
            AND    mp.primary_cost_method = cict.cost_type_id
            AND    msib.inventory_item_id = ciqt.inventory_item_id
            AND    msib.organization_id = ciqt.organization_id
            AND    cict.cost_type_id = ciqt.cost_type_id
            AND    cict.cost_group_id = ciqt.cost_group_id
            --GROUP BY
            GROUP BY
                   mp.organization_code,
                   msib.segment1,
                   mp.organization_id,
                   msib.inventory_item_id,
                   ciqt.qty_source,
                   MSIB.primary_uom_code,
                   CICT.item_cost
            --ORDER BY
            --ORDER BY mp.organization_code,
            --         msib.segment1
            HAVING SUM(CIQT.rollback_qty) <> 0)
            GROUP BY organization_code,
                   segment1,
                   organization_id,
                   inventory_item_id,
                   qty_source,
                   uom_code
              ORDER BY organization_code, segment1;
            
      EXCEPTION
          WHEN OTHERS THEN
            write_log_output('Could not insert');
      END;
  
      
        execute immediate 'truncate table BOM.CST_INV_QTY_TEMP';
        
        execute immediate 'truncate table BOM.CST_INV_COST_TEMP';
        
        execute immediate 'truncate table BOM.cst_item_list_temp';
        
        execute immediate 'truncate table BOM.cst_cg_list_temp';
        
        execute immediate 'truncate table BOM.cst_sub_list_temp';


      COMMIT;

    END LOOP;
    
    write_output('INTRANSIT VALUES');
    write_output('AS OF DATE ' || l_date);
    write_output(' ');
    write_output('TOTALS');
    write_output(' ');
    write_output('ORGANIZATION_CODE'||'|'||
                 'INTRANSIT_VALUE');
                 
    FOR r_totals IN c_totals (l_request_id)
      
        LOOP
        
          write_output(r_totals.organization_code||'|'||
                       r_totals.intransit_value);
                       
        
        END loop;
    
    write_output(' ');
    write_output('DETAILS');
    write_output(' ');
    write_output('ORGANIZATION_CODE'||'|'||
                 'ITEM_NUMBER'||'|'||
                 'INTRANSIT_QUANTITY'||'|'||
                 'INTRANSIT_VALUE');
                 
    FOR r_details IN c_details (l_request_id)
      
        LOOP
        
          write_output(r_details.organization_code||'|'||
                       r_details.item_number||'|'||
                       r_details.intransit_quantity||'|'||
                       r_details.intransit_value);                 
      END LOOP;
      
   RETCODE := 0;
   
   EXCEPTION
   
      WHEN OTHERS THEN
            
            G_MESSAGE := (G_PROGRAM_NAME || ' when others error');
            RETCODE := 2;
      
   END INTRANSIT_VALUES_CCR;        
                      
END XXWC_INV_MONTH_END_REPORTS_PKG;