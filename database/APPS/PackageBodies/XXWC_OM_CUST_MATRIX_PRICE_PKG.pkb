/* Formatted on 5/3/2017 4:12:49 PM (QP5 v5.256.13226.35538) */
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_CUST_MATRIX_PRICE_PKG
AS
   /************************************************************************************************************************************************************
          $Header XXWC_OM_CUST_MATRIX_PRICE_PKG $
          Module Name: XXWC_OM_CUST_MATRIX_PRICE_PKG.pkb

          PURPOSE:   This package is to populate Custom Matrix price eligibility info into custom table.

        REVISIONS:
        Ver        Date        Author                  Description
       ---------  ----------  ---------------         -------------------------
        1.0        04/17/2017  P.Vamshidhar            Initial Version
                                                       TMS# 20160526-00017 - Ability to inactive Matrix modifiers in Bulk
                                                       TMS# 20160113-00014 -  Display Matrix Customer Flag on SOE banner, and this flag will be used in OBIEE

      *********************************************************************************************************************************************************/

   g_err_callfrom    VARCHAR2 (100) := 'XXWC_OM_CUST_MATRIX_PRICE_PKG';
   g_err_callpoint   VARCHAR2 (150);
   g_distro_list     VARCHAR2 (100) := 'HDSOracleDevelopers@hdsupply.com';


   PROCEDURE populate_data (x_errbuf              OUT VARCHAR2,
                            x_retcode             OUT VARCHAR2,
                            p_list_header_id   IN     NUMBER)
   IS
      ln_user_id     FND_USER.USER_ID%TYPE := FND_GLOBAL.USER_ID;
      l_err_msg      VARCHAR2 (2000);
      l_procedure    VARCHAR2 (100) := 'POPULATE_DATA';
      ln_row_count   NUMBER := 0;
   BEGIN
      g_err_callpoint := 'Deleting Data from Custom Table';
      write_log (g_err_callpoint);

      -- Truncate table

      IF p_list_header_id IS NULL
      THEN
         DELETE FROM XXWC.XXWC_CUST_MTRX_PRICE_ELIG_TBL;
      END IF;

      g_err_callpoint := 'Inserting data into custom table.';
      write_log (g_err_callpoint);

      INSERT INTO /*+ APPEND */
                 XXWC.XXWC_CUST_MTRX_PRICE_ELIG_TBL (CUST_ACCOUNT_ID,
                                                     QUALIFIER_TYPE,
                                                     CREATION_DATE,
                                                     CREATED_BY,
                                                     LAST_UPDATE_DATE,
                                                     LAST_UPDATED_BY)
         SELECT DISTINCT A.QUALIFIER_ATTR_VALUE,
                         'BILL_TO',
                         SYSDATE,
                         ln_user_id,
                         SYSDATE,
                         ln_user_id
           FROM apps.qp_qualifiers A
          WHERE     list_header_id IN (SELECT list_header_id
                                         FROM apps.qp_list_headers
                                        WHERE     attribute10 =
                                                     'Segmented Price'
                                              AND active_flag = 'Y'
                                              AND list_header_id =
                                                     NVL (p_list_header_id,
                                                          list_header_id))
                AND qualifier_attribute = 'QUALIFIER_ATTRIBUTE32';


      ln_row_count := SQL%ROWCOUNT;

      write_log ('Number of rows inserted' || ln_row_count);

      IF ln_row_count = 0
      THEN
         x_errbuf :=
            ' No Records Inserted into XXWC.XXWC_CUST_MTRX_PRICE_ELIG_TBL table, Please contact Development Team';
         x_retcode := 2;
         ROLLBACK;
      ELSE
         x_retcode := 0;
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_retcode := 2;
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
         write_log (l_err_msg);

         ROLLBACK;
   END;


   PROCEDURE xxwc_qp_ntl_prl_maint_tool (X_ERRBUF       OUT VARCHAR2,
                                         X_RETCODE      OUT VARCHAR2,
                                         P_FILE_ID   IN     NUMBER)
   IS
      l_msg_data                    VARCHAR2 (4000);
      l_error_message2              CLOB;

      l_retcode                     NUMBER;
      l_errmsg                      VARCHAR2 (2000);

      -- File Processing Variables
      l_file_id                     NUMBER := P_FILE_ID;
      l_file                        BLOB;
      l_file_name                   VARCHAR2 (256);
      i                             INTEGER := 1;               -- Row counter
      j                             INTEGER := 1;            -- Column Counter
      is_file_open                  INTEGER;
      l_hdr                         INTEGER;
      l_line                        INTEGER;

      l_comma_count                 NUMBER;
      l_offset                      NUMBER := 1;
      l_eol_pos                     NUMBER := 32767;
      l_amount                      NUMBER;
      l_file_len                    NUMBER := DBMS_LOB.getlength (l_file);
      l_buffer                      RAW (30000);

      l_data                        VARCHAR2 (240);
      l_is_number                   VARCHAR2 (1);

      l_start_time                  NUMBER;
      l_end_time                    NUMBER;
      l_conc_req_id                 NUMBER := fnd_global.conc_request_id;
      l_login_id                    NUMBER := fnd_global.login_id;

      l_cnt                         NUMBER;
      l                             NUMBER;                               -- i
      m                             NUMBER;                               -- j
      n                             NUMBER;                               -- k
      l_api_version_number          NUMBER := 1.0;
      l_init_msg_list               VARCHAR2 (10) := FND_API.G_FALSE;
      l_return_values               VARCHAR2 (10) := FND_API.G_FALSE;
      l_commit                      VARCHAR2 (10) := FND_API.G_TRUE;
      l_x_return_status             VARCHAR2 (255) := 'S';
      l_x_msg_count                 NUMBER;
      l_x_msg_data                  VARCHAR2 (255);
      g_user_id                     NUMBER;
      l_price_list_rec              QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE
                                       := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;
      l_price_list_val_rec          QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE
         := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_VAL_REC;
      l_price_list_line             QP_PRICE_LIST_PUB.PRICE_LIST_LINE_REC_TYPE
         := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
      l_price_list_line_tbl         QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE
         := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL;
      l_price_list_line_val_tbl     QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE
         := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_VAL_TBL;
      l_qualifiers_tbl              QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE
         := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_TBL;
      l_qualifiers_val_tbl          QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE
         := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_VAL_TBL;
      l_pricing_att_rec             QP_PRICE_LIST_PUB.PRICING_ATTR_REC_TYPE
         := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;
      l_pricing_attr_tbl            QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE
         := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
      l_pricing_attr_val_tbl_type   QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE
         := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_VAL_TBL;
      l_x_price_list_rec            QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE;
      l_x_price_list_val_rec        QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE;
      l_x_price_list_line_tbl       QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE;
      l_x_price_list_line_val_tbl   QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE;
      l_x_qualifiers_tbl            QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE;
      l_x_qualifiers_val_tbl        QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE;
      l_x_pricing_attr_tbl          QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE;
      l_x_pricing_attr_val_tbl      QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE;

      l_error_message_list          ERROR_HANDLER.ERROR_TBL_TYPE;

      l_commit_size                 NUMBER;
      l_submit_time                 VARCHAR2 (240);

      -- QP Pricing API Variables/Cursors

      l_cfp_flag                    VARCHAR2 (1);
      l_prog_error                  EXCEPTION;
      l_sec                         VARCHAR2 (200);


      TYPE price_modifier_rec_type IS RECORD
      (
         NAME                VARCHAR2 (240 BYTE):= FND_API.G_MISS_CHAR,
         ACTIVE_FLAG         VARCHAR2 (1 BYTE):= FND_API.G_MISS_CHAR,
         START_DATE_ACTIVE   DATE:= FND_API.G_MISS_DATE,
         END_DATE_ACTIVE     DATE:= FND_API.G_MISS_DATE,
         OPERATION           VARCHAR2 (20 BYTE):= FND_API.G_MISS_CHAR,
         FILE_ID             NUMBER:= FND_API.G_MISS_NUM,
         CREATION_DATE       DATE:= FND_API.G_MISS_DATE,
         CREATED_BY          NUMBER:= FND_API.G_MISS_NUM,
         LAST_UPDATE_DATE    DATE:= FND_API.G_MISS_DATE,
         LAST_UPDATED_BY     NUMBER:= FND_API.G_MISS_NUM,
         REQUEST_ID          NUMBER:= FND_API.G_MISS_NUM,
         STATUS              VARCHAR2 (20 BYTE):= FND_API.G_MISS_CHAR
      );


      TYPE price_modifier_tbl_type IS TABLE OF price_modifier_rec_type
         INDEX BY BINARY_INTEGER;

      l_price_modifier_tbl_type     price_modifier_tbl_type;
      lvc_procedure                 VARCHAR2 (100)
                                       := 'XXWC_QP_NTL_PRL_MAINT_TOOL';
   BEGIN
      write_output (
         'Starting Maintenance Tool Processing for File ID: ' || l_file_id);
      write_output (
         '---------------------------------------------------------------');

      l_Start_time := DBMS_UTILITY.get_time;
      write_log ('l_Start_time: ' || l_Start_time);
      l_sec :=
         'Starting Maintenance Tool Processing for File ID: ' || l_file_id;

      BEGIN
         SELECT file_data, file_name
           INTO l_file, l_file_name
           FROM fnd_lobs
          WHERE file_id = l_file_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_file := NULL;
            l_file_name := NULL;
            write_log ('File Not Found. Error: ' || SQLERRM);
            l_msg_data := SUBSTR (SQLERRM, 1, 4000);
            x_errbuf := l_msg_data;
            x_retcode := '2';
            RAISE;
      END;

      is_file_open := DBMS_LOB.ISOPEN (l_file);

      IF is_file_open = 1
      THEN
         DBMS_LOB.CLOSE (l_file);
      END IF;

      BEGIN
         DBMS_LOB.open (l_file, DBMS_LOB.lob_readonly);
         is_file_open := DBMS_LOB.ISOPEN (l_file);
      EXCEPTION
         WHEN OTHERS
         THEN
            write_log ('Could not open file. Error: ' || SQLERRM);
            l_msg_data := SUBSTR (SQLERRM, 1, 4000);
            x_errbuf := l_msg_data;
            x_retcode := '2';
            RAISE;
      END;

      l_file_len := DBMS_LOB.getlength (l_file);
      write_output (
         'File Name ' || l_file_name || ' and length ' || l_file_len);

      write_output ('Starting File read and parsing from LOB...');
      l_sec := 'Starting File read and parsing from LOB ';

      l_hdr := 1;
      l_line := 1;
      l_data := NULL;

      WHILE (l_offset < l_file_len)
      LOOP
         BEGIN
            -- i: Row Counter. Reset Per File
            -- j: Column Counter. Reset Per row

            -- Finding EOL position  for new line/line feed ascii 0A
            l_eol_pos :=
               DBMS_LOB.INSTR (l_file,
                               '0A',
                               1,
                               i);

            -- Calculating how many chars to pull in buffer based on EOL position and offset
            IF i = 1
            THEN
               l_amount := l_eol_pos - 1;
            ELSE
               l_amount := l_eol_pos - l_offset;
            END IF;

            -- Reading first record from LOB file
            DBMS_LOB.read (l_file,
                           l_amount,
                           l_offset,
                           l_buffer);

            -- From the first record pulled retrieve the number of columns based on comma count
            l_sec := 'Retrieve the number of columns based on comma count';

            IF i = 1
            THEN
               SELECT REGEXP_COUNT (
                         REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                  CHR (13),
                                  NULL),
                         ',')
                 INTO l_comma_count
                 FROM DUAL;

               write_log ('l_comma_count: ' || l_comma_count);
               l_comma_count := l_comma_count + 1;
               write_log (' ');
            END IF;

            j := 1;

            FOR j IN 1 .. l_comma_count
            LOOP
               -- Parsing Column Data is slightly different for the first column and the remaining cols
               l_sec := 'Parsing Column Data';

               IF j = 1
               THEN
                  write_log (
                        'Line Split#'
                     || j
                     || ': '
                     || SUBSTR (
                           REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                    CHR (13),
                                    NULL),
                           1,
                             INSTR (
                                REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                         CHR (13),
                                         NULL),
                                ',',
                                1,
                                1)
                           - 1));

                  l_data :=
                     SUBSTR (
                        REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                 CHR (13),
                                 NULL),
                        1,
                          INSTR (
                             REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                      CHR (13),
                                      NULL),
                             ',',
                             1,
                             1)
                        - 1);
               ELSIF j = l_comma_count
               THEN
                  write_log (
                        'Line Split#'
                     || j
                     || ': '
                     || SUBSTR (
                           REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                    CHR (13),
                                    NULL),
                             INSTR (
                                REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                         CHR (13),
                                         NULL),
                                ',',
                                1,
                                j - 1)
                           + 1));

                  l_data :=
                     SUBSTR (
                        REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                 CHR (13),
                                 NULL),
                          INSTR (
                             REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                      CHR (13),
                                      NULL),
                             ',',
                             1,
                             j - 1)
                        + 1);
               ELSE
                  write_log (
                        'Line Split#'
                     || j
                     || ': '
                     || SUBSTR (
                           REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                    CHR (13),
                                    NULL),
                             INSTR (
                                REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                         CHR (13),
                                         NULL),
                                ',',
                                1,
                                j - 1)
                           + 1,
                             (INSTR (
                                 REPLACE (
                                    UTL_RAW.cast_to_varchar2 (l_buffer),
                                    CHR (13),
                                    NULL),
                                 ',',
                                 1,
                                 j))
                           - (  INSTR (
                                   REPLACE (
                                      UTL_RAW.cast_to_varchar2 (l_buffer),
                                      CHR (13),
                                      NULL),
                                   ',',
                                   1,
                                   j - 1)
                              + 1)));

                  l_data :=
                     SUBSTR (
                        REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                 CHR (13),
                                 NULL),
                          INSTR (
                             REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                      CHR (13),
                                      NULL),
                             ',',
                             1,
                             j - 1)
                        + 1,
                          (INSTR (
                              REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                       CHR (13),
                                       NULL),
                              ',',
                              1,
                              j))
                        - (  INSTR (
                                REPLACE (UTL_RAW.cast_to_varchar2 (l_buffer),
                                         CHR (13),
                                         NULL),
                                ',',
                                1,
                                j - 1)
                           + 1));
               END IF;

               IF i > 1
               THEN
                  IF j = 1
                  THEN
                     l_price_modifier_tbl_type (l_line).NAME := TRIM (l_data);
                  ELSIF j = 2
                  THEN
                     l_price_modifier_tbl_type (l_line).ACTIVE_FLAG :=
                        TRIM (l_data);
                  -- Version# 8.2 > Start
                  ELSIF j = 3
                  THEN
                     l_price_modifier_tbl_type (l_line).START_DATE_ACTIVE :=
                        TRIM (l_data);
                  ELSIF j = 4
                  THEN
                     l_price_modifier_tbl_type (l_line).END_DATE_ACTIVE :=
                        TRIM (l_data);
                  ELSE
                     NULL;
                  END IF;
               END IF;
            END LOOP;

            l_price_modifier_tbl_type (l_line).FILE_ID := l_file_id;
            l_price_modifier_tbl_type (l_line).OPERATION := 'UPDATE';


            l_price_modifier_tbl_type (l_line).CREATION_DATE := SYSDATE;
            l_price_modifier_tbl_type (l_line).CREATED_BY :=
               fnd_global.user_id;
            l_price_modifier_tbl_type (l_line).LAST_UPDATE_DATE := SYSDATE;
            l_price_modifier_tbl_type (l_line).LAST_UPDATED_BY :=
               fnd_global.user_id;

            l_price_modifier_tbl_type (l_line).REQUEST_ID := l_conc_req_id;
            l_price_modifier_tbl_type (l_line).STATUS := 'NEW';

            -----------------------------------------------------------------------------------------------------
            -- Validations
            -----------------------------------------------------------------------------------------------------
            l_sec := 'File data Validations';


            IF l_price_modifier_tbl_type (l_line).NAME IS NULL
            THEN
               write_output ('MODIFIER_NAME is empty for Record# - ' || i);
               RAISE l_prog_error;
            END IF;
         EXCEPTION
            WHEN l_prog_error
            THEN
               NULL;
         END;

         l_line := l_line + 1; -- This will be initialized per file and increase as it reads throught the LOB
         l_offset := l_offset + l_amount + 1; -- Increasing offset for read. Starting position of next read
         i := i + 1;                                    -- Rowcounter increase
         write_log (' ');
      END LOOP;

      DBMS_LOB.close (l_file);

      write_output (
         'LOB file read and parsing complete. Starting bulk insert into staging...');
      l_sec := 'Starting bulk insert into staging';

      BEGIN
         FORALL x
             IN l_price_modifier_tbl_type.FIRST ..
                l_price_modifier_tbl_type.LAST
            INSERT INTO xxwc.xxwc_qp_prc_modifiers_stg_tbl
                 VALUES l_price_modifier_tbl_type (x);
      EXCEPTION
         WHEN OTHERS
         THEN
            write_log ('Error writing into staging table...' || SQLERRM);
            write_output ('Error writing into staging table...' || SQLERRM);
      END;

      COMMIT;

      write_output ('Bulk insert to staging complete.');

      l_end_time := DBMS_UTILITY.get_time;
      write_log ('l_end_time : ' || l_end_time);
      write_log (
            'File read/parsing processing time: '
         || TO_CHAR (l_end_time - l_start_time));
      write_output (
            'File read/parsing processing time: '
         || TO_CHAR (l_end_time - l_start_time));

      COMMIT;

      XXWC_OM_CUST_MATRIX_PRICE_PKG.xxwc_qualifiers_update (l_errmsg,
                                                            l_retcode,
                                                            p_file_id);

      x_retcode := l_retcode;
      write_log ('Child process completed l_ret_code' || x_retcode);

      write_output ('Completed data processing and load to EBS');
      l_end_time := DBMS_UTILITY.get_time;
      write_log ('l_end_time : ' || l_end_time);
      write_log (
         'QP Processing Time: ' || TO_CHAR (l_end_time - l_start_time));
      write_output (
         'QP Processing Time: ' || TO_CHAR (l_end_time - l_start_time));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         BEGIN
            is_file_open := NULL;
            is_file_open := DBMS_LOB.ISOPEN (l_file);

            IF is_file_open = 1
            THEN
               DBMS_LOB.close (l_file);
               COMMIT;
            END IF;
         END;

         l_msg_data := SUBSTR (SQLERRM, 1, 4000);
         Write_log (
            'load_seg_modifier_hdr. No Data Found Excep: ' || l_msg_data);
         Write_log ('Line#: ' || DBMS_UTILITY.format_error_backtrace ());
         x_errbuf := l_msg_data;
         x_retcode := '2';
      WHEN OTHERS
      THEN
         BEGIN
            is_file_open := NULL;
            is_file_open := DBMS_LOB.ISOPEN (l_file);

            IF is_file_open = 1
            THEN
               DBMS_LOB.close (l_file);
               COMMIT;
            END IF;
         END;

         l_error_message2 :=
               'load_seg_modifier_hdr '
            || 'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => 'XXWC_QP_NTL_PRL_MAINT_TOOL',
            p_request_id          => l_conc_req_id,
            p_ora_error_msg       => l_error_message2,
            p_error_desc          => 'Error running XXWC QP Maintenance Tool Processor',
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');

         write_log (
            'Error in main load_seg_modifier_hdr: ' || l_error_message2);

         x_errbuf := l_error_message2;
         x_retcode := '2';
   END xxwc_qp_ntl_prl_maint_tool;


   PROCEDURE write_log (p_message IN VARCHAR2)
   IS
   BEGIN
      DBMS_OUTPUT.put_line (p_message);
      fnd_file.put_line (fnd_file.LOG, p_message);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END write_log;

   PROCEDURE write_output (p_message IN VARCHAR2)
   IS
   BEGIN
      DBMS_OUTPUT.put_line (p_message);
      fnd_file.put_line (fnd_file.output, p_message);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END write_output;


   PROCEDURE xxwc_qualifiers_update (x_errbuf       OUT VARCHAR2,
                                     x_retcode      OUT VARCHAR2,
                                     p_file_id   IN     NUMBER)
   IS
      l_control_rec               QP_GLOBALS.Control_Rec_Type;
      l_x_return_status           VARCHAR2 (1);

      l_MODIFIER_LIST_rec         QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec     QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl             QP_Modifiers_PUB.Modifiers_Tbl_Type;
      n_MODIFIERS_tbl             QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl         QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl            QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl        QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl          QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      n_PRICING_ATTR_tbl          QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl      QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

      l_x_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_x_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_x_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_x_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_x_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_x_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_x_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_x_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

      ld_end_date                 DATE;
      ld_start_date               DATE;
      l_x_msg_count               VARCHAR (100);
      l_x_msg_data                VARCHAR2 (2000);
      l_msg                       VARCHAR2 (2000);
      l_error_message             VARCHAR2 (2000);
      lvc_sec                     VARCHAR2 (100);
      lvc_procedure               VARCHAR2 (100) := 'XXWC_QUALIFIERS_UPDATE';

      CURSOR CUR_MOD
      IS
         SELECT stg.ROWID row_id,
                stg.name,
                qh.list_header_id,
                UPPER (stg.active_flag) active_flag,
                stg.start_date_active,
                stg.end_date_active
           FROM XXWC.XXWC_QP_PRC_MODIFIERS_STG_TBL STG,
                APPS.QP_LIST_HEADERS QH
          WHERE     TRIM (QH.NAME) = TRIM (STG.NAME)
                AND STG.STATUS = 'NEW'
                AND stg.FILE_ID = p_file_id;
   BEGIN
      lvc_sec := 'Procedure Begin';

      FOR REC_MOD IN CUR_MOD
      LOOP
         lvc_sec := ' Loop Start ' || rec_mod.name;
         ld_end_date := NULL;
         ld_start_date := NULL;

         IF REC_MOD.ACTIVE_FLAG NOT IN ('N', 'Y')
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  REC_MOD.NAME
               || ' has invalid active flag in data file'
               || ' '
               || REC_MOD.ACTIVE_FLAG);
            x_retcode := 1;
            CONTINUE;
         END IF;

         IF REC_MOD.ACTIVE_FLAG = 'Y'
         THEN
            IF rec_mod.start_date_active IS NOT NULL
            THEN
               ld_start_date := rec_mod.start_date_active;
               l_modifier_list_rec.start_date_active := ld_start_date;
            ELSE
               ld_start_date := SYSDATE;
               l_modifier_list_rec.start_date_active := ld_start_date;
            END IF;

            IF rec_mod.end_date_active IS NOT NULL
            THEN
               ld_end_date := rec_mod.end_date_active;
               l_modifier_list_rec.end_date_active := ld_end_date;
            ELSE
               ld_end_date := NULL;
               l_modifier_list_rec.end_date_active := ld_end_date;
            END IF;
         END IF;

         IF REC_MOD.ACTIVE_FLAG = 'N'
         THEN
            ld_end_date := SYSDATE;
            l_modifier_list_rec.end_date_active := ld_end_date;
         END IF;

         l_modifier_list_rec.list_header_id := rec_mod.list_header_id;
         l_modifier_list_rec.active_flag := rec_mod.active_flag;
         l_modifier_list_rec.operation := qp_globals.g_opr_update;



         write_log ('list_header_id ' || l_modifier_list_rec.list_header_id);

         write_log ('active_flag ' || l_modifier_list_rec.active_flag);

         write_log (
            'start_date_active ' || l_modifier_list_rec.start_date_active);

         write_log (
            'end_date_active ' || l_modifier_list_rec.end_date_active);

         lvc_sec := ' API Calling List header_id ' || rec_mod.list_header_id;

         /*  API Call */
         QP_Modifiers_PUB.Process_Modifiers (
            p_api_version_number      => 1.0,
            p_init_msg_list           => FND_API.G_FALSE,
            p_return_values           => FND_API.G_FALSE,
            p_commit                  => FND_API.G_FALSE,
            x_return_status           => l_x_return_status,
            x_msg_count               => l_x_msg_count,
            x_msg_data                => l_x_msg_data,
            p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec,
            p_MODIFIERS_tbl           => l_MODIFIERS_tbl,
            p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl,
            p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl,
            x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec,
            x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec,
            x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl,
            x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl,
            x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl,
            x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl,
            x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl,
            x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);

         lvc_sec :=
               'API processed for List header_id '
            || rec_mod.list_header_id
            || ' Return status '
            || l_x_return_status;


         IF l_x_return_status = 'S'
         THEN
            UPDATE XXWC.XXWC_QP_PRC_MODIFIERS_STG_TBL STG
               SET STATUS = 'PROCESSED'
             WHERE ROWID = rec_mod.row_id;
         ELSE
            UPDATE XXWC.XXWC_QP_PRC_MODIFIERS_STG_TBL STG
               SET STATUS = 'ERROR'
             WHERE ROWID = rec_mod.row_id;
         END IF;


         write_log ('l_x_return_status ' || l_x_return_status);
         write_log ('l_x_msg_count ' || l_x_msg_count);
         write_log ('l_x_msg_data ' || l_x_msg_data);



         IF l_x_msg_count > 0
         THEN
            FOR i IN 1 .. l_x_msg_count
            LOOP
               l_msg :=
                     l_msg
                  || SUBSTR (
                        fnd_msg_pub.get (p_msg_index   => i,
                                         p_encoded     => fnd_api.g_false),
                        1,
                        255);
            END LOOP;
         END IF;

         write_log ('l_msg_data ' || l_msg);

         COMMIT;
      END LOOP;

      lvc_sec := 'Procedure End';
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_message :=
               'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => lvc_sec,
            p_request_id          => FND_GLOBAL.CONC_REQUEST_ID,
            p_ora_error_msg       => l_error_message,
            p_error_desc          => 'Error Occured ',
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');

         write_log ('Error occured  ' || lvc_sec || ' ' || l_error_message);

         x_errbuf := l_error_message;
         x_retcode := '2';
   END;
END XXWC_OM_CUST_MATRIX_PRICE_PKG;
/