--
-- XXWC_MRPRPROP_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.xxwc_mrprprop_pkg
AS
   /******************************************************************************
      NAME:       xxwc_mrprprop_pkg

      PURPOSE:    Update Min and Max Levels after Reorder Point Planning and truncate staging table

      Logic:     1) Reorder Point Planning Report will insert records into xxwc_mrprprop that holds min and max quantities
                 2) Process Records on xxwc_mrprprop
                 3) If user select truncate temp table, the purge the records

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        29-Dec-11   Lee Spitzer       1. Created this trigger.
      2.0        11/03/2014  Vijaysrinivasan        TMS#20141002-00048  Multi org changes 
   ******************************************************************************/

   G_EXCEPTION   EXCEPTION;
   G_MESSAGE     VARCHAR2 (2000);
   G_NAME        VARCHAR2 (30);


   PROCEDURE update_items_ccr (ERRBUF          OUT NOCOPY VARCHAR2
                              ,RETCODE         OUT        NUMBER
                              ,P_BATCH_ID   IN            NUMBER
                              ,P_USER_ID    IN            NUMBER
                              ,P_PURGE      IN            NUMBER)
   IS
      x_return_status   NUMBER;
   BEGIN
      G_NAME := 'UPDATE_ITEMS_CCR';

      fnd_file.put_line (fnd_file.LOG, G_NAME || ' Starting program');
      fnd_file.put_line (fnd_file.LOG
                        ,G_NAME || ' --------------------------------');
      fnd_file.put_line (fnd_file.LOG, G_NAME || ' Parameters');
      fnd_file.put_line (fnd_file.LOG
                        ,G_NAME || ' P_BATCH_ID := ' || P_BATCH_ID);
      fnd_file.put_line (fnd_file.LOG
                        ,G_NAME || ' P_USER_ID  := ' || P_USER_ID);
      fnd_file.put_line (fnd_file.LOG
                        ,G_NAME || ' P_PURGE    := ' || P_PURGE);
      fnd_file.put_line (fnd_file.LOG
                        ,G_NAME || ' --------------------------------');



      fnd_file.put_line (fnd_file.LOG
                        ,G_NAME || ' Calling Procedure UPDATE_ITEMS');

      update_items (p_batch_id        => p_batch_id
                   ,p_user_id         => p_user_id
                   ,p_return_status   => x_return_status);

      fnd_file.put_line (fnd_file.LOG
                        ,G_NAME || ' P_RETURN_STATUS ' || x_return_status);


      --if there is an exception in update_items, error the concurrent request

      IF x_return_status != 0
      THEN
         retcode := x_return_status;
      END IF;

      --If the user passes the parameter to purge the custom table
      IF p_purge = 1
      THEN
         fnd_file.put_line (fnd_file.LOG
                           ,G_NAME || ' Calling Procedure PURGE_TABLE');

         purge_table (p_batch_id        => p_batch_id
                     ,p_return_status   => x_return_status);

         fnd_file.put_line (fnd_file.LOG
                           ,G_NAME || ' P_RETURN_STATUS ' || x_return_status);

         IF x_return_status != 0
         THEN
            retcode := x_return_status;
         END IF;
      END IF;


      fnd_file.put_line (fnd_file.LOG, G_NAME || ' End Concurrent Request');

      retcode := 0;
   END update_items_ccr;

   PROCEDURE update_items (P_BATCH_ID        IN     NUMBER
                          ,P_USER_ID         IN     NUMBER
                          ,P_RETURN_STATUS      OUT NUMBER)
   IS
      --ln_user_id                  NUMBER;
      ln_resp_id            NUMBER;
      ln_resp_appl_id       NUMBER;
      l_item_rec            inv_item_grp.item_rec_type;
      lo_item_rec           inv_item_grp.item_rec_type;
      x_error_tbl           inv_item_grp.error_tbl_type;
      x_message_list        error_handler.error_tbl_type;
      x_return_status       VARCHAR2 (2);
      x_msg_count           NUMBER := 0;
      x_msg_data            VARCHAR2 (2000);
      lv_item_errors        VARCHAR2 (4000);
      l_exception           EXCEPTION;
      l_message             VARCHAR2 (2000);
      ln_uom_code           VARCHAR2 (3);
      l_copy_from_item_id   NUMBER;
      l_inventory_item_id   NUMBER;
      v_organization_id     NUMBER;
      l_cogs_account_id     NUMBER;
      l_sales_account_id    NUMBER;
      l_count               NUMBER;
      l_item_number         VARCHAR2 (40);
      l_organization_code   VARCHAR2 (3);

      --Added for Exception Handling--
      g_user_id             NUMBER := fnd_global.user_id;
      g_login_id            NUMBER := fnd_profile.VALUE ('LOGIN_ID');
      l_err_callfrom        VARCHAR2 (75)
                               DEFAULT 'XXWC_RCV_UPDATE_LIST_PRICE';
      l_err_callpoint       VARCHAR2 (75) DEFAULT 'START';
      l_distro_list         VARCHAR2 (75)
                               DEFAULT 'HDSOracleDevelopers@hdsupply.com';

      --Cursor to query the table

      CURSOR c_items
      IS
         SELECT ROWID
               ,organization_id
               ,inventory_item_id
               ,min_minmax_quantity
               ,max_minmax_quantity
        --enabled the below synonym with rls policy for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
           FROM xxwc_mrprprop
          WHERE batch_id = p_batch_id AND process_flag = 1; --unprocessed_records
   BEGIN
      G_NAME := 'UPDATE_ITEMS';

      BEGIN                                                       --Debug only
         --get the responsibility_id and application_id from the Inventory responsibility to initialize
         SELECT responsibility_id, application_id
           INTO ln_resp_id, ln_resp_appl_id
           FROM fnd_responsibility_vl
          WHERE responsibility_name = 'Inventory';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            g_message :=
               G_NAME || 'Can not find responsibility id or application_id';
            --Raise Exception to not execute program anymore
            RAISE g_exception;
         WHEN OTHERS
         THEN
            g_message :=
                  G_NAME
               || 'Other exception getting respoinsibility id'
               || SQLCODE
               || SQLERRM;
            --Raise Exception to not execute program anymore
            RAISE g_exception;
      END;


      --Initial based on User Name
      fnd_global.apps_initialize (p_user_id, ln_resp_id, ln_resp_appl_id);


      --Loop through cursor and call the api to update items

      fnd_file.put_line (
         fnd_file.LOG
        ,'ITEM NUMBER  | ORG_CODE | MIN_MINMAX_QUANTITY | MAX_MINMAX_QUANTITY | RETURN_STATUS | MESSAGE');

      FOR r_items IN c_items
      LOOP
         BEGIN
            SELECT msib.segment1, mp.organization_code
              INTO l_item_number, l_organization_code
              FROM mtl_system_items_b msib, mtl_parameters mp
             WHERE     msib.inventory_item_id = r_items.inventory_item_id
                   AND msib.organization_id = r_items.organization_id
                   AND msib.organization_id = mp.organization_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_message :=
                     G_NAME
                  || ' Could not find item number '
                  || SQLCODE
                  || SQLERRM;
               RAISE g_exception;
         END;


         BEGIN
            UPDATE mtl_system_items_b
               SET min_minmax_quantity = r_items.min_minmax_quantity
                  ,max_minmax_quantity = r_items.max_minmax_quantity
                  ,last_updated_by = p_user_id
                  ,last_update_date = SYSDATE
                  ,program_update_date = SYSDATE
             WHERE     r_items.inventory_item_id = inventory_item_id
                   AND r_items.organization_id = organization_id;

            lv_item_errors := NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_return_status := 'E';
               lv_item_errors := SQLCODE || SQLERRM;
         END;


         COMMIT;


         lv_item_errors := 'Success';
         x_return_status := 'S';

         BEGIN                      --Update Custom Table if records processed
        --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
            UPDATE xxwc_mrprprop
               SET process_flag = 2
                  ,return_status = x_return_status
                  ,MESSAGE = g_message
             WHERE     ROWID = r_items.ROWID
                   AND inventory_item_id = r_items.inventory_item_id
                   AND organization_id = r_items.organization_id
                   AND batch_id = p_batch_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               ROLLBACK;
               g_message := G_NAME || SQLCODE || SQLERRM;
               RAISE g_exception;
         END;

         COMMIT;

         fnd_file.put_line (
            fnd_file.LOG
           ,   L_ITEM_NUMBER
            || ' | '
            || L_ORGANIZATION_CODE
            || ' | '
            || r_items.MIN_MINMAX_QUANTITY
            || ' | '
            || r_items.MAX_MINMAX_QUANTITY
            || ' | '
            || x_return_status
            || '|'
            || g_message);
      END LOOP;

      p_return_status := 0;
   EXCEPTION
      WHEN g_exception
      THEN
         --Return a failure to the concurrent request
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_name || '.' || l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => g_message
           ,p_error_desc          =>    'Error running xxwc_mrprprop_pkg update '
                                     || g_name
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'PO');

         p_return_status := 2;
   END update_items;

   PROCEDURE purge_table (P_BATCH_ID IN NUMBER, P_RETURN_STATUS OUT NUMBER)
   IS
      --Added for Exception Handling--
      g_user_id         NUMBER := fnd_global.user_id;
      g_login_id        NUMBER := fnd_profile.VALUE ('LOGIN_ID');
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_RCV_UPDATE_LIST_PRICE';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      G_NAME := 'PURGE_TABLE';

      fnd_file.put_line (fnd_file.LOG
                        ,G_NAME || ' Deleting batch_id ' || p_batch_id);


      BEGIN
        --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
         DELETE xxwc_mrprprop
          WHERE batch_id = p_batch_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            ROLLBACK;
            g_message := G_NAME || SQLCODE || SQLERRM;
            RAISE g_exception;
      END;

      COMMIT;

      p_return_status := 0;
   EXCEPTION
      WHEN g_exception
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_name || '.' || l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => g_message
           ,p_error_desc          =>    'Error deleting records on xxwc_mrprprop_pkg delete '
                                     || g_name
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'PO');

         p_return_status := 2;
   END purge_table;
END xxwc_mrprprop_pkg;
/
