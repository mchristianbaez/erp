CREATE OR REPLACE PACKAGE BODY APPS.XXWC_HZ_OAF_INFO_PKG AS

   /* ************************************************************************************************
   *   $Header XXWC_HZ_OAF_INFO_PKG $                                                               *
   *   Module Name: XXWC_HZ_OAF_INFO_PKG                                                            *
   *                                                                                                *
   *   PURPOSE:   This package is used by the Customer Account Site OAF page                        *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        01/14/2015  Lee Spitzer                Initial Version 20121217-00636             *
   *                                                         OAF Customer Page Extension            *
   *  1.1         06/16/2015   Bala Seshadri              Add error email api to few SQL blocks     *
   *  1.2         06/29/2015  Maharajan Shunmugam     TMS#20150618-00219 error alert on customer    *
   *						      site use update                               *
   * *********************************************************************************************** */
  
   --Error Handling Global Parameters--
    g_err_msg         VARCHAR2 (2000);
    g_err_callfrom    VARCHAR2 (175) := 'XXWC_HZ_OAF_INFO_PKG';
    g_err_callpoint   VARCHAR2 (175) := 'START';
    g_distro_list     VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com'; --Updated to use new Distribution List
    g_module          VARCHAR2 (80) := 'WC AR';
    g_message         VARCHAR2 (1000);
    g_sqlcode         VARCHAR2 (1000);
    g_sqlerrm         VARCHAR2 (1000);
  
  /*************************************************************************************************   
   *   PROCEDURE party_site_update                                                                  *
   *   Purpose : Procedure is a concurrent program to call the hz_party_site update api             *
   *                  The trigger XXWC_HZ_CUST_SITE_USES_BU will fire when there is a location      *
   *                      update on HZ_CUST_SITE_USES_ALL table and pass the party site id to the   *
   *                        procedure to execute.  No data on hz party site changes but will spawn  *
   *                        the dqm sync concurrent request to populate the HZ_STAGED_PARTY_SITE.   *
   *                        The object version will increment on HZ_CUST_SITES_USES after submitting* 
   *                        this request.                                                           *
   *   Parameters:                                                                                  *
   *      In :    p_party_site_id  -- party_site_id from HZ_PARTY_SITES                             *
   *      OUT :   ERRBUF   VARCHAR2 --Standard Oracle call for concurrent request procedures        *
   *              RETCODE OUT VARCHAR2 --Standrd Oracle call for concurrent reuqest procedures      *
   *                                                                                                *
   *      PROCEDURE party_site_update ( errbuf               OUT VARCHAR2                           *
   *                                  , retcode              OUT VARCHAR2                           *
   *                                  , p_party_site_id      IN NUMBER);                            *
   *                                                                                                *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        01/21/2015  Lee Spitzer                Initial Version 20121217-00636             *
   *                                                         OAF Customer Page DQM Extension        *
   *  1.1        06/16/2015   Bala Seshadri              Add error email api to few SQL blocks      *
   *  1.2        06/29/2015  Maharajan Shunmugam     TMS#20150618-00219 error alert on customer    *
   *						      site use update                               *
   *************************************************************************************************/
   
  PROCEDURE party_site_update ( errbuf               OUT VARCHAR2
                              , retcode              OUT VARCHAR2
                              , p_party_site_id      IN NUMBER) 
  IS
  
  
  l_party_site_rec_type HZ_PARTY_SITE_V2PUB.party_site_rec_type;
  l_obj_num        NUMBER;
  l_return_status  VARCHAR2(1);
  l_msg_count      NUMBER;
  l_msg_data       VARCHAR2(2000);
  l_party_site_id   NUMBER;
  l_exception       EXCEPTION;
  l_return          NUMBER;
  
  BEGIN
 -- g_err_callfrom := g_err_callfrom||'.primary_salesrep_exist';   --commented for ver#1.2
  g_err_callpoint := '101, getting l_party_site_id';
  
  BEGIN
    SELECT hps.party_site_id
    INTO   l_party_site_id
    FROM   hz_party_sites hps
    WHERE  hps.party_site_id = p_party_site_id;
  EXCEPTION
    WHEN OTHERS THEN
      g_sqlcode := SQLCODE;
      g_sqlerrm := SQLERRM;
      g_message := 'Error getting party_site_id ' || g_sqlcode || g_sqlerrm;
      RAISE L_EXCEPTION;
  END;

  l_party_site_rec_type.party_site_id  := l_party_site_id;
  
  g_err_callpoint := '102, getting l_obj_num';
 
  BEGIN
    SELECT nvl(object_version_number,1)
    INTO   l_obj_num
    FROM   hz_party_sites
    WHERE  party_site_id = l_party_site_rec_type.party_site_id;
  EXCEPTION
    WHEN OTHERS THEN
      g_sqlcode := SQLCODE;
      g_sqlerrm := SQLERRM;
      g_message := 'Error getting object number ' || g_sqlcode || g_sqlerrm;
      RAISE l_exception;
  END;
 
  g_err_callpoint := '103, starting  hz_party_site_v2pub.update_party_site API';
  
  BEGIN -- Ver 1.1
        hz_party_site_v2pub.update_party_site
        ( p_init_msg_list         =>  FND_API.G_TRUE
        , p_party_site_rec        =>  l_party_site_rec_type
        , p_object_version_number => l_obj_num
        , x_return_status         => l_return_status
        , x_msg_count             => l_msg_count
        , x_msg_data              => l_msg_data
      ) ;
  EXCEPTION  -- Ver 1.1
   WHEN OTHERS THEN -- Ver 1.1
    g_message := substr('API error, '||SQLCODE || ' ' || SQLERRM, 1,1000) ;  -- Ver 1.1
    --NULL; -- Ver 1.1 
    RAISE L_EXCEPTION;   --Added for ver#1.2
  END;
  
  --g_err_callpoint := substr('104, p_party_site_id ='||p_party_site_id||',l_return_status ='||l_return_status||', l_msg_data'||l_msg_data, 1 , 175); -- Ver 1.1 --commented for ver#1.2
  
  if l_return_status = 'E' THEN
  
      l_return := 1;
      
  elsif l_return_status = 'S' THEN
    
      l_return := 0;
      
  ELSE
  
      l_return := 2;
      
  end if;
  
    retcode := l_return;
  
  EXCEPTION
   -- Begin Ver 1.1
   WHEN L_EXCEPTION THEN
     l_return := 2;
     
     --g_message := substr(SQLCODE || ' ' || SQLERRM,1,1000) ;  -- Ver 1.1
      
        xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => g_message
               ,p_error_desc          => 'WHEN L_EXCEPTION in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

  
      retcode := l_return;
    -- End Ver 1.1      
    WHEN others THEN
      
     l_return := 2;
     
     g_message := substr(SQLCODE || ' ' || SQLERRM,1,1000) ; -- Ver 1.1   --Uncommented for ver# 1.2
      
        xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => g_err_callfrom
               ,p_calling             => g_err_callpoint
               ,p_ora_error_msg       => g_message
               ,p_error_desc          => 'WHEN OTHERS in call ' || g_err_callfrom || g_err_callpoint
               ,p_distribution_list   => g_distro_list
               ,p_module              => g_module);

  
      retcode := l_return;
  
  END party_site_update;
  
END;
/