CREATE OR REPLACE PACKAGE BODY APPS."XXCUSCE_CUSTOM_PKG" IS

  -- Function Is_Master checks to see if the account passed
  -- to it is one of the 3 master accounts
  FUNCTION is_master(v_account_num VARCHAR2) RETURN VARCHAR2 IS
    l_bank_account_id NUMBER;
    x_sub_account     VARCHAR2(1);
  
  BEGIN
    BEGIN
      SELECT 'T'
        INTO x_sub_account
        FROM dual
       WHERE (SELECT a.bank_account_id
                FROM ce.ce_bank_accounts a
               WHERE a.bank_account_num = v_account_num
                 AND a.account_classification = 'INTERNAL') IN
             (SELECT b.conc_account_id
                FROM ce.ce_cashpools b);
    EXCEPTION
      WHEN no_data_found THEN
        x_sub_account := 'F';
    END;
    RETURN x_sub_account;
  END is_master;

  -- Function get_cashflow_b gets the cashflow balance for the day passed into it
  -- for the account passed into it.  If used in combination
  -- with the previous day function will return previous day cashflow balance.
  FUNCTION get_cashflow_b(v_date            DATE
                         ,v_bank_account_id NUMBER) RETURN NUMBER IS
  
    v_float NUMBER;
  
  BEGIN
    BEGIN
      SELECT nvl(hdr.cashflow_balance, 0)
        INTO v_float
        FROM ce.ce_statement_headers hdr
       WHERE hdr.bank_account_id = v_bank_account_id
         AND trunc(hdr.statement_date) = trunc(v_date);
    EXCEPTION
      WHEN no_data_found THEN
        v_float := 0;
    END;
    RETURN v_float;
  
  END get_cashflow_b;





  -- Function get_float gets the float for the day passed into it
  -- for the account passed into it.  If used in combination
  -- with the previous day function will return previous day float.
  FUNCTION get_float(v_date            DATE
                    ,v_bank_account_id NUMBER) RETURN NUMBER IS
  
    v_float NUMBER;
  
  BEGIN
    BEGIN
      SELECT nvl(hdr.one_day_float, 0)
        INTO v_float
        FROM ce.ce_statement_headers hdr
       WHERE hdr.bank_account_id = v_bank_account_id
         AND trunc(hdr.statement_date) = trunc(v_date);
    EXCEPTION
      WHEN no_data_found THEN
        v_float := 0;
    END;
    RETURN v_float;
  
  END get_float;

  -- Function get_ach gets the transfer amount for the table IAC for
  -- a date other than the one specified by the view.  This is used
  -- to deliver two sets of values for transfer amounts for the account
  -- Used to retrieve a previous day's transfers.
  FUNCTION get_prev_ach(v_date         DATE
                       ,v_bank_account VARCHAR2) RETURN NUMBER IS
  
    v_ach NUMBER;
  
  BEGIN
    BEGIN
      SELECT nvl(xtr.transfer_amount, 0) * -1
        INTO v_ach
        FROM xtr.xtr_interacct_transfers xtr
       WHERE (xtr.account_no_from = v_bank_account OR
             xtr.account_no_to = v_bank_account)
         AND xtr.product_type = 'ACH'
         AND trunc(xtr.transfer_date) = trunc(v_date);
    EXCEPTION
      WHEN no_data_found THEN
        v_ach := 0;
    END;
    RETURN v_ach;
  
  END get_prev_ach;

  -- Function get_ach gets the transfer amount for the table IAC for
  -- a date other than the one specified by the view.  This is used
  -- to deliver two sets of values for transfer amounts for the account
  -- Used to retrieve a previous day's transfers.
  FUNCTION get_curr_ach(v_date         DATE
                       ,v_bank_account VARCHAR2) RETURN NUMBER IS
  
    v_ach NUMBER;
  
  BEGIN
    BEGIN
      SELECT nvl(xtr.transfer_amount, 0) * -1
        INTO v_ach
        FROM xtr.xtr_interacct_transfers xtr
       WHERE (xtr.account_no_from = v_bank_account OR
             xtr.account_no_to = v_bank_account)
         AND xtr.product_type = 'ACH'
         AND trunc(xtr.deal_date) = trunc(v_date);
    EXCEPTION
      WHEN no_data_found THEN
        v_ach := 0;
    END;
    RETURN v_ach;
  
  END get_curr_ach;


  -- Function get_end_balance is used to retrieve the end balance used
  -- usually by an opening balance formula.
  FUNCTION get_end_balance(v_date            DATE
                          ,v_bank_account_id NUMBER) RETURN NUMBER IS
  
    v_end_balance NUMBER;
  
  BEGIN
    BEGIN
      SELECT nvl(hdr.control_end_balance, 0)
        INTO v_end_balance
        FROM ce.ce_statement_headers hdr
       WHERE hdr.bank_account_id = v_bank_account_id
         AND trunc(hdr.statement_date) = trunc(v_date);
    EXCEPTION
      WHEN no_data_found THEN
        v_end_balance := 0;
    END;
    RETURN v_end_balance;
  
  END get_end_balance;


  -- Function intra_get_end_balance is used to retrieve the end balance used
  -- usually be an opening balance formula.  This function is driven from
  -- the intra_day table.
  FUNCTION intra_get_balance(v_date            DATE
                            ,v_bank_account_id NUMBER) RETURN NUMBER IS
  
    v_inter_zba    NUMBER;
    v_intra_bal    NUMBER;
    v_return_value NUMBER;
  
  BEGIN
    BEGIN
      SELECT SUM(decode(line.trx_type, 'DEBIT', line.amount * -1,
                        line.amount))
        INTO v_intra_bal
        FROM ce.ce_intra_stmt_lines   line
            ,ce.ce_intra_stmt_headers head
       WHERE 1 = 1
         AND line.statement_header_id = head.statement_header_id
         AND line.trx_date = trunc(v_date)
         AND head.bank_account_id = v_bank_account_id;
    EXCEPTION
      WHEN no_data_found THEN
        v_intra_bal := 0;
    END;
  
    BEGIN
      SELECT SUM(nvl(decode(xit.account_no_from, bank.account_number,
                            xit.transfer_amount * -1,
                            decode(xxcusce_custom_pkg.is_master(xit.account_no_from),
                                    'T',
                                    decode(xit.account_no_to,
                                            bank.account_number,
                                            xit.transfer_amount,
                                            xit.transfer_amount * -1),
                                    xit.transfer_amount)), 0)) sub_iac_transfer_amt
        INTO v_inter_zba
        FROM xtr.xtr_interacct_transfers xit
            ,xtr.xtr_bank_accounts       bank
       WHERE 1 = 1
         AND xit.product_type = 'ZBA'
         AND bank.ap_bank_account_id = v_bank_account_id
         AND (xit.account_no_from = bank.account_number OR
             xit.account_no_to = bank.account_number)
         AND transfer_date = v_date;
    EXCEPTION
      WHEN no_data_found THEN
        v_inter_zba := 0;
    END;
  
    v_return_value := nvl(v_inter_zba, 0) + nvl(v_intra_bal, 0);
    RETURN v_return_value;
  
  END intra_get_balance;


  -- Function get_net_IAC is used to retrieve the net IAC's

  FUNCTION get_net_iac(v_date            DATE
                      ,v_bank_account_id NUMBER) RETURN NUMBER IS
  
    v_net_iac NUMBER;
  
  BEGIN
    BEGIN
      SELECT nvl(SUM(decode(xit.account_no_from, xba.account_number,
                            -1 * xit.transfer_amount, xit.transfer_amount)),
                 0)
        INTO v_net_iac
        FROM xtr.xtr_interacct_transfers xit
            ,ce.ce_statement_headers csh
            ,xtr_bank_accounts xba
            ,(SELECT trunc(SYSDATE + 1) - rownum dates
                FROM all_objects
               WHERE rownum < 60) dta
       WHERE 1 = 1
         AND csh.bank_account_id(+) = xba.ap_bank_account_id
         AND xba.attribute3 = 'Y'
         AND xba.authorised = 'Y'
         AND csh.statement_date =
             xxcusce_custom_pkg.get_prev_business_day(dta.dates)
         AND trunc(xit.transfer_date) = dta.dates
         AND (xit.account_no_from = xba.account_number OR
             xit.account_no_to = xba.account_number)
         AND dta.dates = trunc(v_date)
         AND csh.bank_account_id = v_bank_account_id
       GROUP BY xba.ap_bank_account_id
               ,csh.statement_header_id
               ,csh.statement_date;
    EXCEPTION
      WHEN no_data_found THEN
        v_net_iac := 0;
    END;
    RETURN v_net_iac;
  
  END get_net_iac;


  -- Function sub_account gets 2 account numbers (usually from IAC table)
  -- and returns only the account that is not a master account.
  -- If both are masters
  FUNCTION sub_account(v_account_to   VARCHAR2
                      ,v_account_from VARCHAR2) RETURN VARCHAR2 IS
    v_sub_account VARCHAR2(20);
    tester        VARCHAR2(1);
  BEGIN
  
    --  TEST accounts to see if it is a sub account
  
    BEGIN
    
      SELECT 'x'
        INTO tester
        FROM dual
       WHERE v_account_to IN
             (SELECT xba.account_number
                FROM xtr.xtr_bank_accounts xba
               WHERE ap_bank_account_id IN
                     (SELECT conc_account_id
                        FROM ce.ce_cashpools));
      /*   logic flow - from above, was the to_account a master?
      If so, continue on, if not, go to the exception */
      BEGIN
        SELECT 'x'
          INTO tester
          FROM dual
         WHERE v_account_from IN --from_account question
               (SELECT xba.account_number
                  FROM xtr.xtr_bank_accounts xba
                 WHERE ap_bank_account_id IN
                       (SELECT conc_account_id
                          FROM ce.ce_cashpools));
        /*    We asked if the from_account is also a master.  If not, then
        we know thier is only one master and this from_account is the sub.
        this is handled with the 1st exception                             */
        IF v_account_to = '2079900421666' THEN
          v_sub_account := v_account_from;
        ELSE
          v_sub_account := v_account_to;
        END IF;
      EXCEPTION
        -- from_account question
        WHEN no_data_found THEN
          v_sub_account := v_account_from;
      END;
    
    EXCEPTION
      -- To_account question
      WHEN no_data_found THEN
        v_sub_account := v_account_to;
    END;
  
    --  Return result based on above results
    RETURN v_sub_account;
  END sub_account;


  -- Function Get_next_business_day retreives the next business date
  -- from the date sumbitted (as varchar)
  FUNCTION get_next_business_day(v_date VARCHAR2) RETURN VARCHAR2 IS
  
    v_nxt_bus_day DATE;
  
  BEGIN
  
    SELECT dt
      INTO v_nxt_bus_day
      FROM (SELECT to_date(v_date) + rownum dt
              FROM all_objects)
     WHERE to_char(dt, 'dy') NOT IN ('sat', 'sun')
       AND NOT EXISTS (SELECT NULL
              FROM xtr.xtr_holidays
             WHERE holiday_date = trunc(dt))
       AND rownum = 1;
  
    RETURN v_nxt_bus_day;
  
  END get_next_business_day;

  -- Function get_prev_business_day retreives the previous business date
  -- from the date sumbitted.
  FUNCTION get_prev_business_day(v_date VARCHAR2) RETURN VARCHAR2 IS
  
    v_nxt_bus_day DATE;
  
  BEGIN
  
    SELECT dt
      INTO v_nxt_bus_day
      FROM (SELECT to_date(v_date) - rownum dt
              FROM all_objects)
     WHERE to_char(dt, 'dy') NOT IN ('sat', 'sun')
       AND NOT EXISTS (SELECT NULL
              FROM xtr.xtr_holidays
             WHERE holiday_date = trunc(dt))
       AND rownum = 1;
  
    RETURN v_nxt_bus_day;
  
  END get_prev_business_day;



  PROCEDURE calc_eft_conc(errbuf  OUT VARCHAR2
                         ,retcode OUT NUMBER) IS
  
    CURSOR wire_accts IS
      SELECT xba.account_number
            ,xba.cashpool_min_payment_amt
        FROM xtr.xtr_bank_accounts xba
       WHERE xba.attribute4 = 'WIRE';
  
    CURSOR ach_accts IS
      SELECT xba.account_number
            ,xba.cashpool_min_payment_amt
        FROM xtr.xtr_bank_accounts xba
       WHERE xba.attribute4 = 'ACH';
  
    CURSOR err_email IS
      SELECT xpi.process_short_name
            ,xpi.receipient_on_error
            ,xpi.email_sender
            ,'WIRE/ACH Concentration Calculation Process Has FAILED' subject_hdr
        FROM xxcus.xxcus_process_info xpi
       WHERE xpi.process_short_name = 'HSIXTREFTT';
  
    v_eff_opp_cost  NUMBER;
    v_wire_cost     NUMBER;
    v_ach_cost      NUMBER;
    v_nxt_bus_day   DATE;
    v_day_of_week   VARCHAR2(3);
    v_is_holiday    VARCHAR2(1);
    v_days_accel    NUMBER;
    v_wire_conc     NUMBER;
    v_ach_conc      NUMBER;
    v_old_wire_conc NUMBER;
    v_old_ach_conc  NUMBER;
    v_err_msg       VARCHAR2(2000) := '';
    v_err_code      NUMBER;
    data_error EXCEPTION;
    v_err_mail_msg VARCHAR2(2000) := '';
  
  BEGIN
  
    --Calculate and store all variables
  
    BEGIN
    
      SELECT (xmp.ask_price / 100) --turn number into a percentage
        INTO v_eff_opp_cost
        FROM xtr.xtr_market_prices xmp
       WHERE xmp.ric_code = 'EFT OPP COST';
    
      IF v_eff_opp_cost = 0
         OR v_eff_opp_cost IS NULL THEN
      
        v_err_msg  := v_err_msg ||
                      'EFT OPP COST is equal to zero or is NULL.';
        v_err_code := 2;
        RAISE data_error;
      
      END IF;
    
    EXCEPTION
      WHEN no_data_found THEN
      
        v_err_msg  := v_err_msg ||
                      'No EFT OPP COST value in table XTR_MARKET_PRICES';
        v_err_code := 2;
        fnd_file.put_line(fnd_file.log, v_err_msg);
        RAISE data_error;
      
    END;
  
    BEGIN
    
      SELECT xmp.ask_price
        INTO v_wire_cost
        FROM xtr.xtr_market_prices xmp
       WHERE xmp.ric_code = 'WIRE COST';
    
    EXCEPTION
      WHEN no_data_found THEN
      
        v_err_msg  := v_err_msg ||
                      'No WIRE COST value in table XTR_MARKET_PRICES';
        v_err_code := 2;
        fnd_file.put_line(fnd_file.log, v_err_msg);
        RAISE data_error;
      
    END;
  
    BEGIN
    
      SELECT xmp.ask_price
        INTO v_ach_cost
        FROM xtr.xtr_market_prices xmp
       WHERE xmp.ric_code = 'ACH COST';
    
    EXCEPTION
      WHEN no_data_found THEN
      
        v_err_msg  := v_err_msg ||
                      'No ACH COST value in table XTR_MARKET_PRICES';
        v_err_code := 2;
        fnd_file.put_line(fnd_file.log, v_err_msg);
        RAISE data_error;
      
    END;
  
    --Get next business day logic
    --Get current day's day of week
    SELECT to_char(SYSDATE, 'DY')
      INTO v_day_of_week
      FROM dual;
  
    --Check to see if current day is a FRI.  If so, add 3 days to get next business day
  
    IF v_day_of_week = 'FRI' THEN
      v_nxt_bus_day := trunc(SYSDATE) + 3;
    ELSE
      v_nxt_bus_day := trunc(SYSDATE) + 1;
    END IF;
  
    --Check to see if next business day is a holiday
    BEGIN
    
      SELECT 'T'
        INTO v_is_holiday
        FROM xtr.xtr_holidays h
       WHERE h.holiday_date = trunc(v_nxt_bus_day);
    
    EXCEPTION
      WHEN no_data_found THEN
        v_is_holiday := 'F';
    END;
  
    IF v_is_holiday = 'T' THEN
      --First check to see if the holiday falls on a FRI
      IF to_char(v_nxt_bus_day, 'DY') = 'FRI' THEN
        v_nxt_bus_day := v_nxt_bus_day + 3;
      ELSE
        v_nxt_bus_day := v_nxt_bus_day + 1;
      END IF;
    END IF;
  
    --Calculate Days Accelerated
  
    v_days_accel := trunc(v_nxt_bus_day) - trunc(SYSDATE);
  
    --Calculate Concentration Wire Threshold
  
    v_wire_conc := round(((v_wire_cost - v_ach_cost) /
                         (v_days_accel * (v_eff_opp_cost / 360))), 0);
  
    --Calculate Concentration ACH Threshold
  
    v_ach_conc := round((v_ach_cost /
                        (v_days_accel * (v_eff_opp_cost / 360))), 0);
  
    --Update all bank accounts set as WIRE concentration accounts if
    --the current cashpool_min_payment_amt is not equal to calculated Wire Conc amount
    FOR wire_accts_rec IN wire_accts
    LOOP
    
      --Check to see if cashpool_min_payment_amt needs to be updated
      --      IF wire_accts_rec.cashpool_min_payment_amt <> v_wire_conc THEN
    
      UPDATE xtr.xtr_bank_accounts xba
         SET xba.cashpool_min_payment_amt = v_wire_conc
            ,xba.updated_on               = SYSDATE
            ,xba.updated_by               = 'XXHSI.CALC_EFT_CONC'
       WHERE xba.account_number = wire_accts_rec.account_number;
    
    --      END IF;
    
    END LOOP;
  
    COMMIT;
  
    --Update all bank accounts set as WIRE concentration accounts if
    --the current cashpool_min_payment_amt is not equal to calculated ACH Conc amount
    FOR ach_accts_rec IN ach_accts
    LOOP
    
      --Check to see if cashpool_min_payment_amt needs to be updated
      --      IF ach_accts_rec.cashpool_min_payment_amt <> v_ach_conc THEN
    
      UPDATE xtr.xtr_bank_accounts xba
         SET xba.cashpool_min_payment_amt = v_ach_conc
            ,xba.updated_on               = SYSDATE
            ,xba.updated_by               = 'XXHSI.CALC_EFT_CONC'
       WHERE xba.account_number = ach_accts_rec.account_number;
    
    --      END IF;
    
    END LOOP;
  
    COMMIT;
  
    --Update the XTR_MARKET_PRICES table where WIRE THRESHOLD and/or the ACH THRESHOLD
    --ASK_PRICE/BID_PRICE fields do not match new calculated concentration values
    --If the WIRE THRESHOLD or ACH THRESHOLD lines do not exist, raise an error
  
    BEGIN
    
      SELECT xmp.ask_price
        INTO v_old_wire_conc
        FROM xtr.xtr_market_prices xmp
       WHERE xmp.ric_code = 'WIRE THRESHOLD';
    
    EXCEPTION
      WHEN no_data_found THEN
        v_err_msg  := v_err_msg ||
                      'WIRE THRESHOLD row in XTR_MARKET_PRICES does not exist!';
        v_err_code := 2;
        fnd_file.put_line(fnd_file.log, v_err_msg);
        RAISE data_error;
    END;
  
    IF v_old_wire_conc <> v_wire_conc THEN
    
      UPDATE xtr.xtr_market_prices
         SET ask_price = v_wire_conc
            ,bid_price = v_wire_conc
       WHERE ric_code = 'WIRE THRESHOLD';
    
      COMMIT;
    
    END IF;
  
    BEGIN
    
      SELECT xmp.ask_price
        INTO v_old_ach_conc
        FROM xtr.xtr_market_prices xmp
       WHERE xmp.ric_code = 'ACH THRESHOLD';
    
    EXCEPTION
      WHEN no_data_found THEN
        v_err_msg  := v_err_msg ||
                      'ACH THRESHOLD row in XTR_MARKET_PRICES does not exist!';
        v_err_code := 2;
        fnd_file.put_line(fnd_file.log, v_err_msg);
        RAISE data_error;
    END;
  
    IF v_old_ach_conc <> v_ach_conc THEN
    
      UPDATE xtr.xtr_market_prices
         SET ask_price = v_ach_conc
            ,bid_price = v_ach_conc
       WHERE ric_code = 'ACH THRESHOLD';
    
      COMMIT;
    
    END IF;
  
    --Exception Handler
  EXCEPTION
    WHEN data_error THEN
    
      retcode        := v_err_code;
      errbuf         := v_err_msg;
      v_err_mail_msg := 'The Wire/ACH Concentration process on ' ||
                        trunc(SYSDATE) ||
                        ' has failed for the following reason:  ' ||
                        v_err_msg;
    
      FOR err_email_rec IN err_email
      LOOP
        --Send the email
        xxcus_email.email_notify(err_email_rec.process_short_name, NULL,
                                 err_email_rec.subject_hdr, v_err_mail_msg,
                                 v_err_code, v_err_code);
      END LOOP;
    
    WHEN OTHERS THEN
      v_err_msg      := v_err_msg || ' ERROR' || SQLCODE ||
                        substr(SQLERRM, 1, 200);
      retcode        := v_err_code;
      errbuf         := v_err_msg;
      v_err_mail_msg := 'The Wire/ACH Concentration process on ' ||
                        trunc(SYSDATE) ||
                        ' has failed for the following reason:  ' ||
                        v_err_msg;
    
      FOR err_email_rec IN err_email
      LOOP
        --Send the email
        xxcus_email.email_notify(err_email_rec.process_short_name, NULL,
                                 err_email_rec.subject_hdr, v_err_mail_msg,
                                 v_err_code, v_err_code);
      END LOOP;
  END calc_eft_conc;

  PROCEDURE update_iac_product_type(errbuf  OUT VARCHAR2
                                   ,retcode OUT NUMBER) IS
    v_sub_account VARCHAR2(200);
    v_prod_type   VARCHAR2(200);
    v_count       NUMBER := 0;
    CURSOR iac IS
      SELECT xit.*
        FROM xtr.xtr_interacct_transfers xit
       WHERE 1 = 1
            ----AND TRANSACTION_NUMBER = 14;
         AND trunc(deal_date) = trunc(SYSDATE)
         AND nvl(xit.comments, ' ') NOT IN ('IAC', 'XMP');
  BEGIN
    FOR c_rec IN iac
    LOOP
      BEGIN
        dbms_output.put_line('Values of ACCT_FROM and ACCT_TO and TRANSACTION_NUMBER are ....:' ||
                             c_rec.account_no_from || ' and ' ||
                             c_rec.account_no_to || ' and ' ||
                             c_rec.transaction_number);
        SELECT attribute4
          INTO v_prod_type
          FROM xtr_bank_accounts xba
         WHERE 1 = 1
           AND (xba.account_number = c_rec.account_no_from OR
               xba.account_number = c_rec.account_no_to)
           AND nvl(xba.attribute3, ' ') <> 'Y'
           AND rownum = 1;
      
        dbms_output.put_line('Values of Prod Type...' || v_prod_type);
        dbms_output.put_line('Values of c_rec.transfer_date from ICA table...' ||
                             c_rec.transfer_date);
        IF c_rec.product_type = 'ZBA' THEN
          c_rec.product_type := v_prod_type;
          c_rec.comments     := 'IAC';
        END IF;
      
        IF c_rec.product_type = 'ACH' THEN
          BEGIN
            SELECT xxcusce_custom_pkg.get_next_business_day(to_char(c_rec.transfer_date,
                                                                    'DD-MON-YYYY'))
              INTO c_rec.transfer_date
              FROM dual;
            dbms_output.put_line('Values of c_rec.transfer_date from next business day...' ||
                                 c_rec.transfer_date);
          END;
          c_rec.comments := 'IAC';
        END IF;
        BEGIN
          UPDATE xtr.xtr_interacct_transfers xit
             SET comments      = c_rec.comments
                ,product_type  = c_rec.product_type
                ,transfer_date = c_rec.transfer_date
           WHERE xit.transaction_number = c_rec.transaction_number;
          v_count := v_count + 1;
        EXCEPTION
          WHEN OTHERS THEN
            dbms_output.put_line('Failed to update XTR.XTR_INTERACCT_TRANSFERS table with error...' ||
                                 SQLERRM);
        END;
      END;
    END LOOP;
    COMMIT;
    dbms_output.put_line('Total no.of records updated ...:' || v_count);
  EXCEPTION
    WHEN OTHERS THEN
      dbms_output.put_line('Program failed with error...' || SQLERRM);
  END update_iac_product_type;
END xxcusce_custom_pkg;
/
