CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_PRICING_GUARDRAIL_PKG
AS
  /*****************************************************************************************************************************************************************
    Package   Body : XXWC_OM_PRICING_GUARDRAIL_PKG
                     This package body has Procedures and Functions related to Pricing GuardRail API Changes
    Change History
     VER  Resource             Date              TMS Ticket #  20130208-01001    Procedure /Function Name
          Satish U             08-FEB-2013                                       Procedure Release_HOld ,
                                                                                 Procedure DB_TRIGGER_RELEASE_HOLD,
                                                                                 Procedure DB_TRIGGER_RELEASE_HOLD2
          Satish U             13-FEB-2013       TMS Ticket#   20130213-01750    Procedure Process_OM_LINE
          Shankar H            07-OCT-2014       TMS Ticket#    20141001-00174   Multi_org project 
	 2.1  Niraj K Ranjan       17-AUG-2016       TMS Ticket#    20160801-00059   Lines stuck in Awaiting Invoice interface on hold when multiple holds applied 
   *****************************************************************************************************************************************************************/

   Procedure Get_PGR_Message ( p_Line_Number   IN  Varchar2,
                  p_Min_Margin     IN Varchar2,
                  p_Max_Discount   IN Varchar2,
                  p_String1        IN Varchar2,
                  x_Return_Message OUT NOCOPY Varchar2 )  IS
    Begin
       If P_Max_Discount = 'N/A'  Then
          x_return_message :=  'Line:'||p_line_number ||'. Pricing GuardRail Violation. '
               || ' Min Margin='
               || p_min_margin
               || ' and Max Discount='
               || p_max_discount
               || P_String1;
       Else
          x_return_message :=  'Line:'|| p_line_number ||'. Pricing GuardRail Violation. '
               || ' Min Margin='
               || p_min_margin
               || ' and Max Discount='
               || p_max_discount
               || '.Max Discount indicates a Source Savings item'
               || p_String1;
       End If;
    End Get_PGR_Message;

   PROCEDURE IMPORT_GUARDRAIL_DATA (iv_pricing_GuardRail_ID IN NUMBER,
                                    iv_price_zone           IN VARCHAR2,
                                    iv_item_category        IN VARCHAR2,
                                    iv_item_number          IN VARCHAR2,
                                    iv_min_margin           IN NUMBER,
                                    iv_max_discount         IN NUMBER,
                                    iv_start_date           IN DATE,
                                    iv_end_date             IN DATE )
   IS
       lv_message varchar2(2000);
       lv_price_zone varchar2(50);
       lv_item_id  number;
       lv_category_id number;
       lv_check number;
       C_STATUS_CURRENT    CONSTANT VARCHAR2(30) := 'CURRENT';
       C_STATUS_HISTORY    CONSTANT VARCHAR2(30) := 'HISTORY' ;
       l_API_NAME                   VARCHAR2(30) := '.IMPORT_GUARDRAIL_DATA';

       l_Return_Status     Varchar2(1);
       l_Module_Name       Varchar2(180) ;

   Cursor   C1_Cur  (P_Pricing_GuardRail_ID varchar2 ) IS
        Select *
        From apps.xxwc_om_pricing_guardrail
        Where Pricing_GuardRail_ID = iv_Pricing_GuardRail_ID;

   l_Pricing_GuardRail_Rec   apps.XXWC_OM_PRICING_GUARDRAIL%ROWTYPE;

   BEGIN
     l_Module_Name   :=  C_MODULE_NAME || l_API_NAME ;
     XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                   p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => 'Beging of API');
     IF iv_item_category is null and iv_item_number is null then
          lv_message := 'Item Category or Item Number should be entered';
          --x_MSG_DATA := lv_Message ;
          XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lv_Message);
          RAISE_APPLICATION_ERROR(-20001,lv_MESSAGE);
     END IF;

     IF iv_min_margin is null or iv_start_date is null or iv_price_zone is null then
          lv_message := 'Price Zone, Min Margin and Start Date should be entered';
          --x_MSG_DATA := lv_Message ;
          XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lv_Message);

          RAISE_APPLICATION_ERROR(-20001,lv_MESSAGE);
     END IF;

     -- Satish U: 28-NOV-2011 :
     IF iv_start_date is Not null And  iv_End_Date is Not null And Iv_End_Date < Iv_Start_Date then
          lv_message := 'End Date Should be greater then Start Date';
          --x_MSG_DATA := lv_Message ;
          XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lv_Message);

          RAISE_APPLICATION_ERROR(-20001,lv_MESSAGE);
     END IF;

     IF iv_item_category is not null and iv_item_number is not null then
          lv_message := 'Enter only Item Category or Item Number should be entered';
          --x_MSG_DATA := lv_Message ;
          XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lv_Message);
          RAISE_APPLICATION_ERROR(-20001,lv_MESSAGE);
     END IF;

     BEGIN
      SELECT a.flex_value
        into lv_price_zone
        FROM apps.fnd_flex_values_vl a, apps.fnd_flex_value_sets b
       WHERE a.flex_value_set_id = b.flex_value_set_id
         AND b.flex_value_set_name = 'WC_PRICE_ZONES'
         AND a.enabled_flag = 'Y'
         AND a.flex_value = iv_price_zone
         AND SYSDATE BETWEEN NVL (start_date_active, SYSDATE - 1)
                       AND NVL (end_date_active, SYSDATE + 1);
     EXCEPTION
       when no_data_found then
         lv_message := 'Invalid Price Zone';
         --x_MSG_DATA := lv_Message ;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lv_Message);
         RAISE_APPLICATION_ERROR(-20001,lv_MESSAGE);

     END;

     IF iv_item_category is not null then
        BEGIN
        select d.category_id
          into lv_category_id
          from apps.MTL_DEFAULT_CATEGORY_SETS_FK_V b
              ,apps.mtl_category_sets_v c
              ,apps.mtl_categories_v d
         where b.functional_area_id=1
           and b.category_set_id=c.category_set_id
           and c.structure_id=d.structure_id
           and d.category_concat_segs=iv_item_category;
        EXCEPTION
       when no_data_found then
         lv_message := 'Invalid Item Category';
         --x_MSG_DATA := lv_Message ;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lv_Message);
         RAISE_APPLICATION_ERROR(-20001,lv_MESSAGE);
        END;
     END IF;

     IF iv_item_number is not null then
       begin
        select a.inventory_item_id
          into lv_item_id
          from apps.mtl_system_items a
         where a.segment1=iv_item_number
           and rownum=1;
       exception
        when no_data_found then
         lv_message := 'Item not found';
         --x_MSG_DATA := lv_Message ;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lv_Message);
         RAISE_APPLICATION_ERROR(-20001,lv_MESSAGE);
       end;
     END IF;

     If iv_pricing_GuardRail_ID is Not Null THen

         --For C1_Rec in C1_Rec(iv_pricing_GuardRail_ID) Loop

             Update apps.xxwc_om_pricing_guardrail
               set end_date          = iv_start_date -1,
                   last_update_date  = sysdate,
                   last_updated_by   = fnd_global.user_id,
                   Status            = C_STATUS_HISTORY
             Where Pricing_GuardRail_ID = iv_pricing_GuardRail_ID ;

             XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_STATEMENT,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => 'UPdate Successful for Pricing Guard Raid ID :' || iv_Pricing_GuardRail_ID);

         -- end loop;
     End If;

     l_Pricing_GuardRail_Rec.Pricing_GuardRail_ID := XXWC_OM_PRICING_GUARDRAIL_S.NextVal;
     l_Pricing_GuardRail_Rec.price_zone           := iv_price_zone ;
     l_Pricing_GuardRail_Rec.item_category_id     := lv_category_id;
     l_Pricing_GuardRail_Rec.inventory_item_id    := lv_item_id;
     l_Pricing_GuardRail_Rec.min_margin           := iv_min_margin;
     l_Pricing_GuardRail_Rec.max_discount         := iv_max_discount;
     l_Pricing_GuardRail_Rec.start_date           := iv_start_date ;
     l_Pricing_GuardRail_Rec.end_date             := iv_end_date ;

     Insert_Record ( P_Pricing_GuardRail_Rec  => l_Pricing_GuardRail_Rec,
                     x_Return_Status          => l_Return_Status,
                     x_Msg_Data               => lv_Message );
     If l_Return_Status <> 'S' Then
       --x_MSG_DATA := lv_Message ;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lv_Message);
         RAISE_APPLICATION_ERROR(-20001,lv_MESSAGE);
     End IF;

    XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_Statement,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => 'Insert is Successful for Pricing Guard Raid ID :' );
EXCEPTION
   When Others Then
      -- Added When Others Exception
      --x_Msg_Data := SQLERRM;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lv_Message);
      RAISE_APPLICATION_ERROR(-20001,lv_MESSAGE);
END import_guardrail_data;

   FUNCTION get_temp_info (iv_info_type VARCHAR2)
      RETURN VARCHAR
   IS
   lv_ret_status varchar2(1);
   lv_ret_msg VARCHAR2(2000);
   BEGIN
   /*
     select return_status, return_message
       into lv_ret_status, lv_ret_msg
       from xxwc_om_pg_info; */
      fnd_profile.get('XXWC_OM_PG_STATUS',lv_ret_status);
      fnd_profile.get('XXWC_OM_PG_MSG',lv_ret_msg);

     IF iv_info_type = 'S' then
      return(lv_ret_status);
     ELSE
      return(lv_ret_msg);
     END IF;
   END get_temp_info;



   FUNCTION get_price_zone (iv_org_id NUMBER)
      RETURN VARCHAR
   IS
      lv_price_zone   VARCHAR2 (50);
   BEGIN
      SELECT attribute6
      INTO lv_price_zone
      From MTL_PARAMETERS
        --FROM mtl_parameters_view
      WHERE organization_id = iv_org_id;

      RETURN (lv_price_zone);
   EXCEPTION
      WHEN OTHERS  THEN
         RETURN (NULL);
   END get_price_zone;

   /********************************************************************************
   *** History
   *** Satish U : 03-DEC-2012 : Added Code to Apply Holds at Order Header Level
   **********************************************************************************/
   PROCEDURE apply_holds (iv_header_id            NUMBER
                        , iv_line_id              NUMBER
                        , iv_guardrail_hold       NUMBER
                        , ov_return_status    OUT VARCHAR2
                        , ov_return_message   OUT VARCHAR2)
   IS

      lv_return_status     VARCHAR2 (30);
      lv_msg_data          VARCHAR2 (4000);
      lv_msg_count         NUMBER;
      lv_hold_source_rec   OE_HOLDS_PVT.HOLD_SOURCE_REC_TYPE;
      lv_hold_exists       Varchar2(1);
      lv_Holds_Count       Number;
      l_API_Name           Varchar2(30):=  '.APPLY_HOLDS' ;

      -- Satish U: 02-FEB-2012
      l_Booked_Flag    Varchar2(1) ;
      l_Line_ID        Number ;
      l_PGR_HOLD_ID    Number ;
      --l_Org_ID             Number;
   BEGIN
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '2000: Begining of API : ' );
      lv_hold_source_rec := OE_HOLDS_PVT.G_MISS_HOLD_SOURCE_REC;

      lv_hold_source_rec.hold_entity_code := 'O';          -- order level hold
      lv_hold_source_rec.hold_entity_id   := iv_header_id; -- header_id of the order
      lv_hold_source_rec.header_id        := iv_header_id;


      lv_return_status := NULL;
      lv_msg_data := NULL;
      lv_msg_count := NULL;

      If G_OM_PG_ACCESS = 'Y' THen
          -- Satish U: 03-DEC-2012 :  Hold Until Date should be NULL
          lv_hold_source_rec.hold_until_date  := NULL; -- SYSDATE + 1;
          lv_hold_source_rec.hold_id          := iv_guardrail_hold;
          l_PGR_Hold_ID                       := iv_guardrail_hold;
          lv_hold_source_rec.line_id          := iv_line_id;       -- line__id of the order
           xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                          p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                          P_DEBUG_MSG     => '2010:  User has PG Access : ' );

      ELSIF G_OM_PG_DB_TRIGGER_FLAG = 'Y' AND G_OM_PG_ACCESS = 'N' THen
          l_Line_ID  := iv_Line_Id ;
          lv_hold_source_rec.line_id          := iv_line_id;
          lv_hold_source_rec.hold_until_date  := NULL;
          lv_hold_source_rec.hold_id          := G_OM_PG_HOLD_LU_BOOK;
          l_PGR_HOLD_ID                       := G_OM_PG_HOLD_LU_BOOK;
          -- Check if OSales Order is Booked or Not
          xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '2020:  Restricted User : Called from DB Trigger : ' );
      END IF;
      --Satish U : 12/03/2012 :
      -- Check if Hold exists at Order Header Level
      Select Count(*)
      Into lv_Holds_Count
      From   apps.oe_order_holds ooh,
             apps.oe_hold_sources ohs
      Where ooh.Header_Id = iv_header_id
      AND   ooh.Line_ID   Is NULL
      and   ohs.Hold_Id   = l_PGR_Hold_ID
      And   ooh.Hold_Release_ID is Null
      And   ooh.hold_source_ID = ohs.Hold_Source_ID ;

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '2021:  Order Header Holds Count : '  || lv_Holds_Count);
      If lv_Holds_Count = 0 Then
           -- 15-DEC-2011  Coomit ws turned off
           --  Commented by Shankar 26-Jul-2012
           -- Uncommented BY Satish U : 12/03/2012
           lv_hold_source_rec.line_id := NULL;
           OE_Holds_PUB.Apply_Holds (p_api_version       => 1.0
                              , p_init_msg_list     => FND_API.G_TRUE
                              , p_commit            => FND_API.G_TRUE
                              , p_hold_source_rec   => lv_hold_source_rec
                              , x_return_status     => lv_return_status
                              , x_msg_count         => lv_msg_count
                              , x_msg_data          => lv_msg_data);


           xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '2022:  Return Status of apply holds : '  || lv_return_status);
      Else
         -- Do not apply Another Order Header Hold
         lv_return_status := FND_API.G_RET_STS_SUCCESS;
      End If;

      If lv_return_status =  FND_API.G_RET_STS_SUCCESS Then
          -- Check if Hold exists for this Order Line
          Select Count(*)
          Into lv_Holds_Count
          From   apps.oe_order_holds ooh,
                  apps.oe_hold_sources ohs
          Where ooh.Header_Id = iv_header_id
          AND   ooh.Line_ID   =  iv_Line_Id
          and   ohs.Hold_Id   = l_PGR_Hold_ID
          And   ooh.Hold_Release_ID is Null
          And   ooh.hold_source_ID = ohs.Hold_Source_ID ;

          xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '2030:  Holds Count : '  || lv_Holds_Count);


          If lv_Holds_Count = 0 Then
               -- 15-DEC-2011  Coomit ws turned off
               -- Commented by Shankar 26-Jul-2012
               -- Uncommented By Satish U : 03-DEC-2012
               -- Uncommented BY Satish U : 12/03/2012
               lv_hold_source_rec.line_id := iv_line_id;
               OE_Holds_PUB.Apply_Holds (p_api_version       => 1.0
                                  , p_init_msg_list     => FND_API.G_TRUE
                                  , p_commit            => FND_API.G_TRUE
                                  , p_hold_source_rec   => lv_hold_source_rec
                                  , x_return_status     => lv_return_status
                                  , x_msg_count         => lv_msg_count
                                  , x_msg_data          => lv_msg_data);

               --Added by Shankar
               lv_return_status := FND_API.G_RET_STS_SUCCESS;
               xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '2040:  Return Status of apply holds : '  || lv_return_status);
          Else
             -- Do not apply Another Hold
             lv_return_status := FND_API.G_RET_STS_SUCCESS;
          End If;
      End IF;

      IF lv_return_status = FND_API.G_RET_STS_SUCCESS
      THEN
         ov_return_status := 'S';
      ELSE
         -- Satish U: 08-DEC-2011 Added following logic to retrieve error message
         FOR K IN 1 .. lv_msg_count loop
            lv_msg_data := oe_msg_pub.get( p_msg_index => k, p_encoded => 'F'  );
         End Loop ;
         ov_return_status := 'E';
         ov_return_message := lv_msg_data;

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '2050: Apply Holds Failed : ' || lv_msg_data);


      End IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         ov_return_status := 'E';
         ov_return_message := 'Exception Applying Holds-' || SUBSTR (SQLERRM, 1, 80);
   END apply_holds;

   FUNCTION get_item_price (iv_item_id         NUMBER
                          , iv_national_pl     NUMBER
                          , iv_category_pl     NUMBER
                          , iv_ordered_date    DATE)
      RETURN NUMBER
   IS
      lv_list_price   NUMBER;
      l_API_NAME      Varchar2(80) := 'GET_ITEM_PRICE';
   BEGIN
      SELECT list_price
        INTO lv_list_price
        FROM apps.qp_list_lines_v
       WHERE     list_header_id = iv_category_pl
             AND product_attribute_context = 'ITEM'
             AND product_attr_value = iv_item_id
             AND iv_ordered_date BETWEEN NVL (start_date_active
                                            , iv_ordered_date - 1)
                                     AND NVL (end_date_active
                                            , iv_ordered_date + 1);
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '600: Get_Item_Price : ' || lv_list_price);

      RETURN (lv_list_price);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         BEGIN

            SELECT list_price
              INTO lv_list_price
              FROM apps.qp_list_lines_v
             WHERE     list_header_id = iv_national_pl
                   AND product_attribute_context = 'ITEM'
                   AND product_attr_value = iv_item_id
                   AND iv_ordered_date BETWEEN NVL (start_date_active
                                                  , iv_ordered_date - 1)
                                           AND NVL (end_date_active
                                                  , iv_ordered_date + 1);
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '610: Get_Item_Price : ' || lv_list_price);

            RETURN (lv_list_price);
         EXCEPTION
            WHEN OTHERS
            THEN
               RETURN (NULL);
         END;
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_item_price;

   FUNCTION get_inventory_category (iv_item_id             NUMBER
                                  , iv_ship_from_org_id    NUMBER)
      RETURN NUMBER
   IS
      lv_category_id   NUMBER;
      l_API_NAME      Varchar2(80) := '.GET_INVENTORY_CATEGORY';
   BEGIN
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '1000: Item ID : ' || iv_item_id);
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '1001: Ship From Org ID : ' || iv_ship_from_org_id);
      SELECT c.category_id
      INTO lv_category_id
      FROM apps.MTL_CATEGORY_SETS_V a
           , apps.MTL_DEFAULT_CATEGORY_SETS_FK_V b
           , apps.mtl_item_categories c
      WHERE     a.category_set_id = b.category_set_id
      AND b.functional_area_id = 1
      AND a.category_set_id = c.category_set_id
      AND c.inventory_item_id = iv_item_id
      AND c.organization_id = iv_ship_from_org_id;

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '1002: Category ID is  : ' || lv_category_id);

      RETURN (lv_category_id);
   EXCEPTION
      WHEN OTHERS
      THEN
          xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '1003: NULL Category ID is  returned : ' );
         RETURN (NULL);
   END get_inventory_category;

   FUNCTION get_margin_disc_info (iv_info_type           VARCHAR2
                                , iv_item_id             NUMBER
                                , iv_ship_from_org_id    NUMBER
                                , iv_price_zone          VARCHAR2
                                , iv_ordered_date        DATE)
      RETURN NUMBER
   IS
      lv_min_margin     NUMBER;
      lv_max_discount   NUMBER;
      lv_item_cat_id    NUMBER;
      l_API_NAMe        Varchar2(80) := '.GET_MARGIN_DISC_INFO';
      l_Start_Date      Date ;
   BEGIN
      -- Satish U: 10/28/2011 : Added Status in The Whare Clause : Considering only PGR Records with Status CURRENT

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '700: Price Zone : ' || iv_price_zone);
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '701: Ordered Date : ' || iv_ordered_date);
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '702: iv_ship_from_org_id : ' || iv_ship_from_org_id);

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '703: iv_item_id : ' || iv_item_id);


      SELECT min_margin, max_discount, Start_Date
      INTO lv_min_margin, lv_max_discount, l_Start_Date
      FROM apps.XXWC_OM_PRICING_GUARDRAIL
      WHERE price_zone = iv_price_zone
      AND inventory_item_id = iv_item_id
      AND  Status = C_PGR_STATUS_CURRENT
      AND iv_ordered_date BETWEEN start_date
            AND NVL (end_date, iv_ordered_date + 1);



      IF iv_info_type = 'MARGIN' THEN
          xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '711: Return Min Margin: ' || lv_min_margin);
         RETURN (lv_min_margin);
      ELSE
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '712: Return Max Discount: ' || lv_max_discount);
         RETURN (lv_max_discount);
      END IF;

   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '720: Ship From Org ID : ' || iv_ship_from_org_id);

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '721: Price Zone: ' || iv_price_zone);

          xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '722: Item Category ID: ' || lv_item_cat_id);

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '723: Ordered Date: ' || To_Char(iv_ordered_date,'DD-MON-YYYY'));

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '724: Item Id: ' || iv_item_id);


         SELECT get_inventory_category (iv_item_id, iv_ship_from_org_id)
         INTO lv_item_cat_id
         FROM sys.DUAL;

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '725: Item Category Id: ' || lv_item_cat_id);
         BEGIN
            SELECT min_margin, max_discount
            INTO lv_min_margin, lv_max_discount
            FROM apps.XXWC_OM_PRICING_GUARDRAIL
            WHERE price_zone = iv_price_zone
            AND  Status = C_PGR_STATUS_CURRENT
            AND item_category_id = lv_item_cat_id
            AND iv_ordered_date BETWEEN start_date    AND NVL (end_date , iv_ordered_date + 1);

            IF iv_info_type = 'MARGIN'  THEN
               xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '731: Return Min Margin: ' || lv_min_margin);
               RETURN (lv_min_margin);
            ELSE
                xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '732: Return Max Discount: ' || lv_max_discount);
               RETURN (lv_max_discount);
            END IF;
         EXCEPTION
            WHEN OTHERS   THEN
               xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '733: When Others : Return Null Value: ');
               RETURN (NULL);
         END;
      WHEN OTHERS THEN
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '734: When Others : Return Null Value: ');
         RETURN (NULL);
   END get_margin_disc_info;

   FUNCTION get_average_cost (iv_item_id NUMBER, iv_ship_from_org_id NUMBER)
      RETURN NUMBER
   IS
      lv_item_cost   NUMBER;
   BEGIN
      SELECT item_cost
      INTO lv_item_cost
      FROM apps.CST_ITEM_COST_TYPE_V
      WHERE     inventory_item_id = iv_item_id
      AND organization_id = iv_ship_from_org_id
      AND cost_type = 'Average';
      -- Satish U : 22-NOV-2011
      --AND default_cost_type = 'Average';

      RETURN (lv_item_cost);
   EXCEPTION
      WHEN OTHERS    THEN
         RETURN (NULL);
   END get_average_cost;
   /***************************************************************************************************************
     PROCEDURE Process_OM_Line   :   Core API to perform Pricing Guard Rail validation
     History
     Resource         Date            Description
     Satish U         13-FEB-2013     TMS# 20130213-01750 Added Logic to not to invoke Pricing Guard Rail Logic when Unit Selling Procie
                                      on Sales Order line ( Database and User Interface has same Values )
   ****************************************************************************************************************/

   PROCEDURE process_om_line (iv_line_id          IN     NUMBER,
                              iv_item_id          IN     NUMBER,
                              iv_unit_sp          IN     NUMBER,
                              iv_ship_from_org_id IN     NUMBER,
                              iv_pricing_date     IN     VARCHAR,
                              iv_header_id        IN     NUMBER,
                              iv_flow_code        IN     VARCHAR,
                              iv_unit_cost        IN     NUMBER,
                              iv_list_price       IN     NUMBER,
                              iv_order_type_id    IN     NUMBER
   )
                            --, ov_return_status       OUT VARCHAR2
                           -- , ov_return_message      OUT VARCHAR2)
   IS
      lv_item_id              NUMBER;
      lv_item_number          VARCHAR2 (40);
      lv_unit_sp              NUMBER;
      lv_ship_from_org_id     NUMBER;
      lv_price_zone           VARCHAR2 (50);
      lv_item_cost            NUMBER;
      lv_ordered_date         DATE;
      lv_min_margin           NUMBER;
      lv_max_discount         NUMBER;
      lv_order_margin_price   NUMBER;
      lv_order_discount       NUMBER;
      lv_item_price           NUMBER;
      lv_header_id            NUMBER;
      lv_return_status        VARCHAR2 (1);
      lv_return_message       VARCHAR2 (2000);
      lv_status_code          apps.oe_order_headers_all.flow_status_code%TYPE;
      lv_order_type_id        NUMBER;
      lv_rental_order_flag    VARCHAR2(1);
      lv_line_num             NUMBER;


      lc_security_access      VARCHAR2 (3) := fnd_profile.VALUE ('XXWC_OM_PG_ACCESS');
      lc_national_pl          NUMBER := fnd_profile.VALUE ('XXWC_OM_NPL');
      lc_category_pl          NUMBER := fnd_profile.VALUE ('XXWC_OM_CPL');
      lc_guardrail_hold       VARCHAR2 (50)    := fnd_profile.VALUE ('XXWC_OM_PG_HOLD');
      l_API_Name              Varchar2(30):=  '.PROCESS_OM_LINE' ;
      l_Max_Discount          Varchar2(10);
      l_Org_Id                Number;
      l_Manual_Adj_Exists     Varchar2(1) := 'N';
      l_Auto_Adj_Exists       Varchar2(1) := 'Y';

      -- Satish U : 13-FEB-2013 : PGR Logic should not fire when USP is not changed.
      -- Define a Variable to capture Database Unit Selling Price on Sales Order Line
      -- TMS # 20130213-01750
      l_DB_SOL_USP                  Number ;
      l_Source_Document_Type_ID     Number ;
      l_Order_Source_ID             Number ;
      l_Lock_Control                Number ;
      l_PGR_HOld_Count              Number ;

      -- When Sales Order is copied it will have following values
      C_Source_Document_Type_ID   Constant Number := 2;
      C_Order_Source_ID           Constant Number := 2;



   BEGIN

      --MO_GLOBAL.set_policy_context('S', L_ORG);
      fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                  resp_id        => fnd_global.resp_id,
                                  resp_appl_id   => fnd_global.resp_appl_id);
      SELECT   mo_global.get_current_org_id ()
      INTO l_org_id
      FROM DUAL;

      -- Assign values to Global Variables . TO capture Profile Values for
      -- Pricing GuardRail Holds for Limited User
      G_OM_PG_HOLD_LU_BOOK    := fnd_profile.VALUE ('XXWC_OM_PG_HOLD_LU_BOOK');
      --G_OM_PG_HOLD_LU_ENTER   := fnd_profile.VALUE ('XXWC_OM_PG_HOLD_LU_ENTER');
      G_OM_PG_ACCESS          := lc_Security_Access;
      G_OM_PG_DB_TRIGGER_FLAG  := 'N';

      --mo_global.set_policy_context ('S', 161);
      mo_global.set_policy_context ('S', l_org_id);


      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '100 : Beging of API');
      -- Satish U : 10/18/2011
      --/**************************
      Delete_temp_info (p_Header_ID  => iv_Header_ID,
                        p_Line_ID    => iv_Line_Id) ;
      --*************************/
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '120: After Deleting Records from GT');
      -- Satish U: 22-NOV-2011 If Profile is not set , treat it as N
      lc_security_access  :=  NVL(lc_security_access, 'N' );
      --Get unit price from the order line
      --18-APR-2012 : Satish U: Assign
      IF lc_Security_Access = 'N' AND G_PRE_BOOK_FLAG = 'Y' Then
         lc_Security_Access := 'Y';
         G_OM_PG_ACCESS          := lc_Security_Access;
      End If;

      lv_item_id  := iv_item_id;
      lv_unit_sp  := iv_unit_sp;
      lv_ship_from_org_id := iv_ship_from_org_id;
      --fnd_profile.put('XXWC_OM_PG_STATUS', 'E');
      --fnd_profile.put('XXWC_OM_PG_MSG',iv_pricing_date);
      --RETURN;
      lv_ordered_date := to_date(iv_pricing_date,'RRRR/MM/DD HH24:MI:SS');
      lv_header_id := iv_header_id;
      -- Satish U: 22-NOV-2011 : If it is NULL then assign NULL String
      lv_status_code := NVL(iv_flow_code,'NULL');
      lv_item_cost := iv_unit_cost;
      lv_item_price := iv_list_price;
      lv_order_type_id := iv_order_type_id;
      begin
        -- Satish U: 13-FEB-2013  Get Unit Selling PRice From Database
        -- TMS# 20130213-01750
         select line_number, Unit_Selling_Price , Source_Document_Type_ID, Order_Source_ID, Lock_Control
         into lv_line_num, l_DB_SOL_USP, l_Source_Document_Type_ID, l_Order_Source_ID, l_Lock_Control
         from apps.oe_order_lines
         where line_id = iv_line_id;
      exception
         when no_data_found then
            lv_line_num := null;
            l_DB_SOL_USP := NULL;
      end;

      -- If Unit Selling Price is not different from DB then do not PGR Check
      -- Satish U: 13-FEB-2013  Get Unit Selling PRice From Database
      -- TMS# 20130213-01750
       IF l_DB_SOL_USP IS NOT NULL AND l_DB_SOL_USP = lv_unit_sp  Then
          If l_Source_Document_Type_ID = C_Source_Document_Type_ID
          AND l_Order_Source_ID = C_Order_Source_ID Then
             Select Count(*)
             Into l_PGR_Hold_Count
             From   apps.oe_order_holds ooh,
                  apps.oe_hold_sources ohs
             Where ooh.Header_Id = iv_header_id
             AND   ooh.Line_ID   =  iv_Line_Id
             and   ohs.Hold_Id   in ( To_Number(lc_guardrail_hold), To_Number(G_OM_PG_HOLD_LU_BOOK) )
             And   ooh.hold_source_ID = ohs.Hold_Source_ID ;


            If  l_PGR_Hold_Count > 0  THEN
                 lv_return_status := NULL;
                 lv_return_message := NULL;
                  xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '125: Unit Selling Price on UI and DB is same');
                 RETURN;
            End If;
         Else
            lv_return_status := NULL;
            lv_return_message := NULL;
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '125: Unit Selling Price on UI and DB is same');
            RETURN;
         End If;




       END IF;

      --END;
     -- Get Order Type.
      select attribute1
        into lv_rental_order_flag
        from apps.oe_transaction_types_vl
       where transaction_type_id = lv_order_Type_id;
      -- If order type is rental then ignore guardrail validation.
       IF nvl(lv_rental_order_flag,'N') = 'Y' then
         lv_return_status := 'S';
         lv_return_message := NULL;
          xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '130: Rental Order : Not Processing further');


         RETURN;

       END IF;

      -- get pricing zone
      SELECT get_price_zone (lv_ship_from_org_id)
        INTO lv_price_zone
        FROM sys.DUAL;

      -- Send error message if no Price Zone is available for the shipping store
      IF lv_price_zone IS NULL   THEN
         lv_return_status := 'E';
         lv_return_message := 'Line:'||lv_line_num||'. No Price Zone is available for the Store';
         --update xxwc_om_pg_info set return_status = lv_return_status, return_message = lv_return_message;

         -- Inserting Into Global Temp Table SatishU : 10/17/2011
          Update_Status(p_Header_ID      => iv_Header_ID ,
                       P_Line_ID         => IV_Line_ID,
                       p_Return_Status   => lv_Return_Status,
                       p_Return_Message  => lv_Return_Message)  ;

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '140: Pricing Zone Not Defined : Not Processing further');


         /************
         fnd_profile.put('XXWC_OM_PG_STATUS', lv_return_status);
         fnd_profile.put('XXWC_OM_PG_MSG',lv_return_message);
         ***************/
          RETURN;

      END IF;

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '141: Price Zone is ' || lv_price_zone);

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '142: Item ID is ' || lv_item_id);

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '143: Ship From Org ID is ' || lv_ship_from_org_id);

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '144: Order Date is ' || To_Char(lv_ordered_date,'DD-MON-RRRR'));
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '145: Line ID is ' || iv_line_id);
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '146: Header ID is ' || iv_Header_id);
      -- Get Minimum Margin
      SELECT get_margin_disc_info ('MARGIN'
                                 , lv_item_id
                                 , lv_ship_from_org_id
                                 , lv_price_zone
                                 , lv_ordered_date)
        INTO lv_min_margin
        FROM sys.DUAL;

      --  Get Maximum Discount
      SELECT get_margin_disc_info ('DISCOUNT'
                                 , lv_item_id
                                 , lv_ship_from_org_id
                                 , lv_price_zone
                                 , lv_ordered_date)
        INTO lv_max_discount
        FROM sys.DUAL;

      IF lv_min_margin IS NULL --OR lv_max_discount IS NULL
      THEN
         --SatishU:11/02/11 : When No Min Margin is set then Do not update Message
         -- Following 2 lines are commented
         --lv_return_status := 'S';
         --lv_return_message := 'Line:'||lv_line_num||'. Min Margin / Max Discount information not available for the item';
         lv_return_status := NULL;
         lv_Return_Message := NULL;

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '150: Min Margin Not Defined : Processing further : Not an error');

      END IF;


      -- Send error message if no Item Cost is available for the shipping store
      IF lv_item_cost IS NULL
      THEN
         -- Satish U: 16-NOV-2011 : When Item Cost is not defined do not raise it as Error
         -- Request from Cristan on 16-NOV-2011
         lv_return_status := NULL;
         lv_Return_Message := NULL;

          xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '160: Item Cost Not Defined : Processing Further , Not an error');

      END IF;
      -- Send error message if no Item Cost is available for the shipping store
      IF lv_item_price IS NULL
      THEN
         lv_return_status := 'E';
         lv_return_message :=
            'Line:'||lv_line_num||'. Category or National Price for the item not available';
        --update xxwc_om_pg_info set return_status = lv_return_status, return_message = lv_return_message;
        -- Inserting Into Global Temp Table SatishU : 10/17/2011
         Update_Status(p_Header_ID      => iv_Header_ID ,
                       P_Line_ID        => iv_line_ID,
                       p_Return_Status  => lv_Return_Status,
                       p_Return_Message => lv_Return_Message)  ;

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '170: Item Price Not Defined : Not Processing further');

          RETURN;

      END IF;
      -- 20-MAR-2012 : CHeck if Sales Order line is Saved before making changes to Unit Selling PRice
      --/******************
       --04JAN2011 : Message Changed as per Cristan's note
      If   (lv_unit_sp <> lv_item_price)  AND lv_status_code = 'NULL' Then
         -- Satish U: 20-MAR-2012 :   Added new API to show consistent messages : Satish Ul
         Get_PGR_Message ( p_Line_Number    => lv_line_num,
                      p_Min_Margin     => lv_min_margin,
                      p_Max_Discount   => l_Max_Discount,
                      p_String1        => '. You must save order line before changing Unit Selling Price.' ,
                      x_Return_Message => lv_return_message ) ;
         lv_return_status := 'E';

         -- Satish U: 20-MAR-2012 : Insert Record into Table : Pricing GuardRail Viloation happened
         Update_Status(p_Header_ID      =>  lv_Header_ID ,
                       P_Line_ID        =>  iv_Line_ID,
                       p_Return_Status  =>  lv_Return_Status,
                       p_Return_Message =>  lv_Return_Message)  ;
         Return ;

      End IF;
     -- *****************************/


      --Satish U: 22-Mar-2012 : Check If Manual Adjustments exists If Not then  Return
      -- Check for Header Level Modifiers too : Satish U: 05-APR-2012
      -- Satish U: 20-JUN-2012 : Added following Code as we should not be worried about other List Line Type Codes while looking for Automatic and Manual Modifiers
      --PRG (Promotional Goods)
      --CIE (Coupon Issue)
      -- PBH (Price Breaks)
      -- DIS ( Discount )
       -- OID (Other Item Discounts)
      Begin
          Select 'Y'
          Into  l_Manual_Adj_Exists
          From apps.Oe_Price_Adjustments
          Where ( Line_ID = iv_Line_ID OR ( Line_ID IS NULL AND HEADER_ID = iv_Header_ID ) )
          And   NVL(Automatic_Flag,'N')  = 'N'
          And  List_Line_Type_Code in ('DIS')
          And Rownum = 1;
      Exception
         When Others Then
            l_Manual_Adj_Exists := 'N';
      End ;

      -- Satish U: 04-APR-2012 : Check if Autom Adjustmetns Exists
      -- Check for Header Level Modifiers too : Satish U: 05-APR-2012
      Begin
          Select 'Y'
          Into  l_Auto_Adj_Exists
          From apps.Oe_Price_Adjustments
          Where ( Line_ID = iv_Line_ID OR ( Line_ID IS NULL AND HEADER_ID = iv_Header_ID ) )
          And   NVL(Automatic_Flag,'N')  = 'Y'
          And  List_Line_Type_Code in ('DIS')
          And Rownum = 1;
      Exception
         When Others Then
            l_Auto_Adj_Exists := 'N';
      End ;
      --Satish 22-MAR-2012 :  If there are Manual Modifiers Exist then do notthing come out.
      If l_Manual_Adj_Exists = 'N'  Then
         -- Satish U: 04-APR-2012
         If lv_item_price <= lv_unit_sp And l_Auto_Adj_Exists  = 'Y' Then
             lv_return_status  := NULL;
             lv_return_message := NULL;

             xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                            p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                            P_DEBUG_MSG     => '170.02: There are no manual adjustments exists' );

             Return ;
         Elsif lv_item_price = lv_unit_sp  Then
             lv_return_status  := NULL;
             lv_return_message := NULL;

             xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                            p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                            P_DEBUG_MSG     => '170.03: There are no manual adjustments exists' );
             Return ;
         End If;
      End IF;

      -- Satish U: 10/27/2011 : If Unit Selling Price is greather then List Price  or equal to List Price then Return
      If lv_item_price <= lv_unit_sp Then
         lv_return_status := 'S';
         lv_return_message := NULL;
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level =>  C_LEVEL_EVENT,
                                      p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                      P_DEBUG_MSG     => '170.1: Release Holds for Process OM Header: ');

         RELEASE_HOLD(p_header_id      => lv_Header_ID,
                      p_Line_ID        => iv_Line_ID    ,
                      p_hold_id        => lc_guardrail_hold    ,
                      x_Return_Status  => lv_Return_Status  ,
                      x_Msg_Data       => lv_return_message);

         If lv_Return_Status <> 'S' THen
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_ERROR,
                                           p_mod_name     => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG    => '170.2: Release Hold API Errored Out: ' || lv_Return_Message);
            -- Satish U: 05-MAR-2012 : As per Critans's Request
            lv_return_message  := ' Could not release Pricing Hold.' ;
            lv_return_status   := 'E';
         ELSE
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  => C_LEVEL_ERROR,
                                           p_mod_name     => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG    => '170.3: Release Hold API Successful: ');
            -- Satish U: 05-MAR-2012 : As per Critans's Request
            lv_return_message  := ' Pricing Hold released successfully.' ;
         ENd If;
         -- Satish U: 05-MAR-2012 : Insert Record into Table : Pricing GuardRail Viloation happened
         Update_Status(p_Header_ID      =>  lv_Header_ID ,
                       P_Line_ID        =>  iv_Line_ID,
                       p_Return_Status  =>  lv_Return_Status,
                       p_Return_Message =>  lv_Return_Message)  ;

         Return ;

      End If;

      IF nvl(lv_item_price,0) <> 0 then
       lv_order_discount := ROUND ( (lv_item_price - lv_unit_sp) / lv_item_price * 100, 2);
      ELSE
       lv_order_discount := 0;
      END IF;
      -- Satish U: 22-NOV-2011  Handle NULL Value
      -- lv_order_margin_price := ROUND (NVL(lv_item_cost,0) * (1 + NVL(lv_min_margin,0) / 100), 2);
      -- Satish U: 19-DEC-2011 : Changed as per Request from Cristan
      --lv_order_margin_price :=  (1 - ROUND((lv_unit_sp - NVL(lv_item_cost,0))/lv_unit_sp, 2)) * 100  ;
      -- Satish U: 05-MAR-2012 : IF Unit Selling Price is zero then Order Margin Price will be zero.
      If lv_Unit_Sp = 0 Then
        lv_order_margin_price := 0;
      Else
         lv_order_margin_price :=  ROUND(((lv_unit_sp - NVL(lv_item_cost,0))/lv_unit_sp) * 100,2)  ;
      End If;
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '171.1: lv_Order_Discount : ' || lv_order_discount);
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '171.2: lv_max_discount : ' || lv_max_discount);
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '171.3: lv_min_margin : ' || lv_min_margin);
       xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '171.4: lv_order_margin_price : ' || lv_order_margin_price);

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '171.5: lv_unit_sp : ' || lv_unit_sp);



      --Satish U: 08-DEC-2011
      -- WHen lv_main_Margin and lv_Max_Discount are NULL then do not do any checking.
      -- Satish U: Added Code 15-DEC-2011 : If lv_status_code Not In 'NULL'    lv_min_margin
      IF (lv_min_margin IS NOT NULL OR lv_max_discount IS NOT NULL ) AND
         ((lv_order_discount > NVL(lv_max_discount,100)) OR (lv_order_margin_price < lv_min_margin ))   THEN

         --((lv_order_discount > NVL(lv_max_discount,100)) OR (lv_order_margin_price > lv_unit_sp ))   THEN ( 19-DEC-2011 ) Satish U
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '174: Before Checking Security Status: ' || lc_security_access);
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '175: Before Checking Flow Status: ' || lv_status_code);
         -- Satish U 18-APR-20212 :
         -- Apply Hold Only When Manual Adjustments Exists
         --IF lc_security_access = 'Y' AND UPPER(lv_status_code) NOT IN ( 'DRAFT', 'NULL')
         IF lc_security_access = 'Y' AND UPPER(lv_status_code) NOT IN ( 'DRAFT','PENDING_CUSTOMER_ACCEPTANCE','NULL')
            AND l_Manual_Adj_Exists = 'Y'  THEN

            apply_holds (lv_header_id
                       , iv_line_id
                       , lc_guardrail_hold
                       , lv_return_status
                       , lv_return_message);

            IF lv_return_status = 'S'
            THEN
               -- Satish U : 17-NOV-2011
               lv_return_status := 'S' ;
               -- Satish U: 05-MAR-2012  : Added following logic make PGR message consistent
               If lv_max_Discount is NULL THEN
                  l_Max_Discount := 'N/A';
               ELSE
                  l_Max_Discount := To_Char(lv_Max_Discount) ;
               END IF;
               Get_PGR_Message ( p_Line_Number    => lv_line_num,
                                 p_Min_Margin     => lv_min_margin,
                                 p_Max_Discount   => l_Max_Discount,
                                 p_String1        => '. Hold has been applied.' ,
                                 x_Return_Message => lv_return_message ) ;
               -- Commented old Code : 05-MAR-2012 : Satish U:
               --lv_return_message :=  'Line:'||lv_line_num||'. Pricing GuardRail Violation. Hold has been applied';

               xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>   C_LEVEL_EVENT,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '180: Pricing Guard Rail Hold Applied : ');
            ELSE
               -- Satish U: 08-DEC-2011
               -- Satish U: 05-MAR-2012  : Added following logic make PGR message consistent
               If lv_max_Discount is NULL THEN
                  l_Max_Discount := 'N/A';
               ELSE
                  l_Max_Discount := To_Char(lv_Max_Discount) ;
               END IF;

               If lv_status_code = 'NULL' THEN
                  lv_return_status := 'E';
                  -- Satish U: 05-MAR-2012 :   Added new API to show consistent messages : Satish Ul
                  Get_PGR_Message ( p_Line_Number    => lv_line_num,
                                 p_Min_Margin     => lv_min_margin,
                                 p_Max_Discount   => l_Max_Discount,
                                 p_String1        => '. Unable to apply PGR Hold, please save changes.' ,
                                 x_Return_Message => lv_return_message ) ;
                  -- Satish U: 05-MAR-2012 : Old code Commented
                  --lv_return_message :=  'Line:'||lv_line_num||'. Pricing GuardRail Violation. Unable to apply PGR Hold, please save changes. ';
                  xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                              p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                              P_DEBUG_MSG     => '185: Error while applying Pricing Guard Rail Hold: ');
               ELSE
                  lv_return_status := 'E';
                  -- Satish U: 05-MAR-2012 :   Added new API to show consistent messages : Satish Ul
                  Get_PGR_Message ( p_Line_Number    => lv_line_num,
                                 p_Min_Margin     => lv_min_margin,
                                 p_Max_Discount   => l_Max_Discount,
                                 p_String1        => '. Unable to apply PGR Hold.' ,
                                 x_Return_Message => lv_return_message ) ;
                  -- Satish U: 05-MAR-2012 : Old code Commented
                  --lv_return_message :=  'Line:'||lv_line_num||'. Pricing GuardRail Violation. Unable to apply PGR Hold ';
                  xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                              p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                              P_DEBUG_MSG     => '190: Error while applying Pricing Guard Rail Hold: ');
               END IF;
            END IF;
            -- Satish U: 17-NOV-2011 : Insert Record into Table : Pricing GuardRail Viloation happened
            Update_Status(p_Header_ID      => iv_Header_ID ,
                       P_Line_ID        =>  iv_Line_ID,
                       p_Return_Status  =>  lv_Return_Status,
                       p_Return_Message =>  lv_Return_Message)  ;

            Return;

         ELSIF lc_security_access = 'Y' AND UPPER(lv_status_code) in  ( 'DRAFT','PENDING_CUSTOMER_ACCEPTANCE')
            AND l_Manual_Adj_Exists = 'Y'  THEN
            lv_return_status := 'S';
            -- Added Code to allow user to go below Pricing Guardrails in Quote. No Hold Will be created.
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '195:  Quotes : Pricing Guard Rail Violation: ');
            -- Satish U 27-FEB-2012
            -- Satish U: 18-APR2012 :   Added new API to show consistent messages : Satish Ul
            Get_PGR_Message ( p_Line_Number    => lv_line_num,
                              p_Min_Margin     => lv_min_margin,
                              p_Max_Discount   => l_Max_Discount,
                              p_String1        => '. Unit Selling Price below pricing guard rail.' ,
                              x_Return_Message => lv_return_message ) ;
            -- Satish U: 18-APR-2012: Insert Record into Table : Pricing GuardRail Viloation happened
            Update_Status(p_Header_ID      => iv_Header_ID ,
                       P_Line_ID        =>  iv_Line_ID,
                       p_Return_Status  =>  lv_Return_Status,
                       p_Return_Message =>  lv_Return_Message)  ;
            Return ;

         ELSIF l_Manual_Adj_Exists = 'Y'  Then
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '200: Pricing Guard Rail Violation: ');
            lv_return_status := 'E';
            If lv_max_Discount is NULL THEN
               l_Max_Discount := 'N/A';
            ELSE
               l_Max_Discount := To_Char(lv_Max_Discount) ;
            END IF;
            --04JAN2011 : Message Changed as per Cristan's note
            If lc_security_access = 'Y'  AND lv_status_code = 'NULL' Then
               -- Satish U: 05-MAR-2012 :   Added new API to show consistent messages : Satish Ul
               Get_PGR_Message ( p_Line_Number    => lv_line_num,
                                 p_Min_Margin     => lv_min_margin,
                                 p_Max_Discount   => l_Max_Discount,
                                 p_String1        => '. You must save order line before changing Unit Selling Price.' ,
                                 x_Return_Message => lv_return_message ) ;

            Else
               -- Satish U 27-FEB-2012
               -- Satish U: 05-MAR-2012 :   Added new API to show consistent messages : Satish Ul
               Get_PGR_Message ( p_Line_Number    => lv_line_num,
                                 p_Min_Margin     => lv_min_margin,
                                 p_Max_Discount   => l_Max_Discount,
                                 p_String1        => '. Please adjust the Order Price.' ,
                                 x_Return_Message => lv_return_message ) ;

            End IF;
            -- Satish U: 17-NOV-2011 : Insert Record into Table : Pricing GuardRail Viloation happened
            Update_Status(p_Header_ID      => iv_Header_ID ,
                       P_Line_ID        =>  iv_Line_ID,
                       p_Return_Status  =>  lv_Return_Status,
                       p_Return_Message =>  lv_Return_Message)  ;
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '201: Header ID : ' || iv_Header_ID);
             xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '202: Line ID : ' || iv_Line_ID);
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '203: Return Status : ' || lv_Return_Status);
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '204: Return Message : ' || lv_Return_Message);

            Return;


         END IF;
      ELSE

         lv_return_status := 'S';
         lv_return_message := NULL;
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level =>  C_LEVEL_EVENT,
                                      p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                      P_DEBUG_MSG     => '210: Release Holds for Process OM Header: ');

         RELEASE_HOLD(p_header_id      => lv_Header_ID,
                      p_Line_ID        => iv_Line_ID    ,
                      p_hold_id        => lc_guardrail_hold    ,
                      x_Return_Status  => lv_Return_Status  ,
                      x_Msg_Data       => lv_return_message);

         If lv_Return_Status <> 'S' THen
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_ERROR,
                                           p_mod_name     => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG    => '220: Release Hold API Errored Out: ' || lv_Return_Message);
            -- Satish U: 05-MAR-2012 : As per Critans's Request
            lv_return_message  := ' Could not release Pricing Hold.' ;
            lv_return_status   := 'E';
         ELSE
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_ERROR,
                                           p_mod_name     => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG    => '230: Release Hold API Successful: ');
            -- Satish U: 05-MAR-2012 : As per Critans's Request
            lv_return_message  := ' Pricing Hold released successfully.' ;
         ENd If;
         -- Satish U: 05-MAR-2012 : Insert Record into Table : Pricing GuardRail Viloation happened
         Update_Status(p_Header_ID      =>  lv_Header_ID ,
                       P_Line_ID        =>  iv_Line_ID,
                       p_Return_Status  =>  lv_Return_Status,
                       p_Return_Message =>  lv_Return_Message)  ;
      END IF;

   END process_om_line;

   PROCEDURE DBTRigger_PGR_Check (iv_line_id        IN     NUMBER,
                              iv_item_id          IN     NUMBER,
                              iv_unit_sp          IN     NUMBER,
                              iv_ship_from_org_id IN     NUMBER,
                              iv_pricing_date     IN     VARCHAR,
                              iv_header_id        IN     NUMBER,
                              iv_flow_code        IN     VARCHAR,
                              iv_unit_cost        IN     NUMBER,
                              iv_list_price       IN     NUMBER,
                              iv_order_type_id    IN     NUMBER,
                              iv_Line_Number      IN     VARCHAR2,
                              iv_Org_ID           IN     NUMBER,
                              iv_Manual_Adj_Exists   IN  Varchar2,
                              iv_Auto_Adj_Exists     IN  Varchar2,
                              x_Return_Status     OUT    Varchar2,
                              x_Msg_Data          OUT    Varchar2
   ) IS
      lv_item_id              NUMBER;
      lv_item_number          VARCHAR2 (40);
      lv_unit_sp              NUMBER;
      lv_ship_from_org_id     NUMBER;
      lv_price_zone           VARCHAR2 (50);
      lv_item_cost            NUMBER;
      lv_ordered_date         DATE;
      lv_min_margin           NUMBER;
      lv_max_discount         NUMBER;
      lv_order_margin_price   NUMBER;
      lv_order_discount       NUMBER;
      lv_item_price           NUMBER;
      lv_header_id            NUMBER;
      lv_return_status        VARCHAR2 (1);
      lv_return_message       VARCHAR2 (2000);
      lv_status_code          apps.oe_order_headers_all.flow_status_code%TYPE;
      lv_order_type_id        NUMBER;
      lv_rental_order_flag    VARCHAR2(1);
      lv_line_num             NUMBER;

      lc_security_access      VARCHAR2 (3) := fnd_profile.VALUE ('XXWC_OM_PG_ACCESS');
      lc_national_pl          NUMBER       := fnd_profile.VALUE ('XXWC_OM_NPL');
      lc_category_pl          NUMBER       := fnd_profile.VALUE ('XXWC_OM_CPL');
      lc_guardrail_hold       VARCHAR2 (50)    := fnd_profile.VALUE ('XXWC_OM_PG_HOLD');
      l_API_Name              Varchar2(30) :=  '.DBTRigger_PGR_Check' ;
      l_Max_Discount          Varchar2(10);
      l_Org_Id                Number;
      l_Manual_Adj_Exists     Varchar2(1) := 'Y';
      l_Auto_Adj_Exists       Varchar2(1) := 'N' ;
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
       xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '100 : Beging of API');
       l_Org_Id  := iv_Org_ID ;
      --MO_GLOBAL.set_policy_context('S', L_ORG);

      --mo_global.set_policy_context ('S', 161);
      mo_global.set_policy_context ('S', l_org_id);
       xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '101 : After Setting Policy Context for org id ' || l_ORG_ID);

      -- Satish U: 22-NOV-2011 If Profile is not set , treat it as N
      lc_security_access  :=  NVL(lc_security_access, 'N' );
      -- Pricing GuardRail Holds for Limited User
      G_OM_PG_HOLD_LU_BOOK    := fnd_profile.VALUE ('XXWC_OM_PG_HOLD_LU_BOOK');
      --G_OM_PG_HOLD_LU_ENTER   := fnd_profile.VALUE ('XXWC_OM_PG_HOLD_LU_ENTER');
      G_OM_PG_ACCESS          := lc_Security_Access;
      G_OM_PG_DB_TRIGGER_FLAG := 'Y';
      -- Satish U : 10/18/2011
      --- No Need to Populate in Pragma Autonomus transactions into Global Temp table
      /*************************
      Delete_temp_info (p_Header_ID  => iv_Header_ID,
                        p_Line_ID    => iv_Line_Id) ;
      --*************************/
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '120: After Deleting Records from GT');

      --Get unit price from the order line

      lv_item_id  := iv_item_id;
      lv_unit_sp  := iv_unit_sp;
      lv_ship_from_org_id := iv_ship_from_org_id;
      --fnd_profile.put('XXWC_OM_PG_STATUS', 'E');
      --fnd_profile.put('XXWC_OM_PG_MSG',iv_pricing_date);
      --RETURN;
      --lv_ordered_date := to_date(iv_pricing_date,'YYYY/MM/DD HH24:MI:SS');
      lv_ordered_date := to_date(iv_pricing_date,'DD-MON-RRRR HH24:MI:SS');
      lv_header_id := iv_header_id;
      lv_line_num  := iv_Line_Number ;
      -- Satish U: 22-NOV-2011 : If it is NULL then assign NULL String
      lv_status_code := NVL(iv_flow_code,'NULL');
      lv_item_cost := iv_unit_cost;
      lv_item_price := iv_list_price;
      lv_order_type_id := iv_order_type_id;
      --Satish U: 15-APR-2012 : Assign Values to Variables
      l_Manual_Adj_Exists     := iv_Manual_Adj_Exists;
      l_Auto_Adj_Exists       := iv_Auto_Adj_Exists ;


      select attribute1
      into lv_rental_order_flag
      from apps.oe_transaction_types_vl
      where transaction_type_id = lv_order_Type_id;

      -- If order type is rental then ignore guardrail validation.
      IF nvl(lv_rental_order_flag,'N') = 'Y' then
         lv_return_status := 'S';
         lv_return_message := NULL;
          xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '130: Rental Order : Not Processing further');
         --Satish U: 18-DEC-2011
         x_Return_Status := lv_return_status ;
         x_Msg_Data      := lv_return_message ;
         Commit;
         RETURN;

      END IF;

      -- get pricing zone
      SELECT get_price_zone (lv_ship_from_org_id)
      INTO lv_price_zone
      FROM sys.DUAL;

      -- Send error message if no Price Zone is available for the shipping store
      IF lv_price_zone IS NULL   THEN
         lv_return_status := 'E';
         lv_return_message := 'Line:'||lv_line_num||'. No Price Zone is available for the Store';
         --update xxwc_om_pg_info set return_status = lv_return_status, return_message = lv_return_message;


         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '140: Pricing Zone Not Defined : Not Processing further');


          -- Satish U: 18-DEC-2011
          x_Return_Status := lv_return_status ;
          x_Msg_Data      := lv_return_message ;
          Commit;
          RETURN;

      END IF;
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '141: Price Zone is ' || lv_price_zone);

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '142: Item ID is ' || lv_item_id);

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '143: Ship From Org ID is ' || lv_ship_from_org_id);

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '144: Order Date is ' || lv_ordered_date);

      -- Get Minimum Margin
      SELECT get_margin_disc_info ('MARGIN'
                                 , lv_item_id
                                 , lv_ship_from_org_id
                                 , lv_price_zone
                                 , lv_ordered_date)
        INTO lv_min_margin
        FROM sys.DUAL;
       xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '145: After the call to Get Margin Discount Min Margin is : ' || lv_min_margin);

      --  Get Maximum Discount
      SELECT get_margin_disc_info ('DISCOUNT'
                                 , lv_item_id
                                 , lv_ship_from_org_id
                                 , lv_price_zone
                                 , lv_ordered_date)
        INTO lv_max_discount
        FROM sys.DUAL;

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '146: After the call to Get Margin Discount Max Discount is : ' || lv_max_discount );
      IF lv_min_margin IS NULL --OR lv_max_discount IS NULL
      THEN
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '147: Min Margin value is Null' );

         --SatishU:11/02/11 : When No Min Margin is set then Do not update Message
         lv_return_status := NULL;
         lv_Return_Message := NULL;
          -- Satish U: 18-DEC-2011
          x_Return_Status := lv_return_status ;
          x_Msg_Data      := lv_return_message ;

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '150: Min Margin Not Defined : Processing further : Not an error');


      END IF;


      -- Send error message if no Item Cost is available for the shipping store
      IF lv_item_cost IS NULL
      THEN
         -- Satish U: 16-NOV-2011 : When Item Cost is not defined do not raise it as Error
         -- Request from Cristan on 16-NOV-2011
         lv_return_status := NULL;
         lv_Return_Message := NULL;
          -- Satish U: 18-DEC-2011
          x_Return_Status := lv_return_status ;
          x_Msg_Data      := lv_return_message ;

          xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '160: Item Cost Not Defined : Processing Further , Not an error');

      END IF;


      -- Send error message if no Item Cost is available for the shipping store
      IF lv_item_price IS NULL
      THEN
         lv_return_status := 'E';
         lv_return_message :=
            'Line:'||lv_line_num||'. Category or National Price for the item not available';

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '170: Item Price Not Defined : Not Processing further');
          -- Satish U: 18-DEC-2011
          x_Return_Status := lv_return_status ;
          x_Msg_Data      := lv_return_message ;
          Commit;
          RETURN;

      END IF;

      -- 20-MAR-2012 : CHeck if Sales Order line is Saved before making changes to Unit Selling PRice
       --04JAN2011 : Message Changed as per Cristan's note
      If   (lv_unit_sp <> lv_item_price)  AND lv_status_code = 'NULL' Then
         -- Satish U: 20-MAR-2012 :   Added new API to show consistent messages : Satish Ul
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '170.01: You must save Order line before changing Unit Selling Price' );

         Get_PGR_Message ( p_Line_Number    => lv_line_num,
                      p_Min_Margin     => lv_min_margin,
                      p_Max_Discount   => l_Max_Discount,
                      p_String1        => '. You must save order line before changing Unit Selling Price.' ,
                      x_Return_Message => lv_return_message ) ;
         lv_return_status := 'E';


         -- Satish U: 21-MAR-2012
          x_Return_Status := lv_return_status ;
          x_Msg_Data      := lv_return_message ;
          Commit;

      End IF;

      --Satish U: 22-Mar-2012 : Check If Manual Adjustments exists If Not then  Return
      -- Check for Header Level Modifiers too : Satish U: 05-APR-2012
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '170.012: Sales Order Line ID: ' || iv_Line_ID);

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                        p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                        P_DEBUG_MSG     => '170.013: Sales Order Header ID: ' || iv_Header_ID);

      --Satish 22-MAR-2012 :  If there are Manual Modifiers Exist then do notthing come out.
      If l_Manual_Adj_Exists = 'N'  Then
         -- Satish U: 04-APR-2012
         If lv_item_price <= lv_unit_sp And l_Auto_Adj_Exists  = 'Y' Then
             lv_return_status  := NULL;
             lv_return_message := NULL;
             x_Return_Status := lv_return_status ;
             x_Msg_Data      := lv_return_message ;
             --23-MAR-2012 : Satish U  Out Variables should be assigned values before leaving.
             x_Return_Status := lv_return_status ;
             x_Msg_Data      := lv_return_message ;
             xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                            p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                            P_DEBUG_MSG     => '170.02: There are no manual adjustments exists' );

             Return ;
         Elsif lv_item_price = lv_unit_sp  Then
             lv_return_status  := NULL;
             lv_return_message := NULL;
             x_Return_Status := lv_return_status ;
             x_Msg_Data      := lv_return_message ;
             --23-MAR-2012 : Satish U  Out Variables should be assigned values before leaving.
             x_Return_Status := lv_return_status ;
             x_Msg_Data      := lv_return_message ;
             xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>  C_LEVEL_EVENT,
                                            p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                            P_DEBUG_MSG     => '170.03: There are no manual adjustments exists' );
             Return ;

         End If;
      End IF;

      -- Satish U: 10/27/2011 : If Unit Selling Price is greather then List Price  or equal to List Price then Return
      If lv_item_price <= lv_unit_sp Then
         lv_return_status := 'S';
         lv_return_message := NULL;
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level =>  C_LEVEL_EVENT,
                                      p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                      P_DEBUG_MSG     => '170.1: Release Holds for Process OM Header: ');

         RELEASE_HOLD(p_header_id      => lv_Header_ID,
                      p_Line_ID        => iv_Line_ID    ,
                      p_hold_id        => lc_guardrail_hold    ,
                      x_Return_Status  => lv_Return_Status  ,
                      x_Msg_Data       => lv_return_message);

         If lv_Return_Status <> 'S' THen
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_ERROR,
                                           p_mod_name     => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG    => '170.2: Release Hold API Errored Out: ' || lv_Return_Message);
             -- Satish U: 05-MAR-2012 : As per Critans's Request
             lv_return_message  := ' Could not release Pricing Hold.' ;
             lv_return_status   := 'E';
         ELSE
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_ERROR,
                                           p_mod_name     => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG    => '170.3: Release Hold API Successful: ');
            -- Satish U: 05-MAR-2012 : As per Critans's Request
            lv_return_message  := ' Pricing Hold released successfully.' ;
         ENd If;

          -- Satish U: 18-DEC-2011
          x_Return_Status := lv_return_status ;
          x_Msg_Data      := lv_return_message ;
          Commit;
          Return ;

      End If;

      IF nvl(lv_item_price,0) <> 0 then
       lv_order_discount := ROUND ( (lv_item_price - lv_unit_sp) / lv_item_price * 100, 2);
      ELSE
       lv_order_discount := 0;
      END IF;
      -- Satish U: 22-NOV-2011  Handle NULL Value
      -- Satish U: 05-MAR-2012 : IF Unit Selling Price is zero then Order Margin Price will be zero.
      If lv_Unit_Sp = 0 Then
        lv_order_margin_price := 0;
      Else
         lv_order_margin_price :=  ROUND(((lv_unit_sp - NVL(lv_item_cost,0))/lv_unit_sp) * 100,2)  ;
      End If;


      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '171.1: lv_Order_Discount : ' || lv_order_discount);
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '171.2: lv_max_discount : ' || lv_max_discount);
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '172: lv_unit_sp : ' || lv_unit_sp);
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '173: lv_order_margin_price : ' || lv_order_margin_price);
      --Satish U: 08-DEC-2011
      -- WHen lv_main_Margin and lv_Max_Discount are NULL then do not do any checking.
      -- Satish U: Added Code 15-DEC-2011 : If lv_status_code Not In 'NULL'
      IF (lv_min_margin IS NOT NULL OR lv_max_discount IS NOT NULL ) AND
        ((lv_order_discount > NVL(lv_max_discount,100)) OR (lv_order_margin_price < lv_min_margin ))   THEN
         --((lv_order_discount > NVL(lv_max_discount,100)) OR (lv_order_margin_price > lv_unit_sp ))   THEN 19-DEC-2011 : Satish U
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '174.1: Before Checking Security Status: ' || lc_security_access);
        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '174.2: Manual Adj Flag Value: ' || l_Manual_Adj_Exists);
         -- ALso Allow Restricted User to go below the PRicing Guard Rails and Apply Hold
         -- Satish U: 04-12-2012 :  Apply Holds for Quotes Too.
         -- Satish U: 18-APR-2012 :  Removed DRAFT from the list : Added additional logic for DRAFT
         IF ( (( lc_security_access = 'Y' ) Or (G_OM_PG_DB_TRIGGER_FLAG = 'Y' AND G_OM_PG_ACCESS = 'N' ) )
            AND UPPER(lv_status_code) NOT IN ( 'DRAFT','PENDING_CUSTOMER_ACCEPTANCE','NULL')  AND l_Manual_Adj_Exists = 'Y' )  THEN

         --IF lc_security_access = 'Y' AND UPPER(lv_status_code) NOT IN ( 'DRAFT', 'NULL')  THEN
            -- 23-FEB-2012  Satish U:
            --IF lv_status_code NOT IN ( 'DRAFT', 'NULL')  THEN
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   =>   C_LEVEL_EVENT,
                                     p_mod_name      =>   C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     =>  '175: Before Apply Hold : ' || lv_status_code);
            apply_holds (lv_header_id
                       , iv_line_id
                       , lc_guardrail_hold
                       , lv_return_status
                       , lv_return_message);

            IF lv_return_status = 'S'
            THEN
               -- Satish U : 17-NOV-2011
               lv_return_status := 'S' ;
               -- Satish U: 05-MAR-2012  : Added following logic make PGR message consistent
               If lv_max_Discount is NULL THEN
                  l_Max_Discount := 'N/A';
               ELSE
                  l_Max_Discount := To_Char(lv_Max_Discount) ;
               END IF;
               Get_PGR_Message ( p_Line_Number    => lv_line_num,
                                 p_Min_Margin     => lv_min_margin,
                                 p_Max_Discount   => l_Max_Discount,
                                 p_String1        => '. Hold has been applied.' ,
                                 x_Return_Message => lv_return_message ) ;
               -- Commented old Code : 05-MAR-2012 : Satish U:
               --lv_return_message :=  'Line:'||lv_line_num||'. Pricing GuardRail Violation. Hold has been applied';
               xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  => C_LEVEL_EVENT,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '180: Pricing Guard Rail Hold Applied : ');
            ELSE
               -- Satish U: 05-MAR-2012  : Added following logic make PGR message consistent
               If lv_max_Discount is NULL THEN
                  l_Max_Discount := 'N/A';
               ELSE
                  l_Max_Discount := To_Char(lv_Max_Discount) ;
               END IF;
               -- Satish U: 08-DEC-2011
               If iv_line_ID is NULL THEN
                  lv_return_status := 'E';
                  -- Satish U: 05-MAR-2012 :   Added new API to show consistent messages : Satish Ul
                  Get_PGR_Message ( p_Line_Number    => lv_line_num,
                                 p_Min_Margin     => lv_min_margin,
                                 p_Max_Discount   => l_Max_Discount,
                                 p_String1        => '. Unable to apply PGR Hold, please save changes.' ,
                                 x_Return_Message => lv_return_message ) ;
                  -- Satish U: 05-MAR-2012 : Old code Commented
                  --lv_return_message :=  'Line:'||lv_line_num||'. Pricing GuardRail Violation. Unable to apply PGR Hold, please save changes. ';
                  xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                              p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                              P_DEBUG_MSG     => '185: Error while applying Pricing Guard Rail Hold: ');
               ELSE
                  lv_return_status := 'E';
                  -- Satish U: 05-MAR-2012 :   Added new API to show consistent messages : Satish Ul
                  Get_PGR_Message ( p_Line_Number    => lv_line_num,
                                 p_Min_Margin     => lv_min_margin,
                                 p_Max_Discount   => l_Max_Discount,
                                 p_String1        => '. Unable to apply PGR Hold.' ,
                                 x_Return_Message => lv_return_message ) ;
                  -- Satish U: 05-MAR-2012 : Old code Commented
                  --lv_return_message :=  'Line:'||lv_line_num||'. Pricing GuardRail Violation. Unable to apply PGR Hold ';
                  xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                              p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                              P_DEBUG_MSG     => '190: Error while applying Pricing Guard Rail Hold: ');
               END IF;
            END IF;


             -- Satish U: 18-DEC-2011
            x_Return_Status := lv_return_status ;
            x_Msg_Data      := lv_return_message ;
            Commit;
            Return;
            -- Satish U: 04-APR-2012 : Check if Manual Adjustments exists
         ELSIF ( lc_security_access = 'Y'  AND UPPER(lv_status_code) in ('DRAFT', 'PENDING_CUSTOMER_ACCEPTANCE' ) AND l_Manual_Adj_Exists = 'Y' )  THEN
            lv_return_status := 'S';
            -- Added Code to allow user to go below Pricing Guardrails in Quote. No Hold Will be created.
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '195:  Quotes : Pricing Guard Rail Violation: ');
            -- Satish U 27-FEB-2012
            -- Satish U: 18-APR2012 :   Added new API to show consistent messages : Satish Ul
            Get_PGR_Message ( p_Line_Number    => lv_line_num,
                              p_Min_Margin     => lv_min_margin,
                              p_Max_Discount   => l_Max_Discount,
                              p_String1        => '. Unit Selling Price below pricing guard rail.' ,
                              x_Return_Message => lv_return_message ) ;

             -- Satish U: 18-DEC-2011
            x_Return_Status := lv_return_status ;
            x_Msg_Data      := lv_return_message ;
            Commit;
            Return;

         ELSIF l_Manual_Adj_Exists = 'Y'   THen
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '200: Pricing Guard Rail Violation: ');
            lv_return_status := 'E';
            If lv_max_Discount is NULL THEN
               l_Max_Discount := 'N/A';
            ELSE
               l_Max_Discount := To_Char(lv_Max_Discount) ;
            END IF;
            If lc_security_access = 'Y'  AND lv_status_code = 'NULL' Then
               -- Satish U: 05-MAR-2012 :   Added new API to show consistent messages : Satish Ul
               Get_PGR_Message ( p_Line_Number    => lv_line_num,
                                 p_Min_Margin     => lv_min_margin,
                                 p_Max_Discount   => l_Max_Discount,
                                 p_String1        => '. You must save order line before changing Unit Selling Price.' ,
                                 x_Return_Message => lv_return_message ) ;

            Else
               -- Satish U: 05-MAR-2012 :   Added new API to show consistent messages : Satish Ul
               Get_PGR_Message ( p_Line_Number    => lv_line_num,
                                 p_Min_Margin     => lv_min_margin,
                                 p_Max_Discount   => l_Max_Discount,
                                 p_String1        => '.Please adjust the Order Price.' ,
                                 x_Return_Message => lv_return_message ) ;
                -- Satish U: 05-MAR-2012 : Old code Commented

            End IF;

            -- Satish U: 17-NOV-2011 : Insert Record into Table : Pricing GuardRail Viloation happened

            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '201: Header ID : ' || iv_Header_ID);
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '202: Line ID : ' || iv_Line_ID);
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '203: Return Status : ' || lv_Return_Status);
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_EVENT,
                                           p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG     => '204: Return Message : ' || lv_Return_Message);

             -- Satish U: 18-DEC-2011
            x_Return_Status := lv_return_status ;
            x_Msg_Data      := lv_return_message ;
            Commit;
            Return;


         END IF;
      ELSE

         lv_return_status := 'S';
         lv_return_message := NULL;
         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level =>  C_LEVEL_EVENT,
                                      p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                      P_DEBUG_MSG     => '210: Release Holds for Process OM Header: ');

         RELEASE_HOLD(p_header_id      => lv_Header_ID,
                      p_Line_ID        => iv_Line_ID    ,
                      p_hold_id        => lc_guardrail_hold    ,
                      x_Return_Status  => lv_Return_Status  ,
                      x_Msg_Data       => lv_return_message);

         If lv_Return_Status <> 'S' THen
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_ERROR,
                                           p_mod_name     => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG    => '220: Release Hold API Errored Out: ' || lv_Return_Message);
            -- Satish U: 05-MAR-2012 : As per Critans's Request
            lv_return_message  := ' Could not release Pricing Hold.' ;
            lv_return_status := 'E';
         ELSE
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level  =>C_LEVEL_ERROR,
                                           p_mod_name     => C_MODULE_NAME || l_API_NAME,
                                           P_DEBUG_MSG    => '230: Release Hold API Successful: ');
            -- Satish U: 05-MAR-2012 : As per Critans's Request
            lv_return_message  := ' Pricing Hold released successfully.' ;
         ENd If;

          -- Satish U: 18-DEC-2011
          x_Return_Status := lv_return_status ;
          x_Msg_Data      := lv_return_message ;
      END IF;

      COMMIT;
   END DBTRigger_PGR_Check;

   PROCEDURE process_om_header (iv_header_id          IN     NUMBER)
   IS
   lv_return_status varchar2(1) := 'S';
   lv_return_message varchar2(32000);
   ov_return_status varchar2(1) := 'S';
   ov_return_message varchar2(32000);
   l_API_Name              Varchar2(30):=  '.PROCESS_OM_HEADER' ;
   cursor c1
       is
   SELECT a.inventory_item_id
        , a.ordered_item
        , a.unit_selling_price
        , a.ship_from_org_id
        , NVL (a.pricing_date, b.ordered_date) pricing_date
        , a.header_id
        , b.flow_status_code
        , a.unit_cost
        , a.unit_list_price
        , b.order_type_id
        , a.line_id
     FROM apps.oe_order_lines_v a, apps.oe_order_headers b
    WHERE a.header_id = b.header_id
      AND b.header_id = iv_header_id
      AND a.LINE_CATEGORY_CODE='ORDER';

   BEGIN
       -- Satish U : 10/18/2011
      /****************
      Delete_temp_info (p_Header_ID  => iv_Header_ID,
                        p_Line_Id    => NULL );
     ****************/
      --Get unit price from the order line

     Begin
        Select 'Y'
        Into G_PRE_BOOK_FLAG
        From apps.Oe_Order_Headers
        Where Header_ID = iv_header_id;
     Exception
        When Others Then
           G_PRE_BOOK_FLAG         := 'N';
     ENd ;

     for c1_rec in c1

       loop
             process_om_line (iv_line_id          => c1_rec.line_id,
                              iv_item_id          => c1_rec.inventory_item_id,
                              iv_unit_sp          => c1_rec.unit_selling_price,
                              iv_ship_from_org_id => c1_rec.ship_from_org_id,
                              iv_pricing_date     => to_date(c1_rec.pricing_date,'RRRR/MM/DD HH24:MI:SS'),
                              iv_header_id        => c1_rec.header_id,
                              iv_flow_code        => c1_rec.flow_status_code,
                              iv_unit_cost        => c1_rec.unit_cost,
                              iv_list_price       => c1_rec.unit_list_price,
                              iv_order_type_id    => c1_rec.order_type_id);

        --fnd_profile.get('XXWC_OM_PG_MSG',lv_return_message);
        --fnd_profile.get('XXWC_OM_PG_STATUS',lv_return_status);

       --IF lv_return_status = 'E' then
       --  ov_return_status := 'E';
       --END IF;
       --IF lv_return_message is not null then
       --  ov_return_message := ov_return_message ||'::'||lv_return_message;
       --END IF;
       end loop;
       --ov_return_message := substr(ov_return_message,1,2000);
       -- Inserting Into Global Temp Table SatishU : 10/17/2011
       /****************
         Update_Status(p_Header_ID      => iv_Header_ID ,
                       P_Line_ID        => NULL,
                       p_Return_Status  => ov_Return_Status,
                       p_Return_Message => ov_Return_Message)  ;
       *******************/

       G_PRE_BOOK_FLAG := 'N';
   END process_om_header;
  /************************************************************************************************************
   PROCEDURE RELEASE_HOLD  :
   Satish U : 08-FEB-2013 : # 20130208-01001 : Pass P_HOLD_ID to Release_Holds API
  *************************************************************************************************************/

  PROCEDURE RELEASE_HOLD(p_header_id            IN   NUMBER    ,
                         p_Line_ID              In   Number    ,
                         p_hold_id              IN   NUMBER    ,
                         x_Return_Status        OUT  VARCHAR2  ,
                         x_Msg_Data             OUT  VARCHAR2) IS

     L_HOLD_ID          NUMBER;
     L_MSG_COUNT        NUMBER;
     l_msg_data         VARCHAR2(4000);

     L_HOLD_SOURCE_REC  OE_HOLDS_PVT.HOLD_SOURCE_REC_TYPE;
     L_HOLD_RELEASE_REC OE_HOLDS_PVT.Hold_Release_Rec_Type;

     L_ORDER_TBL        OE_HOLDS_PVT.ORDER_TBL_TYPE;
     l_Holds_Count      Number;

     RELEASE_EXCEPTION  EXCEPTION;
     l_realease_comment     Varchar2(80) :=  ' Automatic Release. Unit Selling Price above Pricing Guard Rail';
     l_release_reason_code  Varchar2(30) := Fnd_Profile.Value('XXWC_PG_HOLD_RELEASE_REASON');
     l_API_Name             Varchar2(30):=  '.RELEASE_HOLD' ;
     --- Satish U: 02-FEB-2012
     l_Booked_Flag            Varchar2(1) ;
     l_LU_Holds_Count         Number;
     L_LU_HOLD_ID             NUMBER;
  BEGIN

     l_hold_source_rec := NULL;
     l_order_tbl.DELETE;
     l_Msg_Count       := 0;
     l_msg_data        :='';

     BEGIN
        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                       p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                       P_DEBUG_MSG     => '7000: Begining of API');

        l_order_tbl(1).header_id := p_header_id;
        l_order_tbl(1).Line_id   := P_line_Id;


        Select Count(*)
        Into l_Holds_Count
        From   apps.oe_order_holds ooh,
              apps.oe_hold_sources ohs
        Where ooh.Header_Id = P_header_id
        And   ooh.Line_ID  =  P_line_Id
        And   ohs.Hold_Id  =  P_hold_ID
        And   ooh.Hold_Release_ID is Null
        And   ooh.hold_source_ID = ohs.Hold_Source_ID ;

        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '7005: Full User Hold Count : ' || l_Holds_Count);
        --x_msg_data := ('Before calling oracle api');
        If l_Holds_Count  > 0 Then
           l_order_tbl(1).Line_id   := p_Line_id;
           xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '7010: Full User Hold Exists : Calling Release Holds API for releasing Hold ID:' || P_HOLD_ID);
           -- SatishU :  Pass P_HOLD_ID :08-FEB-2013
           OE_Holds_PUB.Release_Holds (
                               p_api_version         => 1.0,
                               p_order_tbl           => l_order_tbl,
                               p_hold_id             => P_HOLD_ID, -- l_hold_id, Satish U : 08-FEB-2013 : # 20130208-01001
                               p_release_reason_code => l_release_reason_code,
                               p_release_comment     => l_realease_comment,
                               x_return_status       => x_return_status,
                               x_msg_count           => l_msg_count,
                               x_msg_data            => x_msg_data);

           IF x_Return_Status = fnd_api.g_ret_sts_success THEN
              xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7011: Order Line Hold Released : Release Hold API Successful: ');
              --- Satish U : 12/03/2012 : Check how many line level holds exists if None then release Pricing Guard Header Level Hold Too.
              Select Count(*)
              Into l_Holds_Count
              From  apps.oe_order_holds ooh,
                    apps.oe_hold_sources ohs
              Where ooh.Header_Id = P_header_id
              And   ooh.Line_ID  Is Not NULL
              And   ohs.Hold_Id  =  P_hold_ID
              And   ooh.Hold_Release_ID is Null
              And   ooh.hold_source_ID = ohs.Hold_Source_ID ;
              xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7021: Full User Hold Count : ' || l_Holds_Count);

              If l_Holds_Count = 0 THen
                 l_order_tbl(1).Line_id   := NULL;
                 xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7022: Order Header Hold Exists : Calling Release Holds API : For Header');
                 OE_Holds_PUB.Release_Holds (
                                       p_api_version         => 1.0,
                                       p_order_tbl           => l_order_tbl,
                                       p_hold_id             => P_HOLD_ID, -- l_hold_id, Satish U : 08-FEB-2013 : # 20130208-01001
                                       p_release_reason_code => l_release_reason_code,
                                       p_release_comment     => l_realease_comment,
                                       x_return_status       => x_return_status,
                                       x_msg_count           => l_msg_count,
                                       x_msg_data            => x_msg_data);
                 If x_Return_Status = fnd_api.g_ret_sts_success THEN

                    Null;
                    x_Msg_Data := 'Pricing Guard Rail Order Header Hold Released.' ;
                    xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7023: Release Hold API Successful: ' );
                 End If;

              End IF ;
           End If;

           IF x_Return_Status <> fnd_api.g_ret_sts_success THEN

              FOR l_index IN 1..l_msg_count  LOOP
                 l_msg_data :=  oe_msg_pub.get
                                      (p_msg_index      => l_index ,
                                       p_encoded        => 'F');

              x_msg_data := x_msg_data || ' :' || l_msg_data;

              END LOOP;
              xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '7024: Release Hold API Errored Out : ' || x_msg_data);


           END IF; -- End If for Checking Return Status from Apply_Holds


        End IF;
        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level        => C_LEVEL_EVENT,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '7031: Check if Pricing Guard Rail Limited User Hods exists: ' );

        l_order_tbl(1).Line_id   := p_Line_id;
        l_LU_HOLD_ID             := G_OM_PG_HOLD_LU_BOOK ;

        Select Count(*)
        Into l_LU_Holds_Count
        From  apps.oe_order_holds ooh,
              apps.oe_hold_sources ohs
        Where ooh.Header_Id      = P_header_id
        And   ooh.Line_ID        =  P_line_Id
        And   ohs.Hold_Id        =  l_LU_HOLD_ID
        And   ooh.Hold_Release_ID is Null
        And   ooh.hold_source_ID = ohs.Hold_Source_ID ;

        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                          p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                          P_DEBUG_MSG     => '7032: Check if Pricing Guard Rail Limited User Hods exists : Order Is  Booked: ' );


        If l_LU_Holds_Count  > 0 Then
           xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '7035: Limited User Hold Exists : Calling Release Holds API');
           OE_Holds_PUB.Release_Holds (
                               p_api_version         => 1.0,
                               p_order_tbl           => l_order_tbl,
                               p_hold_id             => l_LU_hold_id,
                               p_release_reason_code => l_release_reason_code,
                               p_release_comment     => l_realease_comment,
                               x_return_status       => x_return_status,
                               x_msg_count           => l_msg_count,
                               x_msg_data            => x_msg_data);


           IF x_Return_Status <> fnd_api.g_ret_sts_success THEN
              FOR l_index IN 1..l_msg_count  LOOP
                 l_msg_data :=  oe_msg_pub.get
                                      (p_msg_index      => l_index ,
                                       p_encoded        => 'F');

              x_msg_data := x_msg_data || ' :' || l_msg_data;

              END LOOP;
              xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '7036: Limited user :Release Hold API Errored Out : ' || x_msg_data);
           ELSE
              Null;
              x_Msg_Data := 'Pricing Guard Rail Hold Released.' ;
              xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '7037: Limited User :Release Hold API Successful: ' );
           END IF; -- End If for Checking Return Status from Apply_Holds


        End IF;

     EXCEPTION
        WHEN OTHERS THEN
           x_Msg_Data  := x_msg_data || ' ****When Others Error Occured while Releasing Hold to Order Header '
                     || SQLCODE || '-'|| SQLERRM ;
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '7040: Release Hold API Errored Out : ' || x_msg_data);

           RAISE RELEASE_EXCEPTION;

     END;


  EXCEPTION
     WHEN RELEASE_EXCEPTION THEN
        x_return_status := fnd_api.g_ret_sts_error;


     WHEN OTHERS THEN
        x_return_status := fnd_api.g_ret_sts_error;
        x_Msg_Data      := x_Msg_Data|| '- '|| sqlerrm;
        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '7050: Release Hold API Errored Out : ' || x_msg_data);
  END RELEASE_HOLD;

 Procedure Update_Status(p_Header_ID   In Number ,
                           P_Line_ID     In Number,
                           p_Return_Status In Varchar2,
                           p_Return_Message in Varchar2)  IS


   l_Row_COunt Number ;
   l_API_NAME  Varchar2(80) := 'UPDATE_STATUS';
 Begin
    Select Count(*)
    Into l_Row_Count
    From  apps.XXWC_PRICING_GUARD_RAILS_GT
    Where Header_ID = p_Header_ID
    And  (p_Line_ID is NULL or Line_ID = P_Line_ID ) ;

    xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                   p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                   P_DEBUG_MSG     => 'Row Count is  : ' || l_Row_Count);

    If l_Row_Count = 0 THen
       Insert Into apps.XXWC_PRICING_GUARD_RAILS_GT
           (
           Header_ID,
           Line_ID,
           Return_Status,
           Return_Message
           )
           Values (
           p_Header_ID,
           p_Line_Id ,
           p_Return_Status,
           P_Return_Message
           );


    Elsif l_Row_Count > 0 THen
       Update apps.XXWC_PRICING_GUARD_RAILS_GT
          Set Return_Status = p_Return_Status ,
              Return_Message = p_Return_Message
       Where Header_ID = p_Header_ID
       And   ( p_Line_Id is Null or line_Id = p_line_ID );



    End If;
  End Update_Status ;

  FUNCTION get_temp_info_GT(p_Header_ID  In Number,
                          p_Line_ID    In Number,
                          p_info_type VARCHAR2)  RETURN VARCHAR IS
     lv_Return_Status  Varchar2(1)    := Null;
     lv_Return_Message Varchar2(2000) := Null;

     Cursor Get_info_Cur is
        Select Return_Status, Return_Message
        From apps.XXWC_PRICING_GUARD_RAILS_GT
        Where Header_Id = p_Header_ID
        And   (p_Line_Id is Null or Line_ID   = p_Line_ID );

  Begin
     For Get_Info_Rec in Get_Info_Cur Loop
         lv_Return_Status := Get_Info_Rec.Return_Status;
         lv_Return_Message := Get_Info_Rec.Return_Message ;
     End Loop;

     IF p_info_type = 'S' then
      return(lv_return_status);
     ELSE
      return(lv_return_message);
     END IF;
  End get_temp_info_GT;

  Procedure Delete_temp_info (p_Header_ID  In Number,
                           p_Line_ID    In Number) Is
  Begin
     Delete From apps.XXWC_PRICING_GUARD_RAILS_GT
     Where Header_Id = p_Header_ID
     and   Line_ID   = P_Line_ID ;
  End  Delete_Temp_Info;

  Function  Get_Pricing_Guard_Rail_ID (p_Inventory_Item_ID  In Number,
                                       p_organization_ID    In Number ) Return Varchar2 IS
    l_Pricing_GUard_Rail_ID   Number;
    l_Category_ID             Number;
    l_Price_Zone              Varchar2(40);

  BEGIN

     -- Get Pricing Zone Informatiuon
     Begin
        Select Attribute6
        Into l_price_zone
        From apps.Mtl_Parameters
        Where Organization_ID = p_organization_Id ;
         Dbms_Output.Put_Line( ' PRicing ZOne is :' || l_Price_Zone );
     Exception
        When Others Then
           ReTurn (0);
     End ;


     SELECT Pricing_GuardRail_ID
     INTO l_Pricing_GUard_Rail_ID
     FROM apps.XXWC_OM_PRICING_GUARDRAIL
     WHERE price_zone = l_price_zone
     AND inventory_item_id = p_Inventory_Item_ID
     AND  Status = C_PGR_STATUS_CURRENT
     And rownum = 1;
     Dbms_Output.Put_Line( ' PRicing Guard Raild ID :' || l_Pricing_GUard_Rail_ID );
     Return l_Pricing_GUard_Rail_ID;

     --AND iv_ordered_date BETWEEN start_date AND NVL (end_date, iv_ordered_date + 1);


   EXCEPTION
      WHEN OTHERS  THEN
         Begin
            Dbms_Output.Put_Line( ' Get Category ID  :' );
            Dbms_Output.Put_Line( ' Inventory Item ID  :'  ||p_Inventory_Item_ID );
            Dbms_Output.Put_Line( ' Organization ID  :'  ||p_Organization_ID );
            SELECT Pg.Pricing_GuardRail_ID
            INTO l_Pricing_Guard_Rail_ID
            FROM  apps.MTL_DEFAULT_CATEGORY_SETS_FK_V b
                , apps.mtl_item_categories c
                , apps.XXWC_OM_PRICING_GUARDRAIL pg
            WHERE    b.functional_area_id = 1
            AND   b.category_set_id  = c.category_set_id
            AND   c.inventory_item_id = p_Inventory_Item_ID
            --And c.Category_Id       = b.category_ID
            AND ( c.organization_id is NULL Or
                c.organization_id   = p_Organization_ID)
            AND pg.Item_Category_ID = c.Category_ID
            AND PG.Status = C_PGR_STATUS_CURRENT
            AND ROWNUM = 1;

            Dbms_Output.Put_Line( ' Category ID  :' || l_Category_ID );
            Return l_Pricing_Guard_Rail_ID ;

         Exception
            When Others Then
               RETURN (NULL);
         End ;



  End Get_Pricing_Guard_Rail_ID ;


   Procedure  Insert_Record ( P_Pricing_GuardRail_Rec  In XXWC_OM_PRICING_GUARDRAIL%ROWTYPE,
                              x_Return_Status          Out NoCopy Varchar2,
                              x_Msg_Data               Out NoCopy Varchar2)  IS


     l_Count Number ;
     C_STATUS_CURRENT   Constant Varchar2(30) := 'CURRENT';

     Cursor  PG_Item_Cat_Cur (P_Category_ID Number ,
                              p_Price_Zone  Varchar2 ) IS
        Select Start_Date, End_Date
        From apps.XXWC_OM_PRICING_GUARDRAIL
        Where Price_Zone       = p_Price_Zone
        And   Item_Category_ID = p_Category_ID
        And   Status           = 'CURRENT';

     Cursor  PG_Item_Cur (P_Inventory_Item_ID Number ,
                          p_Price_Zone        Varchar2 ) IS
        Select Start_Date, End_Date
        From apps.XXWC_OM_PRICING_GUARDRAIL
        Where Price_Zone       = p_Price_Zone
        And   Item_Category_ID = p_Inventory_Item_ID
        And   Status           = 'CURRENT';
   Begin
      x_Return_Status := 'S';

        SELECT Count(*)
        INTO l_Count
        FROM apps.xxwc_om_pricing_guardrail
        WHERE price_zone = P_Pricing_GuardRail_Rec.price_zone
        AND   Status     = 'CURRENT'
        AND NVL (item_category_id, -1) = NVL (P_Pricing_GuardRail_Rec.item_category_id, -1)
        AND NVL (inventory_item_id, -1) = NVL (P_Pricing_GuardRail_Rec.inventory_item_id, -1)
        AND ( P_Pricing_GuardRail_Rec.start_date BETWEEN start_date AND NVL (end_date, To_Date('01-JAN-4712','DD-MON-RRRR'))
        Or NVL (P_Pricing_GuardRail_Rec.end_date, To_Date('01-JAN-4712','DD-MON-RRRR')) BETWEEN start_date
                                                AND NVL (end_date, To_Date('01-JAN-4712','DD-MON-RRRR'))) ;
        --AND ( :xxwc_om_pricing_guardrail.Pricing_GuardRail_Id IS Null
        --  Or Pricing_GuardRail_Id <>:xxwc_om_pricing_guardrail.Pricing_GuardRail_Id)
        --AND ROWNUM = 1;

       If l_Count > 0 Then
          x_Return_Status := 'E';
          x_Msg_Data      := 'Duplicate Record Exists. Can not create Pricing GuardRail Record.';
       Elsif l_Count = 0 THen
          insert into apps.XXWC_OM_PRICING_GUARDRAIL
            ( Pricing_GuardRail_ID,
            price_zone,
            item_category_id,
            inventory_item_id,
            min_margin,
            max_discount,
            start_date,
            end_date,
            creation_date,
            created_by,
            last_update_date,
            last_updated_by,
            last_update_login,
            Status,
            Org_id            )
         values
           ( XXWC_OM_PRICING_GUARDRAIL_S.NextVal ,
            P_Pricing_GuardRail_Rec.price_zone,
            P_Pricing_GuardRail_Rec.Item_category_id,
            P_Pricing_GuardRail_Rec.Inventory_item_id,
            P_Pricing_GuardRail_Rec.min_margin,
            P_Pricing_GuardRail_Rec.max_discount,
            P_Pricing_GuardRail_Rec.start_date,
            P_Pricing_GuardRail_Rec.end_date,
            sysdate,
            fnd_global.user_id,
            sysdate,
            fnd_global.user_id,
            fnd_global.login_id,
            C_STATUS_CURRENT,
            mo_global.get_current_org_id ());
       End If ;
   Exception
      When others Then
          X_Return_Status := 'U';
          x_Msg_Data := 'When Others Error : While creating Pricing GuardRail Record';
      -- Check For Duplicacies
   End Insert_Record;

   /********************************************************************************
   **** Procedure  DB_Trigger_Apply_holds
   *** History
   *** Satish U : 03-DEC-2012 : Added Code to Apply Holds at Order Header Level and
   ***                          Holds are not released automatically after one day
   **********************************************************************************/

  PROCEDURE DB_Trigger_Apply_holds (
                 iv_header_id         IN   NUMBER
               , iv_line_id           IN   NUMBER
               , iv_Org_ID            IN   NUMBER
               , iv_guardrail_hold    IN   NUMBER
               , ov_return_status    OUT VARCHAR2
               , ov_return_message   OUT VARCHAR2) IS

      --PRAGMA AUTONOMOUS_TRANSACTION;

      lv_return_status     VARCHAR2 (30);
      lv_msg_data          VARCHAR2 (4000);
      lv_msg_count         NUMBER;
      lv_hold_source_rec   OE_HOLDS_PVT.HOLD_SOURCE_REC_TYPE;
      lv_hold_exists       Varchar2(1);
      lv_Holds_Count       Number;
      l_API_Name           Varchar2(30):=  '.DB_Trigger_Apply_holds' ;
      l_Org_ID             Number;
   BEGIN
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '100: Apply Holds Begining : ' );

      l_Org_ID    := iv_Org_ID ;
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '101: Responsibility ID : ' || fnd_global.resp_id );

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '102: Resp Application ID : ' || fnd_global.resp_appl_id );

      --/************
      fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                  resp_id        => fnd_global.resp_id,
                                  resp_appl_id   => fnd_global.resp_appl_id);


      mo_global.set_policy_context ('S', l_org_id);




      --***************/
      lv_hold_source_rec                  := OE_HOLDS_PVT.G_MISS_HOLD_SOURCE_REC;
      lv_hold_source_rec.hold_id          := iv_guardrail_hold;
      lv_hold_source_rec.hold_entity_code := 'O';          -- order level hold
      lv_hold_source_rec.hold_entity_id   := iv_header_id; -- header_id of the order
      lv_hold_source_rec.header_id        := iv_header_id;
      lv_hold_source_rec.line_id          := iv_line_id;       -- line__id of the order
      -- Satish U : Holds will not be released next day. 12/03/2012
      lv_hold_source_rec.hold_until_date  := NULL; -- SYSDATE + 1;
      lv_return_status                    := NULL;
      lv_msg_data                         := NULL;
      lv_msg_count                        := NULL;

      --Satish U: 12/03/2012 : Check if  Hold exists at the Header Level
      -- Check if Hold exists for this Order Line
      Select Count(*)
      Into lv_Holds_Count
      From   apps.oe_order_holds ooh,
             apps.oe_hold_sources ohs
      Where ooh.Header_Id = iv_header_id
      And   ooh.Line_ID  Is NULL
      and   ohs.Hold_Id  = iv_guardrail_hold
      And   ooh.Hold_Release_ID is Null
      And   ooh.hold_source_ID = ohs.Hold_Source_ID ;

      -- Apply Hold at Header Level
      If lv_Holds_Count = 0 Then
           -- 15-DEC-2011  Coomit ws turned off
           --/* Commented by Shankar 26-Jul-2012
           -- Uncommented as it is a stop gap solution
           lv_hold_source_rec.line_id          := NULL;
           OE_Holds_PUB.Apply_Holds (p_api_version       => 1.0
                              , p_init_msg_list     => FND_API.G_FALSE
                              , p_commit            => FND_API.G_TRUE
                              , p_hold_source_rec   => lv_hold_source_rec
                              , x_return_status     => lv_return_status
                              , x_msg_count         => lv_msg_count
                              , x_msg_data          => lv_msg_data);

      Else
         -- Do not apply Another Hold
         lv_return_status := FND_API.G_RET_STS_SUCCESS;
      End If;
      -- End of Adding code to apply hold order header level

      IF lv_Return_Status = FND_API.G_RET_STS_SUCCESS Then
          -- Check if Hold exists for this Order Line
          Select Count(*)
          Into lv_Holds_Count
          From   apps.oe_order_holds ooh,
                 apps.oe_hold_sources ohs
          Where ooh.Header_Id = iv_header_id
          And   ooh.Line_ID  =  iv_line_Id
          and   ohs.Hold_Id  = iv_guardrail_hold
          And   ooh.Hold_Release_ID is Null
          And   ooh.hold_source_ID = ohs.Hold_Source_ID ;

          If lv_Holds_Count = 0 Then
               -- 15-DEC-2011  Coomit ws turned off
               --/* Commented by Shankar 26-Jul-2012
               -- Uncommented as it is a stop gap solution
               lv_hold_source_rec.line_id          := iv_line_id;
               OE_Holds_PUB.Apply_Holds (p_api_version       => 1.0
                                  , p_init_msg_list     => FND_API.G_FALSE
                                  , p_commit            => FND_API.G_TRUE
                                  , p_hold_source_rec   => lv_hold_source_rec
                                  , x_return_status     => lv_return_status
                                  , x_msg_count         => lv_msg_count
                                  , x_msg_data          => lv_msg_data);

          Else
             -- Do not apply Another Hold
             lv_return_status := FND_API.G_RET_STS_SUCCESS;
          End If;
      End If;

      IF lv_return_status = FND_API.G_RET_STS_SUCCESS       THEN
         ov_return_status := 'S';
      ELSE
         -- Satish U: 08-DEC-2011 Added following logic to retrieve error message
         FOR K IN 1 .. lv_msg_count loop
            lv_msg_data := oe_msg_pub.get( p_msg_index => k, p_encoded => 'F'  );
         End Loop ;
         ov_return_status := 'E';
         ov_return_message := lv_msg_data;

         xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '110: Apply Holds Failed : ' || lv_msg_data);

         -- Inserting Into Global Temp Table SatishU : 10/17/2011
          Update_Status(p_Header_ID       => iv_Header_ID ,
                        P_Line_ID         => IV_Line_ID,
                        p_Return_Status   => ov_Return_Status,
                        p_Return_Message  => ov_Return_Message)  ;

      END IF;
      --COMMIT;
   EXCEPTION
      WHEN OTHERS       THEN
         ov_return_status := 'E';
         ov_return_message :=
            'Exception Applying Holds-' || SUBSTR (SQLERRM, 1, 80);
   End DB_Trigger_Apply_holds ;

   /********************************************************************************
   **** Procedure  DB_TRIGGER_RELEASE_HOLD
   *** History
   *** Satish U : 03-DEC-2012 : Release Header Level Hold if All Line Level Holds are released
   *** Satish U: 08-FEB-2013 : # 20130208-01001 :  Pass P_HOLD_ID to release Holds API
   **********************************************************************************/

   PROCEDURE DB_TRIGGER_RELEASE_HOLD(p_header_id IN   NUMBER    ,
                          p_Line_ID              In   Number    ,
                          p_hold_id              IN   NUMBER    ,
                          P_Org_ID               IN   NUMBER    ,
                          x_Return_Status        OUT  VARCHAR2  ,
                          x_Msg_Data             OUT VARCHAR2) IS
     --PRAGMA AUTONOMOUS_TRANSACTION;
     L_HOLD_ID          NUMBER;
     L_MSG_COUNT        NUMBER;
     l_msg_data         VARCHAR2(4000);

     L_HOLD_SOURCE_REC  OE_HOLDS_PVT.HOLD_SOURCE_REC_TYPE;
     L_HOLD_RELEASE_REC OE_HOLDS_PVT.Hold_Release_Rec_Type;

     L_ORDER_TBL        OE_HOLDS_PVT.ORDER_TBL_TYPE;
     l_Holds_Count      Number;

     RELEASE_EXCEPTION  EXCEPTION;
     l_realease_comment     Varchar2(80) :=  ' Automatic Release. Unit Selling Price above Pricing Guard Rail';
     l_release_reason_code  Varchar2(30) := Fnd_Profile.Value('XXWC_PG_HOLD_RELEASE_REASON');
     l_API_Name             Varchar2(30):=  '.DB_TRIGGER_RELEASE_HOLD' ;
     l_Org_ID               Number;
  BEGIN
     xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                    p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                    P_DEBUG_MSG     => '7000: Begining of API');


      l_Org_ID    := p_Org_ID ;
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '7001: Responsibility ID : ' || fnd_global.resp_id );

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '7002: Resp Application ID : ' || fnd_global.resp_appl_id );


      fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                  resp_id        => fnd_global.resp_id,
                                  resp_appl_id   => fnd_global.resp_appl_id);

      mo_global.set_policy_context ('S', l_org_id);

      --***************/
     l_hold_source_rec := NULL;
     l_order_tbl.DELETE;
     l_Msg_Count       := 0;
     l_msg_data        :='';

     BEGIN
        --SAVEPOINT RELEASE_HOLD;
        l_order_tbl(1).header_id := p_header_id;
        l_order_tbl(1).Line_id   := p_Line_id;

        Select Count(*)
        Into l_Holds_Count
        From   apps.oe_order_holds ooh,
              apps.oe_hold_sources ohs
        Where ooh.Header_Id = P_header_id
        And   ooh.Line_ID  =  P_line_Id
        And   ohs.Hold_Id  =  P_hold_ID
        And   ooh.Hold_Release_ID is Null
        And   ooh.hold_source_ID = ohs.Hold_Source_ID ;

        --x_msg_data := ('Before calling oracle api');
        If l_Holds_Count  > 0 Then
           xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '7010: Hold Exists : Calling Release Holds API for releasing Hold ID :' || P_hold_ID);
           -- SatishU :  Pass P_HOLD_ID :08-FEB-2013
           OE_Holds_PUB.Release_Holds (
                               p_api_version         => 1.0,
                               p_commit              => FND_API.G_TRUE,
                               p_order_tbl           => l_order_tbl,
                               p_hold_id             => P_HOLD_ID, --l_hold_id, Satish U : 08-FEB-2013 : # 20130208-01001
                               p_release_reason_code => l_release_reason_code,
                               p_release_comment     => l_realease_comment,
                               x_return_status       => x_return_status,
                               x_msg_count           => l_msg_count,
                               x_msg_data            => x_msg_data);

           -- Satish U: 03-DEC-2012 : Added Code to check if all line level holds are released then release Header Level Hold
           IF x_Return_Status =  fnd_api.g_ret_sts_success THEN

              xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '7011: Line Level Holds Released Successfully');
              -- Check if All Line Level Holds are released
              Select Count(*)
              Into l_Holds_Count
              From   apps.oe_order_holds ooh,
                     apps.oe_hold_sources ohs
              Where ooh.Header_Id = P_header_id
              And   ooh.Line_ID  IS NOT NULL
              And   ohs.Hold_Id  =  P_hold_ID
              And   ooh.Hold_Release_ID is Null
              And   ooh.hold_source_ID = ohs.Hold_Source_ID ;

              If l_Holds_Count  = 0 Then
                  xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '7011: Line Level Holds Do not exists');
                  -- Release Header Level Hold
                  l_order_tbl(1).Line_id   := NULL;
                  OE_Holds_PUB.Release_Holds (
                               p_api_version         => 1.0,
                               p_commit              => FND_API.G_TRUE,
                               p_order_tbl           => l_order_tbl,
                               p_hold_id             => P_HOLD_ID, -- l_hold_id, Satish U : 08-FEB-2013 : # 20130208-01001
                               p_release_reason_code => l_release_reason_code,
                               p_release_comment     => l_realease_comment,
                               x_return_status       => x_return_status,
                               x_msg_count           => l_msg_count,
                               x_msg_data            => x_msg_data);
                   If x_Return_Status = fnd_api.g_ret_sts_success THEN

                      x_Msg_Data := 'Pricing Guard Rail Hold Released Successfully' ;
                      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_EVENT,
                                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                     P_DEBUG_MSG     => '7012: Order Header Release Hold API Successful: ' );

                   End If;
              End IF;

              IF x_Return_Status <> fnd_api.g_ret_sts_success THEN
                 FOR l_index IN 1..l_msg_count  LOOP
                     l_msg_data :=  oe_msg_pub.get
                                          (p_msg_index      => l_index ,
                                           p_encoded        => 'F');

                  x_msg_data := x_msg_data || ' :' || l_msg_data;

                  END LOOP;
                  xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7020: Release Hold API Errored Out : ' || x_msg_data);

              END IF; -- End If for Checking Return Status from Apply_Holds
           End IF;
        End IF;

     EXCEPTION
        WHEN OTHERS THEN
           x_Msg_Data  := x_msg_data || ' ****When Others Error Occured while Releasing Hold to Order Header '
                     || SQLCODE || '-'|| SQLERRM ;
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '7040: Release Hold API Errored Out : ' || x_msg_data);

           RAISE RELEASE_EXCEPTION;

     END;

     COMMIT;
  EXCEPTION
     WHEN RELEASE_EXCEPTION THEN
        x_return_status := fnd_api.g_ret_sts_error;
        --ROLLBACK TO RELEASE_HOLD;

     WHEN OTHERS THEN
        x_return_status := fnd_api.g_ret_sts_error;
        x_Msg_Data      := x_Msg_Data|| '- '|| sqlerrm;
        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                       p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                       P_DEBUG_MSG     => '7050: Release Hold API Errored Out : ' || x_msg_data);
         --ROLLBACK TO RELEASE_HOLD;

   END DB_TRIGGER_RELEASE_HOLD;

   FUNCTION GET_PGR_MESSAGE(p_Header_ID  In Number,
                           p_Line_ID    In Number,
                           p_info_type VARCHAR2)  RETURN VARCHAR IS
     lv_Return_Status  Varchar2(1)    := Null;
     lv_Return_Message Varchar2(2000) := Null;

     Cursor Get_info_Cur is
        Select Return_Status, Return_Message
        From apps.XXWC_PGR_MESSAGE
        Where Header_Id = p_Header_ID
        And   Line_ID   = p_Line_ID ;

   Begin
      For Get_Info_Rec in Get_Info_Cur Loop
         lv_Return_Status := Get_Info_Rec.Return_Status;
         lv_Return_Message := Get_Info_Rec.Return_Message ;
      End Loop;

      IF p_info_type = 'S' then
         return(lv_return_status);
      ELSE
         return(lv_return_message);
      END IF;
   End GET_PGR_MESSAGE;

   Procedure DELETE_MESSAGE (p_Header_ID  In Number,
                             p_Line_ID    In Number) IS

   PRAGMA AUTONOMOUS_TRANSACTION;
   Begin
      Delete From apps.XXWC_PGR_MESSAGE
      Where Header_Id = p_Header_ID
      and   Line_ID   = P_Line_ID ;

      Commit;

   End DELETE_MESSAGE;

 Procedure Update_Message(p_Header_ID     In Number ,
                           P_Line_ID        In Number,
                           p_Return_Status  In Varchar2,
                           p_Return_Message in Varchar2) IS
   l_Row_COunt Number ;
   l_API_NAME  Varchar2(80) := 'UPDATE_STATUS';

   PRAGMA AUTONOMOUS_TRANSACTION;
 Begin
    Select Count(*)
    Into l_Row_Count
    From  apps.XXWC_PGR_MESSAGE
    Where Header_ID = p_Header_ID
    And  Line_ID = P_Line_ID  ;



    If l_Row_Count = 0 THen
       xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                   p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                   P_DEBUG_MSG     => ' 100 : API Update Message : Inserting Record : ' );
       Insert Into apps.XXWC_PGR_MESSAGE
           (
           Header_ID,
           Line_ID,
           Return_Status,
           Return_Message
           )
           Values (
           p_Header_ID,
           p_Line_Id ,
           p_Return_Status,
           P_Return_Message
           );


    Elsif l_Row_Count > 0 THen
       xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                   p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                   P_DEBUG_MSG     => ' 110 : API Update Message : Updating Record : ' );

       Update apps.XXWC_PGR_MESSAGE
          Set Return_Status = p_Return_Status ,
              Return_Message = p_Return_Message
       Where Header_ID = p_Header_ID
       And   line_Id = p_line_ID ;



    End If;
    Commit;

  End Update_Message ;

   Function IS_PREBOOK_HOLD_EXIST(p_Header_ID In Number)  Return Varchar2 Is

   Cursor Hold_Cur (P_Header_ID  Number ) Is
      Select Count(*) Hold_Count
       From   apps.oe_order_holds ooh,
              apps.oe_hold_sources ohs,
              apps.oe_order_Lines ool
       Where ooh.Header_Id      = P_header_id
       And   ooh.Line_Id        = ool.Line_ID
       And   ohs.Hold_Id        = G_OM_PG_HOLD_LU_BOOK
       And   ooh.Header_Id      = ool.Header_Id
       And   ooh.Hold_Release_ID is Null
       And   ooh.hold_source_ID = ohs.Hold_Source_ID ;




       l_Hold_Count  Number := 0;
       l_API_NAME    Varchar2(30) := 'IS_PREBOOK_HOLD_EXIST' ;
   Begin
      -- Pricing GuardRail Holds for Limited User
      G_OM_PG_HOLD_LU_BOOK    := fnd_profile.VALUE ('XXWC_OM_PG_HOLD_LU_BOOK');
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => ' 210 : Limited User Hold ID : ' || G_OM_PG_HOLD_LU_BOOK );
      For Hold_Rec In Hold_Cur (p_Header_ID ) Loop
        l_Hold_Count := Hold_Rec.Hold_Count ;
      End Loop;
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => ' 220 : Hold Count  : ' || l_Hold_Count);
      If l_Hold_Count = 0 Then
         Return 'N' ;
      Else
         Return 'Y' ;
      End If;
   End IS_PREBOOK_HOLD_EXIST;

    /********************************************************************************
   **** Procedure  DB_TRIGGER_RELEASE_HOLD2
   *** History
   *** Satish U : 03-DEC-2012 : Release all Line Level Holds if Header Level Hold is released
   *** Satish U : 08-FEB-2013 : # 20130208-01001 :  Added NVL functions and additional Debuging Statements
   **********************************************************************************/

   PROCEDURE DB_TRIGGER_RELEASE_HOLD2(p_header_id IN   NUMBER    ,
                          p_hold_id              IN   NUMBER    ,
                          P_Org_ID               IN   NUMBER    ,
                          x_Return_Status        OUT  VARCHAR2  ,
                          x_Msg_Data             OUT VARCHAR2) IS
     PRAGMA AUTONOMOUS_TRANSACTION;
     L_HOLD_ID          NUMBER;
     L_MSG_COUNT        NUMBER;
     l_msg_data         VARCHAR2(4000);

     L_HOLD_SOURCE_REC  OE_HOLDS_PVT.HOLD_SOURCE_REC_TYPE;
     L_HOLD_RELEASE_REC OE_HOLDS_PVT.Hold_Release_Rec_Type;

     L_ORDER_TBL        OE_HOLDS_PVT.ORDER_TBL_TYPE;
     l_Holds_Count      Number;

     RELEASE_EXCEPTION  EXCEPTION;
     l_realease_comment     Varchar2(80) :=  'Automatic Release. Order Header Pricing Guard Rail hold is released.';
     l_release_reason_code  Varchar2(30) := Fnd_Profile.Value('XXWC_PG_HOLD_RELEASE_REASON');
     --l_API_Name             Varchar2(80) :=  'PLSQL.XXWC_OM_PRICING_GUARDRAIL_PKG.DB_TRIGGER_RELEASE_HOLD2' ;
     l_API_Name             Varchar2(80) :=  'DB_TRIGGER_RELEASE_HOLD2' ;
     l_Org_ID               Number;
      l_OM_PG_Hold_ID         Number  := NVL(fnd_profile.VALUE ('XXWC_OM_PG_HOLD'),0); -- Satish U : 08-FEB-2013 : # 20130208-01001 Added NVL Function
      l_OM_PG_HOLD_LU_BOOK    Number  := NVL(fnd_profile.VALUE ('XXWC_OM_PG_HOLD_LU_BOOK'),0); -- Satish U : 08-FEB-2013 : # 20130208-01001 Added NVL Function

     -- Define a Cursor that gets list of All Lines that have Pricing Guard Rail Holds not released
     Cursor  Cur_SO_Line_Holds (p_Hold_ID Number,
                           p_SO_Header_ID Number )  Is
        Select ooh.Order_Hold_Id, ooh.Header_Id, ooh.Line_Id
        From  apps.oe_order_holds ooh,
              apps.oe_hold_sources ohs
        Where ooh.Header_Id      =  P_SO_Header_ID
        And   ooh.Line_ID       is Not NULL
        And   ohs.Hold_Id        =  P_HOLD_ID
        AND   ohs.Hold_ID        in (l_OM_PG_Hold_ID, l_OM_PG_HOLD_LU_BOOK )
        And   ooh.Hold_Release_ID is Null
        And   ooh.hold_source_ID = ohs.Hold_Source_ID ;

  BEGIN
     xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                    p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                    P_DEBUG_MSG     => '7000: Begining of API');


      l_Org_ID    := p_Org_ID ;
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '7001: Responsibility ID : ' || fnd_global.resp_id );

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '7002: Resp Application ID : ' || fnd_global.resp_appl_id );


      fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                  resp_id        => fnd_global.resp_id,
                                  resp_appl_id   => fnd_global.resp_appl_id);

      mo_global.set_policy_context ('S', l_org_id);


     l_hold_source_rec := NULL;
     l_order_tbl.DELETE;
     l_Msg_Count       := 0;
     l_msg_data        :='';

     BEGIN
        -- Initialize Return Status
        x_Return_Status :=  fnd_api.g_ret_sts_success ;
        l_msg_data := NULL;
        l_msg_count := NULL;

        -- Loop Through all Cursor and Release All The Holds at the Line Level
        For  Hold_rec In Cur_SO_Line_Holds (p_Hold_ID      => P_HOLD_ID,
                                       p_SO_Header_ID => P_Header_ID ) Loop
            --SAVEPOINT RELEASE_HOLD;
            l_order_tbl(1).header_id := Hold_rec.header_id;
            l_order_tbl(1).Line_id   := Hold_rec.Line_id;

           xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '7010: Hold Exists : Calling Release Holds API for Hold ID :' || P_HOLD_ID);
           OE_Holds_PUB.Release_Holds (
                               p_api_version         => 1.0,
                               p_commit              => FND_API.G_TRUE,
                               p_order_tbl           => l_order_tbl,
                               p_hold_id             => p_hold_id,
                               p_release_reason_code => l_release_reason_code,
                               p_release_comment     => l_realease_comment,
                               x_return_status       => x_return_status,
                               x_msg_count           => l_msg_count,
                               x_msg_data            => x_msg_data);
            -- Satish U : 08-FEB-2013 : # 20130208-01001 : Added following Debug statement
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7015: Release Holds API status: ' || x_return_status);

              IF x_Return_Status <> fnd_api.g_ret_sts_success THEN
                 FOR l_index IN 1..l_msg_count  LOOP
                     l_msg_data :=  oe_msg_pub.get
                                          (p_msg_index      => l_index ,
                                           p_encoded        => 'F');

                  x_msg_data := x_msg_data || ' :' || l_msg_data;

                  END LOOP;
                  xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7020: Release Hold API Errored Out : ' || x_msg_data);
              Else
                 -- Satish U : 08-FEB-2013 : # 20130208-01001 : Added Following Debug Statement
                 xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7030: Release Hold API is successful: ' );

              END IF; -- End If for Checking Return Status from Apply_Holds

         End Loop ;
     EXCEPTION
        WHEN OTHERS THEN
           x_Msg_Data  := x_msg_data || ' ****When Others Error Occured while Releasing Hold at Line Level'
                     || SQLCODE || '-'|| SQLERRM ;
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '7040: Release Hold API Errored Out : ' || x_msg_data);

           RAISE RELEASE_EXCEPTION;

     END;

     COMMIT;
  EXCEPTION
     WHEN RELEASE_EXCEPTION THEN
        x_return_status := fnd_api.g_ret_sts_error;
        --ROLLBACK TO RELEASE_HOLD;

     WHEN OTHERS THEN
        x_return_status := fnd_api.g_ret_sts_error;
        x_Msg_Data      := x_Msg_Data|| '- '|| sqlerrm;
        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                       p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                       P_DEBUG_MSG     => '7050: Release Hold API Errored Out : ' || x_msg_data);
         --ROLLBACK TO RELEASE_HOLD;

   END DB_TRIGGER_RELEASE_HOLD2;
   
  /*******************************************************************************
  * function:   check_record_locked
  * Description: This function will attempt to lock the hold source and holds records 
  *              prior to attempt releasing the hold. If record cannot be locked, it will 
  *              be skipped and attempted in the following run.
 HISTORY
 ===============================================================================
 VERSION DATE          AUTHOR(S)       DESCRIPTION
 ------- -----------   --------------- -----------------------------------------
 1.0     12/2292014    Shankar Hariharan  Initial creation of the procedure
 ********************************************************************************/

function check_record_locked (p_order_hold_id in number, p_hold_source_id in number) return boolean
is
   cursor c
       is
   select 'dummy'
     from apps.oe_order_holds
    where order_hold_id = p_order_hold_id
      for update nowait;
      
   cursor c1
       is
   select 'dummy'
     from apps.oe_hold_sources
    where hold_source_id = p_hold_source_id
      for update nowait;
      
 
  e_resource_busy exception;
  pragma exception_init(e_resource_busy,-54);
 begin
   open c;
   close c;

   open c1;
   close c1;

   --dbms_output.put_line('Record ' || to_char(p_id) || ' is not locked.');
   --rollback;
   return FALSE;
 exception
  when e_resource_busy then
  --  dbms_output.put_line('Record ' || to_char(p_id) || ' is locked.');
   return TRUE;
 end check_record_locked;
  
   
  /********************************************************************************
   **** Procedure  PGR_LINE_HOLD_RELEASE
     * Description: This procedure will be called from a concurrent program to  
                  Release all Line Level Holds if Header Level Hold is released 
   *** History
   ===============================================================================
     VERSION DATE  AUTHOR(S)       DESCRIPTION
    -----------   --------------- -----------------------------------------   
    29-DEC-2014 : Shankar H       Initial Creation TMS 20140723-00285
   **********************************************************************************/

   PROCEDURE PGR_LINE_HOLD_RELEASE(RETCODE     OUT NUMBER
                               , ERRMSG    OUT VARCHAR2)
   IS
     PRAGMA AUTONOMOUS_TRANSACTION;
     L_HOLD_ID          NUMBER;
     L_MSG_COUNT        NUMBER;
     l_msg_data         VARCHAR2(4000);

     L_HOLD_SOURCE_REC  OE_HOLDS_PVT.HOLD_SOURCE_REC_TYPE;
     L_HOLD_RELEASE_REC OE_HOLDS_PVT.Hold_Release_Rec_Type;

     L_ORDER_TBL        OE_HOLDS_PVT.ORDER_TBL_TYPE;
     l_Holds_Count      Number;
     x_Return_Status    VARCHAR2(30);
     X_Msg_Data         Varchar2(2000);
     l_conc_req_id      Number := fnd_global.conc_request_id;

     RELEASE_EXCEPTION  EXCEPTION;
     l_realease_comment     Varchar2(80) :=  'Automatic Release. Order Header Pricing Guard Rail hold is released.';
     l_release_reason_code  Varchar2(30) := Fnd_Profile.Value('XXWC_PG_HOLD_RELEASE_REASON');
     l_API_Name             Varchar2(80) :=  'PGR_LINE_HOLD_RELEASE' ;
     l_Org_ID               Number;
     l_OM_PG_Hold_ID         Number  := NVL(fnd_profile.VALUE ('XXWC_OM_PG_HOLD'),0); 
     l_OM_PG_HOLD_LU_BOOK    Number  := NVL(fnd_profile.VALUE ('XXWC_OM_PG_HOLD_LU_BOOK'),0); 
     l_skip_record           Varchar2(1);
     
     Cursor Cur_Order_List (p_org_id in number) Is
        Select header_id, hold_id, rowid, creation_date
          from apps.xxwc_om_pgr_hold_release_tbl
         where process_flag = 'N'
           and org_id = p_org_id;
     
     -- Define a Cursor that gets list of All Lines that have Pricing Guard Rail Holds not released
     Cursor  Cur_SO_Line_Holds (p_hold_id number, p_header_id number, p_creation_date date) Is
        Select ooh.Order_Hold_Id, ooh.Header_Id, ooh.Line_Id, pgr.order_number, ohs.hold_source_id
        From  apps.oe_order_holds ooh,
              apps.oe_hold_sources ohs,
              apps.oe_order_headers pgr
        Where ooh.Header_Id      =  P_Header_ID
        And   ooh.Line_ID       is Not NULL
        And   ohs.Hold_Id        =  P_HOLD_ID
        AND   ohs.Hold_ID        in (l_OM_PG_Hold_ID, l_OM_PG_HOLD_LU_BOOK )
        And   ooh.Hold_Release_ID is Null
        And   ooh.hold_source_ID = ohs.Hold_Source_ID
        AND   ooh.header_id = pgr.header_id
        AND   ooh.creation_date <= p_creation_date;

  BEGIN
     xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                    p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                    P_DEBUG_MSG     => '7000: Begining of API');

      l_Org_ID    := fnd_global.Org_ID ;
      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '7001: Responsibility ID : ' || fnd_global.resp_id );

      xxwc_gen_routines_pkg.LOG_MSG (p_debug_level    => C_LEVEL_ERROR,
                                     p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                     P_DEBUG_MSG     => '7002: Resp Application ID : ' || fnd_global.resp_appl_id );


      fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                  resp_id        => fnd_global.resp_id,
                                  resp_appl_id   => fnd_global.resp_appl_id);

      mo_global.set_policy_context ('S', l_org_id);


     l_hold_source_rec := NULL;
     l_order_tbl.DELETE;
     l_Msg_Count       := 0;
     l_msg_data        :='';

     BEGIN

        IF to_number(to_char(sysdate,'HH24')) > 22 then
         delete from apps.xxwc_om_pgr_hold_release_tbl
         where process_flag = 'Y'
           and org_id = l_org_id
           and last_update_date < sysdate-2;
           commit;
        END IF;

        For cur_order_rec in cur_order_list(l_org_id)
        loop
        x_Return_Status :=  fnd_api.g_ret_sts_success ;
        l_msg_data := NULL;
        l_msg_count := NULL;
        l_skip_record := 'N';

        -- Loop Through all Cursor and Release All The Holds at the Line Level
        For  Hold_rec In Cur_SO_Line_Holds (p_Hold_ID      => cur_order_rec.HOLD_ID,
                                            p_Header_ID => cur_order_rec.Header_ID
                                           ,p_creation_date => cur_order_rec.creation_date ) Loop
           --SAVEPOINT RELEASE_HOLD;
           l_order_tbl(1).header_id := Hold_rec.header_id;
           l_order_tbl(1).Line_id   := Hold_rec.Line_id;
           
           IF NOT check_record_locked (Hold_rec.order_hold_id, Hold_rec.hold_source_id ) then

           xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                         p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                         P_DEBUG_MSG     => '7010: Hold Exists : Calling Release Holds API for Hold ID :' || cur_order_rec.HOLD_ID);
                                         
           OE_Holds_PUB.Release_Holds (
                               p_api_version         => 1.0,
                               p_commit              => FND_API.G_TRUE,
                               p_order_tbl           => l_order_tbl,
                               p_hold_id             => cur_order_rec.hold_id,
                               p_release_reason_code => l_release_reason_code,
                               p_release_comment     => l_realease_comment,
                               x_return_status       => x_return_status,
                               x_msg_count           => l_msg_count,
                               x_msg_data            => x_msg_data);
                               
           commit;
           
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7015: Release Holds API status: ' || x_return_status);

              IF x_Return_Status <> fnd_api.g_ret_sts_success THEN
                 FOR l_index IN 1..l_msg_count  LOOP
                     l_msg_data :=  oe_msg_pub.get
                                          (p_msg_index      => l_index ,
                                           p_encoded        => 'F');

                  x_msg_data := x_msg_data || ' :' || l_msg_data;

                  END LOOP;
                  xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7020: Release Hold API Errored Out : ' || x_msg_data);
              Else

                 xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                                 p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                                 P_DEBUG_MSG     => '7030: Release Hold API is successful: ' );
                                                 
                 fnd_file.put_line (fnd_file.output, 'Released Line '||Hold_rec.line_id ||' for Order '||hold_rec.order_number);                                

              END IF; -- End If for Checking Return Status from Apply_Holds
           ELSE
             fnd_file.put_line (fnd_file.output, 'Unable to Lock Line '||Hold_rec.line_id ||' for Order '||hold_rec.order_number);
             l_skip_record := 'Y';
           END IF;
         End Loop ;
         
         IF l_skip_record = 'N' then
            update apps.xxwc_om_pgr_hold_release_tbl
               set process_flag = 'Y'
             where rowid= cur_order_rec.rowid;
         END IF;
         commit;
        End Loop; 
     EXCEPTION
        WHEN OTHERS THEN
           x_Msg_Data  := x_msg_data || ' ****When Others Error Occured while Releasing Hold at Line Level'
                     || SQLCODE || '-'|| SQLERRM ;
            xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                             p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                             P_DEBUG_MSG     => '7040: Release Hold API Errored Out : ' || x_msg_data);

           RAISE RELEASE_EXCEPTION;

     END;

     COMMIT;
  EXCEPTION
     WHEN RELEASE_EXCEPTION THEN
        x_return_status := fnd_api.g_ret_sts_error;
        xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'xxwc_om_pricing_guardrail_pkg.pgr_line_hold_release'
           ,p_calling             => 'xxwc_om_pricing_guardrail_pkg.pgr_line_hold_release'
           ,p_request_id          => l_conc_req_id
           ,p_ora_error_msg       => x_msg_data
           ,p_error_desc          => 'Error running Pricing Guardrail Hold Line release'
           ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
           ,p_module              => 'ONT');

     WHEN OTHERS THEN
        x_return_status := fnd_api.g_ret_sts_error;
        x_Msg_Data      := x_Msg_Data|| '- '|| sqlerrm;
        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                       p_mod_name      => C_MODULE_NAME || l_API_NAME,
                                       P_DEBUG_MSG     => '7050: Release Hold API Errored Out : ' || x_msg_data);
        xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'xxwc_om_pricing_guardrail_pkg.pgr_line_hold_release'
           ,p_calling             => 'xxwc_om_pricing_guardrail_pkg.pgr_line_hold_release'
           ,p_request_id          => l_conc_req_id
           ,p_ora_error_msg       => x_msg_data
           ,p_error_desc          => 'Error running Pricing Guardrail Hold Line release'
           ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
           ,p_module              => 'ONT');

        ERRMSG := x_msg_data;
        RETCODE := '2';


   END PGR_LINE_HOLD_RELEASE;

   
   

-- 06/24/2013 CG: TMS 20130618-01074: Added new procedure to programatically release PGR Holds
  /********************************************************************************
   ProcedureName : RELEASE_PGR_HOLDS
   Purpose       : 

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   2.1     17-AUG-2016   Niraj K Ranjan   TMS#20160801-00059   Lines stuck in Awaiting Invoice interface on hold when multiple holds applied
   ********************************************************************************/ 
    PROCEDURE release_pgr_holds (RETCODE     OUT NUMBER
                               , ERRMSG    OUT VARCHAR2
                               , P_APPLIED_AGE  IN NUMBER)
    IS
        l_error_message2 clob;
        l_conc_req_id   number := fnd_global.conc_request_id;
        l_return_status     varchar2(10) := null;
        l_msg_count         number := null;
        l_msg_data          varchar2(300) := null;
        l_hold_source_rec  OE_HOLDS_PVT.hold_source_rec_type := OE_HOLDS_PVT.G_MISS_HOLD_SOURCE_REC;
        l_hold_release_rec  OE_HOLDS_PVT.Hold_Release_Rec_Type := OE_HOLDS_PVT.G_MISS_HOLD_RELEASE_REC;

        l_release_reason_code VARCHAR2(240) := 'MGR_APPROVAL_HLD';
        l_release_comment VARCHAR2(240) := 'Automatic Release';
        l_release_hold      VARCHAR2(1);
        l_count             NUMBER;
        l_commit_count      NUMBER;

        l_hold_activity    VARCHAR2(250);
        l_item_type        VARCHAR2(10);
        l_activity         VARCHAR2(250);
        l_result           VARCHAR2(10);  
        
        cursor cur_pgr_hold is
                select  oh.order_number
                        , (ol.line_number||(decode(ol.line_number,null,null,'.'))||ol.shipment_number) Line_number
                        , ooha.order_hold_id
                        , ooha.hold_source_id
                        , ooha.header_id
                        , ooha.line_id
                        , ooha.creation_date apply_date
                        , ohsa.hold_entity_code
                        , ohsa.hold_entity_id
                        , ohd.hold_id
                        , ohd.name hold_name
                        , ohd.type_code hold_type_code
                        , (select count(ol2.line_id) 
                            from    apps.oe_order_holds ooha2
                                    , apps.oe_hold_sources ohsa2
                                    , apps.oe_hold_definitions ohd2
                                    , apps.oe_order_lines ol2
                            where   ooha2.hold_source_id = ohsa2.hold_source_id
                            and     ohsa2.hold_release_id is null
                            and     ohsa2.hold_id = ohd2.hold_id
                            and     ohd2.type_code = 'GUARD'
                            and     ooha2.header_id = ooha.header_id 
                            and     ooha.line_id is null           
                            and     ooha2.line_id = ol2.line_id) total_ln_pgr_holds
                        , (select count(ol2.line_id) 
                            from    apps.oe_order_holds ooha2
                                    , apps.oe_hold_sources ohsa2
                                    , apps.oe_hold_definitions ohd2
                                    , apps.oe_order_lines ol2
                            where   ooha2.hold_source_id = ohsa2.hold_source_id
                            and     trunc(ooha2.creation_date) = (trunc(sysdate) - P_APPLIED_AGE)
                            and     ohsa2.hold_release_id is null
                            and     ohsa2.hold_id = ohd2.hold_id
                            and     ohd2.type_code = 'GUARD'
                            and     ooha2.header_id = ooha.header_id 
                            and     ooha.line_id is null
                            and     ooha2.line_id = ol2.line_id) ln_pgr_hold_same_apply_date
                from    apps.oe_order_holds ooha
                        , apps.oe_hold_sources ohsa
                        , apps.oe_hold_definitions ohd
                        , apps.oe_order_headers oh
                        , apps.oe_order_lines ol
                where   ooha.hold_source_id = ohsa.hold_source_id
                and     trunc(ooha.creation_date) = (trunc(sysdate) - P_APPLIED_AGE)
                and     ohsa.hold_release_id is null
                and     ohsa.hold_id = ohd.hold_id
                and     ohd.type_code = 'GUARD'
                and     ooha.header_id = oh.header_id
                and     ooha.line_id = ol.line_id(+)
                order by 1,2 asc nulls first;
          
    BEGIN
        fnd_file.put_line (fnd_file.output, 'Starting procedure to release Pricing Guardrail Holds...');
        fnd_file.put_line (fnd_file.output, '========================================================');
        
        
        l_count             := 0;
        l_commit_count      := 0;
        for c1 in cur_pgr_hold
        Loop
            exit when cur_pgr_hold%notfound;

            l_release_hold      := null;
            fnd_file.put_line (fnd_file.output, ' ');
            fnd_file.put_line (fnd_file.output, 'Order Number/Line Number: '||c1.order_number||'/'||c1.line_number);
            fnd_file.put_line (fnd_file.output, 'Count total PGR Line Holds: '||c1.total_ln_pgr_holds);
            fnd_file.put_line (fnd_file.output, 'Count PGR Line Holds with same apply date as Header: '||c1.ln_pgr_hold_same_apply_date);
            
            if c1.line_id is null and c1.total_ln_pgr_holds != c1.ln_pgr_hold_same_apply_date then
                l_release_hold := 'N';
                
                fnd_file.put_line (fnd_file.output, 'Order Header hold will not be released');
            else
                l_release_hold := 'Y';
            end if;

            if nvl(l_release_hold,'N') = 'Y' then
                l_hold_source_rec.hold_source_id := c1.hold_source_id ;
                l_hold_source_rec.hold_id := c1.hold_id ;
                l_hold_source_rec.hold_entity_code := c1.hold_entity_code ;
                l_hold_source_rec.hold_entity_id := c1.hold_entity_id ;
                l_hold_source_rec.header_id :=  c1.header_id ;
                l_hold_source_rec.line_id :=  c1.line_id ;

                l_hold_release_rec.hold_source_id := c1.hold_source_id ;
                l_hold_release_rec.release_reason_code := l_release_reason_code ;
                l_hold_release_rec.order_hold_id := c1.order_hold_id;

                oe_holds_pub.Release_Holds  ( p_api_version             => 1.0
                                              , p_init_msg_list         => FND_API.G_TRUE
                                              , p_commit                => FND_API.G_TRUE
                                              , p_validation_level      => FND_API.G_VALID_LEVEL_FULL
                                              , p_hold_source_rec       => l_hold_source_rec
                                              , p_hold_release_rec      => l_hold_release_rec
                                              , x_return_status         => l_return_status
                                              , x_msg_count             => l_msg_count
                                              , x_msg_data              => l_msg_data);


                fnd_file.put_line (fnd_file.output,'Status: '||l_return_status);

                if l_return_status <> 'S' then
                    fnd_file.put_line (fnd_file.output,'Msg Cnt: '||l_msg_count);

                    IF l_msg_count > 1 THEN

                            FOR i IN 1 .. l_msg_count
                            LOOP
                                fnd_file.put_line (fnd_file.output, 'Msg (' || i ||'): '|| SUBSTR (fnd_msg_pub.get(p_encoded => fnd_api.g_false), 1, 255) );
                            END LOOP;
                    ELSE
                        fnd_file.put_line (fnd_file.output, 'Msg: ' || l_msg_data);
                    END IF;

                else
                    l_count             := l_count + 1;
                    l_commit_count      := l_commit_count + 1;
                    
                    if c1.line_id is not null then
                        -- Progressing Line
                        fnd_file.put_line (fnd_file.output,'Attempting to progress line...');
                        l_hold_activity    := null;
                        l_item_type        := null;
                        l_activity         := null;
                        l_result           := null;

                        BEGIN
                            SELECT activity_name
                            INTO   l_hold_activity
                            FROM   apps.oe_hold_definitions
                            WHERE  hold_id = c1.hold_id;
                        EXCEPTION
                            WHEN NO_DATA_FOUND THEN
                                l_hold_activity := null;
                                fnd_file.put_line (fnd_file.output,'Hold Activity name not found!!');
                            WHEN OTHERS THEN
                                l_hold_activity := null;
                                fnd_file.put_line (fnd_file.output,'Error looking for hold activity name. Error: '||SQLERRM);
                        END;
                        
                        if l_hold_activity is not null then
                     
							   fnd_file.put_line (fnd_file.output,'Fetch lines for order:'||c1.order_number); --Ver 2.1
                               FOR c2 IN (SELECT ool.line_id
                                          FROM oe_order_lines_all ool
                                          WHERE 1=1
                                          AND ool.header_id = c1.header_id
                                          AND ool.flow_status_code = 'INVOICE_HOLD'
                                          AND ool.cancelled_flag = 'N'
                                          AND ool.ordered_quantity > 0
										 ) --Ver 2.1
                               LOOP
							      begin
                                     SELECT wpa_to.process_name || ':' || wpa_to.activity_name,
                                            wias_to.item_type
                                     INTO   l_activity, l_item_type
                                     FROM   wf_item_activity_statuses wias_to,
                                            wf_process_activities wpa_to,
                                            wf_activities wa,
                                            wf_item_activity_statuses wias_from,
                                            wf_process_activities wpa_from,
                                            wf_activity_transitions wat
                                     WHERE  wpa_to.instance_id= wias_to.process_activity
                                     AND    wat.to_process_activity = wpa_to.instance_id
                                     AND    wat.result_code = 'ON_HOLD'
                                     AND    wias_from.process_activity = wat.from_process_activity
                                     AND    wpa_from.instance_id = wias_from.process_activity
                                     AND    wpa_from.activity_name = l_hold_activity  -- 8284926
                                     AND    wias_from.activity_result_code = 'ON_HOLD'
                                     AND    wias_from.end_date IS NOT NULL
                                     AND    wias_from.item_type = 'OEOL'
                                     AND    wias_from.item_key =  To_Char(c2.line_id) --Ver 2.1
                                     AND    wa.item_type = wias_to.item_type
                                     AND    wa.NAME = wpa_to.activity_name
                                     AND    wa.FUNCTION = 'OE_STANDARD_WF.STANDARD_BLOCK'
                                     AND    wa.end_date IS NULL
                                     AND    wias_to.end_date IS NULL
                                     AND    wias_to.activity_status = 'NOTIFIED'
                                     AND    wias_to.item_type = wias_from.item_type
                                     AND    wias_to.item_key = wias_from.item_key;
                                     
                                     fnd_file.put_line (fnd_file.output,'Line WF Progress. Processing item type and activity: '||l_item_type||'/'||l_activity||' for header/line '||c1.header_id||'/'||c2.line_id);
                                     
                                     wf_engine.CompleteActivity(l_item_type, To_Char(c2.line_id),l_activity, l_result);
                                     
                                     fnd_file.put_line (fnd_file.output,'Line progression result '||l_result);
								  exception
                                    when others then
                                      l_item_type        := null;
                                      l_activity         := null;
                                      fnd_file.put_line (fnd_file.output,'Could not get activity ID for header/line: '||c1.header_id||'/'||c2.line_id);
                                  end;
                               END LOOP;    

                        end if;
                        
                    end if;

                    if l_commit_count >= 100 then
                        commit;
                        l_commit_count := 0;
                    end if;

                end if;

            end if;
            
        END LOOP ;

        fnd_file.put_line (fnd_file.output, ' ');
        fnd_file.put_line (fnd_file.output, '========================================================');
        fnd_file.put_line (fnd_file.output,'Released '||l_count||' PGR Holds');

        COMMIT;
        
    EXCEPTION
    WHEN OTHERS THEN
        l_error_message2 := 'XXWC_OM_PRICING_GUARDRAIL_PKG.release_pgr_holds '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();
    
        fnd_file.put_line (fnd_file.output,'Stat:'||l_return_status);
        fnd_file.put_line (fnd_file.output,'Msg:'||l_msg_data);
        fnd_file.put_line (fnd_file.output,'Err:'||l_error_message2);
        
        xxcus_error_pkg.xxcus_error_main_api
                                            (p_called_from            => 'XXWC_OM_PRICING_GUARDRAIL_PKG.release_pgr_holds',
                                             p_calling                => 'Exception in concurrent program to release pricing guardrail holds',
                                             p_request_id             => l_conc_req_id,
                                             p_ora_error_msg          => l_error_message2,
                                             p_error_desc             => 'When others Exception ',
                                             p_distribution_list      => 'HDSOracleDevelopers@hdsupply.com',
                                             p_module                 => 'ONT'
                                            );

        ERRMSG := SQLERRM;
        retcode := 2;
        
        rollback;
        raise;
    END release_pgr_holds;


END XXWC_OM_PRICING_GUARDRAIL_PKG;
/