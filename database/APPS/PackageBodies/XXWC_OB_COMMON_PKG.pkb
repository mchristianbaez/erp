--
-- XXWC_OB_COMMON_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ob_common_pkg
AS
   /********************************************************************************
   FILE NAME: XXWC_OB_COMMON_PKG.pkg

   PROGRAM TYPE: PL/SQL Package Body

   PURPOSE: Common API for Outbound Interfaces.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)            DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     10/20/2011    Gopi Damuluri        Initial creation of the procedure
   1.1     09/29/2014    Maharajan Shunmugam  TMS# 20141001-00157 Canada Multi-Org changes
   ********************************************************************************/

   --Email Defaults
   pl_dflt_email   fnd_user.email_address%TYPE
                      := 'HDSOracleDevelopers@hdsupply.com';

   /********************************************************************************
   ProcedureName : CREATE_FILE
   Purpose       : Common API for Outbound Interfaces.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
   1.1     09/29/2014    Maharajan Shunmugam  TMS# 20141001-00157 Canada Multi-Org changes
   ********************************************************************************/
   PROCEDURE CREATE_FILE (p_errbuf              OUT VARCHAR2
                         ,p_retcode             OUT NUMBER
                         ,p_interface_name   IN     VARCHAR2
                         ,p_view_name        IN     VARCHAR2
                         ,p_directory_path   IN     VARCHAR2
                         ,p_file_name        IN     VARCHAR2
                         ,p_org_name         IN     VARCHAR2)
   IS
      -- Intialize Variables
      l_err_msg          VARCHAR2 (2000);
      l_err_code         NUMBER;
      l_sec              VARCHAR2 (150);
      l_procedure_name   VARCHAR2 (75) := 'xxwc_ob_common_pkg.create_file';

      -- Reference Cursor Variables
      TYPE ref_cur IS REF CURSOR;

      view_cur           ref_cur;
      view_rec           xxwc_ob_common_pkg.xxcus_ob_file_rec;

      --File Variables
      l_file_handle      UTL_FILE.file_type;
      l_file_name        VARCHAR2 (150);
      l_file_dir         VARCHAR2 (100);
      l_file_name_temp   VARCHAR2 (150);
      l_org_id           NUMBER;
      l_org_name         VARCHAR2 (240);
      l_rec_cnt          NUMBER := 0;
      l_dir_exists       NUMBER;

      l_program_error    EXCEPTION;
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG
        ,'**********************************************************************************');
      fnd_file.put_line (
         fnd_file.LOG
        ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Start of Procedure : create_file');
      fnd_file.put_line (
         fnd_file.LOG
        ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Input Parameters     :');
      fnd_file.put_line (fnd_file.LOG
                        ,'p_interface_name     :' || p_interface_name);
      fnd_file.put_line (fnd_file.LOG
                        ,'p_view_name          :' || p_view_name);
      fnd_file.put_line (fnd_file.LOG
                        ,'p_directory_path     :' || p_directory_path);
      fnd_file.put_line (fnd_file.LOG
                        ,'p_file_name          :' || p_file_name);
      fnd_file.put_line (fnd_file.LOG
                        ,'p_org_name           :' || p_org_name);
      fnd_file.put_line (
         fnd_file.LOG
        ,'----------------------------------------------------------------------------------');

      l_org_name := p_org_name;
      l_sec := 'Create File concurrent request ' || p_interface_name;

      -- Check if directory path exists
      l_dir_exists := 0;

      BEGIN
         SELECT COUNT (1)
           INTO l_dir_exists
           FROM all_directories
          WHERE directory_name = p_directory_path;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg := 'Error validating directory   :' || SQLERRM;
            RAISE l_program_error;
      END;

      -- Raise ProgramError if directory path does not exist
      IF l_dir_exists = 0
      THEN
         l_err_msg :=
            'Directory does not exist in Oracle :' || p_directory_path;
         RAISE l_program_error;
      END IF;

      -- Derive Operating Unit details
      BEGIN
         SELECT organization_id
           INTO l_org_id
           FROM hr_operating_units
          WHERE name = l_org_name;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg := 'OperatingUnit does not exist :' || l_org_name;
            RAISE l_program_error;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving OperatingUnit details :' || l_org_name;
            RAISE l_program_error;
      END;

       mo_global.set_policy_context ('S', l_org_id);
      fnd_file.put_line (fnd_file.LOG, 'l_org_id   :' || l_org_id);

      l_file_name := p_file_name;
      l_file_name_temp := 'TEMP_' || l_file_name;

      -- Delete if file already exists
      BEGIN
         UTL_FILE.FREMOVE (p_directory_path, l_file_name);
         UTL_FILE.FREMOVE (p_directory_path, l_file_name_temp);
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- Start creating the file
      l_file_handle :=
         UTL_FILE.fopen (p_directory_path
                        ,l_file_name_temp
                        ,'w'
                        ,32767);



      --        IF l_org_id IS NULL THEN
      --           OPEN view_cur for 'SELECT * FROM '||p_view_name||' WHERE ROWNUM < 20';
      --        ELSE
      OPEN view_cur FOR
         'SELECT * FROM ' || p_view_name || ' WHERE org_id = ' || l_org_id;

      --        END IF;

      -- Write data into the File
      LOOP
         FETCH view_cur INTO view_rec;

         EXIT WHEN view_cur%NOTFOUND;

         -- Write file info
         UTL_FILE.put_line (l_file_handle, view_rec.REC_LINE);
      END LOOP;

      -- Close the file
      UTL_FILE.fclose (l_file_handle);

      -- 'Rename file for pickup';
      UTL_FILE.frename (p_directory_path
                       ,l_file_name_temp
                       ,p_directory_path
                       ,l_file_name);

      fnd_file.put_line (
         fnd_file.output
        ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (
         fnd_file.output
        ,'**********************************************************************************');

      fnd_file.put_line (
         fnd_file.LOG
        ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : create_file');
      fnd_file.put_line (
         fnd_file.LOG
        ,'----------------------------------------------------------------------------------');
   EXCEPTION                                   -- this section traps my errors
      WHEN l_program_error
      THEN
         l_err_code := 2;
         --      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||substr(SQLERRM, 1, 2000);

         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         UTL_FILE.fclose (l_file_handle);
         UTL_FILE.fremove (l_file_dir, l_file_name);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_procedure_name
           ,p_calling             => l_sec
           ,p_request_id          => fnd_global.conc_request_id
           ,p_ora_error_msg       => l_err_msg
           ,p_error_desc          => 'Error running Outbound Common package with Program Error Exception'
           ,p_distribution_list   => pl_dflt_email
           ,p_module              => 'XXCUS');
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);

         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         UTL_FILE.fclose (l_file_handle);
         UTL_FILE.fremove (l_file_dir, l_file_name);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_procedure_name
           ,p_calling             => l_sec
           ,p_request_id          => fnd_global.conc_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error running Outbound Common package with Program Error Exception'
           ,p_distribution_list   => pl_dflt_email
           ,p_module              => 'XXCUS');
   END CREATE_FILE;

   /********************************************************************************
   ProcedureName : MAIN
   Purpose       : Procedure which calls the Concurrent Program -  "XXWC Create
                   Outbound File Common Program" to create files

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
   1.1     09/29/2014    Maharajan Shunmugam  TMS# 20141001-00157 Canada Multi-Org changes
   ********************************************************************************/
   PROCEDURE main (p_errbuf                   OUT VARCHAR2
                  ,p_retcode                  OUT NUMBER
                  ,p_interface_name        IN     VARCHAR2
                  ,p_view_name             IN     VARCHAR2
                  ,p_directory_path        IN     VARCHAR2
                  ,p_file_name             IN     VARCHAR2
                  ,p_user_name             IN     VARCHAR2
                  ,p_responsibility_name   IN     VARCHAR2
                  ,p_org_name              IN     VARCHAR2)
   IS
      --
      -- Package Variables
      --

      l_package               VARCHAR2 (50) := 'XXWC_OB_COMMON_PKG';
      l_dflt_email            VARCHAR2 (200) := 'HDSOracleDevelopers@hdsupply.com';
      l_email                 fnd_user.email_address%TYPE;

      l_req_id                NUMBER NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      v_supplier_id           NUMBER;
      v_rec_cnt               NUMBER := 0;
      l_message               VARCHAR2 (150);
      l_errormessage          VARCHAR2 (3000);
      pl_errorstatus          NUMBER;
      l_can_submit_request    BOOLEAN := TRUE;
      l_globalset             VARCHAR2 (100);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_sec                   VARCHAR2 (255);
      l_statement             VARCHAR2 (9000);
      l_user                  fnd_user.user_id%TYPE;      --REBTINTERFACE user
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_org_id                NUMBER;			   --Added for version 1.1

      -- Error DEBUG
      l_err_callfrom          VARCHAR2 (75) DEFAULT 'xxcus_error_pkg';
      l_err_callpoint         VARCHAR2 (75) DEFAULT 'START';
      l_distro_list           VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      --------------------------------------------------------------------------
      -- Deriving UserId
      --------------------------------------------------------------------------
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     1 = 1
                AND user_name = UPPER (p_user_name)
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName - ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      --------------------------------------------------------------------------
      -- Deriving ResponsibilityId and ResponsibilityApplicationId
      --------------------------------------------------------------------------
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility - '
               || p_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for ResponsibilityName - '
               || p_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      l_sec :=
            'UC4 call to run concurrent request '
         || p_interface_name
         || 'File Extract job.';

      --------------------------------------------------------------------------
      -- Deriving ORG ID
      --------------------------------------------------------------------------
       BEGIN  
  	SELECT organization_id 
  	INTO   l_org_id
  	FROM   apps.hr_operating_units 
  	WHERE   name =p_org_name;
	EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_message :=
                  'Operating unit '
               || p_org_name
               || ' is invalid in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_message  :=
                  'Other Operating unit validation error for '
               || p_org_name
               || '. Error: '
               || SUBSTR (SQLERRM, 1, 250);
            RAISE PROGRAM_ERROR;
      END;
      
      --------------------------------------------------------------------------
      -- Org id Initialize
      --------------------------------------------------------------------------

       mo_global.set_policy_context('S',l_org_id);

      --------------------------------------------------------------------------
      -- Apps Initialize
      --------------------------------------------------------------------------
      FND_GLOBAL.APPS_INITIALIZE (l_user_id
                                 ,l_responsibility_id
                                 ,l_resp_application_id);

      --------------------------------------------------------------------------
      -- Submit "XXWC Create Outbound File Common Program"
      --------------------------------------------------------------------------
      l_req_id :=
         fnd_request.submit_request (
            application   => 'XXWC'
           ,program       => 'XXWC_CREATE_FILE_COMMON_PROG'
           ,description   => NULL
           ,start_time    => SYSDATE
           ,sub_request   => FALSE
           ,argument1     => p_interface_name
           ,argument2     => p_view_name
           ,argument3     => p_directory_path
           ,argument4     => p_file_name
           ,argument5     => p_org_name);

      COMMIT;
      DBMS_OUTPUT.put_line ('After fnd_request');

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id
                                            ,6
                                            ,15000
                                            ,v_phase
                                            ,v_status
                                            ,v_dev_phase
                                            ,v_dev_status
                                            ,v_message)
         THEN
            v_error_message :=
                  CHR (10)
               || 'ReqID='
               || l_req_id
               || ' DPhase '
               || v_dev_phase
               || ' DStatus '
               || v_dev_status
               || CHR (10)
               || ' MSG - '
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'An error occured running the xxwc_ob_common_pkg'
                  || v_error_message
                  || '.';
               fnd_file.put_line (fnd_file.LOG, l_statement);
               fnd_file.put_line (fnd_file.output, l_statement);
               RAISE PROGRAM_ERROR;
            END IF;
         -- Then Success!
         ELSE
            l_statement :=
                  'An error occured running the xxwc_ob_common_pkg'
               || v_error_message
               || '.';
            fnd_file.put_line (fnd_file.LOG, l_statement);
            fnd_file.put_line (fnd_file.output, l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_statement := 'An error occured running the xxwc_ob_common_pkg';
         fnd_file.put_line (fnd_file.LOG, l_statement);
         fnd_file.put_line (fnd_file.output, l_statement);
         RAISE PROGRAM_ERROR;
      END IF;

      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
      p_retcode := 0;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := l_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error running xxwc_ob_common_pkg package with PROGRAM ERROR'
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXCUS');

         fnd_file.put_line (fnd_file.output, 'Fix the error!');
         p_retcode := 2;
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := l_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000)
           ,p_error_desc          => 'Error running xxwc_ob_common_pkg package with OTHERS Exception'
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXCUS');
         p_retcode := 2;
   END main;
END xxwc_ob_common_pkg;
/

