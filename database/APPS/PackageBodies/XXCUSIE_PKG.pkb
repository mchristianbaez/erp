CREATE OR REPLACE PACKAGE BODY APPS."XXCUSIE_PKG" IS
 
  /*******************************************************************************
  * Procedure:   XXCUS_IEXP_CB_DELEGATE
  * Description: This procedure looks for Crown Bolt Merchandisers and addes them
  *              to a delegates that are setup for keying expense reports.
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/20/2011    Kathy Poling    Initial creation of the procedure copied 
                                        from R11
  ********************************************************************************/

  PROCEDURE iexp_cb_delegate(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS

    --Intialize Variables
    l_err_msg    VARCHAR2(4000);
    l_err_code   NUMBER;
    l_sec        VARCHAR2(4000);
    l_time       NUMBER;
    l_type       VARCHAR2(30) DEFAULT 'HDS_IEXP_CB_DELEGATE'; --lookup_type

    l_procedure_name VARCHAR2(100) := 'XXCUSIE_PKG.IEXP_CB_DELEGATE';
    pl_dflt_email    fnd_user.email_address%TYPE := 'hds-HDSFASTap-u1@hdsupply.com';

  BEGIN
    --Version 1.1  start of changes
    EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSIE_CB_DELEGATE_TBL';

    --Insert Crown Bolt employees into table
    l_sec := 'Loading Crown Bolt Merchandisers into table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    INSERT /*+ APPEND */
    INTO XXCUS.XXCUSIE_CB_DELEGATE_TBL
        (SELECT TRIM(emp.emplid) employee_number 
             ,TRIM(emp.last_name) last_name
             ,TRIM(emp.first_name) first_name
             ,TRIM(emp.middle_name) middle_name
             ,to_char(to_date(emp.last_hire_dt), 'MMDDYYYY') last_hire_date
             ,to_char(trunc(SYSDATE), 'MMDDYYYY') begin_date
             ,TRIM(emp.fru) branch
             ,to_char(to_date(emp.termination_dt), 'MMDDYYYY') termination_date
             ,TRIM(emp.native_dept)
             ,TRIM(emp.empl_status) employee_status
             ,to_char(to_date(emp.creation_date), 'MMDDYYYY')
             ,TRIM(emp.business_unit) process_level
             ,TRIM(emp.job_descr) job_descr
             ,TRIM(emp.job_family) job_family
             ,TRIM(emp.jobcode) jobcode
         FROM apxcmmn.hr_employee_all_vw@apxprd_lnk.hsi.hughessupply.com emp
        WHERE TRIM(emp.business_unit) IN             
              (SELECT description
                 FROM fnd_lookup_values
                WHERE lookup_type = l_type
                  and meaning like 'PROCESS_LEVEL_%'
                  AND enabled_flag = 'Y'
                  AND nvl(end_date_active, SYSDATE) >= SYSDATE
                  AND description = TRIM(emp.business_unit))
          AND TRIM(emp.jobcode) IN             
              (SELECT description
                 FROM fnd_lookup_values
                WHERE lookup_type = l_type
                  and meaning like 'JCODE_1%'
                  AND enabled_flag = 'Y'
                  AND nvl(end_date_active, SYSDATE) >= SYSDATE));
    COMMIT;
   
    SELECT description
      INTO l_time
      FROM fnd_lookup_values
     WHERE lookup_type = l_type
       AND meaning = 'DELETE_TIME'
       AND enabled_flag = 'Y'
                  AND nvl(end_date_active, SYSDATE) >= SYSDATE;

    --fnd_file.put_line(fnd_file.log, 'Starting to insert delegates');

    FOR sel_delegate IN (SELECT u.user_id
                               ,v.meaning
                               ,v.enabled_flag
                               ,v.creation_date
                               ,v.last_update_date
                           FROM apps.fnd_lookup_values v, fnd_user u
                          WHERE lookup_type = l_type                       
                            AND v.meaning = u.user_name) 
    LOOP

      --Cursor to to start inserting delegates
      l_sec := 'Starting the insert for new Merchandisers to the delegate:  ' ||
               sel_delegate.meaning;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      FOR sel_emp IN (SELECT person_id, current_employee_flag, person_type_id
                        FROM hr.per_all_people_f a
                            ,XXCUS.XXCUSIE_CB_DELEGATE_TBL
                       WHERE employee_number = stg_employee_number
                         AND object_version_number IN
                             (SELECT MAX(object_version_number)
                                FROM hr.per_all_people_f b
                               WHERE b.person_id = a.person_id)
                         AND NOT EXISTS
                       (SELECT number_value
                                FROM ak.ak_web_user_sec_attr_values v
                               WHERE v.number_value = a.person_id
                                 AND v.attribute_code = 'ICX_HR_PERSON_ID'
                                 AND v.attribute_application_id = 178
                                 AND v.web_user_id = sel_delegate.user_id))
      
      LOOP

        IF sel_emp.person_type_id = 6 AND sel_delegate.enabled_flag = 'Y'
        THEN

          INSERT INTO ak_web_user_sec_attr_values
            (web_user_id
            ,attribute_code
            ,attribute_application_id
            ,varchar2_value
            ,date_value
            ,number_value
            ,created_by
            ,creation_date
            ,last_updated_by
            ,last_update_date
            ,last_update_login)
          VALUES
            (sel_delegate.user_id
            , --user_id of the delegate
             'ICX_HR_PERSON_ID'
            ,178
            ,''
            ,''
            ,sel_emp.person_id
            , --person_id of the merchandiser
             0
            ,SYSDATE
            ,0
            ,SYSDATE
            ,0);

          fnd_file.put_line(fnd_file.log
                           ,'Added Delegate:  ' || sel_delegate.meaning ||
                            '  Employee_id:  ' || sel_emp.person_id);
          fnd_file.put_line(fnd_file.output
                           ,'Added Delegate:  ' || sel_delegate.meaning ||
                            '  Employee_id:  ' || sel_emp.person_id);
        END IF;
        COMMIT;
      END LOOP;

      l_sec := 'Starting process to delete Merchandisers from the delegate:  ' ||
               sel_delegate.meaning;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      FOR sel_emp2 IN (SELECT person_id
                             ,current_employee_flag
                             ,person_type_id
                             ,effective_start_date + l_time process_date 
                         FROM hr.per_all_people_f a
                             ,XXCUS.XXCUSIE_CB_DELEGATE_TBL
                        WHERE employee_number = stg_employee_number
                          AND object_version_number IN
                              (SELECT MAX(object_version_number)
                                 FROM hr.per_all_people_f b
                                WHERE b.person_id = a.person_id)
                          AND EXISTS
                        (SELECT number_value
                                 FROM ak_web_user_sec_attr_values v
                                WHERE v.number_value = a.person_id
                                  AND v.attribute_code = 'ICX_HR_PERSON_ID'
                                  AND v.attribute_application_id = 178
                                  AND v.web_user_id = sel_delegate.user_id))
      LOOP

        IF sel_emp2.person_type_id = 9 AND
           sel_emp2.process_date <= trunc(SYSDATE) 
           OR sel_delegate.enabled_flag = 'N'
        THEN

          DELETE FROM ak_web_user_sec_attr_values
           WHERE web_user_id = sel_delegate.user_id
             AND number_value = sel_emp2.person_id;

          COMMIT;

          fnd_file.put_line(fnd_file.log
                           ,'Deleted Delegate:  ' || sel_delegate.meaning ||
                            '  Employee_id:  ' || sel_emp2.person_id);
          fnd_file.put_line(fnd_file.output
                           ,'Deleted Delegate:  ' || sel_delegate.meaning ||
                            '  Employee_id:  ' || sel_emp2.person_id);

        END IF;

      END LOOP;

    END LOOP;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      XXCUS_error_pkg.XXCUS_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Concurrent Program exception for HDS Crown Bolt Delegate'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running the iExpense delegate process.'
                                          ,p_distribution_list => pl_dflt_email
                                          ,p_module            => 'AP');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      XXCUS_error_pkg.XXCUS_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Concurrent Program exception for HDS Crown Bolt Delegate'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running the iExpense delegate process.'
                                          ,p_distribution_list => pl_dflt_email
                                          ,p_module            => 'AP');
  END iexp_cb_delegate;
END xxcusie_pkg;
/
