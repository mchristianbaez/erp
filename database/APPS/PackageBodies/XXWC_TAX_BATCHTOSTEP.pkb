CREATE OR REPLACE PACKAGE BODY apps."XXWC_TAX_BATCHTOSTEP"
AS
   PROCEDURE stepaside (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      -- -----------------------------------------------------------------------------
      -- |----------------------------< stepaside >------------------------------|
      -- -----------------------------------------------------------------------------
      --
      -- {Start Of Comments}
      --
      -- Description:
      --   This is a procedure to ensure that the required field(s) are passed to
      --   the STEP system.
      /**********************************************************************************************
      File Name: APPS.XXWC_TAX_BATCHTOSTEP

      PROGRAM TYPE: PACKAGE BODY

          HISTORY
         =============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- ----------------------------------------
      1.0     05-May-2015   Raghavendra S    TMS# 20150317-00066 -Tax - XXWC STEP Taxware Batch process for Hierachy changes' not working
      1.1     25-may-2016   Neha Saini       TMS# Task ID: 20160315-00152 --adding log messages for testing
     ***********************************************************************************************/

      l_products         VARCHAR2 (500) DEFAULT NULL;
      l_prod_string      VARCHAR2 (32000 ) DEFAULT NULL;--ver1.1
      l_specialrate      VARCHAR2 (50) DEFAULT 'F';
      l_statecode        VARCHAR2 (50) DEFAULT NULL;
      l_tax_flex_value   VARCHAR2 (100) DEFAULT 'XXWC_TAX_EXEMPTION_TYPE';

      -- Error DEBUG
      l_sec              VARCHAR2 (150);
      l_err_callfrom     VARCHAR2 (75) DEFAULT 'XXWC_TAX_BATCHTOSTEP';
      l_distro_list      VARCHAR2 (75)
                            DEFAULT 'hdsoracledevelopers@hdsupply.com';

      TYPE employees_aat IS TABLE OF xxwc.xxwc_batch_taxec_tbl%ROWTYPE; -- Added for V 1.0

      l_employees        employees_aat;                     -- Added for V 1.0
   BEGIN
      l_sec := ' main loop, gathering all changes made';

      -- the below SELECT statement Added for V 1.0
      SELECT *
        BULK COLLECT INTO l_employees
        FROM xxwc.xxwc_batch_taxec_tbl;

      -- FOR c_main IN (SELECT party_site_number, tax_exemption FROM xxwc.xxwc_batch_taxec_tbl) -- commented for V1.0
      FOR indx IN 1 .. l_employees.COUNT                    -- Added for V 1.0
      LOOP
         l_sec := 'Check to see if this is a TEC with special rates.';

         -- l_specialrate will be ='F' when there are no special rates(most cases)
         BEGIN
            SELECT ffv.attribute1
              INTO l_specialrate
              FROM apps.fnd_flex_values ffv, apps.fnd_flex_value_sets fvs
             WHERE     ffv.flex_value_set_id = fvs.flex_value_set_id
                   AND fvs.flex_value_set_name = l_tax_flex_value
                   -- AND ffv.flex_value = c_main.tax_exemption  -- Commented for V 1.0
                   AND ffv.flex_value = l_employees (indx).tax_exemption -- Added for V 1.0
                   AND ffv.attribute1 IS NOT NULL;

            fnd_file.put_line (fnd_file.LOG,
                               'l_specialrate ' || l_specialrate);--ver1.1
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_specialrate := 'F';
            WHEN OTHERS                                      -- Added for V1.0
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Special Rate for party Site Number='
                  || l_employees (indx).party_site_number
                  || 'for'
                  || l_sec);
         END;



         l_sec := ' sub loop creating the prod string for step';

         -- create product string.
         BEGIN
            -- FORALL i IN l_tab_type.FIRST .. l_tab_type.LAST
            DELETE FROM taxware.steprate_tbl
                  --WHERE key_1 =c_main.party_site_number;-- Commented for V 1.0
                  WHERE key_1 = l_employees (indx).party_site_number; --Added for V 1.0
         EXCEPTION
            WHEN OTHERS                                      -- Added for V1.0
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Deleting the data for party Site Number='
                  || l_employees (indx).party_site_number
                  || 'for'
                  || l_sec);
         --               NULL;                 -- no action, no rows to delete, so must be a new change to special rate.
         END;


         l_sec := ' gathering data for special rate products';

         IF l_specialrate != 'F'
         THEN
            BEGIN
               SELECT step.sstatecode
                 INTO l_statecode
                 FROM taxware.steptec_tbl step
                --WHERE key_1 = c_main.party_site_number; -- Commented for V 1.0
                WHERE key_1 = l_employees (indx).party_site_number;

               --Added for V 1.0
               fnd_file.put_line (fnd_file.LOG,
                                  'l_statecode ' || l_statecode);--ver1.1
            EXCEPTION
               WHEN OTHERS                                   -- Added for V1.0
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG,
                        'Satate code issue for party Site Number='
                     || l_employees (indx).party_site_number
                     || 'for'
                     || l_sec);
            END;

            l_sec := ' updating special rate product parent table';

            UPDATE taxware.steptec_tbl step1
               SET step1.cproductflag = NULL, step1.cspecialrateflag = 'Y'
             WHERE key_1 = l_employees (indx).party_site_number; --Added for V 1.0
         END IF;

         fnd_file.put_line (
            fnd_file.LOG,
               'before loop l_employees (indx).tax_exemption'
            || l_employees (indx).tax_exemption);

         FOR c_products
            IN (SELECT child_flex_value_low
                  FROM apps.fnd_flex_value_norm_hierarchy
                 --WHERE parent_flex_value = c_main.tax_exemption) -- commented for V 1.0
                 WHERE parent_flex_value = l_employees (indx).tax_exemption) --Added for V 1.0
         LOOP
            fnd_file.put_line (fnd_file.LOG, 'inside loop');          --ver1.1
            fnd_file.put_line (
               fnd_file.LOG,
               'length of l_prod_string ' || LENGTH (l_prod_string)); --ver1.1
            l_prod_string :=
               l_prod_string || RPAD (c_products.child_flex_value_low, 25);
            fnd_file.put_line (
               fnd_file.LOG,
               'length of l_prod_string  ' || LENGTH (l_prod_string)); --ver1.1

            --special rates are actually normalized and not rpadd'ed.  wierd.
            l_sec := ' updating special rate products';

            IF l_specialrate != 'F'
            THEN
               fnd_file.put_line (fnd_file.LOG, 'inside if ');        --ver1.1

               -- if special rates
               INSERT INTO taxware.steprate_tbl c (c.companyid,
                                                   c.key_1,
                                                   c.stcode,
                                                   c.steptecoccurnumber,
                                                   c.productcode,
                                                   c.basispercent,
                                                   c.federaltaxrate,
                                                   c.statetaxrate,
                                                   c.secstatetaxrate,
                                                   c.countytaxrate,
                                                   c.citytaxrate,
                                                   c.seccountytaxrate,
                                                   c.seccitytaxrate,
                                                   c.districttaxrate,
                                                   c.countytaxind)
                    VALUES ('WCI'-- ,c_main.party_site_number  -- commented for V 1.0
                            ,
                            l_employees (indx).party_site_number --Added for V 1.0
                                                                ,        --key
                            l_statecode,                           --statecode
                            0,
                            c_products.child_flex_value_low,    --product code
                            1,                                        --basis,
                            0,                                    --fedtaxrate
                            0,                                  --statetaxrate
                            0,                               --secstatetaxrate
                            TO_NUMBER (l_specialrate),         --countytaxrate
                            0,                                   --citytaxrate
                            0,                                --seccounty rate
                            0,                                  --seccity rate
                            0,                             --district tax rate
                            1                                --county tax rate
                             );
            END IF;

            fnd_file.put_line (fnd_file.LOG, 'now next will be next entry '); --ver1.1
         END LOOP;


         fnd_file.put_line (fnd_file.LOG, 'after loop');              --ver1.1
         l_sec := ' check to see if products exists in step';

         BEGIN
            SELECT NVL (productentries, 'TRUE')
              INTO l_products
              FROM stepprod_tbl
             --   WHERE key_1 = c_main.party_site_number; -- Commented for V 1.0
             WHERE key_1 = l_employees (indx).party_site_number; --Added for V 1.0
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_products := 'FALSE';
            WHEN OTHERS                                      -- Added for V1.0
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'productentries issue for party Site Number='
                  || l_employees (indx).party_site_number
                  || 'for'
                  || l_sec);
         END;



         BEGIN
            IF l_products = 'False'
            THEN                  --no row for products found, insert new one.
               IF l_specialrate = 'F'
               THEN
                  --only do this if not special rate
                  fnd_file.put_line (fnd_file.LOG, 'before insert');  --ver1.1

                  INSERT
                    INTO taxware.stepprod_tbl prod (prod.companyid,
                                                    prod.key_1,
                                                    prod.stcode,
                                                    prod.jurisdictionlevel,
                                                    prod.steptecoccurnumber,
                                                    prod.stepprodoccurnumber,
                                                    prod.productentries)
                     VALUES (
                               'WCI',                        --prod.companyid,
                               -- c_main.party_site_number -- Commented for V 1.0
                               l_employees (indx).party_site_number --Added for V 1.0
                                                                   , --prod.key_1,
                               (SELECT sstatecode
                                  FROM taxware.steptec_tbl
                                 --WHERE key_1 = c_main.party_site_number) -- commented for V 1.0
                                 WHERE key_1 =
                                          l_employees (indx).party_site_number) --Added for V 1.0
                                                                               , --prod.stcode,
                               1,                    --prod.jurisdictionlevel,
                               0,                   --prod.steptecoccurnumber,
                               1,                  --prod.stepprodoccurnumber,
                               l_prod_string             --prod.productentries
                                            );
               END IF;
            ELSE                               --row found, do a simple update
               IF l_specialrate = 'F'
               THEN                         --only do this if not special rate
                  UPDATE taxware.stepprod_tbl prod
                     SET prod.productentries = l_prod_string
                   -- WHERE prod.key_1 = c_main.party_site_number; -- Commented for V 1.0
                   WHERE prod.key_1 = l_employees (indx).party_site_number; --Added for V 1.0
               END IF;
            END IF;
         /*EXCEPTION
           WHEN others THEN
             null ; -- no data in step to match.*/
         END;

         fnd_file.put_line (
            fnd_file.LOG,
               'Modifying Customer ID:'
            || l_employees (indx).party_site_number          --Added for V 1.0
            || ' with tax certificate '
            || l_employees (indx).tax_exemption              --Added for V 1.0
            || ' in STEP.');
         fnd_file.put_line (
            fnd_file.output,
               'Modifying Customer ID:'
            || l_employees (indx).party_site_number          --Added for V 1.0
            || ' with tax certificate '
            || l_employees (indx).tax_exemption              --Added for V 1.0
            || ' in STEP.');


         l_sec := ' cleaning up ';

         DELETE FROM xxwc.xxwc_batch_taxec_tbl
               --WHERE     party_site_number = c_main.party_site_number -- Comented for V 1.0
               --          AND tax_exemption = c_main.tax_exemption; -- Comented for V 1.0
               WHERE     party_site_number =
                            l_employees (indx).party_site_number
                     AND tax_exemption = l_employees (indx).tax_exemption; --Added for V 1.0


         --initialise
         l_prod_string := NULL;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => 'STEP Taxware flex table monitor batch process', --p_request_id => l_req_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error at  ' || l_sec,
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');



         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         retcode := l_sec;
         errbuf := SQLERRM;
   END;
END xxwc_tax_batchtostep;
/