create or replace PACKAGE BODY      xxwc_po_document_util AS

  FUNCTION  xxwc_get_first_ds_pll_id(p_po_header_id    in NUMBER) RETURN NUMBER IS
    l_po_line_location_id  NUMBER;
  BEGIN
  
    l_po_line_location_id := null;
    
    select    min(plla.line_location_id)
    into    l_po_line_location_id
    from    apps.po_line_locations plla
    where    plla.po_header_id = p_po_header_id
    and        nvl(plla.drop_ship_flag, 'N') = 'Y'
    and        rownum = 1;    
       
    RETURN(l_po_line_location_id);
  EXCEPTION
    WHEN OTHERS THEN
      RETURN(null);  
  END xxwc_get_first_ds_pll_id;
  
  FUNCTION  xxwc_get_first_ds_ship_loc_id(p_po_line_loc_id    in NUMBER) RETURN NUMBER IS
    l_ship_location_id  NUMBER;
  BEGIN
  
    l_ship_location_id := null;
    
    select    plla.ship_to_location_id
    into    l_ship_location_id
    from    apps.po_line_locations plla
    where    plla.line_location_id = p_po_line_loc_id
    and        rownum = 1;    
       
    RETURN(l_ship_location_id);
  EXCEPTION
    WHEN OTHERS THEN
      RETURN(null);  
  END xxwc_get_first_ds_ship_loc_id;
                    
  function    xxwc_get_address_info (p_po_line_loc_id    in NUMBER, p_po_ship_loc_id    in NUMBER, p_return_info    in VARCHAR2) return varchar2 IS
      l_return_address_info        varchar2(500);
    
    v_customer_name        hz_parties.party_name%type := null;
    v_address_line1        hr_locations.address_line_1%type    := null;
    v_address_line2        hr_locations.address_line_2%type := null;
    v_address_line3        hr_locations.address_line_3%type := null;
    v_address_line4        hz_locations.address4%type := null;
    v_territory_name    fnd_territories_tl.territory_short_name%type := null;
    v_address_info        varchar2(500) := null;
    v_location_name        hr_locations.location_code%type := null;
    v_contact_phone        hr_locations.telephone_number_1%type := null;    
    v_contact_fax        hr_locations.telephone_number_2%type := null;
    v_town_or_city        hr_locations.town_or_city%type := null;
    v_postal_code        hr_locations.postal_code%type := null;
    v_state_or_provice    varchar2(100) := null;
    
  BEGIN
      l_return_address_info     := null;
    
    if p_po_ship_loc_id is not null then
        v_customer_name            := null;
        v_address_line1            := null;
        v_address_line2            := null;
        v_address_line3            := null;
        v_address_line4            := null;
        v_territory_name        := null;
        v_address_info            := null;
        v_location_name            := null;
        v_contact_phone            := null;    
        v_contact_fax            := null;
        v_town_or_city            := null;
        v_postal_code            := null;
        v_state_or_provice        := null;
        
        /*po_hr_location.get_alladdress_lines(  p_po_ship_loc_id,
                                              v_address_line1, --PO_COMMUNICATION_PVT.g_address_line1,
                                              v_address_line2, --PO_COMMUNICATION_PVT.g_address_line2,
                                              v_address_line3, --PO_COMMUNICATION_PVT.g_address_line3,
                                              v_territory_name, --PO_COMMUNICATION_PVT.g_Territory_short_name,
                                              v_address_info, --PO_COMMUNICATION_PVT.g_address_info,
                                              v_location_name, --PO_COMMUNICATION_PVT.g_location_name,
                                              v_contact_phone, --PO_COMMUNICATION_PVT.g_phone,
                                              v_contact_fax, --PO_COMMUNICATION_PVT.g_fax,
                                              v_address_line4, --PO_COMMUNICATION_PVT.g_address_line4,
                                              v_town_or_city, --PO_COMMUNICATION_PVT.g_town_or_city,
                                              v_postal_code, --PO_COMMUNICATION_PVT.g_postal_code,
                                              v_state_or_provice);    -- PO_COMMUNICATION_PVT.g_state_or_province*/
        
        select    pllx.ship_cust_name
                , pllx.ship_to_address_line1
                , pllx.ship_to_address_line2
                , pllx.ship_to_address_line3
                , pllx.ship_to_country
                , pllx.ship_to_address_info
                , pllx.ship_to_location_name
                , pllx.ship_to_address_line4
                , null
                , null
                , null
        into    v_customer_name,
                v_address_line1, 
                v_address_line2,
                v_address_line3,
                v_territory_name,
                v_address_info,
                v_location_name,                
                v_address_line4,
                v_town_or_city,
                v_postal_code,
                v_state_or_provice
        from    po_line_locations_xml pllx
        where    pllx.line_location_id = p_po_line_loc_id
        and        rownum = 1;
                                              
        if p_return_info = 'ADDR_LINE1' then
            l_return_address_info := v_address_line1;
        elsif p_return_info = 'ADDR_LINE2' then
            l_return_address_info := v_address_line2;
        elsif p_return_info = 'ADDR_LINE3' then
            l_return_address_info := v_address_line3;
        elsif p_return_info = 'ADDR_LINE4' then
            l_return_address_info := v_address_line4;
        elsif p_return_info = 'TERRITORY' then
            l_return_address_info := v_territory_name;
        elsif p_return_info = 'LOC_NAME' then
            l_return_address_info := v_location_name;
        elsif p_return_info = 'ADDR_INFO' then
            l_return_address_info := v_address_info;
        elsif p_return_info = 'TOWN_CITY' then
            l_return_address_info := v_town_or_city;
        elsif p_return_info = 'POSTAL_CODE' then
            l_return_address_info := v_postal_code;
        elsif p_return_info = 'STATE_PROVINCE' then
            l_return_address_info := v_state_or_provice;
        elsif p_return_info = 'CUSTOMER' then
            l_return_address_info := v_customer_name;
        else
            l_return_address_info := NULL;
        end if;
        
    end if;   
    
    return(l_return_address_info);
      
  EXCEPTION
     when others then
        return(null);
  end xxwc_get_address_info;

-- 01/07/2014 CG: TMS 20131121-00162: added function to retrieve Drop Ship Bill To Contact Phone Number
    function    xxwc_get_ds_bill_cont_phone (p_po_header_id in NUMBER) return varchar2 is
        l_first_ds_pll_id   number;
        l_contact_phone_num varchar2(240);
    begin
        l_contact_phone_num := null;        
        l_first_ds_pll_id   := null;
        
        begin
            select  xxwc_get_first_ds_pll_id(p_po_header_id)
            into    l_first_ds_pll_id
            from    dual;
        exception
        when others then
            l_first_ds_pll_id := null;
        end;
        
        if l_first_ds_pll_id is not null then
            select  substr ((hoc.phone_country_code||decode(hoc.phone_country_code, NULL, NULL, ' ')||
                            xxwc_mv_routines_pkg.format_phone_number(hoc.phone_area_code||hoc.phone_number)||
                            decode(hoc.phone_extension, NULL, NULL, ' X ')||hoc.phone_extension)
                            , 1, 240 )  
            into    l_contact_phone_num
            from    oe_drop_ship_sources odss
                    , apps.oe_order_headers oh
                    , ar_contacts_v acv
                    , hz_org_contacts_v hoc
            where   odss.line_location_id = l_first_ds_pll_id
            and     odss.header_id = oh.header_id
            and     oh.invoice_to_contact_id = acv.contact_id
            and     acv.contact_number = hoc.contact_number
            and     hoc.contact_point_type = 'PHONE'
            and     hoc.phone_line_type = 'GEN'
            and     hoc.contact_status = 'A'
            and     hoc.contact_primary_flag = 'Y'
            and     rownum = 1;            
        end if;
        
        return l_contact_phone_num;
    exception
    when others then
        return null;
    end xxwc_get_ds_bill_cont_phone;
    
  /*************************************************************************************************   
   *   Function xxwc_get_vendor_contact_id                                                          *
   *   Purpose : get the vendor contact id for the correct address for the PO Docuemnt              *
   *                                                                                                *
   *                                                                                                *
   *       function   xxwc_get_vendor_contact_id (p_po_header_id in NUMBER)                         *
   *           RETURN NUMBER;                                                                       *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   2.1`       30-Jan-2014   Lee Spitzer             Vendor Cost Improvements 0130917-00676      *
   *                                                                                                *
   /************************************************************************************************/
    
     
   function   xxwc_get_vendor_contact_id (p_po_header_id in NUMBER)
               RETURN NUMBER IS
               
          l_vendor_id NUMBER;
          l_vendor_site_id NUMBER;
          l_vendor_contact_id NUMBER;
          l_per_party_id NUMBER;
          l_relationship_id NUMBER;
          l_rel_party_id NUMBER;
          l_vendor_contact_id2 NUMBER;
               
        BEGIN
        
            BEGIN
              select vendor_id, vendor_site_id, vendor_contact_id
              into   l_vendor_id, l_vendor_site_id, l_vendor_contact_id
              from   apps.po_headers
              where  po_header_id = p_po_header_id;
           EXCEPTION
              when others then
                    l_vendor_id := NULL;
                    l_vendor_site_id := NULL;
                    l_vendor_contact_id := NULL;
           END;
          
          If l_vendor_id is not null and l_vendor_site_id is not null and l_vendor_contact_id is not null then
             
             BEGIN
               select per_party_id, relationship_id, rel_party_id
               into   l_per_party_id, l_relationship_id, l_rel_party_id
               from   po_vendor_contacts
               where  vendor_id = l_vendor_id
               and    vendor_site_id = l_vendor_site_id
               and    vendor_contact_id = l_vendor_contact_id;
            EXCEPTION
                WHEN OTHERS THEN
                      l_per_party_id := NULL;
                      l_relationship_id := NULL;
                      l_rel_party_id := NULL;
            END;

        ELSE
           
                      l_per_party_id := NULL;
                      l_relationship_id := NULL;
                      l_rel_party_id := NULL;    
        END IF;
        
        
        if l_per_party_id is not null and l_relationship_id is not null and l_rel_party_id is not null then
        
          BEGIN
           
           select vendor_contact_id 
           into   l_vendor_contact_id2
           from   po_vendor_contacts
           where  vendor_id = l_vendor_id
           and    per_party_id = l_per_party_id
           and    relationship_id = l_relationship_id
           and    rel_party_id = l_rel_party_id
           and    vendor_contact_id != l_vendor_contact_id
           and    sysdate < inactive_date
           and   rownum = 1;
          
          EXCEPTION
              WHEN OTHERS THEN
                  l_vendor_contact_id2 := NULL;
          END;
          
        else
        
            l_vendor_contact_id2 := NULL;
            
            
        END IF;
        
        
            return l_vendor_contact_id2;
            
        END xxwc_get_vendor_contact_id;

        
   /*************************************************************************************************   
   *   Function xxwc_get_contact_address_info                                                       *
   *   Purpose : get the vendor contact address information for the correct address for the PO Document*
   *                                                                                                *
   *                                                                                                *
   *   function xxwc_get_contact_address_info (p_vendor_contact_id in NUMBER, p_return_info in VARCHAR2)*
   *           RETURN VARCHAR2;                                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   2.1        30-Jan-2014   Lee Spitzer             Vendor Cost Improvements 0130917-00676      *
   *   2.2        23-Jan-2015   Lee Spitzer             TMS Ticket 20140909-00029 - 1SEE NOTES      *
   *                                                                                                *
   /************************************************************************************************/
      
        function xxwc_get_contact_address_info (p_vendor_contact_id in NUMBER, p_return_info in VARCHAR2) 
              RETURN VARCHAR2 IS
              
    v_address_line1        hr_locations.address_line_1%type    := null;
    v_address_line2        hr_locations.address_line_2%type := null;
    v_address_line3        hr_locations.address_line_3%type := null;
    v_address_line4        hz_locations.address4%type := null;
    v_town_or_city        hr_locations.town_or_city%type := null;
    v_postal_code        hr_locations.postal_code%type := null;
    v_country           hr_locations.country%type := NULL;
    v_phone             varchar2(100);
    v_fax               varchar2(100);
    v_territory_name    fnd_territories_tl.territory_short_name%type := null;
    v_state_or_province    varchar2(100) := null;
    l_return_address_info        varchar2(500);
    
    ----
    l_1see_notes VARCHAR2(45) := '1SEE NOTES'; --added 1/23/2015 TMS Ticket 20140909-00029 - 1SEE NOTES
    l_last_name  VARCHAR2(45); --added 1/23/2015 TMS Ticket 20140909-00029 - 1SEE NOTES
              
        BEGIN
        l_return_address_info := NULL;
        
        begin
          
          select assa.address_line1,
                 assa.address_line2,
                 assa.address_line3,
                 assa.address_line4,
                 nvl(assa.province,assa.state),
                 assa.country,
                 assa.city,
                 assa.zip,
                 assa.phone,
                 assa.fax,
                 --
                 pvc.last_name  --added 1/23/2015 TMS Ticket 20140909-00029 - 1SEE NOTES
          into   v_address_line1,
                 v_address_line2,
                 v_address_line3,
                 v_address_line4,
                 v_state_or_province,
                 v_country,
                 v_town_or_city,
                 v_postal_code,
                 v_phone,
                 v_fax,
                 ---
                 l_last_name  --added 1/23/2015 TMS Ticket 20140909-00029 - 1SEE NOTES
          from   apps.ap_supplier_sites assa,
                 po_vendor_contacts pvc
          where  assa.vendor_site_id = pvc.vendor_site_id
          and    pvc.vendor_contact_id = p_vendor_contact_id;
        exception
            when others then
                l_return_address_info := NULL;
        END;
        
        BEGIN
        
          select territory_short_name
          into   v_territory_name
          from   fnd_territories_tl
          where  territory_code = v_country
          and    language = 'US';
       exception
          when others then
                v_territory_name := NULL;
       END;
  
        --Added 1/23/2015 for '****SEE NOTES****' 1/23/2015 TMS Ticket 20140909-00029 - 1SEE NOTES
        --If contact is equal to 1SEE NOTES contact then null out address information but keep address line 1
        IF l_last_name = l_1see_notes THEN
        
          v_address_line2 := NULL;
          v_address_line2 := NULL;
          v_address_line3 := NULL;
          v_address_line4 := NULL;
          v_state_or_province := NULL;
          v_country := NULL;
          v_town_or_city := NULL;
          v_postal_code := NULL;
          v_phone := NULL;
          v_fax := NULL;
          v_territory_name := NULL;

        END IF;
  
  
        if p_return_info = 'ADDR_LINE1' then
            l_return_address_info := v_address_line1;
        elsif p_return_info = 'ADDR_LINE2' then
            l_return_address_info := v_address_line2;
        elsif p_return_info = 'ADDR_LINE3' then
            l_return_address_info := v_address_line3;
        elsif p_return_info = 'ADDR_LINE4' then
            l_return_address_info := v_address_line4;
        elsif p_return_info = 'TOWN_CITY' then
            l_return_address_info := initcap(v_town_or_city);
        elsif p_return_info = 'POSTAL_CODE' then
            l_return_address_info := v_postal_code;
        elsif p_return_info = 'STATE_PROVINCE' then
            l_return_address_info := v_state_or_province;
        elsif p_return_info = 'COUNTRY' then
            l_return_address_info := v_territory_name;
        elsif p_return_info = 'PHONE' then
            l_return_address_info := v_phone;
        elsif p_return_info = 'FAX' then
            l_return_address_info := v_fax;
        elsif p_return_info = 'ADDRESS_INFO' THEN
            --l_return_address_info := initcap(v_town_or_city)||', '|| v_state_or_province ||' '|| v_territory_name||' ' ||v_postal_code; --removed 1/23/2015 TMS Ticket 20140909-00029 - 1SEE NOTES
            l_return_address_info := CASE WHEN l_last_name = l_1see_notes then NULL ELSE initcap(v_town_or_city)||', '|| v_state_or_province ||' '|| v_territory_name||' ' ||v_postal_code END; --added 1/23/2015 TMS Ticket 20140909-00029 - 1SEE NOTES
        else
            l_return_address_info := NULL;
        end if;
      
    
    return(l_return_address_info);
  
        
        END  xxwc_get_contact_address_info;
		
   /* Start Change for Ver 3.0 */
   /******************************************************************************
    FUNCTION : xxwc_get_1seenotes_address
	PURPOSE  : To get 1SEE NOTES address from custom table
	REVISION :
	Ver      Date           Author            Description
	-----------------------------------------------------------------------------
	3.0      26-OCT-2017    Niraj K Ranjan    TMS#20170914-00041   DMS Phase-2.0 Outbound Extract(PO), modify the PO for 1SEENOTES
   ******************************************************************************/	
   FUNCTION  xxwc_get_1seenotes_address(p_po_header_id    IN NUMBER) 
   RETURN VARCHAR2 IS
      l_1seenotes_address VARCHAR2(2000);
   BEGIN
  
      l_1seenotes_address := null;
      
      SELECT address_line1||chr(13)||
             address_line2||decode(address_line2,null,'',chr(13))||
             city||decode(city,null,'',chr(13))||
             state||decode(state,null,'',chr(13))||
             zip||decode(zip,null,'',chr(13))||
             phone_number
	 INTO l_1seenotes_address
     FROM XXWC_PO_ADDITIONAL_ATTRS
     WHERE po_header_id = p_po_header_id;   
         
      RETURN(l_1seenotes_address);
   EXCEPTION
      WHEN OTHERS THEN
	     l_1seenotes_address := NULL;
         RETURN(l_1seenotes_address);  
   END xxwc_get_1seenotes_address;
   /* Start Change for Ver 3.0 */
END xxwc_po_document_util;
/