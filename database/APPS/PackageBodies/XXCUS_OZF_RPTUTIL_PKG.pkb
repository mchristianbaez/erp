CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_RPTUTIL_PKG AS
/*
 ESMS         DATE            COMMENT
 ====================================
 228687       11/04/2013      Compile latest source with changes related to several views using the package XXCUS_OZF_RPTUTIL_PKG.
                              The modifications are primarily in the order of the fields listed in the where clause of several routines.
 261669       09/11/2014      Changes related to adding grp_bu_id to xxcusozf_purchases_mv.							  
*/
FUNCTION get_ytd_spend (p_agr_year       IN NUMBER,
                        p_fiscal_period  IN NUMBER,
                        p_mvid           IN NUMBER) RETURN NUMBER IS
l_ytd_spend NUMBER;                      
BEGIN

  SELECT sum(total_purchases)
    INTO l_ytd_spend
    FROM xxcusozf_purchases_mv 
   where 1 =1
    and  grp_mvid        =0
    AND  GRP_LOB         =1
    and  grp_bu_id       =1   --SR 261669 9/11/2014 Added GRP_BU_ID
    and  grp_period      =0
    and  grp_qtr         =1        
    and  grp_branch      =1
    and  grp_year        =1
    and  grp_cal_period  =1
    and  grp_cal_year    =0    
    and  mvid            =p_mvid   
    and  calendar_year   =p_agr_year
    and  period_id <     =p_fiscal_period;
 
 RETURN NVL(l_ytd_spend,0);
EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999;  
END get_ytd_spend;

FUNCTION get_cur_spend (p_agr_year       IN NUMBER,
                        p_fiscal_period  IN NUMBER,
                        p_mvid           IN NUMBER) RETURN NUMBER IS
l_curr_spend NUMBER;                      
BEGIN

  SELECT sum(total_purchases)
    INTO l_curr_spend
    FROM xxcusozf_purchases_mv
   where 1=1 
    and  grp_mvid        =0
    AND  GRP_LOB         =1
    and  grp_bu_id       =1    --SR 261669 9/11/2014 Added GRP_BU_ID
    and  grp_period      =0
    and  grp_qtr         =1    
    and  grp_branch      =1
    and  grp_year        =1
    and  grp_cal_period  =1
    and  grp_cal_year    =0   
    and  mvid            =p_mvid   
    and  calendar_year   =p_agr_year
    and  period_id       =p_fiscal_period;
 
 RETURN NVL(l_curr_spend,0);
EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999;  
END get_cur_spend;

                        
FUNCTION get_ytd_inc   (p_fiscal_period  IN NUMBER,
                        p_plan_id        IN NUMBER,
                        p_rebate_type    IN VARCHAR2,
                        p_mvid           IN NUMBER) RETURN NUMBER IS
l_ytd_inc   NUMBER;
BEGIN
   SELECT sum(accrued_amount)
     INTO l_ytd_inc
     FROM xxcusozf_accruals_mv
    WHERE 1 =1
      AND grp_mvid         =0
      AND grp_cal_year     =1
      AND grp_lob          =1
      AND grp_period       =0
      AND grp_qtr          =1
      AND grp_plan_id      =0
      AND grp_branch       =1   
      AND grp_year         =1
      AND grp_adj_type     =1
      AND grp_rebate_type  =0
      AND mvid             =p_mvid
      AND period_id        <p_fiscal_period      
      AND plan_id          =p_plan_id  
      AND rebate_type_id   =p_rebate_type;     
  /*   
    WHERE 1=1
      AND plan_id = p_plan_id
      AND mvid= p_mvid
      AND period_id < p_fiscal_period  
      AND rebate_type_id=p_rebate_type   
      AND grp_mvid=0
      AND grp_plan_id=0
      AND grp_period=0
      AND grp_lob=1
      AND grp_branch=1      
      AND grp_qtr=1
      AND grp_year=1
      AND grp_cal_year=1      
      AND grp_rebate_type=0
      AND grp_adj_type=1;
  */    
      RETURN NVL(l_ytd_inc,0);

EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999;  
      

END get_ytd_inc ;

FUNCTION get_curr_inc  (p_fiscal_period  IN NUMBER,
                        p_plan_id        IN NUMBER,
                        p_rebate_type    IN VARCHAR2,
                        p_mvid           IN NUMBER) RETURN NUMBER IS
l_curr_inc   NUMBER;
BEGIN
   SELECT sum(accrued_amount)
     INTO l_curr_inc
     FROM xxcusozf_accruals_mv
     -- grp_mvid,grp_cal_year,grp_lob,grp_period,grp_qtr,grp_plan_id,grp_branch,grp_year,grp_adj_type,grp_rebate_type
    WHERE 1 =1
      AND grp_mvid         =0
      AND grp_cal_year     =1
      AND grp_lob          =1      
      AND grp_period       =0
      AND grp_qtr          =1
      AND grp_plan_id      =0
      AND grp_branch       =1
      AND grp_year         =1     
      AND grp_adj_type     =1       
      AND grp_rebate_type  =0   
      AND mvid             =p_mvid 
      AND period_id        =p_fiscal_period         
      AND plan_id          =p_plan_id  
      AND rebate_type_id   =p_rebate_type;
      
      RETURN NVL(l_curr_inc,0);

EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999;  
      

END get_curr_inc ;

FUNCTION get_cur_adj   (p_fiscal_period  IN NUMBER,
                        p_plan_id        IN NUMBER,
                        p_rebate_type    IN VARCHAR2,
                        p_mvid           IN NUMBER) RETURN NUMBER IS
l_adj_inc  NUMBER;
BEGIN
   SELECT sum(accrued_amount)
     INTO l_adj_inc
     FROM xxcusozf_accruals_mv
    WHERE 1 =1
      AND grp_mvid           =0
      AND grp_cal_year       =1    
      AND grp_lob            =1
      AND grp_period         =0
      AND grp_qtr            =1    
      AND grp_plan_id        =0
      AND grp_branch         =1    
      AND grp_year           =1
      AND grp_adj_type       =0
      AND grp_rebate_type    =0    
      AND mvid               =p_mvid
      AND plan_id            =p_plan_id      
      AND period_id          =p_fiscal_period
      AND adjustment_type_id NOT IN (-99,-6,-7)
      AND rebate_type_id     =p_rebate_type;     
   /*
    WHERE 1=1
      AND plan_id = p_plan_id
      AND mvid= p_mvid
      AND period_id = p_fiscal_period
      AND adjustment_type_id NOT IN (-99,-6,-7)
      AND rebate_type_id=p_rebate_type
      AND grp_mvid=0
      AND grp_plan_id=0
      AND grp_period=0
      AND grp_rebate_type=0
      AND grp_adj_type=0
      AND grp_lob=1
      AND grp_branch=1
      AND grp_qtr=1
      AND grp_year=1
      AND grp_cal_year=1;
  */                
      RETURN NVL(l_adj_inc,0);

EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999;  

END get_cur_adj;          

FUNCTION get_period_income(
                           p_cal_year    IN NUMBER,
                           p_mvid        IN NUMBER,
                           p_lob_id      IN NUMBER,
                           p_rebate_type IN VARCHAR2,
                           p_seq         IN NUMBER) RETURN NUMBER IS

l_accrued_amount   NUMBER;

BEGIN
   l_accrued_amount := 0;
   
   IF p_seq = 12 THEN
     SELECT acr.accrued_amount
       INTO l_accrued_amount
      FROM xxcusozf_accruals_mv acr,
           ozf_time_ent_period  period
     WHERE 1=1 
       AND acr.grp_mvid        =0
       AND acr.grp_cal_year    =0
       AND acr.grp_lob         =0                  
       AND acr.grp_period      =0
       AND acr.grp_qtr         =1
       AND acr.grp_plan_id     =1       
       AND acr.grp_branch      =1
       AND acr.grp_year        =1
       AND acr.grp_adj_type    =1                            
       AND acr.grp_rebate_type =0     
       AND acr.mvid            =p_mvid     
       AND acr.calendar_year   =p_cal_year
       AND acr.lob_id          =p_lob_id
       AND acr.rebate_type_id  =p_rebate_type           
       AND SUBSTR(acr.period_id,1,4) < acr.calendar_year
       AND period.ent_period_id=acr.period_id
       AND period.sequence     =p_seq;     
     /*
       AND acr.calendar_year  = p_cal_year
       AND acr.mvid = p_mvid
       AND acr.lob_id=p_lob_id
       AND acr.rebate_type_id = p_rebate_type
       AND acr.grp_mvid        = 0
       AND acr.grp_period      = 0
       AND acr.grp_cal_year    = 0
       AND acr.grp_rebate_type = 0
       AND acr.grp_lob         = 0
       AND acr.grp_branch      = 1
       AND acr.grp_plan_id     = 1
       AND acr.grp_adj_type    = 1
       AND acr.grp_qtr         = 1
       AND acr.grp_year        = 1    
       AND SUBSTR(acr.period_id,1,4) < acr.calendar_year
       AND period.ent_period_id=acr.period_id
       AND period.sequence=p_seq; 
    */
 ELSE
     SELECT acr.accrued_amount
       INTO l_accrued_amount
      FROM xxcusozf_accruals_mv acr,
           ozf_time_ent_period  period
     WHERE 1=1
       AND acr.grp_mvid        =0
       AND acr.grp_cal_year    =0
       AND acr.grp_lob         =0                  
       AND acr.grp_period      =0
       AND acr.grp_qtr         =1
       AND acr.grp_plan_id     =1       
       AND acr.grp_branch      =1
       AND acr.grp_year        =1
       AND acr.grp_adj_type    =1                            
       AND acr.grp_rebate_type =0     
       AND acr.mvid            =p_mvid     
       AND acr.calendar_year   =p_cal_year
       AND acr.lob_id          =p_lob_id
       AND acr.rebate_type_id  =p_rebate_type     
     /* 
       AND acr.calendar_year  = p_cal_year
       AND acr.mvid = p_mvid
       AND acr.lob_id=p_lob_id
       AND acr.rebate_type_id = p_rebate_type
       AND acr.grp_mvid       =  0
       AND acr.grp_period     =  0
       AND acr.grp_cal_year   =  0
       AND acr.grp_rebate_type = 0
       AND acr.grp_lob	       = 0
       AND acr.grp_branch      = 1
       AND acr.grp_plan_id     = 1
       AND acr.grp_adj_type    = 1
       AND acr.grp_qtr         = 1
       AND acr.grp_year        = 1 
     */  
       AND period.ent_period_id =acr.period_id
       AND period.sequence      =p_seq;
END IF; 
  RETURN NVL(l_accrued_amount,0);
EXCEPTION
   WHEN NO_DATA_FOUND THEN
       --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
       --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999;
END get_period_income;

  /*******************************************************************************
  * Procedure:   GET_LOB_ADJ
  * Description: Used in EiS Reporting
  *              
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0                                   Initial creation
  1.1     09-18-2013    Kathy Poling    Change in join for the grouping
                                        Service Ticket 223074
  ********************************************************************************/

FUNCTION get_lob_adj   (p_fiscal_period  IN NUMBER,
                        p_plan_id        IN NUMBER,
                        p_lob            IN VARCHAR2,
                        p_rebate_type    IN VARCHAR2,
                        p_mvid           IN NUMBER) RETURN NUMBER IS
l_lob_adj NUMBER;                      
BEGIN

     SELECT sum(accrued_amount)
       INTO l_lob_adj
       FROM xxcusozf_accruals_mv acr,
            hz_parties hp
      WHERE 1=1
      AND grp_mvid         =0
      AND grp_cal_year     =1
      --AND grp_lob          =1  --Version 1.1 9/18/13
      AND grp_lob          =0    --Version 1.1 9/18/13
      AND grp_period       =0
      AND grp_qtr          =1
      AND grp_plan_id      =0
      AND grp_branch       =1   
      AND grp_year         =1
      --AND grp_adj_type     =1  --Version 1.1 9/18/13
      AND grp_adj_type     =0  --Version 1.1 9/18/13     
      AND grp_rebate_type  =0            
      AND mvid             =p_mvid
      AND period_id        =p_fiscal_period         
      AND plan_id          =p_plan_id
      AND rebate_type_id   =p_rebate_type
      AND adjustment_type_id NOT IN (-99,-6,-7)
      AND acr.lob_id       =hp.party_id
      AND hp.party_name    =p_lob;            
   /*         
      WHERE 1=1
        AND plan_id=p_plan_id
        AND mvid= p_mvid
        AND period_id = p_fiscal_period
        AND rebate_type_id=p_rebate_type
        AND grp_mvid=0
        AND grp_lob=0
        AND grp_period=0
        AND grp_rebate_type=0
        AND grp_plan_id=0
        AND grp_branch=1
        AND grp_qtr=1
        AND grp_year=1
        AND grp_cal_year=1
        AND grp_adj_type=0
        AND adjustment_type_id NOT IN (-99,-6,-7)
        AND acr.lob_id=hp.party_id
        AND hp.party_name=p_lob;
   */  

   RETURN NVL(l_lob_adj,0);

EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999;  

END get_lob_adj;

FUNCTION get_lob_accr  (p_fiscal_period  IN NUMBER,
                        p_plan_id        IN NUMBER,
                        p_lob            IN VARCHAR2,
                        p_rebate_type    IN VARCHAR2,
                        p_mvid           IN NUMBER) RETURN NUMBER IS
l_lob_accr NUMBER;                      
BEGIN

     SELECT sum(accrued_amount)
       INTO l_lob_accr
       FROM xxcusozf_accruals_mv acr,
            hz_parties hp
            -- grp_mvid,grp_cal_year,grp_lob,grp_period,grp_qtr,grp_plan_id,grp_branch,grp_year,grp_adj_type,grp_rebate_type
      WHERE 1 =1
        AND grp_mvid           =0
        AND grp_cal_year       =1
        AND grp_lob            =0
        AND grp_period         =0
        AND grp_qtr            =1
        AND grp_plan_id        =0
        AND grp_branch         =1
        AND grp_year           =1
        AND grp_adj_type       =0                     
        AND grp_rebate_type    =0      
        AND mvid               =p_mvid   
        AND period_id          =p_fiscal_period           
        AND plan_id            =p_plan_id
        AND rebate_type_id     =p_rebate_type
        AND adjustment_type_id IN (-99,-6,-7)
        AND acr.lob_id         =hp.party_id
        AND hp.party_name      =p_lob;
 
   RETURN NVL(l_lob_accr,0);

EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999;  

END get_lob_accr;
FUNCTION get_invoice_amt(p_plan_id        IN NUMBER,
                         p_mvid           IN NUMBER,
                         p_lob            IN VARCHAR2)
                         RETURN NUMBER IS
l_inv_amt     NUMBER;
BEGIN
   SELECT sum(amount_due_original)
     INTO l_inv_amt
     FROM ar_payment_schedules_all aps,
          ra_customer_trx_all rct,
          hz_relationships rel,
          hz_parties hp
    WHERE 1=1
      AND rct.bill_to_customer_id = p_mvid
      AND aps.customer_trx_id=rct.customer_trx_id
      AND rct.bill_to_customer_id = p_mvid
      AND rct.attribute5=TO_CHAR(p_plan_id)
      AND rel.object_id = rct.attribute1
      AND rel.relationship_code='HEADQUARTERS_OF'
      AND rel.subject_id= hp.party_id
      AND hp.party_name=p_lob;
     
      RETURN NVL(l_inv_amt,0);

EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999;  

END get_invoice_amt;

FUNCTION get_payment_amt(p_plan_id        IN NUMBER,
                         p_mvid           IN NUMBER,
                         p_lob            IN VARCHAR2)
                         RETURN NUMBER IS
l_pay_amt   NUMBER;
BEGIN
   SELECT ABS(sum(amount_applied))
    INTO l_pay_amt
     FROM ar_receivable_applications_all ara,
          ra_customer_trx_all rct,
          ar_cash_receipts_all acr,
          hz_relationships rel,
          hz_parties hp
    WHERE 1=1
      AND acr.pay_from_customer=p_mvid
      AND ara.cash_receipt_id=acr.cash_receipt_id
      AND ara.display='Y'
      AND ara.status='APP'
      AND rct.customer_trx_id=ara.applied_customer_trx_id
      AND rct.attribute5=TO_CHAR(p_plan_id)
      AND TO_CHAR(rel.object_id) = acr.attribute1
      AND rel.relationship_code='HEADQUARTERS_OF'
      AND rel.subject_id= hp.party_id
      AND hp.party_name=p_lob;
      
  RETURN NVL(l_pay_amt,0);

EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999; 
END get_payment_amt; 

FUNCTION get_balance_amt(p_plan_id        IN NUMBER,
                         p_mvid           IN NUMBER,
                         p_lob            IN VARCHAR2)
                         RETURN NUMBER IS
l_bal_amt     NUMBER;
BEGIN
   SELECT sum(amount_due_remaining)
     INTO l_bal_amt
     FROM ar_payment_schedules_all aps,
          ra_customer_trx_all rct,
          hz_relationships rel,
          hz_parties hp
    WHERE 1=1 
      AND rct.bill_to_customer_id = p_mvid
      AND aps.customer_trx_id=rct.customer_trx_id      
      AND rct.attribute5=TO_CHAR(p_plan_id)
      AND rel.object_id = rct.attribute1
      AND rel.relationship_code='HEADQUARTERS_OF'
      AND rel.subject_id= hp.party_id
      AND hp.party_name=p_lob;
     
      RETURN NVL(l_bal_amt,0);

EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN -999.999; 
END GET_BALANCE_AMT;

-- 9-DEC-2013  Dragan added get_adjusted_invoice_amt function
-- Need it in order to get actual invoice amount when there are adjustments
FUNCTION get_adjusted_invoice_amt(p_plan_id        IN NUMBER,
                                  p_mvid           IN NUMBER,
                                  p_lob            IN VARCHAR2)
                                      RETURN NUMBER IS
l_inv_amt     NUMBER;
BEGIN
   SELECT sum(amount_adjusted)
     INTO l_inv_amt
     FROM ar_payment_schedules_all aps,
          ra_customer_trx_all rct,
          hz_relationships rel,
          hz_parties hp
    WHERE 1=1
      AND rct.bill_to_customer_id = p_mvid
      AND aps.customer_trx_id=rct.customer_trx_id
      AND rct.bill_to_customer_id = p_mvid
      AND rct.attribute5=TO_CHAR(p_plan_id)
      AND rel.object_id = rct.attribute1
      AND rel.relationship_code='HEADQUARTERS_OF'
      AND rel.subject_id= hp.party_id
      AND hp.party_name=p_lob;
     
      RETURN NVL(l_inv_amt,0);

EXCEPTION
     WHEN NO_DATA_FOUND THEN
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN  0;
   WHEN OTHERS THEN   
     --dbms_output.put_line('Error'||SQLERRM);
       RETURN 0;  

END get_adjusted_invoice_amt;
END XXCUS_OZF_RPTUTIL_PKG;
/
