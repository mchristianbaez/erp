--
-- XXWC_MSTR_PARTS_EXCL_LIST_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.xxwc_mstr_parts_excl_list_pkg
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_MSTR_PARTS_EXCL_LIST_PKG.pks $
   *   Module Name: XXWC_MSTR_PARTS_EXCL_LIST_PKG.pks
   *
   *   PURPOSE:   This package is called by the concurrent program xxwc cyclecount
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/30/2011  Vivek Lakaman             Initial Version
   * ************************************************************************/

   /*************************************************************************
 *   Procedure : Write_Log
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/
   -- add debug message to log table and concurrent log file
   PROCEDURE Write_Log (p_debug_level   IN NUMBER
                       ,p_mod_name      IN VARCHAR2
                       ,p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;

      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      COMMIT;
   END Write_Log;

   /*************************************************************************
 *   Procedure : Write_Error
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_Error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'xxwc_mstr_parts_excl_list_pkg';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => l_req_id
        ,p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000)
        ,p_error_desc          => 'Error running xxwc_mstr_parts_excl_list_pkg with PROGRAM ERROR'
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'AR');
   END Write_Error;

   /*************************************************************************
    *   Procedure : Write_output
    *
    *   PURPOSE:   This procedure logs message into concurrent output
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;


   /*****************************************************************************
   *   Procedure Name: Insert_Row
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   * ***************************************************************************/

   PROCEDURE insert_row (px_parts_excl_list_id     IN OUT NOCOPY NUMBER
                        ,p_item_id                               NUMBER
                        ,p_item_number                           VARCHAR2
                        ,p_last_update_date                      DATE
                        ,p_last_updated_by                       NUMBER
                        ,p_creation_date                         DATE
                        ,p_created_by                            NUMBER
                        ,p_last_update_login                     NUMBER
                        ,p_object_version_number                 NUMBER)
   IS
      l_mod_name   VARCHAR2 (100)
                      := 'xxwc_mstr_parts_excl_list_pkg.insert_row';

      CURSOR c1
      IS
         SELECT xxwc_mstr_parts_excl_list_s.NEXTVAL FROM sys.DUAL;
   BEGIN
      IF    (px_parts_excl_list_id IS NULL)
         OR (px_parts_excl_list_id = fnd_api.g_miss_num)
      THEN
         OPEN c1;

         FETCH c1 INTO px_parts_excl_list_id;

         CLOSE c1;
      END IF;

      Write_Log (g_level_statement
                ,l_mod_name
                ,'px_parts_excl_list_id=' || px_parts_excl_list_id);

      INSERT INTO xxwc_mstr_parts_excl_list (parts_excl_list_id
                                            ,item_id
                                            ,item_number
                                            ,organization_id
                                            , --Added by LHS 4/16/2012 to default Master Organization
                                             last_update_date
                                            ,last_updated_by
                                            ,creation_date
                                            ,created_by
                                            ,last_update_login
                                            ,object_version_number)
           VALUES (px_parts_excl_list_id
                  ,p_item_id
                  ,p_item_number
                  ,fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG')
                  ,p_last_update_date
                  ,p_last_updated_by
                  ,p_creation_date
                  ,p_created_by
                  ,p_last_update_login
                  ,p_object_version_number);

      Write_Log (
         g_level_statement
        ,l_mod_name
        ,   'sucessfully inserted for the px_parts_excl_list_id='
         || px_parts_excl_list_id);
   /*   EXCEPTION
         WHEN OTHERS THEN
            Write_Log (
            g_level_statement,
            l_mod_name,
            'Error='
            || sqlerrm
         );
   */
   END Insert_Row;


   /*****************************************************************************
   *   Procedure Name: Update_Row
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Update Trigger
   * ***************************************************************************/
   PROCEDURE update_row (p_parts_excl_list_id       NUMBER
                        ,p_item_id                  NUMBER
                        ,p_item_number              VARCHAR2
                        ,p_last_update_date         DATE
                        ,p_last_updated_by          NUMBER
                        ,p_last_update_login        NUMBER
                        ,p_object_version_number    NUMBER)
   IS
      l_mod_name   VARCHAR2 (100)
                      := 'xxwc_mstr_parts_excl_list_pkg.Update_Row';
   BEGIN
      Write_Log (g_LEVEL_STATEMENT
                ,l_mod_name
                ,' p_parts_excl_list_id=' || p_parts_excl_list_id);

      UPDATE xxwc_mstr_parts_excl_list
         SET item_id =
                DECODE (p_item_id, fnd_api.g_miss_num, item_id, p_item_id)
            ,item_number =
                DECODE (p_item_number
                       ,fnd_api.g_miss_char, item_number
                       ,p_item_number)
            ,last_update_date =
                DECODE (p_last_update_date
                       ,fnd_api.g_miss_date, last_update_date
                       ,p_last_update_date)
            ,last_updated_by =
                DECODE (p_last_updated_by
                       ,fnd_api.g_miss_num, last_updated_by
                       ,p_last_updated_by)
            ,last_update_login =
                DECODE (p_last_update_login
                       ,fnd_api.g_miss_num, last_update_login
                       ,p_last_update_login)
            ,object_version_number =
                DECODE (p_object_version_number
                       ,fnd_api.g_miss_num, object_version_number
                       ,p_object_version_number)
       WHERE parts_excl_list_id = p_parts_excl_list_id;

      Write_Log (
         g_LEVEL_STATEMENT
        ,l_mod_name
        ,   'sucessfully updated for the p_parts_excl_list_id='
         || p_parts_excl_list_id);
   /*   EXCEPTION
         WHEN OTHERS
         THEN
            Write_Log (g_LEVEL_STATEMENT, l_mod_name, 'error msg=' || SQLERRM);
   */
   END Update_Row;

   /*****************************************************************************
   *   Procedure Name: Lock_Row
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Lock Trigger
   * ***************************************************************************/

   PROCEDURE lock_row (p_parts_excl_list_id       NUMBER
                      ,p_object_version_number    NUMBER)
   IS
      CURSOR c
      IS
             SELECT object_version_number
               FROM xxwc_mstr_parts_excl_list
              WHERE parts_excl_list_id = p_parts_excl_list_id
         FOR UPDATE OF parts_excl_list_id NOWAIT;

      recinfo   c%ROWTYPE;
   BEGIN
      OPEN c;

      FETCH c INTO recinfo;

      IF (c%NOTFOUND)
      THEN
         CLOSE c;

         fnd_message.set_name ('FND', 'FORM_RECORD_DELETED');
         app_exception.raise_exception;
      END IF;

      CLOSE c;

      IF (recinfo.object_version_number = p_object_version_number)
      THEN
         NULL;
      ELSE
         fnd_message.set_name ('FND', 'FORM_RECORD_CHANGED');
         app_exception.raise_exception;
      END IF;
   END lock_row;

   /*****************************************************************************
   *   Procedure Name: Delete_Row
   *
   *   purpose:   this procedure is the table handler called by the on-delete trigger
   * ***************************************************************************/

   PROCEDURE delete_row (p_parts_excl_list_id NUMBER)
   IS
   BEGIN
      DELETE FROM xxwc_mstr_parts_excl_list
            WHERE parts_excl_list_id = p_parts_excl_list_id;
   END delete_row;
END xxwc_mstr_parts_excl_list_pkg;
/

