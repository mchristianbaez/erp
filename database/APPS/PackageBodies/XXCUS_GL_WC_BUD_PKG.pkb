CREATE OR REPLACE PACKAGE BODY APPS."XXCUS_GL_WC_BUD_PKG" IS
  /********************************************************************************
  
  File Name: XXCUS_GL_WC_BUD_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Procedures and functions for creating the Budget loads to WC Intranet
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     1/06/2012     John Bozik    Initial creation of the package for R12
  Modified for R12 from 11i version developed by Jason Sloan
  ********************************************************************************/

  /*******************************************************************************
  * Procedure:   create_wc_dw_budget_extract
  * Description: Writes Budget data used by Enterprise Translator (ET).
  *              ET will translate this data and create/perform the following:
  *              WCE_GL_BUD_<YYYY>_BUDG_<date in YYYYMMDDHH24MISS format>.txt
  *              ftp'ed to WhiteCap California.  
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     1/06/2012     John Bozik    Initial creation of the package for R12
   ********************************************************************************/

  PROCEDURE create_wc_gl_budget_extract(errbuf       OUT VARCHAR2
                                       ,retcode      OUT NUMBER
                                       ,p_budgetname IN VARCHAR2) IS
  
    --Intialize Variables
    l_err_msg  VARCHAR2(2000);
    l_err_code NUMBER;
    lc_coa CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := '50328'; --HSI Main Chart of Accounts
    l_budgetname gl_balances.period_name%TYPE := p_budgetname; --Will hold the period the extract is to be run for
    lc_ledgerid CONSTANT gl_period_statuses.ledger_id%TYPE := 2061; --HSI USD Ledger ID
    l_seq_num NUMBER := 0;
  
    --Error Handler Variables
    l_sec            VARCHAR2(4000);
    l_procedure_name VARCHAR2(100) := 'XXCUS_GL_WC_BUD_PKG.CREATE_WC_GL_BUDGET_EXTRACT';
    pl_dflt_email2   fnd_user.email_address%TYPE := 'hds.oracleglsupport@hdsupply.com';
  
    --Start Main Program
  BEGIN
    l_sec := 'Obtain Parameters; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    l_budgetname := p_budgetname;
  
    --Truncate data tables prior to load
    BEGIN
      l_sec := 'Truncate the table before loading ';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_WC_DW_GL_BUD_T';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUS_WC_DW_GL_BUD_T: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    --Insert Sequence
    SELECT xxcus_wc_dw_seq.nextval INTO l_seq_num FROM dual;
  
    --Insert Budget Table
    l_sec := 'Loading Budget Table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    INSERT INTO xxcus.xxcus_wc_dw_gl_bud_t
      (SELECT DISTINCT l_seq_num
                      ,a.period_num
                      ,a.period_year
                      ,a.period_net_dr
                      ,a.period_net_cr
                      ,b.segment1
                      ,b.segment2
                      ,b.segment3
                      ,b.segment4
                      ,b.segment5
                      ,b.segment6
                      ,b.segment7
         FROM gl_balances a, gl_code_combinations b, gl.gl_budget_versions c
        WHERE a.code_combination_id = b.code_combination_id
          AND a.currency_code <> 'STAT'
          AND a.ledger_id = lc_ledgerid
          AND a.actual_flag = 'B'
          AND b.segment1 IN (SELECT h.description
                               FROM apps.fnd_lookup_values_vl h
                              WHERE h.lookup_type = 'HDS_WCEHDREXTRAUTO'
                                AND h.meaning LIKE 'SEG1_%')
          AND b.chart_of_accounts_id = lc_coa
          AND a.budget_version_id = c.budget_version_id
          AND c.budget_name = l_budgetname
          AND (a.period_net_dr <> 0 OR a.period_net_cr <> 0));
  
    COMMIT;
  
  EXCEPTION
  
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running WhiteCap GL Budget Extract, custom error.'
                                          ,p_distribution_list => pl_dflt_email2
                                          ,p_module            => 'GL');
      COMMIT;
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running WhiteCap GL Budget Extract, others error.'
                                          ,p_distribution_list => pl_dflt_email2
                                          ,p_module            => 'GL');
    
      COMMIT;
    
  END create_wc_gl_budget_extract;
END xxcus_gl_wc_bud_pkg;
/
