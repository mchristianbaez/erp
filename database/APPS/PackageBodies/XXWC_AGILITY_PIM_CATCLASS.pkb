CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AGILITY_PIM_CATCLASS AS

  g_err_callfrom VARCHAR2(75) DEFAULT 'XXWC_AGILITY_PIM_CATCLASS';
  g_distro_list  VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  g_host         VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
  g_hostport     VARCHAR2(20) := '25';
/*********************************************************************************
  -- Package XXWC_AGILITY_PIM_CATCLASS
  -- *******************************************************************************
  --
  -- PURPOSE: Package is for Agility PIM.
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     06-Mar-2018   Niraj K Ranjan  20171116-00176   Agility PIM Project Catclass Structure
  **********************************************************************************************/

  /********************************************************************************
  -- PROCEDURE: GEN_CATCLASS_FILE
  --
  -- PURPOSE: procedure to generate catclass structure for Agility PIM
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     06-Mar-2018   Niraj K Ranjan  20171116-00176   Agility PIM Project Catclass Structure
  
  *******************************************************************************/
  PROCEDURE gen_catclass_file(p_errbuf      OUT VARCHAR2
                             ,p_retcode     OUT VARCHAR2) 
  IS
    l_phase  VARCHAR2(240);
    l_status VARCHAR2(240);

    l_err_msg          CLOB;
    l_sec              VARCHAR2(110) DEFAULT 'START';
    l_user_id          fnd_user.user_id%TYPE;
    l_procedure        VARCHAR2(50) := 'GEN_CATCLASS_FILE';
    l_application_name VARCHAR2(30) := 'XXWC';
    l_sequence         NUMBER;
    l_run_date         DATE;
	
    l_filename             VARCHAR2(100);
    l_file_dir             VARCHAR2(100) := 'XXWC_INV_AGILITY_FEED_DIR';
    l_file_handle          utl_file.file_type;
    l_file_name_temp       VARCHAR2(100);
    l_base_file_exists     BOOLEAN;
    l_base_file_length     NUMBER;
    l_base_file_block_size BINARY_INTEGER;
	l_sid                  VARCHAR2(8);
	
	--l_org_id            NUMBER := mo_global.get_current_org_id;
	
	CURSOR cr_catclass
	IS
	  SELECT BUSINESS_UNIT, 
             SOURCE_SYSTEM, 
	         CATEGORY, 
	         CATEGORY_VALUE, 
	         CATEGORY_DESC, 
	         SUB_CATEGORY, 
	         SUB_CATEGORY_VALUE, 
	         SUB_CATEGORY_DESC, 
	         PRODUCT_LINE, 
	         PRODUCT_LINE_VALUE, 
	         PRODUCT_LINE_DESC, 
	         CAT_CLASS, 
	         CAT_CLASS_VALUE, 
	         CAT_CLASS_DESC, 
	         OPERATING_UNIT_ID, 
	         INTERFACE_DATE
      FROM XXWC_AGILITY_PIM_CATCLASS_VW;
  
  BEGIN
    fnd_file.put_line(fnd_file.log
                     ,'Beginning EBS To Agility File Generation ');
    fnd_file.put_line(fnd_file.log
                     ,'========================================================');
    fnd_file.put_line(fnd_file.log, '  ');
    --Initialize the Out Parameter
    p_errbuf  := '';
    p_retcode := '0';
	--derive Instance name
	SELECT substr(NAME,4,LENGTH(NAME)) INTO l_sid FROM v$database;
	
	--Derive the file name
    SELECT 'XXWC_'||l_sid||'_Agility_Category_Class_hierarchy_' || to_char(systimestamp, 'MMDDYYYYHH24MISS') ||'.txt'
      INTO l_filename
      FROM dual;
	
	--Set the file name and open the file
    l_file_name_temp := 'TEMP_' || l_filename;
	fnd_file.put_line(fnd_file.log, 'File Name:  ' || l_filename);
	
	--Open the file handler
    l_file_handle := utl_file.fopen(location  => l_file_dir
                                   ,filename  => l_file_name_temp
                                   ,open_mode => 'w');
    fnd_file.put_line(fnd_file.log, ' Writing to the file ... ');
	utl_file.put_line(l_file_handle
                           ,'HDS Business Unit'||'|'|| 
                            'Source System'||'|'|| 
	                        'Product Hierarchy Level 1 Label'||'|'|| 
	                        'Product Hierarchy Level 1 Member Value'||'|'|| 
	                        'Product Hierarchy Level 1 Member Description'||'|'|| 
	                        'Product Hierarchy Level 2 Label'||'|'|| 
	                        'Product Hierarchy Level 2 Member Value'||'|'|| 
	                        'Product Hierarchy Level 2 Member Description'||'|'|| 
	                        'Product Hierarchy Level 3 Label'||'|'|| 
	                        'Product Hierarchy Level 3 Member Value'||'|'|| 
	                        'Product Hierarchy Level 3 Member Description'||'|'|| 
	                        'Product Hierarchy Level 4 Label'||'|'|| 
	                        'Product Hierarchy Level 4 Member Value'||'|'|| 
	                        'Product Hierarchy Level 4 Member Description');
	FOR rec_catclass IN cr_catclass
	LOOP
	   l_sec := 'Writing file';
          -- Writting file -- Version# 1.6 > Start
          -- Removed special Chars in file  -- Version# 1.11 < Start
          utl_file.put_line(l_file_handle
                           ,rec_catclass.BUSINESS_UNIT||'|'|| 
                            rec_catclass.SOURCE_SYSTEM||'|'|| 
	                        rec_catclass.CATEGORY||'|'|| 
	                        lpad(rec_catclass.CATEGORY_VALUE,4,0)||'|'|| 
	                        rec_catclass.CATEGORY_DESC||'|'|| 
	                        rec_catclass.SUB_CATEGORY||'|'|| 
	                        lpad(rec_catclass.SUB_CATEGORY_VALUE,4,0)||'|'|| 
	                        rec_catclass.SUB_CATEGORY_DESC||'|'|| 
	                        rec_catclass.PRODUCT_LINE||'|'|| 
	                        rec_catclass.PRODUCT_LINE_VALUE||'|'|| 
	                        rec_catclass.PRODUCT_LINE_DESC||'|'|| 
	                        rec_catclass.CAT_CLASS||'|'|| 
	                        rec_catclass.CAT_CLASS_VALUE||'|'|| 
	                        rec_catclass.CAT_CLASS_DESC); 
	                        --rec_catclass.OPERATING_UNIT_ID||'|'|| 
	                        --rec_catclass.INTERFACE_DATE
	END LOOP;
	--utl_file.put_line(l_file_handle, 'EOF');
    --Close the file
    l_sec := 'Closing the File; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    utl_file.fclose(l_file_handle);
	
	l_sec := 'Verifying if data exists in file';
    fnd_file.put_line(fnd_file.log, 'Verifying if data exists in file');
    utl_file.fgetattr(location    => l_file_dir
                     ,filename    => l_file_name_temp
                     ,fexists     => l_base_file_exists
                     ,file_length => l_base_file_length
                     ,block_size  => l_base_file_block_size);

    fnd_file.put_line(fnd_file.log
                     ,'l_base_file_length: ' || l_base_file_length ||
                      ' l_base_file_block_size: ' ||
                      l_base_file_block_size);
					  
    IF l_base_file_length <= 1 --Version 1.1 change from 0 to allow for end of line record
    THEN

      utl_file.fremove(location => l_file_dir
                      ,filename => l_file_name_temp);
    ELSE
      utl_file.frename(l_file_dir
                      ,l_file_name_temp
                      ,l_file_dir
                      ,l_filename);
    END IF;
  
  EXCEPTION
  WHEN utl_file.invalid_path THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'File Path is invalid.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN utl_file.invalid_mode THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'The open_mode parameter in FOPEN is invalid.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN utl_file.invalid_filehandle THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'File handle is invalid..';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN utl_file.invalid_operation THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'File could not be opened or operated on as requested';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN utl_file.write_error THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'Operating system error occurred during the write operation';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN utl_file.internal_error THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'Unspecified PL/SQL error.';
      fnd_file.put_line(fnd_file.log, l_err_msg);

    WHEN utl_file.file_open THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'The requested operation failed because the file is open.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN utl_file.invalid_filename THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'The filename parameter is invalid.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN utl_file.access_denied THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'Permission to access to the file location is denied.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN OTHERS THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');

      p_retcode := 2;
      p_errbuf  := l_err_msg;
  END gen_catclass_file;

END XXWC_AGILITY_PIM_CATCLASS;
/  