CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OID_FND_UTIL IS
  --------------------------------------------------------------------------------
  -- Copyright 2010 HD Supply  Inc (Orlando, FL) - All rights reserved
  --
  --
  --   Scope: To reset OID and EBIZ user SSO login issues.
  --
  --    REVISIONS:
  --   ESMS/RFC          Ver        Date         Author              Description
  --   ----------------  ---------  ----------  ---------------      -------------------------------
  --  244950            1.0        04/01/2014   Balaguru Seshadri    Created.
  --  245945           1.1         04/11/2014   Balaguru Seshadri    Include logic to print active and inactive employees for reporting.
  --  246806           1.2        04/17/2014   Balaguru Seshadri    Added a log table for ease of sorting and some report output changes.
  --  250436           1.3        12/10/2015   Balaguru Seshadri    Clear GUID value rather than trying to match. [TMS 20150909-00156 ]
  -------------------------------------------------------------------------------
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  -- 
  procedure print_output(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.output, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_output routine ='||sqlerrm);
  end print_output; 
  --  
  --------------------------------------------------------------------------------
  -- Copyright 2010 HD Supply  Inc (Orlando, FL) - All rights reserved
  --
  --
  --   Procedure: reset_guid
  --
  --    REVISIONS:
  --   ESMS/RFC       Ver   Date                 Author                          Description
  --   ----------------  ----    --------------      ---------------                 -------------------------------
  --  250436            1.3   12/10/2015   Balaguru Seshadri    For inactive users, clear GUID value. [TMS 20150909-00156 ] 
  -------------------------------------------------------------------------------  
  --
 PROCEDURE reset_guid
    (
       ERRBUF          OUT NOCOPY VARCHAR2
      ,RETCODE         OUT NOCOPY VARCHAR2
      ,p_user          IN VARCHAR2
      ,p_reset_flag    IN VARCHAR2
    ) AS
 --    
   v_oid_guid   varchar2 (2000);  
   n_count      number;
 --
   v_psft_empl_status  varchar2(20);
 --
 type OID_Status_Tbl is RECORD
  (
    NT_ID             VARCHAR2(30)
   ,NAME              VARCHAR2(80)
   ,PSFT_EMPL_STATUS  VARCHAR2(20)
   ,OID_USER_COUNT    NUMBER
   ,EBS_GUID          VARCHAR2(400)
   ,OID_GUID          VARCHAR2(400)
   ,COMMENTS          VARCHAR2(4000) --Ver 1.3...Just to be safe increasing the message text field to hold 4k instead of 2k chars.
  );
 --
 type status_tbl is TABLE of OID_Status_Tbl Index by Binary_Integer;
 active_tbl    status_tbl;
 inactive_tbl  status_tbl;
 --
 indx1 NUMBER :=0; 
 indx2 NUMBER :=0;
 --
 n_user_id NUMBER :=0;
 n_person_id NUMBER :=0; 
 --
 ex_dml_errors      exception;
 PRAGMA EXCEPTION_INIT(ex_dml_errors, -24381);
 -- 
 n_loc varchar2(4);
 l_error_count number;
 -- 
begin --Main processing
 --
 active_tbl.delete;
 inactive_tbl.delete;
 --
 for rec
      in (  select user_id, user_name, user_guid, description ebiz_user_desc, employee_id, end_date
            from fnd_user
            where     1 = 1
                   and user_id > 1090
                   and user_guid is not null
                   and user_name =nvl(p_user, user_name)
                   and employee_id is not null
                   and (user_name not like 'XX%' or user_name not like '%REBA%' OR USER_name not like 'HDS%' OR USER_name not like '%INT%' OR USER_name not like 'GLINT%' )
            order by 2 asc
         )          
 --          
 loop
     --
     n_user_id    :=rec.user_id;
     n_person_id  :=rec.employee_id;
     --
     n_loc :='0001';
     -- Get People Soft HR Employee Status
     begin 
     --
      v_psft_empl_status :=Null;  
     --
      select 
       case
        when e.process_flag ='I' then 'InActive'
        when e.process_flag ='A' then 'Active'        
        else 'NA'
       end
      into   v_psft_empl_status
      from   xxcus.xxcushr_ps_emp_all_tbl e
            ,per_people_x                 a
      where  1 =1
        and  a.person_id       =rec.employee_id
        and  e.employee_number =a.employee_number;
     --
     exception
      when no_data_found then
       --
       --print_log('v_psft_empl_status, msg =Table xxcus.xxcushr_ps_emp_all_tbl has no data for employee_id ='||rec.employee_id);
       --       
       v_psft_empl_status :='NONE';
       --
      when others then
       --
         print_log('@when others, v_psft_empl_status, msg ='||sqlerrm);
       --      
       v_psft_empl_status :='UNKNOWN';
       --      
     end;
     --
     n_loc :='0002';
     --               
     if (v_psft_empl_status <>'InActive' AND rec.end_date is null) then
         --
         -- Get OID guid value by calling the ldap package
         --
         begin
          --
           v_oid_guid :=fnd_ldap_user.get_user_guid_and_count (rec.user_name, n_count); 
          --
           --print_log('LDAP Guid ='||v_oid_guid||', N_Count ='||n_count||', FND Guid ='||rec.user_guid);        
          --
         exception
          when no_data_found then
           --
           v_oid_guid :=Null;
           --
           print_log('@LDAP-No Data found: v_oid_guid is null, '||'user ='||rec.user_name||', msg ='||sqlerrm);
           --           
          when others then
           --
           print_log('@LDAP-when others: v_oid_guid is null, '||'user ='||rec.user_name||', msg ='||sqlerrm);
           --
           v_oid_guid :=Null;
           --
         end;
         --
         -- if OID guid and EBS guid are not equal, we are going to update EBS user_guid with OID guid value.
         --
         if ((v_oid_guid is not null)) then
          --
          n_loc :='0003';
          --          
          if (rec.user_guid <> v_oid_guid) then
           --
           n_loc :='0004';
           --
            if p_reset_flag ='Y' then
             --
              begin  
               --
               savepoint start_here;
               --
               update fnd_user
               set user_guid =v_oid_guid
               where 1 =1
                 and user_id =rec.user_id;
               --
               if (sql%rowcount >0) then
                   --
                   if v_psft_empl_status ='Active' then
                    --
                    begin 
                        --                        
                         n_loc :='0005'; 
                        --
                        indx1 :=indx1 + 1;
                        --
                        active_tbl(indx1).NT_ID            :=rec.user_name;
                        active_tbl(indx1).NAME             :=rec.ebiz_user_desc;
                        active_tbl(indx1).PSFT_EMPL_STATUS :=v_psft_empl_status;
                        active_tbl(indx1).OID_USER_COUNT   :=n_count;
                        active_tbl(indx1).EBS_GUID         :=rec.user_guid;
                        active_tbl(indx1).OID_GUID         :=v_oid_guid;
                        active_tbl(indx1).COMMENTS         :='Message: FND USER_GUID was successfully reset.' ;                                                                       
                       --
                    exception
                        when others then
                         print_log('@Populate active_tbl-101, user_name '||rec.user_name);            
                         print_log('@Populate active_tbl-101, message ='||sqlerrm);
                    end;
                    -- 
                   else --if v_psft_empl_status NOT EQUALS 'Active' then
                    --
                    begin  
                        --
                        indx2 :=indx2 + 1;
                        --
                        n_loc :='0006';
                        --
                        active_tbl(indx2).NT_ID            :=rec.user_name;
                        active_tbl(indx2).NAME             :=rec.ebiz_user_desc;
                        active_tbl(indx2).PSFT_EMPL_STATUS :=v_psft_empl_status;
                        active_tbl(indx2).OID_USER_COUNT   :=n_count;
                        active_tbl(indx2).EBS_GUID         :=rec.user_guid;
                        active_tbl(indx2).OID_GUID         :=v_oid_guid;
                        active_tbl(indx2).COMMENTS         :='Message: FND USER_GUID was successfully reset.' ;                                                                       
                       --
                    exception
                        when others then
                         print_log('@Populate active_tbl-101, user_name '||rec.user_name);            
                         print_log('@Populate active_tbl-101, message ='||sqlerrm);
                    end;
                    --                        
                   end if;          
                 --
                 --print_log('User: '||rec.user_name||' was successfully updated with OID guid. '||sql%rowcount);
               else
                 --
                 if v_psft_empl_status ='Active' then
                 --
                   begin 
                    --
                    n_loc :='0007';
                    --
                    indx1 :=indx1 + 1;
                    --                            
                    active_tbl(indx1).NT_ID            :=rec.user_name;
                    active_tbl(indx1).NAME             :=rec.ebiz_user_desc;
                    active_tbl(indx1).PSFT_EMPL_STATUS :=v_psft_empl_status;
                    active_tbl(indx1).OID_USER_COUNT   :=n_count;
                    active_tbl(indx1).EBS_GUID         :=rec.user_guid;
                    active_tbl(indx1).OID_GUID         :=v_oid_guid;
                    active_tbl(indx1).COMMENTS         :=Substr('Failed to reset fnd user_guid for user: '||rec.user_name||' Msg ='||sqlerrm, 1, 2000);                                                                       
                   --
                   exception
                    when others then
                     print_log('@Populate active_tbl-102, user_name '||rec.user_name);            
                     print_log('@Populate active_tbl-102, message ='||sqlerrm);
                   end;
                 --                       
                 else -- v_psft_empl_status NOT EQUALS 'Active' then
                 --
                   begin 
                    --
                    n_loc :='0008';
                    --
                    indx2 :=indx2 + 1;
                    --                        
                    active_tbl(indx2).NT_ID            :=rec.user_name;
                    active_tbl(indx2).NAME             :=rec.ebiz_user_desc;
                    active_tbl(indx2).PSFT_EMPL_STATUS :=v_psft_empl_status;
                    active_tbl(indx2).OID_USER_COUNT   :=n_count;
                    active_tbl(indx2).EBS_GUID         :=rec.user_guid;
                    active_tbl(indx2).OID_GUID         :=v_oid_guid;
                    active_tbl(indx2).COMMENTS         :=Substr('Failed to reset fnd user_guid for user: '||rec.user_name||' Msg ='||sqlerrm, 1, 2000);                                                                       
                   --
                   exception
                    when others then
                     print_log('@Populate active_tbl-102, user_name '||rec.user_name);            
                     print_log('@Populate active_tbl-102, message ='||sqlerrm);
                   end;
                 --                      
                 end if;          
               end if;
               --
              exception
               when others then
                rollback to start_here;
                print_log('Other errors during reset of fnd_user.user_guid, current_user: '||rec.user_name||' message ='||sqlerrm);           
              end;
             --          
            else --if p_reset_flag <> 'Y' then
             --
             if v_psft_empl_status ='Active' then
              --
              begin 
                --
                n_loc :='0009';
                --
                indx1 :=indx1 + 1;
                --                    
                active_tbl(indx1).NT_ID            :=rec.user_name;
                active_tbl(indx1).NAME             :=rec.ebiz_user_desc;
                active_tbl(indx1).PSFT_EMPL_STATUS :=v_psft_empl_status;
                active_tbl(indx1).OID_USER_COUNT   :=n_count;
                active_tbl(indx1).EBS_GUID         :=rec.user_guid;
                active_tbl(indx1).OID_GUID         :=v_oid_guid;
                active_tbl(indx1).COMMENTS         :='OID EBS GUID Mismatch.' ;                                                                       
               --
              exception
                when others then
                 print_log('@Populate active_tbl, user_name '||rec.user_name);            
                 print_log('@Populate active_tbl, message ='||sqlerrm);
              end; 
              --                  
             else  --if v_psft_empl_status not equals 'Active' then
              --
              begin 
                --
                n_loc :='0010';
                --
                indx2 :=indx2 + 1;
                --                    
                active_tbl(indx2).NT_ID            :=rec.user_name;
                active_tbl(indx2).NAME             :=rec.ebiz_user_desc;
                active_tbl(indx2).PSFT_EMPL_STATUS :=v_psft_empl_status;
                active_tbl(indx2).OID_USER_COUNT   :=n_count;
                active_tbl(indx2).EBS_GUID         :=rec.user_guid;
                active_tbl(indx2).OID_GUID         :=v_oid_guid;
                active_tbl(indx2).COMMENTS         :='OID EBS GUID Mismatch.' ;                                                                       
               --
              exception
                when others then
                 print_log('@Populate active_tbl, user_name '||rec.user_name);            
                 print_log('@Populate active_tbl, message ='||sqlerrm);
              end; 
              --                  
             end if;        
            end if;           
           --
          else
           --
           Null; --what this means is we have no issues
           --
          end if;
          --
         else --if ((v_oid_guid is not null)) then
           --
           if v_psft_empl_status ='Active' then
               --
               begin 
                --
                n_loc :='0011';
                --
                --
                indx1 :=indx1 + 1;
                --                
                inactive_tbl(indx1).NT_ID            :=rec.user_name;
                inactive_tbl(indx1).NAME             :=rec.ebiz_user_desc;
                inactive_tbl(indx1).PSFT_EMPL_STATUS :=v_psft_empl_status;
                inactive_tbl(indx1).OID_USER_COUNT   :=n_count;
                inactive_tbl(indx1).EBS_GUID         :=rec.user_guid;
                inactive_tbl(indx1).OID_GUID         :=v_oid_guid;
                inactive_tbl(indx1).COMMENTS         :='Verify FND User Record Equals NT ID.';                                                                       
                --
               exception
                when others then
                 print_log('@Populate inactive_tbl, user_name '||rec.user_name);            
                 print_log('@Populate inactive_tbl, message ='||sqlerrm);
               end;         
               --           
           else --if v_psft_empl_status not equals 'Active' then
           --
               begin 
                --
                n_loc :='0013';
                --
                --
                indx2 :=indx2 + 1;
                --                
                inactive_tbl(indx2).NT_ID            :=rec.user_name;
                inactive_tbl(indx2).NAME             :=rec.ebiz_user_desc;
                inactive_tbl(indx2).PSFT_EMPL_STATUS :=v_psft_empl_status;
                inactive_tbl(indx2).OID_USER_COUNT   :=n_count;
                inactive_tbl(indx2).EBS_GUID         :=rec.user_guid;
                inactive_tbl(indx2).OID_GUID         :=v_oid_guid;
                inactive_tbl(indx2).COMMENTS         :='None.';                                                                       
                --
               exception
                when others then
                 print_log('@Populate inactive_tbl, user_name '||rec.user_name);            
                 print_log('@Populate inactive_tbl, message ='||sqlerrm);
               end;         
               --            
           end if;
         end if;
         --     
     --
     -- Begin Ver 1.3
     elsif v_psft_empl_status ='InActive' then --Inactive users
             --
              begin  
               --
               savepoint start_here;
               --
               update fnd_user
               set user_guid =Null --clear GUID value for Inactive users
               where 1 =1
                 and user_id =rec.user_id;
                   --
                   if (sql%rowcount >0) then
                           --
                                --
                                begin  
                                    --
                                    indx2 :=indx2 + 1;
                                    --
                                    n_loc :='0006';
                                    --
                                    active_tbl(indx2).NT_ID            :=rec.user_name;
                                    active_tbl(indx2).NAME             :=rec.ebiz_user_desc;
                                    active_tbl(indx2).PSFT_EMPL_STATUS :=v_psft_empl_status;
                                    active_tbl(indx2).OID_USER_COUNT   :=n_count;
                                    active_tbl(indx2).EBS_GUID         :=rec.user_guid;
                                    active_tbl(indx2).OID_GUID         :=v_oid_guid;
                                    active_tbl(indx2).COMMENTS         :='Message: Inactive user: FND USER_GUID was successfully cleared.' ;                                                                       
                                   --
                                exception
                                    when others then
                                     print_log('@Populate active_tbl-101, user_name '||rec.user_name);            
                                     print_log('@Populate active_tbl-101, message ='||sqlerrm);
                                end;
                                --                        
                   else
                                 --
                               --
                               begin 
                                --
                                n_loc :='0008';
                                --
                                indx2 :=indx2 + 1;
                                --                        
                                active_tbl(indx2).NT_ID            :=rec.user_name;
                                active_tbl(indx2).NAME             :=rec.ebiz_user_desc;
                                active_tbl(indx2).PSFT_EMPL_STATUS :=v_psft_empl_status;
                                active_tbl(indx2).OID_USER_COUNT   :=n_count;
                                active_tbl(indx2).EBS_GUID         :=rec.user_guid;
                                active_tbl(indx2).OID_GUID         :=v_oid_guid;
                                active_tbl(indx2).COMMENTS         :=Substr('@ Inactive user: Failed to reset fnd user_guid for user: '||rec.user_name||' Msg ='||sqlerrm, 1, 2000);                                                                       
                               --
                               exception
                                when others then
                                 print_log('@Populate active_tbl-102, user_name '||rec.user_name);            
                                 print_log('@Populate active_tbl-102, message ='||sqlerrm);
                               end;
                               --                          
                   end if;
                   --
              exception
               when others then
                rollback to start_here;
                print_log('Other errors during reset of Inactive users: fnd_user.user_guid, current_user: '||rec.user_name||' message ='||sqlerrm);           
              end;
             --  
     else
        Null;     
     -- End Ver 1.3
     end if; --(v_psft_empl_status <>'InActive' AND rec.end_date is null)
     --
 end loop; 
 --  
 if active_tbl.count >0 then
 --
     begin
          forall idx in active_tbl.first .. active_tbl.last --save exceptions
            insert into  xxcus.xxcus_fnd_oid_guid_log values active_tbl (idx);
            print_log(' ');        
            print_log('Successfully bulk inserted Active table');
            print_log(' ');
     exception
      when ex_dml_errors then
           l_error_count := SQL%BULK_EXCEPTIONS.count;
           print_log ('@Bulk Errors, l_error_count ='||l_error_count);
              for i in 1 .. l_error_count loop
                print_log ('Error: ' || i ||
                  ' Error Index: ' || sql%bulk_exceptions(i).error_index ||
                  ' Message: ' || sqlerrm(-sql%bulk_exceptions(i).error_code));
              end loop;
      when others then
             print_log('@Active Table: Issue in bulk insert of GUID messages, error ='||sqlerrm);
     end;
   --     
 --                  
 end if;
 --  
 if inactive_tbl.count >0 then
 --
     begin
          forall idx in inactive_tbl.first .. inactive_tbl.last --save exceptions
            insert into  xxcus.xxcus_fnd_oid_guid_log values inactive_tbl (idx);
            print_log(' ');        
            print_log('Successfully bulk inserted InActive table');
            print_log(' ');
     exception
      when ex_dml_errors then
           l_error_count := SQL%BULK_EXCEPTIONS.count;
           print_log ('@Bulk Errors, l_error_count ='||l_error_count);
              for i in 1 .. l_error_count loop
                print_log ('Error: ' || i ||
                  ' Error Index: ' || sql%bulk_exceptions(i).error_index ||
                  ' Message: ' || sqlerrm(-sql%bulk_exceptions(i).error_code));
              end loop;
      when others then
             print_log('@InActive Table: Issue in bulk insert of GUID messages, error ='||sqlerrm);
     end;
   --     
 --                  
 end if;
 -- 
 -- Print header record in the concurrent output file
 print_output
  (
     '|NT_ID|NAME|PSFT_EMPL_STATUS|OID_USER_COUNT|EBS_GUID|OID_GUID|COMMENTS|'
  );
 --
 -- Print detail records in the concurrent output file
  for rec in (
               select nt_id, name, psft_empl_status, oid_user_count, ebs_guid, oid_guid, comments     
               from   xxcus.xxcus_fnd_oid_guid_log 
               order by 
                case
                 when psft_empl_status ='Active' then 1
                 else 2
                end asc
               ,nt_id asc
             ) 
  loop 
   --
    print_output (
               '|'||
               rec.nt_id||
               '|'||
               rec.name||
               '|'||
               rec.psft_empl_status||
               '|'||
               rec.oid_user_count||
               '|'||
               rec.ebs_guid||
               '|'||
               rec.oid_guid||
               '|'||
               rec.comments||
               '|'
              );   
   --
        n_loc :='0014';
  end loop;  
 --   
exception
when others then
 print_log('**** CRITICAL ERROR ****, n_user_id ='||n_user_id||', n_person_id ='||n_person_id||', n_loc ='||n_loc||', message ='||sqlerrm);
end reset_guid;
-- 
END XXCUS_OID_FND_UTIL;
/
