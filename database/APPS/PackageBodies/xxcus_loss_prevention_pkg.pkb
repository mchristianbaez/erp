CREATE OR REPLACE PACKAGE BODY APPS.xxcus_loss_prevention_pkg IS

  /****************************************************************************************
    File Name: xxcus_loss_prevention_pkg
    
    PROGRAM TYPE: PL/SQL Package spec and body
    
    PURPOSE: Create iExpense data extract for Loss Protection Auditor
    
    HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     26-Nov-2012   Luong Vu        Initial creation of the package
    1.1     01/14/2011    Luong Vu        Add history comment
                                          Change method for getting current fiscal period
                                          (l_period).  SR 188842, RFC 36356
    1.2    07/18/2016    Balaguru Seshadri TMS  20160718-00205 and ESMS 325171                                           
  *****************************************************************************************/
  PROCEDURE create_oie_lp_reports(errbuf  OUT VARCHAR2
                                 ,retcode OUT NUMBER) IS
  
    i                INTEGER := 0;
    l_period         VARCHAR2(15);
    l_file_dir       VARCHAR2(100) := 'ORACLE_LP_EXTR';
    l_file_name_temp VARCHAR2(100);
    l_file_name_fin  VARCHAR2(100);
    --l_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar    
    l_sid               VARCHAR2(25);
    l_sec               VARCHAR2(150) := 'START';
    l_file_handle_recon utl_file.file_type;
    l_err_code          NUMBER;
    l_err_msg           VARCHAR2(150);
    l_procedure_name    VARCHAR2(50) DEFAULT 'create_oie_lp_reports';
    l_distro_list       VARCHAR2(50) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
  BEGIN
  
    --dbms_output.enable(100000000);
    l_sec := 'Get DB name';
    -- Get DB name
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
  
    l_sec := 'Get period name';
    --JG - Updated period find logic to leverage gl_periods and sys date. ESMS SR 238964 
    -- SR 188842 START
    -- Get max period name        
    /*    SELECT to_char(MAX(to_date(period_name, 'MM-YYYY')), 'Mon-YYYY')
    INTO l_period
    FROM apps.xxeis_lp_fuel_same_day_v;*/
  
    /*SELECT period_name
      INTO l_period
      FROM gl.gl_periods
     WHERE trunc(SYSDATE) BETWEEN start_date AND end_date
       AND period_set_name = l_calendar;*/
    --SR 188842 END
    
    SELECT period_name
     INTO l_period
      FROM gl.gl_periods                    
      WHERE period_name not like 'FYE%'
      AND end_date = (SELECT start_date
      FROM gl.gl_periods                   
      WHERE sysDate between start_date and end_date) -1;
    /*END ESMS 238964 Update*/  
    -------------------------------------------------------------------------------------
    --                             Fuel Spend Same Day Same City                       --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report Fuel Spend Same Day Same City';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_fuel_same_day_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
      -- Get record count.  If 0, get next group value to process    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_fuel_same_day_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
        -- init file parm             
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Fuel Spend Same Day Same City ' || l_period ||
                            '.csv';
        l_file_name_fin  := c_cur.rpt_grp || ' ' ||
                            'Fuel Spend Same Day Same City ' || l_period ||
                            '.csv';
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Writing report Fuel Spend Same Day Same City';
      
        FOR c_fuel IN (SELECT *
                         FROM apps.xxeis_lp_fuel_same_day_v
                        WHERE rpt_grp = c_cur.rpt_grp
                          AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_fuel.full_name) || '","' ||
                             TRIM(c_fuel.employee_number) || '","' ||
                             TRIM(c_fuel.expense_report_number) || '","' ||
                             TRIM(c_fuel.transaction_date) || '","' ||
                             TRIM(c_fuel.dow) || '","' ||
                             TRIM(c_fuel.amount_spent) || '","' ||
                             TRIM(c_fuel.item_description) || '","' ||
                             TRIM(c_fuel.sub_category) || '","' ||
                             TRIM(c_fuel.merchant_name) || '","' ||
                             TRIM(c_fuel.merchant_location) || '","' ||
                             TRIM(c_fuel.mcc_code) || '","' ||
                             TRIM(c_fuel.mcc_code_no) || '","' ||
                             TRIM(c_fuel.justification) || '","' ||
                             TRIM(c_fuel.business_purpose) || '","' ||
                             TRIM(c_fuel.remarks) || '","' ||
                             TRIM(c_fuel.attendees) || '","' ||
                             TRIM(c_fuel.approver_name) || '","' ||
                             TRIM(c_fuel.query_descr) || '","' ||
                             TRIM(c_fuel.card_program_name) || '","' ||
                             TRIM(c_fuel.cardmember_name) || '","' ||
                             TRIM(c_fuel.oracle_product) || '","' ||
                             TRIM(c_fuel.oracle_cost_center) || '","' ||
                             TRIM(c_fuel.period_name) || '","' ||
                             TRIM(c_fuel.job_family) || '","' ||
                             TRIM(c_fuel.job_family_descr) || '","' ||
                             TRIM(c_fuel.lob_name) || '","' ||
                             TRIM(c_fuel.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                              Expense Type Ranked                                --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report Expense Type Ranked';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_exp_by_type_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_exp_by_type_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Expense Type Ranked ' || l_period || '.csv';
        l_file_name_fin  := c_cur.rpt_grp || ' ' || 'Expense Type Ranked ' ||
                            l_period || '.csv';
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
        utl_file.put_line(l_file_handle_recon,
                          '"SUB_CATEGORY","AMOUNT_SPENT","PERIOD_NAME","RPT_GRP"');
      
        l_sec := 'writing report Expense Type Ranked';
      
        FOR c_ebt IN (SELECT *
                        FROM apps.xxeis_lp_exp_by_type_v
                       WHERE rpt_grp = c_cur.rpt_grp
                         AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_ebt.sub_category) || '","' ||
                             TRIM(c_ebt.amount_spent) || '","' ||
                             TRIM(c_ebt.period_name) || '","' ||
                             TRIM(c_ebt.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                              Employee Spend Ranked                              --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report Expense Type Ranked';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_empl_exp_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_empl_exp_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Employee Spend Ranked ' || l_period || '.csv';
        l_file_name_fin  := c_cur.rpt_grp || ' ' ||
                            'Employee Spend Ranked ' || l_period || '.csv';
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","ORACLE_COST_CENTER","AMOUNT_SPENT","PERIOD_NAME","RPT_GRP"');
      
        l_sec := 'Writing report Expense Type Ranked';
      
        FOR c_empl IN (SELECT *
                         FROM apps.xxeis_lp_empl_exp_v
                        WHERE rpt_grp = c_cur.rpt_grp
                          AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_empl.full_name) || '","' ||
                             TRIM(c_empl.employee_number) || '","' ||
                             TRIM(c_empl.oracle_cost_center) || '","' ||
                             TRIM(c_empl.amount_spent) || '","' ||
                             TRIM(c_empl.period_name) || '","' ||
                             TRIM(c_empl.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
  
    -------------------------------------------------------------------------------------
    --                 Automobile Service Transactions by MCC Code                     --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report Automobile Service Transactions by MCC Code';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_auto_srvc_mcc_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_auto_srvc_mcc_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Automobile Service Transactions by MCC Code ' ||
                            l_period || '.csv';
      
        l_file_name_fin := c_cur.rpt_grp || ' ' ||
                           'Automobile Service Transactions by MCC Code ' ||
                           l_period || '.csv';
      
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
      
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
      
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Writing report Automobile Service Transactions by MCC Code';
      
        FOR c_auto IN (SELECT *
                         FROM apps.xxeis_lp_auto_srvc_mcc_v
                        WHERE rpt_grp = c_cur.rpt_grp
                          AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_auto.full_name) || '","' ||
                             TRIM(c_auto.employee_number) || '","' ||
                             TRIM(c_auto.expense_report_number) || '","' ||
                             TRIM(c_auto.transaction_date) || '","' ||
                             TRIM(c_auto.dow) || '","' ||
                             TRIM(c_auto.amount_spent) || '","' ||
                             TRIM(c_auto.item_description) || '","' ||
                             TRIM(c_auto.sub_category) || '","' ||
                             TRIM(c_auto.merchant_name) || '","' ||
                             TRIM(c_auto.merchant_location) || '","' ||
                             TRIM(c_auto.mcc_code) || '","' ||
                             TRIM(c_auto.mcc_code_no) || '","' ||
                             TRIM(c_auto.justification) || '","' ||
                             TRIM(c_auto.business_purpose) || '","' ||
                             TRIM(c_auto.remarks) || '","' ||
                             TRIM(c_auto.attendees) || '","' ||
                             TRIM(c_auto.approver_name) || '","' ||
                             TRIM(c_auto.query_descr) || '","' ||
                             TRIM(c_auto.card_program_name) || '","' ||
                             TRIM(c_auto.cardmember_name) || '","' ||
                             TRIM(c_auto.oracle_product) || '","' ||
                             TRIM(c_auto.oracle_cost_center) || '","' ||
                             TRIM(c_auto.period_name) || '","' ||
                             TRIM(c_auto.job_family) || '","' ||
                             TRIM(c_auto.job_family_descr) || '","' ||
                             TRIM(c_auto.lob_name) || '","' ||
                             TRIM(c_auto.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
  
    -------------------------------------------------------------------------------------
    --                     Entertainment Transactions by MCC Code                      --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report Entertainment Transactions by MCC Code';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_entr_xsact_mcc_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_entr_xsact_mcc_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Entertainment Transactions by MCC Code ' ||
                            l_period || '.csv';
      
        l_file_name_fin := c_cur.rpt_grp || ' ' ||
                           'Entertainment Transactions by MCC Code ' ||
                           l_period || '.csv';
      
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
      
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
      
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Writing report Entertainment Transactions by MCC Code';
      
        FOR c_exm IN (SELECT *
                        FROM apps.xxeis_lp_entr_xsact_mcc_v
                       WHERE rpt_grp = c_cur.rpt_grp
                         AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_exm.full_name) || '","' ||
                             TRIM(c_exm.employee_number) || '","' ||
                             TRIM(c_exm.expense_report_number) || '","' ||
                             TRIM(c_exm.transaction_date) || '","' ||
                             TRIM(c_exm.dow) || '","' ||
                             TRIM(c_exm.amount_spent) || '","' ||
                             TRIM(c_exm.item_description) || '","' ||
                             TRIM(c_exm.sub_category) || '","' ||
                             TRIM(c_exm.merchant_name) || '","' ||
                             TRIM(c_exm.merchant_location) || '","' ||
                             TRIM(c_exm.mcc_code) || '","' ||
                             TRIM(c_exm.mcc_code_no) || '","' ||
                             TRIM(c_exm.justification) || '","' ||
                             TRIM(c_exm.business_purpose) || '","' ||
                             TRIM(c_exm.remarks) || '","' ||
                             TRIM(c_exm.attendees) || '","' ||
                             TRIM(c_exm.approver_name) || '","' ||
                             TRIM(c_exm.query_descr) || '","' ||
                             TRIM(c_exm.card_program_name) || '","' ||
                             TRIM(c_exm.cardmember_name) || '","' ||
                             TRIM(c_exm.oracle_product) || '","' ||
                             TRIM(c_exm.oracle_cost_center) || '","' ||
                             TRIM(c_exm.period_name) || '","' ||
                             TRIM(c_exm.job_family) || '","' ||
                             TRIM(c_exm.job_family_descr) || '","' ||
                             TRIM(c_exm.lob_name) || '","' ||
                             TRIM(c_exm.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                         Fuel Spend and Mileage in Same Month                    --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report Fuel Spend and Mileage in Same Month';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_fuel_mile_month_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_fuel_mile_month_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Fuel Spend and Mileage in Same Month ' ||
                            l_period || '.csv';
        l_file_name_fin  := c_cur.rpt_grp || ' ' ||
                            'Fuel Spend and Mileage in Same Month ' ||
                            l_period || '.csv';
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Writing report Fuel Spend and Mileage in Same Month';
      
        FOR c_fmm IN (SELECT *
                        FROM apps.xxeis_lp_fuel_mile_month_v
                       WHERE rpt_grp = c_cur.rpt_grp
                         AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_fmm.full_name) || '","' ||
                             TRIM(c_fmm.employee_number) || '","' ||
                             TRIM(c_fmm.expense_report_number) || '","' ||
                             TRIM(c_fmm.transaction_date) || '","' ||
                             TRIM(c_fmm.dow) || '","' ||
                             TRIM(c_fmm.amount_spent) || '","' ||
                             TRIM(c_fmm.item_description) || '","' ||
                             TRIM(c_fmm.sub_category) || '","' ||
                             TRIM(c_fmm.merchant_name) || '","' ||
                             TRIM(c_fmm.merchant_location) || '","' ||
                             TRIM(c_fmm.mcc_code) || '","' ||
                             TRIM(c_fmm.mcc_code_no) || '","' ||
                             TRIM(c_fmm.justification) || '","' ||
                             TRIM(c_fmm.business_purpose) || '","' ||
                             TRIM(c_fmm.remarks) || '","' ||
                             TRIM(c_fmm.attendees) || '","' ||
                             TRIM(c_fmm.approver_name) || '","' ||
                             TRIM(c_fmm.query_descr) || '","' ||
                             TRIM(c_fmm.card_program_name) || '","' ||
                             TRIM(c_fmm.cardmember_name) || '","' ||
                             TRIM(c_fmm.oracle_product) || '","' ||
                             TRIM(c_fmm.oracle_cost_center) || '","' ||
                             TRIM(c_fmm.period_name) || '","' ||
                             TRIM(c_fmm.job_family) || '","' ||
                             TRIM(c_fmm.job_family_descr) || '","' ||
                             TRIM(c_fmm.lob_name) || '","' ||
                             TRIM(c_fmm.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                      PNC Purchases vs Out of Pocket Purchases                   --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report PNC Purchases vs Out of Pocket Purchases';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_pnc_vs_oop_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_pnc_vs_oop_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'PNC Purchases vs Out of Pocket Purchases ' ||
                            l_period || '.csv';
        l_file_name_fin  := c_cur.rpt_grp || ' ' ||
                            'PNC Purchases vs Out of Pocket Purchases ' ||
                            l_period || '.csv';
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","PERIOD_NAME","OOP_AMOUNT","PNC_AMOUNT","RPT_GRP"');
      
        l_sec := 'Writing report PNC Purchases vs Out of Pocket Purchases';
      
        FOR c_pvo IN (SELECT *
                        FROM apps.xxeis_lp_pnc_vs_oop_v
                       WHERE rpt_grp = c_cur.rpt_grp
                         AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_pvo.full_name) || '","' ||
                             TRIM(c_pvo.employee_number) || '","' ||
                             TRIM(c_pvo.period_name) || '","' ||
                             TRIM(c_pvo.oop_amount) || '","' ||
                             TRIM(c_pvo.pnc_amount) || '","' ||
                             TRIM(c_pvo.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                   Inventory Purchases - No PO # Recorded                        --
    -------------------------------------------------------------------------------------+
  
    l_sec := 'Processing report for Inventory Purchases - No PO # Recorded';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_invent_nopo_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_invent_nopo_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Inventory Purchases - No PO # Recorded ' ||
                            l_period || '.csv';
        l_file_name_fin  := c_cur.rpt_grp || ' ' ||
                            'Inventory Purchases - No PO # Recorded ' ||
                            l_period || '.csv';
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Writing report for Inventory Purchases - No PO # Recorded';
      
        FOR c_invent IN (SELECT *
                           FROM apps.xxeis_lp_invent_nopo_v
                          WHERE rpt_grp = c_cur.rpt_grp
                            AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_invent.full_name) || '","' ||
                             TRIM(c_invent.employee_number) || '","' ||
                             TRIM(c_invent.expense_report_number) || '","' ||
                             TRIM(c_invent.transaction_date) || '","' ||
                             TRIM(c_invent.dow) || '","' ||
                             TRIM(c_invent.amount_spent) || '","' ||
                             TRIM(c_invent.item_description) || '","' ||
                             TRIM(c_invent.sub_category) || '","' ||
                             TRIM(c_invent.merchant_name) || '","' ||
                             TRIM(c_invent.merchant_location) || '","' ||
                             TRIM(c_invent.mcc_code) || '","' ||
                             TRIM(c_invent.mcc_code_no) || '","' ||
                             TRIM(c_invent.justification) || '","' ||
                             TRIM(c_invent.business_purpose) || '","' ||
                             TRIM(c_invent.remarks) || '","' ||
                             TRIM(c_invent.attendees) || '","' ||
                             TRIM(c_invent.approver_name) || '","' ||
                             TRIM(c_invent.query_descr) || '","' ||
                             TRIM(c_invent.card_program_name) || '","' ||
                             TRIM(c_invent.cardmember_name) || '","' ||
                             TRIM(c_invent.oracle_product) || '","' ||
                             TRIM(c_invent.oracle_cost_center) || '","' ||
                             TRIM(c_invent.period_name) || '","' ||
                             TRIM(c_invent.job_family) || '","' ||
                             TRIM(c_invent.job_family_descr) || '","' ||
                             TRIM(c_invent.lob_name) || '","' ||
                             TRIM(c_invent.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                                 GIFT_CARD                                   --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report for Gift Card';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_gift_card_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_gift_card_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Gift Cards ' || l_period || '.csv';
        l_file_name_fin  := c_cur.rpt_grp || ' ' || 'Gift Cards ' ||
                            l_period || '.csv';
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Writing report for Gift Card';
      
        FOR c_gift IN (SELECT *
                         FROM apps.xxeis_lp_gift_card_v
                        WHERE rpt_grp = c_cur.rpt_grp
                          AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_gift.full_name) || '","' ||
                             TRIM(c_gift.employee_number) || '","' ||
                             TRIM(c_gift.expense_report_number) || '","' ||
                             TRIM(c_gift.transaction_date) || '","' ||
                             TRIM(c_gift.dow) || '","' ||
                             TRIM(c_gift.amount_spent) || '","' ||
                             TRIM(c_gift.item_description) || '","' ||
                             TRIM(c_gift.sub_category) || '","' ||
                             TRIM(c_gift.merchant_name) || '","' ||
                             TRIM(c_gift.merchant_location) || '","' ||
                             TRIM(c_gift.mcc_code) || '","' ||
                             TRIM(c_gift.mcc_code_no) || '","' ||
                             TRIM(c_gift.justification) || '","' ||
                             TRIM(c_gift.business_purpose) || '","' ||
                             TRIM(c_gift.remarks) || '","' ||
                             TRIM(c_gift.attendees) || '","' ||
                             TRIM(c_gift.approver_name) || '","' ||
                             TRIM(c_gift.query_descr) || '","' ||
                             TRIM(c_gift.card_program_name) || '","' ||
                             TRIM(c_gift.cardmember_name) || '","' ||
                             TRIM(c_gift.oracle_product) || '","' ||
                             TRIM(c_gift.oracle_cost_center) || '","' ||
                             TRIM(c_gift.period_name) || '","' ||
                             TRIM(c_gift.job_family) || '","' ||
                             TRIM(c_gift.job_family_descr) || '","' ||
                             TRIM(c_gift.lob_name) || '","' ||
                             TRIM(c_gift.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                     Mileage Transactions over $100 (MRO over $300)              --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report for Mileage Transactions over $100 (MRO over $300)';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_mileage_xaction_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_mileage_xaction_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Mileage Transactions over $100 (MRO over $300) ' ||
                            l_period || '.csv';
      
        l_file_name_fin := c_cur.rpt_grp || ' ' ||
                           'Mileage Transactions over $100 (MRO over $300) ' ||
                           l_period || '.csv';
      
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
      
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
      
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Writing report for Mileage Transactions over $100 (MRO over $300)';
      
        FOR c_mile IN (SELECT *
                         FROM apps.xxeis_lp_mileage_xaction_v
                        WHERE rpt_grp = c_cur.rpt_grp
                          AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_mile.full_name) || '","' ||
                             TRIM(c_mile.employee_number) || '","' ||
                             TRIM(c_mile.expense_report_number) || '","' ||
                             TRIM(c_mile.transaction_date) || '","' ||
                             TRIM(c_mile.dow) || '","' ||
                             TRIM(c_mile.amount_spent) || '","' ||
                             TRIM(c_mile.item_description) || '","' ||
                             TRIM(c_mile.sub_category) || '","' ||
                             TRIM(c_mile.merchant_name) || '","' ||
                             TRIM(c_mile.merchant_location) || '","' ||
                             TRIM(c_mile.mcc_code) || '","' ||
                             TRIM(c_mile.mcc_code_no) || '","' ||
                             TRIM(c_mile.justification) || '","' ||
                             TRIM(c_mile.business_purpose) || '","' ||
                             TRIM(c_mile.remarks) || '","' ||
                             TRIM(c_mile.attendees) || '","' ||
                             TRIM(c_mile.approver_name) || '","' ||
                             TRIM(c_mile.query_descr) || '","' ||
                             TRIM(c_mile.card_program_name) || '","' ||
                             TRIM(c_mile.cardmember_name) || '","' ||
                             TRIM(c_mile.oracle_product) || '","' ||
                             TRIM(c_mile.oracle_cost_center) || '","' ||
                             TRIM(c_mile.period_name) || '","' ||
                             TRIM(c_mile.job_family) || '","' ||
                             TRIM(c_mile.job_family_descr) || '","' ||
                             TRIM(c_mile.lob_name) || '","' ||
                             TRIM(c_mile.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                                 OOP_NO_MILE                                     --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report for Out of Pocket Transactions (No Mileage))';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_oop_no_mile_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_oop_no_mile_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Out of Pocket Transactions (No Mileage) ' ||
                            l_period || '.csv';
      
        l_file_name_fin := c_cur.rpt_grp || ' ' ||
                           'Out of Pocket Transactions (No Mileage) ' ||
                           l_period || '.csv';
      
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
      
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
      
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","VENDOR_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"'); --1.2
      
        l_sec := 'Writing report for Out of Pocket Transactions (No Mileage))';
      
        FOR c_onm IN (SELECT *
                        FROM apps.xxeis_lp_oop_no_mile_v
                       WHERE rpt_grp = c_cur.rpt_grp
                         AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_onm.full_name) || '","' ||
                             TRIM(c_onm.employee_number) || '","' ||
                             TRIM(c_onm.expense_report_number) || '","' ||
                             TRIM(c_onm.transaction_date) || '","' ||
                             TRIM(c_onm.dow) || '","' ||
                             TRIM(c_onm.amount_spent) || '","' ||
                             TRIM(c_onm.item_description) || '","' ||
                             TRIM(c_onm.sub_category) || '","' ||
                             --TRIM(c_onm.merchant_name) || '","' || --1.2
                             TRIM(c_onm.vendor_name) || '","' || --1.2                      
                             TRIM(c_onm.merchant_location) || '","' ||
                             TRIM(c_onm.mcc_code) || '","' ||
                             TRIM(c_onm.mcc_code_no) || '","' ||
                             TRIM(c_onm.justification) || '","' ||
                             TRIM(c_onm.business_purpose) || '","' ||
                             TRIM(c_onm.remarks) || '","' ||
                             TRIM(c_onm.attendees) || '","' ||
                             TRIM(c_onm.approver_name) || '","' ||
                             TRIM(c_onm.query_descr) || '","' ||
                             TRIM(c_onm.card_program_name) || '","' ||
                             TRIM(c_onm.cardmember_name) || '","' ||
                             TRIM(c_onm.oracle_product) || '","' ||
                             TRIM(c_onm.oracle_cost_center) || '","' ||
                             TRIM(c_onm.period_name) || '","' ||
                             TRIM(c_onm.job_family) || '","' ||
                             TRIM(c_onm.job_family_descr) || '","' ||
                             TRIM(c_onm.lob_name) || '","' ||
                             TRIM(c_onm.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                                 RETAIL_STORE_MCC                                --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report for Retail Store Transactions by MCC Code';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_retail_store_mcc_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_retail_store_mcc_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Retail Store Transactions by MCC Code ' ||
                            l_period || '.csv';
      
        l_file_name_fin := c_cur.rpt_grp || ' ' ||
                           'Retail Store Transactions by MCC Code ' ||
                           l_period || '.csv';
      
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
      
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
      
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Writing report for Retail Store Transactions by MCC Code';
      
        FOR c_rsm IN (SELECT *
                        FROM apps.xxeis_lp_retail_store_mcc_v
                       WHERE rpt_grp = c_cur.rpt_grp
                         AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_rsm.full_name) || '","' ||
                             TRIM(c_rsm.employee_number) || '","' ||
                             TRIM(c_rsm.expense_report_number) || '","' ||
                             TRIM(c_rsm.transaction_date) || '","' ||
                             TRIM(c_rsm.dow) || '","' ||
                             TRIM(c_rsm.amount_spent) || '","' ||
                             TRIM(c_rsm.item_description) || '","' ||
                             TRIM(c_rsm.sub_category) || '","' ||
                             TRIM(c_rsm.merchant_name) || '","' ||
                             TRIM(c_rsm.merchant_location) || '","' ||
                             TRIM(c_rsm.mcc_code) || '","' ||
                             TRIM(c_rsm.mcc_code_no) || '","' ||
                             TRIM(c_rsm.justification) || '","' ||
                             TRIM(c_rsm.business_purpose) || '","' ||
                             TRIM(c_rsm.remarks) || '","' ||
                             TRIM(c_rsm.attendees) || '","' ||
                             TRIM(c_rsm.approver_name) || '","' ||
                             TRIM(c_rsm.query_descr) || '","' ||
                             TRIM(c_rsm.card_program_name) || '","' ||
                             TRIM(c_rsm.cardmember_name) || '","' ||
                             TRIM(c_rsm.oracle_product) || '","' ||
                             TRIM(c_rsm.oracle_cost_center) || '","' ||
                             TRIM(c_rsm.period_name) || '","' ||
                             TRIM(c_rsm.job_family) || '","' ||
                             TRIM(c_rsm.job_family_descr) || '","' ||
                             TRIM(c_rsm.lob_name) || '","' ||
                             TRIM(c_rsm.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                   Service and Membership Transactions by MCC Code               --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report for Service and Membership Transactions by MCC Code';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_serv_mem_mcc_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_serv_mem_mcc_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Service and Membership Transactions by MCC Code ' ||
                            l_period || '.csv';
      
        l_file_name_fin := c_cur.rpt_grp || ' ' ||
                           'Service and Membership Transactions by MCC Code ' ||
                           l_period || '.csv';
      
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
      
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
      
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Writing report for Service and Membership Transactions by MCC Code';
      
        FOR c_smm IN (SELECT *
                        FROM apps.xxeis_lp_serv_mem_mcc_v
                       WHERE rpt_grp = c_cur.rpt_grp
                         AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_smm.full_name) || '","' ||
                             TRIM(c_smm.employee_number) || '","' ||
                             TRIM(c_smm.expense_report_number) || '","' ||
                             TRIM(c_smm.transaction_date) || '","' ||
                             TRIM(c_smm.dow) || '","' ||
                             TRIM(c_smm.amount_spent) || '","' ||
                             TRIM(c_smm.item_description) || '","' ||
                             TRIM(c_smm.sub_category) || '","' ||
                             TRIM(c_smm.merchant_name) || '","' ||
                             TRIM(c_smm.merchant_location) || '","' ||
                             TRIM(c_smm.mcc_code) || '","' ||
                             TRIM(c_smm.mcc_code_no) || '","' ||
                             TRIM(c_smm.justification) || '","' ||
                             TRIM(c_smm.business_purpose) || '","' ||
                             TRIM(c_smm.remarks) || '","' ||
                             TRIM(c_smm.attendees) || '","' ||
                             TRIM(c_smm.approver_name) || '","' ||
                             TRIM(c_smm.query_descr) || '","' ||
                             TRIM(c_smm.card_program_name) || '","' ||
                             TRIM(c_smm.cardmember_name) || '","' ||
                             TRIM(c_smm.oracle_product) || '","' ||
                             TRIM(c_smm.oracle_cost_center) || '","' ||
                             TRIM(c_smm.period_name) || '","' ||
                             TRIM(c_smm.job_family) || '","' ||
                             TRIM(c_smm.job_family_descr) || '","' ||
                             TRIM(c_smm.lob_name) || '","' ||
                             TRIM(c_smm.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                             Personal Transactions                               --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report for Personal Transactions';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_personal_xsact_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_personal_xsact_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Personal Transactions ' || l_period || '.csv';
      
        l_file_name_fin := c_cur.rpt_grp || ' ' || 'Personal Transactions ' ||
                           l_period || '.csv';
      
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
      
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
      
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Processing report for Personal Transactions';
      
        FOR c_per IN (SELECT *
                        FROM apps.xxeis_lp_personal_xsact_v
                       WHERE rpt_grp = c_cur.rpt_grp
                         AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_per.full_name) || '","' ||
                             TRIM(c_per.employee_number) || '","' ||
                             TRIM(c_per.expense_report_number) || '","' ||
                             TRIM(c_per.transaction_date) || '","' ||
                             TRIM(c_per.dow) || '","' ||
                             TRIM(c_per.amount_spent) || '","' ||
                             TRIM(c_per.item_description) || '","' ||
                             TRIM(c_per.sub_category) || '","' ||
                             TRIM(c_per.merchant_name) || '","' ||
                             TRIM(c_per.merchant_location) || '","' ||
                             TRIM(c_per.mcc_code) || '","' ||
                             TRIM(c_per.mcc_code_no) || '","' ||
                             TRIM(c_per.justification) || '","' ||
                             TRIM(c_per.business_purpose) || '","' ||
                             TRIM(c_per.remarks) || '","' ||
                             TRIM(c_per.attendees) || '","' ||
                             TRIM(c_per.approver_name) || '","' ||
                             TRIM(c_per.query_descr) || '","' ||
                             TRIM(c_per.card_program_name) || '","' ||
                             TRIM(c_per.cardmember_name) || '","' ||
                             TRIM(c_per.oracle_product) || '","' ||
                             TRIM(c_per.oracle_cost_center) || '","' ||
                             TRIM(c_per.period_name) || '","' ||
                             TRIM(c_per.job_family) || '","' ||
                             TRIM(c_per.job_family_descr) || '","' ||
                             TRIM(c_per.lob_name) || '","' ||
                             TRIM(c_per.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                               Weekend Transactions                              --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report for Weekend Transactions';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_wkend_xsact_v
                   WHERE rpt_grp IS NOT NULL
                     AND period_name = l_period)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_wkend_xsact_v
         WHERE rpt_grp = c_cur.rpt_grp
           AND period_name = l_period;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Weekend Transactions ' || l_period || '.csv';
      
        l_file_name_fin := c_cur.rpt_grp || ' ' || 'Weekend Transactions ' ||
                           l_period || '.csv';
      
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
      
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
      
        utl_file.put_line(l_file_handle_recon,
                          '"FULL_NAME","EMPLOYEE_NUMBER","EXPENSE_REPORT_NUMBER","TRANSACTION_DATE","DOW","MOUNT_SPENT","ITEM_DESCRIPTION","SUB_CATEGORY","MERCHANT_NAME","MERCHANT_LOCATION","MCC_CODE","MCC_CODE_NO","JUSTIFICATION","BUSINESS_PURPOSE","REMARKS","ATTENDEES","APPROVER_NAME","QUERY_DESCR","CARD_PROGRAM_NAME","CARDMEMBER_NAME","ORACLE_PRODUCT","ORACLE_COST_CENTER","PERIOD_NAME","JOB_FAMILY","JOB_FAMILY_DESCR","LOB_NAME","RPT_GRP"');
      
        l_sec := 'Writing report for Weekend Transactions';
      
        FOR c_wkend IN (SELECT *
                          FROM apps.xxeis_lp_wkend_xsact_v
                         WHERE rpt_grp = c_cur.rpt_grp
                           AND period_name = l_period)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_wkend.full_name) || '","' ||
                             TRIM(c_wkend.employee_number) || '","' ||
                             TRIM(c_wkend.expense_report_number) || '","' ||
                             TRIM(c_wkend.transaction_date) || '","' ||
                             TRIM(c_wkend.dow) || '","' ||
                             TRIM(c_wkend.amount_spent) || '","' ||
                             TRIM(c_wkend.item_description) || '","' ||
                             TRIM(c_wkend.sub_category) || '","' ||
                             TRIM(c_wkend.merchant_name) || '","' ||
                             TRIM(c_wkend.merchant_location) || '","' ||
                             TRIM(c_wkend.mcc_code) || '","' ||
                             TRIM(c_wkend.mcc_code_no) || '","' ||
                             TRIM(c_wkend.justification) || '","' ||
                             TRIM(c_wkend.business_purpose) || '","' ||
                             TRIM(c_wkend.remarks) || '","' ||
                             TRIM(c_wkend.attendees) || '","' ||
                             TRIM(c_wkend.approver_name) || '","' ||
                             TRIM(c_wkend.query_descr) || '","' ||
                             TRIM(c_wkend.card_program_name) || '","' ||
                             TRIM(c_wkend.cardmember_name) || '","' ||
                             TRIM(c_wkend.oracle_product) || '","' ||
                             TRIM(c_wkend.oracle_cost_center) || '","' ||
                             TRIM(c_wkend.period_name) || '","' ||
                             TRIM(c_wkend.job_family) || '","' ||
                             TRIM(c_wkend.job_family_descr) || '","' ||
                             TRIM(c_wkend.lob_name) || '","' ||
                             TRIM(c_wkend.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
    -------------------------------------------------------------------------------------
    --                              Unprocessed Transactions                           --
    -------------------------------------------------------------------------------------
  
    l_sec := 'Processing report for Unprocessed Transactions';
  
    FOR c_cur IN (SELECT DISTINCT rpt_grp
                    FROM apps.xxeis_lp_unprocessed_xsact_v
                   WHERE rpt_grp IS NOT NULL)
    LOOP
    
      BEGIN
        SELECT COUNT(*)
          INTO i
          FROM apps.xxeis_lp_unprocessed_xsact_v
         WHERE rpt_grp = c_cur.rpt_grp;
      END;
    
      IF i <> 0 THEN
      
        l_file_name_temp := 'TEMP_' || c_cur.rpt_grp || '_' ||
                            'Unprocessed Transactions ' || l_period ||
                            '.csv';
      
        l_file_name_fin := c_cur.rpt_grp || ' ' ||
                           'Unprocessed Transactions ' || l_period ||
                           '.csv';
      
        fnd_file.put_line(fnd_file.log,
                          'Filename generated : ' || l_file_name_fin);
        fnd_file.put_line(fnd_file.output,
                          'Filename generated : ' || l_file_name_fin);
      
        l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                              'w');
      
        utl_file.put_line(l_file_handle_recon,
                          '"ORACLE_PRODUCT","CARD_PROGRAM_NAME","FULL_NAME","TOTAL_OUTSTANDING_AMOUNT","NUM_OF_OUTSTANDING_TRANS","RPT_GRP"');
      
        l_sec := 'Writing report for Unprocessed Transactions';
      
        FOR c_unp IN (SELECT *
                        FROM apps.xxeis_lp_unprocessed_xsact_v
                       WHERE rpt_grp = c_cur.rpt_grp)
        LOOP
        
          utl_file.put_line(l_file_handle_recon,
                            '"' || TRIM(c_unp.oracle_product) || '","' ||
                             TRIM(c_unp.card_program_name) || '","' ||
                             TRIM(c_unp.full_name) || '","' ||
                             TRIM(c_unp.total_outstanding_amount) || '","' ||
                             TRIM(c_unp.num_of_outstanding_trans) || '","' ||
                             TRIM(c_unp.rpt_grp) || '"');
        
        END LOOP;
        utl_file.fclose(l_file_handle_recon);
      
        utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                         l_file_name_fin, TRUE);
      
        fnd_file.put_line(fnd_file.log, 'File Completed ');
        fnd_file.put_line(fnd_file.output, 'File Completed ');
      END IF;
    END LOOP;
  
  EXCEPTION
  
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Loss Prevention Report program.',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Loss Prevention Report program.',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
  
  END create_oie_lp_reports;

END xxcus_loss_prevention_pkg;
/