CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_IBY_ACH_POST_PROCESS AS
   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE: 
     REVISIONS:
     Ticket                          Ver             Date               Author                            Description
     ----------------------    ----------   ----------          ------------------------      -------------------------
     20150812-00198   1.0           08/18/2015    Bala Seshadri              Created.
   **************************************************************************/
   --  
   Function rename_file
                     (
                        P_Subscription_Guid In Raw
                       ,P_Event             In Out Nocopy Wf_Event_T
                     )
      Return Varchar2 Is
      --
      L_Api_Name    Constant Varchar2(30) := 'rename_file';
      L_Api_Version Constant Number := 1.0;
      L_Rule           Varchar2(20);
      --
      l_command        varchar2(4000) :=Null;
      l_result                varchar2(4000) :=Null;
      l_msg                   varchar2(2000)   :=Null;
      --
      L_Parameter_List Wf_Parameter_List_T := Wf_Parameter_List_T();
      L_Parameter_T    Wf_Parameter_T      := Wf_Parameter_T(Null, Null);
      L_Parameter_Name Varchar2(2000);
      --
      Param_Index      Pls_Integer;
      L_Event_Key      Varchar2(240);
      L_Event_Name     Varchar2(50);
      --
      l_request_id number :=0;
      l_concurrent_program_id          Number :=0;
      l_temp_file_name varchar2(240) :=Null;
      l_original_file_name varchar2(240) :=Null;
      --
      l_fmt_pmt_instr_cp_id number :=0;
      l_pmt_instruction_id number :=0;
      l_ob_pmt_file_directory iby_payment_profiles.outbound_pmt_file_directory%type :=Null;
      --
      l_ob_pmt_file_extension iby_payment_profiles.outbound_pmt_file_extension%type :=Null;
      l_ob_pmt_file_prefix iby_payment_profiles.outbound_pmt_file_prefix%type :=Null;
      --
     l_ex    BOOLEAN;
     l_flen  NUMBER;
     l_bsize NUMBER;       
      --
   --
   Begin 
              begin 
                select concurrent_program_id
                into     l_fmt_pmt_instr_cp_id 
                from   fnd_concurrent_programs_vl
                where 1 =1
                and user_concurrent_program_name ='Format Payment Instructions with Text Output';
              exception
               when no_data_found then
                l_fmt_pmt_instr_cp_id :=0;               
               when others then
                l_fmt_pmt_instr_cp_id :=0;
              end;           
   
              L_Parameter_List := P_Event.Getparameterlist();
              L_Event_Key      := P_Event.Geteventkey();
           
              If L_Parameter_List Is Not Null Then
              
                 Param_Index := L_Parameter_List.First; 
                              
                 While (Param_Index <= L_Parameter_List.Last) 
                 
                 Loop 
                    if ( l_parameter_list(param_index).getname() ='REQUEST_ID') then 
                        --
                        l_request_id := l_parameter_list(param_index).getvalue();
                        --
                    elsif ( l_parameter_list(param_index).getname() ='CONCURRENT_PROGRAM_ID') then
                        --
                         l_concurrent_program_id := l_parameter_list(param_index).getvalue();
                        --                    
                    else
                     Null; --We don't need other parameters...
                    end if;
                    Param_Index :=L_Parameter_List.Next(Param_Index);
                    --
                 End Loop;
                --
              End If;                           
          --
          -- lets move forward with checking other things if and only if the program was "Format Payment Instructions with Text Output"
          --
          if ( l_concurrent_program_id = l_fmt_pmt_instr_cp_id ) then 
           --
           begin
            select to_number(argument1) --payment_instruction_id value
            into     l_pmt_instruction_id
            from   fnd_concurrent_requests
            where  1 =1
                and  request_id =l_request_id;
                 --
                 if ( l_pmt_instruction_id >0 ) then
                  --
                      begin
                            select b.outbound_pmt_file_directory
                                       ,b.outbound_pmt_file_extension
                                       ,b.outbound_pmt_file_prefix
                            into    l_ob_pmt_file_directory
                                      ,l_ob_pmt_file_extension
                                      ,l_ob_pmt_file_prefix                                
                            from iby_pay_instructions_all a
                                     ,iby_payment_profiles b
                            where 1 =1
                                 and a.payment_instruction_id =l_pmt_instruction_id
                                 and b.payment_profile_id =a.payment_profile_id
                                 and b.payment_profile_name ='HDS_AP_OUTSOURCE_CHK_PPP'
                                 and b.processing_type ='ELECTRONIC'; 
                                 --
                                 l_temp_file_name :=l_ob_pmt_file_prefix||l_request_id||'.'||l_ob_pmt_file_extension;
                                 -- Check if the temp file exists in the outbound directory...
                                 begin 
                                  utl_file.fgetattr('XXCUS_IBY_ACH_PMT_DIR', l_temp_file_name, l_ex, l_flen, l_bsize);
                                 exception
                                  when others then
                                   l_ex :=FALSE;
                                 end;
                                 --
                                 if (l_ex) then --temp file exists
                                      --
                                      -- Rename temp file by removing the string TEMP_, so UC4 can pick up for FTP and move further...
                                      --
                                      l_original_file_name :=replace(l_temp_file_name, 'TEMP_', '');
                                      --
                                       begin 
                                        --
                                        -- Note: The temp file created above is owned by applbizprd or a non prod account, so we cannot move or copy.
                                        -- Lets change the permissions so we can rename the temp file
                                        -- or even remove when all are done [probably handled by UC4 ]
                                        -- if the OS command fails then UC4 will not be able to remove as the it is owned by oradb
                                        --
                                        l_command :='chmod 777'
                                                 ||' '
                                                 ||l_ob_pmt_file_directory
                                                 ||'/' 
                                                 ||l_temp_file_name;
                                          --                     
                                          select xxwc_edi_iface_pkg.xxwc_oscommand_run (l_command)
                                          into   l_result 
                                          from   dual; 
                                          --
                                       exception
                                        when others then 
                                          l_msg :=substr(sqlerrm, 1, 2000);
                                                INSERT INTO XXCUS.XXCUS_IBY_ACH_LOG
                                                (
                                                  request_id
                                                 ,command
                                                 ,error_msg
                                                )
                                                VALUES
                                                (
                                                  fnd_global.conc_request_id
                                                 ,l_command
                                                 ,l_msg
                                                );
                                       end;                                        
                                      --
                                       begin 
                                        --
                                        -- rename the temp file to the final resting file name [l_original_file_name ]
                                        --
                                         l_command :='mv'
                                                   ||' '
                                                   ||l_ob_pmt_file_directory
                                                   ||'/' 
                                                   ||l_temp_file_name --temp zip file
                                                   ||' '
                                                   ||l_ob_pmt_file_directory
                                                   ||'/' 
                                                   ||l_original_file_name; --temp zip file
                                          --              
                                          select xxwc_edi_iface_pkg.xxwc_oscommand_run (l_command)
                                          into   l_result 
                                          from   dual; 
                                          --
                                       exception
                                          when others then            
                                             l_msg :=substr(sqlerrm, 1, 2000);
                                                INSERT INTO XXCUS.XXCUS_IBY_ACH_LOG
                                                (
                                                  request_id
                                                 ,command
                                                 ,error_msg
                                                )
                                                VALUES
                                                (
                                                  fnd_global.conc_request_id
                                                 ,l_command
                                                 ,l_msg
                                                );
                                            Null;
                                       end;                                            
                                      --
                                       begin 
                                        --
                                        -- Note: The original file created should carry the same privileges as the temp file that was created
                                        --
                                        l_command :='chmod 644'
                                                 ||' '
                                                 ||l_ob_pmt_file_directory
                                                 ||'/' 
                                                 ||l_original_file_name;
                                          --                     
                                          select xxwc_edi_iface_pkg.xxwc_oscommand_run (l_command)
                                          into   l_result 
                                          from   dual; 
                                          --
                                       exception
                                        when others then 
                                          l_msg :=substr(sqlerrm, 1, 2000);
                                                INSERT INTO XXCUS.XXCUS_IBY_ACH_LOG
                                                (
                                                  request_id
                                                 ,command
                                                 ,error_msg
                                                )
                                                VALUES
                                                (
                                                  fnd_global.conc_request_id
                                                 ,l_command
                                                 ,l_msg
                                                );
                                         Null;
                                       end;                                        
                                      --
                                      /* -- no need for this logic as we've used the xxwc_edi_iface_pkg.xxwc_oscommand_run to rename the file.                                      
                                      begin
                                          Null;
                                       --utl_file.frename('XXCUS_IBY_ACH_PMT_DIR', l_temp_file_name, 'XXCUS_IBY_ACH_PMT_DIR', l_original_file_name,TRUE);
                                       --utl_file.fcopy('XXCUS_IBY_ACH_PMT_DIR', l_temp_file_name, 'XXCUS_IBY_ACH_PMT_DIR', l_original_file_name);
                                      exception
                                       when others then
                                         Null;
                                      end;
                                      */
                                  --
                                 end if;
                                 --
                      exception
                        when no_data_found then
                                       l_ob_pmt_file_directory :=Null;
                                       l_ob_pmt_file_extension :=Null;
                                       l_ob_pmt_file_prefix :=Null;
                        when others then 
                                       l_ob_pmt_file_directory :=Null;
                                       l_ob_pmt_file_extension :=Null;
                                       l_ob_pmt_file_prefix :=Null;
                      end;                  
                  --
                 end if;
                 --
           exception
            when no_data_found then
               l_pmt_instruction_id :=0;
            when others then
               l_pmt_instruction_id :=0;
           end;
           --
          end if;
          --
          Return 'SUCCESS';
          -- 
      --Else
        --Null; --Other than EBIZPRD, we are not going to monitor the events...
      --End If;
   Exception
      When Others Then
       WF_CORE.CONTEXT('oracle.apps.fnd.concurrent.request.completed',
                             p_event.getEventName( ), p_subscription_guid);
       WF_EVENT.setErrorInfo(p_event, 'ERROR');      
         Return 'ERROR';
   End rename_file;    
   --
END XXCUS_IBY_ACH_POST_PROCESS;
/
