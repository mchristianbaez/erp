CREATE OR REPLACE PACKAGE BODY APPS.xxcusoie_no_zero_amt_not_pkg IS

  /********************************************************************************
  
  File Name: XXCUSOIE_NO_ZERO_AMT_NOT_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Used in AP Credit Cards Workflow to customize the notifications for zero amount transactions
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/18/2011    Mani Kumar       Initial creation of the package
  ********************************************************************************/

  PROCEDURE check_zero_amount_er(p_item_type IN VARCHAR2
                                ,p_item_key  IN VARCHAR2
                                ,p_actid     IN NUMBER
                                ,p_funmode   IN VARCHAR2
                                ,p_result    OUT NOCOPY VARCHAR2) IS
  
    l_check_number ap_checks_all.check_number%TYPE;
    --l_exp_rep_num  ap_expense_report_headers_all.invoice_num%type;
    l_check_amount ap_checks_all.amount%TYPE;
    l_debug_info   VARCHAR2(200);
    l_distro_list  VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
  BEGIN
    ap_web_utilities_pkg.logprocedure('XXCUSOIE_NO_ZERO_AMT_NOT_PKG',
                                      'Start Check Zero Amount');
  
    IF (p_funmode = 'RUN') THEN
      ------------------------------------------------------
      l_debug_info := 'Retrieve Check Number Item Attribute';
      -------------------------------------------------------
      l_check_number := wf_engine.getitemattrnumber(p_item_type, p_item_key,
                                                    'CHECK_NUMBER');
    
      /*  ------------------------------------------------------------
      l_debug_info := 'Retrieve Expense Report Number Item Attributes';
      ------------------------------------------------------------
      l_exp_rep_num := WF_ENGINE.GetItemAttrText(p_item_type,
                                                 p_item_key,
                                                 'EXPENSE_REPORT_NUMBER');*/
    
      ------------------------------------------------------------
      l_debug_info := 'Retreive Check Amount from AP Checks';
      /*If there is a exception then the standard notification will be sent. If the check amount is zero then,
      custom notification will be sent*/
      ------------------------------------------------------------                                           
      BEGIN
        SELECT SUM(amount)
          INTO l_check_amount
          FROM ap_checks
         WHERE check_number = l_check_number;
      EXCEPTION
        WHEN OTHERS THEN
          p_result := 'COMPLETE:N';
      END;
      IF nvl(l_check_amount, 0) = 0 THEN
        p_result := 'COMPLETE:Y';
      ELSE
        p_result := 'COMPLETE:N';
      END IF;
    ELSIF (p_funmode = 'CANCEL') THEN
    
      p_result := 'COMPLETE';
    
    END IF;
  
    ap_web_utilities_pkg.logprocedure('XXCUSOIE_NO_ZERO_AMT_NOT_PKG',
                                      'End Check Zero Amount');
  EXCEPTION
    WHEN OTHERS THEN
    
      wf_core.context('XXCUSOIE_NO_ZERO_AMT_NOT_PKG',
                      'check_zero_amount_er', p_item_type, p_item_key,
                      to_char(p_actid), l_debug_info);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => 'XXCUSOIE_NO_ZERO_AMT_NOT_PKG.check_zero_amount_er',
                                           p_calling => 'Exception for XXCUSOIE_NO_ZERO_AMT_NOT_PKG',
                                           p_request_id => 0,
                                           p_ora_error_msg => to_char(p_actid),
                                           p_error_desc => l_debug_info,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS');
      RAISE;
  END;

END xxcusoie_no_zero_amt_not_pkg;
/
