CREATE OR REPLACE PACKAGE BODY APPS.XXWC_SPEEDBUILD_OB_PKG
AS
   /********************************************************************************
   FILE NAME: XXWC_SPEEDBUILD_OB_PKG.pkb

   PROGRAM TYPE: PL/SQL Package Body

   PURPOSE: SpeedBuild Outbound Interfaces.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     11/20/2013    Gopi Damuluri    Initial creation of the procedure
   1.1     01/27/2014    Gopi Damuluri    TMS# 20140116-00071
                                          Added a new procedure - ORDER_HDR_LINE_ORG
   1.2     01/31/2014    Gopi Damuluri    TMS# 20140211-00049
                                          Added a new procedure - CREATE_CATALOG_FILE
   1.3     04/09/2014    Gopi Damuluri    TMS# 20140409-00184
                                          Item Sequence on EComm Requisition
   1.4     04/24/2014    Gopi Damuluri    TMS# 20140421-00080
                                          Process is updated to Pick last 2 days transactions.
   1.5     04/30/2014    Gopi Damuluri    TMS# 20140502-00193  
                                          SpeedBuild catalogfilter cat entry fix
   1.6     05/18/2014    Gopi Damuluri    TMS# 20140518-00004
                                          Resolve the issue with Master Party - Master Req
   1.7     05/27/2014    Gopi Damuluri    TMS# 20140529-00334
                                          Catalog Filter at Master Party Level
   1.8     07/01/2014    Gopi Damuluri    TMS# 20140714-00020
                                          Send the Sites to EComm even when Customer# = Site#
   ********************************************************************************/

   --Email Defaults
   g_dflt_email            fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';

   g_job_req_type_id       NUMBER  := 1162; -- WC SB JOB QUOTE
   g_mstr_req_type_id      NUMBER  := 1163; -- WC SB MASTER QUOTE

/********************************************************************************
ProcedureName : load_global_temp_tbl
Purpose       : API to load Accounts CrossReference global temporary table
                from Enterprise Transalator
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     11/20/2013    Gopi Damuluri    Initial version.
1.8     07/01/2014    Gopi Damuluri    TMS# 20140714-00020
                                       Send the Sites to EComm even when Customer# = Site#
********************************************************************************/
   PROCEDURE load_global_temp_tbl(p_date_from            IN VARCHAR2
                                , p_date_to              IN VARCHAR2)
          IS

      l_error_code              NUMBER;
      l_error_message           VARCHAR2 (240);
      
      l_date_from               DATE;
      l_date_to                 DATE;
   BEGIN
--      EXECUTE IMMEDIATE 'truncate table xxwc.xxwc_sb_gtt_tbl';
      IF p_date_from IS NULL OR p_date_to IS NULL THEN
         l_date_from := TRUNC(SYSDATE) - 2; -- Version# 1.4
         l_date_to   := TRUNC(SYSDATE);
      ELSE
         l_date_from := TO_DATE(p_date_from,'DD-MON-YYYY');
         l_date_to   := TO_DATE(p_date_to,'DD-MON-YYYY');
      END IF;         

        fnd_file.put_line (fnd_file.LOG,'l_date_from - '||l_date_from);
        fnd_file.put_line (fnd_file.LOG,'l_date_to - '||l_date_to);

      IF l_date_from IS NOT NULL AND l_date_to IS NOT NULL THEN
         INSERT INTO xxwc.xxwc_sb_gtt_tbl(header_id
                                        , quote_type_id
                                        , quote_number
                                        , quote_name
                                        , sold_to_org_id
                                        , invoice_to_org_id
                                        , ship_to_org_id          -- Version# 1.1
                                        , organization_code
                                        , delete_flag
                                        , creation_date
                                        , last_update_date)
         SELECT ooha.header_id
              , ooha.order_Type_id
              , ooha.quote_number
              , ooha.sales_document_name
              , ooha.sold_to_org_id
              , ooha.invoice_to_org_id
              , ooha.ship_to_org_id          -- Version# 1.1
              , mp.organization_code
              , 0 delete_flag
              , ooha.creation_date
              , (select MAX(ool.last_update_date) from oe_order_lines_all ool where ool.header_id = ooha.header_id) last_update_date
           FROM oe_order_headers_all  ooha
              , mtl_parameters mp
          WHERE 1 =1 
            AND ooha.order_Type_id IN (g_mstr_req_type_id, g_job_req_type_id)
            AND ooha.flow_status_code IN ('PENDING_CUSTOMER_ACCEPTANCE', 'CUSTOMER_ACCEPTED')
            AND mp.organization_id = ooha.ship_from_org_id
            AND EXISTS (SELECT '1' 
                          FROM oe_order_lines_all  oola
                         WHERE 1 = 1
                           AND ooha.header_id = oola.header_id
                           AND oola.flow_status_code IN ('PENDING_CUSTOMER_ACCEPTANCE', 'CUSTOMER_ACCEPTED')
                           AND trunc(oola.last_update_date) BETWEEN l_date_from AND l_date_to)
          UNION
                SELECT ooha.header_id
                     , ooha.order_Type_id
                     , ooha.quote_number
                     , ooha.sales_document_name
                     , ooha.sold_to_org_id
                     , ooha.invoice_to_org_id
                     , ooha.ship_to_org_id          -- Version# 1.1
                     , mp.organization_code
                     , 1 delete_flag
                     , ooha.creation_date
                     , ooha.last_update_date
                  FROM oe_order_headers_all  ooha
                     , mtl_parameters mp
                 WHERE 1 =1 
                   AND ooha.order_Type_id IN (g_mstr_req_type_id, g_job_req_type_id)
                   AND mp.organization_id = ooha.ship_from_org_id
                   AND TRUNC(ooha.expiration_date) BETWEEN l_date_from AND l_date_to
                   AND l_date_from != l_date_to;
       END IF;

   EXCEPTION
     WHEN OTHERS
     THEN
        l_error_code := SQLCODE;
        l_error_message := SUBSTR (SQLERRM, 1, 200);
        fnd_file.put_line (fnd_file.LOG,
                           'Exception Block of Load Global Temporary Table Procedure'
                          );
        fnd_file.put_line (fnd_file.LOG,
                           'The Error Code Traced is : ' || l_error_code
                          );
        fnd_file.put_line (fnd_file.LOG,
                           'The Error Message Traced is : ' || l_error_message
                          );

         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => 'XXWC_SPEEDBUILD_OB_PKG.LOAD_GLOBAL_TEMP_TBL'
           ,p_calling             => 'LOAD_GLOBAL_TEMP_TBL'
           ,p_request_id          => -1
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error in SpeedBuild Load Global Temp table, When Others Exception'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');

   END load_global_temp_tbl;

   /********************************************************************************
   ProcedureName : CREATE_FILE
   Purpose       : Common API for Outbound Interfaces.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
   1.6     05/18/2014    Gopi Damuluri    TMS# 20140518-00004
                                          Resolve the issue with Master Party - Master Req
   1.8     07/01/2014    Gopi Damuluri    TMS# 20140714-00020
                                          Send the Sites to EComm even when Customer# = Site#
   ********************************************************************************/
   PROCEDURE CREATE_FILE (p_errbuf              OUT VARCHAR2
                        , p_retcode             OUT NUMBER
                        , p_directory_path       IN VARCHAR2
                        , p_date_from            IN VARCHAR2
                        , p_date_to              IN VARCHAR2)
   IS

      -- ####################
      -- Requisition Headers
      -- ###################
      CURSOR cur_req_hdr
          IS
      SELECT hca.account_number
           , hps.party_site_number
           , hps.party_id
           , gtt.quote_type_id
           , 'ou='||LOWER(hps.party_site_number)||',o='||hca.account_number||',o=whitecap buyer organization,o=buyer organization,o=root organization' orgentity -- Version# 1.1
           , gtt.quote_name              description
           , TO_CHAR(gtt.last_update_date,'YYYY-MM-DD hh:mi:ss')||'.0000'  lastupdate
--           , TO_CHAR(gtt.last_update_date,'YYYY-MM-DD hh:mi:ss')  lastupdate
           , gtt.creation_date           creation_date
           , gtt.last_update_date        last_update_date
           , gtt.header_id               quoteID
           , gtt.delete_flag             Delete_flag
        FROM xxwc.xxwc_sb_gtt_tbl        gtt
           , hz_cust_accounts            hca
           , hz_cust_site_uses_all       hcsu
           , hz_cust_acct_sites_all      hcas
           , hz_party_sites              hps
       WHERE 1 =1
         and hca.cust_account_id       = gtt.sold_to_org_id
--         AND hcsu.site_use_id          = gtt.invoice_to_org_id -- Version# 1.1
         AND hcsu.site_use_id          = gtt.ship_to_org_id      -- Version# 1.1
         AND hcas.cust_acct_site_id    = hcsu.cust_acct_site_id
         AND hps.party_site_id         = hcas.party_site_id
         -- AND hca.account_number        != hps.party_site_number -- Version# 1.8
         ;

      -- ###################
      -- Requisition Lines
      -- ###################
      CURSOR cur_req_dtl (p_header_id IN NUMBER)
          IS
      SELECT msib.segment1                              partnum
           , oola.ordered_quantity                      quantity
           , oola.header_id                             sourceid
           , DECODE(oola.ordered_quantity,0,'1','0')    Delete_flag
           , oola.line_number                           line_number  -- Version# 1.2
        FROM oe_order_lines_all                         oola
           , mtl_system_items_b                         msib
       WHERE 1 =1 
         AND oola.header_id                           = p_header_id
         AND oola.inventory_item_id                   = msib.inventory_item_id
         AND oola.flow_status_code                   IN ('PENDING_CUSTOMER_ACCEPTANCE', 'CUSTOMER_ACCEPTED') -- Version# 1.5
         AND msib.organization_id                     = 222
    ORDER BY oola.line_number; -- Version# 1.2


/*
      SELECT oola.ordered_item                          partnum
           , oola.ordered_quantity                      quantity
           , oola.header_id                             sourceid
           , DECODE(oola.ordered_quantity,0,'1','0')    Delete_flag
        FROM oe_order_lines_all    oola
       WHERE 1 =1 
         AND oola.header_id = p_header_id;
*/

      -- ################################
      -- Global Temp Table - Req Hdr
      -- ################################
      CURSOR cur_req_gtt
          IS
      SELECT gtt.orgentity             orgentity
           , gtt.organization_code     ffmcentername
           , gtt.quote_name            Description
           , gtt.header_id             header_id
           , gtt.quote_type_id         quote_type_id
           , gtt.party_id              party_id
           , gtt.account_number        account_number
           , gtt.party_site_number     party_site_number
        FROM xxwc.xxwc_sb_gtt_tbl  gtt
       WHERE 1 = 1
         AND gtt.delete_flag     = 0
--         AND TO_CHAR(gtt.account_number) != gtt.party_site_number -- Version# 1.8
         ;

      -- #######################
      -- Master Req PartySites
      -- #######################
      CURSOR cur_party_sites (p_party_id IN NUMBER, p_customer_num  IN VARCHAR2)
          IS
      SELECT hps.party_site_number
        FROM hz_party_sites                         hps
       WHERE 1 =1
         AND hps.party_id                         = p_party_id
--         AND hps.party_site_number !              = p_customer_num -- Version# 1.8
--         AND EXISTS (SELECT '1'
--                       FROM hz_cust_accounts_all    hca
--                          , hz_cust_acct_sites_all  hcas
--                          , hz_cust_site_uses_all   hcsu
--                      WHERE 1 = 1
--                        AND hca.account_number      = p_customer_num
--                        AND hcas.cust_account_id    = hca.cust_account_id
--                        AND hcas.cust_acct_site_id  = hcsu.cust_acct_site_id
--                        AND hcsu.site_use_code      = 'BILL_TO'
--                        AND hcas.party_site_id      = hps.party_site_id)
         AND EXISTS (SELECT '1'
                       FROM hz_cust_acct_sites_all  hcas
                          , hz_cust_site_uses_all   hcsu
                          , hz_cust_accounts_all    hca    -- Version# 1.6
                      WHERE 1 = 1
                        AND hcsu.site_use_code      = 'SHIP_TO'
                        AND hcas.cust_acct_site_id  = hcsu.cust_acct_site_id
                        AND hca.cust_account_id     = hcas.cust_account_id          -- Version# 1.6
                        AND hca.account_number      = p_customer_num                -- Version# 1.6
                        AND hcas.party_site_id      = hps.party_site_id);


      -- Intialize Variables
      l_err_msg                  VARCHAR2 (2000);
      l_err_code                 NUMBER;
      l_sec                      VARCHAR2 (150);
      l_procedure_name           VARCHAR2 (75) := 'XXWC_SPEEDBUILD_OB_PKG.create_file';

      --File Variables
      l_file_handle              UTL_FILE.file_type;
      l_req_hdr_file_name        VARCHAR2 (150) := 'wc_requisitionheader.csv';
      l_req_dtl_file_name        VARCHAR2 (150) := 'wc_requisitionlineitem.csv';
      l_catalog_directory_path   VARCHAR2 (50)  := 'XXWC_WCS_CATALOG_DIR';
      
      l_ctlgfltr_file_name       VARCHAR2 (150) := 'wc_catalogfilter.csv';
      l_cf_catentry_file_name    VARCHAR2 (150) := 'wc_catalogfiltercatentry.csv';
      
      l_file_name_temp           VARCHAR2 (150);
      l_file_rec_line            VARCHAR2(2000);
      l_file_dir                 VARCHAR2 (100);
      l_org_id                   NUMBER;
      l_org_name                 VARCHAR2 (240);
      l_rec_cnt                  NUMBER := 0;
      l_dir_exists               NUMBER;

      l_program_error            EXCEPTION;
   BEGIN
      fnd_file.put_line (fnd_file.LOG,'**********************************************************************************');
      fnd_file.put_line (fnd_file.LOG,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Start of Procedure : Requistion Create_file');
      fnd_file.put_line (fnd_file.LOG,'----------------------------------------------------------------------------------');

      fnd_file.put_line (fnd_file.LOG,'Input Parameters     :');
      fnd_file.put_line (fnd_file.LOG,'p_directory_path     :' || p_directory_path);
      fnd_file.put_line (fnd_file.LOG,'----------------------------------------------------------------------------------');

      ----------------------------------------------------------------------------------
      -- Check if directory path exists
      ----------------------------------------------------------------------------------
      l_dir_exists := 0;
      BEGIN
         SELECT COUNT (1)
           INTO l_dir_exists
           FROM all_directories
          WHERE directory_name = p_directory_path;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg := 'Error validating directory   :' || SQLERRM;
            RAISE l_program_error;
      END;

      -- Raise ProgramError if directory path does not exist
      IF l_dir_exists = 0
      THEN
         l_err_msg := 'Directory does not exist in Oracle :' || p_directory_path;
         RAISE l_program_error;
      END IF;

      ----------------------------------------------------------------------------------
      -- Load Global Temporary Table with the modified Order details.
      ----------------------------------------------------------------------------------
      load_global_temp_tbl(p_date_from,p_date_to);

fnd_file.put_line (fnd_file.LOG, '################## Creating Requisition Header File ##################');

      ----------------------------------------------------------------------------------
      -- Creating Requisition Header File
      ----------------------------------------------------------------------------------
         l_file_name_temp := 'TEMP_' || l_req_hdr_file_name;

         -- Delete if file already exists
         BEGIN
            UTL_FILE.FREMOVE (p_directory_path, l_req_hdr_file_name);
            UTL_FILE.FREMOVE (p_directory_path, l_file_name_temp);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         -- Start creating the file
         l_file_handle :=
            UTL_FILE.fopen (p_directory_path
                           ,l_file_name_temp
                           ,'w'
                           ,32767);

         -- Write Quote Header information into Headers file
         UTL_FILE.put_line (l_file_handle, 'Orders||||');
         UTL_FILE.put_line (l_file_handle, 'orgentity|description|lastupdate|quoteID|Delete');

         -- Write data into the File
         FOR rec_req_hdr IN cur_req_hdr LOOP
            IF rec_req_hdr.quote_type_id = g_job_req_type_id THEN -- WC SB JOB QUOTE

               l_file_rec_line := rec_req_hdr.orgentity        ||'|'||
                                  rec_req_hdr.description      ||'|'||
                                  rec_req_hdr.lastupdate       ||'|'||
                                  rec_req_hdr.quoteID          ||'|'||
                                  rec_req_hdr.delete_flag;
               UTL_FILE.put_line (l_file_handle, l_file_rec_line);
               l_file_rec_line := NULL;

            ELSIF rec_req_hdr.quote_type_id = g_mstr_req_type_id THEN -- WC SB MASTER QUOTE
               FOR rec_party_sites IN cur_party_sites(rec_req_hdr.party_id, rec_req_hdr.account_number) LOOP

                  l_file_rec_line := 'ou='||LOWER(rec_party_sites.party_site_number)||',o='||rec_req_hdr.account_number||',o=whitecap buyer organization,o=buyer organization,o=root organization' -- Version# 1.1
                            ||'|'||  rec_req_hdr.description      
                            ||'|'||  rec_req_hdr.lastupdate       
                            ||'|'||  rec_req_hdr.quoteID
                            ||'|'||  rec_req_hdr.delete_flag;

                  UTL_FILE.put_line (l_file_handle, l_file_rec_line);
                  l_file_rec_line := NULL;
               END LOOP; -- cur_party_sites
            END IF;

            UPDATE xxwc.xxwc_sb_gtt_tbl
               SET orgentity         = rec_req_hdr.orgentity
                 , account_number    = rec_req_hdr.account_number
                 , party_id          = rec_req_hdr.party_id
                 , party_site_number = rec_req_hdr.party_site_number
             WHERE header_id = rec_req_hdr.quoteID;
         END LOOP; -- cur_req_hdr
--         COMMIT;

         -- Close the file
         UTL_FILE.fclose (l_file_handle);

         -- 'Rename file for pickup';
         UTL_FILE.frename (p_directory_path
                          ,l_file_name_temp
                          ,p_directory_path
                          ,l_req_hdr_file_name);

fnd_file.put_line (fnd_file.LOG, '################## Creating Requisition Details File ##################');

      ----------------------------------------------------------------------------------
      -- Creating Requisition Details File
      ----------------------------------------------------------------------------------
         l_file_handle := NULL;
         l_file_name_temp := 'TEMP_' || l_req_dtl_file_name;

         -- Delete if file already exists
         BEGIN
            UTL_FILE.FREMOVE (p_directory_path, l_req_dtl_file_name);
            UTL_FILE.FREMOVE (p_directory_path, l_file_name_temp);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         -- Start creating the file
         l_file_handle := UTL_FILE.fopen (p_directory_path
                                         ,l_file_name_temp
                                         ,'w'
                                         ,32767);

         -- Write Quote Line Information into Details file
         UTL_FILE.put_line (l_file_handle, 'OrderItems|||||');
         UTL_FILE.put_line (l_file_handle, 'orgentity|partNum|ffmcentername|quantity|description|sourceid|Delete');

         -- Write data into the File
         FOR rec_req_gtt IN cur_req_gtt LOOP
           IF rec_req_gtt.quote_type_id = g_job_req_type_id THEN -- WC SB JOB QUOTE
              FOR rec_req_dtl IN cur_req_dtl(rec_req_gtt.header_id) LOOP
                 l_file_rec_line := rec_req_gtt.orgentity           ||'|'||
                                    rec_req_dtl.partNum             ||'|'||
                                    rec_req_gtt.ffmcentername       ||'|'||
                                    rec_req_dtl.quantity            ||'|'||
                                    rec_req_gtt.description         ||'|'||
                                    rec_req_dtl.sourceid            ||'|'||
                                    rec_req_dtl.Delete_flag;
                 UTL_FILE.put_line (l_file_handle, l_file_rec_line);
                 l_file_rec_line := NULL;
              END LOOP; -- cur_req_dtl
           ELSIF rec_req_gtt.quote_type_id = g_mstr_req_type_id THEN -- WC SB MASTER QUOTE
              FOR rec_party_sites IN cur_party_sites(rec_req_gtt.party_id, rec_req_gtt.account_number) LOOP

                 FOR rec_req_dtl IN cur_req_dtl(rec_req_gtt.header_id) LOOP
                    l_file_rec_line := 'ou='||LOWER(rec_party_sites.party_site_number)||',o='||rec_req_gtt.account_number||',o=whitecap buyer organization,o=buyer organization,o=root organization'
                               ||'|'|| rec_req_dtl.partNum     
                               ||'|'|| rec_req_gtt.ffmcentername  
                               ||'|'|| rec_req_dtl.quantity      
                               ||'|'|| rec_req_gtt.description   
                               ||'|'|| rec_req_dtl.sourceid
                               ||'|'|| rec_req_dtl.Delete_flag;
                    UTL_FILE.put_line (l_file_handle, l_file_rec_line);
                    l_file_rec_line := NULL;
                 END LOOP; -- cur_req_dtl
              END LOOP; -- cur_party_sites
           END IF;
         END LOOP; -- cur_req_gtt

         -- Close the file
         UTL_FILE.fclose (l_file_handle);

         -- 'Rename file for pickup';
         UTL_FILE.frename (p_directory_path
                          ,l_file_name_temp
                          ,p_directory_path
                          ,l_req_dtl_file_name);

fnd_file.put_line (fnd_file.LOG, '################## Creating Catalog Files ##################');

      ----------------------------------------------------------------------------------
      -- Creating Catalog Files
      ----------------------------------------------------------------------------------
      CREATE_CATALOG_FILE;

      fnd_file.put_line (fnd_file.output,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.output,'**********************************************************************************');

      fnd_file.put_line (fnd_file.LOG,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : create_file');
      fnd_file.put_line (fnd_file.LOG,'----------------------------------------------------------------------------------');

   EXCEPTION                                   -- this section traps my errors
      WHEN l_program_error
      THEN
         l_err_code := 2;
         --      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||substr(SQLERRM, 1, 2000);

         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         UTL_FILE.fclose (l_file_handle);
--         UTL_FILE.fremove (l_file_dir, l_file_name);

         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => l_procedure_name
           ,p_calling             => l_sec
           ,p_request_id          => fnd_global.conc_request_id
           ,p_ora_error_msg       => l_err_msg
           ,p_error_desc          => 'Error running SpeedBuild Outbound process with Program Error Exception'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
            
            l_sec := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;

         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_sec);

         UTL_FILE.fclose (l_file_handle);
--         UTL_FILE.fremove (l_file_dir, l_file_name);

         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => l_procedure_name
           ,p_calling             => l_sec
           ,p_request_id          => fnd_global.conc_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error running SpeedBuild Outbound process with When Others Exception'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
   END CREATE_FILE;

   /********************************************************************************
   ProcedureName : pc_duplicate_line
   Purpose       : Order Line Processing Constract to prevent Duplicate Line

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
   ********************************************************************************/
PROCEDURE pc_duplicate_line(p_application_id                IN NUMBER,
                            p_entity_short_name             IN VARCHAR2,
                            p_validation_entity_short_name  IN VARCHAR2,
                            p_validation_tmplt_short_name   IN VARCHAR2,
                            p_record_set_short_name         IN VARCHAR2,
                            p_scope                         IN VARCHAR2,
                            x_result                       OUT NOCOPY NUMBER )
    IS
    l_line_count    NUMBER := 0;
    l_error_message  VARCHAR2(200);
  BEGIN
   
   ----------------------------------------------------------
   -- Check if there is already an OrderLine with that item
   ----------------------------------------------------------
   SELECT COUNT(1)
     INTO l_line_count
     FROM oe_order_lines_all      ool
    WHERE 1 =1 
      AND ool.line_type_id      = 1161   -- WC SB QUOTE LINE
      AND ool.flow_status_code IN ('PENDING_CUSTOMER_ACCEPTANCE', 'CUSTOMER_ACCEPTED') -- Version# 1.5
      AND ool.header_id         = oe_line_security.g_record.header_id
      AND ool.inventory_item_id = oe_line_security.g_record.inventory_item_id;

   IF l_line_count >= 1 THEN

/*       
       FND_MESSAGE.SET_NAME('XXWC','XXWC_ADI_ERROR_MSG'); 
       FND_MESSAGE.SET_TOKEN('ERROR_MESSAGE','Unidentified error - TEST TEST TEST '); 
       l_error_message := FND_MESSAGE.GET;

       OE_MSG_PUB.ADD_TEXT(l_error_message); 

       l_error_message := 'Unidentified error - TEST TEST TEST ';
       FND_MESSAGE.SET_NAME('XXWC','XXWC_ADI_ERROR_MSG'); 
       FND_MESSAGE.SET_TOKEN('ERROR_MESSAGE','Unidentified error - TEST TEST TEST '); 

       FND_MESSAGE.RAISE_ERROR;
*/
     x_result  := 1;
   ELSE
     x_result  := 0;
   END IF;
   

  EXCEPTION
  WHEN OTHERS THEN
         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => 'XXWC_SPEEDBUILD_OB_PKG.PC_DUPLICATE_LINE'
           ,p_calling             => 'PC_DUPLICATE_LINE'
           ,p_request_id          => -1
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error in SpeedBuild DuplicateLine Processing Constraint, When Others Exception'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');

     x_result := 0;
  END pc_duplicate_line;


   /********************************************************************************
   FunctionName : CSP_EXISTS
   Purpose      : Check if CSP exists for an Item

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/20/2013    Gopi Damuluri    Initial creation of the procedure
   ********************************************************************************/
  FUNCTION csp_exists(p_item_id            IN NUMBER)
           RETURN NUMBER
  IS
      l_csp_cnt     NUMBER;
  BEGIN

   ----------------------------------------------------------
   -- Check if CSP exists for an Item
   ----------------------------------------------------------
    SELECT COUNT(1)
      INTO l_csp_cnt
      FROM apps.mtl_item_categories mic,
           apps.mtl_categories_b    mc,
           apps.mtl_category_sets   mcs,
           apps.mtl_system_items_b  msi,
           apps.mtl_parameters      mp,
           apps.qp_list_headers     qph,
           apps.qp_list_lines_v     qpl
     WHERE 1=1
       AND msi.inventory_item_id    = mic.inventory_item_id
       AND msi.organization_id      = mic.organization_id
       AND mic.category_id          = mc.category_id
       AND mic.category_set_id      = mcs.category_set_id
       AND mcs.structure_id         = mc.structure_id
       AND mcs.category_set_name    = 'Website Item'
       AND mc.segment1             IN ('Y','C')
       AND msi.organization_id      = mp.organization_id
       AND mp.organization_code     = 'MST'
       AND qph.list_header_id       = qpl.list_header_id
       AND qph.name                 = 'MARKET NATIONAL'
       AND msi.inventory_item_id    =  TO_NUMBER(qpl.product_attr_value)
       AND nvl(qpl.attribute1,'N')  = 'Y' 
       AND qph.context              = '162'
       AND msi.inventory_item_id    = p_item_id;
     RETURN l_csp_cnt;

  EXCEPTION
  WHEN OTHERS THEN
         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => 'XXWC_SPEEDBUILD_OB_PKG.CSP_EXISTS'
           ,p_calling             => 'CSP_EXISTS'
           ,p_request_id          => -1
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error in SpeedBuild CSP_EXISTS function, When Others Exception'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');

    RETURN 0;
  END csp_exists;

-- Verson# 1.1 > Start
   /********************************************************************************
   ProcedureName : order_hdr_line_org
   Purpose       : Processing Constraints - Order Header Org Different From Order Line Org

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     01/27/2014    Gopi Damuluri    Initial creation of the procedure
   ********************************************************************************/
PROCEDURE order_hdr_line_org(p_application_id                IN NUMBER,
                            p_entity_short_name             IN VARCHAR2,
                            p_validation_entity_short_name  IN VARCHAR2,
                            p_validation_tmplt_short_name   IN VARCHAR2,
                            p_record_set_short_name         IN VARCHAR2,
                            p_scope                         IN VARCHAR2,
                            x_result                       OUT NOCOPY NUMBER )
    IS
    l_hdr_ship_from_org    NUMBER := 0;
    l_order_type_id        NUMBER := 0;
    l_error_message        VARCHAR2(200);
  BEGIN  

   ----------------------------------------------------------
   -- Check if there is already an OrderLine with that item
   ----------------------------------------------------------
   SELECT ship_from_org_id
        , order_type_id
     INTO l_hdr_ship_from_org
        , l_order_type_id
     FROM oe_order_headers_all
    WHERE header_id = oe_line_security.g_record.header_id;

   IF l_order_type_id IN (1162, 1163) THEN
      IF l_hdr_ship_from_org != oe_line_security.g_record.ship_from_org_id THEN
        x_result  := 1;
      ELSE
        x_result  := 0;
      END IF;
   ELSE
     x_result  := 0;
   END IF;

  EXCEPTION
  WHEN OTHERS THEN
         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => 'XXWC_SPEEDBUILD_OB_PKG.ORDER_HDR_LINE_ORG'
           ,p_calling             => 'ORDER_HDR_LINE_ORG'
           ,p_request_id          => -1
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error in SpeedBuild ORDER_HDR_LINE_ORG Processing Constraint, When Others Exception'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');

     x_result := 0;
  END ORDER_HDR_LINE_ORG;
-- Verson# 1.1 < End

-- Verson# 1.2 > Start
   /********************************************************************************
   ProcedureName : CREATE_CATALOG_FILE
   Purpose       : Common API for Outbound Interfaces.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     10/20/2013    Gopi Damuluri    Initial creation of the procedure
   1.6     05/18/2014    Gopi Damuluri    TMS# TMS# 20140518-00004
                                          Resolve the issue with Master Party - Master Req
   1.7     05/27/2014    Gopi Damuluri    TMS# 20140529-00334
                                          Catalog Filter at Master Party Level
   1.8     07/01/2014    Gopi Damuluri    TMS# 20140714-00020
                                          Send the Sites to EComm even when Customer# = Site#
   ********************************************************************************/
   PROCEDURE CREATE_CATALOG_FILE
   IS

      -- ###################
      -- Requisition Lines
      -- ###################
      CURSOR cur_req_dtl
          IS
      SELECT DISTINCT '1'                               party_site_number
           , msib.segment1                              partnum
           , gtt.quote_type_id                          quote_type_id
           , gtt.party_id                               party_id
           , gtt.party_number                           party_number    -- Version# 1.7
           , gtt.account_number                         account_number           
        FROM oe_order_lines_all                         oola
           , mtl_system_items_b                         msib
           , xxwc.xxwc_sb_gtt_tbl                       gtt
       WHERE 1 =1          
         AND oola.inventory_item_id                   = msib.inventory_item_id
         AND msib.organization_id                     = 222
         AND oola.flow_status_code                   IN ('PENDING_CUSTOMER_ACCEPTANCE', 'CUSTOMER_ACCEPTED') -- Version# 1.5
         AND oola.header_id                           = gtt.header_id
         AND gtt.quote_type_id                        = g_mstr_req_type_id
      UNION
      SELECT DISTINCT gtt.party_site_number             party_site_number
           , msib.segment1                              partnum
           , gtt.quote_type_id                          quote_type_id
           , gtt.party_id                               party_id
           , gtt.party_number                           party_number   -- Version# 1.7
           , gtt.account_number                         account_number
        FROM oe_order_lines_all                         oola
           , mtl_system_items_b                         msib
           , xxwc.xxwc_sb_gtt_tbl                       gtt
       WHERE 1 =1          
         AND oola.inventory_item_id                   = msib.inventory_item_id
         AND oola.flow_status_code                   IN ('PENDING_CUSTOMER_ACCEPTANCE', 'CUSTOMER_ACCEPTED')  -- Version# 1.5
         AND msib.organization_id                     = 222
         AND oola.header_id                           = gtt.header_id
         AND gtt.quote_type_id                        = g_job_req_type_id
         AND NOT EXISTS (SELECT '1' 
                           FROM xxwc.xxwc_sb_gtt_tbl         gtt_mst
                              , oe_order_lines_all           oola_mst
                          WHERE 1 = 1
                            AND gtt_mst.quote_type_id      = g_mstr_req_type_id
                            AND gtt_mst.account_number     = gtt.account_number
                            AND gtt_mst.party_id           = gtt.party_id
                            AND oola_mst.header_id         = gtt_mst.header_id
                            AND oola_mst.inventory_item_id = oola.inventory_item_id)
         ;

      -- ################################
      -- Global Temp Table - Req Hdr
      -- ################################
      CURSOR cur_req_gtt
          IS
      SELECT DISTINCT gtt.quote_type_id         quote_type_id
           , gtt.party_id                       party_id
           , gtt.party_number                   party_number   -- Version# 1.7
           , gtt.account_number                 account_number
           , '1'                                party_site_number
        FROM xxwc.xxwc_sb_gtt_tbl      gtt
           , hz_parties                hp
       WHERE 1 = 1
         AND gtt.party_id                 = hp.party_id
         AND gtt.delete_flag              = 0
--         AND TO_CHAR(gtt.account_number) != gtt.party_site_number -- Version# 1.8
         AND gtt.quote_type_id            = g_mstr_req_type_id
      UNION
      SELECT DISTINCT gtt.quote_type_id         quote_type_id
           , gtt.party_id                       party_id
           , gtt.party_number                   party_number   -- Version# 1.7
           , gtt.account_number                 account_number
           , gtt.party_site_number              party_site_number
        FROM xxwc.xxwc_sb_gtt_tbl      gtt
       WHERE 1 = 1
         AND gtt.delete_flag              = 0
--         AND TO_CHAR(gtt.account_number) != gtt.party_site_number -- Version# 1.8
         AND gtt.quote_type_id            = g_job_req_type_id
         AND NOT EXISTS (SELECT '1' 
                           FROM xxwc.xxwc_sb_gtt_tbl     gtt_mst
                          WHERE 1 = 1
                            AND gtt_mst.quote_type_id  = g_mstr_req_type_id
                            AND gtt_mst.account_number = gtt.account_number
                            AND gtt_mst.party_id       = gtt.party_id)
         ;

      -- #######################
      -- Master Req PartySites
      -- #######################
      CURSOR cur_party_sites (p_party_id IN NUMBER, p_customer_num  IN VARCHAR2)
          IS
      SELECT hps.party_site_number
        FROM hz_party_sites                         hps
       WHERE 1 =1
         AND hps.party_id                         = p_party_id
--         AND hps.party_site_number !              = p_customer_num -- Version# 1.8
--         AND EXISTS (SELECT '1'
--                       FROM hz_cust_accounts_all    hca
--                          , hz_cust_acct_sites_all  hcas
--                          , hz_cust_site_uses_all   hcsu
--                      WHERE 1 = 1
--                        AND hca.account_number      = p_customer_num
--                        AND hcas.cust_account_id    = hca.cust_account_id
--                        AND hcas.cust_acct_site_id  = hcsu.cust_acct_site_id
--                        AND hcsu.site_use_code      = 'BILL_TO'
--                        AND hcas.party_site_id      = hps.party_site_id)
         AND EXISTS (SELECT '1'
                       FROM hz_cust_acct_sites_all  hcas
                          , hz_cust_site_uses_all   hcsu
                          , hz_cust_accounts_all    hca                             -- Version# 1.6
                      WHERE 1 = 1
                        AND hcsu.site_use_code      = 'SHIP_TO'
                        AND hcas.cust_acct_site_id  = hcsu.cust_acct_site_id
                        AND hca.cust_account_id     = hcas.cust_account_id          -- Version# 1.6
                        AND hca.account_number      = p_customer_num                -- Version# 1.6
                        AND hcas.party_site_id      = hps.party_site_id);

      -- Intialize Variables
      l_err_msg                  VARCHAR2 (2000);
      l_err_code                 NUMBER;
      l_sec                      VARCHAR2 (150);
      l_procedure_name           VARCHAR2 (75) := 'XXWC_SPEEDBUILD_OB_PKG.CREATE_CATALOG_FILE';

      --File Variables
      l_file_handle              UTL_FILE.file_type;
      l_catalog_directory_path   VARCHAR2 (50)  := 'XXWC_WCS_CATALOG_DIR';
      
      l_ctlgfltr_file_name       VARCHAR2 (150) := 'wc_catalogfilter.csv';
      l_cf_catentry_file_name    VARCHAR2 (150) := 'wc_catalogfiltercatentry.csv';
      
      l_file_name_temp           VARCHAR2 (150);
      l_file_rec_line            VARCHAR2(2000);
      l_file_dir                 VARCHAR2 (100);
      l_org_id                   NUMBER;
      l_org_name                 VARCHAR2 (240);
      l_rec_cnt                  NUMBER := 0;
      l_dir_exists               NUMBER;

      l_program_error            EXCEPTION;
   BEGIN
      fnd_file.put_line (fnd_file.LOG,'**********************************************************************************');
      fnd_file.put_line (fnd_file.LOG,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Start of Procedure : Requistion CREATE_CATALOG_FILE');
      fnd_file.put_line (fnd_file.LOG,'----------------------------------------------------------------------------------');

fnd_file.put_line (fnd_file.LOG, '################## Creating CatalogFilter File ##################');

-- Version# 1.7 > Start
      ----------------------------------------------------------------------------------
      -- Update Global Temporary Table with PartyNumber
      ----------------------------------------------------------------------------------
         BEGIN
            UPDATE xxwc.xxwc_sb_gtt_tbl gtt
               SET gtt.party_number = get_parent_party_num(gtt.party_id)
             WHERE 1 = 1;
         EXCEPTION
         WHEN OTHERS THEN
            NULL;
         END;
-- Version# 1.7 < End

      ----------------------------------------------------------------------------------
      -- Creating CatalogFilter File
      ----------------------------------------------------------------------------------
         l_file_name_temp := 'TEMP_' || l_ctlgfltr_file_name;

         -- Delete if file already exists
         BEGIN
            UTL_FILE.FREMOVE (l_catalog_directory_path, l_ctlgfltr_file_name);
            UTL_FILE.FREMOVE (l_catalog_directory_path, l_file_name_temp);
         EXCEPTION
         WHEN OTHERS THEN
            NULL;
         END;

         -- Start creating the file
         l_file_handle :=
            UTL_FILE.fopen (l_catalog_directory_path
                           ,l_file_name_temp
                           ,'w'
                           ,32767);

         -- Write CatalogFilter information into CatalogFilter file
         UTL_FILE.put_line (l_file_handle, 'CatalogFilter||||||');
         UTL_FILE.put_line (l_file_handle, 'CatalogFilterId|CatalogFilterName|StoreIdentifier|CatalogIdentifier|Usage|Delete|EnglishDescription');

         -- Write data into the File
         FOR rec_req_hdr IN cur_req_gtt LOOP

            IF rec_req_hdr.quote_type_id = g_job_req_type_id THEN -- WC SB JOB QUOTE

               l_file_rec_line := ''        ||'|'||
                                  'WC_'||rec_req_hdr.party_number      ||'|'||  -- Version# 1.7
                                  'WhiteCap'                         ||'|'||
                                  'HDSEnterprise CAS'                ||'|'||
                                  'Buyer'                            ||'|'||
                                  '0'                                ||'|'||
                                  'SpeedBuild Filter for '||rec_req_hdr.party_site_number;
               UTL_FILE.put_line (l_file_handle, l_file_rec_line);
               l_file_rec_line := NULL;

            ELSIF rec_req_hdr.quote_type_id = g_mstr_req_type_id THEN -- WC SB MASTER QUOTE
               FOR rec_party_sites IN cur_party_sites(rec_req_hdr.party_id, rec_req_hdr.account_number) LOOP

               l_file_rec_line := ''                                 ||'|'||
                                  'WC_'||rec_req_hdr.party_number  ||'|'||   -- Version# 1.7
                                  'WhiteCap'                         ||'|'||
                                  'HDSEnterprise CAS'                ||'|'||
                                  'Buyer'                            ||'|'||
                                  '0'                                ||'|'||  -- Version# 1.1
                                  'SpeedBuild Filter for '||rec_party_sites.party_site_number;

                  UTL_FILE.put_line (l_file_handle, l_file_rec_line);
                  l_file_rec_line := NULL;
               END LOOP; -- cur_party_sites
            END IF;      
         END LOOP; -- cur_req_gtt

         -- Close the file
         UTL_FILE.fclose (l_file_handle);

         -- 'Rename file for pickup';
         UTL_FILE.frename (l_catalog_directory_path
                          ,l_file_name_temp
                          ,l_catalog_directory_path
                          ,l_ctlgfltr_file_name);

fnd_file.put_line (fnd_file.LOG, '################## Creating CatalogFilterCatEntry Details File ##################');

      ----------------------------------------------------------------------------------
      -- Creating CatalogFilterCatEntry File
      ----------------------------------------------------------------------------------
         l_file_handle := NULL;
         l_file_name_temp := 'TEMP_' || l_cf_catentry_file_name;

         -- Delete if file already exists
         BEGIN
            UTL_FILE.FREMOVE (l_catalog_directory_path, l_cf_catentry_file_name);
            UTL_FILE.FREMOVE (l_catalog_directory_path, l_file_name_temp);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         -- Start creating the file
         l_file_handle := UTL_FILE.fopen (l_catalog_directory_path
                                         ,l_file_name_temp
                                         ,'w'
                                         ,32767);

         -- Write CatalogFilterCatEntry Information into CatalogFilterCatEntry file
         UTL_FILE.put_line (l_file_handle, 'CatalogFilterCatentry||||||');
         UTL_FILE.put_line (l_file_handle, 'CatalogFilterId|CatalogFilterName|StoreIdentifier|CatalogIdentifier|SelectionType|PartNumber|Delete');

         -- Write data into the File

         FOR rec_req_dtl IN cur_req_dtl LOOP
           IF rec_req_dtl.quote_type_id = g_job_req_type_id THEN -- WC SB JOB QUOTE

                 l_file_rec_line := ''                                 ||'|'||
                                    'WC_'||rec_req_dtl.party_number      ||'|'||  -- Version# 1.7
                                    'WhiteCap'                         ||'|'||
                                    'HDSEnterprise CAS'                ||'|'||
                                    'Include'                          ||'|'||
                                    rec_req_dtl.partNum                ||'|'||
                                    '0';

                 UTL_FILE.put_line (l_file_handle, l_file_rec_line);
                 l_file_rec_line := NULL;
 
                    l_file_rec_line := ''                                 ||'|'||
                                       'WC_'||rec_req_dtl.party_number    ||'|'||  -- Version# 1.7
                                       'WhiteCap'                         ||'|'||
                                       'HDSEnterprise CAS'                ||'|'||
                                       'Include'                          ||'|'||
                                       'P_'||rec_req_dtl.partNum          ||'|'||
                                       '0';
 
                       UTL_FILE.put_line (l_file_handle, l_file_rec_line);
                      l_file_rec_line := NULL;
 
           ELSIF rec_req_dtl.quote_type_id = g_mstr_req_type_id THEN -- WC SB MASTER QUOTE
--              FOR rec_party_sites IN cur_party_sites(rec_req_dtl.party_id, rec_req_dtl.account_number) LOOP

                   l_file_rec_line := ''                                 ||'|'||
                                      'WC_'||rec_req_dtl.party_number      ||'|'||  -- Version# 1.7
                                      'WhiteCap'                         ||'|'||
                                      'HDSEnterprise CAS'                ||'|'||
                                      'Include'                          ||'|'||
                                      rec_req_dtl.partNum                ||'|'||
                                      '0';

                      UTL_FILE.put_line (l_file_handle, l_file_rec_line);
                      l_file_rec_line := NULL;

                   l_file_rec_line := ''                                 ||'|'||
                                      'WC_'||rec_req_dtl.party_number    ||'|'||  -- Version# 1.7
                                      'WhiteCap'                         ||'|'||
                                      'HDSEnterprise CAS'                ||'|'||
                                      'Include'                          ||'|'||
                                      'P_'||rec_req_dtl.partNum          ||'|'||
                                      '0';

                      UTL_FILE.put_line (l_file_handle, l_file_rec_line);
                      l_file_rec_line := NULL;

--              END LOOP; -- cur_party_sites
           END IF;
         END LOOP; -- cur_req_dtl

         -- Close the file
         UTL_FILE.fclose (l_file_handle);

         -- 'Rename file for pickup';
         UTL_FILE.frename (l_catalog_directory_path
                          ,l_file_name_temp
                          ,l_catalog_directory_path
                          ,l_cf_catentry_file_name);


      fnd_file.put_line (fnd_file.output,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.output,'**********************************************************************************');

      fnd_file.put_line (fnd_file.LOG,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : CREATE_CATALOG_FILE');
      fnd_file.put_line (fnd_file.LOG,'----------------------------------------------------------------------------------');

   EXCEPTION                                   -- this section traps my errors
      WHEN l_program_error
      THEN
         l_err_code := 2;
         --      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||substr(SQLERRM, 1, 2000);

         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         UTL_FILE.fclose (l_file_handle);
--         UTL_FILE.fremove (l_file_dir, l_file_name);

         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => l_procedure_name
           ,p_calling             => l_sec
           ,p_request_id          => fnd_global.conc_request_id
           ,p_ora_error_msg       => l_err_msg
           ,p_error_desc          => 'Error running SpeedBuild Outbound process with Program Error Exception'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
            
            l_sec := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;

         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_sec);

         UTL_FILE.fclose (l_file_handle);
--         UTL_FILE.fremove (l_file_dir, l_file_name);

         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => l_procedure_name
           ,p_calling             => l_sec
           ,p_request_id          => fnd_global.conc_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error running SpeedBuild Outbound process with When Others Exception'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
   END CREATE_CATALOG_FILE;
-- Verson# 1.2 < End

-- Verson# 1.7 > Start
   /********************************************************************************
   FunctionName  : GET_PARENT_PARTY_NUM
   Purpose       : Derive the Parent Party Number

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.7     05/27/2014    Gopi Damuluri    TMS# 
                                          Catalog Filter at Master Party Level
   ********************************************************************************/
   FUNCTION get_parent_party_num (p_party_id IN NUMBER) return VARCHAR2 
         IS
      l_party_number       VARCHAR2(30);
   BEGIN
      -----------------------------------------------------------------------------------------------
      -- Check if Parent Party exists in Party Merge Xref Table - XXWC_SB_PARTY_MERGE_XREF_TBL
      -----------------------------------------------------------------------------------------------
      BEGIN
         SELECT stg.parent_party_number           parent_party_number
           INTO l_party_number
           FROM xxwc.xxwc_sb_party_merge_xref_tbl stg
          WHERE 1 = 1
            AND stg.child_party_id              = p_party_id
            AND rownum                          = 1;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         -----------------------------------------------------------------------------------------------
         -- If Parent Party does not exist in Party Merge Xref Table - XXWC_SB_PARTY_MERGE_XREF_TBL
         -- derive the actual/child Party Number
         -----------------------------------------------------------------------------------------------
         SELECT hp.party_number                   parent_party_number
           INTO l_party_number
           FROM hz_parties                        hp
          WHERE 1 = 1
            AND hp.party_id                     = p_party_id;
      WHEN OTHERS THEN
         l_party_number := NULL;
      END;
      
      RETURN l_party_number;
   EXCEPTION
   WHEN OTHERS THEN
      return NULL;
   END get_parent_party_num;
-- Verson# 1.7 < End

END XXWC_SPEEDBUILD_OB_PKG;
/
