CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ar_getpaid_files_ob_pkg
IS
 /********************************************************************************
   FILE NAME: XXWC_AR_GETPAID_FILES_OB_PKG.pkg

   PROGRAM TYPE: PL/SQL Package Body

   PURPOSE: Getpaid outbound files.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)            DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0                                       Initial creation
   1.1     09/29/2014   Maharajan Shunmugam  TMS# 20141001-00157 Canada Multi-Org changes
   1.2     04/27/2016   Neha Saini           TMS#20160407-00183  removing parallelism.
   1.3     05/26/2017   Pattabhi Avula       TMS#20170526-00002 Production address1 variable length issue
   ********************************************************************************/


    l_errbuf          CLOB;
    l_error_message   CLOB;
    v_tuning_loop     NUMBER;
    --*************************************************************
    --*************************************************************
    --***************drop_temp_table (p_table_name VARCHAR2)
    --                 Helper function
    --*************************************************************
    --*************************************************************
    pl_dflt_email     fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';

    --******************************************************
    g_start           NUMBER;

    --*************************************************************
    --*************************************************************
    PROCEDURE create_file (p_view_name        IN VARCHAR2
                          ,p_directory_path   IN VARCHAR2
                          ,p_file_name        IN VARCHAR2
                          ,p_org_id           IN NUMBER)
    IS
        v_rtn_cd           INTEGER;

        v_file_string      VARCHAR2 (1024);
        l_file_name_temp   VARCHAR2 (124);

        TYPE ref_cur IS REF CURSOR;

        view_cur           ref_cur;
        view_rec           xxwc_ob_common_pkg.xxcus_ob_file_rec;
        l_file_handle      UTL_FILE.file_type;
    BEGIN
        l_file_name_temp := 'TEMP_' || p_file_name;

        BEGIN
            UTL_FILE.fremove (p_directory_path, p_file_name);
            UTL_FILE.fremove (p_directory_path, l_file_name_temp);
            UTL_FILE.fclose (l_file_handle);
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        l_file_handle :=
            UTL_FILE.fopen (p_directory_path
                           ,l_file_name_temp
                           ,'w'
                           ,32767);

        OPEN view_cur FOR 'SELECT   rec_line ,org_id  FROM ' || p_view_name || ' WHERE org_id = ' || p_org_id;

        DBMS_OUTPUT.put_line ('SELECT  rec_line ,org_id  FROM ' || p_view_name || ' WHERE org_id = ' || p_org_id);

        -- Write data into the File
        LOOP
            FETCH view_cur INTO view_rec;

            EXIT WHEN view_cur%NOTFOUND;

            -- Write file info
            UTL_FILE.put_line (l_file_handle, view_rec.rec_line);
        END LOOP;

        -- Close the file
        UTL_FILE.fclose (l_file_handle);

        -- 'Rename file for pickup';
        UTL_FILE.frename (p_directory_path
                         ,l_file_name_temp
                         ,p_directory_path
                         ,p_file_name);
    END;

    --*************************************************************
    --*************************************************************
    /*BEGIN
        xxwc_ar_getpaid_files_ob_pkg.create_AREXTI##_file ('XXWC_TEST_RUN'
                                                ,'my_first_pr.txt'
                                                ,'log_file.txt'
                                                ,162);
    END;*/
    --*************************************************************

    --*************************************************************

    /* This is called directly by UC4. Creates AREXTI file and the log */
    PROCEDURE create_arexti_file (p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT NUMBER
                                 ,p_directory_name          VARCHAR2
                                 ,p_file_name               VARCHAR2
                                 ,p_log_file_name           VARCHAR2
                                 ,p_org_name         IN     VARCHAR2)
    IS
        l_file_name        VARCHAR2 (124);
        l_log_file_name    VARCHAR2 (124);

        v_step             NUMBER := 1;
        v_query_str        VARCHAR2 (214);
        v_num_of_records   NUMBER;
        p_org_id           NUMBER;
    BEGIN
        p_retcode := 0;

        SELECT organization_id
          INTO p_org_id
          FROM hr_operating_units
         WHERE name = p_org_name;


       mo_global.set_policy_context ('S', p_org_id);
        -- Enable parallelism in our RAC environments
        --EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

        -- This removes the temp tables that are left after both processes run.
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_cust_trxg'); -- The helper function eats the exception.
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_cash_rcpts_acragl');

        -- Clean up any mess, any tables found with this convention
        BEGIN
            FOR r
                IN (  SELECT *
                        FROM all_objects
                       WHERE     object_type = 'TABLE'
                             AND created >= SYSDATE - 7
                             AND object_name LIKE 'AREXTI##TMP%'
                             AND owner = 'XXWC'
                             AND object_name NOT IN
                                     ('AREXTI##TMP_CUST_TRXGX'
                                     ,                                                                          --ARMAST
                                      'AREXTI##TMP_CASH_RCPTS_ACRAGX'
                                     ,                                                                          --ARMAST
                                      'AREXTI##TMP_ARMAST_FILEX'
                                     ,                                                                     --ARMAST FILE
                                      'AREXTI##TMP_CUST_TRXG'
                                     ,                                                                          --AREXTI
                                      'AREXTI##TMP_CASH_RCPTS_ACRAGL'
                                     ,                                                                          --AREXTI
                                      'AREXTI##TMP_PAYDOC_FILE'
                                     ,                                                                     --AREXTI FILE
                                      'ARTRAN##TMP_CUST_TRXGY'
                                     ,                                                                          --ARTRAN
                                      'ARTRAN##TMP_TRXGZ_FILEX'                                            --ARTRAN FILE
                                                               )
                    ORDER BY created DESC)
            LOOP
                xxwc_common_tunning_helpers.drop_temp_table (r.owner, r.object_name);
            END LOOP;
        END;

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'Start Arexti', g_start);

        -- Create temp table structure.
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_pmt_schdl_trx');

        EXECUTE IMMEDIATE
            'CREATE TABLE xxwc.AREXTI##tmp_pmt_schdl_trx
AS
   SELECT apsa.payment_schedule_id
      ,apsa.customer_trx_id
      ,apsa.discount_original
      ,apsa.org_id
      ,apsa.amount_due_remaining
      ,apsa.class
      ,apsa.due_date
      , (SELECT al.lookup_code
           FROM apps.ar_lookups al
          WHERE al.lookup_type = ''XXWC_DCMT_REASON_CODES'' AND apsa.attribute1 = al.meaning)
           reason_code
  FROM apps.ar_payment_schedules apsa
 WHERE apsa.amount_due_remaining <> 0 AND apsa.customer_trx_id IS NOT NULL AND apsa.status = ''OP'' AND 1 = 2';

        -- Settings to
      --  xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_pmt_schdl_trx');

        -- Populate table, the APPEND hint uses parallelism
        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/       
          INTO  xxwc.AREXTI##tmp_pmt_schdl_trx
        SELECT apsa.payment_schedule_id
      ,apsa.customer_trx_id
      ,apsa.discount_original
      ,apsa.org_id
      ,apsa.amount_due_remaining
      ,apsa.class
      ,apsa.due_date
      , (SELECT al.lookup_code
           FROM apps.ar_lookups al
          WHERE al.lookup_type = ''XXWC_DCMT_REASON_CODES'' AND apsa.attribute1 = al.meaning)
           reason_code
  FROM apps.ar_payment_schedules apsa
 WHERE apsa.amount_due_remaining <> 0 AND apsa.customer_trx_id IS NOT NULL AND apsa.status = ''OP''';

        COMMIT; -- Important to commit after using APPEND in a NO LOGGING table, won't be available to this session until this commit
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_pmt_schdl_trx', g_start);
        --create table to keep all trx for open payments schedules:

        -- Take all open transations with non-0 balances and merge them with TRX table
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_paydoc_cust_trx');

        EXECUTE IMMEDIATE
            'CREATE TABLE xxwc.AREXTI##tmp_paydoc_cust_trx
AS
    SELECT rcta.trx_number
          ,rcta.trx_date
          ,rcta.ct_reference
          ,rcta.customer_trx_id
          ,rcta.org_id
          ,rcta.interface_header_attribute7
          ,rcta.interface_header_attribute1
          ,rcta.interface_header_attribute12
          ,rcta.primary_salesrep_id
          ,rcta.fob_point
          ,rcta.bill_to_customer_id
          ,rcta.bill_to_site_use_id
          ,rcta.ship_to_address_id
          ,rcta.ship_to_site_use_id
          ,rcta.ship_to_customer_id
          ,rcta.term_id
          ,rcta.batch_source_id
          ,rcta.purchase_order
          ,rcta.creation_date
          ,rcta.interface_header_attribute8
          ,rcta.interface_header_attribute2
      FROM apps.ra_customer_trx rcta
     WHERE rcta.customer_trx_id IN (SELECT customer_trx_id FROM xxwc.AREXTI##tmp_pmt_schdl_trx) AND 1 = 2';

        -- This is the single call to populate the data for both AREXTI and
        -- ARMAST.
        EXECUTE IMMEDIATE
            ' INSERT /*+APPEND*/
          INTO  xxwc.AREXTI##tmp_paydoc_cust_trx
        SELECT rcta.trx_number
              ,rcta.trx_date
              ,rcta.ct_reference
              ,rcta.customer_trx_id
              ,rcta.org_id
              ,rcta.interface_header_attribute7
              ,rcta.interface_header_attribute1
              ,rcta.interface_header_attribute12
              ,rcta.primary_salesrep_id
              ,rcta.fob_point
              ,rcta.bill_to_customer_id
              ,rcta.bill_to_site_use_id
              ,rcta.ship_to_address_id
              ,rcta.ship_to_site_use_id
              ,rcta.ship_to_customer_id
              ,rcta.term_id
              ,rcta.batch_source_id
              ,rcta.purchase_order
              ,rcta.creation_date
              ,rcta.interface_header_attribute8
              ,rcta.interface_header_attribute2
          FROM apps.ra_customer_trx rcta
         WHERE rcta.customer_trx_id IN (SELECT customer_trx_id FROM xxwc.AREXTI##tmp_pmt_schdl_trx)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_paydoc_cust_trx', g_start);
        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************
        -- create table AREXTI##tmp_trx_order_date to populate order_date from trx lines table
        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_trx_order_date');

        EXECUTE IMMEDIATE
            'CREATE TABLE xxwc.AREXTI##tmp_trx_order_date
AS
    SELECT rctl.sales_order_date, rctl.customer_trx_id
      FROM apps.ra_customer_trx_lines rctl
     WHERE rctl.customer_trx_id IN (SELECT customer_trx_id FROM xxwc.AREXTI##tmp_paydoc_cust_trx) AND 1 = 2';

        --xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_trx_order_date');

        -- Get sales order date from TRX_LINES

        v_tuning_loop :=
            xxwc_common_tunning_helpers.add_tuning_parameter ('XXWC', 'xxwc.arexti##tmp_paydoc_cust_trx', 50);

        EXECUTE IMMEDIATE
            ' begin for r in 1..:v_tuning_loop loop
 INSERT /*+APPEND*/
      INTO  xxwc.arexti##tmp_trx_order_date
    SELECT rctl.sales_order_date, rctl.customer_trx_id
      FROM apps.ra_customer_trx_lines rctl
     WHERE     rctl.customer_trx_id IN (SELECT customer_trx_id FROM xxwc.arexti##tmp_paydoc_cust_trx WHERE GROUP_NUMBER =R)
          AND rctl.line_type = ''LINE''
          AND rctl.sales_order_date IS NOT NULL;
     COMMIT;
end loop; end;'
            USING v_tuning_loop;

        COMMIT;

        xxwc_common_tunning_helpers.remove_tuning_factor_columns ('XXWC', 'arexti##tmp_paydoc_cust_trx');

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'arexti##tmp_trx_order_date'
                                                                  ,'arexti##tmp_trx_order_date1');
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_trx_order_date,1', g_start);

        /*
            The next set of statements builds verious temp tables and
            adds various data points
        */

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_trx_order_date1
              SELECT MIN (sales_order_date), customer_trx_id
                FROM xxwc.AREXTI##tmp_trx_order_date
            GROUP BY customer_trx_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_trx_order_date1,1', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_trx_order_date');

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx1');

        EXECUTE IMMEDIATE 'ALTER TABLE  xxwc.AREXTI##tmp_paydoc_cust_trx1 ADD (sales_order_date DATE)';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_paydoc_cust_trx1
            SELECT rcta.*, dt.sales_order_date
              FROM xxwc.AREXTI##tmp_paydoc_cust_trx rcta, xxwc.AREXTI##tmp_trx_order_date1 dt
             WHERE rcta.customer_trx_id = dt.customer_trx_id(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_paydoc_cust_trx', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_trx_order_date1');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_paydoc_cust_trx');

        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************
        -- create table xxwc.AREXTI##tmp_trx_salesrep to populate salesrep name in trx table
        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_trx_salesrep');

     --   EXECUTE IMMEDIATE
     --       'CREATE TABLE xxwc.AREXTI##tmp_trx_salesrep
     -- AS
    --SELECT jrdv.source_first_name || '' '' || jrdv.source_last_name sales_rep_name, rsa.salesrep_id, rsa.org_id
    --  FROM apps.jtf_rs_defresources_v jrdv, apps.ra_salesreps rsa
    -- WHERE rsa.resource_id = jrdv.resource_id AND 1 = 2';

 --commented above and added below for ver1.2

   EXECUTE IMMEDIATE
           'CREATE TABLE xxwc.AREXTI##tmp_trx_salesrep
     AS
    SELECT jrdv.source_first_name || '' '' || jrdv.source_last_name sales_rep_name, rsa.salesrep_id, rsa.org_id
      FROM apps.jtf_rs_defresources_v jrdv, apps.ra_salesreps rsa
     WHERE rsa.person_id = jrdv.source_id AND 1 = 2';


        --xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_trx_salesrep');

       -- EXECUTE IMMEDIATE
--            'INSERT /*+APPEND*/
--              INTO  xxwc.AREXTI##tmp_trx_salesrep
--            SELECT jrdv.source_first_name || '' '' || jrdv.source_last_name sales_rep_name, rsa.salesrep_id, rsa.org_id
--              FROM apps.jtf_rs_defresources_v jrdv, apps.ra_salesreps rsa
--             WHERE rsa.resource_id = jrdv.resource_id';

 --commented above and added below for ver1.2

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_trx_salesrep
            SELECT jrdv.source_first_name || '' '' || jrdv.source_last_name sales_rep_name, rsa.salesrep_id, rsa.org_id
              FROM apps.jtf_rs_defresources_v jrdv, apps.ra_salesreps rsa
             WHERE rsa.person_id = jrdv.source_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_trx_salesrep', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx1'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx');

        EXECUTE IMMEDIATE 'ALTER TABLE  xxwc.AREXTI##tmp_paydoc_cust_trx ADD (sales_rep_name VARCHAR2(214))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_paydoc_cust_trx
            SELECT rcta.*, st.sales_rep_name
              FROM xxwc.AREXTI##tmp_paydoc_cust_trx1 rcta, xxwc.AREXTI##tmp_trx_salesrep st
             WHERE rcta.primary_salesrep_id = st.salesrep_id(+) AND rcta.org_id = st.org_id(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_paydoc_cust_trx,2', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_paydoc_cust_trx1');

        COMMIT;

        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************
        -- populate source_from
        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx1');

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.AREXTI##tmp_paydoc_cust_trx1 ADD(
source_from VARCHAR2(512))';

        --select * from xxwc.AREXTI##tmp_paydoc_cust_trx1

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_paydoc_cust_trx1
            SELECT rcta.*, xxwc_ar_getpaid_ob_pkg.get_inv_source (rcta.customer_trx_id, rcta.org_id)
              FROM xxwc.AREXTI##tmp_paydoc_cust_trx rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_paydoc_cust_trx1,2', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx1'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_paydoc_cust_trx
            SELECT * FROM xxwc.AREXTI##tmp_paydoc_cust_trx1';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_paydoc_cust_trx,3', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_paydoc_cust_trx1');

        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************
        -- populate order_org_code
        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************

        --  xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_trx_order_org');

        --  EXECUTE IMMEDIATE 'CREATE TABLE xxwc.AREXTI##tmp_trx_order_org
        --AS
        --  SELECT ood.organization_code, ood.organization_id, ooha.order_number
        --   FROM oe_order_headers_all ooha, org_organization_definitions ood
        --  WHERE ooha.ship_from_org_id = ood.organization_id AND 1 = 2';

        -- xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_trx_order_org');

        --  EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
        --    INTO  xxwc.AREXTI##tmp_trx_order_org
        --  SELECT ood.organization_code, ood.organization_id, ooha.order_number
        --   FROM oe_order_headers_all ooha, org_organization_definitions ood
        --  WHERE ooha.ship_from_org_id = ood.organization_id';

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx2');

        EXECUTE IMMEDIATE 'ALTER TABLE  xxwc.AREXTI##tmp_paydoc_cust_trx2  ADD (order_org_code   VARCHAR2(124))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_paydoc_cust_trx2
            SELECT rcta.*

                  ,NVL((CASE WHEN rcta.source_from IN (''ORDER MANAGEMENT'',''STANDARD OM SOURCE''
                                        , ''REPAIR OM SOURCE'')
                  THEN (SELECT ood.organization_code
                          FROM apps.oe_order_headers           ooha
                             , apps.org_organization_definitions   ood
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = ood.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN rcta.source_from = ''PRISM''
                  THEN rcta.interface_header_attribute7
                  ELSE '' ''
                  END),'' '')

              FROM xxwc.AREXTI##tmp_paydoc_cust_trx rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_paydoc_cust_trx2,3', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_paydoc_cust_trx');

        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************
        -- populate AMOUNT_REMAINING
        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_paydoc_cust_trx2'
                                                                  ,'AREXTI##tmp_cust_trxg');

        EXECUTE IMMEDIATE
            'ALTER TABLE xxwc.AREXTI##tmp_cust_trxg ADD (  payment_schedule_id NUMBER , discount_original NUMBER, amount_due_remaining NUMBER
, class VARCHAR2(32)
          , due_date DATE, reason_code varchar2(128) )';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg
            SELECT rcta.*
                  ,OP.payment_schedule_id
                  ,op.discount_original discount_original
                  ,op.amount_due_remaining amount_due_remaining
                  ,op.class
                  ,op.due_date
                  ,op.reason_code
              FROM xxwc.AREXTI##tmp_paydoc_cust_trx2 rcta, xxwc.AREXTI##tmp_pmt_schdl_trx op
             WHERE op.customer_trx_id = rcta.customer_trx_id';

        COMMIT;

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trxg,4', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_paydoc_cust_trx2');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_pmt_schdl_trx');

        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************
        -- populate billto shipto fields
        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cust_trxg'
                                                                  ,'AREXTI##tmp_cust_trxg1');

        EXECUTE IMMEDIATE ' ALTER TABLE xxwc.AREXTI##tmp_cust_trxg1 ADD(
bill_to_account_number VARCHAR2(128),
bill_to_account_name VARCHAR2(512),
bill_to_address1 VARCHAR2(240), -- bill_to_address1 VARCHAR2(128), -- Ver#1.3 Commented this field by Pattabhi Avula on 26-May-2017 for TMS#20170526-00002(Prod Issue)
bill_to_address2 VARCHAR2(240), -- bill_to_address1 VARCHAR2(128),  -- Ver#1.3 Commented this field by Pattabhi Avula on 26-May-2017 for TMS#20170526-00002(Prod Issue)
bill_to_city VARCHAR2(128),
bill_to_state VARCHAR2(32),
bill_to_postal_code VARCHAR2(32),
bill_to_cust_account_id NUMBER)';

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.AREXTI##tmp_cust_trxg1 ADD(
ship_to_location VARCHAR2(512),
ship_to_address1 VARCHAR2(512),
ship_to_address2 VARCHAR2(512),
ship_to_city VARCHAR2(512),
ship_to_state VARCHAR2(64),
ship_to_postal_code VARCHAR2(32),
ship_to_attribute19 VARCHAR2(512),
ship_to_party_site_number   VARCHAR2(512),
 ship_to_location_id  NUMBER)';

        --select * from xxwc_customer_sites_br where cust_account_id in(
        --select  cust_account_id from xxwc_customer_sites_br where  primary_flag  = 'Y' group by cust_account_id
        --having count(cust_account_id)>1)  and primary_flag  = 'Y'
        --select count(*) from xxwc.AREXTI##tmp_cust_trxg

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg1
            SELECT a.*
                  ,bill_to.account_number
                  ,bill_to.account_name
                  ,bill_to.address1
                  ,bill_to.address2
                  ,bill_to.city
                  ,bill_to.state
                  ,bill_to.postal_code
                  ,bill_to.cust_account_id
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
              FROM xxwc.AREXTI##tmp_cust_trxg a, xxwc_customer_sites_br bill_to
             WHERE     a.bill_to_customer_id = bill_to.cust_account_id(+)
                   AND bill_to.primary_flag(+) = ''Y''
                   AND bill_to.org_id(+) = a.org_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trxg1,5', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_cust_trxg');

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cust_trxg1'
                                                                  ,'AREXTI##tmp_cust_trxg');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg
            SELECT a.trx_number
                  ,a.trx_date
                  ,a.ct_reference
                  ,a.customer_trx_id
                  ,a.org_id
                  ,a.interface_header_attribute7
                  ,a.interface_header_attribute1
                  ,a.interface_header_attribute12
                  ,a.primary_salesrep_id
                  ,a.fob_point
                  ,a.bill_to_customer_id
                  ,a.bill_to_site_use_id
                  ,nvl(a.ship_to_address_id,ship_to.cust_acct_site_id)
                  ,a.ship_to_site_use_id
                  ,nvl(a.ship_to_customer_id,ship_to.cust_account_id)--a.ship_to_customer_id
                  ,a.term_id
                  ,a.batch_source_id
                  ,a.purchase_order
                  ,a.creation_date
                  ,a.interface_header_attribute8
                  ,a.interface_header_attribute2
                  ,a.sales_order_date
                  ,a.sales_rep_name
                  ,a.source_from
                  ,a.order_org_code
                  ,A.payment_schedule_id
                  ,a.discount_original
                  ,a.amount_due_remaining
                  ,a.class
                  ,a.due_date
                  ,a.reason_code
                  ,a.bill_to_account_number
                  ,a.bill_to_account_name
                  ,a.bill_to_address1
                  ,a.bill_to_address2
                  ,a.bill_to_city
                  ,a.bill_to_state
                  ,a.bill_to_postal_code
                  ,a.bill_to_cust_account_id
                  ,ship_to.location
                  ,ship_to.address1
                  ,ship_to.address2
                  ,ship_to.city
                  ,ship_to.state
                  ,ship_to.postal_code
                  ,ship_to.attribute19
                  ,ship_to.party_site_number
                  ,ship_to.location_id
              FROM xxwc.AREXTI##tmp_cust_trxg1 a, apps.xxwc_customer_sites_sr ship_to
             WHERE     a.ship_to_site_use_id = ship_to.site_use_id(+)
                   AND a.ship_to_site_use_id IS NOT NULL
                   AND a.org_id = ship_to.org_id(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trxg,6', g_start);

        --select * from XXWC.AREXTI##tmp_cust_trxg

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg
            SELECT a.trx_number
                  ,a.trx_date
                  ,a.ct_reference
                  ,a.customer_trx_id
                  ,a.org_id
                  ,a.interface_header_attribute7
                  ,a.interface_header_attribute1
                  ,a.interface_header_attribute12
                  ,a.primary_salesrep_id
                  ,a.fob_point
                  ,a.bill_to_customer_id
                  ,a.bill_to_site_use_id
                  ,a.ship_to_address_id
                  ,nvl(a.ship_to_site_use_id,ship_to.site_use_id)                                                             --ship_to_site_use_id
                  ,nvl(a.ship_to_customer_id,ship_to.cust_account_id)--a.ship_to_customer_id
                  ,a.term_id
                  ,a.batch_source_id
                  ,a.purchase_order
                  ,a.creation_date
                  ,a.interface_header_attribute8
                  ,a.interface_header_attribute2
                  ,a.sales_order_date
                  ,a.sales_rep_name
                  ,a.source_from
                  ,a.order_org_code
                   ,A.payment_schedule_id
                  ,a.discount_original
                  ,a.amount_due_remaining
                  ,a.class
                  ,a.due_date
                  ,a.reason_code
                  ,a.bill_to_account_number
                  ,a.bill_to_account_name
                  ,a.bill_to_address1
                  ,a.bill_to_address2
                  ,a.bill_to_city
                  ,a.bill_to_state
                  ,a.bill_to_postal_code
                  ,a.bill_to_cust_account_id
                  ,ship_to.location
                  ,ship_to.address1
                  ,ship_to.address2
                  ,ship_to.city
                  ,ship_to.state
                  ,ship_to.postal_code
                  ,ship_to.attribute19
                  ,ship_to.party_site_number
                  ,ship_to.location_id
              FROM xxwc.AREXTI##tmp_cust_trxg1 a, apps.xxwc_customer_sites_sr ship_to
             WHERE     a.ship_to_address_id = ship_to.cust_acct_site_id
                   AND a.ship_to_site_use_id IS NULL
                   AND a.ship_to_address_id IS NOT NULL
                   AND a.bill_to_customer_id = ship_to.cust_account_id
                   AND a.org_id = ship_to.org_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trxg,7', g_start);

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg
            SELECT a.trx_number
                  ,a.trx_date
                  ,a.ct_reference
                  ,a.customer_trx_id
                  ,a.org_id
                  ,a.interface_header_attribute7
                  ,a.interface_header_attribute1
                  ,a.interface_header_attribute12
                  ,a.primary_salesrep_id
                  ,a.fob_point
                  ,a.bill_to_customer_id
                  ,a.bill_to_site_use_id
                  ,nvl(a.ship_to_address_id,ship_to.cust_acct_site_id)                                                      -- ship_to_address_id
                  ,ship_to.site_use_id                                                             --ship_to_site_use_id
                  ,nvl(a.ship_to_customer_id,ship_to.cust_account_id)
                  ,a.term_id
                  ,a.batch_source_id
                  ,a.purchase_order
                  ,a.creation_date
                  ,a.interface_header_attribute8
                  ,a.interface_header_attribute2
                  ,a.sales_order_date
                  ,a.sales_rep_name
                  ,a.source_from
                  ,a.order_org_code
                   ,A.payment_schedule_id
                  ,a.discount_original
                  ,a.amount_due_remaining
                  ,a.class
                  ,a.due_date
                   ,a.reason_code
                  ,a.bill_to_account_number
                  ,a.bill_to_account_name
                  ,a.bill_to_address1
                  ,a.bill_to_address2
                  ,a.bill_to_city
                  ,a.bill_to_state
                  ,a.bill_to_postal_code
                  ,a.bill_to_cust_account_id
                  ,ship_to.location
                  ,ship_to.address1
                  ,ship_to.address2
                  ,ship_to.city
                  ,ship_to.state
                  ,ship_to.postal_code
                  ,ship_to.attribute19
                  ,ship_to.party_site_number
                  ,ship_to.location_id
              FROM xxwc.AREXTI##tmp_cust_trxg1 a, apps.xxwc_customer_sites_sr ship_to
             WHERE     a.bill_to_customer_id = ship_to.cust_account_id
                   AND a.ship_to_site_use_id IS NULL
                   AND a.ship_to_address_id IS NULL
                   AND ship_to.primary_flag = ''Y''
                   AND a.org_id = ship_to.org_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trxg,8', g_start);
        /*INSERT /*+APPEND*/
        --  INTO  xxwc.AREXTI##tmp_cust_trxg
        --SELECT *
        --  FROM xxwc.AREXTI##tmp_cust_trxg1
        -- WHERE customer_trx_id IN (SELECT customer_trx_id FROM xxwc.AREXTI##tmp_cust_trxg1
        --   MINUS
        --  SELECT customer_trx_id FROM xxwc.AREXTI##tmp_cust_trxg);

        --COMMIT;
        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************
        -- populate term and discount days fields
        --*******************************************************************************
        --*******************************************************************************
        --*******************************************************************************

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cust_trxg'
                                                                  ,'AREXTI##tmp_cust_trxg1');

        EXECUTE IMMEDIATE ' ALTER TABLE xxwc.AREXTI##tmp_cust_trxg1 ADD(
 discount_days   NUMBER)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg1
            SELECT a.*, rtld.discount_days
              FROM xxwc.AREXTI##tmp_cust_trxg a, apps.ra_terms_lines_discounts rtld
             WHERE a.term_id = rtld.term_id(+)';

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cust_trxg1'
                                                                  ,'AREXTI##tmp_cust_trxg');

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.AREXTI##tmp_cust_trxg ADD(
 payment_terms   VARCHAR2(124))';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg
            SELECT a.*, rt.description
              FROM xxwc.AREXTI##tmp_cust_trxg1 a, apps.ra_terms rt
             WHERE a.term_id = rt.term_id(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trxg,9', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cust_trxg'
                                                                  ,'AREXTI##tmp_cust_trxg1');

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.AREXTI##tmp_cust_trxg1   ADD(
gp_dmp_ordate DATE ,
gp_dmp_warehse VARCHAR2(124) ,
gp_dmp_shipvia VARCHAR2(124)  ,
gp_dmp_shipname VARCHAR2(124)  ,
gp_dmp_shipadd1 VARCHAR2(124)  ,
gp_dmp_shipadd2 VARCHAR2(124),
 gp_dmp_shipadd3 VARCHAR2(124) ,
gp_dmp_shipadd4 VARCHAR2(124) ,
gp_dmp_shipadd5 VARCHAR2(124) )';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg1
            SELECT a.*
                  ,gp_dmp.ordate
                  ,gp_dmp.warehse
                  ,gp_dmp.shipvia
                  ,gp_dmp.shipname
                  ,gp_dmp.shipadd1
                  ,gp_dmp.shipadd2
                  ,gp_dmp.shipadd3
                  ,gp_dmp.shipadd4
                  ,gp_dmp.shipadd5
              FROM xxwc.AREXTI##tmp_cust_trxg a, xxwc.xxwc_arexti_getpaid_dump_tbl gp_dmp
             WHERE a.trx_number = gp_dmp.invno(+) AND a.bill_to_account_number = gp_dmp.custno(+)';

        COMMIT;

        ---------
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trxg1,10', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_ship_via');

        EXECUTE IMMEDIATE
            'CREATE TABLE xxwc.AREXTI##tmp_ship_via
AS
    SELECT ship_method_meaning
          ,ooh.shipping_method_code
          ,TO_CHAR (ooh.order_number) order_number
          ,ooh.org_id
      FROM apps.oe_order_headers ooh, apps.wsh_carrier_services wcs
     WHERE ooh.shipping_method_code IS NOT NULL AND ooh.shipping_method_code = wcs.ship_method_code AND 1 = 2';

        --xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_ship_via');

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_ship_via
            SELECT ship_method_meaning
                  ,ooh.shipping_method_code
                  ,TO_CHAR (ooh.order_number) order_number
                  ,ooh.org_id
              FROM apps.oe_order_headers ooh, apps.wsh_carrier_services wcs
             WHERE ooh.shipping_method_code IS NOT NULL AND ooh.shipping_method_code = wcs.ship_method_code';

        --select * from wsh_carrier_services
        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_ship_via', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cust_trxg1'
                                                                  ,'AREXTI##tmp_cust_trxg');

        EXECUTE IMMEDIATE
            'ALTER TABLE xxwc.AREXTI##tmp_cust_trxg ADD(ship_via VARCHAR2(124),order_number VARCHAR2(124))';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg
            SELECT a.*, NULL, NVL   (a.ct_reference,a.interface_header_attribute1)
              FROM xxwc.AREXTI##tmp_cust_trxg1 a';

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cust_trxg'
                                                                  ,'AREXTI##tmp_cust_trxg1');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg1
            SELECT a.trx_number
                  ,a.trx_date
                  ,a.ct_reference
                  ,a.customer_trx_id
                  ,a.org_id
                  ,a.interface_header_attribute7
                  ,a.interface_header_attribute1
                  ,a.interface_header_attribute12
                  ,a.primary_salesrep_id
                  ,a.fob_point
                  ,a.bill_to_customer_id
                  ,a.bill_to_site_use_id
                  ,a.ship_to_address_id
                  ,a.ship_to_site_use_id
                  ,a.ship_to_customer_id
                  ,a.term_id
                  ,a.batch_source_id
                  ,a.purchase_order
                  ,a.creation_date
                  ,a.interface_header_attribute8
                  ,a.interface_header_attribute2
                  ,a.sales_order_date
                  ,a.sales_rep_name
                  ,a.source_from
                  ,a.order_org_code
                   ,A.payment_schedule_id
                  ,a.discount_original
                  ,a.amount_due_remaining
                  ,a.class
                  ,a.due_date
                  ,a.reason_code
                  ,a.bill_to_account_number
                  ,a.bill_to_account_name
                  ,a.bill_to_address1
                  ,a.bill_to_address2
                  ,a.bill_to_city
                  ,a.bill_to_state
                  ,a.bill_to_postal_code
                  ,a.bill_to_cust_account_id
                  ,a.ship_to_location
                  ,a.ship_to_address1
                  ,a.ship_to_address2
                  ,a.ship_to_city
                  ,a.ship_to_state
                  ,a.ship_to_postal_code
                  ,a.ship_to_attribute19
                  ,a.ship_to_party_site_number
                  ,a.ship_to_location_id
                  ,a.discount_days
                  ,a.payment_terms
                  ,a.gp_dmp_ordate
                  ,a.gp_dmp_warehse
                  ,a.gp_dmp_shipvia
                  ,a.gp_dmp_shipname
                  ,a.gp_dmp_shipadd1
                  ,a.gp_dmp_shipadd2
                  ,a.gp_dmp_shipadd3
                  ,a.gp_dmp_shipadd4
                  ,a.gp_dmp_shipadd5
                  ,sv.ship_method_meaning
                  ,a.order_number
              FROM xxwc.AREXTI##tmp_cust_trxg a, xxwc.AREXTI##tmp_ship_via sv
             WHERE sv.order_number(+) = a.order_number AND a.org_id = sv.org_id(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trxg1,11', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_ship_via');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_cust_trxg');

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cust_trxg1'
                                                                  ,'AREXTI##tmp_cust_trxg');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trxg
            SELECT a.trx_number
                  ,a.trx_date
                  ,a.ct_reference
                  ,a.customer_trx_id
                  ,a.org_id
                  ,a.interface_header_attribute7
                  ,a.interface_header_attribute1
                  ,a.interface_header_attribute12
                  ,a.primary_salesrep_id
                  ,a.fob_point
                  ,a.bill_to_customer_id
                  ,a.bill_to_site_use_id
                  ,a.ship_to_address_id
                  ,a.ship_to_site_use_id
                  ,a.ship_to_customer_id
                  ,a.term_id
                  ,a.batch_source_id
                  ,a.purchase_order
                  ,a.creation_date
                  ,a.interface_header_attribute8
                  ,a.interface_header_attribute2
                  ,a.sales_order_date
                  ,a.sales_rep_name
                  ,a.source_from
                  ,a.order_org_code
                   ,A.payment_schedule_id
                  ,a.discount_original
                  ,a.amount_due_remaining
                  ,a.class
                  ,a.due_date
                   ,a.reason_code
                  ,a.bill_to_account_number
                  ,a.bill_to_account_name
                  ,a.bill_to_address1
                  ,a.bill_to_address2
                  ,a.bill_to_city
                  ,a.bill_to_state
                  ,a.bill_to_postal_code
                  ,a.bill_to_cust_account_id
                  ,a.ship_to_location
                  ,a.ship_to_address1
                  ,a.ship_to_address2
                  ,a.ship_to_city
                  ,a.ship_to_state
                  ,a.ship_to_postal_code
                  ,a.ship_to_attribute19
                  ,a.ship_to_party_site_number
                  ,a.ship_to_location_id
                  ,a.discount_days
                  ,a.payment_terms
                  ,a.gp_dmp_ordate
                  ,a.gp_dmp_warehse
                  ,a.gp_dmp_shipvia
                  ,a.gp_dmp_shipname
                  ,a.gp_dmp_shipadd1
                  ,a.gp_dmp_shipadd2
                  ,a.gp_dmp_shipadd3
                  ,a.gp_dmp_shipadd4
                  ,a.gp_dmp_shipadd5
                  ,NVL (
                       DECODE (a.source_from
                              ,''PRISM'', a.interface_header_attribute12
                              ,''ORDER MANAGEMENT'', a.ship_via
                              ,''STANDARD OM SOURCE'', a.ship_via
                              ,''REPAIR OM SOURCE'', a.ship_via
                              ,'' '')
                      ,''99:WILL CALL'')
                  ,a.order_number
              FROM xxwc.
              AREXTI##tmp_cust_trxg1 a';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trxg,12', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_cust_trxg1');

        xxwc_common_tunning_helpers.deleteduplicates ('xxwc.AREXTI##tmp_cust_trxg'
                                                     ,' customer_trx_id,payment_schedule_id '
                                                     ,NULL);

        -- This and the next statement are where the CONVERSION and non-CONVERSION
        -- rows are collected into a single table.
        EXECUTE IMMEDIATE
            ' UPDATE xxwc.arexti##tmp_cust_trxg
           SET purchase_order = xxwc_common_tunning_helpers.remove_control (purchase_order)
              ,interface_header_attribute12 = xxwc_common_tunning_helpers.remove_control (interface_header_attribute12)
              ,sales_rep_name = xxwc_common_tunning_helpers.remove_control (sales_rep_name)
              ,bill_to_account_name = xxwc_common_tunning_helpers.remove_control (bill_to_account_name)
              ,bill_to_address1 = xxwc_common_tunning_helpers.remove_control (bill_to_address1)
              ,bill_to_address2 = xxwc_common_tunning_helpers.remove_control (bill_to_address2)
              ,ship_to_location = xxwc_common_tunning_helpers.remove_control (ship_to_location)
              ,ship_to_address1 = xxwc_common_tunning_helpers.remove_control (ship_to_address1)
              ,ship_to_address2 = xxwc_common_tunning_helpers.remove_control (ship_to_address2)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trxg,13', g_start);

        EXECUTE IMMEDIATE
            'CREATE TABLE xxwc.AREXTI##tmp_cust_trx_file
AS
    SELECT    rpad (NVL (bill_to_account_number, '' ''), 20, '' '')                                      --  CUSTOMER_NUMBER
           ||rpad (NVL (trx.trx_number, '' ''), 20, '' '')                                           --  TRANSACTION_NUMBER
           ||rpad (NVL (ship_to_location, '' ''), 35, '' '')                                               --  SHIP_TO_NAME
           ||rpad (NVL (ship_to_address1, '' ''), 50, '' '')                                          --  SHIP_TO_ADDRESS_1
           ||rpad (NVL (TRIM (ship_to_address2), '' ''), 50, '' '')                                   --  SHIP_TO_ADDRESS_2
           ||rpad (NVL (ship_to_city, '' ''), 50, '' '')                                              --  SHIP_TO_ADDRESS_3
           || RPAD (NVL (ship_to_state, '' ''), 50, '' '')                                             --  SHIP_TO_ADDRESS_4
           || RPAD (NVL (ship_to_postal_code, '' ''), 50, '' '')                                       --  SHIP_TO_ADDRESS_5
           ||rpad (NVL (trx.ct_reference, '' ''), 11, '' '')
           --  ORDER_NUMBER
           || RPAD (
                  NVL (
                      (RPAD (NVL (  TO_CHAR (trx.trx_date, ''MMDDYYYY''),TO_CHAR (trx.sales_order_date, ''MMDDYYYY''))
                            ,8
                            ,'' ''))
                     ,'' '')
                 ,8
                 ,'' '')                                                                                    --  ORDER_DATE
           --  ORDER_DATE

           ||rpad (NVL (trx.order_org_code, '' ''), 35, '' '')                                                --  WAREHOUSE
           || RPAD (NVL (trx.payment_terms, '' ''), 20, '' '')                                             --  PAYMENT_TERMS
           ||rpad (trx.ship_via, 30, '' '')                                                           --  SHIP_VIA_NUMBER
           || RPAD (NVL ('' '', '' ''), 15, '' '')                                                            --  ORDER_STATUS
           || RPAD (NVL ('' '', '' ''), 5, '' '')                                                   --  ORDER_SHIPMENT_CARRIER
           || LPAD (NVL (TO_CHAR (trx.discount_original, ''9999999990.99''), ''0.00''), 20, '' '')         --  DISCOUNT_AMOUNT
           || LPAD (NVL (TO_CHAR (trx.discount_days), ''0''), 3, '' '')                                    --  DISCOUNT_DAYS
           ||rpad (NVL (trx.sales_rep_name, '' ''), 10, '' '')                                                --  SALES_MAN
           ||rpad (NVL (trx.fob_point, '' ''), 30, '' '')                                                     --  FOB_POINT
           || RPAD (NVL (TO_CHAR (trx.trx_date, ''MMDDYYYY''), '' ''), 8, '' '')                                 --  SHIP_DATE
           ||rpad (NVL (bill_to_account_name, '' ''), 50, '' '')                                           --  BILL_TO_NAME
           --     || xxwc_ar_getpaid_ob_pkg.get_bill_to_addr(TRX.org_id
           --                                              , BILL_TO_cust_account_id
           --                                              , SHIP_TO_bill_to_site_use_id)                              --  BILL_TO_ADDRESS
           ||rpad (NVL (bill_to_address1, '' ''), 50, '' '')                                          --  BILL_TO_ADDRESS_1
           ||rpad (NVL (TRIM (bill_to_address2), '' ''), 50, '' '')                                   --  BILL_TO_ADDRESS_2
           ||rpad (NVL (bill_to_city, '' ''), 50, '' '')                                              --  BILL_TO_ADDRESS_3
           || RPAD (NVL (bill_to_state, '' ''), 50, '' '')                                             --  BILL_TO_ADDRESS_4
           || RPAD (NVL (bill_to_postal_code, '' ''), 50, '' '')                                       --  BILL_TO_ADDRESS_5
               rec_line
          ,trx.org_id     --, BILL_TO_account_number, BILL_TO_cust_account_id , TRX.trx_number, TRX.AMOUNT_DUE_REMAINING
      FROM xxwc.AREXTI##tmp_cust_trxg trx
     WHERE trx.source_from <> ''CONVERSION'' AND 1 = 2';

        --xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_cust_trx_file');

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.AREXTI##tmp_cust_trx_file ADD( TRX_RECEIPT_PAYMENT_ID VARCHAR2(224) )';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trx_file
            SELECT    RPAD (NVL (bill_to_account_number, '' ''), 20, '' '')                              --  CUSTOMER_NUMBER
                   || RPAD (NVL (trx.trx_number, '' ''), 20, '' '')                                   --  TRANSACTION_NUMBER
                   ||rpad (NVL (ship_to_location, '' ''), 35, '' '')                                       --  SHIP_TO_NAME
                   ||rpad (NVL (ship_to_address1, '' ''), 50, '' '')                                  --  SHIP_TO_ADDRESS_1
                   ||rpad (NVL (TRIM (ship_to_address2), '' ''), 50, '' '')                           --  SHIP_TO_ADDRESS_2
                   || RPAD (NVL (ship_to_city, '' ''), 50, '' '')                                      --  SHIP_TO_ADDRESS_3
                   || RPAD (NVL (ship_to_state, '' ''), 50, '' '')                                     --  SHIP_TO_ADDRESS_4
                   || RPAD (NVL (ship_to_postal_code, '' ''), 50, '' '')                               --  SHIP_TO_ADDRESS_5
                   ||rpad (NVL (trx.ct_reference, '' ''), 11, '' '')
                   --  ORDER_NUMBER
                   ||RPAD(NVL((SELECT RPAD(NVL(TO_CHAR(rctl.sales_order_date,''MMDDYYYY'')
                                , TO_CHAR(trx.trx_date,''MMDDYYYY'')),8,'' '')
                    FROM apps.ra_customer_trx_lines rctl
                   WHERE rctl.customer_trx_id = trx.customer_trx_id
                     AND ROWNUM = 1)
                , '' '')
              , 8, '' '')                                                                           --  ORDER_DATE
                   --  ORDER_DATE
                   ||rpad (NVL (trx.order_org_code, '' ''), 35, '' '')                                        --  WAREHOUSE
                   || RPAD (NVL (trx.payment_terms, '' ''), 20, '' '')                                     --  PAYMENT_TERMS
                   ||rpad (trx.ship_via, 30, '' '')                                                   --  SHIP_VIA_NUMBER
                   || RPAD (NVL ('' '', '' ''), 15, '' '')                                                    --  ORDER_STATUS
                   || RPAD (NVL ('' '', '' ''), 5, '' '')                                           --  ORDER_SHIPMENT_CARRIER
                   || LPAD (NVL (TO_CHAR (trx.discount_original, ''9999999990.99''), ''0.00''), 20, '' '') --  DISCOUNT_AMOUNT
                   || LPAD (NVL (TO_CHAR (trx.discount_days), ''0''), 3, '' '')                            --  DISCOUNT_DAYS
                   ||rpad (NVL (trx.sales_rep_name, '' ''), 10, '' '')                                        --  SALES_MAN
                   ||rpad (NVL (trx.fob_point, '' ''), 30, '' '')                                             --  FOB_POINT
                   || RPAD (NVL (TO_CHAR (trx.trx_date, ''MMDDYYYY''), '' ''), 8, '' '')                         --  SHIP_DATE
                   ||rpad (NVL (bill_to_account_name, '' ''), 50, '' '')                                   --  BILL_TO_NAME
                   --     || xxwc_ar_getpaid_ob_pkg.get_bill_to_addr(TRX.org_id
                   --                                              , BILL_TO_cust_account_id
                   --                                              , SHIP_TO_bill_to_site_use_id)                              --  BILL_TO_ADDRESS
                   ||rpad (NVL (bill_to_address1, '' ''), 50, '' '')                                  --  BILL_TO_ADDRESS_1
                   ||rpad (NVL (TRIM (bill_to_address2), '' ''), 50, '' '')                           --  BILL_TO_ADDRESS_2
                   ||rpad (NVL (bill_to_city, '' ''), 50, '' '')                                      --  BILL_TO_ADDRESS_3
                   || RPAD (NVL (bill_to_state, '' ''), 50, '' '')                                     --  BILL_TO_ADDRESS_4
                   || RPAD (NVL (bill_to_postal_code, '' ''), 50, '' '')                               --  BILL_TO_ADDRESS_5
                       rec_line
                  ,trx.org_id , customer_trx_id||payment_schedule_id
              FROM xxwc.AREXTI##tmp_cust_trxg trx
             WHERE trx.source_from <> ''CONVERSION''';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trx_file,1', g_start);

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cust_trx_file
            SELECT     RPAD (NVL (bill_to_account_number, '' ''), 20, '' '')                              --  CUSTOMER_NUMBER
                   || RPAD (NVL (trx.trx_number, '' ''), 20, '' '')                                   --  TRANSACTION_NUMBER
                   ||rpad (NVL (ship_to_location, '' ''), 35, '' '')                                       --  SHIP_TO_NAME
                   ||rpad (NVL (ship_to_address1, '' ''), 50, '' '')                                  --  SHIP_TO_ADDRESS_1
                   ||rpad (NVL (TRIM (ship_to_address2), '' ''), 50, '' '')                           --  SHIP_TO_ADDRESS_2
                   || RPAD (NVL (ship_to_city, '' ''), 50, '' '')                                      --  SHIP_TO_ADDRESS_3
                   || RPAD (NVL (ship_to_state, '' ''), 50, '' '')                                     --  SHIP_TO_ADDRESS_4
                   || RPAD (NVL (ship_to_postal_code, '' ''), 50, '' '')                               --  SHIP_TO_ADDRESS_5
                   ||rpad (NVL (trx.ct_reference, '' ''), 11, '' '')                                       --  ORDER_NUMBER
                   || RPAD (NVL (TO_CHAR (gp_dmp_ordate, ''MMDDYYYY''), '' ''), 8, '' '')                       --  ORDER_DATE
                   || RPAD (NVL (gp_dmp_warehse, ''000''), 35, '' '')                                          --  WAREHOUSE
                   || RPAD (NVL (trx.payment_terms, '' ''), 20, '' '')                                     --  PAYMENT_TERMS
                   ||rpad (NVL (gp_dmp_shipvia, ''99:WILL CALL''), 30, '' '')                           --  SHIP_VIA_NUMBER
                   || RPAD (NVL ('' '', '' ''), 15, '' '')                                                    --  ORDER_STATUS
                   || RPAD (NVL ('' '', '' ''), 5, '' '')                                           --  ORDER_SHIPMENT_CARRIER
                   || LPAD (NVL (TO_CHAR (trx.discount_original, ''9999999990.99''), ''0.00''), 20, '' '') --  DISCOUNT_AMOUNT
                   || LPAD (NVL (TO_CHAR (trx.discount_days), ''0''), 3, '' '')                            --  DISCOUNT_DAYS
                   ||rpad (NVL (trx.sales_rep_name, '' ''), 10, '' '')                                        --  SALES_MAN
                   ||rpad (NVL (trx.fob_point, '' ''), 30, '' '')                                             --  FOB_POINT
                   || RPAD (NVL (TO_CHAR (trx.trx_date, ''MMDDYYYY''), '' ''), 8, '' '')                         --  SHIP_DATE
                   ||rpad (NVL (bill_to_account_name, '' ''), 50, '' '')                                   --  BILL_TO_NAME
                   --     || xxwc_ar_getpaid_ob_pkg.get_bill_to_addr(rcta.org_id
                   --                                              , BILL_TO_cust_account_id
                   --                                              , hcsu_s.bill_to_site_use_id)                              --  BILL_TO_ADDRESS
                   ||rpad (NVL (bill_to_address1, '' ''), 50, '' '')                                  --  BILL_TO_ADDRESS_1
                   ||rpad (NVL (TRIM (bill_to_address2), '' ''), 50, '' '')                           --  BILL_TO_ADDRESS_2
                   ||rpad (NVL (bill_to_city, '' ''), 50, '' '')                                      --  BILL_TO_ADDRESS_3
                   || RPAD (NVL (bill_to_state, '' ''), 50, '' '')                                     --  BILL_TO_ADDRESS_4
                   || RPAD (NVL (bill_to_postal_code, '' ''), 50, '' '')                               --  BILL_TO_ADDRESS_5
                       rec_line
                  ,trx.org_id , customer_trx_id||payment_schedule_id
              FROM xxwc.AREXTI##tmp_cust_trxg trx
             WHERE trx.source_from = ''CONVERSION''';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cust_trx_file,2', g_start);
        ---********************************************************************************
        ---********************************************************************************
        ---********************************************************************************
        ---********************************************************************************

        /* Find all aging receipts. Same approach as code above. */

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_rcpt_pshd_acra');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.AREXTI##tmp_rcpt_pshd_acra
AS
    SELECT apsa.cash_receipt_id
          ,apsa.payment_schedule_id
          ,apsa.discount_original
          ,apsa.org_id
          ,apsa.amount_due_remaining
          ,apsa.class
          ,apsa.due_date
          ,apsa.customer_id
          ,acra.receipt_number
          ,acra.receipt_date
          ,acra.customer_site_use_id
          ,acra.TYPE
          ,acra.pay_from_customer
          ,acra.receipt_method_id
      FROM apps.ar_payment_schedules apsa, apps.ar_cash_receipts acra
     WHERE     apsa.amount_due_remaining <> 0
           AND acra.cash_receipt_id = apsa.cash_receipt_id
           AND apsa.cash_receipt_id IS NOT NULL
           AND apsa.org_id = acra.org_id
           AND acra.TYPE = ''CASH''
           AND 1 = 2';

       -- xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_rcpt_pshd_acra');

        EXECUTE IMMEDIATE ' INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_rcpt_pshd_acra
            SELECT apsa.cash_receipt_id
                  ,payment_schedule_id
                  ,apsa.discount_original
                  ,apsa.org_id
                  ,apsa.amount_due_remaining
                  ,apsa.class
                  ,apsa.due_date
                  ,apsa.customer_id
                  ,acra.receipt_number
                  ,acra.receipt_date
                  ,acra.customer_site_use_id
                  ,acra.TYPE
                  ,acra.pay_from_customer
                  ,acra.receipt_method_id
              FROM apps.ar_payment_schedules apsa, apps.ar_cash_receipts acra
             WHERE     apsa.amount_due_remaining <> 0
                   AND acra.cash_receipt_id = apsa.cash_receipt_id
                   AND apsa.customer_id = acra.pay_from_customer
                   AND apsa.cash_receipt_id IS NOT NULL
                   AND apsa.org_id = acra.org_id
                   AND acra.TYPE = ''CASH''';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_rcpt_pshd_acra,1', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_rcpt_pshd_acra'
                                                                  ,'AREXTI##tmp_cash_rpts_acrag');

        EXECUTE IMMEDIATE
            'ALTER TABLE xxwc.AREXTI##tmp_cash_rpts_acrag ADD (  arm_attribute2 VARCHAR2(512), arm_name  VARCHAR2(512),arc_name  VARCHAR2(512),arm_attribute1 

varchar2(16))';

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_recpt_method');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.AREXTI##tmp_recpt_method
AS
    SELECT arm.attribute2
          ,arm.name arm_name
          ,arc.name arc_name
          ,arm.receipt_method_id
          ,arm.attribute1
      FROM apps.ar_receipt_methods arm, apps.ar_receipt_classes arc
     WHERE arm.receipt_class_id = arc.receipt_class_id AND 1 = 2';

       -- xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_recpt_method');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_recpt_method
            SELECT arm.attribute2
                  ,arm.name arm_name
                  ,arc.name arc_name
                  ,arm.receipt_method_id
                  ,arm.attribute1
              FROM ar_receipt_methods arm, ar_receipt_classes arc
             WHERE arm.receipt_class_id = arc.receipt_class_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_recpt_method,1', g_start);

        EXECUTE IMMEDIATE ' INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cash_rpts_acrag
            SELECT apsa.cash_receipt_id
                  ,apsa.payment_schedule_id
                  ,apsa.discount_original
                  ,apsa.org_id
                  ,apsa.amount_due_remaining
                  ,apsa.class
                  ,apsa.due_date
                  ,apsa.customer_id
                  ,apsa.receipt_number
                  ,apsa.receipt_date
                  ,apsa.customer_site_use_id
                  ,apsa.TYPE
                  ,apsa.pay_from_customer
                  ,apsa.receipt_method_id
                  ,arm.attribute2 arm_attribute2
                  ,arm.arm_name arm_name
                  ,arm.arc_name arc_name
                  ,arm.attribute1 arm_attribute1
              FROM xxwc.AREXTI##tmp_rcpt_pshd_acra apsa, xxwc.AREXTI##tmp_recpt_method arm
             WHERE apsa.receipt_method_id = arm.receipt_method_id(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cash_rpts_acrag,2', g_start);

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.AREXTI##tmp_cash_rpts_acrag ADD(
bill_to_account_number VARCHAR2(128),
bill_to_account_name VARCHAR2(512),
bill_to_address1 VARCHAR2(240), -- bill_to_address1 VARCHAR2(128) -- Ver#1.3 Commented and added this field by Pattabhi Avula on 26-May-2017 for TMS#20170526-00002(Prod Issue)
bill_to_address2 VARCHAR2(240),  -- bill_to_address2 VARCHAR2(128)  -- Ver#1.3 Commented and added this field by Pattabhi Avula on 26-May-2017 for TMS#20170526-00002(Prod Issue)
bill_to_city VARCHAR2(128),
bill_to_state VARCHAR2(32),
bill_to_postal_code VARCHAR2(32),
bill_to_cust_account_id NUMBER,
bill_to_primary_salesrep_id NUMBER,
bill_to_attribute6 VARCHAR2(128))';

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cash_rpts_acrag'
                                                                  ,'AREXTI##tmp_cash_rcpts_acragl');

        EXECUTE IMMEDIATE ' INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cash_rcpts_acragl
            SELECT a.cash_receipt_id
                  ,a.payment_schedule_id
                  ,a.discount_original
                  ,a.org_id
                  ,a.amount_due_remaining
                  ,a.class
                  ,a.due_date
                  ,a.customer_id
                  ,a.receipt_number
                  ,a.receipt_date
                  ,a.customer_site_use_id
                  ,a.TYPE
                  ,a.pay_from_customer
                  ,a.receipt_method_id
                  ,a.arm_attribute2
                  ,a.arm_name
                  ,a.arc_name
                  ,a.arm_attribute1
                  ,bill_to.account_number bill_to_account_number
                  ,bill_to.account_name bill_to_account_name
                  ,bill_to.address1 bill_to_address1
                  ,bill_to.address2 bill_to_address2
                  ,bill_to.city bill_to_city
                  ,bill_to.state bill_to_state
                  ,bill_to.postal_code bill_to_postal_code
                  ,bill_to.cust_account_id bill_to_cust_account_id
                  ,bill_to.primary_salesrep_id bill_to_primary_salesrep_id
                  ,bill_to.attribute6 bill_to_attribute6
              FROM xxwc.AREXTI##tmp_cash_rpts_acrag a, apps.xxwc_customer_sites_br bill_to
             WHERE     bill_to.site_use_id(+) = a.customer_site_use_id
                   AND bill_to.cust_account_id(+) = a.customer_id
                   AND a.org_id = bill_to.org_id(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cash_rpts_acrag1,3', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cash_rcpts_acragl'
                                                                  ,'AREXTI##tmp_cash_rpts_acrag');

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.AREXTI##tmp_cash_rpts_acrag ADD(order_number VARCHAR2(124))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cash_rpts_acrag
            SELECT a.cash_receipt_id
                  ,a.payment_schedule_id
                  ,a.discount_original
                  ,a.org_id
                  ,a.amount_due_remaining
                  ,a.class
                  ,a.due_date
                  ,a.customer_id
                  ,a.receipt_number
                  ,a.receipt_date
                  ,a.customer_site_use_id
                  ,a.TYPE
                  ,a.pay_from_customer
                  ,a.receipt_method_id
                  ,a.arm_attribute2
                  ,a.arm_name
                  ,a.arc_name
                  ,a.arm_attribute1
                  ,a.bill_to_account_number
                  ,a.bill_to_account_name
                  ,a.bill_to_address1
                  ,a.bill_to_address2
                  ,a.bill_to_city
                  ,a.bill_to_state
                  ,a.bill_to_postal_code
                  ,a.bill_to_cust_account_id
                  ,a.bill_to_primary_salesrep_id
                  ,a.bill_to_attribute6
                  , (SELECT araa.application_ref_num                                                    --  ORDER_NUMBER
                       FROM apps.ar_receivable_applications araa
                      WHERE     araa.cash_receipt_id = a.cash_receipt_id
                            AND araa.application_ref_num IS NOT NULL
                            AND ROWNUM = 1)
              FROM xxwc.AREXTI##tmp_cash_rcpts_acragl a';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cash_rpts_acrag,4', g_start);
        --********************************************

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_cash_rcpts_acragl');

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_trx_salesrep');

-- EXECUTE IMMEDIATE
--            'CREATE TABLE xxwc.AREXTI##tmp_trx_salesrep
--AS
--    SELECT jrdv.source_first_name || '' '' || jrdv.source_last_name sales_rep_name, rsa.salesrep_id, rsa.org_id
--      FROM apps.jtf_rs_defresources_v jrdv, apps.ra_salesreps rsa
--     WHERE rsa.resource_id = jrdv.resource_id AND 1 = 2';

--commented above and added below for ver 1.2

        EXECUTE IMMEDIATE
            'CREATE TABLE xxwc.AREXTI##tmp_trx_salesrep
AS
    SELECT jrdv.source_first_name || '' '' || jrdv.source_last_name sales_rep_name, rsa.salesrep_id, rsa.org_id
      FROM apps.jtf_rs_defresources_v jrdv, apps.ra_salesreps rsa
     WHERE rsa.person_id = jrdv.source_id AND 1 = 2';

        --xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_trx_salesrep');

-- EXECUTE IMMEDIATE
--            'INSERT /*+APPEND*/
--              INTO  xxwc.AREXTI##tmp_trx_salesrep
--            SELECT jrdv.source_first_name || '' '' || jrdv.source_last_name sales_rep_name, rsa.salesrep_id, rsa.org_id
--              FROM apps.jtf_rs_defresources_v jrdv, apps.ra_salesreps rsa
--             WHERE rsa.resource_id = jrdv.resource_id';

--commented above and added below for ver 1.2

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_trx_salesrep
            SELECT jrdv.source_first_name || '' '' || jrdv.source_last_name sales_rep_name, rsa.salesrep_id, rsa.org_id
              FROM apps.jtf_rs_defresources_v jrdv, apps.ra_salesreps rsa
             WHERE rsa.person_id = jrdv.source_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_trx_salesrep,4', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##tmp_cash_rpts_acrag'
                                                                  ,'AREXTI##tmp_cash_rcpts_acragl');

        EXECUTE IMMEDIATE 'ALTER TABLE  xxwc.AREXTI##tmp_cash_rcpts_acragl ADD (sales_rep_name VARCHAR2(214))';

        EXECUTE IMMEDIATE
            ' INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_cash_rcpts_acragl
            SELECT a.*, st.sales_rep_name
              FROM xxwc.AREXTI##tmp_cash_rpts_acrag a, xxwc.AREXTI##tmp_trx_salesrep st
             WHERE a.bill_to_primary_salesrep_id = st.salesrep_id(+) AND a.org_id = st.org_id(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_cash_rcpts_acragl,5', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_cash_rpts_acrag');

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_cust_rcpt_file');

        -- The next several statements collect the data into a single table

        xxwc_common_tunning_helpers.deleteduplicates ('xxwc.AREXTI##tmp_cash_rcpts_acragl'
                                                     ,' cash_receipt_id,payment_schedule_id '
                                                     ,NULL);

        EXECUTE IMMEDIATE
            'UPDATE xxwc.arexti##tmp_cash_rcpts_acragl
   SET receipt_number = xxwc_common_tunning_helpers.remove_control (receipt_number)
      ,sales_rep_name = xxwc_common_tunning_helpers.remove_control (sales_rep_name)
      ,bill_to_account_name = xxwc_common_tunning_helpers.remove_control (bill_to_account_name)
      ,bill_to_address1 = xxwc_common_tunning_helpers.remove_control (bill_to_address1)
      ,bill_to_address2 = xxwc_common_tunning_helpers.remove_control (bill_to_address2)';

        COMMIT;

        EXECUTE IMMEDIATE
            'CREATE TABLE xxwc.AREXTI##tmp_cust_rcpt_file
AS
    SELECT   rpad (NVL (v.bill_to_account_number, '' ''), 20, '' '')                                      --  CUSTOMER_NUMBER
           ||rpad (NVL (v.receipt_number, '' ''), 20, '' '')                                           --  TRANSACTION_NUMBER
           || RPAD ('' '', 35, '' '')                                                                       --  SHIP_TO_NAME
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_1
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_2
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_3
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_4
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_5
           || NVL((SELECT rpad(NVL(araa.application_ref_num, '' ''),11,'' '')                                   --  ORDER_NUMBER
               FROM apps.ar_receivable_applications araa
              WHERE araa.cash_receipt_id         = v.cash_receipt_id
                AND ROWNUM                      = 1
             ), RPAD(''0'', 11, '' ''))
           || RPAD (NVL (TO_CHAR (v.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                                --  ORDER_DATE
           ||rpad (NVL (v.arm_attribute2, ''80''), 35, '' '')                                                   --  WAREHOUSE
           || RPAD ('' '', 20, '' '')                                                                      --  PAYMENT_TERMS
           || RPAD ('' '', 30, '' '')                                                                    --  SHIP_VIA_NUMBER
           || RPAD ('' '', 15, '' '')                                                                       --  ORDER_STATUS
           || RPAD ('' '', 5, '' '')                                                              --  ORDER_SHIPMENT_CARRIER
           || LPAD (''0.00'', 20, '' '')                                                                 --  DISCOUNT_AMOUNT
           || LPAD ('' '', 3, '' '')                                                                       --  DISCOUNT_DAYS
           ||rpad (NVL (v.sales_rep_name, '' ''), 10, '' '')                                                    --  SALES_MAN
           || RPAD ('' '', 30, '' '')                                                                          --  FOB_POINT
           || RPAD (NVL (TO_CHAR (v.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                                 --  SHIP_DATE
           ||rpad (NVL (v.bill_to_account_name, '' ''), 50, '' '')                                           --  BILL_TO_NAME
           ||rpad (NVL (v.bill_to_address1, '' ''), 50, '' '')                                          --  BILL_TO_ADDRESS_1
           ||rpad (NVL (TRIM (v.bill_to_address2), '' ''), 50, '' '')                                   --  BILL_TO_ADDRESS_2
           ||rpad (NVL (v.bill_to_city, '' ''), 50, '' '')                                              --  BILL_TO_ADDRESS_3
           || RPAD (NVL (v.bill_to_state, '' ''), 50, '' '')                                             --  BILL_TO_ADDRESS_4
           || RPAD (NVL (v.bill_to_postal_code, '' ''), 50, '' '')                                       --  BILL_TO_ADDRESS_5
               rec_line
          ,v.org_id        --, bill_to_account_number, bill_to_cust_account_id , receipt_number, apsa.AMOUNT_DUE_REMAINING
      FROM xxwc.AREXTI##tmp_cash_rcpts_acragl v
     WHERE arc_name = ''CONV - Receipt'' AND 1 = 2';

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.arexti##tmp_cust_rcpt_file ADD( TRX_RECEIPT_PAYMENT_ID VARCHAR2(224) )';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
      INTO  xxwc.arexti##tmp_cust_rcpt_file
    SELECT    rpad (NVL (v.bill_to_account_number, '' ''), 20, '' '')                                    --  CUSTOMER_NUMBER
           || RPAD (NVL (SUBSTR (v.receipt_number, 1, 20), '' ''), 20, '' '')                         --  TRANSACTION_NUMBER
           || RPAD ('' '', 35, '' '')                                                                       --  SHIP_TO_NAME
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_1
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_2
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_3
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_4
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_5
           || NVL ( (SELECT rpad (NVL (araa.application_ref_num, '' ''), 11, '' '')                         --  ORDER_NUMBER
                       FROM apps.ar_receivable_applications araa
                      WHERE araa.cash_receipt_id = v.cash_receipt_id AND ROWNUM = 1)
                  ,RPAD (''0'', 11, '' ''))
           || RPAD (NVL (TO_CHAR (v.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                              --  ORDER_DATE
           ||rpad (NVL (v.arm_attribute2, ''80''), 35, '' '')                                                 --  WAREHOUSE
           || RPAD ('' '', 20, '' '')                                                                      --  PAYMENT_TERMS
           || RPAD ('' '', 30, '' '')                                                                    --  SHIP_VIA_NUMBER
           || RPAD ('' '', 15, '' '')                                                                       --  ORDER_STATUS
           || RPAD ('' '', 5, '' '')                                                              --  ORDER_SHIPMENT_CARRIER
           || LPAD (''0.00'', 20, '' '')                                                                 --  DISCOUNT_AMOUNT
           || LPAD ('' '', 3, '' '')                                                                       --  DISCOUNT_DAYS
           ||rpad (NVL (v.sales_rep_name, '' ''), 10, '' '')                                                  --  SALES_MAN
           || RPAD ('' '', 30, '' '')                                                                          --  FOB_POINT
           || RPAD (NVL (TO_CHAR (v.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                               --  SHIP_DATE
           ||rpad (NVL (v.bill_to_account_name, '' ''), 50, '' '')                                         --  BILL_TO_NAME
           ||rpad (NVL (v.bill_to_address1, '' ''), 50, '' '')                                        --  BILL_TO_ADDRESS_1
           ||rpad (NVL (TRIM (v.bill_to_address2), '' ''), 50, '' '')                                 --  BILL_TO_ADDRESS_2
           ||rpad (NVL (v.bill_to_city, '' ''), 50, '' '')                                            --  BILL_TO_ADDRESS_3
           || RPAD (NVL (v.bill_to_state, '' ''), 50, '' '')                                           --  BILL_TO_ADDRESS_4
           || RPAD (NVL (v.bill_to_postal_code, '' ''), 50, '' '')                                     --  BILL_TO_ADDRESS_5
               rec_line
          ,org_id     , cash_receipt_id||payment_schedule_id
      FROM xxwc.arexti##tmp_cash_rcpts_acragl v
     WHERE v.arc_name = ''CONV - Receipt''
    UNION
    ------------------------------------------------------------------------------------------------------------------------------
    -- CONVERSION
    -- Cash Receipts, Customer# on bill_to_attribute16
    ------------------------------------------------------------------------------------------------------------------------------
    SELECT    rpad (NVL (a.bill_to_account_number, '' ''), 20, '' '')                                    --  CUSTOMER_NUMBER
           ||rpad (NVL (SUBSTR (a.receipt_number, 1, 20), '' ''), 20, '' '')                         --  TRANSACTION_NUMBER
           || RPAD ('' '', 35, '' '')                                                                       --  SHIP_TO_NAME
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_1
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_2
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_3
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_4
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_5
           || NVL ( (SELECT rpad (NVL (araa.application_ref_num, '' ''), 11, '' '')                         --  ORDER_NUMBER
                       FROM apps.ar_receivable_applications araa
                      WHERE araa.cash_receipt_id = a.cash_receipt_id AND ROWNUM = 1)
                  ,RPAD (''0'', 11, '' ''))
           || RPAD (NVL (TO_CHAR (a.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                              --  ORDER_DATE
           ||rpad (NVL (a.arm_attribute2, ''80''), 35, '' '')                                                 --  WAREHOUSE
           || RPAD ('' '', 20, '' '')                                                                      --  PAYMENT_TERMS
           || RPAD ('' '', 30, '' '')                                                                    --  SHIP_VIA_NUMBER
           || RPAD ('' '', 15, '' '')                                                                       --  ORDER_STATUS
           || RPAD ('' '', 5, '' '')                                                              --  ORDER_SHIPMENT_CARRIER
           || LPAD (''0.00'', 20, '' '')                                                                 --  DISCOUNT_AMOUNT
           || LPAD ('' '', 3, '' '')                                                                       --  DISCOUNT_DAYS
           ||rpad (NVL (a.sales_rep_name, '' ''), 10, '' '')                                                  --  SALES_MAN
           || RPAD ('' '', 30, '' '')                                                                          --  FOB_POINT
           || RPAD (NVL (TO_CHAR (a.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                               --  SHIP_DATE
           ||rpad (NVL (a.bill_to_account_name, '' ''), 50, '' '')                                         --  BILL_TO_NAME
           ||rpad (NVL (a.bill_to_address1, '' ''), 50, '' '')                                        --  BILL_TO_ADDRESS_1
           ||rpad (NVL (TRIM (a.bill_to_address2), '' ''), 50, '' '')                                 --  BILL_TO_ADDRESS_2
           ||rpad (NVL (a.bill_to_city, '' ''), 50, '' '')                                            --  BILL_TO_ADDRESS_3
           || RPAD (NVL (a.bill_to_state, '' ''), 50, '' '')                                           --  BILL_TO_ADDRESS_4
           || RPAD (NVL (a.bill_to_postal_code, '' ''), 50, '' '')                                     --  BILL_TO_ADDRESS_5
               rec_line
          ,a.org_id      , cash_receipt_id||payment_schedule_id
      FROM xxwc.arexti##tmp_cash_rcpts_acragl a, xxwc.xxwc_armast_getpaid_dump_tbl dmp
     WHERE     NVL (a.arc_name, ''a'') = ''CONV - Receipt''
           AND TO_CHAR (a.receipt_number) = dmp.invno
           AND a.bill_to_cust_account_id = pay_from_customer
           AND a.bill_to_account_number != dmp.custno
           AND a.bill_to_attribute6 = dmp.custno
    UNION
    ------------------------------------------------------------------------------------------------------------------------------
    -- NON-CONVERSION
    -- Cash Receipts, ReceiptMethod != ''WC PRISM Lockbox''
    ------------------------------------------------------------------------------------------------------------------------------
    SELECT   rpad (NVL (v.bill_to_account_number, '' ''), 20, '' '')                                    --  CUSTOMER_NUMBER
           ||rpad (NVL (SUBSTR (v.receipt_number, 1, 20), '' ''), 20, '' '')                         --  TRANSACTION_NUMBER
           || RPAD ('' '', 35, '' '')                                                                       --  SHIP_TO_NAME
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_1
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_2
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_3
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_4
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_5
           || NVL ( (SELECT rpad (NVL (araa.application_ref_num, '' ''), 11, '' '')                         --  ORDER_NUMBER
                       FROM apps.ar_receivable_applications araa
                      WHERE araa.cash_receipt_id = v.cash_receipt_id AND ROWNUM = 1)
                  ,RPAD (''0'', 11, '' ''))
           || RPAD (NVL (TO_CHAR (v.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                              --  ORDER_DATE
           ||rpad (NVL (v.arm_attribute2, ''80''), 35, '' '')                                                 --  WAREHOUSE
           || RPAD ('' '', 20, '' '')                                                                      --  PAYMENT_TERMS
           || RPAD ('' '', 30, '' '')                                                                    --  SHIP_VIA_NUMBER
           || RPAD ('' '', 15, '' '')                                                                       --  ORDER_STATUS
           || RPAD ('' '', 5, '' '')                                                              --  ORDER_SHIPMENT_CARRIER
           || LPAD (''0.00'', 20, '' '')                                                                 --  DISCOUNT_AMOUNT
           || LPAD ('' '', 3, '' '')                                                                       --  DISCOUNT_DAYS
           ||rpad (NVL (v.sales_rep_name, '' ''), 10, '' '')                                                  --  SALES_MAN
           || RPAD ('' '', 30, '' '')                                                                          --  FOB_POINT
           || RPAD (NVL (TO_CHAR (v.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                               --  SHIP_DATE
           ||rpad (NVL (v.bill_to_account_name, '' ''), 50, '' '')                                         --  BILL_TO_NAME
           ||rpad (NVL (v.bill_to_address1, '' ''), 50, '' '')                                        --  BILL_TO_ADDRESS_1
           ||rpad (NVL (TRIM (v.bill_to_address2), '' ''), 50, '' '')                                 --  BILL_TO_ADDRESS_2
           ||rpad (NVL (v.bill_to_city, '' ''), 50, '' '')                                            --  BILL_TO_ADDRESS_3
           || RPAD (NVL (v.bill_to_state, '' ''), 50, '' '')                                           --  BILL_TO_ADDRESS_4
           || RPAD (NVL (v.bill_to_postal_code, '' ''), 50, '' '')                                     --  BILL_TO_ADDRESS_5
               rec_line
          ,v.org_id     , cash_receipt_id||payment_schedule_id
      FROM xxwc.arexti##tmp_cash_rcpts_acragl v
     WHERE NVL (v.arc_name, ''a'') <> ''CONV - Receipt'' AND NVL (v.arm_name, ''a'') <> ''WC PRISM Lockbox''
    UNION
    ------------------------------------------------------------------------------------------------------------------------------
    -- NON-CONVERSION
    -- Cash Receipts, ReceiptMethod = ''WC PRISM Lockbox''
    ------------------------------------------------------------------------------------------------------------------------------
    SELECT    rpad (NVL (v.bill_to_account_number, '' ''), 20, '' '')                                    --  CUSTOMER_NUMBER
           ||rpad (NVL (SUBSTR (v.receipt_number, 1, 19) || ''-P'', '' ''), 20, '' '')                 --  TRANSACTION_NUMBER
           || RPAD ('' '', 35, '' '')                                                                       --  SHIP_TO_NAME
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_1
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_2
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_3
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_4
           || RPAD ('' '', 50, '' '')                                                                  --  SHIP_TO_ADDRESS_5
           || NVL ( (SELECT rpad (NVL (araa.application_ref_num, '' ''), 11, '' '')                         --  ORDER_NUMBER
                       FROM apps.ar_receivable_applications araa
                      WHERE araa.cash_receipt_id = v.cash_receipt_id AND ROWNUM = 1)
                  ,RPAD (''0'', 11, '' ''))
           || RPAD (NVL (TO_CHAR (v.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                              --  ORDER_DATE
           ||rpad (NVL (v.arm_attribute2, ''80''), 35, '' '')                                                 --  WAREHOUSE
           || RPAD ('' '', 20, '' '')                                                                      --  PAYMENT_TERMS
           || RPAD ('' '', 30, '' '')                                                                    --  SHIP_VIA_NUMBER
           || RPAD ('' '', 15, '' '')                                                                       --  ORDER_STATUS
           || RPAD ('' '', 5, '' '')                                                              --  ORDER_SHIPMENT_CARRIER
           || LPAD (''0.00'', 20, '' '')                                                                 --  DISCOUNT_AMOUNT
           || LPAD ('' '', 3, '' '')                                                                       --  DISCOUNT_DAYS
           ||rpad (NVL (v.sales_rep_name, '' ''), 10, '' '')                                                  --  SALES_MAN
           || RPAD ('' '', 30, '' '')                                                                          --  FOB_POINT
           || RPAD (NVL (TO_CHAR (v.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                               --  SHIP_DATE
           ||rpad (NVL (v.bill_to_account_name, '' ''), 50, '' '')                                         --  BILL_TO_NAME
           ||rpad (NVL (v.bill_to_address1, '' ''), 50, '' '')                                        --  BILL_TO_ADDRESS_1
           ||rpad (NVL (TRIM (v.bill_to_address2), '' ''), 50, '' '')                                 --  BILL_TO_ADDRESS_2
           ||rpad (NVL (v.bill_to_city, '' ''), 50, '' '')                                            --  BILL_TO_ADDRESS_3
           || RPAD (NVL (v.bill_to_state, '' ''), 50, '' '')                                           --  BILL_TO_ADDRESS_4
           || RPAD (NVL (v.bill_to_postal_code, '' ''), 50, '' '')                                     --  BILL_TO_ADDRESS_5
               rec_line
          ,org_id cash_receipt_id  , cash_receipt_id||payment_schedule_id
      FROM xxwc.arexti##tmp_cash_rcpts_acragl v
     WHERE NVL (v.arc_name, ''a'') <> ''CONV - Receipt'' AND v.arm_name = ''WC PRISM Lockbox''';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_rcpt_file,6', g_start);
        xxwc_common_tunning_helpers.deleteduplicates ('xxwc.arexti##tmp_cust_rcpt_file'
                                                     ,'TRX_RECEIPT_PAYMENT_ID'
                                                     ,NULL);
        -- "PAYDOC" is Rasikha's nickname for Get Paid
        -- The table AREXTI##tmp_PAYDOC_FILE contains the text output that
        -- will go into the file

        -- These statements move data into the AREXTI##tmp_PAYDOC_FILE table
        xxwc_common_tunning_helpers.elapsed_time (
            'GP'
           ,'xxwc_common_tunning_helpers.deleteduplicates xxwc.arexti##tmp_cust_rcpt_file,6'
           ,g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##TMP_PAYDOC_FILE');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.AREXTI##tmp_PAYDOC_FILE
AS
    SELECT a.rec_line, a.org_id,A.TRX_RECEIPT_PAYMENT_ID
      FROM xxwc.AREXTI##tmp_cust_rcpt_file a
     WHERE 1 = 2';

        --xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_PAYDOC_FILE');

        EXECUTE IMMEDIATE ' INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_PAYDOC_FILE
            SELECT a.rec_line, a.org_id,A.TRX_RECEIPT_PAYMENT_ID
              FROM xxwc.AREXTI##tmp_cust_trx_file a';

        COMMIT;

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_PAYDOC_FILE
            SELECT a.rec_line, a.org_id,A.TRX_RECEIPT_PAYMENT_ID
              FROM xxwc.AREXTI##tmp_cust_rcpt_file a';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##tmp_PAYDOC_FILE,7', g_start);
        xxwc_common_tunning_helpers.deleteduplicates ('xxwc.AREXTI##tmp_PAYDOC_FILE', 'TRX_RECEIPT_PAYMENT_ID', NULL);
        xxwc_common_tunning_helpers.elapsed_time (
            'GP'
           ,'xxwc_common_tunning_helpers.deleteduplicates xxwc.AREXTI##tmp_PAYDOC_FILE,7'
           ,g_start);
        -- Drop these huge tables now that data is in AREXTI##tmp_PAYDOC_FILE
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_cust_trx_file');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_cust_rcpt_file');

        --************************************************************

        -- Set the default file name. UC4 will always provide one
        IF p_file_name IS NULL
        THEN
            l_file_name := 'new_wc_arexti##' || TO_CHAR (SYSDATE, 'YYYYMMDD') || '.txt';
        ELSE
            l_file_name := p_file_name;
        END IF;

        -- Writes the file!!
        create_file ('xxwc.AREXTI##tmp_PAYDOC_FILE'                                     --p_view_name        IN VARCHAR2
                    ,p_directory_name                                                   --p_directory_path   IN VARCHAR2
                    ,l_file_name                                                        --p_file_name        IN VARCHAR2
                    ,p_org_id                                                            --p_org_id           IN NUMBER)
                             );
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'create_file (xxwc.AREXTI##tmp_PAYDOC_FILE ,7', g_start);
        -- Create the log table for the log file
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_getpaid_log');

        -- Go back to the original tables to check the counts
        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.AREXTI##tmp_getpaid_log
AS
    SELECT *
      FROM (SELECT org_id
                  ,bill_to_account_number
                  ,bill_to_cust_account_id
                  ,trx_number
                  ,amount_due_remaining
              FROM xxwc.AREXTI##tmp_cust_trxg
            UNION ALL
            SELECT org_id
                  ,bill_to_account_number
                  ,bill_to_cust_account_id
                  ,receipt_number
                  ,amount_due_remaining
              FROM xxwc.AREXTI##tmp_cash_rcpts_acragl)
     WHERE 1 = 2';

--        BEGIN
--            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'AREXTI##tmp_getpaid_log');
--        END;

        EXECUTE IMMEDIATE
               'INSERT /*+APPEND*/
              INTO  xxwc.AREXTI##tmp_getpaid_log
            SELECT *
              FROM (SELECT org_id
                          ,bill_to_account_number
                          ,bill_to_cust_account_id
                          ,trx_number
                          ,amount_due_remaining
                      FROM xxwc.AREXTI##tmp_cust_trxg
                      where org_id='
            || p_org_id
            || ' UNION ALL
                    SELECT org_id
                          ,bill_to_account_number
                          ,bill_to_cust_account_id
                          ,receipt_number
                          ,amount_due_remaining
                      FROM xxwc.AREXTI##tmp_cash_rcpts_acragl
                       where org_id='
            || p_org_id
            || ')';

        COMMIT;

        BEGIN
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_getpaid_log_total');
        END;

        -- Totals for the log file
        EXECUTE IMMEDIATE
            'CREATE TABLE xxwc.AREXTI##tmp_getpaid_log_total
AS
      SELECT COUNT (1) RECORD_COUNT, COUNT (1) || ''_'' || SUM (amount_due_remaining) rec_line, org_id
        FROM xxwc.AREXTI##tmp_getpaid_log
    GROUP BY org_id';

        BEGIN
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'AREXTI##tmp_getpaid_log');
        END;

        -- default file name
        --SELECT TO_CHAR(SYSDATE,'DD_MM_YYYY_HH24') FROM DUAL
        IF p_log_file_name IS NULL
        THEN
            l_log_file_name := 'new_wc_AREXTI##_log' || TO_CHAR (SYSDATE, 'YYYYMMDD') || '.txt';
        ELSE
            l_log_file_name := p_log_file_name;
        END IF;

        DECLARE
            v_rtn_cd        INTEGER;

            l_fexists       BOOLEAN;
            l_file_length   NUMBER;
            l_block_size    INTEGER;
            l_file_handle   UTL_FILE.file_type;

            TYPE ref_cur IS REF CURSOR;

            view_cur        ref_cur;
            view_rec        xxwc_ob_common_pkg.xxcus_ob_file_rec;
        BEGIN
            -- Get file attributes directly from the file

            UTL_FILE.fgetattr (p_directory_name
                              ,l_file_name
                              ,l_fexists
                              ,l_file_length
                              ,l_block_size);
            l_file_handle :=
                UTL_FILE.fopen (p_directory_name
                               ,l_log_file_name
                               ,'w'
                               ,32767);

            UTL_FILE.put_line (l_file_handle, 'FILE: ' || l_file_name || CHR (10) || CHR (13));

            v_query_str := 'SELECT record_count
                        FROM xxwc.arexti##tmp_getpaid_log_total
                       WHERE org_id = :p_org_id';

            EXECUTE IMMEDIATE v_query_str INTO v_num_of_records USING p_org_id;

            -- write log file!!
            UTL_FILE.put_line (l_file_handle, 'RECORDS: ' || v_num_of_records || CHR (13) || CHR (10));

            UTL_FILE.put_line (l_file_handle, 'BYTES: ' || l_file_length || CHR (10) || CHR (13));

            UTL_FILE.fclose (l_file_handle);
            xxwc_common_tunning_helpers.elapsed_time ('GP', 'the end,9', g_start);
        END;

        --************************************************************
        --data still in tables
        /*
        SELECT *
          FROM xxwc.AREXTI##tmp_cash_rcpts_acragl select
         * from XXWC.AREXTI##tmp_cust_trxg
        select * from xxwc.AREXTI##tmp_PAYDOC_FILE
        select * from xxwc.AREXTI##tmp_getpaid_log_TOTAL*/
        --***************************************************

        /*TRUNCATE TABLE xxwc.xxwc_getpaid_AREXTI##_original;

        --INSERT                                                                                                        /*append*/
        --  INTO  xxwc.xxwc_getpaid_AREXTI##_original
        -- SELECT *
        --   FROM apps.xxwc_getpaid_AREXTI##_v
        /*
        DECLARE
            v_rtn_cd    INTEGER;
            file_name   VARCHAR2 (124);
        BEGIN
            file_name := 'ORIGINAL_' || TO_CHAR (SYSDATE, 'DD_MM_YYYY_HH24') || '.txt';
            xxwc_ar_getpaid_files_ob_pkg.sp_extract_data_to_file (
                'SELECT REC_LINE FROM xxwc.xxwc_getpaid_AREXTI##_original where org_id=162 order by 1' --p_query       IN     VARCHAR2
               ,NULL                                                                             --p_col_names   IN     VARCHAR2
               ,''                                                                   --p_separator   IN     VARCHAR2 DEFAULT ';'
               ,p_directory_name                                                                 --p_dir         IN     VARCHAR2
               ,'original.TXT'                                                                             --    IN     VARCHAR2
               ,1000                                                                --p_limit       IN     INTEGER DEFAULT 10000
               ,NULL                                                                 --p_unicode     IN     VARCHAR2 DEFAULT 'N'
               ,v_rtn_cd                                                                         --p_rtn_cd         OUT INTEGER)
                        );
        END;*/

        -- Housekeeping. Clean up and drop all temp tables that are NOT
        -- needed by ARMAST

        -- xxwc.arexti##tmp_cust_trxgx, --armast
        --xxwc.arexti##tmp_cash_rcpts_acragx, --armast
        --XXWC.arexti##tmp_armast_fileX, --armast file
        --xxwc.AREXTI##tmp_cust_trxg,--arexti
        --xxwc.AREXTI##tmp_cash_rcpts_acragl, --arexti
        --xxwc.AREXTI##tmp_cust_trx_file, --arexti file
        --xxwc.artran##tmp_cust_trxgy ,--artran
        --xxwc.artran##tmp_trxgz_filex--artran file
        BEGIN
            FOR r
                IN (  SELECT *
                        FROM all_objects
                       WHERE     object_type = 'TABLE'
                             AND object_name LIKE 'AREXTI##TMP%'
                             AND owner = 'XXWC'
                             AND object_name NOT IN
                                     ('AREXTI##TMP_CUST_TRXGX'
                                     ,                                                                          --ARMAST
                                      'AREXTI##TMP_CASH_RCPTS_ACRAGX'
                                     ,                                                                          --ARMAST
                                      'AREXTI##TMP_ARMAST_FILEX'
                                     ,                                                                     --ARMAST FILE
                                      'AREXTI##TMP_CUST_TRXG'
                                     ,                                                                          --AREXTI
                                      'AREXTI##TMP_CASH_RCPTS_ACRAGL'
                                     ,                                                                          --AREXTI
                                      'AREXTI##TMP_PAYDOC_FILE'
                                     ,                                                                     --AREXTI FILE
                                      'ARTRAN##TMP_CUST_TRXGY'
                                     ,                                                                          --ARTRAN
                                      'ARTRAN##TMP_TRXGZ_FILEX'                                            --ARTRAN FILE
                                                               )
                    ORDER BY created DESC)
            LOOP
                xxwc_common_tunning_helpers.drop_temp_table (r.owner, r.object_name);
            END LOOP;
        END;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'xxwc_arexti '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();
            DBMS_OUTPUT.put_line (l_error_message);
            xxwc_common_tunning_helpers.elapsed_time ('GP', SUBSTR (l_error_message, 1, 500), g_start);
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => 'xxwc_ar_getpaid_files_ob_pkg.CREATE_AREXTI_FILE'
               ,p_calling             => 'xxwc_ar_getpaid_files_ob_pkg.CREATE_AREXTI_FILE'
               ,p_request_id          => fnd_global.conc_request_id
               ,p_ora_error_msg       => l_error_message
               ,p_error_desc          => 'Error running Outbound File AREXTI Package with Program Error Exception'
               ,p_distribution_list   => pl_dflt_email
               ,p_module              => 'AR');
            p_retcode := 2;
            p_errbuf := l_error_message;
    END;

    --*************************************************************
    -- This procedure builds ARMAST temp tables and goes step by step
    -- to load its data for each field. This is based on the original
    -- views, which were based on the MD50. See the views and/or MD50
    -- for more information.
    PROCEDURE populate_armast_fields
    IS
    BEGIN
        -- AREXTI##TMP_CUST_TRXGM
        -- AREXTI##TMP_CUST_TRXG
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXG'
                                                                  ,'AREXTI##TMP_CUST_TRXGM');

        EXECUTE IMMEDIATE 'alter table XXWC.AREXTI##TMP_CUST_TRXGM add (cust_trx_type_id number)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND  */
      INTO  xxwc.AREXTI##TMP_CUST_TRXGM
    SELECT a.*, b.cust_trx_type_id
      FROM xxwc.AREXTI##TMP_CUST_TRXG a, apps.ra_customer_trx b
     WHERE a.customer_trx_id = b.customer_trx_id AND a.org_id = b.org_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##TMP_CUST_TRXGM,1', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGM'
                                                                  ,'AREXTI##TMP_CUST_TRXGX');

        EXECUTE IMMEDIATE 'alter table xxwc.AREXTI##TMP_CUST_TRXGX add (rctt_name varchar2(124))';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND  */
      INTO  xxwc.AREXTI##TMP_CUST_TRXGX
    SELECT a.*, b.name rctt_name
      FROM xxwc.AREXTI##TMP_CUST_TRXGM a, apps.ra_cust_trx_types b
     WHERE a.cust_trx_type_id = b.cust_trx_type_id AND a.org_id = b.org_id';

        COMMIT;
        --*********************************create new
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##TMP_CUST_TRXGX,2', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGX'
                                                                  ,'AREXTI##TMP_CUST_TRXGM');

        EXECUTE IMMEDIATE 'UPDATE xxwc.AREXTI##TMP_CUST_TRXGX
   SET bill_to_cust_account_id = bill_to_customer_id
 WHERE bill_to_cust_account_id IS NULL';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'update xxwc.AREXTI##TMP_CUST_TRXGX,3', g_start);

        EXECUTE IMMEDIATE
            'ALTER TABLE xxwc.arexti##tmp_cust_trxgm ADD(hcp_credit_hold VARCHAR2(32), hcp_attribute2 VARCHAR2(12),hcp_account_status VARCHAR2(64))';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND  */
      INTO  xxwc.AREXTI##TMP_CUST_TRXGM
    SELECT a.*
          ,hcp.credit_hold
          ,hcp.attribute2
          ,hcp.account_status
      FROM xxwc.AREXTI##TMP_CUST_TRXGX a, apps.hz_customer_profiles hcp
     WHERE a.bill_to_cust_account_id = hcp.cust_account_id AND hcp.site_use_id IS NULL';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##TMP_CUST_TRXGM,4', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGM'
                                                                  ,'AREXTI##TMP_CUST_TRXGX');

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.arexti##tmp_cust_trxgx ADD( hcp_s_credit_hold VARCHAR2(12))';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND  */
      INTO  xxwc.AREXTI##TMP_CUST_TRXGX
    SELECT a.*, hcp_s.credit_hold
      FROM xxwc.AREXTI##TMP_CUST_TRXGM a, apps.hz_customer_profiles hcp_s
     WHERE a.ship_to_site_use_id = hcp_s.site_use_id(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##TMP_CUST_TRXGX,5', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGX'
                                                                  ,'AREXTI##TMP_CUST_TRXGM');

        EXECUTE IMMEDIATE
            'ALTER TABLE xxwc.arexti##tmp_cust_trxgm ADD(flv_lookup_code VARCHAR2(512), flv_attribute1  VARCHAR2(512), flv_attribute2  VARCHAR2(512))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND  */
      INTO  xxwc.AREXTI##TMP_CUST_TRXGM
    SELECT a.*
          ,flv.lookup_code
          ,flv.attribute1
          ,flv.attribute2
      FROM xxwc.AREXTI##TMP_CUST_TRXGX a, apps.fnd_lookup_values_vl flv
     WHERE flv.lookup_type = ''XXWC_GETPAID_REMIT_TO_CODES'' AND NVL (hcp_attribute2, ''2'') = flv.lookup_code(+)';

        COMMIT;
        --*********************************
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##TMP_CUST_TRXGM,6', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGM'
                                                                  ,'AREXTI##TMP_CUST_TRXGX');

        --***********************************

        EXECUTE IMMEDIATE
            'ALTER TABLE xxwc.arexti##tmp_cust_trxgx ADD(tax_original NUMBER, amount_due_original NUMBER,freight_original NUMBER,
amount_line_items_original NUMBER)';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND  */
      INTO  xxwc.AREXTI##TMP_CUST_TRXGX
    SELECT a.*, apsa.tax_original, apsa.amount_due_original,apsa.freight_original,apsa.amount_line_items_original
      FROM xxwc.AREXTI##TMP_CUST_TRXGM a, apps.ar_payment_schedules apsa
     WHERE a.customer_trx_id = apsa.customer_trx_id AND apsa.org_id = a.org_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##TMP_CUST_TRXGX,7', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGX'
                                                                  ,'AREXTI##TMP_CUST_TRXGM');

        EXECUTE IMMEDIATE
            'ALTER TABLE xxwc.arexti##tmp_cust_trxgm ADD(interface_header_attribute11 VARCHAR(124),bill_to_contact_id NUMBER,
interface_header_attribute9 VARCHAR(124), interface_header_attribute10 VARCHAR(124))';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgm
    SELECT a.*
          ,trx.interface_header_attribute11
          ,trx.bill_to_contact_id
          ,trx.interface_header_attribute9
          ,trx.interface_header_attribute10
      FROM xxwc.arexti##tmp_cust_trxgx a, apps.ra_customer_trx trx
     WHERE a.customer_trx_id = trx.customer_trx_id AND trx.org_id = a.org_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.AREXTI##TMP_CUST_TRXGM,8', g_start);
       
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGM'
                                                                  ,'AREXTI##TMP_CUST_TRXGX');

 
        EXECUTE IMMEDIATE 'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgx
    SELECT a.* 
      FROM xxwc.arexti##tmp_cust_trxgm a';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgx,13', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGX'
                                                                  ,'AREXTI##TMP_CUST_TRXGM');

        EXECUTE IMMEDIATE 'ALTER TABLE   xxwc.AREXTI##TMP_CUST_TRXGM ADD(overall_credit_limit NUMBER)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgM
    SELECT a.*,  NVL(hcpa_s.overall_credit_limit, 0)
      FROM xxwc.arexti##tmp_cust_trxgX a,apps.hz_cust_profile_amts hcpa_s
     WHERE   A.bill_to_site_use_id = hcpa_s.site_use_id(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgM,14', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGM'
                                                                  ,'AREXTI##TMP_CUST_TRXGX');

        EXECUTE IMMEDIATE 'ALTER TABLE   xxwc.AREXTI##TMP_CUST_TRXGX ADD(FLEXFIELD15 VARCHAR2(124))';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgx
    SELECT a.* ,xxwc_ar_getpaid_ob_pkg.get_lien_by_state (A.bill_to_site_use_id)
      FROM xxwc.arexti##tmp_cust_trxgm a';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgx,15', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGX'
                                                                  ,'AREXTI##TMP_CUST_TRXGM');

        EXECUTE IMMEDIATE 'ALTER TABLE   xxwc.AREXTI##TMP_CUST_TRXGM ADD(FLEXFIELD13 VARCHAR2(124))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgm
    SELECT rcta.*
          ,CASE
               WHEN rcta.source_from IN (''ORDER MANAGEMENT'', ''STANDARD OM SOURCE'', ''REPAIR OM SOURCE'')
               THEN
                   (SELECT LPAD (ood.organization_code, 3, ''0'')
                      FROM apps.oe_order_headers ooha, apps.org_organization_definitions ood
                     WHERE     ooha.order_number = NVL (rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id = ood.organization_id
                           AND ROWNUM = 1)
               WHEN rcta.source_from = ''PRISM''
               THEN
                   LPAD (NVL (rcta.interface_header_attribute7, ''080''), 3, ''0'')
               ELSE
                   ''080''
           END
      FROM xxwc.arexti##tmp_cust_trxgx rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgm,16', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGM'
                                                                  ,'AREXTI##TMP_CUST_TRXGX');

        EXECUTE IMMEDIATE 'ALTER TABLE   xxwc.AREXTI##TMP_CUST_TRXGX ADD(SALES_AREA VARCHAR2(124))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgx
    SELECT rcta.*
          ,CASE
               WHEN rcta.source_from IN (''ORDER MANAGEMENT'', ''STANDARD OM SOURCE'', ''REPAIR OM SOURCE'')
               THEN
                   (SELECT LPAD (ood.organization_code, 3, ''0'')
                      FROM apps.oe_order_headers ooha, apps.org_organization_definitions ood
                     WHERE     ooha.order_number = NVL (rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id = ood.organization_id
                           AND ROWNUM = 1)
               WHEN rcta.source_from = ''PRISM''
               THEN
                   LPAD (rcta.interface_header_attribute7, 3, ''0'')
               ELSE
                   '' ''
           END
      FROM xxwc.arexti##tmp_cust_trxgm rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgX,17', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGX'
                                                                  ,'AREXTI##TMP_CUST_TRXGM');

        EXECUTE IMMEDIATE 'ALTER TABLE   xxwc.AREXTI##TMP_CUST_TRXGM ADD(FLEXFIELD1 VARCHAR2(124))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgm
    SELECT rcta.*
          ,CASE
               WHEN rcta.source_from IN (''ORDER MANAGEMENT'', ''STANDARD OM SOURCE'', ''REPAIR OM SOURCE'')
               THEN
                   (SELECT DECODE (COUNT (1), 0, ''INVOICE'', ''RENTAL INVOICE'')
                      FROM apps.oe_order_headers ooha, apps.oe_transaction_types_tl ott
                     WHERE     ooha.order_number = NVL (rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.order_type_id = ott.transaction_type_id
                           AND ott.name IN (''WC SHORT TERM RENTAL'', ''WC LONG TERM RENTAL'')
                           AND ROWNUM = 1)
               WHEN rcta.source_from = ''PRISM''
               THEN
                   rcta.interface_header_attribute8
               ELSE
                   ''INVOICE''
           END
      FROM xxwc.arexti##tmp_cust_trxgx rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgM,18', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGM'
                                                                  ,'AREXTI##TMP_CUST_TRXGX');

        EXECUTE IMMEDIATE 'ALTER TABLE   xxwc.AREXTI##TMP_CUST_TRXGX ADD(FLEXFIELD5 VARCHAR2(124))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgx
    SELECT rcta.*
          , CASE
                        WHEN rcta.source_from IN (''ORDER MANAGEMENT'', ''STANDARD OM SOURCE'', ''REPAIR OM SOURCE'')
                        THEN
                            (SELECT rpad (NVL (houv.location_code, '' ''), 35, '' '')
                               FROM apps.oe_order_headers ooha, apps.hr_organization_units_v houv
                              WHERE     ooha.order_number = NVL (rcta.interface_header_attribute1, rcta.ct_reference)
                                    AND ooha.ship_from_org_id = houv.organization_id
                                    AND ROWNUM = 1)
                        WHEN rcta.source_from = ''PRISM''
                        THEN
                            (SELECT rpad (NVL (hla.location_code, '' ''), 35, '' '')
                               FROM apps.hr_locations hla
                              WHERE     SUBSTR (location_code, 1, INSTR (location_code, ''-'') - 2) =
                                            LPAD (rcta.interface_header_attribute7, 3, ''0'')
                                    AND ROWNUM = 1)
                        ELSE
                            '' ''
                    END
      FROM xxwc.arexti##tmp_cust_trxgm rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgx,19', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGX'
                                                                  ,'AREXTI##TMP_CUST_TRXGM');

        EXECUTE IMMEDIATE 'ALTER TABLE   xxwc.AREXTI##TMP_CUST_TRXGM ADD(FLEXFIELD_ALL VARCHAR2(264))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgm
    SELECT rcta.*
          ,CASE
               WHEN rcta.source_from IN (''ORDER MANAGEMENT'', ''STANDARD OM SOURCE'', ''REPAIR OM SOURCE'')
               THEN
                   (SELECT   rpad (NVL (houv.telephone_number_1, '' ''), 50, '' '')                        --    FLEXFIELD6
                           ||rpad (NVL (houv.address_line_1, '' ''), 50, '' '')                            --    FLEXFIELD7
                           ||rpad (NVL (houv.town_or_city || '','' || houv.region_2 || '' '' || houv.postal_code, '' '')
                                   ,50
                                   ,'' '')                                                                --    FLEXFIELD8
                      FROM apps.oe_order_headers ooha, apps.hr_organization_units_v houv
                     WHERE     ooha.order_number = NVL (rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id = houv.organization_id
                           AND ROWNUM = 1)
               WHEN rcta.source_from = ''PRISM''
               THEN
                   (SELECT   rpad (NVL (hla.telephone_number_1, '' ''), 50, '' '')                         --    FLEXFIELD6
                           ||rpad (NVL (hla.address_line_1, '' ''), 50, '' '')                             --    FLEXFIELD7
                           ||rpad (NVL (hla.town_or_city || '','' || hla.region_2 || '' '' || hla.postal_code, '' '')
                                   ,50
                                   ,'' '')                                                                --    FLEXFIELD8
                      FROM apps.hr_locations hla, apps.hr_all_organization_units hou
                     WHERE     SUBSTR (location_code, 1, INSTR (location_code, ''-'') - 2) =
                                   LPAD (rcta.interface_header_attribute7, 3, ''0'')
                           AND SYSDATE BETWEEN hou.date_from AND NVL (hou.date_to, SYSDATE + 1)
                           AND hla.location_id = hou.location_id
                           AND ROWNUM = 1)
               ELSE
                   '' ''
           END
      FROM xxwc.arexti##tmp_cust_trxgx rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgm,20', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGM'
                                                                  ,'AREXTI##TMP_CUST_TRXGX');

        EXECUTE IMMEDIATE 'ALTER TABLE   xxwc.AREXTI##TMP_CUST_TRXGX ADD(FLEXFIELD2 VARCHAR2(164))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgx
    SELECT rcta.*
          ,CASE
               WHEN rcta.source_from IN (''ORDER MANAGEMENT'', ''STANDARD OM SOURCE'', ''REPAIR OM SOURCE'')
               THEN
                   (SELECT xxwc_mv_routines_pkg.get_user_employee (ooha.created_by)
                      FROM apps.oe_order_headers ooha
                     WHERE ooha.order_number = NVL (rcta.interface_header_attribute1, rcta.ct_reference) AND ROWNUM = 1)
               WHEN rcta.source_from = ''PRISM''
               THEN
                   rcta.interface_header_attribute10
               ELSE
                   NULL
           END
      FROM xxwc.arexti##tmp_cust_trxgm rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgx,21', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGX'
                                                                  ,'AREXTI##TMP_CUST_TRXGM');

        EXECUTE IMMEDIATE 'ALTER TABLE   xxwc.AREXTI##TMP_CUST_TRXGM ADD(FLEXFIELD_3 VARCHAR2(264))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgm
    SELECT rcta.*
          ,CASE
               WHEN rcta.source_from IN (''ORDER MANAGEMENT'', ''STANDARD OM SOURCE'', ''REPAIR OM SOURCE'')
               THEN
                   (SELECT xxwc_mv_routines_pkg.get_contact_name (ooha.invoice_to_contact_id)
                      FROM apps.oe_order_headers ooha
                     WHERE ooha.order_number = NVL (rcta.interface_header_attribute1, rcta.ct_reference) AND ROWNUM = 1)
               WHEN rcta.source_from = ''PRISM''
               THEN
                   rcta.interface_header_attribute9
               ELSE
                   xxwc_mv_routines_pkg.get_contact_name (rcta.bill_to_contact_id)
           END
      FROM xxwc.arexti##tmp_cust_trxgx rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgm,22', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'AREXTI##TMP_CUST_TRXGM'
                                                                  ,'AREXTI##TMP_CUST_TRXGX');

        EXECUTE IMMEDIATE 'ALTER TABLE   xxwc.AREXTI##TMP_CUST_TRXGX ADD(FLEXFIELD4 VARCHAR2(164))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND  */
      INTO  xxwc.arexti##tmp_cust_trxgx
    SELECT rcta.*
          ,CASE
                        WHEN rcta.source_from IN (''ORDER MANAGEMENT'', ''STANDARD OM SOURCE'', ''REPAIR OM SOURCE'')
                        THEN
                            (SELECT oola.revrec_signature
                               FROM apps.oe_order_headers ooha, apps.oe_order_lines oola
                              WHERE     ooha.order_number = NVL (rcta.interface_header_attribute1, rcta.ct_reference)
                                    AND ooha.header_id = oola.header_id
                                    AND oola.revrec_signature IS NOT NULL
                                    AND ROWNUM = 1)
                        WHEN rcta.source_from = ''PRISM''
                        THEN
                            rcta.interface_header_attribute11
                        ELSE
                            NULL
                    END
      FROM xxwc.arexti##tmp_cust_trxgm rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cust_trxgx,23', g_start);
        --***********************************************
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'arexti##tmp_cash_rcpts_acragl'
                                                                  ,'arexti##tmp_cash_rcpts_acragX');

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.arexti##tmp_cash_rcpts_acragX ADD(CREATION_DATE DATE, AMOUNT NUMBER)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
      INTO  xxwc.arexti##tmp_cash_rcpts_acragx
    SELECT a.*, ac.creation_date, AC.AMOUNT
      FROM xxwc.arexti##tmp_cash_rcpts_acragl a, apps.ar_cash_receipts ac
     WHERE a.cash_receipt_id = ac.cash_receipt_id AND a.org_id = ac.org_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cash_rcpts_acragx,24', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'arexti##tmp_cash_rcpts_acragx'
                                                                  ,'arexti##tmp_cash_rcpts_acraglX');

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.arexti##tmp_cash_rcpts_acraglx ADD(hcp_account_status  varchar2(64),
        hca_attribute6 varchar2(124))';

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
      INTO  xxwc.arexti##tmp_cash_rcpts_acraglX
    SELECT a.*, hcp.account_status,hca.attribute6
      FROM xxwc.arexti##tmp_cash_rcpts_acragx a, apps.hz_cust_accounts hca, apps.hz_customer_profiles hcp
     WHERE     hca.cust_account_id = a.customer_id
           AND hca.cust_account_id = hcp.cust_account_id
           AND hcp.site_use_id IS NULL';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cash_rcpts_acraglX,25', g_start);
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arexti##tmp_RCPT_LOC');

        EXECUTE IMMEDIATE ' CREATE TABLE xxwc.arexti##tmp_RCPT_LOC AS
SELECT SUBSTR (hla.location_code, 1, INSTR (hla.location_code, ''-'') - 2) attr2
      ,hla.location_code
      ,hla.telephone_number_1
      ,hla.address_line_1
      ,hla.town_or_city
      ,hla.region_2
      ,hla.postal_code
  FROM apps.hr_locations hla, apps.hr_all_organization_units hou
 WHERE hla.inventory_organization_id IS NOT NULL AND hla.location_id = hou.location_id
 AND SYSDATE BETWEEN hou.date_from AND NVL (hou.date_to, SYSDATE + 1)';

        EXECUTE IMMEDIATE ' ALTER TABLE xxwc.arexti##tmp_RCPT_LOC NOLOGGING';

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'CREATE TABLE xxwc.arexti##tmp_RCPT_LOC,26', g_start);

        EXECUTE IMMEDIATE
            'update XXWC.arexti##tmp_cash_rcpts_acragLx set arm_attribute2=''WCC'' where arm_attribute2 is null';

        COMMIT;
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'arexti##tmp_cash_rcpts_acragLx'
                                                                  ,'arexti##tmp_cash_rcpts_acragX');

        EXECUTE IMMEDIATE
            'ALTER TABLE xxwc.arexti##tmp_cash_rcpts_acragX add(attr2 VARCHAR2(64),location_code VARCHAR2(128),telephone_number_1 VARCHAR2

(128),address_line_1 VARCHAR2(240),town_or_city VARCHAR2(128),region_2 VARCHAR2(128),postal_code VARCHAR2(128))'; -- Ver#1.3 -- Added this field by Pattabhi Avula on 26-May-2017 for TMS#20170526-00002(Prod Issue)
--(128),address_line_1 VARCHAR2(128),town_or_city VARCHAR2(128),region_2 VARCHAR2(128),postal_code VARCHAR2(128))'; -- Ver#1.3 -- Commented this field by Pattabhi Avula on 26-May-2017 for TMS#20170526-00002(Prod Issue)

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
      INTO  xxwc.arexti##tmp_cash_rcpts_acragx
    SELECT a.*
          ,lc.attr2
          ,lc.location_code
          ,lc.telephone_number_1
          ,lc.address_line_1
          ,lc.town_or_city
          ,lc.region_2
          ,lc.postal_code
      FROM xxwc.arexti##tmp_cash_rcpts_acraglx a, xxwc.arexti##tmp_rcpt_loc lc
     WHERE a.arm_attribute2  = lc.attr2(+)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.arexti##tmp_cash_rcpts_acragx,27', g_start);
    END;

    --*************************************************************
    -- This creates a temp table with the ARMAST text output formatted
    -- for GetPaid
    PROCEDURE armast_file
    IS
    BEGIN
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arexti##tmp_armast_file');
        xxwc_common_tunning_helpers.deleteduplicates ('xxwc.arexti##tmp_cust_trxgx'
                                                     ,' customer_trx_id,payment_schedule_id '
                                                     ,NULL);
        apps.xxwc_mv_routines_add_pkg.enable_parallelism;--ver1.2 by Neha                                             
        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'arexti##TMP_ARMAST_FILEX');--Added foe ver 1.2 by Neha

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.arexti##tmp_armast_file
(
    rec_line   VARCHAR2 (4000)
   ,org_id     NUMBER
   , TRX_RECEIPT_PAYMENT_ID VARCHAR2(224)
   ,amount_due_remaining number
) ';

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'arexti##tmp_armast_file'
                                                                  ,'arexti##tmp_armast_fileX');

        EXECUTE IMMEDIATE
            'UPDATE xxwc.arexti##tmp_cust_trxgx
   SET flexfield_3 = xxwc_common_tunning_helpers.remove_control (flexfield_3)
      ,flexfield2 = xxwc_common_tunning_helpers.remove_control (flexfield2)
      ,interface_header_attribute10 = xxwc_common_tunning_helpers.remove_control (interface_header_attribute10)
      ,flexfield_all = xxwc_common_tunning_helpers.remove_control (flexfield_all)
      ,flexfield15 = xxwc_common_tunning_helpers.remove_control (flexfield15)
      ,interface_header_attribute9 = xxwc_common_tunning_helpers.remove_control (interface_header_attribute9)
      ,interface_header_attribute11 = xxwc_common_tunning_helpers.remove_control (interface_header_attribute11)
      ,flexfield4 = xxwc_common_tunning_helpers.remove_control (flexfield4)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time (
            'GP'
           ,'XXWC.arexti##tmp_armast_fileX,xxwc_common_tunning_helpers.remove_control '
           ,g_start);

       EXECUTE IMMEDIATE
            '
INSERT/*+APPEND*/ INTO XXWC.arexti##tmp_armast_fileX
SELECT (  rpad (NVL (rcta.bill_to_account_number, '' ''), 20, '' '')                                  --    CUSTOMER_NUMBER
        ||rpad (NVL (rcta.trx_number, '' ''), 20, '' '')                                           --    TRANSACTION_NUMBER
        || RPAD (NVL (TO_CHAR (rcta.trx_date, ''MMDDYYYY''), '' ''), 8, '' '')                          --    TRANSACTION_DATE
        || LPAD (NVL (TO_CHAR (rcta.amount_due_remaining, ''9999999990.99''), ''0.00''), 20, '' '')       --    UNPAID_BALANCE
        || LPAD (NVL (TO_CHAR (rcta.due_date - rcta.trx_date), ''0.00''), 4, '' '')                       --    NET_DUE_DAYS
        || LPAD (NVL (TO_CHAR (gp_dmp.invamt, ''9999999990.99''), ''0.00''), 20, '' '')               --    TRANSACTION_AMOUNT
        ||rpad (
               NVL (
                   SUBSTR (
                       rcta.ship_to_location
                      ,1
                      ,DECODE (INSTR (rcta.ship_to_location, ''-'', -1)
                              ,0, LENGTH (rcta.ship_to_location)
                              ,INSTR (rcta.ship_to_location, ''-'', -1) - 1))
                  ,'' '')
              ,30
              ,'' '')                                                                                --    REFERENCE_NUMBER
        ||rpad ( NVL (rcta.purchase_order, '' '') , 54, '' '')                                     --    PURCHASE_ORDER_NUMBER
        || RPAD ('' '', 10, '' '')                                                                            --    DIVISION
        || LPAD (NVL (TO_CHAR (gp_dmp.tax, ''9999999990.99''), ''0.00''), 20, '' '')                                 --    TAX
        || LPAD (NVL (TO_CHAR (gp_dmp.freight, ''9999999990.99''), ''0.00''), 20, '' '')                         --    FREIGHT
        || LPAD (NVL (TO_CHAR (gp_dmp.other, ''9999999990.99''), ''0.00''), 20, '' '')                             --    OTHER
        || RPAD (NVL (gp_dmp.arstat, '' ''), 1, '' '')                                                --    TRANSACTION_TYPE
        ||rpad (DECODE (NVL (rcta.hcp_s_credit_hold, rcta.hcp_credit_hold),  ''Y'', ''H'',  ''N'', ''N''), 20, '' '') --    CHECK_NUMBER
        || RPAD ('' '', 10, '' '')                                                                      --    PROBLEM_NUMBER
        ||rpad (NVL (rcta.reason_code, '' ''), 10, '' '')                                                 --    REASON_CODE
        ||rpad (NVL (rcta.sales_rep_name, '' ''), 10, '' '')                                             --    SALES_PERSON
        || RPAD (NVL (TO_CHAR (rcta.ship_to_party_site_number), '' ''), 20, '' '')                          --    SHIP_TO_ID
        || RPAD ('' '', 20, '' '')                                                                         --    SRC_INVOICE
        || RPAD ('' '', 10, '' '')                                                                --    TRANSACTION_CURRENCY
        || LPAD (NVL (TO_CHAR (gp_dmp.invamt, ''9999999990.99''), ''0.00''), 20, '' '')              --    TRANS_CURR_ORIG_AMT
        || LPAD (NVL (TO_CHAR (rcta.amount_due_remaining, ''9999999990.99''), ''0.00''), 20, '' '') --    TRANSACTION_CURRENCY_BALANCE
        || LPAD (''0.00'', 20, '' '')                                                   --    LOCAL_CURRENCY_ORIGINAL_AMOUNT
        || LPAD (''0.00'', 20, '' '')                                                           --    LOCAL_CURRENCY_BALANCE
        ||rpad (NVL (rcta.flv_lookup_code, '' ''), 10, '' '')                                           --    DIVISION_CODE
        ||rpad (NVL (LPAD (gp_exti_dmp.warehse, 3, ''0''), ''000''), 10, '' '')                              --    SALES_AREA
        ||rpad (NVL (xxwc_common_tunning_helpers.remove_control(gp_dmp.flexfield1), '' ''), 35, '' '')                                                 --    FLEXFIELD1
        ||rpad (NVL (xxwc_common_tunning_helpers.remove_control(gp_dmp.flexfield2), '' ''), 35, '' '')                                                 --    FLEXFIELD2
        ||rpad (NVL (xxwc_common_tunning_helpers.remove_control(gp_dmp.flexfield3), '' ''), 35, '' '')                                                 --    FLEXFIELD3
        ||rpad (NVL (xxwc_common_tunning_helpers.remove_control(gp_dmp.flexfield4), '' ''), 35, '' '')                                                 --    FLEXFIELD4
        ||rpad (NVL (xxwc_common_tunning_helpers.remove_control(gp_dmp.flexfield5), '' ''), 35, '' '')                                                 --    FLEXFIELD5
        || LPAD (NVL (TO_CHAR (gp_dmp.flexnum1, ''9999999990.99''), ''0.00''), 20, '' '')                       --    FLEXNUM1
        --     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM2,''9999999990.99''),''0.00''), 20, '' '')                    --    FLEXNUM2
        || LPAD (NVL (TO_CHAR (rcta.overall_credit_limit, ''9999999990.99''), ''0.00''), 20, '' '')             --    FLEXNUM2
        || LPAD (NVL (TO_CHAR (xxwc_ar_getpaid_ob_pkg.get_flexnum3 (rcta.org_id
                                                                   ,rcta.class
                                                                   ,rcta.ship_to_party_site_number
                                                                   ,rcta.ship_to_site_use_id
                                                                   ,rcta.ship_to_location_id
                                                                   ,rcta.interface_header_attribute2
                                                                   ,rcta.interface_header_attribute8)
                              ,''9999999990.99'')
                     ,'' '')
                ,20
                ,'' '')                                                                                     --    FLEXNUM3
        || LPAD (NVL (TO_CHAR (gp_dmp.flexnum4, ''9999999990.99''), ''0.00''), 20, '' '')                       --    FLEXNUM4
        || LPAD (NVL (TO_CHAR (gp_dmp.flexnum5, ''9999999990.99''), ''0.00''), 20, '' '')                       --    FLEXNUM5
        || RPAD ('' '', 8, '' '')                                                                            --    FLEXDATE1
        || RPAD ('' '', 8, '' '')                                                                            --    FLEXDATE2
        || RPAD ('' '', 8, '' '')                                                                            --    FLEXDATE3
        || RPAD ('' '', 8, '' '')                                                                            --    FLEXDATE4
        || RPAD (TO_CHAR (rcta.creation_date, ''MMDDYYYY''), 8, '' '')                                       --    FLEXDATE5
        || RPAD ('' '', 10, '' '')                                                                       --    DISCOUNT_CODE
        || RPAD ('' '', 10, '' '')                                                                    --    AR_TARGET_SYSTEM
        ||rpad (NVL (xxwc_common_tunning_helpers.remove_control(gp_dmp.flexfield6), '' ''), 50, '' '')                                                 --    FLEXFIELD6
        ||rpad (NVL (xxwc_common_tunning_helpers.remove_control(gp_dmp.flexfield7), '' ''), 50, '' '')                                                 --    FLEXFIELD7
        ||rpad (NVL (xxwc_common_tunning_helpers.remove_control(gp_dmp.flexfield8), '' ''), 50, '' '')                                                 --    FLEXFIELD8
        ||rpad (NVL (rcta.flv_attribute1, '' ''), 50, '' '')                                               --    FLEXFIELD9
        ||rpad (NVL (rcta.flv_attribute2, '' ''), 50, '' '')                                              --    FLEXFIELD10
        ||rpad (
               NVL (
                   (CASE
                        WHEN SUBSTR (rcta.ship_to_location, INSTR (rcta.ship_to_location, ''-'', -1) + 1) =
                                 rcta.ship_to_party_site_number
                        THEN
                            RPAD ('' '', 50, '' '')
                        WHEN INSTR (rcta.ship_to_location, ''-'', -1) = 0
                        THEN
                            RPAD ('' '', 50, '' '')
                        ELSE
                            RPAD (SUBSTR (rcta.ship_to_location, INSTR (rcta.ship_to_location, ''-'', -1) + 1), 50, '' '')
                    END)
                  ,'' '')
               ,50
              ,'' '')                                                                                     --    FLEXFIELD11
        ||rpad (NVL (rcta.ship_to_attribute19, '' ''), 50, '' '')                                         --    FLEXFIELD12
        ||rpad (
               NVL (SUBSTR (xxwc_common_tunning_helpers.remove_control(gp_dmp.flexfield13), 1, 15) || '' '' || SUBSTR (NVL (rcta.hcp_account_status, '' ''), 1, 1)
                   ,''080'')
              ,50
              ,'' '')                                                                                    --    FLEXFIELD13
        || RPAD (NVL (xxwc_common_tunning_helpers.remove_control(gp_dmp.flexfield14), ''0''), 50, '' '')                                               --    FLEXFIELD14
        || RPAD (NVL (xxwc_common_tunning_helpers.remove_control(rcta.FLEXFIELD15), '' ''), 50, '' '') --   FLEXFIELD15
        || RPAD ('' '', 10, '' '')                                                                   --   JOURNAL_IDENTIFIER
                              )
           rec_line
      ,rcta.org_id,rcta.customer_trx_id||rcta.payment_schedule_id,rcta.amount_due_remaining
  FROM xxwc.arexti##tmp_cust_trxgx rcta
      ,xxwc.xxwc_armast_getpaid_dump_tbl gp_dmp
      ,xxwc.xxwc_arexti_getpaid_dump_tbl gp_exti_dmp
 WHERE     1 = 1
       AND rcta.source_from = ''CONVERSION''
       AND rcta.trx_number = gp_dmp.invno
       AND rcta.bill_to_account_number = gp_dmp.custno
       AND rcta.trx_number = gp_exti_dmp.invno
       AND rcta.bill_to_account_number = gp_exti_dmp.custno';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'XXWC.arexti##tmp_armast_fileX,28', g_start);

        DECLARE
            v_tuning_factor   NUMBER;
        BEGIN
            v_tuning_factor := 100;
            v_tuning_factor :=
                xxwc_common_tunning_helpers.add_tuning_parameter ('XXWC', 'arexti##tmp_cust_trxgx', v_tuning_factor);

            EXECUTE IMMEDIATE
                ' BEGIN
            FOR n IN 1 .. :tuning_factor
            LOOP
                   INSERT/*+APPEND*/ INTO XXWC.arexti##tmp_armast_fileX
SELECT (  rpad (NVL (rcta.bill_to_account_number, '' ''), 20, '' '')                                  --    CUSTOMER_NUMBER
        ||rpad (NVL (rcta.trx_number, '' ''), 20, '' '')                                           --    TRANSACTION_NUMBER
        || RPAD (NVL (TO_CHAR (rcta.trx_date, ''MMDDYYYY''), '' ''), 8, '' '')                          --    TRANSACTION_DATE
        || LPAD (NVL (TO_CHAR (rcta.amount_due_remaining, ''9999999990.99''), ''0.00''), 20, '' '')       --    UNPAID_BALANCE
        || LPAD (NVL (TO_CHAR (rcta.due_date - rcta.trx_date), ''0.00''), 4, '' '')                       --    NET_DUE_DAYS
        || LPAD (NVL (TO_CHAR (rcta.amount_due_original, ''9999999990.99''), ''0.00''), 20, '' '')    --    TRANSACTION_AMOUNT
        ||rpad (
                   NVL (
                       SUBSTR (
                           rcta.ship_to_location
                          ,1
                          ,DECODE (INSTR (rcta.ship_to_location, ''-'', -1)
                                  ,0, LENGTH (rcta.ship_to_location)
                                  ,INSTR (rcta.ship_to_location, ''-'', -1) - 1))
                      ,'' '')
              ,30
              ,'' '')                                                                               --    REFERENCE_NUMBER
        ||rpad (NVL (rcta.purchase_order, '' '') , 54, '' '') --    PURCHASE_ORDER_NUMBER
        || RPAD ('' '', 10, '' '')                                                                            --    DIVISION
        || LPAD (NVL (TO_CHAR (rcta.tax_original, ''9999999990.99''), ''0.00''), 20, '' '')                          --    TAX
        || LPAD (NVL (TO_CHAR (rcta.freight_original, ''9999999990.99''), ''0.00''), 20, '' '')                  --    FREIGHT
        || LPAD (
               NVL (
                   TO_CHAR (
                         rcta.amount_due_original
                       - (rcta.amount_line_items_original + rcta.tax_original + rcta.freight_original)
                      ,''9999999990.99'')
                  ,''0.00'')
              ,20
              ,'' '')                                                                                          --    OTHER
        || RPAD (NVL (DECODE (rcta.rctt_name, ''Interest Invoice'', ''F'', rcta.rctt_name), '' ''), 1, '' '') --    TRANSACTION_TYPE
        ||rpad (DECODE (NVL (rcta.hcp_s_credit_hold, rcta.hcp_credit_hold),  ''Y'', ''H'',  ''N'', ''N''), 20, '' '') --    CHECK_NUMBER -- ?????
        || RPAD ('' '', 10, '' '')                                                                      --    PROBLEM_NUMBER
        ||rpad (NVL (rcta.reason_code, '' ''), 10, '' '')                                                 --    REASON_CODE
        ||rpad (NVL (rcta.sales_rep_name, '' ''), 10, '' '')                                             --    SALES_PERSON
        || RPAD (NVL (TO_CHAR (rcta.ship_to_party_site_number), '' ''), 20, '' '')                          --    SHIP_TO_ID
        || RPAD ('' '', 20, '' '')                                                                         --    SRC_INVOICE
        || RPAD ('' '', 10, '' '')                                                                --    TRANSACTION_CURRENCY
        || LPAD (NVL (TO_CHAR (rcta.amount_due_original, ''9999999990.99''), ''0.00''), 20, '' '')   --    TRANS_CURR_ORIG_AMT
        || LPAD (NVL (TO_CHAR (rcta.amount_due_remaining, ''9999999990.99''), ''0.00''), 20, '' '') --    TRANSACTION_CURRENCY_BALANCE
        || LPAD (''0.00'', 20, '' '')                                                   --    LOCAL_CURRENCY_ORIGINAL_AMOUNT
        || LPAD (''0.00'', 20, '' '')                                                           --    LOCAL_CURRENCY_BALANCE
        ||rpad (NVL (rcta.flv_lookup_code, '' ''), 10, '' '')                                           --    DIVISION_CODE
        ||rpad (NVL ( (rcta.sales_area), '' ''), 10, '' '')                                                --    SALES_AREA
        ||rpad (NVL ( (rcta.flexfield1), '' ''), 35, '' '')                                                --    FLEXFIELD1
        ||rpad (NVL ( (rcta.flexfield2), '' ''), 35, '' '')                                                --    FLEXFIELD2
        ||rpad (NVL ( (rcta.flexfield_3), '' ''), 35, '' '')                                               --    FLEXFIELD3
        ||rpad (NVL ( (rcta.flexfield4), '' ''), 35, '' '')                                                --    FLEXFIELD4
        ||rpad (NVL ( (rcta.flexfield5), '' ''), 35, '' '')                                                --    FLEXFIELD5
        || LPAD (
               NVL (
                   ROUND (
                         (  NVL (rcta.tax_original, 0)
                          / DECODE (
                                rcta.amount_due_original - NVL (rcta.tax_original, 0) - NVL (rcta.freight_original, 0)
                               ,0, 1))
                       * 100
                      ,2)
                  ,''0.00'')
              ,20
              ,'' '')                                                                                       --    FLEXNUM1
        || LPAD (NVL (TO_CHAR (rcta.overall_credit_limit, ''9999999990.99''), ''0.00''), 20, '' '')             --    FLEXNUM2
        || LPAD (NVL (TO_CHAR (xxwc_ar_getpaid_ob_pkg.get_flexnum3 (rcta.org_id
                                                                   ,rcta.class
                                                                   ,rcta.ship_to_party_site_number
                                                                   ,rcta.ship_to_site_use_id
                                                                   ,rcta.ship_to_location_id
                                                                   ,rcta.interface_header_attribute2
                                                                   ,rcta.interface_header_attribute8)
                              ,''9999999990.99'')
                     ,'' '')
                ,20
                ,'' '')                                                                                     --    FLEXNUM3
        || LPAD (NVL (TO_CHAR ( (rcta.amount_due_original - NVL (rcta.tax_original, 0)), ''9999999990.99''), ''0.00'')
                ,20
                ,'' '')                                                                                     --    FLEXNUM4
        || LPAD (NVL (TO_CHAR (rcta.tax_original, ''9999999990.99''), ''0.00''), 20, '' '')                     --    FLEXNUM5
        || RPAD ('' '', 8, '' '')                                                                            --    FLEXDATE1
        || RPAD ('' '', 8, '' '')                                                                            --    FLEXDATE2
        || RPAD ('' '', 8, '' '')                                                                            --    FLEXDATE3
        || RPAD ('' '', 8, '' '')                                                                            --    FLEXDATE4
        || RPAD (TO_CHAR (rcta.creation_date, ''MMDDYYYY''), 8, '' '')                                       --    FLEXDATE5
        || RPAD ('' '', 10, '' '')                                                                       --    DISCOUNT_CODE
        || RPAD ('' '', 10, '' '')                                                                    --    AR_TARGET_SYSTEM
        ||rpad (NVL ( (rcta.flexfield_all), '' ''), 150, '' '')
        ||rpad (NVL (rcta.flv_attribute1, '' ''), 50, '' '')                                               --    FLEXFIELD9
        ||rpad (NVL (rcta.flv_attribute2, '' ''), 50, '' '')                                              --    FLEXFIELD10
        ||rpad (
                   NVL (
                       (CASE
                            WHEN SUBSTR (rcta.ship_to_location, INSTR (rcta.ship_to_location, ''-'', -1) + 1) =
                                     rcta.ship_to_party_site_number
                            THEN
                                RPAD ('' '', 50, '' '')
                            WHEN INSTR (rcta.ship_to_location, ''-'', -1) = 0
                            THEN
                                RPAD ('' '', 50, '' '')
                            ELSE
                                RPAD (SUBSTR (rcta.ship_to_location, INSTR (rcta.ship_to_location, ''-'', -1) + 1)
                                     ,50
                                     ,'' '')
                        END)
                      ,'' '')
              ,50
              ,'' '')                                                                                    --    FLEXFIELD11
        ||rpad (NVL (rcta.ship_to_attribute19, '' ''), 50, '' '')                                         --    FLEXFIELD12
        ||rpad (
                  NVL ( (rcta.flexfield13) || ''  '' || RPAD (rcta.ship_to_party_site_number, 11, '' ''), '' '')
               || SUBSTR (NVL (SUBSTR (NVL (rcta.hcp_account_status, '' ''), 1, 1), '' ''), 1, 1)
              ,50
              ,'' '')                                                                                     --   FLEXFIELD13
        || RPAD (''0'', 50, '' '')                                                                         --    FLEXFIELD14
        ||rpad (NVL (rcta.flexfield15, '' ''), 50, '' '')                                                  --   FLEXFIELD15
        || RPAD ('' '', 10, '' '')                                                                  --    JOURNAL_IDENTIFIER
                              )
           rec_line
      ,rcta.org_id
      ,rcta.customer_trx_id || rcta.payment_schedule_id
      ,rcta.amount_due_remaining
  FROM xxwc.arexti##tmp_cust_trxgx rcta
 WHERE   rcta.source_from <> ''CONVERSION''
                           AND group_number = n;

                COMMIT;
            END LOOP;
        END;'
                USING v_tuning_factor;

            xxwc_common_tunning_helpers.elapsed_time ('GP', 'XXWC.arexti##tmp_armast_fileX,29A', g_start);
            xxwc_common_tunning_helpers.remove_tuning_factor_columns ('XXWC', 'arexti##tmp_cust_trxgx');
            xxwc_common_tunning_helpers.elapsed_time ('GP'
                                                     ,'remove_tuning_factor_columns  arexti##tmp_cust_trxgx ,2B'
                                                     ,g_start);
        END;

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'XXWC.arexti##tmp_armast_fileX,29', g_start);
        xxwc_common_tunning_helpers.deleteduplicates ('xxwc.arexti##tmp_cash_rcpts_acragx'
                                                     ,' cash_receipt_id,payment_schedule_id '
                                                     ,NULL);

        ------------------------------------------------------------------------------------------------------------------------------
        -- CONVERSION Cash
        ------------------------------------------------------------------------------------------------------------------------------
        EXECUTE IMMEDIATE 'UPDATE xxwc.arexti##tmp_cash_rcpts_acragx
   SET address_line_1 = xxwc_common_tunning_helpers.remove_control (address_line_1)
      ,telephone_number_1 = xxwc_common_tunning_helpers.remove_control (telephone_number_1)
      ,location_code = xxwc_common_tunning_helpers.remove_control (location_code)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc_common_tunning_helpers.remove_control,29_1', g_start);

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
      INTO  xxwc.arexti##tmp_armast_filex
    SELECT    RPAD (NVL (acra.bill_to_account_number, '' ''), 20, '' '')                                               -- CUSTNO
       ||rpad (NVL (dmp.invno, '' ''), 20, '' '')                                                                  -- INVNO
       || RPAD (NVL (TO_CHAR (dmp.invdte, ''MMDDYYYY''), '' ''), 8, '' '')                                           -- INVDTE
       || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                     -- BALANCE
       || LPAD (NVL (TO_CHAR (dmp.pnet), '' ''), 4, '' '')                                                           -- PNET
       || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                                    -- INVAMT
       ||rpad ( NVL (dmp.refno, '' ''), 30, '' '')              -- REFNO
       ||rpad ( NVL (dmp.ponum, '' '') , 54, '' '')              -- PONUM
       ||rpad (NVL (dmp.division, '' ''), 10, '' '')                                                            -- DIVISION
       || LPAD (NVL (TO_CHAR (dmp.tax, ''9999999990.99''), '' ''), 20, '' '')                                           -- TAX
       || LPAD (NVL (TO_CHAR (dmp.freight, ''9999999990.99''), '' ''), 20, '' '')                                   -- FREIGHT
       || LPAD (NVL (TO_CHAR (dmp.other, ''9999999990.99''), '' ''), 20, '' '')                                       -- OTHER
       ||rpad (NVL (dmp.arstat, '' ''), 1, '' '')                                                                 -- ARSTAT
       ||rpad (NVL (dmp.checknum, '' ''), 20, '' '')                                                    -- CHECKNUM --?????
       ||rpad (NVL (dmp.probnum, '' ''), 10, '' '')                                                              -- PROBNUM
       ||rpad (NVL (dmp.reascode, '' ''), 10, '' '')                                                     -- REASCODE --????
       ||rpad (NVL (dmp.salespn, '' ''), 10, '' '')                                                       -- SALESPN --????
       || RPAD (NVL (dmp.contactid, '' ''), 20, '' '')                                                          -- CONTACTID
       ||rpad (NVL (dmp.srcinvoice, '' ''), 20, '' '')                                                        -- SRCINVOICE
       || RPAD (''USD'', 10, '' '')                                                                              -- TRANCURR
       || LPAD (NVL (TO_CHAR (dmp.tranorig, ''9999999990.99''), '' ''), 20, '' '')                                 -- TRANORIG
       || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                     -- TRANBAL
       || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                                   -- LOCORIG
       || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                      -- LOCBAL
       ||rpad (NVL (dmp.divcode, '' ''), 10, '' '')                                                              -- DIVCODE
       ||rpad (NVL (dmp.salesarea, '' ''), 10, '' '')                                                          -- SALESAREA
       ||rpad (NVL (dmp.flexfield1, '' ''), 35, '' '')                                                        -- FLEXFIELD1
       ||rpad (NVL (dmp.flexfield2, '' ''), 35, '' '')                                                        -- FLEXFIELD2
       ||rpad (NVL (dmp.flexfield3, '' ''), 35, '' '')                                                        -- FLEXFIELD3
       ||rpad (NVL (dmp.flexfield4, '' ''), 35, '' '')                                                        -- FLEXFIELD4
       ||rpad (NVL (dmp.flexfield5, '' ''), 35, '' '')                                                        -- FLEXFIELD5
       || LPAD (NVL (TO_CHAR (dmp.flexnum1), '' ''), 20, '' '')                                                  -- FLEXNUM1
       || LPAD (NVL (TO_CHAR (dmp.flexnum2), '' ''), 20, '' '')                                                  -- FLEXNUM2
       || LPAD ('' '', 20, '' '')                                                                                -- FLEXNUM3
       || LPAD (NVL (TO_CHAR (dmp.flexnum4), '' ''), 20, '' '')                                                  -- FLEXNUM4
       || LPAD (NVL (TO_CHAR (dmp.flexnum5), '' ''), 20, '' '')                                                  -- FLEXNUM5
       || RPAD (NVL (TO_CHAR (dmp.flexdate1, ''MMDDYYYY''), '' ''), 8, '' '')                                     -- FLEXDATE1
       || RPAD (NVL (TO_CHAR (dmp.flexdate2, ''MMDDYYYY''), '' ''), 8, '' '')                                     -- FLEXDATE2
       || RPAD (NVL (TO_CHAR (dmp.flexdate3, ''MMDDYYYY''), '' ''), 8, '' '')                                     -- FLEXDATE3
       || RPAD (NVL (TO_CHAR (dmp.flexdate4, ''MMDDYYYY''), '' ''), 8, '' '')                                     -- FLEXDATE4
       || RPAD (TO_CHAR (acra.creation_date, ''MMDDYYYY''), 8, '' '')                                           -- FLEXDATE5
       || RPAD (NVL (dmp.discode, '' ''), 10, '' '')                                                              -- DISCODE
       ||rpad (NVL (dmp.artargetsystem, '' ''), 10, '' '')                                                -- ARTARGETSYSTEM
       ||rpad (NVL (dmp.flexfield6, '' ''), 50, '' '')                                                        -- FLEXFIELD6
       ||rpad (NVL (dmp.flexfield7, '' ''), 50, '' '')                                                        -- FLEXFIELD7
       ||rpad (NVL (dmp.flexfield8, '' ''), 50, '' '')                                                        -- FLEXFIELD8
       ||rpad (NVL (dmp.flexfield9, '' ''), 50, '' '')                                                        -- FLEXFIELD9
       ||rpad (NVL (dmp.flexfield10, '' ''), 50, '' '')                                                      -- FLEXFIELD10
       ||rpad (NVL (dmp.flexfield11, '' ''), 50, '' '')                                                      -- FLEXFIELD11
       || RPAD ('' '', 50, '' '')                                                                             -- FLEXFIELD12
       ||rpad (
              NVL (SUBSTR (dmp.flexfield13, 1, 15) || '' '' || SUBSTR (NVL (acra.hcp_account_status, '' ''), 1, 1), ''080'')
             ,50
             ,'' '')                                                                                        -- FLEXFIELD13
       ||rpad (NVL (dmp.flexfield14, '' ''), 50, '' '')                                                      -- FLEXFIELD14
       ||rpad (NVL (dmp.flexfield15, '' ''), 50, '' '')                                                      -- FLEXFIELD15
       || RPAD ('' '', 10, '' '')                                                                           -- GA_JOURNAL_ID
           rec_line
      ,acra.org_id
      ,acra.cash_receipt_id || acra.payment_schedule_id
      ,acra.amount_due_remaining
  FROM                                                                                   --ar_cash_receipts_all     acra
      xxwc.arexti##tmp_cash_rcpts_acragx acra, xxwc.xxwc_armast_getpaid_dump_tbl dmp
 WHERE     1 = 1
       AND acra.arc_name = ''CONV - Receipt''
       AND TO_CHAR (acra.receipt_number) = dmp.invno(+)
       AND acra.bill_to_account_number = dmp.custno(+)
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Cash
-- hca.attribute16 has PRISM Customer#
------------------------------------------------------------------------------------------------------------------------------

SELECT   rpad (NVL (acra.bill_to_account_number, '' ''), 20, '' '')                                               -- CUSTNO
       ||rpad (NVL (dmp.invno, '' ''), 20, '' '')                                                                  -- INVNO
       || RPAD (NVL (TO_CHAR (dmp.invdte, ''MMDDYYYY''), '' ''), 8, '' '')                                           -- INVDTE
       || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                     -- BALANCE
       || LPAD (NVL (TO_CHAR (dmp.pnet), '' ''), 4, '' '')                                                           -- PNET
       || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                                    -- INVAMT
       ||rpad ( NVL (dmp.refno, '' '') , 30, '' '')              -- REFNO
       ||rpad ( NVL (dmp.ponum, '' '') , 54, '' '')              -- PONUM
       ||rpad (NVL (dmp.division, '' ''), 10, '' '')                                                            -- DIVISION
       || LPAD (NVL (TO_CHAR (dmp.tax, ''9999999990.99''), '' ''), 20, '' '')                                           -- TAX
       || LPAD (NVL (TO_CHAR (dmp.freight, ''9999999990.99''), '' ''), 20, '' '')                                   -- FREIGHT
       || LPAD (NVL (TO_CHAR (dmp.other, ''9999999990.99''), '' ''), 20, '' '')                                       -- OTHER
       ||rpad (NVL (dmp.arstat, '' ''), 1, '' '')                                                                 -- ARSTAT
       ||rpad (NVL (dmp.checknum, '' ''), 20, '' '')                                                    -- CHECKNUM --?????
       ||rpad (NVL (dmp.probnum, '' ''), 10, '' '')                                                              -- PROBNUM
       ||rpad (NVL (dmp.reascode, '' ''), 10, '' '')                                                     -- REASCODE --????
       ||rpad (NVL (dmp.salespn, '' ''), 10, '' '')                                                       -- SALESPN --????
       ||rpad (NVL (dmp.contactid, '' ''), 20, '' '')                                                          -- CONTACTID
       ||rpad (NVL (dmp.srcinvoice, '' ''), 20, '' '')                                                        -- SRCINVOICE
       || RPAD (''USD'', 10, '' '')                                                                              -- TRANCURR
       || LPAD (NVL (TO_CHAR (dmp.tranorig, ''9999999990.99''), '' ''), 20, '' '')                                 -- TRANORIG
       || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                     -- TRANBAL
       || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                                   -- LOCORIG
       || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                      -- LOCBAL
       ||rpad (NVL (dmp.divcode, '' ''), 10, '' '')                                                              -- DIVCODE
       ||rpad (NVL (dmp.salesarea, '' ''), 10, '' '')                                                          -- SALESAREA
       ||rpad (NVL (dmp.flexfield1, '' ''), 35, '' '')                                                        -- FLEXFIELD1
       ||rpad (NVL (dmp.flexfield2, '' ''), 35, '' '')                                                        -- FLEXFIELD2
       ||rpad (NVL (dmp.flexfield3, '' ''), 35, '' '')                                                        -- FLEXFIELD3
       ||rpad (NVL (dmp.flexfield4, '' ''), 35, '' '')                                                        -- FLEXFIELD4
       ||rpad (NVL (dmp.flexfield5, '' ''), 35, '' '')                                                        -- FLEXFIELD5
       || LPAD (NVL (TO_CHAR (dmp.flexnum1), '' ''), 20, '' '')                                                  -- FLEXNUM1
       || LPAD (NVL (TO_CHAR (dmp.flexnum2), '' ''), 20, '' '')                                                  -- FLEXNUM2
       || LPAD ('' '', 20, '' '')                                                                                -- FLEXNUM3
       || LPAD (NVL (TO_CHAR (dmp.flexnum4), '' ''), 20, '' '')                                                  -- FLEXNUM4
       || LPAD (NVL (TO_CHAR (dmp.flexnum5), '' ''), 20, '' '')                                                  -- FLEXNUM5
       || RPAD (NVL (TO_CHAR (dmp.flexdate1, ''MMDDYYYY''), '' ''), 8, '' '')                                     -- FLEXDATE1
       || RPAD (NVL (TO_CHAR (dmp.flexdate2, ''MMDDYYYY''), '' ''), 8, '' '')                                     -- FLEXDATE2
       || RPAD (NVL (TO_CHAR (dmp.flexdate3, ''MMDDYYYY''), '' ''), 8, '' '')                                     -- FLEXDATE3
       || RPAD (NVL (TO_CHAR (dmp.flexdate4, ''MMDDYYYY''), '' ''), 8, '' '')                                     -- FLEXDATE4
       || RPAD (TO_CHAR (acra.creation_date, ''MMDDYYYY''), 8, '' '')                                           -- FLEXDATE5
       || RPAD (NVL (dmp.discode, '' ''), 10, '' '')                                                              -- DISCODE
       ||rpad (NVL (dmp.artargetsystem, '' ''), 10, '' '')                                                -- ARTARGETSYSTEM
       ||rpad (NVL (dmp.flexfield6, '' ''), 50, '' '')                                                        -- FLEXFIELD6
       ||rpad (NVL (dmp.flexfield7, '' ''), 50, '' '')                                                        -- FLEXFIELD7
       ||rpad (NVL (dmp.flexfield8, '' ''), 50, '' '')                                                        -- FLEXFIELD8
       ||rpad (NVL (dmp.flexfield9, '' ''), 50, '' '')                                                        -- FLEXFIELD9
       ||rpad (NVL (dmp.flexfield10, '' ''), 50, '' '')                                                      -- FLEXFIELD10
       ||rpad (NVL (dmp.flexfield11, '' ''), 50, '' '')                                                      -- FLEXFIELD11
       || RPAD ('' '', 50, '' '')                                                                             -- FLEXFIELD12
       ||rpad (
              NVL (SUBSTR (dmp.flexfield13, 1, 15) || '' '' || SUBSTR (NVL (acra.hcp_account_status, '' ''), 1, 1), ''080'')
             ,50
             ,'' '')                                                                                        -- FLEXFIELD13
       ||rpad (NVL (dmp.flexfield14, '' ''), 50, '' '')                                                      -- FLEXFIELD14
       ||rpad (NVL (dmp.flexfield15, '' ''), 50, '' '')                                                      -- FLEXFIELD15
       || RPAD ('' '', 10, '' '')                                                                           -- GA_JOURNAL_ID
           rec_line
      ,acra.org_id
      ,acra.cash_receipt_id || acra.payment_schedule_id
      ,acra.amount_due_remaining
  FROM                                                                                   --ar_cash_receipts_all     acra
      xxwc.arexti##tmp_cash_rcpts_acragx acra, xxwc.xxwc_armast_getpaid_dump_tbl dmp
 WHERE     1 = 1
       AND acra.arc_name = ''CONV - Receipt''
       AND TO_CHAR (acra.receipt_number) = dmp.invno
       AND acra.bill_to_account_number != dmp.custno
       AND acra.hca_attribute6 = dmp.custno';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'XXWC.arexti##tmp_armast_fileX,30', g_start);

        ------------------------------------------------------------------------------------------------------------------------------
        -- NON-CONVERSION Cash
        -- ReceiptMethod != ''WC PRISM Lockbox''
        ------------------------------------------------------------------------------------------------------------------------------

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
      INTO  xxwc.arexti##tmp_armast_filex
    SELECT   rpad (NVL (acra.bill_to_account_number, '' ''), 20, '' '')                                           -- CUSTNO
           ||rpad (NVL (acra.receipt_number, '' ''), 20, '' '')                                                    -- INVNO
           || RPAD (NVL (TO_CHAR (acra.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                                -- INVDTE
           || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                 -- BALANCE
           || LPAD (''0'', 4, '' '')                                                                                 -- PNET
           || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                                -- INVAMT
           || RPAD ('' '', 30, '' '')                                                                               -- REFNO
           || RPAD ('' '', 54, '' '')                                                                               -- PONUM
           || RPAD ('' '', 10, '' '')                                                                            -- DIVISION
           || LPAD ('' '', 20, '' '')                                                                                 -- TAX
           || LPAD ('' '', 20, '' '')                                                                             -- FREIGHT
           || LPAD ('' '', 20, '' '')                                                                               -- OTHER
           || DECODE (NVL (acra.arm_attribute1, ''N''), ''Y'', ''O'', ''U'')                                               -- ARSTAT
           || RPAD ('' '', 20, '' '')                                                                    -- CHECKNUM --?????
           || RPAD ('' '', 10, '' '')                                                                             -- PROBNUM
           || RPAD ('' '', 10, '' '')                                                                     -- REASCODE --????
           || RPAD ('' '', 10, '' '')                                                                      -- SALESPN --????
           || RPAD ('' '', 20, '' '')                                                                           -- CONTACTID
           || RPAD ('' '', 20, '' '')                                                                          -- SRCINVOICE
           || RPAD (''USD'', 10, '' '')                                                                          -- TRANCURR
           || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                              -- TRANORIG
           || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                 -- TRANBAL
           || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                               -- LOCORIG
           || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                  -- LOCBAL
           || RPAD ('' '', 10, '' '')                                                                             -- DIVCODE
           ||rpad (NVL (acra.arm_attribute2, ''80''), 10, '' '')                                               -- SALESAREA
           || RPAD ('' '', 35, '' '')                                                                          -- FLEXFIELD1
           || RPAD ('' '', 35, '' '')                                                                          -- FLEXFIELD2
           || RPAD ('' '', 35, '' '')                                                                          -- FLEXFIELD3
           || RPAD ('' '', 35, '' '')                                                                          -- FLEXFIELD4
           ||rpad (acra.location_code, 35, '' '')                                                           -- FLEXFIELD5
           || LPAD ('' '', 20, '' '')                                                                            -- FLEXNUM1
           || LPAD ('' '', 20, '' '')                                                                            -- FLEXNUM2
           || LPAD ('' '', 20, '' '')                                                                            -- FLEXNUM3
           || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                              -- FLEXNUM4
           || LPAD ('' '', 20, '' '')                                                                            -- FLEXNUM5
           || RPAD ('' '', 8, '' '')                                                                            -- FLEXDATE1
           || RPAD ('' '', 8, '' '')                                                                            -- FLEXDATE2
           || RPAD ('' '', 8, '' '')                                                                            -- FLEXDATE3
           || RPAD ('' '', 8, '' '')                                                                            -- FLEXDATE4
           || RPAD (TO_CHAR (acra.creation_date, ''MMDDYYYY''), 8, '' '')                                       -- FLEXDATE5
           || RPAD ('' '', 10, '' '')                                                                             -- DISCODE
           || RPAD ('' '', 10, '' '')                                                                      -- ARTARGETSYSTEM
           ||rpad (NVL (acra.telephone_number_1, '' ''), 50, '' '')                                        --    FLEXFIELD6
           ||rpad  (NVL (acra.address_line_1, '' '') , 50, '' '')                                            --    FLEXFIELD7
           ||rpad ( NVL (acra.town_or_city || '','' || acra.region_2 || '' '' || acra.postal_code, '' '')  , 50, '' '')  --    FLEXFIELD8
           || RPAD ('' '', 50, '' '')                                                                          -- FLEXFIELD9
           || RPAD ('' '', 50, '' '')                                                                         -- FLEXFIELD10
           || RPAD ('' '', 50, '' '')                                                                         -- FLEXFIELD11
           || RPAD ('' '', 50, '' '')                                                                         -- FLEXFIELD12
           ||rpad (
                  NVL (
                         RPAD (NVL (acra.arm_attribute2, ''080''), 16, '' '')
                      || SUBSTR (NVL (acra.hcp_account_status, '' ''), 1, 1)
                     ,''080'')
                 ,50
                 ,'' '')                                                                                    -- FLEXFIELD13
           || RPAD ('' '', 50, '' '')                                                                         -- FLEXFIELD14
           || RPAD ('' '', 50, '' '')                                                                         -- FLEXFIELD15
           || RPAD ('' '', 10, '' '')                                                                       -- GA_JOURNAL_ID
               rec_line
          ,acra.org_id,acra.cash_receipt_id||acra.payment_schedule_id,acra.amount_due_remaining
      FROM                                                                               --ar_cash_receipts_all     acra
          xxwc.arexti##tmp_cash_rcpts_acragx acra
     WHERE 1 = 1 AND acra.arc_name != ''CONV - Receipt'' AND acra.arm_name != ''WC PRISM Lockbox''';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'XXWC.arexti##tmp_armast_fileX,31', g_start);

        ------------------------------------------------------------------------------------------------------------------------------
        -- NON-CONVERSION Cash
        -- ReceiptMethod = ''WC PRISM Lockbox''
        ------------------------------------------------------------------------------------------------------------------------------

        EXECUTE IMMEDIATE
            'INSERT /*+APPEND*/
      INTO  xxwc.arexti##tmp_armast_filex
    SELECT   rpad (NVL (acra.bill_to_account_number, '' ''), 20, '' '')                                           -- CUSTNO
           ||rpad (NVL (acra.receipt_number || ''-P'', '' ''), 20, '' '')                                            -- INVNO
           || RPAD (NVL (TO_CHAR (acra.receipt_date, ''MMDDYYYY''), '' ''), 8, '' '')                                -- INVDTE
           || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                 -- BALANCE
           || LPAD (''0'', 4, '' '')                                                                                 -- PNET
           || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                                -- INVAMT
           || RPAD ('' '', 30, '' '')                                                                               -- REFNO
           || RPAD ('' '', 54, '' '')                                                                               -- PONUM
           || RPAD ('' '', 10, '' '')                                                                            -- DIVISION
           || LPAD ('' '', 20, '' '')                                                                                 -- TAX
           || LPAD ('' '', 20, '' '')                                                                             -- FREIGHT
           || LPAD ('' '', 20, '' '')                                                                               -- OTHER
           || DECODE (NVL (acra.arm_attribute1, ''N''), ''Y'', ''O'', ''U'')                                               -- ARSTAT
           || RPAD ('' '', 20, '' '')                                                                    -- CHECKNUM --?????
           || RPAD ('' '', 10, '' '')                                                                             -- PROBNUM
           || RPAD ('' '', 10, '' '')                                                                     -- REASCODE --????
           || RPAD ('' '', 10, '' '')                                                                      -- SALESPN --????
           || RPAD ('' '', 20, '' '')                                                                           -- CONTACTID
           || RPAD ('' '', 20, '' '')                                                                          -- SRCINVOICE
           || RPAD (''USD'', 10, '' '')                                                                          -- TRANCURR
           || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                              -- TRANORIG
           || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                 -- TRANBAL
           || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                               -- LOCORIG
           || LPAD (NVL (TO_CHAR (acra.amount_due_remaining, ''9999999990.99''), '' ''), 20, '' '')                  -- LOCBAL
           || RPAD ('' '', 10, '' '')                                                                             -- DIVCODE
           ||rpad (NVL (acra.arm_attribute2, ''80''), 10, '' '')                                               -- SALESAREA
           || RPAD ('' '', 35, '' '')                                                                          -- FLEXFIELD1
           || RPAD ('' '', 35, '' '')                                                                          -- FLEXFIELD2
           || RPAD ('' '', 35, '' '')                                                                          -- FLEXFIELD3
           || RPAD ('' '', 35, '' '')                                                                          -- FLEXFIELD4
           ||rpad (acra.location_code, 35, '' '')                                                           -- FLEXFIELD5
           || LPAD ('' '', 20, '' '')                                                                            -- FLEXNUM1
           || LPAD ('' '', 20, '' '')                                                                            -- FLEXNUM2
           || LPAD ('' '', 20, '' '')                                                                            -- FLEXNUM3
           || LPAD (NVL (TO_CHAR (acra.amount, ''9999999990.99''), '' ''), 20, '' '')                              -- FLEXNUM4
           || LPAD ('' '', 20, '' '')                                                                            -- FLEXNUM5
           || RPAD ('' '', 8, '' '')                                                                            -- FLEXDATE1
           || RPAD ('' '', 8, '' '')                                                                            -- FLEXDATE2
           || RPAD ('' '', 8, '' '')                                                                            -- FLEXDATE3
           || RPAD ('' '', 8, '' '')                                                                            -- FLEXDATE4
           || RPAD (TO_CHAR (acra.creation_date, ''MMDDYYYY''), 8, '' '')                                       -- FLEXDATE5
           || RPAD ('' '', 10, '' '')                                                                             -- DISCODE
           || RPAD ('' '', 10, '' '')                                                                      -- ARTARGETSYSTEM
           ||rpad (NVL (acra.telephone_number_1, '' ''), 50, '' '')                                        --    FLEXFIELD6
           ||rpad (NVL (acra.address_line_1, '' '') , 50, '' '')                                           --    FLEXFIELD7
           ||rpad (NVL (acra.town_or_city || '','' || acra.region_2 || '' '' || acra.postal_code, '' '') , 50, '' '')  --    FLEXFIELD8
           || RPAD ('' '', 50, '' '')                                                                          -- FLEXFIELD9
           || RPAD ('' '', 50, '' '')                                                                         -- FLEXFIELD10
           || RPAD ('' '', 50, '' '')                                                                         -- FLEXFIELD11
           || RPAD ('' '', 50, '' '')                                                                         -- FLEXFIELD12
           ||rpad (
                  NVL (
                         RPAD (NVL (acra.arm_attribute2, ''080''), 16, '' '')
                      || SUBSTR (NVL (acra.hcp_account_status, '' ''), 1, 1)
                     ,''080'')
                 ,50
                 ,'' '')                                                                                    -- FLEXFIELD13
           || RPAD ('' '', 50, '' '')                                                                         -- FLEXFIELD14
           || RPAD ('' '', 50, '' '')                                                                         -- FLEXFIELD15
           || RPAD ('' '', 10, '' '')                                                                       -- GA_JOURNAL_ID
               rec_line
          ,acra.org_id,acra.cash_receipt_id||acra.payment_schedule_id,acra.amount_due_remaining
      FROM                                                                               --ar_cash_receipts_all     acra
          xxwc.arexti##tmp_cash_rcpts_acragx acra
     WHERE 1 = 1 AND
  acra.arc_name != ''CONV - Receipt'' AND acra.arm_name = ''WC PRISM Lockbox''';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'XXWC.arexti##tmp_armast_fileX,32', g_start);
        apps.xxwc_mv_routines_add_pkg.disable_parallelism;--ver1.2 by Neha
    END;

    --*************************************************************
    /* This is called directly by UC4. Creates ARMAST file and the log */
    PROCEDURE create_armast_file (p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT NUMBER
                                 ,p_directory_name          VARCHAR2
                                 ,p_file_name               VARCHAR2
                                 ,p_log_file_name           VARCHAR2
                                 ,p_org_name         IN     VARCHAR2)
    IS
        v_balance          NUMBER;
        l_file_name        VARCHAR2 (164);
        l_log_file_name    VARCHAR2 (164);
        v_query_str        VARCHAR2 (512);
        v_num_of_records   NUMBER;
        p_org_id           NUMBER;
    BEGIN
        p_retcode := 0;
        p_errbuf := NULL;

        SELECT organization_id
          INTO p_org_id
          FROM hr_operating_units
         WHERE name = p_org_name;
        
        mo_global.set_policy_context ('S', p_org_id);
        
        -- Creates temp tables with all data needed
        populate_armast_fields;
        -- Create temp table with text formatted data
        armast_file;
        xxwc_common_tunning_helpers.deleteduplicates ('xxwc.AREXTI##TMP_ARMAST_FILEX'
                                                     ,' TRX_RECEIPT_PAYMENT_ID '
                                                     ,NULL);

        -- default name
        IF p_file_name IS NULL
        THEN
            l_file_name := 'wcc_armast_##' || TO_CHAR (SYSDATE, 'YYYYMMDD') || '.txt';
        ELSE
            l_file_name := p_file_name;
        END IF;

        -- Write the file!!
        create_file ('xxwc.arexti##tmp_armast_filex'                                    --p_view_name        IN VARCHAR2
                    ,p_directory_name                                                   --p_directory_path   IN VARCHAR2
                    ,l_file_name                                                        --p_file_name        IN VARCHAR2
                    ,p_org_id                                                            --p_org_id           IN NUMBER)
                             );

        -- Work on the log file

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'ARMAST##tmp_getpaid_log_total');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.ARMAST##tmp_getpaid_log_total
AS
      SELECT COUNT (1) RECORD_COUNT, SUM (amount_due_remaining) BALANCE , org_id
        FROM xxwc.arexti##tmp_armast_filex
    GROUP BY org_id';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'ARMAST##tmp_getpaid_log');

        IF p_log_file_name IS NULL
        THEN
            l_log_file_name := 'wcc_armast_##' || TO_CHAR (SYSDATE, 'YYYYMMDD') || '.log';
        ELSE
            l_log_file_name := p_log_file_name;
        END IF;

        DECLARE
            v_rtn_cd        INTEGER;

            l_fexists       BOOLEAN;
            l_file_length   NUMBER;
            l_block_size    INTEGER;
            l_file_handle   UTL_FILE.file_type;

            TYPE ref_cur IS REF CURSOR;

            view_cur        ref_cur;
            view_rec        xxwc_ob_common_pkg.xxcus_ob_file_rec;
        BEGIN
            UTL_FILE.fgetattr (p_directory_name
                              ,l_file_name
                              ,l_fexists
                              ,l_file_length
                              ,l_block_size);
            l_file_handle :=
                UTL_FILE.fopen (p_directory_name
                               ,l_log_file_name
                               ,'w'
                               ,32767);

            UTL_FILE.put_line (l_file_handle, 'FILE: ' || l_file_name || CHR (10) || CHR (13));

            v_query_str := 'SELECT record_count, balance
                        FROM xxwc.ARMAST##tmp_getpaid_log_total
                       WHERE org_id = :p_org_id';

            EXECUTE IMMEDIATE v_query_str INTO v_num_of_records, v_balance USING p_org_id;

            UTL_FILE.put_line (l_file_handle, 'RECORDS: ' || v_num_of_records || CHR (13) || CHR (10));

            UTL_FILE.put_line (l_file_handle, 'BYTES: ' || l_file_length || CHR (10) || CHR (13));
            UTL_FILE.put_line (l_file_handle, 'BALANCE: ' || v_balance || CHR (13) || CHR (10));
            UTL_FILE.fclose (l_file_handle);
        END;                                                                                   -- Finished with Log file

        -- Clean up temp tables
        BEGIN
            FOR r
                IN (  SELECT *
                        FROM all_objects
                       WHERE     object_type = 'TABLE'
                             AND object_name LIKE 'AREXTI##TMP%'
                             AND owner = 'XXWC'
                             AND object_name NOT IN
                                     ('AREXTI##TMP_CUST_TRXGX'
                                     ,                                                                          --ARMAST
                                      'AREXTI##TMP_CASH_RCPTS_ACRAGX'
                                     ,                                                                          --ARMAST
                                      'AREXTI##TMP_ARMAST_FILEX'
                                     ,                                                                     --ARMAST FILE
                                      'AREXTI##TMP_CUST_TRXG'
                                     ,                                                                          --AREXTI
                                      'AREXTI##TMP_CASH_RCPTS_ACRAGL'
                                     ,                                                                          --AREXTI
                                      'AREXTI##TMP_PAYDOC_FILE'
                                     ,                                                                     --AREXTI FILE
                                      'ARTRAN##TMP_CUST_TRXGY'
                                     ,                                                                          --ARTRAN
                                      'ARTRAN##TMP_TRXGZ_FILEX'                                            --ARTRAN FILE
                                                               )
                    ORDER BY created DESC)
            LOOP
                xxwc_common_tunning_helpers.drop_temp_table (r.owner, r.object_name);
            END LOOP;
        END;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'xxwc_arexti '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_common_tunning_helpers.elapsed_time ('GP', SUBSTR (l_error_message, 1, 500), g_start);
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => 'xxwc_ar_getpaid_files_ob_pkg.create_armast_file'
               ,p_calling             => 'xxwc_ar_getpaid_files_ob_pkg.create_armast_file'
               ,p_request_id          => fnd_global.conc_request_id
               ,p_ora_error_msg       => l_error_message
               ,p_error_desc          => 'Error running Outbound File ARMAST with Program Error Exception'
               ,p_distribution_list   => pl_dflt_email
               ,p_module              => 'AR');
            p_retcode := 2;
            p_errbuf := l_error_message;
    END;

    --*****************************************************************
    --*****************************************************************
    PROCEDURE create_artran_file (p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT NUMBER
                                 ,p_directory_name          VARCHAR2
                                 ,p_file_name               VARCHAR2
                                 ,p_log_file_name           VARCHAR2
                                 ,p_org_name         IN     VARCHAR2
                                 ,p_tuning_factor           NUMBER)
    IS
        l_file_name        VARCHAR2 (124);
        l_log_file_name    VARCHAR2 (124);

        v_step             NUMBER := 1;
        v_query_str        VARCHAR2 (214);
        v_num_of_records   NUMBER;
        p_org_id           NUMBER;
        v_tuning_factor    NUMBER;
    BEGIN
        p_retcode := 0;

        SELECT organization_id
          INTO p_org_id
          FROM hr_operating_units
         WHERE name = p_org_name;

        mo_global.set_policy_context ('S', p_org_id);

        IF p_tuning_factor IS NULL
        THEN
            v_tuning_factor := 70;
        ELSE
            v_tuning_factor := p_tuning_factor;
        END IF;

        BEGIN
            FOR r
                IN (  SELECT *
                        FROM all_objects
                       WHERE     object_type = 'TABLE'
                             AND object_name LIKE '%ARTRAN##TMP%'
                             AND owner = 'XXWC'
                             AND object_name NOT IN
                                     ('AREXTI##TMP_CUST_TRXGX'
                                     ,                                                                          --ARMAST
                                      'AREXTI##TMP_CASH_RCPTS_ACRAGX'
                                     ,                                                                          --ARMAST
                                      'AREXTI##TMP_ARMAST_FILEX'
                                     ,                                                                     --ARMAST FILE
                                      'AREXTI##TMP_CUST_TRXG'
                                     ,                                                                          --AREXTI
                                      'AREXTI##TMP_CASH_RCPTS_ACRAGL'
                                     ,                                                                          --AREXTI
                                      'AREXTI##TMP_PAYDOC_FILE'
                                     ,                                                                     --AREXTI FILE
                                      'ARTRAN##TMP_CUST_TRXGY'
                                     ,                                                                          --ARTRAN
                                      'ARTRAN##TMP_TRXGZ_FILEX'                                            --ARTRAN FILE
                                                               )
                    ORDER BY created DESC)
            LOOP
                xxwc_common_tunning_helpers.drop_temp_table (r.owner, r.object_name);
            END LOOP;
        END;

        -- Enable parallelism in our RAC environments
        --EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'ARTRAN##tmp_cust_trxgz');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.artran##tmp_cust_trxgz
AS
    SELECT rcta.trx_number
          ,rcta.org_id
          ,rcta.customer_trx_id
          ,rcta.bill_to_customer_id
          ,source_from
          ,bill_to_account_number
      FROM xxwc.arexti##tmp_cust_trxg rcta
     WHERE 1 = 2';

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'ARTRAN Start,1', g_start);
       -- xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'artran##tmp_cust_trxgz');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
      INTO  xxwc.artran##tmp_cust_trxgz
    SELECT rcta.trx_number
          ,rcta.org_id
          ,rcta.customer_trx_id
          ,rcta.bill_to_customer_id
          ,source_from
          ,bill_to_account_number
      FROM xxwc.arexti##tmp_cust_trxg rcta';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgz,2', g_start);

        EXECUTE IMMEDIATE 'UPDATE xxwc.arexti##tmp_cust_trxg rcta
   SET rcta.bill_to_account_number =
           (SELECT account_number
              FROM xxwc_customer_sites_br bill_to
             WHERE cust_account_id = rcta.bill_to_customer_id and rownum=1)
 WHERE rcta.bill_to_account_number IS NULL';

        COMMIT;
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'artran##tmp_cust_trxgz'
                                                                  ,'artran##tmp_cust_trxgy');

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.artran##tmp_cust_trxgy ADD(hca_attribute6 VARCHAR2(124))';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
      INTO  xxwc.artran##tmp_cust_trxgy
    SELECT a.*, hca.attribute6
      FROM xxwc.artran##tmp_cust_trxgz a, apps.hz_cust_accounts hca
     WHERE hca.cust_account_id = a.bill_to_customer_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgy,3', g_start);

        v_tuning_factor :=
            xxwc_common_tunning_helpers.add_tuning_parameter ('XXWC', 'xxwc.artran##tmp_cust_trxgy', v_tuning_factor);

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgy add tuning factor,4', g_start);

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'artran##tmp_cust_trxgy'
                                                                  ,'artran##tmp_cust_trxgz');

        EXECUTE IMMEDIATE
            'ALTER TABLE xxwc.artran##tmp_cust_trxgz ADD(
unit_selling_price NUMBER,quantity_invoiced NUMBER,extended_amount NUMBER,line_number NUMBER,
quantity_ordered NUMBER, inventory_item_id NUMBER,uom_code VARCHAR2(64),
interface_line_attribute3  VARCHAR2(164),interface_line_attribute2 VARCHAR2(164),line_type VARCHAR2(64),
customer_trx_line_id NUMBER, description VARCHAR2(512),interface_line_attribute4 VARCHAR2(164),SEGMENT1 VARCHAR2(164),
link_to_cust_trx_line_id NUMBER )';

        EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.artran##tmp_cust_trxgz';

        EXECUTE IMMEDIATE
            ' BEGIN
            FOR n IN 1 .. :tuning_factor
            LOOP
                INSERT /*+APPEND*/
                      INTO  xxwc.artran##tmp_cust_trxgz
                    SELECT a.*
                          ,rctl.unit_selling_price
                          ,rctl.quantity_invoiced
                          ,rctl.extended_amount
                          ,rctl.line_number
                          ,rctl.quantity_ordered
                          ,rctl.inventory_item_id
                          ,rctl.uom_code
                          ,rctl.interface_line_attribute3
                          ,rctl.interface_line_attribute2
                          ,rctl.line_type
                          ,rctl.customer_trx_line_id
                          ,rctl.description
                          ,rctl.interface_line_attribute4
                          , (SELECT i.segment1
                               FROM apps.mtl_system_items_b i
                              WHERE i.inventory_item_id = rctl.inventory_item_id AND i.organization_id = 222)
                          ,link_to_cust_trx_line_id
                      FROM xxwc.artran##tmp_cust_trxgy a, apps.ra_customer_trx_lines rctl
                     WHERE     rctl.line_type IN (''LINE'', ''TAX'')
                           AND a.org_id = rctl.org_id
                           AND a.customer_trx_id = rctl.customer_trx_id
                           AND group_number = n;

                COMMIT;
            END LOOP;
        END;'
            USING v_tuning_factor;

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgz,5', g_start);
        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'artran##tmp_cust_trxgz'
                                                                  ,'artran##tmp_cust_trxgY');

        EXECUTE IMMEDIATE 'ALTER TABLE xxwc.artran##tmp_cust_trxgY ADD (LINK_ID NUMBER)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
              INTO  xxwc.artran##tmp_cust_trxgy
            SELECT l.*, t.link_to_cust_trx_line_id link_id
              FROM (SELECT *
                      FROM xxwc.artran##tmp_cust_trxgz
                     WHERE line_type = ''TAX'') t
                  ,(SELECT *
                      FROM xxwc.artran##tmp_cust_trxgz
                     WHERE line_type = ''LINE'') l
             WHERE t.link_to_cust_trx_line_id(+) = l.customer_trx_line_id';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgy,6', g_start);
        xxwc_common_tunning_helpers.deleteduplicates ('xxwc.artran##tmp_cust_trxgY', ' customer_trx_line_id  ', NULL);
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgy delete dups,6', g_start);

        EXECUTE IMMEDIATE
            'UPDATE xxwc.artran##tmp_cust_trxgy
   SET interface_line_attribute4 = xxwc_common_tunning_helpers.remove_control (interface_line_attribute4)
      ,segment1 = xxwc_common_tunning_helpers.remove_control (segment1)
      ,description = xxwc_common_tunning_helpers.remove_control (description)';

        COMMIT;
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgy remove non ascii,6', g_start);

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'artran##tmp_cust_trxgz');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'artran##tmp_trxgz_FILEX');

        EXECUTE IMMEDIATE
            ' CREATE TABLE  xxwc.artran##tmp_trxgz_FILEX
AS
SELECT   rpad (NVL (rcta.bill_to_account_number, '' ''), 20, '' '')                                     --  CUSTOMER_NUMBER
       ||rpad (NVL (rcta.trx_number, '' ''), 20, '' '')                                              --  TRANSACTION_NUMBER
       ||rpad (NVL (rcta.segment1, '' ''), 16, '' '')                                             --  INVENTORY_ITEM_NUMBER
       ||rpad (NVL ( description , '' ''), 65, '' '')                                               --  ITEM_DESCRIPTION
       || LPAD (''0'', 7, '' '')                                                               --  SALES_DISCOUNT_PERCENTAGE
       || LPAD (NVL (TO_CHAR (rcta.unit_selling_price, ''9999999990.99''), ''0.00''), 20, '' '')        --  UNIT_SELLING_PRICE
       || LPAD (NVL (TO_CHAR (rcta.quantity_invoiced), ''0''), 12, '' '')                              --  SHIPMENT_QUANTITY
       || LPAD (NVL (TO_CHAR (rcta.extended_amount, ''9999999990.99''), ''0.00''), 20, '' '')         --  TOTAL_EXTENDED_PRICE
       || LPAD (NVL (TO_CHAR (rcta.line_number), ''0''), 10, '' '')                                      --  SEQUENCE_NUMBER
       || LPAD (NVL (TO_CHAR (rcta.line_number), ''0''), 10, '' '')                                            --  UNIQUE_ID
       || RPAD (NVL (rcta.uom_code, ''EA''), 10, '' '')                                                  --  UNIT_OF_MEASURE
       || LPAD (NVL (TO_CHAR (rcta.quantity_ordered - rcta.quantity_invoiced), ''0''), 12, '' '')  --  QUANTITY_BACK_ORDERED
        || LPAD(NVL(TO_CHAR(rcta.quantity_ordered), ''0''), 12, '' '')                           --  QUANTITY_ORDERED
       || RPAD (DECODE (NVL (TO_CHAR (link_id), ''N''), ''N'', ''N'', ''T''), 1, '' '')
                                                                          --  TAXABLE_ITEM_FLAG                                                               

             --  TAXABLE_ITEM_FLAG
           rec_line
      ,rcta.org_id,rctA.customer_trx_line_id,rcta.trx_number,rcta.source_from
  FROM  xxwc.artran##tmp_cust_trxgY rcta
 WHERE rcta.source_from NOT IN (''CONVERSION'', ''PRISM'') AND rcta.line_type = ''LINE'' AND 1=2';

       -- xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'artran##tmp_trxgz_FILEX');

        BEGIN
            EXECUTE IMMEDIATE ' drop index xxwc.group_number##1_i';
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        BEGIN
            EXECUTE IMMEDIATE ' drop index xxwc.group_number##_i';
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        EXECUTE IMMEDIATE
            ' create bitmap index xxwc.group_number##_i on xxwc.artran##tmp_cust_trxgY(group_number) tablespace xxwc_data';

        EXECUTE IMMEDIATE
            ' create bitmap index xxwc.group_number##1_i on xxwc.artran##tmp_cust_trxgY(source_from) tablespace xxwc_data';

        DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'GROUP_NUMBER##_I');
        DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'GROUP_NUMBER##1_I');

        --EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

        EXECUTE IMMEDIATE
            '
BEGIN

    FOR n IN 1 .. :tuning_factor
    LOOP
        INSERT /*+APPEND*/
              INTO  xxwc.artran##tmp_trxgz_fileX
            SELECT   rpad (NVL (rcta.bill_to_account_number, '' ''), 20, '' '')                         --  CUSTOMER_NUMBER
                   ||rpad (NVL (rcta.trx_number, '' ''), 20, '' '')                                  --  TRANSACTION_NUMBER
                   ||rpad (NVL (rcta.segment1, '' ''), 16, '' '')                                 --  INVENTORY_ITEM_NUMBER
                   ||rpad (NVL (description, '' ''), 65, '' '')                                   --  ITEM_DESCRIPTION
                   || LPAD (''0'', 7, '' '')                                                   --  SALES_DISCOUNT_PERCENTAGE
                   || LPAD (NVL (TO_CHAR (rcta.unit_selling_price, ''9999999990.99''), ''0.00''), 20, '' '') --  UNIT_SELLING_PRICE
                   || LPAD (NVL (TO_CHAR (rcta.quantity_invoiced), ''0''), 12, '' '')                  --  SHIPMENT_QUANTITY
                   || LPAD (NVL (TO_CHAR (rcta.extended_amount, ''9999999990.99''), ''0.00''), 20, '' '') --  TOTAL_EXTENDED_PRICE
                   || LPAD (NVL (TO_CHAR (rcta.line_number), ''0''), 10, '' '')                          --  SEQUENCE_NUMBER
                   || LPAD (NVL (TO_CHAR (rcta.line_number), ''0''), 10, '' '')                                --  UNIQUE_ID
                   || RPAD (NVL (rcta.uom_code, ''EA''), 10, '' '')                                      --  UNIT_OF_MEASURE
                   || LPAD (NVL (TO_CHAR (rcta.quantity_ordered - rcta.quantity_invoiced), ''0''), 12, '' '') --  QUANTITY_BACK_ORDERED
                    || LPAD(NVL(TO_CHAR(rcta.quantity_ordered), ''0''), 12, '' '')                           --  QUANTITY_ORDERED
                   || RPAD (DECODE (NVL (TO_CHAR (link_id), ''N''), ''N'', ''N'', ''T''), 1, '' '')
                       --  TAXABLE_ITEM_FLAG                                                                            --  TAXABLE_ITEM_FLAG
                       rec_line
                  ,rcta.org_id,rctA.customer_trx_line_id,rcta.trx_number,rcta.source_from
              FROM xxwc.artran##tmp_cust_trxgy rcta
             WHERE rcta.source_from NOT IN (''CONVERSION'', ''PRISM'') --AND rcta.line_type = ''LINE''
                   AND rcta.group_number = n;

        COMMIT;
    END LOOP;
END;'
            USING v_tuning_factor;

        xxwc_common_tunning_helpers.elapsed_time ('GP'
                                                 ,'xxwc.artran##tmp_cust_trxgy NOT IN (CONVERSION, PRISM),7'
                                                 ,g_start);

        EXECUTE IMMEDIATE
            'BEGIN

    FOR n IN 1 .. :tuning_factor
    LOOP
        INSERT /*+APPEND*/
              INTO  xxwc.artran##tmp_trxgz_fileX
           SELECT   rpad (rcta.bill_to_account_number, 20, '' '')                                                --  CUSTOMER_NUMBER
       || RPAD (invno, 20, '' '')                                                                   --  TRANSACTION_NUMBER
       || RPAD (xxwc_common_tunning_helpers.remove_control (NVL (item, ''No Item'')), 16, '' '')                                                --  INVENTORY_ITEM_NUMBER
       ||rpad (xxwc_common_tunning_helpers.remove_control (NVL (descrip, ''No Description'')), 65, '' '')                                           --  ITEM_DESCRIPTION
       || LPAD (NVL (disc, ''0.00''), 7, '' '')                                                --  SALES_DISCOUNT_PERCENTAGE
       || LPAD (TO_CHAR (NVL (price, 0), ''9999999990.99''), 20, '' '')                               --  UNIT_SELLING_PRICE
       || LPAD (NVL (qtyshp, 0), 12, '' '')                                                          --  SHIPMENT_QUANTITY
       || LPAD (TO_CHAR (NVL (extprice, 0), ''9999999990.99''), 20, '' '')                          --  TOTAL_EXTENDED_PRICE
       || LPAD (NVL (seqnum, 99999999), 10, '' '')                                                     --  SEQUENCE_NUMBER
       || LPAD (NVL (uniqueid, 0), 10, '' '')                                                                --  UNIQUE_ID
       || RPAD (NVL (uom, ''EA''), 10, '' '')                                                            --  UNIT_OF_MEASURE
       || LPAD (NVL (qtybko, 0), 12, '' '')                                                      --  QUANTITY_BACK_ORDERED
       || LPAD (NVL (qtyord, 0), 12, '' '')
       || RPAD(NVL(TAXABLEITEM, ''N''), 1, '' '')                                                           --  QUANTITY_ORDERED
                                        --  TAXABLE_ITEM_FLAG
           rec_line
      ,rcta.org_id org_id,rctA.customer_trx_line_id,rcta.trx_number,rcta.source_from
  FROM xxwc.xxwc_artran_getpaid_dump_tbl dmp, xxwc.artran##tmp_cust_trxgY rcta
 WHERE     1 = 1
       AND TRIM (dmp.invno) = rcta.trx_number
       AND TRIM (dmp.custno) != NVL(rcta.bill_to_account_number,1)
       AND TRIM (dmp.custno) = NVL(rcta.hca_attribute6,''1'')
       AND rcta.source_from = ''CONVERSION''
                   AND rcta.group_number = n;

        COMMIT;
    END LOOP;
END;'
            USING v_tuning_factor;

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgy   IN  CONVERSION ,8', g_start);

        EXECUTE IMMEDIATE
            'BEGIN


    FOR n IN 1 .. :tuning_factor
    LOOP
        INSERT /*+APPEND*/
              INTO  xxwc.artran##tmp_trxgz_fileX
           SELECT   rpad (rcta.bill_to_account_number, 20, '' '')                                                --  CUSTOMER_NUMBER
       ||rpad (invno, 20, '' '')                                                                   --  TRANSACTION_NUMBER
       || RPAD (xxwc_common_tunning_helpers.remove_control (NVL (item, ''No Item'')), 16, '' '')                                                --  INVENTORY_ITEM_NUMBER
       ||rpad (xxwc_common_tunning_helpers.remove_control (NVL (descrip, ''No Description'')), 65, '' '')                                           --  ITEM_DESCRIPTION
       || LPAD (NVL (disc, ''0.00''), 7, '' '')                                                --  SALES_DISCOUNT_PERCENTAGE
       || LPAD (TO_CHAR (NVL (price, 0), ''9999999990.99''), 20, '' '')                               --  UNIT_SELLING_PRICE
       || LPAD (NVL (qtyshp, 0), 12, '' '')                                                          --  SHIPMENT_QUANTITY
       || LPAD (TO_CHAR (NVL (extprice, 0), ''9999999990.99''), 20, '' '')                          --  TOTAL_EXTENDED_PRICE
       || LPAD (NVL (seqnum, 99999999), 10, '' '')                                                     --  SEQUENCE_NUMBER
       || LPAD (NVL (uniqueid, 0), 10, '' '')                                                                --  UNIQUE_ID
       || RPAD (NVL (uom, ''EA''), 10, '' '')                                                            --  UNIT_OF_MEASURE
       || LPAD (NVL (qtybko, 0), 12, '' '')                                                      --  QUANTITY_BACK_ORDERED
       || LPAD (NVL (qtyord, 0), 12, '' '')                                                           --  QUANTITY_ORDERED
       || RPAD(NVL(TAXABLEITEM, ''N''), 1, '' '')                                                --  TAXABLE_ITEM_FLAG
           rec_line
      ,rcta.org_id org_id,rctA.customer_trx_line_id,rcta.trx_number,rcta.source_from
  FROM xxwc.xxwc_artran_getpaid_dump_tbl dmp, xxwc.artran##tmp_cust_trxgY rcta
 WHERE     1 = 1
       AND TRIM (dmp.invno) = rcta.trx_number
       AND TRIM (dmp.custno) = NVL(rcta.bill_to_account_number,1)

       AND rcta.source_from = ''CONVERSION''
                   AND rcta.group_number = n;

        COMMIT;
    END LOOP;
END;'
            USING v_tuning_factor;

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgy   IN  CONVERSION ,9', g_start);

        EXECUTE IMMEDIATE
            'BEGIN


    FOR n IN 1 .. :tuning_factor
    LOOP
        INSERT /*+APPEND*/
              INTO  xxwc.artran##tmp_trxgz_filex
            SELECT   rpad (NVL (rcta.bill_to_account_number, '' ''), 20, '' '')                         --  CUSTOMER_NUMBER
                   ||rpad (NVL (rcta.trx_number, '' ''), 20, '' '')                                  --  TRANSACTION_NUMBER
                   || RPAD (NVL (rcta.interface_line_attribute4, '' ''), 16, '' '')                --  INVENTORY_ITEM_NUMBER
                   ||rpad (NVL ( description , '' ''), 65, '' '')                                   --  ITEM_DESCRIPTION
                   || LPAD (''0'', 7, '' '')                                                   --  SALES_DISCOUNT_PERCENTAGE
                   || LPAD (NVL (TO_CHAR (rcta.unit_selling_price, ''9999999990.99''), ''0.00''), 20, '' '') --  UNIT_SELLING_PRICE
                   || LPAD (NVL (TO_CHAR (rcta.quantity_invoiced), ''0''), 12, '' '')                  --  SHIPMENT_QUANTITY
                   || LPAD (NVL (TO_CHAR (rcta.extended_amount, ''9999999990.99''), ''0.00''), 20, '' '') --  TOTAL_EXTENDED_PRICE
                   || LPAD (NVL (TO_CHAR (rcta.line_number), ''0''), 10, '' '')                          --  SEQUENCE_NUMBER
                   || LPAD (NVL (TO_CHAR (rcta.line_number), ''0''), 10, '' '')                                --  UNIQUE_ID
                   || RPAD (NVL (rcta.uom_code, ''EA''), 10, '' '')                                      --  UNIT_OF_MEASURE
                   || LPAD (NVL (rcta.interface_line_attribute3, ''0''), 12, '' '')                --  QUANTITY_BACK_ORDERED
                   || LPAD (NVL (rcta.interface_line_attribute2, ''0''), 12, '' '')                     --  QUANTITY_ORDERED
                   || RPAD (DECODE (NVL (TO_CHAR (link_id), ''N''), ''N'', ''N'', ''T''), 1, '' '')          --  TAXABLE_ITEM_FLAG
                       rec_line
                  ,rcta.org_id org_id,rctA.customer_trx_line_id,rcta.trx_number,rcta.source_from
              FROM xxwc.artran##tmp_cust_trxgy rcta
             WHERE 1 = 1 AND rcta.source_from = ''PRISM'' AND rcta.inventory_item_id IS NULL AND rcta.group_number = n;


        COMMIT;
    END LOOP;
END;'
            USING v_tuning_factor;

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgy   IN  PRISM ,10', g_start);

        EXECUTE IMMEDIATE
            'BEGIN


    FOR n IN 1 .. :tuning_factor
    LOOP
        INSERT /*+APPEND*/
              INTO  xxwc.artran##tmp_trxgz_filex
            SELECT   rpad (NVL (rcta.bill_to_account_number, '' ''), 20, '' '')                         --  CUSTOMER_NUMBER
                   ||rpad (NVL (rcta.trx_number, '' ''), 20, '' '')                                  --  TRANSACTION_NUMBER
                   ||rpad (NVL (rcta.segment1, '' ''), 16, '' '')                                 --  INVENTORY_ITEM_NUMBER
                   ||rpad (NVL ( description , '' ''), 65, '' '')                                   --  ITEM_DESCRIPTION
                   || LPAD (''0'', 7, '' '')                                                   --  SALES_DISCOUNT_PERCENTAGE
                   || LPAD (NVL (TO_CHAR (rcta.unit_selling_price, ''9999999990.99''), ''0.00''), 20, '' '') --  UNIT_SELLING_PRICE
                   || LPAD (NVL (TO_CHAR (rcta.quantity_invoiced), ''0''), 12, '' '')                  --  SHIPMENT_QUANTITY
                   || LPAD (NVL (TO_CHAR (rcta.extended_amount, ''9999999990.99''), ''0.00''), 20, '' '') --  TOTAL_EXTENDED_PRICE
                   || LPAD (NVL (TO_CHAR (rcta.line_number), ''0''), 10, '' '')                          --  SEQUENCE_NUMBER
                   || LPAD (NVL (TO_CHAR (rcta.line_number), ''0''), 10, '' '')                                --  UNIQUE_ID
                   || RPAD (NVL (rcta.uom_code, ''EA''), 10, '' '')                                      --  UNIT_OF_MEASURE
                   || LPAD (NVL (TO_CHAR (rcta.quantity_ordered - rcta.quantity_invoiced), ''0''), 12, '' '') --  QUANTITY_BACK_ORDERED
                   || LPAD (NVL (TO_CHAR (rcta.quantity_ordered), ''0''), 12, '' '')
                   || RPAD (DECODE (NVL (TO_CHAR (link_id), ''N''), ''N'', ''N'', ''T''), 1, '' '')          --  TAXABLE_ITEM_FLAG
                       rec_line
                  ,rcta.org_id org_id,rctA.customer_trx_line_id,rcta.trx_number,rcta.source_from
              FROM xxwc.artran##tmp_cust_trxgy rcta
             WHERE     1 = 1
                   AND rcta.source_from = ''PRISM''
                   AND rcta.inventory_item_id IS NOT NULL
                   AND rcta.group_number = n;

        COMMIT;
    END LOOP;
END;'
            USING v_tuning_factor;

        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgy   IN  PRISM ,11', g_start);
        xxwc_common_tunning_helpers.deleteduplicates ('xxwc.artran##tmp_trxgz_filex', ' rec_line ', NULL);

        IF p_file_name IS NULL
        THEN
            l_file_name := 'new_wc_artran_##' || TO_CHAR (SYSDATE, 'YYYYMMDD') || '.txt';
        ELSE
            l_file_name := p_file_name;
        END IF;

        -- Write the file!!
        create_file ('xxwc.artran##tmp_trxgz_filex'                                     --p_view_name        IN VARCHAR2
                    ,p_directory_name                                                   --p_directory_path   IN VARCHAR2
                    ,l_file_name                                                        --p_file_name        IN VARCHAR2
                    ,p_org_id                                                            --p_org_id           IN NUMBER)
                             );
        xxwc_common_tunning_helpers.elapsed_time ('GP', 'xxwc.artran##tmp_cust_trxgy   CREATE FILE ,12', g_start);

        IF p_log_file_name IS NULL
        THEN
            l_log_file_name := 'new_wc_artran##_log' || TO_CHAR (SYSDATE, 'YYYYMMDD') || '.txt';
        ELSE
            l_log_file_name := p_log_file_name;
        END IF;

        DECLARE
            v_rtn_cd        INTEGER;

            l_fexists       BOOLEAN;
            l_file_length   NUMBER;
            l_block_size    INTEGER;
            l_file_handle   UTL_FILE.file_type;

            TYPE ref_cur IS REF CURSOR;

            view_cur        ref_cur;
            view_rec        xxwc_ob_common_pkg.xxcus_ob_file_rec;
        BEGIN
            UTL_FILE.fgetattr (p_directory_name
                              ,l_file_name
                              ,l_fexists
                              ,l_file_length
                              ,l_block_size);
            l_file_handle :=
                UTL_FILE.fopen (p_directory_name
                               ,l_log_file_name
                               ,'w'
                               ,32767);

            v_query_str := 'SELECT count(1)
                        FROM xxwc.artran##tmp_trxgz_filex
                       WHERE org_id = :p_org_id';

            EXECUTE IMMEDIATE v_query_str INTO v_num_of_records USING p_org_id;

            UTL_FILE.put_line (l_file_handle, 'FILE: ' || l_file_name || CHR (13) || CHR (10));
            UTL_FILE.put_line (l_file_handle, 'RECORDS: ' || v_num_of_records || CHR (13) || CHR (10));
            UTL_FILE.put_line (l_file_handle, 'BYTES: ' || l_file_length || CHR (10) || CHR (13));

            UTL_FILE.fclose (l_file_handle);
            xxwc_common_tunning_helpers.elapsed_time ('GP', 'ARTRAN ENDS,12', g_start);
        END;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'xxwc_artran '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_common_tunning_helpers.elapsed_time ('GP', SUBSTR (l_error_message, 1, 500), g_start);
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => 'xxwc_ar_getpaid_files_ob_pkg.create_artran_file'
               ,p_calling             => 'xxwc_ar_getpaid_files_ob_pkg.create_artran_file'
               ,p_request_id          => fnd_global.conc_request_id
               ,p_ora_error_msg       => l_error_message
               ,p_error_desc          => 'Error running Outbound File artran with Program Error Exception'
               ,p_distribution_list   => pl_dflt_email
               ,p_module              => 'AR');
            p_retcode := 2;
            p_errbuf := l_error_message;
    END;
END;
/
