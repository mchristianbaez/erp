CREATE OR REPLACE PACKAGE BODY APPS.xxcusgl_acct_recon_pkg IS

  /********************************************************************************
  
  File Name: xxcusgl_acct_recon_pkg
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send balance sheet accounts ending balance to AssureNET GL
           (Account Recon Tool) for Plumbing and Electrical and IPVF.  Plumbing will
           roll product 06 and 07 into Parent of P73 for reporting.  Electrical will
           roll product 21 into Parent P89. IPVF will run for product 26, 27, 28 and
           33 USD.
  
           Recon will be on Parent Product and Account.  Service Ticket 17072
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/12/2009    Kathy Poling    Initial creation of the procedure
  1.1     12/07/2009    Kathy Poling    RFC #10437 fix the value set id for production
  1.2     12/15/2009    Kathy Poling    RFC #10969 Added chart_of_accounts_id to variables
                                        Changed flex_value_set_id to flex_value_set_name
          12/22/2009                    Added JAVA Mailer to email the file and delete
                                        the file in the first part of procedure if they
                                        run the file multiply times during a month.
                                        (Service Ticket 26072) Removed checking if ccid
                                        or accounts are active
  1.3     07/08/2010    Kathy Poling    Removed the JAVA Mailer and file directory for UC4
                                        to pickup the files and deliver to AssureNET.
  1.4     10/04/2010    Luong Vu        For corporate, add "A" prefix for accnt number
                                        Service Ticket # 51558
  1.5     11/09/2010    Luong Vu        Modified data pull to use 2 separate cursors
                                        one for Corp and one for other LOBs
                                        Lock get l_year to l_calendar     
  2.0     04/10/2011    Luong Vu        Changes to migrate to R12
                                         * XXHSI -> XXCUS      
                                         * additional specific changes are denoted 
                                           at line of change    
  2.1     03/30/2012    Luong Vu        Change ledger_id lock - RFC 33230                                                                                                                             
  ********************************************************************************/

  PROCEDURE create_glbal_two_segs(errbuf        OUT VARCHAR2
                                 ,retcode       OUT NUMBER
                                 ,p_period_name IN VARCHAR2
                                 ,p_parent      IN VARCHAR2) IS
  
    --Intialize Variables
    l_rec_count         NUMBER := 0;
    l_period_name       VARCHAR2(20);
    l_file_period       VARCHAR2(20);
    l_year              NUMBER;
    l_lob_name          VARCHAR2(20); -- v.1.4
    l_err_msg           VARCHAR2(2000);
    l_err_code          NUMBER;
    l_processed_count   NUMBER := 0; --Version 1.2
    l_file_name_recon   VARCHAR2(150);
    l_file_name_temp    VARCHAR2(150);
    l_file_dir          VARCHAR2(100);
    l_file_handle_recon utl_file.file_type;
    l_sid               VARCHAR2(10);
    l_sec               VARCHAR2(150);
    l_procedure_name    VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_two_segs';
    l_distro_list       fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    --l_distro_list fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    l_calendar     CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_fnd_category CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014550; --Account Category segment4
    l_fnd_product  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547; --Product segment1
    l_fnd_location CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014548; --Location/Branch segment2
    l_fnd_costc    CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014549; --Cost Center segment3
    l_fnd_project  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014551; --Project segment5
    lc_coa         CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --Main Chart of Accounts  Version 1.2
    l_ledger       CONSTANT gl_ledgers.ledger_id%TYPE := 2061; -- HDS Ledger
  
    --Version 1.2
    pl_dflt_email fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; --'kathy.poling@hdsupply.com';
    --pl_dflt_email   fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
    --
  BEGIN
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    --pl_sender     := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --l_file_dir    := '/xx_iface/' || l_sid || '/outbound/uc4';
    l_file_dir    := 'ORACLE_INT_GL_RECON'; --Version 1.3
    l_period_name := p_period_name;
    l_file_period := REPLACE(p_period_name, '-', '');
    errbuf        := NULL;
    retcode       := NULL;
  
    l_processed_count := 0; --Version1.2
    dbms_output.put_line('YOU ARE HERE');
    SELECT period_year
      INTO l_year
      FROM gl.gl_periods
     WHERE period_name = p_period_name
       AND period_set_name = l_calendar; -- V1.5
  
    --Create output file
  
    --Truncate XXCUS_GL_BAL_ECLIPSE
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSGL_BAL_EXTRACT_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_BAL_EXTRACT_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    -- Get extract information begin
    INSERT /*+ APPEND */
    INTO xxcus.xxcusgl_bal_extract_tbl
      (SELECT c.code_combination_id
             ,a.segment1 segment1
             ,a.segment2
             , --Account
              a.segment3
             , -- BRANCH/LOCATION
              a.segment4
             , --CC
              a.segment5
             , -- PROJ
              a.segment6
             ,a.segment7 --v2.0
             ,substr(b.description, 1, 50) description
             , --Acct Name
              nvl(c.period_net_dr, 0) period_net_dr
             ,nvl(c.period_net_cr, 0) period_net_cr
             ,c.currency_code
             ,nvl(c.begin_balance_dr, 0) begin_balance_dr
             ,nvl(c.begin_balance_cr, 0) begin_balance_cr
              --   ,p.period_year - 1 YEAR     -- In R12, the calendar is already fixed (Claudia)
             ,p.period_year
             ,p.period_num
             ,a.enabled_flag ccid_enabled
             ,a.detail_posting_allowed_flag
             ,nvl(a.end_date_active, '31-DEC-4712') ccid_end_date
             ,b.enabled_flag acct_enabled
             ,nvl(b.end_date_active, '31-DEC-4712') acct_end_date
             ,prod.enabled_flag prod_enabled
             ,nvl(prod.end_date_active, '31-DEC-4712') prod_end_date
             ,loc.enabled_flag loc_enabled
             ,nvl(loc.end_date_active, '31-DEC-4712') loc_end_date
             ,cc.enabled_flag cc_enabled
             ,nvl(cc.end_date_active, '31-DEC-4712') cc_end_date
             ,proj.enabled_flag proj_enabled
             ,nvl(proj.end_date_active, '31-DEC-4712') proj_end_date
             ,(SELECT SUM(nvl(bal.period_net_dr, 0))
                 FROM gl.gl_balances bal, gl.gl_periods per
                WHERE bal.code_combination_id = c.code_combination_id
                  AND bal.actual_flag = 'A'
                  AND bal.ledger_id = c.ledger_id
                  AND bal.currency_code = c.currency_code
                  AND per.period_set_name = l_calendar
                  AND bal.period_name = per.period_name
                  AND per.period_year = l_year) fiscal_sum_dr
             ,(SELECT SUM(nvl(bal.period_net_cr, 0))
                 FROM gl.gl_balances bal, gl.gl_periods per
                WHERE bal.code_combination_id = c.code_combination_id
                  AND bal.actual_flag = 'A'
                  AND bal.ledger_id = c.ledger_id
                  AND bal.currency_code = c.currency_code
                  AND per.period_set_name = l_calendar
                  AND bal.period_name = per.period_name
                  AND per.period_year = l_year) fiscal_sum_cr
         FROM gl.gl_balances          c
             ,gl.gl_code_combinations a
             ,apps.fnd_flex_values_vl b
             ,gl.gl_periods           p
             ,apps.fnd_flex_values_vl prod
             ,apps.fnd_flex_values_vl loc
             ,apps.fnd_flex_values_vl cc
             ,apps.fnd_flex_values_vl proj
        WHERE chart_of_accounts_id = lc_coa -- Version 1.2 changes from 101 to variable lc_coa
          AND c.period_name = l_period_name
          AND c.ledger_id = prod.attribute1
             --AND c.ledger_id = l_ledger
          AND c.currency_code = prod.attribute2
          AND c.actual_flag = 'A'
          AND c.code_combination_id = a.code_combination_id
          AND a.account_type IN ('A', 'L', 'O')
          AND a.segment1 IN
             /*(select flex_value
              from apps.fnd_flex_values
             where flex_value_set_id = 1010939
               and parent_flex_value_low = p_parent
               and enabled_flag = 'Y') */ -- Version 1.2 change from flex_value_set_id to flex_value_set_name
              (SELECT fv2.flex_value
                 FROM applsys.fnd_flex_values_tl  fvt2
                     ,applsys.fnd_flex_values     fv2
                     ,applsys.fnd_flex_value_sets fvs2
                WHERE fvt2.flex_value_id = fv2.flex_value_id
                  AND fv2.parent_flex_value_low = p_parent
                  AND fv2.flex_value_set_id = fvs2.flex_value_set_id
                     --AND fvs2.flex_value_set_name = 'HDS_CHILD_XXHSIGLACCTRECON'
                  AND fvs2.flex_value_set_name = 'XXCUS_CHILD_GLACCTRECON' --v2.0
                  AND fvt2.language = 'US'
                  AND enabled_flag = 'Y')
          AND a.segment4 = b.flex_value_meaning
          AND b.flex_value_set_id = l_fnd_category
          AND a.segment1 = prod.flex_value_meaning
          AND prod.flex_value_set_id = l_fnd_product
          AND a.segment2 = loc.flex_value_meaning
          AND loc.flex_value_set_id = l_fnd_location
          AND a.segment3 = cc.flex_value_meaning
          AND cc.flex_value_set_id = l_fnd_costc
          AND a.segment5 = proj.flex_value_meaning
          AND proj.flex_value_set_id = l_fnd_project
          AND p.period_set_name = l_calendar
          AND c.period_name = p.period_name);
  
    COMMIT;
    dbms_output.put_line('YOU ARE HERE 2');
    -- V 1.4 - Determine LOB for the giving parent account
    SELECT fvt.description
      INTO l_lob_name
      FROM applsys.fnd_flex_values_tl  fvt
          ,applsys.fnd_flex_values     fv
          ,applsys.fnd_flex_value_sets fvs
     WHERE fvt.flex_value_id = fv.flex_value_id
       AND fv.flex_value_set_id = fvs.flex_value_set_id
          --AND fvs.flex_value_set_name = 'HDS_PARENT_XXHSIGLACCTRECON'
       AND fvs.flex_value_set_name = 'XXCUS_PARENT_GLACCTRECON' --v2.0
       AND fvt.flex_value_meaning = p_parent;
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || 'GLBAL_P' || p_parent || '_' ||
                        l_file_period || '.txt';
  
    l_file_name_recon := 'GLBAL_P' || p_parent || '_' || l_file_period ||
                         '.txt';
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- Get extract information begin
    IF l_lob_name LIKE '%HD SUPPLY GSC'
    THEN
      -- V 1.5 mod start
      FOR c_glbal IN (SELECT segment1
                            ,segment4
                            ,description
                            ,SUM(nvl(period_net_dr, 0)) period_net_dr
                            ,SUM(nvl(period_net_cr, 0)) period_net_cr
                            ,(CASE
                               WHEN currency_code = 'CAD' THEN
                                NULL
                               ELSE
                                currency_code
                             END) ccy1code
                            ,(CASE
                               WHEN currency_code = 'CAD' THEN
                                NULL
                               ELSE
                                TRIM(to_char(SUM(nvl(period_net_dr, 0) +
                                                 nvl(begin_balance_dr, 0)) -
                                             SUM(nvl(period_net_cr, 0) +
                                                 nvl(begin_balance_cr, 0))
                                            ,'999999999990.99'))
                             END) ccy1glendbalance
                            ,(CASE
                               WHEN currency_code = 'USD' THEN
                                NULL
                               ELSE
                                currency_code
                             END) ccy2code
                            ,(CASE
                               WHEN currency_code = 'USD' THEN
                                NULL
                               ELSE
                                TRIM(to_char(SUM(nvl(period_net_dr, 0) +
                                                 nvl(begin_balance_dr, 0)) -
                                             SUM(nvl(period_net_cr, 0) +
                                                 nvl(begin_balance_cr, 0))
                                            ,'99999999990.99'))
                             END) ccy2glendbalance
                            ,period_year YEAR
                            ,period_num
                            ,SUM(fiscal_sum_dr) fiscal_sum_dr
                            ,SUM(fiscal_sum_cr) fiscal_sum_cr
                        FROM xxcus.xxcusgl_bal_extract_tbl
                      --Version 1.2 Service 26072
                      /*           where ccid_enabled = 'Y'
                                 and detail_posting_allowed_flag = 'Y'
                                 and acct_enabled = 'Y'
                                 and prod_enabled = 'Y'
                                 and loc_enabled = 'Y'
                                 and cc_enabled = 'Y'
                                 and proj_enabled = 'Y'
                                 and ccid_end_date > sysdate
                                 and acct_end_date > sysdate
                                 and prod_end_date > sysdate
                                 and loc_end_date > sysdate
                                 and cc_end_date > sysdate
                                 and proj_end_date > sysdate
                      */
                       GROUP BY segment1
                               ,segment4
                               ,description
                               ,currency_code
                               ,period_year
                               ,period_num)
      LOOP
      
        /*     IF c_glbal.ccy1glendbalance <> 0
           OR c_glbal.ccy2glendbalance <> 0
           OR c_glbal.period_net_dr <> 0
           OR c_glbal.period_net_cr <> 0 THEN
        
          l_rec_count := l_rec_count + 1;
          utl_file.put_line(l_file_handle_recon,
                            'P' || p_parent || '|' || c_glbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                             c_glbal.description || '|' || c_glbal.ccy1code || '|' ||
                             c_glbal.ccy1glendbalance || '|' ||
                             c_glbal.ccy2code || '|' ||
                             c_glbal.ccy2glendbalance || '|' ||
                             c_glbal.period_num || '|' || c_glbal.year);
        END IF;*/
      
        IF c_glbal.ccy1glendbalance <> 0 OR c_glbal.ccy2glendbalance <> 0 OR
           c_glbal.period_net_dr <> 0 OR c_glbal.period_net_cr <> 0
        THEN
        
          l_rec_count := l_rec_count + 1;
        
          utl_file.put_line(l_file_handle_recon
                           ,'P' || c_glbal.segment1 || '|' || 'A' ||
                            c_glbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                            c_glbal.description || '|' || c_glbal.ccy1code || '|' ||
                            c_glbal.ccy1glendbalance || '|' ||
                            c_glbal.ccy2code || '|' ||
                            c_glbal.ccy2glendbalance || '|' ||
                            c_glbal.period_num || '|' || c_glbal.year);
        END IF;
      
        IF c_glbal.ccy1glendbalance = 0 AND c_glbal.period_net_dr = 0 AND
           c_glbal.period_net_cr = 0 OR
           c_glbal.ccy2glendbalance = 0 AND c_glbal.period_net_dr = 0 AND
           c_glbal.period_net_cr = 0
        THEN
        
          IF c_glbal.fiscal_sum_dr <> 0 OR c_glbal.fiscal_sum_cr <> 0
          THEN
          
            l_rec_count := l_rec_count + 1;
            utl_file.put_line(l_file_handle_recon
                             ,'P' || c_glbal.segment1 || '|' || 'A' ||
                              c_glbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                              c_glbal.description || '|' ||
                              c_glbal.ccy1code || '|' ||
                              c_glbal.ccy1glendbalance || '|' ||
                              c_glbal.ccy2code || '|' ||
                              c_glbal.ccy2glendbalance || '|' ||
                              c_glbal.period_num || '|' || c_glbal.year);
          END IF;
        END IF;
      
        l_processed_count := l_processed_count + 1;
      
      END LOOP;
    
    ELSE
      --other LOB
      FOR c_glbal IN (SELECT segment4
                            ,description
                            ,SUM(nvl(period_net_dr, 0)) period_net_dr
                            ,SUM(nvl(period_net_cr, 0)) period_net_cr
                            ,(CASE
                               WHEN currency_code = 'CAD' THEN
                                NULL
                               ELSE
                                currency_code
                             END) ccy1code
                            ,(CASE
                               WHEN currency_code = 'CAD' THEN
                                NULL
                               ELSE
                                TRIM(to_char(SUM(nvl(period_net_dr, 0) +
                                                 nvl(begin_balance_dr, 0)) -
                                             SUM(nvl(period_net_cr, 0) +
                                                 nvl(begin_balance_cr, 0))
                                            ,'999999999990.99'))
                             END) ccy1glendbalance
                            ,(CASE
                               WHEN currency_code = 'USD' THEN
                                NULL
                               ELSE
                                currency_code
                             END) ccy2code
                            ,(CASE
                               WHEN currency_code = 'USD' THEN
                                NULL
                               ELSE
                                TRIM(to_char(SUM(nvl(period_net_dr, 0) +
                                                 nvl(begin_balance_dr, 0)) -
                                             SUM(nvl(period_net_cr, 0) +
                                                 nvl(begin_balance_cr, 0))
                                            ,'99999999990.99'))
                             END) ccy2glendbalance
                            ,period_year YEAR
                            ,period_num
                            ,SUM(fiscal_sum_dr) fiscal_sum_dr
                            ,SUM(fiscal_sum_cr) fiscal_sum_cr
                        FROM xxcus.xxcusgl_bal_extract_tbl
                      --Version 1.2 Service 26072
                      /*           where ccid_enabled = 'Y'
                                 and detail_posting_allowed_flag = 'Y'
                                 and acct_enabled = 'Y'
                                 and prod_enabled = 'Y'
                                 and loc_enabled = 'Y'
                                 and cc_enabled = 'Y'
                                 and proj_enabled = 'Y'
                                 and ccid_end_date > sysdate
                                 and acct_end_date > sysdate
                                 and prod_end_date > sysdate
                                 and loc_end_date > sysdate
                                 and cc_end_date > sysdate
                                 and proj_end_date > sysdate
                      */
                       GROUP BY segment4
                               ,description
                               ,currency_code
                               ,period_year
                               ,period_num)
      LOOP
      
        IF c_glbal.ccy1glendbalance <> 0 OR c_glbal.ccy2glendbalance <> 0 OR
           c_glbal.period_net_dr <> 0 OR c_glbal.period_net_cr <> 0
        THEN
        
          l_rec_count := l_rec_count + 1;
          utl_file.put_line(l_file_handle_recon
                           ,'P' || p_parent || '|' || c_glbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                            c_glbal.description || '|' || c_glbal.ccy1code || '|' ||
                            c_glbal.ccy1glendbalance || '|' ||
                            c_glbal.ccy2code || '|' ||
                            c_glbal.ccy2glendbalance || '|' ||
                            c_glbal.period_num || '|' || c_glbal.year);
        
        END IF;
        IF c_glbal.ccy1glendbalance = 0 AND c_glbal.period_net_dr = 0 AND
           c_glbal.period_net_cr = 0 OR
           c_glbal.ccy2glendbalance = 0 AND c_glbal.period_net_dr = 0 AND
           c_glbal.period_net_cr = 0
        THEN
        
          IF c_glbal.fiscal_sum_dr <> 0 OR c_glbal.fiscal_sum_cr <> 0
          THEN
          
            l_rec_count := l_rec_count + 1;
            utl_file.put_line(l_file_handle_recon
                             ,'P' || p_parent || '|' || c_glbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                              c_glbal.description || '|' ||
                              c_glbal.ccy1code || '|' ||
                              c_glbal.ccy1glendbalance || '|' ||
                              c_glbal.ccy2code || '|' ||
                              c_glbal.ccy2glendbalance || '|' ||
                              c_glbal.period_num || '|' || c_glbal.year);
          
          END IF;
        END IF;
      
        l_processed_count := l_processed_count + 1;
      
      END LOOP;
    END IF;
    /*      IF c_glbal.ccy1glendbalance = 0
       AND c_glbal.period_net_dr = 0
       AND c_glbal.period_net_cr = 0
       OR c_glbal.ccy2glendbalance = 0
       AND c_glbal.period_net_dr = 0
       AND c_glbal.period_net_cr = 0 THEN
    
      IF c_glbal.fiscal_sum_dr <> 0
         OR c_glbal.fiscal_sum_cr <> 0 THEN
    
        l_rec_count := l_rec_count + 1;
        utl_file.put_line(l_file_handle_recon,
                          'P' || p_parent || '|' || c_glbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                           c_glbal.description || '|' || c_glbal.ccy1code || '|' ||
                           c_glbal.ccy1glendbalance || '|' ||
                           c_glbal.ccy2code || '|' ||
                           c_glbal.ccy2glendbalance || '|' ||
                           c_glbal.period_num || '|' || c_glbal.year);
    
      END IF;
    END IF;*/
    --   dbms_output.put_line(l_lob_name);
    /*      IF c_glbal.ccy1glendbalance = 0
     AND c_glbal.period_net_dr = 0
     AND c_glbal.period_net_cr = 0
     OR c_glbal.ccy2glendbalance = 0
     AND c_glbal.period_net_dr = 0
     AND c_glbal.period_net_cr = 0 THEN
    
    IF c_glbal.fiscal_sum_dr <> 0
       OR c_glbal.fiscal_sum_cr <> 0 THEN
    
      IF l_lob_name LIKE '%HD SUPPLY GSC' THEN
        dbms_output.put_line('if2 ' || l_lob_name);
        l_rec_count := l_rec_count + 1;
        utl_file.put_line(l_file_handle_recon,
                          'P' || c_glbal.segment1 || '|' || 'A' ||
                           c_glbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                           c_glbal.description || '|' ||
                           c_glbal.ccy1code || '|' ||
                           c_glbal.ccy1glendbalance || '|' ||
                           c_glbal.ccy2code || '|' ||
                           c_glbal.ccy2glendbalance || '|' ||
                           c_glbal.period_num || '|' || c_glbal.year);*/
  
    -- V 1.5 mod END
  
    --utl_file.put_line(l_file_handle_recon, 'FOOTER' || '|' || l_rec_count);  removed Oct 15 per Claudia
  
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
  
    /* Version 1.3
     --SEND the exptracted file through email     Version 1.2
    
     If l_processed_count > 0 then
       pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';
      -- pl_dflt_email := 'kathy.poling@hdsupply.com';  --used for testing
    
       pl_ErrorStatus := SendMailJPkg.SendMail(SMTPServerName => p_host --'hsim1.hsi.hughessupply.com'
                                              ,
                                               Sender         => pl_sender --'Oracle.Applications@hughessupply.com'
                                              ,
                                               Recipient      => pl_dflt_email,
                                               CcRecipient    => '',
                                               BccRecipient   => '',
                                               Subject        => 'AssureNET Extract File ' ||
                                                                 l_file_name_recon,
                                               Body           => 'Please review and move the file to AssureNET directory \\ghdglr01wps\gl_upload$. ' ||
                                                                 SendMailJPkg.EOL ||
                                                                 'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                               ErrorMessage   => pl_ErrorMessage,
                                               Attachments    => SendMailJPkg.ATTACHMENTS_LIST(l_file_dir || '/' ||
                                                                                               l_file_name_recon));
    
     END IF;
     --
     --Delete any existing file   Version 1.2
     BEGIN
       utl_file.fremove(l_file_dir, l_file_name_recon);
     END fremove;
     --
    */
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Eclipse Acct Extract Process in GL_ACCT_RECON_PKG.CREATE_GLBAL_TWO_SEGS package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Eclipse Acct Extract Process in GL_ACCT_RECON_PKG.CREATE_GLBAL_TWO_SEGS package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
  END create_glbal_two_segs;

  /********************************************************************************
  
  File Name: xxcusgl_acct_recon_pkg
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send balance sheet accounts ending balance to AssureNET GL
           (Account Recon Tool) for Corporate.  Corporate will run for product 48 USD
           50 CAD.
  
           Recon will be on Product, Account, Branch, Cost Center and Project. Service
           Ticket 17072
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/15/2009    Kathy Poling    Initial creation of the procedure
  1.1     12/07/2009    Kathy Poling    RFC #10437 fix the value set id for production
  1.2     12/15/2009    Kathy Poling    RFC # 10969 Added chart_of_accounts_id to variables
                                        Changed flex_value_set_id to flex_value_set_name
          12/22/2009                    Added JAVA Mailer to email the file and delete
                                        the file in the first part of procedure if they
                                        run the file multiply times during a month.
                                        Service Ticket 26072) Removed checking if ccid
                                        or accounts are active
  1.3     07/08/2010    Kathy Poling    Removed the JAVA Mailer and file directory for UC4
                                        to pickup the files and deliver to AssureNET.
  1.4     11/10/2010    Luong Vu        Lock get l_year to l_calendar
  2.0     04/10/2011    Luong Vu        Changes to migrate to R12
                                         * XXHSI -> XXCUS      
                                         * additional specific changes are denoted 
                                           at line of change  
  ********************************************************************************/

  PROCEDURE create_glbal_five_segs(errbuf        OUT VARCHAR2
                                  ,retcode       OUT NUMBER
                                  ,p_period_name IN VARCHAR2
                                  ,p_parent      IN VARCHAR2) IS
  
    --Intialize Variables
    l_rec_count         NUMBER := 0;
    l_period_name       VARCHAR2(20);
    l_file_period       VARCHAR2(20);
    l_year              NUMBER;
    l_err_msg           VARCHAR2(2000);
    l_err_code          NUMBER;
    l_processed_count   NUMBER := 0; --Version 1.2
    l_file_name_recon   VARCHAR2(150);
    l_file_name_temp    VARCHAR2(150); --1.4
    l_file_dir          VARCHAR2(100);
    l_file_handle_recon utl_file.file_type;
    l_sid               VARCHAR2(10);
    l_sec               VARCHAR2(150);
    l_procedure_name    VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_glbal_five_segs';
    --l_distro_list       fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    l_distro_list fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
  
    l_calendar     CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_fnd_category CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014550; --Account Category segment4
    l_fnd_product  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547; --Product segment1
    l_fnd_location CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014548; --Location/Branch segment2
    l_fnd_costc    CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014549; --Cost Center segment3
    l_fnd_project  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014551; --Project segment5
    lc_coa         CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 101; --Main Chart of Accounts  Version 1.2
    l_ledger       CONSTANT gl_ledgers.ledger_id%TYPE := 2061; -- HDS Ledger
  
    --Version 1.2
    --pl_dflt_email   fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; --'kathy.poling@hdsupply.com';
    pl_dflt_email   fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
    --
  
  BEGIN
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    --pl_sender     := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --l_file_dir        := '/xx_iface/' || l_sid || '/outbound/uc4';
    l_file_dir        := 'ORACLE_INT_GL_RECON'; --Version 1.3
    l_period_name     := p_period_name;
    l_file_period     := REPLACE(p_period_name, '-', '');
    errbuf            := NULL;
    retcode           := NULL;
    l_processed_count := 0; --Version 1.2
  
    SELECT period_year
      INTO l_year
      FROM gl.gl_periods
     WHERE period_name = p_period_name
       AND period_set_name = l_calendar; -- V1.4 
  
    --Create output file
  
    --Truncate XXCUSGL_BAL_EXTRACT_TBL
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSGL_BAL_EXTRACT_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_BAL_EXTRACT_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    -- Get extract information begin
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcusgl_bal_extract_tbl
      (SELECT c.code_combination_id
             ,a.segment1 segment1
             ,a.segment2
             , --Account
              a.segment3
             , -- BRANCH/LOCATION
              a.segment4
             , --CC
              a.segment5
             , -- PROJ
              a.segment6
             ,a.segment7 --v2.0
             ,substr(b.description, 1, 50) description
             , --Acct Name
              nvl(c.period_net_dr, 0) period_net_dr
             ,nvl(c.period_net_cr, 0) period_net_cr
             ,c.currency_code
             ,nvl(c.begin_balance_dr, 0) begin_balance_dr
             ,nvl(c.begin_balance_cr, 0) begin_balance_cr
              --,p.period_year - 1 YEAR
             ,p.period_year
             ,p.period_num
             ,a.enabled_flag ccid_enabled
             ,a.detail_posting_allowed_flag
             ,nvl(a.end_date_active, '31-DEC-4712') ccid_end_date
             ,b.enabled_flag acct_enabled
             ,nvl(b.end_date_active, '31-DEC-4712') acct_end_date
             ,prod.enabled_flag prod_enabled
             ,nvl(prod.end_date_active, '31-DEC-4712') prod_end_date
             ,loc.enabled_flag loc_enabled
             ,nvl(loc.end_date_active, '31-DEC-4712') loc_end_date
             ,cc.enabled_flag cc_enabled
             ,nvl(cc.end_date_active, '31-DEC-4712') cc_end_date
             ,proj.enabled_flag proj_enabled
             ,nvl(proj.end_date_active, '31-DEC-4712') proj_end_date
             ,(SELECT SUM(nvl(bal.period_net_dr, 0))
                 FROM gl.gl_balances bal, gl.gl_periods per
                WHERE bal.code_combination_id = c.code_combination_id
                  AND bal.actual_flag = 'A'
                  AND bal.ledger_id = c.ledger_id
                  AND bal.currency_code = c.currency_code
                  AND per.period_set_name = l_calendar
                  AND bal.period_name = per.period_name
                  AND per.period_year = l_year) fiscal_sum_dr
             ,(SELECT SUM(nvl(bal.period_net_cr, 0))
                 FROM gl.gl_balances bal, gl.gl_periods per
                WHERE bal.code_combination_id = c.code_combination_id
                  AND bal.actual_flag = 'A'
                  AND bal.ledger_id = c.ledger_id
                  AND bal.currency_code = c.currency_code
                  AND per.period_set_name = l_calendar
                  AND bal.period_name = per.period_name
                  AND per.period_year = l_year) fiscal_sum_cr
         FROM gl.gl_balances          c
             ,gl.gl_code_combinations a
             ,apps.fnd_flex_values_vl b
             ,gl.gl_periods           p
             ,apps.fnd_flex_values_vl prod
             ,apps.fnd_flex_values_vl loc
             ,apps.fnd_flex_values_vl cc
             ,apps.fnd_flex_values_vl proj
        WHERE chart_of_accounts_id = lc_coa -- Version 1.2 changes from 101 to variable lc_coa
          AND c.period_name = l_period_name
          AND c.ledger_id = prod.attribute1
             --AND c.ledger_id = l_ledger
          AND c.currency_code = prod.attribute2
          AND c.actual_flag = 'A'
          AND c.code_combination_id = a.code_combination_id
          AND a.account_type IN ('A', 'L', 'O')
          AND a.segment1 IN
             /*(select flex_value
              from apps.fnd_flex_values
             where flex_value_set_id = 1010939
               and parent_flex_value_low = p_parent
               and enabled_flag = 'Y') */ -- Version 1.2 change from flex_value_set_id to flex_value_set_name
              (SELECT fv2.flex_value
                 FROM applsys.fnd_flex_values_tl  fvt2
                     ,applsys.fnd_flex_values     fv2
                     ,applsys.fnd_flex_value_sets fvs2
                WHERE fvt2.flex_value_id = fv2.flex_value_id
                  AND fv2.parent_flex_value_low = p_parent
                  AND fv2.flex_value_set_id = fvs2.flex_value_set_id
                     --AND fvs2.flex_value_set_name = 'HDS_CHILD_XXHSIGLACCTRECON'
                  AND fvs2.flex_value_set_name = 'XXCUS_CHILD_GLACCTRECON' --v2.0
                  AND fvt2.language = 'US'
                  AND enabled_flag = 'Y')
          AND a.segment4 = b.flex_value_meaning
          AND b.flex_value_set_id = l_fnd_category
          AND a.segment1 = prod.flex_value_meaning
          AND prod.flex_value_set_id = l_fnd_product
          AND a.segment2 = loc.flex_value_meaning
          AND loc.flex_value_set_id = l_fnd_location
          AND a.segment3 = cc.flex_value_meaning
          AND cc.flex_value_set_id = l_fnd_costc
          AND a.segment5 = proj.flex_value_meaning
          AND proj.flex_value_set_id = l_fnd_project
          AND p.period_set_name = l_calendar
          AND c.period_name = p.period_name);
  
    COMMIT;
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || 'GLBAL_P' || p_parent || '_' ||
                        l_file_period || '.txt';
  
    l_file_name_recon := 'GLBAL_P' || p_parent || '_' || l_file_period ||
                         '.txt';
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- Get extract information begin
    FOR c_glbal IN (SELECT segment1
                          ,segment4
                          ,segment2
                          ,segment3
                          ,segment5
                          ,description
                          ,SUM(nvl(period_net_dr, 0)) period_net_dr
                          ,SUM(nvl(period_net_cr, 0)) period_net_cr
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy1code
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(nvl(period_net_dr, 0) +
                                               nvl(begin_balance_dr, 0)) -
                                           SUM(nvl(period_net_cr, 0) +
                                               nvl(begin_balance_cr, 0))
                                          ,'999999999990.99'))
                           END) ccy1glendbalance
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy2code
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(nvl(period_net_dr, 0) +
                                               nvl(begin_balance_dr, 0)) -
                                           SUM(nvl(period_net_cr, 0) +
                                               nvl(begin_balance_cr, 0))
                                          ,'99999999990.99'))
                           END) ccy2glendbalance
                          ,period_year YEAR
                          ,period_num
                          ,SUM(fiscal_sum_dr) fiscal_sum_dr
                          ,SUM(fiscal_sum_cr) fiscal_sum_cr
                      FROM xxcus.xxcusgl_bal_extract_tbl
                    --Version 1.2 Service 26072
                    /*           where ccid_enabled = 'Y'
                                and detail_posting_allowed_flag = 'Y'
                                and acct_enabled = 'Y'
                                and prod_enabled = 'Y'
                                and loc_enabled = 'Y'
                                and cc_enabled = 'Y'
                                and proj_enabled = 'Y'
                                and ccid_end_date > sysdate
                                and acct_end_date > sysdate
                                and prod_end_date > sysdate
                                and loc_end_date > sysdate
                                and cc_end_date > sysdate
                                and proj_end_date > sysdate
                    */
                     GROUP BY segment1
                             ,segment4
                             ,segment2
                             ,segment3
                             ,segment5
                             ,description
                             ,currency_code
                             ,period_year
                             ,period_num)
    LOOP
    
      IF c_glbal.ccy1glendbalance <> 0 OR c_glbal.ccy2glendbalance <> 0 OR
         c_glbal.period_net_dr <> 0 OR c_glbal.period_net_cr <> 0
      THEN
      
        l_rec_count := l_rec_count + 1;
        utl_file.put_line(l_file_handle_recon
                         ,'P' || c_glbal.segment1 || '|' ||
                          c_glbal.segment4 || '|' || NULL || '|' ||
                          c_glbal.segment2 || '|' || c_glbal.segment3 || '|' ||
                          c_glbal.segment5 || '|' || c_glbal.description || '|' ||
                          c_glbal.ccy1code || '|' ||
                          c_glbal.ccy1glendbalance || '|' ||
                          c_glbal.ccy2code || '|' ||
                          c_glbal.ccy2glendbalance || '|' ||
                          c_glbal.period_num || '|' || c_glbal.year);
      
      END IF;
      IF c_glbal.ccy1glendbalance = 0 AND c_glbal.period_net_dr = 0 AND
         c_glbal.period_net_cr = 0 OR
         c_glbal.ccy2glendbalance = 0 AND c_glbal.period_net_dr = 0 AND
         c_glbal.period_net_cr = 0
      THEN
      
        IF c_glbal.fiscal_sum_dr <> 0 OR c_glbal.fiscal_sum_cr <> 0
        THEN
        
          l_rec_count := l_rec_count + 1;
          utl_file.put_line(l_file_handle_recon
                           ,'P' || c_glbal.segment1 || '|' ||
                            c_glbal.segment4 || '|' || NULL || '|' ||
                            c_glbal.segment2 || '|' || c_glbal.segment3 || '|' ||
                            c_glbal.segment5 || '|' || c_glbal.description || '|' ||
                            c_glbal.ccy1code || '|' ||
                            c_glbal.ccy1glendbalance || '|' ||
                            c_glbal.ccy2code || '|' ||
                            c_glbal.ccy2glendbalance || '|' ||
                            c_glbal.period_num || '|' || c_glbal.year);
        
        END IF;
      END IF;
    
      l_processed_count := l_processed_count + 1; --Version 1.2
    
    END LOOP;
  
    --utl_file.put_line(l_file_handle_recon, 'FOOTER' || '|' || l_rec_count);  --Removed Oct 15 per Claudia
  
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
  
    /* Version 1.3
     --SEND the exptracted file through email  Version 1.2
    
     If l_processed_count > 0 then
       pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';
       --pl_dflt_email := 'kathy.poling@hdsupply.com';  --used for testing
    
       pl_ErrorStatus := SendMailJPkg.SendMail(SMTPServerName => p_host --'hsim1.hsi.hughessupply.com'
                                              ,
                                               Sender         => pl_sender --'Oracle.Applications@hughessupply.com'
                                              ,
                                               Recipient      => pl_dflt_email,
                                               CcRecipient    => '',
                                               BccRecipient   => '',
                                               Subject        => 'AssureNET Extract File ' ||
                                                                 l_file_name_recon,
                                               Body           => 'Please review and move the file to AssureNET directory \\ghdglr01wps\gl_upload$. ' ||
                                                                 SendMailJPkg.EOL ||
                                                                 'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                               ErrorMessage   => pl_ErrorMessage,
                                               Attachments    => SendMailJPkg.ATTACHMENTS_LIST(l_file_dir || '/' ||
                                                                                               l_file_name_recon));
    
     END IF;
    
     --Delete any existing file   Version 1.2
     BEGIN
       utl_file.fremove(l_file_dir, l_file_name_recon);
     END fremove;
     --
    */
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Corporate Acct Extract Process in GL_ACCT_RECON_PKG.CREATE_GLBAL_FIVE_SEGS package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Corporate Acct Extract Process in GL_ACCT_RECON_PKG.CREATE_GLBAL_FIVE_SEGS package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
  END create_glbal_five_segs;

  /********************************************************************************
  
  File Name: XXCUS_GLACCT_RECON_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send balance sheet accounts ending balance to AssureNET GL
           (Account Recon Tool) for Utilities and White Cap.  Utilities will run for
           product 16 USD, 15 CAD. White Cap will run for 0W USD, 08 USD, 31 USD and
           34 USD.
  
           Recon will be on Product, Account, Branch and Cost Center.  Service Ticket 17072
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/15/2009    Kathy Poling    Initial creation of the procedure
  1.1     12/07/2009    Kathy Poling    RFC #10437 fix the value set id for production
  1.2     12/15/2009    Kathy Poling    RFC #10969 Added chart_of_accounts_id to variables
                                        Changed flex_value_set_id to flex_value_set_name
          12/22/2009                    Added JAVA Mailer to email the file and delete
                                        the file in the first part of procedure if they
                                        run the file multiply times during a month.
  1.3     07/08/2010    Kathy Poling    Removed the JAVA Mailer and file directory for UC4
                                        to pickup the files and deliver to AssureNET.
  1.4     11/10/2010    Luong Vu        Lock get l_year to l_calendar
  2.0     04/10/2011    Luong Vu        Changes to migrate to R12
                                         * XXHSI -> XXCUS      
                                         * additional specific changes are denoted 
                                           at line of change  
  ********************************************************************************/

  PROCEDURE create_glbal_four_segs(errbuf        OUT VARCHAR2
                                  ,retcode       OUT NUMBER
                                  ,p_period_name IN VARCHAR2
                                  ,p_parent      IN VARCHAR2) IS
  
    --Intialize Variables
    l_rec_count         NUMBER := 0;
    l_period_name       VARCHAR2(20);
    l_file_period       VARCHAR2(20);
    l_year              NUMBER;
    l_err_msg           VARCHAR2(2000);
    l_err_code          NUMBER;
    l_processed_count   NUMBER := 0; --Version 1.2
    l_file_name_recon   VARCHAR2(150);
    l_file_name_temp    VARCHAR2(150); --1.4
    l_file_dir          VARCHAR2(100);
    l_file_handle_recon utl_file.file_type;
    l_sid               VARCHAR2(10);
    l_sec               VARCHAR2(150);
    l_procedure_name    VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_glbal_foursegs';
    --l_distro_list       fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    l_distro_list fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    l_calendar     CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_fnd_category CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014550; --Account Category segment4
    l_fnd_product  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547; --Product segment1
    l_fnd_location CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014548; --Location/Branch segment2
    l_fnd_costc    CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014549; --Cost Center segment3
    l_fnd_project  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014551; --Project segment5
    lc_coa         CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --Main Chart of Accounts Version 1.2
    l_ledger       CONSTANT gl_ledgers.ledger_id%TYPE := 2061; -- HDS Ledger
    --Version 1.2
    --pl_dflt_email   fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; --'kathy.poling@hdsupply.com';
    pl_dflt_email   fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
    --
  
  BEGIN
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    --pl_sender     := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --l_file_dir        := '/xx_iface/' || l_sid || '/outbound/uc4';
    l_file_dir        := 'ORACLE_INT_GL_RECON'; --Version 1.3
    l_period_name     := p_period_name;
    l_file_period     := REPLACE(p_period_name, '-', '');
    errbuf            := NULL;
    retcode           := NULL;
    l_processed_count := 0; --Version 1.2
  
    SELECT period_year
      INTO l_year
      FROM gl.gl_periods
     WHERE period_name = p_period_name
       AND period_set_name = l_calendar; -- V1.4     ;
  
    --Create output file
  
    --Truncate XXCUSGL_BAL_EXTRACT_TBL
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSGL_BAL_EXTRACT_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_BAL_EXTRACT_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    -- Get extract information begin
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcusgl_bal_extract_tbl
      (SELECT c.code_combination_id
             ,a.segment1 segment1
             ,a.segment2
             , --Account
              a.segment3
             , -- BRANCH/LOCATION
              a.segment4
             , --CC
              a.segment5
             , -- PROJ
              a.segment6
             ,a.segment7 --v2.0
             ,substr(b.description, 1, 50) description
             , --Acct Name
              nvl(c.period_net_dr, 0) period_net_dr
             ,nvl(c.period_net_cr, 0) period_net_cr
             ,c.currency_code
             ,nvl(c.begin_balance_dr, 0) begin_balance_dr
             ,nvl(c.begin_balance_cr, 0) begin_balance_cr
              --,p.period_year - 1 YEAR
             ,p.period_year
             ,p.period_num
             ,a.enabled_flag ccid_enabled
             ,a.detail_posting_allowed_flag
             ,nvl(a.end_date_active, '31-DEC-4712') ccid_end_date
             ,b.enabled_flag acct_enabled
             ,nvl(b.end_date_active, '31-DEC-4712') acct_end_date
             ,prod.enabled_flag prod_enabled
             ,nvl(prod.end_date_active, '31-DEC-4712') prod_end_date
             ,loc.enabled_flag loc_enabled
             ,nvl(loc.end_date_active, '31-DEC-4712') loc_end_date
             ,cc.enabled_flag cc_enabled
             ,nvl(cc.end_date_active, '31-DEC-4712') cc_end_date
             ,proj.enabled_flag proj_enabled
             ,nvl(proj.end_date_active, '31-DEC-4712') proj_end_date
             ,(SELECT SUM(nvl(bal.period_net_dr, 0))
                 FROM gl.gl_balances bal, gl.gl_periods per
                WHERE bal.code_combination_id = c.code_combination_id
                  AND bal.actual_flag = 'A'
                  AND bal.ledger_id = c.ledger_id
                  AND bal.currency_code = c.currency_code
                  AND per.period_set_name = l_calendar
                  AND bal.period_name = per.period_name
                  AND per.period_year = l_year) fiscal_sum_dr
             ,(SELECT SUM(nvl(bal.period_net_cr, 0))
                 FROM gl.gl_balances bal, gl.gl_periods per
                WHERE bal.code_combination_id = c.code_combination_id
                  AND bal.actual_flag = 'A'
                  AND bal.ledger_id = c.ledger_id
                  AND bal.currency_code = c.currency_code
                  AND per.period_set_name = l_calendar
                  AND bal.period_name = per.period_name
                  AND per.period_year = l_year) fiscal_sum_cr
         FROM gl.gl_balances          c
             ,gl.gl_code_combinations a
             ,apps.fnd_flex_values_vl b
             ,gl.gl_periods           p
             ,apps.fnd_flex_values_vl prod
             ,apps.fnd_flex_values_vl loc
             ,apps.fnd_flex_values_vl cc
             ,apps.fnd_flex_values_vl proj
        WHERE chart_of_accounts_id = lc_coa -- Version 1.2 changes from 101 to variable lc_coa
          AND c.period_name = l_period_name
          AND c.ledger_id = prod.attribute1
             --AND c.ledger_id = l_ledger
          AND c.currency_code = prod.attribute2
          AND c.actual_flag = 'A'
          AND c.code_combination_id = a.code_combination_id
          AND a.account_type IN ('A', 'L', 'O')
          AND a.segment1 IN
             /*(select flex_value
              from apps.fnd_flex_values
             where flex_value_set_id = 1010939
               and parent_flex_value_low = p_parent
               and enabled_flag = 'Y') */ -- Version 1.2 change from flex_value_set_id to flex_value_set_name
              (SELECT fv2.flex_value
                 FROM applsys.fnd_flex_values_tl  fvt2
                     ,applsys.fnd_flex_values     fv2
                     ,applsys.fnd_flex_value_sets fvs2
                WHERE fvt2.flex_value_id = fv2.flex_value_id
                  AND fv2.parent_flex_value_low = p_parent
                  AND fv2.flex_value_set_id = fvs2.flex_value_set_id
                     --AND fvs2.flex_value_set_name = 'HDS_CHILD_XXHSIGLACCTRECON'
                  AND fvs2.flex_value_set_name = 'XXCUS_CHILD_GLACCTRECON' --v2.0
                  AND fvt2.language = 'US'
                  AND enabled_flag = 'Y')
          AND a.segment4 = b.flex_value_meaning
          AND b.flex_value_set_id = l_fnd_category
          AND a.segment1 = prod.flex_value_meaning
          AND prod.flex_value_set_id = l_fnd_product
          AND a.segment2 = loc.flex_value_meaning
          AND loc.flex_value_set_id = l_fnd_location
          AND a.segment3 = cc.flex_value_meaning
          AND cc.flex_value_set_id = l_fnd_costc
          AND a.segment5 = proj.flex_value_meaning
          AND proj.flex_value_set_id = l_fnd_project
          AND p.period_set_name = l_calendar
          AND c.period_name = p.period_name);
  
    COMMIT;
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || 'GLBAL_P' || p_parent || '_' ||
                        l_file_period || '.txt';
  
    l_file_name_recon := 'GLBAL_P' || p_parent || '_' || l_file_period ||
                         '.txt';
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- Get extract information begin
    FOR c_glbal IN (SELECT segment1
                          ,segment4
                          ,segment2
                          ,segment3
                          ,description
                          ,SUM(nvl(period_net_dr, 0)) period_net_dr
                          ,SUM(nvl(period_net_cr, 0)) period_net_cr
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy1code
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(nvl(period_net_dr, 0) +
                                               nvl(begin_balance_dr, 0)) -
                                           SUM(nvl(period_net_cr, 0) +
                                               nvl(begin_balance_cr, 0))
                                          ,'999999999990.99'))
                           END) ccy1glendbalance
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy2code
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(nvl(period_net_dr, 0) +
                                               nvl(begin_balance_dr, 0)) -
                                           SUM(nvl(period_net_cr, 0) +
                                               nvl(begin_balance_cr, 0))
                                          ,'99999999990.99'))
                           END) ccy2glendbalance
                          ,period_year YEAR
                          ,period_num
                          ,SUM(fiscal_sum_dr) fiscal_sum_dr
                          ,SUM(fiscal_sum_cr) fiscal_sum_cr
                      FROM xxcus.xxcusgl_bal_extract_tbl
                    --Version 1.2 Service 26072
                    /*          where ccid_enabled = 'Y'
                              and detail_posting_allowed_flag = 'Y'
                              and acct_enabled = 'Y'
                              and prod_enabled = 'Y'
                              and loc_enabled = 'Y'
                              and cc_enabled = 'Y'
                              and proj_enabled = 'Y'
                              and ccid_end_date > sysdate
                              and acct_end_date > sysdate
                              and prod_end_date > sysdate
                              and loc_end_date > sysdate
                              and cc_end_date > sysdate
                              and proj_end_date > sysdate
                    */
                     GROUP BY segment1
                             ,segment4
                             ,segment2
                             ,segment3
                             ,description
                             ,currency_code
                             ,period_year
                             ,period_num)
    LOOP
    
      IF c_glbal.ccy1glendbalance <> 0 OR c_glbal.ccy2glendbalance <> 0 OR
         c_glbal.period_net_dr <> 0 OR c_glbal.period_net_cr <> 0
      THEN
      
        l_rec_count := l_rec_count + 1;
        utl_file.put_line(l_file_handle_recon
                         ,'P' || c_glbal.segment1 || '|' ||
                          c_glbal.segment4 || '|' || NULL || '|' ||
                          c_glbal.segment2 || '|' || c_glbal.segment3 || '|' || NULL || '|' ||
                          c_glbal.description || '|' || c_glbal.ccy1code || '|' ||
                          c_glbal.ccy1glendbalance || '|' ||
                          c_glbal.ccy2code || '|' ||
                          c_glbal.ccy2glendbalance || '|' ||
                          c_glbal.period_num || '|' || c_glbal.year);
      
      END IF;
      IF c_glbal.ccy1glendbalance = 0 AND c_glbal.period_net_dr = 0 AND
         c_glbal.period_net_cr = 0 OR
         c_glbal.ccy2glendbalance = 0 AND c_glbal.period_net_dr = 0 AND
         c_glbal.period_net_cr = 0
      THEN
      
        IF c_glbal.fiscal_sum_dr <> 0 OR c_glbal.fiscal_sum_cr <> 0
        THEN
        
          l_rec_count := l_rec_count + 1;
          utl_file.put_line(l_file_handle_recon
                           ,'P' || c_glbal.segment1 || '|' ||
                            c_glbal.segment4 || '|' || NULL || '|' ||
                            c_glbal.segment2 || '|' || c_glbal.segment3 || '|' || NULL || '|' ||
                            c_glbal.description || '|' || c_glbal.ccy1code || '|' ||
                            c_glbal.ccy1glendbalance || '|' ||
                            c_glbal.ccy2code || '|' ||
                            c_glbal.ccy2glendbalance || '|' ||
                            c_glbal.period_num || '|' || c_glbal.year);
        
        END IF;
      END IF;
    
      l_processed_count := l_processed_count + 1; --Version 1.2
    
    END LOOP;
  
    --utl_file.put_line(l_file_handle_recon, 'FOOTER' || '|' || l_rec_count);  --Removed Oct 15 per Claudia
  
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
  
    /* Version 1.3
      --SEND the exptracted file through email  Version 1.2
    
      If l_processed_count > 0 then
        pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';
        --pl_dflt_email := 'kathy.poling@hdsupply.com';  --used for testing
    
        pl_ErrorStatus := SendMailJPkg.SendMail(SMTPServerName => p_host --'hsim1.hsi.hughessupply.com'
                                               ,
                                                Sender         => pl_sender --'Oracle.Applications@hughessupply.com'
                                               ,
                                                Recipient      => pl_dflt_email,
                                                CcRecipient    => '',
                                                BccRecipient   => '',
                                                Subject        => 'AssureNET Extract File ' ||
                                                                  l_file_name_recon,
                                                Body           => 'Please review and move the file to AssureNET directory \\ghdglr01wps\gl_upload$. ' ||
                                                                  SendMailJPkg.EOL ||
                                                                  'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                ErrorMessage   => pl_ErrorMessage,
                                                Attachments    => SendMailJPkg.ATTACHMENTS_LIST(l_file_dir || '/' ||
                                                                                                l_file_name_recon));
    
      END IF;
    
      --Delete any existing file   Version 1.2
      BEGIN
        utl_file.fremove(l_file_dir, l_file_name_recon);
      END fremove;
      --
    */
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_GLBAL_FOUR_SEGS package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_GLBAL_FOUR_SEGS package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
  END create_glbal_four_segs;

  /********************************************************************************
  
  File Name: XXHSI_GLACCT_RECON_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send subledger balances to AssureNET GL
           (Account Recon Tool) for Corporate.  Corporate will run for product 48 USD
           50 CAD.  Recon will be on Product, Account, Branch, Cost Center and Project.
  
           Service Ticket 21134  RFC 9812
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/27/2009    Kathy Poling    Initial creation of the procedure
                                        RFC #9812, Service Ticket #21134
  1.1     12/07/2009    Kathy Poling    RFC #10437 fix the value set id for production
  1.2     12/15/2009    Kathy Poling    RFC #10969 Added chart_of_accounts_id to variables
                                        Changed flex_value_set_id to flex_value_set_name
          12/22/2009                    Added JAVA Mailer to email the file and delete
                                        the file in the first part of procedure if they
                                        run the file multiply times during a month.
  1.3     07/08/2010    Kathy Poling    Removed the JAVA Mailer and file directory for UC4
                                        to pickup the files and deliver to AssureNET.
  2.0     04/10/2011    Luong Vu        Changes to migrate to R12
                                         * XXHSI -> XXCUS      
                                         * additional specific changes are denoted 
                                           at line of change  
  2.1     11/07/2012    Luong Vu        SR 177109 add NVL for SUM function                                            
  ********************************************************************************/

  PROCEDURE create_apsl_corporate(errbuf      OUT VARCHAR2
                                 ,retcode     OUT NUMBER
                                 ,p_thru_date IN DATE
                                 ,p_parent    IN VARCHAR2) IS
  
    --Intialize Variables
    l_rec_count         NUMBER := 0;
    l_period_name       VARCHAR2(20);
    l_period_num        NUMBER;
    l_file_period       VARCHAR2(20);
    l_year              NUMBER;
    l_thru_date         DATE;
    l_err_msg           VARCHAR2(2000);
    l_err_code          NUMBER;
    l_processed_count   NUMBER := 0; --Version 1.2
    l_file_name_recon   VARCHAR2(150);
    l_file_name_temp    VARCHAR2(150);
    l_file_dir          VARCHAR2(100);
    l_file_handle_recon utl_file.file_type;
    l_sid               VARCHAR2(10);
    l_sec               VARCHAR2(150);
    l_procedure_name    VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_apsl_corporate';
    l_distro_list       fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    --l_distro_list fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    l_calendar     CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_fnd_category CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014550; --Account Category segment4
    l_fnd_product  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547; --Product segment1
    lc_coa         CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --Main Chart of Accounts    Version 1.2
    --Version 1.2
    pl_dflt_email fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; --'kathy.poling@hdsupply.com';
    --pl_dflt_email   fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
    --
  
  BEGIN
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    SELECT period_year, period_num, period_name
      INTO l_year, l_period_num, l_period_name
      FROM gl.gl_periods
     WHERE p_thru_date BETWEEN start_date AND end_date
       AND period_set_name = l_calendar;
  
    --pl_sender     := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --l_file_dir := '/xx_iface/' || l_sid || '/outbound/uc4';
    l_file_dir := 'ORACLE_INT_GL_RECON'; --Version 1.3
    --l_period_name := p_period_name;
    l_file_period     := REPLACE(l_period_name, '-', '');
    errbuf            := NULL;
    retcode           := NULL;
    l_processed_count := 0; --Version 1.2
  
    --Create output file
  
    --Truncate XXCUSGL_BAL_EXTRACT_TBL
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSGL_AP_SUBLEDG_BAL_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_AP_SUBLEDG_BAL_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    -- Get extract information begin
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcusgl_ap_subledg_bal_tbl
      (SELECT --i.invoice_id
        i.code_combination_id
      ,a.segment1
      , --Product
        a.segment2
      , --Account
        a.segment3
      , -- BRANCH/LOCATION
        a.segment4
      , --CC
        a.segment5
      , -- PROJ
        a.segment6
        
      ,a.segment7 --v2.0
      ,substr(b.description, 1, 50) description
      , --Acct Name
        SUM(nvl(i.acctd_unrounded_dr, 0) - nvl(i.acctd_unrounded_cr, 0)) --inv balance SR 177109
      ,i.ledger_id
      ,prod.attribute2
         FROM xla.xla_trial_balances  i
             ,gl.gl_code_combinations a
             ,apps.fnd_flex_values_vl b
             ,apps.fnd_flex_values_vl prod
             ,xla.xla_ae_headers      aeh
        WHERE a.chart_of_accounts_id = lc_coa -- Version 1.2 changes from 101 to variable lc_coa
          AND i.ae_header_id = aeh.ae_header_id
          AND i.code_combination_id = a.code_combination_id
          AND trunc(aeh.accounting_date) <= p_thru_date
          AND a.account_type IN ('A', 'L', 'O')
          AND a.segment1 IN
             /*(select flex_value
              from apps.fnd_flex_values
             where flex_value_set_id = 1010939
               and parent_flex_value_low = p_parent
               and enabled_flag = 'Y') */ -- Version 1.2 change from flex_value_set_id to flex_value_set_name
              (SELECT fv2.flex_value
                 FROM applsys.fnd_flex_values_tl  fvt2
                     ,applsys.fnd_flex_values     fv2
                     ,applsys.fnd_flex_value_sets fvs2
                WHERE fvt2.flex_value_id = fv2.flex_value_id
                  AND fv2.parent_flex_value_low = p_parent
                  AND fv2.flex_value_set_id = fvs2.flex_value_set_id
                     --AND fvs2.flex_value_set_name = 'HDS_CHILD_XXHSIGLACCTRECON'
                  AND fvs2.flex_value_set_name = 'XXCUS_CHILD_GLACCTRECON' --v2.0
                  AND fvt2.language = 'US'
                  AND enabled_flag = 'Y')
          AND a.segment4 = b.flex_value_meaning
          AND b.flex_value_set_id = l_fnd_category
          AND a.segment1 = prod.flex_value_meaning
          AND prod.flex_value_set_id = l_fnd_product
        GROUP BY i.code_combination_id
                 --,i.invoice_id
                ,i.ledger_id
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7 --v2.0
                ,b.description
                ,prod.attribute2
       HAVING SUM(i.acctd_unrounded_cr) <> SUM(i.acctd_unrounded_dr));
  
    COMMIT;
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || 'SBAL_P' || p_parent || 'AP' || '_' ||
                        l_file_period || '.txt';
  
    l_file_name_recon := 'SBAL_P' || p_parent || 'AP' || '_' ||
                         l_file_period || '.txt';
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- Get extract information begin
    FOR c_slbal IN (SELECT segment1
                          ,segment4
                          ,segment2
                          ,segment3
                          ,segment5
                          ,description
                          ,SUM(invoice_bal) balance
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy1code
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(invoice_bal)
                                          ,'999999999990.99'))
                           END) ccy1glendbalance
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy2code
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(invoice_bal)
                                          ,'999999999990.99'))
                           END) ccy2glendbalance
                      FROM xxcus.xxcusgl_ap_subledg_bal_tbl
                     GROUP BY segment1
                             ,segment4
                             ,segment2
                             ,segment3
                             ,segment5
                             ,description
                             ,currency_code)
    LOOP
    
      IF c_slbal.ccy1glendbalance <> 0 OR c_slbal.ccy2glendbalance <> 0
      THEN
      
        l_rec_count := l_rec_count + 1;
        utl_file.put_line(l_file_handle_recon
                         ,'P' || c_slbal.segment1 || '|' ||
                          c_slbal.segment4 || '|' || NULL || '|' ||
                          c_slbal.segment2 || '|' || c_slbal.segment3 || '|' ||
                          c_slbal.segment5 || '|' || c_slbal.description || '|' ||
                          c_slbal.ccy1code || '|' ||
                          c_slbal.ccy1glendbalance || '|' ||
                          c_slbal.ccy2code || '|' ||
                          c_slbal.ccy2glendbalance || '|' || l_period_num || '|' ||
                          l_year);
      
      END IF;
    
      l_processed_count := l_processed_count + 1; --Version 1.2
    
    END LOOP;
  
    --utl_file.put_line(l_file_handle_recon, 'FOOTER' || '|' || l_rec_count);  --Removed Oct 15 per Claudia
  
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_APSL_CORPORATE package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_APSL_CORPORATE package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
  END create_apsl_corporate;

  /********************************************************************************
  
  File Name: XXCUS_GLACCT_RECON_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send subledger balances to AssureNET GL
           (Account Recon Tool) for Corporate.  Corporate will run for product 48 USD
           50 CAD.  Recon will be on Product, Account, Branch, Cost Center and Project.
  
           Service Ticket 21134  RFC 9812
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/27/2009    Kathy Poling    Initial creation of the procedure
                                        RFC #9812, Service Ticket #21134
  1.1     12/07/2009    Kathy Poling    RFC #10437 fix the value set id for production
  1.2     12/15/2009    Kathy Poling    RFC #10969 Added chart_of_accounts_id to variables
                                        Changed flex_value_set_id to flex_value_set_name
          12/22/2009                    Added JAVA Mailer to email the file and delete
                                        the file in the first part of procedure if they
                                        run the file multiply times during a month.
  1.3     07/08/2010    Kathy Poling    Removed the JAVA Mailer and file directory for UC4
                                        to pickup the files and deliver to AssureNET.
  2.0     04/10/2011    Luong Vu        Changes to migrate to R12
                                         * XXHSI -> XXCUS      
                                         * additional specific changes are denoted 
                                           at line of change    
  2.1     11/07/2012    Luong Vu        SR 177109 add NVL for SUM function                                          
  ********************************************************************************/

  PROCEDURE create_apsl_corp_two_segs(errbuf      OUT VARCHAR2
                                     ,retcode     OUT NUMBER
                                     ,p_thru_date IN DATE
                                     ,p_parent    IN VARCHAR2) IS
  
    --Intialize Variables
    l_rec_count         NUMBER := 0;
    l_period_name       VARCHAR2(20);
    l_period_num        NUMBER;
    l_file_period       VARCHAR2(20);
    l_year              NUMBER;
    l_thru_date         DATE;
    l_err_msg           VARCHAR2(2000);
    l_err_code          NUMBER;
    l_processed_count   NUMBER := 0; --Version 1.2
    l_file_name_recon   VARCHAR2(150);
    l_file_name_temp    VARCHAR2(150);
    l_file_dir          VARCHAR2(100);
    l_file_handle_recon utl_file.file_type;
    l_sid               VARCHAR2(10);
    l_sec               VARCHAR2(150);
    l_procedure_name    VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_apsl_corp_two_segs';
    l_distro_list       fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    --l_distro_list fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    l_calendar     CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_fnd_category CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014550; --Account Category segment4
    l_fnd_product  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547; --Product segment1
    lc_coa         CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --Main Chart of Accounts    Version 1.2
    --Version 1.2
    pl_dflt_email fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; --'kathy.poling@hdsupply.com';
    --pl_dflt_email   fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
    --
  
  BEGIN
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    SELECT period_year, period_num, period_name
      INTO l_year, l_period_num, l_period_name
      FROM gl.gl_periods
     WHERE p_thru_date BETWEEN start_date AND end_date
       AND period_set_name = l_calendar;
  
    --pl_sender     := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --l_file_dir := '/xx_iface/' || l_sid || '/outbound/uc4';
    l_file_dir := 'ORACLE_INT_GL_RECON'; --Version 1.3
    --l_period_name := p_period_name;
    l_file_period     := REPLACE(l_period_name, '-', '');
    errbuf            := NULL;
    retcode           := NULL;
    l_processed_count := 0; --Version 1.2
  
    --Create output file
  
    --Truncate XXCUSGL_BAL_EXTRACT_TBL
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSGL_AP_SUBLEDG_BAL_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_AP_SUBLEDG_BAL_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    -- Get extract information begin
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcusgl_ap_subledg_bal_tbl
      (SELECT --i.invoice_id
        i.code_combination_id
      ,a.segment1
      , --Product
        a.segment2
      , --Account
        a.segment3
      , -- BRANCH/LOCATION
        a.segment4
      , --CC
        a.segment5
      , -- PROJ
        a.segment6
      ,a.segment7 --v2.0
      ,substr(b.description, 1, 50) description
      , --Acct Name
        SUM(nvl(i.acctd_unrounded_dr, 0) - nvl(i.acctd_unrounded_cr, 0)) -- SR 177109
      ,i.ledger_id
      ,prod.attribute2
         FROM xla.xla_trial_balances  i
             ,gl.gl_code_combinations a
             ,apps.fnd_flex_values_vl b
             ,apps.fnd_flex_values_vl prod
             ,xla.xla_ae_headers      aeh
        WHERE a.chart_of_accounts_id = lc_coa -- Version 1.2 changes from 101 to variable lc_coa
          AND i.ae_header_id = aeh.ae_header_id
          AND i.code_combination_id = a.code_combination_id
          AND trunc(aeh.accounting_date) <= p_thru_date
          AND a.account_type IN ('A', 'L', 'O')
          AND a.segment1 IN
             /*(select flex_value
              from apps.fnd_flex_values
             where flex_value_set_id = 1010939
               and parent_flex_value_low = p_parent
               and enabled_flag = 'Y') */ -- Version 1.2 change from flex_value_set_id to flex_value_set_name
              (SELECT fv2.flex_value
                 FROM applsys.fnd_flex_values_tl  fvt2
                     ,applsys.fnd_flex_values     fv2
                     ,applsys.fnd_flex_value_sets fvs2
                WHERE fvt2.flex_value_id = fv2.flex_value_id
                  AND fv2.parent_flex_value_low = p_parent
                  AND fv2.flex_value_set_id = fvs2.flex_value_set_id
                     --AND fvs2.flex_value_set_name = 'HDS_CHILD_XXHSIGLACCTRECON'
                  AND fvs2.flex_value_set_name = 'XXCUS_CHILD_GLACCTRECON' --v2.0
                  AND fvt2.language = 'US'
                  AND enabled_flag = 'Y')
          AND a.segment4 = b.flex_value_meaning
          AND b.flex_value_set_id = l_fnd_category
          AND a.segment1 = prod.flex_value_meaning
          AND prod.flex_value_set_id = l_fnd_product
        GROUP BY i.code_combination_id
                 --,i.invoice_id
                ,i.ledger_id
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7 --v2.0
                ,b.description
                ,prod.attribute2
       HAVING SUM(i.acctd_unrounded_cr) <> SUM(i.acctd_unrounded_dr));
  
    COMMIT;
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || 'SBAL_P' || p_parent || 'AP' || '_' ||
                        l_file_period || '.txt';
  
    l_file_name_recon := 'SBAL_P' || p_parent || 'AP' || '_' ||
                         l_file_period || '.txt';
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- Get extract information begin
    FOR c_slbal IN (SELECT segment1
                          ,segment4
                           --                ,segment2
                           --                ,segment3
                           --                ,segment5
                          ,description
                          ,SUM(invoice_bal) balance
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy1code
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(invoice_bal)
                                          ,'999999999990.99'))
                           END) ccy1glendbalance
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy2code
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(invoice_bal)
                                          ,'999999999990.99'))
                           END) ccy2glendbalance
                      FROM xxcus.xxcusgl_ap_subledg_bal_tbl
                     GROUP BY segment1
                             ,segment4
                              --              ,segment2
                              --              ,segment3
                              --              ,segment5
                             ,description
                             ,currency_code)
    LOOP
    
      IF c_slbal.ccy1glendbalance <> 0 OR c_slbal.ccy2glendbalance <> 0
      THEN
      
        l_rec_count := l_rec_count + 1;
        utl_file.put_line(l_file_handle_recon
                         ,'P' || c_slbal.segment1 || '|' || 'A' ||
                          c_slbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                          c_slbal.description || '|' || c_slbal.ccy1code || '|' ||
                          c_slbal.ccy1glendbalance || '|' ||
                          c_slbal.ccy2code || '|' ||
                          c_slbal.ccy2glendbalance || '|' || l_period_num || '|' ||
                          l_year);
      
      END IF;
    
      l_processed_count := l_processed_count + 1; --Version 1.2
    
    END LOOP;
  
    --utl_file.put_line(l_file_handle_recon, 'FOOTER' || '|' || l_rec_count);  --Removed Oct 15 per Claudia
  
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
  
    /* Version 1.3
      --SEND the exptracted file through email  Version 1.2
    
      If l_processed_count > 0 then
        pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';
        --pl_dflt_email := 'kathy.poling@hdsupply.com';  --used for testing
    
        pl_ErrorStatus := SendMailJPkg.SendMail(SMTPServerName => p_host --'hsim1.hsi.hughessupply.com'
                                               ,
                                                Sender         => pl_sender --'Oracle.Applications@hughessupply.com'
                                               ,
                                                Recipient      => pl_dflt_email,
                                                CcRecipient    => '',
                                                BccRecipient   => '',
                                                Subject        => 'AssureNET Extract File ' ||
                                                                  l_file_name_recon,
                                                Body           => 'Please review and move the file to AssureNET directory \\ghdglr01wps\gl_upload$. ' ||
                                                                  SendMailJPkg.EOL ||
                                                                  'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                ErrorMessage   => pl_ErrorMessage,
                                                Attachments    => SendMailJPkg.ATTACHMENTS_LIST(l_file_dir || '/' ||
                                                                                                l_file_name_recon));
    
      END IF;
    
      --Delete any existing file   Version 1.2
      BEGIN
        utl_file.fremove(l_file_dir, l_file_name_recon);
      END fremove;
      --
    */
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.create_apsl_corp_two_segs package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.create_apsl_corp_two_segs package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
  END create_apsl_corp_two_segs;

  /********************************************************************************
  
  File Name: XXCUS_GLACCT_RECON_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send subledger balances to AssureNET GL
           (Account Recon Tool) for Utilities.  Utilities will run for product 16.
           Recon will be on Product, Account, Branch, and Cost Center.
  
           Service Ticket 25427
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     12/15/2009    Kathy Poling    Initial creation of the procedure
                                        Service Ticket #25427
  1.1     07/08/2010    Kathy Poling    Removed the JAVA Mailer and file directory for UC4
                                        to pickup the files and deliver to AssureNET.
  2.0     04/10/2011    Luong Vu        Changes to migrate to R12
                                         * XXHSI -> XXCUS      
                                         * additional specific changes are denoted 
                                           at line of change    
  2.1     11/07/2012    Luong Vu        SR 177109 add NVL for SUM function                                            
  ********************************************************************************/

  PROCEDURE create_apsl_foursegs(errbuf      OUT VARCHAR2
                                ,retcode     OUT NUMBER
                                ,p_thru_date IN DATE
                                ,p_parent    IN VARCHAR2) IS
  
    --Intialize Variables
    l_rec_count         NUMBER := 0;
    l_period_name       VARCHAR2(20);
    l_period_num        NUMBER;
    l_file_period       VARCHAR2(20);
    l_year              NUMBER;
    l_thru_date         DATE;
    l_err_msg           VARCHAR2(2000);
    l_err_code          NUMBER;
    l_processed_count   NUMBER := 0;
    l_file_name_recon   VARCHAR2(150);
    l_file_name_temp    VARCHAR2(150);
    l_file_dir          VARCHAR2(100);
    l_file_handle_recon utl_file.file_type;
    l_sid               VARCHAR2(10);
    l_sec               VARCHAR2(150);
    l_procedure_name    VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_apsl_foursegs';
    l_distro_list       fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    --l_distro_list fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    l_calendar     CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_fnd_category CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014550; --Account Category segment4
    l_fnd_product  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547; --Product segment1
    lc_coa         CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --Main Chart of Accounts
  
    pl_dflt_email fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; --'kathy.poling@hdsupply.com';
    --pl_dflt_email   fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
  
  BEGIN
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    SELECT period_year, period_num, period_name
      INTO l_year, l_period_num, l_period_name
      FROM gl.gl_periods
     WHERE p_thru_date BETWEEN start_date AND end_date
       AND period_set_name = l_calendar;
  
    --pl_sender     := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --l_file_dir := '/xx_iface/' || l_sid || '/outbound/uc4';
    l_file_dir := 'ORACLE_INT_GL_RECON'; --Version 1.3
    --l_period_name := p_period_name;
    l_file_period     := REPLACE(l_period_name, '-', '');
    errbuf            := NULL;
    retcode           := NULL;
    l_processed_count := 0;
  
    --Truncate XXCUSGL_BAL_EXTRACT_TBL
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSGL_AP_SUBLEDG_BAL_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_AP_SUBLEDG_BAL_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    -- Get extract information begin
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcusgl_ap_subledg_bal_tbl
      (SELECT --i.invoice_id
        i.code_combination_id
      ,a.segment1
      , --Product
        a.segment2
      , --Account
        a.segment3
      , -- BRANCH/LOCATION
        a.segment4
      , --CC
        a.segment5
      , -- PROJ
        a.segment6
      ,a.segment7 --v2.0
      ,substr(b.description, 1, 50) description
      , --Acct Name
        SUM(nvl(i.acctd_unrounded_dr, 0) - nvl(i.acctd_unrounded_cr, 0)) -- SR 177109
      ,i.ledger_id
      ,prod.attribute2
         FROM xla.xla_trial_balances  i
             ,gl.gl_code_combinations a
             ,apps.fnd_flex_values_vl b
             ,apps.fnd_flex_values_vl prod
             ,xla.xla_ae_headers      aeh
        WHERE a.chart_of_accounts_id = lc_coa
          AND i.ae_header_id = aeh.ae_header_id
          AND i.code_combination_id = a.code_combination_id
          AND trunc(aeh.accounting_date) <= p_thru_date
          AND a.account_type IN ('A', 'L', 'O')
          AND a.segment1 IN
              (SELECT fv2.flex_value
                 FROM applsys.fnd_flex_values_tl  fvt2
                     ,applsys.fnd_flex_values     fv2
                     ,applsys.fnd_flex_value_sets fvs2
                WHERE fvt2.flex_value_id = fv2.flex_value_id
                  AND fv2.parent_flex_value_low = p_parent
                  AND fv2.flex_value_set_id = fvs2.flex_value_set_id
                     --AND fvs2.flex_value_set_name = 'HDS_CHILD_XXHSIGLACCTRECON'
                  AND fvs2.flex_value_set_name = 'XXCUS_CHILD_GLACCTRECON' --v2.0
                  AND fvt2.language = 'US'
                  AND enabled_flag = 'Y')
          AND a.segment4 = b.flex_value_meaning
          AND b.flex_value_set_id = l_fnd_category
          AND a.segment1 = prod.flex_value_meaning
          AND prod.flex_value_set_id = l_fnd_product
        GROUP BY i.code_combination_id
                 --,i.invoice_id
                ,i.ledger_id
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7 --v2.0 
                ,b.description
                ,prod.attribute2
       HAVING SUM(i.acctd_unrounded_cr) <> SUM(i.acctd_unrounded_dr));
  
    COMMIT;
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || 'SBAL_P' || p_parent || 'AP' || '_' ||
                        l_file_period || '.txt';
  
    l_file_name_recon := 'SBAL_P' || p_parent || 'AP' || '_' ||
                         l_file_period || '.txt';
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- Get extract information begin
    FOR c_slbal IN (SELECT segment1
                          ,segment4
                          ,segment2
                          ,segment3
                          ,description
                          ,SUM(invoice_bal) balance
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy1code
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(invoice_bal)
                                          ,'999999999990.99'))
                           END) ccy1glendbalance
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy2code
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(invoice_bal)
                                          ,'999999999990.99'))
                           END) ccy2glendbalance
                      FROM xxcus.xxcusgl_ap_subledg_bal_tbl
                     GROUP BY segment1
                             ,segment4
                             ,segment2
                             ,segment3
                             ,description
                             ,currency_code)
    LOOP
    
      IF c_slbal.ccy1glendbalance <> 0 OR c_slbal.ccy2glendbalance <> 0
      THEN
      
        l_rec_count := l_rec_count + 1;
        utl_file.put_line(l_file_handle_recon
                         ,'P' || c_slbal.segment1 || '|' ||
                          c_slbal.segment4 || '|' || NULL || '|' ||
                          c_slbal.segment2 || '|' || c_slbal.segment3 || '|' || NULL || '|' ||
                          c_slbal.description || '|' || c_slbal.ccy1code || '|' ||
                          c_slbal.ccy1glendbalance || '|' ||
                          c_slbal.ccy2code || '|' ||
                          c_slbal.ccy2glendbalance || '|' || l_period_num || '|' ||
                          l_year);
      
      END IF;
    
      l_processed_count := l_processed_count + 1;
    
    END LOOP;
  
    --utl_file.put_line(l_file_handle_recon, 'FOOTER' || '|' || l_rec_count);  --Removed Oct 15 per Claudia
  
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
  
    /* Version 1.1
      --SEND the exptracted file through email
    
      If l_processed_count > 0 then
        pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';
        --pl_dflt_email := 'kathy.poling@hdsupply.com';  --used for testing
    
        pl_ErrorStatus := SendMailJPkg.SendMail(SMTPServerName => p_host --'hsim1.hsi.hughessupply.com'
                                               ,
                                                Sender         => pl_sender --'Oracle.Applications@hughessupply.com'
                                               ,
                                                Recipient      => pl_dflt_email,
                                                CcRecipient    => '',
                                                BccRecipient   => '',
                                                Subject        => 'AssureNET Extract File ' ||
                                                                  l_file_name_recon,
                                                Body           => 'Please review and move the file to AssureNET directory \\ghdglr01wps\gl_upload$. ' ||
                                                                  SendMailJPkg.EOL ||
                                                                  'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                ErrorMessage   => pl_ErrorMessage,
                                                Attachments    => SendMailJPkg.ATTACHMENTS_LIST(l_file_dir || '/' ||
                                                                                                l_file_name_recon));
    
      END IF;
    
      --Delete any existing file
      BEGIN
        utl_file.fremove(l_file_dir, l_file_name_recon);
      END fremove;
      --
    */
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_APSL_FOURSEGS package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_APSL_FOURSEGS package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
  END create_apsl_foursegs;
  /********************************************************************************
  
  File Name: XXCUS_GLACCT_RECON_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Pull details of Fixed Assets subledger to get balances to AssureNET GL
           (Account Recon Tool)
  
           RFC #9812 Service Ticket 21137
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/11/2009    Kathy Poling    Initial creation of the procedure
  1.2     12/22/2009    Kathy Poling    Changed the depreciation amount RFC #10969
                                        Service Ticket 26190
  1.3     01/08/2010    Kathy Poling    Changed the accum depreciation amount sign
                                        Service Ticket 27590 RFC 11613
  1.4     03/29/3012    Luong Vu        Increase v_dist_book size from 15 to 50.                                                                    
  ********************************************************************************/

  PROCEDURE create_fasl_data(errbuf   OUT VARCHAR2
                            ,retcode  OUT VARCHAR2
                            ,p_book   IN VARCHAR2
                            ,p_period IN VARCHAR2) IS
  
    -- Intialize Variables
    l_err_msg  VARCHAR2(2000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(150);
    l_fnd_category CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014550; --Account Category segment4
    l_fnd_product  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547; --Product segment1
    l_procedure_name VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_fasl_data';
  
    v_dist_book      VARCHAR2(50);
    v_ucd            DATE;
    v_upc            NUMBER;
    v_tod            DATE;
    v_tpc            NUMBER;
    v_reporting_flag VARCHAR2(1);
  
    --Email Defaults
    l_distro_list fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    --l_distro_list fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
  
    --Start Main Program
  BEGIN
    --Create output file
    l_sec := 'Starting Fixed Asset Subledger pull.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    -- get mrc related info
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    BEGIN
      SELECT sob.mrc_sob_type_code
        INTO v_reporting_flag
        FROM apps.fa_book_controls fbc, apps.gl_sets_of_books sob
       WHERE fbc.book_type_code = p_book
         AND fbc.set_of_books_id = sob.set_of_books_id;
    EXCEPTION
      WHEN OTHERS THEN
        v_reporting_flag := 'P';
    END;
  
    IF (v_reporting_flag = 'R')
    THEN
      SELECT bc.distribution_source_book dbk
            ,nvl(dp.period_close_date, SYSDATE) v_ucd
            ,dp.period_counter v_upc
            ,MIN(dp_fy.period_open_date) v_tod
            ,MIN(dp_fy.period_counter) v_tpc
        INTO v_dist_book, v_ucd, v_upc, v_tod, v_tpc
        FROM fa_deprn_periods_mrc_v dp
            ,fa_deprn_periods_mrc_v dp_fy
            ,fa_book_controls_mrc_v bc
       WHERE dp.book_type_code = p_book
         AND dp.period_name = p_period
         AND dp_fy.book_type_code = p_book
         AND dp_fy.fiscal_year = dp.fiscal_year
         AND bc.book_type_code = p_book
       GROUP BY bc.distribution_source_book
               ,dp.period_close_date
               ,dp.period_counter;
      l_sec := 'Inside Reporting Flag : ' || v_reporting_flag;
    ELSE
      SELECT bc.distribution_source_book dbk
            ,nvl(dp.period_close_date, SYSDATE) v_ucd
            ,dp.period_counter v_upc
            ,MIN(dp_fy.period_open_date) v_tod
            ,MIN(dp_fy.period_counter) v_tpc
        INTO v_dist_book, v_ucd, v_upc, v_tod, v_tpc
        FROM fa_deprn_periods dp
            ,fa_deprn_periods dp_fy
            ,fa_book_controls bc
       WHERE dp.book_type_code = p_book
         AND dp.period_name = p_period
         AND dp_fy.book_type_code = p_book
         AND dp_fy.fiscal_year = dp.fiscal_year
         AND bc.book_type_code = p_book
       GROUP BY bc.distribution_source_book
               ,dp.period_close_date
               ,dp.period_counter;
      fnd_file.put_line(fnd_file.log
                       ,'Inside Reporting Flag : ' || v_reporting_flag);
    END IF;
  
    fnd_file.put_line(fnd_file.log, 'v_dist_book :' || v_dist_book);
    fnd_file.put_line(fnd_file.log, 'v_ucd : ' || v_ucd);
    fnd_file.put_line(fnd_file.log, 'v_upc : ' || v_upc);
    fnd_file.put_line(fnd_file.log, 'v_tod : ' || v_tod);
    fnd_file.put_line(fnd_file.log, 'v_tpc : ' || v_tpc);
  
    IF (nvl(fnd_profile.value('CRL-FA ENABLED'), 'N') = 'N')
    THEN
      IF (v_reporting_flag = 'R')
      THEN
        -- Begin if CRL not installed and v_reporting_flag = R
        l_sec := 'Getting into the loop - CRL not installed and v_reporting_flag = R';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      
        INSERT /*+ APPEND */
        INTO xxcus.xxcusgl_fa_subled_tbl
          (asset_id
          ,dh_ccid
          ,deprn_reserve_acct
          ,date_placed_in_service
          ,method_code
          ,life
          ,rate
          ,capacity
          ,cost
          ,deprn_amount
          ,ytd_deprn
          ,deprn_reserve
          ,percent
          ,transaction_type
          ,period_counter
          ,date_effective
          ,reserve_acct
          ,units_assigned
          ,location_id
          ,segment1
          ,segment2
          ,segment3
          ,asset_cost_acct
          ,segment5
          ,cost_acct_desc
          ,dep_resv_desc
          ,currency_code)
          SELECT dh.asset_id asset_id
                ,dh.code_combination_id dh_ccid
                ,cb.deprn_reserve_acct rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code method
                ,books.life_in_months life
                ,books.adjusted_rate rate
                ,books.production_capacity capacity
                ,dd_bonus.cost cost
                ,decode(dd_bonus.period_counter
                       ,v_upc
                       ,dd_bonus.deprn_amount -
                        dd_bonus.bonus_deprn_amount
                       ,0) deprn_amount
                ,decode(sign(v_tpc - dd_bonus.period_counter)
                       ,1
                       ,0
                       ,dd_bonus.ytd_deprn - dd_bonus.bonus_ytd_deprn) ytd_deprn
                ,dd_bonus.deprn_reserve - dd_bonus.bonus_deprn_reserve deprn_reserve
                ,decode(th.transaction_type_code
                       ,NULL
                       ,dh.units_assigned / ah.units * 100) percent
                ,decode(th.transaction_type_code
                       ,NULL
                       ,decode(th_rt.transaction_type_code
                              ,'FULL RETIREMENT'
                              ,'F'
                              ,decode(books.depreciate_flag, 'NO', 'N'))
                       ,'TRANSFER'
                       ,'T'
                       ,'TRANSFER OUT'
                       ,'P'
                       ,'RECLASS'
                       ,'R') t_type
                ,dd_bonus.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,''
                ,dh.units_assigned
                ,dh.location_id
                ,gl.segment1
                ,gl.segment2
                ,gl.segment3
                ,cb.asset_cost_acct
                ,gl.segment5
                ,substr(cost_fx.description, 1, 50) cost_desc
                , --Acct Name
                 substr(dep_fx.description, 1, 50) dep_desc
                , --Acct Name
                 prod.attribute2
            FROM fa_deprn_detail_mrc_v   dd_bonus
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books_mrc_v          books
                ,fa_distribution_history dh
                ,fa_category_books       cb
                ,gl_code_combinations    gl
                ,apps.fnd_flex_values_vl prod
                ,apps.fnd_flex_values_vl cost_fx
                ,apps.fnd_flex_values_vl dep_fx
           WHERE cb.book_type_code = p_book
             AND cb.category_id = ah.category_id
             AND ah.asset_id = dh.asset_id
             AND ah.date_effective < nvl(th.date_effective, v_ucd)
             AND nvl(ah.date_ineffective, SYSDATE) >=
                 nvl(th.date_effective, v_ucd)
             AND ah.asset_type = 'CAPITALIZED'
             AND dd_bonus.book_type_code = p_book
             AND dd_bonus.distribution_id = dh.distribution_id
             AND dd_bonus.period_counter =
                 (SELECT MAX(dd_sub.period_counter)
                    FROM fa_deprn_detail_mrc_v dd_sub
                   WHERE dd_sub.book_type_code = p_book
                     AND dd_sub.asset_id = dh.asset_id
                     AND dd_sub.distribution_id = dh.distribution_id
                     AND dd_sub.period_counter <= v_upc)
             AND th_rt.book_type_code = p_book
             AND th_rt.transaction_header_id =
                 books.transaction_header_id_in
             AND books.book_type_code = p_book
             AND books.asset_id = dh.asset_id
             AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
             AND books.date_effective <= nvl(th.date_effective, v_ucd)
             AND nvl(books.date_ineffective, SYSDATE + 1) >
                 nvl(th.date_effective, v_ucd)
             AND th.book_type_code(+) = v_dist_book
             AND th.transaction_header_id(+) = dh.transaction_header_id_out
             AND th.date_effective(+) BETWEEN v_tod AND v_ucd
             AND dh.book_type_code = v_dist_book
             AND dh.date_effective <= v_ucd
             AND nvl(dh.date_ineffective, SYSDATE) > v_tod
             AND dh.code_combination_id = gl.code_combination_id
             AND cb.asset_cost_acct = cost_fx.flex_value_meaning
             AND cost_fx.flex_value_set_id = l_fnd_category
             AND cb.deprn_reserve_acct = dep_fx.flex_value_meaning
             AND dep_fx.flex_value_set_id = l_fnd_category
             AND gl.segment1 = prod.flex_value_meaning
             AND prod.flex_value_set_id = l_fnd_product
          UNION ALL
          SELECT dh.asset_id asset_id
                ,dh.code_combination_id dh_ccid
                ,cb.bonus_deprn_reserve_acct rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code method
                ,books.life_in_months life
                ,books.adjusted_rate rate
                ,books.production_capacity capacity
                ,0 cost
                ,decode(dd.period_counter, v_upc, dd.bonus_deprn_amount, 0) deprn_amount
                ,decode(sign(v_tpc - dd.period_counter)
                       ,1
                       ,0
                       ,dd.bonus_ytd_deprn) ytd_deprn
                ,dd.bonus_deprn_reserve deprn_reserve
                ,0 percent
                ,'B' t_type
                ,dd.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,cb.bonus_deprn_expense_acct
                ,dh.units_assigned
                ,dh.location_id
                ,gl.segment1
                ,gl.segment2
                ,gl.segment3
                ,cb.asset_cost_acct
                ,gl.segment5
                ,substr(cost_fx.description, 1, 50) cost_desc
                , --Acct Name
                 substr(dep_fx.description, 1, 50) dep_desc
                , --Acct Name
                 prod.attribute2
            FROM fa_deprn_detail_mrc_v   dd
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books_mrc_v          books
                ,fa_distribution_history dh
                ,fa_category_books       cb
                ,gl_code_combinations    gl
                ,apps.fnd_flex_values_vl prod
                ,apps.fnd_flex_values_vl cost_fx
                ,apps.fnd_flex_values_vl dep_fx
           WHERE cb.book_type_code = p_book
             AND cb.category_id = ah.category_id
             AND ah.asset_id = dh.asset_id
             AND ah.date_effective < nvl(th.date_effective, v_ucd)
             AND nvl(ah.date_ineffective, SYSDATE) >=
                 nvl(th.date_effective, v_ucd)
             AND ah.asset_type = 'CAPITALIZED'
             AND dd.book_type_code = p_book
             AND dd.distribution_id = dh.distribution_id
             AND dd.period_counter =
                 (SELECT MAX(dd_sub.period_counter)
                    FROM fa_deprn_detail_mrc_v dd_sub
                   WHERE dd_sub.book_type_code = p_book
                     AND dd_sub.asset_id = dh.asset_id
                     AND dd_sub.distribution_id = dh.distribution_id
                     AND dd_sub.period_counter <= v_upc)
             AND th_rt.book_type_code = p_book
             AND th_rt.transaction_header_id =
                 books.transaction_header_id_in
             AND books.book_type_code = p_book
             AND books.asset_id = dh.asset_id
             AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
             AND books.date_effective <= nvl(th.date_effective, v_ucd)
             AND nvl(books.date_ineffective, SYSDATE + 1) >
                 nvl(th.date_effective, v_ucd)
             AND books.bonus_rule IS NOT NULL
             AND th.book_type_code(+) = v_dist_book
             AND th.transaction_header_id(+) = dh.transaction_header_id_out
             AND th.date_effective(+) BETWEEN v_tod AND v_ucd
             AND dh.book_type_code = v_dist_book
             AND dh.date_effective <= v_ucd
             AND nvl(dh.date_ineffective, SYSDATE) > v_tod
             AND dh.code_combination_id = gl.code_combination_id
             AND cb.asset_cost_acct = cost_fx.flex_value_meaning
             AND cost_fx.flex_value_set_id = l_fnd_category
             AND cb.deprn_reserve_acct = dep_fx.flex_value_meaning
             AND dep_fx.flex_value_set_id = l_fnd_category
             AND gl.segment1 = prod.flex_value_meaning
             AND prod.flex_value_set_id = l_fnd_product;
      
        COMMIT;
        -- End  if CRL not installed and v_reporting_flag = R
      ELSE
        -- Begin if CRL not installed and v_reporting_flag Is NOT R
        l_sec := 'Getting into the loop - CRL not installed and v_reporting_flag Is NOT R';
      
        INSERT /*+ APPEND */
        INTO xxcus.xxcusgl_fa_subled_tbl
          (asset_id
          ,dh_ccid
          ,deprn_reserve_acct
          ,date_placed_in_service
          ,method_code
          ,life
          ,rate
          ,capacity
          ,cost
          ,deprn_amount
          ,ytd_deprn
          ,deprn_reserve
          ,percent
          ,transaction_type
          ,period_counter
          ,date_effective
          ,reserve_acct
          ,units_assigned
          ,location_id
          ,segment1
          ,segment2
          ,segment3
          ,asset_cost_acct
          ,segment5
          ,cost_acct_desc
          ,dep_resv_desc
          ,currency_code)
          SELECT dh.asset_id                  asset_id
                ,dh.code_combination_id       dh_ccid
                ,cb.deprn_reserve_acct        rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code      method
                ,books.life_in_months         life
                ,books.adjusted_rate          rate
                ,books.production_capacity    capacity
                ,dd_bonus.cost                cost
                ,
                 
                 decode(dd_bonus.period_counter
                       ,v_upc
                       ,dd_bonus.deprn_amount - dd_bonus.bonus_deprn_amount
                       ,0) deprn_amount
                ,
                 
                 decode(sign(v_tpc - dd_bonus.period_counter)
                       ,1
                       ,0
                       ,dd_bonus.ytd_deprn - dd_bonus.bonus_ytd_deprn) ytd_deprn
                ,
                 
                 dd_bonus.deprn_reserve - dd_bonus.bonus_deprn_reserve deprn_reserve
                ,decode(th.transaction_type_code
                       ,NULL
                       ,dh.units_assigned / ah.units * 100) percent
                ,decode(th.transaction_type_code
                       ,NULL
                       ,decode(th_rt.transaction_type_code
                              ,'FULL RETIREMENT'
                              ,'F'
                              ,decode(books.depreciate_flag, 'NO', 'N'))
                       ,'TRANSFER'
                       ,'T'
                       ,'TRANSFER OUT'
                       ,'P'
                       ,'RECLASS'
                       ,'R') t_type
                ,dd_bonus.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,''
                ,dh.units_assigned
                ,dh.location_id
                ,gl.segment1
                ,gl.segment2
                ,gl.segment3
                ,cb.asset_cost_acct
                ,gl.segment5
                ,substr(cost_fx.description, 1, 50) cost_desc
                , --Acct Name
                 substr(dep_fx.description, 1, 50) dep_desc
                , --Acct Name
                 prod.attribute2
            FROM fa_deprn_detail         dd_bonus
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books                books
                ,fa_distribution_history dh
                ,fa_category_books       cb
                ,gl_code_combinations    gl
                ,apps.fnd_flex_values_vl prod
                ,apps.fnd_flex_values_vl cost_fx
                ,apps.fnd_flex_values_vl dep_fx
           WHERE cb.book_type_code = p_book
             AND cb.category_id = ah.category_id
             AND ah.asset_id = dh.asset_id
             AND ah.date_effective < nvl(th.date_effective, v_ucd)
             AND nvl(ah.date_ineffective, SYSDATE) >=
                 nvl(th.date_effective, v_ucd)
             AND ah.asset_type = 'CAPITALIZED'
             AND dd_bonus.book_type_code = p_book
             AND dd_bonus.distribution_id = dh.distribution_id
             AND dd_bonus.period_counter =
                 (SELECT MAX(dd_sub.period_counter)
                    FROM fa_deprn_detail dd_sub
                   WHERE dd_sub.book_type_code = p_book
                     AND dd_sub.asset_id = dh.asset_id
                     AND dd_sub.distribution_id = dh.distribution_id
                     AND dd_sub.period_counter <= v_upc)
             AND th_rt.book_type_code = p_book
             AND th_rt.transaction_header_id =
                 books.transaction_header_id_in
             AND books.book_type_code = p_book
             AND books.asset_id = dh.asset_id
             AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
             AND books.date_effective <= nvl(th.date_effective, v_ucd)
             AND nvl(books.date_ineffective, SYSDATE + 1) >
                 nvl(th.date_effective, v_ucd)
             AND th.book_type_code(+) = v_dist_book
             AND th.transaction_header_id(+) = dh.transaction_header_id_out
             AND th.date_effective(+) BETWEEN v_tod AND v_ucd
             AND dh.book_type_code = v_dist_book
             AND dh.date_effective <= v_ucd
             AND nvl(dh.date_ineffective, SYSDATE) > v_tod
             AND dh.code_combination_id = gl.code_combination_id
             AND cb.asset_cost_acct = cost_fx.flex_value_meaning
             AND cost_fx.flex_value_set_id = l_fnd_category
             AND cb.deprn_reserve_acct = dep_fx.flex_value_meaning
             AND dep_fx.flex_value_set_id = l_fnd_category
             AND gl.segment1 = prod.flex_value_meaning
             AND prod.flex_value_set_id = l_fnd_product
          UNION ALL
          SELECT dh.asset_id asset_id
                ,dh.code_combination_id dh_ccid
                ,cb.bonus_deprn_reserve_acct rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code method
                ,books.life_in_months life
                ,books.adjusted_rate rate
                ,books.production_capacity capacity
                ,0 cost
                ,decode(dd.period_counter, v_upc, dd.bonus_deprn_amount, 0) deprn_amount
                ,decode(sign(v_tpc - dd.period_counter)
                       ,1
                       ,0
                       ,dd.bonus_ytd_deprn) ytd_deprn
                ,dd.bonus_deprn_reserve deprn_reserve
                ,0 percent
                ,'B' t_type
                ,dd.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,cb.bonus_deprn_expense_acct
                ,dh.units_assigned
                ,dh.location_id
                ,gl.segment1
                ,gl.segment2
                ,gl.segment3
                ,cb.asset_cost_acct
                ,gl.segment5
                ,substr(cost_fx.description, 1, 50) cost_desc
                , --Acct Name
                 substr(dep_fx.description, 1, 50) dep_desc
                , --Acct Name
                 prod.attribute2
            FROM fa_deprn_detail         dd
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books                books
                ,fa_distribution_history dh
                ,fa_category_books       cb
                ,gl_code_combinations    gl
                ,apps.fnd_flex_values_vl prod
                ,apps.fnd_flex_values_vl cost_fx
                ,apps.fnd_flex_values_vl dep_fx
           WHERE cb.book_type_code = p_book
             AND cb.category_id = ah.category_id
             AND ah.asset_id = dh.asset_id
             AND ah.date_effective < nvl(th.date_effective, v_ucd)
             AND nvl(ah.date_ineffective, SYSDATE) >=
                 nvl(th.date_effective, v_ucd)
             AND ah.asset_type = 'CAPITALIZED'
             AND dd.book_type_code = p_book
             AND dd.distribution_id = dh.distribution_id
             AND dd.period_counter =
                 (SELECT MAX(dd_sub.period_counter)
                    FROM fa_deprn_detail dd_sub
                   WHERE dd_sub.book_type_code = p_book
                     AND dd_sub.asset_id = dh.asset_id
                     AND dd_sub.distribution_id = dh.distribution_id
                     AND dd_sub.period_counter <= v_upc)
             AND th_rt.book_type_code = p_book
             AND th_rt.transaction_header_id =
                 books.transaction_header_id_in
             AND books.book_type_code = p_book
             AND books.asset_id = dh.asset_id
             AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
             AND books.date_effective <= nvl(th.date_effective, v_ucd)
             AND nvl(books.date_ineffective, SYSDATE + 1) >
                 nvl(th.date_effective, v_ucd)
             AND books.bonus_rule IS NOT NULL
             AND th.book_type_code(+) = v_dist_book
             AND th.transaction_header_id(+) = dh.transaction_header_id_out
             AND th.date_effective(+) BETWEEN v_tod AND v_ucd
             AND dh.book_type_code = v_dist_book
             AND dh.date_effective <= v_ucd
             AND nvl(dh.date_ineffective, SYSDATE) > v_tod
             AND dh.code_combination_id = gl.code_combination_id
             AND cb.asset_cost_acct = cost_fx.flex_value_meaning
             AND cost_fx.flex_value_set_id = l_fnd_category
             AND cb.deprn_reserve_acct = dep_fx.flex_value_meaning
             AND dep_fx.flex_value_set_id = l_fnd_category
             AND gl.segment1 = prod.flex_value_meaning
             AND prod.flex_value_set_id = l_fnd_product;
      
        COMMIT;
        -- End if CRL not installed and v_reporting_flag Is NOT R
      END IF;
    ELSIF (nvl(fnd_profile.value('CRL-FA ENABLED'), 'N') = 'Y')
    THEN
      -- Insert Non-Group Details
      l_sec := 'Inserting Non-Group Details';
    
      IF (v_reporting_flag = 'R')
      THEN
        INSERT /*+ APPEND */
        INTO xxcus.xxcusgl_fa_subled_tbl
          (asset_id
          ,dh_ccid
          ,deprn_reserve_acct
          ,date_placed_in_service
          ,method_code
          ,life
          ,rate
          ,capacity
          ,cost
          ,deprn_amount
          ,ytd_deprn
          ,deprn_reserve
          ,percent
          ,transaction_type
          ,period_counter
          ,date_effective
          ,units_assigned
          ,location_id
          ,segment1
          ,segment2
          ,segment3
          ,asset_cost_acct
          ,segment5
          ,cost_acct_desc
          ,dep_resv_desc
          ,currency_code)
          SELECT dh.asset_id asset_id
                ,dh.code_combination_id dh_ccid
                ,cb.deprn_reserve_acct rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code method
                ,books.life_in_months life
                ,books.adjusted_rate rate
                ,books.production_capacity capacity
                ,dd.cost cost
                ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
                ,decode(sign(v_tpc - dd.period_counter)
                       ,1
                       ,0
                       ,dd.ytd_deprn) ytd_deprn
                ,dd.deprn_reserve deprn_reserve
                ,decode(th.transaction_type_code
                       ,NULL
                       ,dh.units_assigned / ah.units * 100) percent
                ,decode(th.transaction_type_code
                       ,NULL
                       ,decode(th_rt.transaction_type_code
                              ,'FULL RETIREMENT'
                              ,'F'
                              ,decode(books.depreciate_flag, 'NO', 'N'))
                       ,'TRANSFER'
                       ,'T'
                       ,'TRANSFER OUT'
                       ,'P'
                       ,'RECLASS'
                       ,'R') t_type
                ,dd.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,dh.units_assigned
                ,dh.location_id
                ,gl.segment1
                ,gl.segment2
                ,gl.segment3
                ,cb.asset_cost_acct
                ,gl.segment5
                ,substr(cost_fx.description, 1, 50) cost_desc
                , --Acct Name
                 substr(dep_fx.description, 1, 50) dep_desc
                , --Acct Name
                 prod.attribute2
            FROM fa_deprn_detail_mrc_v   dd
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books_mrc_v          books
                ,fa_distribution_history dh
                ,fa_category_books       cb
                ,gl_code_combinations    gl
                ,apps.fnd_flex_values_vl prod
                ,apps.fnd_flex_values_vl cost_fx
                ,apps.fnd_flex_values_vl dep_fx
           WHERE books.group_asset_id IS NULL
             AND cb.book_type_code = p_book
             AND cb.category_id = ah.category_id
             AND ah.asset_id = dh.asset_id
             AND ah.date_effective < nvl(th.date_effective, v_ucd)
             AND nvl(ah.date_ineffective, SYSDATE) >=
                 nvl(th.date_effective, v_ucd)
             AND ah.asset_type = 'CAPITALIZED'
             AND dd.book_type_code = p_book
             AND dd.distribution_id = dh.distribution_id
             AND dd.period_counter =
                 (SELECT MAX(dd_sub.period_counter)
                    FROM fa_deprn_detail_mrc_v dd_sub
                   WHERE dd_sub.book_type_code = p_book
                     AND dd_sub.asset_id = dh.asset_id
                     AND dd_sub.distribution_id = dh.distribution_id
                     AND dd_sub.period_counter <= v_upc)
             AND th_rt.book_type_code = p_book
             AND th_rt.transaction_header_id =
                 books.transaction_header_id_in
             AND books.book_type_code = p_book
             AND books.asset_id = dh.asset_id
             AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
             AND books.date_effective <= nvl(th.date_effective, v_ucd)
             AND nvl(books.date_ineffective, SYSDATE + 1) >
                 nvl(th.date_effective, v_ucd)
             AND th.book_type_code(+) = v_dist_book
             AND th.transaction_header_id(+) = dh.transaction_header_id_out
             AND th.date_effective(+) BETWEEN v_tod AND v_ucd
             AND dh.book_type_code = v_dist_book
             AND dh.date_effective <= v_ucd
             AND nvl(dh.date_ineffective, SYSDATE) > v_tod
             AND books.group_asset_id IS NULL
             AND dh.code_combination_id = gl.code_combination_id
             AND cb.asset_cost_acct = cost_fx.flex_value_meaning
             AND cost_fx.flex_value_set_id = l_fnd_category
             AND cb.deprn_reserve_acct = dep_fx.flex_value_meaning
             AND dep_fx.flex_value_set_id = l_fnd_category
             AND gl.segment1 = prod.flex_value_meaning
             AND prod.flex_value_set_id = l_fnd_product;
      
        COMMIT;
      ELSE
        INSERT /*+ APPEND */
        INTO xxcus.xxcusgl_fa_subled_tbl
          (asset_id
          ,dh_ccid
          ,deprn_reserve_acct
          ,date_placed_in_service
          ,method_code
          ,life
          ,rate
          ,capacity
          ,cost
          ,deprn_amount
          ,ytd_deprn
          ,deprn_reserve
          ,percent
          ,transaction_type
          ,period_counter
          ,date_effective
          ,units_assigned
          ,location_id
          ,segment1
          ,segment2
          ,segment3
          ,asset_cost_acct
          ,segment5
          ,cost_acct_desc
          ,dep_resv_desc
          ,currency_code)
          SELECT dh.asset_id asset_id
                ,dh.code_combination_id dh_ccid
                ,cb.deprn_reserve_acct rsv_account
                ,books.date_placed_in_service start_date
                ,books.deprn_method_code method
                ,books.life_in_months life
                ,books.adjusted_rate rate
                ,books.production_capacity capacity
                ,dd.cost cost
                ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
                ,decode(sign(v_tpc - dd.period_counter)
                       ,1
                       ,0
                       ,dd.ytd_deprn) ytd_deprn
                ,dd.deprn_reserve deprn_reserve
                ,decode(th.transaction_type_code
                       ,NULL
                       ,dh.units_assigned / ah.units * 100) percent
                ,decode(th.transaction_type_code
                       ,NULL
                       ,decode(th_rt.transaction_type_code
                              ,'FULL RETIREMENT'
                              ,'F'
                              ,decode(books.depreciate_flag, 'NO', 'N'))
                       ,'TRANSFER'
                       ,'T'
                       ,'TRANSFER OUT'
                       ,'P'
                       ,'RECLASS'
                       ,'R') t_type
                ,dd.period_counter
                ,nvl(th.date_effective, v_ucd)
                ,dh.units_assigned
                ,dh.location_id
                ,gl.segment1
                ,gl.segment2
                ,gl.segment3
                ,cb.asset_cost_acct
                ,gl.segment5
                ,substr(cost_fx.description, 1, 50) cost_desc
                , --Acct Name
                 substr(dep_fx.description, 1, 50) dep_desc
                , --Acct Name
                 prod.attribute2
            FROM fa_deprn_detail         dd
                ,fa_asset_history        ah
                ,fa_transaction_headers  th
                ,fa_transaction_headers  th_rt
                ,fa_books                books
                ,fa_distribution_history dh
                ,fa_category_books       cb
                ,gl_code_combinations    gl
                ,apps.fnd_flex_values_vl prod
                ,apps.fnd_flex_values_vl cost_fx
                ,apps.fnd_flex_values_vl dep_fx
           WHERE books.group_asset_id IS NULL
             AND cb.book_type_code = p_book
             AND cb.category_id = ah.category_id
             AND ah.asset_id = dh.asset_id
             AND ah.date_effective < nvl(th.date_effective, v_ucd)
             AND nvl(ah.date_ineffective, SYSDATE) >=
                 nvl(th.date_effective, v_ucd)
             AND ah.asset_type = 'CAPITALIZED'
             AND dd.book_type_code = p_book
             AND dd.distribution_id = dh.distribution_id
             AND dd.period_counter =
                 (SELECT MAX(dd_sub.period_counter)
                    FROM fa_deprn_detail dd_sub
                   WHERE dd_sub.book_type_code = p_book
                     AND dd_sub.asset_id = dh.asset_id
                     AND dd_sub.distribution_id = dh.distribution_id
                     AND dd_sub.period_counter <= v_upc)
             AND th_rt.book_type_code = p_book
             AND th_rt.transaction_header_id =
                 books.transaction_header_id_in
             AND books.book_type_code = p_book
             AND books.asset_id = dh.asset_id
             AND nvl(books.period_counter_fully_retired, v_upc) >= v_tpc
             AND books.date_effective <= nvl(th.date_effective, v_ucd)
             AND nvl(books.date_ineffective, SYSDATE + 1) >
                 nvl(th.date_effective, v_ucd)
             AND th.book_type_code(+) = v_dist_book
             AND th.transaction_header_id(+) = dh.transaction_header_id_out
             AND th.date_effective(+) BETWEEN v_tod AND v_ucd
             AND dh.book_type_code = v_dist_book
             AND dh.date_effective <= v_ucd
             AND nvl(dh.date_ineffective, SYSDATE) > v_tod
             AND books.group_asset_id IS NULL
             AND dh.code_combination_id = gl.code_combination_id
             AND cb.asset_cost_acct = cost_fx.flex_value_meaning
             AND cost_fx.flex_value_set_id = l_fnd_category
             AND cb.deprn_reserve_acct = dep_fx.flex_value_meaning
             AND dep_fx.flex_value_set_id = l_fnd_category
             AND gl.segment1 = prod.flex_value_meaning
             AND prod.flex_value_set_id = l_fnd_product;
      END IF;
    
      COMMIT;
      -- IF (v_reporting_flag = 'R') THEN
    END IF; -- IF (nvl(fnd_profile.value('CRL-FA ENABLED'), 'N') = 'N' ) THEN
  
    -- Insert the Group Depreciation Details
    l_sec := 'Inserting the Group Depreciation Details';
  
    IF (v_reporting_flag = 'R')
    THEN
      INSERT /*+ APPEND */
      INTO xxcus.xxcusgl_fa_subled_tbl
        (asset_id
        ,dh_ccid
        ,deprn_reserve_acct
        ,date_placed_in_service
        ,method_code
        ,life
        ,rate
        ,capacity
        ,cost
        ,deprn_amount
        ,ytd_deprn
        ,deprn_reserve
        ,percent
        ,transaction_type
        ,period_counter
        ,date_effective
        ,units_assigned
        ,location_id
        ,segment1
        ,segment2
        ,segment3
        ,asset_cost_acct
        ,segment5
        ,cost_acct_desc
        ,dep_resv_desc
        ,currency_code)
        SELECT gar.group_asset_id asset_id
              ,gad.deprn_expense_acct_ccid ch_ccid
              ,gad.deprn_reserve_acct rsv_account
              ,gar.deprn_start_date start_date
              ,gar.deprn_method_code method
              ,gar.life_in_months life
              ,gar.adjusted_rate rate
              ,gar.production_capacity capacity
              ,dd.adjusted_cost cost
              ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
              ,decode(sign(v_tpc - dd.period_counter), 1, 0, dd.ytd_deprn) ytd_deprn
              ,dd.deprn_reserve deprn_reserve
              ,100 percent
              ,'G' t_type
              ,dd.period_counter
              ,v_ucd
              ,NULL
              ,NULL
              ,gl.segment1
              ,gl.segment2
              ,gl.segment3
              ,gad.asset_cost_acct
              ,gl.segment5
              ,substr(cost_fx.description, 1, 50) cost_desc
              , --Acct Name
               substr(dep_fx.description, 1, 50) dep_desc
              , --Acct Name
               prod.attribute2
          FROM fa_deprn_summary_mrc_v  dd
              ,fa_group_asset_rules    gar
              ,fa_group_asset_default  gad
              ,fa_deprn_periods_mrc_v  dp
              ,gl_code_combinations    gl
              ,apps.fnd_flex_values_vl prod
              ,apps.fnd_flex_values_vl cost_fx
              ,apps.fnd_flex_values_vl dep_fx
         WHERE dd.book_type_code = p_book
           AND dd.asset_id = gar.group_asset_id
           AND gad.super_group_id IS NULL
           AND gar.book_type_code = dd.book_type_code
           AND gad.book_type_code = gar.book_type_code
           AND gad.group_asset_id = gar.group_asset_id
           AND dd.period_counter =
               (SELECT MAX(dd_sub.period_counter)
                  FROM fa_deprn_detail_mrc_v dd_sub
                 WHERE dd_sub.book_type_code = p_book
                   AND dd_sub.asset_id = gar.group_asset_id
                   AND dd_sub.period_counter <= v_upc)
           AND dd.period_counter = dp.period_counter
           AND dd.book_type_code = dp.book_type_code
           AND gar.date_effective <= dp.calendar_period_close_date
           AND nvl(gar.date_ineffective
                  ,(dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date
           AND gad.deprn_expense_acct_ccid = gl.code_combination_id
           AND gad.asset_cost_acct = cost_fx.flex_value_meaning
           AND cost_fx.flex_value_set_id = l_fnd_category
           AND gad.deprn_reserve_acct = dep_fx.flex_value_meaning
           AND dep_fx.flex_value_set_id = l_fnd_category
           AND gl.segment1 = prod.flex_value_meaning
           AND prod.flex_value_set_id = l_fnd_product;
    
      COMMIT;
    ELSE
      -- IF (v_reporting_flag = 'R') THEN
      INSERT /*+ APPEND */
      INTO xxcus.xxcusgl_fa_subled_tbl
        (asset_id
        ,dh_ccid
        ,deprn_reserve_acct
        ,date_placed_in_service
        ,method_code
        ,life
        ,rate
        ,capacity
        ,cost
        ,deprn_amount
        ,ytd_deprn
        ,deprn_reserve
        ,percent
        ,transaction_type
        ,period_counter
        ,date_effective
        ,units_assigned
        ,location_id
        ,segment1
        ,segment2
        ,segment3
        ,asset_cost_acct
        ,segment5
        ,cost_acct_desc
        ,dep_resv_desc
        ,currency_code)
        SELECT gar.group_asset_id asset_id
              ,gad.deprn_expense_acct_ccid ch_ccid
              ,gad.deprn_reserve_acct_ccid rsv_account
              ,gar.deprn_start_date start_date
              ,gar.deprn_method_code method
              ,gar.life_in_months life
              ,gar.adjusted_rate rate
              ,gar.production_capacity capacity
              ,dd.adjusted_cost cost
              ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
              ,decode(sign(v_tpc - dd.period_counter), 1, 0, dd.ytd_deprn) ytd_deprn
              ,dd.deprn_reserve deprn_reserve
              ,100 percent
              ,'G' t_type
              ,dd.period_counter
              ,v_ucd
              ,NULL
              ,NULL
              ,gl.segment1
              ,gl.segment2
              ,gl.segment3
              ,gad.asset_cost_acct
              ,gl.segment5
              ,substr(cost_fx.description, 1, 50) cost_desc
              , --Acct Name
               substr(dep_fx.description, 1, 50) dep_desc
              , --Acct Name
               prod.attribute2
          FROM fa_deprn_summary        dd
              ,fa_group_asset_rules    gar
              ,fa_group_asset_default  gad
              ,fa_deprn_periods        dp
              ,gl_code_combinations    gl
              ,apps.fnd_flex_values_vl prod
              ,apps.fnd_flex_values_vl cost_fx
              ,apps.fnd_flex_values_vl dep_fx
         WHERE dd.book_type_code = p_book
           AND dd.asset_id = gar.group_asset_id
           AND gad.super_group_id IS NULL
           AND gar.book_type_code = dd.book_type_code
           AND gad.book_type_code = gar.book_type_code
           AND gad.group_asset_id = gar.group_asset_id
           AND dd.period_counter =
               (SELECT MAX(dd_sub.period_counter)
                  FROM fa_deprn_detail dd_sub
                 WHERE dd_sub.book_type_code = p_book
                   AND dd_sub.asset_id = gar.group_asset_id
                   AND dd_sub.period_counter <= v_upc)
           AND dd.period_counter = dp.period_counter
           AND dd.book_type_code = dp.book_type_code
           AND gar.date_effective <= dp.calendar_period_close_date
           AND nvl(gar.date_ineffective
                  ,(dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date
           AND gad.deprn_expense_acct_ccid = gl.code_combination_id
           AND gad.asset_cost_acct = cost_fx.flex_value_meaning
           AND cost_fx.flex_value_set_id = l_fnd_category
           AND gad.deprn_reserve_acct = dep_fx.flex_value_meaning
           AND dep_fx.flex_value_set_id = l_fnd_category
           AND gl.segment1 = prod.flex_value_meaning
           AND prod.flex_value_set_id = l_fnd_product;
    
      COMMIT;
    
    END IF; -- IF (v_reporting_flag = 'R') THEN
  
    -- Insert the SuperGroup Depreciation Details
    l_sec := 'Inserting the SuperGroup Depreciation Details';
  
    IF (v_reporting_flag = 'R')
    THEN
      INSERT /*+ APPEND */
      INTO xxcus.xxcusgl_fa_subled_tbl
        (asset_id
        ,dh_ccid
        ,deprn_reserve_acct
        ,date_placed_in_service
        ,method_code
        ,life
        ,rate
        ,capacity
        ,cost
        ,deprn_amount
        ,ytd_deprn
        ,deprn_reserve
        ,percent
        ,transaction_type
        ,period_counter
        ,date_effective
        ,units_assigned
        ,location_id
        ,segment1
        ,segment2
        ,segment3
        ,asset_cost_acct
        ,segment5
        ,cost_acct_desc
        ,dep_resv_desc
        ,currency_code)
        SELECT gar.group_asset_id asset_id
              ,gad.deprn_expense_acct_ccid dh_ccid
              ,gad.deprn_reserve_acct_ccid rsv_account
              ,gar.deprn_start_date start_date
              ,sgr.deprn_method_code method
              ,gar.life_in_months life
              ,sgr.adjusted_rate rate
              ,gar.production_capacity capacity
              ,dd.adjusted_cost cost
              ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
              ,decode(sign(v_tpc - dd.period_counter), 1, 0, dd.ytd_deprn) ytd_deprn
              ,dd.deprn_reserve deprn_reserve
              ,100 percent
              ,'G' t_type
              ,dd.period_counter
              ,v_ucd
              ,NULL
              ,NULL
              ,gl.segment1
              ,gl.segment2
              ,gl.segment3
              ,gad.asset_cost_acct
              ,gl.segment5
              ,substr(cost_fx.description, 1, 50) cost_desc
              , --Acct Name
               substr(dep_fx.description, 1, 50) dep_desc
              , --Acct Name
               prod.attribute2
          FROM fa_deprn_summary_mrc_v  dd
              ,fa_group_asset_rules    gar
              ,fa_group_asset_default  gad
              ,fa_super_group_rules    sgr
              ,fa_deprn_periods_mrc_v  dp
              ,gl_code_combinations    gl
              ,apps.fnd_flex_values_vl prod
              ,apps.fnd_flex_values_vl cost_fx
              ,apps.fnd_flex_values_vl dep_fx
         WHERE dd.book_type_code = p_book
           AND dd.asset_id = gar.group_asset_id
           AND gar.book_type_code = dd.book_type_code
           AND gad.super_group_id = sgr.super_group_id
           AND gad.book_type_code = sgr.book_type_code
           AND gad.book_type_code = gar.book_type_code
           AND gad.group_asset_id = gar.group_asset_id
           AND dd.period_counter =
               (SELECT MAX(dd_sub.period_counter)
                  FROM fa_deprn_detail_mrc_v dd_sub
                 WHERE dd_sub.book_type_code = p_book
                   AND dd_sub.asset_id = gar.group_asset_id
                   AND dd_sub.period_counter <= v_upc)
           AND dd.period_counter = dp.period_counter
           AND dd.book_type_code = dp.book_type_code
           AND gar.date_effective <= dp.calendar_period_close_date
           AND nvl(gar.date_ineffective
                  ,(dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date
           AND sgr.date_effective <= dp.calendar_period_close_date
           AND nvl(sgr.date_ineffective
                  ,(dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date
           AND gad.deprn_expense_acct_ccid = gl.code_combination_id
           AND gad.asset_cost_acct = cost_fx.flex_value_meaning
           AND cost_fx.flex_value_set_id = l_fnd_category
           AND gad.deprn_reserve_acct = dep_fx.flex_value_meaning
           AND dep_fx.flex_value_set_id = l_fnd_category
           AND gl.segment1 = prod.flex_value_meaning
           AND prod.flex_value_set_id = l_fnd_product;
    
      COMMIT;
    ELSE
      INSERT /*+ APPEND */
      INTO xxcus.xxcusgl_fa_subled_tbl
        (asset_id
        ,dh_ccid
        ,deprn_reserve_acct
        ,date_placed_in_service
        ,method_code
        ,life
        ,rate
        ,capacity
        ,cost
        ,deprn_amount
        ,ytd_deprn
        ,deprn_reserve
        ,percent
        ,transaction_type
        ,period_counter
        ,date_effective
        ,units_assigned
        ,location_id
        ,segment1
        ,segment2
        ,segment3
        ,asset_cost_acct
        ,segment5
        ,cost_acct_desc
        ,dep_resv_desc
        ,currency_code)
        SELECT gar.group_asset_id asset_id
              ,gad.deprn_expense_acct_ccid dh_ccid
              ,gad.deprn_reserve_acct_ccid rsv_account
              ,gar.deprn_start_date start_date
              ,sgr.deprn_method_code method
              ,gar.life_in_months life
              ,sgr.adjusted_rate rate
              ,gar.production_capacity capacity
              ,dd.adjusted_cost cost
              ,decode(dd.period_counter, v_upc, dd.deprn_amount, 0) deprn_amount
              ,decode(sign(v_tpc - dd.period_counter), 1, 0, dd.ytd_deprn) ytd_deprn
              ,dd.deprn_reserve deprn_reserve
              ,100 percent
              ,'G' t_type
              ,dd.period_counter
              ,v_ucd
              ,NULL
              ,NULL
              ,gl.segment1
              ,gl.segment2
              ,gl.segment3
              ,gad.asset_cost_acct
              ,gl.segment5
              ,substr(cost_fx.description, 1, 50) cost_desc
              , --Acct Name
               substr(dep_fx.description, 1, 50) dep_desc
              , --Acct Name
               prod.attribute2
          FROM fa_deprn_summary        dd
              ,fa_group_asset_rules    gar
              ,fa_group_asset_default  gad
              ,fa_super_group_rules    sgr
              ,fa_deprn_periods        dp
              ,gl_code_combinations    gl
              ,apps.fnd_flex_values_vl prod
              ,apps.fnd_flex_values_vl cost_fx
              ,apps.fnd_flex_values_vl dep_fx
         WHERE dd.book_type_code = p_book
           AND dd.asset_id = gar.group_asset_id
           AND gar.book_type_code = dd.book_type_code
           AND gad.super_group_id = sgr.super_group_id
           AND gad.book_type_code = sgr.book_type_code
           AND gad.book_type_code = gar.book_type_code
           AND gad.group_asset_id = gar.group_asset_id
           AND dd.period_counter =
               (SELECT MAX(dd_sub.period_counter)
                  FROM fa_deprn_detail dd_sub
                 WHERE dd_sub.book_type_code = p_book
                   AND dd_sub.asset_id = gar.group_asset_id
                   AND dd_sub.period_counter <= v_upc)
           AND dd.period_counter = dp.period_counter
           AND dd.book_type_code = dp.book_type_code
           AND gar.date_effective <= dp.calendar_period_close_date
           AND nvl(gar.date_ineffective
                  ,(dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date
           AND sgr.date_effective <= dp.calendar_period_close_date
           AND nvl(sgr.date_ineffective
                  ,(dp.calendar_period_close_date + 1)) >
               dp.calendar_period_close_date
           AND gad.deprn_expense_acct_ccid = gl.code_combination_id
           AND gad.asset_cost_acct = cost_fx.flex_value_meaning
           AND cost_fx.flex_value_set_id = l_fnd_category
           AND gad.deprn_reserve_acct = dep_fx.flex_value_meaning
           AND dep_fx.flex_value_set_id = l_fnd_category
           AND gl.segment1 = prod.flex_value_meaning
           AND prod.flex_value_set_id = l_fnd_product;
    
      COMMIT;
    END IF;
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcusgl_fa_sl_bal_tbl
      (segment1
      ,segment2
      ,segment3
      ,segment4
      ,segment5
      ,acct_desc
      ,amount
      ,currency_code)
      SELECT segment1
            ,segment2
            ,segment3
            ,asset_cost_acct account
            ,segment5
            ,cost_acct_desc description
            ,SUM(cost) amount
            ,currency_code
        FROM xxcus.xxcusgl_fa_subled_tbl
       GROUP BY asset_cost_acct
               ,cost_acct_desc
               ,segment1
               ,segment2
               ,segment3
               ,segment5
               ,currency_code
      UNION ALL
      SELECT segment1
            ,segment2
            ,segment3
            ,deprn_reserve_acct account
            ,segment5
            ,dep_resv_desc      description
            ,
             --sum(deprn_reserve - deprn_amount) amount,  -- Version 1.2
             --sum(deprn_reserve) amount,                 -- Version 1.2
             SUM(deprn_reserve * -1) amount
            , -- Version 1.3
             currency_code
        FROM xxcus.xxcusgl_fa_subled_tbl
       GROUP BY deprn_reserve_acct
               ,dep_resv_desc
               ,segment1
               ,segment2
               ,segment3
               ,segment5
               ,currency_code;
    COMMIT;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_FASL_DATA package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_FASL_DATA package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
  END create_fasl_data;

  /********************************************************************************
  
  File Name: XXCUS_GLACCT_RECON_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send Fixed Assets subledger balances to AssureNET GL
           (Account Recon Tool) for Corporate.
  
           Recon will be on Parent Product and Account.
  
           Plumbing, Electrical and IPVF
  
           Service Ticket 21137  RFC 9812
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/18/2009    Kathy Poling    Initial creation of the procedure
  1.2     12/22/2009                    Added JAVA Mailer to email the file and delete
                                        the file in the first part of procedure if they
                                        run the file multiply times during a month.
                                        Changed flex_value_set_id to flex_value_set_name
                                        RFC #10969
  1.3     07/08/2010    Kathy Poling    Removed the JAVA Mailer and file directory for UC4
                                        to pickup the files and deliver to AssureNET.
  1.4     10/04/2010    Luong Vu        For corporate, add "A" prefix for accnt number
                                        Service Ticket # 51558
  1.5     11/09/2010    Luong Vu        Modified data pull to use 2 separate cursors
                                        one for Corp and one for other LOBs                                              
  ********************************************************************************/

  PROCEDURE create_fasl_twosegs(errbuf   OUT VARCHAR2
                               ,retcode  OUT VARCHAR2
                               ,p_period IN VARCHAR2
                               ,p_parent IN VARCHAR2) IS
  
    -- Intialize Variables
    l_err_msg         VARCHAR2(2000);
    l_err_code        NUMBER;
    l_processed_count NUMBER := 0; --Version 1.2
    l_file_name_recon VARCHAR2(150);
    l_file_name_temp  VARCHAR2(150);
    l_file_dir        VARCHAR2(100);
    l_file_period     VARCHAR2(20);
    l_period_name     VARCHAR2(20);
    l_year            NUMBER;
    l_period_num      NUMBER;
    l_sid             VARCHAR2(15);
    l_lob_name        VARCHAR2(20); -- v.1.4
  
    l_file_handle_recon utl_file.file_type;
    l_sec               VARCHAR2(150);
    l_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    --l_fnd_product CONSTANT FND_FLEX_VALUES_VL.flex_value_set_id%TYPE := 1010925; --Parent to select FA Book  Version 1.2
    l_procedure_name VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_fasl_twosegs';
  
    --Email Defaults
    l_distro_list fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    --Version 1.2
    pl_dflt_email   fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; --'kathy.poling@hdsupply.com';
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
    --
  
    --Start Main Program
  BEGIN
    -- Truncate Data from Staging table
    BEGIN
      l_sec := 'Truncate the detail table before loading specified time frame.';
      EXECUTE IMMEDIATE 'truncate table XXCUS.XXCUSGL_FA_SUBLED_TBL';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_FA_SUBLED_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'truncate table XXCUS.XXCUSGL_FA_SL_BAL_TBL';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_FA_SL_BAL_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    BEGIN
      l_sec := 'Calling create_fasl_data procedure';
    
      FOR c_book IN (SELECT fv2.flex_value
                       FROM applsys.fnd_flex_values_tl  fvt2
                           ,applsys.fnd_flex_values     fv2
                           ,applsys.fnd_flex_value_sets fvs2
                      WHERE fvt2.flex_value_id = fv2.flex_value_id
                        AND fv2.parent_flex_value_low = p_parent
                        AND fv2.flex_value_set_id = fvs2.flex_value_set_id
                           --AND fvs2.flex_value_set_name = 'HDS_FA_CHILD_XXHSIFAACCTRECON'
                        AND fvs2.flex_value_set_name =
                            'XXCUS_FA_CHILD_FAACCTRECON' --v2.0
                        AND fvt2.language = 'US'
                        AND enabled_flag = 'Y')
      LOOP
        create_fasl_data(l_err_msg
                        ,l_err_code
                        ,c_book.flex_value
                        ,p_period);
      END LOOP;
    END;
    l_sec := 'Get period and DB name';
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    SELECT period_year, period_num
      INTO l_year, l_period_num
      FROM gl.gl_periods
     WHERE period_name = p_period
       AND period_set_name = l_calendar;
  
    l_sec := 'Set variables before SL pull';
  
    l_file_dir        := 'ORACLE_INT_GL_RECON'; --Version 1.3
    l_period_name     := p_period;
    l_file_period     := REPLACE(l_period_name, '-', '');
    errbuf            := NULL;
    retcode           := NULL;
    l_processed_count := 0; --Version 1.2
    --Create output file
    l_sec := 'Starting Fixed Asset Subledger pull.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || 'SBAL_P' || p_parent || 'FA' || '_' ||
                        l_file_period || '.txt';
  
    l_file_name_recon := 'SBAL_P' || p_parent || 'FA' || '_' ||
                         l_file_period || '.txt';
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- V 1.4 - Determine LOB for the giving parent account
    SELECT fvt.description
      INTO l_lob_name
      FROM applsys.fnd_flex_values_tl  fvt
          ,applsys.fnd_flex_values     fv
          ,applsys.fnd_flex_value_sets fvs
     WHERE fvt.flex_value_id = fv.flex_value_id
       AND fv.flex_value_set_id = fvs.flex_value_set_id
          --AND fvs.flex_value_set_name = 'HDS_PARENT_XXHSIGLACCTRECON'
       AND fvs.flex_value_set_name = 'XXCUS_PARENT_GLACCTRECON' --v2.0
       AND fvt.flex_value_meaning = p_parent;
  
    -- Get extract information begin    
    IF l_lob_name LIKE '%HD SUPPLY GSC'
    THEN
      --V1.5
      FOR c_slbal IN (SELECT segment1
                            ,segment4
                            ,acct_desc
                            ,SUM(amount) balance
                            ,(CASE
                               WHEN currency_code = 'CAD' THEN
                                NULL
                               ELSE
                                currency_code
                             END) ccy1code
                            ,(CASE
                               WHEN currency_code = 'CAD' THEN
                                NULL
                               ELSE
                                TRIM(to_char(SUM(amount), '999999999990.99'))
                             END) ccy1glendbalance
                            ,(CASE
                               WHEN currency_code = 'USD' THEN
                                NULL
                               ELSE
                                currency_code
                             END) ccy2code
                            ,(CASE
                               WHEN currency_code = 'USD' THEN
                                NULL
                               ELSE
                                TRIM(to_char(SUM(amount), '999999999990.99'))
                             END) ccy2glendbalance
                        FROM xxcus.xxcusgl_fa_sl_bal_tbl
                       GROUP BY segment1, segment4, acct_desc, currency_code)
      LOOP
      
        IF c_slbal.ccy1glendbalance <> 0 OR c_slbal.ccy2glendbalance <> 0
        THEN
        
          utl_file.put_line(l_file_handle_recon
                           ,'P' || c_slbal.segment1 || '|' || 'A' ||
                            c_slbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                            c_slbal.acct_desc || '|' || c_slbal.ccy1code || '|' ||
                            c_slbal.ccy1glendbalance || '|' ||
                            c_slbal.ccy2code || '|' ||
                            c_slbal.ccy2glendbalance || '|' ||
                            l_period_num || '|' || l_year);
        END IF;
      
        -- V 1.4 end mod
        l_processed_count := l_processed_count + 1; --Version 1.2
      
      END LOOP;
    
    ELSE
    
      FOR c_slbal IN (SELECT segment4
                            ,acct_desc
                            ,SUM(amount) balance
                            ,(CASE
                               WHEN currency_code = 'CAD' THEN
                                NULL
                               ELSE
                                currency_code
                             END) ccy1code
                            ,(CASE
                               WHEN currency_code = 'CAD' THEN
                                NULL
                               ELSE
                                TRIM(to_char(SUM(amount), '999999999990.99'))
                             END) ccy1glendbalance
                            ,(CASE
                               WHEN currency_code = 'USD' THEN
                                NULL
                               ELSE
                                currency_code
                             END) ccy2code
                            ,(CASE
                               WHEN currency_code = 'USD' THEN
                                NULL
                               ELSE
                                TRIM(to_char(SUM(amount), '999999999990.99'))
                             END) ccy2glendbalance
                        FROM xxcus.xxcusgl_fa_sl_bal_tbl
                       GROUP BY segment4, acct_desc, currency_code)
      LOOP
        IF c_slbal.ccy1glendbalance <> 0 OR c_slbal.ccy2glendbalance <> 0
        THEN
        
          utl_file.put_line(l_file_handle_recon
                           ,'P' || p_parent || '|' || c_slbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                            c_slbal.acct_desc || '|' || c_slbal.ccy1code || '|' ||
                            c_slbal.ccy1glendbalance || '|' ||
                            c_slbal.ccy2code || '|' ||
                            c_slbal.ccy2glendbalance || '|' ||
                            l_period_num || '|' || l_year);
        
        END IF;
        --END V1.5 
        l_processed_count := l_processed_count + 1; --Version 1.2
      
      END LOOP;
    END IF;
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
  
    /* Version 1.3
      --SEND the exptracted file through email  Version 1.2
    
      If l_processed_count > 0 then
        pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';
        --pl_dflt_email := 'kathy.poling@hdsupply.com';  --used for testing
    
        pl_ErrorStatus := SendMailJPkg.SendMail(SMTPServerName => p_host --'hsim1.hsi.hughessupply.com'
                                               ,
                                                Sender         => pl_sender --'Oracle.Applications@hughessupply.com'
                                               ,
                                                Recipient      => pl_dflt_email,
                                                CcRecipient    => '',
                                                BccRecipient   => '',
                                                Subject        => 'AssureNET Extract File ' ||
                                                                  l_file_name_recon,
                                                Body           => 'Please review and move the file to AssureNET directory \\ghdglr01wps\gl_upload$. ' ||
                                                                  SendMailJPkg.EOL ||
                                                                  'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                ErrorMessage   => pl_ErrorMessage,
                                                Attachments    => SendMailJPkg.ATTACHMENTS_LIST(l_file_dir || '/' ||
                                                                                                l_file_name_recon));
    
      END IF;
    
      --Delete any existing file   Version 1.2
      BEGIN
        utl_file.fremove(l_file_dir, l_file_name_recon);
      END fremove;
      --
    */
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_FASL_TWOSEGS package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_FASL_TWOSEGS package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
  END create_fasl_twosegs;

  /********************************************************************************
  
  File Name: XXCUS_GLACCT_RECON_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send Fixed Assets subledger balances to AssureNET GL
           (Account Recon Tool) for Corporate.
  
           Recon will be on Product, Account, Branch and Cost Center.
  
           Used by Utilities and White Cap
  
           Service Ticket 21137  RFC 9812
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/19/2009    Kathy Poling    Initial creation of the procedure
  1.2     12/22/2009                    Added JAVA Mailer to email the file and delete
                                        the file in the first part of procedure if they
                                        run the file multiply times during a month.
                                        Changed flex_value_set_id to flex_value_set_name
                                        RFC #10969
  1.3     07/08/2010    Kathy Poling    Removed the JAVA Mailer and file directory for UC4
                                        to pickup the files and deliver to AssureNET.
  
  ********************************************************************************/

  PROCEDURE create_fasl_foursegs(errbuf   OUT VARCHAR2
                                ,retcode  OUT VARCHAR2
                                ,p_period IN VARCHAR2
                                ,p_parent IN VARCHAR2) IS
  
    -- Intialize Variables
    l_err_msg         VARCHAR2(2000);
    l_err_code        NUMBER;
    l_processed_count NUMBER := 0; --Version 1.2
    l_file_name_recon VARCHAR2(150);
    l_file_name_temp  VARCHAR2(150);
    l_file_dir        VARCHAR2(100);
    l_file_period     VARCHAR2(20);
    l_period_name     VARCHAR2(20);
    l_year            NUMBER;
    l_period_num      NUMBER;
    l_sid             VARCHAR2(15);
  
    l_file_handle_recon utl_file.file_type;
    l_sec               VARCHAR2(150);
    l_calendar    CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_fnd_product CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1010925; --Parent to select FA Book
    l_procedure_name VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_fasl_fivesegs';
  
    --Email Defaults
    l_distro_list fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    --l_distro_list fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    --Version 1.2
    pl_dflt_email fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; --'kathy.poling@hdsupply.com';
    --pl_dflt_email   fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
    --
    --Start Main Program
  
  BEGIN
    -- Truncate Data from Staging table
    BEGIN
      l_sec := 'Truncate the detail table before loading specified time frame.';
      EXECUTE IMMEDIATE 'truncate table XXCUS.XXCUSGL_FA_SUBLED_TBL';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_FA_SUBLED_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'truncate table XXCUS.XXCUSGL_FA_SL_BAL_TBL';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_FA_SL_BAL_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    BEGIN
      l_sec := 'Calling create_fasl_data procedure';
      FOR c_book IN (SELECT fv2.flex_value
                       FROM applsys.fnd_flex_values_tl  fvt2
                           ,applsys.fnd_flex_values     fv2
                           ,applsys.fnd_flex_value_sets fvs2
                      WHERE fvt2.flex_value_id = fv2.flex_value_id
                        AND fv2.parent_flex_value_low = p_parent
                        AND fv2.flex_value_set_id = fvs2.flex_value_set_id
                           --AND fvs2.flex_value_set_name = 'HDS_FA_CHILD_XXHSIFAACCTRECON'
                        AND fvs2.flex_value_set_name =
                            'XXCUS_FA_CHILD_FAACCTRECON' --v2.0
                        AND fvt2.language = 'US'
                        AND enabled_flag = 'Y')
      LOOP
        create_fasl_data(l_err_msg
                        ,l_err_code
                        ,c_book.flex_value
                        ,p_period);
      END LOOP;
    END;
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    SELECT period_year, period_num
      INTO l_year, l_period_num
      FROM gl.gl_periods
     WHERE period_name = p_period
       AND period_set_name = l_calendar;
  
    --pl_sender     := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --l_file_dir        := '/xx_iface/' || l_sid || '/outbound/uc4';
    l_file_dir        := 'ORACLE_INT_GL_RECON'; --Version 1.3
    l_period_name     := p_period;
    l_file_period     := REPLACE(l_period_name, '-', '');
    errbuf            := NULL;
    retcode           := NULL;
    l_processed_count := 0; --Version 1.2
  
    --Create output file
    l_sec := 'Starting Fixed Asset Subledger pull.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || 'SBAL_P' || p_parent || 'FA' || '_' ||
                        l_file_period || '.txt';
  
    l_file_name_recon := 'SBAL_P' || p_parent || 'FA' || '_' ||
                         l_file_period || '.txt';
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- Get extract information begin
    FOR c_slbal IN (SELECT segment1
                          ,segment4
                          ,segment2
                          ,segment3
                          ,acct_desc
                          ,SUM(amount) balance
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy1code
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(amount), '999999999990.99'))
                           END) ccy1glendbalance
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy2code
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(amount), '999999999990.99'))
                           END) ccy2glendbalance
                      FROM xxcus.xxcusgl_fa_sl_bal_tbl
                     GROUP BY segment1
                             ,segment4
                             ,segment2
                             ,segment3
                             ,acct_desc
                             ,currency_code)
    LOOP
    
      IF c_slbal.ccy1glendbalance <> 0 OR c_slbal.ccy2glendbalance <> 0
      THEN
      
        utl_file.put_line(l_file_handle_recon
                         ,'P' || c_slbal.segment1 || '|' ||
                          c_slbal.segment4 || '|' || NULL || '|' ||
                          c_slbal.segment2 || '|' || c_slbal.segment3 || '|' || NULL || '|' ||
                          c_slbal.acct_desc || '|' || c_slbal.ccy1code || '|' ||
                          c_slbal.ccy1glendbalance || '|' ||
                          c_slbal.ccy2code || '|' ||
                          c_slbal.ccy2glendbalance || '|' || l_period_num || '|' ||
                          l_year);
      
      END IF;
    
      l_processed_count := l_processed_count + 1; --Version 1.2
    
    END LOOP;
  
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
  
    /* Version 1.3
      --SEND the exptracted file through email  Version 1.2
    
      If l_processed_count > 0 then
        pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';
        --pl_dflt_email := 'kathy.poling@hdsupply.com';  --used for testing
    
        pl_ErrorStatus := SendMailJPkg.SendMail(SMTPServerName => p_host --'hsim1.hsi.hughessupply.com'
                                               ,
                                                Sender         => pl_sender --'Oracle.Applications@hughessupply.com'
                                               ,
                                                Recipient      => pl_dflt_email,
                                                CcRecipient    => '',
                                                BccRecipient   => '',
                                                Subject        => 'AssureNET Extract File ' ||
                                                                  l_file_name_recon,
                                                Body           => 'Please review and move the file to AssureNET directory \\ghdglr01wps\gl_upload$. ' ||
                                                                  SendMailJPkg.EOL ||
                                                                  'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                ErrorMessage   => pl_ErrorMessage,
                                                Attachments    => SendMailJPkg.ATTACHMENTS_LIST(l_file_dir || '/' ||
                                                                                                l_file_name_recon));
    
      END IF;
    
      --Delete any existing file   Version 1.2
      BEGIN
        utl_file.fremove(l_file_dir, l_file_name_recon);
      END fremove;
      --
    */
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_FASL_FOURSEGS package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_FASL_FOURSEGS package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
  END create_fasl_foursegs;

  /********************************************************************************
  
  File Name: XXCUS_GLACCT_RECON_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send Fixed Assets subledger balances to AssureNET GL
           (Account Recon Tool) for Corporate.
  
           Recon will be on Product, Account, Branch, Cost Center and Project.
  
           Used by Corporate
  
           Service Ticket 21137  RFC 9812
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/18/2009    Kathy Poling    Initial creation of the procedure
  1.2     12/22/2009                    Added JAVA Mailer to email the file and delete
                                        the file in the first part of procedure if they
                                        run the file multiply times during a month.
                                        Changed flex_value_set_id to flex_value_set_name
                                        RFC #10969
  1.3     07/08/2010    Kathy Poling    Removed the JAVA Mailer and file directory for UC4
                                        to pickup the files and deliver to AssureNET.
  
  ********************************************************************************/

  PROCEDURE create_fasl_fivesegs(errbuf   OUT VARCHAR2
                                ,retcode  OUT VARCHAR2
                                ,p_period IN VARCHAR2
                                ,p_parent IN VARCHAR2) IS
  
    -- Intialize Variables
    l_err_msg         VARCHAR2(2000);
    l_err_code        NUMBER;
    l_processed_count NUMBER := 0; --Version 1.2
    l_file_name_recon VARCHAR2(150);
    l_file_name_temp  VARCHAR2(150);
    l_file_dir        VARCHAR2(100);
    l_file_period     VARCHAR2(20);
    l_period_name     VARCHAR2(20);
    l_year            NUMBER;
    l_period_num      NUMBER;
    l_sid             VARCHAR2(15);
  
    l_file_handle_recon utl_file.file_type;
    l_sec               VARCHAR2(150);
    l_calendar    CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_fnd_product CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1010925; --Parent to select FA Book
    l_procedure_name VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_fasl_fivesegs';
  
    --Email Defaults
    l_distro_list fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    --l_distro_list fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    --Version 1.2
    pl_dflt_email fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; --'kathy.poling@hdsupply.com';
    --pl_dflt_email   fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
    --
  
    --Start Main Program
  
  BEGIN
  
    -- Truncate Data from Staging table
    BEGIN
      l_sec := 'Truncate the detail table before loading specified time frame.';
      EXECUTE IMMEDIATE 'truncate table XXCUS.XXCUSGL_FA_SUBLED_TBL';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_FA_SUBLED_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'truncate table XXCUS.XXCUSGL_FA_SL_BAL_TBL';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSGL_FA_SL_BAL_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    BEGIN
      l_sec := 'Calling create_fasl_data procedure';
      FOR c_book IN (SELECT fv2.flex_value
                       FROM applsys.fnd_flex_values_tl  fvt2
                           ,applsys.fnd_flex_values     fv2
                           ,applsys.fnd_flex_value_sets fvs2
                      WHERE fvt2.flex_value_id = fv2.flex_value_id
                        AND fv2.parent_flex_value_low = p_parent
                        AND fv2.flex_value_set_id = fvs2.flex_value_set_id
                           --AND fvs2.flex_value_set_name = 'HDS_FA_CHILD_XXHSIFAACCTRECON'
                        AND fvs2.flex_value_set_name =
                            'XXCUS_FA_CHILD_FAACCTRECON' --v2.0
                        AND fvt2.language = 'US'
                        AND enabled_flag = 'Y')
      LOOP
        create_fasl_data(l_err_msg
                        ,l_err_code
                        ,c_book.flex_value
                        ,p_period);
      END LOOP;
    END;
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    SELECT period_year, period_num
      INTO l_year, l_period_num
      FROM gl.gl_periods
     WHERE period_name = p_period
       AND period_set_name = l_calendar;
  
    --pl_sender     := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --l_file_dir        := '/xx_iface/' || l_sid || '/outbound/uc4';
    l_file_dir        := 'ORACLE_INT_GL_RECON'; --Version 1.3
    l_period_name     := p_period;
    l_file_period     := REPLACE(l_period_name, '-', '');
    errbuf            := NULL;
    retcode           := NULL;
    l_processed_count := 0; --Version 1.2
  
    --Create output file
    l_sec := 'Starting Fixed Asset Subledger pull.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || 'SBAL_P' || p_parent || 'FA' || '_' ||
                        l_file_period || '.txt';
  
    l_file_name_recon := 'SBAL_P' || p_parent || 'FA' || '_' ||
                         l_file_period || '.txt';
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- Get extract information begin
    FOR c_slbal IN (SELECT segment1
                          ,segment4
                          ,segment2
                          ,segment3
                          ,segment5
                          ,acct_desc
                          ,SUM(amount) balance
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy1code
                          ,(CASE
                             WHEN currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(amount), '999999999990.99'))
                           END) ccy1glendbalance
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              currency_code
                           END) ccy2code
                          ,(CASE
                             WHEN currency_code = 'USD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(amount), '999999999990.99'))
                           END) ccy2glendbalance
                      FROM xxcus.xxcusgl_fa_sl_bal_tbl
                     GROUP BY segment1
                             ,segment4
                             ,segment2
                             ,segment3
                             ,segment5
                             ,acct_desc
                             ,currency_code)
    LOOP
    
      IF c_slbal.ccy1glendbalance <> 0 OR c_slbal.ccy2glendbalance <> 0
      THEN
      
        utl_file.put_line(l_file_handle_recon
                         ,'P' || c_slbal.segment1 || '|' ||
                          c_slbal.segment4 || '|' || NULL || '|' ||
                          c_slbal.segment2 || '|' || c_slbal.segment3 || '|' ||
                          c_slbal.segment5 || '|' || c_slbal.acct_desc || '|' ||
                          c_slbal.ccy1code || '|' ||
                          c_slbal.ccy1glendbalance || '|' ||
                          c_slbal.ccy2code || '|' ||
                          c_slbal.ccy2glendbalance || '|' || l_period_num || '|' ||
                          l_year);
      
      END IF;
    
      l_processed_count := l_processed_count + 1; --Version 1.2
    
    END LOOP;
  
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
    /* Version 1.3
      --SEND the exptracted file through email  Version 1.2
    
      If l_processed_count > 0 then
        pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';
        --pl_dflt_email := 'kathy.poling@hdsupply.com';  --used for testing
    
        pl_ErrorStatus := SendMailJPkg.SendMail(SMTPServerName => p_host --'hsim1.hsi.hughessupply.com'
                                               ,
                                                Sender         => pl_sender --'Oracle.Applications@hughessupply.com'
                                               ,
                                                Recipient      => pl_dflt_email,
                                                CcRecipient    => '',
                                                BccRecipient   => '',
                                                Subject        => 'AssureNET Extract File ' ||
                                                                  l_file_name_recon,
                                                Body           => 'Please review and move the file to AssureNET directory \\ghdglr01wps\gl_upload$. ' ||
                                                                  SendMailJPkg.EOL ||
                                                                  'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                ErrorMessage   => pl_ErrorMessage,
                                                Attachments    => SendMailJPkg.ATTACHMENTS_LIST(l_file_dir || '/' ||
                                                                                                l_file_name_recon));
    
      END IF;
    
      --Delete any existing file   Version 1.2
      BEGIN
        utl_file.fremove(l_file_dir, l_file_name_recon);
      END fremove;
      --
    */
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_FASL_FIVESEGS package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Account Extract Process in GL_ACCT_RECON_PKG.CREATE_FASL_FIVESEGS package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'GL');
    
  END create_fasl_fivesegs;

  /********************************************************************************
  
  File Name: XXCUS_GLACCT_RECON_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send subledger balances to AssureNET GL
           (Account Recon Tool) for Corporate.  This is for the bank balance from
           Cash Management.
  
           Recon will be on Product, Account, Branch, Cost Center and Project.
  
           Service Ticket # 48468   RFC # 18941
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/02/2010    Kathy Poling    Initial creation of the procedure
  1.3     07/08/2010    Kathy Poling    Removed the JAVA Mailer and file directory for UC4
                                        to pickup the files and deliver to AssureNET.
  
  
  ********************************************************************************/

  PROCEDURE create_ocmsl(errbuf           OUT VARCHAR2
                        ,retcode          OUT NUMBER
                        ,p_statement_date IN DATE
                        ,p_parent         IN VARCHAR2) IS
  
    --Intialize Variables
    l_rec_count         NUMBER := 0;
    l_period_name       VARCHAR2(20);
    l_period_num        NUMBER;
    l_file_period       VARCHAR2(20);
    l_year              NUMBER;
    l_thru_date         DATE;
    l_err_msg           VARCHAR2(2000);
    l_err_code          NUMBER;
    l_processed_count   NUMBER := 0;
    l_file_name_recon   VARCHAR2(150);
    l_file_name_temp    VARCHAR2(150);
    l_file_dir          VARCHAR2(100);
    l_file_handle_recon utl_file.file_type;
    l_sid               VARCHAR2(10);
    l_sec               VARCHAR2(150);
    l_procedure_name    VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_ocmsl';
    l_distro_list       fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    --l_distro_list fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    l_calendar     CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_fnd_category CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014550; --Account Category segment4
    l_fnd_product  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547; --Product segment1
    lc_coa         CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --Main Chart of Accounts    Version 1.2
  
    pl_dflt_email fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; --'kathy.poling@hdsupply.com';
    --pl_dflt_email   fnd_user.email_address%TYPE := 'luong.vu@hdsupply.com';
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
    --
  
  BEGIN
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    SELECT period_year, period_num, period_name
      INTO l_year, l_period_num, l_period_name
      FROM gl.gl_periods
     WHERE p_statement_date BETWEEN start_date AND end_date
       AND period_set_name = l_calendar;
  
    --pl_sender     := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --l_file_dir := '/xx_iface/' || l_sid || '/outbound/uc4';
    l_file_dir := 'ORACLE_INT_GL_RECON'; --Version 1.3
    --l_period_name := p_period_name;
    l_file_period     := REPLACE(l_period_name, '-', '');
    errbuf            := NULL;
    retcode           := NULL;
    l_processed_count := 0;
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || 'SBAL_P' || p_parent || 'OCM' || '_' ||
                        l_file_period || '.txt';
  
    l_file_name_recon := 'SBAL_P' || p_parent || 'OCM' || '_' ||
                         l_file_period || '.txt';
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- Get extract information begin
    FOR c_slbal IN (SELECT g.segment1
                          ,g.segment2
                          ,g.segment3
                          ,g.segment4
                          ,g.segment5
                          ,substr(f.description, 1, 50) description
                          , --Acct Name
                           (CASE
                             WHEN b.currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              b.currency_code
                           END) ccy1code
                          ,(CASE
                             WHEN b.currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(s.control_end_balance)
                                          ,'999999999990.99'))
                           END) ccy1glendbalance
                          ,(CASE
                             WHEN b.currency_code = 'USD' THEN
                              NULL
                             ELSE
                              b.currency_code
                           END) ccy2code
                          ,(CASE
                             WHEN b.currency_code = 'USD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(s.control_end_balance)
                                          ,'999999999990.99'))
                           END) ccy2glendbalance
                      FROM --ce.ce_statement_headers_all s
                           ce.ce_statement_headers s --v2.0
                          ,ap.ap_bank_accounts_all b
                          ,gl.gl_code_combinations g
                          ,apps.fnd_flex_values_vl f
                          ,apps.fnd_flex_values_vl prod
                     WHERE s.org_id IN (163, 166)
                       AND s.org_id = b.org_id
                       AND b.bank_account_id = s.bank_account_id
                       AND nvl(inactive_date, SYSDATE + 30) > SYSDATE
                       AND b.asset_code_combination_id =
                           g.code_combination_id
                       AND g.chart_of_accounts_id = lc_coa
                       AND g.segment4 = f.flex_value_meaning
                       AND f.flex_value_set_id = l_fnd_category
                       AND g.segment1 = prod.flex_value_meaning
                       AND prod.flex_value_set_id = l_fnd_product
                       AND s.statement_date = p_statement_date
                     GROUP BY g.segment1
                             ,g.segment4
                             ,g.segment2
                             ,g.segment3
                             ,g.segment5
                             ,f.description
                             ,b.currency_code)
    LOOP
    
      IF c_slbal.ccy1glendbalance <> 0 OR c_slbal.ccy2glendbalance <> 0
      THEN
      
        l_rec_count := l_rec_count + 1;
        utl_file.put_line(l_file_handle_recon
                         ,'P' || c_slbal.segment1 || '|' ||
                          c_slbal.segment4 || '|' || NULL || '|' ||
                          c_slbal.segment2 || '|' || c_slbal.segment3 || '|' ||
                          c_slbal.segment5 || '|' || c_slbal.description || '|' ||
                          c_slbal.ccy1code || '|' ||
                          c_slbal.ccy1glendbalance || '|' ||
                          c_slbal.ccy2code || '|' ||
                          c_slbal.ccy2glendbalance || '|' || l_period_num || '|' ||
                          l_year);
      
      END IF;
    
      l_processed_count := l_processed_count + 1;
    
    END LOOP;
  
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
    /*  --version 1.2
        --SEND the exptracted file through email
    
       If l_processed_count > 0 then
         pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';
         --pl_dflt_email := 'kathy.poling@hdsupply.com';  --used for testing
    
         pl_ErrorStatus := SendMailJPkg.SendMail(SMTPServerName => p_host --'hsim1.hsi.hughessupply.com'
                                                ,
                                                 Sender         => pl_sender --'Oracle.Applications@hughessupply.com'
                                                ,
                                                 Recipient      => pl_dflt_email,
                                                 CcRecipient    => '',
                                                 BccRecipient   => '',
                                                 Subject        => 'AssureNET Extract File ' ||
                                                                   l_file_name_recon,
                                                 Body           => 'Please review and move the file to AssureNET directory \\ghdglr01wps\gl_upload$. ' ||
                                                                   SendMailJPkg.EOL ||
                                                                   'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                 ErrorMessage   => pl_ErrorMessage,
                                                 Attachments    => SendMailJPkg.ATTACHMENTS_LIST(l_file_dir || '/' ||
                                                                                                 l_file_name_recon));
    
       END IF;
    
         --Delete any existing file   Version 1.2
       BEGIN
         utl_file.fremove(l_file_dir, l_file_name_recon);
       END fremove;
    */
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Eclipse Acct Extract Process in GL_ACCT_RECON_PKG.CREATE_OCMSL package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OCM');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Eclipse Acct Extract Process in GL_ACCT_RECON_PKG.CREATE_OCMSL package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OCM');
    
  END create_ocmsl;

  /********************************************************************************
  
  File Name: XXCUS_GLACCT_RECON_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send subledger balances to AssureNET GL
           (Account Recon Tool) for Corporate.  This is for the bank balance from
           Cash Management.
  
           Recon will be on Product, Account.
  
           Service Ticket # 51558
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/28/2010    Luong Vu        Initial creation of the procedure (A modified
                                        clone of create_ocmsl)
  1.1     05/03/2013    Kathy Poling    SR 192331 enhancement to include CAD org 167
  ********************************************************************************/

  PROCEDURE create_ocmsl_two_segs(errbuf           OUT VARCHAR2
                                 ,retcode          OUT NUMBER
                                 ,p_statement_date IN DATE
                                 ,p_parent         IN VARCHAR2) IS
  
    --Intialize Variables
    l_rec_count         NUMBER := 0;
    l_period_name       VARCHAR2(20);
    l_period_num        NUMBER;
    l_file_period       VARCHAR2(20);
    l_year              NUMBER;
    l_thru_date         DATE;
    l_err_msg           VARCHAR2(2000);
    l_err_code          NUMBER;
    l_processed_count   NUMBER := 0;
    l_file_name_recon   VARCHAR2(150);
    l_file_name_temp    VARCHAR2(150);
    l_file_dir          VARCHAR2(100);
    l_file_handle_recon utl_file.file_type;
    l_sid               VARCHAR2(10);
    l_sec               VARCHAR2(150);
    l_org_id            NUMBER;    --Version 1.1
    l_procedure_name    VARCHAR2(75) := 'xxcusgl_acct_recon_pkg.create_ocmsl_two_segs';
    l_distro_list       fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    
    l_calendar     CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_fnd_category CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014550; --Account Category segment4
    l_fnd_product  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547; --Product segment1
    lc_coa         CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --Main Chart of Accounts    
    l_acctnum VARCHAR2(10); --Luong
  
    pl_dflt_email fnd_user.email_address%TYPE := 'hdsassurenetglsupport@hdsupply.com'; 
    
    pl_email        fnd_user.email_address%TYPE;
    pl_instance     VARCHAR2(100);
    pl_sender       VARCHAR2(100);
    pl_errorstatus  NUMBER;
    p_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    pl_errormessage VARCHAR2(4000);
    --
  
  BEGIN
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    SELECT period_year, period_num, period_name
      INTO l_year, l_period_num, l_period_name
      FROM gl.gl_periods
     WHERE p_statement_date BETWEEN start_date AND end_date
       AND period_set_name = l_calendar;
  
    l_file_dir := 'ORACLE_INT_GL_RECON';
    
    l_file_period     := REPLACE(l_period_name, '-', '');
    errbuf            := NULL;
    retcode           := NULL;
    l_processed_count := 0;
  
    --Set the file name and open the file
  
    IF p_parent = '79'
    THEN
      l_org_id := 163;
      l_file_name_temp := 'TEMP_' || 'SBAL_P' || p_parent || 'OCM' || '_' ||
                          l_file_period || '.txt';
    
      l_file_name_recon := 'SBAL_P' || p_parent || 'OCM' || '_' ||
                           l_file_period || '.txt';
    ELSE
      l_org_id := 167;
      l_file_name_temp := 'TEMP_' || 'SBAL_P' || p_parent || '_CAD_OCM' || '_' ||
                          l_file_period || '.txt';
    
      l_file_name_recon := 'SBAL_P' || p_parent || '_CAD_OCM' || '_' ||
                           l_file_period || '.txt';
    END IF;
  
    fnd_file.put_line(fnd_file.log
                     ,'Filename generated : ' || l_file_name_recon);
    fnd_file.put_line(fnd_file.output
                     ,'Filename generated : ' || l_file_name_recon);
  
    l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp, 'w');
  
    -- Get extract information begin
    FOR c_slbal IN (SELECT CASE
                             WHEN g.segment1 IN
                                  (SELECT c.child_flex_value_low
                                     FROM apps.fnd_flex_value_hierarchies c
                                    WHERE c.flex_value_set_id = l_fnd_product --Version 1.1  1014547
                                      AND c.parent_flex_value = '79') THEN --parent hierarchy that includes 48,49,50,53 
                              'A' || g.segment4
                             ELSE
                              g.segment4
                           END segment4
                          ,CASE
                             WHEN g.segment1 = '06' THEN
                              '73'
                             WHEN g.segment1 = '21' THEN
                              '89'
                             WHEN g.segment1 = '27' THEN
                              '91'
                             ELSE
                              segment1
                           END segment1
                          ,substr(f.description, 1, 50) description
                          , --Acct Name
                           (CASE
                             WHEN b.currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              b.currency_code
                           END) ccy1code
                          ,(CASE
                             WHEN b.currency_code = 'CAD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(s.control_end_balance)
                                          ,'999999999990.99'))
                           END) ccy1glendbalance
                          ,(CASE
                             WHEN b.currency_code = 'USD' THEN
                              NULL
                             ELSE
                              b.currency_code
                           END) ccy2code
                          ,(CASE
                             WHEN b.currency_code = 'USD' THEN
                              NULL
                             ELSE
                              TRIM(to_char(SUM(s.control_end_balance)
                                          ,'999999999990.99'))
                           END) ccy2glendbalance
                      FROM ce.ce_statement_headers  s 
                          ,ce.ce_bank_accounts      b
                          ,ce.ce_bank_acct_uses_all u
                          ,gl.gl_code_combinations  g
                          ,apps.fnd_flex_values_vl  f
                          ,apps.fnd_flex_values_vl  prod
                     WHERE --Version 1.1 5/7/2013
                    --u.org_id IN (163, 166)    
                    u.org_id = l_org_id
                    --end 5/7/2013
                 AND s.bank_account_id = u.bank_account_id
                 AND b.bank_account_id = s.bank_account_id
                 AND nvl(b.end_date, SYSDATE + 30) > SYSDATE
                 AND b.asset_code_combination_id = g.code_combination_id
                 AND g.chart_of_accounts_id = lc_coa
                 AND g.segment4 = f.flex_value_meaning
                 AND f.flex_value_set_id = l_fnd_category
                 AND g.segment1 = prod.flex_value_meaning
                 AND prod.flex_value_set_id = l_fnd_product
                 AND s.statement_date = p_statement_date
                     GROUP BY g.segment4
                             ,segment1
                             ,f.description
                             ,b.currency_code)
    LOOP
    
      --      IF c_slbal.CCY1GLENDBALANCE <> 0 or c_slbal.CCY2GLENDBALANCE <> 0 THEN
      -- IF c_slbal.segment1 between -- Luong
      l_rec_count := l_rec_count + 1;
      utl_file.put_line(l_file_handle_recon
                       ,'P' || c_slbal.segment1 || '|' || c_slbal.segment4 || '|' || NULL || '|' || NULL || '|' || NULL || '|' || NULL || '|' ||
                        c_slbal.description || '|' || c_slbal.ccy1code || '|' ||
                        c_slbal.ccy1glendbalance || '|' ||
                        c_slbal.ccy2code || '|' ||
                        c_slbal.ccy2glendbalance || '|' || l_period_num || '|' ||
                        l_year);
    
      --      END IF;
    
      l_processed_count := l_processed_count + 1;
    
    END LOOP;
  
    utl_file.fclose(l_file_handle_recon);
  
    utl_file.frename(l_file_dir
                    ,l_file_name_temp
                    ,l_file_dir
                    ,l_file_name_recon);
  
    fnd_file.put_line(fnd_file.log, 'File Completed ');
    fnd_file.put_line(fnd_file.output, 'File Completed ');
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Eclipse Acct Extract Process in GL_ACCT_RECON_PKG.create_ocmsl_two_segs package with Program Error Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OCM');
    
    WHEN OTHERS THEN
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Eclipse Acct Extract Process in GL_ACCT_RECON_PKG.create_ocmsl_two_segs package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'OCM');
    
  END create_ocmsl_two_segs;

END xxcusgl_acct_recon_pkg;
/
