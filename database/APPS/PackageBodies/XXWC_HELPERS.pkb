/* Formatted on 01-Mar-2013 17:22:52 (QP5 v5.206) */
-- Start of DDL Script for Package Body APPS.XXWC_HELPERS
-- Generated 01-Mar-2013 17:22:51 from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE BODY apps.xxwc_helpers
--Package will keep usefull XXWC function and procedures ;
--Started By Rasikha Galimova at 1-march-2013
--***************************************************
-- FUNCTION remove_special_chars will remove all characters from string based on
-- ASCII value starting from 33-124(keep oly ASCII in this interval);
IS
    l_req_id          NUMBER := fnd_global.conc_request_id;
    l_error_message   CLOB;

    FUNCTION remove_special_chars (p_string IN VARCHAR2)
        RETURN VARCHAR2
    IS
        v_n                NUMBER := 0;
        v_end              NUMBER := LENGTH (p_string);
        v_ascii_value      NUMBER;
        v_value            VARCHAR2 (256);
        p_return_string    VARCHAR2 (7168);
        v_string_default   VARCHAR2 (7168);
    BEGIN
        v_string_default := p_string;

        LOOP
            v_n := v_n + 1;

            --  EXIT WHEN v_n = v_end;

            SELECT ASCII (SUBSTR (p_string, v_n, 1)), SUBSTR (p_string, v_n, 1)
              INTO v_ascii_value, v_value
              FROM DUAL;

            IF v_ascii_value >= 32 AND v_ascii_value <= 126                                                        --all
            -- Or (V_ascii_value >= 48 And V_ascii_value <= 57) -- numbers
            --  Or (V_ascii_value >= 65 And V_ascii_value <= 90)--upper case letters
            -- Or (V_ascii_value >= 97 And V_ascii_value <= 122)--lower case letters
            THEN
                p_return_string := p_return_string || v_value;
            END IF;

            EXIT WHEN v_n = v_end;
        END LOOP;

        --remove double space
        p_return_string := REPLACE (p_return_string, '  ', ' ');
        RETURN p_return_string;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'xxwc_helpers.remove_special_chars '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            xxwc_log_debug (l_error_message);
            --if something is wrong - just return in string
            --no need to report error  for

            RETURN v_string_default;
    END;

    --********************************************
    --if called from apps will put to log file, from database to the spool- by Rasikha.
    --*******************************************
    PROCEDURE xxwc_log_debug (p_message VARCHAR2)
    IS
    BEGIN
        IF l_req_id > 0
        THEN
            fnd_file.put_line (fnd_file.LOG, p_message);
        ELSE
            DBMS_OUTPUT.put_line (p_message);
        END IF;
    END;

    --********************************************
    --log errors to    XXWC error system
    --*******************************************
    PROCEDURE xxwc_log_error (p_message           CLOB
                             ,p_package_name      VARCHAR2
                             ,p_procedure_name    VARCHAR2
                             ,p_module_name       VARCHAR2)
    IS
        v_distro_list   VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    BEGIN
        IF l_req_id > 0
        THEN
            fnd_file.put_line (fnd_file.LOG
                              ,'*Message from ' || p_package_name || '.' || p_procedure_name || ' ' || p_message);
        ELSE
            DBMS_OUTPUT.put_line ('*Message from ' || p_package_name || '.' || p_procedure_name || ' ' || p_message);
        END IF;

        xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => p_package_name
           ,p_calling             => p_procedure_name
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SUBSTR (p_message, 1, 2000)
           ,p_error_desc          => 'Error running ' || p_package_name || '.' || p_procedure_name
           ,p_distribution_list   => v_distro_list
           ,p_module              => p_module_name);
    END;
END;
/

-- End of DDL Script for Package Body APPS.XXWC_HELPERS
