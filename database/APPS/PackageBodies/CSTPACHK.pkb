create or replace 
PACKAGE BODY                CSTPACHK AS
/* $Header: CSTACHKB.pls 120.3.12010000.3 2010/02/26 08:14:10 lchevala ship $ */

-- FUNCTION
--  actual_cost_hook		Cover routine to allow users to add
--				customization. This would let users circumvent
--				our transaction cost processing.  This function
--				is called by both CSTPACIN and CSTPACWP.
--
--
-- RETURN VALUES
--  integer		1	Hook has been used.
--			0  	Continue cost processing for this transaction
--				as usual.
--
/*

      PURPOSE:    Freight Burden Extension Added

      Logic:     1) Find out of if the extension is enabled via the profile option WC: Freight Costing Hook Enabled
                 2) If the extension is enabled then find out if the transaction is a PO Receipt Transaction
                 3) Grab the relevant information from MTL_MATERIAL_TRANSACTIONS
                 4) Find the Freight Overhead adder
                 5) Insert new cost values intothe MTL_CST_ACTUAL_COST_DETAILS
                 6) Return 1 if the hook executed sucessfully

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        05-Nov-11   Lee Spitzer       1. Created this trigger.
      2.0        20-FEB-12   Lee Spitzer       2. Updated trigger to derive revenue account from the item
      2.1        24-APR-13   Lee Spitzer       3. Added to reset Freight Burden and Import Duty to 0 for Drop Ship Receipts TMS Ticket - 20130102-01229          
      2.2        01-MAY-13   Lee Spitzer       4. Added for Hawaii Freight Burden TMS Ticket - 20130102-01229
      2.3        20-SEP-13   Lee Spitzer       5. Updated for Receipt Correction Transaction TMS Ticket - 20130916-00501
      2.4        30-OCT-13   Lee Spitzer       6. New Task Assigned : Fix calculation  of freight burden based on Import Duty rates - 20131101-00408
   ******************************************************************************/


 
function actual_cost_hook(
  I_ORG_ID	IN	NUMBER,
  I_TXN_ID	IN 	NUMBER,
  I_LAYER_ID	IN	NUMBER,
  I_COST_TYPE	IN	NUMBER,
  I_COST_METHOD	IN	NUMBER,
  I_USER_ID	IN	NUMBER,
  I_LOGIN_ID    IN	NUMBER,
  I_REQ_ID	IN	NUMBER,
  I_PRG_APPL_ID	IN	NUMBER,
  I_PRG_ID	IN 	NUMBER,
  O_Err_Num	OUT NOCOPY	NUMBER,
  O_Err_Code	OUT NOCOPY	VARCHAR2,
  O_Err_Msg	OUT NOCOPY	VARCHAR2
)
return integer  IS

    l_transaction_type_id       NUMBER; --Transaction_Type_id from mtl_material_transactions
    l_transaction_type_id2      NUMBER; --Transaction_Type_Id #2 to compare to l_transaction_type_id if the value is PO Receipt 
    l_transaction_type_id3      NUMBER; --Transaction_Type_Id #3 to compare to l_transaction_type_id if the value is Intransit Receipt
    l_transaction_type_id4      NUMBER; --Transaction_Type_ID #4 to compare to l_transaction_type_id if the value is PO Rcpt Adjust
    l_cost_type_id              NUMBER; -- Cost Type Id
    l_update_cost               NUMBER := 0;
    l_inventory_item_id         NUMBER;  --Inventory Item Id
    l_layer_id                  NUMBER;  --Layer ID
    l_transaction_action_id     NUMBER;  --Transaction Action Id
    l_applicable                VARCHAR2(10);  --If the profile option is enabled
    l_mat_oh_exists             NUMBER;
    l_mat_exists                NUMBER;
    l_mat_oh_exists2            NUMBER;
    l_mat_exists2               NUMBER;
    l_exists                    NUMBER;
    l_exixts2                   NUMBER;
    l_transaction_cost          NUMBER;
    l_material_cost             NUMBER; 
    l_material_overhead_cost    NUMBER;
    l_resource_cost             NUMBER;
    l_overhead_cost             NUMBER;
    l_outside_processing_cost   NUMBER;
    l_mat_insert                VARCHAR2(1);
    l_mat_oh_insert             VARCHAR2(1);
    l_exception                 EXCEPTION;
    l_message                   VARCHAR2(2000);    
    l_sqlcode                   VARCHAR2(100);    
    l_resource_id               NUMBER;
    l_transaction_source_id     NUMBER;
    l_vendor_id                 NUMBER;
    l_vendor_site_id            NUMBER;
    l_import_dty_moh_rate       NUMBER;
    l_po_freight_moh_rate       NUMBER;
    l_import_moh_id             NUMBER;
    l_po_freight_moh_id         NUMBER;
    l_import_flag               VARCHAR2(1);
    
    --Added Local Variables 4/24/2013 TMS Ticket - 20130102-01229  
    l_rcv_transaction_id        NUMBER;
    l_transaction_source_type_id NUMBER;
    l_drop_ship_flag            VARCHAR2(1) DEFAULT 'N';
    l_shipment_header_id        NUMBER;
    l_freight_per_lb            NUMBER;
    l_source_organization_id    NUMBER;
    l_freight_basis             VARCHAR2(1) DEFAULT 'C';
    l_shipping_weight           NUMBER;
    l_unit_weight               NUMBER;
    l_moh_weight_cost           NUMBER;
    l_organization_Id           NUMBER;
    l_transfer_organization_id  NUMBER;
    l_transfer_transaction_id   NUMBER;
    --End Added Local Variables TMS Ticket - 20130102-01229  
    

BEGIN

   --Check to see if the profile option WC: Freight Costing Hook Enabled is set to Yes
   l_applicable := fnd_profile.VALUE ('XXWC_FREIGHT_COSTING_HOOK_ENABLED');

     --insert into xxwc.xxwc_debug values (sysdate, 1001, 'l_applicable = ' || l_applicable);
     --commit;
             
    --if applicable - peform the logic to check the cost hook  otherwise don't execute the hook
    IF l_applicable = 'Y' THEN
  
        /*
              insert into xxwc.xxwc_debug values (sysdate, 1001, 'i_txn_id = ' || i_txn_id);
              INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1001, 'i_org_id = ' || i_org_id);
              insert into xxwc.xxwc_debug values (sysdate, 1001, 'i_layer_id = ' || i_layer_id);
                                 COMMIT;
          */         
        -- Get the relevant data about the passed material transaction 
        BEGIN
            
            SELECT transaction_type_id, 
                   inventory_item_id, 
                   transaction_action_id, 
                   transaction_cost,
                   transaction_source_id,
                   rcv_transaction_id, --Added 4/24/2013
                   transaction_source_type_id,
                   organization_id,
                   transfer_organization_id,
                   transfer_transaction_id
            INTO   l_transaction_type_id, 
                   l_inventory_item_id, 
                   l_transaction_action_id,
                   l_transaction_cost,
                   l_transaction_source_id,
                   l_rcv_transaction_id, --Added 4/24/2013
                   l_transaction_source_type_id,
                   l_organization_id,
                   l_transfer_organization_id,
                   l_transfer_transaction_id
            FROM   mtl_material_transactions
            WHERE  i_txn_id = transaction_id
            AND    i_org_id = organization_id;
        
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
               BEGIN
                SELECT transaction_type_id, 
                       inventory_item_id, 
                       transaction_action_id, 
                       transaction_cost,
                       transaction_source_id,
                       rcv_transaction_id, --Added 4/24/2013
                       transaction_source_type_id,
                       organization_id,
                       transfer_organization_id,
                       transfer_transaction_id
                INTO   l_transaction_type_id, 
                       l_inventory_item_id, 
                       l_transaction_action_id,
                       l_transaction_cost,
                       l_transaction_source_id,
                       l_rcv_transaction_id, --Added 4/24/2013
                       l_transaction_source_type_id,
                       l_organization_id,
                       l_transfer_organization_id,
                       l_transfer_transaction_id
                FROM   mtl_material_transactions
                WHERE  i_txn_id = transaction_id
                AND    i_org_id = transfer_organization_id;
            EXCEPTION
                WHEN OTHERS THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving query information from MTL_MATERIAL_TRANSACTION ' || substrb(SQLERRM,1,150);
                   --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1001, 'could not find transaction data = ' || l_sqlcode||l_message);
                   --COMMIT;
                   RAISE l_exception;
            END;
            WHEN  others THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving query information from MTL_MATERIAL_TRANSACTION ' || substrb(SQLERRM,1,150);
                   --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1001, 'could not find transaction data = ' || l_sqlcode||l_message);
                   --commit;
                   RAISE l_exception;
        END;

              --insert into xxwc.xxwc_debug values (sysdate, 1002, 'i_txn_id = ' || i_txn_id);
              --insert into xxwc.xxwc_debug values (sysdate, 1003, 'i_org_id = ' || i_org_id);
              --insert into xxwc.xxwc_debug values (sysdate, 1004, 'i_layer_id = ' || i_layer_id);
              --insert into xxwc.xxwc_debug values (sysdate, 1005, 'l_inventory_item_id = ' || l_inventory_item_id);
              --insert into xxwc.xxwc_debug values (sysdate, 1006, 'l_transaction_type_id = ' || l_transaction_type_id);
              --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1007, 'l_transaction_action_id = ' || l_transaction_action_id);
              --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1008, 'l_rcv_transaction_id = ' || l_rcv_transaction_id);
              --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1009, 'l_transaction_source_type_id = ' || l_transaction_source_type_id);
              --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1009, 'l_organization_id = ' || l_organization_id);
              --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1009, 'l_transfer_organization_id = ' || l_transfer_organization_id);
            --commit;

        --Find if this is a PO receiving transaction
        BEGIN
            SELECT transaction_type_id
            INTO   l_transaction_type_id2
            FROM   mtl_transaction_types
            where  transaction_type_name = 'PO Receipt'; 
            
        EXCEPTION
            when others then
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving query information from MTL_TRANSACTION_TYPES ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
        END;                       

      
        BEGIN
            SELECT transaction_type_id
            INTO   l_transaction_type_id3
            from   mtl_transaction_types
            WHERE  transaction_type_name = 'Int Req Intr Rcpt';
        EXCEPTION
            when others then
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving query information from MTL_TRANSACTION_TYPES ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
        END;                       


       --Block added 9/20/2013 for TMS Ticket 20130916-00501
        BEGIN
            SELECT transaction_type_id
            INTO   l_transaction_type_id4
            FROM   mtl_transaction_types
            where  transaction_type_name = 'PO Rcpt Adjust';
            
        EXCEPTION
            when others then
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving query information from MTL_TRANSACTION_TYPES ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
        END;                       
        
        
         --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1009, 'l_transaction_type_id2 = ' || l_transaction_type_id2);
         --commit;
         
         --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1009, 'l_transaction_type_id3 = ' || l_transaction_type_id3);
         --COMMIT;
                        
        --Validate if the transaction type id passed into the Hook is the PO Receipt Transaction Type         
                      
       --IF l_transaction_type_id = l_transaction_type_id2 THEN --Removed 9/20/2013 TMS Ticket 20130916-00501
         IF l_transaction_type_id in (l_transaction_type_id2,l_transaction_type_id4) THEN --Added 20130916-00501 for PO Receipt and PO Rcpt Adjust
       
      -- IF l_transaction_type_id = l_transaction_type_id2  or l_transaction_type_id = l_transaction_type_id3 THEN
         
         --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1010, 'If Clause executed');
         --COMMIT;
          
            --PO = Transaction Source Type = 1
          if l_transaction_source_type_id = 1 THEN
              --Get the Vendor_ID and Vendor_Site_Id from the Purchase Order
              BEGIN
                  SELECT vendor_id,
                         vendor_site_id
                  INTO   l_vendor_id,
                         l_vendor_site_id
                  FROM   po_headers_all pha
                  where  pha.po_header_id = l_transaction_source_id;                        
              EXCEPTION
                   WHEN others  THEN
                      l_vendor_id := NULL;
                      l_vendor_site_id := NULL;
              END;
            ELSE 
                      l_vendor_id := NULL;
                      l_vendor_site_id := NULL;
            END IF;
        --Added 4/24/2013 to see if this is a drop ship TMS Ticket - 20130102-01229  
        
          --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1010, 'l_vendor_id = ' || l_vendor_id);
         --COMMIT;
         
         --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1010, 'l_vendor_site_id = ' || l_vendor_site_id);
         --COMMIT;
         
        IF l_vendor_id IS NOT NULL AND l_vendor_site_id IS NOT NULL THEN
        
            
            BEGIN
                  SELECT nvl(plla.drop_ship_flag,'N'), shipment_header_id
                  into   l_drop_ship_flag, l_shipment_header_id
                  FROM   rcv_transactions rt,
                         po_line_locations_all plla
                  WHERE  rt.transaction_id = l_rcv_transaction_id
                  AND    rt.po_line_location_id = plla.line_location_id;
            EXCEPTION
                  WHEN OTHERS THEN
                  
                        l_drop_ship_flag  :=  'N';
                        l_shipment_header_id := NULL;
            END;
            --End Added TMS Ticket - 20130102-01229  
          
        ELSE
                   l_drop_ship_flag  :=  'N';
                   l_shipment_header_id := NULL;

        END IF;   
    
        --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1010, 'l_drop_ship_flag = ' || l_drop_ship_flag);
         --COMMIT;
         
         --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1010, 'l_shipment_header_id = ' || l_shipment_header_id);
         --COMMIT;
         
         
         
        IF l_transaction_source_type_id = 7 THEN
        
           BEGIN
           
              SELECT rsh.organization_id
              INTO   l_source_organization_id
              FROM   rcv_shipment_headers rsh,
                     rcv_transactions rt
              WHERE  rt.transaction_id = l_rcv_transaction_id
              AND    rt.shipment_header_id = rsh.shipment_header_id
              ;
            EXCEPTION
              WHEN others THEN
                  l_source_organization_id := NULL;
            END;
          
        ELSE    
              l_source_organization_id := NULL;
        END IF;
      
         --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1010, 'l_source_organization_id = ' || l_source_organization_id);
         --COMMIT;
        
         
        --Get the default org amount for PO Freight and resource_id
         BEGIN
               SELECT br.resource_id,
                      nvl(ciod.usage_rate_or_amount,0),
                      nvl(br.attribute1,'C'),
                      nvl(ciod.attribute1,0)
               INTO   l_po_freight_moh_id,
                      l_po_freight_moh_rate,
                      l_freight_basis,
                      l_freight_per_lb
               FROM   bom_resources br,
                      cst_item_overhead_defaults ciod
               --WHERE  br.organization_id = i_org_id
               WHERE  br.organization_id = l_organization_id
               AND    br.cost_element_id = 2
               AND    br.resource_code = 'PO Freight'
               AND    br.organization_id = ciod.organization_id
               and    br.resource_id = ciod.material_overhead_id;
         EXCEPTION
          WHEN others  THEN
                   l_po_freight_moh_rate := 0;
                   l_po_freight_moh_id := NULL;
                   l_freight_basis := 'C';
                   l_freight_per_lb := 0;
                   RAISE l_exception;
         
         END;               
         
         
         --Get the default Import Duty resource id for the org
         BEGIN
               SELECT br.resource_id
               INTO   l_import_moh_id
               FROM   bom_resources br
              -- WHERE  br.organization_id = i_org_id
               WHERE br.organization_id = l_organization_id
               AND    br.cost_element_id = 2
               AND    br.resource_code = 'Import Dty';
               
         EXCEPTION
          WHEN others  THEN
                   l_import_moh_id := NULL;
         END;   
         
      if l_vendor_id is not null and l_vendor_site_id is not null and l_transaction_source_type_id = 1 then 
         --Find the custom rates from the custom XXWC_PO_FREIGHT_BURDEN_TBL XPFBT
        BEGIN
            SELECT nvl(xpfbt.freight_duty,0),
                   xpfbt.import_flag,
                   --Added for Hawaii Freight Burden Extension 20130102-01229
                   nvl(xpfbt.freight_per_lb,0),
                   nvl(xpfbt.freight_basis,'C')
            INTO   l_po_freight_moh_rate,
                   l_import_flag,
                   l_freight_per_lb,
                   l_freight_basis
            FROM   XXWC_PO_FREIGHT_BURDEN_TBL XPFBT
            --WHERE  XPFBT.organization_id = i_org_id
            WHERE  xpfbt.organization_id = l_organization_id
            AND    xpfbt.vendor_id = l_vendor_id 
            AND    xpfbt.vendor_site_id = l_vendor_site_id
            AND    xpfbt.vendor_type = 'EXTERNAL';
            
        
        EXCEPTION
            WHEN others  THEN
                   l_po_freight_moh_rate := l_po_freight_moh_rate; --get the rate from the org
                   l_import_flag := 'N';
                   l_freight_per_lb := l_freight_per_lb;
                   l_freight_basis := l_freight_basis; --get the org default basis
        END;
        
        /*
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1011, 'l_po_freight_moh_rate := ' || l_po_freight_moh_rate); --get the rate from the org
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1011, 'l_import_flag := ' || l_import_flag);
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1011, 'l_freight_per_lb := ' || l_freight_per_lb);
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1011, 'l_freight_basis := ' || l_freight_basis); --get the org default basis
         COMMIT;
        */
      
      ELSIF l_source_organization_id is not null and l_transaction_source_type_id = 7 THEN
      
        BEGIN
            SELECT nvl(xpfbt.freight_duty,0),
                   xpfbt.import_flag,
                   --Added for Hawaii Freight Burden Extension 20130102-01229
                   nvl(xpfbt.freight_per_lb,0),
                   nvl(xpfbt.freight_basis,'C')
            INTO   l_po_freight_moh_rate,
                   l_import_flag,
                   l_freight_per_lb,
                   l_freight_basis
            FROM   XXWC_PO_FREIGHT_BURDEN_TBL XPFBT
            --WHERE  XPFBT.organization_id = i_org_id
            WHERE  xpfbt.organization_id = l_organization_id
            AND    xpfbt.vendor_type = 'INTERNAL'
            AND    xpfbt.source_organization_id = l_source_organization_id;
        
        EXCEPTION
            WHEN others  THEN
                   l_po_freight_moh_rate := l_po_freight_moh_rate; --get the rate from the org
                   l_import_flag := 'N';
                   l_freight_per_lb := l_freight_per_lb;
                   l_freight_basis := l_freight_basis; --get the org default basis;
        END;
      /*
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1012, 'l_po_freight_moh_rate := ' || l_po_freight_moh_rate); --get the rate from the org
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1012, 'l_import_flag := ' || l_import_flag);
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1012, 'l_freight_per_lb := ' || l_freight_per_lb);
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1012, 'l_freight_basis := ' || l_freight_basis); --get the org default basis
         COMMIT;
        */
      --Use the seeded
      ELSE
      
                   l_po_freight_moh_rate := l_po_freight_moh_rate; --get the rate from the org
                   l_import_flag := 'N';
                   l_freight_per_lb := l_freight_per_lb;
                   l_freight_basis := l_freight_basis; --get the org default basis;
        /*
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1013, 'l_po_freight_moh_rate := ' || l_po_freight_moh_rate); --get the rate from the org
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1013, 'l_import_flag := ' || l_import_flag);
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1013, 'l_freight_per_lb := ' || l_freight_per_lb);
        INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1013, 'l_freight_basis := ' || l_freight_basis); --get the org default basis
         COMMIT;
        */
      END IF;                   

        --Get the import rate from the item master
        IF l_import_flag = 'Y' THEN
        
        
            --Freight Rate store on attribute 25 at the org level
         BEGIN
              --SELECT nvl(msib.attribute25,0),  --removed 10/30/2013 20131101-00408
              SELECT nvl(msib.attribute27,0),  --updated 10/30/2013 20131101-00408
                     --Added for Hawaii Freight Burden Extension 
                     --Attribute23 is the Unit Weight Override or Unit Weight Per LB
                     nvl(msib.attribute23,nvl(unit_weight,0)) 
              INTO   l_import_dty_moh_rate, l_unit_weight 
              FROM   mtl_system_items_b msib
              --WHERE  msib.organization_id =i_org_id
              WHERE  msib.organization_id = l_organization_id
              AND    msib.inventory_item_id = l_inventory_item_id; 
         EXCEPTION
              WHEN others  THEN
                l_import_dty_moh_rate  := 0;
                l_unit_weight  := 0;
         END;
       
      ELSE

            --Shipping Weight store on attribute 23 at the org level
         BEGIN
              SELECT --Added for Hawaii Freight Burden Extension 20130102-01229
                     --Attribute23 is the Unit Weight Override or Unit Weight Per LB
                     nvl(msib.attribute23,nvl(unit_weight,0))
              INTO   l_unit_weight 
              FROM   mtl_system_items_b msib
              --WHERE  msib.organization_id =i_org_id
              WHERE  msib.organization_id = l_organization_id
              AND    msib.inventory_item_id = l_inventory_item_id; 
         EXCEPTION
              WHEN others  THEN
                l_unit_weight  := 0;
         END;

      
        l_import_dty_moh_rate := 0;
      
      
      END IF;
      
      /*
      INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1013, 'l_import_dty_moh_rate := '||  l_import_dty_moh_rate);
      INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1013, 'l_unit_weight := ' || l_unit_weight);
         COMMIT;
      */
       --Find out if there is Material Cost Element record on MCACD  
        
      --Added logic 4/24/2013 TMS Ticket - 20130102-01229  
      
       --Calculate the MOH_Weight_Cost
       
        l_moh_weight_cost := nvl(l_unit_weight,0) * nvl(l_freight_per_lb,0);
      
      
      --If this is a drop ship po receipt then we need the MOH and Importy Duty to be 0
      
        IF l_drop_ship_flag = 'Y' THEN
        
            l_po_freight_moh_rate := 0;
            l_import_dty_moh_rate := 0;
            l_moh_weight_cost := 0;
        
        END IF;
      /*
      INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1013, 'l_po_freight_moh_rate := ' || l_po_freight_moh_rate);
      INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1013, 'l_import_dty_moh_rate := ' ||l_import_dty_moh_rate);
      INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1013, 'l_moh_weight_cost := ' ||  l_moh_weight_cost);
         COMMIT;
      */
        --End added TMS Ticket - 20130102-01229 
        
          BEGIN
               SELECT COUNT(*)
               INTO   l_mat_exists
               FROM   mtl_cst_actual_cost_details
               WHERE  layer_id = i_layer_id
               AND    transaction_id = i_txn_id
               --AND    organization_id = i_org_id
               AND    organization_id = l_organization_id
               AND    cost_element_id = 1
               AND    level_type = 1
               AND    transaction_action_id = l_transaction_action_id;
          EXCEPTION
               WHEN OTHERS THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving local variable l_mat_exists ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
          END;                    

        
        -- insert into xxwc.xxwc_debug values (sysdate, 1009, 'l_mat_exists ' || l_mat_exists);  
        --commit;
          
          --Find out if there is MOH Cost Element record on MCACD      
          BEGIN
                SELECT COUNT(*)
                INTO   l_mat_oh_exists
                FROM   mtl_cst_actual_cost_details
                WHERE  layer_id = i_layer_id
                AND    transaction_id = i_txn_id
                --AND    organization_id = i_org_id
                AND    organization_id = l_organization_id
                AND    cost_element_id = 2
                AND    level_type = 1
                AND    transaction_action_id = l_transaction_action_id;
          EXCEPTION
                WHEN OTHERS then
                        l_sqlcode := SQLCODE;
                        l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving local variable l_mat_oh_exists ' || substrb(SQLERRM,1,150);
          END;                    

        
           --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1014, 'l_mat_oh_exists ' || l_mat_oh_exists);  
           --commit;
          
          --Find out if there is Material Cost Element record on CLCD      
          BEGIN
               SELECT COUNT(*)
               INTO   l_mat_exists2
               FROM   cst_layer_cost_details
               WHERE  layer_id = i_layer_id
               AND    cost_element_id = 1
               AND    level_type = 1;
               
          EXCEPTION
               WHEN OTHERS then
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving local variable l_mat_exists2 ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
          END;                    

        
         --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1014, 'l_mat_exists2 ' || l_mat_exists2);
         --commit;
        --If a Material Cost Element Record does not exist in CLCD, insert a record (this would be for items that have never been costed)
        
         IF l_mat_exists2 = 0 THEN
            
            BEGIN
         
              INSERT INTO CST_LAYER_COST_DETAILS CLCD
                    (clcd.COST_ELEMENT_ID, 
                     clcd.CREATED_BY, 
                     clcd.CREATION_DATE, 
                     clcd.ITEM_COST, 
                     clcd.LAST_UPDATE_DATE, 
                     clcd.LAST_UPDATE_LOGIN, 
                     clcd.LAST_UPDATED_BY, 
                     clcd.LAYER_ID, 
                     clcd.LEVEL_TYPE, 
                     clcd.PROGRAM_APPLICATION_ID, 
                     clcd.PROGRAM_ID, 
                     clcd.PROGRAM_UPDATE_DATE, 
                     clcd.REQUEST_ID)
                    VALUES
                    (1, --clcd.COST_ELEMENT_ID, 
                     i_user_id, --clcd.CREATED_BY, 
                     sysdate, --clcd.CREATION_DATE, 
                     0, --clcd.ITEM_COST, 
                     sysdate, --clcd.LAST_UPDATE_DATE, 
                     i_login_id,-- clcd.LAST_UPDATE_LOGIN, 
                     i_user_id, --clcd.LAST_UPDATED_BY, 
                     i_layer_id, --clcd.LAYER_ID, 
                     1, --clcd.LEVEL_TYPE, 
                     i_prg_appl_id, 
                     i_prg_id, 
                     sysdate, 
                     i_req_id);
            EXCEPTION
                 WHEN others THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error inserting records in CST_LAYER_COST_DETAILS for Material Subelement ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
              
            END;    
           
         commit;
                   
                     
                          
         END IF;  
                          
      
          --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1014, 'l_mat_insert ' || l_mat_insert);
          --COMMIT;
        
          --Find out if there is Material OH Cost Element record on CLCD      
         BEGIN
               SELECT COUNT(*)
               INTO   l_mat_oh_exists2
               FROM   cst_layer_cost_details
               WHERE  layer_id = i_layer_id
               AND    cost_element_id = 2
               AND    level_type = 1;
               
         EXCEPTION
               WHEN OTHERS then
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving local variable l_mat_oh_exists2 ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
           
         END;                    

         
          --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1014, 'l_mat_oh_exists2 ' || l_mat_oh_exists2);
          --commit;
         
        --If a Material OH Cost Element Record does not exist in CLCD, insert a record (this would be for items that have never been costed)
         
         IF l_mat_oh_exists2 = 0 THEN
           
            BEGIN
             
              INSERT INTO CST_LAYER_COST_DETAILS CLCD
                    (clcd.COST_ELEMENT_ID, 
                     clcd.CREATED_BY, 
                     clcd.CREATION_DATE, 
                     clcd.ITEM_COST, 
                     clcd.LAST_UPDATE_DATE, 
                     clcd.LAST_UPDATE_LOGIN, 
                     clcd.LAST_UPDATED_BY, 
                     clcd.LAYER_ID, 
                     clcd.LEVEL_TYPE, 
                     clcd.PROGRAM_APPLICATION_ID, 
                     clcd.PROGRAM_ID, 
                     clcd.PROGRAM_UPDATE_DATE, 
                     clcd.REQUEST_ID)
                    VALUES
                    (2, --clcd.COST_ELEMENT_ID, 
                     i_user_id, --clcd.CREATED_BY, 
                     sysdate, --clcd.CREATION_DATE, 
                     0, --clcd.ITEM_COST, 
                     --l_transaction_cost*.1, --cld.ITEM_COST --Removed 7/2/2012 by LHS causing an issue on items with out MOH
                     sysdate, --clcd.LAST_UPDATE_DATE, 
                     i_login_id,-- clcd.LAST_UPDATE_LOGIN, 
                     i_user_id, --clcd.LAST_UPDATED_BY, 
                     i_layer_id, --clcd.LAYER_ID, 
                     1, --clcd.LEVEL_TYPE, 
                     i_prg_appl_id, 
                     i_prg_id, 
                     sysdate, 
                     i_req_id);
            EXCEPTION
                   WHEN others THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error inserting records in CST_LAYER_COST_DETAILS for Material Overhead Subelement ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
            END;   
        
          
          COMMIT;
            
         END IF;  
          
         --INSERT INTO xxwc.xxwc_debug VALUES (SYSDATE, 1014, 'l_mat_oh_insert ' || l_mat_insert);
         --commit;
         
         --retrieve the current cost of cost layer from CQL
         BEGIN
            /*SELECTmaterial_cost, 
                   material_overhead_cost,
                   resource_cost,
                   overhead_cost,
                   outside_processing_cost
            INTO   l_material_cost, 
                   l_material_overhead_cost,
                   l_resource_cost,
                   l_overhead_cost,
                   l_outside_processing_cost
            FROM   cst_quantity_layers cql
            WHERE  cql.layer_id = i_layer_id;    
            
            */
            select  nvl(sum(item_cost),0)
            into    l_material_cost
            from    CST_LAYER_COST_DETAILS CLCD
            where   layer_id = i_layer_id
            and     cost_element_id = 1;         
         EXCEPTION
            WHEN OTHERS THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving local variables from CST_LAYER_COST_DETAILS MATERIAL' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
         
         END;
         
         
         BEGIN
         
          
            select  nvl(sum(item_cost),0)
            into    l_material_overhead_cost
            from    CST_LAYER_COST_DETAILS CLCD
            where   layer_id = i_layer_id
            and     cost_element_id = 2;
            
         EXCEPTION
               WHEN OTHERS THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error retrieving local variables from CST_LAYER_COST_DETAILS MATERIAL OVERHEAD ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
         END;                    
  
        --Update Material_Cost
       
      
       IF l_mat_exists = 0 THEN
        --Insert into the MTL_CST_ACTUAL_COST_DETAILS Table
       
         BEGIN --Material Cost
       
         INSERT INTO mtl_cst_actual_cost_details
                                    (LAYER_ID
                                    ,TRANSACTION_ID
                                    ,ORGANIZATION_ID
                                    ,COST_ELEMENT_ID
                                    ,LEVEL_TYPE
                                    ,TRANSACTION_ACTION_ID
                                    ,LAST_UPDATE_DATE
                                    ,LAST_UPDATED_BY
                                    ,CREATION_DATE
                                    ,CREATED_BY
                                    ,LAST_UPDATE_LOGIN
                                    ,REQUEST_ID
                                    ,PROGRAM_APPLICATION_ID
                                    ,PROGRAM_ID
                                    ,PROGRAM_UPDATE_DATE
                                    ,INVENTORY_ITEM_ID
                                    ,ACTUAL_COST
                                    ,PRIOR_COST
                                    ,NEW_COST
                                    ,INSERTION_FLAG
                                    ,VARIANCE_AMOUNT
                                    ,USER_ENTERED
                                    ,TRANSACTION_COSTED_DATE
                                    ,PAYBACK_VARIANCE_AMOUNT)
                                VALUES
                                    (i_layer_id --LAYER_ID
                                    ,i_txn_id--TRANSACTION_ID
                                    ,l_organization_id --i_org_id --,ORGANIZATION_ID
                                    ,1 --c_layer.cost_element_id --COST_ELEMENT_ID
                                    ,1 --c_layer.level_type --LEVEL_TYPE
                                    ,l_transaction_action_id --TRANSACTION_ACTION_ID
                                    ,sysdate --LAST_UPDATE_DATE
                                    ,i_user_id --LAST_UPDATED_BY
                                    ,SYSDATE --CREATION_DATE
                                    ,i_user_id --CREATED_BY
                                    ,i_login_id --LAST_UPDATE_LOGIN
                                    ,i_req_id --REQUEST_ID
                                    ,i_prg_appl_id --PROGRAM_APPLICATION_ID
                                    ,i_prg_id --PROGRAM_ID
                                    ,SYSDATE --PROGRAM_UPDATE_DATE
                                    ,l_inventory_item_Id --INVENTORY_ITEM_ID
                                    ,l_transaction_cost --ACTUAL_COST
                                    ,l_material_cost --i.item_cost --PRIOR_COST
                                    ,l_transaction_cost --NEW_COST
                                    ,'N' --INSERTION_FLAG
                                    ,0 --VARIANCE_AMOUNT
                                    ,'Y' --USER_ENTERED
                                    ,SYSDATE --TRANSACTION_COSTED_DATE
                                    ,NULL); --PAYBACK_VARIANCE_AMOUNT)
          
        --Update Material_Over_Head_Cost
         EXCEPTION
                   WHEN others THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error inserting records in MTL_CST_ACTUAL_COST_DETAILS for Material Subelement ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
         END;   
    
      
       END IF;
       
       
         --insert into xxwc.xxwc_debug values (sysdate, 1015, 'l_po_freight_moh_id ' || l_po_freight_moh_id);
         --commit;
        
         --insert into xxwc.xxwc_debug values (sysdate, 1015, 'l_import_moh_id ' || l_import_moh_id);
         --commit;
        
       IF l_mat_oh_exists = 0 THEN
       
        --Hawaii Freight Burden
        
         IF l_freight_basis = 'W' THEN
         
         
           BEGIN ----Material OH Cost for PO Duty and Import Duty
           
               INSERT INTO mtl_cst_actual_cost_details
                                    (LAYER_ID
                                    ,TRANSACTION_ID
                                    ,ORGANIZATION_ID
                                    ,COST_ELEMENT_ID
                                    ,LEVEL_TYPE
                                    ,TRANSACTION_ACTION_ID
                                    ,LAST_UPDATE_DATE
                                    ,LAST_UPDATED_BY
                                    ,CREATION_DATE
                                    ,CREATED_BY
                                    ,LAST_UPDATE_LOGIN
                                    ,REQUEST_ID
                                    ,PROGRAM_APPLICATION_ID
                                    ,PROGRAM_ID
                                    ,PROGRAM_UPDATE_DATE
                                    ,INVENTORY_ITEM_ID
                                    ,ACTUAL_COST
                                    ,PRIOR_COST
                                    ,NEW_COST
                                    ,INSERTION_FLAG
                                    ,VARIANCE_AMOUNT
                                    ,USER_ENTERED
                                    ,TRANSACTION_COSTED_DATE
                                    ,PAYBACK_VARIANCE_AMOUNT)
                                VALUES
                                    (i_layer_id --LAYER_ID
                                    ,i_txn_id--TRANSACTION_ID
                                    ,l_organization_id --i_org_id --,ORGANIZATION_ID
                                    ,2 --c_layer.cost_element_id --COST_ELEMENT_ID
                                    ,1 --c_layer.level_type --LEVEL_TYPE
                                    ,l_transaction_action_id --TRANSACTION_ACTION_ID
                                    ,sysdate --LAST_UPDATE_DATE
                                    ,i_user_id --LAST_UPDATED_BY
                                    ,sysdate --CREATION_DATE
                                    ,i_user_id --CREATED_BY
                                    ,i_login_id --LAST_UPDATE_LOGIN
                                    ,i_req_id --REQUEST_ID
                                    ,i_prg_appl_id --PROGRAM_APPLICATION_ID
                                    ,i_prg_id --PROGRAM_ID
                                    ,sysdate --PROGRAM_UPDATE_DATE
                                    ,l_inventory_item_id --INVENTORY_ITEM_ID
                                    ,((l_moh_weight_cost)+(l_transaction_cost*l_import_dty_moh_rate)) --ACTUAL_COST
                                    ,l_material_overhead_cost --i.item_cost --PRIOR_COST
                                    ,l_transaction_cost--NEW_COST
                                    ,'N'--l_mat_oh_insert --INSERTION_FLAG
                                    ,0 --VARIANCE_AMOUNT
                                    ,'Y' --USER_ENTERED
                                    ,sysdate --TRANSACTION_COSTED_DATE
                                    ,null); --PAYBACK_VARIANCE_AMOUNT)
                                
                                                                
                    
           EXCEPTION
                   WHEN others THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error inserting records in MTL_CST_ACTUAL_COST_DETAILS for Material OH Subelement ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
           END;  
         
         ELSE
         
         
           BEGIN ----Material OH Cost for PO Duty and Import Duty
           
               INSERT INTO mtl_cst_actual_cost_details
                                    (LAYER_ID
                                    ,TRANSACTION_ID
                                    ,ORGANIZATION_ID
                                    ,COST_ELEMENT_ID
                                    ,LEVEL_TYPE
                                    ,TRANSACTION_ACTION_ID
                                    ,LAST_UPDATE_DATE
                                    ,LAST_UPDATED_BY
                                    ,CREATION_DATE
                                    ,CREATED_BY
                                    ,LAST_UPDATE_LOGIN
                                    ,REQUEST_ID
                                    ,PROGRAM_APPLICATION_ID
                                    ,PROGRAM_ID
                                    ,PROGRAM_UPDATE_DATE
                                    ,INVENTORY_ITEM_ID
                                    ,ACTUAL_COST
                                    ,PRIOR_COST
                                    ,NEW_COST
                                    ,INSERTION_FLAG
                                    ,VARIANCE_AMOUNT
                                    ,USER_ENTERED
                                    ,TRANSACTION_COSTED_DATE
                                    ,PAYBACK_VARIANCE_AMOUNT)
                                VALUES
                                    (i_layer_id --LAYER_ID
                                    ,i_txn_id--TRANSACTION_ID
                                    ,l_organization_id --i_org_id --,ORGANIZATION_ID
                                    ,2 --c_layer.cost_element_id --COST_ELEMENT_ID
                                    ,1 --c_layer.level_type --LEVEL_TYPE
                                    ,l_transaction_action_id --TRANSACTION_ACTION_ID
                                    ,sysdate --LAST_UPDATE_DATE
                                    ,i_user_id --LAST_UPDATED_BY
                                    ,sysdate --CREATION_DATE
                                    ,i_user_id --CREATED_BY
                                    ,i_login_id --LAST_UPDATE_LOGIN
                                    ,i_req_id --REQUEST_ID
                                    ,i_prg_appl_id --PROGRAM_APPLICATION_ID
                                    ,i_prg_id --PROGRAM_ID
                                    ,sysdate --PROGRAM_UPDATE_DATE
                                    ,l_inventory_item_id --INVENTORY_ITEM_ID
                                    ,((l_transaction_cost*l_po_freight_moh_rate)+(l_transaction_cost*l_import_dty_moh_rate)) --ACTUAL_COST
                                    ,l_material_overhead_cost --i.item_cost --PRIOR_COST
                                    ,l_transaction_cost--NEW_COST
                                    ,'N'--l_mat_oh_insert --INSERTION_FLAG
                                    ,0 --VARIANCE_AMOUNT
                                    ,'Y' --USER_ENTERED
                                    ,sysdate --TRANSACTION_COSTED_DATE
                                    ,null); --PAYBACK_VARIANCE_AMOUNT)
                                
                                                                
                    
           EXCEPTION
                   WHEN others THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error inserting records in MTL_CST_ACTUAL_COST_DETAILS for Material OH Subelement ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
           END;   

          END IF;
           ----Material OH Cost for PO Duty Cost Elemet
                    
          --If the resource_id for po freight duty is not null then 
          IF l_po_freight_moh_id IS NOT NULL THEN

    
              --If Hawaii Freight Burden
              
              IF l_freight_basis = 'W' THEN


                BEGIN


                   INSERT INTO mtl_actual_cost_subelement
                                       (LAYER_ID
                                       ,TRANSACTION_ID
                                       ,ORGANIZATION_ID
                                       ,COST_ELEMENT_ID
                                       ,LEVEL_TYPE
                                       ,RESOURCE_ID
                                       ,LAST_UPDATE_DATE
                                       ,LAST_UPDATED_BY
                                       ,CREATION_DATE
                                       ,CREATED_BY
                                       ,LAST_UPDATE_LOGIN
                                       ,REQUEST_ID
                                       ,PROGRAM_APPLICATION_ID
                                       ,PROGRAM_ID
                                       ,PROGRAM_UPDATE_DATE
                                       ,ACTUAL_COST
                                       ,USER_ENTERED
    )
                                    VALUES
                                        (i_layer_id --LAYER_ID
                                        ,i_txn_id--TRANSACTION_ID
                                        ,l_organization_id --i_org_id --,ORGANIZATION_ID
                                        ,2 --c_layer.cost_element_id --COST_ELEMENT_ID
                                        ,1 --c_layer.level_type --LEVEL_TYPE
                                        ,l_po_freight_moh_id --Resource ID for PO Freight MOH ID
                                        ,sysdate --LAST_UPDATE_DATE
                                        ,i_user_id --LAST_UPDATED_BY
                                        ,sysdate --CREATION_DATE
                                        ,i_user_id --CREATED_BY
                                        ,i_login_id --LAST_UPDATE_LOGIN
                                        ,i_req_id --REQUEST_ID
                                        ,i_prg_appl_id --PROGRAM_APPLICATION_ID
                                        ,i_prg_id --PROGRAM_ID
                                        ,SYSDATE --PROGRAM_UPDATE_DATE
                                        ,l_moh_weight_cost--ACTUAL_COST
                                        ,'Y'); --USER_ENTERED
                                    
                                                                    
                        
               EXCEPTION
                       WHEN others THEN
                       l_sqlcode := SQLCODE;
                       l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error inserting records in MTL_CST_ACTUAL_COST_DETAILS for Material OH Subelement ' || substrb(SQLERRM,1,150);
                       RAISE l_exception;
               END;               

                  
              
              ELSE            
    
               BEGIN
                   INSERT INTO mtl_actual_cost_subelement
                                       (LAYER_ID
                                       ,TRANSACTION_ID
                                       ,ORGANIZATION_ID
                                       ,COST_ELEMENT_ID
                                       ,LEVEL_TYPE
                                       ,RESOURCE_ID
                                       ,LAST_UPDATE_DATE
                                       ,LAST_UPDATED_BY
                                       ,CREATION_DATE
                                       ,CREATED_BY
                                       ,LAST_UPDATE_LOGIN
                                       ,REQUEST_ID
                                       ,PROGRAM_APPLICATION_ID
                                       ,PROGRAM_ID
                                       ,PROGRAM_UPDATE_DATE
                                       ,ACTUAL_COST
                                       ,USER_ENTERED
    )
                                    VALUES
                                        (i_layer_id --LAYER_ID
                                        ,i_txn_id--TRANSACTION_ID
                                        ,l_organization_id --i_org_id --,ORGANIZATION_ID
                                        ,2 --c_layer.cost_element_id --COST_ELEMENT_ID
                                        ,1 --c_layer.level_type --LEVEL_TYPE
                                        ,l_po_freight_moh_id --Resource ID for PO Freight MOH ID
                                        ,sysdate --LAST_UPDATE_DATE
                                        ,i_user_id --LAST_UPDATED_BY
                                        ,sysdate --CREATION_DATE
                                        ,i_user_id --CREATED_BY
                                        ,i_login_id --LAST_UPDATE_LOGIN
                                        ,i_req_id --REQUEST_ID
                                        ,i_prg_appl_id --PROGRAM_APPLICATION_ID
                                        ,i_prg_id --PROGRAM_ID
                                        ,sysdate --PROGRAM_UPDATE_DATE
                                        ,l_transaction_cost*l_po_freight_moh_rate --ACTUAL_COST
                                        ,'Y'); --USER_ENTERED
                                    
                                                                    
                        
               EXCEPTION
                       WHEN others THEN
                       l_sqlcode := SQLCODE;
                       l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error inserting records in MTL_CST_ACTUAL_COST_DETAILS for Material OH Subelement ' || substrb(SQLERRM,1,150);
                       RAISE l_exception;
               END;               
    
    
    
            
              END IF;
          
          END IF;
          
          IF l_import_flag =  'Y' AND l_import_moh_id IS NOT NULL THEN
           
          
           ----Material OH Cost for PO Duty Cost Elemet
                    
           BEGIN
               INSERT INTO mtl_actual_cost_subelement
                                   (LAYER_ID
                                   ,TRANSACTION_ID
                                   ,ORGANIZATION_ID
                                   ,COST_ELEMENT_ID
                                   ,LEVEL_TYPE
                                   ,RESOURCE_ID
                                   ,LAST_UPDATE_DATE
                                   ,LAST_UPDATED_BY
                                   ,CREATION_DATE
                                   ,CREATED_BY
                                   ,LAST_UPDATE_LOGIN
                                   ,REQUEST_ID
                                   ,PROGRAM_APPLICATION_ID
                                   ,PROGRAM_ID
                                   ,PROGRAM_UPDATE_DATE
                                   ,ACTUAL_COST
                                   ,USER_ENTERED
)
                                VALUES
                                    (i_layer_id --LAYER_ID
                                    ,i_txn_id--TRANSACTION_ID
                                    ,l_organization_id --i_org_id --,ORGANIZATION_ID
                                    ,2 --c_layer.cost_element_id --COST_ELEMENT_ID
                                    ,1 --c_layer.level_type --LEVEL_TYPE
                                    ,l_import_moh_id  --Resource ID for Import Duty MOH ID
                                    ,sysdate --LAST_UPDATE_DATE
                                    ,i_user_id --LAST_UPDATED_BY
                                    ,sysdate --CREATION_DATE
                                    ,i_user_id --CREATED_BY
                                    ,i_login_id --LAST_UPDATE_LOGIN
                                    ,i_req_id --REQUEST_ID
                                    ,i_prg_appl_id --PROGRAM_APPLICATION_ID
                                    ,i_prg_id --PROGRAM_ID
                                    ,sysdate --PROGRAM_UPDATE_DATE
                                    ,l_transaction_cost*l_import_dty_moh_rate --ACTUAL_COST
                                    ,'Y'); --USER_ENTERED
                                
                                                                
                    
           EXCEPTION
                   WHEN others THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error inserting records in MTL_CST_ACTUAL_COST_DETAILS for Material OH Subelement ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
           END;               
           
           
         END IF;


        
        if l_transfer_organization_id is not null and l_transfer_transaction_id is not null then
        
          BEGIN
          
             INSERT INTO mtl_cst_actual_cost_details
                                    (LAYER_ID
                                    ,TRANSACTION_ID
                                    ,ORGANIZATION_ID
                                    ,COST_ELEMENT_ID
                                    ,LEVEL_TYPE
                                    ,TRANSACTION_ACTION_ID
                                    ,LAST_UPDATE_DATE
                                    ,LAST_UPDATED_BY
                                    ,CREATION_DATE
                                    ,CREATED_BY
                                    ,LAST_UPDATE_LOGIN
                                    ,REQUEST_ID
                                    ,PROGRAM_APPLICATION_ID
                                    ,PROGRAM_ID
                                    ,PROGRAM_UPDATE_DATE
                                    ,INVENTORY_ITEM_ID
                                    ,ACTUAL_COST
                                    ,PRIOR_COST
                                    ,NEW_COST
                                    ,INSERTION_FLAG
                                    ,VARIANCE_AMOUNT
                                    ,USER_ENTERED
                                    ,TRANSACTION_COSTED_DATE
                                    ,PAYBACK_VARIANCE_AMOUNT)
                                  (SELECT LAYER_ID
                                    ,i_txn_id --TRANSACTION_ID
                                    ,i_org_id
                                    ,COST_ELEMENT_ID
                                    ,LEVEL_TYPE
                                    ,l_TRANSACTION_ACTION_ID
                                    ,sysdate
                                    ,i_user_id
                                    ,SYSDATE
                                    ,i_user_id
                                    ,i_login_id
                                    ,i_req_id
                                    ,i_prg_appl_id
                                    ,i_prg_id
                                    ,sysdate
                                    ,INVENTORY_ITEM_ID
                                    ,ACTUAL_COST
                                    ,PRIOR_COST
                                    ,NEW_COST
                                    ,INSERTION_FLAG
                                    ,VARIANCE_AMOUNT
                                    ,'Y'
                                    ,SYSDATE
                                    ,PAYBACK_VARIANCE_AMOUNT
                                    FROM mtl_cst_actual_cost_details
                                    WHERE transaction_id = l_transfer_transaction_id
                                    and   organization_id = l_transfer_organization_id);
                                
            
           EXCEPTION
                   WHEN others THEN
                   l_sqlcode := SQLCODE;
                   l_message := 'CSTPACHK.ACTUAL_COST_HOOK:' || 'Error inserting transfer cost record into MTL_CST_ACTUAL_COST_DETAILS  ' || substrb(SQLERRM,1,150);
                   RAISE l_exception;
           END;               
           
        END IF;
        
            IF l_transfer_organization_id IS NULL THEN
               l_update_cost := 1;
            ELSE
               l_update_cost := 0;
            END IF;
            -- insert into xxwc.xxwc_debug values (sysdate, 1010, 'Inserted into mtl_cst_actual_cost_details');
            
            commit;           
       ELSE 
           
            l_update_cost := 0;
            
              
       END IF;    

    
    ELSE 
        
        -- insert into xxwc.xxwc_debug values (sysdate, 1010, 'Did not insert, hit the else statement');
        commit;
        l_update_cost := 0;

    END IF;

  ELSE
  
     -- insert into xxwc.xxwc_debug values (sysdate, 1010, 'Did not insert, hit the else statement');
       commit;
        l_update_cost := 0;

  END IF;

  o_err_num := 0;
  o_err_code := '';
  o_err_msg := '';

    -- insert into xxwc.xxwc_debug values (sysdate, 1011, 'l_update_cost ' || l_update_cost);
    commit;
  COMMIT;  
    
  RETURN l_update_cost;
  
  



EXCEPTION
  
  WHEN l_exception THEN
    
    ROLLBACK;
    o_err_num := l_sqlcode;
    o_err_msg := l_message;
    RETURN 0;


  WHEN OTHERS THEN
    o_err_num := SQLCODE;
    o_err_msg := 'CSTPACHK.ACTUAL_COST_HOOK:' || substrb(SQLERRM,1,150);
    -- insert into xxwc.xxwc_debug values (sysdate, 1012, 'Program Exception ' || o_err_num || o_err_msg);
    return 0;
    commit;
END actual_cost_hook;


-- FUNCTION
--  cost_dist_hook		Cover routine to allow users to customize.
--				They will be able to circumvent the
--				average cost distribution processor.
--
--
-- RETURN VALUES
--  integer		1	Hook has been used.
--			0	Continue cost distribution for this transaction
--				as ususal.
--
function cost_dist_hook(
  I_ORG_ID		IN	NUMBER,
  I_TXN_ID		IN 	NUMBER,
  I_USER_ID		IN	NUMBER,
  I_LOGIN_ID    	IN	NUMBER,
  I_REQ_ID		IN	NUMBER,
  I_PRG_APPL_ID		IN	NUMBER,
  I_PRG_ID		IN 	NUMBER,
  O_Err_Num		OUT NOCOPY	NUMBER,
  O_Err_Code		OUT NOCOPY	VARCHAR2,
  O_Err_Msg		OUT NOCOPY	VARCHAR2
)
return integer  IS
BEGIN
  o_err_num := 0;
  o_err_code := '';
  o_err_msg := '';

  return 0;

EXCEPTION

  when others then
    o_err_num := SQLCODE;
    o_err_msg := 'CSTPACHK.COST_DIST_HOOK:' || substrb(SQLERRM,1,150);
    return 0;

END cost_dist_hook;

-- FUNCTION
--  get_account_id		Cover routine to allow users the flexbility
--				in determining the account they want to
--				post the inventory transaction to.
--
--
-- RETURN VALUES
--  integer		>0	User selected account number
--			-1  	Use the default account for distribution.
--                       0      Error
--
/*
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        23-MAR-15   Lee Spitzer       1. Updated the function
                                                  20150316-00080 - Optimize Workflow for the Account Generation Extension: Average Costing
                                                  Function always returns -1
*/

function get_account_id(
  I_ORG_ID		IN	NUMBER,
  I_TXN_ID		IN 	NUMBER,
  I_DEBIT_CREDIT	IN	NUMBER,
  I_ACCT_LINE_TYPE	IN	NUMBER,
  I_COST_ELEMENT_ID	IN	NUMBER,
  I_RESOURCE_ID		IN	NUMBER,
  I_SUBINV		IN	VARCHAR2,
  I_EXP			IN	NUMBER,
  I_SND_RCV_ORG		IN	NUMBER,
  O_Err_Num		OUT NOCOPY	NUMBER,
  O_Err_Code		OUT NOCOPY	VARCHAR2,
  O_Err_Msg		OUT NOCOPY	VARCHAR2,
  I_COST_GROUP_ID       IN      NUMBER  DEFAULT NULL /*8881927*/
)
return integer  IS

l_account_num number := -1;
l_cost_method number;
l_txn_type_id number;
l_txn_act_id number;
l_txn_src_type_id number;
l_item_id number;
l_cg_id number;
l_txn_cg_id number;/*8881927*/
wf_err_num number := 0;
wf_err_code varchar2(500) := '';
wf_err_msg varchar2(500) := '';

BEGIN
  o_err_num := 0;
  o_err_code := '';
  o_err_msg := '';

/*03/23/2015 - 20150316-00080 commented out
  SELECT transaction_type_id,
         transaction_action_id,
         transaction_source_type_id,
         inventory_item_id,
         /* BUG#5970447 FP of 5839922 */
         /* BUG#7149071 fix: regression from bug 5970447
            for regular transactions, cost group to be obtained from
            cost_group_id, for project related transactions, cost group to be
            obtained from the logic of i_debit_credit flag either from
            current txn cost_group_id or transfer cost group id
         */ /* 03/23/2015
         DECODE(transaction_type_id, 67,
         decode(i_debit_credit, -1, nvl(cost_group_id, -1), 1,
                nvl(transfer_cost_group_id, nvl(cost_group_id, -1))),
         66,decode(i_debit_credit, -1, nvl(cost_group_id, -1), 1,
                nvl(transfer_cost_group_id, nvl(cost_group_id, -1))),
         68,decode(i_debit_credit, -1, nvl(cost_group_id, -1), 1,
                nvl(transfer_cost_group_id, nvl(cost_group_id, -1))),
         nvl(cost_group_id, -1))
  INTO   l_txn_type_id,
         l_txn_act_id,
         l_txn_src_type_id,
         l_item_id,
         l_txn_cg_id /*BUG 8881927*/  /*03/23/2015 - 20150316-00080 commented out
  FROM   MTL_MATERIAL_TRANSACTIONS
  WHERE  transaction_id = I_TXN_ID;
*/
  /*BUG 8881927*/
  /*03/23/2015 - 20150316-00080 commented out
  if (NVL(I_COST_GROUP_ID,0) =0) THEN
       l_cg_id :=l_txn_cg_id;
  else
       l_cg_id :=I_COST_GROUP_ID;
  end if;
  

  l_account_num := CSTPACWF.START_AVG_WF(i_txn_id, l_txn_type_id,l_txn_act_id,
                                          l_txn_src_type_id,i_org_id, l_item_id,
                                        i_cost_element_id,i_acct_line_type,
                                        l_cg_id,i_resource_id,
                                        wf_err_num, wf_err_code, wf_err_msg);
    o_err_num := NVL(wf_err_num, 0);
    o_err_code := NVL(wf_err_code, 'No Error in CSTPAWF.START_AVG_WF');
    o_err_msg := NVL(wf_err_msg, 'No Error in CSTPAWF.START_AVG_WF');

-- if -1 then use default account, else use this account for distribution

   return l_account_num;
*/

    RETURN -1;  --03/23/2015 - 20150316-00080 set return to be -1
    
EXCEPTION

  when others then
    o_err_num := -1;
    o_err_code := to_char(SQLCODE);
    o_err_msg := 'Error in CSTPACHK.GET_ACCOUNT_ID:' || substrb(SQLERRM,1,150);
    return 0;

END get_account_id;

-- FUNCTION
--  layer_hook                  This routine is a client extension that lets the
--                              user specify which layer to consume from.
--
--
-- RETURN VALUES
--  integer             >0      Hook has been used,return value is inv layer id.
--                      0       Hook has not been used.
--                      -1      Error in Hook.

function layer_hook(
  I_ORG_ID      IN      NUMBER,
  I_TXN_ID      IN      NUMBER,
  I_LAYER_ID    IN      NUMBER,
  I_COST_METHOD IN      NUMBER,
  I_USER_ID     IN      NUMBER,
  I_LOGIN_ID    IN      NUMBER,
  I_REQ_ID      IN      NUMBER,
  I_PRG_APPL_ID IN      NUMBER,
  I_PRG_ID      IN      NUMBER,
  O_Err_Num     OUT NOCOPY     NUMBER,
  O_Err_Code    OUT NOCOPY     VARCHAR2,
  O_Err_Msg     OUT NOCOPY     VARCHAR2
)
return integer  IS
BEGIN
  o_err_num := 0;
  o_err_code := '';
  o_err_msg := '';

  return 0;

EXCEPTION

  when others then
    o_err_num := SQLCODE;
    o_err_msg := 'CSTPACHK.layer_hook:' || substrb(SQLERRM,1,150);
    return 0;

END layer_hook;

PROCEDURE layers_hook(
     i_txn_id        IN            NUMBER,
     i_required_qty  IN            NUMBER,
     i_cost_method   IN            NUMBER,
     o_custom_layers IN OUT NOCOPY inv_layer_tbl,
     o_err_num       OUT NOCOPY    NUMBER,
     o_err_code      OUT NOCOPY    VARCHAR2,
     o_err_msg       OUT NOCOPY    VARCHAR2
   )
   IS
   BEGIN
     o_err_num := 0;
     o_err_code := '';
     o_err_msg := '';

     -- To customize this hook, extend o_custom_layers and populate
     -- it with inv_layer_id(s) of record(s) in CST_INV_LAYERS that
     -- correspond to the organization, item and cost group of the
     -- transaction. Also specify the quantity that should be
     -- consumed for each layer. The quantity must be positive and
     -- must be less than or equal to the available quantity in the
     -- specified layer.

     -- When the total quantity of the custom layers is less
     -- than the required quantity, the regular consumption
     -- logic will be used to derive the layers that should be
     -- consumed for the rest of the quantity. By default, this
     -- hook does not specify any custom layers, which means that
     -- the regular consumption logic will be used for all of the
     -- required quantity.

   EXCEPTION
     WHEN OTHERS THEN
       o_err_num := SQLCODE;
       o_err_msg := 'CSTPACHK.layers_hook:' || substrb(SQLERRM,1,150);
   END;

   -- FUNCTION
   --  LayerMerge_hook             This routine is a client extension that lets the
   --                              user specify if layer merge should be attempted.
   --
   -- PARAMETERS
   --  i_txn_id                    Id of the receipt transaction in
   --                              MTL_MATERIAL_TRANSACTIONS
   --  o_err_num                   0 indicates no error. Other values indicates errors.
   --  o_err_code                  A short code to help identify errors.
   --  o_err_msg                   A message to help identify errors.
   --
   -- RETURN VALUE
   --  1                           Attempt to combine the quantity from the specified
   --                              receipt transaction with an existing inventory layer
   --  0                           Create a new inventory layer for the specified
   --                              receipt transaction

   FUNCTION LayerMerge_hook(
     i_txn_id        IN            NUMBER,
     o_err_num       OUT NOCOPY    NUMBER,
     o_err_code      OUT NOCOPY    VARCHAR2,
     o_err_msg       OUT NOCOPY    VARCHAR2
   )
   RETURN INTEGER
   IS
   BEGIN
     o_err_num := 0;
     o_err_code := '';
     o_err_msg := '';
     -- By default, the program will attempt to merge layers
     RETURN 1;
   EXCEPTION
     WHEN OTHERS THEN
       o_err_num := SQLCODE;
       o_err_msg := 'CSTPACHK.layers_hook:' || substrb(SQLERRM,1,150);
       RETURN -1;
   END;

function get_date(
  I_ORG_ID              IN      NUMBER,
  O_Error_Message       OUT NOCOPY     VARCHAR2
)
return date IS
BEGIN
   return (SYSDATE+1);
END get_date;

-- FUNCTION
--  get_absorption_account_id
--    Cover routing to allow users to specify the resource absorption account
--    based on the resource instance and charge department
--
--  Return Values
--   integer		> 0 	User selected account number
--   			 -1	Use default account
--                        0     get_absorption_account_id failed
--
function get_absorption_account_id (
	I_ORG_ID		IN	NUMBER,
 	I_TXN_ID		IN	NUMBER,
	I_CHARGE_DEPT_ID	IN	NUMBER,
	I_RES_INSTANCE_ID	IN	NUMBER
)
return integer IS

l_account_num 	NUMBER	:= -1;

BEGIN

 return l_account_num;

EXCEPTION
  when others then
   return 0;

END get_absorption_account_id;


-- FUNCTION validate_job_est_status_hook
--  introduced as part of support for EAM Job Costing
--  This function can be modified to contain validations that allow/disallow
--  job cost re-estimation.
--  The Work Order Value summary form calls this function, to determine if the
--  re-estimation flag can be updated or not. If the function is not used, then
--  the default validations contained in cst_eamcost_pub.validate_for_reestimation
--  procedure will be implemented
-- RETURN VALUES
--   0  	hook is not used or procedure raises exception
--   1		hook is used
-- VALUES for o_validate_flag
--   0		reestimation flag is not updateable
--   1          reestimation flag is updateable

function validate_job_est_status_hook (
   	i_wip_entity_id		IN	NUMBER,
	i_job_status		IN	NUMBER,
	i_curr_est_status	IN	NUMBER,
	o_validate_flag		OUT NOCOPY	NUMBER,
	o_err_num		OUT NOCOPY	NUMBER,
	o_err_code		OUT NOCOPY	VARCHAR2,
	o_err_msg		OUT NOCOPY	VARCHAR2 )
return integer IS

l_hook	NUMBER	:= 0;
l_err_num NUMBER := 0;
l_err_code VARCHAR2(240) := '';
l_err_msg  VARCHAR2(8000) := '';

BEGIN

  o_err_num := l_err_num;
  o_err_code := l_err_code;
  o_err_msg := l_err_msg;
  return l_hook;

EXCEPTION
  when others then
    o_err_num := SQLCODE;
    o_err_msg := 'CSTPACHK.layer_hook:' || substrb(SQLERRM,1,150);
    return 0;
END validate_job_est_status_hook;

--
-- OPM INVCONV umoogala  Process-Discrete Xfers Enh.
-- Hook to get transfer price
--
procedure Get_xfer_price_user_hook
  ( p_api_version                       IN            NUMBER
  , p_init_msg_list                     IN            VARCHAR2

  , p_transaction_uom                   IN            VARCHAR2
  , p_inventory_item_id                 IN            NUMBER
  , p_transaction_id                    IN            NUMBER
  , p_from_organization_id              IN            NUMBER
  , p_to_organization_id                IN            NUMBER
  , p_from_ou                           IN            NUMBER
  , p_to_ou                             IN            NUMBER

  , x_return_status                     OUT NOCOPY    NUMBER
  , x_msg_data                          OUT NOCOPY    VARCHAR2
  , x_msg_count                         OUT NOCOPY    NUMBER

  , x_transfer_price                    OUT NOCOPY    NUMBER
  , x_currency_code                     OUT NOCOPY    VARCHAR2
  )
IS
BEGIN

 x_return_status := -1;
 x_msg_count     := 0;
 
END;


END CSTPACHK;