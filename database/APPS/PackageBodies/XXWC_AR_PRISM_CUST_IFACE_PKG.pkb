--
-- XXWC_AR_PRISM_CUST_IFACE_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ar_prism_cust_iface_pkg
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_ar_prism_cust_iface_pkg.pls 120.1.12010000.1 $
     Module Name: xxwc_ar_prism_cust_iface_pkg.pks

     PURPOSE:   This package is called by a UC4 scheduler and controller
         program that will then initiate Concurrent Program
                XXWC Prism-EBS Customer Interface Processing
                to validate and process cash accounts, job sites and contacts


     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        03/13/2012  Consuelo Gonzalez      Initial Version
   **************************************************************************/

   /*************************************************************************
     Procedure :  populate_collectors

     Purpose:    Procedure called by validate_data that will repopulate the
         xref staging table for collectors with their matching
                   acting EBS Collector ID
   ************************************************************************/
   PROCEDURE populate_collectors
   IS
      CURSOR collector_cur
      IS
         SELECT xca.oracle_collector, ac.collector_id
           FROM (SELECT DISTINCT oracle_collector
                   FROM xxwc_collector_cr_analyst_xref
                  WHERE collector_id IS NULL) xca
               ,ar_collectors ac
          WHERE     UPPER (ac.name) = UPPER (oracle_collector)
                AND ac.status = 'A';
   BEGIN
      -- To ensure we are not getting Collector_ID from Other Instances
      UPDATE xxwc_collector_cr_analyst_xref
         SET collector_id = NULL
       WHERE Collector_ID IS NOT NULL;

      FOR collector_rec IN collector_cur
      LOOP
         -- Loop through all the Collectors and update Collector ID in XREF Table.

         BEGIN
            UPDATE xxwc_collector_cr_analyst_xref
               SET collector_id = collector_rec.collector_id
             WHERE UPPER (oracle_collector) =
                      UPPER (collector_rec.oracle_collector);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END LOOP;

      COMMIT;
   END populate_collectors;

   /*************************************************************************
     Procedure :  populate_credit_analyst

     Purpose:    Procedure called by validate_data that will repopulate
         the xref staging table for credit analysts with a matching
                   EBS Credit Analyst
   ************************************************************************/
   PROCEDURE populate_credit_analyst
   IS
      CURSOR crdt_analyst_cur
      IS
         SELECT DISTINCT c.resource_id, x.oracle_credit_analyst, x.prism_code
           FROM jtf_rs_role_relations a
               ,jtf_rs_roles_vl b
               ,jtf_rs_resource_extns_vl c
               ,per_all_people_f per
               ,xxwc_collector_cr_analyst_xref x
          WHERE     a.role_resource_type = 'RS_INDIVIDUAL'
                AND a.role_resource_id = c.resource_id
                AND a.role_id = b.role_id
                AND b.role_code = 'CREDIT_ANALYST'
                AND c.category = 'EMPLOYEE'
                AND c.source_id = per.person_id
                AND SYSDATE BETWEEN per.effective_start_date
                                AND per.effective_end_date
                AND per.current_employee_flag = 'Y'
                AND NVL (a.delete_flag, 'N') <> 'Y'
                AND UPPER (c.resource_name) = UPPER (x.oracle_credit_analyst)
                AND x.credit_analyst_id IS NULL;
   BEGIN
      -- To ensure we are not getting credit_analyst_id from Other Instances.
      UPDATE xxwc_collector_cr_analyst_xref
         SET credit_analyst_id = NULL
       WHERE credit_analyst_id IS NOT NULL;

      FOR crdt_analyst_rec IN crdt_analyst_cur
      LOOP
         BEGIN
            UPDATE xxwc_collector_cr_analyst_xref
               SET credit_analyst_id = crdt_analyst_rec.resource_id
             WHERE prism_code = crdt_analyst_rec.prism_code;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END LOOP;

      COMMIT;
   END populate_credit_analyst;

   /*************************************************************************
     Procedure:  update_prism_site_dff

     Purpose:    Procedure called by customer_interface to null out
         the site DFF when prism legacy_party_site_number is
                   re-used within PRISM. It will null the existing one
                   in EBS to allow the new record to use the
                   legacy_party_site_number
   ************************************************************************/
   PROCEDURE update_prism_site_dff (p_prism_site_number   IN     VARCHAR2
                                   ,p_return_status          OUT VARCHAR2)
   IS
      ostatus                VARCHAR2 (1);
      omsgcount              NUMBER;
      omsgdata               VARCHAR2 (2000);
      p_site_err             VARCHAR2 (4000);

      x_return_status        VARCHAR2 (1);
      x_msg_count            NUMBER;
      x_msg_data             VARCHAR2 (2000);
      l_return_status        VARCHAR2 (1);
      l_version_number       NUMBER;
      l_Cust_Acct_Site_Rec   hz_cust_account_site_v2pub.cust_acct_site_rec_type;

      -- Cursor to pull the original sites in Oracle where the prism legacy_party_site_number
      -- is already being used. The API Will null out the DFF
      CURSOR cur_duplicate_sites
      IS
         SELECT hcasa.cust_account_id
               ,hcasa.cust_acct_site_id
               ,hcasa.object_version_number
               ,hcasa.org_id
           FROM apps.hz_cust_acct_sites hcasa
          WHERE     hcasa.attribute17 =
                       SUBSTR (p_prism_site_number
                              ,1
                              ,DECODE (INSTR (p_prism_site_number
                                             ,'-'
                                             ,1
                                             ,1)
                                      ,0, LENGTH (p_prism_site_number)
                                      ,  INSTR (p_prism_site_number
                                               ,'-'
                                               ,1
                                               ,1)
                                       - 1))             --p_prism_site_number
                AND hcasa.attribute17 IS NOT NULL;
   BEGIN
      FOR c1 IN cur_duplicate_sites
      LOOP
         EXIT WHEN cur_duplicate_sites%NOTFOUND;

         l_Cust_Acct_Site_Rec := NULL;

         -- Populating record set with data needed to clear out the DFF
         l_Cust_Acct_Site_Rec.cust_account_id := c1.cust_account_id;
         l_Cust_Acct_Site_Rec.cust_acct_site_id := c1.cust_acct_site_id;
         l_Cust_Acct_Site_Rec.attribute17 := FND_API.G_MISS_CHAR;
         l_Cust_Acct_Site_Rec.org_id := c1.org_id;

         l_version_number := c1.object_version_number;

         hz_cust_account_site_v2pub.update_cust_acct_site (
            'T'
           ,l_Cust_Acct_Site_Rec
           ,l_version_number
           ,x_return_status
           ,x_msg_count
           ,x_msg_data);

         IF x_return_status <> 'S'
         THEN
            IF x_msg_count > 1
            THEN
               FOR i IN 1 .. x_msg_count
               LOOP
                  p_site_err :=
                        NVL (p_site_err, ' ')
                     || SUBSTR (
                           fnd_msg_pub.get (i, p_encoded => fnd_api.g_false)
                          ,1
                          ,255);
               END LOOP;
            ELSE
               p_site_err := x_msg_data;
            END IF;

            fnd_file.put_line (
               fnd_file.LOG
              ,' Could not reset to null DFF for ' || c1.cust_acct_site_id);
            l_return_status := 'E';
         ELSE
            l_return_status := 'S';
            fnd_file.put_line (
               fnd_file.LOG
              ,   ' Successfully nulled DFF17 in HCASA for '
               || c1.cust_acct_site_id
               || '-'
               || p_prism_site_number);
         END IF;
      END LOOP;

      -- Returning the API status to the main processor for validation
      p_return_status := l_return_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG
                           ,' Error updating cust acct site DFF');
         RAISE;
   END update_prism_site_dff;

   /*************************************************************************
     Procedure:  pop_err

     Purpose:    Procedure to capture Validation Errors and program errors
         for all three entities
   ************************************************************************/
   PROCEDURE pop_err (p_entity_name        VARCHAR2
                     ,p_entity_no          VARCHAR2
                     ,p_validation_rule    VARCHAR2
                     ,p_error_msg          VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO xxwc_customer_int_errors (request_id
                                           ,entity_name
                                           ,entity_no
                                           ,validation_rule
                                           ,error_msg)
           VALUES (fnd_global.conc_request_id
                  ,p_entity_name
                  ,p_entity_no
                  ,p_validation_rule
                  ,p_error_msg);

      COMMIT;
   END pop_err;

   /*************************************************************************
     Procedure :  validate_data

     Purpose:    Procedure called by the Process File procedure to validate
         data sent from prism
   ************************************************************************/
   PROCEDURE validate_data
   IS
      -- Satish U: 10/25/2011: Included NULL value for Process Status as Records are loaded with Null Value
      CURSOR get_cust
      IS
         SELECT stg.ROWID rid, stg.*
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG STG
          WHERE     orig_system_reference IS NOT NULL
                AND (process_status IS NULL OR process_status IN ('R', 'E'));


      -- Satish U: 10/25/2011: Included NULL value for Process Status as Records are loaded with Null Value
      CURSOR get_sites (
         p_customer VARCHAR2)
      IS
         SELECT stg.ROWID rid, stg.*
           FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG stg
          WHERE     orig_system_reference = p_customer
                AND (process_status IS NULL OR process_status IN ('R', 'E'));

      -- Cursor added to validate only job sites
      CURSOR get_job_sites
      IS
         SELECT x1.ROWID rid, x1.*
           FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG x1
          WHERE     (   x1.process_status IS NULL
                     OR x1.process_status IN ('R', 'E'))
                AND NOT EXISTS
                           (SELECT 'Does not have account'
                              FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG x2
                             WHERE x2.orig_system_reference =
                                      x1.orig_system_reference);

      -- Satish U: 10/25/2011: Included NULL value for PRocess Status as Records are loaded with Null Value
      CURSOR get_contacts (
         p_customer VARCHAR2)
      IS
         SELECT con.ROWID rid, con.*
           FROM XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG con
          WHERE     1 = 1
                AND con.legacy_party_site_number = p_customer
                AND orig_system_reference = legacy_party_site_number
                AND (   con.process_status IS NULL
                     OR process_status IN ('R', 'E'));

      -- Satish U: 10/25/2011: Included NULL value for PRocess Status as Records are loaded with Null Value
      CURSOR get_site_contacts (
         p_customer                  VARCHAR2
        ,p_legacy_customer_number    VARCHAR2)
      IS
         SELECT con.ROWID rid, con.*
           FROM XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG con
          WHERE     1 = 1
                AND con.orig_system_reference = p_customer
                AND con.legacy_party_site_number = p_legacy_customer_number
                AND (   con.process_status IS NULL
                     OR process_status IN ('R', 'E'));

      l_rec_found                   NUMBER;
      l_err                         VARCHAR2 (1);
      l_customer_exists_flag        VARCHAR2 (1);
      l_rec_count                   NUMBER;
      c_max_commit_count   CONSTANT NUMBER := 1000;
      l_has_suffix                  NUMBER;
      l_return_status               VARCHAR2 (3);
      l_site_suffix                 VARCHAR2 (5);
   BEGIN
      populate_collectors;
      populate_credit_analyst;
      populate_site_locations;
      update_customer_without_sites;
      update_contacts_without_sites;

      BEGIN
         UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
            SET account_name = organization_name
          WHERE account_name IS NULL AND process_status IS NULL;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- Trim Spaces on Customer records
      BEGIN
         UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
            SET ORIG_SYSTEM_REFERENCE = TRIM (ORIG_SYSTEM_REFERENCE)
               ,ORIG_SYSTEM = TRIM (ORIG_SYSTEM)
               ,STATUS = TRIM (STATUS)
               ,ATTRIBUTE_CATEGORY = TRIM (ATTRIBUTE_CATEGORY)
               ,ATTRIBUTE1 = TRIM (ATTRIBUTE1)
               ,ATTRIBUTE2 = TRIM (ATTRIBUTE2)
               ,ATTRIBUTE3 = TRIM (ATTRIBUTE3)
               ,ATTRIBUTE4 = TRIM (ATTRIBUTE4)
               ,ATTRIBUTE5 = TRIM (ATTRIBUTE5)
               ,ATTRIBUTE6 = TRIM (ATTRIBUTE6)
               ,ATTRIBUTE7 = TRIM (ATTRIBUTE7)
               ,ATTRIBUTE8 = TRIM (ATTRIBUTE8)
               ,ATTRIBUTE9 = TRIM (ATTRIBUTE9)
               ,ATTRIBUTE10 = TRIM (ATTRIBUTE10)
               ,ATTRIBUTE11 = TRIM (ATTRIBUTE11)
               ,ATTRIBUTE12 = TRIM (ATTRIBUTE12)
               ,ATTRIBUTE13 = TRIM (ATTRIBUTE13)
               ,ATTRIBUTE14 = TRIM (ATTRIBUTE14)
               ,ATTRIBUTE15 = TRIM (ATTRIBUTE15)
               ,ATTRIBUTE16 = TRIM (ATTRIBUTE16)
               ,ATTRIBUTE17 = TRIM (ATTRIBUTE17)
               ,ATTRIBUTE18 = TRIM (ATTRIBUTE18)
               ,ATTRIBUTE19 = TRIM (ATTRIBUTE19)
               ,ATTRIBUTE20 = TRIM (ATTRIBUTE20)
               ,ATTRIBUTE21 = TRIM (ATTRIBUTE21)
               ,ATTRIBUTE22 = TRIM (ATTRIBUTE22)
               ,ATTRIBUTE23 = TRIM (ATTRIBUTE23)
               ,ATTRIBUTE24 = TRIM (ATTRIBUTE24)
               ,ORGANIZATION_NAME = TRIM (ORGANIZATION_NAME)
               ,SIC_CODE_TYPE = TRIM (SIC_CODE_TYPE)
               ,SIC_CODE = TRIM (SIC_CODE)
               ,DUNS_NUMBER_C = TRIM (DUNS_NUMBER_C)
               ,GSA_INDICATOR_FLAG = TRIM (GSA_INDICATOR_FLAG)
               ,PARTY_TYPE = TRIM (PARTY_TYPE)
               ,DB_RATING = TRIM (DB_RATING)
               ,TAX_REFERENCE = TRIM (TAX_REFERENCE)
               ,DUNS_NUMBER = TRIM (DUNS_NUMBER)
               ,KNOWN_AS = TRIM (KNOWN_AS)
               ,EMPLOYEES_TOTAL = TRIM (EMPLOYEES_TOTAL)
               ,TOTAL_EMPLOYEES_IND = TRIM (TOTAL_EMPLOYEES_IND)
               ,TOTAL_EMP_MIN_IND = TRIM (TOTAL_EMP_MIN_IND)
               ,TOTAL_EMP_EST_IND = TRIM (TOTAL_EMP_EST_IND)
               ,CURR_FY_POTENTIAL_REVENUE = TRIM (CURR_FY_POTENTIAL_REVENUE)
               ,NEXT_FY_POTENTIAL_REVENUE = TRIM (NEXT_FY_POTENTIAL_REVENUE)
               ,YEAR_ESTABLISHED = TRIM (YEAR_ESTABLISHED)
               ,COMPETITOR_FLAG = TRIM (COMPETITOR_FLAG)
               ,CEO_NAME = TRIM (CEO_NAME)
               ,CEO_TITLE = TRIM (CEO_TITLE)
               ,PRINCIPAL_NAME = TRIM (PRINCIPAL_NAME)
               ,PRINCIPAL_TITLE = TRIM (PRINCIPAL_TITLE)
               ,MINORITY_OWNED_IND = TRIM (MINORITY_OWNED_IND)
               ,MINORITY_OWNED_TYPE = TRIM (MINORITY_OWNED_TYPE)
               ,WOMAN_OWNED_IND = TRIM (WOMAN_OWNED_IND)
               ,DISADV_8A_IND = TRIM (DISADV_8A_IND)
               ,SMALL_BUS_IND = TRIM (SMALL_BUS_IND)
               ,CONTROL_YR = TRIM (CONTROL_YR)
               ,RENT_OWN_IND = TRIM (RENT_OWN_IND)
               ,PARTY_NUMBER = TRIM (PARTY_NUMBER)
               ,PARTY_ID = TRIM (PARTY_ID)
               ,PROFILE_ID = TRIM (PROFILE_ID)
               ,HQ_BRANCH_IND = TRIM (HQ_BRANCH_IND)
               ,CUSTOMER_PROFILE_CLASS_NAME =
                   TRIM (CUSTOMER_PROFILE_CLASS_NAME)
               ,COLLECTOR_NAME = TRIM (COLLECTOR_NAME)
               ,CREIDT_CHECKING = TRIM (CREIDT_CHECKING)
               ,CREDIT_HOLD = TRIM (CREDIT_HOLD)
               ,CUSTOMER_TYPE = TRIM (CUSTOMER_TYPE)
               ,CUSTOMER_CLASS_CODE = TRIM (CUSTOMER_CLASS_CODE)
               ,SALES_CHANNEL_CODE = TRIM (SALES_CHANNEL_CODE)
               ,ACCOUNT_NAME = TRIM (ACCOUNT_NAME)
               ,DUNNING_LETTERS = TRIM (DUNNING_LETTERS)
               ,SEND_STATEMENTS = TRIM (SEND_STATEMENTS)
               ,CREDIT_BALANCE_STATEMENTS = TRIM (CREDIT_BALANCE_STATEMENTS)
               ,CREDIT_RATING = TRIM (CREDIT_RATING)
               ,INTEREST_PERIOD_DAYS = TRIM (INTEREST_PERIOD_DAYS)
               ,DISCOUNT_GRACE_DAYS = TRIM (DISCOUNT_GRACE_DAYS)
               ,PAYMENT_GRACE_DAYS = TRIM (PAYMENT_GRACE_DAYS)
               ,TOLERANCE = TRIM (TOLERANCE)
               ,STATEMENT_CYCLE_NAME = TRIM (STATEMENT_CYCLE_NAME)
               ,STANDARD_TERM_NAME = TRIM (STANDARD_TERM_NAME)
               ,OVERRIDE_TERMS = TRIM (OVERRIDE_TERMS)
               ,DUNNING_LETTER_SET_NAME = TRIM (DUNNING_LETTER_SET_NAME)
               ,INTEREST_CHARGES = TRIM (INTEREST_CHARGES)
               ,TAX_PRINTING_OPTIONS = TRIM (TAX_PRINTING_OPTIONS)
               ,RISK_CODE = TRIM (RISK_CODE)
               ,CURRENCY_CODE = TRIM (CURRENCY_CODE)
               ,PERCENT_COLLECTABLE = TRIM (PERCENT_COLLECTABLE)
               ,OVERALL_CREDIT_LIMIT = TRIM (OVERALL_CREDIT_LIMIT)
               ,TRX_CREDIT_LIMIT = TRIM (TRX_CREDIT_LIMIT)
               ,MIN_DUNNING_AMOUNT = TRIM (MIN_DUNNING_AMOUNT)
               ,MIN_DUNNING_INVOICE_AMPOUNT =
                   TRIM (MIN_DUNNING_INVOICE_AMPOUNT)
               ,MAX_INTEREST_CHARGE = TRIM (MAX_INTEREST_CHARGE)
               ,MIN_STATEMENT_AMOUNT = TRIM (MIN_STATEMENT_AMOUNT)
               ,CLEARING_DAYS = TRIM (CLEARING_DAYS)
               ,AUTO_REC_INCL_DISPUTED_FLAG =
                   TRIM (AUTO_REC_INCL_DISPUTED_FLAG)
               ,AUTO_REC_MIN_RECEIPT_AMOUNT =
                   TRIM (AUTO_REC_MIN_RECEIPT_AMOUNT)
               ,CHARGE_ON_FINANCE_CHARGE_FLAG =
                   TRIM (CHARGE_ON_FINANCE_CHARGE_FLAG)
               ,LOCK_BOX_MATCHING_OPTION = TRIM (LOCK_BOX_MATCHING_OPTION)
               ,AUTOCASH_HIERARCHY_NAME = TRIM (AUTOCASH_HIERARCHY_NAME)
               ,REVIEW_CYCLE = TRIM (REVIEW_CYCLE)
               ,CREDIT_CLASSIFICATION = TRIM (CREDIT_CLASSIFICATION)
               ,AUTOPAY_FLAG = TRIM (AUTOPAY_FLAG)
               ,CUST_ACCOUNT_ID = TRIM (CUST_ACCOUNT_ID)
               ,CUST_ACCOUNT_PROFILE_ID = TRIM (CUST_ACCOUNT_PROFILE_ID)
               ,CREDIT_ANALYST_NAME = TRIM (CREDIT_ANALYST_NAME)
               ,CUSTOMER_SOURCE = TRIM (CUSTOMER_SOURCE)
               ,COD_COMMENT = TRIM (COD_COMMENT)
               ,KEYWORD = TRIM (KEYWORD)
               ,PROCESS_STATUS = TRIM (PROCESS_STATUS)
               ,PROCESS_ERROR = NULL
          WHERE (process_status IS NULL OR process_status IN ('R', 'E'));
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      BEGIN
         UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
            SET ORIG_SYSTEM_REFERENCE = TRIM (ORIG_SYSTEM_REFERENCE)
               ,ORIG_SYSTEM = TRIM (ORIG_SYSTEM)
               ,ATTRIBUTE1 = TRIM (ATTRIBUTE1)
               ,ATTRIBUTE2 = TRIM (ATTRIBUTE2)
               ,ATTRIBUTE3 = TRIM (ATTRIBUTE3)
               ,ATTRIBUTE4 = TRIM (ATTRIBUTE4)
               ,ATTRIBUTE5 = TRIM (ATTRIBUTE5)
               ,ATTRIBUTE6 = TRIM (ATTRIBUTE6)
               ,ATTRIBUTE7 = TRIM (ATTRIBUTE7)
               ,ATTRIBUTE8 = TRIM (ATTRIBUTE8)
               ,ATTRIBUTE9 = TRIM (ATTRIBUTE9)
               ,ATTRIBUTE10 = TRIM (ATTRIBUTE10)
               ,ATTRIBUTE11 = TRIM (ATTRIBUTE11)
               ,ATTRIBUTE12 = TRIM (ATTRIBUTE12)
               ,ATTRIBUTE13 = TRIM (ATTRIBUTE13)
               ,ATTRIBUTE14 = TRIM (ATTRIBUTE14)
               ,ATTRIBUTE15 = TRIM (ATTRIBUTE15)
               ,ATTRIBUTE16 = TRIM (ATTRIBUTE16)
               ,ATTRIBUTE17 = TRIM (ATTRIBUTE17)
               ,ATTRIBUTE18 = TRIM (ATTRIBUTE18)
               ,ATTRIBUTE19 = TRIM (ATTRIBUTE19)
               ,ATTRIBUTE20 = TRIM (ATTRIBUTE20)
               ,COUNTRY = TRIM (COUNTRY)
               ,ADDRESS1 = TRIM (ADDRESS1)
               ,ADDRESS2 = TRIM (ADDRESS2)
               ,ADDRESS3 = TRIM (ADDRESS3)
               ,ADDRESS4 = TRIM (ADDRESS4)
               ,CITY = TRIM (CITY)
               ,POSTAL_CODE = TRIM (POSTAL_CODE)
               ,STATE = TRIM (STATE)
               ,COUNTY = TRIM (COUNTY)
               ,LEGACY_CUSTOMER_NUMBER = TRIM (LEGACY_CUSTOMER_NUMBER)
               ,LEGACY_PARTY_SITE_NUMBER = TRIM (LEGACY_PARTY_SITE_NUMBER)
               ,LEGACY_PARTY_SITE_NAME = TRIM (LEGACY_PARTY_SITE_NAME)
               ,PROVINCE = TRIM (PROVINCE)
               ,LOCATION_ID = TRIM (LOCATION_ID)
               ,TIMEZONE_ID = TRIM (TIMEZONE_ID)
               ,IDENTIFYING_ADDRESS_FLAG = TRIM (IDENTIFYING_ADDRESS_FLAG)
               ,PARTY_SITE_NAME = TRIM (PARTY_SITE_NAME)
               ,SITE_USE_TYPE = TRIM (SITE_USE_TYPE)
               ,PRIMARY_PER_TYPE = TRIM (PRIMARY_PER_TYPE)
               ,TERRITORY = TRIM (TERRITORY)
               ,BILL_TO_LOCATION = TRIM (BILL_TO_LOCATION)
               ,PRIMARY_BILL_TO_FLAG = TRIM (PRIMARY_BILL_TO_FLAG)
               ,GSA_INDICATOR = TRIM (GSA_INDICATOR)
               ,PRIMARY_SALESREP_NUMBER = TRIM (PRIMARY_SALESREP_NUMBER)
               ,PRIMARY_SHIP_TO_FLAG = TRIM (PRIMARY_SHIP_TO_FLAG)
               ,FOB_POINT = TRIM (FOB_POINT)
               ,FREIGHT_TERM = TRIM (FREIGHT_TERM)
               ,ORDER_TYPE = TRIM (ORDER_TYPE)
               ,PRICE_LIST_NAME = TRIM (PRICE_LIST_NAME)
               ,SHIP_VIA = TRIM (SHIP_VIA)
               ,SHIP_SETS_INCLUDE_LINES_FLAG =
                   TRIM (SHIP_SETS_INCLUDE_LINES_FLAG)
               ,ARRIVALSETS_INCLUDE_LINES_FLAG =
                   TRIM (ARRIVALSETS_INCLUDE_LINES_FLAG)
               ,INVOICE_QUANTITY_RULE = TRIM (INVOICE_QUANTITY_RULE)
               ,OVER_SHIPMENT_TOLERANCE = TRIM (OVER_SHIPMENT_TOLERANCE)
               ,UNDER_SHIPMENT_TOLERANCE = TRIM (UNDER_SHIPMENT_TOLERANCE)
               ,ITEM_CROSS_REF_PREF = TRIM (ITEM_CROSS_REF_PREF)
               ,DATE_TYPE_PREFERENCE = TRIM (DATE_TYPE_PREFERENCE)
               ,OVER_RETURN_TOLERANCE = TRIM (OVER_RETURN_TOLERANCE)
               ,UNDER_RETURN_TOLERANCE = TRIM (UNDER_RETURN_TOLERANCE)
               ,SCHED_DATE_PUSH_FLAG = TRIM (SCHED_DATE_PUSH_FLAG)
               ,WAREHOUSE_CODE = TRIM (WAREHOUSE_CODE)
               ,DATES_NEGATIVE_TOLERANCE = TRIM (DATES_NEGATIVE_TOLERANCE)
               ,DATES_POSITIVE_TOLERANCE = TRIM (DATES_POSITIVE_TOLERANCE)
               ,FINCHRG_RECEIVABLES_TRX_NAME =
                   TRIM (FINCHRG_RECEIVABLES_TRX_NAME)
               ,LEGACY_PARTY_NUMBER = TRIM (LEGACY_PARTY_NUMBER)
               ,PARTY_SITE_ID = TRIM (PARTY_SITE_ID)
               ,PARTY_SITE_NUMBER = TRIM (PARTY_SITE_NUMBER)
               ,PARTY_SITE_USE_ID = TRIM (PARTY_SITE_USE_ID)
               ,CUST_ACCT_SITE_ID = TRIM (CUST_ACCT_SITE_ID)
               ,CUST_SITE_USE_ID = TRIM (CUST_SITE_USE_ID)
               ,LOCATION = TRIM (LOCATION)
               ,OVERALL_CREDIT_LIMIT = TRIM (OVERALL_CREDIT_LIMIT)
               ,TRX_CREDIT_LIMIT = TRIM (TRX_CREDIT_LIMIT)
               ,MIN_DUNNING_AMOUNT = TRIM (MIN_DUNNING_AMOUNT)
               ,MIN_DUNNING_INVOICE_AMPOUNT =
                   TRIM (MIN_DUNNING_INVOICE_AMPOUNT)
               ,MAX_INTEREST_CHARGE = TRIM (MAX_INTEREST_CHARGE)
               ,MIN_STATEMENT_AMOUNT = TRIM (MIN_STATEMENT_AMOUNT)
               ,CLEARING_DAYS = TRIM (CLEARING_DAYS)
               ,AUTO_REC_INCL_DISPUTED_FLAG =
                   TRIM (AUTO_REC_INCL_DISPUTED_FLAG)
               ,AUTO_REC_MIN_RECEIPT_AMOUNT =
                   TRIM (AUTO_REC_MIN_RECEIPT_AMOUNT)
               ,CHARGE_ON_FINANCE_CHARGE_FLAG =
                   TRIM (CHARGE_ON_FINANCE_CHARGE_FLAG)
               ,LOCK_BOX_MATCHING_OPTION = TRIM (LOCK_BOX_MATCHING_OPTION)
               ,AUTOCASH_HIERARCHY_NAME = TRIM (AUTOCASH_HIERARCHY_NAME)
               ,REVIEW_CYCLE = TRIM (REVIEW_CYCLE)
               ,CREDIT_CLASSIFICATION = TRIM (CREDIT_CLASSIFICATION)
               ,AUTOPAY_FLAG = TRIM (AUTOPAY_FLAG)
               ,HDS_SITE_FLAG = TRIM (HDS_SITE_FLAG)
               ,JOB_NAME = TRIM (JOB_NAME)
               ,JOB_NUMBER = TRIM (JOB_NUMBER)
               ,CREDIT_HOLD = TRIM (CREDIT_HOLD)
               ,CREDIT_ANALYST_NAME = TRIM (CREDIT_ANALYST_NAME)
               ,LEGACY_SITE_TYPE = TRIM (LEGACY_SITE_TYPE)
               ,PROCESS_STATUS = TRIM (PROCESS_STATUS)
               ,VALIDATED_FLAG = TRIM (VALIDATED_FLAG)
               ,PROCESS_ERROR = NULL
               ,SALESREP_ID = TRIM (SALESREP_ID)
          WHERE (process_status IS NULL OR process_status IN ('R', 'E'));
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      BEGIN
         UPDATE XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
            SET ARC_JOB_NO = TRIM (ARC_JOB_NO)
               ,OWNER_TABLE_NAME = TRIM (OWNER_TABLE_NAME)
               ,OWNER_TABLE_ID = TRIM (OWNER_TABLE_ID)
               ,LEGACY_PARTY_NUMBER = TRIM (LEGACY_PARTY_NUMBER)
               ,LEGACY_PARTY_SITE_NUMBER = TRIM (LEGACY_PARTY_SITE_NUMBER)
               ,PRIMARY_FLAG = TRIM (PRIMARY_FLAG)
               ,ORIG_SYSTEM_REFERENCE = TRIM (ORIG_SYSTEM_REFERENCE)
               ,ORIG_SYSTEM = TRIM (ORIG_SYSTEM)
               ,EDI_TRANSACTION_HANDLING = TRIM (EDI_TRANSACTION_HANDLING)
               ,EDI_ID_NUMBER = TRIM (EDI_ID_NUMBER)
               ,EDI_PAYMENT_METHOD = TRIM (EDI_PAYMENT_METHOD)
               ,EDI_PAYMENT_FORMAT = TRIM (EDI_PAYMENT_FORMAT)
               ,EDI_REMITTANCE_METHOD = TRIM (EDI_REMITTANCE_METHOD)
               ,EDI_REMITTANCE_INSTRUCTION = TRIM (EDI_REMITTANCE_INSTRUCTION)
               ,EDI_TP_HEADER_ID = TRIM (EDI_TP_HEADER_ID)
               ,EDI_ECE_TPE_LOCATION_CODE = TRIM (EDI_ECE_TPE_LOCATION_CODE)
               ,EMAIL_FORMAT = TRIM (EMAIL_FORMAT)
               ,EMAIL_ADDRESS = TRIM (EMAIL_ADDRESS)
               ,FAX_AREA_CODE = TRIM (FAX_AREA_CODE)
               ,FAX_COUNTRY_CODE = TRIM (FAX_COUNTRY_CODE)
               ,FAX_NUMBER = TRIM (FAX_NUMBER)
               ,FAX_EXTENSION = TRIM (FAX_EXTENSION)
               ,PHONE_AREA_CODE = TRIM (PHONE_AREA_CODE)
               ,PHONE_COUNTRY_CODE = TRIM (PHONE_COUNTRY_CODE)
               ,PHONE_NUMBER = TRIM (PHONE_NUMBER)
               ,PHONE_EXTENSION = TRIM (PHONE_EXTENSION)
               ,TELEX_NUMBER = TRIM (TELEX_NUMBER)
               ,WEB_TYPE = TRIM (WEB_TYPE)
               ,URL = TRIM (URL)
               ,PROCESS_STATUS = TRIM (PROCESS_STATUS)
               ,PROCESS_ERROR = NULL
               ,HDS_SITE_FLAG = TRIM (HDS_SITE_FLAG)
          WHERE (process_status IS NULL OR process_status IN ('R', 'E'));
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      l_rec_count := 0;

      FOR rec_cust IN get_cust
      LOOP
         -- Initialize Variables before starting of the LOOP
         l_err := 'N';
         l_customer_exists_flag := 'N';

         --VR1 :Orig_System_Reference and Orig_System Should be Unique for each granular entity.
         --VR2 :Orig_System and Orig_System_Reference should both be Null or both should have value.
         IF    (    rec_cust.orig_system IS NULL
                AND rec_cust.orig_system_reference IS NOT NULL)
            OR (    rec_cust.orig_system IS NOT NULL
                AND rec_cust.orig_system_reference IS NULL)
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Customer '
               || g_parameter_prefix
               || rec_cust.orig_system_reference
               || 'VR2 Invalid ORIG SYSTEM,ORIG SYSTEM REFERENCE Combination');
            l_err := 'Y';

            BEGIN
               UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                  SET process_error =
                         SUBSTR (
                               process_error
                            || '.Invalid ORIG_SYSTEM/ORIG_SYSTEM_REFERENCE Comb'
                           ,1
                           ,4000)
                WHERE ROWID = rec_cust.rid;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || g_parameter_prefix
                     || rec_cust.orig_system_reference
                     || ' could not update stg');
            END;
         END IF;

         --VR3  Check if Customer already exists in the system
         BEGIN
            SELECT 'Y'
              INTO l_customer_exists_flag
              FROM hz_cust_accounts cus
             WHERE             -- 03/26/2012 CG: Updaed to vrify against DFF 6
                   -- cus.orig_system_reference = g_parameter_prefix || rec_cust.orig_system_reference;
                   NVL (cus.attribute6, 'UNKNOWN') =
                      rec_cust.orig_system_reference;

            --AND cus.org_id = g_org_id; -- 03/01/2012 CG Org Id is not being populated in the account record

            fnd_file.put_line (
               fnd_file.LOG
              ,   'Customer '
               || g_parameter_prefix
               || rec_cust.orig_system_reference
               || 'VR3 Customer Already Exists');
            l_err := 'Y';

            BEGIN
               UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                  SET process_error =
                         SUBSTR (process_error || '.Customer already exists'
                                ,1
                                ,4000)
                WHERE ROWID = rec_cust.rid;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || g_parameter_prefix
                     || rec_cust.orig_system_reference
                     || ' could not update stg');
            END;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_customer_exists_flag := 'N';
         END;

         --VR42 : Customer_Profile_Class_Name Validation: If value is passed then it is validated against HZ_CUST_PROFILE_CLASSES
         IF rec_cust.customer_profile_class_name IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM hz_cust_profile_classes cpc
                     ,xxwc_cust_profile_class_xref x
                WHERE     UPPER (cpc.name) = UPPER (x.profile_class)
                      AND x.prism_code =
                             TO_NUMBER (rec_cust.customer_profile_class_name)
                      AND cpc.status = 'A';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || rec_cust.orig_system_reference
                     || 'VR42 Invalid Customer Profile');
            -- l_err := 'Y';
            END;
         END IF;

         --VR5 SIC Code validation
         IF rec_cust.sic_code IS NOT NULL AND rec_cust.sic_code <> 0
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM ar_lookups b
                WHERE     b.lookup_code = rec_cust.sic_code
                      AND b.lookup_type = '1972 SIC';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || rec_cust.orig_system_reference
                     || 'VR5 Invalid SIC Code');
            END;
         END IF;

         --fnd_file.put_line (fnd_file.LOG, 'Before Account Status Validation');

         -- VR43 profile cust account status
         IF rec_cust.credit_analyst_name IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM xxwc_account_status_xref a, ar_lookups b
                WHERE     a.account_status = b.meaning
                      AND b.lookup_type = 'ACCOUNT_STATUS'
                      AND a.prism_credit_group =
                             TRIM (rec_cust.credit_analyst_name);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || rec_cust.orig_system_reference
                     || 'VR43 Invalid Profile Account Status');
            END;
         END IF;

         --fnd_file.put_line (fnd_file.LOG, 'Before Collector or Credit Analyst Validation');

         --VR44 : Collector Name: Should exist in AR_COLLECTORS
         IF rec_cust.collector_name IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM xxwc_collector_cr_analyst_xref x
                WHERE     1 = 1
                      AND UPPER (x.prism_code) =
                             TRIM (UPPER (rec_cust.collector_name));
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || rec_cust.orig_system_reference
                     || 'VR44 Invalid Collector');
            --l_err := 'Y';
            END;
         END IF;

         --VR45 : Credit Hold Validation : Should have values 'Y' or 'N'
         IF rec_cust.credit_hold NOT IN ('Y', 'N')
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Customer '
               || rec_cust.orig_system_reference
               || 'VR45 Invalid Credit Hold');
            l_err := 'Y';

            BEGIN
               UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                  SET process_error =
                         SUBSTR (process_error || '.Invalid Credit Hold val'
                                ,1
                                ,4000)
                WHERE ROWID = rec_cust.rid;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || g_parameter_prefix
                     || rec_cust.orig_system_reference
                     || ' could not update stg');
            END;
         END IF;

         --fnd_file.put_line (fnd_file.LOG, 'Before Customer TYpe Validation');

         --VR46: Customer_Type : Validated against AR Lookup Type CUSTOMER_TYPE
         IF rec_cust.customer_type IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM ar_lookups
                WHERE     meaning = rec_cust.customer_type
                      AND lookup_type = 'CUSTOMER_TYPE'
                      AND TRUNC (SYSDATE) BETWEEN start_date_active
                                              AND NVL (end_date_active
                                                      ,TRUNC (SYSDATE) + 1)
                      AND enabled_flag = 'Y';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || rec_cust.orig_system_reference
                     || 'VR46 Invalid CUSTOMER TYPE');
                  l_err := 'Y';

                  BEGIN
                     UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                        SET process_error =
                               SUBSTR (
                                  process_error || '.Invalid Customer Type'
                                 ,1
                                 ,4000)
                      WHERE ROWID = rec_cust.rid;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   'Customer '
                           || g_parameter_prefix
                           || rec_cust.orig_system_reference
                           || ' could not update stg');
                  END;
            END;
         END IF;

         --fnd_file.put_line (fnd_file.LOG, 'Before Customer Class Validation');

         --VR47 :Customer Class Code : Validated against AR Lookup Type CUSTOMER CLASS
         IF rec_cust.customer_class_code IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM fnd_lookup_values_vl fl
                     ,xxwc_cust_classification_xref x
                WHERE     fl.lookup_type = 'CUSTOMER CLASS'
                      AND UPPER (fl.lookup_code) =
                             UPPER (x.cust_classification)
                      AND UPPER (x.prism_code) =
                             UPPER (rec_cust.customer_class_code)
                      AND TRUNC (SYSDATE) BETWEEN fl.start_date_active
                                              AND NVL (fl.end_date_active
                                                      ,TRUNC (SYSDATE) + 1)
                      AND fl.enabled_flag = 'Y';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || rec_cust.orig_system_reference
                     || 'VR47 Invalid CUSTOMER CLASS');
            --l_err := 'Y';
            END;
         END IF;

         --VR51 : Send Statements Validation : Should have value ?Y? or ?N?
         IF rec_cust.send_statements NOT IN ('Y', 'N', 'C')
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Customer '
               || rec_cust.orig_system_reference
               || 'VR51 Invalid Send Statements Flag');
         --l_err := 'Y';
         END IF;

         --VR53 : Credit Balance Statements : Validation Can have value if Send_Statements is ?Y?. Should have values ?Y? or ?N?
         IF rec_cust.send_statements IN ('Y', 'C')
         THEN
            IF rec_cust.credit_balance_statements NOT IN ('Y', 'N', 'C')
            THEN
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'Customer '
                  || rec_cust.orig_system_reference
                  || 'VR53 Invalid Credit Balance Statement');
               l_err := 'Y';

               BEGIN
                  UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                     SET process_error =
                            SUBSTR (
                                  process_error
                               || '.Invalid Credit Bal Stmt Flag'
                              ,1
                              ,4000)
                   WHERE ROWID = rec_cust.rid;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Customer '
                        || g_parameter_prefix
                        || rec_cust.orig_system_reference
                        || ' could not update stg');
               END;
            END IF;
         END IF;

         -- fnd_file.put_line (fnd_file.LOG, 'Before AR Terms Validation');

         --VR62 : Standard_Term_Name : Must be valid term in AR_TERMS
         IF rec_cust.standard_term_name IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM xxwc_payment_terms_xref xpt, ra_terms rt
                WHERE     UPPER (rt.name) = UPPER (xpt.oracle_term)
                      AND xpt.prism_code =
                             TO_NUMBER (rec_cust.standard_term_name)
                      AND TRUNC (SYSDATE) BETWEEN start_date_active
                                              AND NVL (end_date_active
                                                      ,TRUNC (SYSDATE) + 1);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || rec_cust.payment_grace_days
                     || 'VR62 Invalid Payment Term');
                  l_err := 'Y';

                  BEGIN
                     UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                        SET process_error =
                               SUBSTR (
                                  process_error || '.Invalid payment term'
                                 ,1
                                 ,4000)
                      WHERE ROWID = rec_cust.rid;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   'Customer '
                           || g_parameter_prefix
                           || rec_cust.orig_system_reference
                           || ' could not update stg');
                  END;
            END;
         END IF;

         -- fnd_file.put_line (fnd_file.LOG, 'Before Currency Code Validation');

         --VR66: Currency_Code : Validated against Fnd_Currencies
         IF rec_cust.currency_code IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM fnd_currencies
                WHERE     currency_code = rec_cust.currency_code
                      AND TRUNC (SYSDATE) BETWEEN start_date_active
                                              AND NVL (end_date_active
                                                      ,TRUNC (SYSDATE) + 1);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || rec_cust.orig_system_reference
                     || 'VR66 Invalid Currency Code');
                  l_err := 'Y';

                  BEGIN
                     UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                        SET process_error =
                               SUBSTR (
                                  process_error || '.Invalid currency code'
                                 ,1
                                 ,4000)
                      WHERE ROWID = rec_cust.rid;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   'Customer '
                           || g_parameter_prefix
                           || rec_cust.orig_system_reference
                           || ' could not update stg');
                  END;
            END;
         END IF;

         -- fnd_file.put_line (fnd_file.LOG, 'Before AR CMGT Credit Classification Validation');

         IF rec_cust.credit_classification IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM ar_lookups al, xxwc_cr_classification_xref x
                WHERE     al.lookup_code = x.oracle_code
                      AND UPPER (x.prism_code) =
                             UPPER (rec_cust.credit_classification)
                      AND al.lookup_type = 'AR_CMGT_CREDIT_CLASSIFICATION'
                      AND al.enabled_flag = 'Y'
                      AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                                     al.start_date_active)
                                              AND TRUNC (
                                                     NVL (al.end_date_active
                                                         ,SYSDATE + 1));
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || rec_cust.orig_system_reference
                     || 'VR71 Invalid Credit Classification');
            --l_err := 'Y';
            END;
         END IF;

         --Open Sites
         FOR rec_sites IN get_sites (rec_cust.orig_system_reference)
         LOOP
            --Check address1
            IF rec_sites.address1 IS NULL AND rec_sites.address2 IS NULL
            THEN
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'Sites '
                  || rec_cust.orig_system_reference
                  || ' VV1 Address1/Address2 IS NULL');
            -- 03/26/2012 CG: Per call will not fail load if Add1 and 2 are blank
            /*l_err := 'Y';

            begin
                update XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                set  process_error = substr(process_error||'.Address Line 1/2 is NULL',1,4000)
                where rowid = rec_sites.rid;
            exception
            when others then
                fnd_file.put_line (fnd_file.LOG,'Customer '|| g_parameter_prefix || rec_cust.orig_system_reference||' could not update stg');
            end;*/

            END IF;

            --Check Country
            -- 03/14/2012 CG: Defaulting Country
            IF rec_sites.country IS NULL
            THEN
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'Sites '
                  || rec_cust.orig_system_reference
                  || ' VV2 Country Value Is NULL');
               l_err := 'Y';

               BEGIN
                  UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                     SET process_error =
                            SUBSTR (process_error || '.Country is NULL'
                                   ,1
                                   ,4000)
                   WHERE ROWID = rec_sites.rid;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Customer '
                        || g_parameter_prefix
                        || rec_cust.orig_system_reference
                        || ' could not update stg');
               END;
            END IF;

            --Check County
            -- Removing county check

            --Check State
            IF rec_sites.state IS NULL AND rec_sites.country = 'US'
            THEN
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'Sites '
                  || rec_cust.orig_system_reference
                  || ' VV3 Invalid State');
               l_err := 'Y';

               BEGIN
                  UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                     SET process_error =
                            SUBSTR (
                                  process_error
                               || '.State is NULL for country US'
                              ,1
                              ,4000)
                   WHERE ROWID = rec_sites.rid;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Customer '
                        || g_parameter_prefix
                        || rec_cust.orig_system_reference
                        || ' could not update stg');
               END;
            END IF;

            --Check postal code
            IF rec_sites.postal_code IS NULL AND rec_sites.country = 'US'
            THEN
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'Postal Code '
                  || rec_cust.orig_system_reference
                  || ' VV4 Invalid Postal Code');
               l_err := 'Y';

               BEGIN
                  UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                     SET process_error =
                            SUBSTR (
                                  process_error
                               || '.Postal Code is NULL for Country US'
                              ,1
                              ,4000)
                   WHERE ROWID = rec_sites.rid;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Customer '
                        || g_parameter_prefix
                        || rec_cust.orig_system_reference
                        || ' could not update stg');
               END;
            END IF;

            --Check City
            IF rec_sites.city IS NULL AND rec_sites.country = 'US'
            THEN
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'City '
                  || rec_cust.orig_system_reference
                  || ' VV5 Invalid City');
               l_err := 'Y';

               BEGIN
                  UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                     SET process_error =
                            SUBSTR (
                                  process_error
                               || '.City is NULL for Country US'
                              ,1
                              ,4000)
                   WHERE ROWID = rec_sites.rid;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Customer '
                        || g_parameter_prefix
                        || rec_cust.orig_system_reference
                        || ' could not update stg');
               END;
            END IF;

            --VR2 :Orig_System and Orig_System_Reference should both be Null or both should have value.
            IF    (    rec_sites.orig_system IS NULL
                   AND rec_sites.orig_system_reference IS NOT NULL)
               OR (    rec_sites.orig_system IS NOT NULL
                   AND rec_sites.orig_system_reference IS NULL)
            THEN
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'Sites '
                  || rec_cust.orig_system_reference
                  || ' VR2 Invalid ORIG SYSTEM,ORIG SYSTEM REFERENCE Combination');
               l_err := 'Y';

               BEGIN
                  UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                     SET process_error =
                            SUBSTR (
                                  process_error
                               || '.Invalid ORIG_SYSTEM,ORIG_SYSTEM_REFERENCE Comb'
                              ,1
                              ,4000)
                   WHERE ROWID = rec_sites.rid;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Customer '
                        || g_parameter_prefix
                        || rec_cust.orig_system_reference
                        || ' could not update stg');
               END;
            END IF;

            fnd_file.put_line (fnd_file.LOG, 'Before Orig_System Validation');

            --VR3 :Validate Orig_System
            IF rec_sites.orig_system IS NOT NULL
            THEN
               BEGIN
                  /*SELECT   1
                  INTO   l_rec_found
                  FROM   hz_cust_acct_sites_all hcasa,
                         -- 03/26/2012 CG: Changing to verify against DFF
                         hz_cust_accounts hca
                  WHERE   --hps.party_site_number = g_parameter_prefix || rec_sites.legacy_party_site_number
                    hcasa.attribute17 = g_parameter_prefix || rec_sites.legacy_party_site_number
                  AND  hcasa.cust_account_id = hca.cust_account_id
                  AND  nvl(hca.attribute6, 'UNKNOWN') = rec_sites.orig_system_reference
                  --AND  hp.orig_system_reference = g_parameter_prefix || rec_sites.orig_system_reference
                  AND  ROWNUM =1;*/

                  SELECT 1
                    INTO l_rec_found
                    FROM hz_cust_accounts hca, apps.hz_cust_acct_sites hcasa
                   --, hz_party_sites hps
                   WHERE /*nvl(hca.attribute6, 'UNKNOWN') = rec_sites.orig_system_reference
                  AND    */
                        hca  .cust_account_id = hcasa.cust_account_id
                         AND hcasa.attribute17 =
                                SUBSTR (
                                   rec_sites.legacy_party_site_number
                                  ,1
                                  ,DECODE (
                                      INSTR (
                                         rec_sites.legacy_party_site_number
                                        ,'-'
                                        ,1
                                        ,1)
                                     ,0, LENGTH (
                                            rec_sites.legacy_party_site_number)
                                     ,  INSTR (
                                           rec_sites.legacy_party_site_number
                                          ,'-'
                                          ,1
                                          ,1)
                                      - 1))
                         --AND  hcasa.party_site_id = hps.party_site_id
                         --AND  hps.party_site_number = g_parameter_prefix || rec_sites.legacy_party_site_number
                         AND ROWNUM = 1;

                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   ' Found duplicate for site '
                     || rec_sites.legacy_party_site_number);

                  -- Found duplicate so adding suffix
                  -- 04/30/2012 CGonzalez: Added portion to NULL out the existing customer site
                  l_has_suffix := NULL;

                  BEGIN
                     SELECT INSTR (rec_sites.legacy_party_site_number
                                  ,'-'
                                  ,1
                                  ,1)
                       INTO l_has_suffix
                       FROM DUAL;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_has_suffix := 0;
                  END;

                  IF l_has_suffix = 0
                  THEN
                     l_site_suffix := NULL;

                     BEGIN
                        SELECT '-' || TO_CHAR (SYSDATE, 'MMDD')
                          INTO l_site_suffix
                          FROM DUAL;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_site_suffix := NULL;
                     END;
                  END IF;

                  l_return_status := NULL;
                  update_prism_site_dff (
                     p_prism_site_number   => rec_sites.legacy_party_site_number
                    ,p_return_status       => l_return_status);

                  IF NVL (l_return_status, 'E') = 'E'
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Sites '
                        || rec_sites.orig_system_reference
                        || ' VR3 Duplicate ORIG SYSTEM REFERENCE');

                     l_err := 'Y';

                     BEGIN
                        UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                           SET legacy_party_site_number =
                                  legacy_party_site_number || l_site_suffix
                              ,process_error =
                                  SUBSTR (process_error || '.Duplicate Site'
                                         ,1
                                         ,4000)
                         WHERE ROWID = rec_sites.rid;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   'Customer '
                              || g_parameter_prefix
                              || rec_cust.orig_system_reference
                              || ' could not update stg');
                     END;
                  ELSE
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,'Successfully Updated Duplicate sute ');

                     BEGIN
                        UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                           SET legacy_party_site_number =
                                  legacy_party_site_number || l_site_suffix
                         WHERE ROWID = rec_sites.rid;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   'Customer '
                              || g_parameter_prefix
                              || rec_cust.orig_system_reference
                              || ' could not update site stg with suffix');
                     END;
                  END IF;
               -- 04/30/2012 CGonzalez

               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     NULL;
               END;

               fnd_file.put_line (fnd_file.LOG
                                 ,'After Orig_System Validation');
            END IF;

            --VR18 :Country : Validation TERRITORY_CODE column in the FND_TERRITORY table.
            IF rec_sites.country IS NOT NULL
            THEN
               BEGIN
                  SELECT 1
                    INTO l_rec_found
                    FROM fnd_territories_vl
                   WHERE territory_code = rec_sites.country;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Sites '
                        || rec_sites.orig_system_reference
                        || ' VR18 Invalid COUNTRY');
                     l_err := 'Y';

                     BEGIN
                        UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                           SET process_error =
                                  SUBSTR (
                                     process_error || '.Invalid country code'
                                    ,1
                                    ,4000)
                         WHERE ROWID = rec_sites.rid;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   'Customer '
                              || g_parameter_prefix
                              || rec_cust.orig_system_reference
                              || ' could not update stg');
                     END;
               END;
            END IF;

            -- fnd_file.put_line (fnd_file.LOG, 'Before Site Use Code Validation');

            --VR22 :Site_Use_Type Validation against AR Lookup Types
            IF rec_sites.site_use_type IS NOT NULL
            THEN
               BEGIN
                  SELECT 1
                    INTO l_rec_found
                    FROM ar_lookups
                   WHERE     meaning = rec_sites.site_use_type
                         AND lookup_type = 'PARTY_SITE_USE_CODE'
                         AND TRUNC (SYSDATE) BETWEEN start_date_active
                                                 AND NVL (
                                                        end_date_active
                                                       ,TRUNC (SYSDATE) + 1)
                         AND enabled_flag = 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Sites '
                        || rec_sites.orig_system_reference
                        || 'VR22 Invalid Site Use Type');
                     l_err := 'Y';


                     BEGIN
                        UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                           SET process_error =
                                  SUBSTR (
                                     process_error || '.Invalid Site Use'
                                    ,1
                                    ,4000)
                         WHERE ROWID = rec_sites.rid;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   'Customer '
                              || g_parameter_prefix
                              || rec_cust.orig_system_reference
                              || ' could not update stg');
                     END;
               END;
            END IF;

            --VR23 :Legal Collection on MSTR OR MISC
            IF     rec_sites.attribute6 IS NOT NULL
               AND rec_sites.hds_site_flag IN ('MISC', 'MSTR')
            THEN
               BEGIN
                  /**********************
                   Satish : 24-JAN-2012  :  Legal Collection Indicators are added to the XREF Table.
                  SELECT   1
                    INTO   l_rec_found
                    FROM   ar_lookups
                   WHERE   meaning = rec_sites.attribute6
                           AND lookup_type = 'XXWC_LEGAL_COLLECTION'
                           AND TRUNC (SYSDATE) BETWEEN start_date_active
                                                   AND  NVL (
                                                            end_date_active,
                                                            TRUNC(SYSDATE)
                                                            + 1)
                           AND enabled_flag = 'Y';
                 *********************/
                  SELECT 1
                    INTO l_Rec_Found
                    FROM XXWC_LEGAL_COLLECTION_IND_XREF
                   WHERE     Prism_Code = Rec_Sites.Attribute6
                         AND rec_sites.hds_site_flag IN ('MISC', 'MSTR')
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Sites '
                        || rec_sites.orig_system_reference
                        || 'VR23 Invalid attribute6 - legal collection');
               --l_err := 'Y';
               END;
            END IF;

            --VR24 :Legal Collection on MSTR OR MISC
            IF     rec_sites.attribute8 IS NOT NULL
               AND rec_sites.hds_site_flag IN ('MISC', 'MSTR')
            THEN
               BEGIN
                  SELECT 1
                    INTO l_rec_found
                    FROM ar_lookups
                   WHERE     meaning = rec_sites.attribute8
                         AND lookup_type = 'XXWC_LEGAL_AGENCY'
                         AND TRUNC (SYSDATE) BETWEEN start_date_active
                                                 AND NVL (
                                                        end_date_active
                                                       ,TRUNC (SYSDATE) + 1)
                         AND enabled_flag = 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Sites '
                        || rec_sites.orig_system_reference
                        || 'VR24 Invalid attribute8 - collection agency');
               --l_err := 'Y';
               END;
            END IF;

            --VR25 :Bill TO Site Use Code can not be null for Ship To Site Use.
            IF     UPPER (rec_sites.site_use_type) = 'SHIP TO'
               AND rec_sites.bill_to_location IS NULL
            THEN
               pop_err ('Sites'
                       ,rec_sites.orig_system_reference
                       ,'VR25'
                       ,'Invalid Bill To Location For Ship To Site');
            --l_err := 'Y';
            END IF;


            --VR27 :GSA_Indicator : Should have values ?Y? or ?N?
            IF rec_sites.gsa_indicator NOT IN ('Y', 'N')
            THEN
               pop_err ('Sites'
                       ,rec_sites.orig_system_reference
                       ,'VR27'
                       ,'Invalid GAS Indicator');
               l_err := 'Y';

               BEGIN
                  UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                     SET process_error =
                            SUBSTR (process_error || '.Invaldi GSA Indicator'
                                   ,1
                                   ,4000)
                   WHERE ROWID = rec_sites.rid;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Customer '
                        || g_parameter_prefix
                        || rec_cust.orig_system_reference
                        || ' could not update stg');
               END;
            END IF;

            --VR28 :Salesrep Number Validation
            IF rec_sites.primary_salesrep_number IS NOT NULL
            THEN
               BEGIN
                  SELECT 1
                    INTO l_rec_found
                    FROM ra_salesreps
                   WHERE salesrep_number = rec_sites.primary_salesrep_number;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     pop_err ('Sites'
                             ,rec_sites.orig_system_reference
                             ,'VR28'
                             ,'Invalid Sales Rep');
               --Satish U: Currently Salesrep is Defaulted
               --l_err := 'Y';
               END;
            END IF;

            -- VR29 Site profile cust account status
            IF rec_sites.credit_analyst_name IS NOT NULL
            THEN
               BEGIN
                  SELECT 1
                    INTO l_rec_found
                    FROM xxwc_account_status_xref a, ar_lookups b
                   WHERE     a.account_status = b.meaning
                         AND b.lookup_type = 'ACCOUNT_STATUS'
                         AND a.prism_credit_group =
                                rec_sites.credit_analyst_name;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     pop_err ('Sites'
                             ,rec_sites.orig_system_reference
                             ,'VR29'
                             ,'Invalid Site Profile Account Status');
               END;
            END IF;

            --VR30 :Validate Freight Terms
            IF rec_sites.fob_point IS NOT NULL
            THEN
               BEGIN
                  SELECT 1
                    INTO l_rec_found
                    FROM fnd_lookup_values_vl
                   WHERE     meaning = rec_sites.fob_point
                         AND lookup_type = 'FREIGHT_TERMS'
                         AND TRUNC (SYSDATE) BETWEEN start_date_active
                                                 AND NVL (
                                                        end_date_active
                                                       ,TRUNC (SYSDATE) + 1)
                         AND enabled_flag = 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     pop_err ('Sites'
                             ,rec_sites.orig_system_reference
                             ,'VR30'
                             ,'Invalid Freight Terms');
               --l_err := 'Y';
               END;
            END IF;

            --Party Contacts Loop
            FOR rec_contacts
               IN get_contacts (rec_sites.orig_system_reference)
            LOOP
               --VR40 : Primary_Flag : Validation : Can have values ?Y? /?N?.
               IF rec_contacts.primary_flag NOT IN ('Y', 'N')
               THEN
                  pop_err ('Contacts'
                          ,rec_contacts.orig_system_reference
                          ,'VR40'
                          ,'Invalid Primary Flag');
                  l_err := 'Y';
               END IF;
            --VR41 : Contact point Purpose Validation: Validated against
            --AR Lookup Type CONTACT_POINT_PURPOSE when contact_type_purpose is not WEB.
            --Validated against AR Lookup Type CONTACT_POINT_PURPOSE_WEB when contact_type_purpose is WEB.
            END LOOP;                                           --get_contacts

            --Site Contacts Loop
            FOR rec_site_contacts
               IN get_site_contacts (rec_sites.orig_system_reference
                                    ,rec_sites.legacy_customer_number)
            LOOP
               --VR40 : Primary_Flag : Validation : Can have values ?Y? /?N?.
               IF rec_site_contacts.primary_flag NOT IN ('Y', 'N')
               THEN
                  pop_err ('Site Contacts'
                          ,rec_site_contacts.orig_system_reference
                          ,'VR41'
                          ,'Invalid Primary Flag');
                  l_err := 'Y';
               END IF;
            --VR41 : Contact point Purpose Validation: Validated against
            --AR Lookup Type CONTACT_POINT_PURPOSE when contact_type_purpose is not WEB.
            --Validated against AR Lookup Type CONTACT_POINT_PURPOSE_WEB when contact_type_purpose is WEB.
            END LOOP;                                      --get_site_contacts
         END LOOP;                                                 --get_sites

         --Update the staging tables
         IF l_err = 'Y'
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,'Validation Failed : ' || rec_cust.orig_system_reference);

            UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
               SET process_status = 'E'
                  ,request_id = g_request_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by = fnd_global.user_id
             WHERE orig_system_reference = rec_cust.orig_system_reference;

            UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
               SET process_status = 'E'
                  ,request_id = g_request_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by = fnd_global.user_id
             WHERE orig_system_reference = rec_cust.orig_system_reference;

            UPDATE XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
               SET process_status = 'E'
                  ,request_id = g_request_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by = fnd_global.user_id
             WHERE orig_system_reference = rec_cust.orig_system_reference;
         ELSE
            fnd_file.put_line (
               fnd_file.LOG
              ,'Validation Successful : ' || rec_cust.orig_system_reference);

            UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
               SET process_status = 'V'
                  ,request_id = g_request_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by = fnd_global.user_id
             --,process_error = NULL
             WHERE orig_system_reference = rec_cust.orig_system_reference;

            UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
               SET process_status = 'V'
                  ,request_id = g_request_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by = fnd_global.user_id
             --,process_error = NULL
             WHERE orig_system_reference = rec_cust.orig_system_reference;

            UPDATE XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
               SET process_status = 'V'
                  ,request_id = g_request_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by = fnd_global.user_id
             --,process_error = NULL
             WHERE orig_system_reference = rec_cust.orig_system_reference;
         END IF;

         l_rec_count := l_rec_count + 1;

         IF l_rec_count >= c_max_commit_count
         THEN
            COMMIT;
            l_rec_count := 0;
         END IF;
      END LOOP;

      COMMIT;                                                       --get_cust

      -- site validation only
      fnd_file.put_line (fnd_file.LOG, 'Validating Sites for Jobs');

      FOR rec_sites IN get_job_sites
      LOOP
         l_err := 'N';

         -- CHeck if customer exists
         BEGIN
            SELECT 'Y'
              INTO l_customer_exists_flag
              FROM hz_cust_accounts cus
             WHERE                 -- 03/26/2012 CG: Changed to reference DFF6
                   -- cus.orig_system_reference = g_parameter_prefix || rec_sites.orig_system_reference; -- added by v.sankar
                   NVL (cus.attribute6, 'UNKNOWN') =
                      rec_sites.orig_system_reference;
         --AND cus.org_id = g_org_id;

         --l_err := 'N';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_customer_exists_flag := 'N';
               l_err := 'Y';
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'Customer '
                  || g_parameter_prefix
                  || rec_sites.orig_system_reference
                  || 'VR3 Not found');

               BEGIN
                  UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                     SET process_error =
                            SUBSTR (process_error || '.Customer Not Found'
                                   ,1
                                   ,4000)
                   WHERE ROWID = rec_sites.rid;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   'Customer '
                        || g_parameter_prefix
                        || rec_sites.orig_system_reference
                        || ' could not update stg');
               END;
         END;

         --Check address1
         IF rec_sites.address1 IS NULL AND rec_sites.address2 IS NULL
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Sites '
               || rec_sites.orig_system_reference
               || ' VV1 Address1/2 IS NULL');
         -- 03/26/2012 CG: Per ccall will not fail load if Add1 and 2 are blank
         /*l_err := 'Y';

         begin
              update XXWC.XXWC_AR_CUST_SITE_IFACE_STG
              set  process_error = substr(process_error||'.Address Line 1/2 is NULL',1,4000)
              where rowid = rec_sites.rid;
         exception
         when others then
             fnd_file.put_line (fnd_file.LOG,'Customer '|| g_parameter_prefix || rec_sites.orig_system_reference||' could not update stg');
         end;*/
         END IF;

         --Check Country
         IF rec_sites.country IS NULL
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Sites '
               || rec_sites.orig_system_reference
               || ' VV2 Country Value Is NULL');
            l_err := 'Y';

            BEGIN
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_error =
                         SUBSTR (process_error || '.Country is NULL'
                                ,1
                                ,4000)
                WHERE ROWID = rec_sites.rid;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || g_parameter_prefix
                     || rec_sites.orig_system_reference
                     || ' could not update stg');
            END;
         END IF;

         --Check County

         --Check State
         IF rec_sites.state IS NULL AND rec_sites.country = 'US'
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Sites '
               || rec_sites.orig_system_reference
               || ' VV3 Invalid State');

            l_err := 'Y';

            BEGIN
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_error =
                         SUBSTR (
                            process_error || '.State is NULL for country US'
                           ,1
                           ,4000)
                WHERE ROWID = rec_sites.rid;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || g_parameter_prefix
                     || rec_sites.orig_system_reference
                     || ' could not update stg');
            END;
         END IF;

         --Check postal code
         IF rec_sites.postal_code IS NULL AND rec_sites.country = 'US'
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Postal Code '
               || rec_sites.orig_system_reference
               || ' VV4 Invalid Postal Code');

            l_err := 'Y';

            BEGIN
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_error =
                         SUBSTR (
                               process_error
                            || '.Postal Code is NULL for Country US'
                           ,1
                           ,4000)
                WHERE ROWID = rec_sites.rid;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || g_parameter_prefix
                     || rec_sites.orig_system_reference
                     || ' could not update stg');
            END;
         END IF;

         --Check City
         IF rec_sites.city IS NULL AND rec_sites.country = 'US'
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'City '
               || rec_sites.orig_system_reference
               || ' VV5 Invalid City');
            l_err := 'Y';

            BEGIN
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_error =
                         SUBSTR (
                            process_error || '.City is NULL for Country US'
                           ,1
                           ,4000)
                WHERE ROWID = rec_sites.rid;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || g_parameter_prefix
                     || rec_sites.orig_system_reference
                     || ' could not update stg');
            END;
         END IF;

         --VR2 :Orig_System and Orig_System_Reference should both be Null or both should have value.
         IF    (    rec_sites.orig_system IS NULL
                AND rec_sites.orig_system_reference IS NOT NULL)
            OR (    rec_sites.orig_system IS NOT NULL
                AND rec_sites.orig_system_reference IS NULL)
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Sites '
               || rec_sites.orig_system_reference
               || 'VR2 Invalid ORIG SYSTEM,ORIG SYSTEM REFERENCE Combination');
            l_err := 'Y';

            BEGIN
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_error =
                         SUBSTR (
                               process_error
                            || '.Invalid ORIG_SYSTEM,ORIG_SYSTEM_REFERENCE Comb'
                           ,1
                           ,4000)
                WHERE ROWID = rec_sites.rid;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || g_parameter_prefix
                     || rec_sites.orig_system_reference
                     || ' could not update stg');
            END;
         END IF;

         --fnd_file.put_line (fnd_file.LOG, 'Before Orig_System Validation');

         --VR3 :Validate Orig_System
         IF rec_sites.orig_system IS NOT NULL
         THEN
            BEGIN
               -- Satish U: Changing to Hz_Party_Sites
               /*SELECT   1
               INTO   l_rec_found
               FROM   hz_cust_acct_sites_all hcasa,
                      -- 03/26/2012 CG: Changing to verify against DFF
                      hz_cust_accounts hca
               WHERE   --hps.party_site_number = g_parameter_prefix || rec_sites.legacy_party_site_number
                       hcasa.attribute17 = g_parameter_prefix || rec_sites.legacy_party_site_number
               AND  hcasa.cust_account_id = hca.cust_account_id
               AND  nvl(hca.attribute6, 'UNKNOWN') = rec_sites.orig_system_reference
               --AND  hp.orig_system_reference = g_parameter_prefix || rec_sites.orig_system_reference
               AND  ROWNUM =1;*/

               /*SELECT   1
               INTO   l_rec_found
               FROM   hz_cust_accounts hca
                      , hz_cust_acct_sites_all hcasa
                      , hz_party_sites hps
               WHERE  nvl(hca.attribute6, 'UNKNOWN') = rec_sites.orig_system_reference
               AND  hca.cust_account_id = hcasa.cust_account_id
               AND  hcasa.party_site_id = hps.party_site_id
               AND  hps.party_site_number = g_parameter_prefix || rec_sites.legacy_party_site_number
               AND  ROWNUM =1;

               fnd_file.put_line (
               fnd_file.LOG,'Sites '||
                        rec_sites.orig_system_reference||
                        'VR3 Duplicate ORIG SYSTEM REFERENCE for site');
               l_err := 'Y';

               begin
                   update XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                   set  process_error = substr(process_error||'.Duplicate Site',1,4000)
                   where rowid = rec_sites.rid;
               exception
               when others then
                   fnd_file.put_line (fnd_file.LOG,'Customer '|| g_parameter_prefix ||rec_sites.orig_system_reference||' could not update stg');
               end;*/

               SELECT 1
                 INTO l_rec_found
                 FROM hz_cust_accounts hca, apps.hz_cust_acct_sites hcasa
                --, hz_party_sites hps
                WHERE /*nvl(hca.attribute6, 'UNKNOWN') = rec_sites.orig_system_reference
               AND    */
                     hca  .cust_account_id = hcasa.cust_account_id
                      AND hcasa.attribute17 =
                             SUBSTR (
                                rec_sites.legacy_party_site_number
                               ,1
                               ,DECODE (
                                   INSTR (rec_sites.legacy_party_site_number
                                         ,'-'
                                         ,1
                                         ,1)
                                  ,0, LENGTH (
                                         rec_sites.legacy_party_site_number)
                                  ,  INSTR (
                                        rec_sites.legacy_party_site_number
                                       ,'-'
                                       ,1
                                       ,1)
                                   - 1))
                      --AND  hcasa.party_site_id = hps.party_site_id
                      --AND  hps.party_site_number = g_parameter_prefix || rec_sites.legacy_party_site_number
                      AND ROWNUM = 1;

               fnd_file.put_line (
                  fnd_file.LOG
                 ,   ' Found duplicate for site '
                  || rec_sites.legacy_party_site_number);


               -- 04/30/2012 CGonzalez: Added portion to NULL out the existing customer site

               l_has_suffix := NULL;

               BEGIN
                  SELECT INSTR (rec_sites.legacy_party_site_number
                               ,'-'
                               ,1
                               ,1)
                    INTO l_has_suffix
                    FROM DUAL;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_has_suffix := 0;
               END;

               IF l_has_suffix = 0
               THEN
                  l_site_suffix := NULL;

                  BEGIN
                     SELECT '-' || TO_CHAR (SYSDATE, 'MMDD')
                       INTO l_site_suffix
                       FROM DUAL;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_site_suffix := NULL;
                  END;
               END IF;

               l_return_status := NULL;
               update_prism_site_dff (
                  p_prism_site_number   => rec_sites.legacy_party_site_number
                 ,p_return_status       => l_return_status);

               IF NVL (l_return_status, 'E') = 'E'
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Sites '
                     || rec_sites.orig_system_reference
                     || ' VR3 Duplicate ORIG SYSTEM REFERENCE');

                  l_err := 'Y';

                  BEGIN
                     UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                        SET process_error =
                               SUBSTR (process_error || '.Duplicate Site'
                                      ,1
                                      ,4000)
                      WHERE ROWID = rec_sites.rid;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   'Customer '
                           || g_parameter_prefix
                           || rec_sites.orig_system_reference
                           || ' could not update stg');
                  END;
               ELSE
                  fnd_file.put_line (fnd_file.LOG
                                    ,' Updated duplicate site ');

                  BEGIN
                     UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                        SET legacy_party_site_number =
                               legacy_party_site_number || l_site_suffix
                      WHERE ROWID = rec_sites.rid;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   'Customer '
                           || g_parameter_prefix
                           || rec_sites.orig_system_reference
                           || ' could not update site stg with suffix');
                  END;
               END IF;
            -- 04/30/2012 CGonzalez

            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  NULL;
            END;
         --fnd_file.put_line (fnd_file.LOG, 'After Orig_System Validation for job sites');
         END IF;

         --VR18 :Country : Validation TERRITORY_CODE column in the FND_TERRITORY table.
         --ra_territories_kfv a
         IF rec_sites.country IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM fnd_territories_vl
                WHERE territory_code = rec_sites.country;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Sites '
                     || rec_sites.orig_system_reference
                     || 'VR18 Invalid COUNTRY');
                  l_err := 'Y';

                  BEGIN
                     UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                        SET process_error =
                               SUBSTR (
                                  process_error || '.Invalid country code'
                                 ,1
                                 ,4000)
                      WHERE ROWID = rec_sites.rid;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   'Customer '
                           || g_parameter_prefix
                           || rec_sites.orig_system_reference
                           || ' could not update stg');
                  END;
            END;
         END IF;

         --fnd_file.put_line (fnd_file.LOG, 'Before Site Use Code Validation');

         --VR22 :Site_Use_Type Validation against AR Lookup Types
         IF rec_sites.site_use_type IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM ar_lookups
                WHERE     meaning = rec_sites.site_use_type
                      AND lookup_type = 'PARTY_SITE_USE_CODE'
                      AND TRUNC (SYSDATE) BETWEEN start_date_active
                                              AND NVL (end_date_active
                                                      ,TRUNC (SYSDATE) + 1)
                      AND enabled_flag = 'Y';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Sites '
                     || rec_sites.orig_system_reference
                     || 'VR22 Invalid Site Use Type');
                  l_err := 'Y';

                  BEGIN
                     UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                        SET process_error =
                               SUBSTR (process_error || '.Invalid Site Use'
                                      ,1
                                      ,4000)
                      WHERE ROWID = rec_sites.rid;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   'Customer '
                           || g_parameter_prefix
                           || rec_sites.orig_system_reference
                           || ' could not update stg');
                  END;
            END;
         END IF;

         --VR23 :Legal Collection on MSTR OR MISC
         IF     rec_sites.attribute6 IS NOT NULL
            AND rec_sites.hds_site_flag IN ('MISC', 'MSTR')
         THEN
            BEGIN
               /**********************
                Satish : 24-JAN-2012  :  Legal Collection Indicators are added to the XREF Table.
               SELECT   1
                 INTO   l_rec_found
                 FROM   ar_lookups
                WHERE   meaning = rec_sites.attribute6
                        AND lookup_type = 'XXWC_LEGAL_COLLECTION'
                        AND TRUNC (SYSDATE) BETWEEN start_date_active
                                                AND  NVL (
                                                         end_date_active,
                                                         TRUNC(SYSDATE)
                                                         + 1)
                        AND enabled_flag = 'Y';
              *********************/
               SELECT 1
                 INTO l_Rec_Found
                 FROM XXWC_LEGAL_COLLECTION_IND_XREF
                WHERE     Prism_Code = Rec_Sites.Attribute6
                      AND rec_sites.hds_site_flag IN ('MISC', 'MSTR')
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Sites '
                     || rec_sites.orig_system_reference
                     || 'VR23 Invalid attribute6 - legal collection');
            --l_err := 'Y';
            END;
         END IF;

         --VR24 :Legal Collection on MSTR OR MISC
         IF     rec_sites.attribute8 IS NOT NULL
            AND rec_sites.hds_site_flag IN ('MISC', 'MSTR')
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM ar_lookups
                WHERE     meaning = rec_sites.attribute8
                      AND lookup_type = 'XXWC_LEGAL_AGENCY'
                      AND TRUNC (SYSDATE) BETWEEN start_date_active
                                              AND NVL (end_date_active
                                                      ,TRUNC (SYSDATE) + 1)
                      AND enabled_flag = 'Y';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Sites '
                     || rec_sites.orig_system_reference
                     || 'VR24 Invalid attribute8 - collection agency');
            --l_err := 'Y';
            END;
         END IF;

         --VR25 :Bill TO Site Use Code can not be null for ?Ship To? Site Use.
         -- Satish U: Revisit this Validation
         IF     UPPER (rec_sites.site_use_type) = 'SHIP TO'
            AND rec_sites.bill_to_location IS NULL
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Sites '
               || rec_sites.orig_system_reference
               || 'VR25 Invalid Bill To Location For Ship To Site');
         --l_err := 'Y';
         END IF;

         --VR27 :GSA_Indicator : Should have values ?Y? or ?N?
         IF rec_sites.gsa_indicator NOT IN ('Y', 'N')
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Sites '
               || rec_sites.orig_system_reference
               || 'VR27 Invalid GAS Indicator');
            l_err := 'Y';

            BEGIN
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_error =
                         SUBSTR (process_error || '.Invaldi GSA Indicator'
                                ,1
                                ,4000)
                WHERE ROWID = rec_sites.rid;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Customer '
                     || g_parameter_prefix
                     || rec_sites.orig_system_reference
                     || ' could not update stg');
            END;
         END IF;

         --VR28 :Salesrep Number Validation
         IF rec_sites.primary_salesrep_number IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM ra_salesreps
                WHERE salesrep_number = rec_sites.primary_salesrep_number;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Sites '
                     || rec_sites.orig_system_reference
                     || 'VR28 Invalid Sales Rep');
            --Satish U: Currently Salesrep is Defaulted
            --l_err := 'Y';
            END;
         END IF;

         -- VR29 Site profile cust account status
         IF rec_sites.credit_analyst_name IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM xxwc_account_status_xref a, ar_lookups b
                WHERE     a.account_status = b.meaning
                      AND b.lookup_type = 'ACCOUNT_STATUS'
                      AND a.prism_credit_group =
                             rec_sites.credit_analyst_name;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Sites '
                     || rec_sites.orig_system_reference
                     || 'VR29 Invalid Site Profile Account Status');
            END;
         END IF;

         --VR30 :Validate Freight Terms
         IF rec_sites.fob_point IS NOT NULL
         THEN
            BEGIN
               SELECT 1
                 INTO l_rec_found
                 FROM fnd_lookup_values_vl
                WHERE     meaning = rec_sites.fob_point
                      AND lookup_type = 'FREIGHT_TERMS'
                      AND TRUNC (SYSDATE) BETWEEN start_date_active
                                              AND NVL (end_date_active
                                                      ,TRUNC (SYSDATE) + 1)
                      AND enabled_flag = 'Y';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Sites '
                     || rec_sites.orig_system_reference
                     || 'VR30 Invalid Freight Terms');
            --l_err := 'Y';
            END;
         END IF;

         --Party Contacts Loop
         /*FOR rec_contacts
         IN get_contacts (rec_sites.orig_system_reference)
         LOOP
             --VR40 : Primary_Flag : Validation : Can have values ?Y? /?N?.
             IF rec_contacts.primary_flag NOT IN ('Y', 'N')
             THEN
                 fnd_file.put_line (
                 fnd_file.LOG,'Contacts '||
                          rec_contacts.orig_system_reference||
                          'VR40 Invalid Primary Flag');
                 l_err := 'Y';
             END IF;
         --VR41 : Contact point Purpose Validation: Validated against
         --AR Lookup Type CONTACT_POINT_PURPOSE when contact_type_purpose is not WEB.
         --Validated against AR Lookup Type CONTACT_POINT_PURPOSE_WEB when contact_type_purpose is WEB.
         END LOOP;  */
         --get_contacts

         --Site Contacts Loop
         FOR rec_site_contacts
            IN get_site_contacts (rec_sites.orig_system_reference
                                 ,rec_sites.legacy_customer_number)
         LOOP
            --VR40 : Primary_Flag : Validation : Can have values ?Y? /?N?.
            IF rec_site_contacts.primary_flag NOT IN ('Y', 'N')
            THEN
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'Site Contacts '
                  || rec_site_contacts.orig_system_reference
                  || 'VR41 Invalid Primary Flag');
               l_err := 'Y';
            END IF;
         --VR41 : Contact point Purpose Validation: Validated against
         --AR Lookup Type CONTACT_POINT_PURPOSE when contact_type_purpose is not WEB.
         --Validated against AR Lookup Type CONTACT_POINT_PURPOSE_WEB when contact_type_purpose is WEB.
         END LOOP;                                         --get_site_contacts


         --Update the staging tables
         IF l_err = 'Y'
         THEN
            fnd_file.put_line (
               fnd_file.LOG
              ,   'Validation Failed : '
               || rec_sites.orig_system_reference
               || '/'
               || rec_sites.legacy_party_site_number);

            UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
               SET process_status = 'E'
                  ,request_id = g_request_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by = fnd_global.user_id
             WHERE ROWID = rec_sites.rid;

            /*orig_system_reference = rec_sites.orig_system_reference
              AND   legacy_party_site_number = rec_sites.legacy_party_site_number;*/

            UPDATE XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
               SET process_status = 'E'
                  ,request_id = g_request_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by = fnd_global.user_id
             WHERE     orig_system_reference =
                          rec_sites.orig_system_reference
                   AND legacy_party_site_number =
                          rec_sites.legacy_party_site_number;
         ELSE
            fnd_file.put_line (
               fnd_file.LOG
              ,'Validation Successful : ' || rec_sites.orig_system_reference);



            UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
               SET process_status = 'V'
                  ,request_id = g_request_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by = fnd_global.user_id
                  ,process_error = NULL
             WHERE ROWID = rec_sites.rid;

            /*orig_system_reference = rec_sites.orig_system_reference
              AND   legacy_party_site_number = rec_sites.legacy_party_site_number;*/

            UPDATE XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
               SET process_status = 'V'
                  ,request_id = g_request_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by = fnd_global.user_id
                  ,process_error = NULL
             WHERE     orig_system_reference =
                          rec_sites.orig_system_reference
                   AND legacy_party_site_number =
                          rec_sites.legacy_party_site_number;
         END IF;

         l_rec_count := l_rec_count + 1;

         IF l_rec_count >= c_max_commit_count
         THEN
            COMMIT;
            l_rec_count := 0;
         END IF;
      END LOOP;

      COMMIT;
   END validate_data;

   /*************************************************************************
     Procedure :  pop_contact_info

     Purpose:    Procedure used by Customer_Interface to create contact
         information when applicable.
                   Not being used, inherited from conversion
   ************************************************************************/
   PROCEDURE pop_contact_info (p_site_use_type       IN     VARCHAR2
                              ,p_party_id            IN     NUMBER
                              ,p_cust_acct_id        IN     NUMBER
                              ,p_cust_acct_site_id   IN     NUMBER
                              ,p_rowid               IN     ROWID
                              ,p_out                    OUT VARCHAR2)
   IS
      -- party person recoprd variables
      ppersonrec               hz_party_v2pub.person_rec_type;
      oppartyid                hz_parties.party_id%TYPE;
      oppartynumber            hz_parties.party_number%TYPE;
      opprofileid              NUMBER;
      --party site contact
      porgcontactrec           hz_party_contact_v2pub.org_contact_rec_type;
      oocpartyid               hz_parties.party_id%TYPE;
      oocpartynumber           hz_parties.party_number%TYPE;
      oocpartyrelnumber        hz_org_contacts.contact_number%TYPE;
      oocpartyrelid            hz_party_relationships.party_relationship_id%TYPE;
      oocorgcontactid          hz_org_contacts.org_contact_id%TYPE;
      -- contact role
      pcustacctrolerec         hz_cust_account_role_v2pub.cust_account_role_rec_type;
      ocustacctroleid          hz_cust_account_roles.cust_account_role_id%TYPE;
      --contact variables
      pcontactpointrec         hz_contact_point_v2pub.contact_point_rec_type;
      pedirec                  hz_contact_point_v2pub.edi_rec_type;
      pemailrec                hz_contact_point_v2pub.email_rec_type;
      pphonerec                hz_contact_point_v2pub.phone_rec_type;
      ptelexrec                hz_contact_point_v2pub.telex_rec_type;
      pwebrec                  hz_contact_point_v2pub.web_rec_type;
      ocontactpointid          NUMBER;
      -- responsibility
      proleresponsibilityrec   hz_cust_account_role_v2pub.role_responsibility_rec_type;
      oresponsibilityid        NUMBER;
      l_proceed                VARCHAR2 (1);
      --
      ostatus                  VARCHAR2 (1);
      omsgcount                NUMBER;
      omsgdata                 VARCHAR2 (2000);

      v_dup_roll               NUMBER;

      CURSOR c2
      IS
         SELECT *
           FROM XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
          WHERE ROWID = p_rowid AND process_status = 'V';
   BEGIN
      NULL;
   END pop_contact_info;

   /*************************************************************************
     Procedure :  pop_org_communication

     Purpose:    Procedure used by Customer_Interface to create communication
         points at the applicable level
   ************************************************************************/
   PROCEDURE pop_org_communication (p_level             IN     VARCHAR2
                                   ,p_party_id          IN     NUMBER
                                   ,p_phone_area_code   IN     VARCHAR2
                                   ,p_contact_phone     IN     VARCHAR2
                                   ,p_fax_area_code     IN     VARCHAR2
                                   ,p_contact_fax       IN     VARCHAR2
                                   ,p_out                  OUT VARCHAR2
                                   ,p_err_msg              OUT VARCHAR2)
   IS
      --contact variables
      pcontactpointrec   hz_contact_point_v2pub.contact_point_rec_type;
      pedirec            hz_contact_point_v2pub.edi_rec_type;
      pemailrec          hz_contact_point_v2pub.email_rec_type;
      pphonerec          hz_contact_point_v2pub.phone_rec_type;
      ptelexrec          hz_contact_point_v2pub.telex_rec_type;
      pwebrec            hz_contact_point_v2pub.web_rec_type;
      ocontactpointid    NUMBER;

      ostatus            VARCHAR2 (1);
      omsgcount          NUMBER;
      omsgdata           VARCHAR2 (2000);
   BEGIN
      IF p_contact_phone IS NOT NULL
      THEN
         ostatus := NULL;
         omsgcount := NULL;
         omsgdata := NULL;
         pcontactpointrec := NULL;
         pcontactpointrec.contact_point_type := 'PHONE';

         IF p_level = 'PARTY'
         THEN
            pcontactpointrec.owner_table_name := 'HZ_PARTIES';
         ELSE
            pcontactpointrec.owner_table_name := 'HZ_PARTY_SITES';
         END IF;

         pcontactpointrec.owner_table_id := p_party_id;
         pcontactpointrec.primary_flag := 'Y';
         pcontactpointrec.contact_point_purpose := 'BUSINESS';
         pphonerec.phone_country_code := NULL;
         pphonerec.phone_area_code := p_phone_area_code;
         pphonerec.phone_number := p_contact_phone;
         pphonerec.phone_extension := NULL;
         pphonerec.phone_line_type := 'GEN';
         pcontactpointrec.created_by_module := 'TCA_V1_API';

         hz_contact_point_v2pub.create_contact_point (
            p_init_msg_list       => 'T'
           ,p_contact_point_rec   => pcontactpointrec
           ,p_phone_rec           => pphonerec
           ,x_contact_point_id    => ocontactpointid
           ,x_return_status       => ostatus
           ,x_msg_count           => omsgcount
           ,x_msg_data            => omsgdata);

         IF ostatus <> 'S'
         THEN
            IF omsgcount > 1
            THEN
               FOR i IN 1 .. omsgcount
               LOOP
                  fnd_file.put_line (
                     fnd_file.output
                    ,   'Org Phone contact point '
                     || SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false)
                               ,1
                               ,255));
                  p_err_msg :=
                        p_err_msg
                     || SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false)
                               ,1
                               ,255);
               END LOOP;
            ELSE
               fnd_file.put_line (fnd_file.output
                                 ,'Org Phone contact point ' || omsgdata);
               p_err_msg := omsgdata;
            END IF;

            p_out := 'E';
            RETURN;
         END IF;
      END IF;

      --Org level fax
      IF p_contact_fax IS NOT NULL
      THEN
         ostatus := NULL;
         omsgcount := NULL;
         omsgdata := NULL;
         pcontactpointrec := NULL;
         pcontactpointrec.contact_point_type := 'PHONE';

         IF p_level = 'PARTY'
         THEN
            pcontactpointrec.owner_table_name := 'HZ_PARTIES';
         ELSE
            pcontactpointrec.owner_table_name := 'HZ_PARTY_SITES';
         END IF;

         pcontactpointrec.owner_table_id := p_party_id;
         pcontactpointrec.contact_point_purpose := 'BUSINESS';
         pphonerec.phone_country_code := NULL;
         pphonerec.phone_area_code := p_fax_area_code;
         pphonerec.phone_number := p_contact_fax;
         pphonerec.phone_line_type := 'FAX';
         pcontactpointrec.created_by_module := 'TCA_V1_API';

         hz_contact_point_v2pub.create_contact_point (
            p_init_msg_list       => 'T'
           ,p_contact_point_rec   => pcontactpointrec
           ,p_phone_rec           => pphonerec
           ,x_contact_point_id    => ocontactpointid
           ,x_return_status       => ostatus
           ,x_msg_count           => omsgcount
           ,x_msg_data            => omsgdata);

         IF ostatus <> 'S'
         THEN
            IF omsgcount > 1
            THEN
               FOR i IN 1 .. omsgcount
               LOOP
                  fnd_file.put_line (
                     fnd_file.output
                    ,   'Org Fax contact point '
                     || SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false)
                               ,1
                               ,255));
                  p_err_msg :=
                        p_err_msg
                     || SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false)
                               ,1
                               ,255);
               END LOOP;
            ELSE
               fnd_file.put_line (fnd_file.output
                                 ,'Org Fax contact point ' || omsgdata);
               p_err_msg := omsgdata;
            END IF;

            p_out := 'E';
            RETURN;
         END IF;
      END IF;
   END pop_org_communication;

   /*************************************************************************
     Procedure :  upd_stagin_table_err

     Purpose:    Procedure to update staging tables with error messages
   ************************************************************************/
   PROCEDURE upd_stagin_table_err (p_cus_err     VARCHAR2
                                  ,p_site_err    VARCHAR2
                                  ,p_con_err     VARCHAR2
                                  ,p_rid         VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      IF p_cus_err IS NOT NULL
      THEN
         UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
            SET process_error = SUBSTR (process_error || p_cus_err, 1, 4000)
          WHERE ROWID = p_rid;
      ELSIF p_site_err IS NOT NULL
      THEN
         UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
            SET process_error = SUBSTR (process_error || p_site_err, 1, 4000)
          WHERE ROWID = p_rid;
      ELSIF p_con_err IS NOT NULL
      THEN
         UPDATE XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
            SET process_error = SUBSTR (process_error || p_con_err, 1, 4000)
          WHERE ROWID = p_rid;
      END IF;

      COMMIT;
   END;

   /*************************************************************************
     Procedure :  upd_stagin_table

     Purpose:    Procedure to update staging tables with error code across
         all staging areas
   ************************************************************************/
   PROCEDURE upd_stagin_table (p_orig_system_reference IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      BEGIN
         UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
            SET process_status = 'E'
          WHERE orig_system_reference = p_orig_system_reference;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      BEGIN
         UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
            SET process_status = 'E'
          WHERE orig_system_reference = p_orig_system_reference;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      BEGIN
         UPDATE XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
            SET process_status = 'E'
          WHERE orig_system_reference = p_orig_system_reference;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      COMMIT;
   END upd_stagin_table;

   /*************************************************************************
     Procedure :  customer_interface

     Purpose:    Procedure called by the Process File to load the valid
         data in the staging tables into Oracle
   ************************************************************************/
   PROCEDURE customer_interface (errbuf       OUT VARCHAR2
                                ,retcode      OUT VARCHAR2
                                ,p_mode           VARCHAR2
                                ,prefix    IN     VARCHAR2 DEFAULT NULL)
   AS
      -- cursor for selecting the party level information.
      CURSOR get_party
      IS
         SELECT cus.ROWID rid, cus.*
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG cus
          WHERE 1 = 1 AND process_status = 'V';

      -- selecting multiple accounts for a party
      CURSOR get_account (
         p_cust IN VARCHAR2)
      IS
         SELECT acc.ROWID rid, acc.*
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG acc
          WHERE     1 = 1
                AND orig_system_reference = p_cust
                AND acc.process_status = 'V';

      -- selecting sites for the customer account
      CURSOR get_sites (
         p_cust IN VARCHAR2)
      IS
           SELECT stg.ROWID rid, stg.*
             FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG stg
            WHERE     1 = 1
                  AND orig_system_reference = p_cust
                  AND stg.process_status = 'V'
                  AND stg.hds_site_flag <> 'SHIP'
         ORDER BY legacy_site_type DESC;

      CURSOR get_job_sites
      IS
         SELECT x1.ROWID rid, x1.*
           FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG x1
          WHERE     x1.process_status = 'V'
                AND NOT EXISTS
                           (SELECT 'Has Account'
                              FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG x2
                             WHERE x2.orig_system_reference =
                                      x1.orig_system_reference);


      cust_party                    get_party%ROWTYPE;

      --Get party contact point
      CURSOR get_contact (
         p_cust_site IN VARCHAR2)
      IS
         SELECT stg.ROWID rid, stg.*
           FROM XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG stg
          WHERE     1 = 1
                AND orig_system_reference = p_cust_site
                AND legacy_party_site_number = orig_system_reference
                AND stg.process_status = 'V';

      --Get site contact point
      CURSOR get_site_contact (
         p_cust_site                IN VARCHAR2
        ,p_legacy_customer_number   IN VARCHAR2)
      IS
         SELECT stg.ROWID rid, stg.*
           FROM XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG stg
          WHERE     1 = 1
                AND orig_system_reference = p_cust_site
                AND legacy_party_site_number = p_legacy_customer_number
                AND stg.process_status = 'V'
                AND (   stg.fax_number IS NOT NULL
                     OR stg.phone_number IS NOT NULL);


      -- Satish U: 09-JAN-2012 Define a Cursor to get Frieght Terms from MISC or MAST and if value exists
      CURSOR Freight_Terms_Cur (
         p_Orig_System_Reference VARCHAR2)
      IS
         SELECT Freight_Term, Legacy_party_Site_Number, HDS_SITE_FLAG
           FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
          WHERE     HDS_SITE_FLAG IN ('MISC', 'MSTR')
                AND Orig_System_Reference = p_Orig_System_Reference
                AND Freight_Term IS NOT NULL
                AND ROWNUM = 1;

      l_Freight_Terms               VARCHAR2 (40);

      -- 03/16/2012 CG: Copying parent account profile information for Job Site
      CURSOR Cur_cust_prof (p_cust_account_id IN NUMBER)
      IS
         SELECT *
           FROM hz_customer_profiles
          WHERE cust_account_id = p_cust_account_id AND site_use_id IS NULL;

      l_cust_acct_profile           hz_customer_profiles%ROWTYPE;

      --
      ostatus                       VARCHAR2 (1);
      omsgcount                     NUMBER;
      omsgdata                      VARCHAR2 (2000);
      v_count                       NUMBER;
      l_count                       NUMBER;
      l_proceed                     VARCHAR2 (1);
      l_party                       VARCHAR2 (1);
      l_profile_id                  NUMBER;
      -- party variables
      porganizationrec              hz_party_v2pub.organization_rec_type;
      opartyid                      hz_parties.party_id%TYPE;
      opartynumber                  hz_parties.party_number%TYPE;
      oprofileid                    NUMBER;
      osprofile_id                  NUMBER;
      -- party account variables
      pcustaccountrec               hz_cust_account_v2pub.cust_account_rec_type;
      ppartyrec                     hz_party_v2pub.party_rec_type;
      pprofilerec                   hz_customer_profile_v2pub.customer_profile_rec_type;
      ocustaccountid                hz_cust_accounts.cust_account_id%TYPE;
      ocustaccountno                hz_cust_accounts.account_number%TYPE;
      --location variable
      plocationrec                  hz_location_v2pub.location_rec_type;
      olocationid                   hz_locations.location_id%TYPE;
      --site variables
      ppartysiterec                 hz_party_site_v2pub.party_site_rec_type;
      opartysiteid                  hz_party_sites.party_site_id%TYPE;
      opartysiteno                  hz_party_sites.party_site_number%TYPE;
      --site account avriables
      pcustacctsiterec              hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      ocustacctsiteid               hz_cust_acct_sites.cust_acct_site_id%TYPE;
      --site use variables
      pcustacctsiteuserec           hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile              hz_customer_profile_v2pub.customer_profile_rec_type;
      ocustacctsiteuseid            NUMBER;
      pbilltositeuseid              NUMBER;
      pcustacctid                   NUMBER;
      l_terms_id                    NUMBER;
      l_sales_person_id             NUMBER;
      l_bp                          NUMBER;
      l_territory                   NUMBER;
      ln_partysiteid_misc_bill_to   NUMBER;
      oalias                        VARCHAR2 (240);

      -- amount profile variables
      pcustproamtrec                hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      ocustacctprofileamtid         NUMBER;
      pcurrencycode                 VARCHAR2 (10);
      ---------
      whereami                      NUMBER := 0;
      e_validation_exception        EXCEPTION;
      p_result                      VARCHAR2 (1);
      p_mstr_counter                NUMBER;
      p_yard_counter                NUMBER;
      p_loc_count                   NUMBER;
      p_mstr_loc_count              NUMBER;
      p_yard_loc_count              NUMBER;
      p_job_loc_count               NUMBER;

      req_status                    BOOLEAN;
      v_version_no                  NUMBER;

      p_collector_id                NUMBER;
      p_credit_analyst_id           NUMBER;
      p_credit_classification       ar_lookups.lookup_code%TYPE;
      p_payment_term_id             NUMBER;
      p_object_version_number       NUMBER;
      v_err                         VARCHAR2 (1) := 'N';
      p_cus_err                     VARCHAR2 (4000);
      p_site_err                    VARCHAR2 (4000);
      p_con_err                     VARCHAR2 (4000);
      p_rid                         VARCHAR2 (100);
      l_rec_count                   NUMBER;
      c_max_commit_count   CONSTANT NUMBER := 100;
      l_api_name                    VARCHAR2 (30)
                                       := 'XXWC_AR_PRISM_CUST_IFACE_STG';
      lv_primary_ship_to_flag       VARCHAR2 (2);

      l_err                         VARCHAR2 (1);
      l_legacy_party_site           VARCHAR2 (30);
   BEGIN
      --If mode Validate the validate the records
      g_parameter_prefix := prefix;
      l_rec_count := 0;


      IF p_mode = 'Validate'
      THEN
         get_cust_conversion_stats;
         RETURN;
      END IF;

      fnd_file.put_line (fnd_file.LOG, 'PRISM TO EBS INTERFACE');
      fnd_file.put_line (fnd_file.LOG, '********************');

      v_count := 0;

      FOR cust_party IN get_party
      LOOP
         SAVEPOINT customer_record;
         l_rec_count := l_rec_count + 1;

         -- fnd_file.put_line(fnd_file.log,'1111-Inside Customer Party Loop'||Cust_party.Orig_System_Reference);
         BEGIN
            v_count := v_count + 1;
            l_party := 'N';

            IF l_party = 'N'
            THEN
               -- start create party
               opartyid := NULL;
               porganizationrec := NULL;
               ostatus := NULL;
               omsgcount := NULL;
               omsgdata := NULL;

               fnd_file.put_line (fnd_file.LOG
                                 ,'200 Building Organization Record');

               -- create the party organization record
               porganizationrec.organization_name :=
                  prefix || cust_party.organization_name;
               porganizationrec.party_rec.orig_system_reference :=
                  prefix || cust_party.orig_system_reference;
               porganizationrec.duns_number_c := cust_party.duns_number_c;
               porganizationrec.gsa_indicator_flag :=
                  cust_party.gsa_indicator_flag;
               porganizationrec.attribute20 :=
                  cust_party.cod_comment || '-' || cust_party.keyword;
               porganizationrec.created_by_module := 'TCA_V1_API';

               SELECT hz_parties_s.NEXTVAL INTO ppartyrec.party_id FROM DUAL;

               --Sic code and sic code type
               IF     cust_party.sic_code IS NOT NULL
                  AND cust_party.sic_code <> 0
               THEN
                  BEGIN
                     whereami := 95;

                     SELECT lookup_code, lookup_type
                       INTO porganizationrec.sic_code
                           ,porganizationrec.sic_code_type
                       FROM ar_lookups b
                      WHERE     b.lookup_code = cust_party.sic_code
                            AND b.lookup_type = '1972 SIC';
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        porganizationrec.sic_code_type := NULL;
                        porganizationrec.sic_code := NULL;
                  END;
               END IF;

               whereami := 100;

               -- party api
               --fnd_file.put_line(fnd_file.log,'900-Before Calling API Create Organization'||Cust_party.Orig_System_Reference);
               hz_party_v2pub.create_organization (
                  p_init_msg_list      => 'T'
                 ,p_organization_rec   => porganizationrec
                 ,x_return_status      => ostatus
                 ,x_msg_count          => omsgcount
                 ,x_msg_data           => omsgdata
                 ,x_party_id           => opartyid
                 ,x_party_number       => opartynumber
                 ,x_profile_id         => oprofileid);
            END IF;


            IF ostatus <> 'S'
            THEN
               whereami := 110;

               --fnd_file.put_line(fnd_file.log,'1111-Create Organization Errored Out : Msg Count :'||omsgcount);
               IF omsgcount > 1
               THEN
                  whereami := 115;

                  FOR i IN 1 .. omsgcount
                  LOOP
                     p_cus_err :=
                           p_cus_err
                        || SUBSTR (
                              fnd_msg_pub.get (i
                                              ,p_encoded   => fnd_api.g_false)
                             ,1
                             ,255);
                  END LOOP;

                  --Satish U: 11/01/2011 : FOllwoing Statement Moved outside the Loop
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'PARTY ORGANIZATION RECORD '
                     || opartynumber
                     || SUBSTR (p_cus_err, 1, 255));
               ELSE
                  whereami := 120;
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,'PARTY ORGANIZATION RECORD ' || omsgdata || opartynumber);
                  p_cus_err := omsgdata;
               END IF;

               p_rid := cust_party.rid;
               whereami := 130;
               RAISE e_validation_exception;
            ELSE
               --initialize the values
               ostatus := NULL;
               omsgcount := NULL;
               omsgdata := NULL;

               -- call the customer account api
               -- get orig system refrence value
               FOR cust_acc IN get_account (cust_party.orig_system_reference)
               LOOP
                  pcustproamtrec := NULL;
                  pcustaccountrec := NULL;
                  pprofilerec := NULL;
                  lv_primary_ship_to_flag := NULL;

                  --create org level communication
                  --xxpop_org_communication(opartyid,cust_acc.contact_phone,cust_acc.contact_fax,p_result);
                  --     if p_result='e' then
                  --               raise e_validation_exception;
                  --     end if;

                  porganizationrec.party_rec.party_id := opartyid;

                  pcustaccountrec.account_name :=
                     prefix || cust_acc.account_name;
                  pcustaccountrec.status := 'A';            --cust_acc.status;
                  pcustaccountrec.orig_system_reference :=
                     prefix || cust_acc.orig_system_reference;
                  pcustaccountrec.tax_code := NULL;
                  pcustaccountrec.account_established_date :=
                     NVL (TO_DATE (cust_acc.year_established, 'YYYY-MM-DD')
                         ,SYSDATE - 1);
                  pcustaccountrec.created_by_module := 'TCA_V1_API';
                  -- 03/26/2012 CG added Prism Account Number to DFF 6
                  pcustaccountrec.attribute6 := cust_acc.orig_system_reference;


                  -- Satish U : Udpate Customer Account : Frieght Terms : 09JAN-2012
                  FOR Freight_Terms_Rec
                     IN Freight_Terms_Cur (cust_acc.Orig_System_Reference)
                  LOOP
                     l_Freight_Terms := NULL;

                     IF UPPER (Freight_Terms_Rec.Freight_Term) =
                           'FREIGHT EXEMPT'
                     THEN
                        pcustaccountrec.Freight_Term := 'EXEMPT';
                     ELSE
                        pcustaccountrec.Freight_Term := NULL;
                     END IF;
                  END LOOP;

                  --account level customer classification
                  BEGIN
                     whereami := 135;

                     SELECT fl.lookup_code
                       INTO pcustaccountrec.customer_class_code
                       FROM fnd_lookup_values_vl fl
                           ,xxwc_cust_classification_xref x
                      WHERE     fl.lookup_type = 'CUSTOMER CLASS'
                            AND UPPER (fl.lookup_code) =
                                   UPPER (x.cust_classification)
                            AND UPPER (x.prism_code) =
                                   UPPER (cust_acc.customer_class_code)
                            AND TRUNC (SYSDATE) BETWEEN fl.start_date_active
                                                    AND NVL (
                                                           fl.end_date_active
                                                          ,  TRUNC (SYSDATE)
                                                           + 1)
                            AND fl.enabled_flag = 'Y';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustaccountrec.customer_class_code := 'GEN_CONTRACT';
                  END;

                  /*DFF Name ======== Customer Information (Account level)*/

                  --CHECK CREDIT CLASS CODE WITH
                  IF UPPER (cust_acc.attribute1) IN ('Y', 'YES')
                  THEN
                     pcustaccountrec.attribute_category := 'Yes';     --'Yes';
                  END IF;

                  --Mandatory Notes
                  --pcustaccountrec.attribute6:='Old Mandatory Note';  --Old Mandatory Note';
                  pcustaccountrec.attribute17 := cust_acc.attribute17;
                  pcustaccountrec.attribute18 := cust_acc.attribute18;
                  pcustaccountrec.attribute19 := cust_acc.attribute19;
                  pcustaccountrec.attribute20 := cust_acc.attribute20;
                  pcustaccountrec.attribute16 := cust_acc.attribute21; -- added by v.sankar on 10/24/2011
                  pcustaccountrec.attribute4 := cust_acc.customer_source; --added by v.sankar on 11/22/2011


                  --collection agency attribute5
                  -- Satish U: 24-JAN-2012 :  Should be Attribute6 not Attribute8 on Sites Record
                  BEGIN
                     --SELECT   Prism_code
                     -- Satish U 02-21-2-12 : SHould be Populating Oracle Legal Indicator column value
                     SELECT Oracle_Legal_Indicator
                       INTO pcustaccountrec.attribute5
                       FROM XXWC_LEGAL_COLLECTION_IND_XREF a
                           ,XXWC.XXWC_AR_CUST_SITE_IFACE_STG sites
                      WHERE     a.PRISM_CODE = sites.attribute6
                            AND sites.orig_system_reference =
                                   cust_acc.orig_system_reference
                            AND sites.hds_site_flag IN ('MSTR', 'MISC')
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        BEGIN
                           -- Check If Site Record Attribute6 has value
                           -- If so then assign default value 'AGENCY' to Attribute5 column
                           SELECT 'AGENCY'
                             INTO pcustaccountrec.attribute5
                             FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG sites
                            WHERE     sites.orig_system_reference =
                                         cust_acc.orig_system_reference
                                  AND sites.hds_site_flag IN ('MSTR', 'MISC')
                                  AND Sites.Attribute6 IS NOT NULL
                                  AND ROWNUM = 1;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              pcustaccountrec.attribute5 := NULL;
                        END;
                  END;


                  --account level get profile class
                  -- 03/14/2012 CG Modified for iFace
                  BEGIN
                     SELECT profile_class_id
                       --customer_profile_class_id
                       INTO l_profile_id
                       FROM hz_cust_profile_classes cpc
                           ,               --ar_customer_profile_classes_v av,
                            xxwc_cust_profile_class_xref x
                      WHERE     UPPER (cpc.name) = UPPER (x.profile_class)
                            AND x.prism_code =
                                   TO_NUMBER (
                                      cust_acc.customer_profile_class_name)
                            AND cpc.status = 'A';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        BEGIN
                           SELECT profile_class_id
                             INTO l_profile_id
                             FROM hz_cust_profile_classes cpc
                            WHERE     cpc.name = 'COD Customers'
                                  AND cpc.status = 'A';
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              l_profile_id := 0;                  --is default
                        END;
                  END;

                  pprofilerec.profile_class_id := l_profile_id;

                  --fnd_file.put_line (fnd_file.LOG,' Customer Profile id: '||pprofilerec.profile_class_id);

                  --pprofilerec. credit_checking:=cust_acc.credit_check;
                  IF cust_acc.send_statements IN ('Y', 'N', 'C')
                  THEN
                     IF cust_acc.send_statements IN ('Y', 'C')
                     THEN
                        pprofilerec.send_statements := 'Y';
                     ELSE
                        pprofilerec.send_statements := 'N';
                     END IF;
                  ELSE
                     pprofilerec.send_statements := 'N';
                  END IF;

                  IF cust_acc.send_statements NOT IN ('Y', 'N', 'C')
                  THEN
                     pprofilerec.credit_balance_statements := 'N';
                  ELSIF cust_acc.send_statements IN ('Y', 'C', 'N')
                  THEN
                     IF cust_acc.credit_balance_statements IN ('Y', 'C')
                     THEN
                        pprofilerec.credit_balance_statements := 'Y';
                     ELSIF cust_acc.credit_balance_statements = 'N'
                     THEN
                        pprofilerec.credit_balance_statements := 'N';
                     END IF;
                  END IF;

                  IF     cust_acc.attribute3 IN ('Y', 'N', 'C')
                     AND cust_acc.send_statements IN ('Y', 'C')
                  THEN
                     IF cust_acc.attribute3 = 'C'              --IN ('Y', 'N')
                     THEN
                        pprofilerec.attribute3 := 'Y'; --cust_acc.attribute3; --Y N null Statement by Job
                     ELSE
                        pprofilerec.attribute3 := 'N';
                     END IF;
                  END IF;

                  pprofilerec.credit_hold := cust_acc.credit_hold;

                  BEGIN
                     SELECT x.collector_id                   --ac.collector_id
                       INTO p_collector_id
                       FROM                                --ar_collectors ac,
                           xxwc_collector_cr_analyst_xref x
                      WHERE     1 = 1
                            --UPPER (ac.name) = UPPER (oracle_collector)
                            AND UPPER (x.prism_code) =
                                   UPPER (cust_acc.collector_name);
                  --AND ac.status = 'A';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        p_collector_id := 1;               --Default collector
                  END;

                  pprofilerec.collector_id := p_collector_id;
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   ' Customer Collector: '
                     || pprofilerec.collector_id
                     || ' for '
                     || cust_acc.collector_name);

                  BEGIN
                     whereami := 140;

                     --- Check this Query for performance and in the exception  add message to OUT
                     SELECT x.credit_analyst_id
                       INTO p_credit_analyst_id
                       FROM xxwc_collector_cr_analyst_xref x
                      WHERE     1 = 1
                            AND UPPER (x.prism_code) =
                                   UPPER (cust_acc.collector_name);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        p_credit_analyst_id := NULL;
                  END;

                  pprofilerec.credit_analyst_id := p_credit_analyst_id;
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   ' Credit Analyst: '
                     || pprofilerec.credit_analyst_id
                     || ' for '
                     || cust_acc.collector_name);

                  BEGIN
                     whereami := 145;

                     --- Check this Query for performance and in the exception  add message to OUT
                     -- Mention in the output that Credit Classification is defaulting
                     SELECT al.lookup_code
                       INTO p_credit_classification
                       FROM ar_lookups al, xxwc_cr_classification_xref x
                      WHERE     al.lookup_code = x.oracle_code
                            AND UPPER (x.prism_code) =
                                   UPPER (cust_acc.credit_classification)
                            AND al.lookup_type =
                                   'AR_CMGT_CREDIT_CLASSIFICATION'
                            AND al.enabled_flag = 'Y'
                            AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                                           al.start_date_active)
                                                    AND TRUNC (
                                                           NVL (
                                                              al.end_date_active
                                                             ,SYSDATE + 1));
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        p_credit_classification := 'VHIGH';
                  END;

                  pprofilerec.credit_classification := p_credit_classification;

                  -- account level payment terms account
                  BEGIN
                     whereami := 150;

                     SELECT rt.term_id
                       INTO p_payment_term_id
                       FROM xxwc_payment_terms_xref xpt, ra_terms rt
                      WHERE     UPPER (rt.name) = UPPER (xpt.oracle_term)
                            AND xpt.prism_code = cust_acc.standard_term_name
                            AND TRUNC (SYSDATE) BETWEEN start_date_active
                                                    AND NVL (
                                                           end_date_active
                                                          ,  TRUNC (SYSDATE)
                                                           + 1);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        p_payment_term_id := NULL;
                  END;

                  pprofilerec.standard_terms := p_payment_term_id;


                  /*DFF Name ======== Customer Credit Profile Information (Account level profile)*/

                  --Invoice Remit To Address Code (account level)
                  BEGIN
                     whereami := 155;

                     SELECT oracle_code
                       INTO pprofilerec.attribute2
                       FROM xxwc_remit_to_addr_xref x
                      WHERE x.prism_code = cust_acc.attribute2;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        pprofilerec.attribute2 := 2;
                  END;

                  --account status . added by v.sankar on 11/22/2011
                  BEGIN
                     whereami := 160;

                     SELECT lookup_code
                       INTO pprofilerec.account_status
                       FROM xxwc_account_status_xref a, ar_lookups b
                      WHERE     a.account_status = b.meaning
                            AND b.lookup_type = 'ACCOUNT_STATUS'
                            AND a.prism_credit_group =
                                   cust_acc.credit_analyst_name;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        pprofilerec.account_status := NULL;
                  END;

                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   ' Profile Account Status :'
                     || pprofilerec.account_status
                     || ' for '
                     || cust_acc.credit_analyst_name);

                  whereami := 165;

                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   'Before Calling Create Customer Account Record :'
                     || pcustaccountrec.account_name);
                  hz_cust_account_v2pub.create_cust_account (
                     p_init_msg_list          => 'T'
                    ,p_cust_account_rec       => pcustaccountrec
                    ,                               -- customer account record
                     p_organization_rec       => porganizationrec
                    ,                             -- party organization record
                     p_customer_profile_rec   => pprofilerec
                    ,p_create_profile_amt     => fnd_api.g_true
                    ,                                         --fnd_api.g_true
                     x_cust_account_id        => ocustaccountid
                    ,x_account_number         => ocustaccountno
                    ,x_party_id               => opartyid
                    ,x_party_number           => opartynumber
                    ,x_profile_id             => oprofileid
                    ,x_return_status          => ostatus
                    ,x_msg_count              => omsgcount
                    ,x_msg_data               => omsgdata);

                  IF ostatus <> 'S'
                  THEN
                     whereami := 200;

                     --fnd_file.put_line(fnd_file.log,'1112-Create Customer Account API Errored OUt'||omsgcount);
                     IF omsgcount > 1
                     THEN
                        whereami := 205;

                        FOR i IN 1 .. omsgcount
                        LOOP
                           p_cus_err :=
                                 NVL (p_cus_err, ' ')
                              || SUBSTR (
                                    fnd_msg_pub.get (
                                       i
                                      ,p_encoded   => fnd_api.g_false)
                                   ,1
                                   ,255);
                        END LOOP;

                        --SatishU: 11/01/11 : Following SQL Statement Moved outside the Loop
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   'CUSTOMER ACCOUNT RECORD '
                           || cust_acc.account_name
                           || ' '
                           || SUBSTR (p_cus_err, 1, 255));
                     ELSE
                        whereami := 210;
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   'CUSTOMER ACCOUNT RECORD '
                           || cust_acc.account_name
                           || ' '
                           || omsgdata);
                        p_cus_err := omsgdata;
                     END IF;

                     p_rid := cust_party.rid;
                     whereami := 220;
                     RAISE e_validation_exception;
                  ELSE
                     IF    NVL (cust_acc.trx_credit_limit, 0) != 0
                        OR NVL (cust_acc.overall_credit_limit, 0) != 0
                     THEN
                        pcustproamtrec := NULL;

                        IF oprofileid IS NULL
                        THEN
                           whereami := 230;

                           SELECT cust_account_profile_id
                             INTO oprofileid
                             FROM hz_customer_profiles
                            WHERE cust_account_id = ocustaccountid;
                        END IF;

                        IF oprofileid IS NOT NULL
                        THEN
                           BEGIN
                              whereami := 240;

                              SELECT cust_acct_profile_amt_id
                                    ,object_version_number
                                INTO ocustacctprofileamtid
                                    ,p_object_version_number
                                FROM hz_cust_profile_amts
                               WHERE     cust_account_profile_id = oprofileid
                                     AND currency_code = 'USD';
                           EXCEPTION
                              WHEN NO_DATA_FOUND
                              THEN
                                 ocustacctprofileamtid := NULL;
                                 p_object_version_number := NULL;
                           END;

                           IF ocustacctprofileamtid IS NOT NULL
                           THEN
                              --Update amount profile
                              pcustproamtrec.cust_acct_profile_amt_id :=
                                 ocustacctprofileamtid;
                              pcustproamtrec.trx_credit_limit :=
                                 cust_acc.trx_credit_limit;
                              pcustproamtrec.overall_credit_limit :=
                                 cust_acc.overall_credit_limit;
                              pcustproamtrec.created_by_module := 'TCA_V1_API';

                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,' Before Calling API Update Customer Profile Amt : ');
                              hz_customer_profile_v2pub.update_cust_profile_amt (
                                 p_init_msg_list           => 'T'
                                ,p_cust_profile_amt_rec    => pcustproamtrec
                                ,p_object_version_number   => p_object_version_number
                                ,x_return_status           => ostatus
                                ,x_msg_count               => omsgcount
                                ,x_msg_data                => omsgdata);

                              IF ostatus <> 'S'
                              THEN
                                 whereami := 230;

                                 -- fnd_file.put_line(fnd_file.log,'1113-Update Profile Amount API Errored Out'||omsgcount);
                                 IF omsgcount > 1
                                 THEN
                                    FOR i IN 1 .. omsgcount
                                    LOOP
                                       p_cus_err :=
                                             NVL (p_cus_err, ' ')
                                          || SUBSTR (
                                                fnd_msg_pub.get (
                                                   i
                                                  ,p_encoded   => fnd_api.g_false)
                                               ,1
                                               ,255);
                                    END LOOP;

                                    -- Satish U: 11/01/11 Moved Outside the Loop
                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,   ' CUSTOMER Amout Profile Updation '
                                       || SUBSTR (p_cus_err, 1, 255));
                                 ELSE
                                    whereami := 240;
                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,   ' CUSTOMER Amount Profile Updation '
                                       || omsgdata);
                                    p_cus_err := omsgdata;
                                 END IF;

                                 p_rid := cust_party.rid;
                                 RAISE e_validation_exception;
                              END IF;
                           ELSE
                              --Create Profile Amount
                              pcustproamtrec.cust_account_profile_id :=
                                 oprofileid; -- customer profile id from above api
                              pcustproamtrec.cust_account_id := ocustaccountid;
                              pcustproamtrec.currency_code := 'USD';
                              pcustproamtrec.trx_credit_limit :=
                                 cust_acc.trx_credit_limit;
                              pcustproamtrec.overall_credit_limit :=
                                 cust_acc.overall_credit_limit;
                              pcustproamtrec.created_by_module := 'TCA_V1_API';

                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,' Before Calling Create Customer Profile Amount API ');
                              hz_customer_profile_v2pub.create_cust_profile_amt (
                                 p_init_msg_list              => 'T'
                                ,p_check_foreign_key          => 'T'
                                ,p_cust_profile_amt_rec       => pcustproamtrec
                                ,x_cust_acct_profile_amt_id   => ocustacctprofileamtid
                                ,x_return_status              => ostatus
                                ,x_msg_count                  => omsgcount
                                ,x_msg_data                   => omsgdata);

                              IF ostatus <> 'S'
                              THEN
                                 whereami := 250;

                                 -- fnd_file.put_line(fnd_file.log,'1114- Create Customer Profile AMpount API Errored Out'||omsgcount);
                                 IF omsgcount > 1
                                 THEN
                                    FOR i IN 1 .. omsgcount
                                    LOOP
                                       p_cus_err :=
                                             NVL (p_cus_err, ' ')
                                          || SUBSTR (
                                                fnd_msg_pub.get (
                                                   i
                                                  ,p_encoded   => fnd_api.g_false)
                                               ,1
                                               ,255);
                                    END LOOP;

                                    -- Satish U: Move Outside the Loop
                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,   ' CUSTOMER Amout Profile Creation '
                                       || SUBSTR (p_cus_err, 1, 255));
                                 ELSE
                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,   ' CUSTOMER Amount Profile Creation '
                                       || omsgdata);
                                    p_cus_err := omsgdata;
                                 END IF;

                                 p_rid := cust_party.rid;
                                 whereami := 260;
                                 RAISE e_validation_exception;
                              END IF;
                           END IF;
                        END IF;
                     END IF;                                  --amount profile


                     --set the yard counter
                     p_yard_counter := 1;
                     p_mstr_counter := 1;
                     p_loc_count := 1;
                     p_mstr_loc_count := 1;
                     p_yard_loc_count := 1;
                     p_job_loc_count := 1;

                     --Open Sites
                     FOR cust_sites
                        IN get_sites (cust_acc.orig_system_reference)
                     LOOP
                        --fnd_file.put_line (fnd_file.output,'CREATING SITE ' || cust_sites.location || 'FOR CUSTOMER ' || cust_party.account_name);
                        plocationrec := NULL;
                        ppartysiterec := NULL;
                        pcustacctsiterec := NULL;
                        pcustacctsiteuserec := NULL;
                        pcustomerprofile := NULL;
                        ostatus := NULL;
                        omsgcount := NULL;
                        omsgdata := NULL;
                        ln_partysiteid_misc_bill_to := NULL;

                        l_count := 0;
                        olocationid := NULL;
                        ostatus := NULL;

                        BEGIN
                           SELECT location_id
                             INTO olocationid
                             FROM hz_locations
                            WHERE location_id = cust_sites.location_id;

                           l_count := 1;
                           --olocationid := Add_Rec.Location_ID;
                           ostatus := 'S';
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              l_count := 0;
                              ostatus := NULL;
                        END;

                        IF l_count = 0
                        THEN
                           -- location creation
                           IF NVL (cust_sites.country, 'USA') IN
                                 ('USA', 'US')
                           THEN
                              plocationrec.country := 'US';
                           ELSE
                              plocationrec.country := cust_sites.country;
                           END IF;

                           plocationrec.location_id := cust_sites.location_id;
                           plocationrec.postal_code := cust_sites.postal_code;
                           -- 03/26/2012 CG
                           plocationrec.address1 :=
                              NVL (
                                 cust_sites.address1
                                ,NVL (cust_sites.address2
                                     ,'NO ADDRESS FOUND'));
                           plocationrec.address2 := cust_sites.address2;
                           plocationrec.address3 := cust_sites.address3;
                           plocationrec.address4 := cust_sites.address4;
                           plocationrec.state := cust_sites.state; -- is mandatory
                           plocationrec.city := cust_sites.city;
                           plocationrec.county := cust_sites.county; --nvl(c2.county,c2.city); --for the time being (will be updated for sales tax later)
                           plocationrec.created_by_module := 'TCA_V1_API';
                           -- Added  Attribute6 PRISM Customer#  Satish U : 02/21/2012
                           plocationrec.attribute6 :=
                              cust_sites.orig_system_reference;

                           whereami := 280;
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,' Before Calling Create Location API ');
                           hz_location_v2pub.create_location (
                              p_init_msg_list   => 'T'
                             ,p_location_rec    => plocationrec
                             ,x_location_id     => olocationid
                             ,x_return_status   => ostatus
                             ,x_msg_count       => omsgcount
                             ,x_msg_data        => omsgdata);
                        END IF;

                        IF ostatus <> 'S'
                        THEN
                           whereami := 290;

                           IF omsgcount > 1
                           THEN
                              FOR i IN 1 .. omsgcount
                              LOOP
                                 p_site_err :=
                                       NVL (p_site_err, ' ')
                                    || SUBSTR (
                                          fnd_msg_pub.get (
                                             i
                                            ,p_encoded   => fnd_api.g_false)
                                         ,1
                                         ,255);
                              END LOOP;

                              --Satish U: Moving Outside the Loop
                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,   ' CUSTOMER LOCATION RECORD '
                                 || SUBSTR (p_site_err, 1, 255));
                           ELSE
                              whereami := 300;
                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,   ' CUSTOMER LOCATION RECORD '
                                 || omsgdata
                                 || '-'
                                 || cust_sites.legacy_customer_number
                                 || '-'
                                 || cust_sites.country
                                 || '-'
                                 || cust_sites.address1
                                 || '-'
                                 || cust_sites.address2);
                              p_site_err := omsgdata;
                           END IF;

                           p_rid := cust_sites.rid;
                           whereami := 310;
                           RAISE e_validation_exception;
                        ELSE
                           --initialize the values
                           ostatus := NULL;
                           omsgcount := NULL;
                           omsgdata := NULL;
                           opartysiteid := NULL;
                           opartysiteno := NULL;

                           -- create a party site now
                           ppartysiterec.party_id := opartyid;
                           ppartysiterec.location_id := olocationid;
                           -- 04/02/2012 CGonzalez: Commented to allow autonumbering
                           ppartysiterec.party_site_number :=
                              SUBSTR (cust_sites.legacy_party_site_number
                                     ,1
                                     ,25);
                           ppartysiterec.orig_system_reference :=
                              prefix || cust_sites.orig_system_reference;
                           ppartysiterec.created_by_module := 'TCA_V1_API';

                           IF cust_sites.hds_site_flag = 'MISC'
                           THEN
                              ppartysiterec.party_site_name :=
                                    cust_sites.hds_site_flag
                                 || '-'
                                 || cust_sites.legacy_customer_number;
                           ELSIF cust_sites.legacy_site_type = 1000
                           THEN
                              ppartysiterec.party_site_name :=
                                    UPPER (cust_sites.hds_site_flag)
                                 || '-'
                                 || LPAD (p_mstr_counter, 2, '0');
                              p_mstr_counter := p_mstr_counter + 1;
                           ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                 AND 999
                           THEN
                              ppartysiterec.party_site_name :=
                                    'YARD'
                                 || '-'
                                 || LPAD (p_yard_counter, 2, '0');
                              p_yard_counter := p_yard_counter + 1;
                           ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                 AND 989
                           THEN
                              ppartysiterec.party_site_name :=
                                    cust_sites.hds_site_flag
                                 || '-'
                                 || cust_sites.legacy_customer_number;
                           END IF;

                           osprofile_id := NULL;

                           whereami := 320;
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,' Before Calling Create Party Site API ');
                           hz_party_site_v2pub.create_party_site (
                              p_init_msg_list       => 'T'
                             ,p_party_site_rec      => ppartysiterec
                             ,x_party_site_id       => opartysiteid
                             ,x_party_site_number   => opartysiteno
                             ,x_return_status       => ostatus
                             ,x_msg_count           => omsgcount
                             ,x_msg_data            => omsgdata);

                           IF ostatus <> 'S'
                           THEN
                              whereami := 330;

                              --fnd_file.put_line(fnd_file.log,'1116- Create Party Site API Errored Out'||omsgcount);
                              IF omsgcount > 1
                              THEN
                                 FOR i IN 1 .. omsgcount
                                 LOOP
                                    p_site_err :=
                                          p_site_err
                                       || SUBSTR (
                                             fnd_msg_pub.get (
                                                i
                                               ,p_encoded   => fnd_api.g_false)
                                            ,1
                                            ,255);
                                 END LOOP;

                                 -- Satish U: Moving Outside the Loop
                                 fnd_file.put_line (
                                    fnd_file.LOG
                                   ,   ' CUSTOMER PARTY SITE RECORD '
                                    || SUBSTR (p_site_err, 1, 255));
                              ELSE
                                 whereami := 340;
                                 fnd_file.put_line (
                                    fnd_file.LOG
                                   ,   ' CUSTOMER  PARTY SITE RECORD '
                                    || omsgdata);
                                 p_site_err := omsgdata;
                              END IF;

                              p_rid := cust_sites.rid;
                              RAISE e_validation_exception;
                           ELSE
                              whereami := 350;
                              --initialize the values
                              ostatus := NULL;
                              omsgcount := NULL;
                              omsgdata := NULL;
                              ocustacctsiteid := NULL;

                              -- create the customer account site now
                              pcustacctsiterec.cust_account_id :=
                                 ocustaccountid;
                              pcustacctsiterec.party_site_id := opartysiteid;
                              pcustacctsiterec.created_by_module :=
                                 'TCA_V1_API';
                              pcustacctsiterec.orig_system_reference :=
                                 prefix || cust_sites.legacy_customer_number;
                              pcustacctsiterec.org_id := g_org_id; --cust_sites.location||'-'||opartysiteid||nvl(rtrim(cust_sites.address1),cust_sites.legacy_customer_number);

                              /*DFF Name ======== Address Information (Account Site level)*/

                              --Notice to Owner
                              pcustacctsiterec.attribute_category :=
                                 cust_sites.attribute4;               --'Yes';
                              pcustacctsiterec.attribute1 :=
                                 cust_sites.attribute16;

                              IF UPPER (cust_sites.attribute15) = 'YES'
                              THEN
                                 pcustacctsiterec.attribute3 := 'Y'; --Y N NULL Mandatory PO Number
                              ELSIF UPPER (cust_sites.attribute15) = 'NO'
                              THEN
                                 pcustacctsiterec.attribute3 := 'N';
                              END IF;

                              IF cust_sites.attribute5 IS NOT NULL
                              THEN
                                 pcustacctsiterec.attribute5 :=
                                    TO_CHAR (
                                       TO_DATE (cust_sites.attribute5
                                               ,'YYYYMMDD')
                                      ,'YYYY/MM/DD HH24:MI:SS');
                              END IF;

                              whereami := 360;
                              --Mandatory Notes
                              pcustacctsiterec.attribute18 :=
                                 cust_sites.attribute18; --Notice to Owner-Job Total
                              pcustacctsiterec.attribute19 :=
                                 cust_sites.attribute19; --'Notice to Owner-Prelim Notice';  --Notice to Owner-Prelim Notice
                              -- 03/15/2012 CG Prism Number DFF
                              -- 03/16/2012 CG Changed to use the legacy party site number instead
                              -- 04/30/2012 CG Changed the data being loaded into the DFF in case it's a duplicate it's passing
                              -- a suffix we strip on load
                              -- pcustacctsiterec.attribute17:= cust_sites.legacy_party_site_number;
                              l_legacy_party_site := NULL;

                              BEGIN
                                 SELECT SUBSTR (
                                           cust_sites.legacy_party_site_number
                                          ,1
                                          ,DECODE (
                                              INSTR (
                                                 cust_sites.legacy_party_site_number
                                                ,'-'
                                                ,1
                                                ,1)
                                             ,0, LENGTH (
                                                    cust_sites.legacy_party_site_number)
                                             ,  INSTR (
                                                   cust_sites.legacy_party_site_number
                                                  ,'-'
                                                  ,1
                                                  ,1)
                                              - 1))
                                   INTO l_legacy_party_site
                                   FROM DUAL;
                              EXCEPTION
                                 WHEN OTHERS
                                 THEN
                                    l_legacy_party_site :=
                                       cust_sites.legacy_party_site_number;
                              END;

                              pcustacctsiterec.attribute17 :=
                                 l_legacy_party_site;

                              IF cust_sites.attribute20 IS NOT NULL
                              THEN
                                 pcustacctsiterec.attribute20 :=
                                    TO_CHAR (
                                       TO_DATE (cust_sites.attribute20
                                               ,'YYYYMMDD')
                                      ,'YYYY/MM/DD HH24:MI:SS');
                              END IF;

                              whereami := 370;
                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,' Before Calling Create Customer Account Site API ');
                              hz_cust_account_site_v2pub.create_cust_acct_site (
                                 p_init_msg_list        => 'T'
                                ,p_cust_acct_site_rec   => pcustacctsiterec
                                ,x_cust_acct_site_id    => ocustacctsiteid
                                ,x_return_status        => ostatus
                                ,x_msg_count            => omsgcount
                                ,x_msg_data             => omsgdata);

                              IF ostatus <> 'S'
                              THEN
                                 whereami := 380;

                                 --fnd_file.put_line(fnd_file.log,'1117- Create Customer Account Site'||omsgcount);
                                 IF omsgcount > 1
                                 THEN
                                    FOR i IN 1 .. omsgcount
                                    LOOP
                                       p_site_err :=
                                             NVL (p_site_err, ' ')
                                          || SUBSTR (
                                                fnd_msg_pub.get (
                                                   i
                                                  ,p_encoded   => fnd_api.g_false)
                                               ,1
                                               ,255);
                                    END LOOP;

                                    -- Satish U: Moving it outside the Loop
                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,   ' CUSTOMER ACCOUNT SITE RECORD '
                                       || cust_party.account_name
                                       || ' '
                                       || omsgdata
                                       || ' '
                                       || SUBSTR (p_site_err, 1, 255));
                                 ELSE
                                    whereami := 390;
                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,   ' CUSTOMER ACCOUNT SITE RECORD '
                                       || cust_party.account_name
                                       || ' '
                                       || omsgdata);
                                    p_site_err := omsgdata;
                                 END IF;

                                 p_rid := cust_sites.rid;
                                 RAISE e_validation_exception;
                              ELSE
                                 whereami := 400;

                                 --initialize the values
                                 pcustacctsiteuserec := NULL;
                                 ostatus := NULL;
                                 omsgcount := NULL;
                                 omsgdata := NULL;
                                 ocustacctsiteuseid := NULL;

                                 -- 03/16/2012 CG Finding sales rep for both sites
                                 BEGIN
                                    l_sales_person_id := NULL;

                                    SELECT rs.SALESREP_ID
                                      INTO l_sales_person_id
                                      FROM XXWC_AR_HOUSE_ACCOUNTS_XREF ha
                                          ,jtf_rs_salesreps rs
                                     WHERE     ha.Salesrep_ID =
                                                  cust_sites.Salesrep_ID
                                           -- 05/14/2012 CG: MOdified to account for House Account Only
                                           --And    To_Char(ha.EMPLID)  = rs.Salesrep_number
                                           AND TO_CHAR (ha.Salesrep_ID) =
                                                  rs.Salesrep_number
                                           AND ha.Salesrep_ID IS NOT NULL;
                                 EXCEPTION
                                    WHEN OTHERS
                                    THEN
                                       l_sales_person_id := NULL;
                                 END;

                                 -- 07-MAR-2012 : IF  House Account Sales Person is not found, Check Primary Salesrep Exists
                                 IF l_sales_person_id IS NULL
                                 THEN
                                    BEGIN
                                       SELECT salesrep_id
                                         INTO l_sales_person_id
                                         FROM jtf_rs_salesreps
                                        WHERE     1 = 1
                                              AND salesrep_number =
                                                     cust_sites.primary_salesrep_number
                                              AND ROWNUM = 1;
                                    EXCEPTION
                                       WHEN NO_DATA_FOUND
                                       THEN
                                          l_sales_person_id := NULL;
                                          whereami := 430;

                                          IF l_Sales_Person_Id IS NULL
                                          THEN
                                             -- Get Default Sales Person
                                             SELECT salesrep_id
                                               INTO l_sales_person_id
                                               FROM jtf_rs_salesreps
                                              WHERE     name =
                                                           'No Sales Credit'
                                                    AND ROWNUM = 1;
                                          END IF;
                                    END;
                                 END IF;

                                 fnd_file.put_line (
                                    fnd_file.LOG
                                   ,   ' SalesrepID '
                                    || cust_sites.Salesrep_ID
                                    || ' - Sales Rep Num '
                                    || cust_sites.primary_salesrep_number
                                    || ' - Found: '
                                    || l_sales_person_id);

                                 -- 03/16/2012 CG Finding sales rep for both sites

                                 --*********create bill to site use
                                 --if cust_sites.bill_to = 'Y' then
                                 --if upper(cust_sites.site_use_type)='BILL_TO' then
                                 IF UPPER (cust_sites.hds_site_flag) IN
                                       ('YARD', 'SITE', 'MSTR', 'JOB', 'MISC') -- added MISC v.sankar 10/31/2011
                                 THEN
                                    pcustacctsiteuserec := NULL;
                                    pcustomerprofile := NULL;      -- 01/14/09

                                    pcustacctsiteuserec.site_use_code :=
                                       'BILL_TO';
                                    pcustacctsiteuserec.cust_acct_site_id :=
                                       ocustacctsiteid;
                                    --thomas guide Page
                                    pcustacctsiteuserec.attribute3 :=
                                       cust_sites.attribute11;
                                    pcustacctsiteuserec.gsa_indicator :=
                                       cust_sites.gsa_indicator;
                                    pcustacctsiteuserec.primary_salesrep_id :=
                                       l_sales_person_id;
                                    pcustacctsiteuserec.created_by_module :=
                                       'TCA_V1_API';

                                    IF UPPER (cust_sites.hds_site_flag) IN
                                          ('MSTR', 'MISC') -- added MISC v.sankar 10/31/2011
                                    THEN
                                       pcustacctsiteuserec.primary_flag := 'Y';
                                    ELSE
                                       pcustacctsiteuserec.primary_flag := 'N';
                                    END IF;

                                    IF     cust_sites.job_name IS NOT NULL
                                       AND cust_sites.job_number IS NOT NULL
                                    THEN
                                       pcustacctsiteuserec.location :=
                                          SUBSTR (
                                                UPPER (cust_sites.job_name)
                                             || '-'
                                             || cust_sites.job_number
                                            ,1
                                            ,40);
                                    ELSIF     cust_sites.legacy_party_site_name
                                                 IS NOT NULL
                                          AND cust_sites.legacy_customer_number
                                                 IS NOT NULL
                                    THEN
                                       pcustacctsiteuserec.location :=
                                          SUBSTR (
                                                UPPER (
                                                   cust_sites.legacy_party_site_name)
                                             || '-'
                                             || UPPER (
                                                   cust_sites.legacy_customer_number)
                                            ,1
                                            ,40);
                                    ELSIF     cust_sites.legacy_party_site_name
                                                 IS NULL
                                          AND cust_sites.legacy_customer_number
                                                 IS NOT NULL
                                    THEN
                                       IF cust_sites.legacy_site_type = 1000
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'MSTR-'
                                             || UPPER (
                                                   cust_sites.legacy_customer_number);
                                       ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                             AND 999
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'YARD-'
                                             || UPPER (
                                                   cust_sites.legacy_customer_number);
                                       ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                             AND 989
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'JOB-'
                                             || UPPER (
                                                   cust_sites.legacy_customer_number);
                                       END IF;
                                    ELSIF     cust_sites.legacy_party_site_name
                                                 IS NOT NULL
                                          AND cust_sites.legacy_customer_number
                                                 IS NULL
                                    THEN
                                       pcustacctsiteuserec.location :=
                                          SUBSTR (
                                                UPPER (
                                                   cust_sites.legacy_party_site_name)
                                             || '-'
                                             || LPAD (p_loc_count, 2, '0')
                                            ,1
                                            ,40);
                                    ELSIF     cust_sites.legacy_party_site_name
                                                 IS NULL
                                          AND cust_sites.legacy_customer_number
                                                 IS NULL
                                    THEN
                                       IF cust_sites.legacy_site_type = 1000
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'MSTR-'
                                             || LPAD (p_mstr_loc_count
                                                     ,2
                                                     ,'0');
                                          p_mstr_loc_count :=
                                             p_mstr_loc_count + 1;
                                       ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                             AND 999
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'YARD-'
                                             || LPAD (p_yard_loc_count
                                                     ,2
                                                     ,'0');
                                       ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                             AND 989
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'JOB-'
                                             || LPAD (p_job_loc_count
                                                     ,2
                                                     ,'0');
                                       END IF;
                                    END IF;

                                    --Customer Site Classification
                                    IF UPPER (cust_sites.hds_site_flag) =
                                          'MSTR'
                                    THEN
                                       pcustacctsiteuserec.attribute1 :=
                                          'MSTR';
                                       pcustacctsiteuserec.attribute5 :=
                                          'MSTR';
                                    ELSIF UPPER (cust_sites.hds_site_flag) =
                                             'YARD'
                                    THEN
                                       pcustacctsiteuserec.attribute1 :=
                                          'YARD';
                                       pcustacctsiteuserec.attribute5 :=
                                          'YARD';
                                    ELSIF UPPER (cust_sites.hds_site_flag) =
                                             'MISC'
                                    THEN     -- added MISC v.sankar 10/31/2011
                                       pcustacctsiteuserec.attribute1 :=
                                          'MISC';
                                       pcustacctsiteuserec.attribute5 :=
                                          'MISC';
                                    ELSIF UPPER (cust_sites.hds_site_flag) IN
                                             ('SITE', 'JOB')
                                    THEN
                                       pcustacctsiteuserec.attribute1 := 'JOB';
                                       pcustacctsiteuserec.attribute5 := 'JOB';
                                    END IF;

                                    IF UPPER (cust_sites.freight_term) =
                                          'FREIGHT EXEMPT'
                                    THEN
                                       pcustacctsiteuserec.freight_term :=
                                          'EXEMPT';
                                    ELSE
                                       pcustacctsiteuserec.freight_term :=
                                          NULL;
                                    END IF;

                                    whereami := 410;

                                    pcustomerprofile.profile_class_id :=
                                       l_profile_id; --setting at cust account level
                                    pcustomerprofile.collector_id :=
                                       p_collector_id; --p_collector_id value is setting at account level
                                    pcustomerprofile.credit_analyst_id :=
                                       p_credit_analyst_id; --p_credit_analyst_id value is setting at account level
                                    pcustomerprofile.credit_classification :=
                                       p_credit_classification; --p_credit_classification alue is setting at account level
                                    pcustomerprofile.standard_terms :=
                                       p_payment_term_id; --Set at account level
                                    pcustomerprofile.credit_hold :=
                                       cust_sites.credit_hold; --defaulted from account leve profile

                                    whereami := 415;

                                    /*DFF Name ======== Customer Credit Profile Information (Site level profile)*/

                                    BEGIN
                                       SELECT oracle_code
                                         INTO pcustomerprofile.attribute2 --Invoice Remit To Address Code
                                         FROM xxwc_remit_to_addr_xref x
                                        WHERE x.prism_code =
                                                 cust_sites.attribute2;
                                    EXCEPTION
                                       WHEN NO_DATA_FOUND
                                       THEN
                                          pcustomerprofile.attribute2 := 2;
                                    END;

                                    IF UPPER (cust_sites.hds_site_flag) IN
                                          ('MSTR', 'MISC')
                                    THEN
                                       IF     cust_acc.attribute3 IN
                                                 ('Y', 'N', 'C')
                                          AND cust_acc.send_statements IN
                                                 ('Y', 'C')
                                       THEN
                                          IF cust_acc.attribute3 = 'C' --IN ('Y', 'N')
                                          THEN
                                             pcustomerprofile.attribute3 :=
                                                'Y';
                                          ELSE
                                             pcustomerprofile.attribute3 :=
                                                'N';
                                          END IF;
                                       END IF;

                                       --defaulted from account level
                                       IF cust_acc.send_statements IN
                                             ('Y', 'N', 'C')
                                       THEN
                                          IF cust_acc.send_statements IN
                                                ('Y', 'C')
                                          THEN
                                             pcustomerprofile.send_statements :=
                                                'Y';
                                          ELSE
                                             pcustomerprofile.send_statements :=
                                                'N';
                                          END IF;
                                       ELSE
                                          pcustomerprofile.send_statements :=
                                             'N';
                                       END IF;

                                       IF cust_acc.send_statements NOT IN
                                             ('Y', 'N', 'C')
                                       THEN
                                          pcustomerprofile.credit_balance_statements :=
                                             'N';
                                       ELSIF cust_acc.send_statements IN
                                                ('Y', 'C', 'N')
                                       THEN
                                          IF cust_acc.credit_balance_statements IN
                                                ('Y', 'C')
                                          THEN
                                             pcustomerprofile.credit_balance_statements :=
                                                'Y';
                                          ELSIF cust_acc.credit_balance_statements =
                                                   'N'
                                          THEN
                                             pcustomerprofile.credit_balance_statements :=
                                                'N';
                                          END IF;
                                       END IF;
                                    END IF;

                                    --account status . added by v.sankar on 12/14/2011
                                    BEGIN
                                       whereami := 420;

                                       SELECT lookup_code
                                         INTO pcustomerprofile.account_status
                                         FROM xxwc_account_status_xref a
                                             ,ar_lookups b
                                        WHERE     a.account_status =
                                                     b.meaning
                                              AND b.lookup_type =
                                                     'ACCOUNT_STATUS'
                                              AND a.prism_credit_group =
                                                     cust_sites.credit_analyst_name;
                                    EXCEPTION
                                       WHEN NO_DATA_FOUND
                                       THEN
                                          pcustomerprofile.account_status :=
                                             NULL;
                                    END;

                                    whereami := 425;

                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,' Before Calling Create Customer Site USe API ');

                                    hz_cust_account_site_v2pub.create_cust_site_use (
                                       p_init_msg_list          => 'T'
                                      ,p_cust_site_use_rec      => pcustacctsiteuserec
                                      ,p_customer_profile_rec   => pcustomerprofile
                                      ,p_create_profile         => 'T'
                                      ,p_create_profile_amt     => 'T'
                                      ,x_site_use_id            => ocustacctsiteuseid
                                      ,x_return_status          => ostatus
                                      ,x_msg_count              => omsgcount
                                      ,x_msg_data               => omsgdata);

                                    IF UPPER (cust_sites.hds_site_flag) IN
                                          ('YARD'
                                          ,'MSTR'
                                          ,'MISC'
                                          ,'JOB'
                                          ,'SITE')
                                    THEN
                                       pbilltositeuseid := ocustacctsiteuseid;
                                    END IF;

                                    pcustacctid := ocustaccountid;

                                    IF ostatus <> 'S'
                                    THEN
                                       whereami := 440;
                                       l_proceed := 'N';

                                       --fnd_file.put_line ( fnd_file.LOG  , ' 1120-Create SIte USe TYpe API errored out: Msg Count' || omsgcount);
                                       IF omsgcount > 1
                                       THEN
                                          FOR i IN 1 .. omsgcount
                                          LOOP
                                             p_site_err :=
                                                   p_site_err
                                                || SUBSTR (
                                                      fnd_msg_pub.get (
                                                         p_encoded => fnd_api.g_false)
                                                     ,1
                                                     ,255);
                                          END LOOP;

                                          -- Satish U: 11/01/11 Moving Outside the Loop
                                          fnd_file.put_line (
                                             fnd_file.LOG
                                            ,   'CUSTOMER BILL_TO SITE USE RECORD '
                                             || SUBSTR (p_site_err, 1, 255));
                                       ELSE
                                          whereami := 450;
                                          fnd_file.put_line (
                                             fnd_file.LOG
                                            ,   'CUSTOMER BILL_TO SITE USE RECORD '
                                             || omsgdata);
                                          p_site_err := omsgdata;
                                       END IF;

                                       p_rid := cust_sites.rid;
                                       RAISE e_validation_exception;
                                    ELSE
                                       whereami := 450;

                                       IF    NVL (cust_sites.trx_credit_limit
                                                 ,0) != 0
                                          OR NVL (
                                                cust_sites.overall_credit_limit
                                               ,0) != 0
                                       THEN
                                          pcustproamtrec := NULL;

                                          BEGIN
                                             SELECT cust_account_profile_id
                                               INTO osprofile_id
                                               FROM hz_customer_profiles
                                              WHERE     cust_account_id =
                                                           ocustaccountid
                                                    AND site_use_id =
                                                           ocustacctsiteuseid;
                                          EXCEPTION
                                             WHEN NO_DATA_FOUND
                                             THEN
                                                whereami := 460;
                                                osprofile_id := NULL;
                                                fnd_file.put_line (
                                                   fnd_file.LOG
                                                  ,   'No Customer Profile Found for site use id '
                                                   || ocustacctsiteuseid);
                                          END;

                                          IF osprofile_id IS NOT NULL
                                          THEN
                                             BEGIN
                                                whereami := 470;

                                                SELECT cust_acct_profile_amt_id
                                                      ,object_version_number
                                                  INTO ocustacctprofileamtid
                                                      ,p_object_version_number
                                                  FROM hz_cust_profile_amts
                                                 WHERE     cust_account_profile_id =
                                                              osprofile_id
                                                       AND currency_code =
                                                              'USD';
                                             EXCEPTION
                                                WHEN NO_DATA_FOUND
                                                THEN
                                                   ocustacctprofileamtid :=
                                                      NULL;
                                                   p_object_version_number :=
                                                      NULL;
                                             END;

                                             IF ocustacctprofileamtid
                                                   IS NOT NULL
                                             THEN
                                                whereami := 480;

                                                --Update amount profile
                                                pcustproamtrec.cust_acct_profile_amt_id :=
                                                   ocustacctprofileamtid;
                                                pcustproamtrec.trx_credit_limit :=
                                                   cust_sites.trx_credit_limit;
                                                pcustproamtrec.overall_credit_limit :=
                                                   cust_sites.overall_credit_limit;
                                                pcustproamtrec.created_by_module :=
                                                   'TCA_V1_API';

                                                fnd_file.put_line (
                                                   fnd_file.LOG
                                                  ,' Before Calling Update Profile Amt API ');
                                                hz_customer_profile_v2pub.update_cust_profile_amt (
                                                   p_init_msg_list           => 'T'
                                                  ,p_cust_profile_amt_rec    => pcustproamtrec
                                                  ,p_object_version_number   => p_object_version_number
                                                  ,x_return_status           => ostatus
                                                  ,x_msg_count               => omsgcount
                                                  ,x_msg_data                => omsgdata);

                                                IF ostatus <> 'S'
                                                THEN
                                                   whereami := 490;

                                                   --fnd_file.put_line ( fnd_file.LOG  , ' 1121 -Update Profile Amt API Errored Out : Msg Count' || omsgcount);
                                                   IF omsgcount > 1
                                                   THEN
                                                      FOR i IN 1 .. omsgcount
                                                      LOOP
                                                         p_site_err :=
                                                               NVL (
                                                                  p_site_err
                                                                 ,' ')
                                                            || SUBSTR (
                                                                  fnd_msg_pub.get (
                                                                     i
                                                                    ,p_encoded   => fnd_api.g_false)
                                                                 ,1
                                                                 ,255);
                                                      END LOOP;

                                                      -- Satish U: 11/01/11 Moving Code Outside the Loop
                                                      fnd_file.put_line (
                                                         fnd_file.LOG
                                                        ,   ' CUSTOMER Site Amout Profile Updation '
                                                         || SUBSTR (
                                                               p_site_err
                                                              ,1
                                                              ,255));
                                                   ELSE
                                                      fnd_file.put_line (
                                                         fnd_file.LOG
                                                        ,   ' CUSTOMER Site Amount Profile Updation '
                                                         || omsgdata);
                                                      p_site_err := omsgdata;
                                                   END IF;

                                                   p_rid := cust_sites.rid;
                                                   RAISE e_validation_exception;
                                                END IF;
                                             ELSE
                                                whereami := 500;
                                                --Create Profile Amount
                                                pcustproamtrec.cust_account_profile_id :=
                                                   osprofile_id;
                                                pcustproamtrec.cust_account_id :=
                                                   ocustaccountid;
                                                pcustproamtrec.site_use_id :=
                                                   ocustacctsiteuseid;
                                                pcustproamtrec.currency_code :=
                                                   'USD';
                                                pcustproamtrec.trx_credit_limit :=
                                                   cust_sites.trx_credit_limit;
                                                pcustproamtrec.overall_credit_limit :=
                                                   cust_sites.overall_credit_limit;
                                                pcustproamtrec.created_by_module :=
                                                   'TCA_V1_API';

                                                fnd_file.put_line (
                                                   fnd_file.LOG
                                                  ,' Before Calling Create Profile Amt API ');

                                                hz_customer_profile_v2pub.create_cust_profile_amt (
                                                   p_init_msg_list              => 'T'
                                                  ,p_check_foreign_key          => 'T'
                                                  ,p_cust_profile_amt_rec       => pcustproamtrec
                                                  ,x_cust_acct_profile_amt_id   => ocustacctprofileamtid
                                                  ,x_return_status              => ostatus
                                                  ,x_msg_count                  => omsgcount
                                                  ,x_msg_data                   => omsgdata);

                                                IF ostatus <> 'S'
                                                THEN
                                                   whereami := 510;

                                                   --fnd_file.put_line ( fnd_file.LOG  , ' Create Profile Amt API Errored Out- : Msg Count' || omsgcount);
                                                   IF omsgcount > 1
                                                   THEN
                                                      FOR i IN 1 .. omsgcount
                                                      LOOP
                                                         p_site_err :=
                                                               NVL (
                                                                  p_site_err
                                                                 ,' ')
                                                            || SUBSTR (
                                                                  fnd_msg_pub.get (
                                                                     i
                                                                    ,p_encoded   => fnd_api.g_false)
                                                                 ,1
                                                                 ,255);
                                                      END LOOP;

                                                      -- SatishU: 11/02/11 : MOved the code out from LOOP
                                                      fnd_file.put_line (
                                                         fnd_file.LOG
                                                        ,   ' CUSTOMER Site Use Level Amout Profile Creation '
                                                         || SUBSTR (
                                                               p_site_err
                                                              ,1
                                                              ,255));
                                                   ELSE
                                                      fnd_file.put_line (
                                                         fnd_file.LOG
                                                        ,   ' CUSTOMER Site Use Level Amount Profile Creation '
                                                         || omsgdata);
                                                      p_site_err := omsgdata;
                                                   END IF;

                                                   p_rid := cust_sites.rid;
                                                   RAISE e_validation_exception;
                                                END IF;
                                             END IF;
                                          END IF;
                                       END IF;           --site amount profile
                                    END IF;

                                    l_proceed := 'Y';
                                 END IF;                       -- bill to site

                                 whereami := 520;

                                 --*********create ship to site use
                                 IF UPPER (cust_sites.hds_site_flag) IN
                                       ('YARD', 'SITE', 'JOB', 'MISC', 'SHIP')
                                 THEN
                                    pcustacctsiteuserec := NULL;
                                    pcustomerprofile := NULL;

                                    pcustacctsiteuserec.site_use_code :=
                                       'SHIP_TO';
                                    pcustacctsiteuserec.bill_to_site_use_id :=
                                       pbilltositeuseid;

                                    IF     UPPER (cust_sites.hds_site_flag) IN
                                              ('YARD', 'MISC')
                                       AND lv_primary_ship_to_flag IS NULL
                                    THEN
                                       pcustacctsiteuserec.primary_flag := 'Y';
                                       lv_primary_ship_to_flag := 'Y';
                                    ELSE
                                       pcustacctsiteuserec.primary_flag := 'N';
                                    END IF;

                                    pcustacctsiteuserec.cust_acct_site_id :=
                                       ocustacctsiteid;
                                    --thomas guide Page
                                    pcustacctsiteuserec.attribute3 :=
                                       cust_sites.attribute11;
                                    pcustacctsiteuserec.created_by_module :=
                                       'TCA_V1_API';
                                    pcustacctsiteuserec.primary_salesrep_id :=
                                       l_sales_person_id;
                                    pcustacctsiteuserec.gsa_indicator :=
                                       cust_sites.gsa_indicator;

                                    IF UPPER (cust_sites.freight_term) =
                                          'FREIGHT EXEMPT'
                                    THEN
                                       pcustacctsiteuserec.freight_term :=
                                          'EXEMPT';
                                    ELSE
                                       pcustacctsiteuserec.freight_term :=
                                          NULL;
                                    END IF;

                                    --populate location
                                    IF     cust_sites.job_name IS NOT NULL
                                       AND cust_sites.job_number IS NOT NULL
                                    THEN
                                       pcustacctsiteuserec.location :=
                                          SUBSTR (
                                                UPPER (cust_sites.job_name)
                                             || '-'
                                             || cust_sites.job_number
                                            ,1
                                            ,40);
                                    ELSIF     cust_sites.legacy_party_site_name
                                                 IS NOT NULL
                                          AND cust_sites.legacy_customer_number
                                                 IS NOT NULL
                                    THEN
                                       pcustacctsiteuserec.location :=
                                          SUBSTR (
                                                UPPER (
                                                   cust_sites.legacy_party_site_name)
                                             || '-'
                                             || UPPER (
                                                   cust_sites.legacy_customer_number)
                                            ,1
                                            ,40);
                                    ELSIF     cust_sites.legacy_party_site_name
                                                 IS NULL
                                          AND cust_sites.legacy_customer_number
                                                 IS NOT NULL
                                    THEN
                                       IF cust_sites.legacy_site_type = 1000
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'MSTR-'
                                             || UPPER (
                                                   cust_sites.legacy_customer_number);
                                       ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                             AND 999
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'YARD-'
                                             || UPPER (
                                                   cust_sites.legacy_customer_number);
                                       ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                             AND 989
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'JOB-'
                                             || UPPER (
                                                   cust_sites.legacy_customer_number);
                                       END IF;
                                    ELSIF     cust_sites.legacy_party_site_name
                                                 IS NOT NULL
                                          AND cust_sites.legacy_customer_number
                                                 IS NULL
                                    THEN
                                       pcustacctsiteuserec.location :=
                                          SUBSTR (
                                                UPPER (
                                                   cust_sites.legacy_party_site_name)
                                             || '-'
                                             || LPAD (p_loc_count, 2, '0')
                                            ,1
                                            ,40);
                                       p_loc_count := p_loc_count + 1;
                                    ELSIF     cust_sites.legacy_party_site_name
                                                 IS NULL
                                          AND cust_sites.legacy_customer_number
                                                 IS NULL
                                    THEN
                                       IF cust_sites.legacy_site_type = 1000
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'MSTR-'
                                             || LPAD (p_mstr_loc_count
                                                     ,2
                                                     ,'0');
                                       ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                             AND 999
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'YARD-'
                                             || LPAD (p_yard_loc_count
                                                     ,2
                                                     ,'0');
                                          p_yard_loc_count :=
                                             p_yard_loc_count + 1;
                                       ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                             AND 989
                                       THEN
                                          pcustacctsiteuserec.location :=
                                                'JOB-'
                                             || LPAD (p_job_loc_count
                                                     ,2
                                                     ,'0');
                                          p_job_loc_count :=
                                             p_job_loc_count + 1;
                                       END IF;
                                    END IF;

                                    whereami := 530;

                                    --Customer Site Classification
                                    IF UPPER (cust_sites.hds_site_flag) =
                                          'MSTR'
                                    THEN
                                       pcustacctsiteuserec.attribute1 :=
                                          'MSTR';
                                       pcustacctsiteuserec.attribute5 :=
                                          'MSTR';
                                    ELSIF UPPER (cust_sites.hds_site_flag) =
                                             'YARD'
                                    THEN
                                       pcustacctsiteuserec.attribute1 :=
                                          'YARD';
                                       pcustacctsiteuserec.attribute5 :=
                                          'YARD';
                                    ELSIF UPPER (cust_sites.hds_site_flag) =
                                             'MISC'
                                    THEN
                                       pcustacctsiteuserec.attribute1 :=
                                          'MISC';
                                       pcustacctsiteuserec.attribute5 :=
                                          'MISC';
                                    ELSIF UPPER (cust_sites.hds_site_flag) IN
                                             ('SITE', 'JOB')
                                    THEN
                                       pcustacctsiteuserec.attribute1 := 'JOB';
                                       pcustacctsiteuserec.attribute5 := 'JOB';
                                    END IF;

                                    /*DFF Name ========
                                    Site Use Information */

                                    --pcustomerprofile.attribute2:=2; --Sieban Number(NO CONVERSION) No Conversion needed - Future Use Only
                                    --pcustacctsiteuserec.attribute3:=cust_sites.attribute1; --Thomas Guide Code'
                                    --pcustacctsiteuserec.attribute9:=11  --Government Funded(NO CONVERSION)Future Use Only (Default to No) (Yes/No Values)

                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,' Before Calling Create Customer Site Use  API ');

                                    hz_cust_account_site_v2pub.create_cust_site_use (
                                       p_init_msg_list          => 'T'
                                      ,p_cust_site_use_rec      => pcustacctsiteuserec
                                      ,p_customer_profile_rec   => pcustomerprofile
                                      ,p_create_profile         => 'F'
                                      ,p_create_profile_amt     => 'F'
                                      ,x_site_use_id            => ocustacctsiteuseid
                                      ,x_return_status          => ostatus
                                      ,x_msg_count              => omsgcount
                                      ,x_msg_data               => omsgdata);
                                    --pbilltositeuseid := ocustacctsiteuseid ;
                                    pcustacctid := ocustaccountid;

                                    IF ostatus <> 'S'
                                    THEN
                                       whereami := 540;
                                       l_proceed := 'N';
                                       fnd_file.put_line (
                                          fnd_file.LOG
                                         ,   ' Create Customer Site Use API Errored Out- Msg Count :'
                                          || omsgcount);

                                       IF omsgcount > 1
                                       THEN
                                          FOR i IN 1 .. omsgcount
                                          LOOP
                                             p_site_err :=
                                                   NVL (p_site_err, ' ')
                                                || SUBSTR (
                                                      fnd_msg_pub.get (
                                                         i
                                                        ,p_encoded   => fnd_api.g_false)
                                                     ,1
                                                     ,255);
                                          END LOOP;

                                          -- Satish U: 11/02/11 : Moved code out of the loop
                                          fnd_file.put_line (
                                             fnd_file.LOG
                                            ,   'CUSTOMER SHIP_TO SITE USE RECORD '
                                             || SUBSTR (p_site_err, 1, 255));
                                       ELSE
                                          whereami := 550;
                                          fnd_file.put_line (
                                             fnd_file.LOG
                                            ,   'CUSTOMER SHIP_TO SITE USE RECORD '
                                             || omsgdata
                                             || '-'
                                             || cust_party.account_name);
                                          p_site_err := omsgdata;
                                       END IF;

                                       p_rid := cust_sites.rid;
                                       RAISE e_validation_exception;
                                    END IF;


                                    l_proceed := 'Y';
                                 END IF;                       -- ship to site
                              END IF;                    -- party site account

                              --create party site level communication
                              whereami := 550;

                              FOR rec_site_contact
                                 IN get_site_contact (
                                       cust_sites.orig_system_reference
                                      ,cust_sites.legacy_customer_number)
                              LOOP
                                 pop_org_communication (
                                    'SITE'
                                   ,opartysiteid
                                   ,rec_site_contact.phone_area_code
                                   ,rec_site_contact.phone_number
                                   ,rec_site_contact.fax_area_code
                                   ,rec_site_contact.fax_number
                                   ,p_result
                                   ,omsgdata);

                                 IF p_result = 'E'
                                 THEN
                                    whereami := 560;
                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,   'ERROR IN PARTY SITE LEVEL COMMUNICATION '
                                       || TO_CHAR (whereami));
                                    p_con_err := omsgdata;
                                    p_rid := rec_site_contact.rid;
                                    RAISE e_validation_exception;
                                 END IF;

                                 -- contacts for MISC billto. since MISC site is created twice, we need to attach
                                 -- the same contact at bill to and ship to
                                 IF ln_partysiteid_misc_bill_to IS NOT NULL
                                 THEN
                                    pop_org_communication (
                                       'SITE'
                                      ,ln_partysiteid_misc_bill_to
                                      ,rec_site_contact.phone_area_code
                                      ,rec_site_contact.phone_number
                                      ,rec_site_contact.fax_area_code
                                      ,rec_site_contact.fax_number
                                      ,p_result
                                      ,omsgdata);

                                    IF p_result = 'E'
                                    THEN
                                       whereami := 561;
                                       fnd_file.put_line (
                                          fnd_file.LOG
                                         ,   'ERROR IN PARTY SITE LEVEL COMMUNICATION FOR MISC BILLTO'
                                          || TO_CHAR (whereami));
                                       p_con_err := omsgdata;
                                       p_rid := rec_site_contact.rid;
                                       RAISE e_validation_exception;
                                    END IF;
                                 END IF;

                                 IF cust_sites.hds_site_flag IN
                                       ('MISC', 'MSTR')
                                 THEN
                                    pop_org_communication (
                                       'PARTY'
                                      ,opartyid
                                      ,rec_site_contact.phone_area_code
                                      ,rec_site_contact.phone_number
                                      ,rec_site_contact.fax_area_code
                                      ,rec_site_contact.fax_number
                                      ,p_result
                                      ,omsgdata);

                                    IF p_result = 'E'
                                    THEN
                                       whereami := 562;
                                       fnd_file.put_line (
                                          fnd_file.LOG
                                         ,   'ERROR IN PARTY LEVEL COMMUNICATION FOR MISC OR MSTR'
                                          || TO_CHAR (whereami));
                                       p_con_err := omsgdata;
                                       p_rid := rec_site_contact.rid;
                                       RAISE e_validation_exception;
                                    END IF;
                                 END IF;

                                 UPDATE XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
                                    SET process_status = 'P'
                                       ,last_update_date = SYSDATE
                                       ,processing_date = SYSDATE
                                  WHERE ROWID = rec_site_contact.rid;
                              END LOOP;                        -- Contact Loop
                           END IF;                               -- party site
                        END IF;                                     --location

                        UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                           SET process_status = 'P'
                              ,last_update_date = SYSDATE
                              ,processing_date = SYSDATE
                         WHERE ROWID = cust_sites.rid;
                     END LOOP;                                    --Sites Loop

                     UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                        SET process_status = 'P'
                           ,last_update_date = SYSDATE
                           ,processing_date = SYSDATE
                      WHERE ROWID = cust_acc.rid;
                  END IF;                                   -- for the account
               END LOOP;                                        --Account Loop

               --create party level communication
               whereami := 569;
            END IF;                                           -- for the party


            IF    cust_party.cod_comment IS NOT NULL
               OR cust_party.keyword IS NOT NULL
            THEN
               pop_org_web_contact ('PARTY'
                                   ,opartyid
                                   ,cust_party.cod_comment
                                   ,cust_party.keyword
                                   ,p_result
                                   ,omsgdata);

               IF p_result = 'E'
               THEN
                  whereami := 580;
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,'ERROR IN PARTY LEVEL WEB CONTACT' || TO_CHAR (whereami));
                  p_con_err := omsgdata;
                  p_rid := cust_party.rid;
                  RAISE e_validation_exception;
               END IF;
            END IF;

            UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
               SET process_status = 'P'
                  ,last_update_date = SYSDATE
                  ,processing_date = SYSDATE
             WHERE orig_system_reference = cust_party.orig_system_reference;
         EXCEPTION
            WHEN e_validation_exception
            THEN
               --whereami    := 590;

               v_err := 'Y';
               ROLLBACK TO customer_record;
               upd_stagin_table_err (p_cus_err
                                    ,p_site_err
                                    ,p_con_err
                                    ,p_rid);
               p_cus_err := NULL;
               p_site_err := NULL;
               p_con_err := NULL;
               upd_stagin_table (cust_party.orig_system_reference);
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'e_Validation_Exception IN PROCEDURE for customer: '
                  || cust_party.orig_system_reference
                  || '---'
                  || l_api_name
                  || '---'
                  || TO_CHAR (whereami));
            WHEN OTHERS
            THEN
               -- whereami  := 600;
               ROLLBACK TO customer_record;
               v_err := 'Y';
               --SatishU: 11/01/11  Added Following SQL Statement
               upd_stagin_table_err (p_cus_err
                                    ,p_site_err
                                    ,p_con_err
                                    ,p_rid);
               p_cus_err := NULL;
               p_site_err := NULL;
               p_con_err := NULL;
               upd_stagin_table (cust_party.orig_system_reference);
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'When Others Error in PROCEDURE for customer: '
                  || cust_party.orig_system_reference
                  || '---'
                  || l_api_name
                  || SQLERRM
                  || '---'
                  || TO_CHAR (whereami));
         END;

         IF l_rec_count >= c_max_commit_count
         THEN
            COMMIT;                               --Commit the single customer
            l_rec_count := 0;
         END IF;
      END LOOP;

      COMMIT; -- Commit after the program completes.          --close get_party;

      -- Load Sites only
      --set the yard counter
      p_yard_counter := 1;
      p_mstr_counter := 1;
      p_loc_count := 1;
      p_mstr_loc_count := 1;
      p_yard_loc_count := 1;
      p_job_loc_count := 1;

      --Open Sites
      fnd_file.put_line (fnd_file.LOG, 'Creating Job Sites...');

      FOR cust_sites IN get_job_sites
      LOOP
         --fnd_file.put_line (fnd_file.output,'CREATING SITE ' || cust_sites.location || 'FOR CUSTOMER ' || cust_party.account_name);

         SAVEPOINT customer_record2;

         BEGIN
            fnd_file.put_line (fnd_file.LOG
                              ,'Site: ' || cust_sites.orig_system_reference);
            plocationrec := NULL;
            ppartysiterec := NULL;
            pcustacctsiterec := NULL;
            pcustacctsiteuserec := NULL;
            pcustomerprofile := NULL;
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            ln_partysiteid_misc_bill_to := NULL;

            l_count := 0;
            olocationid := NULL;
            ostatus := NULL;

            BEGIN
               SELECT location_id
                 INTO olocationid
                 FROM hz_locations
                WHERE location_id = cust_sites.location_id;

               l_count := 1;
               --olocationid := Add_Rec.Location_ID;
               ostatus := 'S';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_count := 0;
                  ostatus := NULL;
            END;


            IF l_count = 0
            THEN
               -- location creation
               IF NVL (cust_sites.country, 'USA') IN ('USA', 'US')
               THEN
                  plocationrec.country := 'US';
               ELSE
                  plocationrec.country := cust_sites.country;
               END IF;

               --SatishU : 10/26/2011
               plocationrec.location_id := cust_sites.location_id; -- added MISC v.sankar 10/31/2011
               plocationrec.postal_code := cust_sites.postal_code;
               -- 03/26/2012 CG
               plocationrec.address1 :=
                  NVL (cust_sites.address1
                      ,NVL (cust_sites.address2, 'NO ADDRESS FOUND'));
               plocationrec.address2 := cust_sites.address2;
               plocationrec.address3 := cust_sites.address3;
               plocationrec.address4 := cust_sites.address4;
               plocationrec.state := cust_sites.state;         -- is mandatory
               plocationrec.city := cust_sites.city;
               plocationrec.county := cust_sites.county; --nvl(c2.county,c2.city); --for the time being (will be updated for sales tax later)
               plocationrec.created_by_module := 'TCA_V1_API';
               -- Added  Attribute6 PRISM Customer#  Satish U : 02/21/2012
               plocationrec.attribute6 := cust_sites.orig_system_reference;

               whereami := 280;
               fnd_file.put_line (fnd_file.LOG
                                 ,' Before Calling Create Location API ');
               hz_location_v2pub.create_location (
                  p_init_msg_list   => 'T'
                 ,p_location_rec    => plocationrec
                 ,x_location_id     => olocationid
                 ,x_return_status   => ostatus
                 ,x_msg_count       => omsgcount
                 ,x_msg_data        => omsgdata);
            END IF;

            IF ostatus <> 'S'
            THEN
               whereami := 290;

               IF omsgcount > 1
               THEN
                  FOR i IN 1 .. omsgcount
                  LOOP
                     p_site_err :=
                           NVL (p_site_err, ' ')
                        || SUBSTR (
                              fnd_msg_pub.get (i
                                              ,p_encoded   => fnd_api.g_false)
                             ,1
                             ,255);
                  END LOOP;

                  --Satish U: Moving Outside the Loop
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   ' CUSTOMER LOCATION RECORD '
                     || SUBSTR (p_site_err, 1, 255));
               ELSE
                  whereami := 300;

                  fnd_file.put_line (
                     fnd_file.LOG
                    ,   ' CUSTOMER LOCATION RECORD '
                     || omsgdata
                     || '-'
                     || cust_sites.legacy_customer_number
                     || '-'
                     || cust_sites.country
                     || '-'
                     || cust_sites.address1
                     || '-'
                     || cust_sites.address2);
                  p_site_err := omsgdata;
               END IF;

               p_rid := cust_sites.rid;
               whereami := 310;
               RAISE e_validation_exception;
            ELSE
               --initialize the values
               ostatus := NULL;
               omsgcount := NULL;
               omsgdata := NULL;
               opartysiteid := NULL;
               opartysiteno := NULL;
               opartyid := NULL;
               ocustaccountid := NULL;

               BEGIN
                  SELECT cus.cust_account_id, cus.party_id
                    INTO ocustaccountid, opartyid
                    FROM hz_cust_accounts cus
                   WHERE                 -- 03/26/2012 CG: Changed to use DFF6
                         -- cus.orig_system_reference = g_parameter_prefix || cust_sites.orig_system_reference;
                         NVL (cus.attribute6, 'UNKNOWN') =
                            cust_sites.orig_system_reference;
               --AND cus.org_id = g_org_id;

               EXCEPTION
                  WHEN OTHERS
                  THEN
                     RAISE;
               --- AND created_by_module <> 'ap_suppliers_api')
               END;


               -- create a party site now
               ppartysiterec.party_id := opartyid;
               ppartysiterec.location_id := olocationid;
               -- 04/02/2012 CGonzalez: Commented to allow autonumbering
               ppartysiterec.party_site_number :=
                  SUBSTR (cust_sites.legacy_party_site_number, 1, 25);
               ppartysiterec.orig_system_reference :=
                  prefix || cust_sites.orig_system_reference;
               ppartysiterec.created_by_module := 'TCA_V1_API';

               IF cust_sites.hds_site_flag = 'MISC'
               THEN
                  ppartysiterec.party_site_name :=
                        cust_sites.hds_site_flag
                     || '-'
                     || cust_sites.legacy_customer_number;
               ELSIF cust_sites.legacy_site_type = 1000
               THEN
                  ppartysiterec.party_site_name :=
                        UPPER (cust_sites.hds_site_flag)
                     || '-'
                     || LPAD (p_mstr_counter, 2, '0');
                  p_mstr_counter := p_mstr_counter + 1;
               ELSIF cust_sites.legacy_site_type BETWEEN 990 AND 999
               THEN
                  ppartysiterec.party_site_name :=
                     'YARD' || '-' || LPAD (p_yard_counter, 2, '0');
                  p_yard_counter := p_yard_counter + 1;
               ELSIF cust_sites.legacy_site_type BETWEEN 001 AND 989
               THEN
                  ppartysiterec.party_site_name :=
                        cust_sites.hds_site_flag
                     || '-'
                     || cust_sites.legacy_customer_number;
               END IF;

               osprofile_id := NULL;

               whereami := 320;
               fnd_file.put_line (fnd_file.LOG
                                 ,' Before Calling Create Party SIte API ');
               hz_party_site_v2pub.create_party_site (
                  p_init_msg_list       => 'T'
                 ,p_party_site_rec      => ppartysiterec
                 ,x_party_site_id       => opartysiteid
                 ,x_party_site_number   => opartysiteno
                 ,x_return_status       => ostatus
                 ,x_msg_count           => omsgcount
                 ,x_msg_data            => omsgdata);

               IF ostatus <> 'S'
               THEN
                  whereami := 330;

                  IF omsgcount > 1
                  THEN
                     FOR i IN 1 .. omsgcount
                     LOOP
                        p_site_err :=
                              p_site_err
                           || SUBSTR (
                                 fnd_msg_pub.get (
                                    i
                                   ,p_encoded   => fnd_api.g_false)
                                ,1
                                ,255);
                     END LOOP;

                     -- Satish U: Moving Outside the Loop
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   ' CUSTOMER PARTY SITE RECORD '
                        || SUBSTR (p_site_err, 1, 255));
                  ELSE
                     whereami := 340;
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,' CUSTOMER  PARTY SITE RECORD ' || omsgdata);
                     p_site_err := omsgdata;
                  END IF;

                  p_rid := cust_sites.rid;
                  RAISE e_validation_exception;
               ELSE
                  whereami := 350;
                  --initialize the values
                  ostatus := NULL;
                  omsgcount := NULL;
                  omsgdata := NULL;
                  ocustacctsiteid := NULL;

                  pcustacctsiterec.cust_account_id := ocustaccountid;
                  pcustacctsiterec.party_site_id := opartysiteid;
                  pcustacctsiterec.created_by_module := 'TCA_V1_API';
                  pcustacctsiterec.orig_system_reference :=
                     prefix || cust_sites.legacy_customer_number;
                  pcustacctsiterec.org_id := g_org_id; --cust_sites.location||'-'||opartysiteid||nvl(rtrim(cust_sites.address1),cust_sites.legacy_customer_number);

                  /*DFF Name ======== Address Information (Account Site level)*/

                  whereami := 360;
                  --Notice to Owner
                  pcustacctsiterec.attribute_category := cust_sites.attribute4; --'Yes';
                  pcustacctsiterec.attribute1 := cust_sites.attribute16;
                  -- 03/15/2012 CG Prism Number DFF
                  -- 03/16/2012 CG Changed to use the legacy party site number instead
                  -- 04/30/2012 CG Changed the data being loaded into the DFF in case it's a duplicate it's passing
                  -- a suffix we strip on load
                  -- pcustacctsiterec.attribute17:= cust_sites.legacy_party_site_number;
                  l_legacy_party_site := NULL;

                  BEGIN
                     SELECT SUBSTR (
                               cust_sites.legacy_party_site_number
                              ,1
                              ,DECODE (
                                  INSTR (cust_sites.legacy_party_site_number
                                        ,'-'
                                        ,1
                                        ,1)
                                 ,0, LENGTH (
                                        cust_sites.legacy_party_site_number)
                                 ,  INSTR (
                                       cust_sites.legacy_party_site_number
                                      ,'-'
                                      ,1
                                      ,1)
                                  - 1))
                       INTO l_legacy_party_site
                       FROM DUAL;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_legacy_party_site :=
                           cust_sites.legacy_party_site_number;
                  END;

                  pcustacctsiterec.attribute17 := l_legacy_party_site;
                  -- 04/30/2012 CG

                  --Mandatory Notes
                  pcustacctsiterec.attribute18 := cust_sites.attribute18; --Notice to Owner-Job Total
                  pcustacctsiterec.attribute19 := cust_sites.attribute19; --'Notice to Owner-Prelim Notice';  --Notice to Owner-Prelim Notice

                  IF UPPER (cust_sites.attribute15) = 'YES'
                  THEN
                     pcustacctsiterec.attribute3 := 'Y'; --Y N NULL Mandatory PO Number
                  ELSIF UPPER (cust_sites.attribute15) = 'NO'
                  THEN
                     pcustacctsiterec.attribute3 := 'N';
                  END IF;

                  IF cust_sites.attribute5 IS NOT NULL
                  THEN
                     pcustacctsiterec.attribute5 :=
                        TO_CHAR (TO_DATE (cust_sites.attribute5, 'YYYYMMDD')
                                ,'YYYY/MM/DD HH24:MI:SS');
                  --pcustacctsiterec.attribute5:=null; --cust_sites.attribute5; --to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');  --Lien Release Date
                  END IF;

                  IF cust_sites.attribute20 IS NOT NULL
                  THEN
                     pcustacctsiterec.attribute20 :=
                        TO_CHAR (TO_DATE (cust_sites.attribute20, 'YYYYMMDD')
                                ,'YYYY/MM/DD HH24:MI:SS');
                  --pcustacctsiterec.attribute20:=null; --cust_sites.attribute20;--to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');--Notice to Owner-Prelim Date
                  END IF;

                  whereami := 370;
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,' Before Calling Create Customer Account SIte API ');
                  hz_cust_account_site_v2pub.create_cust_acct_site (
                     p_init_msg_list        => 'T'
                    ,p_cust_acct_site_rec   => pcustacctsiterec
                    ,x_cust_acct_site_id    => ocustacctsiteid
                    ,x_return_status        => ostatus
                    ,x_msg_count            => omsgcount
                    ,x_msg_data             => omsgdata);

                  IF ostatus <> 'S'
                  THEN
                     whereami := 380;

                     IF omsgcount > 1
                     THEN
                        FOR i IN 1 .. omsgcount
                        LOOP
                           p_site_err :=
                                 NVL (p_site_err, ' ')
                              || SUBSTR (
                                    fnd_msg_pub.get (
                                       i
                                      ,p_encoded   => fnd_api.g_false)
                                   ,1
                                   ,255);
                        END LOOP;

                        -- Satish U: Moving it outside the Loop
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   ' CUSTOMER ACCOUNT SITE RECORD '
                           || cust_party.account_name
                           || ' '
                           || omsgdata
                           || ' '
                           || SUBSTR (p_site_err, 1, 255));
                     ELSE
                        whereami := 390;
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   ' CUSTOMER ACCOUNT SITE RECORD '
                           || cust_party.account_name
                           || ' '
                           || omsgdata);
                        p_site_err := omsgdata;
                     END IF;

                     p_rid := cust_sites.rid;
                     RAISE e_validation_exception;
                  ELSE
                     whereami := 400;

                     --initialize the values
                     pcustacctsiteuserec := NULL;
                     ostatus := NULL;
                     omsgcount := NULL;
                     omsgdata := NULL;
                     ocustacctsiteuseid := NULL;

                     -- 03/16/2012 CG Finding sales rep for both sites
                     BEGIN
                        l_sales_person_id := NULL;

                        SELECT rs.SALESREP_ID
                          INTO l_sales_person_id
                          FROM XXWC_AR_HOUSE_ACCOUNTS_XREF ha
                              ,jtf_rs_salesreps rs
                         WHERE     ha.Salesrep_ID = cust_sites.Salesrep_ID
                               -- 05/14/2012 CG: MOdified to account for House Account Only
                               --And    To_Char(ha.EMPLID)  = rs.Salesrep_number
                               AND TO_CHAR (ha.Salesrep_ID) =
                                      rs.Salesrep_number
                               AND ha.Salesrep_ID IS NOT NULL;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_sales_person_id := NULL;
                     END;

                     -- 07-MAR-2012 : IF  House Account Sales Person is not found, Check Primary Salesrep Exists
                     IF l_sales_person_id IS NULL
                     THEN
                        BEGIN
                           SELECT salesrep_id
                             INTO l_sales_person_id
                             FROM jtf_rs_salesreps
                            WHERE     1 = 1
                                  AND salesrep_number =
                                         cust_sites.primary_salesrep_number
                                  AND ROWNUM = 1;
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              l_sales_person_id := NULL;
                              whereami := 430;

                              IF l_Sales_Person_Id IS NULL
                              THEN
                                 -- Get Default Sales Person
                                 SELECT salesrep_id
                                   INTO l_sales_person_id
                                   FROM jtf_rs_salesreps
                                  WHERE     name = 'No Sales Credit'
                                        AND ROWNUM = 1;
                              END IF;
                        END;
                     END IF;

                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   ' SalesrepID '
                        || cust_sites.Salesrep_ID
                        || ' - Sales Rep Num '
                        || cust_sites.primary_salesrep_number
                        || ' - Found: '
                        || l_sales_person_id);

                     -- 03/16/2012 CG Finding sales rep for both sites

                     --*********create bill to site use
                     --if cust_sites.bill_to = 'Y' then
                     --if upper(cust_sites.site_use_type)='BILL_TO' then
                     IF UPPER (cust_sites.hds_site_flag) IN
                           ('YARD', 'SITE', 'MSTR', 'JOB', 'MISC')
                     THEN
                        pcustacctsiteuserec := NULL;
                        pcustomerprofile := NULL;

                        pcustacctsiteuserec.site_use_code := 'BILL_TO';
                        pcustacctsiteuserec.cust_acct_site_id :=
                           ocustacctsiteid;
                        pcustacctsiteuserec.gsa_indicator :=
                           cust_sites.gsa_indicator;
                        pcustacctsiteuserec.primary_salesrep_id :=
                           l_sales_person_id;
                        pcustacctsiteuserec.created_by_module := 'TCA_V1_API';

                        --pcustacctsiteuserec.primary_flag := cust_sites.primary_bill_to_flag;
                        IF UPPER (cust_sites.hds_site_flag) IN
                              ('MSTR', 'MISC')
                        THEN
                           pcustacctsiteuserec.primary_flag := 'Y';
                        ELSE
                           pcustacctsiteuserec.primary_flag := 'N';
                        END IF;

                        IF UPPER (cust_sites.freight_term) = 'FREIGHT EXEMPT'
                        THEN
                           pcustacctsiteuserec.freight_term := 'EXEMPT';
                        ELSE
                           pcustacctsiteuserec.freight_term := NULL;
                        END IF;

                        IF     cust_sites.job_name IS NOT NULL
                           AND cust_sites.job_number IS NOT NULL
                        THEN
                           pcustacctsiteuserec.location :=
                              SUBSTR (
                                    UPPER (cust_sites.job_name)
                                 || '-'
                                 || cust_sites.job_number
                                ,1
                                ,40);
                        ELSIF     cust_sites.legacy_party_site_name
                                     IS NOT NULL
                              AND cust_sites.legacy_customer_number
                                     IS NOT NULL
                        THEN
                           pcustacctsiteuserec.location :=
                              SUBSTR (
                                    UPPER (cust_sites.legacy_party_site_name)
                                 || '-'
                                 || UPPER (cust_sites.legacy_customer_number)
                                ,1
                                ,40);
                        ELSIF     cust_sites.legacy_party_site_name IS NULL
                              AND cust_sites.legacy_customer_number
                                     IS NOT NULL
                        THEN
                           IF cust_sites.legacy_site_type = 1000
                           THEN
                              pcustacctsiteuserec.location :=
                                    'MSTR-'
                                 || UPPER (cust_sites.legacy_customer_number);
                           ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                 AND 999
                           THEN
                              pcustacctsiteuserec.location :=
                                    'YARD-'
                                 || UPPER (cust_sites.legacy_customer_number);
                           ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                 AND 989
                           THEN
                              pcustacctsiteuserec.location :=
                                    'JOB-'
                                 || UPPER (cust_sites.legacy_customer_number);
                           END IF;
                        ELSIF     cust_sites.legacy_party_site_name
                                     IS NOT NULL
                              AND cust_sites.legacy_customer_number IS NULL
                        THEN
                           pcustacctsiteuserec.location :=
                              SUBSTR (
                                    UPPER (cust_sites.legacy_party_site_name)
                                 || '-'
                                 || LPAD (p_loc_count, 2, '0')
                                ,1
                                ,40);
                        ELSIF     cust_sites.legacy_party_site_name IS NULL
                              AND cust_sites.legacy_customer_number IS NULL
                        THEN
                           IF cust_sites.legacy_site_type = 1000
                           THEN
                              pcustacctsiteuserec.location :=
                                 'MSTR-' || LPAD (p_mstr_loc_count, 2, '0');
                              p_mstr_loc_count := p_mstr_loc_count + 1;
                           ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                 AND 999
                           THEN
                              pcustacctsiteuserec.location :=
                                 'YARD-' || LPAD (p_yard_loc_count, 2, '0');
                           ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                 AND 989
                           THEN
                              pcustacctsiteuserec.location :=
                                 'JOB-' || LPAD (p_job_loc_count, 2, '0');
                           END IF;
                        END IF;

                        IF UPPER (cust_sites.hds_site_flag) = 'MSTR'
                        THEN
                           pcustacctsiteuserec.attribute1 := 'MSTR';
                           pcustacctsiteuserec.attribute5 := 'MSTR';
                        ELSIF UPPER (cust_sites.hds_site_flag) = 'YARD'
                        THEN
                           pcustacctsiteuserec.attribute1 := 'YARD';
                           pcustacctsiteuserec.attribute5 := 'YARD';
                        ELSIF UPPER (cust_sites.hds_site_flag) = 'MISC'
                        THEN
                           pcustacctsiteuserec.attribute1 := 'MISC';
                           pcustacctsiteuserec.attribute5 := 'MISC';
                        ELSIF UPPER (cust_sites.hds_site_flag) IN
                                 ('SITE', 'JOB')
                        THEN
                           pcustacctsiteuserec.attribute1 := 'JOB';
                           pcustacctsiteuserec.attribute5 := 'JOB';
                        END IF;

                        whereami := 410;

                        --thomas guide Page
                        pcustacctsiteuserec.attribute3 :=
                           cust_sites.attribute11;

                        -- 03/16/2012 CG
                        OPEN Cur_cust_prof (ocustaccountid);

                        FETCH Cur_cust_prof INTO l_cust_acct_profile;

                        IF Cur_cust_prof%NOTFOUND
                        THEN
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   ' Could not find customer profile for job '
                              || cust_sites.legacy_party_site_number);

                           BEGIN
                              SELECT profile_class_id
                                INTO l_profile_id
                                FROM hz_customer_profiles
                               WHERE     cust_account_id = ocustaccountid
                                     AND site_use_id IS NULL;
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                                 l_profile_id := NULL;
                           END;

                           pcustomerprofile.profile_class_id := l_profile_id; --setting at cust account level
                        END IF;

                        IF Cur_cust_prof%ISOPEN
                        THEN
                           CLOSE Cur_cust_prof;
                        END IF;

                        --pcustomerprofile.cust_account_id := ocustaccountid;
                        --pcustomerprofile.site_use_id := p_site_use_id;
                        --pcustomerprofile.created_by_module := 'ONT_UI_ADD_CUSTOMER';
                        --pcustomerprofile.account_status := l_cust_acct_profile. account_status;
                        pcustomerprofile.profile_class_id :=
                           l_cust_acct_profile.profile_class_id;
                        pcustomerprofile.collector_id :=
                           l_cust_acct_profile.collector_id;
                        pcustomerprofile.credit_analyst_id :=
                           l_cust_acct_profile.credit_analyst_id;
                        pcustomerprofile.credit_checking :=
                           l_cust_acct_profile.credit_checking;
                        pcustomerprofile.next_credit_review_date :=
                           l_cust_acct_profile.next_credit_review_date;
                        pcustomerprofile.tolerance :=
                           l_cust_acct_profile.tolerance;
                        pcustomerprofile.discount_terms :=
                           l_cust_acct_profile.discount_terms;
                        pcustomerprofile.next_credit_review_date :=
                           l_cust_acct_profile.next_credit_review_date;
                        pcustomerprofile.dunning_letters :=
                           l_cust_acct_profile.dunning_letters;
                        pcustomerprofile.interest_charges :=
                           l_cust_acct_profile.interest_charges;
                        pcustomerprofile.send_statements :=
                           l_cust_acct_profile.send_statements;
                        pcustomerprofile.credit_balance_statements :=
                           l_cust_acct_profile.credit_balance_statements;
                        pcustomerprofile.credit_hold :=
                           l_cust_acct_profile.credit_hold;
                        pcustomerprofile.credit_rating :=
                           l_cust_acct_profile.credit_rating;
                        pcustomerprofile.risk_code :=
                           l_cust_acct_profile.risk_code;
                        pcustomerprofile.standard_terms :=
                           l_cust_acct_profile.standard_terms;
                        pcustomerprofile.override_terms :=
                           l_cust_acct_profile.override_terms;
                        pcustomerprofile.dunning_letter_set_id :=
                           l_cust_acct_profile.dunning_letter_set_id;
                        pcustomerprofile.interest_period_days :=
                           l_cust_acct_profile.interest_period_days;
                        pcustomerprofile.payment_grace_days :=
                           l_cust_acct_profile.payment_grace_days;
                        pcustomerprofile.discount_grace_days :=
                           l_cust_acct_profile.discount_grace_days;
                        pcustomerprofile.statement_cycle_id :=
                           l_cust_acct_profile.statement_cycle_id;
                        pcustomerprofile.percent_collectable :=
                           l_cust_acct_profile.percent_collectable;
                        pcustomerprofile.autocash_hierarchy_id :=
                           l_cust_acct_profile.autocash_hierarchy_id;
                        pcustomerprofile.auto_rec_incl_disputed_flag :=
                           l_cust_acct_profile.auto_rec_incl_disputed_flag;
                        pcustomerprofile.tax_printing_option :=
                           l_cust_acct_profile.tax_printing_option;
                        pcustomerprofile.charge_on_finance_charge_flag :=
                           l_cust_acct_profile.charge_on_finance_charge_flag;
                        pcustomerprofile.grouping_rule_id :=
                           l_cust_acct_profile.grouping_rule_id;
                        pcustomerprofile.clearing_days :=
                           l_cust_acct_profile.clearing_days;
                        pcustomerprofile.cons_inv_flag :=
                           l_cust_acct_profile.cons_inv_flag;
                        pcustomerprofile.cons_inv_type :=
                           l_cust_acct_profile.cons_inv_type;
                        pcustomerprofile.autocash_hierarchy_id_for_adr :=
                           l_cust_acct_profile.autocash_hierarchy_id_for_adr;
                        pcustomerprofile.lockbox_matching_option :=
                           l_cust_acct_profile.lockbox_matching_option;
                        pcustomerprofile.review_cycle :=
                           l_cust_acct_profile.review_cycle;
                        pcustomerprofile.party_id :=
                           l_cust_acct_profile.party_id;
                        pcustomerprofile.late_charge_calculation_trx :=
                           l_cust_acct_profile.late_charge_calculation_trx;
                        pcustomerprofile.credit_classification :=
                           l_cust_acct_profile.credit_classification;
                        pcustomerprofile.cons_bill_level :=
                           l_cust_acct_profile.cons_bill_level;
                        pcustomerprofile.late_charge_calculation_trx :=
                           l_cust_acct_profile.late_charge_calculation_trx;
                        pcustomerprofile.credit_items_flag :=
                           l_cust_acct_profile.credit_items_flag;
                        pcustomerprofile.disputed_transactions_flag :=
                           l_cust_acct_profile.disputed_transactions_flag;
                        pcustomerprofile.late_charge_type :=
                           l_cust_acct_profile.late_charge_type;
                        pcustomerprofile.late_charge_term_id :=
                           l_cust_acct_profile.late_charge_term_id;
                        pcustomerprofile.interest_calculation_period :=
                           l_cust_acct_profile.interest_calculation_period;
                        pcustomerprofile.hold_charged_invoices_flag :=
                           l_cust_acct_profile.hold_charged_invoices_flag;
                        pcustomerprofile.message_text_id :=
                           l_cust_acct_profile.message_text_id;
                        pcustomerprofile.multiple_interest_rates_flag :=
                           l_cust_acct_profile.multiple_interest_rates_flag;
                        pcustomerprofile.charge_begin_date :=
                           l_cust_acct_profile.charge_begin_date;
                        pcustomerprofile.automatch_set_id :=
                           l_cust_acct_profile.automatch_set_id;
                        --pcustomerprofile.attribute2 := l_cust_acct_profile.attribute2;--Invoice Remit To Addre
                        pcustomerprofile.attribute3 :=
                           l_cust_acct_profile.attribute3;  --Statement by Job

                        BEGIN
                           SELECT oracle_code
                             INTO pcustomerprofile.attribute2 --Invoice Remit To Address Code
                             FROM xxwc_remit_to_addr_xref x
                            WHERE x.prism_code = cust_sites.attribute2;
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              pcustomerprofile.attribute2 := 2;
                        END;

                        whereami := 420;

                        BEGIN
                           whereami := 429;

                           SELECT lookup_code
                             INTO pcustomerprofile.account_status
                             FROM xxwc_account_status_xref a, ar_lookups b
                            WHERE     a.account_status = b.meaning
                                  AND b.lookup_type = 'ACCOUNT_STATUS'
                                  AND a.prism_credit_group =
                                         cust_sites.credit_analyst_name;
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              pcustomerprofile.account_status := NULL;
                        END;

                        whereami := 430;
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,' Before Calling Create Customer Site USe API ');
                        hz_cust_account_site_v2pub.create_cust_site_use (
                           p_init_msg_list          => 'T'
                          ,p_cust_site_use_rec      => pcustacctsiteuserec
                          ,p_customer_profile_rec   => pcustomerprofile
                          ,p_create_profile         => 'T'
                          ,p_create_profile_amt     => 'T'
                          ,x_site_use_id            => ocustacctsiteuseid
                          ,x_return_status          => ostatus
                          ,x_msg_count              => omsgcount
                          ,x_msg_data               => omsgdata);

                        IF UPPER (cust_sites.hds_site_flag) IN
                              ('YARD', 'MSTR', 'MISC', 'JOB', 'SITE')
                        THEN
                           pbilltositeuseid := ocustacctsiteuseid;
                        END IF;

                        pcustacctid := ocustaccountid;

                        IF ostatus <> 'S'
                        THEN
                           whereami := 440;
                           l_proceed := 'N';

                           --fnd_file.put_line ( fnd_file.LOG  , ' 1120-Create SIte USe TYpe API errored out: Msg Count' || omsgcount);
                           IF omsgcount > 1
                           THEN
                              FOR i IN 1 .. omsgcount
                              LOOP
                                 p_site_err :=
                                       p_site_err
                                    || SUBSTR (
                                          fnd_msg_pub.get (
                                             p_encoded => fnd_api.g_false)
                                         ,1
                                         ,255);
                              END LOOP;

                              -- Satish U: 11/01/11 Moving Outside the Loop
                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,   'CUSTOMER BILL_TO SITE USE RECORD '
                                 || SUBSTR (p_site_err, 1, 255));
                           ELSE
                              whereami := 450;
                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,   'CUSTOMER BILL_TO SITE USE RECORD '
                                 || omsgdata);
                              p_site_err := omsgdata;
                           END IF;

                           p_rid := cust_sites.rid;
                           RAISE e_validation_exception;
                        ELSE
                           whereami := 450;

                           IF    NVL (cust_sites.trx_credit_limit, 0) != 0
                              OR NVL (cust_sites.overall_credit_limit, 0) !=
                                    0
                           THEN
                              pcustproamtrec := NULL;

                              BEGIN
                                 SELECT cust_account_profile_id
                                   INTO osprofile_id
                                   FROM hz_customer_profiles
                                  WHERE     cust_account_id = ocustaccountid
                                        AND site_use_id = ocustacctsiteuseid;
                              EXCEPTION
                                 WHEN NO_DATA_FOUND
                                 THEN
                                    whereami := 460;
                                    osprofile_id := NULL;
                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,   'No Customer Profile Found for site use id '
                                       || ocustacctsiteuseid);
                              END;

                              IF osprofile_id IS NOT NULL
                              THEN
                                 BEGIN
                                    whereami := 470;

                                    SELECT cust_acct_profile_amt_id
                                          ,object_version_number
                                      INTO ocustacctprofileamtid
                                          ,p_object_version_number
                                      FROM hz_cust_profile_amts
                                     WHERE     cust_account_profile_id =
                                                  osprofile_id
                                           AND currency_code = 'USD';
                                 EXCEPTION
                                    WHEN NO_DATA_FOUND
                                    THEN
                                       ocustacctprofileamtid := NULL;
                                       p_object_version_number := NULL;
                                 END;

                                 IF ocustacctprofileamtid IS NOT NULL
                                 THEN
                                    whereami := 480;

                                    --Update amount profile
                                    pcustproamtrec.cust_acct_profile_amt_id :=
                                       ocustacctprofileamtid;
                                    pcustproamtrec.trx_credit_limit :=
                                       cust_sites.trx_credit_limit;
                                    pcustproamtrec.overall_credit_limit :=
                                       cust_sites.overall_credit_limit;
                                    pcustproamtrec.created_by_module :=
                                       'TCA_V1_API';

                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,' Before Calling Update Profile Amt API ');
                                    hz_customer_profile_v2pub.update_cust_profile_amt (
                                       p_init_msg_list           => 'T'
                                      ,p_cust_profile_amt_rec    => pcustproamtrec
                                      ,p_object_version_number   => p_object_version_number
                                      ,x_return_status           => ostatus
                                      ,x_msg_count               => omsgcount
                                      ,x_msg_data                => omsgdata);

                                    IF ostatus <> 'S'
                                    THEN
                                       whereami := 490;

                                       --fnd_file.put_line ( fnd_file.LOG  , ' 1121 -Update Profile Amt API Errored Out : Msg Count' || omsgcount);
                                       IF omsgcount > 1
                                       THEN
                                          FOR i IN 1 .. omsgcount
                                          LOOP
                                             p_site_err :=
                                                   NVL (p_site_err, ' ')
                                                || SUBSTR (
                                                      fnd_msg_pub.get (
                                                         i
                                                        ,p_encoded   => fnd_api.g_false)
                                                     ,1
                                                     ,255);
                                          END LOOP;

                                          -- Satish U: 11/01/11 Moving Code Outside the Loop
                                          fnd_file.put_line (
                                             fnd_file.LOG
                                            ,   ' CUSTOMER Site Amout Profile Updation '
                                             || SUBSTR (p_site_err, 1, 255));
                                       ELSE
                                          fnd_file.put_line (
                                             fnd_file.LOG
                                            ,   ' CUSTOMER Site Amount Profile Updation '
                                             || omsgdata);
                                          p_site_err := omsgdata;
                                       END IF;

                                       p_rid := cust_sites.rid;
                                       RAISE e_validation_exception;
                                    END IF;
                                 ELSE
                                    whereami := 500;

                                    --Create Profile Amount
                                    pcustproamtrec.cust_account_profile_id :=
                                       osprofile_id;
                                    pcustproamtrec.cust_account_id :=
                                       ocustaccountid;
                                    pcustproamtrec.site_use_id :=
                                       ocustacctsiteuseid;
                                    pcustproamtrec.currency_code := 'USD';
                                    pcustproamtrec.trx_credit_limit :=
                                       cust_sites.trx_credit_limit;
                                    pcustproamtrec.overall_credit_limit :=
                                       cust_sites.overall_credit_limit;
                                    pcustproamtrec.created_by_module :=
                                       'TCA_V1_API';
                                    fnd_file.put_line (
                                       fnd_file.LOG
                                      ,' Before Calling Create Profile Amt API ');
                                    hz_customer_profile_v2pub.create_cust_profile_amt (
                                       p_init_msg_list              => 'T'
                                      ,p_check_foreign_key          => 'T'
                                      ,p_cust_profile_amt_rec       => pcustproamtrec
                                      ,x_cust_acct_profile_amt_id   => ocustacctprofileamtid
                                      ,x_return_status              => ostatus
                                      ,x_msg_count                  => omsgcount
                                      ,x_msg_data                   => omsgdata);

                                    IF ostatus <> 'S'
                                    THEN
                                       whereami := 510;

                                       --fnd_file.put_line ( fnd_file.LOG  , ' Create Profile Amt API Errored Out- : Msg Count' || omsgcount);
                                       IF omsgcount > 1
                                       THEN
                                          FOR i IN 1 .. omsgcount
                                          LOOP
                                             p_site_err :=
                                                   NVL (p_site_err, ' ')
                                                || SUBSTR (
                                                      fnd_msg_pub.get (
                                                         i
                                                        ,p_encoded   => fnd_api.g_false)
                                                     ,1
                                                     ,255);
                                          END LOOP;

                                          -- SatishU: 11/02/11 : MOved the code out from LOOP
                                          fnd_file.put_line (
                                             fnd_file.LOG
                                            ,   ' CUSTOMER Site Use Level Amout Profile Creation '
                                             || SUBSTR (p_site_err, 1, 255));
                                       ELSE
                                          fnd_file.put_line (
                                             fnd_file.LOG
                                            ,   ' CUSTOMER Site Use Level Amount Profile Creation '
                                             || omsgdata);
                                          p_site_err := omsgdata;
                                       END IF;

                                       p_rid := cust_sites.rid;
                                       RAISE e_validation_exception;
                                    END IF;
                                 END IF;
                              END IF;
                           END IF;                       --site amount profile
                        END IF;

                        --fnd_file.put_line (fnd_file.output,'CUSTOMER BILL TO SITE CREATED FOR THE SITE :- ' || cust_party.account_name);
                        --fnd_file.put_line (fnd_file.output,'****************************');
                        l_proceed := 'Y';
                     END IF;                                   -- bill to site

                     whereami := 520;

                     --*********create ship to site use
                     IF UPPER (cust_sites.hds_site_flag) IN
                           ('YARD', 'SITE', 'JOB', 'MISC')
                     THEN
                        pcustacctsiteuserec := NULL;
                        pcustomerprofile := NULL;
                        pcustacctsiteuserec.site_use_code := 'SHIP_TO';
                        pcustacctsiteuserec.bill_to_site_use_id :=
                           pbilltositeuseid;
                        pcustacctsiteuserec.cust_acct_site_id :=
                           ocustacctsiteid;
                        pcustacctsiteuserec.gsa_indicator :=
                           cust_sites.gsa_indicator;
                        pcustacctsiteuserec.primary_salesrep_id :=
                           l_sales_person_id;
                        pcustacctsiteuserec.created_by_module := 'TCA_V1_API';

                        IF UPPER (cust_sites.freight_term) = 'FREIGHT EXEMPT'
                        THEN
                           pcustacctsiteuserec.freight_term := 'EXEMPT';
                        ELSE
                           pcustacctsiteuserec.freight_term := NULL;
                        END IF;

                        IF     UPPER (cust_sites.hds_site_flag) IN
                                  ('YARD', 'MISC')
                           AND lv_primary_ship_to_flag IS NULL
                        THEN
                           pcustacctsiteuserec.primary_flag := 'Y';
                           lv_primary_ship_to_flag := 'Y';
                        ELSE
                           pcustacctsiteuserec.primary_flag := 'N';
                        END IF;

                        --populate location
                        IF     cust_sites.job_name IS NOT NULL
                           AND cust_sites.job_number IS NOT NULL
                        THEN
                           pcustacctsiteuserec.location :=
                              SUBSTR (
                                    UPPER (cust_sites.job_name)
                                 || '-'
                                 || cust_sites.job_number
                                ,1
                                ,40);
                        ELSIF     cust_sites.legacy_party_site_name
                                     IS NOT NULL
                              AND cust_sites.legacy_customer_number
                                     IS NOT NULL
                        THEN
                           pcustacctsiteuserec.location :=
                              SUBSTR (
                                    UPPER (cust_sites.legacy_party_site_name)
                                 || '-'
                                 || UPPER (cust_sites.legacy_customer_number)
                                ,1
                                ,40);
                        ELSIF     cust_sites.legacy_party_site_name IS NULL
                              AND cust_sites.legacy_customer_number
                                     IS NOT NULL
                        THEN
                           IF cust_sites.legacy_site_type = 1000
                           THEN
                              pcustacctsiteuserec.location :=
                                    'MSTR-'
                                 || UPPER (cust_sites.legacy_customer_number);
                           ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                 AND 999
                           THEN
                              pcustacctsiteuserec.location :=
                                    'YARD-'
                                 || UPPER (cust_sites.legacy_customer_number);
                           ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                 AND 989
                           THEN
                              pcustacctsiteuserec.location :=
                                    'JOB-'
                                 || UPPER (cust_sites.legacy_customer_number);
                           END IF;
                        ELSIF     cust_sites.legacy_party_site_name
                                     IS NOT NULL
                              AND cust_sites.legacy_customer_number IS NULL
                        THEN
                           pcustacctsiteuserec.location :=
                              SUBSTR (
                                    UPPER (cust_sites.legacy_party_site_name)
                                 || '-'
                                 || LPAD (p_loc_count, 2, '0')
                                ,1
                                ,40);
                           p_loc_count := p_loc_count + 1;
                        ELSIF     cust_sites.legacy_party_site_name IS NULL
                              AND cust_sites.legacy_customer_number IS NULL
                        THEN
                           IF cust_sites.legacy_site_type = 1000
                           THEN
                              pcustacctsiteuserec.location :=
                                 'MSTR-' || LPAD (p_mstr_loc_count, 2, '0');
                           ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                 AND 999
                           THEN
                              pcustacctsiteuserec.location :=
                                 'YARD-' || LPAD (p_yard_loc_count, 2, '0');
                              p_yard_loc_count := p_yard_loc_count + 1;
                           ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                 AND 989
                           THEN
                              pcustacctsiteuserec.location :=
                                 'JOB-' || LPAD (p_job_loc_count, 2, '0');
                              p_job_loc_count := p_job_loc_count + 1;
                           END IF;
                        END IF;

                        whereami := 530;

                        --Customer Site Classification
                        /*DFF Name ======== Site Use Information */
                        IF UPPER (cust_sites.hds_site_flag) = 'MSTR'
                        THEN
                           pcustacctsiteuserec.attribute1 := 'MSTR';
                           pcustacctsiteuserec.attribute5 := 'MSTR';
                        ELSIF UPPER (cust_sites.hds_site_flag) = 'YARD'
                        THEN
                           pcustacctsiteuserec.attribute1 := 'YARD';
                           pcustacctsiteuserec.attribute5 := 'YARD';
                        ELSIF UPPER (cust_sites.hds_site_flag) = 'MISC'
                        THEN
                           pcustacctsiteuserec.attribute1 := 'MISC';
                           pcustacctsiteuserec.attribute5 := 'MISC';
                        ELSIF UPPER (cust_sites.hds_site_flag) IN
                                 ('SITE', 'JOB')
                        THEN
                           pcustacctsiteuserec.attribute1 := 'JOB';
                           pcustacctsiteuserec.attribute5 := 'JOB';
                        END IF;

                        --thomas guide Page
                        pcustacctsiteuserec.attribute3 :=
                           cust_sites.attribute11;


                        --pcustomerprofile.attribute2:=2; --Sieban Number(NO CONVERSION) No Conversion needed - Future Use Only
                        --pcustacctsiteuserec.attribute3:=cust_sites.attribute1; --Thomas Guide Code'
                        --pcustacctsiteuserec.attribute9:=11  --Government Funded(NO CONVERSION)Future Use Only (Default to No) (Yes/No Values)
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,' Before Calling Create Customer Site Use  API ');
                        hz_cust_account_site_v2pub.create_cust_site_use (
                           p_init_msg_list          => 'T'
                          ,p_cust_site_use_rec      => pcustacctsiteuserec
                          ,p_customer_profile_rec   => pcustomerprofile
                          ,p_create_profile         => 'F'
                          ,p_create_profile_amt     => 'F'
                          ,x_site_use_id            => ocustacctsiteuseid
                          ,x_return_status          => ostatus
                          ,x_msg_count              => omsgcount
                          ,x_msg_data               => omsgdata);
                        --pbilltositeuseid := ocustacctsiteuseid ;
                        pcustacctid := ocustaccountid;

                        IF ostatus <> 'S'
                        THEN
                           whereami := 540;
                           l_proceed := 'N';
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   ' Create Customer Site Use API Errored Out- Msg Count :'
                              || omsgcount);

                           IF omsgcount > 1
                           THEN
                              FOR i IN 1 .. omsgcount
                              LOOP
                                 p_site_err :=
                                       NVL (p_site_err, ' ')
                                    || SUBSTR (
                                          fnd_msg_pub.get (
                                             i
                                            ,p_encoded   => fnd_api.g_false)
                                         ,1
                                         ,255);
                              END LOOP;

                              -- Satish U: 11/02/11 : Moved code out of the loop
                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,   'CUSTOMER SHIP_TO SITE USE RECORD '
                                 || SUBSTR (p_site_err, 1, 255));
                           ELSE
                              whereami := 550;
                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,   'CUSTOMER SHIP_TO SITE USE RECORD '
                                 || omsgdata
                                 || '-'
                                 || cust_party.account_name);
                              p_site_err := omsgdata;
                           END IF;

                           p_rid := cust_sites.rid;
                           RAISE e_validation_exception;
                        END IF;


                        l_proceed := 'Y';
                     END IF;                                   -- ship to site
                  END IF;                                -- party site account

                  --create party site level communication
                  whereami := 550;

                  FOR rec_site_contact
                     IN get_site_contact (cust_sites.orig_system_reference
                                         ,cust_sites.legacy_customer_number)
                  LOOP
                     pop_org_communication ('SITE'
                                           ,opartysiteid
                                           ,rec_site_contact.phone_area_code
                                           ,rec_site_contact.phone_number
                                           ,rec_site_contact.fax_area_code
                                           ,rec_site_contact.fax_number
                                           ,p_result
                                           ,omsgdata);

                     IF p_result = 'E'
                     THEN
                        whereami := 560;
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   'ERROR IN PARTY SITE LEVEL COMMUNICATION '
                           || TO_CHAR (whereami));
                        p_con_err := omsgdata;
                        p_rid := rec_site_contact.rid;
                        RAISE e_validation_exception;
                     END IF;

                     -- contacts for MISC billto. since MISC site is created twice, we need to attach
                     -- the same contact at bill to and ship to
                     IF ln_partysiteid_misc_bill_to IS NOT NULL
                     THEN
                        pop_org_communication (
                           'SITE'
                          ,ln_partysiteid_misc_bill_to
                          ,rec_site_contact.phone_area_code
                          ,rec_site_contact.phone_number
                          ,rec_site_contact.fax_area_code
                          ,rec_site_contact.fax_number
                          ,p_result
                          ,omsgdata);

                        IF p_result = 'E'
                        THEN
                           whereami := 561;
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   'ERROR IN PARTY SITE LEVEL COMMUNICATION FOR MISC BILLTO'
                              || TO_CHAR (whereami));
                           p_con_err := omsgdata;
                           p_rid := rec_site_contact.rid;
                           RAISE e_validation_exception;
                        END IF;
                     END IF;

                     IF cust_sites.hds_site_flag IN ('MISC', 'MSTR')
                     THEN
                        pop_org_communication (
                           'PARTY'
                          ,opartyid
                          ,rec_site_contact.phone_area_code
                          ,rec_site_contact.phone_number
                          ,rec_site_contact.fax_area_code
                          ,rec_site_contact.fax_number
                          ,p_result
                          ,omsgdata);

                        IF p_result = 'E'
                        THEN
                           whereami := 562;
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   'ERROR IN PARTY LEVEL COMMUNICATION FOR MISC OR MSTR'
                              || TO_CHAR (whereami));
                           p_con_err := omsgdata;
                           p_rid := rec_site_contact.rid;
                           RAISE e_validation_exception;
                        END IF;
                     END IF;

                     UPDATE XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
                        SET process_status = 'P'
                           ,last_update_date = SYSDATE
                           ,processing_date = SYSDATE
                      WHERE ROWID = rec_site_contact.rid;
                  END LOOP;
               END IF;                                           -- party site
            END IF;                                                 --location

            UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
               SET process_status = 'P'
                  ,last_update_date = SYSDATE
                  ,processing_date = SYSDATE
             WHERE ROWID = cust_sites.rid;
         EXCEPTION
            WHEN e_validation_exception
            THEN
               --whereami    := 590;

               v_err := 'Y';
               ROLLBACK TO customer_record2;
               upd_stagin_table_err (p_cus_err
                                    ,p_site_err
                                    ,p_con_err
                                    ,p_rid);
               p_cus_err := NULL;
               p_site_err := NULL;
               p_con_err := NULL;
               upd_stagin_table (cust_party.orig_system_reference);
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'e_Validation_Exception IN PROCEDURE for customer: '
                  || cust_party.orig_system_reference
                  || '---'
                  || l_api_name
                  || '---'
                  || TO_CHAR (whereami));
            WHEN OTHERS
            THEN
               -- whereami  := 600;
               ROLLBACK TO customer_record2;
               v_err := 'Y';
               --SatishU: 11/01/11  Added Following SQL Statement
               upd_stagin_table_err (p_cus_err
                                    ,p_site_err
                                    ,p_con_err
                                    ,p_rid);
               p_cus_err := NULL;
               p_site_err := NULL;
               p_con_err := NULL;
               upd_stagin_table (cust_party.orig_system_reference);
               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'When Others Error in PROCEDURE for customer: '
                  || cust_party.orig_system_reference
                  || '---'
                  || l_api_name
                  || SQLERRM
                  || '---'
                  || TO_CHAR (whereami));
         END;
      END LOOP;


      IF v_err = 'Y'
      THEN
         req_status := fnd_concurrent.set_completion_status ('WARNING', NULL);
      END IF;
   END customer_interface;

   /*************************************************************************
     Procedure :  get_cust_conversion_stats

     Purpose:    Procedure used to determine customer stats in validation
         mode
   ************************************************************************/
   PROCEDURE get_cust_conversion_stats
   IS
      CURSOR cust_cur
      IS
           SELECT NVL (process_status, 'N') process_status, COUNT (*) rec_count
             FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG
         GROUP BY process_status;

      CURSOR sites_cur
      IS
           SELECT NVL (process_status, 'N') process_status, COUNT (*) rec_count
             FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
         GROUP BY process_status;

      CURSOR contacts_cur
      IS
           SELECT NVL (process_status, 'N') process_status, COUNT (*) rec_count
             FROM XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
         GROUP BY process_status;

      ln_customer_count   NUMBER;
      ln_site_count       NUMBER;
   BEGIN
      fnd_file.put_line (
         fnd_file.output
        ,'*******************Customer Conversion Statistics **************************');


      SELECT COUNT (*)
        INTO ln_customer_count
        FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG cus
       WHERE NOT EXISTS
                (SELECT 1
                   FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  WHERE orig_system_reference = cus.orig_system_reference);

      fnd_file.put_line (
         fnd_file.output
        ,   'Total Customers without Related Site Information :'
         || ln_customer_count);

      SELECT COUNT (*)
        INTO ln_site_count
        FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG sites
       WHERE NOT EXISTS
                (SELECT 1
                   FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG
                  WHERE orig_system_reference = sites.orig_system_reference);

      fnd_file.put_line (
         fnd_file.output
        ,   'Total Customer Sites without Related Customer Information :'
         || ln_site_count);

      fnd_file.put_line (fnd_file.output, ' ');

      fnd_file.put_line (
         fnd_file.output
        ,'----------------Customer Statistics --------------------------');

      FOR cust_rec IN cust_cur
      LOOP
         IF cust_rec.process_status = 'P'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Customer Records Processed           : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'V'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Customer Records in Validated status : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'E'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Customer Records Errored Out         : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'N'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Customer Records with NULL Status    : '
               || cust_rec.rec_count);
         END IF;
      END LOOP;

      fnd_file.put_line (
         fnd_file.output
        ,'----------------Customer Sites Statistics --------------------------');

      FOR cust_rec IN sites_cur
      LOOP
         IF cust_rec.process_status = 'P'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Customer Sites Records Processed           : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'V'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Customer Sites Records in Validated status : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'E'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Customer Sites Records Errored Out         : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'N'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Customer Sites Records with NULL Status    : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'I'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Orphan Customer Sites Records   : '
               || cust_rec.rec_count);
         END IF;
      END LOOP;

      fnd_file.put_line (
         fnd_file.output
        ,'----------------Contact Points Statistics --------------------------');

      FOR cust_rec IN contacts_cur
      LOOP
         IF cust_rec.process_status = 'P'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Contact Points Records Processed           : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'V'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Contact Points Records in Validated status : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'E'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Contact Points Records Errored Out         : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'N'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Contact Points Records with NULL Status    : '
               || cust_rec.rec_count);
         ELSIF cust_rec.process_status = 'I'
         THEN
            fnd_file.put_line (
               fnd_file.output
              ,   'Number Of Orphan Contact Points Records  : '
               || cust_rec.rec_count);
         END IF;
      END LOOP;
   END get_cust_conversion_stats;

   /*************************************************************************
     Procedure :  populate_site_locations

     Purpose:    Procedure used to populate Customer Site Locations. Identify
          duplicate addresses within a party and assign oracle location Id,
                   Legacy Site Type column value in Customer Sites Staging table.
   ************************************************************************/
   PROCEDURE populate_site_locations
   IS
      -- Define a Cursor that gets
      CURSOR cust_cur
      IS
         SELECT orig_system_reference
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG
          WHERE 1 = 1;

      CURSOR site_cur (
         p_customer_number VARCHAR2)
      IS
           SELECT orig_system_reference
                 ,address1
                 ,address2
                 ,address3
                 ,address4
                 ,city
                 ,state
                 ,postal_code
                 ,county
                 ,country
                 ,province
                 ,legacy_party_site_number
                 ,legacy_site_type
                 ,location_id
             FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
            WHERE     orig_system_reference = p_customer_number
                  AND location_id IS NULL
         ORDER BY postal_code
                 ,state
                 ,city
                 ,address1
                 ,address2;

      -- Define Local Variables
      l_curr_postal_code            XXWC.XXWC_AR_CUST_SITE_IFACE_STG.postal_code%TYPE;
      l_curr_state                  XXWC.XXWC_AR_CUST_SITE_IFACE_STG.state%TYPE;
      l_curr_city                   XXWC.XXWC_AR_CUST_SITE_IFACE_STG.city%TYPE;
      l_curr_address1               XXWC.XXWC_AR_CUST_SITE_IFACE_STG.address1%TYPE;
      l_curr_address2               XXWC.XXWC_AR_CUST_SITE_IFACE_STG.address2%TYPE;
      l_curr_location_id            XXWC.XXWC_AR_CUST_SITE_IFACE_STG.location_id%TYPE;
      l_rec_count                   NUMBER;
      l_legacy_site_type            NUMBER;
      c_max_commit_count   CONSTANT NUMBER := 1000;
   BEGIN
      -- Open the Customer Cursor
      l_rec_count := 0;
      fnd_file.put_line (fnd_file.LOG, 'Begining of populate_site_locations');

      FOR cust_rec IN cust_cur
      LOOP
         FOR sites_rec IN site_cur (cust_rec.orig_system_reference)
         LOOP
            l_rec_count := l_rec_count + 1;

            --fnd_file.put_line (fnd_file.LOG,'301 Inside Sites Rec Cursor ');
            IF    (NVL (sites_rec.postal_code, 'NULL') <>
                      NVL (l_curr_postal_code, 'NULL'))
               OR (NVL (sites_rec.city, 'NULL') <> NVL (l_curr_city, 'NULL'))
               OR (NVL (sites_rec.state, 'NULL') <>
                      NVL (l_curr_state, 'NULL'))
               OR (NVL (sites_rec.address1, 'NULL') <>
                      NVL (l_curr_address1, 'NULL'))
               OR (NVL (sites_rec.address2, 'NULL') <>
                      NVL (l_curr_address2, 'NULL'))
            THEN
               SELECT hr_locations_s.NEXTVAL
                 INTO l_curr_location_id
                 FROM DUAL;
            ELSE
               -- Do not Get New Location ID : Use THe Old One
               NULL;
            END IF;

            l_curr_postal_code := sites_rec.postal_code;
            l_curr_city := sites_rec.city;
            l_curr_state := sites_rec.state;
            l_curr_address1 := sites_rec.address1;
            l_curr_address2 := sites_rec.address2;

            IF    sites_rec.legacy_site_type IS NULL
               OR sites_rec.location_id IS NULL
            THEN
               --fnd_file.put_line (fnd_file.LOG,'303 If Statement : Legacy SIte Type ');
               --fnd_file.put_line (fnd_file.LOG,'303.5 Legacy Party Site Number ' || sites_rec.legacy_party_site_number);

               IF TO_NUMBER (
                     SUBSTR (
                        RTRIM (
                           RTRIM (
                              RTRIM (sites_rec.legacy_party_site_number
                                    ,'-WC')
                             ,'-ST')
                          ,'-WC-ST')
                       ,-3
                       ,3)) = 0
               THEN
                  l_legacy_site_type := 1000;
               ELSE
                  l_legacy_site_type :=
                     TO_NUMBER (
                        SUBSTR (
                           RTRIM (
                              RTRIM (
                                 RTRIM (sites_rec.legacy_party_site_number
                                       ,'-WC')
                                ,'-ST')
                             ,'-WC-ST')
                          ,-3
                          ,3));
               END IF;

               --fnd_file.put_line (fnd_file.LOG,'304 Before updating XXWC.XXWC_AR_CUST_SITE_IFACE_STG ');
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET location_id = NVL (location_id, l_curr_location_id)
                     ,legacy_site_type = l_legacy_site_type
                WHERE     orig_system_reference =
                             sites_rec.orig_system_reference
                      AND legacy_party_site_number =
                             sites_rec.legacy_party_site_number;
            --fnd_file.put_line (fnd_file.LOG,'305 After updating XXWC.XXWC_AR_CUST_SITE_IFACE_STG ');
            END IF;
         END LOOP;

         IF l_rec_count >= c_max_commit_count
         THEN
            l_rec_count := 0;
            COMMIT;
         END IF;
      END LOOP;

      fnd_file.put_line (fnd_file.LOG
                        ,'306 End of the API Populate Site Locations ');
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (
            fnd_file.LOG
           ,'307 When Others Error : API Populate_Site_Locations ');
         fnd_file.put_line (
            fnd_file.output
           ,' When Others Error : API Populate_Site_Locations :' || SQLERRM);
         RAISE;
   END populate_site_locations;

   /*************************************************************************
     Procedure :  update_customer_without_sites

     Purpose:    Identifies Customer Records with out any sites and updates
         their status as Inactive
   ************************************************************************/
   PROCEDURE update_customer_without_sites
   IS
      CURSOR c1
      IS
         SELECT orig_system_reference
           FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG cus
          WHERE     NOT EXISTS
                           (SELECT 1
                              FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                             WHERE orig_system_reference =
                                      cus.orig_system_reference)
                AND Process_status <> 'I';
   BEGIN
      FOR crec IN c1
      LOOP
         UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
            SET Status = 'I'
          WHERE orig_system_reference = crec.orig_system_reference;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (
            fnd_file.output
           ,   ' When Others Error : API update_customer_without_sites :'
            || SQLERRM);
         RAISE;
   END update_customer_without_sites;


   /*************************************************************************
     Procedure :  update_contacts_without_sites

     Purpose:    Procedure to identify Orphan Customer Contacts,
         Records with out corresponding Customer are updated
                   to a status of Error
   ************************************************************************/
   PROCEDURE update_contacts_without_sites
   IS
      CURSOR Contact_Cur
      IS
         (SELECT ROWID
                ,Orig_System_Reference
                ,Legacy_party_Site_Number
                ,hds_Site_flag
            FROM XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG cont
           WHERE     1 = 1
                 AND NOT EXISTS
                            (SELECT 1
                               FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG sites
                              WHERE     sites.Orig_System_Reference =
                                           cont.Orig_System_Reference
                                    AND (   Cont.legacy_Party_Site_Number =
                                               Sites.Orig_System_Reference
                                         OR Cont.Legacy_Party_Site_Number =
                                               Sites.Legacy_Party_Site_Number)));
   BEGIN
      FOR Contact_Rec IN Contact_Cur
      LOOP
         UPDATE XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
            SET Process_status = 'E', Process_Error = 'Orphan Contact Record'
          WHERE ROWID = Contact_Rec.ROWID;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         Fnd_File.put_line (
            fnd_file.output
           ,   ' When Others Error : API update_Contacts_without_sites :'
            || SQLERRM);
         RAISE;
   END update_contacts_without_sites;

   /*************************************************************************
     Procedure :  oe_ship_to

     Purpose:    Another Concurrent Program to process OE SHIP TO sites.
         This program should be called only after main customer
                   Conversion program is processed successfully. API Processes
                   all Customer Sites that are validated and whose customers
                   are already processed through main program and creates
                   Customer Sites and its contacts

                   Procedure inherited from conversion. Currently not used
                   by the Prism Customer Interface
   ************************************************************************/
   PROCEDURE oe_ship_to (errbuf       OUT VARCHAR2
                        ,retcode      OUT VARCHAR2
                        ,prefix    IN     VARCHAR2 DEFAULT NULL)
   IS
      CURSOR get_oe_ship_to_cust
      IS
         SELECT DISTINCT sites.orig_system_reference, cust.account_name
           FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG sites
               ,XXWC.XXWC_AR_CUSTOMER_IFACE_STG cust
          WHERE     hds_site_flag = 'SHIP'
                AND sites.process_status = 'V'
                AND sites.orig_system_reference = cust.orig_system_reference
                AND cust.process_status = 'P'
                AND EXISTS
                       (SELECT 1
                          FROM hz_cust_accounts
                         WHERE account_number = cust.orig_system_reference);

      CURSOR get_oe_ship_to_sites (
         p_customer VARCHAR2)
      IS
         SELECT sites.ROWID rid
               ,sites.*
               ,hcsu.site_use_id
               ,hca.cust_account_id
               ,hca.party_id
           FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG sites
               ,hz_cust_accounts hca
               ,apps.hz_cust_acct_sites hcas
               ,apps.hz_cust_site_uses hcsu
          WHERE     hds_site_flag = 'SHIP'
                AND process_status = 'V'
                AND sites.orig_system_reference = p_customer
                AND sites.orig_system_reference = hca.account_number
                AND hca.cust_account_id = hcas.cust_account_id
                AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                AND hcsu.attribute1 IN ('MSTR', 'MISC')
                AND hcsu.site_use_code = 'BILL_TO'
                AND hcsu.primary_flag = 'Y';

      --Get site contact point
      CURSOR get_site_contact (
         p_cust_site                IN VARCHAR2
        ,p_legacy_customer_number   IN VARCHAR2)
      IS
         SELECT stg.ROWID rid, stg.*
           FROM XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG stg
          WHERE     1 = 1
                AND orig_system_reference = p_cust_site
                AND legacy_party_site_number = p_legacy_customer_number
                AND stg.process_status IN ('V');

      ostatus                       VARCHAR2 (1);
      omsgcount                     NUMBER;
      omsgdata                      VARCHAR2 (2000);
      v_count                       NUMBER;
      l_count                       NUMBER;
      l_proceed                     VARCHAR2 (1);
      l_party                       VARCHAR2 (1);
      l_profile_id                  NUMBER;
      -- party variables
      porganizationrec              hz_party_v2pub.organization_rec_type;
      opartyid                      hz_parties.party_id%TYPE;
      opartynumber                  hz_parties.party_number%TYPE;
      oprofileid                    NUMBER;
      osprofile_id                  NUMBER;
      -- party account variables
      pcustaccountrec               hz_cust_account_v2pub.cust_account_rec_type;
      ppartyrec                     hz_party_v2pub.party_rec_type;
      pprofilerec                   hz_customer_profile_v2pub.customer_profile_rec_type;
      ocustaccountid                hz_cust_accounts.cust_account_id%TYPE;
      ocustaccountno                hz_cust_accounts.account_number%TYPE;
      --location variable
      plocationrec                  hz_location_v2pub.location_rec_type;
      olocationid                   hz_locations.location_id%TYPE;
      --site variables
      ppartysiterec                 hz_party_site_v2pub.party_site_rec_type;
      opartysiteid                  hz_party_sites.party_site_id%TYPE;
      opartysiteno                  hz_party_sites.party_site_number%TYPE;
      --site account avriables
      pcustacctsiterec              hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      ocustacctsiteid               hz_cust_acct_sites.cust_acct_site_id%TYPE;
      --site use variables
      pcustacctsiteuserec           hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile              hz_customer_profile_v2pub.customer_profile_rec_type;
      ocustacctsiteuseid            NUMBER;
      pbilltositeuseid              NUMBER;
      pcustacctid                   NUMBER;
      l_terms_id                    NUMBER;
      l_sales_person_id             NUMBER;
      l_bp                          NUMBER;
      l_territory                   NUMBER;
      oalias                        VARCHAR2 (240);

      -- amount profile variables
      pcustproamtrec                hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      ocustacctprofileamtid         NUMBER;
      pcurrencycode                 VARCHAR2 (10);
      ---------
      whereami                      NUMBER := 0;
      e_validation_exception        EXCEPTION;
      p_result                      VARCHAR2 (1);
      p_mstr_counter                NUMBER;
      p_yard_counter                NUMBER;
      p_ship_counter                NUMBER;

      p_loc_count                   NUMBER;
      p_mstr_loc_count              NUMBER;
      p_yard_loc_count              NUMBER;
      p_ship_loc_count              NUMBER;
      p_job_loc_count               NUMBER;
      p_site_counter                NUMBER;

      req_status                    BOOLEAN;
      v_version_no                  NUMBER;

      p_collector_id                NUMBER;
      p_credit_analyst_id           NUMBER;
      p_credit_classification       ar_lookups.lookup_code%TYPE;
      p_payment_term_id             NUMBER;
      p_object_version_number       NUMBER;
      v_err                         VARCHAR2 (1) := 'N';
      p_cus_err                     VARCHAR2 (4000);
      p_site_err                    VARCHAR2 (4000);
      p_con_err                     VARCHAR2 (4000);
      p_rid                         VARCHAR2 (100);
      l_rec_count                   NUMBER;
      c_max_commit_count   CONSTANT NUMBER := 100;
      l_api_name                    VARCHAR2 (30)
                                       := 'XXWC_CUSTOMER_OE_SHIP_TO';
      lv_primary_ship_to_flag       VARCHAR2 (2);
   BEGIN
      g_parameter_prefix := prefix;
      l_rec_count := 0;

      FOR cust_acc IN get_oe_ship_to_cust
      LOOP
         BEGIN
            SAVEPOINT customer_record;
            l_rec_count := l_rec_count + 1;
            --set the counter

            p_ship_counter := 1;
            p_loc_count := 1;
            p_ship_loc_count := 1;
            p_site_counter := 1;

            FOR cust_sites
               IN get_oe_ship_to_sites (cust_acc.orig_system_reference)
            LOOP
               plocationrec := NULL;
               ppartysiterec := NULL;
               pcustacctsiterec := NULL;
               pcustacctsiteuserec := NULL;
               pcustomerprofile := NULL;
               ostatus := NULL;
               omsgcount := NULL;
               omsgdata := NULL;

               l_count := 0;
               olocationid := NULL;
               ostatus := NULL;

               BEGIN
                  SELECT location_id
                    INTO olocationid
                    FROM hz_locations
                   WHERE location_id = cust_sites.location_id;

                  l_count := 1;
                  ostatus := 'S';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_count := 0;
                     ostatus := NULL;
               END;


               IF l_count = 0
               THEN
                  -- location creation
                  IF NVL (cust_sites.country, 'USA') IN ('USA', 'US')
                  THEN
                     plocationrec.country := 'US';
                  ELSE
                     plocationrec.country := cust_sites.country;
                  END IF;

                  --SatishU : 10/26/2011
                  plocationrec.location_id := cust_sites.location_id; -- added MISC v.sankar 10/31/2011
                  plocationrec.postal_code := (cust_sites.postal_code);
                  plocationrec.address1 :=
                     NVL (cust_sites.address1, 'NO ADDRESS FOUND');
                  plocationrec.address2 := cust_sites.address2;
                  plocationrec.address3 := cust_sites.address3;
                  plocationrec.address4 := cust_sites.address4;
                  plocationrec.state := cust_sites.state;
                  plocationrec.city := cust_sites.city;
                  plocationrec.county := cust_sites.county;
                  plocationrec.created_by_module := 'TCA_V1_API';

                  whereami := 1001;
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,' Before Calling Create Location API for OE SHIP TO');
                  hz_location_v2pub.create_location (
                     p_init_msg_list   => 'T'
                    ,p_location_rec    => plocationrec
                    ,x_location_id     => olocationid
                    ,x_return_status   => ostatus
                    ,x_msg_count       => omsgcount
                    ,x_msg_data        => omsgdata);
               END IF;

               IF ostatus <> 'S'
               THEN
                  whereami := 1002;

                  IF omsgcount > 1
                  THEN
                     FOR i IN 1 .. omsgcount
                     LOOP
                        p_site_err :=
                              NVL (p_site_err, ' ')
                           || SUBSTR (
                                 fnd_msg_pub.get (
                                    i
                                   ,p_encoded   => fnd_api.g_false)
                                ,1
                                ,255);
                     END LOOP;

                     --Satish U: Moving Outside the Loop
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   ' CUSTOMER LOCATION RECORD FOR OE SHIP TO '
                        || SUBSTR (p_site_err, 1, 255));
                  ELSE
                     whereami := 1003;
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,   ' CUSTOMER LOCATION RECORD FOR OE SHIP TO'
                        || omsgdata
                        || '-'
                        || cust_sites.legacy_customer_number
                        || '-'
                        || cust_sites.country
                        || '-'
                        || cust_sites.address1
                        || '-'
                        || cust_sites.address2);
                     p_site_err := omsgdata;
                  END IF;

                  p_rid := cust_sites.rid;
                  whereami := 1004;
                  RAISE e_validation_exception;
               ELSE
                  --initialize the values
                  ostatus := NULL;
                  omsgcount := NULL;
                  omsgdata := NULL;
                  opartysiteid := NULL;
                  opartysiteno := NULL;
                  -- create a party site now
                  ppartysiterec.party_id := cust_sites.party_id;
                  ppartysiterec.location_id := olocationid;

                  --v.sankar 11/22/2011. needs to be fixed next round.
                  ppartysiterec.party_site_number :=
                     SUBSTR (cust_sites.legacy_party_site_number, 1, 25);


                  ppartysiterec.party_site_name :=
                        cust_sites.hds_site_flag
                     || '-'
                     || LPAD (p_ship_counter, 2, '0');
                  p_ship_counter := p_ship_counter + 1;



                  osprofile_id := NULL;
                  ppartysiterec.orig_system_reference :=
                     prefix || cust_sites.orig_system_reference;
                  ppartysiterec.created_by_module := 'TCA_V1_API';
                  whereami := 1005;
                  fnd_file.put_line (
                     fnd_file.LOG
                    ,' Before Calling Create Party Site API for OE Ship To');
                  hz_party_site_v2pub.create_party_site (
                     p_init_msg_list       => 'T'
                    ,p_party_site_rec      => ppartysiterec
                    ,x_party_site_id       => opartysiteid
                    ,x_party_site_number   => opartysiteno
                    ,x_return_status       => ostatus
                    ,x_msg_count           => omsgcount
                    ,x_msg_data            => omsgdata);


                  IF ostatus <> 'S'
                  THEN
                     whereami := 1006;

                     IF omsgcount > 1
                     THEN
                        FOR i IN 1 .. omsgcount
                        LOOP
                           p_site_err :=
                                 p_site_err
                              || SUBSTR (
                                    fnd_msg_pub.get (
                                       i
                                      ,p_encoded   => fnd_api.g_false)
                                   ,1
                                   ,255);
                        END LOOP;

                        -- Satish U: Moving Outside the Loop
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   ' CUSTOMER PARTY SITE RECORD FOR OE SHIP TO'
                           || SUBSTR (p_site_err, 1, 255));
                     ELSE
                        whereami := 1007;
                        fnd_file.put_line (
                           fnd_file.LOG
                          ,   ' CUSTOMER  PARTY SITE RECORD FOR OE SHIP TO'
                           || omsgdata);
                        p_site_err := omsgdata;
                     END IF;

                     p_rid := cust_sites.rid;
                     RAISE e_validation_exception;
                  ELSE
                     whereami := 1008;
                     --initialize the values
                     ostatus := NULL;
                     omsgcount := NULL;
                     omsgdata := NULL;
                     ocustacctsiteid := NULL;

                     pcustacctsiterec.cust_account_id :=
                        cust_sites.cust_account_id;
                     pcustacctsiterec.party_site_id := opartysiteid;
                     pcustacctsiterec.created_by_module := 'TCA_V1_API';

                     pcustacctsiterec.orig_system_reference :=
                           prefix
                        || cust_sites.legacy_customer_number
                        || '-'
                        || p_site_counter;
                     p_site_counter := p_site_counter + 1;
                     pcustacctsiterec.org_id := g_org_id;

                     /*DFF Name ========
                     Address Information (Account Site level)*/

                     --Notice to Owner
                     pcustacctsiterec.attribute_category :=
                        cust_sites.attribute4;                        --'Yes';

                     pcustacctsiterec.attribute1 := cust_sites.attribute16;

                     IF UPPER (cust_sites.attribute15) = 'YES'
                     THEN
                        pcustacctsiterec.attribute3 := 'Y'; --Y N NULL Mandatory PO Number
                     ELSIF UPPER (cust_sites.attribute15) = 'NO'
                     THEN
                        pcustacctsiterec.attribute3 := 'N';
                     END IF;

                     IF cust_sites.attribute5 IS NOT NULL
                     THEN
                        pcustacctsiterec.attribute5 :=
                           TO_CHAR (
                              TO_DATE (cust_sites.attribute5, 'YYYYMMDD')
                             ,'YYYY/MM/DD HH24:MI:SS');
                     --pcustacctsiterec.attribute5:=null; --cust_sites.attribute5; --to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');  --Lien Release Date
                     END IF;

                     whereami := 1009;
                     --Mandatory Notes
                     pcustacctsiterec.attribute18 := cust_sites.attribute18; --Notice to Owner-Job Total
                     pcustacctsiterec.attribute19 := cust_sites.attribute19; --'Notice to Owner-Prelim Notice';  --Notice to Owner-Prelim Notice

                     IF cust_sites.attribute20 IS NOT NULL
                     THEN
                        pcustacctsiterec.attribute20 :=
                           TO_CHAR (
                              TO_DATE (cust_sites.attribute20, 'YYYYMMDD')
                             ,'YYYY/MM/DD HH24:MI:SS');
                     --pcustacctsiterec.attribute20:=null; --cust_sites.attribute20;--to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');--Notice to Owner-Prelim Date
                     END IF;

                     whereami := 1010;
                     fnd_file.put_line (
                        fnd_file.LOG
                       ,' Before Calling Create Customer Account Site API for OE Ship To');
                     hz_cust_account_site_v2pub.create_cust_acct_site (
                        p_init_msg_list        => 'T'
                       ,p_cust_acct_site_rec   => pcustacctsiterec
                       ,x_cust_acct_site_id    => ocustacctsiteid
                       ,x_return_status        => ostatus
                       ,x_msg_count            => omsgcount
                       ,x_msg_data             => omsgdata);

                     IF ostatus <> 'S'
                     THEN
                        whereami := 1011;

                        --fnd_file.put_line(fnd_file.log,'1117- Create Customer Account Site'||omsgcount);
                        IF omsgcount > 1
                        THEN
                           FOR i IN 1 .. omsgcount
                           LOOP
                              p_site_err :=
                                    NVL (p_site_err, ' ')
                                 || SUBSTR (
                                       fnd_msg_pub.get (
                                          i
                                         ,p_encoded   => fnd_api.g_false)
                                      ,1
                                      ,255);
                           END LOOP;

                           -- Satish U: Moving it outside the Loop
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   ' CUSTOMER ACCOUNT SITE RECORD FOR OE SHIP TO'
                              || cust_acc.account_name
                              || ' '
                              || SUBSTR (p_site_err, 1, 255));
                        ELSE
                           whereami := 1012;
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   ' CUSTOMER ACCOUNT SITE RECORD FOR OE SHIP TO'
                              || cust_acc.account_name
                              || ' '
                              || omsgdata);
                           p_site_err := omsgdata;
                        END IF;

                        p_rid := cust_sites.rid;
                        RAISE e_validation_exception;
                     ELSE
                        whereami := 1013;

                        --initialize the values
                        pcustacctsiteuserec := NULL;
                        ostatus := NULL;
                        omsgcount := NULL;
                        omsgdata := NULL;
                        ocustacctsiteuseid := NULL;

                        pcustacctsiteuserec := NULL;
                        pcustomerprofile := NULL;                  -- 01/14/09
                        pcustacctsiteuserec.site_use_code := 'SHIP_TO';
                        --get bill to site use id for ship to site
                        pcustacctsiteuserec.bill_to_site_use_id :=
                           cust_sites.site_use_id;
                        pcustacctsiteuserec.primary_flag := 'N';

                        pcustacctsiteuserec.cust_acct_site_id :=
                           ocustacctsiteid;

                        --populate location
                        IF     cust_sites.job_name IS NOT NULL
                           AND cust_sites.job_number IS NOT NULL
                        THEN
                           pcustacctsiteuserec.location :=
                              SUBSTR (
                                    UPPER (cust_sites.job_name)
                                 || '-'
                                 || cust_sites.job_number
                                ,1
                                ,40);
                        ELSIF     cust_sites.legacy_party_site_name
                                     IS NOT NULL
                              AND cust_sites.legacy_customer_number
                                     IS NOT NULL
                        THEN
                           pcustacctsiteuserec.location :=
                              SUBSTR (
                                    UPPER (cust_sites.legacy_party_site_name)
                                 || '-'
                                 || UPPER (cust_sites.legacy_customer_number)
                                ,1
                                ,40);
                        ELSIF     cust_sites.legacy_party_site_name IS NULL
                              AND cust_sites.legacy_customer_number
                                     IS NOT NULL
                        THEN
                           pcustacctsiteuserec.location :=
                                 'SHIP-'
                              || UPPER (cust_sites.legacy_customer_number);
                        ELSIF     cust_sites.legacy_party_site_name
                                     IS NOT NULL
                              AND cust_sites.legacy_customer_number IS NULL
                        THEN
                           pcustacctsiteuserec.location :=
                              SUBSTR (
                                    UPPER (cust_sites.legacy_party_site_name)
                                 || '-'
                                 || LPAD (p_loc_count, 2, '0')
                                ,1
                                ,40);
                           p_loc_count := p_loc_count + 1;
                        ELSIF     cust_sites.legacy_party_site_name IS NULL
                              AND cust_sites.legacy_customer_number IS NULL
                        THEN
                           pcustacctsiteuserec.location :=
                              'SHIP-' || LPAD (p_ship_loc_count, 2, '0');
                        END IF;

                        --whereami := 1014;

                        --Customer Site Classification

                        pcustacctsiteuserec.attribute1 := 'SHIP';
                        pcustacctsiteuserec.attribute5 := 'SHIP';
                        pcustacctsiteuserec.attribute3 :=
                           cust_sites.attribute11;


                        pcustacctsiteuserec.created_by_module := 'TCA_V1_API';

                        BEGIN
                           SELECT salesrep_id
                             INTO l_sales_person_id
                             FROM ra_salesreps
                            WHERE     1 = 1
                                  --and org_id=fnd_profile.value('ORG_ID')
                                  AND salesrep_number =
                                         cust_sites.primary_salesrep_number
                                  AND ROWNUM = 1;
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              whereami := 1014;

                              SELECT salesrep_id
                                INTO l_sales_person_id
                                FROM ra_salesreps
                               WHERE name = 'No Sales Credit' AND ROWNUM = 1;
                        END;


                        pcustacctsiteuserec.primary_salesrep_id :=
                           l_sales_person_id;

                        --added by v.sankar on 11/22/2011
                        pcustacctsiteuserec.gsa_indicator :=
                           cust_sites.gsa_indicator;



                        IF UPPER (cust_sites.freight_term) = 'FREIGHT EXEMPT'
                        THEN
                           pcustacctsiteuserec.freight_term := 'EXEMPT';
                        ELSE
                           pcustacctsiteuserec.freight_term := NULL;
                        END IF;


                        fnd_file.put_line (
                           fnd_file.LOG
                          ,' Before Calling Create Customer Site Use API for OE SHIP TO');

                        hz_cust_account_site_v2pub.create_cust_site_use (
                           p_init_msg_list          => 'T'
                          ,p_cust_site_use_rec      => pcustacctsiteuserec
                          ,p_customer_profile_rec   => pcustomerprofile
                          ,p_create_profile         => 'F'
                          ,p_create_profile_amt     => 'F'
                          ,x_site_use_id            => ocustacctsiteuseid
                          ,x_return_status          => ostatus
                          ,x_msg_count              => omsgcount
                          ,x_msg_data               => omsgdata);
                        --pbilltositeuseid := ocustacctsiteuseid ;
                        pcustacctid := ocustaccountid;

                        IF ostatus <> 'S'
                        THEN
                           whereami := 1015;
                           l_proceed := 'N';
                           fnd_file.put_line (
                              fnd_file.LOG
                             ,   ' Create Customer Site Use API Errored Out for OE SHIP TO- Msg Count :'
                              || omsgcount);

                           IF omsgcount > 1
                           THEN
                              FOR i IN 1 .. omsgcount
                              LOOP
                                 p_site_err :=
                                       NVL (p_site_err, ' ')
                                    || SUBSTR (
                                          fnd_msg_pub.get (
                                             i
                                            ,p_encoded   => fnd_api.g_false)
                                         ,1
                                         ,255);
                              END LOOP;

                              -- Satish U: 11/02/11 : Moved code out of the loop
                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,   'CUSTOMER SHIP_TO SITE USE RECORD FOR OE SHIP TO'
                                 || SUBSTR (p_site_err, 1, 255));
                           ELSE
                              whereami := 1016;
                              fnd_file.put_line (
                                 fnd_file.LOG
                                ,   'CUSTOMER SHIP_TO SITE USE RECORD FOR OE SHIP TO '
                                 || omsgdata
                                 || '-'
                                 || cust_acc.account_name);
                              p_site_err := omsgdata;
                           END IF;

                           p_rid := cust_sites.rid;
                           RAISE e_validation_exception;
                        END IF;


                        l_proceed := 'Y';
                     END IF;                             -- party site account

                     --create party site level communication
                     whereami := 1017;
                  END IF;                                        -- party site
               END IF;                                              --location

               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_status = 'P'
                WHERE ROWID = cust_sites.rid;
            END LOOP;

            --Sites
            IF l_rec_count >= c_max_commit_count
            THEN
               COMMIT;                            --Commit the single customer
               l_rec_count := 0;
            END IF;
         EXCEPTION
            WHEN e_validation_exception
            THEN
               --whereami      := 590;
               v_err := 'Y';
               ROLLBACK TO customer_record;
               upd_stagin_table_err (p_cus_err
                                    ,p_site_err
                                    ,p_con_err
                                    ,p_rid);
               p_cus_err := NULL;
               p_site_err := NULL;
               p_con_err := NULL;

               --upd_stagin_table (cust_acc.orig_system_reference);
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_status = 'E'
                WHERE     orig_system_reference =
                             cust_acc.orig_system_reference
                      AND hds_site_flag = 'SHIP';

               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'e_Validation_Exception IN PROCEDURE for customer OE Ship To: '
                  || cust_acc.orig_system_reference
                  || '---'
                  || l_api_name
                  || '---'
                  || TO_CHAR (whereami));
            WHEN OTHERS
            THEN
               -- whereami  := 600;
               ROLLBACK TO customer_record;
               v_err := 'Y';
               --SatishU: 11/01/11    Added Following SQL Statement
               upd_stagin_table_err (p_cus_err
                                    ,p_site_err
                                    ,p_con_err
                                    ,p_rid);
               p_cus_err := NULL;
               p_site_err := NULL;
               p_con_err := NULL;

               --upd_stagin_table (cust_acc.orig_system_reference);
               UPDATE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
                  SET process_status = 'E'
                WHERE     orig_system_reference =
                             cust_acc.orig_system_reference
                      AND hds_site_flag = 'SHIP';


               fnd_file.put_line (
                  fnd_file.LOG
                 ,   'When Others Error in PROCEDURE for customer OE Ship To: '
                  || cust_acc.orig_system_reference
                  || '---'
                  || l_api_name
                  || SQLERRM
                  || '---'
                  || TO_CHAR (whereami));
         END;
      END LOOP;                                               --close cust acc

      COMMIT;                           -- Commit after the program completes.
      get_cust_conversion_stats;

      IF v_err = 'Y'
      THEN
         req_status := fnd_concurrent.set_completion_status ('WARNING', NULL);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END oe_ship_to;

   /*************************************************************************
     Procedure:  pop_org_web_contact

     Purpose:    Procedure to create WEB Contact at Party or Party Site Level.
         Procedure called by customer_inteface
   ************************************************************************/
   PROCEDURE pop_org_web_contact (p_level         IN     VARCHAR2
                                 ,p_party_id      IN     NUMBER
                                 ,p_cod_comment   IN     VARCHAR2
                                 ,p_keyword       IN     VARCHAR2
                                 ,p_out              OUT VARCHAR2
                                 ,p_err_msg          OUT VARCHAR2)
   IS
      --contact variables
      pcontactpointrec   hz_contact_point_v2pub.contact_point_rec_type;
      pedirec            hz_contact_point_v2pub.edi_rec_type;
      pemailrec          hz_contact_point_v2pub.email_rec_type;
      pphonerec          hz_contact_point_v2pub.phone_rec_type;
      ptelexrec          hz_contact_point_v2pub.telex_rec_type;
      pwebrec            hz_contact_point_v2pub.web_rec_type;
      ocontactpointid    NUMBER;

      ostatus            VARCHAR2 (1);
      omsgcount          NUMBER;
      omsgdata           VARCHAR2 (2000);
   BEGIN
      ostatus := NULL;
      omsgcount := NULL;
      omsgdata := NULL;
      pcontactpointrec := NULL;
      pcontactpointrec.contact_point_type := 'WEB';

      IF p_level = 'PARTY'
      THEN
         pcontactpointrec.owner_table_name := 'HZ_PARTIES';
      ELSE
         pcontactpointrec.owner_table_name := 'HZ_PARTY_SITES';
      END IF;

      pcontactpointrec.owner_table_id := p_party_id;
      pcontactpointrec.primary_flag := 'Y';
      pcontactpointrec.contact_point_purpose := 'HOMEPAGE';
      pwebrec.web_type := 'HTTP';

      IF p_cod_comment IS NULL AND p_keyword IS NOT NULL
      THEN
         pwebrec.url := p_keyword;
      ELSIF p_cod_comment IS NOT NULL AND p_keyword IS NULL
      THEN
         pwebrec.url := p_cod_comment;
      ELSE
         pwebrec.url := p_cod_comment || ' - ' || p_keyword;
      END IF;

      pcontactpointrec.created_by_module := 'TCA_V1_API';

      hz_contact_point_v2pub.create_contact_point (
         p_init_msg_list       => 'T'
        ,p_contact_point_rec   => pcontactpointrec
        ,p_web_rec             => pwebrec
        ,x_contact_point_id    => ocontactpointid
        ,x_return_status       => ostatus
        ,x_msg_count           => omsgcount
        ,x_msg_data            => omsgdata);

      IF ostatus <> 'S'
      THEN
         IF omsgcount > 1
         THEN
            FOR i IN 1 .. omsgcount
            LOOP
               fnd_file.put_line (
                  fnd_file.output
                 ,   'Org Web contact point '
                  || SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false)
                            ,1
                            ,255));
               p_err_msg :=
                     p_err_msg
                  || SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false)
                            ,1
                            ,255);
            END LOOP;
         ELSE
            fnd_file.put_line (fnd_file.output
                              ,'Org Web contact point ' || omsgdata);
            p_err_msg := omsgdata;
         END IF;

         p_out := 'E';
         RETURN;
      END IF;
   END pop_org_web_contact;

   /*************************************************************************
     Procedure:  process_files

     Purpose:    Main procedure that sequences the customer data validation
         and then the load of the valid data
   ************************************************************************/
   PROCEDURE process_files (errbuf                        OUT VARCHAR2
                           ,retcode                       OUT VARCHAR2
                           ,p_process_type             IN     VARCHAR2
                           ,p_customer_file_name       IN     VARCHAR2
                           ,p_cust_site_file_name      IN     VARCHAR2
                           ,p_cust_contact_file_name   IN     VARCHAR2)
   IS
      v_request_id     NUMBER;
      v_boolval_mode   BOOLEAN;
      v_boolval_temp   BOOLEAN;

      v_req_stat       BOOLEAN;
      v_phase          VARCHAR2 (20);
      v_status         VARCHAR2 (20);
      v_dev_phase      VARCHAR2 (100);
      v_dev_status     VARCHAR2 (100);
      v_message        VARCHAR2 (100);
      v_wait           NUMBER;

      v_retcode        VARCHAR2 (240);
      v_errbuf         VARCHAR2 (240);
   BEGIN
      -- May need if called from UC4?
      fnd_global.apps_initialize (fnd_global.user_id
                                 ,fnd_global.resp_id
                                 ,fnd_global.resp_appl_id);

      g_request_id := fnd_global.conc_request_id;

      fnd_file.put_line (fnd_file.LOG
                        ,'Begining of Customer Validation Process');
      validate_data;
      fnd_file.put_line (fnd_file.LOG
                        ,'Customer Validation Process is Complete');

      COMMIT;

      fnd_file.put_line (fnd_file.LOG, 'Begining of Customer Load Process');
      customer_interface (errbuf    => v_errbuf
                         ,retcode   => v_retcode
                         ,p_mode    => NULL
                         ,prefix    => NULL);
      fnd_file.put_line (fnd_file.LOG
                        ,'Customer Validation Load is Complete');
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error in process_files:' || SQLERRM;
         retcode := '2';
   --Write_Error (errbuf);
   END process_files;

   /*************************************************************************
     Procedure:  submit_job

     Purpose:    Procedure called by UC4 to initiate the customer
         interface code. Starts with validation and moves on
                   to actual load of customer/site records
   ************************************************************************/
   PROCEDURE submit_job (errbuf                     OUT VARCHAR2
                        ,retcode                    OUT VARCHAR2
                        ,p_process_type          IN     VARCHAR2
                        ,p_user_name             IN     VARCHAR2
                        ,p_responsibility_name   IN     VARCHAR2)
   IS
      -- Variable definitions
      l_package               VARCHAR2 (50) := 'XXWC_AR_PRISM_CUST_IFACE_PKG';
      l_email                 VARCHAR2 (200) := 'HDSOracleDevelopers@hdsupply.com';

      l_req_id                NUMBER NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_statement             VARCHAR2 (9000);
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;

      l_err_callfrom          VARCHAR2 (75) DEFAULT 'XXCUS_ERROR_PKG';
      l_err_callpoint         VARCHAR2 (75) DEFAULT 'START';
      l_distro_list           VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      -- Deriving Ids from variables
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     user_name = UPPER (p_user_name)
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility '
               || p_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for '
               || p_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      -- Apps Initialize
      FND_GLOBAL.APPS_INITIALIZE (l_user_id
                                 ,l_responsibility_id
                                 ,l_resp_application_id);

      -- Submitting program XXWC Prism-EBS Customer Interface Processing
      l_req_id :=
         fnd_request.submit_request (
            application   => 'XXWC'
           ,program       => 'XXWC_AR_PRISM_EBS_CUST_IFACE'
           ,description   => NULL
           ,start_time    => SYSDATE
           ,sub_request   => FALSE
           ,argument1     => p_process_type
           ,argument2     => NULL
           ,argument3     => NULL
           ,argument4     => NULL);

      COMMIT;

      DBMS_OUTPUT.put_line (
         'Concurrent Program Request Submitted. Request ID: ' || l_req_id);

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id
                                            ,6
                                            ,3600 -- 04/17/2012 Changed from 15000
                                            ,v_phase
                                            ,v_status
                                            ,v_dev_phase
                                            ,v_dev_status
                                            ,v_message)
         THEN
            v_error_message :=
                  'ReqID:'
               || l_req_id
               || '-DPhase:'
               || v_dev_phase
               || '-DStatus:'
               || v_dev_status
               || CHR (10)
               || 'MSG:'
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'EBS Conc Prog Completed with problems '
                  || v_error_message
                  || '.';
               l_err_msg := l_statement;
               DBMS_OUTPUT.put_line (l_statement);
               RAISE PROGRAM_ERROR;
            ELSE
               retcode := 1;
            END IF;
         ELSE
            l_statement := 'EBS Conc Program Wait timed out';
            l_err_msg := l_statement;
            DBMS_OUTPUT.put_line (l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_statement := 'EBS Conc Program not initated';
         l_err_msg := l_statement;
         DBMS_OUTPUT.put_line (l_statement);
         RAISE PROGRAM_ERROR;
      END IF;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000))
                   ,1
                   ,3000);
         DBMS_OUTPUT.put_line (l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000)
           ,p_error_desc          => SUBSTR (l_err_msg, 1, 2000)
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'AR');
         retcode := l_err_code;
         errbuf := l_err_msg;
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000))
                   ,1
                   ,3000);
         DBMS_OUTPUT.put_line (l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000)
           ,p_error_desc          => SUBSTR (l_err_msg, 1, 2000)
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'AR');

         retcode := l_err_code;
         errbuf := l_err_msg;
   END submit_job;

   /*************************************************************************
     Procedure:  archive_data

     Purpose:    Procedure used to archive processed records from the
         main staging tables to the archiving tables
   ************************************************************************/
   PROCEDURE archive_data (errbuf                OUT VARCHAR2
                          ,retcode               OUT VARCHAR2
                          ,p_archive_type     IN     VARCHAR2
                          ,p_statuses         IN     VARCHAR2
                          ,p_from_proc_date   IN     VARCHAR2
                          ,p_to_proc_date     IN     VARCHAR2)
   IS
   BEGIN
      NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END archive_data;
END xxwc_ar_prism_cust_iface_pkg;
/

