create or replace 
PACKAGE BODY      XXWC_AUTHBUYER_DACT_WEBADI_PKG
/********************************************************************************
FILE NAME: APPS.XXWC_AUTHBUYER_DACT_WEBADI_PKG.pkg

PROGRAM TYPE: PL/SQL Package Body

PURPOSE:  API to de-active the customer contact roles for auth buyers in Oracle.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)                DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     10/23/2017    Pattabhi Avula    TMS#20171003-00065 Initial version
********************************************************************************/
AS


   PROCEDURE IMPORT_CUST_ACCOUNTS (p_role             IN VARCHAR2,
                                   p_account_number   IN VARCHAR2,
                                   p_cust_acct_id     IN NUMBER
                                   )
   IS
      -- contact acct
      l_cr_cust_acc_role_rec           hz_cust_account_role_v2pub.cust_account_role_rec_type;
      l_cust_account_role_id           NUMBER;
      
      --contact variables
      l_contact_point_id               NUMBER;
      l_object_version_number          NUMBER;
      l_party_id                       NUMBER;
      l_cust_account_id                NUMBER;
	  l_dff_val                        VARCHAR2 (5);
      l_api_name                       VARCHAR2 (30) := 'ONT_UI_ADD_CUSTOMER';

      l_status                         VARCHAR2 (1);
      l_msg_count                      NUMBER;
      l_msg_data                       VARCHAR2 (2000);
      l_error_message                  VARCHAR2 (5000);
	  l_active_buyer_cnt               NUMBER;

      l_program_error                  EXCEPTION;
      
   BEGIN
      MO_GLOBAL.SET_POLICY_CONTEXT ('S', 162);
      MO_GLOBAL.INIT ('AR');
      DBMS_OUTPUT.PUT_LINE('Started Import');

     
         --------------------------------------------------------
         -- Derive party id
         --------------------------------------------------------
         BEGIN
            SELECT party_id, cust_account_id, ATTRIBUTE7
              INTO l_party_id, l_cust_account_id, l_dff_val
              FROM hz_cust_accounts
             WHERE account_number = p_account_number;
         EXCEPTION
            WHEN OTHERS THEN
               l_error_message := 'Invalid Account Number';
               RAISE l_program_error;
         END;

         DBMS_OUTPUT.PUT_LINE('Party Id '||l_party_id || ' cust_account_id '|| l_cust_account_id);
		 
		  IF l_cust_account_id  IS NULL 
		   THEN
		   l_error_message:='Invalid Account Number';
	      RAISE l_program_error;
	      END IF;
         -----------------------------------------------------------
        -- Deactivating Authorize Buyer count of roles for this Account
         -------------------------------------------------------------
        BEGIN		 
		 SELECT COUNT(1)
		   INTO l_active_buyer_cnt
           FROM apps.hz_cust_account_roles hcar,
                apps.hz_role_responsibility hrr
          WHERE hcar.cust_account_role_id = hrr.cust_account_role_id
            AND hcar.cust_account_id      = l_cust_account_id
            AND TRUNC(hrr.creation_date) <= TRUNC (SYSDATE)
            AND hrr.RESPONSIBILITY_TYPE   = 'AUTH_BUYER'
		    AND hcar.STATUS='A';
		EXCEPTION
		 WHEN OTHERS THEN
         l_error_message:='This customer account do not have any valid authorized buyer contacts';
	      RAISE l_program_error;
	      END;		 
		

		IF   l_active_buyer_cnt > 0 AND (l_dff_val='Y' OR l_dff_val='N' OR l_dff_val IS NULL)
		 THEN
          FOR rec IN (SELECT hcar.cust_account_role_id,
		                     hcar.cust_account_id,
                             hcar.party_id,
                             hcar.status,
                             hcar.object_version_number
                        FROM apps.hz_cust_account_roles hcar,
                             apps.hz_role_responsibility hrr
                       WHERE hcar.cust_account_role_id = hrr.cust_account_role_id
                         AND cust_account_id           = l_cust_account_id
                         AND TRUNC (hrr.creation_date) < TRUNC (SYSDATE)
                         AND hrr.RESPONSIBILITY_TYPE   = 'AUTH_BUYER'
		              	 AND hcar.STATUS='A'
					 )            
          LOOP
              DBMS_OUTPUT.PUT_LINE(' making inactive specified Authorize Buyers ' ||rec.cust_account_role_id);
              DBMS_OUTPUT.PUT_LINE('  for cust account id '||rec.cust_account_id);
              DBMS_OUTPUT.PUT_LINE('  object_version_number '||rec.object_version_number);
            
              IF rec.object_version_number is NULL THEN
                 l_object_version_number                     := 1;
              ELSE
                 l_object_version_number                     := rec.object_version_number;
              END IF;
            
              l_cr_cust_acc_role_rec                      := NULL;
              --l_cr_cust_acc_role_rec.party_id             := l_rel_party_id;
              l_cr_cust_acc_role_rec.cust_account_role_id := rec.cust_account_role_id;
              l_cr_cust_acc_role_rec.cust_account_id      := rec.cust_account_id;
              l_cr_cust_acc_role_rec.status               := 'I';
            
            
              HZ_CUST_ACCOUNT_ROLE_V2PUB.update_cust_account_role ('T',
                                                                    l_cr_cust_acc_role_rec,
                                                                    l_object_version_number,
                                                                    l_status,
                                                                    l_msg_count,
                                                                    l_msg_data);
              IF l_msg_count >= 1 THEN
                  FOR I IN 1 .. l_msg_count
               LOOP
                 l_error_message :=
                       l_error_message
                    || ' . '
                    || SUBSTR (
                          FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                          1,
                          255);
                dbms_output.put_line(I||'. '||SubStr(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
               END LOOP;
               l_error_message:= l_error_message || l_msg_data;
               RAISE l_program_error;
              END IF;            
          END LOOP;

            DBMS_OUTPUT.PUT_LINE(' This customer account do not have any valid authorized buyer contacts');
           -- No Buyers set up previously so updating Buyer required DFF
           UPDATE apps.HZ_CUST_ACCOUNTS_ALL
           SET    ATTRIBUTE7  = 'N'
		         ,ATTRIBUTE8 = NULL							  
           WHERE  cust_account_id= l_cust_account_id;
           COMMIT;
		   
		ELSE
		  
		     DBMS_OUTPUT.PUT_LINE(' Updating Buyer required DFF as No AND Buyer Notes as NULL');
		     l_error_message:='Invalid Account or Inactive Role';
             RAISE l_program_error;	 
		END IF;   
     
   EXCEPTION
      WHEN l_program_error THEN

         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_ADI_ERROR_MSG');
         FND_MESSAGE.SET_TOKEN ('ERROR_MESSAGE', l_error_message);
         DBMS_OUTPUT.PUT_LINE('Error: '||l_error_message);

      WHEN OTHERS THEN

         l_error_message := 'Unidentified error - ' || SQLERRM;
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_ADI_ERROR_MSG');
         FND_MESSAGE.SET_TOKEN ('ERROR_MESSAGE', l_error_message);
         DBMS_OUTPUT.PUT_LINE('Error: '||l_error_message);

   END IMPORT_CUST_ACCOUNTS;
END XXWC_AUTHBUYER_DACT_WEBADI_PKG;
/