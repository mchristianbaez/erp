CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CUST_INACTIVATE_SITE_PKG
AS
   /****************************************************************************************************
    *
    * FUNCTION | PROCEDURE | CURSOR
    *  XXWC_CUST_INACTIVATE_SITE_PKG
    *
    * DESCRIPTION
    *  FIN / Mass update of accounts that haven't had activity within the last XX month
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
   .* ----------------- -------- ------------------------------------------------------------------------
    * P_MODE           <IN>       Report Only shows only report and commit will inactivate sites
    * P_CLASS          <IN>       Class name
    * P_MONTH          <IN>       No of months
    * P_AS_OF_DATE     <IN>       Report run date
    * P_EMAIL          <IN>       Email report output needs to be send
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
    *
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ----------------------------------------------------------------
    * 1.0    11/11/2013   Maharajan S     Creation
    * 1.1    03/03/2014   Maharajan S     TMS 20140226-00194,changed the directory path for purging
    *                                      output files
    * 1.2    06/26/2014   Maharajan S     TMS# 20140530-00240 Fix not to include Booked, Entered, Draft and 
    *                                     Pending Customer Acceptance statuses at the Order Header Level.
    * 1.3    07/24/2014   Maharajan S     TMS# 20140220-00170 Added org id condition
	* 1.4    11/28/2016   Pattabhi Avula  TMS#20160606-00273 FTP helper developed in UC4 for output 
	*                                     file transfer to destination shared drive as below
	*                                     P:\087 Accounts Receivable\Inactivated Accounts Sites Reports
    ******************************************************************************************************/

   PROCEDURE XXWC_CUST_INACTIVATE_SITE_PRC (errbuf       OUT VARCHAR2,
                                            retcode      OUT NUMBER,
                                            p_resp        IN VARCHAR2,
                                            P_resp_status IN VARCHAR2,
                                            p_mode    IN     VARCHAR2,
                                            p_class   IN     VARCHAR2,
                                            p_month   IN     NUMBER,
                                            p_date    IN     DATE,
                                            p_email   IN     VARCHAR2)
   IS
      l_party_site_rec   hz_party_site_v2pub.PARTY_SITE_REC_TYPE;
      l_obj_num          NUMBER := 2;
      l_return_status    VARCHAR2 (1);
      l_msg_count        NUMBER;
      l_msg_data         VARCHAR2 (2000);
      l_error_message    VARCHAR2 (2000);
      v_data_file_name   VARCHAR2 (1000);
      v_email_subject    VARCHAR2 (100);
      n_req_id           NUMBER := 0;
      v_email            fnd_user.email_address%TYPE := NULL;
      v_cp_long_name     fnd_concurrent_programs_tl.user_concurrent_program_name%TYPE        := NULL;
      v_path             VARCHAR2 (80)  := 'XXWC_INACTIVE_CUST_SITE';                        --Added for ver 1.1
      ln_request_id      NUMBER := 0;
      v_child_requests   VARCHAR2 (2);
      output_file_id     UTL_FILE.FILE_TYPE;
      v_header           VARCHAR2 (2000) := 'Account Number|Account Name|Location Number|Location|Collector|Profile Class|Site Classification|Last Sale Date|Last Payment Date|Order Date|Account Receivable balance';
      v_detail           VARCHAR2 (3000);
      l_as_of_date       DATE;
      l_class            VARCHAR2 (30);
      l_msg                    VARCHAR2 (150);
      l_distro_list            VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_req_id                 NUMBER        := fnd_global.conc_request_id;
      l_user_id                NUMBER        := fnd_global.user_id;
      l_sec                    VARCHAR2(200);      
      l_bill_to_site           NUMBER;
      l_bill_sale_date         DATE;
      l_ship_to_site           NUMBER;
      l_ship_sale_date         DATE;
      l_cust_site_use          NUMBER;
      l_payment_date           DATE ;
      l_account_bal            NUMBER;
      l_invoice_site_use       NUMBER;
      l_invoice_order_date     DATE;
      l_ship_site_use          NUMBER;
      l_ship_order_date        DATE;
     
      CURSOR xxwc_inactive_site_cur (l_class_name    VARCHAR2)
      IS
     SELECT /*+leading(hca)*/ acct_site.party_site_id,
         cust.account_number,
         cust.account_name,
         party_site.party_site_number,
         site_uses.site_use_id,
         site_uses.location,
         coll.name collector,
         pc.name profile_class,
         NVL (party_site.object_version_number, 1) object_version_number,
         site_uses.attribute1 site_classification
    FROM AR.hz_cust_accounts cust,
         AR.hz_parties party,
         AR.hz_party_sites party_site,
         apps.hz_cust_acct_sites acct_site,          
         apps.hz_cust_site_uses site_uses,       
         AR.hz_locations loc,
         AR.hz_customer_profiles prof,
         AR.hz_cust_profile_classes pc,    
         AR.ar_collectors coll
   WHERE cust.party_id                         = party.party_id
    AND  party.party_id                        = party_site.party_id
    AND  cust.cust_account_id                  = acct_site.cust_account_id
    AND  acct_site.party_site_id               = party_site.party_site_id
    AND acct_site.cust_acct_site_id            = site_uses.cust_acct_site_id
    AND loc.location_id                        = party_site.location_id
    AND prof.site_use_id                       = site_uses.site_use_id   
    AND prof.profile_class_id                  = pc.profile_class_id
    AND prof.collector_id                      = coll.collector_id(+)
    AND site_uses.org_id                       = acct_site.org_id		--Added below two conditions for ver 1.3
  --  AND site_uses.org_id = 162                       -- 23/09/2014 Commented by pattabhi for Canada & US OU Testing
    AND NVL (acct_site.status, 'A')         = 'A'
    AND NVL (site_uses.status, 'A')         = 'A'
    AND NVL (cust.status, 'A')              = 'A'
    AND pc.name NOT IN ('DEFAULT', 
                         'Intercompany Customers', 
                         'WC Branches')
    AND site_uses.attribute1 IS NOT NULL
    AND site_uses.attribute1                    = NVL (l_class_name, site_uses.attribute1)
    AND site_uses.primary_flag = 'N';
   BEGIN
      n_req_id := fnd_global.conc_request_id;

     fnd_file.put_line (fnd_file.LOG, 'P_CLASS: '||P_class);

      ----------------------------------------------------------------
      -- Validation for Class parameter
      ----------------------------------------------------------------

      IF p_class = 'ALL'
      THEN
         l_class := NULL;
      ELSE
         l_class := p_class;
      END IF;

    fnd_file.put_line (fnd_file.LOG, 'L_CLASS: '||l_class);
    fnd_file.put_line (fnd_file.LOG, 'P_MODE: '||p_mode);

      ----------------------------------------------------------------
      -- Opening file
      ----------------------------------------------------------------

         BEGIN

           l_sec := ' Opening file to write data';

            v_data_file_name :=
               'HDS_Inactive_customer_Sites' || '_' || n_req_id || '.txt';

            output_file_id := UTL_FILE.fopen (v_path, v_data_file_name, 'w');

            fnd_file.put_line (
               fnd_file.LOG,
                  'After opening file id for file '
               || v_path
               || '/'
               || v_data_file_name);

            UTL_FILE.put_line (output_file_id, v_header);

            fnd_file.put_line (
               fnd_file.LOG,
                  'Header record copied into the file '
               || v_path
               || '/'
               || v_data_file_name);

      ----------------------------------------------------------------
      -- Open cursor for writing into output file
      ----------------------------------------------------------------

           l_sec := ' Opening cursor';

        FOR inactive_site_rec IN xxwc_inactive_site_cur (l_class)
        LOOP


	l_sec := 'Get last bill to sale date'; 
           
	BEGIN
     	SELECT bill_to_site_use_id, 
            MAX (trx_date)
       	INTO l_bill_to_site, 
            l_bill_sale_date
        FROM apps.ra_customer_trx       -- 23/09/2014 Removed _all by pattabhi for Canada & US OU Testing
      	WHERE bill_to_site_use_id = inactive_site_rec.site_use_id
   	GROUP BY bill_to_site_use_id;
	EXCEPTION
   	WHEN OTHERS
   	THEN
      	l_bill_to_site := NULL;
      	l_bill_sale_date := NULL;
	END;

	l_sec := 'Get last ship to sale date';         

	BEGIN
            SELECT   ship_to_site_use_id,
                     MAX (trx_date)
                INTO l_ship_to_site,
                     l_ship_sale_date
            FROM apps.ra_customer_trx   -- 23/09/2014 Removed _all by pattabhi for Canada & US OU Testing
            where ship_to_site_use_id = inactive_site_rec.site_use_id
            GROUP BY ship_to_site_use_id;
        EXCEPTION
        WHEN OTHERS THEN
          l_ship_to_site := NULL;
          l_ship_sale_date := NULL;
        END;
       
	l_sec := 'Get last payment date'; 
        
	BEGIN
        SELECT   customer_site_use_id,
                 MAX (TRUNC (last_update_date)),
                 SUM (amount_due_remaining) 
            INTO l_cust_site_use,
                 l_payment_date,
                 l_account_bal
         FROM apps.ar_payment_schedules     -- 23/09/2014 Removed _all by pattabhi for Canada & US OU Testing
         WHERE customer_site_use_id = inactive_site_rec.site_use_id
         GROUP BY customer_site_use_id;
         EXCEPTION
         WHEN OTHERS THEN
         l_cust_site_use := NULL;
          l_payment_date     := NULL;
         l_account_bal := -1;
        END;
        
        l_sec := 'Check if any open SO for bill to'; 
       
        BEGIN
        SELECT oeh.invoice_to_org_id,
              MAX(oeh.ordered_date)
         INTO l_invoice_site_use,
              l_invoice_order_date 
        FROM apps.oe_order_headers oeh      -- 23/09/2014 Removed _all by pattabhi for Canada & US OU Testing
        WHERE oeh.invoice_to_org_id = inactive_site_rec.site_use_id
         AND NOT EXISTS ( SELECT 1 FROM oe_order_headers ooh WHERE    -- 23/09/2014 Removed _all by pattabhi for Canada & US OU Testing
        ooh.invoice_to_org_id = oeh.invoice_to_org_id
        AND ooh.flow_status_code IN ('ENTERED','BOOKED','DRAFT','PENDING_CUSTOMER_ACCEPTANCE'))
        GROUP BY oeh.invoice_to_org_id;
        EXCEPTION
        WHEN OTHERS THEN
           l_invoice_site_use := NULL;
           l_invoice_order_date := NULL;
        END;
        
	l_sec := 'Check if any open SO for ship to'; 

        BEGIN
        SELECT oeh.ship_to_org_id,
              MAX(oeh.ordered_date)
         INTO l_ship_site_use,
              l_ship_order_date 
        FROM apps.oe_order_headers oeh  -- 23/09/2014 Removed _all by pattabhi for Canada & US OU Testing
        WHERE oeh.ship_to_org_id = inactive_site_rec.site_use_id
         AND NOT EXISTS ( SELECT 1 FROM oe_order_headers ooh WHERE   -- 23/09/2014 Removed _all by pattabhi for Canada & US OU Testing
        ooh.ship_to_org_id = oeh.ship_to_org_id
        AND ooh.flow_status_code IN ('ENTERED','BOOKED','DRAFT','PENDING_CUSTOMER_ACCEPTANCE'))
        GROUP BY oeh.invoice_to_org_id;
        EXCEPTION
        WHEN OTHERS THEN
           l_ship_site_use := NULL;
           l_ship_order_date := NULL;
        END;


       l_sec := 'Checking if any activity within N period of time'; 

        IF  l_payment_date  < ADD_MONTHS (
                   NVL (
                      P_DATE,
                      TO_DATE (TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
                               'DD-MON-YYYY')),
                   (-1) * p_month) 
        AND l_account_bal = 0 
        AND NVL(l_bill_sale_date,l_ship_sale_date)  < ADD_MONTHS (
                   NVL (
                      P_DATE,
                      TO_DATE (TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
                               'DD-MON-YYYY')),
                   (-1) * p_month)
         AND (l_ship_order_date IS NOT NULL OR l_invoice_order_date IS NOT NULL) THEN

       l_sec := 'Check for commit mode'; 


 IF p_mode = 'N' THEN 
  BEGIN                   
               l_party_site_rec.party_site_id := inactive_site_rec.party_site_id;
               l_party_site_rec.status           := 'I';
               l_obj_num               := inactive_site_rec.object_version_number;

               hz_party_site_v2pub.update_party_site (
                  p_init_msg_list           => FND_API.G_FALSE,
                  p_party_site_rec          => l_party_site_rec,
                  p_object_version_number   => l_obj_num,
                  x_return_status           => l_return_status,
                  x_msg_count               => l_msg_count,
                  x_msg_data                => l_msg_data);

               fnd_file.put_line (FND_FILE.LOG, 'Ret Status:' || l_return_status);
               fnd_file.put_line (FND_FILE.LOG, 'l_msg_data:' || l_msg_data);

               IF (l_return_status <> 'S')
               THEN
                  l_error_message := l_msg_data;
                  fnd_file.put_line ( APPS.fnd_file.LOG,'Error In Party Site update API ' || l_error_message);
                  ROLLBACK;
               ELSE
                  COMMIT;
               END IF;
END;
END IF;                      
      
             v_detail :=
                        inactive_site_rec.account_number
                     || '|'
                     || inactive_site_rec.account_name
                     || '|'
                     || inactive_site_rec.party_site_number
                     || '|'
                     || inactive_site_rec.location
                     || '|'
                     || inactive_site_rec.collector
                     || '|'
                     || inactive_site_rec.profile_class
                     || '|'
                     || inactive_site_rec.site_classification
                     || '|'
                     ||NVL(l_ship_sale_date,l_bill_sale_date)
                     || '|'
                     || l_payment_date
                     || '|'
                     || NVL(l_invoice_order_date,l_ship_order_date)
                     || '|'
                     || l_account_bal;

                  UTL_FILE.put_line (output_file_id, v_detail);
                  
                  END IF;
            END LOOP;

            fnd_file.put_line (
               fnd_file.LOG,
                  'All detail records are copied into the file '
               || v_path
               || '/'
               || v_data_file_name);
            UTL_FILE.fclose (output_file_id);

            fnd_file.put_line (
               fnd_file.LOG,
               'After close of file ' || v_path || '/' || v_data_file_name);

      ------------------------------------------------------------------------------------------
      --create a file to point us where the data file is located along with the absolute path.          
      ------------------------------------------------------------------------------------------

          l_sec := ' Creating file to point data file location in report mode';
            
            v_data_file_name := 'zip_file_extract_' || n_req_id || '.txt';

            output_file_id := UTL_FILE.fopen (v_path, v_data_file_name, 'w');

            fnd_file.put_line (
               fnd_file.LOG,
                  'After opening fileid for file '
               || v_path
               || '/'
               || v_data_file_name);
         
      ------------------------------------------------------------------------------------------
      --  Getting the direcoty path from direcoty name ** Added below for ver 1.2 <start          
      ------------------------------------------------------------------------------------------
          l_sec := ' Getting directory path';
  
              SELECT directory_path
               INTO v_path
              FROM all_directories 
              WHERE directory_name = 'XXWC_INACTIVE_CUST_SITE';
       --End>

            UTL_FILE.put_line (
               output_file_id,
                  v_path
               || '/'
               || 'HDS_Inactive_customer_Sites'
               || '_'
               || n_req_id
               || '.txt');

            UTL_FILE.fclose (output_file_id);
            fnd_file.put_line (
               fnd_file.LOG,
               'After close of file ' || v_path || '/' || v_data_file_name);

            v_child_requests := 'OK';
         EXCEPTION
            WHEN OTHERS
            THEN
               v_child_requests := 'NA';
         END;

         fnd_file.put_line (fnd_file.LOG, 'Before IF clause ' || p_email);
 	-- <START>  Ver#1.4        
    /*  ------------------------------------------------------------------------------------------
      --  Submitting the emailing program         
      ------------------------------------------------------------------------------------------
   l_sec := ' Submitting email program in report mode';

         IF v_child_requests <> 'NA'
         THEN
            v_cp_long_name := 'XXHDS Inactive Customer Site Update';
            v_email := p_email;          --'maharajan.shunmugam@hdsupply.com';
            fnd_file.put_line (fnd_file.LOG, 'In IF clause ' || v_email);          

            ln_request_id :=
               fnd_request.submit_request (
                  application   => 'XXCUS',
                  program       => 'XXCUS_PEREND_POSTPROCESSOR',
                  description   => 'Taxwriteoff Send Email',
                  start_time    => '',
                  sub_request   => FALSE,
                  argument1     => n_req_id,
                  argument2     =>    v_path
                                   || '/zip_file_extract_'
                                   || n_req_id
                                   || '.txt',
                  argument3     => v_cp_long_name,
                  argument4     => v_email,
                  argument5     =>    'HDS_Inactive_customer_Sites'
                                   || '_'
                                   || n_req_id
                                   || '.zip',
                  argument6     => 'HDS Inactive Customer Sites Report');

            
            IF ln_request_id > 0
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Successfully submitted program XXHDS Period End Post Processor Program to send email');
            ELSE
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Failed to submit send email program XXHDS Period End Post Processor Program');
            END IF;
         ELSE                                         --v_child_requests ='NA'
            NULL;
         END IF;  */
	-- <END>  Ver#1.4	 
   EXCEPTION
      WHEN OTHERS
      THEN
          l_msg := 'Error: ' || SQLERRM;
         fnd_file.put_line (FND_FILE.LOG,'Error in XXWC_CUST_INACTIVATE_SITE_PKG');
         xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXWC_CUST_INACTIVATE_SITE_PKG',
          p_calling                => l_sec,
          p_request_id             => l_req_id,
          p_ora_error_msg          => l_msg,
          p_error_desc             => 'Error running XXWC_CUST_INACTIVATE_SITE_PKG with PROGRAM ERROR',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );
   END;
END;
/