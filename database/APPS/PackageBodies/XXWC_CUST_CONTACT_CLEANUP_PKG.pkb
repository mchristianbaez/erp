CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CUST_CONTACT_CLEANUP_PKG
AS
   /**************************************************************************
    *
    * FUNCTION | PROCEDURE | CURSOR
    *  XXWC_CUST_CONTACT_CLEANUP_PKG
    *
    * DESCRIPTION
    * customer contact clean up (remove contacts where first and last name are the same, duplicate
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
   .* ----------------- -------- ---------------------------------------------
    *
    *
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
    *
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ------------------------------------
    * 1.00    10/16/2013   Maharajan S     Creation
	* 1.1	  9/17/2014    Veera C		   TMS# 20141006-00025  Modified code as per the Canada OU Test
    *
    *************************************************************************/

   PROCEDURE XXWC_CUST_CONTACT_CLEANUP_PRC (errbuf    OUT VARCHAR2,
                                            retcode   OUT NUMBER,
                                            p_acc_number IN VARCHAR2)
   IS
      ----------------------------------------------------------------
      -- Cursor for Customer Contacts where FirstName = LastName
      ----------------------------------------------------------------
      CURSOR cust_contact_cur1
      IS
         SELECT party.party_id,
                acct_role.cust_account_role_id,
                acct_role.object_version_number,
                role_acct.account_number,
                party.object_version_number party_object_version_number,
                UPPER(SUBSTRB (party.person_first_name, 1, 40)) first_name,
                UPPER(SUBSTRB (party.person_last_name, 1, 50)) last_name
           FROM hz_cust_account_roles acct_role,
                hz_parties party,
                hz_parties rel_party,
                hz_relationships rel,
                hz_org_contacts org_cont,
                hz_cust_accounts role_acct
          WHERE acct_role.party_id = rel.party_id
            AND NOT EXISTS (select '1' from hz_role_responsibility rr where rr.cust_account_role_id = acct_role.cust_account_role_id)
            AND acct_role.role_type = 'CONTACT'
            AND org_cont.party_relationship_id = rel.relationship_id
            AND rel.subject_id = party.party_id
            AND rel_party.party_id = rel.party_id
            AND acct_role.cust_account_id = role_acct.cust_account_id
            AND role_acct.party_id = rel.object_id
            AND rel.subject_table_name = 'HZ_PARTIES'
            AND rel.object_table_name = 'HZ_PARTIES'
            AND (UPPER(party.person_first_name) = UPPER(party.person_last_name))-- OR cont_point.phone_number is NULL)
            AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)
            AND party.status ='A'
            AND rel_party.status = 'A'
            AND rel.status = 'A'
            AND acct_role.status = 'A'
            AND (role_acct.account_number = p_acc_number OR p_acc_number IS NULL);

      ----------------------------------------------------------------
      -- Cursor for Duplicate Customer Contacts
      ----------------------------------------------------------------
     CURSOR cust_contact_cur2
     IS
      SELECT role_acct.party_id,
             role_acct.account_number,
             UPPER(party.person_first_name) person_first_name,
             UPPER(party.person_last_name)  person_last_name,
             COUNT(1) count
           FROM hz_cust_account_roles acct_role,
                hz_parties party,
                hz_parties rel_party,
                hz_relationships rel,
                hz_org_contacts org_cont,
                hz_cust_accounts role_acct,
                 hz_contact_points hcp
          WHERE     acct_role.party_id = rel.party_id
                AND NOT EXISTS (select '1' from hz_role_responsibility rr where rr.cust_account_role_id = acct_role.cust_account_role_id)
                AND acct_role.role_type = 'CONTACT'
                AND org_cont.party_relationship_id = rel.relationship_id
                AND rel.subject_id = party.party_id
                AND rel_party.party_id = rel.party_id
                AND acct_role.cust_account_id = role_acct.cust_account_id
                AND role_acct.party_id = rel.object_id
                AND rel.subject_table_name = 'HZ_PARTIES'
                AND rel.object_table_name = 'HZ_PARTIES'
                AND hcp.contact_point_type(+) = 'PHONE'
                 AND hcp.status(+) = 'A'
                AND rel_party.party_id = hcp.owner_table_id(+)
                AND hcp.owner_table_name(+) = 'HZ_PARTIES' 
               AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)
                AND party.status ='A'
                AND rel_party.status = 'A'
                AND rel.status = 'A'
                AND acct_role.status = 'A'
                AND (role_acct.account_number = p_acc_number OR p_acc_number IS NULL)
                GROUP BY role_acct.party_id,role_acct.account_number, UPPER(party.person_first_name) , UPPER(party.person_last_name)
                HAVING COUNT(1) > 1 ;

       -------------------------------------------------------------------
       -- Cursor for Duplicate Customer Contact Details - With PHONE info
       -------------------------------------------------------------------
         CURSOR cust_contact_cur3(p_first_name VARCHAR2, p_last_name VARCHAR2,p_party_id NUMBER)
         IS
         SELECT   role_acct.party_id,
                  role_acct.account_number,
                  UPPER(party.person_first_name) person_first_name,
                  UPPER(party.person_last_name)  person_last_name,
                 MAX(acct_role.last_update_date)
           FROM hz_cust_account_roles acct_role,
                hz_parties party,
                hz_parties rel_party,
                hz_relationships rel,
                hz_org_contacts org_cont,
                hz_cust_accounts role_acct,
                hz_contact_points hcp
          WHERE     acct_role.party_id = rel.party_id
                AND NOT EXISTS (select '1' from hz_role_responsibility rr where rr.cust_account_role_id = acct_role.cust_account_role_id)
                AND acct_role.role_type = 'CONTACT'
                AND org_cont.party_relationship_id = rel.relationship_id
                AND rel.subject_id = party.party_id
                AND rel_party.party_id = rel.party_id
                AND acct_role.cust_account_id = role_acct.cust_account_id
                AND role_acct.party_id = rel.object_id
                AND rel.subject_table_name = 'HZ_PARTIES'
                AND rel.object_table_name = 'HZ_PARTIES'
                AND hcp.contact_point_type(+) = 'PHONE'
                AND hcp.status(+) = 'A'
                AND rel_party.party_id = hcp.owner_table_id(+)
                AND hcp.owner_table_name(+) = 'HZ_PARTIES'       
                AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)        
                AND party.status ='A'
                AND rel_party.status = 'A'
                AND rel.status = 'A'
                AND acct_role.status = 'A'
                AND UPPER(party.person_first_name) = p_first_name --cust_contact_cur2_rec.person_first_name --'KENNY'
                AND UPPER(party.person_last_name)  = p_last_name--cust_contact_cur2_rec.person_first_name  --'SADLER'
                AND role_acct.party_id =  p_party_id--cust_contact_cur2_rec.party_id
                AND (role_acct.account_number = p_acc_number OR p_acc_number IS NULL)
                AND hcp.phone_number IS NOT NULL
        GROUP BY  role_acct.party_id,
          role_acct.account_number,
                  UPPER(party.person_first_name),
                  UPPER(party.person_last_name);

       -------------------------------------------------------------------
       -- Cursor for Duplicate Customer Contact Details - With no PHONE 
       -------------------------------------------------------------------
         CURSOR cust_contact_cur4(p_first_name VARCHAR2, p_last_name VARCHAR2,p_party_id NUMBER)
             IS
         SELECT role_acct.party_id,
                role_acct.account_number,
                UPPER(party.person_first_name)  person_first_name,
                UPPER(party.person_last_name)   person_last_name,
                MAX(acct_role.last_update_date)
           FROM hz_cust_account_roles acct_role,
                hz_parties party,
                hz_parties rel_party,
                hz_relationships rel,
                hz_org_contacts org_cont,
                hz_cust_accounts role_acct,
                hz_contact_points hcp
          WHERE     acct_role.party_id = rel.party_id
                AND NOT EXISTS (select '1' from hz_role_responsibility rr where rr.cust_account_role_id = acct_role.cust_account_role_id)
                AND acct_role.role_type = 'CONTACT'
                AND org_cont.party_relationship_id = rel.relationship_id
                AND rel.subject_id = party.party_id
                AND rel_party.party_id = rel.party_id
                AND acct_role.cust_account_id = role_acct.cust_account_id
                AND role_acct.party_id = rel.object_id
                AND rel.subject_table_name = 'HZ_PARTIES'
                AND rel.object_table_name = 'HZ_PARTIES'
                AND hcp.contact_point_type(+) = 'PHONE'
                AND hcp.status(+) = 'A'
                AND rel_party.party_id = hcp.owner_table_id(+)
                AND hcp.owner_table_name(+) = 'HZ_PARTIES'        
                AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)        
                AND party.status ='A'
                AND rel_party.status = 'A'
                AND rel.status = 'A'
                AND acct_role.status = 'A'
                AND UPPER(party.person_first_name) = p_first_name --cust_contact_cur2_rec.person_first_name --'KENNY'
                AND UPPER(party.person_last_name)  = p_last_name--cust_contact_cur2_rec.person_first_name  --'SADLER'
                AND role_acct.party_id =  p_party_id--cust_contact_cur2_rec.party_id
                AND (role_acct.account_number = p_acc_number OR p_acc_number IS NULL)
                AND hcp.phone_number IS NULL
        GROUP BY  role_acct.party_id,
          role_acct.account_number,
                  UPPER(party.person_first_name),
                  UPPER(party.person_last_name);

      l_cust_cont_rec cust_contact_cur3%ROWTYPE;
      l_cust_null_cont_rec cust_contact_cur4%ROWTYPE;


      l_person_rec             hz_party_v2pub.person_rec_type;
      l_cust_role_rec          hz_cust_account_role_v2pub.cust_account_role_rec_type;
      l_party_id               NUMBER;
      x_party_number           VARCHAR2 (100);
      x_party_id               NUMBER;
      x_profile_id             NUMBER;
      l_version_number         NUMBER;
      l_object_number          NUMBER;
      x_return_status          VARCHAR2 (1000);
      x_msg_count              NUMBER;
      x_msg_data               VARCHAR2 (1000);
      x_return_status1         VARCHAR2 (1000);
      x_msg_count1             NUMBER;
      x_msg_data1              VARCHAR2 (1000);
      l_error_message          VARCHAR2 (1000);
      l_cust_role_obj_number   NUMBER;
      l_msg                    VARCHAR2 (150);
      l_distro_list            VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_req_id                 NUMBER        := fnd_global.conc_request_id;
      l_user_id                NUMBER        := fnd_global.user_id;
      l_sec                    VARCHAR2(200);

BEGIN
fnd_file.put_line (APPS.fnd_file.LOG,'==================================================================');
fnd_file.put_line (APPS.fnd_file.LOG,'           Customer contacts with First and Last name same           ');
fnd_file.put_line (APPS.fnd_file.LOG,'==================================================================');

  /* mo_global.init ('AR');
   fnd_global.apps_initialize (user_id           => l_user_id
                             , resp_id           => 20678
                             , resp_appl_id      => 222);

   mo_global.set_policy_context ('S', 162);*/ --17/09/2014 Commented by Veera as per the Canada OU Test
   fnd_global.set_nls_context ('AMERICAN');

----------------------------------------------------------------
-- Process Customer Contacts where FirstName = LastName
----------------------------------------------------------------
l_sec := 'Process Customer Contacts where FirstName = LastName';
FOR cust_contact_cur1_rec IN cust_contact_cur1
LOOP
 fnd_file.put_line (APPS.fnd_file.LOG,cust_contact_cur1_rec.last_name||','||cust_contact_cur1_rec.first_name);
        
         l_cust_role_rec.cust_account_role_id  := cust_contact_cur1_rec.cust_account_role_id;
         l_cust_role_rec.status            := 'I';
         l_cust_role_obj_number            := cust_contact_cur1_rec.object_version_number; 

        apps.hz_cust_account_role_v2pub.update_cust_account_role(FND_API.G_FALSE,
                                       l_cust_role_rec,
                                          l_cust_role_obj_number,
                                          x_return_status1,
                                          x_msg_count1,
                                          x_msg_data1);
      fnd_file.put_line (APPS.fnd_file.LOG,'Update Person API return status: ' || x_return_status1);

         IF (x_return_status1 <> 'S')
         THEN
            l_error_message := x_msg_data1;
            fnd_file.put_line (
               APPS.fnd_file.LOG,
               'Error In Contact point update API' || l_error_message);
            ROLLBACK;
         ELSE
            fnd_file.put_line (
               APPS.fnd_file.output,
                  'Updated Contact point'
               || cust_contact_cur1_rec.last_name
               || ','
               || cust_contact_cur1_rec.first_name
               || ' as INACTIVE');
         COMMIT;
         END IF;
 END LOOP; -- cust_contact_cur1
fnd_file.put_line (APPS.fnd_file.LOG,'*****************************************************************************');
fnd_file.put_line (APPS.fnd_file.LOG,'              Duplicate customer contacts                  ');
fnd_file.put_line (APPS.fnd_file.LOG,'==================================================================');

--END;

----------------------------------------------------------------
-- Process Duplicate Customer Contacts
----------------------------------------------------------------
l_sec := 'Process Duplicate Customer Contacts';
BEGIN
 FOR cust_contact_cur2_rec IN cust_contact_cur2
 LOOP

BEGIN
 OPEN cust_contact_cur4(cust_contact_cur2_rec.person_first_name,cust_contact_cur2_rec.person_last_name,cust_contact_cur2_rec.party_id);
 LOOP
 FETCH cust_contact_cur4 INTO l_cust_null_cont_rec;
  EXIT WHEN cust_contact_cur4%NOTFOUND;

IF (cust_contact_cur4%ROWCOUNT = 1 OR cust_contact_cur4%ROWCOUNT > 1 ) THEN
FOR I1 IN ( SELECT   role_acct.party_id,
                  role_acct.account_number,
                  acct_role.cust_account_role_id,
                  UPPER(party.person_first_name) person_first_name,
                  UPPER(party.person_last_name)  person_last_name,
                  party.primary_phone_number,
                  acct_role.last_update_date,
                  acct_role.object_version_number
           FROM hz_cust_account_roles acct_role,
                hz_parties party,
                hz_parties rel_party,
                hz_relationships rel,
                hz_org_contacts org_cont,
                hz_cust_accounts role_acct,
                hz_contact_points hcp
          WHERE     acct_role.party_id = rel.party_id
                AND NOT EXISTS (select '1' from hz_role_responsibility rr where rr.cust_account_role_id = acct_role.cust_account_role_id)
                AND acct_role.role_type = 'CONTACT'
                AND org_cont.party_relationship_id = rel.relationship_id
                AND rel.subject_id = party.party_id
                AND rel_party.party_id = rel.party_id
                AND acct_role.cust_account_id = role_acct.cust_account_id
                AND role_acct.party_id = rel.object_id
                AND rel.subject_table_name = 'HZ_PARTIES'
                AND rel.object_table_name = 'HZ_PARTIES'
                AND hcp.contact_point_type(+) = 'PHONE'
                AND hcp.status(+) = 'A'
                AND rel_party.party_id = hcp.owner_table_id(+)
                AND hcp.owner_table_name(+) = 'HZ_PARTIES'
                AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)
                AND party.status ='A'
                AND rel_party.status = 'A'
                AND rel.status = 'A'
                AND acct_role.status = 'A'
                AND UPPER(party.person_first_name) = cust_contact_cur2_rec.person_first_name --'KENNY'
                AND UPPER(party.person_last_name)  = cust_contact_cur2_rec.person_last_name  --'SADLER'
                AND role_acct.party_id =  cust_contact_cur2_rec.party_id
                AND hcp.phone_number IS NULL
                AND (role_acct.account_number = p_acc_number OR p_acc_number IS NULL)
                AND acct_role.last_update_date < (SELECT  MAX(acct_role.last_update_date)
           FROM hz_cust_account_roles acct_role,
                hz_parties party,
                hz_parties rel_party,
                hz_relationships rel,
                hz_org_contacts org_cont,
                hz_cust_accounts role_acct,
                hz_contact_points hcp
          WHERE      acct_role.party_id = rel.party_id
                AND NOT EXISTS (select '1' from hz_role_responsibility rr where rr.cust_account_role_id = acct_role.cust_account_role_id)
                AND acct_role.role_type = 'CONTACT'
                AND org_cont.party_relationship_id = rel.relationship_id
                AND rel.subject_id = party.party_id
                AND rel_party.party_id = rel.party_id
                AND acct_role.cust_account_id = role_acct.cust_account_id
                AND role_acct.party_id = rel.object_id
                AND rel.subject_table_name = 'HZ_PARTIES'
                AND rel.object_table_name = 'HZ_PARTIES'
                AND hcp.owner_table_name(+) = 'HZ_PARTIES'
                AND hcp.contact_point_type(+) = 'PHONE'
                AND hcp.status(+) = 'A'
                AND rel_party.party_id = hcp.owner_table_id(+)
                AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)
                AND party.status ='A'
                AND rel_party.status = 'A'
                AND rel.status = 'A'
                AND acct_role.status = 'A'
                AND UPPER(party.person_first_name) = cust_contact_cur2_rec.person_first_name --'KENNY'
                AND UPPER(party.person_last_name)  = cust_contact_cur2_rec.person_last_name  --'SADLER'
                AND role_acct.party_id =  cust_contact_cur2_rec.party_id
                AND (role_acct.account_number = p_acc_number OR p_acc_number IS NULL)
                AND hcp.phone_number IS NULL))
   LOOP

   fnd_file.put_line (APPS.fnd_file.LOG,i1.person_last_name||','||i1.person_first_name||'-'||i1.account_number);


        l_cust_role_rec.cust_account_role_id  := i1.cust_account_role_id;
         l_cust_role_rec.status            := 'I';
         l_cust_role_obj_number            := i1.object_version_number; 

    apps.hz_cust_account_role_v2pub.update_cust_account_role(FND_API.G_FALSE,
                                       l_cust_role_rec,
                                          l_cust_role_obj_number,
                                          x_return_status1,
                                         x_msg_count1,
                                          x_msg_data1);
      fnd_file.put_line (
            APPS.fnd_file.LOG,
            'Update Person API return status: ' || x_return_status1);


         IF (x_return_status1 <> 'S')
         THEN
            l_error_message := x_msg_data1;
            fnd_file.put_line (
               APPS.fnd_file.LOG,
               'Error In Contact point update API' || l_error_message);
            ROLLBACK;
         ELSE
            fnd_file.put_line (
               APPS.fnd_file.output,
                  'Updated Contact point'
               || i1.person_last_name
               || ','
               || i1.person_first_name
               || ' as INACTIVE');
         COMMIT;
         END IF;
 END LOOP;      
END IF; 

END LOOP;
close cust_contact_cur4;
END;

 -------------------------------------------------------------------
 -- Process Duplicate Customer Contacts - With PHONE info
 -------------------------------------------------------------------
l_sec := 'Process Duplicate Customer Contacts - With PHONE info';
BEGIN
 OPEN cust_contact_cur3(cust_contact_cur2_rec.person_first_name,cust_contact_cur2_rec.person_last_name,cust_contact_cur2_rec.party_id);
 LOOP
 FETCH cust_contact_cur3 INTO l_cust_cont_rec;
  EXIT WHEN cust_contact_cur3%NOTFOUND;


 IF (cust_contact_cur3%ROWCOUNT = 1 OR cust_contact_cur3%ROWCOUNT > 1 )
 THEN

   FOR J in(SELECT role_acct.party_id,
          role_acct.account_number,
                  acct_role.cust_account_role_id,
                  UPPER(party.person_first_name)   person_first_name,
                  UPPER(party.person_last_name)    person_last_name,
                  party.primary_phone_number,
                  acct_role.last_update_date,
                  acct_role.object_version_number,
                  hcp.phone_number
           FROM hz_cust_account_roles acct_role,
                hz_parties party,
                hz_parties rel_party,
                hz_relationships rel,
                hz_org_contacts org_cont,
                hz_cust_accounts role_acct,
                hz_contact_points hcp
          WHERE     acct_role.party_id = rel.party_id
                AND NOT EXISTS (select '1' from hz_role_responsibility rr where rr.cust_account_role_id = acct_role.cust_account_role_id)
                AND acct_role.role_type = 'CONTACT'
                AND org_cont.party_relationship_id = rel.relationship_id
                AND rel.subject_id = party.party_id
                AND rel_party.party_id = rel.party_id
                AND acct_role.cust_account_id = role_acct.cust_account_id
                AND role_acct.party_id = rel.object_id
                AND rel.subject_table_name = 'HZ_PARTIES'
                AND rel.object_table_name = 'HZ_PARTIES'
                AND hcp.contact_point_type(+) = 'PHONE'
                AND hcp.status(+) = 'A'
                AND rel_party.party_id = hcp.owner_table_id(+)
                AND hcp.owner_table_name(+) = 'HZ_PARTIES'    
               AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)  
               AND party.status ='A'
                AND rel_party.status = 'A'
                AND rel.status = 'A'
                AND acct_role.status = 'A'
                AND (role_acct.account_number = p_acc_number OR p_acc_number IS NULL)
                AND UPPER(party.person_first_name) = cust_contact_cur2_rec.person_first_name --'KENNY'
                AND UPPER(party.person_last_name)  = cust_contact_cur2_rec.person_last_name  --'SADLER'
                AND role_acct.party_id =  cust_contact_cur2_rec.party_id
                AND hcp.phone_number IS NULL
            UNION 
                SELECT   role_acct.party_id,
          role_acct.account_number,
                  acct_role.cust_account_role_id,
                  UPPER(party.person_first_name)   person_first_name,
                  UPPER(party.person_last_name)    person_last_name,
                  party.primary_phone_number,
                  acct_role.last_update_date,
                  acct_role.object_version_number,
           hcp.phone_number
           FROM hz_cust_account_roles acct_role,
                hz_parties party,
                hz_parties rel_party,
                hz_relationships rel,
                hz_org_contacts org_cont,
                hz_cust_accounts role_acct,
        hz_contact_points hcp
          WHERE     acct_role.party_id = rel.party_id
                AND NOT EXISTS (select '1' from hz_role_responsibility rr where rr.cust_account_role_id = acct_role.cust_account_role_id)
                AND acct_role.role_type = 'CONTACT'
                AND org_cont.party_relationship_id = rel.relationship_id
                AND rel.subject_id = party.party_id
                AND rel_party.party_id = rel.party_id
                AND acct_role.cust_account_id = role_acct.cust_account_id
                AND role_acct.party_id = rel.object_id
                AND rel.subject_table_name = 'HZ_PARTIES'
                AND rel.object_table_name = 'HZ_PARTIES'
                AND hcp.contact_point_type(+) = 'PHONE'
                AND hcp.status(+) = 'A'
                AND rel_party.party_id = hcp.owner_table_id(+)
                AND hcp.owner_table_name(+) = 'HZ_PARTIES'       
                AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)
                AND (role_acct.account_number = p_acc_number OR p_acc_number IS NULL)
                AND party.status ='A'
                AND rel_party.status = 'A'
                AND rel.status = 'A'
                AND acct_role.status = 'A'
                AND UPPER(party.person_first_name) = cust_contact_cur2_rec.person_first_name --'KENNY'
                AND UPPER(party.person_last_name)  = cust_contact_cur2_rec.person_last_name  --'SADLER'
               AND role_acct.party_id =  cust_contact_cur2_rec.party_id
                AND hcp.phone_number  IS NOT NULL
                AND acct_role.last_update_date < (SELECT  MAX(acct_role.last_update_date)
           FROM hz_cust_account_roles acct_role,
                hz_parties party,
                hz_parties rel_party,
                hz_relationships rel,
                hz_org_contacts org_cont,
                hz_cust_accounts role_acct,
                hz_contact_points hcp
          WHERE     acct_role.party_id = rel.party_id
                AND NOT EXISTS (select '1' from hz_role_responsibility rr where rr.cust_account_role_id = acct_role.cust_account_role_id)
                AND acct_role.role_type = 'CONTACT'
                AND org_cont.party_relationship_id = rel.relationship_id
                AND rel.subject_id = party.party_id
                AND rel_party.party_id = rel.party_id
                AND acct_role.cust_account_id = role_acct.cust_account_id
                AND role_acct.party_id = rel.object_id
                AND rel.subject_table_name = 'HZ_PARTIES'
                AND rel.object_table_name = 'HZ_PARTIES'
                AND hcp.contact_point_type(+) = 'PHONE'
                AND hcp.status(+) = 'A'
                AND rel_party.party_id = hcp.owner_table_id(+)
                AND hcp.owner_table_name(+) = 'HZ_PARTIES' 
                AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)
                AND party.status ='A'
                AND rel_party.status = 'A'
                AND rel.status = 'A'
                AND acct_role.status = 'A'
                AND (role_acct.account_number = p_acc_number OR p_acc_number IS NULL)
                AND UPPER(party.person_first_name) = cust_contact_cur2_rec.person_first_name --'KENNY'
                AND UPPER(party.person_last_name)  = cust_contact_cur2_rec.person_last_name  --'SADLER'
                AND role_acct.party_id =  cust_contact_cur2_rec.party_id
                AND hcp.phone_number IS NOT NULL))

  LOOP         

   fnd_file.put_line (APPS.fnd_file.LOG,j.person_last_name||','||j.person_first_name||'   '||j.phone_number||'-'||j.account_number);


         l_cust_role_rec.cust_account_role_id  := j.cust_account_role_id;
         l_cust_role_rec.status            := 'I';
         l_cust_role_obj_number            := j.object_version_number; 

        apps.hz_cust_account_role_v2pub.update_cust_account_role(FND_API.G_FALSE,
                                       l_cust_role_rec,
                                          l_cust_role_obj_number,
                                          x_return_status1,
                                         x_msg_count1,
                                          x_msg_data1);
      fnd_file.put_line (
            APPS.fnd_file.LOG,
            'Update Person API return status: ' || x_return_status1);


         IF (x_return_status1 <> 'S')
         THEN
            l_error_message := x_msg_data1;
            fnd_file.put_line (
               APPS.fnd_file.LOG,
               'Error In Contact point update API' || l_error_message);
            ROLLBACK;
         ELSE
            fnd_file.put_line (
               APPS.fnd_file.output,
                  'Updated Contact point'
               || j.person_last_name
               || ','
               || j.person_first_name
               || ' as INACTIVE');
         COMMIT;
         END IF;

      END LOOP;
END IF;

END LOOP;
close cust_contact_cur3;
END; 

END LOOP;
END;

EXCEPTION
WHEN OTHERS THEN
      l_msg := 'Error: ' || SQLERRM;
      -- Calling ERROR API

      xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXWC_CUST_CONTACT_CLEANUP_PKG',
          p_calling                => l_sec,
          p_request_id             => l_req_id,
          p_ora_error_msg          => l_msg,
          p_error_desc             => 'Error running xxwc_billtrust_intf_pkg with PROGRAM ERROR',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );

END XXWC_CUST_CONTACT_CLEANUP_PRC;

END XXWC_CUST_CONTACT_CLEANUP_PKG;
/
