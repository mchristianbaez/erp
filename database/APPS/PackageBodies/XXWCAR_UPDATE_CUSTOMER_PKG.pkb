CREATE OR REPLACE PACKAGE BODY APPS.XXWCAR_UPDATE_CUSTOMER_PKG AS

/********************************************************************************
Procedure Name: UPDATE_CUST_PROFILE

Description : API to update Collector and CreditAnalyst on CusotmerProfile.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/07/2013    Gopi Damuluri    Initial version.
1.1     05/15/2013    Gopi Damuluri    Uncommented update_cust_profile procedure
                                       ESMS : 203814
1.2     06/11/2013    Gopi Damuluri    Added a new procedure UPDATE_CUST_NOTES
                                       for WebADI loader. TMS:20130418-01738
1.3     09/09/2013    Gopi Damuluri    Modified UPDATE_CUST_PROF_AMT process to 
                                       accept NULL values. TMS:20130228-01478
1.4     11/11/2013    Gopi Damuluri    TMS# 20130808-00965
                                       Automation of Monthly Oracle Risk Update Process 
                                       TMS# 20130228-01478
                                       Update No Notice Customers credit limits from $0 to Null
1.5     01/31/2014 Maharajan Shunmugam  TMS#20131126-00046
1.6     09/17/2014   PATTABHI AVULA 	         TMS#20141001-00029
********************************************************************************/

l_org_id            NUMBER:=mo_global.get_current_org_id;    -- 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing
l_resp_id           NUMBER:=fnd_global.resp_id;              -- 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing
l_resp_appl_id      NUMBER:=fnd_global.resp_appl_id;         -- 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing
l_user_id           NUMBER:= fnd_global.user_id;             -- 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing
   

PROCEDURE update_cust_profile (p_errbuf            OUT      VARCHAR2
                             , p_retcode           OUT      NUMBER)
       IS
     p_customer_profile_rec_type     HZ_CUSTOMER_PROFILE_V2PUB.CUSTOMER_PROFILE_REC_TYPE;
     p_cust_account_profile_id       NUMBER;
     p_object_version_number         NUMBER;
     x_return_status                 VARCHAR2(2000);
     x_msg_count                     NUMBER;
     x_msg_data                      VARCHAR2(2000);

     --------------------------------------------------------------------
     -- Cursor to derive extract Customer Profile values
     --------------------------------------------------------------------
     CURSOR cur_cust_prof IS
     SELECT stg.cust_account_profile_id
          , stg.collector_id
          , stg.credit_analyst_id
          , hcp.object_version_number
       FROM xxwc_update_cust_profile_tbl stg
          , hz_customer_profiles hcp
      WHERE 1 = 1
        AND NVL(stg.status,'N') IN ('N', 'E')
        AND stg.cust_account_profile_id = hcp.cust_account_profile_id;

BEGIN

   --------------------------------------------------------------------
   -- APPS Initializing
   -- Note: The values are hard-coded as this process will be executed
   -- from "HDS IT Operations Analyst - WC" responsibility
   --------------------------------------------------------------------
   
  
   mo_global.init ('AR');
   fnd_global.apps_initialize (user_id           => l_user_id
                             , resp_id           => l_resp_id
                             , resp_appl_id      => l_resp_appl_id);

   mo_global.set_policy_context ('S', l_org_id);
   
   
   fnd_global.set_nls_context ('AMERICAN');

   FOR rec_cust_prof IN cur_cust_prof LOOP
     p_customer_profile_rec_type.cust_account_profile_id := rec_cust_prof.cust_account_profile_id; -- Documentation on using TCA APIs - V2 Page 73
     p_customer_profile_rec_type.collector_id            := rec_cust_prof.collector_id;
     p_customer_profile_rec_type.credit_analyst_id       := rec_cust_prof.credit_analyst_id;
     p_object_version_number                             := rec_cust_prof.object_version_number;
--     p_object_version_number                             := 9; --rec_cust_prof.object_version_number;

     --------------------------------------------------------------------
     -- API Call to update CustomerProfile
     --------------------------------------------------------------------
     hz_customer_profile_v2pub.update_customer_profile(p_init_msg_list              => fnd_api.g_true
                                     ,                 p_customer_profile_rec       => p_customer_profile_rec_type
                                     ,                 p_object_version_number      => p_object_version_number
                                     ,                 x_return_status              => x_return_status
                                     ,                 x_msg_count                  => x_msg_count
                                     ,                 x_msg_data                   => x_msg_data                );

     --------------------------------------------------------------------
     -- Validate the status of API output
     --------------------------------------------------------------------
     IF x_msg_count >1 THEN
        FOR I IN 1..x_msg_count LOOP
          fnd_file.put_line(fnd_file.log,SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255));
        END LOOP;

        UPDATE xxwc_update_cust_profile_tbl
           SET status        = 'E'
             , error_message = SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255)
         WHERE cust_account_profile_id = rec_cust_prof.cust_account_profile_id;

     ELSE
        UPDATE xxwc_update_cust_profile_tbl
           SET status = SUBSTR(x_return_status,1,1)
         WHERE cust_account_profile_id = rec_cust_prof.cust_account_profile_id;
     END IF;
   END LOOP; -- rec_cust_prof

COMMIT;

EXCEPTION
WHEN OTHERS THEN
  fnd_file.put_line(fnd_file.log,'Error Message = '|| SQLERRM);
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWCAR_UPDATE_CUSTOMER_PKG.UPDATE_CUST_PROFILE'
                                        ,p_calling           => 'OTHERS exception'
                                        ,p_request_id        => NULL
                                        ,p_ora_error_msg     => substr(SQLERRM
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => 'Error running XXWCAR_UPDATE_CUSTOMER_PKG.UPDATE_CUST_PROFILE procedure with OTHERS Exception'
                                        ,p_distribution_list => 'HDSOracleDevelopers@hdsupply.com'
                                        ,p_module            => 'AR');
END update_cust_profile;

PROCEDURE update_cust_prof_amt(p_errbuf            OUT      VARCHAR2
                             , p_retcode           OUT      NUMBER)
/********************************************************************************
Procedure Name: UPDATE_CUST_PROF_AMT

Description : API to update Customer Profile Amounts

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)            DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/10/2013    Gopi Damuluri        Initial version.
********************************************************************************/

IS
     l_prof_amt_rec                  HZ_CUSTOMER_PROFILE_V2PUB.cust_profile_amt_rec_type;
     l_obj_version                   NUMBER;
     l_ret_status                    VARCHAR2(2000);
     l_msg_count                     NUMBER;
     l_msg_data                      VARCHAR2(2000);
     l_msg_index                     NUMBER;
     l_errors                        NUMBER;
     l_null_cr_limit                 NUMBER := FND_API.G_MISS_NUM;  -- Version# 1.3 Added

     CURSOR c_prof_amts
         IS
     SELECT stg.cust_acct_profile_amt_id                           -- Version# 1.3 Modified
          , hcpa.object_version_number
          , stg.updated_credit_limit
       FROM xxwc_update_cust_prof_amt_tbl stg
          , hz_cust_profile_amts hcpa
      WHERE 1 = 1
        AND stg.cust_acct_profile_amt_id = hcpa.cust_acct_profile_amt_id
        AND NVL(stg.STATUS,'N') IN ('N','E') ;

BEGIN

   --------------------------------------------------------------------
   -- APPS Initializing
   -- Note: The values are hard-coded as this process will be executed
   -- from "HDS IT Operations Analyst - WC" responsibility
   --------------------------------------------------------------------
   
   
   mo_global.init ('AR');
   fnd_global.apps_initialize (user_id           => l_user_id  --fnd_profile.value('user_id')  17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 
                             , resp_id           => l_resp_id  --20678  -- 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 
                             , resp_appl_id      => l_resp_appl_id); -- 222 --  17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 

   mo_global.set_policy_context ('S', l_org_id);   --  17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing
   
    
   fnd_global.set_nls_context ('AMERICAN');

   FOR l_prof_amts IN c_prof_amts
   LOOP
     l_prof_amt_rec.cust_acct_profile_amt_id    := l_prof_amts.cust_acct_profile_amt_id;
--     l_prof_amt_rec.OVERALL_CREDIT_LIMIT        := l_prof_amts.updated_credit_limit;    -- Version# 1.3 Commented
     l_obj_version                              := l_prof_amts.object_version_number;

-- Version# 1.3 > Start     
     IF l_prof_amts.updated_credit_limit IS NOT NULL THEN
       l_prof_amt_rec.OVERALL_CREDIT_LIMIT        := l_prof_amts.updated_credit_limit;
     ELSE
       l_prof_amt_rec.OVERALL_CREDIT_LIMIT        := l_null_cr_limit;
     END IF; 
-- Version# 1.3 < End

     --------------------------------------------------------------------
     -- API Call to update CustomerProfileAmount
     --------------------------------------------------------------------
     HZ_CUSTOMER_PROFILE_V2PUB.update_cust_profile_amt     ( p_init_msg_list          => FND_API.G_TRUE
                                                           , p_cust_profile_amt_rec   => l_prof_amt_rec
                                                           , p_object_version_number  => l_obj_version
                                                           , x_return_status          => l_ret_status
                                                           , x_msg_count              => l_msg_count
                                                           , x_msg_data               => l_msg_data     ) ;

     --------------------------------------------------------------------
     -- Validate the status of API output
     --------------------------------------------------------------------
     IF l_ret_status <> FND_API.G_RET_STS_SUCCESS     THEN
       l_errors := 1;
       IF l_msg_count >= 1        THEN
         FOR I IN 1..l_msg_count LOOP
           fnd_file.put_line(fnd_file.log,SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255));
         END LOOP;
       END IF;

         UPDATE xxwc_update_cust_prof_amt_tbl
            SET status        = 'E'
              , error_message = SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255)
          WHERE cust_acct_profile_amt_id = l_prof_amt_rec.cust_acct_profile_amt_id;
     ELSE
         UPDATE xxwc_update_cust_prof_amt_tbl
            SET status        = 'P'
          WHERE cust_acct_profile_amt_id = l_prof_amt_rec.cust_acct_profile_amt_id;
     END IF;
   END LOOP;

EXCEPTION
 WHEN OTHERS THEN
   fnd_file.put_line(fnd_file.log,'Error Message = '|| SQLERRM);

    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWCAR_UPDATE_CUSTOMER_PKG.UPDATE_CUST_PROF_AMT'
                                        ,p_calling           => 'OTHERS exception'
                                        ,p_request_id        => NULL
                                        ,p_ora_error_msg     => substr(SQLERRM
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => 'Error running XXWCAR_UPDATE_CUSTOMER_PKG.UPDATE_CUST_PROF_AMT procedure with OTHERS Exception'
                                        ,p_distribution_list => 'HDSOracleDevelopers@hdsupply.com'
                                        ,p_module            => 'AR');
END UPDATE_CUST_PROF_AMT;

PROCEDURE update_cust_notes(  p_cust_account_id               IN NUMBER
                            , p_account_number                IN VARCHAR2
                            , p_account_name                  IN VARCHAR2
                            , p_customer_account_status       IN VARCHAR2
                            , p_Total_AR                      IN NUMBER
                            , p_collector_name                IN VARCHAR2
                            , p_Note1                         IN VARCHAR2
                            , p_Note2                         IN VARCHAR2
                            , p_Note3                         IN VARCHAR2
                            , p_Note4                         IN VARCHAR2
                            , p_Note5                         IN VARCHAR2
                            , p_account_customer_source       IN VARCHAR2
                            , p_Account_Credit_Hold           IN VARCHAR2
                            , p_Account_Payment_Terms         IN VARCHAR2
                            , p_Account_Profile_Class_Name    IN VARCHAR2
                            , p_Account_Credit_limit          IN NUMBER
                            , p_PO_Required                   IN VARCHAR2)
/********************************************************************************
Procedure Name: UPDATE_CUST_NOTES

Description : API to update Mandatory Customer Notes using WebADI.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     06/11/2013    Gopi Damuluri    Initial version.
********************************************************************************/
IS

   l_error_message       VARCHAR2(1000);
   l_attribute_category  VARCHAR2(150);
   l_val_flag            VARCHAR2(1);
   
   l_exception           EXCEPTION;

BEGIN

   --------------------------------------------------------------------
   -- Check if P_CUST_ACCOUNT_ID and P_ACCOUNT_NUMBER are valid
   --------------------------------------------------------------------
   BEGIN
     SELECT '1'
       INTO l_val_flag 
       FROM hz_cust_accounts  -- hz_cust_accounts_all TO hz_cust_accounts on 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 
      WHERE 1               = 1
        AND cust_account_id = p_cust_account_id
        AND account_number  = p_account_number;
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
      l_error_message := ' ~ CUST_ACCOUNT_ID and ACCOUNT_NUMBER are not a valid combination';
   WHEN OTHERS THEN
      l_error_message := ' ~ Unidentified exception - validating customer '||SQLERRM;
   END;

   --------------------------------------------------------------------
   -- Check if Notes exceeds 150 characters
   --------------------------------------------------------------------
   IF LENGTH(NVL(p_Note1,' ')) > 150 THEN
      l_error_message := l_error_message||' ~ NOTE1 exceeds 150 characters';
   END IF;

   IF LENGTH(NVL(p_Note2,' ')) > 150 THEN
      l_error_message := l_error_message||' ~ NOTE2 exceeds 150 characters';
   END IF;
   
   IF LENGTH(NVL(p_Note3,' ')) > 150 THEN
      l_error_message := l_error_message||' ~ NOTE3 exceeds 150 characters';
   END IF;
   
   IF LENGTH(NVL(p_Note4,' ')) > 150 THEN
      l_error_message := l_error_message||' ~ NOTE4 exceeds 150 characters';
   END IF;

   IF LENGTH(NVL(p_Note5,' ')) > 150 THEN
      l_error_message := l_error_message||' ~ NOTE5 exceeds 150 characters';
   END IF;

   IF l_error_message IS NOT NULL THEN
      RAISE l_exception;
   END IF;

   IF NVL(p_note1,'-1') = '-1' AND
      NVL(p_note2,'-1') = '-1' AND
      NVL(p_note3,'-1') = '-1' AND
      NVL(p_note4,'-1') = '-1' AND
      NVL(p_note5,'-1') = '-1'
   THEN
--------------------------------------------------------------------
-- If Note1 to Note5 are NULL, then update ATTRIBUTE_CATEGORY to "No"
--------------------------------------------------------------------
     UPDATE hz_cust_accounts hca   -- MO Changes made on 17/09/2014 by pattabhi for TMS#20141001-00029 OU Testing 
        SET hca.attribute17         = p_note1
          , hca.attribute18         = p_note2
          , hca.attribute19         = p_note3
          , hca.attribute20         = p_note4
          , hca.attribute16         = p_note5
          , hca.attribute_category  = 'No'
      WHERE hca.cust_account_id = p_cust_account_id;
   ELSE
     UPDATE hz_cust_accounts hca  -- MO Changes made on 17/09/2014 by pattabhi for TMS#20141001-00029 OU Testing 
        SET hca.attribute17     = p_note1
          , hca.attribute18     = p_note2
          , hca.attribute19     = p_note3
          , hca.attribute20     = p_note4
          , hca.attribute16     = p_note5
      WHERE hca.cust_account_id = p_cust_account_id;
   END IF;

COMMIT;

EXCEPTION
 WHEN l_exception THEN
   FND_MESSAGE.SET_NAME('XXWC','XXWC_ADI_ERROR_MSG');
   FND_MESSAGE.SET_TOKEN('ERROR_MESSAGE',l_error_message);
 WHEN OTHERS THEN
   l_error_message := 'Unidentified error in - XXWCAR_UPDATE_CUSTOMER_PKG '||SQLERRM;
   FND_MESSAGE.SET_NAME('XXWC','XXWC_ADI_ERROR_MSG');
   FND_MESSAGE.SET_TOKEN('ERROR_MESSAGE',l_error_message);
END UPDATE_CUST_NOTES;

-- Version# 1.4 > Start

PROCEDURE update_profile_class (p_cust_account_profile_id     IN NUMBER
                             , p_object_version_num       IN NUMBER
                             , p_profile_class_id            IN NUMBER)
          IS
     p_customer_profile_rec_type     HZ_CUSTOMER_PROFILE_V2PUB.CUSTOMER_PROFILE_REC_TYPE;

     x_return_status                 VARCHAR2(2000);
     x_msg_count                     NUMBER;
     x_msg_data                      VARCHAR2(2000);
     l_object_version_num            NUMBER;

     CURSOR cur_cust_prof
         IS
     SELECT *
       FROM hz_customer_profiles
      WHERE cust_account_profile_id = p_cust_account_profile_id;

    BEGIN
      --------------------------------------------------------------------
      -- APPS Initializing
      -- Note: The values are hard-coded as this process will be executed
      -- from "HDS IT Operations Analyst - WC" responsibility
      --------------------------------------------------------------------
      mo_global.init ('AR');
      fnd_global.apps_initialize (user_id           => l_user_id  --fnd_profile.value('user_id')  17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 
                                , resp_id           => l_resp_id  --20678  -- 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 
                                , resp_appl_id      => l_resp_appl_id); -- 222 --  17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 

      mo_global.set_policy_context ('S', l_org_id);   --  17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing
      fnd_global.set_nls_context ('AMERICAN');

      p_customer_profile_rec_type.cust_account_profile_id := p_cust_account_profile_id;
      p_customer_profile_rec_type.profile_class_id        := p_profile_class_id;
      l_object_version_num                                := p_object_version_num;


      FOR rec_cust_prof IN cur_cust_prof LOOP
--        p_customer_profile_rec_type.collector_id          := rec_cust_prof.collector_id;
--        p_customer_profile_rec_type.credit_analyst_id     := rec_cust_prof.credit_analyst_id;

   p_customer_profile_rec_type.cust_account_id                      :=   rec_cust_prof.cust_account_id              ;
   p_customer_profile_rec_type.status                               :=   rec_cust_prof.status                       ;
   p_customer_profile_rec_type.collector_id                         :=   rec_cust_prof.collector_id                 ;
   p_customer_profile_rec_type.credit_analyst_id                    :=   rec_cust_prof.credit_analyst_id            ;
   p_customer_profile_rec_type.credit_checking                      :=   rec_cust_prof.credit_checking              ;
   p_customer_profile_rec_type.next_credit_review_date              :=   rec_cust_prof.next_credit_review_date      ;
   p_customer_profile_rec_type.tolerance                            :=   rec_cust_prof.tolerance                    ;
   p_customer_profile_rec_type.discount_terms                       :=   rec_cust_prof.discount_terms               ;
   p_customer_profile_rec_type.dunning_letters                      :=   rec_cust_prof.dunning_letters              ;
   p_customer_profile_rec_type.interest_charges                     :=   rec_cust_prof.interest_charges             ;
   p_customer_profile_rec_type.send_statements                      :=   rec_cust_prof.send_statements              ;
   p_customer_profile_rec_type.credit_balance_statements            :=   rec_cust_prof.credit_balance_statements    ;
   p_customer_profile_rec_type.credit_hold                          :=   rec_cust_prof.credit_hold                  ;
   p_customer_profile_rec_type.site_use_id                          :=   rec_cust_prof.site_use_id                  ;
   p_customer_profile_rec_type.credit_rating                        :=   rec_cust_prof.credit_rating                ;
   p_customer_profile_rec_type.risk_code                            :=   rec_cust_prof.risk_code                    ;
   p_customer_profile_rec_type.standard_terms                       :=   rec_cust_prof.standard_terms               ;
   p_customer_profile_rec_type.override_terms                       :=   rec_cust_prof.override_terms               ;
   p_customer_profile_rec_type.dunning_letter_set_id                :=   rec_cust_prof.dunning_letter_set_id        ;
   p_customer_profile_rec_type.interest_period_days                 :=   rec_cust_prof.interest_period_days         ;
   p_customer_profile_rec_type.payment_grace_days                   :=   rec_cust_prof.payment_grace_days           ;
   p_customer_profile_rec_type.discount_grace_days                  :=   rec_cust_prof.discount_grace_days          ;
   p_customer_profile_rec_type.statement_cycle_id                   :=   rec_cust_prof.statement_cycle_id           ;
   p_customer_profile_rec_type.account_status                       :=   rec_cust_prof.account_status               ;
   p_customer_profile_rec_type.percent_collectable                  :=   rec_cust_prof.percent_collectable          ;
   p_customer_profile_rec_type.autocash_hierarchy_id                :=   rec_cust_prof.autocash_hierarchy_id        ;
   p_customer_profile_rec_type.attribute_category                   :=   rec_cust_prof.attribute_category           ;
   p_customer_profile_rec_type.attribute1                           :=   rec_cust_prof.attribute1                   ;
   p_customer_profile_rec_type.attribute2                           :=   rec_cust_prof.attribute2                   ;
   p_customer_profile_rec_type.attribute3                           :=   rec_cust_prof.attribute3                   ;
   p_customer_profile_rec_type.attribute4                           :=   rec_cust_prof.attribute4                   ;
   p_customer_profile_rec_type.attribute5                           :=   rec_cust_prof.attribute5                   ;
   p_customer_profile_rec_type.attribute6                           :=   rec_cust_prof.attribute6                   ;
   p_customer_profile_rec_type.attribute7                           :=   rec_cust_prof.attribute7                   ;
   p_customer_profile_rec_type.attribute8                           :=   rec_cust_prof.attribute8                   ;
   p_customer_profile_rec_type.attribute9                           :=   rec_cust_prof.attribute9                   ;
   p_customer_profile_rec_type.attribute10                          :=   rec_cust_prof.attribute10                  ;
   p_customer_profile_rec_type.attribute11                          :=   rec_cust_prof.attribute11                  ;
   p_customer_profile_rec_type.attribute12                          :=   rec_cust_prof.attribute12                  ;
   p_customer_profile_rec_type.attribute13                          :=   rec_cust_prof.attribute13                  ;
   p_customer_profile_rec_type.attribute14                          :=   rec_cust_prof.attribute14                  ;
   p_customer_profile_rec_type.attribute15                          :=   rec_cust_prof.attribute15                  ;
   p_customer_profile_rec_type.auto_rec_incl_disputed_flag          :=   rec_cust_prof.auto_rec_incl_disputed_flag  ;
   p_customer_profile_rec_type.tax_printing_option                  :=   rec_cust_prof.tax_printing_option          ;
   p_customer_profile_rec_type.charge_on_finance_charge_flag        :=   rec_cust_prof.charge_on_finance_charge_flag;
   p_customer_profile_rec_type.grouping_rule_id                     :=   rec_cust_prof.grouping_rule_id             ;
   p_customer_profile_rec_type.clearing_days                        :=   rec_cust_prof.clearing_days                ;
   p_customer_profile_rec_type.jgzz_attribute_category              :=   rec_cust_prof.jgzz_attribute_category      ;
   p_customer_profile_rec_type.jgzz_attribute1                      :=   rec_cust_prof.jgzz_attribute1              ;
   p_customer_profile_rec_type.jgzz_attribute2                      :=   rec_cust_prof.jgzz_attribute2              ;
   p_customer_profile_rec_type.jgzz_attribute3                      :=   rec_cust_prof.jgzz_attribute3              ;
   p_customer_profile_rec_type.jgzz_attribute4                      :=   rec_cust_prof.jgzz_attribute4              ;
   p_customer_profile_rec_type.jgzz_attribute5                      :=   rec_cust_prof.jgzz_attribute5              ;
   p_customer_profile_rec_type.jgzz_attribute6                      :=   rec_cust_prof.jgzz_attribute6              ;
   p_customer_profile_rec_type.jgzz_attribute7                      :=   rec_cust_prof.jgzz_attribute7              ;
   p_customer_profile_rec_type.jgzz_attribute8                      :=   rec_cust_prof.jgzz_attribute8              ;
   p_customer_profile_rec_type.jgzz_attribute9                      :=   rec_cust_prof.jgzz_attribute9              ;
   p_customer_profile_rec_type.jgzz_attribute10                     :=   rec_cust_prof.jgzz_attribute10             ;
   p_customer_profile_rec_type.jgzz_attribute11                     :=   rec_cust_prof.jgzz_attribute11             ;
   p_customer_profile_rec_type.jgzz_attribute12                     :=   rec_cust_prof.jgzz_attribute12             ;
   p_customer_profile_rec_type.jgzz_attribute13                     :=   rec_cust_prof.jgzz_attribute13             ;
   p_customer_profile_rec_type.jgzz_attribute14                     :=   rec_cust_prof.jgzz_attribute14             ;
   p_customer_profile_rec_type.jgzz_attribute15                     :=   rec_cust_prof.jgzz_attribute15             ;
   p_customer_profile_rec_type.global_attribute1                    :=   rec_cust_prof.global_attribute1            ;
   p_customer_profile_rec_type.global_attribute2                    :=   rec_cust_prof.global_attribute2            ;
   p_customer_profile_rec_type.global_attribute3                    :=   rec_cust_prof.global_attribute3            ;
   p_customer_profile_rec_type.global_attribute4                    :=   rec_cust_prof.global_attribute4            ;
   p_customer_profile_rec_type.global_attribute5                    :=   rec_cust_prof.global_attribute5            ;
   p_customer_profile_rec_type.global_attribute6                    :=   rec_cust_prof.global_attribute6            ;
   p_customer_profile_rec_type.global_attribute7                    :=   rec_cust_prof.global_attribute7            ;
   p_customer_profile_rec_type.global_attribute8                    :=   rec_cust_prof.global_attribute8            ;
   p_customer_profile_rec_type.global_attribute9                    :=   rec_cust_prof.global_attribute9            ;
   p_customer_profile_rec_type.global_attribute10                   :=   rec_cust_prof.global_attribute10           ;
   p_customer_profile_rec_type.global_attribute11                   :=   rec_cust_prof.global_attribute11           ;
   p_customer_profile_rec_type.global_attribute12                   :=   rec_cust_prof.global_attribute12           ;
   p_customer_profile_rec_type.global_attribute13                   :=   rec_cust_prof.global_attribute13           ;
   p_customer_profile_rec_type.global_attribute14                   :=   rec_cust_prof.global_attribute14           ;
   p_customer_profile_rec_type.global_attribute15                   :=   rec_cust_prof.global_attribute15           ;
   p_customer_profile_rec_type.global_attribute16                   :=   rec_cust_prof.global_attribute16           ;
   p_customer_profile_rec_type.global_attribute17                   :=   rec_cust_prof.global_attribute17           ;
   p_customer_profile_rec_type.global_attribute18                   :=   rec_cust_prof.global_attribute18           ;
   p_customer_profile_rec_type.global_attribute19                   :=   rec_cust_prof.global_attribute19           ;
   p_customer_profile_rec_type.global_attribute20                   :=   rec_cust_prof.global_attribute20           ;
   p_customer_profile_rec_type.global_attribute_category            :=   rec_cust_prof.global_attribute_category    ;
   p_customer_profile_rec_type.cons_inv_flag                        :=   rec_cust_prof.cons_inv_flag                ;
   p_customer_profile_rec_type.cons_inv_type                        :=   rec_cust_prof.cons_inv_type                ;
   p_customer_profile_rec_type.autocash_hierarchy_id_for_adr        :=   rec_cust_prof.autocash_hierarchy_id_for_adr;
   p_customer_profile_rec_type.lockbox_matching_option              :=   rec_cust_prof.lockbox_matching_option      ;
   p_customer_profile_rec_type.created_by_module                    :=   rec_cust_prof.created_by_module            ;
   p_customer_profile_rec_type.application_id                       :=   rec_cust_prof.application_id               ;
   p_customer_profile_rec_type.review_cycle                         :=   rec_cust_prof.review_cycle                 ;
   p_customer_profile_rec_type.last_credit_review_date              :=   rec_cust_prof.last_credit_review_date      ;
   p_customer_profile_rec_type.party_id                             :=   rec_cust_prof.party_id                     ;
   p_customer_profile_rec_type.credit_classification                :=   rec_cust_prof.credit_classification        ;
   p_customer_profile_rec_type.cons_bill_level                      :=   rec_cust_prof.cons_bill_level              ;
   p_customer_profile_rec_type.late_charge_calculation_trx          :=   rec_cust_prof.late_charge_calculation_trx  ;
   p_customer_profile_rec_type.credit_items_flag                    :=   rec_cust_prof.credit_items_flag            ;
   p_customer_profile_rec_type.disputed_transactions_flag           :=   rec_cust_prof.disputed_transactions_flag   ;
   p_customer_profile_rec_type.late_charge_type                     :=   rec_cust_prof.late_charge_type             ;
   p_customer_profile_rec_type.late_charge_term_id                  :=   rec_cust_prof.late_charge_term_id          ;
   p_customer_profile_rec_type.interest_calculation_period          :=   rec_cust_prof.interest_calculation_period  ;
   p_customer_profile_rec_type.hold_charged_invoices_flag           :=   rec_cust_prof.hold_charged_invoices_flag   ;
   p_customer_profile_rec_type.message_text_id                      :=   rec_cust_prof.message_text_id              ;
   p_customer_profile_rec_type.multiple_interest_rates_flag         :=   rec_cust_prof.multiple_interest_rates_flag ;
   p_customer_profile_rec_type.charge_begin_date                    :=   rec_cust_prof.charge_begin_date            ;
   p_customer_profile_rec_type.automatch_set_id                     :=   rec_cust_prof.automatch_set_id             ;

      END LOOP;

      --------------------------------------------------------------------
      -- API Call to update CustomerProfile
      --------------------------------------------------------------------
      hz_customer_profile_v2pub.update_customer_profile(p_init_msg_list              => fnd_api.g_true
                                      ,                 p_customer_profile_rec       => p_customer_profile_rec_type
                                      ,                 p_object_version_number      => l_object_version_num
                                      ,                 x_return_status              => x_return_status
                                      ,                 x_msg_count                  => x_msg_count
                                      ,                 x_msg_data                   => x_msg_data);

      --------------------------------------------------------------------
      -- Validate the status of API output
      --------------------------------------------------------------------
      IF x_msg_count >1 THEN
         FOR I IN 1..x_msg_count LOOP
           fnd_file.put_line(fnd_file.log,SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255));
         END LOOP;

      --   p_error_message := SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255);
      END IF;

    COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
       fnd_file.put_line(fnd_file.log,'Error Message = '|| SQLERRM);
       -- p_error_message  := SQLERRM;
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWCAR_UPDATE_CUSTOMER_PKG.UPDATE_PROFILE_CLASS'
                                            ,p_calling           => 'OTHERS exception'
                                            ,p_request_id        => NULL
                                            ,p_ora_error_msg     => substr(SQLERRM
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => 'Error running XXWCAR_UPDATE_CUSTOMER_PKG.UPDATE_PROFILE_CLASS procedure with OTHERS Exception'
                                            ,p_distribution_list => 'HDSOracleDevelopers@hdsupply.com'
                                            ,p_module            => 'AR');
END update_profile_class;

PROCEDURE update_no_notice_cust_amt(p_errbuf            OUT      VARCHAR2
                                  , p_retcode           OUT      NUMBER
                                  , p_resp              IN       VARCHAR2
                                  , P_resp_status       IN       VARCHAR2
                                  , p_report_only       IN       VARCHAR2
				                  , p_collector         IN       NUMBER)
/********************************************************************************
Procedure Name: UPDATE_NO_NOTICE_CUST_AMT

Description : API to update Customer Profile Amounts for No-Notice Customers

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)            DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     09/10/2013    Gopi Damuluri        Initial version.
1.1     01/31/2014    Maharajan Shunmugam  TMS#20131126-00046

********************************************************************************/

IS
     l_prof_amt_rec                  HZ_CUSTOMER_PROFILE_V2PUB.cust_profile_amt_rec_type;
     l_obj_version                   NUMBER;
     l_ret_status                    VARCHAR2(2000);
     l_msg_count                     NUMBER;
     l_msg_data                      VARCHAR2(2000);
     l_msg_index                     NUMBER;
     l_cust_account_id               NUMBER;
     l_collector                     NUMBER;

     CURSOR c_cust_prof(p_collector IN VARCHAR2)
         IS
     SELECT hca.account_number
          , hca.account_name
          , hca.cust_account_id
          , to_char(hcpa.overall_credit_limit)  cust_credit_limit
          , hcpa.cust_acct_profile_amt_id       cust_acct_profile_amt_id
          , hcpa.object_version_number          cust_profamt_obj_version_num
          , hcp.cust_account_profile_id         cust_cust_account_profile_id
          , hcp.object_version_number           cust_prof_obj_version_num
          , hcp.profile_class_id                cust_profile_class_id
          , ac.name                             collector
          , hcpa.attribute1
       FROM hz_customer_profiles                hcp
          , hz_cust_profile_classes             hcpc
          , hz_cust_profile_amts                hcpa
          , hz_cust_accounts                    hca   -- hz_cust_accounts_all TO hz_cust_accounts on 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 
	      , ar_collectors                       ac
      WHERE 1 = 1       
        AND hcpc.name                         = 'Contractor - No Notice'
        AND hcp.profile_class_id              = hcpc.profile_class_id
        AND hcpa.cust_account_id              = hca.cust_account_id
        AND hcpa.cust_account_profile_id      = hcp.cust_account_profile_id
        AND hcp.collector_id                  = ac.collector_id
        AND hcp.site_use_id                  IS NULL
        AND hcpa.site_use_id                 IS NULL
        AND ac.status			      ='A'
        AND hcp.collector_id                  = NVL(l_collector,hcp.collector_id)
      ORDER BY 1, 2;

     CURSOR c_cust_site_prof (p_customer_id  IN NUMBER)
         IS
     SELECT to_char(hcpa.overall_credit_limit)  site_credit_limit
          , hcpa.cust_acct_profile_amt_id       site_acct_profile_amt_id
          , hcpa.object_version_number          site_profamt_obj_version_num
          , hcp.cust_account_profile_id         site_cust_account_profile_id
          , hcp.object_version_number           site_prof_obj_version_num
          , hcpc.name                           site_prof_class
          , hcsu.location                       location
          , hps.party_site_number               site_number
       FROM hz_customer_profiles                hcp
          , hz_cust_profile_classes             hcpc
          , hz_cust_profile_amts                hcpa
          , hz_cust_site_uses                   hcsu  -- hz_cust_site_useS_ALL TO hz_cust_site_uses on 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 
          , hz_cust_acct_sites                  hcas  -- hz_cust_acct_sites_all to hz_cust_acct_sites on 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 
          , hz_party_sites                       hps
      WHERE 1 = 1       
        AND hcp.cust_account_id               = p_customer_id
        AND hcpa.cust_account_id              = p_customer_id
        AND hcsu.site_use_id                  = hcpa.site_use_id
        AND hcp.site_use_id                   = hcpa.site_use_id
        AND hcpa.cust_account_profile_id      = hcp.cust_account_profile_id
        AND hcp.profile_class_id              = hcpc.profile_class_id
        AND hcas.cust_acct_site_id            = hcsu.cust_acct_site_id
        AND hcas.party_site_id                = hps.party_site_id
        AND hcp.site_use_id                  IS NOT NULL
        AND hcpa.site_use_id                 IS NOT NULL
      ORDER BY 1, 2;

BEGIN
IF p_collector = 1 THEN
l_collector := NULL;
ELSE
l_collector := p_collector;
END IF;

IF p_report_only = 'Y' OR p_report_only is NULL THEN
--   IF p_report_only = 'Y' THEN
      fnd_file.put_line(fnd_file.output,'################################################################################################################################################################################################################################################');
      fnd_file.put_line(fnd_file.output,'Customer#             CustomerName                              Collector                                 CustCredLimit         SiteNumber            SiteLocation                                                                 SiteCredLimit ');
      fnd_file.put_line(fnd_file.output,'################################################################################################################################################################################################################################################');
      
      FOR r_cust IN c_cust_prof(p_collector) LOOP
          l_cust_account_id :=r_cust.cust_account_id;
          FOR r_site IN c_cust_site_prof (l_cust_account_id) LOOP
              fnd_file.put_line(fnd_file.output, RPAD(NVL(r_cust.account_number, ' '),20,' ')    ||'  '||
                                                 RPAD(NVL(r_cust.account_name, ' '),40,' ')      ||'  '||
                                                 RPAD(NVL(r_cust.collector, ' '),40,' ')      ||'  '||
                                                 RPAD(NVL(r_cust.cust_credit_limit,' '),20,' ')   ||'  '||
                                                 RPAD(NVL(r_site.site_number,' '),20,' ')           ||'  '||
                                                 RPAD(NVL(r_site.location,' '),75,' ')           ||'  '||
                                                 RPAD(NVL(r_site.site_credit_limit, ' '),20,' ')
                          );
          END LOOP;  -- r_site
      END LOOP;  -- r_cust
   ELSE  -- p_report_only = 'N'

      --------------------------------------------------------------------
      -- Delete from staging table
      --------------------------------------------------------------------
      DELETE FROM xxwc_update_cust_prof_amt_tbl;

      FOR r_cust_prof IN c_cust_prof(p_collector) LOOP
         l_cust_account_id :=r_cust_prof.cust_account_id;
         IF r_cust_prof.attribute1 IS NOT NULL THEN
            --------------------------------------------------------------------
            -- Update Customer ProfileAmount to NULL
            --------------------------------------------------------------------
            update hz_cust_profile_amts
               set attribute1 = NULL
             where 1 = 1
               and cust_acct_profile_amt_id = r_cust_prof.cust_acct_profile_amt_id;
/*
            INSERT INTO xxwc_update_cust_prof_amt_tbl (CUST_ACCT_PROFILE_AMT_ID
                                                          , OBJECT_VERSION_NUMBER
                                                          , STATUS)
                                                   VALUES (r_cust_prof.cust_acct_profile_amt_id
                                                         , r_cust_prof.cust_profamt_obj_version_num
                                                         , 'N');
*/
         END IF;

         FOR r_site_prof IN c_cust_site_prof(l_cust_account_id)
         LOOP
           --------------------------------------------------------------------
           -- Update Customer Site ProfileAmount to NULL
           --------------------------------------------------------------------
           IF r_site_prof.site_credit_limit IS NOT NULL THEN
              INSERT INTO xxwc_update_cust_prof_amt_tbl (CUST_ACCT_PROFILE_AMT_ID
                                                            , OBJECT_VERSION_NUMBER
                                                            , STATUS)
                                                      VALUES (r_site_prof.site_acct_profile_amt_id
                                                            , r_site_prof.site_profamt_obj_version_num
                                                            , 'N');
           END IF;

           IF r_site_prof.site_prof_class != 'Contractor - No Notice' THEN

              --------------------------------------------------------------------
              -- Update CustomerSite ProfileClass to 'Contractor - No Notice'
              --------------------------------------------------------------------
              update_profile_class(r_site_prof.site_cust_account_profile_id
                                     , r_site_prof.site_prof_obj_version_num
                                     , r_cust_prof.cust_profile_class_id);
           END IF;
         END LOOP; -- c_cust_site_prof
      END LOOP;  -- c_cust_prof
   COMMIT;

      --------------------------------------------------------------------
      -- API to Update ProfileAmounts
      --------------------------------------------------------------------
      update_cust_prof_amt(p_errbuf, p_retcode);

   COMMIT;

   END IF;  -- p_report_only = 'Y'

EXCEPTION
 WHEN OTHERS THEN
    fnd_file.put_line(fnd_file.log,'Error Message = '|| SQLERRM);

    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWCAR_UPDATE_CUSTOMER_PKG.update_no_notice_cust_amt'
                                        ,p_calling           => 'OTHERS exception'
                                        ,p_request_id        => NULL
                                        ,p_ora_error_msg     => substr(SQLERRM
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => 'Error running XXWCAR_UPDATE_CUSTOMER_PKG.update_no_notice_cust_amt procedure with OTHERS Exception'
                                        ,p_distribution_list => 'HDSOracleDevelopers@hdsupply.com'
                                        ,p_module            => 'AR');
END UPDATE_NO_NOTICE_CUST_AMT;

/********************************************************************************
Procedure Name: UPDATE_CUST_CLASSIFICATION

Description : API to update Customer's New Classification

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     10/11/2013    Gopi Damuluri    Initial version.
********************************************************************************/

PROCEDURE update_cust_classification(p_errbuf            OUT      VARCHAR2
                                   , p_retcode           OUT      NUMBER) IS

-- ====================================================
v_application_short_code  varchar2(3) :='AR';
v_loc                     varchar2(3) :=Null;
n_org_id                  number      :=mo_global.get_current_org_id;  --162 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing
n_obj_ver_num             number      :=Null;
-- ====================================================  

 CURSOR all_profiles IS
  SELECT stg.cust_profile_id
       , stg.credit_classification
    FROM xxcus.xxcus_upd_ar_cust_profiles stg
       , ar.hz_customer_profiles hcp
   WHERE 1 =1
     AND stg.cust_profile_id  = hcp.cust_account_profile_id
     AND stg.credit_classification != hcp.credit_classification;

 type prof_tbl is table of all_profiles%rowtype index by binary_integer;
 prof_rec prof_tbl;

-- ==================================================== 
 l_customer_profile_rec_type  HZ_CUSTOMER_PROFILE_V2PUB.customer_profile_rec_type;
 l_return_status              VARCHAR2(1); 
 l_msg_count                  NUMBER :=0; 
 l_msg_data                   VARCHAR2(2000);
-- ==================================================== 

BEGIN
  --
  mo_global.init(v_application_short_code);
  mo_global.set_policy_context('S', n_org_id);
  v_loc :='001';

--
OPEN all_profiles;
--
LOOP 
 --
  FETCH all_profiles BULK COLLECT INTO prof_rec LIMIT 5000;
  EXIT WHEN prof_rec.count =0;
   v_loc :='002'; 
   -- 
  IF prof_rec.count >0 THEN 
   --
   v_loc :='003'; 
   -- 
   FOR idx IN prof_rec.first .. prof_rec.last LOOP 
    --
       BEGIN
        --
        v_loc :='004';
        --
        SELECT object_version_number
          INTO n_obj_ver_num
          FROM hz_customer_profiles
         WHERE 1 =1
           AND cust_account_profile_id =prof_rec(idx).cust_profile_id;
        --
        v_loc :='005';
        --        
       EXCEPTION
       WHEN NO_DATA_FOUND THEN
         v_loc :='109';
         fnd_file.put_line(fnd_file.log,'Location ='||v_loc||', no data found for customer profile id ='||prof_rec(idx).cust_profile_id);
         n_obj_ver_num:=Null;
        --
       WHEN OTHERS THEN
         fnd_file.put_line(fnd_file.log,'Location ='||v_loc||', when others for customer profile id ='||prof_rec(idx).cust_profile_id);
         fnd_file.put_line(fnd_file.log,'Location ='||v_loc||', when others, message ='||sqlerrm);         
         n_obj_ver_num :=Null;
        --
       END;

       BEGIN
       --
         l_customer_profile_rec_type.cust_account_profile_id :=prof_rec(idx).cust_profile_id;
         l_customer_profile_rec_type.credit_classification   :=prof_rec(idx).credit_classification;         
       --
         savepoint start_here;
          v_loc :='006';
            HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile (
                p_init_msg_list             =>fnd_api.g_false,
                p_customer_profile_rec      =>l_customer_profile_rec_type,
                p_object_version_number     =>n_obj_ver_num,
                x_return_status             =>l_return_status,
                x_msg_count                 =>l_msg_count,
                x_msg_data                  =>l_msg_data
            );
          IF l_return_status <>'S' then  
            fnd_file.put_line(fnd_file.log,'API status  ='||l_return_status);
            fnd_file.put_line(fnd_file.log,'API Message ='||l_msg_data);
          END IF;
        --     
       EXCEPTION 
       --
        WHEN OTHERS THEN
          rollback to start_here;
          v_loc :='110'; 
          fnd_file.put_line(fnd_file.log,'Location ='||v_loc);
          fnd_file.put_line(fnd_file.log,'Message  ='||sqlerrm);  
       --   
       END;
       --    
   END LOOP; --end if for idx loop 
  --
  END IF; --prof_rec.count >0 
  --
END LOOP;
--
CLOSE all_profiles;
--
COMMIT;
EXCEPTION
WHEN OTHERS THEN
  v_loc :='111'; 
  fnd_file.put_line(fnd_file.log,'Outer location ='||v_loc);
  fnd_file.put_line(fnd_file.log,'Outer block message  ='||sqlerrm);   
  rollback;
END update_cust_classification;

/********************************************************************************
Procedure Name: GET_NEW_CUST_CLASSIFICATION

Description : API to get Customer New Classification using Preventive Metrics
              from GetPaid

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     10/11/2013    Gopi Damuluri    Initial version.
********************************************************************************/

PROCEDURE get_new_cust_classification(p_raw_score    IN  NUMBER
                                    , p_new_classif  OUT VARCHAR2)
IS
BEGIN
  IF p_raw_score = 999 THEN p_new_classif := 'SIGDEL';
  ELSIF p_raw_score > -999999 AND p_raw_score <= 30 THEN p_new_classif := 'VHIGH';
  ELSIF p_raw_score > 30 AND p_raw_score <= 50 THEN p_new_classif := 'HIGH';
  ELSIF p_raw_score > 50 AND p_raw_score <= 60 THEN p_new_classif := 'MODERATE';
  ELSIF p_raw_score > 60 AND p_raw_score <= 70 THEN p_new_classif := 'LOW';
  ELSIF p_raw_score > 70 AND p_raw_score <= 100 THEN p_new_classif := 'VLOW';
  ELSE p_new_classif := 'NORATING';
  END IF;
EXCEPTION
WHEN OTHERS THEN
  fnd_file.put_line(fnd_file.log,'Error Message = '|| SQLERRM);
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWCAR_UPDATE_CUSTOMER_PKG.get_new_cust_classification'
                                        ,p_calling           => 'OTHERS exception'
                                        ,p_request_id        => NULL
                                        ,p_ora_error_msg     => substr(SQLERRM
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => 'Error running XXWCAR_UPDATE_CUSTOMER_PKG.get_new_cust_classification procedure with OTHERS Exception'
                                        ,p_distribution_list => 'HDSOracleDevelopers@hdsupply.com'
                                        ,p_module            => 'AR');
END get_new_cust_classification;

/********************************************************************************
Procedure Name: UPDATE_CUST_RISK_INFO

Description : API to update Customer Risk Information

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     10/11/2013    Gopi Damuluri    Initial version.
********************************************************************************/

PROCEDURE update_cust_risk_info (p_errbuf            OUT      VARCHAR2
                               , p_retcode           OUT      NUMBER)
IS

   --------------------------------------------------------
   -- Cursor to get GetPaid Predective Metrics data
   --------------------------------------------------------
   CURSOR cur_gp_pm
       IS
   SELECT customer_number  
        , raw_score        
        , created_by       
        , creation_date    
        , last_updated_by  
        , last_update_date 
        , status           
        , error_message    
     FROM xxwc_getpaid_pred_metrics_tbl
    WHERE NVL(status,'N') != 'S';

   --------------------------------------------------------
   -- Cursor to get Oracle Customer Profiles Info
   --------------------------------------------------------
   CURSOR cur_oca_cust_prof(p_cust_num  IN VARCHAR2)
       IS
   SELECT a.cust_account_profile_id,
          a.cust_account_id,
          a.site_use_id,
          a.credit_classification,
          b.account_number
     FROM ar.hz_customer_profiles a,
          ar.hz_cust_accounts b
    WHERE 1 = 1
      AND b.account_number        = p_cust_num
      AND a.cust_account_id       = b.cust_account_id
      AND a.credit_classification NOT IN ('CSALE');

   --------------------------------------------------------
   -- Cursor to get New Customer Credit Limits
   --------------------------------------------------------
   CURSOR cur_credit_lmt
       IS
   SELECT hcpa.cust_acct_profile_amt_id,
          hcpa.object_version_number,
          10000   UPDATED_CREDIT_LIMIT
     FROM ar.hz_cust_accounts hca,
          ar.hz_customer_profiles hcp,
          ar.hz_cust_profile_amts hcpa,
          ar.hz_cust_profile_classes hcpc,
          xxcus.xxcus_upd_ar_cust_profiles stg
    WHERE 1 = 1     
      AND hcp.cust_account_profile_id  = stg.cust_profile_id
      AND hcpa.cust_account_profile_id = stg.cust_profile_id
      AND hca.cust_account_id          = hcp.cust_account_id      
      AND hcp.cust_account_profile_id  = hcpa.cust_account_profile_id
      AND hcp.site_use_id             IS NULL
      AND hcp.credit_classification   IN ('VLOW', 'LOW', 'MODERATE')
      AND hcpc.profile_class_id        = hcp.profile_class_id
      AND hcpc.name                   <>'Contractor - Jobs Only'
      AND SYSDATE - hca.account_established_date > 365
      AND (hcp.credit_rating = 'Y' OR hcp.credit_rating IS NULL)
      AND hcpa.overall_credit_limit    < 10000
   UNION
   SELECT hcpa.cust_acct_profile_amt_id,
          hcpa.object_version_number,
          10000   UPDATED_CREDIT_LIMIT
     FROM ar.hz_cust_accounts hca,
          ar.hz_customer_profiles hcp_c,
          ar.hz_customer_profiles hcp,
          ar.hz_cust_profile_amts hcpa,
          ar.hz_cust_profile_classes hcpc,
          hz_cust_site_uses hcsu,    -- hz_cust_site_uses_all TO hz_cust_site_uses on 17/09/2014 Added by pattabhi for TMS#20141001-00029 OU Testing 
          xxcus.xxcus_upd_ar_cust_profiles stg
    WHERE 1 = 1     
      AND hcp_c.cust_account_profile_id  = stg.cust_profile_id
      AND hcp.cust_account_id          = hcp_c.cust_account_id  
      AND hca.cust_account_id          = hcp.cust_account_id      
      AND hcp.cust_account_profile_id  = hcpa.cust_account_profile_id
      AND hcp.site_use_id             IS NOT NULL
      AND hcp.credit_classification   IN ('VLOW', 'LOW', 'MODERATE')
      AND hcpc.profile_class_id        = hcp.profile_class_id
      AND hcpc.name                   <>'Contractor - Jobs Only'
      AND SYSDATE - hca.account_established_date > 365
      AND (hcp.credit_rating = 'Y' OR hcp.credit_rating IS NULL)
      AND (hcp_c.credit_rating = 'Y' OR hcp_c.credit_rating IS NULL)
      AND hcsu.site_use_id             = hcp.site_use_id
      AND hcsu.attribute1             IN ('YARD', 'MSTR','MISC')
      AND hcpa.overall_credit_limit    < 10000;

l_errbuf               VARCHAR2(2000);
l_retcode              NUMBER;

l_new_classification   VARCHAR2(50);

l_sec                  VARCHAR2(100);

BEGIN

  DELETE FROM xxcus.xxcus_upd_ar_cust_profiles;
  DELETE FROM xxwc_update_cust_prof_amt_tbl;

  --------------------------------------------------------
  -- Insert into Credit Classification staging table
  --------------------------------------------------------
  FOR rec_pm IN cur_gp_pm LOOP
    FOR rec_prof IN cur_oca_cust_prof (rec_pm.customer_number) LOOP

      get_new_cust_classification(rec_pm.raw_score
                                , l_new_classification);

      fnd_file.put_line(fnd_file.log,'Customer# = '|| rec_pm.customer_number);
      fnd_file.put_line(fnd_file.log,'RawScore = '|| rec_pm.raw_score);
      fnd_file.put_line(fnd_file.log,'OldClassification = '|| rec_prof.credit_classification);
      fnd_file.put_line(fnd_file.log,'NewClassification = '|| l_new_classification);

--      IF rec_prof.credit_classification != l_new_classification THEN
        INSERT INTO xxcus.xxcus_upd_ar_cust_profiles (CUST_PROFILE_ID
                                                    , CREDIT_CLASSIFICATION) 
                                              VALUES (rec_prof.cust_account_profile_id
                                                    , l_new_classification);
--      END IF;

    END LOOP; -- cur_oca_cust_prof

    UPDATE xxwc_getpaid_pred_metrics_tbl
       SET STATUS = 'S'
     WHERE 1 = 1
       AND customer_number = rec_pm.customer_number;
  END LOOP; -- cur_gp_pm

  --------------------------------------------------------
  -- Update Customer Classification - NORATING
  --------------------------------------------------------
  INSERT INTO xxcus.xxcus_upd_ar_cust_profiles (CUST_PROFILE_ID
                                              , CREDIT_CLASSIFICATION)
                                     SELECT hcp.CUST_ACCOUNT_PROFILE_ID
                                          , 'NORATING'
                                       FROM ar.hz_customer_profiles hcp
                                      WHERE 1 = 1 
                                        AND hcp.credit_classification != 'CSALE'
                                        AND NOT EXISTS (SELECT '1' 
                                                          FROM xxcus.xxcus_upd_ar_cust_profiles stg 
                                                         WHERE stg.CUST_PROFILE_ID = hcp.CUST_ACCOUNT_PROFILE_ID);

  --------------------------------------------------------
  -- Call API to update Customer Classification
  --------------------------------------------------------
  update_cust_classification(l_errbuf, l_retcode);

  COMMIT;
  --------------------------------------------------------
  -- Insert into Credit Limit staging table
  --------------------------------------------------------
  FOR rec_credit_lmt IN cur_credit_lmt LOOP

    INSERT INTO xxwc_update_cust_prof_amt_tbl (CUST_ACCT_PROFILE_AMT_ID
                                                  , OBJECT_VERSION_NUMBER
                                                  , UPDATED_CREDIT_LIMIT) 
                                           VALUES (rec_credit_lmt.CUST_ACCT_PROFILE_AMT_ID 
                                                 , rec_credit_lmt.OBJECT_VERSION_NUMBER
                                                 , rec_credit_lmt.UPDATED_CREDIT_LIMIT);
  END LOOP;

  --------------------------------------------------------
  -- Call API to update Customer Credit Limit
  --------------------------------------------------------
  update_cust_prof_amt(l_errbuf, l_retcode);

  --------------------------------------------------------
  -- Mark unprocessed records as Error
  --------------------------------------------------------
  UPDATE xxwc_getpaid_pred_metrics_tbl
     SET status = 'E'
       , error_message = 'Customer Not Found'
   WHERE 1 = 1
     AND NVL(status,'N') != 'S';

EXCEPTION
WHEN OTHERS THEN

--  fnd_file.put_line(fnd_file.log,'l_sec = '|| l_sec);
  fnd_file.put_line(fnd_file.log,'Error Message = '|| SQLERRM);
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWCAR_UPDATE_CUSTOMER_PKG.update_cust_risk_info'
                                        ,p_calling           => 'OTHERS exception'
                                        ,p_request_id        => NULL
                                        ,p_ora_error_msg     => substr(SQLERRM
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => 'Error running XXWCAR_UPDATE_CUSTOMER_PKG.update_cust_risk_info procedure with OTHERS Exception'
                                        ,p_distribution_list => 'HDSOracleDevelopers@hdsupply.com'
                                        ,p_module            => 'AR');
END UPDATE_CUST_RISK_INFO;
-- Version# 1.4 < End

END XXWCAR_UPDATE_CUSTOMER_PKG;


