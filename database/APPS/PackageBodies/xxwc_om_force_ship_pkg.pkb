/* Formatted on 11/6/2013 2:48:39 PM (QP5 v5.206) */
-- Start of DDL Script for Package Body APPS.XXWC_OM_FORCE_SHIP_PKG
-- Generated 11/6/2013 2:48:38 PM from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE BODY apps.xxwc_om_force_ship_pkg
AS
    /*************************************************************************
      $Header xxwc_om_force_ship_pkg $
      Module Name: xxwc_om_force_ship_pkg.pkb

      PURPOSE:   This package is called by the "Force Ship" extension

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        07/23/2012  Shankar Hariharan        Initial Version
      1.1       10/12/2012   Shankar Hariharan        Made changes to cursor to exclude order
                                                     lines that are in GeneralPCK subinv
      2.0       3/11/2014    Ram Talluri            Added procedure to submit interface trip stop for TMS #20140310-00017
	  2.1       10/28/2014   Pattabhi Avula         Canada OU Changes done for TMS# 20141002-00060
    **************************************************************************/

    /*************************************************************************
      Procedure : Write_Log

      PURPOSE:   This procedure logs debug message in Concurrent Log file
      Parameter:
             IN
                 p_debug_msg      -- Debug Message
    ************************************************************************/
    PROCEDURE write_log (p_debug_msg IN VARCHAR2)
    IS
    BEGIN
        fnd_file.put_line (fnd_file.LOG, p_debug_msg);
        DBMS_OUTPUT.put_line (p_debug_msg);
    END write_log;

    /*************************************************************************
       Procedure : Write_Error

      PURPOSE:   This procedure logs error message
      Parameter:
             IN
                 p_debug_msg      -- Debug Message
    ************************************************************************/
    PROCEDURE write_error (p_debug_msg IN VARCHAR2, p_call_point IN VARCHAR2)
    IS
        l_req_id          NUMBER := fnd_global.conc_request_id;
        l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_OM_FORCE_SHIP_PKG';
        l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
        l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    BEGIN
        xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000)
           ,p_error_desc          => 'Error running xxwc_om_force_ship_pkg with PROGRAM ERROR'
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'WSH');
    END write_error;

    /*************************************************************************
      Procedure : Update Sales Order Line Subinventory
    **************************************************************************/
    PROCEDURE update_order_line (p_header_id       IN     NUMBER
                                ,p_line_id         IN     NUMBER
                                ,p_return_status      OUT VARCHAR2
                                ,p_return_msg         OUT VARCHAR2)
    IS
        l_header_rec               oe_order_pub.header_rec_type;
        o_header_rec               oe_order_pub.header_rec_type;
        o_header_val_rec           oe_order_pub.header_val_rec_type;
        o_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
        o_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type;
        o_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type;
        o_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type;
        o_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type;
        o_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type;
        o_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type;
        l_line_tbl                 oe_order_pub.line_tbl_type;
        o_line_tbl                 oe_order_pub.line_tbl_type;
        o_line_val_tbl             oe_order_pub.line_val_tbl_type;
        o_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
        o_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type;
        o_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type;
        o_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type;
        o_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type;
        o_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
        o_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type;
        o_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type;
        o_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type;
        l_action_request_tbl       oe_order_pub.request_tbl_type;
        o_action_request_tbl       oe_order_pub.request_tbl_type;
        l_return_status            VARCHAR2 (240);
        l_msg_count                NUMBER;
        l_msg_data                 VARCHAR2 (4000);
    BEGIN
        oe_msg_pub.initialize;
        /*
          FND_GLOBAL.apps_initialize ( user_id => 16010,
                                       resp_id => 50886,
                                       resp_appl_id => 660 );
         mo_global.init('ONT'); */
        l_line_tbl (1) := oe_order_pub.g_miss_line_rec;
        l_line_tbl (1).line_id := p_line_id;
        l_line_tbl (1).header_id := p_header_id;
        l_line_tbl (1).subinventory := g_pick_subinv;
        l_line_tbl (1).operation := oe_globals.g_opr_update;
        oe_order_pub.process_order (p_api_version_number       => 1.0
                                   ,p_header_rec               => l_header_rec
                                   ,p_line_tbl                 => l_line_tbl
                                   ,p_action_request_tbl       => l_action_request_tbl
                                   ,x_return_status            => l_return_status
                                   ,x_msg_count                => l_msg_count
                                   ,x_msg_data                 => l_msg_data
                                   ,x_header_rec               => o_header_rec
                                   ,x_header_val_rec           => o_header_val_rec
                                   ,x_header_adj_tbl           => o_header_adj_tbl
                                   ,x_header_adj_val_tbl       => o_header_adj_val_tbl
                                   ,x_header_price_att_tbl     => o_header_price_att_tbl
                                   ,x_header_adj_att_tbl       => o_header_adj_att_tbl
                                   ,x_header_adj_assoc_tbl     => o_header_adj_assoc_tbl
                                   ,x_header_scredit_tbl       => o_header_scredit_tbl
                                   ,x_header_scredit_val_tbl   => o_header_scredit_val_tbl
                                   ,x_line_tbl                 => o_line_tbl
                                   ,x_line_val_tbl             => o_line_val_tbl
                                   ,x_line_adj_tbl             => o_line_adj_tbl
                                   ,x_line_adj_val_tbl         => o_line_adj_val_tbl
                                   ,x_line_price_att_tbl       => o_line_price_att_tbl
                                   ,x_line_adj_att_tbl         => o_line_adj_att_tbl
                                   ,x_line_adj_assoc_tbl       => o_line_adj_assoc_tbl
                                   ,x_line_scredit_tbl         => o_line_scredit_tbl
                                   ,x_line_scredit_val_tbl     => o_line_scredit_val_tbl
                                   ,x_lot_serial_tbl           => o_lot_serial_tbl
                                   ,x_lot_serial_val_tbl       => o_lot_serial_val_tbl
                                   ,x_action_request_tbl       => o_action_request_tbl);
        write_log ('  1.1.Order Update.' || l_return_status || '=' || l_msg_data || '=' || l_msg_count);

        IF (l_return_status != fnd_api.g_ret_sts_success)
        THEN
            IF (l_msg_count > 0)
            THEN
                FOR i IN 1 .. l_msg_count
                LOOP
                    l_msg_data := SUBSTR (l_msg_data || oe_msg_pub.get (p_msg_index => i, p_encoded => 'F'), 1, 2000);
                    write_log ('  1.2.Order Update.' || l_return_status || '=' || l_msg_data || '=' || l_msg_count);
                END LOOP;

                p_return_msg := SUBSTR (l_msg_data, 1, 80);
                p_return_status := l_return_status;
            ELSE
                p_return_msg := SUBSTR (l_msg_data, 1, 80);
                p_return_status := l_return_status;
                write_log ('  3.1.Unable to Update the Order.' || l_msg_data);
            END IF;
        ELSIF (l_return_status = fnd_api.g_ret_sts_success)
        THEN
            p_return_status := l_return_status;
        END IF;
    END update_order_line;

    /*************************************************************************
      Procedure : Process_Subinv_Transf
    **************************************************************************/
    PROCEDURE process_subinv_transfer (p_organization_id     IN     NUMBER
                                      ,p_inventory_item_id   IN     NUMBER
                                      ,p_transfer_qty        IN     NUMBER
                                      ,p_transfer_uom        IN     VARCHAR2
                                      ,p_header_id           IN     NUMBER
                                      ,p_line_id             IN     NUMBER
                                      ,p_lot_control         IN     VARCHAR2
                                      ,p_lot_number          IN     VARCHAR2
                                      ,p_return_status          OUT VARCHAR2
                                      ,p_return_msg             OUT VARCHAR2)
    IS
        l_transaction_header_id      NUMBER;
        l_interface_transaction_id   NUMBER;
        l_count_serials              NUMBER;
        l_transaction_type_id        NUMBER;
        l_transaction_uom            VARCHAR2 (3);
        l_transaction_qty            NUMBER;
        l_gral_transaction_qty       NUMBER;
        l_processing_return          NUMBER;
        l_return_status              VARCHAR2 (240);
        l_msg_count                  NUMBER;
        l_msg_data                   VARCHAR2 (4000);
        l_trans_count                NUMBER;
    BEGIN
        write_log ('  3.1. Entering process_subinv_transf for Line_ID ' || p_line_id);
        l_transaction_header_id := NULL;
        l_interface_transaction_id := NULL;
        l_count_serials := NULL;
        l_transaction_type_id := NULL;
        l_transaction_uom := NULL;
        l_transaction_qty := NULL;
        l_gral_transaction_qty := NULL;

        -- Finding Transaction Type ID
        BEGIN
            SELECT transaction_type_id
              INTO l_transaction_type_id
              FROM mtl_transaction_types
             WHERE transaction_type_name = 'Subinventory Transfer';
        EXCEPTION
            WHEN OTHERS
            THEN
                write_log ('  3.1. Err Could not find transaction type information for Subinventory Transfer');
        END;

        l_transaction_uom := p_transfer_uom;
        l_transaction_qty := p_transfer_qty;

        IF l_transaction_qty > 0
        THEN
            write_log ('  3.1. Subinventory Xfer to GeneralPCK...');

            SELECT mtl_material_transactions_s.NEXTVAL INTO l_transaction_header_id FROM DUAL;

            SELECT mtl_material_transactions_s.NEXTVAL INTO l_interface_transaction_id FROM DUAL;

            BEGIN
                INSERT INTO mtl_transactions_interface (transaction_interface_id
                                                       ,transaction_header_id
                                                       ,source_code
                                                       ,source_line_id
                                                       ,source_header_id
                                                       ,process_flag
                                                       ,transaction_mode
                                                       ,lock_flag
                                                       ,last_update_date
                                                       ,last_updated_by
                                                       ,creation_date
                                                       ,created_by
                                                       ,transaction_type_id
                                                       ,transaction_date
                                                       ,transaction_uom
                                                       ,inventory_item_id
                                                       ,subinventory_code
                                                       ,organization_id
                                                       ,transfer_subinventory
                                                       ,transfer_organization
                                                       ,transaction_quantity                        --, primary_quantity
                                                                            )
                     VALUES (l_interface_transaction_id
                            ,l_transaction_header_id
                            ,'Subinventory Transfer'
                            ,                                                                              --source code
                             l_interface_transaction_id
                            ,                                                                           --source line id
                             l_interface_transaction_id
                            ,                                                                         --source header id
                             1
                            ,                                                                             --process flag
                             3
                            ,                                                                         --transaction mode
                             2
                            ,                                                                                --lock flag
                             SYSDATE
                            ,                                                                         --last update date
                             0
                            ,                                                                          --last updated by
                             SYSDATE
                            ,                                                                             --created date
                             0
                            ,                                                                               --created by
                             l_transaction_type_id
                            ,                                                                        -- transaction type
                             SYSDATE
                            ,                                                                        -- transaction date
                             l_transaction_uom
                            ,                                                                                     -- uom
                             p_inventory_item_id
                            ,                                                                        --inventory item id
                             g_subinv
                            ,                                    -- from subinventory (data pulled from move order lines
                             p_organization_id
                            ,g_pick_subinv
                            ,p_organization_id
                            ,l_transaction_qty                                                     --, l_transaction_qty
                                              );

                IF p_lot_control != '1'
                THEN
                    INSERT INTO mtl_transaction_lots_interface (transaction_interface_id
                                                               ,last_update_date
                                                               ,last_updated_by
                                                               ,creation_date
                                                               ,created_by
                                                               ,lot_number
                                                               ,transaction_quantity)
                         VALUES (l_interface_transaction_id
                                ,SYSDATE
                                ,0
                                ,SYSDATE
                                ,0
                                ,p_lot_number
                                ,l_transaction_qty);
                END IF;
            EXCEPTION
                WHEN OTHERS
                THEN
                    write_log ('  3.1. Error inserting into MTI (Subinv Transfer) for Line_ID ' || p_line_id);
                    p_return_status := 'E';
                    RETURN;
            END;

            -- Calling API to transact record
            l_processing_return := NULL;
            l_return_status := NULL;
            l_msg_count := NULL;
            l_msg_data := NULL;
            l_trans_count := NULL;
            l_processing_return :=
                inv_txn_manager_pub.process_transactions (p_api_version        => 1.0
                                                         ,p_init_msg_list      => fnd_api.g_true
                                                         ,p_commit             => fnd_api.g_true
                                                         ,p_validation_level   => fnd_api.g_valid_level_full
                                                         ,x_return_status      => l_return_status
                                                         ,x_msg_count          => l_msg_count
                                                         ,x_msg_data           => l_msg_data
                                                         ,x_trans_count        => l_trans_count
                                                         ,p_table              => 1
                                                         ,p_header_id          => l_transaction_header_id);
            write_log ('  3.1. l_processing_return: ' || l_processing_return);
            write_log ('  3.1. l_return_status: ' || l_return_status);
            write_log ('  3.1. l_msg_count: ' || l_msg_count);
            write_log ('  3.1. l_msg_data: ' || l_msg_data);
            write_log ('  3.1. l_trans_count: ' || l_trans_count);

            IF NVL (l_return_status, 'E') <> 'S'
            THEN
                p_return_status := l_return_status;

                IF (NVL (l_msg_count, 0) = 0)
                THEN
                    write_log ('  3.1. no message return');
                ELSE
                    FOR i IN 1 .. l_msg_count
                    LOOP
                        l_msg_data := l_msg_data || '-' || fnd_msg_pub.get (i, 'F');
                        write_log ('  3.3. Message: ' || l_msg_data);
                    END LOOP;
                END IF;

                p_return_msg := l_msg_data;
            ELSE
                p_return_status := l_return_status;
            END IF;
        ELSE
            write_log ('  3.1. No quantity to transact for LINE_ID ' || p_line_id);
            p_return_status := 'S';
        END IF;

        p_return_status := NVL (l_return_status, 'S');                                                    --'COMPLETED';
        write_log ('  3.1. Exiting process_subinv_transf for LINE_ID ' || p_line_id);
    END process_subinv_transfer;

    PROCEDURE query_reservation (p_inventory_item_id   IN     NUMBER
                                ,p_organization_id     IN     NUMBER
                                ,p_line_id             IN     NUMBER
                                ,p_header_id           IN     NUMBER
                                ,p_rsv_tbl                OUT inv_reservation_global.mtl_reservation_tbl_type
                                ,p_return_status          OUT VARCHAR2
                                ,p_return_msg             OUT VARCHAR2)
    IS
        -- Common Declarations
        l_api_version                 NUMBER := 1.0;
        l_init_msg_list               VARCHAR2 (2) := fnd_api.g_true;
        l_return_status               VARCHAR2 (1);
        l_msg_count                   NUMBER;
        l_msg_data                    VARCHAR2 (240);
        -- WHO columns
        l_user_id                     NUMBER := -1;
        l_resp_id                     NUMBER := -1;
        l_application_id              NUMBER := -1;
        l_row_cnt                     NUMBER := 1;
        l_user_name                   VARCHAR2 (30) := 'SETUPUSER';
        l_resp_name                   VARCHAR2 (30) := 'INVENTORY';
        -- API specific declarations
        l_rsv_rec                     inv_reservation_global.mtl_reservation_rec_type;
        l_rsv_tbl                     inv_reservation_global.mtl_reservation_tbl_type;
        l_serial_number               inv_reservation_global.serial_number_tbl_type;
        l_primary_relieved_qty        NUMBER := 0;
        l_validation_flag             VARCHAR2 (2) := fnd_api.g_true;
        x_mtl_reservation_tbl         inv_reservation_global.mtl_reservation_tbl_type;
        x_mtl_reservation_tbl_count   NUMBER := 0;
        x_primary_relieved_qty        NUMBER := 0;
        x_primary_remain_qty          NUMBER := 0;
        l_open_quantity               NUMBER := 0;
        l_reserved_quantity           NUMBER := 0;
        l_mtl_sales_order_id          NUMBER;
        l_count                       NUMBER;
        l_x_error_code                NUMBER;
        l_lock_records                VARCHAR2 (1);
        l_sort_by_req_date            NUMBER;
        l_converted_qty               NUMBER;
        l_inventory_item_id           NUMBER;
        l_order_quantity_uom          VARCHAR2 (30);
    BEGIN
        l_mtl_sales_order_id := oe_header_util.get_mtl_sales_order_id (p_header_id => p_header_id);
        l_rsv_rec.demand_source_header_id := l_mtl_sales_order_id;
        l_rsv_rec.demand_source_line_id := p_line_id;
        l_rsv_rec.organization_id := NULL;
        inv_reservation_pub.query_reservation_om_hdr_line (p_api_version_number          => 1.0
                                                          ,p_init_msg_lst                => fnd_api.g_true
                                                          ,x_return_status               => l_return_status
                                                          ,x_msg_count                   => l_msg_count
                                                          ,x_msg_data                    => l_msg_data
                                                          ,p_query_input                 => l_rsv_rec
                                                          ,x_mtl_reservation_tbl         => l_rsv_tbl
                                                          ,x_mtl_reservation_tbl_count   => l_count
                                                          ,x_error_code                  => l_x_error_code
                                                          ,p_lock_records                => l_lock_records
                                                          ,p_sort_by_req_date            => l_sort_by_req_date);

        IF l_return_status = 'S'
        THEN
            p_return_status := 'S';
            p_rsv_tbl := l_rsv_tbl;
            write_log ('  1.0. Reservation Record Count=' || l_count);
        ELSE
            IF (NVL (l_msg_count, 0) = 0)
            THEN
                write_log ('  1.0. no message return');
            ELSE
                FOR i IN 1 .. l_msg_count
                LOOP
                    l_msg_data := l_msg_data || '-' || fnd_msg_pub.get (i, 'F');
                    write_log ('  1.0. Message: ' || l_msg_data);
                END LOOP;
            END IF;

            p_return_status := l_return_status;
            p_return_msg := l_msg_data;
        END IF;
    END query_reservation;

    /*************************************************************************
      Procedure : create_reservation
    **************************************************************************/
    PROCEDURE create_reservation (p_inventory_item_id   IN     NUMBER
                                 ,p_organization_id     IN     NUMBER
                                 ,p_reservation_qty     IN     NUMBER
                                 ,p_reservation_uom     IN     VARCHAR2
                                 ,p_line_id             IN     NUMBER
                                 ,p_header_id           IN     NUMBER
                                 ,p_serial_control      IN     VARCHAR2
                                 ,p_lot_control         IN     VARCHAR2
                                 ,p_lot_number          IN     VARCHAR2
                                 ,p_return_status          OUT VARCHAR2
                                 ,p_return_msg             OUT VARCHAR2)
    IS
        -- Common Declarations
        l_api_version                   NUMBER := 1.0;
        l_init_msg_list                 VARCHAR2 (2) := fnd_api.g_true;
        x_return_status                 VARCHAR2 (2);
        x_msg_count                     NUMBER := 0;
        x_msg_data                      VARCHAR2 (255);
        -- WHO columns
        l_user_id                       NUMBER := -1;
        l_resp_id                       NUMBER := -1;
        l_application_id                NUMBER := -1;
        l_row_cnt                       NUMBER := 1;
        l_user_name                     VARCHAR2 (30) := 'SETUPUSER';
        l_resp_name                     VARCHAR2 (30) := 'INVENTORY';
        -- API specific declarations
        l_rsv_rec                       inv_reservation_global.mtl_reservation_rec_type;
        l_serial_number                 inv_reservation_global.serial_number_tbl_type;
        l_partial_reservation_flag      VARCHAR2 (2) := fnd_api.g_false;
        l_force_reservation_flag        VARCHAR2 (2) := fnd_api.g_false;
        l_validation_flag               VARCHAR2 (2) := fnd_api.g_true;
        l_partial_reservation_exists    BOOLEAN := FALSE;
        x_serial_number                 inv_reservation_global.serial_number_tbl_type;
        x_quantity_reserved             NUMBER := 0;
        x_reservation_id                NUMBER := 0;
        l_primary_reservation_qty       NUMBER := 0;
        l_primary_uom_code              VARCHAR2 (30);
        l_subinventory_code             VARCHAR2 (40) := 'WHSE';
        l_count_sns                     NUMBER := 0;
        v_count                         NUMBER := 0;
        l_serial_reservation_quantity   NUMBER := 0;
        l_trx_uom                       VARCHAR2 (15) := NULL;
        l_demand_source_header_id       NUMBER;
        l_lot_number                    VARCHAR2 (80) := NULL;
        l_lot_quantity                  NUMBER;
        l_order_line_qty                NUMBER;
        -- Record set clearing
        g_miss_rsv_rec                  inv_reservation_global.mtl_reservation_rec_type;
        g_miss_serial_number            inv_reservation_global.serial_number_tbl_type;
        g_miss_mtl_reservations_tbl     inv_reservation_global.mtl_reservation_tbl_type;
        l_source_document_type_id       NUMBER;                                                     --added LHS 08142012
        l_demand_source_type_id         NUMBER;                                                     --added LHS 08142012
    BEGIN
        write_log ('3.3. Entering create_reservation for Item ' || p_inventory_item_id);
        l_trx_uom := p_reservation_uom;
        l_demand_source_header_id := NULL;

        SELECT oe_header_util.get_mtl_sales_order_id (p_header_id => p_header_id)
          INTO l_demand_source_header_id
          FROM DUAL;

        write_log ('  4.1. Reservation Qty ' || p_reservation_qty);
        write_log ('  4.2. Lot Number ' || p_lot_number);

        IF l_demand_source_header_id IS NOT NULL
        THEN
            SELECT primary_uom_code
              INTO l_primary_uom_code
              FROM mtl_system_items
             WHERE organization_id = p_organization_id AND inventory_item_id = p_inventory_item_id;

            IF l_primary_uom_code = p_reservation_uom
            THEN
                l_primary_reservation_qty := p_reservation_qty;
            ELSE
                po_uom_s.uom_convert (from_quantity   => p_reservation_qty
                                     ,from_uom        => p_reservation_uom
                                     ,item_id         => p_inventory_item_id
                                     ,to_uom          => l_primary_uom_code
                                     ,to_quantity     => l_primary_reservation_qty);
            END IF;

            --Query to get the source document_type_id to find out if this an internal order
            BEGIN
                SELECT source_document_type_id
                  INTO l_source_document_type_id
                  FROM apps.oe_order_headers
                 WHERE header_id = p_header_id;
            EXCEPTION
                WHEN OTHERS
                THEN
                    l_source_document_type_id := NULL;
            END;

            --if 10 then it was an internal order
            IF l_source_document_type_id = 10
            THEN
                l_demand_source_type_id := 8;
            --initialize the demand_source_type_id to be 8
            ELSE
                l_demand_source_type_id := 2;
            --initialize the demand source type id to be 2
            END IF;

            l_rsv_rec := g_miss_rsv_rec;
            l_rsv_rec.organization_id := p_organization_id;
            l_rsv_rec.inventory_item_id := p_inventory_item_id;
            l_rsv_rec.requirement_date := SYSDATE;
            l_rsv_rec.demand_source_type_id := l_demand_source_type_id;
            --inv_reservation_global.g_source_type_oe;
            l_rsv_rec.supply_source_type_id := inv_reservation_global.g_source_type_inv;
            l_rsv_rec.demand_source_name := NULL;
            l_rsv_rec.primary_reservation_quantity := l_primary_reservation_qty;
            l_rsv_rec.primary_uom_code := l_primary_uom_code;
            l_rsv_rec.subinventory_code := g_pick_subinv;
            l_rsv_rec.demand_source_header_id := l_demand_source_header_id;
            l_rsv_rec.demand_source_line_id := p_line_id;
            l_rsv_rec.reservation_uom_code := p_reservation_uom;
            l_rsv_rec.reservation_quantity := p_reservation_qty;
            l_rsv_rec.supply_source_header_id := NULL;
            l_rsv_rec.supply_source_line_id := NULL;
            l_rsv_rec.supply_source_name := NULL;
            l_rsv_rec.supply_source_line_detail := NULL;
            l_rsv_rec.lot_number := p_lot_number;
            l_rsv_rec.serial_number := NULL;
            l_rsv_rec.ship_ready_flag := NULL;
            l_rsv_rec.attribute15 := NULL;
            l_rsv_rec.attribute14 := NULL;
            l_rsv_rec.attribute13 := NULL;
            l_rsv_rec.attribute12 := NULL;
            l_rsv_rec.attribute11 := NULL;
            l_rsv_rec.attribute10 := NULL;
            l_rsv_rec.attribute9 := NULL;
            l_rsv_rec.attribute8 := NULL;
            l_rsv_rec.attribute7 := NULL;
            l_rsv_rec.attribute6 := NULL;
            l_rsv_rec.attribute5 := NULL;
            l_rsv_rec.attribute4 := NULL;
            l_rsv_rec.attribute3 := NULL;
            l_rsv_rec.attribute2 := NULL;
            l_rsv_rec.attribute1 := NULL;
            l_rsv_rec.attribute_category := NULL;
            l_rsv_rec.lpn_id := NULL;
            l_rsv_rec.pick_slip_number := NULL;
            l_rsv_rec.lot_number_id := NULL;
            l_rsv_rec.locator_id := NULL;
            l_rsv_rec.subinventory_id := NULL;
            l_rsv_rec.revision := NULL;
            l_rsv_rec.external_source_line_id := NULL;
            l_rsv_rec.external_source_code := NULL;
            l_rsv_rec.autodetail_group_id := NULL;
            l_rsv_rec.reservation_uom_id := NULL;
            l_rsv_rec.primary_uom_id := NULL;
            l_rsv_rec.demand_source_delivery := NULL;
            l_rsv_rec.serial_reservation_quantity := l_serial_reservation_quantity;
            l_rsv_rec.project_id := NULL;
            l_rsv_rec.task_id := NULL;
            inv_reservation_pub.create_reservation (p_api_version_number         => l_api_version
                                                   ,p_init_msg_lst               => l_init_msg_list
                                                   ,p_rsv_rec                    => l_rsv_rec
                                                   ,p_serial_number              => l_serial_number
                                                   ,p_partial_reservation_flag   => l_partial_reservation_flag
                                                   ,p_force_reservation_flag     => l_force_reservation_flag
                                                   ,p_partial_rsv_exists         => l_partial_reservation_exists
                                                   ,p_validation_flag            => l_validation_flag
                                                   ,x_serial_number              => x_serial_number
                                                   ,x_return_status              => x_return_status
                                                   ,x_msg_count                  => x_msg_count
                                                   ,x_msg_data                   => x_msg_data
                                                   ,x_quantity_reserved          => x_quantity_reserved
                                                   ,x_reservation_id             => x_reservation_id);

            IF (x_return_status <> fnd_api.g_ret_sts_success)
            THEN
                write_log ('  4.3. Errored Reservation for Line_ID:' || p_line_id);
                write_log ('  4.4. Message Count :' || x_msg_count);
                write_log ('  4.5. Error Message :' || x_msg_data);

                IF (NVL (x_msg_count, 0) = 0)
                THEN
                    write_log ('  4.6. no message return');
                ELSE
                    FOR i IN 1 .. x_msg_count
                    LOOP
                        x_msg_data := x_msg_data || '-' || fnd_msg_pub.get (i, 'F');
                        write_log ('  4.7. Message: ' || x_msg_data);
                    END LOOP;
                END IF;

                p_return_status := x_return_status;
                p_return_msg := x_msg_data;
            ELSE
                write_log (
                    '  4.8. Reservation ID :' || x_reservation_id || ' Quantity Reserved:' || x_quantity_reserved);
                p_return_status := 'S';
            END IF;
        ELSE
            write_log (
                '  4.9. MTL Sales Order not found for line_id ' || p_line_id || ' and order header id ' || p_header_id);
        END IF;

        p_return_status := x_return_status;
        write_log ('  4.10. Exiting create_reservation for Line ' || p_line_id);
    END create_reservation;

    PROCEDURE delete_reservation (p_inventory_item_id   IN     NUMBER
                                 ,p_organization_id     IN     NUMBER
                                 ,p_line_id             IN     NUMBER
                                 ,p_header_id           IN     NUMBER
                                 ,p_reservation_id      IN     NUMBER
                                 ,p_lot_number          IN     VARCHAR2
                                 ,p_return_status          OUT VARCHAR2
                                 ,p_return_msg             OUT VARCHAR2)
    IS
        -- Common Declarations
        l_api_version          NUMBER := 1.0;
        l_init_msg_list        VARCHAR2 (2) := fnd_api.g_true;
        l_return_status        VARCHAR2 (1);
        l_msg_count            NUMBER;
        l_msg_data             VARCHAR2 (240);
        -- API specific declarations
        l_rsv_rec              inv_reservation_global.mtl_reservation_rec_type;
        l_serial_number        inv_reservation_global.serial_number_tbl_type;
        l_validation_flag      VARCHAR2 (2) := fnd_api.g_true;
        l_mtl_sales_order_id   NUMBER;
    BEGIN
        l_mtl_sales_order_id := oe_header_util.get_mtl_sales_order_id (p_header_id => p_header_id);
        l_rsv_rec.demand_source_header_id := l_mtl_sales_order_id;
        l_rsv_rec.demand_source_line_id := p_line_id;
        l_rsv_rec.organization_id := NULL;
        l_rsv_rec.lot_number := p_lot_number;
        l_rsv_rec.reservation_id := p_reservation_id;
        inv_reservation_pub.delete_reservation (p_api_version_number   => 1.0
                                               ,p_init_msg_lst         => fnd_api.g_true
                                               ,x_return_status        => l_return_status
                                               ,x_msg_count            => l_msg_count
                                               ,x_msg_data             => l_msg_data
                                               ,p_rsv_rec              => l_rsv_rec
                                               ,p_serial_number        => l_serial_number);

        IF (l_return_status <> fnd_api.g_ret_sts_success)
        THEN
            write_log ('  2.0. Errored Reservation delete for Line_ID:' || p_line_id);
            write_log ('  2.1. Message Count :' || l_msg_count);
            write_log ('  2.2. Error Message :' || l_msg_data);

            IF (NVL (l_msg_count, 0) = 0)
            THEN
                write_log ('  2.0. no message return');
            ELSE
                FOR i IN 1 .. l_msg_count
                LOOP
                    l_msg_data := l_msg_data || '-' || fnd_msg_pub.get (i, 'F');
                    write_log ('  2.1. Message: ' || l_msg_data);
                END LOOP;
            END IF;

            p_return_status := l_return_status;
            p_return_msg := l_msg_data;
        ELSE
            p_return_status := 'S';
            write_log ('  2.0. Reservation Deleted Successfully');
        END IF;
    END delete_reservation;

    /*************************************************************************
      Procedure : get_onhand_quantity
    ************************************************************************/
    FUNCTION get_on_hand (i_inventory_item_id IN NUMBER, i_organization_id IN NUMBER)
        RETURN NUMBER
    IS
        qoh                NUMBER;
        rqoh               NUMBER;
        qr                 NUMBER;
        qs                 NUMBER;
        att                NUMBER;
        atr                NUMBER;
        ls                 VARCHAR2 (1);
        mc                 NUMBER;
        md                 VARCHAR2 (1000);
        l_lot_control      BOOLEAN;
        l_serial_control   BOOLEAN;
        l_lc_code          NUMBER;
        l_sn_code          NUMBER;
    BEGIN
        inv_quantity_tree_pub.clear_quantity_cache;

        SELECT lot_control_code, serial_number_control_code
          INTO l_lc_code, l_sn_code
          FROM mtl_system_items
         WHERE inventory_item_id = i_inventory_item_id AND organization_id = i_organization_id;

        /*
              IF l_lc_code = 1
              THEN
                 l_lot_control := FALSE;
              ELSE
                 l_lot_control := TRUE;
              END IF;

              IF l_sn_code = 1
              THEN
                 l_serial_control := FALSE;
              ELSE
                 l_serial_control := TRUE;
              END IF;
        */
        inv_quantity_tree_pub.query_quantities (p_api_version_number    => 1.0
                                               ,p_init_msg_lst          => 'T'
                                               ,x_return_status         => ls
                                               ,x_msg_count             => mc
                                               ,x_msg_data              => md
                                               ,p_organization_id       => i_organization_id
                                               ,p_inventory_item_id     => i_inventory_item_id
                                               ,p_tree_mode             => 1
                                               --'Reservation Mode'
                                               ,p_is_revision_control   => FALSE
                                               ,p_is_lot_control        => FALSE
                                               --l_lot_control
                                               ,p_is_serial_control     => FALSE
                                               --l_serial_control
                                               ,p_revision              => NULL
                                               ,p_lot_number            => NULL
                                               ,p_subinventory_code     => 'General'
                                               ,p_locator_id            => NULL
                                               ,x_qoh                   => qoh
                                               ,x_rqoh                  => rqoh
                                               ,x_qr                    => qr
                                               ,x_qs                    => qs
                                               ,x_att                   => att
                                               ,x_atr                   => atr);
        RETURN (atr);
    END get_on_hand;

    PROCEDURE process_force_ship (i_header_id         IN     NUMBER
                                 ,i_organization_id   IN     NUMBER
                                 ,                                                                    --added 10/26/2012
                                  p_return_status        OUT VARCHAR2
                                 ,p_return_msg           OUT VARCHAR2)
    IS
        -- Common Declarations
        l_api_version     NUMBER := 1.0;
        l_init_msg_list   VARCHAR2 (2) := fnd_api.g_true;
        x_return_status   VARCHAR2 (2);
        x_msg_count       NUMBER := 0;
        x_msg_data        VARCHAR2 (255);
        x_error_code      NUMBER := 0;
        l_atr             NUMBER;
        l_ship_qty        NUMBER;
        l_rsv_rec         inv_reservation_global.mtl_reservation_rec_type;
        l_rsv_tbl         inv_reservation_global.mtl_reservation_tbl_type;

        CURSOR line_cur
        IS
            SELECT a.order_number
                  ,b.header_id
                  ,b.line_id
                  ,b.line_number
                  ,b.inventory_item_id
                  ,b.ship_from_org_id
                  ,b.attribute11
                  ,d.lot_control_code
                  ,d.serial_number_control_code
                  ,b.flow_status_code
                  ,d.segment1
                  ,get_on_hand (b.inventory_item_id, b.ship_from_org_id) atr_qty
                  ,d.primary_uom_code
                  ,b.ordered_quantity
                  ,b.order_quantity_uom
                  ,d.mtl_transactions_enabled_flag
              FROM apps.oe_order_headers a
                  ,apps.oe_order_lines b
                  ,oe_transaction_types_vl c
                  ,mtl_system_items d
             WHERE     a.header_id = b.header_id
                   AND a.flow_status_code = 'BOOKED'
                   --AND b.flow_status_code IN ('BOOKED', 'AWAITING_SHIPPING')
                   AND b.flow_status_code = 'AWAITING_SHIPPING'                --added by shankar for double click issue
                   AND b.subinventory <> xxwc_om_force_ship_pkg.g_pick_subinv -- added by shankar for double click issue
                   AND b.line_type_id = c.transaction_type_id
                   AND NVL (c.attribute1, 'N') = 'Y'
                   AND b.inventory_item_id = d.inventory_item_id
                   AND b.ship_from_org_id = d.organization_id
                   -- AND NVL (b.attribute11, '1') <> '0'
                   AND NVL (b.attribute11, b.ordered_quantity) <= b.ordered_quantity
                   AND a.header_id = i_header_id
                   AND b.ship_from_org_id = NVL (i_organization_id, b.ship_from_org_id);      --added by Lee 24-OCT-2012
    BEGIN
        FOR line_rec IN line_cur
        LOOP
            IF line_rec.lot_control_code = 1                                          --AND line_rec.attribute11 IS NULL
            THEN
                -- compare the order uom and item uom
                IF line_rec.order_quantity_uom = line_rec.primary_uom_code
                THEN
                    l_atr := line_rec.atr_qty;
                ELSE
                    po_uom_s.uom_convert (from_quantity   => line_rec.atr_qty
                                         ,from_uom        => line_rec.primary_uom_code
                                         ,item_id         => line_rec.inventory_item_id
                                         ,to_uom          => line_rec.order_quantity_uom
                                         ,to_quantity     => l_atr);
                END IF;

                IF NVL (line_rec.attribute11, line_rec.ordered_quantity) <= l_atr
                THEN
                    l_ship_qty := NVL (line_rec.attribute11, line_rec.ordered_quantity);
                ELSIF l_atr <= 0
                THEN
                    l_ship_qty := 0;
                ELSE
                    l_ship_qty := l_atr;
                END IF;

                /*
                1    Query reservation if any --done
                2    delete existing reservation -- done
                3    do subinventory xfr to generalpck -- done
                4    create reservation -- done
                */
                --IF l_ship_qty > 0 then
                update_order_line (p_header_id       => line_rec.header_id
                                  ,p_line_id         => line_rec.line_id
                                  ,p_return_status   => x_return_status
                                  ,p_return_msg      => x_msg_data);

                -- Added by Shankar 24-OCT-2012
                IF x_return_status = 'S' AND NVL (line_rec.mtl_transactions_enabled_flag, 'N') = 'Y'
                THEN
                    COMMIT;
                    --END IF;
                    process_subinv_transfer (p_organization_id     => line_rec.ship_from_org_id
                                            ,p_inventory_item_id   => line_rec.inventory_item_id
                                            ,p_transfer_qty        => l_ship_qty
                                            ,p_transfer_uom        => line_rec.order_quantity_uom
                                            ,p_header_id           => line_rec.header_id
                                            ,p_line_id             => line_rec.line_id
                                            ,p_lot_control         => line_rec.lot_control_code
                                            ,p_lot_number          => NULL
                                            ,p_return_status       => x_return_status
                                            ,p_return_msg          => x_msg_data);

                    --DBMS_OUTPUT.put_line ('si_xfr=' || x_return_status);
                    IF x_return_status = 'S'
                    THEN
                        create_reservation (p_inventory_item_id   => line_rec.inventory_item_id
                                           ,p_organization_id     => line_rec.ship_from_org_id
                                           ,p_reservation_qty     => l_ship_qty
                                           ,p_reservation_uom     => line_rec.order_quantity_uom
                                           ,p_line_id             => line_rec.line_id
                                           ,p_header_id           => line_rec.header_id
                                           ,p_serial_control      => line_rec.serial_number_control_code
                                           ,p_lot_control         => line_rec.lot_control_code
                                           ,p_lot_number          => NULL
                                           ,p_return_status       => x_return_status
                                           ,p_return_msg          => x_msg_data);

                        --DBMS_OUTPUT.put_line ('reservation=' || x_return_status);
                        IF x_return_status = 'S'
                        THEN
                            COMMIT;
                        ELSE
                            ROLLBACK;
                            p_return_msg :=
                                   p_return_msg
                                || '-'
                                || 'Line '
                                || line_rec.line_number
                                || ' failed due to '
                                || SUBSTR (x_msg_data, 1, 80);
                        END IF;
                    ELSE
                        ROLLBACK;
                        p_return_msg :=
                               p_return_msg
                            || '-'
                            || 'Line '
                            || line_rec.line_number
                            || ' failed due to '
                            || SUBSTR (x_msg_data, 1, 80);
                    END IF;
                -- Added by Shankar 24-OCT-2012
                ELSIF x_return_status = 'S' AND NVL (line_rec.mtl_transactions_enabled_flag, 'N') = 'N'
                THEN
                    COMMIT;
                ELSE
                    ROLLBACK;
                    p_return_msg :=
                           p_return_msg
                        || '-'
                        || 'Line '
                        || line_rec.line_number
                        || ' failed due to '
                        || SUBSTR (x_msg_data, 1, 80);
                END IF;
            --==============================================================================
            ELSE                                                                                  -- LOt controlled item
                --==============================================================================
                -- compare the order uom and item uom
                -- For now assume that the order qty or force ship qty will be equal
                -- to the reservation quantity
                IF line_rec.order_quantity_uom = line_rec.primary_uom_code
                THEN
                    l_atr := line_rec.atr_qty;
                ELSE
                    po_uom_s.uom_convert (from_quantity   => line_rec.atr_qty
                                         ,from_uom        => line_rec.primary_uom_code
                                         ,item_id         => line_rec.inventory_item_id
                                         ,to_uom          => line_rec.order_quantity_uom
                                         ,to_quantity     => l_atr);
                END IF;

                write_log ('  1.0. Force Ship Qty:' || line_rec.attribute11);
                write_log ('  1.0. ATR Qty:' || line_rec.atr_qty);

                IF NVL (line_rec.attribute11, line_rec.ordered_quantity) <= l_atr
                THEN
                    l_ship_qty := NVL (line_rec.attribute11, line_rec.ordered_quantity);
                ELSIF l_atr <= 0
                THEN
                    l_ship_qty := 0;
                ELSE
                    l_ship_qty := l_atr;
                END IF;

                write_log ('  1.0. Ship Qty:' || l_ship_qty);
                write_log ('  1.1. Before Query Reservation:' || line_rec.line_id);
                -- First Query Reservation
                query_reservation (p_inventory_item_id   => line_rec.inventory_item_id
                                  ,p_organization_id     => line_rec.ship_from_org_id
                                  ,p_line_id             => line_rec.line_id
                                  ,p_header_id           => line_rec.header_id
                                  ,p_rsv_tbl             => l_rsv_tbl
                                  ,p_return_status       => x_return_status
                                  ,p_return_msg          => x_msg_data);

                IF x_return_status <> 'S'
                THEN
                    write_log ('  1.1. Query Reservation Failure:' || line_rec.line_id);
                    ROLLBACK;
                    p_return_msg :=
                           p_return_msg
                        || '-'
                        || 'Line '
                        || line_rec.line_number
                        || ' failed due to '
                        || SUBSTR (x_msg_data, 1, 80);
                ELSE
                    -- Loop through the reservation record
                    -- Delete the reservation
                    -- Do Subinventory Xfer
                    -- Recreate Reservation
                    write_log ('  1.1. Query Reservation Success:' || line_rec.line_id || '-' || l_rsv_tbl.COUNT);
                    write_log ('  1.1. Update Order Line:');
                    update_order_line (p_header_id       => line_rec.header_id
                                      ,p_line_id         => line_rec.line_id
                                      ,p_return_status   => x_return_status
                                      ,p_return_msg      => x_msg_data);

                    IF x_return_status = 'S'
                    THEN
                        COMMIT;

                        FOR i IN 1 .. l_rsv_tbl.COUNT
                        LOOP
                            l_rsv_rec := l_rsv_tbl (i);
                            write_log ('  2.1. Delete Reservation before:' || l_rsv_rec.reservation_id);
                            /*
                            delete_reservation(p_inventory_item_id   => l_rsv_rec.inventory_item_id
                                         , p_organization_id     => l_rsv_rec.organization_id
                                         , p_line_id             => line_rec.line_id
                                         , p_header_id           => line_rec.header_id
                                         , p_reservation_id      => l_rsv_rec.reservation_id
                                         , p_lot_number          => l_rsv_rec.lot_number
                                         , p_return_status       => x_return_status
                                         , p_return_msg          => x_msg_data);
                            */
                            x_return_status := 'S';

                            IF x_return_status <> 'S'
                            THEN
                                write_log ('  2.1. Delete Reservation failure:' || l_rsv_rec.reservation_id);
                                ROLLBACK;
                                p_return_msg :=
                                       p_return_msg
                                    || '-'
                                    || 'Line '
                                    || line_rec.line_number
                                    || ' failed due to '
                                    || SUBSTR (x_msg_data, 1, 80);
                            ELSE
                                write_log ('  2.1. Delete Reservation success:' || l_rsv_rec.reservation_id);
                                process_subinv_transfer (p_organization_id     => l_rsv_rec.organization_id
                                                        ,p_inventory_item_id   => l_rsv_rec.inventory_item_id
                                                        ,p_transfer_qty        => l_rsv_rec.reservation_quantity
                                                        ,p_transfer_uom        => l_rsv_rec.reservation_uom_code
                                                        ,p_header_id           => line_rec.header_id
                                                        ,p_line_id             => line_rec.line_id
                                                        ,p_lot_control         => line_rec.lot_control_code
                                                        ,p_lot_number          => l_rsv_rec.lot_number
                                                        ,p_return_status       => x_return_status
                                                        ,p_return_msg          => x_msg_data);

                                IF x_return_status <> 'S'
                                THEN
                                    write_log ('  3.1. Subinv xfer failure:' || l_rsv_rec.reservation_id);
                                    ROLLBACK;
                                    p_return_msg :=
                                           p_return_msg
                                        || '-'
                                        || 'Line '
                                        || line_rec.line_number
                                        || ' failed due to '
                                        || SUBSTR (x_msg_data, 1, 80);
                                ELSE
                                    write_log ('  3.1. Subinv xfer success:' || l_rsv_rec.reservation_id);
                                    create_reservation (p_inventory_item_id   => l_rsv_rec.inventory_item_id
                                                       ,p_organization_id     => l_rsv_rec.organization_id
                                                       ,p_reservation_qty     => l_rsv_rec.reservation_quantity
                                                       ,p_reservation_uom     => l_rsv_rec.reservation_uom_code
                                                       ,p_line_id             => line_rec.line_id
                                                       ,p_header_id           => line_rec.header_id
                                                       ,p_serial_control      => line_rec.serial_number_control_code
                                                       ,p_lot_control         => line_rec.lot_control_code
                                                       ,p_lot_number          => l_rsv_rec.lot_number
                                                       ,p_return_status       => x_return_status
                                                       ,p_return_msg          => x_msg_data);

                                    --DBMS_OUTPUT.put_line ('reservation=' || x_return_status);
                                    IF x_return_status = 'S'
                                    THEN
                                        write_log ('  4.1. create reservation success:' || l_rsv_rec.reservation_id);
                                    ELSE
                                        write_log ('  4.1. create reservation failure:' || l_rsv_rec.reservation_id);
                                        ROLLBACK;
                                        p_return_msg :=
                                               p_return_msg
                                            || '-'
                                            || 'Line '
                                            || line_rec.line_number
                                            || ' failed due to '
                                            || SUBSTR (x_msg_data, 1, 80);
                                        EXIT;
                                    END IF;
                                END IF;                                                -- subinventory xfer status check
                            END IF;                                                   -- delete reservation status check
                        END LOOP;                                                              -- Reservation table loop

                        -- If all reservation records processed successfully then the status is S
                        -- and do commit, if not rollback and display the message.
                        IF x_return_status = 'S'
                        THEN
                            COMMIT;
                        ELSE
                            ROLLBACK;
                        END IF;
                    ELSE
                        write_log ('  1.1. Update Order Line failure:' || line_rec.line_id);
                        ROLLBACK;
                        p_return_msg :=
                               p_return_msg
                            || '-'
                            || 'Line '
                            || line_rec.line_number
                            || ' failed due to '
                            || SUBSTR (x_msg_data, 1, 80);
                    END IF;
                END IF;                                                                -- Query Reservation Status check
            END IF;
        END LOOP;

        IF p_return_msg IS NOT NULL
        THEN
            p_return_status := 'E';
        ELSE
            p_return_status := 'S';
            COMMIT;
        END IF;
    END process_force_ship;

    PROCEDURE update_line_subinv (errbuf OUT VARCHAR2, retcode OUT NUMBER)
    IS
        l_header_rec               oe_order_pub.header_rec_type;
        o_header_rec               oe_order_pub.header_rec_type;
        o_header_val_rec           oe_order_pub.header_val_rec_type;
        o_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
        o_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type;
        o_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type;
        o_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type;
        o_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type;
        o_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type;
        o_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type;
        l_line_tbl                 oe_order_pub.line_tbl_type;
        o_line_tbl                 oe_order_pub.line_tbl_type;
        o_line_val_tbl             oe_order_pub.line_val_tbl_type;
        o_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
        o_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type;
        o_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type;
        o_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type;
        o_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type;
        o_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
        o_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type;
        o_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type;
        o_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type;
        l_action_request_tbl       oe_order_pub.request_tbl_type;
        o_action_request_tbl       oe_order_pub.request_tbl_type;
        l_return_status            VARCHAR2 (240);
        l_msg_count                NUMBER;
        l_msg_data                 VARCHAR2 (4000);

        CURSOR c1
        IS
            SELECT * FROM xxwc_om_upd_del_subinv;
    BEGIN
        FOR c1_rec IN c1
        LOOP
            oe_msg_pub.initialize;
            /*
                FND_GLOBAL.apps_initialize ( user_id => 16010,
                                             resp_id => 50886,
                                             resp_appl_id => 660 );
               mo_global.init('ONT');  */
            l_line_tbl (1) := oe_order_pub.g_miss_line_rec;
            l_line_tbl (1).line_id := c1_rec.line_id;
            l_line_tbl (1).header_id := c1_rec.header_id;
            l_line_tbl (1).subinventory := xxwc_om_force_ship_pkg.g_subinv;
            l_line_tbl (1).operation := oe_globals.g_opr_update;
            oe_order_pub.process_order (p_api_version_number       => 1.0
                                       ,p_header_rec               => l_header_rec
                                       ,p_line_tbl                 => l_line_tbl
                                       ,p_action_request_tbl       => l_action_request_tbl
                                       ,x_return_status            => l_return_status
                                       ,x_msg_count                => l_msg_count
                                       ,x_msg_data                 => l_msg_data
                                       ,x_header_rec               => o_header_rec
                                       ,x_header_val_rec           => o_header_val_rec
                                       ,x_header_adj_tbl           => o_header_adj_tbl
                                       ,x_header_adj_val_tbl       => o_header_adj_val_tbl
                                       ,x_header_price_att_tbl     => o_header_price_att_tbl
                                       ,x_header_adj_att_tbl       => o_header_adj_att_tbl
                                       ,x_header_adj_assoc_tbl     => o_header_adj_assoc_tbl
                                       ,x_header_scredit_tbl       => o_header_scredit_tbl
                                       ,x_header_scredit_val_tbl   => o_header_scredit_val_tbl
                                       ,x_line_tbl                 => o_line_tbl
                                       ,x_line_val_tbl             => o_line_val_tbl
                                       ,x_line_adj_tbl             => o_line_adj_tbl
                                       ,x_line_adj_val_tbl         => o_line_adj_val_tbl
                                       ,x_line_price_att_tbl       => o_line_price_att_tbl
                                       ,x_line_adj_att_tbl         => o_line_adj_att_tbl
                                       ,x_line_adj_assoc_tbl       => o_line_adj_assoc_tbl
                                       ,x_line_scredit_tbl         => o_line_scredit_tbl
                                       ,x_line_scredit_val_tbl     => o_line_scredit_val_tbl
                                       ,x_lot_serial_tbl           => o_lot_serial_tbl
                                       ,x_lot_serial_val_tbl       => o_lot_serial_val_tbl
                                       ,x_action_request_tbl       => o_action_request_tbl);

            IF l_return_status = 'S'
            THEN
                DELETE FROM xxwc_om_upd_del_subinv
                      WHERE header_id = c1_rec.header_id AND line_id = c1_rec.line_id;

                write_log (' Subinv Update Success' || c1_rec.line_id || '=' || l_msg_data);
            ELSE
                write_log (' Subinv Update Error' || c1_rec.line_id || '=' || l_msg_data);
            END IF;

            DBMS_OUTPUT.put_line (l_return_status || '=' || l_msg_count || '=' || l_msg_data);
        END LOOP;
    END update_line_subinv;

    --Added by Rasikha- to check that row may locked by another process
    --*************************************************************************
    FUNCTION is_row_locked (v_rowid ROWID, table_name VARCHAR2)
        RETURN VARCHAR2
    IS
        x   NUMBER;
        PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        EXECUTE IMMEDIATE
               'Begin
                           Select 1 into :x from '
            || table_name
            || ' where rowid =:v_rowid for update nowait;
                         Exception
                            When Others Then
                              :x:=null;
                         End;'
            USING OUT x, v_rowid;

        -- now release the lock if we got it.
        ROLLBACK;

        IF x = 1
        THEN
            RETURN 'N';
        ELSIF x IS NULL
        THEN
            RETURN 'Y';
        END IF;
    END;

    --*************************************************************************
    --*************************************************************************
    --*************************************************************************

    PROCEDURE update_line_subinv_catchup (errbuf OUT VARCHAR2, retcode OUT NUMBER, p_last_delta_date VARCHAR2)
    IS
        l_header_rec               oe_order_pub.header_rec_type;
        o_header_rec               oe_order_pub.header_rec_type;
        o_header_val_rec           oe_order_pub.header_val_rec_type;
        o_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
        o_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type;
        o_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type;
        o_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type;
        o_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type;
        o_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type;
        o_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type;
        l_line_tbl                 oe_order_pub.line_tbl_type;
        o_line_tbl                 oe_order_pub.line_tbl_type;
        o_line_val_tbl             oe_order_pub.line_val_tbl_type;
        o_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
        o_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type;
        o_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type;
        o_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type;
        o_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type;
        o_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
        o_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type;
        o_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type;
        o_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type;
        l_action_request_tbl       oe_order_pub.request_tbl_type;
        o_action_request_tbl       oe_order_pub.request_tbl_type;
        l_return_status            VARCHAR2 (240);
        l_msg_count                NUMBER;
        l_msg_data                 VARCHAR2 (4000);
        v_exception                EXCEPTION;

        v_locked_line              VARCHAR2 (1) := 'N';
        v_locked_header            VARCHAR2 (1) := 'N';
        v_running_message1         VARCHAR2 (2048);
        l_msg_dummy                VARCHAR2 (1024);
        v_last_delta_date          DATE;
        v_errbuf                   VARCHAR2 (1024) := NULL;
        v_retcode                  NUMBER := 0;
        v_procedure_name           VARCHAR2 (256) := 'update_line_subinv_catchup';
        v_package_name             VARCHAR2 (256) := 'xxwc_om_force_ship_pkg';
        v_distro_list              VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
    BEGIN
        v_errbuf := errbuf;
        v_retcode := retcode;
        v_last_delta_date := TRUNC (TO_DATE (p_last_delta_date, 'YYYY/MM/DD HH24:MI:SS'));

        FOR c1_rec
            IN (SELECT /*+ index(a XXWC_OE_ORDER_LN_LUD1) */--Ram Talluri 7/30/2014 TMS #20140609-00203 index hint updated to reflect the latest index name changes.
                      a.line_id
                      ,h.order_number
                      ,a.line_number
                      ,a.header_id
                      ,a.ROWID line_rowid
                      ,h.ROWID header_rowid
                  FROM apps.oe_order_lines a, apps.oe_order_headers h
                 WHERE     a.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
                       AND a.subinventory = 'GeneralPCK'
                       AND a.line_type_id IN (FND_PROFILE.VALUE('XXWC_STANDARD_LINE_TYPE'),FND_PROFILE.VALUE('XXWC_INTERNAL_ORDER_LINE_TYPE'))   --(1002, 1012)  Repaced the Hard coded value with Profile options by Pattabhi on 10/28/2014 for TMS# 20141002-00060
                       AND h.header_id = a.header_id
                       AND TRUNC (a.last_update_date) BETWEEN TRUNC (NVL (v_last_delta_date, SYSDATE)) - 1.01
                                                          AND TRUNC (NVL (v_last_delta_date, SYSDATE) + 0.9)
                       AND NOT EXISTS
                               (SELECT 1
                                  FROM xxwc_wsh_shipping_stg b
                                 WHERE a.line_id = b.line_id AND b.delivery_id IS NOT NULL))
        LOOP
            BEGIN
                v_locked_line := is_row_locked (c1_rec.line_rowid, 'OE_ORDER_LINES_ALL');
                v_locked_header := is_row_locked (c1_rec.header_rowid, 'OE_ORDER_HEADERS_ALL');
            EXCEPTION
                WHEN OTHERS
                THEN
                    v_locked_line := 'Y';
                    v_locked_header := 'Y';
            END;

            IF v_locked_line = 'N' AND v_locked_header = 'N'
            THEN
                oe_msg_pub.initialize;
                /*
                    FND_GLOBAL.apps_initialize ( user_id => 16010,
                                                 resp_id => 50886,
                                                 resp_appl_id => 660 );
                   mo_global.init('ONT');  */
                l_line_tbl (1) := oe_order_pub.g_miss_line_rec;
                l_line_tbl (1).line_id := c1_rec.line_id;
                l_line_tbl (1).header_id := c1_rec.header_id;
                l_line_tbl (1).subinventory := xxwc_om_force_ship_pkg.g_subinv;
                l_line_tbl (1).attribute11 := NULL;
                l_line_tbl (1).operation := oe_globals.g_opr_update;
                oe_order_pub.process_order (p_api_version_number       => 1.0
                                           ,p_header_rec               => l_header_rec
                                           ,p_line_tbl                 => l_line_tbl
                                           ,p_action_request_tbl       => l_action_request_tbl
                                           ,x_return_status            => l_return_status
                                           ,x_msg_count                => l_msg_count
                                           ,x_msg_data                 => l_msg_data
                                           ,x_header_rec               => o_header_rec
                                           ,x_header_val_rec           => o_header_val_rec
                                           ,x_header_adj_tbl           => o_header_adj_tbl
                                           ,x_header_adj_val_tbl       => o_header_adj_val_tbl
                                           ,x_header_price_att_tbl     => o_header_price_att_tbl
                                           ,x_header_adj_att_tbl       => o_header_adj_att_tbl
                                           ,x_header_adj_assoc_tbl     => o_header_adj_assoc_tbl
                                           ,x_header_scredit_tbl       => o_header_scredit_tbl
                                           ,x_header_scredit_val_tbl   => o_header_scredit_val_tbl
                                           ,x_line_tbl                 => o_line_tbl
                                           ,x_line_val_tbl             => o_line_val_tbl
                                           ,x_line_adj_tbl             => o_line_adj_tbl
                                           ,x_line_adj_val_tbl         => o_line_adj_val_tbl
                                           ,x_line_price_att_tbl       => o_line_price_att_tbl
                                           ,x_line_adj_att_tbl         => o_line_adj_att_tbl
                                           ,x_line_adj_assoc_tbl       => o_line_adj_assoc_tbl
                                           ,x_line_scredit_tbl         => o_line_scredit_tbl
                                           ,x_line_scredit_val_tbl     => o_line_scredit_val_tbl
                                           ,x_lot_serial_tbl           => o_lot_serial_tbl
                                           ,x_lot_serial_val_tbl       => o_lot_serial_val_tbl
                                           ,x_action_request_tbl       => o_action_request_tbl);

                IF l_return_status = 'S'
                THEN
                    write_log (
                           ' Subinv Update Success for order number ='
                        || c1_rec.order_number
                        || ', '
                        || ' LINE_NUMBER='
                        || c1_rec.line_number
                        || ', '
                        || 'LINE_ID='
                        || c1_rec.line_id
                        || l_msg_data);
                ELSE
                    IF l_msg_count > 0
                    THEN
                        v_running_message1 := '';

                        FOR i IN 1 .. l_msg_count
                        LOOP
                            fnd_msg_pub.get (i
                                            ,fnd_api.g_false
                                            ,l_msg_data
                                            ,l_msg_dummy);

                            IF NVL (v_running_message1, 'null') <> l_msg_data
                            THEN
                                write_log (
                                       'Subinv Update Error for order number ='
                                    || c1_rec.order_number
                                    || ', '
                                    || ' LINE_NUMBER='
                                    || c1_rec.line_number
                                    || ', '
                                    || 'LINE_ID='
                                    || c1_rec.line_id
                                    || (SUBSTR ('Msg' || TO_CHAR (i) || ': ' || l_msg_data, 1, 255)));
                            END IF;

                            v_running_message1 := l_msg_data;
                        END LOOP;
                    END IF;
                END IF;

                DBMS_OUTPUT.put_line (l_return_status || '=' || l_msg_count || '=' || l_msg_data);
                COMMIT;
            END IF;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            write_log ('Error running ' || v_package_name || '.' || v_procedure_name || ' ' || v_errbuf);
            v_retcode := 2;
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name
               ,p_calling             => v_procedure_name
               ,p_request_id          => NULL
               ,p_ora_error_msg       => SUBSTR (v_errbuf, 1, 2000)
               ,p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name
               ,p_distribution_list   => v_distro_list
               ,p_module              => 'XXWC');
            retcode := v_retcode;
            errbuf := SUBSTR (v_errbuf, 1, 1024);
    END update_line_subinv_catchup;
    --Added by Ram Talluri on 3/11/2014 for TMS #20140310-00017
    PROCEDURE submit_its_stuck_deliveries (errbuf OUT VARCHAR2, retcode OUT NUMBER, p_number_of_days IN NUMBER, p_header_id IN NUMBER)
    IS
    
        l_return_status            VARCHAR2 (240);
        l_msg_count                NUMBER;
        l_msg_data                 VARCHAR2 (4000);
        ln_request_id              NUMBER;
        v_exception                EXCEPTION;

        v_running_message1         VARCHAR2 (2048);
        l_msg_dummy                VARCHAR2 (1024);
        v_errbuf                   VARCHAR2 (1024) := NULL;
        v_retcode                  NUMBER := 0;
        v_procedure_name           VARCHAR2 (256) := 'submit_its_stuck_deliveries';
        v_package_name             VARCHAR2 (256) := 'xxwc_om_force_ship_pkg';
        v_distro_list              VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
        
        v_phase                     VARCHAR2 (50);
        v_status                    VARCHAR2 (50);
        v_dev_status                VARCHAR2 (50);
        v_dev_phase                 VARCHAR2 (50);
        v_message                   VARCHAR2 (250);
        v_error_message             VARCHAR2 (3000);
        v_wait                      BOOLEAN;
        
    CURSOR c_stuck_deliveries IS 
    SELECT /*+ index(a XXWC_OE_ORDER_LN_LUD1) */--Ram Talluri 7/30/2014 TMS #20140609-00203 index hint updated to reflect the latest index name changes.
       DISTINCT wnd.delivery_id,
       ool.ship_from_org_id
  FROM apps.wsh_delivery_details wdd,
       apps.wsh_delivery_assignments wda,
       apps.wsh_new_deliveries wnd,
       apps.wsh_delivery_legs wdl,
       apps.wsh_trip_stops wts,
       apps.oe_order_headers ooh,
       apps.oe_order_lines ool,
       apps.gl_periods prd
 WHERE     wdd.source_code = 'OE'
       AND wdd.released_status = 'C'
       AND wdd.inv_interfaced_flag IN ('N', 'P')
       AND wda.delivery_detail_id = wdd.delivery_detail_id
       AND wnd.delivery_id = wda.delivery_id
       AND wnd.status_code IN ('CL', 'IT')
       AND wdl.delivery_id = wnd.delivery_id
       AND TRUNC (SYSDATE) BETWEEN start_date AND end_date
       AND TRUNC (wts.actual_departure_date) BETWEEN start_date AND end_date
       AND wdl.pick_up_stop_id = wts.stop_id
       AND wdd.source_header_id = ooh.header_id
       AND wdd.source_line_id = ool.line_id
       AND apps.xxwc_om_force_ship_pkg.is_row_locked (ooh.ROWID, 'OE_ORDER_HEADERS_ALL') = 'N'
       AND apps.xxwc_om_force_ship_pkg.is_row_locked (ool.ROWID, 'OE_ORDER_LINES_ALL')  = 'N'
       AND apps.xxwc_om_force_ship_pkg.is_row_locked (wdd.ROWID, 'WSH_DELIVERY_DETAILS')  = 'N'
       AND ooh.header_id =NVL(p_header_id,ooh.header_id)
       AND TRUNC(ool.last_update_date) >= TRUNC(SYSDATE) - NVL(p_number_of_days,31);
       
   BEGIN
       
       mo_global.init ('ONT');

      /* fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                   resp_id        => fnd_global.resp_id,
                                   resp_appl_id   => fnd_global.resp_appl_id);

       mo_global.set_policy_context ('S', 162);  */

    FOR i in c_stuck_deliveries loop
         ln_request_id :=
         fnd_request.submit_request ('WSH',                     -- application
                                     'WSHINTERFACE', -- program short name--Interface trip stop
                                     '',                        -- description
                                     '',                         -- start time
                                     FALSE,                     -- sub request
                                     'ALL',              -- argument1
                                     NULL,                         -- argument2
                                     i.delivery_id,
                                     '0',
                                     NULL,
                                     NULL,
                                     i.ship_from_org_id,
                                     '1',
                                     '1',
                                      CHR (0)    -- represents end of arguments
                                            );
                                                                                       
                                                                                      
      COMMIT;

      IF ln_request_id = 0
      THEN
      
      write_log (      ' Failed to submit interface trip stop program for delivery-'
                        || i.delivery_id
                        || ', '
                        || ' Ship_from_org_id='
                        || i.Ship_from_org_id
                        || l_msg_data);
      ELSIF ln_request_id <> 0
      THEN
      
      write_log (      ' Successfully submitted interface trip stop program for delivery-'
                        || i.delivery_id
                        || ', '
                        || ' Ship_from_org_id='
                        || i.Ship_from_org_id
                        || ', '
                        || ' request_id='
                        || ln_request_id
                        || l_msg_data);
                        
       v_wait:=  fnd_concurrent.wait_for_request (ln_request_id
                                                 , 6
                                                 , 3600
                                                 , v_phase
                                                 , v_status
                                                 , v_dev_phase
                                                 , v_dev_status
                                                 , v_message);
                                                 
       write_log (      'Interface trip stop program request Status-'
                        || v_status
                        || l_msg_data);
                
      END IF;
      END LOOP;
      
  EXCEPTION
        WHEN OTHERS
        THEN
            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            write_log ('Error running ' || v_package_name || '.' || v_procedure_name || ' ' || v_errbuf);
            v_retcode := 2;
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name
               ,p_calling             => v_procedure_name
               ,p_request_id          => NULL
               ,p_ora_error_msg       => SUBSTR (v_errbuf, 1, 2000)
               ,p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name
               ,p_distribution_list   => v_distro_list
               ,p_module              => 'XXWC');
            retcode := v_retcode;
            errbuf := SUBSTR (v_errbuf, 1, 1024);    
  END submit_its_stuck_deliveries;
    
END xxwc_om_force_ship_pkg;
/

-- End of DDL Script for Package Body APPS.XXWC_OM_FORCE_SHIP_PKG
