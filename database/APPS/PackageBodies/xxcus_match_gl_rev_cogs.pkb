create or replace 
PACKAGE BODY      xxcus_match_gl_rev_cogs
/**************************************************************************
 * PROGRAM NAME
 *  XXCUS_MATCH_GL_REV_COGS.pks
 *
 * DESCRIPTION
 *  Used BY HDS AR Sales and COGS Analysis -Master
 *
 * DEPENDENCIES
 *   None.
 *
 * CALLED BY
 *   Program: HDS AR Sales and COGS Analysis -Master
 *
 * LAST UPDATE DATE: 09/08/2014
 *
 * HISTORY
 * =======
 *
 * Version  ESMS                    DATE         AUTHOR(S)           DESCRIPTION
 * -------- --------                -----------  ---------------     ------------------------------------
 * 1.0      200496                 04/22/2013    Balaguru Seshadri   Created
 * 1.1      202811                 05/09/2013    Balaguru Seshadri   Modify sales-cogs percentage formula
 * 1.2      20140131-00244 [TMS]    09/08/2014    Balaguru Seshadri   New routine detail_wrapper added.
 * 1.3      20160606-00273          06/07/2016    Neha Saini          adding log messgaes to validate and analysis
 * 1.4      TMS#20180815-00112     08/20/2018    Pattabhi Avula      SALES and COGS files FTP to P Drive Fix -- Provided file permissions
 *************************************************************************/
/**************************************************************************
 *
 * PROCEDURE: print_log
 * DESCRIPTION: To print log messages either to the SQL PLUS session window or concurrent program log file
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_message         IN       debug messages
 * RETURN VALUE: Mentioned above
 * CALLED BY: Multiple
 *************************************************************************/
AS
  PROCEDURE print_log(p_message in varchar2) IS
  BEGIN
   fnd_file.put_line(fnd_file.log, p_message);--ver1.3 starts
   --IF fnd_global.conc_request_id >0 THEN
   -- fnd_file.put_line(fnd_file.log, p_message);
  -- ELSE
    dbms_output.put_line(p_message);
  -- END IF;--ver1.3 ends
  EXCEPTION
   WHEN OTHERS THEN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Issue in print_log routine ='||sqlerrm);
  END print_log;
  --
/**************************************************************************
 *
 * FUNCTION: get_field
 * DESCRIPTION: To extract fields delimited by like comma, semi colon and pipe symbols.
 * PARAMETERS
 * ==========
 * NAME              TYPE      DESCRIPTION
.* ----------------- ------    ---------------------------------------------
 * p_delimiter       IN        Delimiter symbol like comma or pipe
 * p_field_no        IN        to be extracted fields order or position in the file
 * p_line_read       IN        The entire line to search for
 * RETURN VALUE: Field extracted
 * CALLED BY: Multiple
 *************************************************************************/
  --
  function get_field (p_delimiter in varchar2, p_field_no in number ,p_line_read in varchar2) return varchar2 is
       n_start_field_pos number;
       n_end_field_pos   number;
       v_get_field       varchar2(2000);
  begin
       if p_field_no = 1 then
          n_start_field_pos := 1;
       else
          n_start_field_pos := instr(p_line_read,p_delimiter,1,p_field_no-1)+1;
       end if;

       n_end_field_pos   := instr(p_line_read,p_delimiter,1,p_field_no) -1;
       if n_end_field_pos > 0 then
          v_get_field := substr(p_line_read,n_start_field_pos,(n_end_field_pos - n_start_field_pos)+1);
       else
          v_get_field := substr(p_line_read,n_start_field_pos);
       end if;

       return v_get_field;

  exception
    when others then
     print_log ('Line ='||p_line_read);
     print_log ('get field: '||sqlerrm);
  end get_field;
  --
/**************************************************************************
 *
 * FUNCTION: get_acct_where_clause
 * DESCRIPTION: To extract fields delimited by like comma, semi colon and pipe symbols.
 * PARAMETERS
 * ==========
 * NAME              TYPE      DESCRIPTION
.* ----------------- ------    ---------------------------------------------
 * p_account_list    IN        comma separated gl account list like 202111, 201220
 * RETURN VALUE: Print SQL like string IN (202111, 201220)
 * CALLED BY: Multiple
 *************************************************************************/
  --
  function get_acct_where_clause (p_account_list in varchar2) return varchar2 is
     v_accounts varchar2(240);
     i number :=1;
    cursor accts is
    select to_number(xmltbl.column_value) hds_acct
    from xmltable(p_account_list) xmltbl;
  begin
      for rec in accts loop
       if i =1 then
        v_accounts :=''''||rec.hds_acct||'''';
        i :=i+1;
       else
        v_accounts :=v_accounts||','||''''||rec.hds_acct||'''';
       end if;
      end loop;
   print_log ('v_accounts ='||v_accounts);
   return v_accounts;
  exception
   when others then
    print_log ('v_accounts ='||'OERR');
    return 'OERR';
  end get_acct_where_clause;
  --
/**************************************************************************
 *
 * PROCEDURE: pivot
 * DESCRIPTION: To pivot the gl sales and cogs summary data.
 * PARAMETERS
 * ==========
 * NAME              TYPE      DESCRIPTION
.* ----------------- ------    ---------------------------------------------
 * p_account_list    IN        comma separated gl account list like 202111, 201220
 * RETURN VALUE: Print SQL like string IN (202111, 201220)
 * CALLED BY: main
 *************************************************************************/
  --
  procedure pivot (p_gl_location IN VARCHAR2) IS
  --
    CURSOR Pivot_Records IS
    SELECT 1 seq
          ,p_gl_location gl_location
          ,order_number
          ,nvl(total_sales, 0) total_sales
          ,nvl(total_cogs, 0) total_cogs
          ,( nvl(total_sales, 0) + nvl(total_cogs, 0) ) cogs_rev_diff
          ,case
            when (nvl(total_sales, 0) <>0) then round((( nvl(total_sales, 0) + nvl(total_cogs, 0) ) / nvl(total_sales, 0)*100), 2)
            else 0
           end cogs_percent
    FROM   (
             SELECT order_number, gl_account, nvl(amount, 0) amount
             FROM   apps.xxcus_ar_cogs_final_v
           )
    PIVOT  (SUM(amount) FOR (gl_account) IN ('401' AS Total_Sales, '501' as Total_Cogs))
    WHERE 1 =1
      AND ((TOTAL_SALES <>0 OR TOTAL_COGS <>0) AND ORDER_NUMBER IS NOT NULL)
    ORDER BY seq asc, order_number;
  --
    TYPE trx_tbl is table of Pivot_Records%rowtype index by binary_integer;
    trx_rec trx_tbl;
  --
  CURSOR GRAND_SUMMARIES IS
    SELECT p_gl_location||'|'||
          nvl(sum(total_sales), 0)||'|'|| -- total_sales
          nvl(sum(total_cogs), 0)||'|'|| -- total_cogs
          ( nvl(sum(total_sales), 0) + nvl(sum(total_cogs), 0) )||'|'|| -- cogs_rev_diff
          case
            when (nvl(sum(total_sales), 0) <>0) then round((( nvl(sum(total_sales), 0) + nvl(sum(total_cogs), 0) ) / nvl(sum(total_sales), 0)*100), 2)
            else 0
           end grand_results
    FROM   (
             SELECT gl_account, nvl(amount, 0) amount
             FROM   apps.xxcus_ar_cogs_final_v
           )
    PIVOT  (SUM(amount) FOR (gl_account) IN ('401' AS Total_Sales, '501' as Total_Cogs));
  --
   p_limit number :=5000;
  --
  begin
   --
    open Pivot_Records;
    --
     loop
      --
       fetch Pivot_Records bulk collect into trx_rec limit p_limit;
       exit when trx_rec.count =0;
       --
        if trx_rec.count >0 then
        --
         begin
          savepoint start1;
          forall idx in trx_rec.first .. trx_rec.last
          insert into xxcus.xxcus_gl_ar_rev_cogs_final_b values trx_rec(idx);
          print_log('Bulk insert, cursor Pivot_Records, rows inserted ='||sql%rowcount);
         exception
          when others then
           print_log('Bulk insert failed in cursor Pivot_Records, error ='||sqlerrm);
           rollback to start1;
         end;
        --
        end if;
       --
      --
     end loop;
    --
    close Pivot_Records;
    --
    print_log('Begin insert of grand summary data');
     FOR rec in GRAND_SUMMARIES
      LOOP
        INSERT INTO xxcus.xxcus_ar_cogs_grand_totals
        VALUES (rec.grand_results);
      END LOOP;
    print_log('End insert of grand summary data');
  exception
   when others then
    --
      print_log('Location: pivot, Other errors, '||sqlerrm);
    --
  end pivot;
  --
/**************************************************************************
 *
 * PROCEDURE: unload_branch_summary_data
 * DESCRIPTION: To dump branch level sales/cogs summary data
 * PARAMETERS
 * ==========
 * NAME              TYPE      DESCRIPTION
.* ----------------- ------    ---------------------------------------------

 * RETURN VALUE:
 * CALLED BY: main
 *************************************************************************/
  --
  Procedure unload_branch_summary_data IS
   --
   --
  begin
  --
   savepoint start1;
     INSERT /*+ APPEND */
     INTO xxcus.xxcus_sales_cogs_summary_all
     (
       SELECT gl_location, ar_rev_cogs_summary
       FROM   xxcus.xxcus_ar_cogs_summary_results
     );
  --
  exception
  --
   when others then
   --
    print_log('@unload_branch_summary_data, outer block, when others -message ='||sqlerrm);
    rollback to start1;
   --
  end unload_branch_summary_data;
  --
/**************************************************************************
 *
 * PROCEDURE: unload_branch_sales_data
 * DESCRIPTION: To dump branch level sales detail data
 * PARAMETERS
 * ==========
 * NAME              TYPE      DESCRIPTION
.* ----------------- ------    ---------------------------------------------

 * RETURN VALUE:
 * CALLED BY: main
 *************************************************************************/
  --
  Procedure unload_branch_sales_data IS
   --
  begin
  --
   savepoint start1;
     INSERT /*+ APPEND */
     INTO xxcus.xxcus_ar_sales_summary_all
     (
       SELECT gl_location, sales_detail
       FROM   xxcus.xxcus_gl_sales_detail_v
     );
  --
  exception
  --
   when others then
   --
    print_log('@unload_branch_sales_data, outer block, when others -message ='||sqlerrm);
    rollback to start1;
   --
  end unload_branch_sales_data;
  --
/**************************************************************************
 *
 * PROCEDURE: unload_branch_COGS_data
 * DESCRIPTION: To dump branch level cost of goods sold detail data
 * PARAMETERS
 * ==========
 * NAME              TYPE      DESCRIPTION
.* ----------------- ------    ---------------------------------------------

 * RETURN VALUE:
 * CALLED BY: main
 *************************************************************************/
  --
  Procedure unload_branch_COGS_data IS
   --
   --
  begin
  --
   savepoint start1;
     INSERT /*+ APPEND */
     INTO xxcus.xxcus_ar_cogs_summary_all
     (
       SELECT gl_location, cogs_detail
       FROM   xxcus.xxcus_gl_cogs_detail_v
     );
  --
  exception
  --
   when others then
   --
    print_log('@unload_branch_COGS_data, outer block, when others -message ='||sqlerrm);
    rollback to start1;
   --
  end unload_branch_COGS_data;
  --
/**************************************************************************
 *
 * PROCEDURE: main
 * DESCRIPTION: HDS AR Sales and COGS Analysis -Detail
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_ledger_id       IN       Primary ledger id for Whitecap
 * p_period_name     IN       Fiscal period id
 * p_product         IN      GL Product segment
 * p_location        IN      GL Location segment
 * p_rev_account     IN      GL Sales Account segment
 * p_cogs_account    IN      GL COGS Account segment
 * retcode           OUT     standard concurrent program return variable
 * errbuf            OUT     standard concurrent program return variable
 * RETURN VALUE: Mentioned above
 * CALLED BY: HDS AR Sales and COGS Analysis -Master
 *************************************************************************/
  --
  PROCEDURE main (
    retcode               out number
   ,errbuf                out varchar2
   ,p_ledger_id           in  number
   ,p_period_name         in  varchar2
   ,p_product             in  varchar2
   ,p_location            in  varchar2
   ,p_rev_account         in  varchar2
   ,p_cogs_account        in  varchar2
  ) IS
  -- Define data set forREVENUE SLA entries
       CURSOR sla_rev_trx IS
       SELECT  inv_info.branch                 branch
              ,jes.user_je_source_name         gl_source
              ,jh.je_category                  gl_category
              ,jh.period_name                  period
              ,gcc.segment1                    gl_product
              ,gcc.segment2                    gl_location
              ,gcc.segment4                    gl_account
              ,jl.effective_date               accounting_date
              ,xel.accounting_class_code       accounting_class_code
              ,xeh.event_type_code             sla_event_type
              ,gb.name                         journal_batch_name
              ,jh.NAME                         journal_header_name
              ,jl.je_line_num                  journal_line_num
              ,inv_info.invoice_number         invoice_number
              ,inv_info.invoice_source         invoice_source
              ,inv_info.inv_creation_date      invoice_creation_date
              ,inv_info.invoice_date           invoice_date
              ,inv_info.sales_order            order_number
              ,inv_info.order_line_num         order_line_num
              ,inv_info.inv_line_num           invoice_line_num
              ,inv_info.quantity               quantity
              ,inv_info.item_number            item_number
              ,nvl
                (
                 xdl.unrounded_accounted_dr, 0
                )
                 -
               nvl
                (
                 xdl.unrounded_accounted_cr, 0
                )                               amount
              ,xdl.source_distribution_type     source_distribution_type
              ,xdl.source_distribution_id_num_1 source_dist_id_num_1
              ,gcc.code_combination_id          gl_ccid
              ,ir.gl_sl_link_id                 gl_sl_link_id
          FROM gl_je_lines jl,
               gl_je_headers jh,
               gl_je_sources jes,
               gl_je_categories jec,
               gl_import_references ir,
               xla_ae_lines xel,
               xla_ae_headers xeh,
               xla_distribution_links xdl,
               gl_code_combinations_kfv gcc,
               gl_ledgers gle,
               gl_period_statuses gps,
               gl_je_batches gb
              ,(
                select nvl
                        (
                          (
                            select segment1
                            from   mtl_system_items
                            where  1 =1
                              and  inventory_item_id =invl.inventory_item_id
                              and  organization_id   =invl.warehouse_id
                          )
                         ,Null
                        )                                item_number
                       ,inv.trx_number                   invoice_number
                       ,trunc(inv.creation_date)         inv_creation_date
                       ,inv.trx_date                     invoice_date
                       ,org.organization_code            branch
                       ,invl.line_number                 inv_line_num
                       ,abs(nvl
                         (
                           invl.quantity_invoiced
                          ,invl.quantity_credited
                         ))                              quantity
                       ,invl.interface_line_attribute1   sales_order
                       ,oeol.line_number                 order_line_num
                       ,argl.cust_trx_line_gl_dist_id    ar_gl_dist_id
                       ,rbs.name                         invoice_source
                from   ra_cust_trx_line_gl_dist_all argl
                      ,ra_customer_trx_lines_all    invl
                      ,ra_customer_trx_all          inv
                      ,org_organization_definitions org
                      ,ra_batch_sources_all         rbs
                      ,oe_order_lines_all           oeol
                where  1 =1
                  and  invl.customer_trx_line_id     =argl.customer_trx_line_id
                  and  inv.customer_trx_id           =invl.customer_trx_id
                  and  org.organization_id(+)        =invl.warehouse_id
                  and  rbs.batch_source_id           =inv.batch_source_id
                  and  rbs.org_id                    =inv.org_id
                  and  to_char(oeol.line_id(+))      =invl.interface_line_attribute6
               ) inv_info
         WHERE     1 = 1
               AND gps.period_name =p_period_name
               AND ((p_rev_account is null AND (GCC.SEGMENT4 like '401%')) OR (p_rev_account is not null and GCC.SEGMENT4 =p_rev_account))
               AND gcc.segment1 =p_product
               AND gcc.segment2 =p_location
               AND gle.ledger_id =p_ledger_id
               --AND gle.name = 'HD Supply USD'
               AND gps.application_id = 101
               AND jl.je_header_id = jh.je_header_id
               AND jes.je_source_name = jh.je_source
               AND jes.user_je_source_name ='Receivables'
               AND jh.je_category = jec.je_category_name
               AND ir.je_header_id = jl.je_header_id
               AND gcc.code_combination_id = jl.code_combination_id
               AND gps.period_name = jh.period_name
               AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
               AND gps.ledger_id = gle.ledger_id
               AND jh.je_batch_id = gb.je_batch_id
               AND ir.je_line_num = jl.je_line_num
               AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
               AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
               AND xel.ae_header_id = xdl.ae_header_id(+)
               AND xel.ae_line_num = xdl.ae_line_num(+)
               AND xdl.application_id =222
               AND xeh.ae_header_id(+) = xel.ae_header_id
               and inv_info.ar_gl_dist_id(+) =xdl.source_distribution_id_num_1
    UNION ALL
       SELECT  inv_info.branch                 branch
              ,jes.user_je_source_name         gl_source
              ,jh.je_category                  gl_category
              ,jh.period_name                  period
              ,gcc.segment1                    gl_product
              ,gcc.segment2                    gl_location
              ,gcc.segment4                    gl_account
              ,jl.effective_date               accounting_date
              ,xel.accounting_class_code       accounting_class_code
              ,xeh.event_type_code             sla_event_type
              ,gb.name                         journal_batch_name
              ,jh.NAME                         journal_header_name
              ,jl.je_line_num                  journal_line_num
              ,inv_info.invoice_number         invoice_number
              ,inv_info.invoice_source         invoice_source
              ,inv_info.inv_creation_date      invoice_creation_date
              ,inv_info.invoice_date           invoice_date
              ,inv_info.sales_order            order_number
              ,inv_info.order_line_num         order_line_num
              ,inv_info.inv_line_num           invoice_line_num
              ,inv_info.quantity               quantity
              ,inv_info.item_number            item_number
              ,nvl
                (
                 xdl.unrounded_accounted_dr, 0
                )
                 -
               nvl
                (
                 xdl.unrounded_accounted_cr, 0
                )                               amount
              ,xdl.source_distribution_type     source_distribution_type
              ,xdl.source_distribution_id_num_1 source_dist_id_num_1
              ,gcc.code_combination_id          gl_ccid
              ,ir.gl_sl_link_id                 gl_sl_link_id
          FROM gl_je_lines jl,
               gl_je_headers jh,
               gl_je_sources jes,
               gl_je_categories jec,
               gl_import_references ir,
               xla_ae_lines xel,
               xla_ae_headers xeh,
               xla_distribution_links xdl,
               gl_code_combinations_kfv gcc,
               gl_ledgers gle,
               gl_period_statuses gps,
               gl_je_batches gb
              ,(
                select nvl
                        (
                          (
                            select segment1
                            from   mtl_system_items
                            where  1 =1
                              and  inventory_item_id =invl.inventory_item_id
                              and  organization_id   =invl.warehouse_id
                          )
                         ,Null
                        )                                item_number
                       ,inv.trx_number                   invoice_number
                       ,trunc(inv.creation_date)         inv_creation_date
                       ,inv.trx_date                     invoice_date
                       ,org.organization_code            branch
                       ,invl.line_number                 inv_line_num
                       ,abs(nvl
                         (
                           invl.quantity_invoiced
                          ,invl.quantity_credited
                         ))                              quantity
                       ,invl.interface_line_attribute1   sales_order
                       ,oeol.line_number                 order_line_num
                       ,argl.cust_trx_line_gl_dist_id    ar_gl_dist_id
                       ,rbs.name                         invoice_source
                from   ra_cust_trx_line_gl_dist_all argl
                      ,ra_customer_trx_lines_all    invl
                      ,ra_customer_trx_all          inv
                      ,org_organization_definitions org
                      ,ra_batch_sources_all         rbs
                      ,oe_order_lines_all           oeol
                where  1 =1
                  and  invl.customer_trx_line_id     =argl.customer_trx_line_id
                  and  inv.customer_trx_id           =invl.customer_trx_id
                  and  org.organization_id(+)        =invl.warehouse_id
                  and  rbs.batch_source_id           =inv.batch_source_id
                  and  rbs.org_id                    =inv.org_id
                  and  to_char(oeol.line_id(+))      =invl.interface_line_attribute6
               ) inv_info
         WHERE     1 = 1
               AND gps.period_name =p_period_name
               AND ((p_rev_account is null AND (GCC.SEGMENT4 like '401%')) OR (p_rev_account is not null and GCC.SEGMENT4 =p_rev_account))
               AND gcc.segment1 =p_product
               AND gcc.segment2 =p_location
               AND gle.ledger_id =p_ledger_id
               --AND gle.name = 'HD Supply USD'
               AND gps.application_id = 101
               AND jl.je_header_id = jh.je_header_id
               AND jes.je_source_name = jh.je_source
               AND jes.user_je_source_name ='Cost Management'
               AND jh.je_category = jec.je_category_name
               AND ir.je_header_id = jl.je_header_id
               AND gcc.code_combination_id = jl.code_combination_id
               AND gps.period_name = jh.period_name
               AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
               AND gps.ledger_id = gle.ledger_id
               AND jh.je_batch_id = gb.je_batch_id
               AND ir.je_line_num = jl.je_line_num
               AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
               AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
               AND xel.ae_header_id = xdl.ae_header_id(+)
               AND xel.ae_line_num = xdl.ae_line_num(+)
               AND xdl.application_id =707
               AND xeh.ae_header_id(+) = xel.ae_header_id
               and inv_info.ar_gl_dist_id(+) =xdl.source_distribution_id_num_1
    UNION ALL
       SELECT  inv_info.branch                 branch
              ,jes.user_je_source_name         gl_source
              ,jh.je_category                  gl_category
              ,jh.period_name                  period
              ,gcc.segment1                    gl_product
              ,gcc.segment2                    gl_location
              ,gcc.segment4                    gl_account
              ,jl.effective_date               accounting_date
              ,xel.accounting_class_code       accounting_class_code
              ,xeh.event_type_code             sla_event_type
              ,gb.name                         journal_batch_name
              ,jh.NAME                         journal_header_name
              ,jl.je_line_num                  journal_line_num
              ,inv_info.invoice_number         invoice_number
              ,inv_info.invoice_source         invoice_source
              ,inv_info.inv_creation_date      invoice_creation_date
              ,inv_info.invoice_date           invoice_date
              ,inv_info.sales_order            order_number
              ,inv_info.order_line_num         order_line_num
              ,inv_info.inv_line_num           invoice_line_num
              ,inv_info.quantity               quantity
              ,inv_info.item_number            item_number
              ,nvl
                (
                 xdl.unrounded_accounted_dr, 0
                )
                 -
               nvl
                (
                 xdl.unrounded_accounted_cr, 0
                )                               amount
              ,xdl.source_distribution_type     source_distribution_type
              ,xdl.source_distribution_id_num_1 source_dist_id_num_1
              ,gcc.code_combination_id          gl_ccid
              ,ir.gl_sl_link_id                 gl_sl_link_id
          FROM gl_je_lines jl,
               gl_je_headers jh,
               gl_je_sources jes,
               gl_je_categories jec,
               gl_import_references ir,
               xla_ae_lines xel,
               xla_ae_headers xeh,
               xla_distribution_links xdl,
               gl_code_combinations_kfv gcc,
               gl_ledgers gle,
               gl_period_statuses gps,
               gl_je_batches gb
              ,(
                select nvl
                        (
                          (
                            select segment1
                            from   mtl_system_items
                            where  1 =1
                              and  inventory_item_id =invl.inventory_item_id
                              and  organization_id   =invl.warehouse_id
                          )
                         ,Null
                        )                                item_number
                       ,inv.trx_number                   invoice_number
                       ,trunc(inv.creation_date)         inv_creation_date
                       ,inv.trx_date                     invoice_date
                       ,org.organization_code            branch
                       ,invl.line_number                 inv_line_num
                       ,abs(nvl
                         (
                           invl.quantity_invoiced
                          ,invl.quantity_credited
                         ))                              quantity
                       ,invl.interface_line_attribute1   sales_order
                       ,oeol.line_number                 order_line_num
                       ,argl.cust_trx_line_gl_dist_id    ar_gl_dist_id
                       ,rbs.name                         invoice_source
                from   ra_cust_trx_line_gl_dist_all argl
                      ,ra_customer_trx_lines_all    invl
                      ,ra_customer_trx_all          inv
                      ,org_organization_definitions org
                      ,ra_batch_sources_all         rbs
                      ,oe_order_lines_all           oeol
                where  1 =1
                  and  invl.customer_trx_line_id     =argl.customer_trx_line_id
                  and  inv.customer_trx_id           =invl.customer_trx_id
                  and  org.organization_id(+)        =invl.warehouse_id
                  and  rbs.batch_source_id           =inv.batch_source_id
                  and  rbs.org_id                    =inv.org_id
                  and  to_char(oeol.line_id(+))      =invl.interface_line_attribute6
               ) inv_info
         WHERE     1 = 1
               AND gps.period_name =p_period_name
               AND ((p_rev_account is null AND (GCC.SEGMENT4 like '401%')) OR (p_rev_account is not null and GCC.SEGMENT4 =p_rev_account))
               AND gcc.segment1 =p_product
               AND gcc.segment2 =p_location
               AND gle.ledger_id =p_ledger_id
               --AND gle.name = 'HD Supply USD'
               AND gps.application_id = 101
               AND jl.je_header_id = jh.je_header_id
               AND jes.je_source_name = jh.je_source
               AND jes.user_je_source_name ='Payables'
               AND jh.je_category = jec.je_category_name
               AND ir.je_header_id = jl.je_header_id
               AND gcc.code_combination_id = jl.code_combination_id
               AND gps.period_name = jh.period_name
               AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
               AND gps.ledger_id = gle.ledger_id
               AND jh.je_batch_id = gb.je_batch_id
               AND ir.je_line_num = jl.je_line_num
               AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
               AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
               AND xel.ae_header_id = xdl.ae_header_id(+)
               AND xel.ae_line_num = xdl.ae_line_num(+)
               AND xdl.application_id =200
               AND xeh.ae_header_id(+) = xel.ae_header_id
               and inv_info.ar_gl_dist_id(+) =xdl.source_distribution_id_num_1
    UNION ALL
       SELECT  inv_info.branch                 branch
              ,jes.user_je_source_name         gl_source
              ,jh.je_category                  gl_category
              ,jh.period_name                  period
              ,gcc.segment1                    gl_product
              ,gcc.segment2                    gl_location
              ,gcc.segment4                    gl_account
              ,jl.effective_date               accounting_date
              ,xel.accounting_class_code       accounting_class_code
              ,xeh.event_type_code             sla_event_type
              ,gb.name                         journal_batch_name
              ,jh.NAME                         journal_header_name
              ,jl.je_line_num                  journal_line_num
              ,inv_info.invoice_number         invoice_number
              ,inv_info.invoice_source         invoice_source
              ,inv_info.inv_creation_date      invoice_creation_date
              ,inv_info.invoice_date           invoice_date
              ,inv_info.sales_order            order_number
              ,inv_info.order_line_num         order_line_num
              ,inv_info.inv_line_num           invoice_line_num
              ,inv_info.quantity               quantity
              ,inv_info.item_number            item_number
              ,nvl
                (
                 xdl.unrounded_accounted_dr, 0
                )
                 -
               nvl
                (
                 xdl.unrounded_accounted_cr, 0
                )                               amount
              ,xdl.source_distribution_type     source_distribution_type
              ,xdl.source_distribution_id_num_1 source_dist_id_num_1
              ,gcc.code_combination_id          gl_ccid
              ,ir.gl_sl_link_id                 gl_sl_link_id
          FROM gl_je_lines jl,
               gl_je_headers jh,
               gl_je_sources jes,
               gl_je_categories jec,
               gl_import_references ir,
               xla_ae_lines xel,
               xla_ae_headers xeh,
               xla_distribution_links xdl,
               gl_code_combinations_kfv gcc,
               gl_ledgers gle,
               gl_period_statuses gps,
               gl_je_batches gb
              ,(
                select nvl
                        (
                          (
                            select segment1
                            from   mtl_system_items
                            where  1 =1
                              and  inventory_item_id =invl.inventory_item_id
                              and  organization_id   =invl.warehouse_id
                          )
                         ,Null
                        )                                item_number
                       ,inv.trx_number                   invoice_number
                       ,trunc(inv.creation_date)         inv_creation_date
                       ,inv.trx_date                     invoice_date
                       ,org.organization_code            branch
                       ,invl.line_number                 inv_line_num
                       ,abs(nvl
                         (
                           invl.quantity_invoiced
                          ,invl.quantity_credited
                         ))                              quantity
                       ,invl.interface_line_attribute1   sales_order
                       ,oeol.line_number                 order_line_num
                       ,argl.cust_trx_line_gl_dist_id    ar_gl_dist_id
                       ,rbs.name                         invoice_source
                from   ra_cust_trx_line_gl_dist_all argl
                      ,ra_customer_trx_lines_all    invl
                      ,ra_customer_trx_all          inv
                      ,org_organization_definitions org
                      ,ra_batch_sources_all         rbs
                      ,oe_order_lines_all           oeol
                where  1 =1
                  and  invl.customer_trx_line_id     =argl.customer_trx_line_id
                  and  inv.customer_trx_id           =invl.customer_trx_id
                  and  org.organization_id(+)        =invl.warehouse_id
                  and  rbs.batch_source_id           =inv.batch_source_id
                  and  rbs.org_id                    =inv.org_id
                  and  to_char(oeol.line_id(+))      =invl.interface_line_attribute6
               ) inv_info
         WHERE     1 = 1
               AND gps.period_name =p_period_name
               AND ((p_rev_account is null AND (GCC.SEGMENT4 like '401%')) OR (p_rev_account is not null and GCC.SEGMENT4 =p_rev_account))
               AND gcc.segment1 =p_product
               AND gcc.segment2 =p_location
               AND gle.ledger_id =p_ledger_id
               --AND gle.name = 'HD Supply USD'
               AND gps.application_id = 101
               AND jl.je_header_id = jh.je_header_id
               AND jes.je_source_name = jh.je_source
               AND jes.user_je_source_name ='Assets'
               AND jh.je_category = jec.je_category_name
               AND ir.je_header_id = jl.je_header_id
               AND gcc.code_combination_id = jl.code_combination_id
               AND gps.period_name = jh.period_name
               AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
               AND gps.ledger_id = gle.ledger_id
               AND jh.je_batch_id = gb.je_batch_id
               AND ir.je_line_num = jl.je_line_num
               AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
               AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
               AND xel.ae_header_id = xdl.ae_header_id(+)
               AND xel.ae_line_num = xdl.ae_line_num(+)
               AND xdl.application_id =140
               AND xeh.ae_header_id(+) = xel.ae_header_id
               and inv_info.ar_gl_dist_id(+) =xdl.source_distribution_id_num_1
    UNION ALL
    SELECT
               to_char(null)                   branch
              ,jes.user_je_source_name         gl_source
              ,jh.je_category                  gl_category
              ,jh.period_name                  period
              ,gcc.segment1                    gl_product
              ,gcc.segment2                    gl_location
              ,gcc.segment4                    gl_account
              ,jl.effective_date               accounting_date
              ,to_char(null)                   accounting_class_code
              ,to_char(null)                   sla_event_type
              ,gb.name                         journal_batch_name
              ,jh.NAME                         journal_header_name
              ,jl.je_line_num                  journal_line_num
              ,to_char(null)                   invoice_number
              ,to_char(null)                   invoice_source
              ,to_date(null)                   invoice_creation_date
              ,to_date(null)                   invoice_date
              ,to_char(null)                   order_number
              ,to_number(null)                 order_line_num
              ,to_number(null)                 invoice_line_num
              ,to_number(null)                 quantity
              ,to_char(null)                   item_number
              ,nvl (jl.accounted_dr, 0)
               -
               nvl(jl.accounted_cr, 0)         amount
              ,to_char(null)                   source_distribution_type
              ,to_number(null)                 source_dist_id_num_1
              ,gcc.code_combination_id         gl_ccid
              ,to_number(null)                 gl_sl_link_id
    FROM gl_je_lines jl,
       gl_je_headers jh,
       gl_je_sources jes,
       gl_je_categories jec,
       gl_import_references ir,
       gl_code_combinations_kfv gcc,
       gl_ledgers gle,
       gl_period_statuses gps,
       gl_je_batches gb
    WHERE     1 = 1
       AND gps.period_name =p_period_name
       AND ((p_rev_account is null AND (GCC.SEGMENT4 like '401%')) OR (p_rev_account is not null and GCC.SEGMENT4 =p_rev_account))
       AND gcc.segment1 =p_product
       AND gcc.segment2 =p_location
       AND gle.ledger_id =p_ledger_id
       --AND gle.name = 'HD Supply USD'
       AND gps.application_id = 101
       AND jl.je_header_id = jh.je_header_id
       AND jes.je_source_name = jh.je_source
       AND jes.user_je_source_name NOT IN ('Receivables', 'Cost Management', 'Payables', 'Assets')
       AND jh.je_category = jec.je_category_name
       AND ir.je_header_id(+) = jl.je_header_id
       AND gcc.code_combination_id = jl.code_combination_id
       AND gps.period_name = jh.period_name
       AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
       AND gps.ledger_id = gle.ledger_id
       AND jh.je_batch_id = gb.je_batch_id
       AND ir.je_line_num(+) = jl.je_line_num;
  --   ================================================================================================
  -- Define data set for COGS SLA entries
       CURSOR sla_cogs_trx is
        SELECT
                   to_char(null)                   branch
                  ,jes.user_je_source_name         gl_source
                  ,jh.je_category                  gl_category
                  ,jh.period_name                  period
                  ,gcc.segment1                    gl_product
                  ,gcc.segment2                    gl_location
                  ,gcc.segment4                    gl_account
                  ,jl.effective_date               accounting_date
                  ,xel.accounting_class_code       accounting_class_code
                  ,xeh.event_type_code             sla_event_type
                  ,gb.name                         journal_batch_name
                  ,jh.NAME                         journal_header_name
                  ,jl.je_line_num                  journal_line_num
                  ,to_date(null)                   creation_date
                  ,to_date(null)                   transaction_date
                  ,to_char(null)                   po_number
                  ,to_char(null)                   order_number
                  ,to_char(null)                   order_line_num
                  ,to_number(null)                 invoice_line_num
                  ,to_number(null)                 quantity
                  ,to_char(null)                   item_number
                  ,nvl
                    (
                     xdl.unrounded_accounted_dr, 0
                    )
                     -
                   nvl
                    (
                     xdl.unrounded_accounted_cr, 0
                    )                               amount
                  ,xdl.source_distribution_type     source_distribution_type
                  ,xdl.source_distribution_id_num_1 source_dist_id_num_1
                  ,gcc.code_combination_id          gl_ccid
                  ,ir.gl_sl_link_id                 gl_sl_link_id
                  ,to_number(null)                  inventory_item_id
                  ,to_number(null)                  organization_id
                  ,to_number(null)                  inv_transaction_id
                  ,to_number(null)                  trxn_source_id
                  ,to_char(null)                    transaction_description
                  ,to_number(null)                  trxn_source_line_id
                  ,to_number(null)                  rcv_transaction_id
                  ,to_number(null)                  source_line_id
              FROM gl_je_lines jl,
                   gl_je_headers jh,
                   gl_je_sources jes,
                   gl_je_categories jec,
                   gl_import_references ir,
                   xla_ae_lines xel,
                   xla_ae_headers xeh,
                   xla_distribution_links xdl,
                   --mtl_transaction_accounts mta,
                   --mtl_system_items msi,
                   gl_code_combinations_kfv gcc,
                   gl_ledgers gle,
                   gl_period_statuses gps,
                   gl_je_batches gb
             WHERE     1 = 1
                   AND gps.period_name =p_period_name
                   AND ((p_cogs_account is null AND (GCC.SEGMENT4 like '501%')) OR (p_cogs_account is not null and GCC.SEGMENT4 =p_cogs_account))
                   AND gcc.segment1 =p_product
                   and gcc.segment2 =p_location
                   AND gle.ledger_id =p_ledger_id
                   --AND gle.name = 'HD Supply USD'
                   AND gps.application_id = 101
                   AND jl.je_header_id = jh.je_header_id
                   AND jes.je_source_name = jh.je_source
                   AND jes.user_je_source_name = 'Cost Management'
                   AND jh.je_category = jec.je_category_name
                   AND ir.je_header_id = jl.je_header_id
                   AND gcc.code_combination_id = jl.code_combination_id
                   AND gps.period_name = jh.period_name
                   AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
                   AND gps.ledger_id = gle.ledger_id
                   AND jh.je_batch_id = gb.je_batch_id
                   AND ir.je_line_num = jl.je_line_num
                   AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
                   AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
                   AND xel.ae_header_id = xdl.ae_header_id(+)
                   AND xel.ae_line_num = xdl.ae_line_num(+)
                   AND xdl.application_id =707
                   AND xeh.ae_header_id(+) = xel.ae_header_id
        UNION ALL
        SELECT
                   to_char(null)                   branch
                  ,jes.user_je_source_name         gl_source
                  ,jh.je_category                  gl_category
                  ,jh.period_name                  period
                  ,gcc.segment1                    gl_product
                  ,gcc.segment2                    gl_location
                  ,gcc.segment4                    gl_account
                  ,jl.effective_date               accounting_date
                  ,xel.accounting_class_code       accounting_class_code
                  ,xeh.event_type_code             sla_event_type
                  ,gb.name                         journal_batch_name
                  ,jh.NAME                         journal_header_name
                  ,jl.je_line_num                  journal_line_num
                  ,to_date(null)                   creation_date
                  ,to_date(null)                   transaction_date
                  ,to_char(null)                   po_number
                  ,to_char(null)                   order_number
                  ,to_char(null)                   order_line_num
                  ,to_number(null)                 invoice_line_num
                  ,to_number(null)                 quantity
                  ,to_char(null)                   item_number
                  ,nvl
                    (
                     xdl.unrounded_accounted_dr, 0
                    )
                     -
                   nvl
                    (
                     xdl.unrounded_accounted_cr, 0
                    )                               amount
                  ,xdl.source_distribution_type     source_distribution_type
                  ,xdl.source_distribution_id_num_1 source_dist_id_num_1
                  ,gcc.code_combination_id          gl_ccid
                  ,ir.gl_sl_link_id                 gl_sl_link_id
                  ,to_number(null)                  inventory_item_id
                  ,to_number(null)                  organization_id
                  ,to_number(null)                  inv_transaction_id
                  ,to_number(null)                  trxn_source_id
                  ,to_char(null)                    transaction_description
                  ,to_number(null)                  trxn_source_line_id
                  ,to_number(null)                  rcv_transaction_id
                  ,to_number(null)                  source_line_id
        FROM gl_je_lines jl,
           gl_je_headers jh,
           gl_je_sources jes,
           gl_je_categories jec,
           gl_import_references ir,
           xla_ae_lines xel,
           xla_ae_headers xeh,
           xla_distribution_links xdl,
           gl_code_combinations_kfv gcc,
           gl_ledgers gle,
           gl_period_statuses gps,
           gl_je_batches gb
        WHERE     1 = 1
           AND gps.period_name =p_period_name
           AND ((p_cogs_account is null AND (GCC.SEGMENT4 like '501%')) OR (p_cogs_account is not null and GCC.SEGMENT4 =p_cogs_account))
           AND gcc.segment1 =p_product
           and gcc.segment2 =p_location
           AND gle.ledger_id =p_ledger_id
           --AND gle.name = 'HD Supply USD'
           AND gps.application_id = 101
           AND jl.je_header_id = jh.je_header_id
           AND jes.je_source_name = jh.je_source
           AND jes.user_je_source_name ='Payables'
           AND jh.je_category = jec.je_category_name
           AND ir.je_header_id = jl.je_header_id
           AND gcc.code_combination_id = jl.code_combination_id
           AND gps.period_name = jh.period_name
           AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
           AND gps.ledger_id = gle.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
           AND ir.je_line_num = jl.je_line_num
           AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
           AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
           AND xel.ae_header_id = xdl.ae_header_id(+)
           AND xel.ae_line_num = xdl.ae_line_num(+)
           AND xdl.application_id =200
           AND xeh.ae_header_id(+) = xel.ae_header_id
        UNION ALL
        SELECT
                   to_char(null)                   branch
                  ,jes.user_je_source_name         gl_source
                  ,jh.je_category                  gl_category
                  ,jh.period_name                  period
                  ,gcc.segment1                    gl_product
                  ,gcc.segment2                    gl_location
                  ,gcc.segment4                    gl_account
                  ,jl.effective_date               accounting_date
                  ,xel.accounting_class_code       accounting_class_code
                  ,xeh.event_type_code             sla_event_type
                  ,gb.name                         journal_batch_name
                  ,jh.NAME                         journal_header_name
                  ,jl.je_line_num                  journal_line_num
                  ,to_date(null)                   creation_date
                  ,to_date(null)                   transaction_date
                  ,to_char(null)                   po_number
                  ,to_char(null)                   order_number
                  ,to_char(null)                   order_line_num
                  ,to_number(null)                 invoice_line_num
                  ,to_number(null)                 quantity
                  ,to_char(null)                   item_number
                  ,nvl
                    (
                     xdl.unrounded_accounted_dr, 0
                    )
                     -
                   nvl
                    (
                     xdl.unrounded_accounted_cr, 0
                    )                               amount
                  ,xdl.source_distribution_type     source_distribution_type
                  ,xdl.source_distribution_id_num_1 source_dist_id_num_1
                  ,gcc.code_combination_id          gl_ccid
                  ,ir.gl_sl_link_id                 gl_sl_link_id
                  ,to_number(null)                  inventory_item_id
                  ,to_number(null)                  organization_id
                  ,to_number(null)                  inv_transaction_id
                  ,to_number(null)                  trxn_source_id
                  ,to_char(null)                    transaction_description
                  ,to_number(null)                  trxn_source_line_id
                  ,to_number(null)                  rcv_transaction_id
                  ,to_number(null)                  source_line_id
        FROM gl_je_lines jl,
           gl_je_headers jh,
           gl_je_sources jes,
           gl_je_categories jec,
           gl_import_references ir,
           xla_ae_lines xel,
           xla_ae_headers xeh,
           xla_distribution_links xdl,
           gl_code_combinations_kfv gcc,
           gl_ledgers gle,
           gl_period_statuses gps,
           gl_je_batches gb
        WHERE     1 = 1
           AND gps.period_name =p_period_name
           AND ((p_cogs_account is null AND (GCC.SEGMENT4 like '501%')) OR (p_cogs_account is not null and GCC.SEGMENT4 =p_cogs_account))
           AND gcc.segment1 =p_product
           and gcc.segment2 =p_location
           AND gle.ledger_id =p_ledger_id
           --AND gle.name = 'HD Supply USD'
           AND gps.application_id = 101
           AND jl.je_header_id = jh.je_header_id
           AND jes.je_source_name = jh.je_source
           AND jes.user_je_source_name ='Assets'
           AND jh.je_category = jec.je_category_name
           AND ir.je_header_id = jl.je_header_id
           AND gcc.code_combination_id = jl.code_combination_id
           AND gps.period_name = jh.period_name
           AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
           AND gps.ledger_id = gle.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
           AND ir.je_line_num = jl.je_line_num
           AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
           AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
           AND xel.ae_header_id = xdl.ae_header_id(+)
           AND xel.ae_line_num = xdl.ae_line_num(+)
           AND xdl.application_id =140
           AND xeh.ae_header_id(+) = xel.ae_header_id
        UNION ALL
        SELECT
                   to_char(null)                   branch
                  ,jes.user_je_source_name         gl_source
                  ,jh.je_category                  gl_category
                  ,jh.period_name                  period
                  ,gcc.segment1                    gl_product
                  ,gcc.segment2                    gl_location
                  ,gcc.segment4                    gl_account
                  ,jl.effective_date               accounting_date
                  ,xel.accounting_class_code       accounting_class_code
                  ,xeh.event_type_code             sla_event_type
                  ,gb.name                         journal_batch_name
                  ,jh.NAME                         journal_header_name
                  ,jl.je_line_num                  journal_line_num
                  ,to_date(null)                   creation_date
                  ,to_date(null)                   transaction_date
                  ,to_char(null)                   po_number
                  ,to_char(null)                   order_number
                  ,to_char(null)                   order_line_num
                  ,to_number(null)                 invoice_line_num
                  ,to_number(null)                 quantity
                  ,to_char(null)                   item_number
                  ,nvl
                    (
                     xdl.unrounded_accounted_dr, 0
                    )
                     -
                   nvl
                    (
                     xdl.unrounded_accounted_cr, 0
                    )                               amount
                  ,xdl.source_distribution_type     source_distribution_type
                  ,xdl.source_distribution_id_num_1 source_dist_id_num_1
                  ,gcc.code_combination_id          gl_ccid
                  ,ir.gl_sl_link_id                 gl_sl_link_id
                  ,to_number(null)                  inventory_item_id
                  ,to_number(null)                  organization_id
                  ,to_number(null)                  inv_transaction_id
                  ,to_number(null)                  trxn_source_id
                  ,to_char(null)                    transaction_description
                  ,to_number(null)                  trxn_source_line_id
                  ,to_number(null)                  rcv_transaction_id
                  ,to_number(null)                  source_line_id
        FROM gl_je_lines jl,
           gl_je_headers jh,
           gl_je_sources jes,
           gl_je_categories jec,
           gl_import_references ir,
           xla_ae_lines xel,
           xla_ae_headers xeh,
           xla_distribution_links xdl,
           gl_code_combinations_kfv gcc,
           gl_ledgers gle,
           gl_period_statuses gps,
           gl_je_batches gb
        WHERE     1 = 1
           AND gps.period_name =p_period_name
           AND ((p_cogs_account is null AND (GCC.SEGMENT4 like '501%')) OR (p_cogs_account is not null and GCC.SEGMENT4 =p_cogs_account))
           AND gcc.segment1 =p_product
           and gcc.segment2 =p_location
           AND gle.ledger_id =p_ledger_id
           --AND gle.name = 'HD Supply USD'
           AND gps.application_id = 101
           AND jl.je_header_id = jh.je_header_id
           AND jes.je_source_name = jh.je_source
           AND jes.user_je_source_name ='Receivables'
           AND jh.je_category = jec.je_category_name
           AND ir.je_header_id = jl.je_header_id
           AND gcc.code_combination_id = jl.code_combination_id
           AND gps.period_name = jh.period_name
           AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
           AND gps.ledger_id = gle.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
           AND ir.je_line_num = jl.je_line_num
           AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
           AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
           AND xel.ae_header_id = xdl.ae_header_id(+)
           AND xel.ae_line_num = xdl.ae_line_num(+)
           AND xdl.application_id =222
           AND xeh.ae_header_id(+) = xel.ae_header_id
        UNION ALL
        SELECT
                   to_char(null)                   branch
                  ,jes.user_je_source_name         gl_source
                  ,jh.je_category                  gl_category
                  ,jh.period_name                  period
                  ,gcc.segment1                    gl_product
                  ,gcc.segment2                    gl_location
                  ,gcc.segment4                    gl_account
                  ,jl.effective_date               accounting_date
                  ,to_char(null)                   accounting_class_code
                  ,to_char(null)                   sla_event_type
                  ,gb.name                         journal_batch_name
                  ,jh.NAME                         journal_header_name
                  ,jl.je_line_num                  journal_line_num
                  ,to_date(null)                   creation_date
                  ,to_date(null)                   transaction_date
                  ,to_char(null)                   po_number
                  ,to_char(null)                   order_number
                  ,to_char(null)                   order_line_num
                  ,to_number(null)                 invoice_line_num
                  ,to_number(null)                 quantity
                  ,to_char(null)                   item_number
                  ,nvl(jl.accounted_dr, 0)
                   -
                   nvl(jl.accounted_cr, 0)          amount
                  ,to_char(null)                    source_distribution_type
                  ,to_number(null)                  source_dist_id_num_1
                  ,gcc.code_combination_id          gl_ccid
                  ,ir.gl_sl_link_id                 gl_sl_link_id
                  ,to_number(null)                  inventory_item_id
                  ,to_number(null)                  organization_id
                  ,to_number(null)                  inv_transaction_id
                  ,to_number(null)                  trxn_source_id
                  ,to_char(null)                    transaction_description
                  ,to_number(null)                  trxn_source_line_id
                  ,to_number(null)                  rcv_transaction_id
                  ,to_number(null)                  source_line_id
              FROM gl_je_lines jl,
                   gl_je_headers jh,
                   gl_je_sources jes,
                   gl_je_categories jec,
                   gl_import_references ir,
                   gl_code_combinations_kfv gcc,
                   gl_ledgers gle,
                   gl_period_statuses gps,
                   gl_je_batches gb
             WHERE     1 = 1
                   AND gps.period_name =p_period_name
                   AND ((p_cogs_account is null AND (GCC.SEGMENT4 like '501%')) OR (p_cogs_account is not null and GCC.SEGMENT4 =p_cogs_account))
                   AND gcc.segment1 =p_product
                   and gcc.segment2 =p_location
                   AND gle.ledger_id =p_ledger_id
                   --AND gle.name = 'HD Supply USD'
                   AND gps.application_id = 101
                   AND jl.je_header_id = jh.je_header_id
                   AND jes.je_source_name = jh.je_source
                   AND jes.user_je_source_name NOT IN ('Receivables', 'Cost Management', 'Payables', 'Assets')
                   AND jh.je_category = jec.je_category_name
                   AND ir.je_header_id(+) = jl.je_header_id
                   AND gcc.code_combination_id = jl.code_combination_id
                   AND gps.period_name = jh.period_name
                   AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
                   AND gps.ledger_id = gle.ledger_id
                   AND jh.je_batch_id = gb.je_batch_id
                   AND ir.je_line_num(+) = jl.je_line_num;
  --
       CURSOR mtl_txn_info (p_inv_sub_ledger_id in number) IS
        select mtx.inventory_item_id                               inventory_item_id
              ,mtx.organization_id                                 organization_id
              ,mtx.transaction_id                                  inv_transaction_id
              ,mtx.transaction_source_id                           trxn_source_id
              ,mtxsource.transaction_source_type_name||' / '||
               mtxtype.transaction_type_name||' / '||
               mtxaction.meaning                                   trxn_description
              ,mtx.trx_source_line_id                              trxn_source_line_id
              ,mtx.rcv_transaction_id                              rcv_transaction_id
              ,mtx.source_line_id                                  source_line_id
              ,oeol.line_number                                    order_line_num
              ,to_char(oeoh.order_number)                          order_number
              ,msi.segment1                                        item_number
              ,org.organization_code                               branch
              ,trunc(mta.transaction_date)                         transaction_date
              ,trunc(mta.creation_date)                            creation_date
              ,abs(mta.primary_quantity)                           quantity
        from   mtl_transaction_accounts  mta
              ,mtl_material_transactions mtx
              ,mtl_transaction_types     mtxtype
              ,mtl_txn_source_types      mtxsource
              ,fnd_lookup_values_vl      mtxaction
              ,oe_order_lines_all        oeol
              ,oe_order_headers_all      oeoh
              ,mtl_system_items              msi
              ,org_organization_definitions  org
        where  1 =1
          and  mta.inv_sub_ledger_id                    =p_inv_sub_ledger_id
          and  mtx.transaction_id(+)                    =mta.transaction_id
          and  mtxsource.transaction_source_type_id(+)  =mtx.transaction_source_type_id
          and  mtxtype.transaction_type_id(+)           =mtx.transaction_type_id
          and  mtxtype.transaction_action_id(+)         =mtx.transaction_action_id
          and  mtxaction.lookup_type(+)                 ='MTL_TRANSACTION_ACTION'
          and  mtxaction.lookup_code(+)                 =mtxtype.transaction_action_id
          and  oeol.line_id(+)                          =mtx.trx_source_line_id
          and  oeoh.header_id(+)                        =oeol.header_id
          and  msi.inventory_item_id(+)                 =mta.inventory_item_id
          and  msi.organization_id(+)                   =mta.organization_id
          and  org.organization_id(+)                   =mta.organization_id;
  --
       CURSOR adj_info (p_line_id in number) IS
            select adj.trx_number                   invoice_number
                  ,adj.source                       invoice_source
                  ,trunc(trx.creation_date)         invoice_creation_date
                  ,trunc(trx.trx_date)              invoice_date
                  ,trxl.line_number                 invoice_line_num
                  ,trx.interface_header_attribute1  order_number
                  ,msi.item_number                  item_number
                  ,org.organization_code            branch
                  ,oeol.line_number                 order_line_num
            from   ar_distributions_all      ard
                  ,ar_adjustments_v          adj
                  ,ra_customer_trx           trx
                  ,ra_customer_trx_lines     trxl
                  ,(
                     select segment1           item_number
                           ,organization_id    org_id
                           ,inventory_item_id  item_id
                     from   mtl_system_items
                     where  1 =1
                       and  organization_id =222
                   ) msi
                  ,org_organization_definitions org
                  ,oe_order_lines_all oeol
            where  1 =1
              and  ard.line_id                   =p_line_id
              and  ard.source_table              ='ADJ'
              and  adj.adjustment_id             =ard.source_id
              and  trx.customer_trx_id(+)        =adj.customer_trx_id
              and  trxl.customer_trx_line_id(+)  =adj.customer_trx_line_id
              and  msi.item_id(+)                =trxl.inventory_item_id
              and  to_char(org.organization_id(+))  =trx.interface_header_attribute10
              and  oeol.line_id(+)                  =trxl.interface_line_attribute6;
  --
  type Rev_Tbl is table of xxcus.xxcus_match_gl_rev_b%rowtype index by binary_integer;
  Rev_Rec Rev_Tbl;
  --
  type COGS_Tbl is table of xxcus.xxcus_match_gl_cogs_b%rowtype index by binary_integer;
  COGS_Rec COGS_Tbl;
  --
  mtl_txn_rec  mtl_txn_info%rowtype;
  adj_rec      adj_info%rowtype;
  -- ================================================================================================
  n_req_id      number :=0;
  p_limit       number :=5000;
  n_fetch_count number :=0;
  v_path        varchar2(80) :=Null;
  --
  n_retcode     number :=Null;
  v_errbuf      varchar2(2000) :=Null;
  lc_request_data  VARCHAR2(20) :='';
  --
  -- =========================================================================================
  l_module        VARCHAR2(24) :='HDS GL -WhiteCap';
  l_err_msg       CLOB;
  l_sec           VARCHAR2(255);
  l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_match_gl_rev_cogs.main';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  --
  -- ================================================================================================
  --
  begin --Main block starts here.
   --
       --
       begin
        execute immediate 'truncate table xxcus.xxcus_match_gl_rev_b';
       exception
        when others then
          print_log('Issue in truncating table xxcus.xxcus_match_gl_rev_b, message ='||sqlerrm);
          rollback;
       end;
       --
       --
       begin
        execute immediate 'truncate table xxcus.xxcus_match_gl_cogs_b';
       exception
        when others then
          print_log('Issue in truncating table xxcus.xxcus_match_gl_cogs_b, message ='||sqlerrm);
          rollback;
       end;
       --
       begin
        execute immediate 'truncate table xxcus.xxcus_gl_ar_rev_cogs_final_b';
       exception
        when others then
          print_log('Issue in truncating table xxcus.xxcus_gl_ar_rev_cogs_final_b, message ='||sqlerrm);
          rollback;
       end;
       --
       -- Store the concurrent request id
       n_req_id :=fnd_global.conc_request_id;
       --
       print_log ('');
       print_log('Before Open cursor sla_rev_trx');
       --
        begin
        --
         open sla_rev_trx;
         --
         loop
          fetch sla_rev_trx bulk collect into Rev_Rec limit p_limit;
          n_fetch_count :=n_fetch_count+1;
          exit when Rev_Rec.count =0;
          --
           if Rev_Rec.count >0 then
           --
            for idx in Rev_Rec.first .. Rev_Rec.last loop
             --
             begin
              --
              If nvl(Rev_Rec(idx).source_distribution_type, 'NONE') = 'AR_DISTRIBUTIONS_ALL' then
               --
                   open adj_info (Rev_Rec(idx).source_dist_id_num_1);
                   fetch adj_info into adj_rec;
                   close adj_info;
                   Rev_Rec(idx).order_line_num         :=adj_rec.order_line_num;
                   Rev_Rec(idx).order_number           :=adj_rec.order_number;
                   Rev_Rec(idx).item_number            :=adj_rec.item_number;
                   Rev_Rec(idx).branch                 :=adj_rec.branch;
                   Rev_Rec(idx).invoice_number         :=adj_rec.invoice_number;
                   Rev_Rec(idx).invoice_source         :=adj_rec.invoice_source;
                   Rev_Rec(idx).invoice_creation_date  :=adj_rec.invoice_creation_date;
                   Rev_Rec(idx).invoice_date           :=adj_rec.invoice_date;
                   Rev_Rec(idx).invoice_line_num       :=adj_rec.invoice_line_num;
               --
              End if;
             --
             exception
              when no_data_found then
               Null;
              when others then
               print_log('Other errors in fetching ar_distributions_all data for line_id ='||Rev_Rec(idx).source_dist_id_num_1);
               print_log('Other errors in fetching ar_distributions_all data, message ='||sqlerrm);
             end;
             --
            end loop;
           --
            begin
              savepoint start_here;
              forall idx101 in Rev_Rec.first .. Rev_Rec.last
              insert into xxcus.xxcus_match_gl_rev_b values Rev_Rec(idx101);
            exception
             when others then
              rollback to start_here;
              print_log('Fetch count ='||n_fetch_count||', Other errors while bulk insert of SLA revenue entries for the period ='||p_period_name);
              print_log('Fetch count ='||n_fetch_count||', Other errors ='||sqlerrm);
            end;
           --
           end if;
          --
         end loop; --loop for cursor sla_rev_trx
         --
         close sla_rev_trx;
         --
         print_log ('');
         print_log('After Close cursor sla_rev_trx');
         print_log ('');
        --
        exception
         when no_data_found then
          print_log('No data returned from SLA REV accounts for location ='||p_location||', period ='||p_period_name);
         when others then
          print_log('Other errors while opening cursor sla_rev_trx, for location ='||p_location||', period ='||p_period_name);
          print_log('Other errors while opening cursor sla_rev_trx, message ='||sqlerrm);
        end;
       --
       --
        begin
        --
         print_log ('');
         print_log('Before Open cursor sla_cogs_trx');
         --
         n_fetch_count :=0; --reset before we start working on COGS entries
        --
         open sla_cogs_trx;
         --
         loop
          fetch sla_cogs_trx bulk collect into COGS_Rec limit p_limit;
          n_fetch_count :=n_fetch_count+1;
          exit when COGS_Rec.count =0;
          --
           if COGS_Rec.count >0 then
           --
            for idx in COGS_Rec.first .. COGS_Rec.last loop
             --
              If nvl(COGS_Rec(idx).source_distribution_type, 'NA') ='MTL_TRANSACTION_ACCOUNTS' Then
                  Begin
                   open mtl_txn_info (COGS_Rec(idx).source_dist_id_num_1);
                   fetch mtl_txn_info into mtl_txn_rec;
                   close mtl_txn_info;
                   --
                   -- Assign Inventory transaction attributes
                   --
                     COGS_Rec(idx).inventory_item_id    :=mtl_txn_rec.inventory_item_id;
                     COGS_Rec(idx).organization_id      :=mtl_txn_rec.organization_id;
                     COGS_Rec(idx).inv_transaction_id   :=mtl_txn_rec.inv_transaction_id;
                     COGS_Rec(idx).trxn_source_id       :=mtl_txn_rec.trxn_source_id;
                     COGS_Rec(idx).trxn_description     :=mtl_txn_rec.trxn_description;
                     COGS_Rec(idx).trxn_source_line_id  :=mtl_txn_rec.trxn_source_line_id;
                     COGS_Rec(idx).rcv_transaction_id   :=mtl_txn_rec.rcv_transaction_id;
                     COGS_Rec(idx).source_line_id       :=mtl_txn_rec.source_line_id;
                     COGS_Rec(idx).order_line_num       :=mtl_txn_rec.order_line_num;
                     COGS_Rec(idx).order_number         :=mtl_txn_rec.order_number;
                     COGS_Rec(idx).item_number          :=mtl_txn_rec.item_number;
                     COGS_Rec(idx).branch               :=mtl_txn_rec.branch;
                     COGS_Rec(idx).creation_date        :=mtl_txn_rec.creation_date;
                     COGS_Rec(idx).transaction_date     :=mtl_txn_rec.transaction_date;
                     COGS_Rec(idx).quantity             :=mtl_txn_rec.quantity;
                  --
                   If nvl(COGS_Rec(idx).sla_event_type, 'NONE') NOT IN ('RMA_RCPT', 'COGS_RECOGNITION', 'NONE') then
                   --
                    begin
                        select poh.segment1
                        into   COGS_Rec(idx).po_number
                        from   po_headers_all    poh
                              ,rcv_transactions  rcvtxn
                        where  1 =1
                          and  rcvtxn.transaction_id  =mtl_txn_rec.rcv_transaction_id
                          and  poh.po_header_id       =rcvtxn.po_header_id;
                    exception
                     when no_data_found then
                      Null;
                     when others then
                       print_log('Other errors in fetching PO data for rcv_transaction_id ='||mtl_txn_rec.rcv_transaction_id);
                       print_log('Other errors in fetching PO data for rcv_transaction_id, message ='||sqlerrm);
                    end;
                   --
                   --
                   End If;
                   --
                  Exception
                   When NO_Data_Found then
                    mtl_txn_rec :=Null;
                   When Others then
                    print_log('Other errors in fetching inventory transaction data for inv_sub_ledger_id ='||COGS_Rec(idx).source_dist_id_num_1);
                    print_log('Other errors in fetching inventory transaction data, message ='||sqlerrm);
                  End;
              ElsIf nvl(COGS_Rec(idx).source_distribution_type, 'NA') ='RCV_RECEIVING_SUB_LEDGER' Then
                  Begin
                    select pol.item_id
                          ,poloc.ship_to_organization_id
                          ,Null
                          ,msi.segment1
                          ,org.organization_code
                          ,trunc(rcvsla.transaction_date)
                          ,rcvsla.source_doc_quantity
                          ,rcvsla.reference4
                    INTO   COGS_Rec(idx).inventory_item_id
                          ,COGS_Rec(idx).organization_id
                          ,COGS_Rec(idx).inv_transaction_id
                          ,COGS_Rec(idx).item_number
                          ,COGS_Rec(idx).branch
                          ,COGS_Rec(idx).transaction_date
                          ,COGS_Rec(idx).quantity
                          ,COGS_Rec(idx).po_number
                    from   rcv_receiving_sub_ledger  rcvsla
                          ,po_distributions_all      pod
                          ,po_line_locations_all     poloc
                          ,po_lines_all              pol
                          ,mtl_system_items          msi
                          ,org_organization_definitions org
                    where  1 =1
                      and  rcvsla.rcv_sub_ledger_id   =COGS_Rec(idx).source_dist_id_num_1
                      and  rcvsla.reference1          ='PO'
                      and  pod.po_distribution_id(+)  =to_number(rcvsla.reference3)
                      and  poloc.line_location_id(+)  =pod.line_location_id
                      and  pol.po_line_id(+)          =poloc.po_line_id
                      and  msi.inventory_item_id(+)   =pol.item_id
                      and  msi.organization_id        =poloc.ship_to_organization_id
                      and  org.organization_id(+)     =poloc.ship_to_organization_id;
                   --
                  Exception
                   When No_Data_Found then
                    Null;
                   When Others then
                    print_log('Other errors in fetching rcv_receiving_sub_ledger data for rcv_sub_ledger_id ='||COGS_Rec(idx).source_dist_id_num_1);
                    print_log('Other errors in fetching rcv_receiving_sub_ledger data, message ='||sqlerrm);
                  End;
              ElsIf nvl(COGS_Rec(idx).source_distribution_type, 'NA') ='RA_CUST_TRX_LINE_GL_DIST_ALL' Then
                  Begin
                    select nvl
                            (
                              (
                                select segment1
                                from   mtl_system_items
                                where  1 =1
                                  and  inventory_item_id =invl.inventory_item_id
                                  and  organization_id   =invl.warehouse_id
                              )
                             ,Null
                            )
                           ,invl.inventory_item_id
                           ,org.organization_id
                           ,trunc(inv.trx_date)
                           ,org.organization_code
                           ,abs(nvl
                                 (
                                   invl.quantity_invoiced
                                  ,invl.quantity_credited
                                 )
                             )                               quantity
                           ,invl.interface_line_attribute1
                           ,oeol.line_number
                           ,invl.line_number
                    into   COGS_Rec(idx).item_number
                          ,COGS_Rec(idx).inventory_item_id
                          ,COGS_Rec(idx).organization_id
                          ,COGS_Rec(idx).transaction_date
                          ,COGS_Rec(idx).branch
                          ,COGS_Rec(idx).quantity
                          ,COGS_Rec(idx).order_number
                          ,COGS_Rec(idx).order_line_num
                          ,COGS_Rec(idx).invoice_line_num
                    from   ra_cust_trx_line_gl_dist_all argl
                          ,ra_customer_trx_lines_all    invl
                          ,ra_customer_trx_all          inv
                          ,org_organization_definitions org
                          ,oe_order_lines_all           oeol
                    where  1 =1
                      and  argl.cust_trx_line_gl_dist_id =COGS_Rec(idx).source_dist_id_num_1
                      and  invl.customer_trx_line_id     =argl.customer_trx_line_id
                      and  inv.customer_trx_id           =invl.customer_trx_id
                      and  org.organization_id(+)        =invl.warehouse_id
                      and  to_char(oeol.line_id(+))      =invl.interface_line_attribute6;
                   --
                  Exception
                   When No_Data_Found then
                    Null;
                   When Others then
                    print_log('Other errors in fetching ra_cust_trx_line_gl_dist_all data for cust_trx_line_gl_dist_id ='||COGS_Rec(idx).source_dist_id_num_1);
                    print_log('Other errors in fetching ra_cust_trx_line_gl_dist_all data, message ='||sqlerrm);
                  End;
              ElsIf nvl(COGS_Rec(idx).source_distribution_type, 'NA') ='AP_INV_DIST' Then
                  Begin
                    select pol.item_id
                          ,poloc.ship_to_organization_id
                          ,Null
                          ,msi.segment1
                          ,org.organization_code
                          ,trunc(apid.accounting_date)
                          ,apid.quantity_invoiced
                          ,poh.segment1
                    into   COGS_Rec(idx).inventory_item_id
                          ,COGS_Rec(idx).organization_id
                          ,COGS_Rec(idx).inv_transaction_id
                          ,COGS_Rec(idx).item_number
                          ,COGS_Rec(idx).branch
                          ,COGS_Rec(idx).transaction_date
                          ,COGS_Rec(idx).quantity
                          ,COGS_Rec(idx).po_number
                    from   ap_invoice_distributions_all apid
                          ,po_distributions_all      pod
                          ,po_line_locations_all     poloc
                          ,po_lines_all              pol
                          ,po_headers_all            poh
                          ,mtl_system_items          msi
                          ,org_organization_definitions org
                    where  1 =1
                      and  apid.invoice_distribution_id =COGS_Rec(idx).source_dist_id_num_1
                      and  pod.po_distribution_id(+)  =apid.po_distribution_id
                      and  poloc.line_location_id(+)  =pod.line_location_id
                      and  pol.po_line_id(+)          =poloc.po_line_id
                      and  poh.po_header_id(+)        =pol.po_header_id
                      and  msi.inventory_item_id(+)   =pol.item_id
                      and  msi.organization_id(+)     =222
                      and  org.organization_id(+)     =poloc.ship_to_organization_id;
                   --
                  Exception
                   When No_Data_Found then
                    Null;
                   When Others then
                    print_log('Other errors in fetching AP_INVOICE_DISTRIBUTIONS_ALL data for invoice_distribution_id ='||COGS_Rec(idx).source_dist_id_num_1);
                    print_log('Other errors in fetching rcv_receiving_sub_ledger data, message ='||sqlerrm);
                  End;
              ElsIf nvl(COGS_Rec(idx).source_distribution_type, 'NA') ='DEPRN' Then
               Null;
              Else
               if nvl(COGS_Rec(idx).source_distribution_type, 'NA') NOT IN ('NA', 'MTL_TRANSACTION_ACCOUNTS', 'RCV_RECEIVING_SUB_LEDGER'
                                                                        ,'RA_CUST_TRX_LINE_GL_DIST_ALL' ,'AP_INV_DIST' ,'DEPRN'
                                                                       ) then
                print_log('Unhandled COGS source distribution type found, value ='||COGS_Rec(idx).source_distribution_type);
               else
                Null;
               end if;
              End If;
             --
             --
            end loop;
           --
            begin
              savepoint start_here;
              forall idx102 in COGS_Rec.first .. COGS_Rec.last
              insert into xxcus.xxcus_match_gl_cogs_b values COGS_Rec(idx102);
            exception
             when others then
              rollback to start_here;
              print_log('Fetch count ='||n_fetch_count||', Other errors while bulk insert of SLA COGS entries for the period ='||p_period_name);
              print_log('Fetch count ='||n_fetch_count||', Other errors ='||sqlerrm);
            end;
           --
           end if;
          --
         end loop; --loop for cursor sla_cogs_trx
         --
         close sla_cogs_trx;
        --
        exception
         when no_data_found then
          print_log('No data returned from SLA rev accounts for location ='||p_location||', period ='||p_period_name);
         when others then
          print_log('Other errors while opening cursor sla_rev_trx, for location ='||p_location||', period ='||p_period_name);
          print_log('Other errors while opening cursor sla_rev_trx, message ='||sqlerrm);
        end;
       --
       print_log ('');
       print_log('After Close cursor sla_cogs_trx');
       --
       print_log ('');
       print_log('Begin Pivot');
       --
        begin
         pivot (p_location);
        exception
         when others then
          print_log('Inside Main, Other errors when calling routine pivot for location ='||p_location||', period ='||p_period_name);
          print_log('Inside Main, Other errors when calling routine pivot, message ='||sqlerrm);
        end;
       --
       print_log ('');
       print_log('End Pivot');
       --
       print_log ('');
       print_log('Begin unload_branch_summary_data for branch ='||p_location||', Period ='||p_period_name);
       --
       begin
        --
        unload_branch_summary_data;
        --
       exception
        when others then
         print_log('Outer block in calling unload_branch_summary_data within main, GL location ='||p_location||', Period ='||p_period_name);
         print_log('Outer block in calling unload_branch_summary_data within main, message ='||sqlerrm);
         print_log('Outer block in calling unload_branch_summary_data within main, message ='||sqlerrm);
         rollback;
       end;
       --
       print_log ('');
       print_log('End unload_branch_summary_data for branch ='||p_location||', Period ='||p_period_name);
       print_log ('');
       print_log('Begin unload_branch_sales_data for branch ='||p_location||', Period ='||p_period_name);
       --
       begin
        --
        unload_branch_sales_data;
        --
       exception
        when others then
         print_log('Outer block in calling unload_branch_sales_data within main, GL location ='||p_location||', Period ='||p_period_name);
         print_log('Outer block in calling unload_branch_sales_data within main, message ='||sqlerrm);
         print_log('Outer block in calling unload_branch_sales_data within main, message ='||sqlerrm);
         rollback;
       end;
       --
       print_log ('');
       print_log('End unload_branch_sales_data for branch ='||p_location||', Period ='||p_period_name);
       print_log ('');
       print_log('Begin unload_branch_COGS_data for branch ='||p_location||', Period ='||p_period_name);
       --
       begin
        --
        unload_branch_COGS_data;
        --
       exception
        when others then
         print_log('Outer block in calling unload_branch_COGS_data within main, GL location ='||p_location||', Period ='||p_period_name);
         print_log('Outer block in calling unload_branch_COGS_data within main, message ='||sqlerrm);
         print_log('Outer block in calling unload_branch_COGS_data within main, message ='||sqlerrm);
         rollback;
       end;
       --
       print_log ('');
       print_log('End unload_branch_COGS_data for branch ='||p_location||', Period ='||p_period_name);
       --
  exception
   --
   when others then
    --
    fnd_file.put_line(fnd_file.log, 'Error in caller xxcus_match_gl_rev_cogs.main ='||sqlerrm);
    rollback;
    l_err_msg :='Error in caller xxcus_match_gl_rev_cogs.main';
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
    --
  end main;
  --
/**************************************************************************
 *
 * PROCEDURE: create_summary_data_files
 * DESCRIPTION: To create grand summary and summary level dat for Sales/COGS  @ all branches
 * PARAMETERS
 * ==========
 * NAME              TYPE      DESCRIPTION
.* ----------------- ------    ---------------------------------------------
 * p_req_id          IN       NUMBER
 * RETURN VALUE:
 * CALLED BY: wrapper
 * 1.4      TMS#20180815-00112     08/20/2018    Pattabhi Avula      SALES and COGS files FTP to P Drive Fix -- Provided file permissions
 *************************************************************************/
  --
  PROCEDURE create_summary_data_files (p_req_id IN NUMBER) IS
  --
  CURSOR grand_data IS
   SELECT grand_summary
   FROM   xxcus.xxcus_ar_cogs_grand_totals
   ORDER BY SUBSTR(grand_summary, 1, 6);
  --
   v_grand_summary_header VARCHAR2(2000) :='GL LOCATION'||'|'||
       'TOTAL SALES'||'|'||
       'TOTAL COGS'||'|'||
       'COGS_SALES_DIFF'||'|'||
       'COGS_PERCENT'||'|';
  --
  C_MAXLINE              CONSTANT PLS_INTEGER :=32767;
  file_id                utl_file.file_type;
  v_buffer               VARCHAR2(32767);
  v_grand_summary_file   VARCHAR2(150);
  v_summary_file         VARCHAR2(150);
  v_sales_detail_file    VARCHAR2(150);
  v_COGS_detail_file     VARCHAR2(150);
  v_file_path            VARCHAR2(150);
  p_limit                NUMBER :=5000;
  v_child_requests       VARCHAR2(3);
  ln_request_id          NUMBER :=0;
  v_email                fnd_user.email_address%type :=Null;
  v_cp_long_name         fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;
  -- Ver#1.4 > Start
  v_command             VARCHAR2(4000):=NULL;  -- Cogs Summary file permission
  v_command_grnd        VARCHAR2(4000):=NULL;  -- Grand Summary file permission  
  v_result_grnd         VARCHAR2(4000):=NULL;  -- Grand Summary file permission result
  v_result              VARCHAR2(4000):=NULL; -- Congs summary file permission result
  -- Ver#1.4 < End
  v_summary_header VARCHAR2(2000) :=
       'GL LOCATION'||'|'||
       'ORDER NUMBER'||'|'||
       'TOTAL SALES'||'|'||
       'TOTAL COGS'||'|'||
       'COGS_SALES_DIFF'||'|'||
       'COGS_PERCENT'||'|';
  --
   CURSOR summary_tbl IS
    SELECT gl_location ||'|'||
           ar_sales_cogs_summary summary_data
    FROM   xxcus.xxcus_sales_cogs_summary_all
    ORDER BY gl_location ASC;
  --
  begin
  --
    select '/xx_iface/'||lower(name)||'/outbound'
    into   v_file_path
    from   v$database;
  --
  -- ========================================================================================
    --Name the grand summary header file
    v_grand_summary_file :='HDS_SALES_COGS_GRAND_SUMMARY_'||p_req_id||'.txt';	

    --Open the file pointer for the grand summary header file
    file_id :=utl_file.fopen(v_file_path, v_grand_summary_file, 'W', C_MAXLINE);	
	
	
    --Write grand summary header data to the grand summary data file
    utl_file.put_line(file_id, v_grand_summary_header);

    --
    for rec in grand_data loop
     utl_file.put_line(file_id, rec.grand_summary);
    end loop;

    --Close the file pointer for the grand summary header file
     utl_file.fclose(file_id);
     print_log('Grand summary data file created.');
	 
	 -- Ver#1.4 > Start	
	    v_command_grnd :='chmod 777'
                         ||' '
                         ||v_file_path
                         ||'/'
                         ||v_grand_summary_file; -- Grand summary header file
						 
                  print_log('File Permission given for Gran Summary file:');
                  print_log('============');
                  print_log(v_command_grnd);
               BEGIN
                  SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command_grnd)
                    INTO   v_result_grnd
                    FROM   dual;
                  print_log('v_grand_result '||v_result_grnd);                 
               EXCEPTION
                WHEN OTHERS THEN                
                 print_log('Failed to provide the permission on '||v_grand_summary_file);
               END;
    -- Ver#1.4 < End

  -- ========================================================================================

    --Name the grand summary header file
    v_summary_file :='HDS_SALES_COGS_SUMMARY_'||p_req_id||'.txt';

    --Open the file pointer for the grand summary header file
    file_id :=utl_file.fopen(v_file_path, v_summary_file, 'W', C_MAXLINE);
	
    --Write grand summary header data to the grand summary data file
    utl_file.put_line(file_id, v_summary_header);

    --
    for rec in summary_tbl loop
      utl_file.put_line(file_id, rec.summary_data);
    end loop;

    --Close the file pointer for the grand summary header file
    utl_file.fclose(file_id);
    print_log('Summary data file created.');
	
	-- Ver#1.4  > Start
	    v_command :='chmod 777'
                         ||' '
                         ||v_file_path
                         ||'/'
                         ||v_summary_file; -- Summary header file
                  --

                  print_log('File Permission given for Summary file:');
                  print_log('============');
                  print_log(v_command);
              BEGIN
                SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
                  INTO   v_result
                  FROM   dual;
                  print_log('v_result '||v_result);
                  --
               EXCEPTION
                WHEN OTHERS THEN                
                 print_log('Failed to provide the permission on '||v_summary_file);
               END;
    --  Ver#1.4  < End
  --
    Begin
        select a.user_concurrent_program_name
              ,c.email_address
        into   v_cp_long_name
              ,v_email
        from fnd_amp_requests_v a
            ,fnd_concurrent_programs d
            ,fnd_user c
        where 1 =1
          and a.request_id =p_req_id
          and a.concurrent_program_id =d.concurrent_program_id
          and a.requested_by =c.user_id;

    Exception
     When Others Then
       Null;
    End;
  --
    Begin
      file_id  :=utl_file.fopen(v_file_path, 'zip_file_extract_'||p_req_id||'.txt', 'w');
      utl_file.put_line(file_id, v_file_path||'/'||v_grand_summary_file);
      utl_file.put_line(file_id, v_file_path||'/'||v_summary_file);
      utl_file.fclose(file_id);
      v_child_requests :='OK';
    Exception
     When Others Then
      v_child_requests :='NA';
    End;
  --
    If v_child_requests <>'NA' Then
    ln_request_id :=fnd_request.submit_request
          (
           application      =>'XXCUS',
           program          =>'XXCUS_PEREND_POSTPROCESSOR',
           description      =>'',
           start_time       =>'',
           sub_request      =>TRUE,
           argument1        =>p_req_id,
           argument2        =>v_file_path||'/zip_file_extract_'||p_req_id||'.txt',
           argument3        =>v_cp_long_name,
           argument4        =>v_email,
           argument5        =>'WC_AR_Sales_COGS_Summary_Report_'||p_req_id||'.zip',
           argument6        =>'WC AR Sales and COGS Summary Report -Status Update'
          );
     --commit work;
      if ln_request_id >0 then
       print_log('Successfully submitted report to email the summary report, request_id ='||ln_request_id);
      else
         print_log('Failed to submit XXHDS Period End Post Processor Program');
      end if;
    Else --v_child_requests ='NA'
      Null;
    End If;
  --
  exception
   when others then
    print_log('@create_summary_data_files, Outer block, '||sqlerrm);
  end create_summary_data_files;
  --
/**************************************************************************
 *
 * PROCEDURE: wrapper
 * DESCRIPTION:  HDS AR Sales and COGS Analysis -Master
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_ledger_id       IN       Primary ledger id for Whitecap
 * p_period_name     IN       Fiscal period id
 * p_product         IN      GL Product segment
 * p_location        IN      GL Location segment
 * p_rev_account     IN      GL Sales Account segment
 * p_cogs_account    IN      GL COGS Account segment
 * retcode           OUT     standard concurrent program return variable
 * errbuf            OUT     standard concurrent program return variable
 * RETURN VALUE: Mentioned above
 * CALLED BY: HDS AR Sales and COGS Analysis -Master
 *************************************************************************/
  --
  PROCEDURE wrapper (
    retcode               out number
   ,errbuf                out varchar2
   ,p_ledger_id           in  number
   ,p_period_name         in  varchar2
   ,p_product             in  varchar2
   ,p_location            in  varchar2  Default Null
   ,p_rev_account         in  varchar2  Default Null
   ,p_cogs_account        in  varchar2  Default Null
  ) IS
   --
   CURSOR gl_locations IS
    SELECT org.organization_code branch_code
          ,org.organization_name branch_name
          ,whse_gl_loc.entrp_loc oracle_gl_location
    FROM   org_organization_definitions   org
          ,xxcus.xxcus_location_code_tbl  whse_gl_loc
    WHERE 1 = 1
      AND org.operating_unit     =mo_global.get_current_org_id
      AND whse_gl_loc.entrp_loc  =nvl(p_location, whse_gl_loc.entrp_loc)
      AND whse_gl_loc.lob_branch =org.organization_code
      --AND whse_gl_loc.entrp_loc in ('BW001', 'BW002') --('BW193', 'BW194', 'BW195', 'BW245', 'BW246', 'BW247')
      AND whse_gl_loc.entrp_loc like 'BW%'
      AND whse_gl_loc.inactive ='N'
      AND org.disable_date IS NULL
      AND org.organization_code NOT IN ('WCC', 'MST') --WC Corporate Organization, WC Item Master
    ORDER BY org.organization_code ASC;
  --
    TYPE gl_loc_tbl IS TABLE OF gl_locations%ROWTYPE INDEX BY BINARY_INTEGER;
    gl_loc_rec gl_loc_tbl;
  --
   v_location       VARCHAR2(2000);
   lc_request_data  VARCHAR2(20) :='';
   ln_request_id    NUMBER :=0;
   --
   n_req_id         NUMBER :=0;
   l_org_id         NUMBER :=0;
   v_cp_name        fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;
  --
  -- =========================================================================================
  l_module        VARCHAR2(24) :='HDS GL -WhiteCap';
  l_err_msg       CLOB;
  l_sec           VARCHAR2(255);
  l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_match_gl_rev_cogs.wrapper';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  --
  begin
  --
   n_req_id        :=fnd_global.conc_request_id;
   l_org_id        :=mo_global.get_current_org_id;
   lc_request_data :=fnd_conc_global.request_data;
  --
   If lc_request_data is null then
    --
     lc_request_data :='1';
    --
     EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_SALES_COGS_SUMMARY_ALL';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_AR_SALES_SUMMARY_ALL';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_AR_COGS_SUMMARY_ALL';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_AR_COGS_GRAND_TOTALS';
    --
     Begin
      open gl_locations;
      fetch gl_locations bulk collect into gl_loc_rec;
      close gl_locations;
     Exception
      When program_error then
       v_location :='103';
     End;
    --
     if gl_loc_rec.count >0 then
      --
      begin
        select cp_tl.user_concurrent_program_name
        into   v_cp_name
        from   fnd_concurrent_programs     cp
              ,fnd_concurrent_programs_tl  cp_tl
        where  1 =1
          and  cp.concurrent_program_name   ='XXCUS_MATCH_GL_REV_COGS_MAIN'
          and  cp_tl.concurrent_program_id  =cp.concurrent_program_id;
      exception
       when others then
        v_cp_name :='NA';
      end;
      --
       for idx in gl_loc_rec.first .. gl_loc_rec.last loop
        --
        ln_request_id :=fnd_request.submit_request
              (
                application      =>'XXCUS'
               ,program          =>'XXCUS_MATCH_GL_REV_COGS_MAIN'
               ,description      =>''
               ,start_time       =>''
               ,sub_request      =>TRUE
               ,argument1        =>p_ledger_id
               ,argument2        =>p_period_name
               ,argument3        =>p_product
               ,argument4        =>gl_loc_rec(idx).oracle_gl_location
               ,argument5        =>p_rev_account
               ,argument6        =>p_cogs_account
              );
        --
          if ln_request_id >0 then
            fnd_file.put_line(fnd_file.log, 'Submitted '||v_cp_name||' for branch ='
                                             ||gl_loc_rec(idx).branch_code
                                             ||', GL location ='
                                             ||gl_loc_rec(idx).oracle_gl_location
                                             ||', Request Id ='
                                             ||ln_request_id
                                            );
          else
            fnd_file.put_line(fnd_file.log, 'Failed to submit '||v_cp_name||' for branch ='
                                             ||gl_loc_rec(idx).branch_code
                                             ||', GL location ='
                                             ||gl_loc_rec(idx).oracle_gl_location
                                            );
          end if;

         fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);

         lc_request_data :=to_char(to_number(lc_request_data)+1);
        --
       end loop; --idx in gl_loc_rec.first .. gl_loc_rec.last
      --
     end if; --gl_loc_rec >0
    --
   Else
    --
     create_summary_data_files (n_req_id);
    --

    --
      retcode :=0;
      errbuf :='Child request completed. Exit HDS Kickoff AR Revenue COGS Analysis -Master';
      fnd_file.put_line(fnd_file.log,errbuf);
    --
   End If;
  --
  exception
  --
   when program_error then
    print_log('Location =>'||v_location||', program error, message ='||sqlerrm);
  --
   when others then
    print_log('Location =>'||v_location||', outer block other errors');
    print_log('Outer block, other errors in calling wrapper, message ='||sqlerrm);
    rollback;
    l_err_msg :='Location =>'||v_location||', outer block other errors';
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
  --
  end wrapper;
 --
/**************************************************************************
 *
 * PROCEDURE: detail
 * DESCRIPTION: HDS AR Sales and COGS Analysis -Branch Detail
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_ledger_id       IN       Primary ledger id for Whitecap
 * p_period_name     IN       Fiscal period id
 * p_product         IN      GL Product segment
 * p_location        IN      GL Location segment
 * p_rev_account     IN      GL Sales Account segment
 * p_cogs_account    IN      GL COGS Account segment
 * retcode           OUT     standard concurrent program return variable
 * errbuf            OUT     standard concurrent program return variable
 * RETURN VALUE: Mentioned above
 * CALLED BY: HDS AR Sales and COGS Analysis -Branch Detail
 *************************************************************************/
  --
  PROCEDURE detail (
    retcode       out number
   ,errbuf        out varchar2
   ,p_location    in  varchar2  Default Null
   ,p_sales_file  in  varchar2
   ,p_cogs_file   in  varchar2
   ,p_file_path   in  varchar2
   ) IS
  --
  C_MAXLINE              CONSTANT PLS_INTEGER :=32767;
  file_id                utl_file.file_type;
  v_sales_detail_file    VARCHAR2(150);
  --
  v_COGS_detail_file     VARCHAR2(150);
  v_file_path            VARCHAR2(150);
  --
  p_limit                NUMBER :=5000;
  v_child_requests       VARCHAR2(3);
  ln_request_id          NUMBER :=0;
  --
  p_req_id               NUMBER :=fnd_global.conc_request_id;
  v_email                fnd_user.email_address%type :=Null;
  v_cp_long_name         fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;
  --
  -- =========================================================================================
  l_module        VARCHAR2(24) :='HDS GL -WhiteCap';
  l_err_msg       CLOB;
  l_sec           VARCHAR2(255);
  l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_match_gl_rev_cogs.detail';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  --
  --
   CURSOR sales IS
    SELECT
      'BRANCH'||'|'||
      'GL_SOURCE'||'|'||
      'GL_CATEGORY'||'|'||
      'PERIOD'||'|'||
      'GL_PRODUCT'||'|'||
      'GL_LOCATION'||'|'||
      'GL_ACCOUNT'||'|'||
      'ACCOUNTING_DATE'||'|'||
      'ACCOUNTING_CLASS_CODE'||'|'||
      'SLA_EVENT_TYPE'||'|'||
      'JOURNAL_BATCH_NAME'||'|'||
      'JOURNAL_HEADER_NAME'||'|'||
      'JOURNAL_LINE_NUM'||'|'||
      'INVOICE_NUMBER'||'|'||
      'INVOICE_SOURCE'||'|'||
      'INVOICE_CREATION_DATE'||'|'||
      'INVOICE_DATE'||'|'||
      'ORDER_NUMBER'||'|'||
      'ORDER_LINE_NUM'||'|'||
      'INVOICE_LINE_NUM'||'|'||
      'QUANTITY'||'|'||
      'ITEM_NUMBER'||'|'||
      'AMOUNT'||'|' rev_detail
    FROM DUAL
    UNION ALL
    select ar_sales_detail rev_detail
    from   xxcus.xxcus_ar_sales_summary_all
    where  1 =1
      and  gl_location =p_location;
  --
   type sales_tbl is table of sales%rowtype index by binary_integer;
   sales_rec sales_tbl;
  --
   CURSOR COGS IS
    SELECT
      'BRANCH'||'|'||
      'GL_SOURCE'||'|'||
      'GL_CATEGORY'||'|'||
      'PERIOD'||'|'||
      'GL_PRODUCT'||'|'||
      'GL_LOCATION'||'|'||
      'GL_ACCOUNT'||'|'||
      'ACCOUNTING_DATE'||'|'||
      'ACCOUNTING_CLASS_CODE'||'|'||
      'SLA_EVENT_TYPE'||'|'||
      'JOURNAL_BATCH_NAME'||'|'||
      'JOURNAL_HEADER_NAME'||'|'||
      'JOURNAL_LINE_NUM'||'|'||
      'CREATION_DATE'||'|'||
      'TRANSACTION_DATE'||'|'||
      'PO_NUMBER'||'|'||
      'ORDER_NUMBER'||'|'||
      'ORDER_LINE_NUM'||'|'||
      'INVOICE_LINE_NUM'||'|'||
      'QUANTITY'||'|'||
      'ITEM_NUMBER'||'|'||
      'AMOUNT'||'|' COGS_detail
    FROM dual
    UNION ALL
    SELECT ar_cogs_detail COGS_detail
    FROM   xxcus.xxcus_ar_cogs_summary_all
    where  1 =1
      and  gl_location =p_location;
  --
   type COGS_tbl is table of COGS%rowtype index by binary_integer;
   COGS_rec COGS_tbl;
  --
  BEGIN
  -- ========================================================================================
    --select  --'/xx_iface/'||lower(name)||'/outbound'
    --into v_file_path
    --from v$database;
    v_file_path :=p_file_path;
        --
        --Name the sales detail file
        v_sales_detail_file :=p_sales_file;

        --Open the file pointer for the sales detail file
        file_id :=utl_file.fopen(v_file_path, v_sales_detail_file, 'W', C_MAXLINE);

        --Write data to the sales detail file
        open sales;
        --
        loop
         --
         fetch sales bulk collect into sales_rec limit p_limit;
         exit when sales_rec.count =0;
         --
         if sales_rec.count >0 then
          for idx in sales_rec.first .. sales_rec.last loop
           utl_file.put_line(file_id, sales_rec(idx).rev_detail);
          end loop;
         end if;
        end loop;
        --
        close sales;
        --
        --Close the file pointer for the sales detail data file
        utl_file.fclose(file_id);
        print_log('Branch Sales detail data file '||v_sales_detail_file||' created.');
        --
        --Name the COGS detail file
        v_COGS_detail_file :=p_cogs_file;

        --Open the file pointer for the COGS detail data file
        file_id :=utl_file.fopen(v_file_path, v_COGS_detail_file, 'W', C_MAXLINE);

        --Write data to the COGS detail data file
        open COGS;
        --
        loop
         --
         fetch COGS bulk collect into COGS_rec limit p_limit;
         exit when COGS_rec.count =0;
         --
         if COGS_rec.count >0 then
          for idx in COGS_rec.first .. COGS_rec.last loop
           utl_file.put_line(file_id, COGS_rec(idx).cogs_detail);
          end loop;
         end if;
        end loop;
        --
        close COGS;
        --
        --Close the file pointer for the COGS detail data file
        utl_file.fclose(file_id);
        print_log('Branch COGS detail data file '||v_COGS_detail_file||' created.');
        --
  --
  EXCEPTION
   WHEN OTHERS THEN
    print_log('@generate detail file routine, outer block, message ='||sqlerrm);
    l_err_msg := '@generate detail file routine, outer block';
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
  END detail;
  --
/**************************************************************************
 *
 * PROCEDURE: detail_wrapper
 * DESCRIPTION: HDS AR Sales COGS Detail Wrapper
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_location        IN       GL Location segment
 * p_ledger_id       IN       Primary ledger id for Whitecap
 * p_period_name     IN       Fiscal period id
 * p_file_path       IN       Folder where detail extracts will be copied. Like /xx_iface/ebizprd/outbound
 * retcode           OUT     standard concurrent program return variable
 * errbuf            OUT     standard concurrent program return variable
 * RETURN VALUE: Mentioned above
 * CALLED BY: HDS AR Sales COGS Detail Wrapper
 *
 * Version  DATE         AUTHOR(S)           DESCRIPTION
 * -------- --------    -----------          ------------------------------------
 *1.4       08/20/2018   Pattabhi Avula      TMS#20180815-00112 SALES and COGS files FTP to P Drive Fix -- Provided file permissions
 *************************************************************************/
  --
  PROCEDURE detail_wrapper (
    retcode               out number
   ,errbuf                out varchar2
   ,p_location            in  varchar2  Default Null
   ,p_ledger_id           in  number
   ,p_period              in  varchar2
   ,p_file_path           in  varchar2
  ) IS
   --
   CURSOR wrapper_req_status (p_req_id IN NUMBER) IS
    select trim(request_id)           parent_req_id
          ,trim(responsibility_name)  resp_name
          ,trim(program)              cp_name
          ,trim(phase)||'-'||trim(status)   cp_status
          ,trim(user_name)            user_name
          ,trim((
            select count(1)
            from   fnd_concurrent_requests
            where  1 =1
              and  parent_request_id =p_req_id
              and  concurrent_program_id =
                    (
                      select concurrent_program_id
                      from   fnd_concurrent_programs
                      where  1 =1
                        and  concurrent_program_name ='XXCUS_REV_COGS_DETAIL_FILE'
                    )
           ))                   total_branches
          ,trim(to_char
            (
              actual_start_date
             ,'mm/dd/yyyy hh24:mi:ss'
            ))                         start_date_time
          ,trim(to_char
            (
              actual_completion_date
             ,'mm/dd/yyyy hh24:mi:ss'
            ))                         end_date_time
    from   fnd_amp_requests_v
    where  1 =1
      and request_id =p_req_id;
   --
    c_req_status wrapper_req_status%rowtype :=Null;
   --
   CURSOR wrapper_child_reqs (p_req_id IN NUMBER) IS
    select (
                select rpad(argument1, 8, ' ')
                from   fnd_concurrent_requests
                where  1 =1
                  and  request_id =a.request_id
               )
             ||rpad(to_char(a.request_id), 12, ' ')
             ||a.phase||' -'||a.status  branch_detail
    from   fnd_amp_requests_v a
    where  1 =1
      and  parent_request_id =p_req_id;
   --
   type c_wrapper_child_reqs is table of wrapper_child_reqs%rowtype index by binary_integer;
   c_child_reqs c_wrapper_child_reqs;
   --
   CURSOR gl_locations IS
    SELECT org.organization_code branch_code
          ,org.organization_name branch_name
          ,whse_gl_loc.entrp_loc oracle_gl_location
    FROM   org_organization_definitions   org
          ,xxcus.xxcus_location_code_tbl  whse_gl_loc
    WHERE 1 = 1
      AND org.operating_unit     =mo_global.get_current_org_id
      AND whse_gl_loc.entrp_loc  =nvl(p_location, whse_gl_loc.entrp_loc)
      AND whse_gl_loc.lob_branch =org.organization_code
      --AND whse_gl_loc.entrp_loc IN ('BW001', 'BW002', 'BW003', 'BW193', 'BW194', 'BW195', 'BW245', 'BW246', 'BW247')
      AND whse_gl_loc.entrp_loc like 'BW%'
      AND whse_gl_loc.inactive ='N'
      AND org.disable_date IS NULL
      AND org.organization_code NOT IN ('WCC', 'MST') --WC Corporate Organization, WC Item Master
    ORDER BY whse_gl_loc.entrp_loc ASC;
   --
    TYPE gl_loc_tbl IS TABLE OF gl_locations%ROWTYPE INDEX BY BINARY_INTEGER;
    gl_loc_rec gl_loc_tbl;
   --
   v_child_requests VARCHAR2(30) :='OK';
   v_location       VARCHAR2(2000);
   lc_request_data  VARCHAR2(20) :='';
   ln_request_id    NUMBER :=0;
   n_req_id         NUMBER :=0;
   file_id          utl_file.file_type;
   --
   l_org_id         NUMBER :=0;
   v_zip_file       VARCHAR2(80) :=Null;
   v_file           VARCHAR2(80) :=Null;
   v_sales_file     VARCHAR2(80) :=Null;
   v_cogs_file      VARCHAR2(80) :=Null;
   --
   v_cp_name        fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;
   v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;
   n_user_id        number;
   --
   b_template_added BOOLEAN;
   b_first_child_request BOOLEAN :=TRUE;
   v_command        varchar2(4000);
   v_result         varchar2(4000);
   v_email          fnd_user.email_address%type :=Null;
   --
   v_detail_wrapper_file varchar2(40) :='HDS_SALES_COGS_DTL_EMAIL_CONTENT.txt';
   v_final_zip_file      varchar2(80) :='HDS_SALES_COGS_DETAIL_'||replace(p_period, '-', '')||'.zip';
   --
   C_MAXLINE             CONSTANT PLS_INTEGER :=32767;
   v_crlf                VARCHAR2(2) := chr(13); --||chr(10);
   --
   v_host         VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
   v_hostport     VARCHAR2(20)  := '25';
   v_subject      VARCHAR2(2000);
   v_email_body   VARCHAR2(4000) :='The HDS WC AR Sales and COGS Detail zip is attached. Please DO NOT reply to this message. Thanks.';
   --
   --
   -- =========================================================================================
    l_module        VARCHAR2(24) :='HDS GL -WhiteCap';
    l_err_msg       CLOB;
    l_sec           VARCHAR2(255);
    l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_match_gl_rev_cogs.detail_wrapper';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   --
  begin
  --
   n_req_id        :=fnd_global.conc_request_id;
   n_user_id       :=fnd_global.user_id;
   l_org_id        :=mo_global.get_current_org_id;
   lc_request_data :=fnd_conc_global.request_data;
  --
   print_log('lc_request_data ='||lc_request_data);
  --
   If lc_request_data is null then
    --
     lc_request_data :='1';
    --
     Begin
      open gl_locations;
      fetch gl_locations bulk collect into gl_loc_rec;
      close gl_locations;
      --
      print_log('gl_loc_rec.count ='||gl_loc_rec.count);
      --
     Exception
      When program_error then
       v_location :='103';
     End;
    --
     if gl_loc_rec.count >0 then
      --
      begin
        select cp_tl.user_concurrent_program_name
        into   v_cp_name
        from   fnd_concurrent_programs     cp
              ,fnd_concurrent_programs_tl  cp_tl
        where  1 =1
          and  cp.concurrent_program_name   ='XXCUS_REV_COGS_DETAIL_FILE'
          and  cp_tl.concurrent_program_id  =cp.concurrent_program_id;
      exception
       when others then
        v_cp_name :='NA';
      end;
      --
           for idx in gl_loc_rec.first .. gl_loc_rec.last loop
            --
            v_sales_file :=gl_loc_rec(idx).oracle_gl_location||'_SALES_DETAIL_'||replace(p_period, '-', '')||'.txt';
            v_cogs_file  :=gl_loc_rec(idx).oracle_gl_location||'_COGS_DETAIL_'||replace(p_period, '-', '')||'.txt';
            --
              ln_request_id :=fnd_request.submit_request
                  (
                    application      =>'XXCUS'
                   ,program          =>'XXCUS_REV_COGS_DETAIL_FILE'
                   ,description      =>''
                   ,start_time       =>''
                   ,sub_request      =>TRUE
                   ,argument1        =>gl_loc_rec(idx).oracle_gl_location
                   ,argument2        =>v_sales_file
                   ,argument3        =>v_cogs_file
                   ,argument4        =>p_file_path
                  );
                      --
                      if ln_request_id >0 then
                        --
                        fnd_file.put_line(fnd_file.log, 'Submitted '||v_cp_name||' for branch ='
                                                         ||gl_loc_rec(idx).branch_code
                                                         ||', GL location ='
                                                         ||gl_loc_rec(idx).oracle_gl_location
                                                         ||', Request Id ='
                                                         ||ln_request_id
                                                        );
                      else
                        fnd_file.put_line(fnd_file.log, 'Failed to submit '||v_cp_name||' for branch ='
                                                         ||gl_loc_rec(idx).branch_code
                                                         ||', GL location ='
                                                         ||gl_loc_rec(idx).oracle_gl_location
                                                        );
                      end if;
                      --
            --
             fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
            --
             lc_request_data :=to_char(to_number(lc_request_data)+1);
            --
           end loop; --idx in gl_loc_rec.first .. gl_loc_rec.last
      --
     end if; --gl_loc_rec >0
    --
   Else
    --
    --
        Begin
            select a.user_concurrent_program_name
                  ,c.email_address
            into   v_cp_long_name
                  ,v_email
            from fnd_amp_requests_v a
                ,fnd_concurrent_programs d
                ,fnd_user c
            where 1 =1
              and a.request_id =n_req_id
              and a.concurrent_program_id =d.concurrent_program_id
              and a.requested_by =c.user_id;

        Exception
         When Others Then
           Null;
        End;
        --
        Begin
         --
         --v_zip_file :='HDS_SALES_COGS_DETAIL_'||replace(p_period, '-', '')||'.zip';
         v_zip_file :='tmp_rev_cogs_dtl_'||n_req_id||'.zip';
         --
         -- Let's create the zip file with all the individual files compressed
         -- the below package is a plsql routine sourced as a java class and runs all UNIX OS commands
         --
          for idx1 in
                   (
                     select argument2 sales_file, argument3 cogs_file
                     from fnd_concurrent_requests
                     where 1 =1
                       and parent_request_id =n_req_id
                       and phase_code ='C'
                   )
          loop
           --
           if (b_first_child_request) then
            --
            b_first_child_request :=FALSE;
            --
            -- m option is used to remove the detail file after it was added to the archive
            -- j option is used to strip of the folder name "/xx_iface/ebiz???/outbound copied to the zip file
            --
             v_command := 'zip -mj'
                         ||' '
                         ||p_file_path
                         ||'/'
                         ||v_zip_file
                         ||' '
                         ||p_file_path
                         ||'/'
                         ||idx1.sales_file --All Sales files are included for compression
                         ||' '
                         ||p_file_path
                         ||'/'
                         ||idx1.cogs_file; --All COGS files are included for compression
             --
             print_log('ZIP command:');
             print_log('============');
             print_log(v_command);
             --
             select xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
             into   v_result
             from   dual;
             print_log('v_result '||v_result);    --ver1.3
            --
             --
             if (v_result is null) then
               --
               -- v_result when blank confirms we have successfully created the zip file
               --
               begin
                --
                -- Note: The zip file created above is owned by oradb, so we cannot move or copy.
                -- Lets change the permissions so we can operate on the above zip files like adding more text files
                -- or even remove when all are done [probably handled by UC4 ]
                -- if the OS command fails then UC4 will not be able to remove as the it is owned by oradb
                --
                v_command :='chmod 777'
                           ||' '
                         ||p_file_path
                         ||'/'
                         ||v_zip_file; --'HDS_SALES_COGS_DETAIL_'||replace(p_period, '-', '')||'.zip';
                  --

                  print_log('ZIP command:');--ver1.3
                  print_log('============');--ver1.3
                  print_log(v_command);--ver1.3

                  select xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
                  into   v_result
                  from   dual;
                  print_log('v_result '||v_result);--ver1.3
                  --
               exception
                when others then
                 v_child_requests :='NA';
                 print_log('Failed to remove the zip file '||'HDS_SALES_COGS_DETAIL_'||replace(p_period, '-', '')||'.zip');
               end;
               --
             else
               --
               v_child_requests :='NA';
               --
             end if;
             --
           else
            --
            -- j option is used to strip of the folder name "/xx_iface/ebiz???/outbound copied to the zip file
            -- m option is used to remove the detail file after it was added to the archive
            --
             v_command := 'zip -mj'
                         ||' '
                         ||p_file_path
                         ||'/'
                         ||v_zip_file
                         ||' '
                         ||p_file_path
                         ||'/'
                         ||idx1.sales_file --All Sales files are included for compression
                         ||' '
                         ||p_file_path
                         ||'/'
                         ||idx1.cogs_file; --All COGS files are included for compression
             --
             print_log(' ');  --ver1.3
             print_log('ZIP command:');--ver1.3
             print_log('============');--ver1.3
             print_log(v_command); --ver1.3
                  --
                  select xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
                  into   v_result
                  from   dual;
                 print_log('v_result '||v_result);--ver1.3
                  --
            --
           end if;
           --
          end loop; --for idx1 in child_reqs_rec.first .. child_reqs_rec.last loop
         --
        Exception
         When Others Then
           v_child_requests :='NA';
        End;
        --
     --
        If v_child_requests <>'NA' Then
           --
           begin
            --
            file_id :=utl_file.fopen(p_file_path, v_detail_wrapper_file, 'W', C_MAXLINE);
            begin
             --
             open wrapper_req_status (p_req_id =>n_req_id);
             fetch wrapper_req_status into c_req_status;
             close wrapper_req_status;
             --
             open wrapper_child_reqs (p_req_id =>n_req_id);
             fetch wrapper_child_reqs bulk collect into c_child_reqs;
             close wrapper_child_reqs;
             --
             utl_file.put_line(file_id, '');
             utl_file.put_line(file_id, 'Responsibility Name : '||c_req_status.resp_name);
             utl_file.put_line(file_id, '');
             utl_file.put_line(file_id, 'Request ID : '||c_req_status.parent_req_id);
             utl_file.put_line(file_id, '');
             --
             utl_file.put_line(file_id, 'Job Name : '||c_req_status.cp_name);
             utl_file.put_line(file_id, '');
             utl_file.put_line(file_id, 'Total Branch Detail Requests : '||c_req_status.total_branches);
             utl_file.put_line(file_id, '');
             --
             utl_file.put_line(file_id, 'Zip file path =>'||p_file_path);
             utl_file.put_line(file_id, '');
             utl_file.put_line(file_id, 'Zip file name => '||v_final_zip_file);
             utl_file.put_line(file_id, '');
             --
             utl_file.put_line(file_id, 'User: '||c_req_status.user_name);
             utl_file.put_line(file_id, 'Start Date/Time: '||c_req_status.start_date_time);
             utl_file.put_line(file_id, 'End Date/Time: '||c_req_status.end_date_time);
             utl_file.put_line(file_id, '');
             --
             utl_file.put_line(file_id, 'Status Summary:');
             utl_file.put_line(file_id, '===============');
             utl_file.put_line(file_id, '');
             --
             utl_file.put_line(file_id, 'Branch  Request Id  Status');
             utl_file.put_line(file_id, '========================================');
             if (c_child_reqs.count >0) then
              --
              for idx2 in c_child_reqs.first .. c_child_reqs.last loop
               --
               utl_file.put_line(file_id, c_child_reqs(idx2).branch_detail);
               --
              end loop;
              --
              utl_file.put_line(file_id, ' ');
              --
             end if;
             --
             utl_file.put_line(file_id, 'This is an automatic email. Please DO NOT reply to this message.');
             utl_file.put_line(file_id, ' ');
             utl_file.put_line(file_id, 'Thanks.');
             utl_file.put_line(file_id, ' ');
             utl_file.put_line(file_id, 'HDS GL Support');
             --
             utl_file.fclose(file_id);
             --
            exception
             when others then
              print_log('Failed to fetch parent request status necessary for print email, msg ='||sqlerrm);
            end;
            --
           exception
            when others then
              print_log('@Failed to create email content, error message ='||sqlerrm);
           end;
           --
           begin
              --
              v_subject :='HDS WC AR Sales COGS Detail Wrapper, Period: '||p_period||'- Status Update';
              --
                ln_request_id :=fnd_request.submit_request
                  (
                   application      =>'XXCUS',
                   program          =>'XXCUS_AR_SALESCOGS_DTL_STAT',
                   description      =>'',
                   start_time       =>'',
                   sub_request      =>TRUE,
                   argument1        =>v_email,
                   argument2        =>v_detail_wrapper_file,
                   argument3        =>v_subject,
                   argument4        =>p_file_path
                  );
                 --
                  if ln_request_id >0 then
                   print_log('Successfully submitted report to email the status of the detail wrapper, request_id ='||ln_request_id);
                  else
                     print_log('Failed to submit email for status update of detail wrapper');
                  end if;
                 --
              --
            /*
              xxcus_misc_pkg.html_email
                (
                  p_to            =>v_email,
                  p_from          =>v_email,
                  p_text          => Null,
                  p_subject       => v_subject,
                  p_html          => v_email_body,
                  p_smtp_hostname => v_host,
                  p_smtp_portnum  => v_hostport
                );
             */
              --
           exception
             when others then
             print_log ('@OS mailx command execution, error message ='||sqlerrm);
           end; 
           --
           begin
            --
            -- rename the zip file to the final resting file name [v_final_zip_file ]
            --
             v_command :='mv'
                       ||' '
                       ||p_file_path
                       ||'/'
                       ||v_zip_file --temp zip file
                       ||' '
                       ||p_file_path
                       ||'/'
                       ||v_final_zip_file; --temp zip file
              --
              print_log('Rename temp zip file, OS command =>'||v_command);
              --
              select xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
              into   v_result
              from   dual;
			  
			  -- Ver#1.4 > Start
			  v_command :='chmod 777'
                           ||' '
                         ||p_file_path
                         ||'/'
                         ||v_final_zip_file; --'Final Name HDS_SALES_COGS_DETAIL_'||replace(p_period, '-', '')||'.zip';
                  --

                  print_log('ZIP command:');
                  print_log('============');
                  print_log(v_command);

                  select xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
                  into   v_result
                  from   dual;
                  print_log('v_result '||v_result);
              --  Ver#1.4 <End
            exception
              when others then
                print_log('Failed to rename temp zip file '||v_zip_file||', msg ='||v_result);
            end;
           --
        Else --v_child_requests ='NA'
          Null;
        End If;
        --
      retcode :=0;
      errbuf :='Child request completed. Exit HDS AR Sales COGS Detail Wrapper';
      fnd_file.put_line(fnd_file.log,errbuf);
    --
   End If;
  --
  exception
  --
   when program_error then
    print_log('Location =>'||v_location||', program error, message ='||sqlerrm);
  --
   when others then
    print_log('Location =>'||v_location||', outer block other errors');
    print_log('Outer block, other errors in calling detail_wrapper, message ='||sqlerrm);
    l_err_msg := l_sec;
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
    rollback;
  --
  end detail_wrapper;
 --
/**************************************************************************
 *
 * FUNCTION: uc4_dtl_batch_submit
 * DESCRIPTION: HDS AR Sales and COGS Analysis -Branch Detail
 * PARAMETERS
 * ==========
 * NAME                   TYPE     DESCRIPTION
.* -----------------      -------- ---------------------------------------------
 * p_org_id               IN       org_id for Whitecap
 * p_user_name            IN       EBS user who owns the current concurrent request
 * p_responsibility_name  IN      Current EBS Responsibility used to submit the job
 * p_period               IN      Fiscal period.
 * retcode                OUT     standard concurrent program return variable
 * errbuf                 OUT     standard concurrent program return variable
 * RETURN VALUE: Mentioned above
 * CALLED BY: None [Future use]
 *************************************************************************/
  --
   procedure uc4_dtl_batch_submit
                       (  errbuf                 out varchar2
                        , retcode                out varchar2
                        , p_org_id               in  number
                        , p_user_name            in  varchar2
                        , p_responsibility_name  in  varchar2
                        , p_period               in  varchar2
                       )
   is
           -- Variable definitions
        l_package    VARCHAR2(50) := 'xxcus_match_gl_rev_cogs.uc4_dtl_batch_submit';
        l_email     VARCHAR2(200) := 'HDSOracleDevelopers@hdsupply.com';
        --
        l_req_id                NUMBER NULL;
        v_phase                 VARCHAR2(50);
        v_status                VARCHAR2(50);
        v_dev_status            VARCHAR2(50);
        v_dev_phase             VARCHAR2(50);
        --
        v_message               VARCHAR2(250);
        v_error_message         VARCHAR2(3000);
        l_err_msg               VARCHAR2(3000);
        l_err_code              NUMBER;
        --
        n_ledger_id             NUMBER;
        v_period                VARCHAR2(10);
        v_file_path             VARCHAR2(80);
        --
        l_statement             VARCHAR2(9000);
        l_user_id               NUMBER;
        l_org_id                NUMBER :=p_org_id;
        l_responsibility_id     NUMBER;
        l_resp_application_id   NUMBER;
        --
        l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_ERROR_PKG';
        l_err_callpoint VARCHAR2(75) DEFAULT 'START';
        l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
        --
   begin
        BEGIN
           if (l_org_id is null or l_org_id !=162) then
             l_err_msg := 'This is a whitecap specific batch job. Please enter 162 for parameter p_org_id';
             RAISE program_error;
           else
             --
                begin
                    select set_of_books_id
                    into   n_ledger_id
                    from   hr_operating_units
                    where  1 =1
                      and  organization_id =l_org_id;
                exception
                 when no_data_found then
                   n_ledger_id :=Null;
                   l_err_msg := 'Failed to fetch GL ledger id for org_id '||l_org_id||', Please check.';
                   raise program_error;
                 when others then
                   n_ledger_id :=Null;
                   l_err_msg := 'Other errors in fetching gl ledger id,  message ='||sqlerrm;
                   raise program_error;
                end;
              --
                begin
                    select period_name
                    into   v_period
                    from   gl_periods
                    where  1 =1
                      and  period_name =p_period
                      and  adjustment_period_flag ='N';
                exception
                 when no_data_found then
                   v_period :=Null;
                   l_err_msg := 'Failed to validate period name for period '||p_period||', Please check.';
                   raise program_error;
                 when too_many_rows then
                   v_period :=Null;
                   l_err_msg := 'Too many rows found for period name '||p_period||', Please check.';
                   raise program_error;
                 when others then
                   v_period :=Null;
                   l_err_msg := 'Other errors in fetching gl period name,  message ='||sqlerrm;
                   raise program_error;
                end;
              --
                begin
                    select '/xx_iface/'||lower(name)||'/outbound'
                    into   v_file_path
                    from   v$database;
                exception
                 when no_data_found then
                   v_file_path :=Null;
                   l_err_msg := 'Failed to fetch v_file_path, Please check.';
                   raise program_error;
                 when others then
                   v_file_path :=Null;
                   l_err_msg := 'Other errors in fetching file path,  message ='||sqlerrm;
                   raise program_error;
                end;
              --
           end if;
        EXCEPTION
         WHEN OTHERS THEN
           l_err_msg := 'Issue in checking allowed values for p_org_id,  message ='||sqlerrm;
           RAISE program_error;
        END;

          -- Deriving Ids from variables
          BEGIN
            SELECT    user_id
              INTO     l_user_id
              FROM     fnd_user
              WHERE     user_name = UPPER(p_user_name)
            AND     SYSDATE BETWEEN start_date AND NVL(end_date, TRUNC(SYSDATE) + 1);
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
           l_err_msg := 'UserName '||p_user_name||' not defined in Oracle';
           RAISE program_error;
        WHEN OTHERS THEN
           l_err_msg := 'Error deriving user_id for UserName - '||p_user_name;
           RAISE program_error;
        END;

          BEGIN
            SELECT     responsibility_id
                     , application_id
              INTO     l_responsibility_id
                     , l_resp_application_id
              FROM     fnd_responsibility_vl
             WHERE     responsibility_name = p_responsibility_name
               AND     SYSDATE BETWEEN start_date AND NVL(end_date, TRUNC(SYSDATE) + 1);
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
             l_err_msg := 'Responsibility '||p_responsibility_name||' not defined in Oracle';
             RAISE program_error;
          WHEN OTHERS THEN
             l_err_msg := 'Error deriving Responsibility_id for '||p_responsibility_name;
             RAISE program_error;
          END;

          -- Apps Initialize
          FND_GLOBAL.APPS_INITIALIZE (l_user_id, l_responsibility_id, l_resp_application_id);
          --
          FND_REQUEST.SET_ORG_ID (org_id =>l_org_id);
          --
          -- Submitting program HDS AR Sales COGS Detail Wrapper
          l_req_id := fnd_request.submit_request(application =>'XXCUS'
                                               , program     =>'XXCUS_REV_COGS_DETAIL_WRAPPER'
                                               , description =>'UC4 Submit'
                                               , start_time  =>SYSDATE
                                               , sub_request =>FALSE
                                               , argument1   =>Null --leave blank for all gl locations or branches
                                               , argument2   =>n_ledger_id
                                               , argument3   =>v_period
                                               , argument4   =>v_file_path
                                               );

          COMMIT;

          dbms_output.put_line('Concurrent Program Request Submitted. Request ID: '||l_req_id);
          IF (l_req_id != 0)
          THEN

            IF fnd_concurrent.wait_for_request(l_req_id
                                          ,6
                                          ,3600 -- 04/17/2012 Changed from 15000
                                          ,v_phase
                                          ,v_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
            THEN

                v_error_message := 'ReqID:' || l_req_id || '-DPhase:' || v_dev_phase || '-DStatus:' || v_dev_status ||
                                    chr(10) || 'MSG:' || v_message;

                -- Error Returned
                IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
                THEN
                    l_statement := 'EBS Conc Prog Completed with problems ' || v_error_message || '.';
                    l_err_msg := l_statement;
                    dbms_output.put_line(l_statement);
                    RAISE program_error;
                ELSE
                    retcode := 1;
                END IF;

            ELSE
                  l_statement := 'EBS Conc Program Wait timed out';
                l_err_msg := l_statement;
                  dbms_output.put_line(l_statement);
                  RAISE program_error;
            END IF;

          ELSE
            l_statement := 'EBS Conc Program not initated';
            l_err_msg := l_statement;
            dbms_output.put_line(l_statement);
            RAISE program_error;
          END IF;

   EXCEPTION
   WHEN program_error THEN
       ROLLBACK;
    l_err_code := 2;
    l_err_msg  := substr ( (l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000)), 1, 3000);
    dbms_output.put_line(l_err_msg);

    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(SQLERRM,1,2000)
                                        ,p_error_desc        => substr(l_err_msg,1,2000)
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'HDS GL -WhiteCap');
       retcode := l_err_code;
    errbuf := l_err_msg;
   WHEN OTHERS THEN
    l_err_code := 2;
    l_err_msg  := substr ( (l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000)), 1, 3000);
    dbms_output.put_line(l_err_msg);

    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(SQLERRM, 1, 2000)
                                        ,p_error_desc        => substr(l_err_msg,1,2000)
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'HDS GL -WhiteCap');

       retcode := l_err_code;
    errbuf := l_err_msg;
   end uc4_dtl_batch_submit;
end xxcus_match_gl_rev_cogs;
/