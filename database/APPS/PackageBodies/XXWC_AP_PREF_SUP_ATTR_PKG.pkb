CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AP_PREF_SUP_ATTR_PKG
/*************************************************************************
  $Header XXWC_AP_PREF_SUP_ATTR_PKG.pks $
  Module Name: XXWC_AP_PREF_SUP_ATTR_PKG

  PURPOSE: Package to upload the Preferred Supplier Attributes though WEB ADI

  TMS Task Id :  20141104-00115

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        29-Oct-2014  Manjula Chellappan    Initial Version
**************************************************************************/
AS
   g_distribution_list   fnd_user.email_address%TYPE
                            := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE upload_main (p_supplier_name    VARCHAR2,
                          p_supplier_num     VARCHAR2,
                          p_supplier_tier    VARCHAR2,
                          p_supplier_sba     VARCHAR2)
   /*************************************************************************
     $Header XXWC_AP_PREF_SUP_ATTR_PKG.upload_main  $
     Module Name: XXWC_AP_PREF_SUP_ATTR_PKG.upload_main

     PURPOSE: Procedure to upload the Preferred Supplier Attributes though WEB ADI

     TMS Task Id :  20141104-00115

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        29-Oct-2014  Manjula Chellappan    Initial Version

   **************************************************************************/

   AS
      l_supplier_id     NUMBER;
      l_process_id      NUMBER;
      l_status          VARCHAR2 (1);
      l_error_msg       VARCHAR2 (2000);
      l_sec             VARCHAR2 (240);
      l_supplier_rec    ap_vendor_pub_pkg.r_vendor_rec_type;
      l_return_status   VARCHAR2 (240);
      l_msg_count       NUMBER;
      l_msg_data        VARCHAR2 (2000);
      l_program_error   EXCEPTION;
   BEGIN
      fnd_message.clear;

      BEGIN
         l_process_id := XXWC.XXWC_AP_PREF_SUP_ATTR_INT_S.NEXTVAL;
         l_sec :=
            'Load data into staging table XXWC.XXWC_AP_PREF_SUP_ATTR_STG_TBL ';

         BEGIN
            INSERT INTO XXWC.XXWC_AP_PREF_SUP_ATTR_STG_TBL (process_id,
                                                            process_date,
                                                            supplier_name,
                                                            supplier_number,
                                                            supplier_tier,
                                                            supplier_sba)
                 VALUES (l_process_id,
                         SYSDATE,
                         p_supplier_name,
                         p_supplier_num,
                         p_supplier_tier,
                         p_supplier_sba);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                  l_error_msg || l_sec || CHR (10) || SQLERRM || CHR (10);

               RAISE l_program_error;
         END;

         COMMIT;

         l_sec := 'Derive the supplier Id ';

         BEGIN
            SELECT vendor_id
              INTO l_supplier_id
              FROM ap_suppliers
             WHERE     UPPER (vendor_name) = UPPER (TRIM (p_supplier_name))
                   AND segment1 = p_supplier_num;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_status := 'E';
               l_error_msg :=
                     l_error_msg
                  || 'No Vendor available to update for the vendor Number '
                  || p_supplier_num
                  || ' and Vendor Name '
                  || p_supplier_name
                  || CHR (10);

               RAISE l_program_error;
            WHEN OTHERS
            THEN
               l_error_msg :=
                  l_error_msg || l_sec || CHR (10) || SQLERRM || CHR (10);

               RAISE l_program_error;
         END;


         l_sec := 'Load the Supplier Attributes to supplier master ';

         BEGIN
            l_supplier_rec.attribute3 := p_supplier_tier;
            l_supplier_rec.attribute4 := p_supplier_sba;

            ap_vendor_pub_pkg.Update_Vendor (
               p_api_version     => 1,
               x_return_status   => l_return_status,
               x_msg_count       => l_msg_count,
               x_msg_data        => l_msg_data,
               p_vendor_rec      => l_supplier_rec,
               p_vendor_id       => l_supplier_id);

            IF (l_return_status <> 'S')
            THEN
               IF l_msg_count > 1
               THEN
                  FOR i IN 1 .. l_msg_count
                  LOOP
                     l_error_msg :=
                           l_error_msg
                        || SUBSTR (
                              FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                              1,
                              255);
                  END LOOP;

                  RAISE l_program_error;
               END IF;
            END IF;

            l_status := l_return_status;
         END;
      END;

      l_sec := 'Update the Staging table with the process status ';

      UPDATE XXWC.XXWC_AP_PREF_SUP_ATTR_STG_TBL
         SET supplier_id = l_supplier_id,
             status = l_status,
             error_msg = l_error_msg
       WHERE process_id = l_process_id;
   EXCEPTION
      WHEN l_program_error
      THEN
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_msg);
      WHEN OTHERS
      THEN
         l_error_msg :=
            l_error_msg || l_sec || CHR (10) || SQLERRM || CHR (10);
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AP_PREF_SUP_ATTR_PKG.UPLOAD_MAIN',
            p_calling             => l_sec,
            p_request_id          => NULL,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_distribution_list,
            p_module              => 'PO');
   END upload_main;
END XXWC_AP_PREF_SUP_ATTR_PKG;