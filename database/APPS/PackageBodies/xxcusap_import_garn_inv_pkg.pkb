CREATE OR REPLACE PACKAGE BODY APPS.xxcusap_import_garn_inv_pkg AS
  /**************************************************************************
    =========================================================================
       Copyright (c) 2008 HD SUPPLY
                  All rights reserved.
     ========================================================================
      File Name:xxcus_ap_load_import_inv_pkg.pls
      PROGRAM TYPE: PL/SQL Package
      PURPOSE: This package is written to Load the AP open interface from
         xxcus.xxcusap_psap_garn_invoices_tbl and import to AP. Also maintains
         process statuses LOADED, IMPORTED, REJECTED in
         xxcus.xxcusap_psap_garn_invoices_tbl
      HISTORY
      =======================================================================
      Last Update Date : 04/06/2008
      =======================================================================
      VERSION DATE          AUTHOR(S)          DESCRIPTION
      ------- -----------   ---------------    ------------------------------
      1.0     06-APR-2008   Murali Krishnan    Created this package.
      2.0     24-MAR-2010   Murali Krishnan  LOADED defaul after import is changed
                           to g.processed to keep existing status
                         LUMP logic changed for CASI ID unqique
      2.1     05-15-2011   Kathy Poling        Changed insert to use invoice type of
                                               STANDARD when amount is positive and
                                               CREDIT when it's less then zero  SR 80363
      3.0     06-20-2011    Luong Vu           Retrofit program for R12
      3.1     04-23-2012    Luong Vu           Fixed payment method code "ETF" ->"EFT"     
                                               SR 136810 - RFC 33470                                             
  *****************************************************************************/
  PROCEDURE main(p_group_id IN VARCHAR2) IS
    --
    -- Find all invoices for this group id
    --
    CURSOR c_invoices(c_org_id NUMBER) IS
      SELECT 'N' lumped
            ,garnish_id
            ,substr(g.site, 1, instr(site, '-') - 1) vendor_number
            ,site
            ,invoice_num
            ,invoice_date
            ,invoice_amount
            ,(CASE
               WHEN pay_grp_lookup IN ('LC', 'LO') THEN
                'LOCAL CHECKS'
               ELSE
                NULL
             END) pay_group_lookup_code
            ,attribute2_ded_code
            ,attribute3_emp_num
            ,attribute1_case_id
            ,(CASE
               WHEN epp.payment_method_code = 'EFT'
                    AND g.pay_grp_lookup NOT IN ('LC', 'LO') THEN
                g.iocomments
               WHEN epp.payment_method_code = 'CHECK'
                    OR g.pay_grp_lookup IN ('LC', 'LO') THEN
                g.comments_msgs
             END) check_comments
            ,g.import_group_id
            ,
             --g.INTERFACE_ID,
             vs.payment_method_lookup_code
        FROM xxcus.xxcusap_psap_garn_invoices_tbl g
            ,po_vendors                           v
            ,po_vendor_sites_all                  vs
            ,iby_external_payees_all              ep
            ,iby_ext_party_pmt_mthds              epp
       WHERE import_group_id = p_group_id
         AND v.vendor_id = vs.vendor_id
         AND vs.vendor_site_id = ep.supplier_site_id
         AND ep.ext_payee_id = epp.ext_pmt_party_id
         AND v.segment1 = substr(g.site, 1, instr(g.site, '-') - 1)
         AND vs.vendor_site_code = g.site
         AND g.lump IN ('N', ' ') -- Not Lumped
         AND g.processed_flag IS NULL
         AND vs.org_id = c_invoices.c_org_id -- found same site code is used in other org
      UNION ALL
      SELECT 'Y' lumped
            ,MAX(garnish_id) garnish_id
            ,substr(g.site, 1, instr(site, '-') - 1) vendor_number
            ,g.site
            ,MAX(invoice_num) invoice_num
            ,MAX(invoice_date) invoice_date
            ,SUM(invoice_amount) invoice_amount
            ,MAX((CASE
                   WHEN pay_grp_lookup IN ('LC', 'LO') THEN
                    'LOCAL CHECKS'
                   ELSE
                    NULL
                 END)) pay_group_lookup_code
            ,attribute2_ded_code
            ,attribute3_emp_num
            ,attribute1_case_id
            ,MAX((CASE
                   WHEN nvl(pay_grp_lookup, ' ') IN ('LC', 'LO') THEN
                    g.comments_msgs
                   WHEN nvl(pay_grp_lookup, ' ') NOT IN ('LC', 'LO')
                       --AND epp.payment_method_code = 'ETF' THEN  --v3.1
                        AND epp.payment_method_code = 'EFT' THEN
                    iocomments
                   WHEN nvl(pay_grp_lookup, ' ') NOT IN ('LC', 'LO')
                        AND epp.payment_method_code = 'CHECK' THEN
                    g.comments_msgs
                   ELSE
                    NULL
                 END)) check_comments
            ,g.import_group_id
            ,decode(MAX((CASE
                          WHEN pay_grp_lookup IN ('LC', 'LO') THEN
                           'LOCAL CHECKS'
                          ELSE
                           NULL
                        END)), 'LOCAL CHECKS', 'CHECK',
                    MAX(vs.payment_method_lookup_code)) payment_method_lookup_code
      --
        FROM xxcus.xxcusap_psap_garn_invoices_tbl g
            ,po_vendors                           v
            ,po_vendor_sites_all                  vs
            ,iby_external_payees_all              ep
            ,iby_ext_party_pmt_mthds              epp
       WHERE import_group_id = p_group_id
         AND v.vendor_id = vs.vendor_id
         AND vs.vendor_site_id = ep.supplier_site_id
         AND ep.ext_payee_id = epp.ext_pmt_party_id
         AND v.segment1 = substr(g.site, 1, instr(g.site, '-') - 1)
         AND vs.vendor_site_code = g.site
         AND g.lump = 'Y' --Lumped
         AND g.processed_flag IS NULL
         AND vs.org_id = c_invoices.c_org_id
       GROUP BY substr(g.site, 1, instr(site, '-') - 1)
               ,g.site
               ,attribute2_ded_code
               ,attribute3_emp_num
               ,attribute1_case_id
               ,g.import_group_id;
  
    v_site_method      VARCHAR2(30);
    v_site_bank_status VARCHAR2(30);
    v_default_ccid     VARCHAR2(240) := nvl(fnd_profile.value('XXCUS_AP_IWO_DEFAULT_CCID'),
                                            '48.Z1167.0000.259501.00000.00000.00000');
  
    -- Error DEBUG
    l_msg           VARCHAR2(1000);
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_AP_LOAD_IMPORT_INV_PKG.main';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    no_org4_garnishment EXCEPTION;
  
  BEGIN
    --
    -- Set new Org ID
    --
    l_err_callpoint := 'Lookgin for Garnishment Org';
    BEGIN
      SELECT MIN(organization_id)
        INTO g_org_id
        FROM hr_organization_units
       WHERE upper(NAME) LIKE 'HDS GARNISHMENT%';
    EXCEPTION
      WHEN OTHERS THEN
        RAISE no_org4_garnishment;
    END;
  
    --
    -- Read each invoice to load in to ap_invoices_interface, lines
    -- and mark them LOADED
    --
    FOR grec IN c_invoices(g_org_id)
    LOOP
    
      --v_site_method := NULL;
      v_site_bank_status := NULL;
      --
      --
      -- If invoice is ACH but vendor site must have bank details.
      -- if not reject it
      -- ------------------------------------------------------------------------------------------
      IF grec.payment_method_lookup_code = 'ACH' THEN
        --
        -- Check to ACH invoices with Bank details
        -- if not, mark ACH REJECTED right away so that Import API will ignore processing it.
        -- mark it in the interface and in the staging table xxcus.xxcusap_psap_garn_invoices_tbl
        --
        BEGIN
          SELECT 'SITE-BANK N/A'
            INTO v_site_bank_status
            FROM dual
           WHERE NOT EXISTS (SELECT 'x'
                    FROM ap_bank_account_uses_all
                   WHERE vendor_site_id =
                         (SELECT vendor_site_id
                            FROM po_vendor_sites_all
                           WHERE vendor_site_code = grec.site)
                     AND end_date IS NULL
                     AND start_date <= SYSDATE);
        EXCEPTION
          WHEN no_data_found THEN
            v_site_bank_status := NULL;
        END;
      END IF;
    
      INSERT INTO ap_invoices_interface
        (invoice_id
        ,invoice_num
        ,description
        ,invoice_type_lookup_code
        ,invoice_date
        ,vendor_num
        ,vendor_site_code
        ,invoice_amount
        ,invoice_currency_code
        ,last_update_date
        ,last_updated_by
        ,creation_date
        ,created_by
        ,attribute1
        ,attribute2
        ,attribute3
        ,attribute4 -- invoice description use for payment extract
        ,SOURCE
        ,group_id
        ,payment_method_lookup_code
        ,pay_group_lookup_code
        ,org_id
        ,attribute_category
        ,status)
      VALUES
        (ap_invoices_interface_s.nextval
        ,grec.invoice_num
        ,grec.check_comments
        ,(CASE WHEN grec.invoice_amount >= 0 THEN 'STANDARD' ELSE 'CREDIT' END)
        ,grec.invoice_date
        ,grec.vendor_number
        ,grec.site
        ,grec.invoice_amount
        ,'USD'
        ,SYSDATE
        ,3 -- FEEDER SYSTEM apps user
        ,SYSDATE
        ,3
        ,grec.attribute1_case_id
        ,grec.attribute2_ded_code
        ,grec.attribute3_emp_num
        ,grec.check_comments
        ,'IWO'
        ,p_group_id
         --,DECODE(UPPER(grec.payment_method_lookup_code), 'LOCAL CHECK', 'CHECK', UPPER(grec.payment_method_lookup_code))
        ,NULL -- vendor setup will be used
         --,DECODE(UPPER(grec.payment_method_lookup_code), 'LOCAL CHECK', 'LOCAL CHECKS', 'IWO')
        ,grec.pay_group_lookup_code
        ,g_org_id
        ,g_org_id
        ,v_site_bank_status);
    
      --
      -- Invoice Line interface
      --
    
      INSERT INTO ap_invoice_lines_interface
        (invoice_id
        ,line_number
        ,line_type_lookup_code
        ,amount
        ,description
        ,dist_code_concatenated
        ,last_updated_by
        ,last_update_date
        ,created_by
        ,creation_date
        ,org_id
        ,attribute_category)
      VALUES
        (ap_invoices_interface_s.currval
        ,1
        ,'MISCELLANEOUS'
        ,grec.invoice_amount
        ,grec.invoice_num
        ,v_default_ccid --'48.Z0167.0000.259501.00000.00000'
        ,3
        ,SYSDATE
        ,3
        ,SYSDATE
        ,g_org_id
        ,g_org_id);
    
      IF v_site_bank_status = 'SITE-BANK N/A' THEN
        -- ACHinvoice, site exists but site is other than ACH
        --
        -- inserst into interface rejection for custom rejection
        --
        INSERT INTO ap_interface_rejections
          (parent_table
          ,parent_id
          ,reject_lookup_code
          ,last_updated_by
          ,last_update_date
          ,last_update_login
          ,created_by
          ,creation_date)
        VALUES
          ('AP_INVOICES_INTERFACE'
          ,ap_invoices_interface_s.currval
          ,'MISSING ACH BANK DETAILS'
          ,-1
          ,SYSDATE
          ,-1
          ,-1
          ,SYSDATE);
      
      END IF;
    
      --
      -- Mark as LOADED if ACH check is ok, otherwise 'SITE BANK AC MISSING'
      --
      IF grec.lumped = 'N' THEN
        UPDATE xxcus.xxcusap_psap_garn_invoices_tbl
           SET processed_flag = 'LOADED'
              ,interface_id   = ap_invoices_interface_s.currval
         WHERE garnish_id = grec.garnish_id;
      END IF;
    
      IF grec.lumped = 'Y' THEN
        UPDATE xxcus.xxcusap_psap_garn_invoices_tbl g
           SET processed_flag = 'LOADED'
              ,interface_id   = ap_invoices_interface_s.currval
         WHERE import_group_id = p_group_id
           AND lump = 'Y'
           AND substr(g.site, 1, instr(site, '-') - 1) = grec.vendor_number
           AND g.site = grec.site
           AND g.attribute2_ded_code = grec.attribute2_ded_code
           AND g.attribute3_emp_num = grec.attribute3_emp_num
           AND g.attribute1_case_id = grec.attribute1_case_id;
      END IF;
    
    END LOOP;
  
    COMMIT;
  
    --
    -- Import the invoices in Group
    --
    import_invoices(p_group_id);
    --
    --
  
    --
    -- Update status = REJECTED for SINE-BANK N/A status
    -- it was marked so that import woun't pick up.
    --
    UPDATE ap_invoices_interface
       SET status   = 'REJECTED'
          ,group_id = substr(vendor_site_code || ' ' || p_group_id, 1, 80) -- max 80
     WHERE group_id = p_group_id
       AND status = 'SITE-BANK N/A';
  
    --
    -- Update status in xxcus.xxcusap_psap_garn_invoices_tbl
    --
    -- All LOADED status changes to PROCESSED or REJECTED or SITE BANK AC MISSING
    --
    FOR gstatusrec IN (SELECT interface_id
                             ,garnish_id
                         FROM xxcus.xxcusap_psap_garn_invoices_tbl
                        WHERE import_group_id = p_group_id)
    LOOP
      --
      UPDATE xxcus.xxcusap_psap_garn_invoices_tbl g
         SET g.processed_flag   = nvl((SELECT status
                                        FROM ap_invoices_interface i
                                       WHERE i.invoice_id =
                                             gstatusrec.interface_id),
                                      g.processed_flag)
            ,g.last_update_date = SYSDATE
       WHERE g.garnish_id = gstatusrec.garnish_id;
    
    END LOOP;
  
    COMMIT;
  
  EXCEPTION
    WHEN no_org4_garnishment THEN
      l_msg := 'HDS Garnishment Org not setup or missing';
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint ||
                                                         ' Not completed',
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => l_msg,
                                           p_argument1 => p_group_id,
                                           p_argument2 => NULL,
                                           p_argument3 => NULL,
                                           p_distribution_list => l_distro_list,
                                           p_request_id => NULL);
    WHEN OTHERS THEN
      l_msg := 'HDS Garnishment OTHERS Exception Raised';
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint ||
                                                         ' Not completed',
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => l_msg,
                                           p_argument1 => p_group_id,
                                           p_argument2 => NULL,
                                           p_argument3 => NULL,
                                           p_distribution_list => l_distro_list,
                                           p_request_id => NULL);
    
  END main;

  PROCEDURE import_invoices(p_group_id IN VARCHAR2) IS
    l_batch_error_flag     VARCHAR2(15);
    l_invoices_fetched     NUMBER;
    l_invoices_created     NUMBER;
    l_total_invoice_amount NUMBER;
    l_print_batch          VARCHAR2(15) := 'N';
  
  BEGIN
    fnd_client_info.set_org_context(g_org_id);
    IF ap_import_invoices_pkg.import_invoices(NULL,
                                              --:p_batch_name, -- bring under a batch
                                              NULL,
                                              --:p_gl_date, -- default value blank
                                              NULL,
                                              --'POS Hold for Approval',       --:p_hold_code, -- default value as "POS Hold for Approval"
                                              NULL,
                                              --'Unapproved - Awaiting approval from POS system',
                                              --:p_hold_reason, -- default value as "Unapproved - Awaiting approval from POS system"
                                              1000,
                                              --:p_commit_cycles, - 1000
                                              'IWO',
                                              --:p_source, -- DCTM
                                              p_group_id,
                                              --like 04092007 10:01:25
                                              NULL,
                                              --p_conc_request_id, --- blank
                                              'N',
                                              --       :p_debug_switch,    = N
                                              g_org_id,
                                              -- org_id
                                              l_batch_error_flag,
                                              --OUT
                                              l_invoices_fetched,
                                              --OUT
                                              l_invoices_created,
                                              --OUT
                                              l_total_invoice_amount,
                                              --OUT  for bug 989221
                                              l_print_batch,
                                              'IWO Invoices From LAWSON') THEN
      NULL;
    END IF;
  
    COMMIT;
  END import_invoices;

  PROCEDURE xxcus_process_invoices_prc(errbuf  OUT VARCHAR2
                                      ,retcode OUT VARCHAR2)
  --The retcode has three values returned by the concurrent manager
    --0--Success
    --1--Success & warning
    --2--Error
  
   IS
    /**************************************************************************
      =========================================================================
         Copyright (c) 2008 HD SUPPLY
                    All rights reserved.
       ========================================================================
        File Name:xxcus_transfer_invoices_prc.pls
        PROGRAM TYPE: PL/SQL Procedure
        PURPOSE: This procedure is written to transfer the Invoice from LAWSON
           using DB link.
        RETURNS: Total read, copied and rejected
        HISTORY
        =======================================================================
        Last Update Date : 04/06/2008
        =======================================================================
        VERSION DATE          AUTHOR(S)          DESCRIPTION
        ------- -----------   ---------------    ------------------------------
        1.0     06-APR-2008   Murali Krishnan    Created this procedure.
      2.0     16-DEC-2010   Murali Krishnan    Changes to aaccomadate PeopleSoft
                                               implementation
    *****************************************************************************/
    --
    -- group is a date and time stamp
    --
    v_group_id      VARCHAR2(60) := 'IWO INVOICES ' ||
                                    to_char(SYSDATE, 'MM/DD/YYYY HH24:MI:SS');
    v_sysdate       DATE := SYSDATE;
    op_read_count   NUMBER := 0;
    op_copied_count NUMBER := 0;
    op_reject_count NUMBER := 0;
  
  BEGIN
    --
    -- Read  LAwson and copy into Oracle using DB link
    --
    FOR c_pro IN (SELECT import_group_id
                    FROM xxcus.xxcusap_garn_que_tbl
                   WHERE processed = 'N')
    LOOP
    
      apps.xxcusap_import_garn_inv_pkg.main(c_pro.import_group_id);
    
      UPDATE xxcus.xxcusap_garn_que_tbl
         SET processed      = 'Y'
            ,processed_dttm = SYSDATE
       WHERE import_group_id = c_pro.import_group_id;
    
      COMMIT;
    
    END LOOP;
    retcode := 0;
  EXCEPTION
    WHEN OTHERS THEN
      retcode := 2;
      errbuf  := SQLERRM;
    
  END xxcus_process_invoices_prc;

  /**************************************************************************
  =========================================================================
     Copyright (c) 2008 HD SUPPLY
                All rights reserved.
   ========================================================================
    File Name:
    PROGRAM TYPE: PL/SQL Procedure
    PURPOSE: This procedure is written as a wrapper to allow unprocessed
             transaction to import into Oracle
             concurrent program:  XXHSI IWO Invoices to Payables Custom one off
    HISTORY
    =======================================================================
    Last Update Date : 06/30/2011
    =======================================================================
    VERSION DATE          AUTHOR(S)          DESCRIPTION
    ------- -----------   ---------------    ------------------------------
    3.0     06-Jun-2011   Kathy Poling       Created this procedure.
  
  *****************************************************************************/

  PROCEDURE one_off_run(errbuf     OUT VARCHAR2
                       ,retcode    OUT VARCHAR2
                       ,p_group_id IN VARCHAR2) IS
  
    l_msg VARCHAR2(1000);
  
  BEGIN
    l_msg := 'IMPORT_GROUP_ID :  ' || p_group_id;
  
    fnd_file.put_line(fnd_file.log, l_msg);
    fnd_file.put_line(fnd_file.output, l_msg);
  
    BEGIN
      -- Call the procedure
      xxcusap_import_garn_inv_pkg.main(p_group_id);
    END;
  
    retcode := 0;
  EXCEPTION
    WHEN OTHERS THEN
      retcode := 2;
      errbuf  := SQLERRM;
    
  END one_off_run;

END xxcusap_import_garn_inv_pkg;
/
