create or replace package body apps.xxwc_ar_prism_refund_pkg
as
    /***************************************************************************
       *    script name: xxwc_prism_refund_pkg
       *
       *    interface name: Prism Refunds
       *
       *    functional purpose:Match the refund debit memo to return credit memo
       *
       *    history:
       *
       *    version    date              author             description
       *************************************************************************
       *    1.0        25-apr-2012  Shankar Hariharan     initial development
       *    1.1        11-May-2012  Shankar Hariharan    Changed the trx match criteria to exclude
       *                                                 amount and the attribute column to 14 from 15
       *    1.2       31-Aug-2012   Shankar Hariharan    Made Batch source and receipt method as parameters                                                 
	   *    1.3	      10-Oct-2014   Veera 		         TMS# 20141001-00166 Canada Multi Org Changes3       ************************************************************************/
    /**************************************************************************/
  -- Global Variable
--g_dm_batch_source varchar2(30) := 'PRISM REFUND';
--g_cm_batch_source varchar2(30) := 'PRISM';
--g_receipt_method  varchar2(30) := 'WC Corporate Receipt Method'; 
procedure process_refund (errbuf out varchar2, retcode out number,i_dummy in number, i_dm_bs_id in number,
                          i_cm_bs_id in number,i_receipt_method_id in number) 
is
 l_dm_bs_id number;
 l_cm_bs_id number;
 l_cm_trx_id number;
 l_cm_ps_id number;
 l_cm_due   number;
 l_process varchar2(1);
 l_cash_receipt_id number;
 l_receipt_method_id number;
 
 l_return_status varchar2(30);
 l_msg_count number;
 l_msg_data varchar2(2000);
 l_count number;

 
cursor c1 
    is
select a.batch_source_id, a.customer_trx_id, a.trx_number, a.trx_date,a.bill_to_customer_id,a.invoice_currency_code,nvl(a.gd_gl_date,a.trx_date) gl_date,
       b.amount_due_remaining, b.payment_schedule_id
  From ra_customer_trx_v a, ar_payment_schedules b
 where a.batch_source_id=l_dm_bs_id
   and a.ctt_class='DM'
   and nvl(a.attribute14,'N')='N'
   and a.complete_flag='Y'
   and a.status_trx='OP'
   and a.customer_trx_id=b.customer_trx_id;
   --and a.customer_trx_id=402040; 
begin
  -- derive batch source id for dm and cm batch source and receipt method id
  begin
  /*
   select batch_source_id
     into l_dm_bs_id
     from ra_batch_sources
    where name=g_dm_batch_source;
  
   select batch_source_id
     into l_cm_bs_id
     from ra_batch_sources
    where name=g_cm_batch_source;
    
   select receipt_method_id
     into l_receipt_method_id
     from ar_receipt_methods
    where name=g_receipt_method; 
    */
    l_dm_bs_id := i_dm_bs_id;
    l_cm_bs_id := i_cm_bs_id;
    l_receipt_method_id := i_receipt_method_id; 
  exception
    when others then
     errbuf := 'Missing batch source or receipt method';
     retcode := 2;
     return;
  end;

-- Loop through record to get the dm payment schedule id
  for c1_rec in c1
    loop
      -- check for whether the cm is available   
      begin
      select a.customer_trx_id, b.payment_schedule_id, b.amount_due_remaining
        into l_cm_trx_id, l_cm_ps_id, l_cm_due
        from ra_customer_trx_v a, ar_payment_schedules b
       where a.batch_source_id=l_cm_bs_id
         and a.ctt_class='CM'
         and a.complete_flag='Y'
         and a.status_trx='OP'
         and a.customer_trx_id=b.customer_trx_id
         and a.trx_number = substr(c1_rec.trx_number,1,length(c1_rec.trx_number)-7)
         and a.bill_to_customer_id=c1_rec.bill_to_customer_id;
        -- and b.amount_due_remaining = c1_rec.amount_due_remaining * -1;
         l_process := 'Y'; 
       exception
        when no_data_found or too_many_rows then
          fnd_file.put_line (fnd_file.OUTPUT,'Credit Memo not available for Debit Memo '||c1_rec.trx_number);
          l_process := 'N';
       end; 
      -- Process the cash receipt if all information are available
       IF l_process = 'Y' then
         --create_cash_receipt();
         AR_RECEIPT_API_PUB.create_cash
        ( p_api_version => 1.0,
        p_init_msg_list => FND_API.G_TRUE, 
        p_commit => FND_API.G_FALSE,
        p_validation_level => FND_API.G_VALID_LEVEL_FULL,
        x_return_status => l_return_status,
        x_msg_count => l_msg_count,
        x_msg_data => l_msg_data,
        p_currency_code => c1_rec.invoice_currency_code,
        p_amount => 0,
        p_receipt_number => c1_rec.trx_number, --substr(c1_rec.trx_number||'-'||c1_rec.trx_date,1,30),
        p_receipt_date => c1_rec.trx_date,
        p_gl_date => sysdate, --c1_rec.gl_date, Shankar 06-Sep-2012
        p_customer_id => c1_rec.bill_to_customer_id,
        p_receipt_method_id => l_receipt_method_id,
        p_cr_id => l_cash_receipt_id ); 
     
    
        IF l_return_status <> 'S' then
         fnd_file.put_line (fnd_file.OUTPUT,'===================================================');
          if l_msg_count = 1 Then
           fnd_file.put_line (fnd_file.OUTPUT,'Error Trx Number: '||c1_rec.trx_number||'='||l_msg_data);
          elsif l_msg_count > 1 Then
           loop
              l_count := l_count + 1;
              l_msg_data := FND_MSG_PUB.Get(FND_MSG_PUB.G_NEXT,FND_API.G_FALSE);
              if l_msg_data is NULL then
                  exit;
              end if;
              fnd_file.put_line (fnd_file.OUTPUT,'Error Trx Number: '||c1_rec.trx_number||'='||l_count||'='||l_msg_data);
           end loop;
           end if;   
          fnd_file.put_line (fnd_file.OUTPUT,'===================================================');
          rollback; 
        ELSE -- Create cash is successful         
         --apply_credit_memo();
          AR_RECEIPT_API_PUB.APPLY (
                  p_api_version                   => 1.0
                , p_init_msg_list                 => 'T'
                , p_commit                        => 'F'
                , p_validation_level              => 100
                , p_cash_receipt_id               => l_cash_receipt_id
                , p_applied_payment_schedule_id   => l_cm_ps_id
                , p_amount_applied                => l_cm_due --c1_rec.amount_due_remaining * -1
                , x_return_status                 => l_return_status
                , x_msg_count                     => l_msg_count
                , x_msg_data                      => l_msg_data);
            IF l_return_status <> 'S' then
             fnd_file.put_line (fnd_file.OUTPUT,'===================================================');
              if l_msg_count = 1 Then
               fnd_file.put_line (fnd_file.OUTPUT,'Error Trx Number: '||c1_rec.trx_number||'='||l_msg_data);
              elsif l_msg_count > 1 Then
               loop
                  l_count := l_count + 1;
                  l_msg_data := FND_MSG_PUB.Get(FND_MSG_PUB.G_NEXT,FND_API.G_FALSE);
                  if l_msg_data is NULL then
                      exit;
                  end if;
                  fnd_file.put_line (fnd_file.OUTPUT,'Error Trx Number: '||c1_rec.trx_number||'='||l_count||'='||l_msg_data);
               end loop;
               end if;   
              fnd_file.put_line (fnd_file.OUTPUT,'===================================================');
              rollback; 
            ELSE  -- Apply credit memo is successful 
              --apply_debit_memo;
              AR_RECEIPT_API_PUB.APPLY (
                      p_api_version                   => 1.0
                    , p_init_msg_list                 => 'T'
                    , p_commit                        => 'F'
                    , p_validation_level              => 100
                    , p_cash_receipt_id               => l_cash_receipt_id
                    , p_applied_payment_schedule_id   => c1_rec.payment_schedule_id
                    , p_amount_applied                => c1_rec.amount_due_remaining
                    , x_return_status                 => l_return_status
                    , x_msg_count                     => l_msg_count
                    , x_msg_data                      => l_msg_data);
                        IF l_return_status <> 'S' then
                         fnd_file.put_line (fnd_file.OUTPUT,'===================================================');
                          if l_msg_count = 1 Then
                           fnd_file.put_line (fnd_file.OUTPUT,'Error Trx Number: '||c1_rec.trx_number||'='||l_msg_data);
                          elsif l_msg_count > 1 Then
                           loop
                              l_count := l_count + 1;
                              l_msg_data := FND_MSG_PUB.Get(FND_MSG_PUB.G_NEXT,FND_API.G_FALSE);
                              if l_msg_data is NULL then
                                  exit;
                              end if;
                              fnd_file.put_line (fnd_file.OUTPUT,'Error Trx Number: '||c1_rec.trx_number||'='||l_count||'='||l_msg_data);
                           end loop;
                           end if;   
                          fnd_file.put_line (fnd_file.OUTPUT,'===================================================');
                          rollback; 
                        ELSE  -- Apply debit memo is successful 
                         fnd_file.put_line (fnd_file.OUTPUT,'===================================================');
                         fnd_file.put_line (fnd_file.OUTPUT,'Successfully Processed Trx: '||c1_rec.trx_number);
                         fnd_file.put_line (fnd_file.OUTPUT,'===================================================');
                          -- Update attribute15 to flag the trx as processed                         
                         update ra_customer_trx
                            set attribute14='Y'
                          where customer_trx_id=c1_rec.customer_trx_id; 
                         commit;
                        END IF; -- Apply debit memo is successful      
            END IF;-- Apply credit memo is successful
         END IF; -- create cash is successful
       END IF;  -- l_process = Y
    end loop;
exception
 when others then
   errbuf:= sqlerrm;
   retcode := 2;
end process_refund;
end xxwc_ar_prism_refund_pkg; 
/
