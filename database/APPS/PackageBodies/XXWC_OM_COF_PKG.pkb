CREATE OR REPLACE PACKAGE BODY APPS.xxwc_om_cof_pkg
/********************************************************************************
FILE NAME: APPS.XXWCAP_INV_INT_PKG.pkg
PROGRAM TYPE: PL/SQL Package Body
PURPOSE: APIs for Order Management Cash On Fly
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)         DESCRIPTION
------- -----------   ---------------   -----------------------------------------
1.0     01/08/2013    Gopi Damuluri     Initial version.
1.01    03/17/2013    Scott Spivey      jof_update_cust procedure: use outer join on hz_customer_profiles in cur_cust_detl cursor
                                        do not update primary bill-to information
                                        update profile information for bill-to site use id
                                        check for NULL parameter
1.02    04/23/2013    Scott Spivey      get_sales_rep function
                                        provide default 'No Sales Credit' if branch mappping doesn't exist
1.03    09/18/2013    Balaguru Seshadri Updates to tax exemption attributes occur only when the 
                                        states are same
1.04    09/24/2013    Gopi Damuluri     TMS# 20130911-00806 Added Freight Terms to the COF update
1.05   10/03/2013    Balaguru Seshadri  Updates to the hz_cust_acct_sites_all for attributes 1 and 2 must be wide open.
1.06   10/04/2013    Balaguru Seshadri  Add gsa_indicator flag 
2.0    10/15/2014    Gopi Damuluri      TMS# 20141001-00169 WC Canada: MultiOrg
********************************************************************************/
IS
   -- ********************************************************************************
   -- Function to get SalesPerson for a given InventoryOrg
   -- *******************************************************************************
   FUNCTION get_sales_rep (p_ship_from_org IN VARCHAR2, p_return_type IN VARCHAR2)
      RETURN VARCHAR2
   IS
      CURSOR salesrep_cur
      IS
         SELECT jrdv.resource_name salesrep_name, rsa.salesrep_id
           FROM jtf_rs_defresources_v jrdv, jtf_rs_salesreps_mo_v rsa -- Version# 2.0
          WHERE     1 = 1
                AND rsa.resource_id = jrdv.resource_id
                AND jrdv.source_number = p_ship_from_org
                AND ROWNUM = 1;

      -- start 1.02    04/23/2013    provide default 'No Sales Credit' if branch mappping doesn't exist
      l_salesrep_name   VARCHAR2 (240) := 'No Sales Credit';
      l_salesrep_id     NUMBER := -3;
   BEGIN
      OPEN salesrep_cur;

      FETCH salesrep_cur
         INTO l_salesrep_name, l_salesrep_id;

      CLOSE salesrep_cur;

      IF p_return_type = 'NAME'
      THEN
         RETURN l_salesrep_name;
      ELSE
         RETURN l_salesrep_id;
      END IF;
   -- end 1.02    04/23/2013    provide default 'No Sales Credit' if branch mappping doesn't exist
   EXCEPTION
      WHEN OTHERS
      THEN
         ----------------------------------------------------------------
         -- Calling ERROR API
         ----------------------------------------------------------------
         xxcus_error_pkg.xxcus_error_main_api (p_called_from         => l_package_name
                                              ,p_calling             => 'GET_SALES_REP - WHEN OTHERS Exception'
                                              ,p_request_id          => NULL
                                              ,p_ora_error_msg       => SQLERRM
                                              ,p_error_desc          => 'GET_SALES_REP - WHEN OTHERS Exception'
                                              ,p_distribution_list   => l_distro_list
                                              ,p_module              => 'OM');
         RETURN NULL;
   END get_sales_rep;

   -- ********************************************************************************
   -- Procedure to update CustomerSite information for Job On Fly
   --   p_site_use_id is the ship-to business usage for a site
   -- *******************************************************************************
   PROCEDURE jof_update_cust (p_site_use_id IN NUMBER)
   IS
      xxwc_error              EXCEPTION;
      l_org_id                NUMBER := NVL (fnd_global.org_id, 162);

      CURSOR cur_cust_detl
      IS
         SELECT hcas.attribute1 hcas_attribute1                                            -- Print_Prices_Ind
               ,hcas.attribute3 hcas_attribute3                                              -- Mandate_PO_Num
               ,hcas.attribute16 hcas_attribute16                                            -- Tax_Exempt_Ind
               ,hcas.attribute15 hcas_attribute15                                           -- Tax_Exempt_Type
               ,hcsu.attribute2 hcsu_attribute2                                                 -- Govt_Funded
               ,hcsu.attribute3 hcsu_attribute3                                           -- Thomas_Guide_Page
               ,hcsu.attribute4 hcsu_attribute4                                                -- Dodge_Number
               ,hcsu.primary_salesrep_id hcsu_primary_salesrep_id                       -- primary_salesrep_id
               ,hcp.attribute2 hcp_attribute2                                             -- Inv_Remit_To_Addr
               ,hcp.attribute3 hcp_attribute3                                                   -- Stmt_By_Job
               ,trim(translate (hl.state, '.1234567890', ' ')) state -- Added to handle the tax exemption certificate issue
               ,hcas.cust_acct_site_id                                                  -- rev 1.01  3/29/2013
               ,hcsu.site_use_id                                                        -- rev 1.01  3/29/2013
               ,hcsu.freight_term                                                       -- Rev 1.04
               ,hcsu.gsa_indicator   gsa_indicator                                      -- Rev 1.06
           FROM hz_cust_acct_sites hcas         -- Version# 2.0
               ,hz_cust_site_uses hcsu          -- Version# 2.0
               ,hz_customer_profiles hcp        -- Version# 2.0
               ,hz_party_sites hps
               ,hz_locations hl
          WHERE     1 = 1
                AND hps.party_site_id = hcas.party_site_id
                AND hps.location_id = hl.location_id
                AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                AND hcsu.site_use_code = 'BILL_TO'
                AND hcp.site_use_id(+) = hcsu.site_use_id                            -- rev 1.01    03/17/2013
                AND hcas.org_id = l_org_id                                             -- rev 1.01    4/4/2013
                AND hcas.status = 'A'                                                  -- rev 1.01    4/4/2013
                AND hcsu.status = 'A'                                                  -- rev 1.01    4/4/2013
                AND hcas.cust_account_id IN
                       (SELECT DISTINCT hcas_s.cust_account_id
                          FROM hz_cust_acct_sites hcas_s, hz_cust_site_uses hcsu_s   -- Version# 2.0
                         WHERE     hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
                               AND hcsu_s.site_use_id = p_site_use_id)
                AND hcsu.primary_flag = 'Y';

      l_hcas_attribute15      VARCHAR2 (50) := NULL;
      l_hcas_attribute16      VARCHAR2 (50) := NULL;
      l_bt_state              VARCHAR2 (50) := NULL;
      l_bill_to_site_use_id   NUMBER;
   BEGIN
      -- begin rev 1.01  4/16/2013
      IF p_site_use_id IS NULL
      THEN
         -- procedure called with NULL parameter
         RAISE xxwc_error;
      ELSE
         BEGIN
            SELECT bill_to_site_use_id
              INTO l_bill_to_site_use_id
              FROM hz_cust_site_uses  -- Version# 2.0
             WHERE site_use_id = p_site_use_id AND org_id = l_org_id;
         END;
      END IF;

      -- end rev 1.01  4/16/2013
      FOR rec_cust_detl IN cur_cust_detl
      LOOP
         --Rev 1.05 
         -- reset the variables for each row
         l_hcas_attribute15 :=Null;
         l_hcas_attribute16 :=Null;
         --Rev 1.05
         ----------------------------------------------------------------
         -- Update Customer Sites
         ----------------------------------------------------------------
         IF NVL (rec_cust_detl.hcas_attribute16, 'N') = 'Y' THEN
            BEGIN
               SELECT trim(translate (hl.state, '.1234567890', ' ')) 
               --Added to handle the tax exemption certificate issue
                 INTO l_bt_state
                 FROM hz_cust_acct_sites hcas -- Version# 2.0
                     ,hz_cust_site_uses hcsu -- Version# 2.0
                     ,hz_party_sites hps
                     ,hz_locations hl
                WHERE     1 = 1
                      AND hcsu.site_use_id = p_site_use_id
                      AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                      AND hps.party_site_id = hcas.party_site_id
                      AND hps.location_id = hl.location_id;
            EXCEPTION
             /*
             Added to handle the tax exemption certificate issue   
            */
               WHEN NO_DATA_FOUND THEN
                l_bt_state :='NONE';
               WHEN OTHERS THEN
                l_bt_state :='NONE';
            END;

            IF l_bt_state = rec_cust_detl.state THEN
             --
               l_hcas_attribute15 := rec_cust_detl.hcas_attribute15;
               l_hcas_attribute16 := rec_cust_detl.hcas_attribute16;
              /* 10/03/2013 --Rev 1.05 comment here to call wide open down below
             --
             -- Added to handle the tax exemption certificate issue
             -- The update has to happen only when the state match for the incoming ship to and the existing bill to   
             --              
                 BEGIN
                  SAVEPOINT square1; 
                    UPDATE hz_cust_acct_sites_all hcas
                       SET hcas.attribute1 = rec_cust_detl.hcas_attribute1                         -- Print_Prices_Ind
                          ,hcas.attribute3 = rec_cust_detl.hcas_attribute3                           -- Mandate_PO_Num
                          ,hcas.attribute15 = l_hcas_attribute15                                    -- Tax_Exempt_Type
                          ,hcas.attribute16 = l_hcas_attribute16                                     -- Tax_Exempt_Ind
                     WHERE     1 = 1
                           AND cust_acct_site_id != rec_cust_detl.cust_acct_site_id             -- rev 1.01  3/29/2013
                           AND cust_acct_site_id IN (SELECT DISTINCT hcsu_s.cust_acct_site_id
                                                       FROM hz_cust_site_uses_all hcsu_s
                                                      WHERE hcsu_s.site_use_id = p_site_use_id);

                    COMMIT;
                 EXCEPTION
                    WHEN OTHERS THEN                     
                     ROLLBACK TO square1;
                 END;
             */ --Rev 1.05  comment here to call wide open down below           
            ELSE
             -- DO NOT UPDATE
             -- Added to handle the  tax exemption certificate issue
             --     
             NULL;         
            END IF;
         END IF;
         --
         ---Rev 1.05
         -- Added to handle the tax exemption certificate issue
         -- The update has to happen only when the state match for the incoming ship to and the existing bill to   
         --              
             BEGIN
              SAVEPOINT square1; 
                UPDATE hz_cust_acct_sites hcas -- Version# 2.0
                   SET hcas.attribute1 = rec_cust_detl.hcas_attribute1                         -- Print_Prices_Ind
                      ,hcas.attribute3 = rec_cust_detl.hcas_attribute3                           -- Mandate_PO_Num
                      ,hcas.attribute15 = l_hcas_attribute15                                    -- Tax_Exempt_Type
                      ,hcas.attribute16 = l_hcas_attribute16                                     -- Tax_Exempt_Ind
                 WHERE     1 = 1
                       AND cust_acct_site_id != rec_cust_detl.cust_acct_site_id             -- rev 1.01  3/29/2013
                       AND cust_acct_site_id IN (SELECT DISTINCT hcsu_s.cust_acct_site_id
                                                   FROM hz_cust_site_uses hcsu_s -- Version# 2.0
                                                  WHERE hcsu_s.site_use_id = p_site_use_id);

                COMMIT;
             EXCEPTION
                WHEN OTHERS THEN                     
                 ROLLBACK TO square1;
             END;
         --Rev 1.05
         --  
         ----------------------------------------------------------------
         -- Update Bill-To / Ship-To Uses for site
         ----------------------------------------------------------------
         BEGIN 
          SAVEPOINT square1;
            UPDATE hz_cust_site_uses hcsu  -- Version# 2.0
               SET hcsu.attribute2    =rec_cust_detl.hcsu_attribute2                  -- Govt_Funded
                  ,hcsu.attribute3    =rec_cust_detl.hcsu_attribute3                  -- Thomas_Guide_Page
                  ,hcsu.attribute4    =rec_cust_detl.hcsu_attribute4                  -- Dodge_Number
                  ,hcsu.freight_term  =rec_cust_detl.freight_term                     -- Rev 1.04
                  ,hcsu.gsa_indicator =rec_cust_detl.gsa_indicator                    -- REV 1.06
             WHERE 1 = 1
               AND hcsu.cust_acct_site_id != rec_cust_detl.cust_acct_site_id      -- rev 1.01  3/29/2013
               --           AND hcsu.site_use_code     = 'BILL_TO'                -- rev 1.01  4/16/2013
               AND hcsu.cust_acct_site_id = (SELECT DISTINCT cust_acct_site_id    -- rev 1.01  4/16/2013
                                                   FROM hz_cust_site_uses         -- rev 1.01  4/16/2013 -- Version# 2.0
                                                  WHERE site_use_id = p_site_use_id); -- rev 1.01  4/16/2013

            COMMIT;
         EXCEPTION
            WHEN OTHERS THEN
               ROLLBACK TO square1;
         END;

         ----------------------------------------------------------------
         -- Update Customer Profiles for bill-to site use
         ----------------------------------------------------------------
         IF l_bill_to_site_use_id IS NOT NULL
         THEN                                                                           -- rev 1.01  4/16/2013
            BEGIN
             SAVEPOINT square1;
               UPDATE hz_customer_profiles hcp
                  SET hcp.attribute2 = rec_cust_detl.hcp_attribute2                       -- Inv_Remit_To_Addr
                     ,hcp.attribute3 = rec_cust_detl.hcp_attribute3                             -- Stmt_By_Job
                WHERE     1 = 1
                      AND hcp.site_use_id != rec_cust_detl.site_use_id                  -- rev 1.01  3/29/2013
                      AND hcp.site_use_id = l_bill_to_site_use_id;                      -- rev 1.01  4/16/2013

               COMMIT;
            EXCEPTION
               WHEN OTHERS THEN
                  ROLLBACK TO square1;
            END;
         END IF;                                                                        -- rev 1.01  4/16/2013
      END LOOP;
   EXCEPTION
      WHEN xxwc_error THEN               -- procedure was called with NULL parameter                    -- rev 1.01  4/16/2013
         NULL;                                                                          -- rev 1.01  4/16/2013
      WHEN OTHERS THEN
         ----------------------------------------------------------------
         -- Calling ERROR API
         ----------------------------------------------------------------
         xxcus_error_pkg.xxcus_error_main_api (p_called_from         => l_package_name
                                              ,p_calling             => 'JOF_UPDATE_CUST - WHEN OTHERS Exception'
                                              ,p_request_id          => NULL
                                              ,p_ora_error_msg       => SQLERRM
                                              ,p_error_desc          => 'JOF_UPDATE_CUST - WHEN OTHERS Exception'
                                              ,p_distribution_list   => l_distro_list
                                              ,p_module              => 'OM');
   END jof_update_cust;

   PROCEDURE update_cust_acct_name (p_customer_id    IN NUMBER
                                   ,p_account_name   IN VARCHAR2
                                   ,p_salesrep_id    IN NUMBER)
   IS
      l_cust_acct_site_id          NUMBER;
      l_object_version_number      NUMBER := 1;
      l_site_use_id                NUMBER;
      l_st_object_version_number   NUMBER := 1;
      l_st_site_use_id             NUMBER;
      l_error_message              VARCHAR2 (500);
      x_return_status              VARCHAR2 (1000);
      x_msg_count                  NUMBER;
      x_msg_data                   VARCHAR2 (1000);
      p_cust_account_rec           hz_cust_account_v2pub.cust_account_rec_type;
      p_cust_site_use_rec          hz_cust_account_site_v2pub.cust_site_use_rec_type;
   BEGIN
      p_cust_account_rec.cust_account_id := p_customer_id;
      p_cust_account_rec.account_name := p_account_name;

      ----------------------------------------------------------------
      -- Update Account Description of the Customer
      ----------------------------------------------------------------
      BEGIN
         hz_cust_account_v2pub.update_cust_account (p_init_msg_list           => fnd_api.g_true
                                                   ,p_cust_account_rec        => p_cust_account_rec
                                                   ,p_object_version_number   => l_object_version_number
                                                   ,x_return_status           => x_return_status
                                                   ,x_msg_count               => x_msg_count
                                                   ,x_msg_data                => x_msg_data);
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'Error - ' || SQLERRM);
      END;

      COMMIT;

      ----------------------------------------------------------------
      -- Update SalesPerson on Primary Bill-To site
      ----------------------------------------------------------------
      BEGIN
         x_return_status := NULL;
         x_msg_count := NULL;
         x_msg_data := NULL;

         SELECT hcsu.object_version_number, hcsu.site_use_id, hcsu.cust_acct_site_id
           INTO l_object_version_number, l_site_use_id, l_cust_acct_site_id
           FROM hz_cust_site_uses hcsu, hz_cust_acct_sites hcas  -- Version# 2.0
          WHERE     1 = 1
                AND hcsu.site_use_code = 'BILL_TO'
                AND hcsu.primary_flag = 'Y'
                AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = p_customer_id;

         p_cust_site_use_rec.site_use_id := l_site_use_id;
         p_cust_site_use_rec.primary_salesrep_id := p_salesrep_id;
         p_cust_site_use_rec.cust_acct_site_id := l_cust_acct_site_id;
         hz_cust_account_site_v2pub.update_cust_site_use (fnd_api.g_true
                                                         ,p_cust_site_use_rec
                                                         ,l_object_version_number
                                                         ,x_return_status
                                                         ,x_msg_count
                                                         ,x_msg_data);
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'Error - ' || SQLERRM);
      END;

      COMMIT;

      IF x_msg_count > 1
      THEN
         FOR i IN 1 .. x_msg_count
         LOOP
            l_error_message := i || '. ' || SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false), 1, 255);
            fnd_file.put_line (fnd_file.LOG, 'l_error_message - ' || l_error_message);
         END LOOP;
      END IF;

      ----------------------------------------------------------------
      -- Update SalesPerson on Ship-To site
      ----------------------------------------------------------------
      BEGIN
         x_return_status := NULL;
         x_msg_count := NULL;
         x_msg_data := NULL;

         SELECT hcsu.object_version_number, hcsu.site_use_id
           INTO l_st_object_version_number, l_st_site_use_id
           FROM hz_cust_site_uses hcsu  -- Version# 2.0
          WHERE     1 = 1
                AND hcsu.site_use_code = 'SHIP_TO'
                AND hcsu.cust_acct_site_id = l_cust_acct_site_id
                AND ROWNUM = 1;

         p_cust_site_use_rec.site_use_id := l_st_site_use_id;
         p_cust_site_use_rec.primary_salesrep_id := p_salesrep_id;
         p_cust_site_use_rec.cust_acct_site_id := l_cust_acct_site_id;
         hz_cust_account_site_v2pub.update_cust_site_use (fnd_api.g_true
                                                         ,p_cust_site_use_rec
                                                         ,l_st_object_version_number
                                                         ,x_return_status
                                                         ,x_msg_count
                                                         ,x_msg_data);
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'Error - ' || SQLERRM);
      END;

      COMMIT;

      IF x_msg_count > 1
      THEN
         FOR i IN 1 .. x_msg_count
         LOOP
            l_error_message := i || '. ' || SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false), 1, 255);
            fnd_file.put_line (fnd_file.LOG, 'l_error_message - ' || l_error_message);
         END LOOP;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         ----------------------------------------------------------------
         -- Calling ERROR API
         ----------------------------------------------------------------
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_package_name
           ,p_calling             => 'UPDATE_CUST_ACCT_NAME - WHEN OTHERS Exception'
           ,p_request_id          => NULL
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'UPDATE_CUST_ACCT_NAME - WHEN OTHERS Exception'
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'OM');
   END update_cust_acct_name;
END XXWC_OM_COF_PKG;
/
