CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_DESCARTES_OB_PKG
AS
/*************************************************************************
     $Header XXWC_OM_DESCARTES_OB_PKG $
     Module Name: XXWC_OM_DESCARTES_OB_PKG.pkb

     PURPOSE:   DESCARTES Project

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/05/2017  Rakesh Patel            TMS#20170811-00005-DMS Phase-2.0 Outbound Extract(Standard and Internal orders)
**************************************************************************/

   --Email Defaults
   g_dflt_email                fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_debug                     BOOLEAN := FALSE;
   g_retcode                   NUMBER;  
   g_err_msg                   VARCHAR2(3000);
   g_sqlcode                   NUMBER;   
   g_sqlerrm                   VARCHAR2(2000);
   g_message                   VARCHAR2(2000);
   g_package                   VARCHAR2(200) :=   'XXWC_OM_DESCARTES_OB_PKG';
   g_call_from                 VARCHAR2(200);
   g_module                    VARCHAR2 (80) := 'OM';
   
   g_wallet_path               VARCHAR2(100);
   g_descartes_username        VARCHAR2(100);
   g_descartes_passwordHash    VARCHAR2(100);
   g_descartes_organizationkey VARCHAR2(100);
   g_descartes_shipmentsvc_url VARCHAR2(2000);
   --g_request_id                NUMBER := fnd_global.conc_request_id;

   
   PROCEDURE write_output (p_message IN VARCHAR2)
   IS
   BEGIN
      --fnd_file.put_line (fnd_file.output, p_message);
      IF g_debug THEN
        DBMS_OUTPUT.PUT_LINE(p_message);
        fnd_file.put_line (fnd_file.log, p_message);
      END IF;  
   END;
   
   FUNCTION replace_special_characters( p_string in VARCHAR2 ) RETURN VARCHAR2 
   IS 
   BEGIN 
      RETURN 
        REPLACE(
           REPLACE(
                   REPLACE(
                           REPLACE(
                                   REPLACE(
                                           REGEXP_REPLACE( p_string
                                                          ,'[[:cntrl:]]'
                                                         )
                                           ,'&'
                                           ,'&amp;'
                                          )
                                  ,'<'
                                  ,'&lt;'
                                 ) 
                           ,'>'
                           ,'&gt;'
                          )              
                   ,''''
                   ,'&apos;'
                  )              
               ,'"'
               ,'&quot;'
              ); 
  END; 

  /*******************************************************************************
   Procedure Name  :   DEBUG_LOG
   Description     :   This function will be used for log messages 
   ******************************************************************************/
   PROCEDURE DEBUG_LOG (p_xxwc_log_rec IN OUT xxwc_log_rec )
   IS
       PRAGMA AUTONOMOUS_TRANSACTION;
       l_order_number XXWC.XXWC_DESCARTES_LOG_TBL.order_number%TYPE;
       l_update BOOLEAN := FALSE;
       
       l_sec           VARCHAR2 (2000);
   BEGIN
      l_sec := 'Start DEBUG_LOG';
      g_call_from := 'DEBUG_LOG';
      
      BEGIN
        SELECT order_number
          INTO l_order_number 
          FROM xxwc.xxwc_descartes_log_tbl
         WHERE interface_type             = 'OUTBOUND'
           AND service_name	              = p_xxwc_log_rec.service_name
           AND order_number               = p_xxwc_log_rec.order_number
           AND NVL(order_type, 1)         = NVL(p_xxwc_log_rec.order_type, 1)
           AND NVL(delivery_id, 1)        = NVL(p_xxwc_log_rec.delivery_id, 1)
           AND NVL(line_id, 1)            = NVL(p_xxwc_log_rec.line_id, 1)
           AND NVL(rpt_con_request_id, 1) = NVL(p_xxwc_log_rec.rpt_con_request_id, 1)
           AND ROWNUM=1;
         
         l_update := TRUE;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_update := FALSE;
      WHEN OTHERS THEN
        l_update := FALSE;
      END;   
        
      IF l_update = TRUE THEN
        UPDATE XXWC.XXWC_DESCARTES_LOG_TBL
           SET SERVICE_NAME            = p_xxwc_log_rec.service_name
              ,REQUEST_PAYLOAD         = p_xxwc_log_rec.request_payload
              ,RESPONSE_PAYLOAD        = p_xxwc_log_rec.response_payload
              ,WS_RESPONSECODE         = p_xxwc_log_rec.ws_responsecode
              ,WS_RESPONSEDESCRIPTION  = p_xxwc_log_rec.ws_responsedescription
              ,HTTP_STATUS_CODE        = p_xxwc_log_rec.http_status_code
              ,HTTP_REASON_PHRASE      = p_xxwc_log_rec.http_reason_phrase
              ,LAST_UPDATE_DATE        = SYSDATE
              ,LAST_UPDATED_BY         = FND_GLOBAL.user_id
              ,CONCURRENT_REQUEST_ID   = Fnd_global.conc_request_id 
              ,RPT_CON_REQUEST_ID      = p_xxwc_log_rec.rpt_con_request_id
         WHERE interface_type                  = 'OUTBOUND'
           AND order_number                    = p_xxwc_log_rec.order_number                
           AND service_name	                   = p_xxwc_log_rec.service_name
           AND NVL(order_type, 1)              = NVL(p_xxwc_log_rec.order_type, 1)
           AND NVL(delivery_id, 1)             = NVL(p_xxwc_log_rec.delivery_id, 1)
           AND NVL(line_id, 1)                 = NVL(p_xxwc_log_rec.line_id, 1);
      ELSE
         INSERT INTO XXWC.XXWC_DESCARTES_LOG_TBL
            (SEQUENCE_ID             ,
             SERVICE_NAME            ,
             INTERFACE_TYPE          ,
             ORDER_NUMBER            ,  
             ORDER_HEADER_ID         ,
             LINE_ID                 ,
             DELIVERY_ID             , 
             ORDER_TYPE              ,
             SCHEDULE_DELIVERY_DATE  ,
             RPT_CON_REQUEST_ID      ,
             REQUEST_PAYLOAD         ,
             RESPONSE_PAYLOAD        ,
             WS_RESPONSECODE         ,
             WS_RESPONSEDESCRIPTION  ,
             HTTP_STATUS_CODE        ,
             HTTP_REASON_PHRASE      ,
             CONCURRENT_REQUEST_ID   ,
             CREATION_DATE           ,
             CREATED_BY              ,
             LAST_UPDATE_DATE        ,
             LAST_UPDATED_BY         ,
             ATTRIBUTE1              ,
             ATTRIBUTE2              ,
             ATTRIBUTE3              ,
             ATTRIBUTE4              ,
             ATTRIBUTE5              
             )
         VALUES
             (
              XXWC.XXWC_DESCARTES_LOG_TBL_SEQ.NEXTVAL  ,
              p_xxwc_log_rec.service_name              ,
              'OUTBOUND'                               , 
              p_xxwc_log_rec.order_number              ,
              p_xxwc_log_rec.header_id                 ,
              p_xxwc_log_rec.line_id                   ,
              p_xxwc_log_rec.delivery_id               ,
              p_xxwc_log_rec.order_type                ,
              p_xxwc_log_rec.schedule_delivery_date    ,
              p_xxwc_log_rec.rpt_con_request_id        ,
              p_xxwc_log_rec.request_payload           ,
              p_xxwc_log_rec.response_payload          ,
              p_xxwc_log_rec.ws_responsecode           ,
              p_xxwc_log_rec.ws_responsedescription    ,
              p_xxwc_log_rec.http_status_code          ,
              p_xxwc_log_rec.http_reason_phrase        ,
              Fnd_global.conc_request_id               ,
              SYSDATE                                  ,
              FND_GLOBAL.user_id                       ,
              SYSDATE                                  ,
              FND_GLOBAL.user_id                       ,
              NULL                                     ,   
              NULL                                     ,
              NULL                                     ,
              NULL                                     ,
              NULL                                   
             ) ;  
      END IF;
      
      COMMIT;
      l_sec := 'End DEBUG_LOG';
  EXCEPTION
    WHEN OTHERS THEN
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in generating debug log ';
    
        xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                              , p_calling             => l_sec
                                              , p_ora_error_msg       => SQLERRM
                                              , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                              , p_distribution_list   => g_dflt_email
                                              , p_module              => g_module);

   
   END DEBUG_LOG;
   
   /*******************************************************************************
   Function Name   :   IS_DMS_ELIGIBLE
   Description     :   This function will be used to check if the data should be send to descartes or not
   ******************************************************************************/
   FUNCTION IS_DMS_ELIGIBLE ( p_con_program_name IN VARCHAR2 )
      RETURN VARCHAR2
   AS 
      l_count NUMBER :=0;
   BEGIN
      SELECT count(1) 
        INTO l_count 
        FROM fnd_lookup_values flv
       WHERE 1=1
         AND flv.lookup_code  = p_con_program_name
         AND flv.lookup_type  = 'XXWC_DMS_SCHEDULE_DEL_DATE'
         AND flv.enabled_flag = 'Y'
         AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
       
        IF l_count > 0 THEN
           RETURN 'Y';
        ELSE
           RETURN 'N';
        END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END IS_DMS_ELIGIBLE;
   
   /*******************************************************************************
   Function Name   :   IS_DMS_SHIP_METHOD
   Description     :   This function will be used to check if the data should be send to descartes or not
   ******************************************************************************/
   FUNCTION IS_DMS_SHIP_METHOD ( p_ship_method IN VARCHAR2 )
      RETURN VARCHAR2
   AS 
      l_count NUMBER :=0;
   BEGIN
      SELECT count(1) 
        INTO l_count 
        FROM fnd_lookup_values flv
       WHERE 1=1
         AND flv.meaning  = p_ship_method
         AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
         AND flv.enabled_flag = 'Y'
         AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
       
        IF l_count > 0 THEN
           RETURN 'Y';
        ELSE
           RETURN 'N';
        END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END IS_DMS_SHIP_METHOD;
   
   /*******************************************************************************
   Function Name   :   GET_DMS_ORG
   Description     :   This function will be used to get descarties depo
   ******************************************************************************/
   FUNCTION GET_DMS_ORG ( p_branch_code IN VARCHAR2 )
      RETURN VARCHAR2
   AS 
      l_dms_branch fnd_lookup_values.meaning%TYPE;
   BEGIN
      SELECT flv.meaning
        INTO l_dms_branch 
        FROM fnd_lookup_values flv
       WHERE 1=1
         AND flv.lookup_code  = p_branch_code
         AND flv.lookup_type  = 'XXWC_DMS_BRANCHES_LOOKUP'
         AND flv.enabled_flag = 'Y'
         AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
       
      RETURN l_dms_branch;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'XXX';
   END GET_DMS_ORG;
   
   PROCEDURE raise_so_cancel_line ( p_header_id IN NUMBER
                                   ,p_line_id  IN NUMBER
                                   ,p_error_msg  OUT VARCHAR2)

   IS PRAGMA AUTONOMOUS_TRANSACTION;

      l_header_id               NUMBER;
      l_line_id                 NUMBER;
      l_error_msg               VARCHAR2 (4000);
      l_parameter_list          wf_parameter_list_t;
      l_event_data              CLOB;
      l_flow_status_code        oe_order_headers_all.flow_status_code%TYPE;
      l_sec                     VARCHAR2 (100);
   BEGIN
      l_sec := 'Initialize Values';
      
      BEGIN
         SELECT ooh.flow_status_code
           INTO l_flow_status_code
           FROM oe_order_headers_all ooh
          WHERE 1 = 1
            AND ooh.header_id = p_header_id;
      EXCEPTION
      WHEN OTHERS THEN
        l_flow_status_code :=NULL;
      END;
      
      IF NVL(l_flow_status_code, 'XXX') <> 'CANCELLED' THEN
         l_header_id := p_header_id;
         l_line_id   := p_line_id;

         l_sec := 'Set values for Parameter List';
           l_parameter_list :=
               wf_parameter_list_t (
                  wf_parameter_t ('SEND_DATE',
                                  fnd_date.date_to_canonical (SYSDATE)),
                  wf_parameter_t ('P_REQUEST_TYPE', 'LINE'),                                  
                  wf_parameter_t ('P_HEADER_ID', l_header_id),
                  wf_parameter_t ('P_LINE_ID', l_line_id)
                            );

         l_sec := 'Raise business Event';
        
        wf_event.raise (
               p_event_name   => 'xxwc.oracle.apps.ont.order.cancel_hdr_line',
               p_event_key    => SYS_GUID (),
               p_event_data   => l_event_data,
               p_parameters   => l_parameter_list);

         COMMIT;
      END IF;  

   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DESCARTES_OB_PKG.raise_po_cancel_line',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');
            
         p_error_msg := 'Failed for cancel line '||SUBSTR (SQLERRM, 1, 240);
   END raise_so_cancel_line;
   
   PROCEDURE raise_so_cancel_order ( p_header_id IN NUMBER
                                    ,p_error_msg  OUT VARCHAR2)

   IS PRAGMA AUTONOMOUS_TRANSACTION;

      l_header_id               NUMBER;
      l_line_id                 NUMBER;
      l_error_msg               VARCHAR2 (4000);
      l_parameter_list          wf_parameter_list_t;
      l_event_data              CLOB;
      l_line_type_id            NUMBER;
      l_sec                     VARCHAR2 (100);
   BEGIN
      l_sec := 'Initialize Values';
      
      l_header_id := p_header_id;
      
      l_sec := 'Set values for Parameter List';
           l_parameter_list :=
               wf_parameter_list_t (
                  wf_parameter_t ('SEND_DATE',
                                  fnd_date.date_to_canonical (SYSDATE)),
                  wf_parameter_t ('P_REQUEST_TYPE', 'ORDER'),    
                  wf_parameter_t ('P_HEADER_ID', l_header_id)
                            );

      l_sec := 'Raise business Event';

      wf_event.raise (
               p_event_name   => 'xxwc.oracle.apps.ont.order.cancel_hdr_line',
               p_event_key    => SYS_GUID (),
               p_event_data   => l_event_data,
               p_parameters   => l_parameter_list);

      COMMIT;

   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DESCARTES_OB_PKG.raise_so_cancel_order',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');
            
         p_error_msg := 'Failed for cancel order '||SUBSTR (SQLERRM, 1, 240);
   END raise_so_cancel_order;
       
FUNCTION new_request(p_method        IN  VARCHAR2,
                     p_namespace     IN  VARCHAR2,
                     p_envelope_tag  IN  VARCHAR2 DEFAULT 'soapenv')
  RETURN t_request AS
-- ---------------------------------------------------------------------
  l_request  t_request;
BEGIN
  l_request.method       := p_method;
  l_request.namespace    := p_namespace;
  l_request.envelope_tag := p_envelope_tag;
  RETURN l_request;
END;

PROCEDURE add_parameter(p_request  IN OUT NOCOPY  t_request,
                        p_name     IN             VARCHAR2,
                        p_value    IN             VARCHAR2,
                        p_type     IN             VARCHAR2 := NULL) AS
-- ---------------------------------------------------------------------
BEGIN
  IF p_type IS NULL THEN
    p_request.body := p_request.body||'<'||p_name||'>'||p_value||'</'||p_name||'>';
  ELSE
    p_request.body := p_request.body||'<'||p_name||' xsi:type="'||p_type||'">'||p_value||'</'||p_name||'>';
  END IF;
END;

PROCEDURE add_complex_parameter(p_request  IN OUT NOCOPY  t_request,
                                p_xml      IN             VARCHAR2) AS
BEGIN
  p_request.body := p_request.body||p_xml;
END;

PROCEDURE add_toclob(p_footer_clob  IN OUT NOCOPY  CLOB,
                     p_name     IN             VARCHAR2,
                     p_value    IN             VARCHAR2) AS
BEGIN
   p_footer_clob := p_footer_clob||'<'||p_name||'>'||p_value||'</'||p_name||'>';
END;

PROCEDURE add_toclob_complex_parameter(p_footer_clob  IN OUT NOCOPY  CLOB,
                                       p_xml      IN             VARCHAR2) AS
BEGIN
  p_footer_clob := p_footer_clob||p_xml;
END;

PROCEDURE generate_envelope(p_request  IN OUT NOCOPY  t_request,
		                    p_env      IN OUT NOCOPY  VARCHAR2) AS
BEGIN
  p_env := '<'||p_request.envelope_tag||':Envelope xmlns:'||p_request.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/" ' ||
               'xmlns:ship="http://shipment.services.operation.cuberoute.com">' ||
              '<'||p_request.envelope_tag||':Header/>' ||  
             '<'||p_request.envelope_tag||':Body>' ||
               '<'||p_request.method||'>'|| p_request.body ||
               '</'||p_request.method||'>' ||
             '</'||p_request.envelope_tag||':Body>' ||
           '</'||p_request.envelope_tag||':Envelope>';
END;

PROCEDURE show_envelope(p_env     IN  VARCHAR2,
                        p_heading IN  VARCHAR2 DEFAULT NULL) AS
  i      PLS_INTEGER;
  l_len  PLS_INTEGER;
BEGIN
  IF g_debug THEN
    IF p_heading IS NOT NULL THEN
      DBMS_OUTPUT.put_line('*****' || p_heading || '*****');
    END IF;

    i := 1; l_len := LENGTH(p_env);
    WHILE (i <= l_len) LOOP
      DBMS_OUTPUT.put_line(SUBSTR(p_env, i, 10000));
      i := i + 10000;
    END LOOP;
  END IF;
END;

PROCEDURE check_fault(p_response     IN OUT NOCOPY  t_response
                     ,p_xxwc_log_rec IN OUT xxwc_log_rec) 
AS
  l_fault_node    XMLTYPE;
  l_fault_code    VARCHAR2(256);
  l_fault_string  VARCHAR2(32767);
  l_error_code           NUMBER;
  p_error_msg            VARCHAR2(2000);
  l_sec                  VARCHAR2(2000);
BEGIN
  l_sec := 'Start check_fault';  
  l_fault_node := p_response.doc.extract('/'||p_response.envelope_tag||':Fault',
                                         'xmlns:'||p_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/');

  IF (l_fault_node IS NOT NULL) THEN
    l_sec := 'Inside l_fault_node condition';  
    l_fault_code   := l_fault_node.extract('/'||p_response.envelope_tag||':Fault/faultcode/child::text()',
                                           'xmlns:'||p_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/').getstringval();
    l_fault_string := l_fault_node.extract('/'||p_response.envelope_tag||':Fault/faultstring/child::text()',
                                           'xmlns:'||p_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/').getstringval();
   
    fnd_file.put_line (fnd_file.log, 'l_fault_code : '|| l_fault_code);
    fnd_file.put_line (fnd_file.log, 'l_fault_string : '|| l_fault_string);

    p_xxwc_log_rec.ws_responsecode := l_fault_code;
    p_xxwc_log_rec.ws_responsedescription := SUBSTR(l_fault_string, 1, 4000);
  END IF;
  l_sec := 'End check_fault';  
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the check_fault procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in check_fault is '||l_error_code);
      fnd_file.put_line(fnd_file.log, 'Exception Block of check_fault Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.check_fault',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	 
END;

PROCEDURE check_success_response(p_response     IN OUT NOCOPY  t_response
                        ,p_xxwc_log_rec IN OUT xxwc_log_rec) 
AS
  l_success_node         XMLTYPE;
  l_success_code         VARCHAR2(256);
  l_success_description  VARCHAR2(32767);
  
  l_error_code           NUMBER;
  p_error_msg            VARCHAR2(2000);
  l_sec                  VARCHAR2(2000);
BEGIN
  l_sec := 'Start check_success_response';  
  l_success_node := p_response.doc.extract('/'||'ns2:'||'shipmentResponse',
                                         'xmlns:'||'ns2'||'="http://shipment.services.operation.cuberoute.com');

  IF (l_success_node IS NOT NULL) THEN
    l_sec := 'Inside l_success_node condition';
    l_success_code   := l_success_node.extract('/'||'ns2:'||'shipmentResponse/return/response/responseCode/child::text()',
                                           'xmlns:'||'ns2'||'="http://shipment.services.operation.cuberoute.com').getstringval();
    fnd_file.put_line (fnd_file.log, 'l_success_code : '|| l_success_code);
    p_xxwc_log_rec.ws_responsecode := l_success_code;   
    /**l_success_description  := l_success_node.extract('/'||'ns2:'||'shipmentResponse/return/response/responseDescription/child::text()',
                                           'xmlns:'||'ns2'||'="http://shipment.services.operation.cuberoute.com').getstringval();
                                                                                      
    write_output('l_success_description : '|| l_success_description);
    p_xxwc_log_rec.ws_responsedescription := SUBSTR(l_success_description, 1, 3500);**/
  END IF;
  l_sec := 'End check_success_response';                                  
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the check_success_response procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in check_success_response is '||l_error_code );
      fnd_file.put_line(fnd_file.log, 'Exception Block of check_success_response Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.check_success_response',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	 
END;

PROCEDURE invoke( p_request      IN OUT NOCOPY  t_request
                 ,p_url          IN             VARCHAR2
                 ,p_action       IN             VARCHAR2
                 ,p_xxwc_log_rec IN OUT xxwc_log_rec
                ) 
AS
  l_envelope       CLOB;
  l_http_request   UTL_HTTP.req;
  l_http_response  UTL_HTTP.resp;
  l_response       t_response;
  --lv_wallet_path      VARCHAR2(100) := 'file:/obase/ebsdev/admin/EBSDEV/wallet';
  -- lv_wallet_pwd       VARCHAR2(30) := 'Summer101';
  
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(4000);
  l_sec                      VARCHAR2 (4000);
  
  l_req_length      BINARY_INTEGER;
  l_amount          PLS_INTEGER := 2000; 
  l_offset          PLS_INTEGER := 1;
  l_buffer          VARCHAR2(4000);
  
  l_resp_length     BINARY_INTEGER;
  l_resp_envelope   CLOB;
BEGIN
  l_sec := 'Srart invoke';
  
  fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
  write_output('p_url : '|| p_url);

  generate_envelope(p_request, l_envelope);
  show_envelope(l_envelope, 'Request');
  p_xxwc_log_rec.request_payload := l_envelope;
  
  l_sec := 'Before set_wallet';
  --Setting the certificate by accessing the wallet
  UTL_HTTP.set_wallet(g_wallet_path);
  
  l_sec := 'Before begin_request';
  l_http_request := UTL_HTTP.begin_request(p_url, 'POST','HTTP/1.1');
  UTL_HTTP.set_header(l_http_request, 'Content-Type', 'text/xml');
  
  l_req_length := LENGTH(l_envelope); 
  write_output('l_req_length : '|| l_req_length);
  
  l_sec := 'Before write_text';
  --If message data is under 32kb limit
  IF l_req_length <= 32767
  THEN
     UTL_HTTP.set_header(l_http_request, 'Content-Length', LENGTH(l_envelope));
     UTL_HTTP.set_header(l_http_request, 'SOAPAction', p_action);
     UTL_HTTP.write_text(l_http_request, l_envelope);
  ELSE
     --If message data is more than 32kb limit
     UTL_HTTP.set_header(l_http_request, 'Transfer-Encoding', 'Chunked');
     
     WHILE(l_offset < l_req_length) 
     LOOP
        DBMS_LOB.read(l_envelope
                     ,l_amount
                     ,l_offset
                     ,l_buffer 
                     );
        UTL_HTTP.write_text(l_http_request, l_buffer);
        l_offset := l_offset + l_amount;    
     END LOOP;

  END IF;   
  
  l_sec := 'Before UTL_HTTP.get_response';  
  
  write_output('Before UTL_HTTP.get_response '); 
  l_http_response := UTL_HTTP.get_response(l_http_request);
  
  --Initialize the CLOB.
  DBMS_LOB.createtemporary(l_resp_envelope,TRUE);
  
  l_sec := 'Before loop to read get_response';  
  --Copy the response into the CLOB.
  BEGIN
    LOOP
      --Reading the text from the http_resp
      UTL_HTTP.read_text(l_http_response, l_buffer, 32767);
      DBMS_LOB.writeappend(l_resp_envelope, LENGTH(l_buffer), l_buffer);
    END LOOP;
  EXCEPTION
  WHEN UTL_HTTP.end_of_body THEN
    UTL_HTTP.end_response(l_http_response);
  END;
  
  l_sec := 'After loop to read get_response';
  
  --print the response 
  show_envelope(l_resp_envelope, 'Response');
  
  --assign to insert in the log table
  p_xxwc_log_rec.response_payload   := l_resp_envelope;
  p_xxwc_log_rec.http_status_code   := l_http_response.status_code;
  p_xxwc_log_rec.http_reason_phrase := l_http_response.reason_phrase;
  
  l_response.doc := XMLTYPE.createxml(l_resp_envelope);
  l_response.envelope_tag := p_request.envelope_tag;
  
  show_envelope(l_envelope, 'Request');
    
  -- Verify the response status and text
  write_output('Response Status: '||l_http_response.status_code||' ' || l_http_response.reason_phrase);
                   
  l_response.doc := l_response.doc.extract('/'||l_response.envelope_tag||':Envelope/'||l_response.envelope_tag||':Body/child::node()',
                                           'xmlns:'||l_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/"');
  --print the response 
  l_sec := 'Before check_fault';  
  check_fault(l_response, p_xxwc_log_rec );
  
  l_sec := 'Before check_success_response';  
  check_success_response(l_response, p_xxwc_log_rec );
  
  DBMS_LOB.freetemporary(l_resp_envelope);
  l_sec := 'end invoke';
EXCEPTION
WHEN utl_http.request_failed THEN
   g_err_msg                             := SUBSTR(UTL_HTTP.GET_DETAILED_SQLERRM,1,255);
   p_xxwc_log_rec.ws_responsecode        := 'ERROR';
   p_xxwc_log_rec.ws_responsedescription := g_err_msg;
   write_output( 'Inside UTL_HTTP.request_failed' || g_err_msg);
WHEN UTL_HTTP.http_server_error THEN
   g_err_msg                             := SUBSTR(UTL_HTTP.GET_DETAILED_SQLERRM,1,255);
   p_xxwc_log_rec.ws_responsecode        := 'ERROR';
   p_xxwc_log_rec.ws_responsedescription := g_err_msg;
   write_output( 'Inside UTL_HTTP.http_server_error' || g_err_msg);
WHEN UTL_HTTP.http_client_error THEN
   g_err_msg                             := SUBSTR(UTL_HTTP.GET_DETAILED_SQLERRM,1,255);
   p_xxwc_log_rec.ws_responsecode        := 'ERROR';
   p_xxwc_log_rec.ws_responsedescription := g_err_msg;
   write_output( 'Inside UTL_HTTP.http_client_error' || g_err_msg);
WHEN UTL_HTTP.end_of_body THEN
  UTL_HTTP.END_RESPONSE(l_http_response);  
   g_err_msg                             := 'Inside UTL_HTTP.end_of_body';
   p_xxwc_log_rec.ws_responsecode        := 'ERROR';
   p_xxwc_log_rec.ws_responsedescription := g_err_msg;
   write_output(g_err_msg);
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the invoke procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in check_success_response is '||l_error_code );
      fnd_file.put_line(fnd_file.log, 'Exception Block of invoke Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.invoke',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	 
END;

FUNCTION get_return_value(p_response   IN OUT NOCOPY  t_response,
                          p_name       IN             VARCHAR2,
                          p_namespace  IN             VARCHAR2)
  RETURN VARCHAR2 AS
BEGIN
  RETURN p_response.doc.extract('//'||p_name||'/child::text()',p_namespace).getstringval();
END;

PROCEDURE debug_on AS
BEGIN
  g_debug := TRUE;
END;

PROCEDURE debug_off AS
BEGIN
  g_debug := FALSE;
END;

PROCEDURE ShipmentSvc_std_int_ord  (p_order_number      IN NUMBER,
                                    p_sch_delivery_date IN DATE,
                                    p_delivery_id       IN NUMBER,
                                    p_request_id        IN NUMBER,
                                    p_xxwc_log_rec      IN OUT xxwc_log_rec )
AS
  l_request   XXWC_OM_DESCARTES_OB_PKG.t_request;
  l_response  XXWC_OM_DESCARTES_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(2000);
  l_sec                      VARCHAR2 (2000);
  
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  l_print_invoice_tag        BOOLEAN := TRUE;
  
  l_order_number             NUMBER := -1;
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_invoice_close            VARCHAR2(32767); 
  l_footer_clob              CLOB; 
  
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  CURSOR c_order_info ( p_order_number IN NUMBER
                       ,p_delivery_id  IN NUMBER )
   IS
   SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,wsh_stg.delivery_id
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( XXWC_OM_SO_CREATE.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      --ool.line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      --,msib.unit_weight ITEM_UNIT_WEIGHT
      ,NVL(wsh_stg.transaction_qty, 0) * NVL(msib.unit_weight, 0) ITEM_UNIT_WEIGHT
      ,NVL(wsh_stg.transaction_qty, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
     FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib
       , xxwc_wsh_shipping_stg wsh_stg
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND wsh_stg.header_id               = ooh.header_id
        AND wsh_stg.line_id                 = ool.line_id 
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.order_number                = p_order_number
        AND wsh_stg.delivery_id             = p_delivery_id
        AND ool.user_item_description       = 'OUT_FOR_DELIVERY'
        AND ool.source_type_code            = 'INTERNAL'
        --AND wsh_stg.delivery_id             = NVL(p_delivery_id, wsh_stg.delivery_id)
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )
        ORDER BY ooh.order_number, wsh_stg.delivery_id;
BEGIN
  l_sec := 'Srart ShipmentSvc_std_int_ord p_order_number: ' ||p_order_number || ' p_delivery_id: '||p_delivery_id;
  l_url         := g_descartes_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_result_name := 'return';
  
  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DESCARTES_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before ShipmentSvc_std_int_ord';                                                  
  FOR r_order_info IN c_order_info ( p_order_number => p_order_number
                                    ,p_delivery_id  => p_delivery_id)
  LOOP    
     l_data_found := TRUE; -- set to true if to generate xml file
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' Delivery_id : '||r_order_info.Delivery_id ||' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);
     
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'operationCode',
                         p_value   => 'Edit');
 
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'addressLine1',
                         p_value   => r_order_info.JOB_SITE_ADD);
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'city',
                         p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'phoneNumber1',
                         p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'postalCode',
                         p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'stateCode',
                         p_value   => r_order_info.JOB_SITE_ADD_STATE);
                                                                       
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet1',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet2',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
                                                         
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

     END IF;                                                   

     IF l_order_number <> r_order_info.ORDER_NUMBER THEN
        --Check if delivery is different and it is not a first record, then close the invoice element    
        IF l_order_number <> -1 THEN
          --start close the invoices element        
          XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</invoices>');
                                                   
          --end close the invoices element   
        END IF;
        
        l_order_number := r_order_info.ORDER_NUMBER;
        l_print_invoice_tag := TRUE;
             
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);
     END IF;                  

     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER);   
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER); 
                             
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_print_invoice_tag = TRUE THEN
        l_print_invoice_tag := FALSE;
        
        l_invoice_close := NULL;
        
        --start close the invoices element        
        --delivery_id as attribute value -- invoices -->attributes
        XXWC_OM_DESCARTES_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_invoice_close ,
                                                                   p_xml    => '</invoices>');
        --end close the invoices element      
     END IF;

     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => 'CustomerDelivery');  
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'internalOrderId',
                         p_value   => r_order_info.delivery_id);  
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
        
        fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
        l_dms_branch_city := XXWC_OM_DESCARTES_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
        fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                   
     END IF ;  
     l_sec := 'End Loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'Afer Loop r_order_info';
  
  l_request.body := l_request.body||l_invoice_close;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
  --Order Type                              
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');                                                  
  --Request id                              
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_request_id);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Request id');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');    
                                                                      
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_descartes_username);                       
        
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_descartes_passwordHash); 
                         
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_descartes_organizationkey);   
                         
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');

     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DESCARTES_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;                                   
  l_sec := 'End procedure ShipmentSvc_std_int_ord';  
   
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_std_int_ord procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_std_int_ord is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_std_int_ord Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_std_int_ord',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
  END ShipmentSvc_std_int_ord;

PROCEDURE ShipmentSvc_rental_ord  ( p_order_number      IN NUMBER
                                   ,p_shipment_type     IN VARCHAR2 
                                   ,p_line_type_id      IN NUMBER  
                                   ,p_flow_status_code  IN VARCHAR2     
                                   ,p_sch_delivery_date IN DATE
                                   ,p_rental_return     IN NUMBER DEFAULT NULL
                                   ,p_request_id        IN NUMBER
                                   ,p_xxwc_log_rec      IN OUT xxwc_log_rec
                                   )
AS
  l_request   XXWC_OM_DESCARTES_OB_PKG.t_request;
  l_response  XXWC_OM_DESCARTES_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code    NUMBER;
  p_error_msg     VARCHAR2(2000);
  l_sec           VARCHAR2 (2000);
	
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_footer_clob              CLOB; 
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  
  CURSOR c_order_info ( p_order_number  IN NUMBER 
                       ,p_line_type_id  IN NUMBER
                       ,p_flow_status_code  IN VARCHAR2 
                       ,p_rental_return IN NUMBER
                      )
   IS
   SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( XXWC_OM_SO_CREATE.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      --,ool.line_number
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
   FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib       
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ool.source_type_code            = 'INTERNAL'
        AND ooh.order_number                = p_order_number
        AND ool.line_type_id                = p_line_type_id   
        AND ool.flow_status_code            = NVL(p_flow_status_code , ool.flow_status_code) 
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )
    UNION
      SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( XXWC_OM_SO_CREATE.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      --,ool.line_number
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
   FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib  
       , fnd_lookup_values flv     
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.order_number                = p_order_number
        --AND ool.line_type_id                = p_line_type_id  
        AND flv.meaning                     = msib.segment1
        AND flv.lookup_type                 = 'XXWC_DMS_RENTAL_ITEMS'
        AND ool.flow_status_code            <> 'CLOSED'
        AND flv.enabled_flag                = 'Y'
        AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
        AND 1                               =  p_rental_return
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             );        
BEGIN
  l_sec         := 'Srart ShipmentSvc_rental_ord p_order_number: ' ||p_order_number;
  l_url         := g_descartes_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_result_name := 'return';

  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DESCARTES_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  l_sec := 'Before r_order_info';        
          
  FOR r_order_info IN c_order_info ( p_order_number     => p_order_number
                                    ,p_line_type_id     => p_line_type_id
                                    ,p_flow_status_code => p_flow_status_code
                                    ,p_rental_return    => p_rental_return
                                   )
  LOOP  
     l_data_found := TRUE; -- set to true if to generate xml file
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);

     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'operationCode',
                         p_value   => 'Edit');
 
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'addressLine1',
                         p_value   => r_order_info.JOB_SITE_ADD);
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'city',
                         p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'phoneNumber1',
                         p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'postalCode',
                         p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'stateCode',
                         p_value   => r_order_info.JOB_SITE_ADD_STATE);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet1',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);                 
                                                                       
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet2',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
                                                         
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        --Order number, job site contact and comment should be printed only one time
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
                         
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                          p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);   
    END IF;                                                   

     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER); 
                           
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER);                          
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     --item_uom as attribute value -- items -->item_uom    
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        XXWC_OM_DESCARTES_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_footer_clob ,
                                                   p_xml    => '</invoices>');      
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => p_shipment_type);  
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
        
       fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
       l_dms_branch_city := XXWC_OM_DESCARTES_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
       fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

       XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                     
     END IF ;  
     l_sec := 'End loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'After loop r_order_info';
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
  
  --Order Type                              
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   
                                                                      
  --Request id                              
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_request_id);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Request id');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_descartes_username);                       
        
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_descartes_passwordHash); 
                         
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_descartes_organizationkey);   
                         
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');
     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DESCARTES_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;  

  l_sec := 'End ShipmentSvc_rental_ord';                                  
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_rental_ord procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_rental_ord is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_rental_ord Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_rental_ord',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
END ShipmentSvc_rental_ord;

PROCEDURE ShipmentSvc_return_ord  ( p_order_number      IN NUMBER
                                   ,p_shipment_type     IN VARCHAR2  
                                   ,p_sch_delivery_date IN DATE
                                   ,p_request_id        IN NUMBER
                                   ,p_xxwc_log_rec      IN OUT xxwc_log_rec
                                   )
AS
  l_request   XXWC_OM_DESCARTES_OB_PKG.t_request;
  l_response  XXWC_OM_DESCARTES_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code    NUMBER;
  p_error_msg     VARCHAR2(2000);
  l_sec           VARCHAR2 (2000);
	
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_footer_clob              CLOB; 
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  
  CURSOR c_order_info ( p_order_number IN NUMBER )
   IS
    SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( XXWC_OM_SO_CREATE.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      --,ool.line_number
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
  FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib       
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ool.source_type_code            = 'INTERNAL'
        AND ooh.order_number                = p_order_number
        AND ool.flow_status_code            NOT IN ( 'CANCELLED', 'CLOSED') 
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             );
BEGIN
  l_sec := 'Srart ShipmentSvc_return_ord p_order_number: ' ||p_order_number;

  l_url         := g_descartes_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_result_name := 'return';
 
  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DESCARTES_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before r_order_info';                                                 
  FOR r_order_info IN c_order_info ( p_order_number => p_order_number
                                   )
  LOOP    
    l_data_found := TRUE; -- set to true if to generate xml file
    l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
    write_output (l_sec);
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'operationCode',
                         p_value   => 'Edit');
 
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'addressLine1',
                         p_value   => r_order_info.JOB_SITE_ADD);
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'city',
                         p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'phoneNumber1',
                         p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'postalCode',
                         p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'stateCode',
                         p_value   => r_order_info.JOB_SITE_ADD_STATE);
                                                                       
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet1',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet2',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
                                                         
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        --Order number, job site contact and comment should be printed only one time
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
                         
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                          p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);   
    END IF;                                                   

     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER); 
                           
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER);                          
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     --item_uom as attribute value -- items -->item_uom    
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        XXWC_OM_DESCARTES_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_footer_clob ,
                                                   p_xml    => '</invoices>');      
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => p_shipment_type);  
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
                         
       fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
       l_dms_branch_city := XXWC_OM_DESCARTES_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
       fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

       XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                       
     END IF ;  
     l_sec := 'End loop r_order_info';                                                                                     
  END LOOP;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                     
  --Order Type                              
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   
  --Request id                              
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_request_id);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Request id');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                                                                    
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_descartes_username);                       
        
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_descartes_passwordHash); 
                         
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_descartes_organizationkey);   
                         
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');
     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DESCARTES_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;  
 
  l_sec := 'End ShipmentSvc_return_ord';                        
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_retrun_ord procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_return_ord is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_return_ord Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_return_ord',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
END ShipmentSvc_return_ord;

  /********************************************************************************
  ProcedureName : Send_to_DMS
  Purpose       : This will be called by printer form
  ----------------------------------------------------------------------------------*/
  PROCEDURE Send_to_DMS(   p_conc_program_name IN VARCHAR2
                          ,p_order_header_id   IN NUMBER
                          ,p_delivery_id       IN NUMBER
                          ,p_sch_delivery_date IN DATE
                          ,p_request_id        IN NUMBER )
  AS
    
    l_error_code         NUMBER;
    p_error_msg          VARCHAR2(2000);
	l_sec                VARCHAR2 (150);
	l_sch_delivery_date  VARCHAR2(11);
    l_Shipment_Type      VARCHAR2 (150);
    l_request_id         NUMBER := 0;
    l_line_count         NUMBER := 0;
    l_order_type_id      NUMBER := 0;
  BEGIN
     XXWC_OM_DESCARTES_OB_PKG.debug_on;
    
     l_sec := 'Start of Procedure : Send_to_DMS';
     
     l_sch_delivery_date := TO_CHAR (p_sch_delivery_date, 'DD-MON-YYYY');
     
     BEGIN
        SELECT ooh.order_type_id
          INTO l_order_type_id
          FROM oe_order_headers_all ooh
         WHERE 1 = 1
           AND ooh.header_id = p_order_header_id;
      EXCEPTION
      WHEN OTHERS THEN
        l_order_type_id :=0;
      END;
    
     IF p_conc_program_name IN ( 'XXWC_OM_PACK_SLIP' , 'XXWC_INT_ORD_PACK' )
     THEN
	    SELECT COUNT(1)
		  INTO l_line_count
		 FROM oe_order_headers_all ooh
             ,oe_order_lines_all ool
        WHERE ooh.header_id                   = ool.header_id
          AND ooh.header_id                   = p_order_header_id
          AND ool.user_item_description       = 'OUT_FOR_DELIVERY'
          AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                                  );
		 
        fnd_global.apps_initialize (fnd_global.user_id,
                                      fnd_global.resp_id,
                                      fnd_global.resp_appl_id
                                      );  
                                 
		IF l_line_count > 0 THEN
	        l_request_id :=  fnd_request.submit_request (application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DESCARTES_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id,
	                                                   argument2        => l_sch_delivery_date,
	                                                   argument3        => p_delivery_id,
	                                                   argument4        => 'CustomerDelivery',
	                                                   argument5        => 'SEND_TO_DMS_SO_STD',
  	                                                   argument6        => FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_DEBUG_FLAG'),
  	                                                   argument7        => p_request_id
	                                                 );	
          --Commit Updates 	
          xxwc_ascp_scwb_pkg.proccommit;
       END IF;
	 END IF; --'XXWC_OM_PACK_SLIP'
	
	 IF p_conc_program_name IN ('XXWC_OM_SRECEIPT', 'XXWC_OM_SRECEIPT_CUSTOMER') 
	 AND l_order_type_id = FND_PROFILE.VALUE ('XXWC_RETURN_ORDER_TYPE') 
	 THEN
   		 SELECT COUNT(1)
		   INTO l_line_count
		   FROM oe_order_headers_all ooh
               ,oe_order_lines_all ool
          WHERE ooh.header_id                   = ool.header_id
            AND ooh.order_type_id               = l_order_type_id
            AND ooh.header_id                   = p_order_header_id
            AND EXISTS                      ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                                );
		   IF l_line_count > 0 THEN
  	          l_request_id :=  fnd_request.submit_request (application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DESCARTES_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id,
	                                                   argument2        => l_sch_delivery_date,
	                                                   argument3        => p_delivery_id,
	                                                   argument4        => 'CustomerPickup',
	                                                   argument5        => 'SEND_TO_DMS_SO_RETURN',
  	                                                   argument6        =>  FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_DEBUG_FLAG'),
  	                                                   argument7        => p_request_id
	                                                 );	
            --Commit Updates 	
            xxwc_ascp_scwb_pkg.proccommit;
          END IF;
  	END IF; --Retrun Order condition
  	                                     
    IF p_conc_program_name IN ('XXWC_OM_SHORTRENT_ACKL', 'XXWCRRWSHEET_ITEM', 'XXWC_OM_RENTAL_PS_RCT1') 
	 AND l_order_type_id IN (1013, 1014)--FND_PROFILE.VALUE ('XXWC_WC_SHORT_TERM_RENTAL_TYPE') 
	 THEN
	     IF p_conc_program_name IN ('XXWC_OM_SHORTRENT_ACKL', 'XXWC_OM_RENTAL_PS_RCT1')
	     THEN
	        l_Shipment_Type := 'CustomerDelivery';
	     ELSIF  p_conc_program_name = 'XXWCRRWSHEET_ITEM'   
	     THEN   
	        l_Shipment_Type := 'CustomerPickup';
	     END IF;
	     
   		 SELECT COUNT(1)
		   INTO l_line_count
		   FROM oe_order_headers_all ooh
               ,oe_order_lines_all ool
          WHERE ooh.header_id                   = ool.header_id
            AND ooh.order_type_id               = l_order_type_id
            AND ooh.header_id                   = p_order_header_id
            AND EXISTS                      ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                                );
		   --insert into XXWC_CLOB_TEMP(step) values ('l_line_count : '||l_line_count );
		   
		   IF l_line_count > 0 THEN
  	          l_request_id :=  fnd_request.submit_request (application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DESCARTES_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id,
	                                                   argument2        => l_sch_delivery_date,
	                                                   argument3        => p_delivery_id,
	                                                   argument4        => l_Shipment_Type,
	                                                   argument5        => 'SEND_TO_DMS_SO_RENTAL',
  	                                                   argument6        =>  FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_DEBUG_FLAG'),
  	                                                   argument7        => p_request_id
	                                                 );	
            --Commit Updates 	
            xxwc_ascp_scwb_pkg.proccommit;
          END IF;
  	END IF; --Retrun Order condition    
  	
    l_sec := 'End of Procedure : Send_to_DMS';
    
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the Send_to_DMS procedure'||SQLERRM;
    
      write_output ('Error in XXWC_OM_DESCARTES_OB_PKG.Send_to_DMS is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.Send_to_DMS',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  						
  END Send_to_DMS;
    
  /********************************************************************************
  ProcedureName : Send_to_DMS_Clear_Delivery
  Purpose       : This will be called by printer form
  ----------------------------------------------------------------------------------*/
  PROCEDURE Send_to_DMS_Clear_Delivery( p_order_header_id   IN NUMBER
                                       ,p_delivery_id       IN NUMBER
                                       ,p_sch_delivery_date IN DATE DEFAULT SYSDATE
                                       ) AS
    
    l_error_code         NUMBER;
    p_error_msg          VARCHAR2(2000);
	l_sec                VARCHAR2 (150);
	l_sch_delivery_date  VARCHAR2(11);
    l_Shipment_Type      VARCHAR2 (150);
    l_request_id         NUMBER := 0;
    l_line_count         NUMBER := 0;
BEGIN
     XXWC_OM_DESCARTES_OB_PKG.debug_on;
     l_sec := 'Start of Procedure : Send_to_DMS_Clear_Delivery';
     
     l_sch_delivery_date := TO_CHAR (p_sch_delivery_date, 'DD-MON-YYYY');
     
     SELECT COUNT(1)
	   INTO l_line_count
	   FROM oe_order_headers_all ooh
           ,oe_order_lines_all ool
      WHERE ooh.header_id                   = ool.header_id
        AND ooh.header_id                   = p_order_header_id
        AND EXISTS                      (     SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                                );

		IF l_line_count > 0 THEN
           l_request_id :=  fnd_request.submit_request (    application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DESCARTES_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id,
	                                                   argument2        => l_sch_delivery_date,
	                                                   argument3        => p_delivery_id,
	                                                   argument4        => 'CustomerDelivery',
	                                                   argument5        => 'SEND_TO_DMS_SO_STD_CLR_DEL',
  	                                                   argument6        =>  FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_DEBUG_FLAG'),
  	                                                   argument7        => NULL
	                                                 );	
           --Commit Updates 	
           xxwc_ascp_scwb_pkg.proccommit;
       END IF;
 	
     l_sec := 'End of Procedure : Send_to_DMS_Clear_Delivery';
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the Send_to_DMS_Clear_Delivery procedure'||SQLERRM;
    
      write_output ('Error in XXWC_OM_DESCARTES_OB_PKG.Send_to_DMS_Clear_Delivery is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.Send_to_DMS_Clear_Delivery',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  						
  END Send_to_DMS_Clear_Delivery;
  
  PROCEDURE ShipmentSvc_delete_hdr_line (p_header_id           IN NUMBER,
                                         p_line_id             IN NUMBER,
                                         p_delivery_id         IN NUMBER,
                                         p_sch_delivery_date   IN DATE,
                                         p_hdr_flow_status_code IN VARCHAR2,
                                         p_ln_flow_status_code IN VARCHAR2,
                                         p_xxwc_log_rec        IN OUT xxwc_log_rec )
  AS
  l_request   XXWC_OM_DESCARTES_OB_PKG.t_request;
  l_response  XXWC_OM_DESCARTES_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(2000);
  l_sec                      VARCHAR2 (2000);
  
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  l_print_invoice_tag        BOOLEAN := TRUE;
  
  l_order_number             NUMBER := -1;
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_invoice_close            VARCHAR2(32767); 
  l_footer_clob              CLOB; 
  
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  
  CURSOR c_order_info ( p_header_id IN NUMBER
                       ,p_line_id  IN NUMBER )
   IS
   SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,wsh_stg.delivery_id DELIVERY_ID
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( XXWC_OM_SO_CREATE.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      --,ool.line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
     FROM oe_order_headers ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines ool
       , mtl_system_items_b msib
       , xxwc_wsh_shipping_stg wsh_stg
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND wsh_stg.header_id               = ooh.header_id
        AND wsh_stg.line_id                 = ool.line_id 
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.header_id                   = p_header_id
        AND ool.line_id                     = NVL(p_line_id, ool.line_id)
        AND wsh_stg.delivery_id             =  p_delivery_id
        /**AND (wsh_stg.status                  = p_ln_flow_status_code --'CANCELLED'/'OUT_FOR_DELIVERY'
              OR ool.flow_status_code        = p_ln_flow_status_code
              OR ooh.flow_status_code        = p_hdr_flow_status_code)**/ -- 'CANCELLED' - For header level cancellation for standard and internal orders.
        AND ool.source_type_code            = 'INTERNAL'
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )
   --     ORDER BY ooh.order_number, wsh_stg.delivery_id
  UNION
     -- This sql will retrun result for only rental and return order
     SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,NULL DELIVERY_ID
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( XXWC_OM_SO_CREATE.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      --,ool.line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
     FROM oe_order_headers ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines ool
       , mtl_system_items_b msib
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.header_id                   = p_header_id
        AND ool.line_id                     = NVL(p_line_id, ool.line_id)
        /**AND (
               ooh.flow_status_code         = NVL(p_hdr_flow_status_code, ooh.flow_status_code)
            OR ool.flow_status_code         = NVL(p_ln_flow_status_code, ool.flow_status_code)
             )**/
        AND ooh.order_type_id               IN (1013, 1014, 1006) --WC LONG TERM RENTAL(1013) /WC SHORT TERM RENTAL(1014)/ RETURN ORDER (1006)
        AND ool.line_type_id                IN (1003, 1015, 1007, 1008) --BILL ONLY (1003)/Rental(1015)/RETURN WITH RECEIPT (1007)/Credit(1008)
        AND ool.source_type_code            = 'INTERNAL';
        /**AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             );**/
BEGIN
  l_sec := 'Srart ShipmentSvc_delete_line p_header_id: ' ||p_header_id || ' p_line_id: '||p_line_id;
  l_url         := g_descartes_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_result_name := 'return';
  
  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DESCARTES_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before ShipmentSvc_delete_line';                                                  
  FOR r_order_info IN c_order_info ( p_header_id    => p_header_id
                                    ,p_line_id      => p_line_id)
  LOOP    
     l_data_found := TRUE; -- set to true if to generate xml file
     p_xxwc_log_rec.delivery_id    := r_order_info.delivery_id;
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' Delivery_id : '||r_order_info.Delivery_id ||' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);
     
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        IF p_line_id IS NULL THEN --IF header is cancelled or ship method is changed to will call at header level.
           XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                                                  p_name    => 'operationCode',
                                                  p_value   => 'Delete');
        ELSE
           XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                                                  p_name    => 'operationCode',
                                                  p_value   => 'DeleteItem');
        END IF;                                        
 
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'addressLine1',
                         p_value   => r_order_info.JOB_SITE_ADD);
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'city',
                         p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'phoneNumber1',
                         p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'postalCode',
                         p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'stateCode',
                         p_value   => r_order_info.JOB_SITE_ADD_STATE);
                                                                       
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet1',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet2',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
                                                         
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

     END IF;                                                   

     IF l_order_number <> r_order_info.ORDER_NUMBER THEN
        --Check if delivery is different and it is not a first record, then close the invoice element    
        IF l_order_number <> -1 THEN
          --start close the invoices element        
          XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</invoices>');
                                                   
          --end close the invoices element   
        END IF;
        
        l_order_number := r_order_info.ORDER_NUMBER;
        l_print_invoice_tag := TRUE;
             
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);
     END IF;                  

     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER);   
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER); 
                             
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_print_invoice_tag = TRUE THEN
        l_print_invoice_tag := FALSE;
        
        l_invoice_close := NULL;
        
        --start close the invoices element        
        --delivery_id as attribute value -- invoices -->attributes
        XXWC_OM_DESCARTES_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_invoice_close ,
                                                                   p_xml    => '</invoices>');
        --end close the invoices element      
     END IF;

     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        p_xxwc_log_rec.order_type := r_order_info.order_type;   
        IF r_order_info.order_type = 'RETURN ORDER' THEN
           XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                            p_name    => 'shipmentType',
                            p_value   => 'CustomerPickup');
        ELSE
           XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                            p_name    => 'shipmentType',
                            p_value   => 'CustomerDelivery');
        END IF;                      
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'internalOrderId',
                         p_value   => r_order_info.delivery_id);  
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
                         
        fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
        l_dms_branch_city := XXWC_OM_DESCARTES_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
        fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                        
     END IF ;  
     l_sec := 'End Loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'Afer Loop r_order_info';
  
  l_request.body := l_request.body||l_invoice_close;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                     
  --Order Type                              
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   
                                                   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_descartes_username);                       
        
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_descartes_passwordHash); 
                         
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_descartes_organizationkey);   
                         
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_header_id || ' Shipment service will not be called');
     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_header_id || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DESCARTES_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;                                   
  l_sec := 'End ShipmentSvc_delete_line'; 
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_delete_line procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_delete_line is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_delete_line Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_delete_line',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
   END ShipmentSvc_delete_hdr_line;
  
   FUNCTION Delete_SO_hdr_Line (p_subscription_guid   IN     RAW,
                                p_event               IN OUT wf_event_t)
      RETURN VARCHAR2
   IS
      l_plist         wf_parameter_list_t := p_event.getparameterlist ();
      l_error_msg     VARCHAR2 (500);
      l_request_type  VARCHAR2 (50);
      l_so_header_id  oe_order_headers_all.header_id%TYPE;
      l_so_line_id    oe_order_lines_all.line_id%TYPE;
      l_order_number  oe_order_headers_all.order_number%TYPE;
      l_order_type    oe_transaction_types_tl.name%TYPE;
      l_sec           VARCHAR2 (100);
      l_xxwc_log_rec  xxwc_log_rec;
      
      CURSOR c_order_delivery_info ( p_header_id     IN NUMBER
                                    ,p_line_id       IN NUMBER
                                 )
      IS
         SELECT DISTINCT wsh_stg.delivery_id, ooh.order_number
           FROM oe_order_headers_all ooh
               ,xxwc_wsh_shipping_stg wsh_stg
          WHERE wsh_stg.header_id   = ooh.header_id
            AND ooh.header_id       = p_header_id
            AND wsh_stg.line_id     = NVL(p_line_id, wsh_stg.line_id)
          ORDER BY wsh_stg.delivery_id;
   BEGIN
      l_sec := 'Initialize values';
      
      l_request_type := wf_event.getvalueforparameter ('P_REQUEST_TYPE', l_plist);
      l_so_header_id := wf_event.getvalueforparameter ('P_HEADER_ID', l_plist);
 
      IF l_request_type = 'LINE' THEN
         l_so_line_id   := wf_event.getvalueforparameter ('P_LINE_ID', l_plist);
         l_xxwc_log_rec.line_id      := l_so_line_id;
         l_xxwc_log_rec.service_name := 'DELETE_SO_LINE';
      ELSE
         l_xxwc_log_rec.service_name := 'DELETE_SO';   
         l_so_line_id                := NULL; 
      END IF;

      l_xxwc_log_rec.header_id    := l_so_header_id;
    
      BEGIN   
         SELECT ooh.order_number
               ,ott.name
           INTO l_order_number 
               ,l_order_type
           FROM oe_order_headers_all ooh
               ,oe_transaction_types_tl ott
          WHERE ooh.order_type_id = ott.transaction_type_id 
            AND ooh.header_id     = l_so_header_id;
      EXCEPTION   
      WHEN OTHERS THEN
          l_order_number := NULL;
          l_order_type   := NULL;
      END;
      
      l_xxwc_log_rec.order_number := l_order_number;
      l_xxwc_log_rec.order_type   := l_order_type;
             
      g_wallet_path                   := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_WALLET_PATH');
      write_output ('Profile XXWC_OM_DESCARTES_WALLET_PATH value : ' || g_wallet_path);
    
      g_descartes_username            := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_USERNAME');
      write_output ('Profile XXWC_OM_DESCARTES_USERNAME value : ' || g_descartes_username);
      
      g_descartes_passwordHash        := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_PASSWORDHASH');
      g_descartes_organizationkey     := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_ORGANIZATIONKEY');
    
      g_descartes_shipmentsvc_url     := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_SHIPMENTSERVICE_URL');
      write_output ('Profile XXWC_OM_DESCARTES_SHIPMENTSERVICE_URL value : ' || g_descartes_shipmentsvc_url);
      
      apps.mo_global.set_policy_context('S',162);
      
      IF l_order_type IN  ('STANDARD ORDER', 'INTERNAL ORDER') THEN 
          FOR r_order_info IN c_order_delivery_info ( p_header_id     => l_so_header_id
                                                     ,p_line_id       => l_so_line_id
                                                     )
          LOOP 
             XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_delete_hdr_line( 
                                                              p_header_id            => l_so_header_id
                                                            , p_line_id              => l_so_line_id
                                                            , p_delivery_id          => r_order_info.delivery_id
                                                            , p_sch_delivery_date    => SYSDATE
                                                            , p_hdr_flow_status_code => 'CANCELLED'
                                                            , p_ln_flow_status_code  => 'CANCELLED'
                                                            , p_xxwc_log_rec         => l_xxwc_log_rec
                                                          );
             DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec ); 
         END LOOP; 
      ELSE
         XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_delete_hdr_line( 
                                                           p_header_id            => l_so_header_id
                                                         , p_line_id              => l_so_line_id
                                                         , p_delivery_id          => NULL
                                                         , p_sch_delivery_date    => SYSDATE
                                                         , p_hdr_flow_status_code => 'CANCELLED'
                                                         , p_ln_flow_status_code  => 'CANCELLED'
                                                         , p_xxwc_log_rec         => l_xxwc_log_rec
                                                       );
     
         DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );
      END IF;   

      RETURN ('SUCCESS');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := SUBSTR (SQLERRM, 1, 240);

         wf_core.context ('XXWC_OM_DESCARTES_OB_PKG',
                          'Delete_SO_Hdr_Line',
                          p_event.geteventname (),
                          p_subscription_guid);
         wf_event.seterrorinfo (p_event, SUBSTR (SQLERRM, 1, 240));
         RETURN 'ERROR';

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DESCARTES_OB_PKG.Delete_SO_Hdr_Line',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');
   END Delete_SO_Hdr_Line;
  
/**PROCEDURE Delete_SO_Lines (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
IS
      l_line_id                 NUMBER;
      l_error_msg               VARCHAR2 (240);
      l_line_type_id            NUMBER;
      l_shipping_method_code   oe_order_lines_all.shipping_method_code%TYPE; 
      l_sec                     VARCHAR2 (100);
      l_shp_m_exist_count       NUMBER;
      l_xxwc_log_rec  xxwc_log_rec;
      l_ln_flow_status_code     oe_order_lines_all.flow_status_code%TYPE; 
    
   CURSOR cur_order_line IS
   SELECT ooh.header_id
         ,ool.shipping_method_code
         ,ooh.order_number
    FROM oe_order_lines ool
        ,oe_order_headers ooh
     WHERE ooh.header_id = ool.header_id 
       AND line_id       = l_line_id
       AND EXISTS ( SELECT 1
                      FROM fnd_lookup_values flv
                     WHERE 1=1
                       AND flv.meaning      = ool.shipping_method_code
                       AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                       AND flv.enabled_flag = 'Y'
                       AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                  );

   BEGIN
      l_sec := 'Initialize Values';

      l_line_id              := oe_line_security.g_record.line_id;
      l_line_type_id         := oe_line_security.g_record.line_type_id;
      l_shipping_method_code := oe_line_security.g_record.shipping_method_code;

      FOR rec_order_line IN cur_order_line
      LOOP
         IF l_line_type_id IN (fnd_profile.value('XXWC_STANDARD_LINE_TYPE'), fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE')
                              ,1003, 1015, 1007, 1008)
         THEN
            SELECT count(1)
              INTO l_shp_m_exist_count
              FROM fnd_lookup_values flv
             WHERE 1=1
               AND flv.meaning      = l_shipping_method_code
               AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
               AND flv.enabled_flag = 'Y'
               AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
            
            IF l_shp_m_exist_count =0 THEN
               l_xxwc_log_rec.service_name := 'DELETE_SO_LINE';
               l_xxwc_log_rec.header_id    := rec_order_line.header_id;
               l_xxwc_log_rec.line_id      := l_line_id;
      
               l_xxwc_log_rec.order_number := rec_order_line.order_number;
             
               g_wallet_path                   := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_WALLET_PATH');
               write_output ('Profile XXWC_OM_DESCARTES_WALLET_PATH value : ' || g_wallet_path);
    
               g_descartes_username            := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_USERNAME');
               write_output ('Profile XXWC_OM_DESCARTES_USERNAME value : ' || g_descartes_username);
      
               g_descartes_passwordHash        := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_PASSWORDHASH');
               g_descartes_organizationkey     := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_ORGANIZATIONKEY');
    
               g_descartes_shipmentsvc_url     := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_SHIPMENTSERVICE_URL');
               write_output ('Profile XXWC_OM_DESCARTES_SHIPMENTSERVICE_URL value : ' || g_descartes_shipmentsvc_url);
    
               IF l_line_type_id IN (fnd_profile.value('XXWC_STANDARD_LINE_TYPE'), fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE')) 
               THEN-- STANDARD ORDER / INTERNAL ORDER
                  l_ln_flow_status_code := 'OUT_FOR_DELIVERY';
               ELSE
                  l_ln_flow_status_code := NULL;
               END IF;
                   
               XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_delete_hdr_line( 
                                                                    p_header_id            => rec_order_line.header_id
                                                                  , p_line_id              => l_line_id
                                                                  , p_sch_delivery_date    => SYSDATE
                                                                  , p_hdr_flow_status_code => NULL
                                                                  , p_ln_flow_status_code  => l_ln_flow_status_code
                                                                  , p_xxwc_log_rec         => l_xxwc_log_rec
                                                              );
     
               DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );
            END IF;   
         END IF;
    END LOOP;

    x_result := 0;

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DESCARTES_OB_PKG.Delete_SO_Lines',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');

         x_result := 1;

   END Delete_SO_Lines;**/
   
PROCEDURE CANCEL_SO_OR_LINE (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
IS
      l_header_id               oe_order_headers_all.header_id%TYPE;
      l_line_id                 oe_order_lines_all.line_id%TYPE;
      l_error_msg               VARCHAR2 (240);
      l_order_type_id           NUMBER;
      l_line_type_id           NUMBER;
      l_shipping_method_code    oe_order_headers_all.shipping_method_code%TYPE; 
      l_ln_flow_status_code     oe_order_lines_all.flow_status_code%TYPE; 
      l_sec                     VARCHAR2 (100);
      l_shp_m_exist_count       NUMBER;
      l_xxwc_log_rec  xxwc_log_rec;
    
   CURSOR cur_order_hdr IS
   SELECT ooh.header_id
         ,ooh.shipping_method_code
         ,ooh.order_number
    FROM oe_order_headers ooh
     WHERE ooh.header_id = l_header_id
       AND ooh.order_type_id IN (1013, 1014, 1006, 1001 , 1011) --WC LONG TERM RENTAL(1013) /WC SHORT TERM RENTAL(1014)/ RETURN ORDER (1006)/ STANDARD ORDER / INTERNAL ORDER
       AND EXISTS ( SELECT 1
                      FROM fnd_lookup_values flv
                     WHERE 1=1
                       AND flv.meaning      = ooh.shipping_method_code
                       AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                       AND flv.enabled_flag = 'Y'
                       AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                  );

   CURSOR c_order_delivery_info ( p_header_id     IN NUMBER
                                 ,p_line_id       IN NUMBER
                                 )
      IS
         SELECT DISTINCT wsh_stg.delivery_id, ooh.order_number
           FROM oe_order_headers_all ooh
               ,xxwc_wsh_shipping_stg wsh_stg
          WHERE wsh_stg.header_id   = ooh.header_id
            AND ooh.header_id       = p_header_id
            AND wsh_stg.line_id     = NVL(p_line_id, wsh_stg.line_id)
          ORDER BY wsh_stg.delivery_id;
   BEGIN
      l_sec := 'Initialize Values';

      l_header_id            := oe_header_security.g_record.header_id;
      l_order_type_id        := oe_header_security.g_record.order_type_id;  
  	  
      IF p_validation_tmplt_short_name = 'WC_CNHD' THEN -- Cancel Header--to delete the order from descarties system
      
         --WC LONG TERM RENTAL(1013) /WC SHORT TERM RENTAL(1014)/ RETURN ORDER (1006)/STANDARD ORDER / INTERNAL ORDER
         insert into XXWC_CLOB_TEMP(step) values ('1p_validation_tmplt_short_name : '||p_validation_tmplt_short_name ||'l_line_id : '||l_line_id ||'l_header_id :'||l_header_id );
         IF l_order_type_id IN (1013, 1014, 1006, 1001, 1011) THEN
            XXWC_OM_DESCARTES_OB_PKG.raise_so_cancel_order(l_header_id,l_error_msg);
         END IF;
      ELSIF p_validation_tmplt_short_name = 'WC_CNLI' THEN -- Cancel Line --to delete the order from descarties system for rental and return
         l_header_id          := oe_line_security.g_record.header_id;
         l_line_id            := oe_line_security.g_record.line_id;
         l_line_type_id       := oe_line_security.g_record.line_type_id;
      
         --insert into XXWC_CLOB_TEMP(step) values ('1p_validation_tmplt_short_name : '||p_validation_tmplt_short_name ||'l_line_id : '||l_line_id ||'l_header_id :'||l_header_id );
         --insert into XXWC_CLOB_TEMP(step) values ('l_line_type_id : '||l_line_type_id);
               
         IF l_line_type_id IN (1003, 1015, 1007, 1008) --BILL ONLY (1003)/Rental(1015)/RETURN WITH RECEIPT (1007)/Credit(1008)
         THEN 
            XXWC_OM_DESCARTES_OB_PKG.raise_so_cancel_line(l_header_id, l_line_id, l_error_msg);
         END IF;
         
      ELSE --WC_CNLI
         -- this logic will delete the order from descartes when the ship_method is changed to will call. 
         l_shipping_method_code := oe_header_security.g_record.shipping_method_code;                                         
         FOR rec_order_hdr IN cur_order_hdr
         LOOP
            IF l_order_type_id IN (1013, 1014, 1006, 1001, 1011) -- WC LONG TERM RENTAL(1013) /WC SHORT TERM RENTAL(1014) /RETURN ORDER (1006) / STANDARD ORDER / INTERNAL ORDER
            THEN
               SELECT count(1)
                 INTO l_shp_m_exist_count
                 FROM fnd_lookup_values flv
                WHERE 1=1
                  AND flv.meaning      = l_shipping_method_code
                  AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                  AND flv.enabled_flag = 'Y'
                  AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
                
                IF l_shp_m_exist_count =0 THEN
                   l_xxwc_log_rec.service_name := 'DELETE_SO';
                   l_xxwc_log_rec.header_id    := rec_order_hdr.header_id;
                   --l_xxwc_log_rec.line_id      := l_line_id;
      
                   l_xxwc_log_rec.order_number := rec_order_hdr.order_number;
             
                   g_wallet_path                   := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_WALLET_PATH');
                   write_output ('Profile XXWC_OM_DESCARTES_WALLET_PATH value : ' || g_wallet_path);
    
                   g_descartes_username            := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_USERNAME');
                   write_output ('Profile XXWC_OM_DESCARTES_USERNAME value : ' || g_descartes_username);
      
                   g_descartes_passwordHash        := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_PASSWORDHASH');
                   g_descartes_organizationkey     := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_ORGANIZATIONKEY');
    
                   g_descartes_shipmentsvc_url     := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_SHIPMENTSERVICE_URL');
                   write_output ('Profile XXWC_OM_DESCARTES_SHIPMENTSERVICE_URL value : ' || g_descartes_shipmentsvc_url);
                   
                   /**IF l_order_type_id IN (1001, 1011) THEN-- STANDARD ORDER / INTERNAL ORDER
                      l_ln_flow_status_code := 'OUT_FOR_DELIVERY';
                   ELSE
                      l_ln_flow_status_code := NULL;
                   END IF;**/
                   
                   apps.mo_global.set_policy_context('S',162);
                   
                   IF l_order_type_id IN (1001, 1011) THEN -- STANDARD ORDER / INTERNAL ORDER
                      FOR r_order_info IN c_order_delivery_info ( p_header_id     => rec_order_hdr.header_id
                                                                 ,p_line_id       => NULL
                                                                )
                      LOOP 
                         XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_delete_hdr_line(  p_header_id            => rec_order_hdr.header_id
                                                                              , p_line_id              => NULL -- to include all lines--Truck Delivery --> will call
                                                                              , p_delivery_id          => r_order_info.delivery_id
                                                                              , p_sch_delivery_date    => SYSDATE
                                                                              , p_hdr_flow_status_code => NULL 
                                                                              , p_ln_flow_status_code  => 'OUT_FOR_DELIVERY'--l_ln_flow_status_code
                                                                              , p_xxwc_log_rec         => l_xxwc_log_rec
                                                                            );
     
                        DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );
                      END LOOP;
                   ELSE
                      XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_delete_hdr_line(  p_header_id            => rec_order_hdr.header_id
                                                                           , p_line_id              => NULL -- to include all lines--Truck Delivery --> will call
                                                                           , p_delivery_id          => NULL  
                                                                           , p_sch_delivery_date    => SYSDATE
                                                                           , p_hdr_flow_status_code => NULL 
                                                                           , p_ln_flow_status_code  => NULL--l_ln_flow_status_code
                                                                           , p_xxwc_log_rec         => l_xxwc_log_rec
                                                                      );
     
                     DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );
                  END IF;    
                END IF;   
            END IF;
         END LOOP;
      END IF; 
      x_result := 0;

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DESCARTES_OB_PKG.Delete_SO',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');

         x_result := 1;

   END CANCEL_SO_OR_LINE;

  PROCEDURE ShipmentSvc_clear_delivery  (p_order_number      IN NUMBER,
                                         p_sch_delivery_date IN DATE,
                                         p_delivery_id       IN NUMBER,
                                         p_xxwc_log_rec      IN OUT xxwc_log_rec )
  AS
  l_request   XXWC_OM_DESCARTES_OB_PKG.t_request;
  l_response  XXWC_OM_DESCARTES_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(2000);
  l_sec                      VARCHAR2 (2000);
  
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  l_print_invoice_tag        BOOLEAN := TRUE;
  
  l_order_number             NUMBER := -1;
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_invoice_close            VARCHAR2(32767); 
  l_footer_clob              CLOB; 
  
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  
  CURSOR c_order_info ( p_order_number IN NUMBER
                       ,p_delivery_id  IN NUMBER )
   IS
   SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,dms_stg.delivery_id
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( XXWC_OM_SO_CREATE.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      --,ool.line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
     FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib
       , xxwc_om_dms_change_tbl dms_stg --xxwc_wsh_shipping_stg wsh_stg
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND dms_stg.header_id               = ooh.header_id
        AND dms_stg.line_id                 = ool.line_id 
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.order_number                = p_order_number
        AND dms_stg.delivery_id             = p_delivery_id
        AND dms_stg.change_type             = 'D'
        AND dms_stg.status                  = 'OUT_FOR_DELIVERY'
        AND ool.source_type_code            = 'INTERNAL'
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )
        ORDER BY ooh.order_number, dms_stg.delivery_id;
BEGIN
  l_sec := 'Srart ShipmentSvc_clear_delivery p_order_number: ' ||p_order_number || ' p_delivery_id: '||p_delivery_id;
  l_url         := g_descartes_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_result_name := 'return';
  
  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DESCARTES_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before ShipmentSvc_clear_delivery';                                                  
  FOR r_order_info IN c_order_info ( p_order_number => p_order_number
                                    ,p_delivery_id  => p_delivery_id)
  LOOP    
     l_data_found := TRUE; -- set to true if to generate xml file
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' Delivery_id : '||r_order_info.Delivery_id ||' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);
     
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                                               p_name    => 'operationCode',
                                               p_value   => 'Delete');
 
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'addressLine1',
                         p_value   => r_order_info.JOB_SITE_ADD);
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'city',
                         p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'phoneNumber1',
                         p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'postalCode',
                         p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'stateCode',
                         p_value   => r_order_info.JOB_SITE_ADD_STATE);
                                                                       
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet1',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet2',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
                                                         
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

     END IF;                                                   

     IF l_order_number <> r_order_info.ORDER_NUMBER THEN
        --Check if delivery is different and it is not a first record, then close the invoice element    
        IF l_order_number <> -1 THEN
          --start close the invoices element        
          XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</invoices>');
                                                   
          --end close the invoices element   
        END IF;
        
        l_order_number := r_order_info.ORDER_NUMBER;
        l_print_invoice_tag := TRUE;
             
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);
     END IF;                  

     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER);   
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER); 
                             
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_print_invoice_tag = TRUE THEN
        l_print_invoice_tag := FALSE;
        
        l_invoice_close := NULL;
        
        --start close the invoices element        
        --delivery_id as attribute value -- invoices -->attributes
        XXWC_OM_DESCARTES_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_invoice_close ,
                                                                   p_xml    => '</invoices>');
        --end close the invoices element      
     END IF;

     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => 'CustomerDelivery');  
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'internalOrderId',
                         p_value   => r_order_info.delivery_id);  
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
                         
        fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
        l_dms_branch_city := XXWC_OM_DESCARTES_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
        fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                        
     END IF ;  
     l_sec := 'End Loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'Afer Loop r_order_info';
  
  l_request.body := l_request.body||l_invoice_close;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                     
  --Order Type                              
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   
                                                   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_descartes_username);                       
        
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_descartes_passwordHash); 
                         
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_descartes_organizationkey);   
                         
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');
     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DESCARTES_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;                                   
  l_sec := 'End ShipmentSvc_clear_delivery'; 
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_clear_delivery procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_clear_delivery is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_clear_delivery Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_clear_delivery',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
  END ShipmentSvc_clear_delivery;
  
  PROCEDURE ShipmentSvc_WCD(p_order_number      IN NUMBER,
                            p_sch_delivery_date IN DATE,
                            p_delivery_id       IN NUMBER,
                            p_xxwc_log_rec      IN OUT xxwc_log_rec )
AS
  l_request   XXWC_OM_DESCARTES_OB_PKG.t_request;
  l_response  XXWC_OM_DESCARTES_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(2000);
  l_sec                      VARCHAR2 (2000);
  
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  l_print_invoice_tag        BOOLEAN := TRUE;
  
  l_order_number             NUMBER := -1;
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_invoice_close            VARCHAR2(32767); 
  l_footer_clob              CLOB; 
  
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  l_user_def_org_code        mtl_parameters.organization_code%TYPE;
  
  CURSOR c_order_info ( p_order_number IN NUMBER
                       ,p_delivery_id  IN NUMBER )
   IS
   SELECT 
   -- SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ooh.order_number ORDER_NUMBER
      ,wsh_stg.delivery_id
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( XXWC_OM_SO_CREATE.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      --,ool.line_number
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,NVL(wsh_stg.transaction_qty, 0) * NVL(msib.unit_weight, 0) ITEM_UNIT_WEIGHT
      ,NVL(wsh_stg.transaction_qty, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DESCARTES_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
     FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib
       , xxwc_wsh_shipping_stg wsh_stg
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND wsh_stg.header_id               = ooh.header_id
        AND wsh_stg.line_id                 = ool.line_id 
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.order_number                = p_order_number
        AND wsh_stg.delivery_id             = p_delivery_id
        AND wsh_stg.status                  = 'DELIVERED'
        AND ool.source_type_code            = 'INTERNAL'
        --AND wsh_stg.delivery_id             = NVL(p_delivery_id, wsh_stg.delivery_id)
        /**AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )**/ -- need to send all types of shipping method
        ORDER BY ooh.order_number, wsh_stg.delivery_id;
BEGIN
  l_sec := 'Srart ShipmentSvc_WCD p_order_number : ' ||p_order_number || ' p_delivery_id: '||p_delivery_id;
  l_url         := g_descartes_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_result_name := 'return';
  
  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DESCARTES_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before ShipmentSvc_std_int_ord';
  
  FOR r_order_info IN c_order_info ( p_order_number => p_order_number
                                    ,p_delivery_id  => p_delivery_id)
  LOOP    
     l_data_found := TRUE; -- set to true if to generate xml file
     
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' Delivery_id : '||r_order_info.Delivery_id ||' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);
          
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'operationCode',
                         p_value   => 'Edit');
 
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'addressLine1',
                         p_value   => r_order_info.JOB_SITE_ADD);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'city',
                         p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'phoneNumber1',
                         p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'postalCode',
                         p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'stateCode',
                         p_value   => r_order_info.JOB_SITE_ADD_STATE);
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet1',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                                                                                        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet2',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
                                                         
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

     END IF;                                                   

     IF l_order_number <> r_order_info.ORDER_NUMBER THEN
        --Check if delivery is different and it is not a first record, then close the invoice element    
        IF l_order_number <> -1 THEN
          --start close the invoices element        
          XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</invoices>');
                                                   
          --end close the invoices element   
        END IF;
        
        l_order_number := r_order_info.ORDER_NUMBER;
        l_print_invoice_tag := TRUE;
             
        XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
        
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);
     END IF;                  

     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER);   
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER); 
                             
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_print_invoice_tag = TRUE THEN
        l_print_invoice_tag := FALSE;
        
        l_invoice_close := NULL;
        
        --start close the invoices element        
        --delivery_id as attribute value -- invoices -->attributes
        XXWC_OM_DESCARTES_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_invoice_close ,
                                                                   p_xml    => '</invoices>');
        --end close the invoices element      
     END IF;

     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => 'CustomerDelivery');  
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'internalOrderId',
                         p_value   => r_order_info.delivery_id);  
        
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
        BEGIN
          SELECT ORGANIZATION_CODE	
            INTO l_user_def_org_code
            FROM mtl_parameters mp
           WHERE mp.organization_id = SUBSTR(fnd_profile.VALUE('XXWC_OM_DEFAULT_SHIPPING_ORG'), 1, 3) ;
        EXCEPTION
        WHEN OTHERS THEN
           l_user_def_org_code := NULL;
        END;   
       
       l_dms_branch_city := XXWC_OM_DESCARTES_OB_PKG.GET_DMS_ORG( p_branch_code => l_user_def_org_code);
       
       fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

       XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
                         
        XXWC_OM_DESCARTES_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                  
     END IF ;  
     l_sec := 'End Loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'Afer Loop r_order_info';
  
  l_request.body := l_request.body||l_invoice_close;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
  
  --Order Type                              
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE || '-' ||'WCD');   
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   
                                                                      
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_descartes_username);                       
        
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_descartes_passwordHash); 
                         
  XXWC_OM_DESCARTES_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_descartes_organizationkey);   
                         
  XXWC_OM_DESCARTES_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');

     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DESCARTES_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;                                   
  
  l_sec := 'End Procedure ShipmentSvc_WCD';
  g_retcode:=0;
  fnd_file.put_line (fnd_file.log, 'End Procedure ShipmentSvc_WCD');  
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_WCD procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_WCD is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_WCD Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_WCD',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
  END ShipmentSvc_WCD;
  
  /********************************************************************************
  ProcedureName : Send_to_DMS_WCD
  Purpose       : This will be called by printer form
  ----------------------------------------------------------------------------------*/
  PROCEDURE Send_to_DMS_WCD( p_order_header_id   IN NUMBER
                            ,p_delivery_id       IN NUMBER
                            ,p_sch_delivery_date IN DATE DEFAULT SYSDATE
                           ) AS
    
    l_error_code         NUMBER;
    p_error_msg          VARCHAR2(2000);
	l_sec                VARCHAR2 (150);
	l_sch_delivery_date  VARCHAR2(11);
    l_Shipment_Type      VARCHAR2 (150);
    l_request_id         NUMBER := 0;
    l_line_count         NUMBER := 0;
    l_order_type_id      NUMBER := 0;
  
  BEGIN
     IF FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_DEBUG_FLAG') = 'Y' THEN
        XXWC_OM_DESCARTES_OB_PKG.debug_on;
     END IF;
       
     l_sec := 'Start of Procedure : Send_to_DMS_WCD';
     
     l_sch_delivery_date := TO_CHAR (p_sch_delivery_date, 'DD-MON-YYYY');
     write_output ('Start of Procedure Send_to_DMS_WCD p_order_header_id: '||p_order_header_id|| ' p_delivery_id : '||p_delivery_id);
          
     l_request_id :=  fnd_request.submit_request  (    application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DESCARTES_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id,
	                                                   argument2        => l_sch_delivery_date,
	                                                   argument3        => p_delivery_id,
	                                                   argument4        => 'CustomerDelivery',
	                                                   argument5        => 'SEND_TO_DMS_SO_WCD',
  	                                                   argument6        =>  FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_DEBUG_FLAG'),
  	                                                   argument7        => NULL
	                                                 );	
     --Commit Updates 	
     xxwc_ascp_scwb_pkg.proccommit;
     
     write_output ('After submitting concurrent request l_request_id: '||l_request_id);

     l_sec := 'End of Procedure : Send_to_DMS_WCD';
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the Send_to_DMS_WCD procedure'||SQLERRM;
    
      write_output ('Error in XXWC_OM_DESCARTES_OB_PKG.Send_to_DMS_WCD is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.Send_to_DMS_WCD',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  						
  END Send_to_DMS_WCD;
  
  /********************************************************************************
  ProcedureName : MAIN
  Purpose       : This is mail program and will be called by concurrent program to 
                  shipment webservice
  
  HISTORY
   ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/20/2017    Rakesh Patel    Initial version.
  ********************************************************************************/
  ----------------------------------------------------------------------------------
  -- Main Procedure
  ----------------------------------------------------------------------------------
  PROCEDURE main(errbuf              OUT VARCHAR2
                ,retcode             OUT NUMBER
                ,p_header_id         IN NUMBER
                ,p_sch_delivery_date IN VARCHAR2
                ,p_delivery_id       IN NUMBER
                ,p_shipment_type     IN VARCHAR2
                ,p_interface_type    IN VARCHAR2
                ,p_debug_flag        IN VARCHAR2
                ,p_request_id        IN NUMBER ) AS
    
    l_error_code         NUMBER;
    p_error_msg          VARCHAR2(2000);
	l_sec                VARCHAR2 (100);
	l_sch_delivery_date  DATE;
    l_xxwc_log_rec       xxwc_log_rec;
    l_order_type         OE_TRANSACTION_TYPES_TL.NAME%TYPE;
    l_order_number       oe_order_headers_all.order_number%TYPE;
    l_line_type_id       NUMBER;
    l_order_found        BOOLEAN := FALSE;
    
    CURSOR c_order_delivery_info ( p_header_id     IN NUMBER
                                  ,p_delivery_id   IN NUMBER
                                 )
   IS
      SELECT DISTINCT wsh_stg.delivery_id, ooh.order_number, ott.name order_type
        FROM oe_order_headers_all ooh
            ,xxwc_wsh_shipping_stg wsh_stg
            ,oe_transaction_types_tl ott
       WHERE ooh.order_type_id   = ott.transaction_type_id 
         AND wsh_stg.header_id   = ooh.header_id
         AND ooh.header_id       = p_header_id
         AND wsh_stg.delivery_id = NVL(p_delivery_id, wsh_stg.delivery_id)
      ORDER BY wsh_stg.delivery_id;
  BEGIN
    l_sec := 'Start of Procedure : MAIN';
    g_retcode := 0;
        
    IF p_debug_flag = 'Y' THEN
       XXWC_OM_DESCARTES_OB_PKG.debug_on;
    ELSE
       XXWC_OM_DESCARTES_OB_PKG.debug_off;
    END IF;
    
    apps.mo_global.set_policy_context('S',162); 
  
    fnd_file.put_line(fnd_file.log, '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'Start of Procedure : MAIN');
    fnd_file.put_line(fnd_file.log, '----------------------------------------------------------------------------------');
	  
    fnd_file.put_line(fnd_file.log, 'Input Parameters     :');
    fnd_file.put_line(fnd_file.log,'Order Header id        : ' || p_header_id);
    fnd_file.put_line(fnd_file.log,'Schedule delivery date : ' || p_sch_delivery_date);
    fnd_file.put_line(fnd_file.log,'Delivery id            : ' || p_delivery_id);
    fnd_file.put_line(fnd_file.log,'Shipment Type          : ' || p_shipment_type);
    fnd_file.put_line(fnd_file.log,'Interface Type         : ' || p_Interface_type);
    fnd_file.put_line(fnd_file.log,'Debug Flag             : ' || p_debug_flag);
    fnd_file.put_line(fnd_file.log,'p_request_id           : ' || p_request_id);
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
    
    IF p_sch_delivery_date IS NOT NULL THEN
       l_sch_delivery_date := TO_DATE (p_sch_delivery_date, 'DD-MON-YYYY');
    ELSE
       l_sch_delivery_date := SYSDATE;
    END IF; 
    
    l_xxwc_log_rec.schedule_delivery_date := l_sch_delivery_date;
    
    write_output ('l_sch_delivery_date : '|| l_sch_delivery_date);
    
    l_xxwc_log_rec.rpt_con_request_id := p_request_id;
    
    write_output ('l_xxwc_log_rec.rpt_con_request_id : '|| l_xxwc_log_rec.rpt_con_request_id);
    
    g_wallet_path                   := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_WALLET_PATH');
    write_output ('Profile XXWC_OM_DESCARTES_WALLET_PATH value : ' || g_wallet_path);
    IF g_wallet_path IS NULL THEN
       retcode := 2;
       errbuf  := 'Profile XXWC_OM_DESCARTES_WALLET_PATH value not defined';
       write_output ('Profile XXWC_OM_DESCARTES_WALLET_PATH value not defined');
       RETURN;
    END IF;
    
    g_descartes_username            := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_USERNAME');
    write_output ('Profile XXWC_OM_DESCARTES_USERNAME value : ' || g_descartes_username);
    IF g_descartes_username IS NULL THEN
       retcode := 2;
       errbuf  := 'Profile XXWC_OM_DESCARTES_USERNAME value not defined';
       write_output ('Profile XXWC_OM_DESCARTES_USERNAME value not defined');
       RETURN;
    END IF;
    
    g_descartes_passwordHash        := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_PASSWORDHASH');
    IF g_descartes_passwordHash IS NULL THEN
       retcode := 2;
       errbuf  := 'Profile XXWC_OM_DESCARTES_PASSWORDHASH value not defined';
       write_output ('Profile XXWC_OM_DESCARTES_PASSWORDHASH value not defined');
       RETURN;
    END IF;
    
    g_descartes_organizationkey     := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_ORGANIZATIONKEY');
    IF g_descartes_organizationkey IS NULL THEN
       retcode := 2;
       errbuf  := 'Profile XXWC_OM_DESCARTES_ORGANIZATIONKEY value not defined';
       write_output ('Profile XXWC_OM_DESCARTES_ORGANIZATIONKEY value not defined');
       RETURN;
    END IF;
    
    g_descartes_shipmentsvc_url     := FND_PROFILE.VALUE ('XXWC_OM_DESCARTES_SHIPMENTSERVICE_URL');
    write_output ('Profile XXWC_OM_DESCARTES_SHIPMENTSERVICE_URL value : ' || g_descartes_shipmentsvc_url);
    IF g_descartes_shipmentsvc_url IS NULL THEN
       retcode := 2;
       errbuf  := 'Profile XXWC_OM_DESCARTES_SHIPMENTSERVICE_URL value not defined';
       write_output ('Profile XXWC_OM_DESCARTES_SHIPMENTSERVICE_URL value not defined');
       RETURN;
    END IF;
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
    
    IF p_interface_type = 'SEND_TO_DMS_SO_STD_CLR_DEL' THEN
       l_sec := ' call ShipmentSvc_clear_delivery';
	   fnd_file.put_line(fnd_file.log,'Before call to ShipmentSvc_clear_delivery');
    
       l_xxwc_log_rec.service_name := 'STD_INT_ORDER_SVC_CLEAR_DELIVERY';
       
       BEGIN   
          SELECT ooh.order_number
                ,ott.name   
            INTO l_order_number
                ,l_order_type
            FROM oe_order_headers ooh
                ,oe_transaction_types_tl ott
           WHERE ooh.order_type_id = ott.transaction_type_id 
             AND ooh.header_id     = p_header_id;
       EXCEPTION   
       WHEN OTHERS THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END;
       
       l_xxwc_log_rec.header_id    := p_header_id;
       l_xxwc_log_rec.order_number := l_order_number;
       l_xxwc_log_rec.order_type   := l_order_type;
       l_xxwc_log_rec.delivery_id  := p_delivery_id;
          
       XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_clear_delivery( p_order_number      => l_xxwc_log_rec.order_number
                                                           ,p_sch_delivery_date => l_sch_delivery_date
                                                           ,p_delivery_id       => p_delivery_id
                                                           ,p_xxwc_log_rec      => l_xxwc_log_rec );
    ELSIF p_interface_type = 'SEND_TO_DMS_SO_WCD' THEN --'WC Direct'
       l_sec := ' call ShipmentSvc_WCD';
	   fnd_file.put_line(fnd_file.log,'Before call to ShipmentSvc_WCD');
    
       l_xxwc_log_rec.service_name := 'STD_INT_ORDER_WCD';
       
       BEGIN   
          SELECT ooh.order_number
                ,ott.name   
            INTO l_order_number
                ,l_order_type
            FROM oe_order_headers ooh
                ,oe_transaction_types_tl ott
           WHERE ooh.order_type_id = ott.transaction_type_id 
             AND ooh.header_id     = p_header_id;
       EXCEPTION   
       WHEN OTHERS THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END;
       
       l_xxwc_log_rec.header_id    := p_header_id;
       l_xxwc_log_rec.order_number := l_order_number;
       l_xxwc_log_rec.order_type   := l_order_type;
       l_xxwc_log_rec.delivery_id  := p_delivery_id;
          
       XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_WCD( p_order_number      => l_xxwc_log_rec.order_number
                                                ,p_sch_delivery_date => l_sch_delivery_date
                                                ,p_delivery_id       => p_delivery_id
                                                ,p_xxwc_log_rec      => l_xxwc_log_rec );
                                                
    ELSIF p_interface_type = 'SEND_TO_DMS_SO_STD' THEN --'Standard/Internal Orders'
       l_sec := ' call ShipmentSvc_std_int_ord';
	   fnd_file.put_line(fnd_file.log,'Before call to ShipmentSvc_std_int_ord');
	   
       l_order_found := FALSE;
	   
	   l_xxwc_log_rec.service_name := 'STD_INT_ORDER_SVC';
          
       FOR r_order_info IN c_order_delivery_info ( p_header_id     => p_header_id
                                                  ,p_delivery_id   => p_delivery_id
                                                  )
       LOOP 
          l_xxwc_log_rec.order_number           := r_order_info.order_number;
          l_xxwc_log_rec.header_id              := p_header_id;
          l_xxwc_log_rec.order_type             := r_order_info.order_type;
          
          l_order_found                  := TRUE;
          write_output ('Inside r_order_info delivery_id : '|| r_order_info.delivery_id);
          l_sec := 'Inside r_order_info delivery_id : '|| r_order_info.delivery_id;
          
          l_xxwc_log_rec.delivery_id := r_order_info.delivery_id;
          
          ----------------------------------------------------------------------------------
          -- Calling the standard / internal order ShipmentService procedure to call shipment web service
          ----------------------------------------------------------------------------------
       
          XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_std_int_ord( p_order_number      => l_xxwc_log_rec.order_number
                                                           ,p_sch_delivery_date => l_sch_delivery_date
                                                           ,p_delivery_id       => r_order_info.delivery_id
                                                           ,p_xxwc_log_rec      => l_xxwc_log_rec
                                                           ,p_request_id        => p_request_id
                                                          );
       END LOOP;      
          
       IF l_order_found = FALSE THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END IF;
    
       l_sec := 'After call to ShipmentSvc_std_int_ord';
       fnd_file.put_line(fnd_file.log,'After call to ShipmentSvc_std_int_ord');
    
    ELSIF p_interface_type = 'SEND_TO_DMS_SO_RENTAL' THEN   --'WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL'
       l_sec := 'Before call ShipmentSvc_rental_ord';
       fnd_file.put_line(fnd_file.log,'Before call ShipmentSvc_rental_ord');
       
       l_xxwc_log_rec.service_name := 'RENTAL_ORDER_SVC';
       
       BEGIN   
          SELECT ooh.order_number
                ,ott.name   
            INTO l_order_number
                ,l_order_type
            FROM oe_order_headers ooh
                ,oe_transaction_types_tl ott
           WHERE ooh.order_type_id = ott.transaction_type_id 
             AND ooh.header_id     = p_header_id;
       EXCEPTION   
       WHEN OTHERS THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END;
       
       l_xxwc_log_rec.header_id    := p_header_id;
       l_xxwc_log_rec.order_number := l_order_number;
       l_xxwc_log_rec.order_type   := l_order_type;
          
       ----------------------------------------------------------------------------------
       -- Calling the Rental Order ShipmentService procedure to call shipment web service
       ----------------------------------------------------------------------------------
       
       IF p_shipment_type = 'CustomerDelivery' THEN
          l_line_type_id := 1015;
          
          XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_rental_ord( p_order_number      => l_order_number
                                                          ,p_shipment_type     => p_shipment_type
                                                          ,p_line_type_id      => l_line_type_id
                                                          ,p_flow_status_code  => 'AWAITING_SHIPPING'
                                                          ,p_sch_delivery_date => l_sch_delivery_date 
                                                          ,p_rental_return     => 1 -- to include the lookup items
                                                          ,p_request_id        => p_request_id  
                                                          ,p_xxwc_log_rec      => l_xxwc_log_rec
                                                         );
       ELSIF p_shipment_type = 'CustomerPickup' THEN
          l_line_type_id := 1007;
         
          XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_rental_ord( p_order_number      => l_order_number
                                                          ,p_shipment_type     => p_shipment_type
                                                          ,p_line_type_id      => l_line_type_id
                                                          ,p_flow_status_code  => 'AWAITING_RETURN' -- to include all flow status code
                                                          ,p_sch_delivery_date => l_sch_delivery_date 
                                                          ,p_rental_return     => 0 -- to exclude the lookup items
                                                          ,p_request_id        => p_request_id  
                                                          ,p_xxwc_log_rec      => l_xxwc_log_rec
                                                         );
       END IF;
    
       l_sec := 'After call to ShipmentSvc_rental_ord';
       fnd_file.put_line(fnd_file.log,'After call to ShipmentSvc_rental_ord');

    ELSIF p_interface_type = 'SEND_TO_DMS_SO_RETURN' THEN   --'WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL'
       fnd_file.put_line(fnd_file.log,'Before call ShipmentSvc_return_ord');
       
       l_xxwc_log_rec.service_name := 'RETURN _ORDER_SVC';
       
       BEGIN   
          SELECT ooh.order_number
                ,ott.name   
            INTO l_order_number
                ,l_order_type
            FROM oe_order_headers ooh
                ,oe_transaction_types_tl ott
           WHERE ooh.order_type_id = ott.transaction_type_id 
             AND ooh.header_id     = p_header_id;
       EXCEPTION   
       WHEN OTHERS THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END;
       
       l_xxwc_log_rec.header_id    := p_header_id;
       l_xxwc_log_rec.order_number := l_order_number;
       l_xxwc_log_rec.order_type   := l_order_type;
       
       ----------------------------------------------------------------------------------
       -- Calling the Return ShipmentService procedure to call shipment web service
       ----------------------------------------------------------------------------------
       IF l_order_number IS NOT NULL THEN
          XXWC_OM_DESCARTES_OB_PKG.ShipmentSvc_return_ord( p_order_number      => l_order_number
                                                          ,p_shipment_type     => p_shipment_type
                                                          ,p_sch_delivery_date => l_sch_delivery_date 
                                                          ,p_request_id        => p_request_id  
                                                          ,p_xxwc_log_rec      => l_xxwc_log_rec
                                                         );
       END IF;                                                  
    
       fnd_file.put_line(fnd_file.log,'After call to ShipmentSvc_return_ord');
    END IF; --p_interface_type
         
	retcode := g_retcode;
    errbuf  := g_err_msg;
  
    DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );
    
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'End of Procedure : MAIN');
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
    
    l_sec := 'End of Procedure : MAIN';
    
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the main procedure'||SQLERRM;
    
      retcode := 2;
      errbuf  := p_error_msg;
      write_output ('Error in XXWC_OM_DESCARTES_OB_PKG.main is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
    
      fnd_file.put_line(fnd_file.log, 'Exception Block of Main Procedure');
      fnd_file.put_line(fnd_file.log
                       ,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log
                       ,'The Error Message Traced is : ' ||
                        p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.main',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  						
  END main;
  
END XXWC_OM_DESCARTES_OB_PKG;
/