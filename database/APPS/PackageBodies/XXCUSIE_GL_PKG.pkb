CREATE OR REPLACE PACKAGE BODY APPS."XXCUSIE_GL_PKG" IS

  /*******************************************************************************
  * Procedure:   load_cti_gl_int
  * Description: Creates output file in output directory indicated named
  *              'CTI_GL_IEXP_<date in 'YYYYMMDDHHMISS' format>.txt'
  *              Employee added.
  *              Concurrent 'Request HDS GL WW Outbound' under the responsibility
  *              XXCUS_CON
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/27/2011    Kathy Poling    Initial creation of the package copied
                                        from R11
  
  1.1     04/14/2012    Kathy Poling    Added NVL to description line
                                        Service Ticket 136532
  1.2     07/10/2018   Balaguru Seshadri - FM I-Expense enhancements ( TMS 20180420-00019 ) 
  ********************************************************************************/
  -- Begin Ver 1.2
  PROCEDURE print_log(p_message in varchar2)  IS
  BEGIN
   dbms_output.put_line(p_message);
  END print_log;
  -- End Ver 1.2
  PROCEDURE load_cti_gl_int(errbuf    OUT VARCHAR2
                           ,retcode   OUT NUMBER
                           ,p_fperiod IN VARCHAR2) IS
  
    --Intialize Variables
  
    l_err_msg         VARCHAR2(2000);
    l_err_code        NUMBER;
    l_processed_count NUMBER := 0;
    l_sec             VARCHAR2(150);
    l_tot_accts       NUMBER := 0;
    l_debit           NUMBER := 0;
    l_credit          NUMBER := 0;
    l_seg_descr       VARCHAR2(240);
    l_lob             VARCHAR2(3);
    l_date            DATE;
    l_sequence        NUMBER;
    l_line_num        NUMBER := 0;
    l_total           NUMBER := 0;
    l_d_c_id          VARCHAR2(10);
    l_runnum          NUMBER := 0;
    l_text            VARCHAR2(50) := 'NA';
    l_whack           NUMBER := 0;
    l_sourcesys       VARCHAR2(75) := 'ORCL-CTI';
  
    l_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_coa      CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := '50328'; --HDS Chart of Accounts
    l_ledger   CONSTANT gl_ledgers.ledger_id%TYPE := 2061; --HDS Supply USD
    l_period         gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_shortperiod    gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_procedure_name VARCHAR2(75) := 'XXCUSIE_GL_PKG.LOAD_CTI_GL_INT';
  
    l_err_callfrom VARCHAR2(75) DEFAULT 'XXCUSIE_GL_PKG';
    --l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id      NUMBER := fnd_global.conc_request_id;
  
    --Start Main Program
  BEGIN
    l_sec := 'In create file name; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    SELECT period_name
          ,substr(period_name, 1, 3)
      INTO l_period
          ,l_shortperiod
      FROM gl.gl_periods
     WHERE period_set_name = l_calendar
       AND start_date <= trunc(SYSDATE)
       AND end_date >= trunc(SYSDATE)
       AND period_num <> 13;
  
    SELECT SYSDATE
      INTO l_date
      FROM dual;
  
    SELECT xxcus.xxcusie_et_extract_s.nextval
      INTO l_sequence
      FROM dual;
  
    --Truncate XXCUS.XXCUSIE_CTI_FILTER_EXTRACT_TBL
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSIE_CTI_FILTER_EXTRACT_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSIE_CTI_FILTER_EXTRACT_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    --Determine Run Number
    BEGIN
      SELECT (MAX(nvl(run_num, 0)) + 1)
        INTO l_runnum
        FROM xxcus.xxcusie_cti_aud_tbl
       WHERE period_name = p_fperiod;
    EXCEPTION
      WHEN no_data_found THEN
        l_runnum := 1;
      WHEN OTHERS THEN
        l_runnum := 1;
    END;
  
    IF l_runnum IS NULL THEN
      l_runnum := 1;
    END IF;
  
    --Insert Global Line Information GTT
    l_sec := 'Loading LINE global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    INSERT INTO xxcus.xxcusie_cti_gl_gtt
      (SELECT a.accounted_cr
             ,a.accounted_dr
             ,a.je_header_id
             ,a.je_line_num
             ,c.segment1
             ,c.segment2
             ,c.segment3
             ,c.segment4
             ,c.segment5
             ,c.segment6
             ,c.segment7
             ,b.je_category
             ,d.gl_sl_link_id
             ,to_char(a.effective_date, 'YYYY-MM-DD') eff_date
             ,to_char(b.posted_date, 'YYYY-MM-DD') post_date
             ,substr(a.description, 1, 50) descr
             ,a.period_name
         FROM gl.gl_je_lines          a
             ,gl.gl_je_headers        b
             ,gl.gl_code_combinations c
             ,gl.gl_import_references d
        WHERE a.code_combination_id = c.code_combination_id
          AND a.je_header_id = b.je_header_id
          AND (b.je_header_id, a.je_line_num) NOT IN
              (SELECT i.je_header_id
                     ,i.je_line_num
                 FROM xxcus.xxcusie_cti_aud_tbl i
                WHERE i.je_header_id = b.je_header_id
                  AND i.je_line_num = a.je_line_num
                  AND i.period_name = p_fperiod)
          AND b.currency_code <> 'STAT'
          AND a.ledger_id = l_ledger
          AND a.period_name = p_fperiod
          AND a.je_header_id = d.je_header_id(+)
          AND a.je_line_num = d.je_line_num(+)
          AND c.segment1 IN
              (SELECT h.description
                 FROM apps.fnd_lookup_values h
                WHERE h.lookup_type = 'HDS_CTIGLEXTR'
                  AND enabled_flag = 'Y'
                  AND nvl(end_date_active, SYSDATE) >= SYSDATE
                  AND h.lookup_code LIKE 'SEG1_%')
          AND a.status = 'P'
          AND c.chart_of_accounts_id = l_coa);
  
    l_sec := 'Load audit table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Cursor to load audit table
    FOR c_gl_cti_audit_rec IN (SELECT DISTINCT je_header_id
                                              ,je_line_num
                                              ,period_name
                                 FROM xxcus.xxcusie_cti_gl_gtt)
    LOOP
    
      INSERT INTO xxcus.xxcusie_cti_aud_tbl
      VALUES
        (c_gl_cti_audit_rec.je_header_id
        ,c_gl_cti_audit_rec.je_line_num
        ,c_gl_cti_audit_rec.period_name
        ,nvl(fnd_global.conc_request_id, 0)
        ,SYSDATE
        ,nvl(fnd_global.user_name, 'NA')
        ,nvl(fnd_global.resp_name, 'NA')
        ,'File created'
        ,SYSDATE
        ,l_runnum
        ,l_sequence);
    
    END LOOP;
  
    --INSERT DR SUM and CR SUM HERE
    --Insert Global Line Information GTT
    l_sec := 'Loading CR 1 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the Intercompany CR transactions
    INSERT INTO xxcus.xxcusie_cti_gl_gtt2
      (SELECT a.descr
             ,0
             ,a.segment1
             ,a.segment2
             ,a.segment3
             ,a.segment4
             ,a.segment5
             ,a.segment6
             ,a.segment7
             ,a.period_name
             ,SUM(nvl(a.accounted_cr, 0))
             ,NULL
             ,NULL
         FROM xxcus.xxcusie_cti_gl_gtt a
        WHERE a.accounted_cr IS NOT NULL
          AND a.gl_sl_link_id IS NULL
           OR a.descr LIKE 'Intracompany%'
        GROUP BY a.descr
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7
                ,a.period_name);
  
    l_sec := 'Loading DR 2 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the Intercompany DB transactions
    INSERT INTO xxcus.xxcusie_cti_gl_gtt2
      (SELECT a.descr
             ,0
             ,a.segment1
             ,a.segment2
             ,a.segment3
             ,a.segment4
             ,a.segment5
             ,a.segment6
             ,a.segment7
             ,a.period_name
             ,NULL
             ,SUM(nvl(a.accounted_dr, 0))
             ,NULL
         FROM xxcus.xxcusie_cti_gl_gtt a
        WHERE a.accounted_dr IS NOT NULL
          AND a.gl_sl_link_id IS NULL
           OR a.descr LIKE 'Intracompany%'
        GROUP BY a.descr
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7
                ,a.period_name);
  
    l_sec := 'Loading DR 3 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the NON-Intercompany CR transactions
    INSERT INTO xxcus.xxcusie_cti_gl_gtt2
      (SELECT substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50) line_item_text
             ,i.invoice_num
             ,gcc.segment1
             ,gcc.segment2
             ,gcc.segment3
             ,gcc.segment4
             ,gcc.segment5
             ,gcc.segment6
             ,gcc.segment7
             ,p_fperiod
             ,SUM(nvl(t.accounted_cr, 0))
             ,NULL
             ,(SELECT (SELECT p.employee_number
                         FROM hr.per_all_people_f p
                        WHERE p.person_id = v.employee_id
                          AND object_version_number IN
                              (SELECT MAX(object_version_number)
                                 FROM hr.per_all_people_f b
                                WHERE b.person_id = p.person_id)) employee_num
                 FROM apps.po_vendors v
                WHERE v.vendor_id = i.vendor_id) employee_num
         FROM xla.xla_ae_lines        t
             ,xla.xla_ae_headers      h
             ,gl.gl_import_references gir
             ,gl.gl_code_combinations gcc
             ,ap.ap_invoices_all      i
             ,ap.ap_suppliers         s
        WHERE t.ae_header_id = h.ae_header_id
          AND t.accounted_cr IS NOT NULL
          AND nvl(t.description, 'X') NOT LIKE 'Intracompany%' --added 3-10-2012  --Version 1.1
          AND t.gl_sl_link_id = gir.gl_sl_link_id
          AND h.event_id IN
              (SELECT DISTINCT accounting_event_id
                 FROM ap.ap_invoice_distributions_all d
                WHERE d.accounting_event_id = h.event_id
                  AND d.invoice_id = i.invoice_id)
          AND i.vendor_id = s.vendor_id
          AND t.code_combination_id = gcc.code_combination_id
          AND t.gl_sl_link_id IN
              (SELECT DISTINCT a.gl_sl_link_id
                 FROM xxcus.xxcusie_cti_gl_gtt a
                WHERE a.gl_sl_link_id = t.gl_sl_link_id
                  AND a.gl_sl_link_id IS NOT NULL)
        GROUP BY substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50)
                ,i.invoice_num
                ,gcc.segment1
                ,gcc.segment2
                ,gcc.segment3
                ,gcc.segment4
                ,gcc.segment5
                ,gcc.segment6
                ,gcc.segment7
                ,i.vendor_id);
  
    l_sec := 'Loading DR 4 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the NON-Intercompany DB transactions
    INSERT INTO xxcus.xxcusie_cti_gl_gtt2
      (SELECT substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50) line_item_text
             ,i.invoice_num
             ,gcc.segment1
             ,gcc.segment2
             ,gcc.segment3
             ,gcc.segment4
             ,gcc.segment5
             ,gcc.segment6
             ,gcc.segment7
             ,p_fperiod
             ,NULL
             ,SUM(nvl(t.accounted_dr, 0))
             ,(SELECT (SELECT p.employee_number
                         FROM hr.per_all_people_f p
                        WHERE p.person_id = v.employee_id
                          AND object_version_number IN
                              (SELECT MAX(object_version_number)
                                 FROM hr.per_all_people_f b
                                WHERE b.person_id = p.person_id)) employee_num
                 FROM apps.po_vendors v
                WHERE v.vendor_id = i.vendor_id) employee_num
         FROM xla.xla_ae_lines        t
             ,xla.xla_ae_headers      h
             ,gl.gl_import_references gir
             ,gl.gl_code_combinations gcc
             ,ap.ap_invoices_all      i
             ,ap.ap_suppliers         s
        WHERE t.ae_header_id = h.ae_header_id
          AND t.accounted_dr IS NOT NULL
          AND nvl(t.description, 'X') NOT LIKE 'Intracompany%' --added 3-10-2012  --Version 1.1
          AND t.gl_sl_link_id = gir.gl_sl_link_id
          AND h.event_id IN
              (SELECT DISTINCT accounting_event_id
                 FROM ap.ap_invoice_distributions_all d
                WHERE d.accounting_event_id = h.event_id
                  AND d.invoice_id = i.invoice_id)
          AND i.vendor_id = s.vendor_id
          AND t.code_combination_id = gcc.code_combination_id
          AND t.gl_sl_link_id IN
              (SELECT DISTINCT a.gl_sl_link_id
                 FROM xxcus.xxcusie_cti_gl_gtt a
                WHERE a.gl_sl_link_id = t.gl_sl_link_id
                  AND a.gl_sl_link_id IS NOT NULL)
        GROUP BY substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50)
                ,i.invoice_num
                ,gcc.segment1
                ,gcc.segment2
                ,gcc.segment3
                ,gcc.segment4
                ,gcc.segment5
                ,gcc.segment6
                ,gcc.segment7
                ,i.vendor_id);
  
    --Cursor to get the LINE rows
    l_sec := 'Writing LINE File; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    FOR c_glfilter_cti_rec IN (SELECT a.descr
                                     ,a.report_num
                                     ,a.segment1
                                     ,a.segment2
                                     ,a.segment3
                                     ,a.segment4
                                     ,a.segment5
                                     ,a.segment6
                                     ,a.segment7
                                     ,a.period_name
                                     ,nvl(a.accounted_dr, 0) accounted_dr
                                     ,nvl(a.accounted_cr, 0) accounted_cr
                                     ,a.employee_num
                                 FROM xxcus.xxcusie_cti_gl_gtt2 a
                                     ,(SELECT b.segment1
                                             ,b.segment2
                                             ,b.segment3
                                             ,b.segment4
                                             ,SUM(nvl(accounted_dr, 0) -
                                                  nvl(accounted_cr, 0)) amount
                                         FROM xxcus.xxcusie_cti_gl_gtt2 b
                                        GROUP BY b.segment1
                                                ,b.segment2
                                                ,b.segment3
                                                ,b.segment4) b
                                WHERE a.segment1 = b.segment1
                                  AND a.segment2 = b.segment2
                                  AND a.segment3 = b.segment3
                                  AND a.segment4 = b.segment4
                                ORDER BY segment1
                                        ,segment2
                                        ,segment3
                                        ,segment4
                                        ,segment5
                                        ,segment6
                                        ,segment7)
    LOOP
      l_tot_accts := 0;
    
      l_debit  := nvl(c_glfilter_cti_rec.accounted_dr, 0);
      l_credit := nvl(c_glfilter_cti_rec.accounted_cr, 0);
      l_total  := l_debit - l_credit;
      --l_abstotal := abs(l_total);
      IF l_total >= 0 THEN
        l_d_c_id := '40';
      ELSE
        l_d_c_id := '50';
      END IF;
      l_text := c_glfilter_cti_rec.descr;
    
      SELECT DISTINCT description
        INTO l_seg_descr
        FROM apps.fnd_flex_values_vl
       WHERE value_category LIKE 'XXCUS_GL_PRODUCT'
         AND flex_value = c_glfilter_cti_rec.segment1;
    
      INSERT INTO xxcus.xxcusie_cti_filter_extract_tbl
      VALUES
        (l_d_c_id
        ,(CASE WHEN l_total > 0 THEN abs(l_total) ELSE 0 END)
        ,(CASE WHEN l_total < 0 THEN abs(l_total) ELSE 0 END)
        ,l_text
        ,c_glfilter_cti_rec.report_num
        ,c_glfilter_cti_rec.segment1
        ,l_seg_descr
        ,c_glfilter_cti_rec.segment2
        ,c_glfilter_cti_rec.segment3
        ,c_glfilter_cti_rec.segment4
        ,c_glfilter_cti_rec.segment5
        ,c_glfilter_cti_rec.segment6
        ,c_glfilter_cti_rec.segment7
        ,c_glfilter_cti_rec.period_name
        ,l_runnum
        ,c_glfilter_cti_rec.employee_num);
    
    END LOOP;
  
    DELETE FROM xxcus.xxcusie_cti_filter_extract_tbl
     WHERE debit = 0
       AND credit = 0;
  
    COMMIT;
  
    FOR c_glprocess_cti_rec IN (SELECT DISTINCT report_num
                                               ,segment1
                                               ,segment2
                                               ,segment3
                                               ,segment4
                                               ,segment5
                                               ,segment6
                                               ,segment7
                                  FROM xxcus.xxcusie_cti_filter_extract_tbl
                                 WHERE segment4 = '211100')
    LOOP
    
      l_whack := 0;
    
      SELECT SUM(t.debit) - SUM(t.credit)
        INTO l_whack
        FROM xxcus.xxcusie_cti_filter_extract_tbl t
       WHERE report_num = c_glprocess_cti_rec.report_num
         AND segment1 = c_glprocess_cti_rec.segment1
         AND segment2 = c_glprocess_cti_rec.segment2
         AND segment3 = c_glprocess_cti_rec.segment3
         AND segment4 = c_glprocess_cti_rec.segment4
         AND segment5 = c_glprocess_cti_rec.segment5
         AND segment6 = c_glprocess_cti_rec.segment6
         AND segment7 = c_glprocess_cti_rec.segment7
       GROUP BY segment1
               ,segment2
               ,segment3
               ,segment4
               ,segment5
               ,segment6
               ,segment7;
    
      IF l_whack = 0 THEN
      
        DELETE FROM xxcus.xxcusie_cti_filter_extract_tbl t
         WHERE report_num = c_glprocess_cti_rec.report_num
           AND segment1 = c_glprocess_cti_rec.segment1
           AND segment2 = c_glprocess_cti_rec.segment2
           AND segment3 = c_glprocess_cti_rec.segment3
           AND segment4 = c_glprocess_cti_rec.segment4
           AND segment5 = c_glprocess_cti_rec.segment5
           AND segment6 = c_glprocess_cti_rec.segment6
           AND segment7 = c_glprocess_cti_rec.segment7;
      
        COMMIT;
      
      END IF;
    
    END LOOP;
  
    FOR c_interc IN (SELECT SUM(debit) - SUM(credit) total
                           ,segment1
                           ,segment1_decsr
                           ,segment2
                           ,segment3
                           ,segment4
                           ,segment5
                           ,segment6
                           ,segment7
                           ,period_name
                           ,run_num
                       FROM xxcus.xxcusie_cti_filter_extract_tbl
                      WHERE segment2 = 'Z9999'
                      GROUP BY segment1
                              ,segment1_decsr
                              ,segment2
                              ,segment3
                              ,segment4
                              ,segment5
                              ,segment6
                              ,segment7
                              ,period_name
                              ,run_num)
    LOOP
    
      l_line_num := l_line_num + 1;
    
      INSERT INTO xxcus.xxcusie_et_extract_tbl
      VALUES
        (l_sequence
        ,l_line_num
        ,l_sourcesys
        ,l_date
        ,(CASE WHEN c_interc.total >= 0 THEN '40' ELSE '50' END) --c_interc.d_c_id
        ,(CASE WHEN c_interc.total > 0 THEN abs(c_interc.total) ELSE 0 END) --debit
        ,(CASE WHEN c_interc.total < 0 THEN abs(c_interc.total) ELSE 0 END) --credit
        ,'Intercompany line added by Posting' --cti_text
        ,c_interc.segment1
        ,c_interc.segment1_decsr
        ,c_interc.segment2
        ,c_interc.segment3
        ,c_interc.segment4
        ,c_interc.segment5
        ,c_interc.segment6
        ,c_interc.segment7
        ,c_interc.period_name
        ,c_interc.run_num
        ,' '); --added space for ET
    
    END LOOP;
  
    FOR c_gl_cti_rec IN (SELECT *
                           FROM xxcus.xxcusie_cti_filter_extract_tbl
                          WHERE segment2 <> 'Z9999')
    LOOP
    
      l_line_num := l_line_num + 1;
    
      INSERT INTO xxcus.xxcusie_et_extract_tbl
      VALUES
        (l_sequence
        ,l_line_num
        ,l_sourcesys
        ,l_date
        ,c_gl_cti_rec.d_c_id
        ,abs(c_gl_cti_rec.debit)
        ,abs(c_gl_cti_rec.credit)
        ,c_gl_cti_rec.cti_text
        ,c_gl_cti_rec.segment1
        ,c_gl_cti_rec.segment1_decsr
        ,c_gl_cti_rec.segment2
        ,c_gl_cti_rec.segment3
        ,c_gl_cti_rec.segment4
        ,c_gl_cti_rec.segment5
        ,c_gl_cti_rec.segment6
        ,c_gl_cti_rec.segment7
        ,c_gl_cti_rec.period_name
        ,c_gl_cti_rec.run_num
        ,nvl(c_gl_cti_rec.employee_num, ' ')); --added space for ET
    
    END LOOP;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      DELETE FROM xxcus.xxcusie_cti_aud_tbl
       WHERE conc_request_id = l_req_id;
      COMMIT;
    
      DELETE FROM xxcus.xxcusie_et_extract_tbl
       WHERE period_name = p_fperiod
         AND run_num = l_runnum
         AND sequence = l_sequence;
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for XXCUSIE_GL_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error during procedure load_cti_gl_int',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
    WHEN OTHERS THEN
      DELETE FROM xxcus.xxcusie_cti_aud_tbl
       WHERE conc_request_id = l_req_id;
      COMMIT;
    
      DELETE FROM xxcus.xxcusie_et_extract_tbl
       WHERE period_name = p_fperiod
         AND run_num = l_runnum
         AND sequence = l_sequence;
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for XXCUSIE_GL_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error during procedure load_cti_gl_int',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
  END load_cti_gl_int;

  /*******************************************************************************
  * Procedure:   AP_PNC_TRG_MONITOR
  * Description:
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/25/2011    Kathy Poling    Initial creation of the procedure copied
                                        from R11
  1.1     04/25/2012    Luong Vu        SR 138616 - Add validation conditions
  1.2     05/21/2013    Kathy Poling    SR 204912 Remove version 1.1 vlidation conditions
                                        added parm p_file name and the staging trigger
                                        will pass the file naame and submit the request
                                        and if the visa loader fails a notification
                                        will be sent.
  ********************************************************************************/

  PROCEDURE ap_pnc_trg_monitor(errbuf  OUT VARCHAR2
                              ,retcode OUT NUMBER
                              ,p_file  IN VARCHAR2) IS
  
    --Intialize Variables
    l_map_log        VARCHAR2(1) := 'N';
    l_sid            VARCHAR2(8);
    l_err_msg        VARCHAR2(2000);
    l_err_code       NUMBER;
    l_sec            VARCHAR2(150);
    l_body           VARCHAR2(4000);
    l_procedure_name VARCHAR2(75) := 'XXCUSIE_GL_PKG.AP_PNC_TRG_MONITOR';
    l_submit_cnt     NUMBER;
    l_program_cnt    NUMBER;
    l_day            VARCHAR2(15);
  
    pl_dflt_email fnd_user.email_address%TYPE := 'hds-HDSFASTap-u1@hdsupply.com';
    pl_instance   VARCHAR2(100);
    pl_sender     VARCHAR2(100);
    p_host        VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
    p_hostport    VARCHAR2(20) := '25';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    v_phase         VARCHAR2(50);
    v_status        VARCHAR2(50);
    v_dev_status    VARCHAR2(50);
    v_dev_phase     VARCHAR2(50);
    v_message       VARCHAR2(50);
    v_error_message VARCHAR2(150);
  
    --Start Main Program
  BEGIN
    l_sec := 'Begin Program; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Get Database
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
  
    pl_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com'; -- v1.1
  
    --Cursor to get the missing account log lines
    l_sec := 'Waiting for Visa Loader to process File; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    FOR c_ap_pnc_rec IN (SELECT *
                           FROM xxcus.xxcusap_pnc_submit_tbl
                          WHERE processed = 'N'
                            AND file_name = p_file) --v1.2
    LOOP
      l_sec := 'File Name:  ' || p_file || ' Request ID:  ' ||
               c_ap_pnc_rec.request_id;
      fnd_file.put_line(fnd_file.log, l_sec);
      --Wait for request to finish
      IF fnd_concurrent.wait_for_request(c_ap_pnc_rec.request_id, 6, 15000,
                                         v_phase, v_status, v_dev_phase,
                                         v_dev_status, v_message) THEN
        v_error_message := 'ReqID=' || c_ap_pnc_rec.request_id ||
                           ' DPhase ' || v_dev_phase || ' DStatus ' ||
                           v_dev_status || ' MSG ' || v_message;
        fnd_file.put_line(fnd_file.log, v_error_message); --v1.2
        fnd_file.put_line(fnd_file.output, v_error_message); --v1.2
      
        l_sec := 'ReqID=' || c_ap_pnc_rec.request_id || ' DPhase ' ||
                 v_dev_phase || ' DStatus ' || v_dev_status || ' MSG ' ||
                 v_message;
      
        IF v_dev_phase != 'COMPLETE'
           OR v_dev_status != 'NORMAL' THEN
        
          l_map_log := 'Y';
        
          l_body := l_body || chr(10) || 'File Name: ' ||
                    c_ap_pnc_rec.file_name || ' with request id ' ||
                    c_ap_pnc_rec.request_id || ' was submitted for ' ||
                    c_ap_pnc_rec.card_program || ' by ' ||
                    c_ap_pnc_rec.requestor || ' under responsibility ' ||
                    c_ap_pnc_rec.requestor_resp || ' on ' ||
                    c_ap_pnc_rec.creation_date || '.' || chr(10);
        
        END IF;
      ELSE
      
        l_map_log := 'Y';
      
        l_body := l_body || chr(10) || 'File Name: ' ||
                  c_ap_pnc_rec.file_name || ' with request id ' ||
                  c_ap_pnc_rec.request_id || ' was submitted for ' ||
                  c_ap_pnc_rec.card_program || ' by ' ||
                  c_ap_pnc_rec.requestor || ' under responsibility ' ||
                  c_ap_pnc_rec.requestor_resp || ' at ' ||
                  c_ap_pnc_rec.creation_date || '.' || chr(10);
      
      END IF;
    
      UPDATE xxcus.xxcusap_pnc_submit_tbl r
         SET r.processed      = 'Y'
            ,r.request_status = v_dev_status
            ,r.request_phase  = v_dev_phase
       WHERE r.submit_sequence = c_ap_pnc_rec.submit_sequence;
    
      COMMIT;
    
    END LOOP;
  
    l_body := l_body || chr(10);
  
    IF l_map_log = 'Y' THEN
      BEGIN
        SELECT NAME
          INTO pl_instance
          FROM v$database;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    
      --Multiple Attachments add a ('1st' , '2nd' , '3rd')
      --Email Addresses XXCUS_PROCESS_INFO table
      pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';
    
      xxcus_misc_pkg.html_email(p_to => pl_dflt_email, p_from => pl_sender,
                                p_text => 'test',
                                p_subject => 'The PNC file load program has not finished successfully.',
                                p_html => 'Please review the provided request id logs. ' ||
                                           chr(10) || l_body ||
                                           '          THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                p_smtp_hostname => p_host,
                                p_smtp_portnum => p_hostport);
    
      xxcus_misc_pkg.html_email(p_to => l_distro_list, p_from => pl_sender,
                                -- v1.1
                                p_text => 'test',
                                p_subject => 'The PNC file load program has not finished successfully.',
                                p_html => 'Please review the provided request id logs. ' ||
                                           chr(10) || l_body ||
                                           '          THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                p_smtp_hostname => p_host,
                                p_smtp_portnum => p_hostport);
    
    END IF;
    --v1.2
    /*   5/17/2013
      -- We expect 1 PNC file for each card program each day.  If we don't, send a warning.
      l_sec := 'Compare unique card program count vs. file prefix count; ';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      SELECT COUNT(DISTINCT(substr(file_name, 7, 5)))
        INTO l_submit_cnt
        FROM xxcus.xxcusap_pnc_tbl t
       WHERE trunc(creation_date) = trunc(SYSDATE);
    
      SELECT COUNT(card_program_id)
        INTO l_program_cnt
        FROM ap.ap_card_programs_all;
    
      SELECT to_char(trunc(SYSDATE), 'DAY') INTO l_day FROM dual;
    
      IF l_submit_cnt <> l_program_cnt AND l_day <> 'MONDAY'
      THEN
        l_body := l_body || chr(10) ||
                  'Warning - We did not received PNC file for every card program today.  Please verify.' ||
                  chr(10);
      
        xxcus_misc_pkg.html_email(p_to            => pl_dflt_email
                                 ,p_from          => pl_sender
                                 ,p_text          => 'test'
                                 ,p_subject       => 'Warning - Not All PNC Files Received'
                                 ,p_html          => l_body ||
                                                     'THIS IS A SEND ONLY EMAIL - DO NOT REPLY '
                                 ,p_smtp_hostname => p_host
                                 ,p_smtp_portnum  => p_hostport);
      
        xxcus_misc_pkg.html_email(p_to            => l_distro_list
                                 ,p_from          => pl_sender
                                 ,p_text          => 'test'
                                 ,p_subject       => 'Warning - Not All PNC Files Received'
                                 ,p_html          => l_body ||
                                                     'THIS IS A SEND ONLY EMAIL - DO NOT REPLY '
                                 ,p_smtp_hostname => p_host
                                 ,p_smtp_portnum  => p_hostport);
      END IF;
    */
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running the PNC trigger montior with OTHERS Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');
    
  END ap_pnc_trg_monitor;

  /*******************************************************************************
  * Procedure:   load_fm_gl_int
  * Description: Creates output file in output directory indicated named
  *              'FM_GL_IEXP_<period>_<date in 'YYYYMMDDHHMISS' format>.txt'
  *              Extract to include all data even if acct balance is 0.00
  *              Concurrent 'Request HDS GL FM Outbound' under the responsibility
  *              XXCUS_CON
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/27/2011    Kathy Poling    Initial creation of the procedure copied from R11
  1.1     04/14/2012    Kathy Poling    Added NVL to description line
                                        Service Ticket 136532
  ********************************************************************************/

  PROCEDURE load_fm_gl_int(errbuf    OUT VARCHAR2
                          ,retcode   OUT NUMBER
                          ,p_fperiod IN VARCHAR2) IS
  
    --Intialize Variables
    l_err_msg         VARCHAR2(2000);
    l_err_code        NUMBER;
    l_processed_count NUMBER := 0;
    l_sec             VARCHAR2(2000) :=Null; -- Ver 1.2 
    l_tot_accts       NUMBER := 0;
    l_debit           NUMBER := 0;
    l_credit          NUMBER := 0;
    l_seg_descr       VARCHAR2(240);
    l_lob             VARCHAR2(3);
    l_date            DATE;
    l_sequence        NUMBER;
    l_line_num        NUMBER := 0;
    l_total           NUMBER := 0;
    l_d_c_id          VARCHAR2(10);
    l_runnum          NUMBER := 0;
    l_text            VARCHAR2(50) := 'NA';
    l_whack           NUMBER := 0;
    l_sourcesys       VARCHAR2(75) := 'ORCL-FM';
    l_count NUMBER :=0; --ver 1.2
  
    l_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_coa      CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := '50328'; --HDS Chart of Accounts
    l_ledger   CONSTANT gl_period_statuses.ledger_id%TYPE := 2061; --HD Supply USD
    l_period         gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_shortperiod    gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_procedure_name VARCHAR2(75) := 'XXCUSIE_GL_PKG.LOAD_FM_GL_INT';
  
    l_err_callfrom VARCHAR2(75) DEFAULT 'XXCUS_IEXPENSE_GL_PKG.LOAD_FM_GL_INT';
    --l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id      NUMBER := fnd_global.conc_request_id;
  
    --Start Main Program
  BEGIN
    --
    l_sec := 'Begin - '||l_procedure_name; -- ver 1.2
    fnd_file.put_line(fnd_file.output, l_sec); -- ver 1.2
    --
    SELECT period_name
          ,substr(period_name, 1, 3)
      INTO l_period
          ,l_shortperiod
      FROM gl.gl_periods
     WHERE period_set_name = l_calendar
       AND start_date <= trunc(SYSDATE)
       AND end_date >= trunc(SYSDATE);
       --
        SELECT trunc(SYSDATE) -- ver 1.2
          INTO l_date
          FROM dual;
      --
      SELECT xxcus.xxcusie_et_extract_s.nextval
      INTO l_sequence
      FROM dual;
      --
        l_sec := 'l_period ='||l_period||', l_shortperiod ='||l_shortperiod||', l_date ='||l_date||', xxcus.xxcusie_et_extract_s.nextval (l_sequence) ='||l_sequence; -- ver 1.2
        fnd_file.put_line(fnd_file.output, l_sec);  -- ver 1.2
       --
    BEGIN
      l_sec := 'Flush the table  XXCUS.XXCUSIE_FM_FILTER_EXTRACT_TB.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSIE_FM_FILTER_EXTRACT_TBL';
            fnd_file.put_line(fnd_file.output, l_sec);  -- ver 1.2
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSIE_FM_FILTER_EXTRACT_TBL: ' ||
                     SQLERRM;
        l_sec := substr('Failed to Truncate XXCUS.XXCUSIE_FM_FILTER_EXTRACT_TBL, Msg :' ||SQLERRM, 1, 2000);                     
            fnd_file.put_line(fnd_file.output, l_sec);  -- ver 1.2                     
        RAISE program_error;
    END;
  
    --Determine Run Number
    BEGIN
      SELECT (MAX(nvl(run_num, 0)) + 1)
        INTO l_runnum
        FROM xxcus.xxcusie_fm_aud_tbl
       WHERE period_name = p_fperiod;
    EXCEPTION
      WHEN no_data_found THEN
        l_runnum := 1;
      WHEN OTHERS THEN
        l_runnum := 1;
    END;
  
    IF l_runnum IS NULL THEN
      l_runnum := 1;
    END IF;
      --
        l_sec := 'Current run number (l_runnum) ='||l_runnum; -- ver 1.2
        fnd_file.put_line(fnd_file.output, l_sec);  -- ver 1.2
       --  
    --Insert Global Line Information GTT
    l_sec := 'Loading xxcus.xxcusie_fm_gl_gtt for gl manual journals where currency is not STAT and product FM (Segment 11)'; -- ver 1.2
    fnd_file.put_line(fnd_file.output, l_sec);
  
    INSERT INTO xxcus.xxcusie_fm_gl_gtt
      (SELECT a.accounted_cr
             ,a.accounted_dr
             ,a.je_header_id
             ,a.je_line_num
             ,c.segment1
             ,c.segment2
             ,c.segment3
             ,c.segment4
             ,c.segment5
             ,c.segment6
             ,c.segment7
             ,b.je_category
             ,d.gl_sl_link_id
             ,to_char(a.effective_date, 'YYYY-MM-DD') eff_date
             ,to_char(b.posted_date, 'YYYY-MM-DD') post_date
             ,substr(a.description, 1, 50) descr
             ,a.period_name
         FROM gl.gl_je_lines          a
             ,gl.gl_je_headers        b
             ,gl.gl_code_combinations c
             ,gl.gl_import_references d
        WHERE a.code_combination_id = c.code_combination_id
          AND a.je_header_id = b.je_header_id
          AND (b.je_header_id, a.je_line_num) NOT IN
              (SELECT i.je_header_id
                     ,i.je_line_num
                 FROM xxcus.xxcusie_fm_aud_tbl i
                WHERE i.je_header_id = b.je_header_id
                  AND i.je_line_num = a.je_line_num
                  AND i.period_name = p_fperiod)
          AND b.currency_code <> 'STAT'
          AND a.ledger_id = l_ledger
          AND a.period_name = p_fperiod
          AND a.je_header_id = d.je_header_id(+)
          AND a.je_line_num = d.je_line_num(+)
          AND c.segment1 IN
              (SELECT h.description
                 FROM apps.fnd_lookup_values h
                WHERE h.lookup_type = 'HDS_FMGLEXTR'
                  AND enabled_flag = 'Y'
                  AND nvl(end_date_active, SYSDATE) >= SYSDATE
                  AND h.lookup_code LIKE 'SEG1_%')
          AND a.status = 'P'
          AND c.chart_of_accounts_id = l_coa
          --AND b.je_source ='Spreadsheet'
          );
      --
        l_sec := 'First pass in table xxcus.xxcusie_fm_gl_gtt, total records inserted ='||sql%rowcount; -- ver 1.2
        fnd_file.put_line(fnd_file.output, l_sec);  -- ver 1.2
        --
    l_sec := 'Load audit table; ';
    --fnd_file.put_line(fnd_file.log, l_sec); -- ver 1.2 
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Cursor to load audit table
    FOR c_gl_fm_audit_rec IN (SELECT DISTINCT je_header_id
                                             ,je_line_num
                                             ,period_name
                                FROM xxcus.xxcusie_fm_gl_gtt)
    LOOP
    
      INSERT INTO xxcus.xxcusie_fm_aud_tbl
      VALUES
        (c_gl_fm_audit_rec.je_header_id
        ,c_gl_fm_audit_rec.je_line_num
        ,c_gl_fm_audit_rec.period_name
        ,nvl(fnd_global.conc_request_id, 0)
        ,SYSDATE
        ,nvl(fnd_global.user_name, 'NA')
        ,nvl(fnd_global.resp_name, 'NA')
        ,'File created'
        ,SYSDATE
        ,l_runnum
        ,l_sequence);
         --
         l_count :=l_count +1; -- ver 1.2
         --
    END LOOP;
        --
        l_sec := 'First pass in table xxcus.xxcusie_fm_aud_tbl, total records inserted ='||sql%rowcount; -- ver 1.2
        fnd_file.put_line(fnd_file.output, l_sec);  -- ver 1.2
       --
    --INSERT DR SUM and CR SUM HERE
    --Insert Global Line Information GTT
    --fnd_file.put_line(fnd_file.log, l_sec); -- ver 1.2
    --
    --Enter the Intercompany CR transactions
    INSERT INTO xxcus.xxcusie_fm_gl_gtt2
      (SELECT a.descr
             ,0
             ,a.segment1
             ,a.segment2
             ,a.segment3
             ,a.segment4
             ,a.segment5
             ,a.segment6
             ,a.segment7
             ,a.period_name
             ,SUM(nvl(a.accounted_cr, 0))
             ,NULL
             ,NULL
         FROM xxcus.xxcusie_fm_gl_gtt a
        WHERE a.accounted_cr IS NOT NULL
          AND a.gl_sl_link_id IS NULL
           OR a.descr LIKE 'Intracompany%'
        GROUP BY a.descr
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7
                ,a.period_name);
      l_sec := 'First pass into  xxcus.xxcusie_fm_gl_gtt2, Intracompany CR only transactions (manual journals), rows inserted :'||sql%rowcount; -- ver 1.2    
    fnd_file.put_line(fnd_file.output, l_sec);
    --
    --Enter the Intercompany DB transactions
    INSERT INTO xxcus.xxcusie_fm_gl_gtt2
      (SELECT a.descr
             ,0
             ,a.segment1
             ,a.segment2
             ,a.segment3
             ,a.segment4
             ,a.segment5
             ,a.segment6
             ,a.segment7
             ,a.period_name
             ,NULL
             ,SUM(nvl(a.accounted_dr, 0))
             ,NULL
         FROM xxcus.xxcusie_fm_gl_gtt a
        WHERE a.accounted_dr IS NOT NULL
          AND a.gl_sl_link_id IS NULL
           OR a.descr NOT LIKE 'Intracompany%' --Ver 1.2
        GROUP BY a.descr
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7
                ,a.period_name);
     --                
     l_sec := 'Second pass into  xxcus.xxcusie_fm_gl_gtt2,  Intracompany DR only transactions (manual journals), rows inserted :'||sql%rowcount; -- ver 1.2  
     fnd_file.put_line(fnd_file.output, l_sec);
     --      
     -- Enter the NON-Intercompany CR transactions
    INSERT INTO xxcus.xxcusie_fm_gl_gtt2
      (SELECT --substr(t.reference5 || ' - ' || t.reference1, 1, 50) line_item_text
       --,t.reference5
        substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50) line_item_text
      ,i.invoice_num
      ,gcc.segment1
      ,gcc.segment2
      ,gcc.segment3
      ,gcc.segment4
      ,gcc.segment5
      ,gcc.segment6
      ,gcc.segment7
      ,p_fperiod
      ,SUM(nvl(t.accounted_cr, 0))
      ,NULL
      ,(SELECT (SELECT p.employee_number
                   FROM hr.per_all_people_f p
                  WHERE p.person_id = v.employee_id
                    AND object_version_number IN
                        (SELECT MAX(object_version_number)
                           FROM hr.per_all_people_f b
                          WHERE b.person_id = p.person_id)) employee_num
           FROM apps.po_vendors v
          WHERE v.vendor_id = i.vendor_id) employee_num
         FROM xla.xla_ae_lines        t
             ,xla.xla_ae_headers      h
             ,gl.gl_import_references gir
             ,gl.gl_code_combinations gcc
             ,ap.ap_invoices_all      i
             ,ap.ap_suppliers         s
        WHERE t.ae_header_id = h.ae_header_id
          AND t.accounted_cr IS NOT NULL
          AND nvl(t.description, 'X') NOT LIKE 'Intracompany%' --added 3-10-2012  --Version 1.1
          AND t.gl_sl_link_id = gir.gl_sl_link_id
          AND h.event_id IN
              (SELECT DISTINCT accounting_event_id
                 FROM ap.ap_invoice_distributions_all d
                WHERE d.accounting_event_id = h.event_id
                  AND d.invoice_id = i.invoice_id)
          AND i.vendor_id = s.vendor_id
          AND t.code_combination_id = gcc.code_combination_id
          AND t.gl_sl_link_id IN
              (SELECT DISTINCT a.gl_sl_link_id
                 FROM xxcus.xxcusie_fm_gl_gtt a
                WHERE a.gl_sl_link_id = t.gl_sl_link_id
                  AND a.gl_sl_link_id IS NOT NULL)
        GROUP BY substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50)
                ,i.invoice_num
                ,gcc.segment1
                ,gcc.segment2
                ,gcc.segment3
                ,gcc.segment4
                ,gcc.segment5
                ,gcc.segment6
                ,gcc.segment7
                ,i.vendor_id);
    l_sec := 'First pass into  xxcus.xxcusie_fm_gl_gtt2,  Non Intracompany CR only transactions (AP/GL journals), rows_inserted '||sql%rowcount; -- ver 1.2  
    fnd_file.put_line(fnd_file.output, l_sec);
    --     
    --Enter the NON-Intercompany DB transactions
    INSERT INTO xxcus.xxcusie_fm_gl_gtt2
      (SELECT substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50) line_item_text
             ,i.invoice_num
             ,gcc.segment1
             ,gcc.segment2
             ,gcc.segment3
             ,gcc.segment4
             ,gcc.segment5
             ,gcc.segment6
             ,gcc.segment7
             ,p_fperiod
             ,NULL
             ,SUM(nvl(t.accounted_dr, 0))
             ,(SELECT (SELECT p.employee_number
                         FROM hr.per_all_people_f p
                        WHERE p.person_id = v.employee_id
                          AND object_version_number IN
                              (SELECT MAX(object_version_number)
                                 FROM hr.per_all_people_f b
                                WHERE b.person_id = p.person_id)) employee_num
                 FROM apps.po_vendors v
                WHERE v.vendor_id = i.vendor_id) employee_num
         FROM xla.xla_ae_lines        t
             ,xla.xla_ae_headers      h
             ,gl.gl_import_references gir
             ,gl.gl_code_combinations gcc
             ,ap.ap_invoices_all      i
             ,ap.ap_suppliers         s
        WHERE t.ae_header_id = h.ae_header_id
          AND t.accounted_dr IS NOT NULL
          AND nvl(t.description, 'X') NOT LIKE 'Intracompany%' --added 3-10-2012  --Version 1.1
          AND t.gl_sl_link_id = gir.gl_sl_link_id
          AND h.event_id IN
              (SELECT DISTINCT accounting_event_id
                 FROM ap.ap_invoice_distributions_all d
                WHERE d.accounting_event_id = h.event_id
                  AND d.invoice_id = i.invoice_id)
          AND i.vendor_id = s.vendor_id
          AND t.code_combination_id = gcc.code_combination_id
          AND t.gl_sl_link_id IN
              (SELECT DISTINCT a.gl_sl_link_id
                 FROM xxcus.xxcusie_fm_gl_gtt a
                WHERE a.gl_sl_link_id = t.gl_sl_link_id
                  AND a.gl_sl_link_id IS NOT NULL)
        GROUP BY substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50)
                ,i.invoice_num
                ,gcc.segment1
                ,gcc.segment2
                ,gcc.segment3
                ,gcc.segment4
                ,gcc.segment5
                ,gcc.segment6
                ,gcc.segment7
                ,i.vendor_id);
    --
    l_sec := 'First pass into  xxcus.xxcusie_fm_gl_gtt2,  Non Intracompany DR only transactions (AP/GL journals), rows_inserted '||sql%rowcount; -- ver 1.2  
    fnd_file.put_line(fnd_file.output, l_sec);
    --    
    l_lob := 'FM';
    fnd_file.put_line(fnd_file.output, l_lob);   --ver 1.2
    --Cursor to get the LINE rows
    l_sec := 'First pass load into xxcus.xxcusie_fm_filter_extract_tbl using xxcus.xxcusie_fm_gl_gtt2 records'; --ver 1.2
    --fnd_file.put_line(fnd_file.log, l_sec); --ver 1.2
    fnd_file.put_line(fnd_file.output, l_sec);
    --
    l_count :=0; --ver 1.2
    --
    FOR c_glfilter_fm_rec IN (SELECT a.descr
                                    ,a.report_num
                                    ,a.segment1
                                    ,a.segment2
                                    ,a.segment3
                                    ,a.segment4
                                    ,a.segment5
                                    ,a.segment6
                                    ,a.segment7
                                    ,a.period_name
                                    ,nvl(a.accounted_dr, 0) accounted_dr
                                    ,nvl(a.accounted_cr, 0) accounted_cr
                                    ,a.employee_num
                                FROM xxcus.xxcusie_fm_gl_gtt2 a
                                    ,(SELECT b.segment1
                                            ,b.segment2
                                            ,b.segment3
                                            ,b.segment4
                                            ,SUM(nvl(accounted_dr, 0) -
                                                 nvl(accounted_cr, 0)) amount
                                        FROM xxcus.xxcusie_fm_gl_gtt2 b
                                       GROUP BY b.segment1
                                               ,b.segment2
                                               ,b.segment3
                                               ,b.segment4) b
                               WHERE a.segment1 = b.segment1
                                 AND a.segment2 = b.segment2
                                 AND a.segment3 = b.segment3
                                 AND a.segment4 = b.segment4
                               ORDER BY segment1
                                       ,segment2
                                       ,segment3
                                       ,segment4
                                       ,segment5
                                       ,segment6
                                       ,segment7)
    LOOP
      --
      l_tot_accts := 0;
      --
      l_debit  := nvl(c_glfilter_fm_rec.accounted_dr, 0);
      l_credit := nvl(c_glfilter_fm_rec.accounted_cr, 0);
      l_total  := l_debit - l_credit;
      --
      l_text   := c_glfilter_fm_rec.descr;
    
      IF l_total >= 0 THEN
        l_d_c_id := '40';
      ELSE
        l_d_c_id := '50';
      END IF;
      --
      SELECT DISTINCT description
        INTO l_seg_descr
        FROM apps.fnd_flex_values_vl
       WHERE value_category LIKE 'XXCUS_GL_PRODUCT'
         AND flex_value = c_glfilter_fm_rec.segment1;
       --
       print_log(' l_seg_descr ='||l_seg_descr); --Ver 1.2
       --
      INSERT INTO xxcus.xxcusie_fm_filter_extract_tbl
      VALUES
        (l_d_c_id
        ,(CASE WHEN l_total > 0 THEN abs(l_total) ELSE 0 END)
        ,(CASE WHEN l_total < 0 THEN abs(l_total) ELSE 0 END)
        ,l_text
        ,c_glfilter_fm_rec.report_num
        ,c_glfilter_fm_rec.segment1
        ,l_seg_descr
        ,c_glfilter_fm_rec.segment2
        ,c_glfilter_fm_rec.segment3
        ,c_glfilter_fm_rec.segment4
        ,c_glfilter_fm_rec.segment5
        ,c_glfilter_fm_rec.segment6
        ,c_glfilter_fm_rec.segment7
        ,c_glfilter_fm_rec.period_name
        ,l_runnum
        ,c_glfilter_fm_rec.employee_num);
         --
         l_count :=l_count +1; -- ver 1.2
         --
    END LOOP;
    --
    l_sec :='@ sequence :'||l_sequence||', Total records inserted into xxcus.xxcusie_fm_filter_extract_tbl ='||l_count; --ver 1.2
    --
    fnd_file.put_line(fnd_file.output, l_sec);
    --  
    DELETE FROM xxcus.xxcusie_fm_filter_extract_tbl
     WHERE debit = 0
       AND credit = 0;
    --
    l_sec :='Total records deleted from xxcus.xxcusie_fm_filter_extract_tbl where debit and credit are zero ='||sql%rowcount; --ver 1.2
    fnd_file.put_line(fnd_file.output, l_sec);    
    --  
    COMMIT;
    --
    l_count :=0; -- ver 1.2
    --
    FOR c_glprocess_fm_rec IN (SELECT DISTINCT report_num
                                              ,segment1
                                              ,segment2
                                              ,segment3
                                              ,segment4
                                              ,segment5
                                              ,segment6
                                              ,segment7
                                 FROM xxcus.xxcusie_fm_filter_extract_tbl
                                WHERE segment4 = '211100')
    LOOP
    
      l_whack := 0;
    
      SELECT SUM(t.debit) - SUM(t.credit)
        INTO l_whack
        FROM xxcus.xxcusie_fm_filter_extract_tbl t
       WHERE report_num = c_glprocess_fm_rec.report_num
         AND segment1 = c_glprocess_fm_rec.segment1
         AND segment2 = c_glprocess_fm_rec.segment2
         AND segment3 = c_glprocess_fm_rec.segment3
         AND segment4 = c_glprocess_fm_rec.segment4
         AND segment5 = c_glprocess_fm_rec.segment5
         AND segment6 = c_glprocess_fm_rec.segment6
         AND segment7 = c_glprocess_fm_rec.segment7
       GROUP BY segment1
               ,segment2
               ,segment3
               ,segment4
               ,segment5
               ,segment6
               ,segment7;
    
      IF l_whack = 0 THEN
        --
        DELETE FROM xxcus.xxcusie_fm_filter_extract_tbl t
         WHERE report_num = c_glprocess_fm_rec.report_num
           AND segment1 = c_glprocess_fm_rec.segment1
           AND segment2 = c_glprocess_fm_rec.segment2
           AND segment3 = c_glprocess_fm_rec.segment3
           AND segment4 = c_glprocess_fm_rec.segment4
           AND segment5 = c_glprocess_fm_rec.segment5
           AND segment6 = c_glprocess_fm_rec.segment6
           AND segment7 = c_glprocess_fm_rec.segment7;
           --
           l_sec :='Report_num : '||c_glprocess_fm_rec.report_num||', total records deleted from xxcus.xxcusie_fm_filter_extract_tbl where sum(debit) and sum(credit) are zero for all journals in account (211100) ='||sql%rowcount; --ver 1.2
           fnd_file.put_line(fnd_file.output, l_sec);     --ver 1.2                 
           --
          COMMIT;
           --
      END IF;
      --
    END LOOP;
    --
    l_count :=0; --ver 1.2
    --
    FOR c_interc IN (SELECT SUM(debit) - SUM(credit) total
                           ,segment1
                           ,segment1_decsr
                           ,segment2
                           ,segment3
                           ,segment4
                           ,segment5
                           ,segment6
                           ,segment7
                           ,period_name
                           ,run_num
                       FROM xxcus.xxcusie_fm_filter_extract_tbl
                      WHERE segment2 = 'Z9999'
                      GROUP BY segment1
                              ,segment1_decsr
                              ,segment2
                              ,segment3
                              ,segment4
                              ,segment5
                              ,segment6
                              ,segment7
                              ,period_name
                              ,run_num)
    LOOP
      --    
      INSERT INTO xxcus.xxcusie_et_extract_tbl
      VALUES
        (l_sequence
        ,l_line_num
        ,l_sourcesys
        ,l_date
        ,(CASE WHEN c_interc.total >= 0 THEN '40' ELSE '50' END) --c_interc.d_c_id
        ,(CASE WHEN c_interc.total > 0 THEN abs(c_interc.total) ELSE 0 END) --debit
        ,(CASE WHEN c_interc.total < 0 THEN abs(c_interc.total) ELSE 0 END) --credit
        ,'Intercompany added by Posting' --fm_text
        ,c_interc.segment1
        ,c_interc.segment1_decsr
        ,c_interc.segment2
        ,c_interc.segment3
        ,c_interc.segment4
        ,c_interc.segment5
        ,c_interc.segment6
        ,c_interc.segment7
        ,c_interc.period_name
        ,c_interc.run_num
        ,' '); --added space for ET
       --
      l_line_num := l_line_num + 1; -- ver 1.2
       --
    END LOOP;
    --
       l_sec :='@ sequence :'||l_sequence||', Total records inserted into xxcus.xxcusie_et_extract_tbl (segment2 =Z9999) for Intercompany added by Posting='||l_line_num; --ver 1.2
       fnd_file.put_line(fnd_file.output, l_sec);     --ver 1.2                     
    --
    l_line_num :=0; -- ver 1.2
    --
    FOR c_gl_fm_rec IN (SELECT *
                          FROM xxcus.xxcusie_fm_filter_extract_tbl
                         WHERE segment2 <> 'Z9999')
    LOOP
      --
      INSERT INTO xxcus.xxcusie_et_extract_tbl
      VALUES
        (l_sequence
        ,l_line_num
        ,l_sourcesys
        ,l_date
        ,c_gl_fm_rec.d_c_id
        ,abs(c_gl_fm_rec.debit)
        ,abs(c_gl_fm_rec.credit)
        ,c_gl_fm_rec.fm_text
        ,c_gl_fm_rec.segment1
        ,c_gl_fm_rec.segment1_decsr
        ,c_gl_fm_rec.segment2
        ,c_gl_fm_rec.segment3
        ,c_gl_fm_rec.segment4
        ,c_gl_fm_rec.segment5
        ,c_gl_fm_rec.segment6
        ,c_gl_fm_rec.segment7
        ,c_gl_fm_rec.period_name
        ,c_gl_fm_rec.run_num
        ,nvl(c_gl_fm_rec.employee_num, ' ')); --added space for ET
          --
          l_line_num := l_line_num + 1;
          --   
    END LOOP;
    --
       l_sec :='@ sequence :'||l_sequence||', Total records inserted into xxcus.xxcusie_et_extract_tbl (segment2 NOT EQUALS Z9999) ='||l_line_num; --ver 1.2
       fnd_file.put_line(fnd_file.output, l_sec);     --ver 1.2                     
    --  
    COMMIT;
    --
      l_sec := 'End - '||l_procedure_name; -- ver 1.2
      fnd_file.put_line(fnd_file.output, l_sec); -- ver 1.2
    --
  EXCEPTION
     --
    WHEN program_error THEN
      --
      ROLLBACK;
    
      DELETE FROM xxcus.xxcusie_fm_aud_tbl
       WHERE conc_request_id = l_req_id
         AND sequence = l_sequence;
    --
       l_sec :='Program-error, deleted from xxcus.xxcusie_fm_aud_tbl where conc_request_id ='||l_req_id||' and sequence ='||l_sequence||', total records ='||sql%rowcount; --ver 1.2
       fnd_file.put_line(fnd_file.output, l_sec);     --ver 1.2                     
    --                
      COMMIT;
    --  
      DELETE FROM xxcus.xxcusie_et_extract_tbl
       WHERE period_name = p_fperiod
         AND run_num = l_runnum
         AND sequence = l_sequence;
    --
       l_sec :='Program-error, deleted from xxcus.xxcusie_et_extract_tbl where period ='||p_fperiod||', run_num ='||l_runnum||', sequence ='||l_sequence||',total records ='||sql%rowcount; --ver 1.2
       fnd_file.put_line(fnd_file.output, l_sec);     --ver 1.2                     
    --          
      COMMIT;
    --
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      retcode := l_err_code;
      errbuf  := l_err_msg;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for XXCUS_FMGLOUTBOUND',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error during procedure load_fm_gl_int',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
    WHEN OTHERS THEN
       --
      DELETE FROM xxcus.xxcusie_fm_aud_tbl
       WHERE conc_request_id = l_req_id
         AND sequence = l_sequence;
    --
       l_sec :='When-others, deleted from xxcus.xxcusie_fm_aud_tbl where conc_request_id ='||l_req_id||' and sequence ='||l_sequence||',total records ='||sql%rowcount; --ver 1.2
       fnd_file.put_line(fnd_file.output, l_sec);     --ver 1.2                     
    --          
      COMMIT;
    --
      DELETE FROM xxcus.xxcusie_et_extract_tbl
       WHERE period_name = p_fperiod
         AND run_num = l_runnum
         AND sequence = l_sequence;
    --
       l_sec :='When-others, deleted from xxcus.xxcusie_et_extract_tbl where period ='||p_fperiod||', run_num ='||l_runnum||', sequence ='||l_sequence||',total records ='||sql%rowcount; --ver 1.2
       fnd_file.put_line(fnd_file.output, l_sec);     --ver 1.2                     
    --           
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      retcode := l_err_code;
      errbuf  := l_err_msg;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for XXCUS_FMGLOUTBOUND',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error during procedure load_fm_gl_int',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
  END load_fm_gl_int;

  /*******************************************************************************
  * Procedure:   load_cb_detail_gl_int
  * Description: Creates output file in output directory indicated named
  *              'CB_GL_IEXP_<period>_<date in 'YYYYMMDDHHMISS' format>.txt'
  *              Concurrent 'Request HDS GL CB Outbound' under the responsibility
  *              XXCUS_CON
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/27/2011    Kathy Poling    Initial creation of the procedure copied from R11
  1.1     04/14/2012    Kathy Poling    Added NVL to description line
                                        Service Ticket 136532
  1.2     04/30/2012    Kathy Poling    Service Ticket 140093 RFC 33515 - Changes for
                                        description to get invoice number and supplier
                                        for manual invoices
  ********************************************************************************/

  PROCEDURE load_cb_gl_int(errbuf    OUT VARCHAR2
                          ,retcode   OUT NUMBER
                          ,p_fperiod IN VARCHAR2) IS
  
    --Intialize Variables
  
    l_err_msg         VARCHAR2(2000);
    l_err_code        NUMBER;
    l_processed_count NUMBER := 0;
    l_sec             VARCHAR2(150);
    l_tot_accts       NUMBER := 0;
    l_debit           NUMBER := 0;
    l_credit          NUMBER := 0;
    l_seg_descr       VARCHAR2(240);
    l_lob             VARCHAR2(3);
    l_date            DATE;
    l_sequence        NUMBER;
    l_line_num        NUMBER := 0;
    l_linecnt         NUMBER := 1;
  
    l_total     NUMBER := 0;
    l_d_c_id    VARCHAR2(10);
    l_runnum    NUMBER := 0;
    l_text      VARCHAR2(50) := 'NA';
    l_whack     NUMBER := 0;
    l_sourcesys VARCHAR2(75) := 'ORCL-CB';
  
    l_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_coa      CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := '50328'; --HDS Chart of Accounts
    l_ledger   CONSTANT gl_period_statuses.ledger_id%TYPE := 2061; --HDS Supply USD
    l_period         gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_shortperiod    gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_procedure_name VARCHAR2(75) := 'XXCUS_IEXPENSE_GL_PKG.LOAD_CB_DETAIL_GL_INT';
  
    l_err_callfrom VARCHAR2(75) DEFAULT 'XXCUS_IEXPENSE_GL_PKG.LOAD_CB_DETAIL_GL_INT';
  
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id      NUMBER := fnd_global.conc_request_id;
  
    --Start Main Program
  BEGIN
    dbms_output.enable(1000000);
    l_sec := 'In create file name; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    SELECT period_name
          ,substr(period_name, 1, 3)
      INTO l_period
          ,l_shortperiod
      FROM gl.gl_periods
     WHERE period_set_name = l_calendar
       AND start_date <= trunc(SYSDATE - 3)
       AND end_date >= trunc(SYSDATE - 3)
       AND period_num <> 13;
  
    SELECT SYSDATE
      INTO l_date
      FROM dual;
  
    SELECT xxcus.xxcusie_et_extract_s.nextval
      INTO l_sequence
      FROM dual;
  
    --Truncate XXCUS.XXCUSIE_CB_FILTER_EXTRACT_TBL
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSIE_CB_FILTER_EXTRACT_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSIE_CB_FILTER_EXTRACT_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    --Determine Run Number
    BEGIN
      SELECT (MAX(nvl(run_num, 0)) + 1)
        INTO l_runnum
        FROM xxcus.xxcusie_cb_aud_tbl
       WHERE period_name = p_fperiod;
    EXCEPTION
      WHEN no_data_found THEN
        l_runnum := 1;
      WHEN OTHERS THEN
        l_runnum := 1;
    END;
  
    IF l_runnum IS NULL THEN
      l_runnum := 1;
    END IF;
  
    --Insert Global Line Information GTT
    l_sec := 'Loading LINE global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    INSERT INTO xxcus.xxcusie_cb_detail_gl_gtt
      (SELECT a.accounted_cr
             ,a.accounted_dr
             ,a.je_header_id
             ,a.je_line_num
             ,c.segment1
             ,c.segment2
             ,c.segment3
             ,c.segment4
             ,c.segment5
             ,c.segment6
             ,c.segment7
             ,b.je_category
             ,a.gl_sl_link_id
             ,to_char(a.effective_date, 'YYYY-MM-DD') eff_date
             ,to_char(b.posted_date, 'YYYY-MM-DD') post_date
              --    ,substr(a.description, 1, 50) descr        *** Add handling empl num for accr lines
             ,substr(nvl((SELECT pap.employee_number
                           FROM hr.per_all_people_f      pap
                               ,hr.per_all_assignments_f paa
                               ,gl.gl_code_combinations  gcc
                          WHERE pap.person_id = paa.person_id
                            AND paa.default_code_comb_id =
                                gcc.code_combination_id
                            AND pap.full_name =
                                TRIM(substr(a.description, 1,
                                            instr(a.description, ' -')))
                            AND pap.effective_end_date = '31-DEC-4712'
                            AND paa.effective_end_date = '31-DEC-4712'
                            AND pap.person_type_id = 6
                            AND gcc.segment2 IN
                                (SELECT segment2
                                   FROM gl.gl_code_combinations
                                  WHERE segment1 IN
                                        (SELECT h.description
                                           FROM apps.fnd_lookup_values h
                                          WHERE h.lookup_type =
                                                'HDS_CBGLEXTR'
                                            AND enabled_flag = 'Y'
                                            AND nvl(end_date_active, SYSDATE) >=
                                                SYSDATE
                                            AND h.lookup_code LIKE 'SEG1_%'))),
                         (TRIM(substr(a.description, 1,
                                       instr(a.description, ' -')))))
                     
                     || regexp_replace((substr(a.description,
                                               instr(a.description, ' -') + 1)),
                                       '-', ''), 1, 50) descr
             ,a.period_name
         FROM gl.gl_je_lines          a
             ,gl.gl_je_headers        b
             ,gl.gl_code_combinations c
        WHERE a.code_combination_id = c.code_combination_id
          AND a.je_header_id = b.je_header_id
          AND (b.je_header_id, a.je_line_num) NOT IN
              (SELECT i.je_header_id
                     ,i.je_line_num
                 FROM xxcus.xxcusie_cb_aud_tbl i
                WHERE i.je_header_id = b.je_header_id
                  AND i.je_line_num = a.je_line_num
                  AND i.period_name = p_fperiod)
          AND b.currency_code <> 'STAT'
          AND nvl(a.accounted_cr, 0) <> nvl(a.accounted_dr, 0)
          AND a.ledger_id = l_ledger
          AND a.period_name = p_fperiod
          AND c.segment1 IN
              (SELECT h.description
                 FROM apps.fnd_lookup_values h
                WHERE h.lookup_type = 'HDS_CBGLEXTR'
                  AND enabled_flag = 'Y'
                  AND nvl(end_date_active, SYSDATE) >= SYSDATE
                  AND h.lookup_code LIKE 'SEG1_%')
          AND a.status = 'P'
          AND c.chart_of_accounts_id = l_coa);
  
    --dbms_output.put_line('Start Enter the Intercompany CR transactions');
  
    l_sec := 'Load audit table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Cursor to load audit table
    FOR c_gl_cb_audit_rec IN (SELECT DISTINCT je_header_id
                                             ,je_line_num
                                             ,period_name
                                FROM xxcus.xxcusie_cb_detail_gl_gtt)
    LOOP
    
      INSERT INTO xxcus.xxcusie_cb_aud_tbl
      VALUES
        (c_gl_cb_audit_rec.je_header_id
        ,c_gl_cb_audit_rec.je_line_num
        ,c_gl_cb_audit_rec.period_name
        ,nvl(fnd_global.conc_request_id, 0)
        ,SYSDATE
        ,nvl(fnd_global.user_name, 'NA')
        ,nvl(fnd_global.resp_name, 'NA')
        ,'File created'
        ,SYSDATE
        ,l_runnum
        ,l_sequence);
    
    END LOOP;
  
    --INSERT DR SUMN and CR SUM HERE
    --Insert Global Line Information GTT
    l_sec := 'Loading CR 1 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the Intercompany CR transactions
    INSERT INTO xxcus.xxcusie_cb_detail_gl_gtt2
      (SELECT a.descr
             ,0
             ,a.segment1
             ,a.segment2
             ,a.segment3
             ,a.segment4
             ,a.segment5
             ,a.segment6
             ,a.segment7
             ,a.period_name
             ,SUM(nvl(a.accounted_cr, 0))
             ,NULL
             ,NULL
         FROM xxcus.xxcusie_cb_detail_gl_gtt a
        WHERE a.accounted_cr IS NOT NULL
          AND a.gl_sl_link_id IS NULL
           OR a.descr LIKE 'Intracompany%'
        GROUP BY a.descr
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7
                ,a.period_name);
  
    l_sec := 'Loading DR 2 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the Intercompany DB transactions
    INSERT INTO xxcus.xxcusie_cb_detail_gl_gtt2
      (SELECT a.descr
             ,0
             ,a.segment1
             ,a.segment2
             ,a.segment3
             ,a.segment4
             ,a.segment5
             ,a.segment6
             ,a.segment7
             ,a.period_name
             ,NULL
             ,SUM(nvl(a.accounted_dr, 0))
             ,NULL
         FROM xxcus.xxcusie_cb_detail_gl_gtt a
        WHERE a.accounted_dr IS NOT NULL
          AND a.gl_sl_link_id IS NULL
           OR a.descr LIKE 'Intracompany%'
        GROUP BY a.descr
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7
                ,a.period_name);
  
    l_sec := 'Loading DR 3 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the NON-Intercompany CR transactions
    INSERT INTO xxcus.xxcusie_cb_detail_gl_gtt2
      (SELECT --substr(t.description, 1, 50)
        substr(nvl(t.description, i.invoice_num || ' ' || s.vendor_name), 1,
               50)
      ,i.invoice_num
      ,gcc.segment1
      ,gcc.segment2
      ,gcc.segment3
      ,gcc.segment4
      ,gcc.segment5
      ,gcc.segment6
      ,gcc.segment7
      ,p_fperiod
      ,SUM(nvl(t.accounted_cr, 0))
      ,NULL
      ,(SELECT (SELECT p.employee_number
                   FROM hr.per_all_people_f p
                  WHERE p.person_id = v.employee_id
                    AND object_version_number IN
                        (SELECT MAX(object_version_number)
                           FROM hr.per_all_people_f b
                          WHERE b.person_id = p.person_id)) employee_num
           FROM apps.po_vendors v
          WHERE v.vendor_id = i.vendor_id) employee_num
         FROM xla.xla_ae_lines        t
             ,xla.xla_ae_headers      h
             ,gl.gl_import_references gir
             ,gl.gl_code_combinations gcc
             ,ap.ap_invoices_all      i
             ,ap.ap_suppliers         s
        WHERE t.ae_header_id = h.ae_header_id
          AND t.accounted_cr IS NOT NULL
          AND nvl(t.description, 'X') NOT LIKE 'Intracompany%' --added 3-10-2012  --Version 1.1
          AND t.gl_sl_link_id = gir.gl_sl_link_id
          AND h.event_id IN
              (SELECT DISTINCT accounting_event_id
                 FROM ap.ap_invoice_distributions_all d
                WHERE d.accounting_event_id = h.event_id
                  AND d.invoice_id = i.invoice_id)
          AND i.vendor_id = s.vendor_id
          AND t.code_combination_id = gcc.code_combination_id
          AND t.gl_sl_link_id IN
              (SELECT DISTINCT a.gl_sl_link_id
                 FROM xxcus.xxcusie_cb_detail_gl_gtt a
                WHERE a.gl_sl_link_id = t.gl_sl_link_id
                  AND a.gl_sl_link_id IS NOT NULL)
        GROUP BY --substr(t.description, 1, 50)  --Version 1.2
                 substr(nvl(t.description,
                            i.invoice_num || ' ' || s.vendor_name), 1, 50) --Version 1.2
                ,i.invoice_num
                ,gcc.segment1
                ,gcc.segment2
                ,gcc.segment3
                ,gcc.segment4
                ,gcc.segment5
                ,gcc.segment6
                ,gcc.segment7
                ,i.vendor_id);
  
    l_sec := 'Loading DR 4 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the NON-Intercompany DB transactions
    INSERT INTO xxcus.xxcusie_cb_detail_gl_gtt2
      (SELECT --substr(t.description, 1, 50)  --Version 1.2
        substr(nvl(t.description, i.invoice_num || ' ' || s.vendor_name), 1,
               50) --Version 1.2
      ,i.invoice_num
      ,gcc.segment1
      ,gcc.segment2
      ,gcc.segment3
      ,gcc.segment4
      ,gcc.segment5
      ,gcc.segment6
      ,gcc.segment7
      ,p_fperiod
      ,NULL
      ,SUM(nvl(t.accounted_dr, 0))
      ,(SELECT (SELECT p.employee_number
                   FROM hr.per_all_people_f p
                  WHERE p.person_id = v.employee_id
                    AND object_version_number IN
                        (SELECT MAX(object_version_number)
                           FROM hr.per_all_people_f b
                          WHERE b.person_id = p.person_id)) employee_num
           FROM apps.po_vendors v
          WHERE v.vendor_id = i.vendor_id) employee_num
         FROM xla.xla_ae_lines        t
             ,xla.xla_ae_headers      h
             ,gl.gl_import_references gir
             ,gl.gl_code_combinations gcc
             ,ap.ap_invoices_all      i
             ,ap.ap_suppliers         s
        WHERE t.ae_header_id = h.ae_header_id
          AND t.accounted_dr IS NOT NULL
          AND nvl(t.description, 'X') NOT LIKE 'Intracompany%' --added 3-10-2012  --Version 1.1
          AND t.gl_sl_link_id = gir.gl_sl_link_id
          AND h.event_id IN
              (SELECT DISTINCT accounting_event_id
                 FROM ap.ap_invoice_distributions_all d
                WHERE d.accounting_event_id = h.event_id
                  AND d.invoice_id = i.invoice_id)
          AND i.vendor_id = s.vendor_id
          AND t.code_combination_id = gcc.code_combination_id
          AND t.gl_sl_link_id IN
              (SELECT DISTINCT a.gl_sl_link_id
                 FROM xxcus.xxcusie_cb_detail_gl_gtt a
                WHERE a.gl_sl_link_id = t.gl_sl_link_id
                  AND a.gl_sl_link_id IS NOT NULL)
        GROUP BY --substr(t.description, 1, 50)     --Version 1.2
                 substr(nvl(t.description,
                            i.invoice_num || ' ' || s.vendor_name), 1, 50) --Version 1.2
                ,i.invoice_num
                ,gcc.segment1
                ,gcc.segment2
                ,gcc.segment3
                ,gcc.segment4
                ,gcc.segment5
                ,gcc.segment6
                ,gcc.segment7
                ,i.vendor_id);
  
    --Cursor to get the LINE rows
    l_sec := 'Writing LINE File; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    FOR c_glfilter_cb_rec IN (SELECT a.descr
                                    ,a.report_num
                                    ,a.segment1
                                    ,a.segment2
                                    ,a.segment3
                                    ,a.segment4
                                    ,a.segment5
                                    ,a.segment6
                                    ,a.segment7
                                    ,a.period_name
                                    ,a.accounted_dr
                                    ,a.accounted_cr
                                    ,a.employee_num
                                FROM xxcus.xxcusie_cb_detail_gl_gtt2 a
                                    ,(SELECT b.segment1
                                            ,b.segment2
                                            ,b.segment3
                                            ,b.segment4
                                            ,SUM(nvl(accounted_dr, 0) -
                                                 nvl(accounted_cr, 0)) amount
                                        FROM xxcus.xxcusie_cb_detail_gl_gtt2 b
                                       GROUP BY b.segment1
                                               ,b.segment2
                                               ,b.segment3
                                               ,b.segment4) b
                               WHERE a.segment1 = b.segment1
                                 AND a.segment2 = b.segment2
                                 AND a.segment3 = b.segment3
                                 AND a.segment4 = b.segment4
                               ORDER BY segment1
                                       ,segment2
                                       ,segment3
                                       ,segment4
                                       ,segment5
                                       ,segment6
                                       ,segment7)
    LOOP
      l_tot_accts := 0;
      --        dbms_output.put_line(c_glfilter_cb_rec.descr);
      l_debit  := nvl(c_glfilter_cb_rec.accounted_dr, 0);
      l_credit := nvl(c_glfilter_cb_rec.accounted_cr, 0);
      l_total  := l_debit - l_credit;
      l_text   := TRIM(substr((c_glfilter_cb_rec.employee_num || ' ' ||
                              c_glfilter_cb_rec.descr), 1, 50));
    
      IF l_total >= 0 THEN
        l_d_c_id := '40';
      ELSE
        l_d_c_id := '50';
      END IF;
    
      --dbms_output.put_line('leg_seg_one 2  ' || l_location);
      SELECT DISTINCT description
        INTO l_seg_descr
        FROM apps.fnd_flex_values_vl
       WHERE value_category LIKE 'XXCUS_GL_PRODUCT'
         AND flex_value = c_glfilter_cb_rec.segment1;
    
      INSERT INTO xxcus.xxcusie_cb_filter_extract_tbl
      VALUES
        (l_d_c_id
        ,(CASE WHEN l_total > 0 THEN abs(l_total) ELSE 0 END)
        ,(CASE WHEN l_total < 0 THEN abs(l_total) ELSE 0 END)
        ,l_text
        ,c_glfilter_cb_rec.report_num
        ,c_glfilter_cb_rec.segment1
        ,l_seg_descr
        ,c_glfilter_cb_rec.segment2
        ,c_glfilter_cb_rec.segment3
        ,c_glfilter_cb_rec.segment4
        ,c_glfilter_cb_rec.segment5
        ,c_glfilter_cb_rec.segment6
        ,c_glfilter_cb_rec.segment7
        ,c_glfilter_cb_rec.period_name
        ,l_runnum
        ,c_glfilter_cb_rec.employee_num);
    
    END LOOP;
  
    DELETE FROM xxcus.xxcusie_cb_filter_extract_tbl
     WHERE debit = 0
       AND credit = 0;
  
    COMMIT;
  
    FOR c_glprocess_cb_rec IN (SELECT DISTINCT report_num
                                              ,segment1
                                              ,segment2
                                              ,segment3
                                              ,segment4
                                              ,segment5
                                              ,segment6
                                              ,segment7
                                 FROM xxcus.xxcusie_cb_filter_extract_tbl
                                WHERE segment4 = '211100')
    LOOP
    
      l_whack := 0;
    
      SELECT SUM(t.debit) - SUM(t.credit)
        INTO l_whack
        FROM xxcus.xxcusie_cb_filter_extract_tbl t
       WHERE report_num = c_glprocess_cb_rec.report_num
         AND segment1 = c_glprocess_cb_rec.segment1
         AND segment2 = c_glprocess_cb_rec.segment2
         AND segment3 = c_glprocess_cb_rec.segment3
         AND segment4 = c_glprocess_cb_rec.segment4
         AND segment5 = c_glprocess_cb_rec.segment5
         AND segment6 = c_glprocess_cb_rec.segment6
         AND segment7 = c_glprocess_cb_rec.segment7
       GROUP BY segment1
               ,segment2
               ,segment3
               ,segment4
               ,segment5
               ,segment6
               ,segment7;
    
      IF l_whack = 0 THEN
      
        DELETE FROM xxcus.xxcusie_cb_filter_extract_tbl t
         WHERE report_num = c_glprocess_cb_rec.report_num
           AND segment1 = c_glprocess_cb_rec.segment1
           AND segment2 = c_glprocess_cb_rec.segment2
           AND segment3 = c_glprocess_cb_rec.segment3
           AND segment4 = c_glprocess_cb_rec.segment4
           AND segment5 = c_glprocess_cb_rec.segment5
           AND segment6 = c_glprocess_cb_rec.segment6
           AND segment7 = c_glprocess_cb_rec.segment7;
      
        COMMIT;
      
      END IF;
    
    END LOOP;
  
    FOR c_interc IN (SELECT SUM(debit) - SUM(credit) total
                           ,segment1
                           ,segment1_decsr
                           ,segment2
                           ,segment3
                           ,segment4
                           ,segment5
                           ,segment6
                           ,segment7
                           ,period_name
                           ,run_num
                       FROM xxcus.xxcusie_cb_filter_extract_tbl
                      WHERE segment2 = 'Z9999'
                      GROUP BY segment1
                              ,segment1_decsr
                              ,segment2
                              ,segment3
                              ,segment4
                              ,segment5
                              ,segment6
                              ,segment7
                              ,period_name
                              ,run_num)
    LOOP
    
      l_line_num := l_line_num + 1;
    
      INSERT INTO xxcus.xxcusie_et_extract_tbl
      VALUES
        (l_sequence
        ,l_line_num
        ,l_sourcesys
        ,l_date
        ,(CASE WHEN c_interc.total >= 0 THEN '40' ELSE '50' END) --c_interc.d_c_id
        ,(CASE WHEN c_interc.total > 0 THEN abs(c_interc.total) ELSE 0 END) --debit
        ,(CASE WHEN c_interc.total < 0 THEN abs(c_interc.total) ELSE 0 END) --credit
        ,'Intercompany added by Posting' --cb_text
        ,c_interc.segment1
        ,c_interc.segment1_decsr
        ,c_interc.segment2
        ,c_interc.segment3
        ,c_interc.segment4
        ,c_interc.segment5
        ,c_interc.segment6
        ,c_interc.segment7
        ,c_interc.period_name
        ,c_interc.run_num
        ,' '); --added space for ET
    
    END LOOP;
  
    FOR c_gl_cb_rec IN (SELECT *
                          FROM xxcus.xxcusie_cb_filter_extract_tbl
                         WHERE segment2 <> 'Z9999')
    LOOP
    
      l_line_num := l_line_num + 1;
    
      INSERT INTO xxcus.xxcusie_et_extract_tbl
      VALUES
        (l_sequence
        ,l_line_num
        ,l_sourcesys
        ,l_date
        ,c_gl_cb_rec.d_c_id
        ,abs(c_gl_cb_rec.debit)
        ,abs(c_gl_cb_rec.credit)
        ,c_gl_cb_rec.cb_text
        ,c_gl_cb_rec.segment1
        ,c_gl_cb_rec.segment1_decsr
        ,c_gl_cb_rec.segment2
        ,c_gl_cb_rec.segment3
        ,c_gl_cb_rec.segment4
        ,c_gl_cb_rec.segment5
        ,c_gl_cb_rec.segment6
        ,c_gl_cb_rec.segment7
        ,c_gl_cb_rec.period_name
        ,c_gl_cb_rec.run_num
        ,nvl(c_gl_cb_rec.employee_num, ' ')); --added space for ET
    
    END LOOP;
  
    COMMIT;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      DELETE FROM xxcus.xxcusie_cb_aud_tbl
       WHERE conc_request_id = l_req_id
         AND sequence = l_sequence;
      COMMIT;
    
      DELETE FROM xxcus.xxcusie_et_extract_tbl
       WHERE period_name = p_fperiod
         AND run_num = l_runnum
         AND sequence = l_sequence;
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for XXCUSIE_GL_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error during procedure load_cb_detail_gl_int',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
    WHEN OTHERS THEN
      DELETE FROM xxcus.xxcusie_cb_aud_tbl
       WHERE conc_request_id = l_req_id
         AND sequence = l_sequence;
      COMMIT;
    
      DELETE FROM xxcus.xxcusie_et_extract_tbl
       WHERE period_name = p_fperiod
         AND run_num = l_runnum
         AND sequence = l_sequence;
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for XXCUSIE_GL_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error during procedure load_cb_gl_int',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
  END load_cb_gl_int;

  /*******************************************************************************
  * Procedure:   load_ww_gl_int
  * Description: Creates output file in output directory indicated named
  *              'WW_GL_IEXP_<period>_<date in 'YYYYMMDDHHMISS' format>.txt'
  *              Concurrent 'Request HDS GL WW Outbound' under the responsibility
  *              XXCUS_CON
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/27/2011    Kathy Poling    Initial creation of the procedure copied
                                        from R11
  1.1     04/14/2012    Kathy Poling    Added NVL to description line
                                        Service Ticket 136532
  ********************************************************************************/

  PROCEDURE load_ww_gl_int(errbuf    OUT VARCHAR2
                          ,retcode   OUT NUMBER
                          ,p_fperiod IN VARCHAR2) IS
  
    --Intialize Variables
    l_err_msg         VARCHAR2(2000);
    l_err_code        NUMBER;
    l_processed_count NUMBER := 0;
    l_sec             VARCHAR2(150);
    l_tot_accts       NUMBER := 0;
    l_debit           NUMBER := 0;
    l_credit          NUMBER := 0;
    l_location        VARCHAR2(100);
    l_cost_center     VARCHAR2(100);
    l_account         VARCHAR2(100);
    l_subacct         VARCHAR2(100);
    l_seg_descr       VARCHAR2(240);
    l_lob             VARCHAR2(3) := 'WW';
    l_date            DATE;
    l_sequence        NUMBER;
    l_line_num        NUMBER := 0;
    l_total           NUMBER := 0;
    l_company         VARCHAR2(3) := '001';
    l_runnum          NUMBER := 0;
    l_jrefnum         NUMBER := 3980;
    l_text            VARCHAR2(50) := 'NA';
    l_whack           NUMBER := 0;
    l_d_c_id          VARCHAR2(10);
    l_sourcesys       VARCHAR2(75) := 'ORCL-WW';
  
    l_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_coa      CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := '50328'; --HDS Chart of Accounts
    l_ledger   CONSTANT gl_period_statuses.ledger_id%TYPE := 2061; --HDS Supply USD
    --l_period         gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_period         VARCHAR2(10) := p_fperiod;
    l_shortperiod    gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_shortyear      gl_balances.period_name%TYPE;
    l_procedure_name VARCHAR2(75) := 'XXCUSIE_GL_PKG.LOAD_WW_GL_INT';
  
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUSIE_GL_PKG.LOAD_WW_GL_INT';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id        NUMBER := fnd_global.conc_request_id;
  
    --Start Main Program
  BEGIN
    l_sec := 'In create file name; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Get fiscal month
    SELECT to_char(to_date(l_period, 'MON-YY'), 'mm')
      INTO l_shortperiod
      FROM dual;
  
    --Get fiscal calendar year
    SELECT substr(per.end_date, -2)
      INTO l_shortyear
      FROM gl.gl_periods per
     WHERE per.period_set_name = l_calendar
       AND per.period_name = l_period;
  
    SELECT SYSDATE
      INTO l_date
      FROM dual;
  
    SELECT xxcus.xxcusie_et_extract_s.nextval
      INTO l_sequence
      FROM dual;
  
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSIE_WW_FILTER_EXTRACT_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSIE_WW_FILTER_EXTRACT_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    l_sec := 'IN DETERMINE RUN NUM; '; -- LV
    --Determine Run Number
    BEGIN
      SELECT (MAX(nvl(run_num, 0)) + 1)
        INTO l_runnum
        FROM xxcus.xxcusie_ww_aud_tbl
       WHERE period_name = p_fperiod;
    EXCEPTION
      WHEN no_data_found THEN
        l_runnum := 1;
      WHEN OTHERS THEN
        l_runnum := 1;
    END;
  
    IF l_runnum IS NULL THEN
      l_runnum := 1;
    END IF;
  
    l_sec     := 'IN DETERMINE JREFNUM; ';
    l_jrefnum := l_jrefnum + l_runnum;
  
    --Insert Global Line Information GTT
    l_sec := 'Loading LINE global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    INSERT INTO xxcus.xxcusie_ww_gl_gtt
      (SELECT a.accounted_cr
             ,a.accounted_dr
             ,a.je_header_id
             ,a.je_line_num
             ,c.segment1
             ,c.segment2
             ,c.segment3
             ,c.segment4
             ,c.segment5
             ,c.segment6
             ,c.segment7
             ,b.je_category
             ,a.gl_sl_link_id
             ,to_char(a.effective_date, 'YYYY-MM-DD') eff_date
             ,to_char(b.posted_date, 'YYYY-MM-DD') post_date
             ,substr(a.description, 1, 50) descr
             ,a.period_name
         FROM gl.gl_je_lines          a
             ,gl.gl_je_headers        b
             ,gl.gl_code_combinations c
        WHERE a.code_combination_id = c.code_combination_id
          AND a.je_header_id = b.je_header_id
          AND (b.je_header_id, a.je_line_num) NOT IN
              (SELECT i.je_header_id
                     ,i.je_line_num
                 FROM xxcus.xxcusie_ww_aud_tbl i
                WHERE i.je_header_id = b.je_header_id
                  AND i.je_line_num = a.je_line_num
                  AND i.period_name = p_fperiod)
          AND b.currency_code <> 'STAT'
          AND a.ledger_id = l_ledger
          AND a.period_name = p_fperiod
          AND c.segment1 IN
              (SELECT h.description
                 FROM apps.fnd_lookup_values h
                WHERE h.lookup_type = 'HDS_WWGLEXTR'
                  AND enabled_flag = 'Y'
                  AND nvl(end_date_active, SYSDATE) >= SYSDATE
                  AND h.lookup_code LIKE 'SEG1_%')
          AND a.status = 'P'
          AND c.chart_of_accounts_id = l_coa);
  
    l_sec := 'Load audit table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Cursor to load audit table
    FOR c_gl_ww_audit_rec IN (SELECT DISTINCT je_header_id
                                             ,je_line_num
                                             ,period_name
                                FROM xxcus.xxcusie_ww_gl_gtt)
    LOOP
    
      INSERT INTO xxcus.xxcusie_ww_aud_tbl
      VALUES
        (c_gl_ww_audit_rec.je_header_id
        ,c_gl_ww_audit_rec.je_line_num
        ,c_gl_ww_audit_rec.period_name
        ,nvl(fnd_global.conc_request_id, 0)
        ,SYSDATE
        ,nvl(fnd_global.user_name, 'NA')
        ,nvl(fnd_global.resp_name, 'NA')
        ,'File created'
        ,SYSDATE
        ,l_runnum
        ,l_sequence);
    
    END LOOP;
  
    --INSERT DR SUMN and CR SUM HERE
    --Insert Global Line Information GTT
    l_sec := 'Loading CR 1 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the Intercompany CR transactions
    INSERT INTO xxcus.xxcusie_ww_gl_gtt2
      (SELECT a.descr
             ,0
             ,a.segment1
             ,a.segment2
             ,a.segment3
             ,a.segment4
             ,a.segment5
             ,a.segment6
             ,a.segment7
             ,a.period_name
             ,SUM(nvl(a.accounted_cr, 0))
             ,NULL
             ,NULL
         FROM xxcus.xxcusie_ww_gl_gtt a
        WHERE a.accounted_cr IS NOT NULL
          AND a.gl_sl_link_id IS NULL
           OR a.descr LIKE 'Intracompany%'
        GROUP BY a.descr
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7
                ,a.period_name);
  
    l_sec := 'Loading DR 2 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the Intercompany DB transactions
    INSERT INTO xxcus.xxcusie_ww_gl_gtt2
      (SELECT a.descr
             ,0
             ,a.segment1
             ,a.segment2
             ,a.segment3
             ,a.segment4
             ,a.segment5
             ,a.segment6
             ,a.segment7
             ,a.period_name
             ,NULL
             ,SUM(nvl(a.accounted_dr, 0))
             ,NULL
         FROM xxcus.xxcusie_ww_gl_gtt a
        WHERE a.accounted_dr IS NOT NULL
          AND a.gl_sl_link_id IS NULL
           OR a.descr LIKE 'Intracompany%'
        GROUP BY a.descr
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7
                ,a.period_name);
  
    l_sec := 'Loading DR 3 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the NON-Intercompany CR transactions
    INSERT INTO xxcus.xxcusie_ww_gl_gtt2
      (SELECT substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50) line_item_text
             ,i.invoice_num
             ,gcc.segment1
             ,gcc.segment2
             ,gcc.segment3
             ,gcc.segment4
             ,gcc.segment5
             ,gcc.segment6
             ,gcc.segment7
             ,p_fperiod
             ,SUM(nvl(t.accounted_cr, 0))
             ,NULL
             ,(SELECT (SELECT p.employee_number
                         FROM hr.per_all_people_f p
                        WHERE p.person_id = v.employee_id
                          AND object_version_number IN
                              (SELECT MAX(object_version_number)
                                 FROM hr.per_all_people_f b
                                WHERE b.person_id = p.person_id)) employee_num
                 FROM apps.po_vendors v
                WHERE v.vendor_id = i.vendor_id) employee_num
         FROM xla.xla_ae_lines        t
             ,xla.xla_ae_headers      h
             ,gl.gl_import_references gir
             ,gl.gl_code_combinations gcc
             ,ap.ap_invoices_all      i
             ,ap.ap_suppliers         s
        WHERE t.ae_header_id = h.ae_header_id
          AND t.accounted_cr IS NOT NULL
          AND nvl(t.description, 'X') NOT LIKE 'Intracompany%' --added 3-10-2012  --Version 1.1
          AND t.gl_sl_link_id = gir.gl_sl_link_id
          AND h.event_id IN
              (SELECT DISTINCT accounting_event_id
                 FROM ap.ap_invoice_distributions_all d
                WHERE d.accounting_event_id = h.event_id
                  AND d.invoice_id = i.invoice_id)
          AND i.vendor_id = s.vendor_id
          AND t.code_combination_id = gcc.code_combination_id
          AND t.gl_sl_link_id IN
              (SELECT DISTINCT a.gl_sl_link_id
                 FROM xxcus.xxcusie_ww_gl_gtt a
                WHERE a.gl_sl_link_id = t.gl_sl_link_id
                  AND a.gl_sl_link_id IS NOT NULL)
        GROUP BY substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50)
                ,i.invoice_num
                ,gcc.segment1
                ,gcc.segment2
                ,gcc.segment3
                ,gcc.segment4
                ,gcc.segment5
                ,gcc.segment6
                ,gcc.segment7
                ,i.vendor_id);
  
    l_sec := 'Loading DR 4 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the NON-Intercompany DB transactions
    INSERT INTO xxcus.xxcusie_ww_gl_gtt2
      (SELECT substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50) line_item_text
             ,i.invoice_num
             ,gcc.segment1
             ,gcc.segment2
             ,gcc.segment3
             ,gcc.segment4
             ,gcc.segment5
             ,gcc.segment6
             ,gcc.segment7
             ,p_fperiod
             ,NULL
             ,SUM(nvl(t.accounted_dr, 0))
             ,(SELECT (SELECT p.employee_number
                         FROM hr.per_all_people_f p
                        WHERE p.person_id = v.employee_id
                          AND object_version_number IN
                              (SELECT MAX(object_version_number)
                                 FROM hr.per_all_people_f b
                                WHERE b.person_id = p.person_id)) employee_num
                 FROM apps.po_vendors v
                WHERE v.vendor_id = i.vendor_id) employee_num
         FROM xla.xla_ae_lines        t
             ,xla.xla_ae_headers      h
             ,gl.gl_import_references gir
             ,gl.gl_code_combinations gcc
             ,ap.ap_invoices_all      i
             ,ap.ap_suppliers         s
        WHERE t.ae_header_id = h.ae_header_id
          AND t.accounted_dr IS NOT NULL
          AND nvl(t.description, 'X') NOT LIKE 'Intracompany%' --added 3-10-2012  --Version 1.1
          AND t.gl_sl_link_id = gir.gl_sl_link_id
          AND h.event_id IN
              (SELECT DISTINCT accounting_event_id
                 FROM ap.ap_invoice_distributions_all d
                WHERE d.accounting_event_id = h.event_id
                  AND d.invoice_id = i.invoice_id)
          AND i.vendor_id = s.vendor_id
          AND t.code_combination_id = gcc.code_combination_id
          AND t.gl_sl_link_id IN
              (SELECT DISTINCT a.gl_sl_link_id
                 FROM xxcus.xxcusie_ww_gl_gtt a
                WHERE a.gl_sl_link_id = t.gl_sl_link_id
                  AND a.gl_sl_link_id IS NOT NULL)
        GROUP BY substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50)
                ,i.invoice_num
                ,gcc.segment1
                ,gcc.segment2
                ,gcc.segment3
                ,gcc.segment4
                ,gcc.segment5
                ,gcc.segment6
                ,gcc.segment7
                ,i.vendor_id);
  
    --Cursor to get the LINE rows
    l_sec := 'Writing LINE File; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    FOR c_glfilter_ww_rec IN (SELECT a.descr
                                    ,a.report_num
                                    ,a.segment1
                                    ,a.segment2
                                    ,a.segment3
                                    ,a.segment4
                                    ,a.segment5
                                    ,a.segment6
                                    ,a.segment7
                                    ,a.period_name
                                    ,nvl(a.accounted_dr, 0) accounted_dr
                                    ,nvl(a.accounted_cr, 0) accounted_cr
                                    ,a.employee_num
                                FROM xxcus.xxcusie_ww_gl_gtt2 a
                                    ,(SELECT b.segment1
                                            ,b.segment2
                                            ,b.segment3
                                            ,b.segment4
                                            ,SUM(nvl(accounted_dr, 0) -
                                                 nvl(accounted_cr, 0)) amount
                                        FROM xxcus.xxcusie_ww_gl_gtt2 b
                                       GROUP BY b.segment1
                                               ,b.segment2
                                               ,b.segment3
                                               ,b.segment4) b
                               WHERE a.segment1 = b.segment1
                                 AND a.segment2 = b.segment2
                                 AND a.segment3 = b.segment3
                                 AND a.segment4 = b.segment4
                               ORDER BY segment1
                                       ,segment2
                                       ,segment3
                                       ,segment4
                                       ,segment5
                                       ,segment6
                                       ,segment7)
    LOOP
      l_tot_accts := 0;
    
      l_debit  := nvl(c_glfilter_ww_rec.accounted_dr, 0);
      l_credit := nvl(c_glfilter_ww_rec.accounted_cr, 0);
      l_total  := l_debit - l_credit;
      l_text   := c_glfilter_ww_rec.descr;
    
      IF l_total >= 0 THEN
        l_d_c_id := '40';
      ELSE
        l_d_c_id := '50';
      END IF;
    
      SELECT DISTINCT description
        INTO l_seg_descr
        FROM apps.fnd_flex_values_vl
       WHERE value_category LIKE 'XXCUS_GL_PRODUCT'
         AND flex_value = c_glfilter_ww_rec.segment1;
    
      INSERT INTO xxcus.xxcusie_ww_filter_extract_tbl
      VALUES
        (l_d_c_id
        ,(CASE WHEN l_total > 0 THEN abs(l_total) ELSE 0 END)
        ,(CASE WHEN l_total < 0 THEN abs(l_total) ELSE 0 END)
        ,l_text
        ,c_glfilter_ww_rec.report_num
        ,c_glfilter_ww_rec.segment1
        ,l_seg_descr
        ,c_glfilter_ww_rec.segment2
        ,c_glfilter_ww_rec.segment3
        ,c_glfilter_ww_rec.segment4
        ,c_glfilter_ww_rec.segment5
        ,c_glfilter_ww_rec.segment6
        ,c_glfilter_ww_rec.segment7
        ,c_glfilter_ww_rec.period_name
        ,l_jrefnum --l_runnum
        ,c_glfilter_ww_rec.employee_num);
    
    END LOOP;
  
    DELETE FROM xxcus.xxcusie_ww_filter_extract_tbl
     WHERE debit = 0
       AND credit = 0;
  
    COMMIT;
  
    FOR c_glprocess_ww_rec IN (SELECT DISTINCT report_num
                                              ,segment1
                                              ,segment2
                                              ,segment3
                                              ,segment4
                                              ,segment5
                                              ,segment6
                                              ,segment7
                                 FROM xxcus.xxcusie_ww_filter_extract_tbl
                                WHERE segment4 = '211100')
    LOOP
    
      l_whack := 0;
    
      SELECT SUM(t.debit) - SUM(t.credit)
        INTO l_whack
        FROM xxcus.xxcusie_ww_filter_extract_tbl t
       WHERE report_num = c_glprocess_ww_rec.report_num
         AND segment1 = c_glprocess_ww_rec.segment1
         AND segment2 = c_glprocess_ww_rec.segment2
         AND segment3 = c_glprocess_ww_rec.segment3
         AND segment4 = c_glprocess_ww_rec.segment4
         AND segment5 = c_glprocess_ww_rec.segment5
         AND segment6 = c_glprocess_ww_rec.segment6
         AND segment7 = c_glprocess_ww_rec.segment7
       GROUP BY segment1
               ,segment2
               ,segment3
               ,segment4
               ,segment5
               ,segment6
               ,segment7;
    
      IF l_whack = 0 THEN
        DELETE FROM xxcus.xxcusie_ww_filter_extract_tbl t
         WHERE report_num = c_glprocess_ww_rec.report_num
           AND segment1 = c_glprocess_ww_rec.segment1
           AND segment2 = c_glprocess_ww_rec.segment2
           AND segment3 = c_glprocess_ww_rec.segment3
           AND segment4 = c_glprocess_ww_rec.segment4
           AND segment5 = c_glprocess_ww_rec.segment5
           AND segment6 = c_glprocess_ww_rec.segment6
           AND segment7 = c_glprocess_ww_rec.segment7;
      
        COMMIT;
      
      END IF;
    
    END LOOP;
  
    FOR c_interc IN (SELECT SUM(debit) - SUM(credit) total
                           ,segment1
                           ,segment1_decsr
                           ,segment2
                           ,segment3
                           ,segment4
                           ,segment5
                           ,segment6
                           ,segment7
                           ,period_name
                           ,run_num
                       FROM xxcus.xxcusie_ww_filter_extract_tbl
                      WHERE segment2 = 'Z9999'
                      GROUP BY segment1
                              ,segment1_decsr
                              ,segment2
                              ,segment3
                              ,segment4
                              ,segment5
                              ,segment6
                              ,segment7
                              ,period_name
                              ,run_num)
    LOOP
    
      l_line_num := l_line_num + 1;
    
      INSERT INTO xxcus.xxcusie_et_extract_tbl
      VALUES
        (l_sequence
        ,l_line_num
        ,l_sourcesys
        ,l_date
        ,(CASE WHEN c_interc.total >= 0 THEN '40' ELSE '50' END) --c_interc.d_c_id
        ,(CASE WHEN c_interc.total > 0 THEN abs(c_interc.total) ELSE 0 END) --debit
        ,(CASE WHEN c_interc.total < 0 THEN abs(c_interc.total) ELSE 0 END) --credit
        ,'Intercompany added by Posting' --ww_text
        ,c_interc.segment1
        ,c_interc.segment1_decsr
        ,c_interc.segment2
        ,c_interc.segment3
        ,c_interc.segment4
        ,c_interc.segment5
        ,c_interc.segment6
        ,c_interc.segment7
        ,c_interc.period_name
        ,c_interc.run_num
        ,' '); --added space for ET
    
    END LOOP;
  
    FOR c_gl_ww_rec IN (SELECT *
                          FROM xxcus.xxcusie_ww_filter_extract_tbl
                         WHERE segment2 <> 'Z9999')
    LOOP
    
      l_line_num := l_line_num + 1;
    
      INSERT INTO xxcus.xxcusie_et_extract_tbl
      VALUES
        (l_sequence
        ,l_line_num
        ,l_sourcesys
        ,l_date
        ,c_gl_ww_rec.d_c_id
        ,abs(c_gl_ww_rec.debit)
        ,abs(c_gl_ww_rec.credit)
        ,c_gl_ww_rec.ww_text
        ,c_gl_ww_rec.segment1
        ,c_gl_ww_rec.segment1_decsr
        ,c_gl_ww_rec.segment2
        ,c_gl_ww_rec.segment3
        ,c_gl_ww_rec.segment4
        ,c_gl_ww_rec.segment5
        ,c_gl_ww_rec.segment6
        ,c_gl_ww_rec.segment7
        ,c_gl_ww_rec.period_name
        ,c_gl_ww_rec.run_num
        ,nvl(c_gl_ww_rec.employee_num, ' ')); --added space for ET
    
    END LOOP;
  
    COMMIT;
  
  EXCEPTION
  
    WHEN program_error THEN
      ROLLBACK;
    
      DELETE FROM xxcus.xxcusie_ww_aud_tbl
       WHERE conc_request_id = l_req_id
         AND sequence = l_sequence;
      COMMIT;
    
      DELETE FROM xxcus.xxcusie_et_extract_tbl
       WHERE period_name = p_fperiod
         AND run_num = l_runnum
         AND sequence = l_sequence;
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for XXCUSIE_GL_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error during procedure load_ww_gl_int',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
    WHEN OTHERS THEN
    
      DELETE FROM xxcus.xxcusie_ww_aud_tbl
       WHERE conc_request_id = l_req_id
         AND sequence = l_sequence;
      COMMIT;
    
      DELETE FROM xxcus.xxcusie_et_extract_tbl
       WHERE period_name = p_fperiod
         AND run_num = l_runnum
         AND sequence = l_sequence;
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for XXCUSIE_GL_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error during procedure load_ww_gl_int',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
  END load_ww_gl_int;

  /*******************************************************************************
  * Procedure:   load_can_gl_int
  * Description: Creates output file in output directory indicated named
  *              'CAN_GL_IEXP_<date in 'YYYYMMDDHHMISS' format>.txt'
  *              Employee added.
  *              Concurrent 'Request HDS GL CAN Outbound' under the responsibility
  *              XXCUS_CON
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/01/2013    Luong Vu        Initial creation of the package.
  
  ********************************************************************************/




  /*******************************************************************************
  * Procedure:   load_can_gl_int
  * Description: Creates output file in output directory indicated named
  *              'CAN_GL_IEXP_<date in 'YYYYMMDDHHMISS' format>.txt'
  *              Employee added.
  *              Concurrent 'Request HDS GL CAN Outbound' under the responsibility
  *              XXCUS_CON
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/01/2013    Luong Vu        Initial creation of the package.
  
  ********************************************************************************/

  PROCEDURE load_can_gl_int(errbuf    OUT VARCHAR2
                           ,retcode   OUT NUMBER
                           ,p_fperiod IN VARCHAR2) IS
  
    --Intialize Variables
  
    l_err_msg         VARCHAR2(2000);
    l_err_code        NUMBER;
    l_processed_count NUMBER := 0;
    l_sec             VARCHAR2(150);
    l_tot_accts       NUMBER := 0;
    l_debit           NUMBER := 0;
    l_credit          NUMBER := 0;
    l_seg_descr       VARCHAR2(240);
    l_lob             VARCHAR2(3);
    l_date            DATE;
    l_sequence        NUMBER;
    l_line_num        NUMBER := 0;
    l_total           NUMBER := 0;
    l_d_c_id          VARCHAR2(10);
    l_runnum          NUMBER := 0;
    l_text            VARCHAR2(50) := 'NA';
    l_whack           NUMBER := 0;
    l_sourcesys       VARCHAR2(75) := 'ORCL-CAN';
  
    l_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    l_coa      CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := '50328';
    l_ledger   CONSTANT gl_ledgers.ledger_id%TYPE := 2063; --HDS Supply CAD
    l_period         gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_shortperiod    gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_procedure_name VARCHAR2(75) := 'XXCUSIE_GL_PKG.LOAD_CAN_GL_INT';
  
    l_err_callfrom VARCHAR2(75) DEFAULT 'XXCUSIE_GL_PKG';
    --l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id      NUMBER := fnd_global.conc_request_id;
  
    --Start Main Program
  BEGIN
    l_sec := 'In create file name; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    SELECT period_name
          ,substr(period_name, 1, 3)
      INTO l_period
          ,l_shortperiod
      FROM gl.gl_periods
     WHERE period_set_name = l_calendar
       AND start_date <= trunc(SYSDATE)
       AND end_date >= trunc(SYSDATE)
       AND period_num <> 13;
  
    SELECT SYSDATE
      INTO l_date
      FROM dual;
  
    SELECT xxcus.xxcusie_et_extract_s.nextval
      INTO l_sequence
      FROM dual;
  
    --Truncate XXCUS.XXCUSIE_can_FILTER_EXTRACT_TBL
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSIE_CAN_FILTER_EXTRACT_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSIE_CAN_FILTER_EXTRACT_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    --Determine Run Number
    BEGIN
      SELECT (MAX(nvl(run_num, 0)) + 1)
        INTO l_runnum
        FROM xxcus.xxcusie_can_aud_tbl
       WHERE period_name = p_fperiod;
    EXCEPTION
      WHEN no_data_found THEN
        l_runnum := 1;
      WHEN OTHERS THEN
        l_runnum := 1;
    END;
  
    IF l_runnum IS NULL THEN
      l_runnum := 1;
    END IF;
  
    --Insert Global Line Information GTT
    l_sec := 'Loading LINE global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    INSERT INTO xxcus.xxcusie_can_gl_gtt
      (SELECT a.accounted_cr
             ,a.accounted_dr
             ,a.je_header_id
             ,a.je_line_num
             ,c.segment1
             ,c.segment2
             ,c.segment3
             ,c.segment4
             ,c.segment5
             ,c.segment6
             ,c.segment7
             ,b.je_category
             ,d.gl_sl_link_id
             ,to_char(a.effective_date, 'YYYY-MM-DD') eff_date
             ,to_char(b.posted_date, 'YYYY-MM-DD') post_date
             ,substr(a.description, 1, 50) descr
             ,a.period_name
         FROM gl.gl_je_lines          a
             ,gl.gl_je_headers        b
             ,gl.gl_code_combinations c
             ,gl.gl_import_references d
        WHERE a.code_combination_id = c.code_combination_id
          AND a.je_header_id = b.je_header_id
          AND (b.je_header_id, a.je_line_num) NOT IN
              (SELECT i.je_header_id
                     ,i.je_line_num
                 FROM xxcus.xxcusie_can_aud_tbl i
                WHERE i.je_header_id = b.je_header_id
                  AND i.je_line_num = a.je_line_num
                  AND i.period_name = p_fperiod)
          AND b.currency_code <> 'STAT'
          AND a.ledger_id = l_ledger
          AND a.period_name = p_fperiod
          AND a.je_header_id = d.je_header_id(+)
          AND a.je_line_num = d.je_line_num(+)
          AND c.segment1 IN
              (SELECT h.description
                 FROM apps.fnd_lookup_values h
                WHERE h.lookup_type = 'HDS_CANGLEXTR'
                  AND enabled_flag = 'Y'
                  AND nvl(end_date_active, SYSDATE) >= SYSDATE
                  AND h.lookup_code LIKE 'SEG1_%')
          AND a.status = 'P'
          AND c.chart_of_accounts_id = l_coa);
    COMMIT;
    l_sec := 'Load audit table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Cursor to load audit table
    FOR c_gl_can_audit_rec IN (SELECT DISTINCT je_header_id
                                              ,je_line_num
                                              ,period_name
                                 FROM xxcus.xxcusie_can_gl_gtt)
    LOOP
    
      INSERT INTO xxcus.xxcusie_can_aud_tbl
      VALUES
        (c_gl_can_audit_rec.je_header_id
        ,c_gl_can_audit_rec.je_line_num
        ,c_gl_can_audit_rec.period_name
        ,nvl(fnd_global.conc_request_id, 0)
        ,SYSDATE
        ,nvl(fnd_global.user_name, 'NA')
        ,nvl(fnd_global.resp_name, 'NA')
        ,'File created'
        ,SYSDATE
        ,l_runnum
        ,l_sequence);
    
    END LOOP;
    COMMIT;
    --INSERT DR SUM and CR SUM HERE
    --Insert Global Line Information GTT
    l_sec := 'Loading CR 1 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the Intercompany CR transactions
    INSERT INTO xxcus.xxcusie_can_gl_gtt2
      (SELECT a.descr
             ,0
             ,a.segment1
             ,a.segment2
             ,a.segment3
             ,a.segment4
             ,a.segment5
             ,a.segment6
             ,a.segment7
             ,a.period_name
             ,SUM(nvl(a.accounted_cr, 0))
              --,nvl(a.accounted_cr, 0)
             ,NULL
             ,NULL
         FROM xxcus.xxcusie_can_gl_gtt a
        WHERE a.accounted_cr IS NOT NULL
          AND a.gl_sl_link_id IS NULL
           OR a.descr LIKE 'Intracompany%'
        GROUP BY a.descr
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7
                ,a.period_name);
    COMMIT;
    l_sec := 'Loading DR 2 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the Intercompany DB transactions
    INSERT INTO xxcus.xxcusie_can_gl_gtt2
      (SELECT a.descr
             ,0
             ,a.segment1
             ,a.segment2
             ,a.segment3
             ,a.segment4
             ,a.segment5
             ,a.segment6
             ,a.segment7
             ,a.period_name
             ,NULL
             ,SUM(nvl(a.accounted_dr, 0))
              --,nvl(a.accounted_dr, 0)
             ,NULL
         FROM xxcus.xxcusie_can_gl_gtt a
        WHERE a.accounted_dr IS NOT NULL
          AND a.gl_sl_link_id IS NULL
           OR a.descr LIKE 'Intracompany%'
        GROUP BY a.descr
                ,a.segment1
                ,a.segment2
                ,a.segment3
                ,a.segment4
                ,a.segment5
                ,a.segment6
                ,a.segment7
                ,a.period_name);
    COMMIT;
    l_sec := 'Loading DR 3 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the NON-Intercompany CR transactions
    INSERT INTO xxcus.xxcusie_can_gl_gtt2
      (SELECT substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50) line_item_text
             ,i.invoice_num
             ,gcc.segment1
             ,gcc.segment2
             ,gcc.segment3
             ,gcc.segment4
             ,gcc.segment5
             ,gcc.segment6
             ,gcc.segment7
             ,p_fperiod
             ,SUM(nvl(t.accounted_cr, 0))
              --,nvl(t.accounted_cr, 0)
             ,NULL
             ,(SELECT (SELECT p.employee_number
                         FROM hr.per_all_people_f p
                        WHERE p.person_id = v.employee_id
                          AND object_version_number IN
                              (SELECT MAX(object_version_number)
                                 FROM hr.per_all_people_f b
                                WHERE b.person_id = p.person_id)) employee_num
                 FROM apps.po_vendors v
                WHERE v.vendor_id = i.vendor_id) employee_num
         FROM xla.xla_ae_lines        t
             ,xla.xla_ae_headers      h
             ,gl.gl_import_references gir
             ,gl.gl_code_combinations gcc
             ,ap.ap_invoices_all      i
             ,ap.ap_suppliers         s
        WHERE t.ae_header_id = h.ae_header_id
          AND t.accounted_cr IS NOT NULL
          AND nvl(t.description, 'X') NOT LIKE 'Intracompany%' --added 3-10-2012  --Version 1.1
          AND t.gl_sl_link_id = gir.gl_sl_link_id
          AND h.event_id IN
              (SELECT DISTINCT accounting_event_id
                 FROM ap.ap_invoice_distributions_all d
                WHERE d.accounting_event_id = h.event_id
                  AND d.invoice_id = i.invoice_id)
          AND i.vendor_id = s.vendor_id
          AND t.code_combination_id = gcc.code_combination_id
          AND t.gl_sl_link_id IN
              (SELECT DISTINCT a.gl_sl_link_id
                 FROM xxcus.xxcusie_can_gl_gtt a
                WHERE a.gl_sl_link_id = t.gl_sl_link_id
                  AND a.gl_sl_link_id IS NOT NULL)
        GROUP BY substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50)
                ,i.invoice_num
                ,gcc.segment1
                ,gcc.segment2
                ,gcc.segment3
                ,gcc.segment4
                ,gcc.segment5
                ,gcc.segment6
                ,gcc.segment7
                ,i.vendor_id);
    COMMIT;
  
    l_sec := 'Loading DR 4 global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the NON-Intercompany DB transactions
    INSERT INTO xxcus.xxcusie_can_gl_gtt2
      (SELECT substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50) line_item_text
             ,i.invoice_num
             ,gcc.segment1
             ,gcc.segment2
             ,gcc.segment3
             ,gcc.segment4
             ,gcc.segment5
             ,gcc.segment6
             ,gcc.segment7
             ,p_fperiod
             ,NULL
             ,SUM(nvl(t.accounted_dr, 0))
              --,nvl(t.accounted_dr, 0)
             ,(SELECT (SELECT p.employee_number
                         FROM hr.per_all_people_f p
                        WHERE p.person_id = v.employee_id
                          AND object_version_number IN
                              (SELECT MAX(object_version_number)
                                 FROM hr.per_all_people_f b
                                WHERE b.person_id = p.person_id)) employee_num
                 FROM apps.po_vendors v
                WHERE v.vendor_id = i.vendor_id) employee_num
         FROM xla.xla_ae_lines        t
             ,xla.xla_ae_headers      h
             ,gl.gl_import_references gir
             ,gl.gl_code_combinations gcc
             ,ap.ap_invoices_all      i
             ,ap.ap_suppliers         s
        WHERE t.ae_header_id = h.ae_header_id
          AND t.accounted_dr IS NOT NULL
          AND nvl(t.description, 'X') NOT LIKE 'Intracompany%' --added 3-10-2012  --Version 1.1
          AND t.gl_sl_link_id = gir.gl_sl_link_id
          AND h.event_id IN
              (SELECT DISTINCT accounting_event_id
                 FROM ap.ap_invoice_distributions_all d
                WHERE d.accounting_event_id = h.event_id
                  AND d.invoice_id = i.invoice_id)
          AND i.vendor_id = s.vendor_id
          AND t.code_combination_id = gcc.code_combination_id
          AND t.gl_sl_link_id IN
              (SELECT DISTINCT a.gl_sl_link_id
                 FROM xxcus.xxcusie_can_gl_gtt a
                WHERE a.gl_sl_link_id = t.gl_sl_link_id
                  AND a.gl_sl_link_id IS NOT NULL)
        GROUP BY substr(i.invoice_num || ' - ' || s.vendor_name, 1, 50)
                ,i.invoice_num
                ,gcc.segment1
                ,gcc.segment2
                ,gcc.segment3
                ,gcc.segment4
                ,gcc.segment5
                ,gcc.segment6
                ,gcc.segment7
                ,i.vendor_id);
    COMMIT;
  
    l_sec := 'Loading JE not exist in AP Distribution to global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Enter the NON-Intercompany CR transactions (not in distribution)
    INSERT INTO xxcus.xxcusie_can_gl_gtt2
      (SELECT cg.descr
             ,'manual journal entry'
             ,cg.segment1
             ,cg.segment2
             ,cg.segment3
             ,cg.segment4
             ,cg.segment5
             ,cg.segment6
             ,cg.segment7
             ,cg.period_name
             ,cg.accounted_cr
             ,cg.accounted_dr
             ,NULL
         FROM xxcus.xxcusie_can_gl_gtt cg
             ,xla.xla_ae_lines         el
             ,xla.xla_ae_headers       eh
        WHERE eh.ae_header_id = el.ae_header_id
          AND el.gl_sl_link_id = cg.gl_sl_link_id
          AND cg.descr <> 'Intracompany Line'
          AND eh.event_id NOT IN (SELECT DISTINCT accounting_event_id
                                    FROM ap.ap_invoice_distributions_all d
                                   WHERE d.accounting_event_id = eh.event_id
                                  --AND d.invoice_id = i.invoice_id
                                  ));
    COMMIT;
  
    --Cursor to get the LINE rows
    l_sec := 'Writing LINE File; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    FOR c_glfilter_can_rec IN (SELECT a.descr
                                     ,a.report_num
                                     ,a.segment1
                                     ,a.segment2
                                     ,a.segment3
                                     ,a.segment4
                                     ,a.segment5
                                     ,a.segment6
                                     ,a.segment7
                                     ,a.period_name
                                     ,nvl(a.accounted_dr, 0) accounted_dr
                                     ,nvl(a.accounted_cr, 0) accounted_cr
                                     ,a.employee_num
                                 FROM xxcus.xxcusie_can_gl_gtt2 a
                                     ,(SELECT b.segment1
                                             ,b.segment2
                                             ,b.segment3
                                             ,b.segment4
                                             ,SUM(nvl(accounted_dr, 0) -
                                                  nvl(accounted_cr, 0)) amount
                                         FROM xxcus.xxcusie_can_gl_gtt2 b
                                        GROUP BY b.segment1
                                                ,b.segment2
                                                ,b.segment3
                                                ,b.segment4) b
                                WHERE a.segment1 = b.segment1
                                  AND a.segment2 = b.segment2
                                  AND a.segment3 = b.segment3
                                  AND a.segment4 = b.segment4
                                ORDER BY segment1
                                        ,segment2
                                        ,segment3
                                        ,segment4
                                        ,segment5
                                        ,segment6
                                        ,segment7)
    LOOP
      l_tot_accts := 0;
    
      l_debit  := nvl(c_glfilter_can_rec.accounted_dr, 0);
      l_credit := nvl(c_glfilter_can_rec.accounted_cr, 0);
      l_total  := l_debit - l_credit;
      --l_abstotal := abs(l_total);
      IF l_total >= 0 THEN
        l_d_c_id := '40';
      ELSE
        l_d_c_id := '50';
      END IF;
      l_text := c_glfilter_can_rec.descr;
    
      SELECT DISTINCT description
        INTO l_seg_descr
        FROM apps.fnd_flex_values_vl
       WHERE value_category LIKE 'XXCUS_GL_PRODUCT'
         AND flex_value = c_glfilter_can_rec.segment1;
    
      INSERT INTO xxcus.xxcusie_can_filter_extract_tbl
      VALUES
        (l_d_c_id
        ,(CASE WHEN l_total > 0 THEN abs(l_total) ELSE 0 END)
        ,(CASE WHEN l_total < 0 THEN abs(l_total) ELSE 0 END)
        ,l_text
        ,c_glfilter_can_rec.report_num
        ,c_glfilter_can_rec.segment1
        ,l_seg_descr
        ,c_glfilter_can_rec.segment2
        ,c_glfilter_can_rec.segment3
        ,c_glfilter_can_rec.segment4
        ,c_glfilter_can_rec.segment5
        ,c_glfilter_can_rec.segment6
        ,c_glfilter_can_rec.segment7
        ,c_glfilter_can_rec.period_name
        ,l_runnum
        ,c_glfilter_can_rec.employee_num);
    
    END LOOP;
    COMMIT;
    FOR c_interc IN (SELECT SUM(debit) - SUM(credit) total
                           ,segment1
                           ,segment1_decsr
                           ,segment2
                           ,segment3
                           ,segment4
                           ,segment5
                           ,segment6
                           ,segment7
                           ,period_name
                           ,run_num
                       FROM xxcus.xxcusie_can_filter_extract_tbl
                      WHERE segment2 = 'Z9999'
                      GROUP BY segment1
                              ,segment1_decsr
                              ,segment2
                              ,segment3
                              ,segment4
                              ,segment5
                              ,segment6
                              ,segment7
                              ,period_name
                              ,run_num)
    LOOP
    
      l_line_num := l_line_num + 1;
    
      INSERT INTO xxcus.xxcusie_et_extract_tbl
      VALUES
        (l_sequence
        ,l_line_num
        ,l_sourcesys
        ,l_date
        ,(CASE WHEN c_interc.total >= 0 THEN '40' ELSE '50' END) --c_interc.d_c_id
        ,(CASE WHEN c_interc.total > 0 THEN abs(c_interc.total) ELSE 0 END) --debit
        ,(CASE WHEN c_interc.total < 0 THEN abs(c_interc.total) ELSE 0 END) --credit
        ,'Intercompany line added by Posting' --can_text
        ,c_interc.segment1
        ,c_interc.segment1_decsr
        ,c_interc.segment2
        ,c_interc.segment3
        ,c_interc.segment4
        ,c_interc.segment5
        ,c_interc.segment6
        ,c_interc.segment7
        ,c_interc.period_name
        ,c_interc.run_num
        ,' '); --added space for ET
    
    END LOOP;
    COMMIT;
    FOR c_gl_can_rec IN (SELECT *
                           FROM xxcus.xxcusie_can_filter_extract_tbl
                          WHERE segment2 <> 'Z9999')
    LOOP
    
      l_line_num := l_line_num + 1;
    
      INSERT INTO xxcus.xxcusie_et_extract_tbl
      VALUES
        (l_sequence
        ,l_line_num
        ,l_sourcesys
        ,l_date
        ,c_gl_can_rec.d_c_id
        ,abs(c_gl_can_rec.debit)
        ,abs(c_gl_can_rec.credit)
        ,c_gl_can_rec.can_text
        ,c_gl_can_rec.segment1
        ,c_gl_can_rec.segment1_decsr
        ,c_gl_can_rec.segment2
        ,c_gl_can_rec.segment3
        ,c_gl_can_rec.segment4
        ,c_gl_can_rec.segment5
        ,c_gl_can_rec.segment6
        ,c_gl_can_rec.segment7
        ,c_gl_can_rec.period_name
        ,c_gl_can_rec.run_num
        ,nvl(c_gl_can_rec.employee_num, ' ')); --added space for ET
    
    END LOOP;
    COMMIT;
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
    
      DELETE FROM xxcus.xxcusie_can_aud_tbl
       WHERE conc_request_id = l_req_id;
      COMMIT;
    
      DELETE FROM xxcus.xxcusie_et_extract_tbl
       WHERE period_name = p_fperiod
         AND run_num = l_runnum
         AND sequence = l_sequence;
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for XXCUSIE_GL_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error during procedure load_can_gl_int',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
    WHEN OTHERS THEN
      DELETE FROM xxcus.xxcusie_can_aud_tbl
       WHERE conc_request_id = l_req_id;
      COMMIT;
    
      DELETE FROM xxcus.xxcusie_et_extract_tbl
       WHERE period_name = p_fperiod
         AND run_num = l_runnum
         AND sequence = l_sequence;
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for XXCUSIE_GL_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error during procedure load_can_gl_int',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    
  END load_can_gl_int;


END xxcusie_gl_pkg;
/