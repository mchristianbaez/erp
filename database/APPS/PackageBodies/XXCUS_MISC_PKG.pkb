CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_MISC_PKG
AS
 /**************************************************************************
    File Name:xxhsi_misc_pkg
    PROGRAM TYPE: PL/SQL
    PURPOSE: Miscellaneous Packages
    HISTORY
    ================================================================
           Last Update Date : 04/27/2016
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
     1.0    06-JUN-2010   Kathy Poling     Creation taken from Edric and Jason
     1.1    02-JAN-2013   Luong Vu         Added function create_ccid_generic
     1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
     1.4    27-APR-2016   Rakesh Patel     Added a new default parameter to email_attachment for TMS#20151023-00047\
     1.5    28-JUL-2017   Nancy Pahwa      Added option to send email to multiple users Task ID: 20170601-00292
   **************************************************************************/

   -- Error DEBUG
   g_err_callfrom                     VARCHAR2 (75)  DEFAULT 'XXCUS_MISC_PKG';
   g_err_callpoint                    VARCHAR2 (75)  DEFAULT 'START';
   g_distro_list                      VARCHAR2 (75)
                                   DEFAULT 'HDSOracleDevelopers@hdsupply.com';
--------------------------
-- Added w.r.t. ver 1.3 --
--------------------------
   g_smtp_host                        VARCHAR2 (256)
                                               := 'mailoutrelay.hdsupply.net';
   g_smtp_port                        PLS_INTEGER    := 25;
   g_smtp_domain                      VARCHAR2 (256) := 'hdsupply.net';
-- -------------------------------------------------------------------
-- Customize the signature that will appear in the email's MIME header
-- -------------------------------------------------------------------
   g_mailer_id               CONSTANT VARCHAR2 (256)
                                               := 'Mailer by Oracle UTL_SMTP';
-- -------------------------------------------------------------------------
-- A unique string that demarcates boundaries of parts in a multi-part email
-- -------------------------------------------------------------------------
   g_boundary                CONSTANT VARCHAR2 (256)
                                           := '-----7D81B75CCC90D2974F7A1CBD';
   g_first_boundary          CONSTANT VARCHAR2 (256)
                                        := '--' || g_boundary || UTL_TCP.crlf;
   g_last_boundary           CONSTANT VARCHAR2 (256)
                                := '--' || g_boundary || '--' || UTL_TCP.crlf;
-- -------------------------------------------------------------------
-- A MIME type that denotes multi-part email (MIME) messages.
-- -------------------------------------------------------------------
   g_multipart_mime_type     CONSTANT VARCHAR2 (256)
                        := 'multipart/mixed; boundary="' || g_boundary || '"';
   g_max_base64_line_width   CONSTANT PLS_INTEGER    := 76 / 4 * 3;

   FUNCTION get_address (p_addr_list IN OUT VARCHAR2)
      RETURN VARCHAR2
   IS
              /**************************************************************************
                  *
                  * FUNCTION
                  *  get_address
                  *
                  * DESCRIPTION
                  *  This function will return the next email address/attachment
      *  name in a list of email addresses/attachment names, separated
                  *     by either a comma"," or a semi-colon";"
                  *
                  * PARAMETERS
                  * ==========
                  * NAME                          TYPE     DESCRIPTION
                  * -----------------             -------- ---------------------------------------------
                  * p_addr_list          IN OUT   String of email addresses/attachment names
                  *
                  * RETURN VALUE
                  *  Email address/attachment name - VARCHAR2
 -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
     1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
                  *************************************************************************/
      l_addr   VARCHAR2 (256);
      i        PLS_INTEGER;

      FUNCTION lookup_unquoted_char (p_str IN VARCHAR2, p_chrs IN VARCHAR2)
         RETURN PLS_INTEGER
      AS
         l_char           VARCHAR2 (5);
         j                PLS_INTEGER;
         l_len            PLS_INTEGER;
         l_inside_quote   BOOLEAN;
      BEGIN
         l_inside_quote := FALSE;
         j := 1;
         l_len := LENGTH (p_str);

         WHILE (j <= l_len)
         LOOP
            l_char := SUBSTR (p_str, j, 1);

            IF (l_inside_quote)
            THEN
               IF (l_char = '"')
               THEN
                  l_inside_quote := FALSE;
               ELSIF (l_char = '\')
               THEN
                  j := j + 1;
               END IF;

               GOTO next_char;
            END IF;

            IF (l_char = '"')
            THEN
               l_inside_quote := TRUE;
               GOTO next_char;
            END IF;

            IF (INSTR (p_chrs, l_char) >= 1)
            THEN
               RETURN j;
            END IF;

            <<next_char>>
            j := j + 1;
         END LOOP;

         RETURN 0;
      END;
   BEGIN
      p_addr_list := LTRIM (p_addr_list);
      i := lookup_unquoted_char (p_addr_list, ',;');

      IF (i >= 1)
      THEN
         l_addr := SUBSTR (p_addr_list, 1, i - 1);
         p_addr_list := SUBSTR (p_addr_list, i + 1);
      ELSE
         l_addr := p_addr_list;
         p_addr_list := '';
      END IF;

      i := lookup_unquoted_char (l_addr, '<');

      IF (i >= 1)
      THEN
         l_addr := SUBSTR (l_addr, i + 1);
         i := INSTR (l_addr, '>');

         IF (i >= 1)
         THEN
            l_addr := SUBSTR (l_addr, 1, i - 1);
         END IF;
      END IF;

      RETURN l_addr;
   END get_address;

   PROCEDURE write_mime_header (
      p_conn    IN OUT NOCOPY   UTL_SMTP.connection,
      p_name    IN              VARCHAR2,
      p_value   IN              VARCHAR2
   )
   IS
   /**************************************************************************
       *
       * PROCEDURE
       *  write_mime_header
       *
       * DESCRIPTION
       *  This procedure will write the MIME header for the attachment type
       *
       * PARAMETERS
       * ==========
       * NAME                          TYPE     DESCRIPTION
       * -----------------             -------- ---------------------------------------------
       * p_conn               IN OUT   SMTP Connection
       * p_name               IN                Content-Type
       * p_value              IN                MIME Type
       *
	   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
     1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
       *************************************************************************/
   BEGIN
      UTL_SMTP.write_data (p_conn, p_name || ': ' || p_value || UTL_TCP.crlf);
   END write_mime_header;

   PROCEDURE write_boundary (
      p_conn   IN OUT NOCOPY   UTL_SMTP.connection,
      p_last   IN              BOOLEAN DEFAULT FALSE
   )
   AS
   /**************************************************************************
       *
       * PROCEDURE
       *  write_boundary
       *
       * DESCRIPTION
       *  This procedure will mark a message-part boundary
       *
       * PARAMETERS
       * ==========
       * NAME                          TYPE     DESCRIPTION
       * -----------------             -------- ---------------------------------------------
       * p_conn               IN OUT   SMTP Connection
       * p_last               IN                Set to TRUE for the last boundary
       *
	   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
         1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
       *************************************************************************/
   BEGIN
      IF (p_last)
      THEN
         UTL_SMTP.write_data (p_conn, g_last_boundary);
      ELSE
         UTL_SMTP.write_data (p_conn, g_first_boundary);
      END IF;
   END write_boundary;

   PROCEDURE write_text (
      p_conn      IN OUT NOCOPY   UTL_SMTP.connection,
      p_message   IN              VARCHAR2
   )
   IS
   /**************************************************************************
       *
       * PROCEDURE
       *  write_text
       *
       * DESCRIPTION
       *  This procedure will write text data to message
       *
       * PARAMETERS
       * ==========
       * NAME                          TYPE     DESCRIPTION
       * -----------------             -------- ---------------------------------------------
       * p_conn               IN OUT   SMTP Connection
       * p_message            IN                Text data
       *
	   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
        1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
       *************************************************************************/
   BEGIN
      UTL_SMTP.write_data (p_conn, p_message);
   END write_text;

   PROCEDURE write_mb_text (
      p_conn      IN OUT NOCOPY   UTL_SMTP.connection,
      p_message   IN              VARCHAR2
   )
   IS
   /**************************************************************************
       *
       * PROCEDURE
       *  write_mb_text
       *
       * DESCRIPTION
       *  This procedure will write text data to message as raw
       *
       * PARAMETERS
       * ==========
       * NAME                          TYPE     DESCRIPTION
       * -----------------             -------- ---------------------------------------------
       * p_conn               IN OUT   SMTP Connection
       * p_message            IN                Text data
       *
	   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
        1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
       *************************************************************************/
   BEGIN
      UTL_SMTP.write_raw_data (p_conn, UTL_RAW.cast_to_raw (p_message));
   END write_mb_text;

   PROCEDURE write_raw (
      p_conn      IN OUT NOCOPY   UTL_SMTP.connection,
      p_message   IN              RAW
   )
   IS
   /**************************************************************************
       *
       * PROCEDURE
       *  write_raw
       *
       * DESCRIPTION
       *  This procedure will write raw data to message
       *
       * PARAMETERS
       * ==========
       * NAME                          TYPE     DESCRIPTION
       * -----------------             -------- ---------------------------------------------
       * p_conn               IN OUT   SMTP Connection
       * p_message            IN                Text data
       *
	   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
     1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
       *************************************************************************/
   BEGIN
      UTL_SMTP.write_raw_data (p_conn, p_message);
   END write_raw;

   FUNCTION begin_session
      RETURN UTL_SMTP.connection
   IS
      /**************************************************************************
          *
          * FUNCTION
          *  begin_session
          *
          * DESCRIPTION
          *  This function will open an SMTP Connection
          *
          * RETURN VALUE
          *  SMTP connestion - UTL_SMTP.connection
		  -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
     1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
          *************************************************************************/
      l_conn   UTL_SMTP.connection;
   BEGIN
      -- open SMTP connection
      l_conn := UTL_SMTP.open_connection (g_smtp_host, g_smtp_port);
      UTL_SMTP.helo (l_conn, g_smtp_domain);
      RETURN l_conn;
   END begin_session;

   PROCEDURE begin_mail_in_session (
      p_conn         IN OUT NOCOPY   UTL_SMTP.connection,
      p_sender       IN              VARCHAR2,
      p_recipients   IN              VARCHAR2,
      p_subject      IN              VARCHAR2,
      p_mime_type    IN              VARCHAR2 DEFAULT 'text/plain',
      p_priority     IN              PLS_INTEGER DEFAULT NULL
   )
   IS
      /**************************************************************************
          *
          * PROCEDURE
          *  begin_mail_in_session
          *
          * DESCRIPTION
          *  This procedure will write mail header
          *
          * PARAMETERS
          * ==========
          * NAME                          TYPE     DESCRIPTION
          * -----------------             -------- ---------------------------------------------
          * p_conn               IN OUT   SMTP Connection
          * p_sender             IN                Sender of the email
          * p_recipients         IN                   Recipient list of the email separated by "," or ";"
          * p_subject            IN                Email subject
          * p_mime_type          IN                MIME Type
          * p_priority           IN                Email priority (1=High, 3=Nornal, 5=Low)
          *
		  -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
     1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
          *************************************************************************/
      l_my_recipients   VARCHAR2 (32767) := p_recipients;
      l_my_sender       VARCHAR2 (32767) := p_sender;
   BEGIN
      UTL_SMTP.mail (p_conn, get_address (l_my_sender));

                  -----------------------------------------------------------
-- Specify recipient(s) of the email.
                  -----------------------------------------------------------
      WHILE (l_my_recipients IS NOT NULL)
      LOOP
         UTL_SMTP.rcpt (p_conn, get_address (l_my_recipients));
      END LOOP;

                  -----------------------------------------------------------
-- Start body of email
                  -----------------------------------------------------------
      UTL_SMTP.open_data (p_conn);
-----------------------------------------------------------
                  -- Set "From" MIME header
                  -----------------------------------------------------------
      write_mime_header (p_conn, 'From', p_sender);
-----------------------------------------------------------
                  -- Set "To" MIME header
                  -----------------------------------------------------------
      write_mime_header (p_conn, 'To', p_recipients);
-----------------------------------------------------------
                  -- Set "Subject" MIME header
                  -----------------------------------------------------------
      write_mime_header (p_conn, 'Subject', p_subject);
-----------------------------------------------------------
                  -- Set "Content-Type" MIME header
                  -----------------------------------------------------------
      write_mime_header (p_conn, 'Content-Type', p_mime_type);
-----------------------------------------------------------
                  -- Set "X-Mailer" MIME header
                  -----------------------------------------------------------
      write_mime_header (p_conn, 'X-Mailer', g_mailer_id);

                  -----------------------------------------------------------
-- Set priority:
--   High      Normal       Low
--   1     2     3     4     5
                  -----------------------------------------------------------
      IF (p_priority IS NOT NULL)
      THEN
         write_mime_header (p_conn, 'X-Priority', p_priority);
      END IF;

                  ---------------------------------------------------------------------------------
-- Send an empty line to denote end of MIME header and beginning of message body.
                  ---------------------------------------------------------------------------------
      UTL_SMTP.write_data (p_conn, UTL_TCP.crlf);

      IF (p_mime_type LIKE 'multipart/mixed%')
      THEN
         write_text (p_conn,
                        'This is a multi-part message in MIME format.'
                     || UTL_TCP.crlf
                    );
      END IF;
   END begin_mail_in_session;

   FUNCTION begin_mail (
      p_sender       IN   VARCHAR2,
      p_recipients   IN   VARCHAR2,
      p_subject      IN   VARCHAR2,
      p_mime_type    IN   VARCHAR2 DEFAULT 'text/plain',
      p_priority     IN   PLS_INTEGER DEFAULT NULL
   )
      RETURN UTL_SMTP.connection
   IS
      /**************************************************************************
          *
          * PROCEDURE
          *  begin_mail
          *
          * DESCRIPTION
          *  This function is wrapper for begin_session and begin_mail_in_session
          *
          * PARAMETERS
          * ==========
          * NAME                          TYPE     DESCRIPTION
          * -----------------             -------- ---------------------------------------------
          * p_sender             IN                Sender of the email
          * p_recipients         IN                   Recipient list of the email separated by "," or ";"
          * p_subject            IN                Email subject
          * p_mime_type          IN                MIME Type
          * p_priority           IN                Email priority (1=High, 3=Nornal, 5=Low)
          *
		  -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
       1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
          *************************************************************************/
      l_conn   UTL_SMTP.connection;
   BEGIN
      l_conn := begin_session;
      begin_mail_in_session (l_conn,
                             p_sender,
                             p_recipients,
                             p_subject,
                             p_mime_type,
                             p_priority
                            );
      RETURN l_conn;
   END begin_mail;

   PROCEDURE begin_attachment (
      p_conn           IN OUT NOCOPY   UTL_SMTP.connection,
      p_mime_type      IN              VARCHAR2 DEFAULT 'text/plain',
      p_inline         IN              BOOLEAN DEFAULT TRUE,
      p_filename       IN              VARCHAR2 DEFAULT NULL,
      p_transfer_enc   IN              VARCHAR2 DEFAULT NULL
   )
   IS
           /**************************************************************************
               *
               * PROCEDURE
               *  begin_attachment
               *
               * DESCRIPTION
               *  Begin attachment by writing first boundary, setting filename
               *
               * PARAMETERS
               * ==========
               * NAME                          TYPE     DESCRIPTION
               * -----------------             -------- ---------------------------------------------
               * p_conn               IN OUT   SMTP Connection
               * p_mime_type          IN                MIME Type
               * p_inline             IN                Attach inline, true/false
               * p_filename           IN                Attachment filename
   * p_transfer_enc       IN                Transfer encoding
               *
			   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
        1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
               *************************************************************************/
   BEGIN
      write_boundary (p_conn);
      write_mime_header (p_conn, 'Content-Type', p_mime_type);

      IF (p_filename IS NOT NULL)
      THEN
         IF (p_inline)
         THEN
            write_mime_header (p_conn,
                               'Content-Disposition',
                               'inline; filename="' || p_filename || '"'
                              );
         ELSE
            write_mime_header (p_conn,
                               'Content-Disposition',
                               'attachment; filename="' || p_filename || '"'
                              );
         END IF;
      END IF;

      IF (p_transfer_enc IS NOT NULL)
      THEN
         write_mime_header (p_conn,
                            'Content-Transfer-Encoding',
                            p_transfer_enc
                           );
      END IF;

      UTL_SMTP.write_data (p_conn, UTL_TCP.crlf);
   END begin_attachment;

   PROCEDURE end_attachment (
      p_conn   IN OUT NOCOPY   UTL_SMTP.connection,
      p_last   IN              BOOLEAN DEFAULT FALSE
   )
   IS
   /**************************************************************************
       *
       * PROCEDURE
       *  end_attachment
       *
       * DESCRIPTION
       *  End attachment by writing last boundary
       *
       * PARAMETERS
       * ==========
       * NAME                          TYPE     DESCRIPTION
       * -----------------             -------- ---------------------------------------------
       * p_conn               IN OUT   SMTP Connection
       * p_last               IN                True if last boundary
       *
	   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
       1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
       *************************************************************************/
   BEGIN
      UTL_SMTP.write_data (p_conn, UTL_TCP.crlf);

      IF (p_last)
      THEN
         write_boundary (p_conn, p_last);
      END IF;
   END end_attachment;

   PROCEDURE attach_text (
      p_conn        IN OUT NOCOPY   UTL_SMTP.connection,
      p_data        IN              VARCHAR2,
      p_mime_type   IN              VARCHAR2 DEFAULT 'text/plain',
      p_inline      IN              BOOLEAN DEFAULT TRUE,
      p_filename    IN              VARCHAR2 DEFAULT NULL,
      p_last        IN              BOOLEAN DEFAULT FALSE
   )
   IS
           /**************************************************************************
               *
               * PROCEDURE
               *  attach_text
               *
               * DESCRIPTION
               *  Wrapper for attaching text document
               *
               * PARAMETERS
               * ==========
               * NAME                          TYPE     DESCRIPTION
               * -----------------             -------- ---------------------------------------------
               * p_conn               IN OUT   SMTP Connection
   * p_data               IN                Text data
   * p_mime_type          IN                MIME Type
               * p_inline             IN                Attach inline, true/false
   * p_filename           IN                Attachment filename
               * p_last               IN                True if last boundary
               *
			   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
     1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
               *************************************************************************/
   BEGIN
      begin_attachment (p_conn, p_mime_type, p_inline, p_filename);
      write_text (p_conn, p_data);
      end_attachment (p_conn, p_last);
   END attach_text;

   PROCEDURE attach_base64 (
      p_conn        IN OUT NOCOPY   UTL_SMTP.connection,
      p_data        IN              RAW,
      p_mime_type   IN              VARCHAR2 DEFAULT 'application/octet',
      p_inline      IN              BOOLEAN DEFAULT TRUE,
      p_filename    IN              VARCHAR2 DEFAULT NULL,
      p_last        IN              BOOLEAN DEFAULT FALSE
   )
   IS
              /**************************************************************************
                  *
                  * PROCEDURE
                  *  attach_base64
                  *
                  * DESCRIPTION
                  *  Wrapper for attaching raw/binary document
                  *
                  * PARAMETERS
                  * ==========
                  * NAME                          TYPE     DESCRIPTION
                  * -----------------             -------- ---------------------------------------------
                  * p_conn               IN OUT   SMTP Connection
      * p_data               IN                Raw/binary data
      * p_mime_type          IN                MIME Type
                  * p_inline             IN                Attach inline, true/false
      * p_filename           IN                Attachment filename
                  * p_last               IN                True if last boundary
                  *
				  -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
     1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
                  *************************************************************************/
      i          PLS_INTEGER;
      l_length   PLS_INTEGER;
   BEGIN
      begin_attachment (p_conn, p_mime_type, p_inline, p_filename, 'base64');
                  -----------------------------------------------------------
-- Split the Base64-encoded attachment into multiple lines
                  -----------------------------------------------------------
      i := 1;
      l_length := UTL_RAW.LENGTH (p_data);

      WHILE (i < l_length)
      LOOP
         IF (i + g_max_base64_line_width < l_length)
         THEN
            UTL_SMTP.write_raw_data
               (p_conn,
                UTL_ENCODE.base64_encode
                                      (UTL_RAW.SUBSTR (p_data,
                                                       i,
                                                       g_max_base64_line_width
                                                      )
                                      )
               );
         ELSE
            UTL_SMTP.write_raw_data
                           (p_conn,
                            UTL_ENCODE.base64_encode (UTL_RAW.SUBSTR (p_data,
                                                                      i
                                                                     )
                                                     )
                           );
         END IF;

         UTL_SMTP.write_data (p_conn, UTL_TCP.crlf);
         i := i + g_max_base64_line_width;
      END LOOP;

      end_attachment (p_conn, p_last);
   END attach_base64;

   PROCEDURE end_mail_in_session (p_conn IN OUT NOCOPY UTL_SMTP.connection)
   IS
   /**************************************************************************
       *
       * PROCEDURE
       *  end_mail_in_session
       *
       * DESCRIPTION
       *  End current session mail
       *
       * PARAMETERS
       * ==========
       * NAME                          TYPE     DESCRIPTION
       * -----------------             -------- ---------------------------------------------
       * p_conn               IN OUT   SMTP Connection
       *
	   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
          1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
       *************************************************************************/
   BEGIN
      UTL_SMTP.close_data (p_conn);
   END end_mail_in_session;

   PROCEDURE end_session (p_conn IN OUT NOCOPY UTL_SMTP.connection)
   IS
   /**************************************************************************
       *
       * PROCEDURE
       *  end_session
       *
       * DESCRIPTION
       *  End current SMTP session
       *
       * PARAMETERS
       * ==========
       * NAME                          TYPE     DESCRIPTION
       * -----------------             -------- ---------------------------------------------
       * p_conn               IN OUT   SMTP Connection
       *
	   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
        1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
       *************************************************************************/
   BEGIN
      UTL_SMTP.quit (p_conn);
   END end_session;

   PROCEDURE end_mail (p_conn IN OUT NOCOPY UTL_SMTP.connection)
   IS
   /**************************************************************************
       *
       * PROCEDURE
       *  end_mail
       *
       * DESCRIPTION
       *  Wrapper for ending current session email
       *
       * PARAMETERS
       * ==========
       * NAME                          TYPE     DESCRIPTION
       * -----------------             -------- ---------------------------------------------
       * p_conn               IN OUT   SMTP Connection
       *
	   -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
        1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
       *************************************************************************/
   BEGIN
      end_mail_in_session (p_conn);
      end_session (p_conn);
   END end_mail;

   PROCEDURE send_email_attachment (
      p_sender        IN       VARCHAR2,
      p_recipients    IN       VARCHAR2,
      p_subject       IN       VARCHAR2,
      p_message       IN       VARCHAR2,
      p_attachments   IN       VARCHAR2,
      x_result        OUT      VARCHAR2,
      x_result_msg    OUT      VARCHAR2,
      p_directory     IN       VARCHAR2 DEFAULT NULL -- Added for 1.4 
   )
   IS
      /**************************************************************************
      *
      * PROCEDURE
      *  send_email_attachment
      *
      * DESCRIPTION
      *  Wrapper for sending email
      *
      * PARAMETERS
      * ==========
      * NAME                          TYPE     DESCRIPTION
      * -----------------             -------- ---------------------------------------------
      * p_sender             IN                SMTP Connection
      * p_recipients         IN                Email recipient list, separated by "," or ";"
      * p_subject            IN                      Email subject
      * p_message            IN                      Email message text
      * p_attachments        IN                   Full path and filename for attachments, multiple
      *                                                separated by "," or ";"
      * x_result             IN                SUCCESS/FAILURE result
      * x_result_msg         IN                Result message
      * p_directory          IN       VARCHAR2      to pass directory name 
	  -- ----------------------------------------------------------------------------
	VERSION  	VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
         1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
         1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
         1.4    27-APR-2016   Rakesh Patel     Added a new default parameter to email_attachment for TMS # TMS#20151023-00047
      *************************************************************************/
      l_mime_type         VARCHAR2 (50);
      l_conn              UTL_SMTP.connection;
      l_path              VARCHAR2 (200);
      l_filename          VARCHAR2 (80);
      l_extension         VARCHAR2 (10);
      l_attachments       VARCHAR2 (1000);
      l_next_attachment   VARCHAR2 (1000);
      l_directory         VARCHAR2 (50)       := 'XXWC_PDH_MISSING_GL_CODE';
      l_read_file         UTL_FILE.file_type;
      l_raw_data          RAW (32767);
      l_chr_data          VARCHAR (4000);
      l_bfile             BFILE;
      l_amount            BINARY_INTEGER      := g_max_base64_line_width;
      l_offset            INTEGER             := 1;
      l_sender            VARCHAR2 (50);
/***************************************************************/
/* MAIN                            */
/***************************************************************/
   BEGIN
      -- Added for 1.4 < Start 
      IF p_directory IS NOT NULL THEN
         l_directory := p_directory; 
      END IF;     
      -- Added for 1.4 > End

      l_sender := NVL (p_sender, 'ES3');
      l_conn :=
         begin_mail (p_sender          => l_sender,
                     p_recipients      => p_recipients,
                     p_subject         => p_subject,
                     p_mime_type       => g_multipart_mime_type
                    );
      attach_text (p_conn           => l_conn,
                   p_data           => p_message,
                   p_mime_type      => 'text/plain'
                  );
      l_attachments := p_attachments;

                  ---------------------------------
--    get first attachment file name
                  ---------------------------------
      WHILE l_attachments IS NOT NULL
      LOOP
         l_next_attachment := get_address (l_attachments);
                           ---------------------------------
-- determine type from extension
                           ---------------------------------
         l_filename :=
            SUBSTR (l_next_attachment,
                    INSTR (l_next_attachment, '/', -1, 1) + 1,
                      LENGTH (l_next_attachment)
                    - INSTR (l_next_attachment, '/', -1, 1)
                   );
         l_path :=
            SUBSTR (l_next_attachment,
                    1,
                    INSTR (l_next_attachment, '/', -1, 1) - 1
                   );
         l_extension :=
            SUBSTR (l_next_attachment,
                    INSTR (l_next_attachment, '.', 1, 1) + 1,
                      LENGTH (l_next_attachment)
                    - INSTR (l_next_attachment, '.', 1, 1)
                   );

         IF l_extension IN ('xls', 'doc', 'pdf')
         THEN
            IF l_extension = 'xls'
            THEN
               l_mime_type := 'application/excel';
            ELSIF l_extension = 'doc'
            THEN
               l_mime_type := 'application/word';
            ELSIF l_extension = 'pdf'
            THEN
               l_mime_type := 'application/pdf';
            END IF;

            begin_attachment (p_conn              => l_conn,
                              p_mime_type         => l_mime_type,
                              p_inline            => TRUE,
                              p_filename          => l_filename,
                              p_transfer_enc      => 'base64'
                             );

            BEGIN
               l_bfile := BFILENAME (l_directory, l_filename);
               DBMS_LOB.OPEN (l_bfile, DBMS_LOB.file_readonly);

               LOOP
                  DBMS_LOB.READ (l_bfile, l_amount, l_offset, l_raw_data);
                  write_raw
                           (p_conn         => l_conn,
                            p_message      => UTL_ENCODE.base64_encode
                                                                   (l_raw_data)
                           );
                  l_offset := l_offset + l_amount;
               END LOOP;
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_LOB.CLOSE (l_bfile);
            END;

            DBMS_LOB.CLOSE (l_bfile);
            end_attachment (p_conn => l_conn);
         ELSE
            begin_attachment (p_conn              => l_conn,
                              p_mime_type         => 'text/html',
                              p_inline            => TRUE,
                              p_filename          => l_filename,
                              p_transfer_enc      => 'text'
                             );
            l_read_file := UTL_FILE.fopen (l_directory, l_filename, 'R');

            BEGIN
               LOOP
                  UTL_FILE.get_line (l_read_file, l_chr_data);
                  write_text (p_conn         => l_conn,
                              p_message      => l_chr_data || CHR (13)
                             );
               END LOOP;
            EXCEPTION
               WHEN OTHERS
               THEN
                  UTL_FILE.fclose (l_read_file);
            END;

            UTL_FILE.fclose (l_read_file);
            end_attachment (p_conn => l_conn);
         END IF;
      END LOOP;

      end_mail (p_conn => l_conn);
      x_result := 'SUCCESS';
      x_result_msg := NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_result := 'FAILURE';
         x_result_msg := SQLERRM;
   END send_email_attachment;

   ---------------------------------------------
   -- End of code modification w.r.t. ver 1.3 --
   ---------------------------------------------
/**************************************************************************
 File Name:xxhsi_misc_pkg
 PROGRAM TYPE: SQL Script
 PURPOSE: Miscellaneous Packages
 HISTORY
 -- Description   : Called before submitting request to set request attributes
 --
 --    Arguments
 --      p_user  - The requested username.
 --      p_resp  - The requested responsibility
 --
 -- Dependencies Tables        : FND_USER
 --                              FND_RESPONSIBILITY_VL
 -- Dependencies Views         : None
 -- Dependencies Sequences     : None
 -- Dependencies Procedures    : None
 -- Dependencies Functions     : None
 -- Dependencies Packages      : FND_PROFILE
 -- Dependencies Types         : None
 -- Dependencies Database Links: None
 ================================================================
        Last Update Date : 06/06/2010
 ================================================================
 ================================================================
 VERSION DATE          AUTHOR(S)       DESCRIPTION
 ------- -----------   --------------- ---------------------------------
  1.0    06-JUN-2010   Kathy Poling     Creation taken from Edric and Jason
  1.1    02-JAN-2013   Luong Vu         Added function create_ccid_generic
**************************************************************************/
   FUNCTION set_responsibility (
      p_user   IN   VARCHAR2 DEFAULT NULL,
      p_resp   IN   VARCHAR2 DEFAULT NULL
   )
      RETURN BOOLEAN
   IS
      no_such_user            EXCEPTION;
      no_such_resp            EXCEPTION;
      l_responsibility_id     fnd_responsibility_vl.responsibility_id%TYPE
                                                                      := NULL;
      l_resp_application_id   fnd_responsibility_vl.application_id%TYPE
                                                                      := NULL;
      l_user_id               fnd_user.user_id%TYPE                   := NULL;
   BEGIN
      IF (p_user IS NULL)
      THEN
         -- We must have a user so raise an exception until a suitable
         -- default can be found
         RAISE no_such_user;
      ELSE
         BEGIN
            SELECT user_id
              INTO l_user_id
              FROM fnd_user
             WHERE user_name = p_user;
         --            fnd_profile.put ('USER_ID', l_user_id);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               RAISE no_such_user;
            WHEN OTHERS
            THEN
               RAISE;
         END;
      END IF;

      IF (p_resp IS NULL)
      THEN
         -- We must have a user so raise an exception until a
         -- suitable default can be found
         RAISE no_such_resp;
      ELSE
         BEGIN
            SELECT responsibility_id, application_id
              INTO l_responsibility_id, l_resp_application_id
              --              FROM fnd_responsibility -- used only in 10.7 apps
            FROM   fnd_responsibility_vl
             WHERE responsibility_name = p_resp;
         --            fnd_profile.put ('RESP_ID', l_responsibility_id);
         --            fnd_profile.put ('RESP_APPL_ID', l_resp_application_id);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               RAISE no_such_resp;
            WHEN OTHERS
            THEN
               RAISE;
         END;
      END IF;

      apps.fnd_global.apps_initialize (l_user_id,
                                       l_responsibility_id,
                                       l_resp_application_id
                                      );
      RETURN (TRUE);
   EXCEPTION
      WHEN no_such_user
      THEN
         RETURN (FALSE);
      WHEN no_such_resp
      THEN
         RETURN (FALSE);
      WHEN OTHERS
      THEN
         RETURN (FALSE);
   END set_responsibility;

   /**************************************************************************
   -- |----------------------------< html_email >----------------------------|
   -- -----------------------------------------------------------------------------
   --
   -- {Start Of Comments}
   --
   -- Description:
   --   This is the email procedure called by the invidual email programs.
   --   This is a straight copy from ASKTOM.  More info can be referenced :
   --   http://asktom.oracle.com/pls/asktom/f?p=100:11:3585877835963984::::P11_QUESTION_ID:1739411218448
   --
   --   VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------
   -- 1.0     21-Sep-2010   Kathy Poling     Created this procedure by Manny 09-Sep-2009.
   -- 1.5    28-JUL-2017   Nancy Pahwa      Added option to send email to multiple users Task ID: 20170601-00292
   -- **************************************************************************/
   PROCEDURE html_email (
      p_to              IN   VARCHAR2,
      p_from            IN   VARCHAR2,
      p_subject         IN   VARCHAR2,
      p_text            IN   VARCHAR2 DEFAULT NULL,
      p_html            IN   VARCHAR2 DEFAULT NULL,
      p_smtp_hostname   IN   VARCHAR2,
      p_smtp_portnum    IN   VARCHAR2
   )
   IS
      l_boundary     VARCHAR2 (255)      DEFAULT 'a1b2c3d4e3f2g1';
      l_connection   UTL_SMTP.connection;
      l_body_html    CLOB                := EMPTY_CLOB;
      --This will be the email message
      l_offset       NUMBER;
      l_ammount      NUMBER;
      l_temp         VARCHAR2 (32767)    DEFAULT NULL;
      l_cc_email     VARCHAR2(4000); -- v1.5
      l_cntr         NUMBER := 1; --v1.5
   BEGIN
      l_connection :=
                   UTL_SMTP.open_connection (p_smtp_hostname, p_smtp_portnum);
      UTL_SMTP.helo (l_connection, p_smtp_hostname);
      UTL_SMTP.mail (l_connection, p_from);
         -- v1.5 > Start
      for i in (SELECT LEVEL AS id, REGEXP_SUBSTR(p_to, '[^;]+', 1, LEVEL) AS cc_email_name
                FROM dual
                CONNECT BY REGEXP_SUBSTR(p_to, '[^;]+', 1, LEVEL) IS NOT NULL) loop
         IF l_cntr = 1 THEN
           UTL_SMTP.rcpt (l_connection, i.cc_email_name);               
         ELSE
           l_cc_email := l_cc_email||';'|| i.cc_email_name;
           utl_smtp.Rcpt(l_connection,i.cc_email_name);
         END IF;
         l_cntr := l_cntr + 1;
      end loop;
      --UTL_SMTP.rcpt (l_connection, p_to);
      -- v1.5 < End
      
      l_temp := l_temp || 'MIME-Version: 1.0' || CHR (13) || CHR (10);
      l_temp := l_temp || 'To: ' || p_to || CHR (13) || CHR (10);
      l_temp := l_temp || 'From: ' || p_from || CHR (13) || CHR (10);
      l_temp := l_temp || 'Subject: ' || p_subject || CHR (13) || CHR (10);
      l_temp := l_temp || 'Reply-To: ' || p_from || CHR (13) || CHR (10);
      l_temp :=
            l_temp
         || 'Content-Type: multipart/alternative; boundary='
         || CHR (34)
         || l_boundary
         || CHR (34)
         || CHR (13)
         || CHR (10);
----------------------------------------------------
-- Write the headers
      DBMS_LOB.createtemporary (l_body_html, FALSE, 10);
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), 1, l_temp);
----------------------------------------------------
-- Write the text boundary
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      l_temp := '--' || l_boundary || CHR (13) || CHR (10);
      l_temp :=
            l_temp
         || 'content-type: text/plain; charset=us-ascii'
         || CHR (13)
         || CHR (10)
         || CHR (13)
         || CHR (10);
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
----------------------------------------------------
-- Write the plain text portion of the email
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      DBMS_LOB.WRITE (l_body_html, LENGTH (p_text), l_offset, p_text);
----------------------------------------------------
-- Write the HTML boundary
      l_temp :=
            CHR (13)
         || CHR (10)
         || CHR (13)
         || CHR (10)
         || '--'
         || l_boundary
         || CHR (13)
         || CHR (10);
      l_temp :=
            l_temp
         || 'content-type: text/html;'
         || CHR (13)
         || CHR (10)
         || CHR (13)
         || CHR (10);
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
----------------------------------------------------
-- Write the HTML portion of the message
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      DBMS_LOB.WRITE (l_body_html, LENGTH (p_html), l_offset, p_html);
----------------------------------------------------
-- Write the final html boundary
      l_temp := CHR (13) || CHR (10) || '--' || l_boundary || '--' || CHR (13);
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
----------------------------------------------------
-- Send the email in 1900 byte chunks to UTL_SMTP
      l_offset := 1;
      l_ammount := 1900;
      UTL_SMTP.open_data (l_connection);

      WHILE l_offset < DBMS_LOB.getlength (l_body_html)
      LOOP
         UTL_SMTP.write_data (l_connection,
                              DBMS_LOB.SUBSTR (l_body_html,
                                               l_ammount,
                                               l_offset
                                              )
                             );
         l_offset := l_offset + l_ammount;
         l_ammount :=
                     LEAST (1900, DBMS_LOB.getlength (l_body_html) - l_ammount);
      END LOOP;

      UTL_SMTP.close_data (l_connection);
      UTL_SMTP.quit (l_connection);
      DBMS_LOB.freetemporary (l_body_html);
   END html_email;

   /**************************************************************************
   -- |-----------------------< email with attachment>------------------------|
   -- -------------------------------------------------------------------------
   --
   -- {Start Of Comments}
   --
   -- Description:
   --   Procedure to get the Oracle Gl Code Combination.
   --
   --
   --   VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------
   -- 1.0     15-Apr-2011   Kathy Poling     Created this procedure
   --
   -- **************************************************************************/
   FUNCTION email_attachment (
      p_from                   IN   VARCHAR2,
      p_to                     IN   VARCHAR2,
      p_cc                     IN   VARCHAR2,
      p_bcc                    IN   VARCHAR2,
      p_subject                IN   VARCHAR2,
      p_body                   IN   VARCHAR2,
      p_smtp_host              IN   VARCHAR2,
      p_attachment_data        IN   BLOB,
      p_attachment_type        IN   VARCHAR2,
      p_attachment_file_name   IN   VARCHAR2
   )
      RETURN NUMBER
   AS
      LANGUAGE JAVA
      NAME 'mail.send( java.lang.String,
                                  java.lang.String,
                                  java.lang.String,
                                  java.lang.String,
                                  java.lang.String,
                                  java.lang.String,
                                  java.lang.String,
                                  oracle.sql.BLOB,
                                  java.lang.String,
                                  java.lang.String
                                ) return oracle.sql.NUMBER';

   /**************************************************************************
   -- |----------------------------< get_ccid >----------------------------|
   -- -----------------------------------------------------------------------------
   --
   -- {Start Of Comments}
   --
   -- Description:
   --   Procedure to get the Oracle Gl Code Combination.
   --
   --
   --   VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------
   -- 1.0     15-Apr-2011   Kathy Poling     Created this procedure
   --
   -- **************************************************************************/
   FUNCTION get_ccid (p_branch VARCHAR2, p_cost_center VARCHAR2)
      RETURN NUMBER
   IS
      l_ccid          NUMBER;
      l_concat_segs   VARCHAR2 (35);
      l_fru_ovrrd     VARCHAR2 (4);
      l_fru           VARCHAR2 (4);
   BEGIN
      BEGIN
         SELECT iexp_fru_override, fru
           INTO l_fru_ovrrd, l_fru
           FROM apps.xxcus_location_code_vw t
          WHERE t.fru = p_branch AND ROWNUM < 2;

         IF l_fru_ovrrd IS NOT NULL
         THEN
            SELECT f.code_combination_id
              INTO l_ccid
              FROM (SELECT t.entrp_entity, t.entrp_loc,
                           p_cost_center                          --t.entrp_cc
                      FROM apps.xxcus_location_code_vw t
                     WHERE t.fru = l_fru_ovrrd
                                              --AND rpad(nvl(t.lob_dept, 0), 20, '0') =
                                              --    rpad(nvl(p_cost_center, '0'), 20, '0')
                           AND ROWNUM < 2
                                         --ORDER BY --decode(t.bif_interface_flag, 'Y', 2, 1) DESC
                   ) frubranch,
                   gl.gl_code_combinations f
             WHERE f.segment1 = frubranch.entrp_entity
               AND f.segment2 = frubranch.entrp_loc
               --AND f.segment3 = frubranch.entrp_cc
               AND f.segment3 = p_cost_center
               AND f.segment4 = '658020'
               AND f.segment5 = '00000'
               AND f.segment6 = '00000';
         ELSE
            SELECT f.code_combination_id
              INTO l_ccid
              FROM (SELECT t.entrp_entity, t.entrp_loc,
                           p_cost_center                          --t.entrp_cc
                      FROM apps.xxcus_location_code_vw t
                     WHERE t.fru = p_branch
                                           --AND rpad(nvl(t.lob_dept, 0), 20, '0') =
                                           --    rpad(nvl(p_cost_center, '0'), 20, '0')
                           AND ROWNUM < 2
                                         --ORDER BY --decode(t.bif_interface_flag, 'Y', 2, 1) DESC
                   ) frubranch,
                   gl.gl_code_combinations f
             WHERE f.segment1 = frubranch.entrp_entity
               AND f.segment2 = frubranch.entrp_loc
               --AND f.segment3 = frubranch.entrp_cc
               AND f.segment3 = p_cost_center
               AND f.segment4 = '658020'
               AND f.segment5 = '00000'
               AND f.segment6 = '00000';
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            IF UPPER (SUBSTR (p_branch, 1, 1)) = 'Z'
            THEN
               l_ccid := NULL;
            ELSE
               l_ccid := NULL;
            END IF;
      END;

      RETURN l_ccid;
   END;

   /**************************************************************************
   -- |----------------------------< send_mail >----------------------------|
   -- -----------------------------------------------------------------------------
   --
   -- {Start Of Comments}
   --
   -- Description:
   --   Procedure to send mail.
   --
   --
   --   VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------
   -- 1.0     15-Apr-2011   Kathy Poling     Created this procedure
   --
   -- **************************************************************************/
   PROCEDURE send_mail (
      p_sender      IN   VARCHAR2,
      p_recipient   IN   VARCHAR2,
      p_message     IN   VARCHAR2
   )
   AS
      l_mailhost    VARCHAR2 (255)      := 'mailoutrelay.hdsupply.net';
      l_mail_conn   UTL_SMTP.connection;
   BEGIN
      l_mail_conn := UTL_SMTP.open_connection (l_mailhost, 25);
      UTL_SMTP.helo (l_mail_conn, l_mailhost);
      UTL_SMTP.mail (l_mail_conn, p_sender);
      UTL_SMTP.rcpt (l_mail_conn, p_recipient);
      UTL_SMTP.open_data (l_mail_conn);
      UTL_SMTP.write_data (l_mail_conn, p_message);
      UTL_SMTP.close_data (l_mail_conn);
      UTL_SMTP.quit (l_mail_conn);
   END;

   /**************************************************************************
   -- |----------------------------< purge_data >----------------------------|
   -- -----------------------------------------------------------------------------
   --
   -- {Start Of Comments}
   --
   -- Description:
   --   Procedure to purge data and or drop tables based on the retention.
   --
   --
   --   VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------
   -- 1.0     24-June-2011   Kathy Poling     Created this procedure
   --
   -- **************************************************************************/

   --what about interfaces that have a reject status, need another column to key off
   PROCEDURE purge_data (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      --Intialize Variables
      l_sec                  VARCHAR2 (750);
      l_err_msg              VARCHAR2 (2000);
      l_err_code             NUMBER;
      l_group_id             NUMBER;
      l_procedure_name       VARCHAR2 (75)   := 'XXCUS_MISC_PKG.purge_data';
      l_err_callfrom         VARCHAR2 (75)   DEFAULT 'xxcus_pn_conv_pkg';
      l_err_callpoint        VARCHAR2 (75)   DEFAULT 'START';
      l_distro_list          VARCHAR2 (75)
                                          DEFAULT 'kathy.poling@hdsupply.com';
      l_table                VARCHAR (50);
      --Submission variables
      l_globalset            VARCHAR2 (100);
      l_can_submit_request   BOOLEAN         := TRUE;
      l_req_id               NUMBER;
      v_phase                VARCHAR2 (50);
      v_status               VARCHAR2 (50);
      v_dev_status           VARCHAR2 (50);
      v_dev_phase            VARCHAR2 (50);
      v_message              VARCHAR2 (50);
      v_error_message        VARCHAR2 (150);
   --
   BEGIN
      --Create output file
      l_sec := 'Start purging data.';
      fnd_file.put_line (fnd_file.LOG, l_sec);
      fnd_file.put_line (fnd_file.output, l_sec);

      FOR c_purge IN (SELECT lookup_code, meaning, description, tag,
                             attribute1, attribute2, attribute3
                        FROM fnd_lookup_values
                       WHERE lookup_type = 'XXCUS_TABLE_PURGE_RETENTION'
                         AND enabled_flag = 'Y')
      LOOP
         IF     UPPER (c_purge.tag) = 'DROP'
            AND TO_DATE (c_purge.description, 'DD-MON-YYYY') <=
                                                               TRUNC (SYSDATE)
         THEN
            --Create output file
            l_sec := 'Table dropped:  ' || c_purge.meaning;
            fnd_file.put_line (fnd_file.LOG, l_sec);
            fnd_file.put_line (fnd_file.output, l_sec);

            EXECUTE IMMEDIATE 'drop table ' || c_purge.meaning;
         END IF;

         IF c_purge.tag IS NULL
            AND c_purge.meaning = 'XXCUS.XXCUS_TEST_DELETE'
         THEN
            --Create output file
            l_sec := 'Table data being purged:  ' || c_purge.meaning;
            fnd_file.put_line (fnd_file.LOG, l_sec);
            fnd_file.put_line (fnd_file.output, l_sec);

            EXECUTE IMMEDIATE    'delete from '
                              || c_purge.meaning
                              || ' where creation_date < sysdate - '
                              || c_purge.description
                              || ' and '
                              || c_purge.attribute1
                              || c_purge.attribute3
                              || c_purge.attribute2;
         /*
           l_table     :=  'PN.PN_ADDRESSES_ALL';
           delete from xxcus.xxcus_test_delete
                              where table_name =  c_purge.meaning;
         */
         END IF;
      END LOOP;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg :=
               'Error in program ' || l_procedure_name || ' Error: ' || l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => l_procedure_name,
             p_calling                => l_sec,
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SQLERRM,
             p_error_desc             => 'Error running xxcus_misc package to purge data with Program Error Exception',
             p_distribution_list      => l_distro_list,
             p_module                 => 'XXCUS'
            );
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => l_procedure_name,
             p_calling                => l_sec,
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SQLERRM,
             p_error_desc             => 'Error running xxcus_misc package to purge data with OTHERS Exception',
             p_distribution_list      => l_distro_list,
             p_module                 => 'XXCUS'
            );
   END purge_data;

   FUNCTION get_description (p_proc_name VARCHAR2, p_parm_name VARCHAR2)
      RETURN VARCHAR2
   IS
-- -----------------------------------------------------------------------------
-- |----------------------------< get_description >------------------------|
-- -----------------------------------------------------------------------------
--   Gets values from fnd_lookup_values table
--
--   VERSION DATE          AUTHOR(S)       DESCRIPTION
--  ------- -----------   --------------- ---------------------------------
--  1.0     07-JUN-2011   Luong Vu         Created this procedure.

      -- Intitalize
      l_value   fnd_lookup_values.description%TYPE   DEFAULT NULL;
   BEGIN
      BEGIN
         SELECT description
           INTO l_value
           FROM fnd_lookup_values
          WHERE lookup_type = p_proc_name AND meaning = p_parm_name;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            fnd_file.put_line
                     (fnd_file.output,
                      'Cannot find valid values in the xxhsi_parmeters table'
                     );
      END;

      RETURN l_value;
   END;

   /**************************************************************************
   -- |----------------------------< get_create_ccid >----------------------------|
   -- -----------------------------------------------------------------------------
   --
   -- {Start Of Comments}
   --
   -- Description:
   --   Procedure to get the Oracle Gl Code Combination or create as needed.
   --
   --
   --   VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------
   -- 1.0     03-Aug-2011   Kathy Poling     Created this procedure
   -- 1.1     06-Mar-2014   Kathy Poling     Added exceptions to return -1
   --                                        added ledger id for Canada iExpense
   --                                        SR 133025
   -- **************************************************************************/
   FUNCTION get_ccid_create (p_branch VARCHAR2, p_cost_center VARCHAR2)
      RETURN NUMBER
   IS
      l_status        BOOLEAN;
      l_coa_id        NUMBER;
      l_ccid          NUMBER;
      l_concat_segs   VARCHAR2 (80);
      l_fru_ovrrd     VARCHAR2 (4);
      l_fru           VARCHAR2 (4);
      l_ledger_id     NUMBER;
   BEGIN
      BEGIN
         SELECT iexp_fru_override, fru
           INTO l_fru_ovrrd, l_fru
           FROM apps.xxcus_location_code_vw t
          WHERE t.fru = p_branch AND ROWNUM < 2;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN -1;
      END;

      IF l_fru_ovrrd IS NOT NULL
      THEN
         BEGIN
            SELECT    t.entrp_entity
                   || '.'
                   || t.entrp_loc
                   || '.'
                   || p_cost_center
                   || '.658020.00000.00000.00000',
                   TO_NUMBER (attribute1)
              INTO l_concat_segs,
                   l_ledger_id
              FROM apps.xxcus_location_code_vw t,
                   apps.fnd_flex_values_vl l                     --Version 1.1
             WHERE t.fru = l_fru_ovrrd
               --AND rpad(nvl(t.lob_dept, 0), 6, '0') = rpad(nvl(p_cost_center, '0'), 6, '0')
               AND ROWNUM < 2
               AND t.entrp_entity = l.flex_value
               AND l.value_category = 'XXCUS_GL_PRODUCT';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               RETURN -1;
         END;
      ELSE
         BEGIN
            SELECT    t.entrp_entity
                   || '.'
                   || t.entrp_loc
                   || '.'
                   || p_cost_center
                   || '.658020.00000.00000.00000',
                   TO_NUMBER (attribute1)
              INTO l_concat_segs,
                   l_ledger_id
              FROM apps.xxcus_location_code_vw t,
                   apps.fnd_flex_values_vl l                     --Version 1.1
             WHERE t.fru = p_branch
               --AND rpad(nvl(t.lob_dept, 0), 6, '0') = rpad(nvl(p_cost_center, '0'), 6, '0')
               AND ROWNUM < 2
               AND t.entrp_entity = l.flex_value
               AND l.value_category = 'XXCUS_GL_PRODUCT';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               RETURN -1;
         END;
      END IF;

      /* EXCEPTION
      WHEN no_data_found THEN
        SELECT t.entrp_entity || '.' || t.entrp_loc || '.' || t.entrp_cc ||
               '.658020.00000.00000.00000'
          INTO l_concat_segs
          FROM apps.xxcus_location_code_vw t
         WHERE t.fru = p_branch
           AND rpad(nvl(t.lob_dept, 0), 6, '0') = '000000'
           AND rownum < 2;   */
      BEGIN
         SELECT chart_of_accounts_id
           INTO l_coa_id
           FROM gl_sets_of_books
          WHERE set_of_books_id = l_ledger_id;
      --Version 1.2 removed hard coding
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN -1;
      END;

      BEGIN
         l_status :=
            fnd_flex_keyval.validate_segs ('CREATE_COMBINATION'    --operation
                                                               ,
                                           'SQLGL'
                                                  --appl_short_name
            ,
                                           'GL#'
                                                --key_flex_code
            ,
                                           l_coa_id
                                                   --structure_number
            ,
                                           l_concat_segs
                                                        --concat_segments
            ,
                                           'V'
                                              --values_or_ids
            ,
                                           SYSDATE
                                                  --validation_date
            ,
                                           'ALL'
                                                --displayable
            ,
                                           NULL
                                               --data_set
            ,
                                           NULL
                                               --vrule
            ,
                                           NULL
                                               --where_clause
            ,
                                           NULL
                                               --get_columns
            ,
                                           FALSE
                                                --allow_nulls
            ,
                                           FALSE
                                                --allow_orphans
            ,
                                           NULL
                                               --allow_orphans
            ,
                                           NULL
                                               --resp_id
            ,
                                           NULL
                                               --user_id
            ,
                                           NULL
                                               --select_comb_from_view
            ,
                                           NULL
                                               --no_combmsg
            ,
                                           NULL
                                          --where_clause_msg
                                          );

         IF l_status
         THEN
            l_ccid := fnd_flex_keyval.combination_id ();
            RETURN l_ccid;
         ELSE
            RETURN -1;                                          --Version 1.1
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            /*     IF upper(substr(p_branch, 1, 1)) = 'Z'
            THEN
              l_ccid := NULL;
            ELSE
              l_ccid := NULL;
            END IF;
            */
            fnd_file.put_line (fnd_file.output,
                               'Can not create ccid for:  ' || l_concat_segs
                              );
            RETURN -1;
      END;
   END get_ccid_create;

   /**************************************************************************
   -- |----------------------------< HR Assignment Data Fix -----------------|
   -- ------------------------------------------------------------------------
   --
   -- {Start Of Comments}
   --
   -- Description:
   --   Procedure for data fix in HR Assignment detail of terminated employee
   --
   --
   --   VERSION       DATE          AUTHOR(S)       DESCRIPTION
   --   -------    -----------   --------------- -----------------------------
   --     1.0      04/27/2012    Luong Vu        Created this procedure
   --
   -- ***********************************************************************/
   PROCEDURE hr_assign_detail_fix (
      errbuf      OUT      VARCHAR2,
      retcode     OUT      NUMBER,
      p_emp_num   IN       VARCHAR2
   )
   IS
      --Intialize Variables
      l_sec              VARCHAR2 (750);
      l_err_msg          VARCHAR2 (2000);
      l_err_code         NUMBER;
      l_person_id        NUMBER;
      l_procedure_name   VARCHAR2 (75)
                                     := 'XXCUS_MISC_PKG.hr_assign_detail_fix';
      l_distro_list      VARCHAR2 (75)
                                   DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   --l_distro_list VARCHAR2(75) DEFAULT 'luong.vu@hdsupply.com';
   BEGIN
      l_sec := 'Get person id for employee';

      SELECT person_id
        INTO l_person_id
        FROM hr.per_all_people_f
       WHERE employee_number = p_emp_num
             AND effective_end_date = '31-Dec-4712';

      l_sec := 'Data fix';

      DELETE FROM hr.per_all_assignments_f
            WHERE person_id = l_person_id AND assignment_status_type_id = 3;

      COMMIT;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg :=
               'Error in program ' || l_procedure_name || ' Error: ' || l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => l_procedure_name,
             p_calling                => l_sec,
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SQLERRM,
             p_error_desc             => 'Error running xxcus_misc package for employee assignment data fix with Program Error Exception',
             p_distribution_list      => l_distro_list,
             p_module                 => 'XXCUS'
            );
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => l_procedure_name,
             p_calling                => l_sec,
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SQLERRM,
             p_error_desc             => 'Error running xxcus_misc package for employee assignment data fix with Program Error Exception',
             p_distribution_list      => l_distro_list,
             p_module                 => 'XXCUS'
            );
   END hr_assign_detail_fix;

   /**************************************************************************
   -- |----------------------------< create_ccid_generic >--------------------|
   -- ------------------------------------------------------------------------
   --
   -- {Start Of Comments}
   --
   -- Description:
   --   A generic function to generate Gl Code Combination as needed.
   --
   --   VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------
   -- 1.0     02-Jan-2013   Luong Vu        Created this procedure
   --
   -- **************************************************************************/
   FUNCTION create_ccid_generic (
      p_entrp_entity   VARCHAR2,
      p_entrp_loc      VARCHAR2,
      p_entpr_cc       VARCHAR2,
      p_account        VARCHAR2,
      p_segment5       VARCHAR2,
      p_segment6       VARCHAR2,
      p_segment7       VARCHAR2
   )
      RETURN NUMBER
   IS
      l_status        BOOLEAN;
      l_coa_id        NUMBER;
      l_ccid          NUMBER;
      l_concat_segs   VARCHAR2 (80);
   BEGIN
      l_concat_segs :=
            p_entrp_entity
         || '.'
         || p_entrp_loc
         || '.'
         || p_entpr_cc
         || '.'
         || p_account
         || '.'
         || p_segment5
         || '.'
         || p_segment6
         || '.'
         || p_segment7;

      BEGIN
         SELECT chart_of_accounts_id
           INTO l_coa_id
           FROM gl_sets_of_books
          WHERE set_of_books_id = 2061;

         l_status :=
            fnd_flex_keyval.validate_segs ('CREATE_COMBINATION'    --operation
                                                               ,
                                           'SQLGL'
                                                  --appl_short_name
            ,
                                           'GL#'
                                                --key_flex_code
            ,
                                           l_coa_id
                                                   --structure_number
            ,
                                           l_concat_segs
                                                        --concat_segments
            ,
                                           'V'
                                              --values_or_ids
            ,
                                           SYSDATE
                                                  --validation_date
            ,
                                           'ALL'
                                                --displayable
            ,
                                           NULL
                                               --data_set
            ,
                                           NULL
                                               --vrule
            ,
                                           NULL
                                               --where_clause
            ,
                                           NULL
                                               --get_columns
            ,
                                           FALSE
                                                --allow_nulls
            ,
                                           FALSE
                                                --allow_orphans
            ,
                                           NULL
                                               --allow_orphans
            ,
                                           NULL
                                               --resp_id
            ,
                                           NULL
                                               --user_id
            ,
                                           NULL
                                               --select_comb_from_view
            ,
                                           NULL
                                               --no_combmsg
            ,
                                           NULL
                                          --where_clause_msg
                                          );

         IF l_status
         THEN
            l_ccid := fnd_flex_keyval.combination_id ();
            RETURN l_ccid;
         ELSE
            RETURN 0;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            fnd_file.put_line (fnd_file.output,
                               'Can not create ccid for:  ' || l_concat_segs
                              );
      END;
   END create_ccid_generic;

   /**************************************************************************
   -- |----------------------------< sleep >----------------------------|
   -- ------------------------------------------------------------------------
   -- Description:
   --   Enabling interface user to use sys.dbms_lock without grant
   --
   --   VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------
   -- 1.0     24-Apr-2013   Luong Vu        Created this procedure
   --
   -- ***********************************************************************/
   PROCEDURE sleep (seconds IN NUMBER)
   IS
   BEGIN
      DBMS_LOCK.sleep (seconds);
   END;

   /******************************************************************************
   -- |-----------------------< uc4_xx_iface_purge_files >------------------------|
   -- -----------------------------------------------------------------------------
   --
   -- {Start Of Comments}
   --
   -- Description:
   --   Procedure to delete files from xx_iface directories using the lookup
   --             XXCUS_INTERFACE_ARCHIVE_DAYS.  UC4 will call this procedure
   --             to run the concurrent request XXCUS XX_IFACE DELETE FILES
   --
   --   VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------
   -- 1.0     09-Apr-2014   Kathy Poling     Created this procedure SR 475294
   --
   --
   -- **************************************************************************/
   PROCEDURE uc4_xx_iface_purge_files (
      errbuf                  OUT      VARCHAR2,
      retcode                 OUT      NUMBER,
      p_user_name             IN       VARCHAR2,
      p_responsibility_name   IN       VARCHAR2,
      p_intf_name             IN       VARCHAR2,
      p_dir_path              IN       VARCHAR2,
      p_retention_days        IN       VARCHAR2,
      p_file_prefix           IN       VARCHAR2
   )
   IS
      --
      -- Package Variables
      --
      l_req_id               NUMBER                  NULL;
      v_phase                VARCHAR2 (50);
      v_status               VARCHAR2 (50);
      v_dev_status           VARCHAR2 (50);
      v_dev_phase            VARCHAR2 (50);
      v_message              VARCHAR2 (250);
      v_error_message        VARCHAR2 (3000);
      v_interval             NUMBER                       := 30;
      -- In seconds
      v_max_time             NUMBER                       := 7200;
      -- In seconds
      l_can_submit_request   BOOLEAN                      := TRUE;
      l_err_msg              VARCHAR2 (3000);
      l_sec                  VARCHAR2 (255);
      l_user_id              fnd_user.user_id%TYPE;
      l_procedure            VARCHAR2 (50)      := 'uc4_xx_iface_purge_files';
      l_application_name     VARCHAR2 (30)                := 'XXCUS';
      l_program_short_name   VARCHAR2 (240)     := 'XXCUS_IFACE_DELETE_FILES';
      l_file_prefix          VARCHAR (150);
   BEGIN
      retcode := 0;

      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = p_user_name;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_sec := 'UserName ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_sec := 'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      --  Setup parameters for running FND JOBS!
      l_can_submit_request :=
         xxcus_misc_pkg.set_responsibility (p_user_name,
                                            p_responsibility_name);

      IF l_can_submit_request
      THEN
         l_sec := 'Global Variables are set.';
      ELSE
         l_sec :=
                'Global Variables are not set for the ' || p_user_name || '.';
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         RAISE PROGRAM_ERROR;
      END IF;

      IF p_file_prefix = '*'
      THEN
         l_file_prefix := NULL;
      ELSE
         l_file_prefix := p_file_prefix;
      END IF;

      l_sec :=
         'UC4 call to run concurrent request XXCUS XX_IFACE DELETE FILES Program.';
      l_req_id :=
         fnd_request.submit_request (application      => l_application_name,
                                     program          => l_program_short_name,
                                     description      => NULL,
                                     start_time       => SYSDATE,
                                     sub_request      => FALSE,
                                     argument1        => p_intf_name,
                                     argument2        => p_dir_path,
                                     argument3        => p_retention_days,
                                     argument4        => l_file_prefix
                                    );
      COMMIT;

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id,
                                             v_interval,
                                             v_max_time,
                                             v_phase,
                                             v_status,
                                             v_dev_phase,
                                             v_dev_status,
                                             v_message
                                            )
         THEN
            v_error_message :=
                  CHR (10)
               || 'ReqID='
               || l_req_id
               || ' DPhase '
               || v_dev_phase
               || ' DStatus '
               || v_dev_status
               || CHR (10)
               || ' MSG - '
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_sec :=
                     'An error occured in the running of the XXCUS XX_IFACE DELETE FILES Program'
                  || v_error_message
                  || '.';
               fnd_file.put_line (fnd_file.LOG, l_sec);
               fnd_file.put_line (fnd_file.output, l_sec);
               RAISE PROGRAM_ERROR;
            END IF;
         -- Then Success!
         ELSE
            l_sec :=
                  'EBS timed out waiting on XXCUS XX_IFACE DELETE FILES Program'
               || v_error_message
               || '.';
            fnd_file.put_line (fnd_file.LOG, l_sec);
            fnd_file.put_line (fnd_file.output, l_sec);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_sec :=
            'An error occured when trying to submit XXCUS XX_IFACE DELETE FILES Program';
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         RAISE PROGRAM_ERROR;
      END IF;

      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
      DBMS_OUTPUT.put_line ('Responsibility Name:  ' || p_responsibility_name);
      DBMS_OUTPUT.put_line ('User Name:  ' || p_user_name);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         l_err_msg :=
               l_sec
            || ' Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (l_err_msg);
         DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
         DBMS_OUTPUT.put_line ('retcode:  ' || retcode);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => g_err_callfrom || '.' || l_procedure,
             p_calling                => l_sec,
             p_request_id             => l_req_id,
             p_ora_error_msg          => SUBSTR
                                              (   SQLERRM
                                               || REGEXP_REPLACE
                                                               (l_err_msg,
                                                                '[[:cntrl:]]',
                                                                NULL
                                                               ),
                                               1,
                                               2000
                                              ),
             p_error_desc             => 'Error running XXCUS_IFACE_DELETE_FILES with Program Error Exception',
             p_distribution_list      => g_distro_list,
             p_module                 => 'AR'
            );
         retcode := 2;
         errbuf := l_err_msg;
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_sec
            || ' Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (l_err_msg);
         DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
         DBMS_OUTPUT.put_line ('retcode:  ' || retcode);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => g_err_callfrom || '.' || l_procedure,
             p_calling                => l_sec,
             p_request_id             => l_req_id,
             p_ora_error_msg          => SUBSTR
                                              (   SQLERRM
                                               || REGEXP_REPLACE
                                                               (l_err_msg,
                                                                '[[:cntrl:]]',
                                                                NULL
                                                               ),
                                               1,
                                               2000
                                              ),
             p_error_desc             => 'Error running XXCUS_IFACE_DELETE_FILES with Exception',
             p_distribution_list      => g_distro_list,
             p_module                 => 'XXIFACE'
            );
         retcode := 2;
         errbuf := l_err_msg;
   END uc4_xx_iface_purge_files;
   
END xxcus_misc_pkg;
/