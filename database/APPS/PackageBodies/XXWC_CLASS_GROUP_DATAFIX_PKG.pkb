CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CLASS_GROUP_DATAFIX_PKG AS
  /********************************************************************************
  FILE NAME: XXWC_CLASS_GROUP_DATAFIX_PKG.pkb
  
  PROGRAM TYPE: PL/SQL Package Body.
  
  PURPOSE: Data fix script for Inserting the new ABC Class and ABC Group for all Inventory Orgs.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     22/12/2017    Ashwin Sridhar  Initial creation 
  2.0     05/04/2018    Rakesh Patel    TMS#20180430-00073-cycle count items are continuously being autoscheduled
  ********************************************************************************/
  
pl_dflt_email fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';
  
PROCEDURE insert_class_group_p(p_errbuf          OUT VARCHAR2
                              ,p_retcode         OUT NUMBER
							  ,p_organization_id IN  NUMBER
                              ) IS

CURSOR cu_inv_org IS							  
SELECT organization_id
FROM   MTL_PARAMETERS
WHERE  master_organization_id =222
AND    organization_id=NVL(p_organization_id,organization_id);

CURSOR cu_group_class IS
SELECT MAC.ABC_CLASS_ID
,      MAC.ORGANIZATION_ID
,      MAAG.assignment_group_id
FROM   MTL_ABC_CLASSES             MAC
,      MTL_ABC_ASSIGNMENT_GROUPS   MAAG
--,      MTL_ABC_ASSGN_GROUP_CLASSES MAAGC --Rev#2.0
WHERE  1=1
AND    MAC.ORGANIZATION_ID = MAAG.ORGANIZATION_ID
--AND    MAAG.ASSIGNMENT_GROUP_ID=MAAGC.ASSIGNMENT_GROUP_ID --Rev#2.0
--AND    MAC.ABC_CLASS_ID(+)=MAAGC.ABC_CLASS_ID--Rev#2.0
AND    MAC.ORGANIZATION_ID = NVL(p_organization_id,MAC.organization_id)
AND    MAC.abc_class_name IN ('A','B','C','E');

--Local Variable Declaration Section...
lv_procedure_name VARCHAR2(50) := 'XXWC_CLASS_GROUP_DATAFIX_PKG.insert_class_group_p';
lv_sec            VARCHAR2(250);
ln_class_seq      NUMBER;
ln_user_id        NUMBER:=fnd_profile.value('USER_ID');
lv_check          VARCHAR2(2);
lv_group_check    VARCHAR2(2);
ln_count          NUMBER:=0;
ln_max_seq        NUMBER;
ln_seq            NUMBER;
							  
BEGIN

  fnd_file.put_line(fnd_file.log, 'Program Begin...');
  fnd_file.put_line(fnd_file.log, 'Starting the Loop for ABC Class...');
  
  FOR rec_inv_org IN cu_inv_org LOOP
  
    fnd_file.put_line(fnd_file.log, 'Organization ID...'||rec_inv_org.organization_id);
  
    lv_check:=check_class_f(rec_inv_org.organization_id 
                           ,'A');
  
    IF lv_check='N' THEN
  
      ln_count:=ln_count+1;
  
      ln_class_seq:=MTL_ABC_CLASSES_S.NEXTVAL;
	  
	  INSERT INTO MTL_ABC_CLASSES(ABC_CLASS_ID    
                                 ,ABC_CLASS_NAME  
                                 ,ORGANIZATION_ID 
                                 ,LAST_UPDATE_DATE
                                 ,LAST_UPDATED_BY 
                                 ,CREATION_DATE   
                                 ,CREATED_BY      
                                 ,DESCRIPTION     
                                 )
	  VALUES
	  (ln_class_seq       
      ,'A' 
      ,rec_inv_org.ORGANIZATION_ID       
      ,SYSDATE         
      ,ln_user_id      
      ,SYSDATE          
      ,ln_user_id      
      ,'Items to be counted once a quarter'
      );	  
	  
	END IF;
	
	lv_check:=check_class_f(rec_inv_org.organization_id 
                           ,'B');
						   
	IF lv_check='N' THEN
	
	  ln_count:=ln_count+1;
  
      ln_class_seq:=MTL_ABC_CLASSES_S.NEXTVAL;
	
	  INSERT INTO MTL_ABC_CLASSES(ABC_CLASS_ID    
                                 ,ABC_CLASS_NAME  
                                 ,ORGANIZATION_ID 
                                 ,LAST_UPDATE_DATE
                                 ,LAST_UPDATED_BY 
                                 ,CREATION_DATE   
                                 ,CREATED_BY      
                                 ,DESCRIPTION     
                                 )
	  VALUES
	  (ln_class_seq       
      ,'B' 
      ,rec_inv_org.ORGANIZATION_ID       
      ,SYSDATE         
      ,ln_user_id      
      ,SYSDATE          
      ,ln_user_id      
      ,'Items to be counted once a year'
      );
	  
	END IF;
	
	lv_check:=check_class_f(rec_inv_org.organization_id 
                           ,'C');
						   
	IF lv_check='N' THEN
	
	  ln_count:=ln_count+1;
  
      ln_class_seq:=MTL_ABC_CLASSES_S.NEXTVAL;
	  
	  INSERT INTO MTL_ABC_CLASSES(ABC_CLASS_ID    
                                 ,ABC_CLASS_NAME  
                                 ,ORGANIZATION_ID 
                                 ,LAST_UPDATE_DATE
                                 ,LAST_UPDATED_BY 
                                 ,CREATION_DATE   
                                 ,CREATED_BY      
                                 ,DESCRIPTION     
                                 )
	  VALUES
	   (ln_class_seq       
      ,'C' 
      ,rec_inv_org.ORGANIZATION_ID       
      ,SYSDATE         
      ,ln_user_id      
      ,SYSDATE          
      ,ln_user_id      
      ,'Items to be counted twice a year'
      );
  
    END IF;
	
    lv_check:=check_class_f(rec_inv_org.organization_id 
                           ,'E');
  
    IF lv_check='N' THEN
  
      ln_count:=ln_count+1;
  
      ln_class_seq:=MTL_ABC_CLASSES_S.NEXTVAL;
	  
	  INSERT INTO MTL_ABC_CLASSES(ABC_CLASS_ID    
                                 ,ABC_CLASS_NAME  
                                 ,ORGANIZATION_ID 
                                 ,LAST_UPDATE_DATE
                                 ,LAST_UPDATED_BY 
                                 ,CREATION_DATE   
                                 ,CREATED_BY      
                                 ,DESCRIPTION     
                                 )
	  VALUES
	  (ln_class_seq       
      ,'E' 
      ,rec_inv_org.ORGANIZATION_ID       
      ,SYSDATE         
      ,ln_user_id      
      ,SYSDATE          
      ,ln_user_id      
      ,'Exceptions not to be autoscheduled'
      );	  
	  
	END IF;
  
  END LOOP;

  COMMIT;
  
  fnd_file.put_line(fnd_file.log, 'Record Inserted Successfully for Class with count...'||ln_count);
  
  fnd_file.put_line(fnd_file.log, 'Starting the Loop for ABC Group Class...');
  
  ln_count:=0;
  
  FOR rec_group_class IN cu_group_class LOOP
  
    fnd_file.put_line(fnd_file.log, 'Assignment Group ID...'||rec_group_class.ASSIGNMENT_GROUP_ID);
	fnd_file.put_line(fnd_file.log, 'ABC Class ID...'||rec_group_class.ABC_CLASS_ID);
  
    lv_group_check:=check_group_class_f(rec_group_class.ASSIGNMENT_GROUP_ID 
                                       ,rec_group_class.ABC_CLASS_ID);
									   
	SELECT MAX(sequence_number)
	INTO   ln_max_seq
    FROM   MTL_ABC_ASSGN_GROUP_CLASSES
    WHERE  assignment_group_id=rec_group_class.ASSIGNMENT_GROUP_ID;
	
	fnd_file.put_line(fnd_file.log, 'Max Seq...'||ln_max_seq);
  
    IF lv_group_check='N' THEN
	
	  ln_seq:=NVL(ln_max_seq, 0)+1; --Rev# 2.0
	  ln_count:=ln_count+1;
  
      INSERT INTO MTL_ABC_ASSGN_GROUP_CLASSES
      (ASSIGNMENT_GROUP_ID
      ,ABC_CLASS_ID       
      ,SEQUENCE_NUMBER    
      ,LAST_UPDATE_DATE   
      ,LAST_UPDATED_BY    
      ,CREATION_DATE      
      ,CREATED_BY         
      ,ITEM_SEQ_NUMBER
      ) 
      VALUES
      (rec_group_class.ASSIGNMENT_GROUP_ID        
      ,rec_group_class.ABC_CLASS_ID       
      ,ln_seq        
      ,SYSDATE         
      ,ln_user_id        
      ,SYSDATE          
      ,ln_user_id        
      ,ln_seq
      );
	
	END IF;
	
  END LOOP;
  
  fnd_file.put_line(fnd_file.log, 'Record Inserted Successfully for Group Class with count...'||ln_count);
  
EXCEPTION
WHEN others THEN

  p_retcode:=2;
  p_errbuf:='Inside Exception '||SQLERRM;
  lv_sec:=lv_sec||'Inside Exception ';
  fnd_file.put_line(fnd_file.log, p_errbuf);
  
  xxcus_error_pkg.xxcus_error_main_api(p_called_from       => lv_procedure_name
                                      ,p_calling           => lv_sec
                                      ,p_request_id        => fnd_global.conc_request_id
                                      ,p_ora_error_msg     => p_errbuf
                                      ,p_error_desc        => 'Error running XXWC Class Group Datfix pkg...'
                                      ,p_distribution_list => pl_dflt_email
                                      ,p_module            => 'XXWC');
									  
END insert_class_group_p;

FUNCTION check_class_f(p_organization_id IN NUMBER
                      ,p_class_name      IN VARCHAR2) RETURN VARCHAR2 IS
	
ln_count NUMBER;
	
BEGIN

  SELECT COUNT(1)
  INTO   ln_count
  FROM   MTL_ABC_CLASSES
  WHERE  organization_id=p_organization_id
  AND    abc_class_name =p_class_name;

  IF ln_count=0 THEN
  
    RETURN 'N';
	
  ELSE
  
    RETURN 'Y';
  
  END IF;
  
EXCEPTION
WHEN others THEN

  RETURN 'N';
  
END check_class_f;

FUNCTION check_group_class_f(p_assignment_group_id IN NUMBER
                            ,p_abc_class_id        IN NUMBER) RETURN VARCHAR2 IS
		
ln_count NUMBER;
		
BEGIN

  SELECT COUNT(1)
  INTO   ln_count
  FROM   MTL_ABC_ASSGN_GROUP_CLASSES
  WHERE  assignment_group_id=p_assignment_group_id
  AND    abc_class_id       =p_abc_class_id;

  IF ln_count=0 THEN
  
    RETURN 'N';
	
  ELSE
  
    RETURN 'Y';
  
  END IF;

EXCEPTION
WHEN others THEN

  RETURN 'N';
  
END;
	
END XXWC_CLASS_GROUP_DATAFIX_PKG;
/