CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_PERSONALIZE_RTNS_PKG
AS
   /*************************************************************************
       *   Procedure : write_log
       *
       *   PURPOSE:   This procedure prints the string in log file.
       *  Author: Ram Talluri- TMS #20131212-00053 
       * Creation_date 12/12/2013
       * Last update Date 12/12/2013
       *   Parameter:
       *          IN p_debug_msg
       *
       * ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   /*************************************************************************
        *   Procedure : write_error
        *
        *   PURPOSE:   This procedure reports the error.
        *  Author: Ram Talluri- TMS #20131212-00053 
        * Creation_date 12/12/2013
        * Last update Date 12/12/2013
        *   Parameter:
        *          IN p_debug_msg
        *
        * ************************************************************************/
   PROCEDURE write_error (p_debug_msg IN VARCHAR2, p_call_point IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_QP_ROUTINES_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_QP_ROUTINES_PKG with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');
   END write_error;

   /*************************************************************************
       *   Function : is_row_locked
       *
       *   PURPOSE:   This function identifies if a record has ben locked by user
       *  Author: Ram Talluri- TMS #20131212-00053 
       * Creation_date 12/12/2013
       * Last update Date 12/12/2013
       *   Parameter:
       *          IN rowid, table_name
       *
       * ************************************************************************/
   FUNCTION is_row_locked (v_rowid ROWID, table_name VARCHAR2)
      RETURN VARCHAR2
   IS
      x   NUMBER;
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      EXECUTE IMMEDIATE
            'Begin
                           Select 1 into :x from '
         || table_name
         || ' where rowid =:v_rowid for update nowait;
                         Exception
                            When Others Then
                              :x:=null;
                         End;'
         USING OUT x, v_rowid;

      -- now release the lock if we got it.
      ROLLBACK;

      IF x = 1
      THEN
         RETURN 'N';
      ELSIF x IS NULL
      THEN
         RETURN 'Y';
      END IF;
   END;

   /*************************************************************************
        *   Procedure : XXWC_OM_RENTAL_PROCESSING
        *
        *   PURPOSE:   This procedure is used in personalization to ship rental items on internal orders..
        *  Author: Ram Talluri- TMS #20131212-00053 
        * Creation_date 12/12/2013
        * Last update Date 12/12/2013
        *   Parameter:
        *          IN p_debug_msg
        *
        * ************************************************************************/
   
   PROCEDURE XXWC_OM_RENTAL_PROCESSING(
      p_header_id        IN   NUMBER,
      p_user_id        IN   NUMBER,
      p_resp_id        IN   NUMBER,
      p_resp_appl_id   IN   NUMBER,
      p_user_name      IN   VARCHAR2
   ) IS
   
   CURSOR cur_pick_orgs
      IS
         SELECT DISTINCT (ooha.header_id) header_id, oola.ship_from_org_id,
                         ooha.source_document_type_id, order_type_id
                    FROM apps.oe_order_headers ooha, 
                         apps.oe_order_lines oola,
                         oe_transaction_types_tl ott
                   WHERE ooha.header_id = p_header_id
                     AND NVL (ooha.booked_flag, 'N') = 'Y'
                     AND NVL (ooha.cancelled_flag, 'N') = 'N'
                     AND ooha.header_id = oola.header_id
                     AND NVL (oola.cancelled_flag, 'N') = 'N'
                     AND oola.line_category_code = 'ORDER'
                     AND oola.ORDERED_ITEM LIKE 'R%'
                     AND oola.SUBINVENTORY='Rental'
                     AND oola.flow_status_code='AWAITING_SHIPPING'
                     AND oola.line_type_id = ott.transaction_type_id
                     AND ott.name = 'INTERNAL LINE'
                     AND oola.ship_from_org_id=APPS.XXWC_OM_PERSONALIZE_RTNS_PKG.XXWC_GET_USER_PROF_VAL(p_user_name);

      l_return_status      VARCHAR2 (1);
      l_msg_count          NUMBER (15);
      l_msg_data           VARCHAR2 (2000);
      p_new_batch_id       NUMBER;
      l_rule_id            NUMBER;
      l_rule_name          VARCHAR2 (2000);
      l_batch_prefix       VARCHAR2 (2000);
      l_batch_info_rec     wsh_picking_batches_pub.batch_info_rec;
      l_request_id         NUMBER;
      v_phase              VARCHAR2 (50);
      v_status             VARCHAR2 (50);
      v_dev_status         VARCHAR2 (50);
      v_dev_phase          VARCHAR2 (50);
      v_message            VARCHAR2 (250);
      v_error_message      VARCHAR2 (3000);
      v_wait               BOOLEAN;
      l_pick_rule_string   VARCHAR2 (50);
      x_ret_batch_id       NUMBER;
      x_ret_request_id     NUMBER;
      v_call_point         VARCHAR2(10);
   BEGIN
      
      fnd_global.apps_initialize (p_user_id, p_resp_id, p_resp_appl_id);
      mo_global.init ('ONT');

      FOR c1 IN cur_pick_orgs
      LOOP
         EXIT WHEN cur_pick_orgs%NOTFOUND;

         l_pick_rule_string := 'Unreleased Rent';
         
         BEGIN
            SELECT wpr.picking_rule_id, wpr.NAME
              INTO l_rule_id, l_rule_name
              FROM wsh_picking_rules wpr
             WHERE wpr.organization_id = c1.ship_from_org_id
               AND wpr.NAME LIKE '%' || l_pick_rule_string || '%'
               AND TRUNC (wpr.start_date_active) <= TRUNC (SYSDATE)
               AND NVL (TRUNC (wpr.end_date_active), TRUNC (SYSDATE + 1)) > TRUNC (SYSDATE)
               AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_rule_id := NULL;
               l_rule_name := NULL;
               v_call_point:='STAGE1';
         END;

         IF l_rule_id IS NOT NULL
         THEN
            
            l_batch_info_rec.order_header_id := c1.header_id;
            l_batch_info_rec.organization_id := c1.ship_from_org_id;
            l_batch_info_rec.order_type_id := c1.order_type_id;
            l_batch_info_rec.document_set_id := NULL;
            l_batch_info_rec.document_set_name := NULL;
            l_batch_prefix := NULL;
            
            wsh_picking_batches_pub.create_batch
                                         (p_api_version        => 1.0,
                                          p_init_msg_list      => 'T', -- fnd_api.g_true,
                                          p_commit             => 'T', -- fnd_api.g_true,
                                          x_return_status      => l_return_status,
                                          x_msg_count          => l_msg_count,
                                          x_msg_data           => l_msg_data,
                                          p_rule_id            => l_rule_id,
                                          p_rule_name          => l_rule_name,
                                          p_batch_rec          => l_batch_info_rec,
                                          p_batch_prefix       => l_batch_prefix,
                                          x_batch_id           => p_new_batch_id
                                         );

            IF l_return_status = 'S'
            THEN
               x_ret_batch_id := p_new_batch_id;
            ELSE
               x_ret_batch_id := -1;
                             
                             write_log ('Could not create pick batch to ship rental order');
                             
               v_call_point:='STAGE2';
            END IF;
                        
                        IF x_ret_batch_id > 0 THEN
                -- Release the batch Created Above
                wsh_picking_batches_pub.release_batch
                                              (p_api_version        => 1.0,
                                               p_init_msg_list      => 'T', -- fnd_api.g_true,
                                               p_commit             => 'T', -- fnd_api.g_true,
                                               x_return_status      => l_return_status,
                                               x_msg_count          => l_msg_count,
                                               x_msg_data           => l_msg_data,
                                               p_batch_id           => p_new_batch_id,
                                               p_batch_name         => NULL,
                                               p_log_level          => 1,
                                               p_release_mode       => 'CONCURRENT',
                                               x_request_id         => l_request_id
                                              );
    
                IF l_return_status = 'S'
                THEN
                    x_ret_request_id := l_request_id;
                  /*v_wait := fnd_concurrent.wait_for_request (l_request_id,
                                                    6,
                                                    1800,
                                                    v_phase,
                                                    v_status,
                                                    v_dev_phase,
                                                    v_dev_status,
                                                    v_message
                                                   );*/
                
                ELSE
                   x_ret_batch_id := -1;
                                 
                                 write_log ('Could not create pick release rental order');
                                 v_call_point:='STAGE3';
                END IF;
                
                        END IF;
                        
                 ELSE
                        write_log ('Could not find pick rull for rental order');
                        v_call_point:='STAGE4';
                      
         END IF;

      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
           write_error ('Error in XXWC_OM_PERSONALIZE_RTNS_PKG procedure',
                      v_call_point);
                 
   END XXWC_OM_RENTAL_PROCESSING;
  /*************************************************************************
        *   Procedure : XXWC_GET_USER_PROF_VAL
        *
        *   PURPOSE:   This procedure is used in personalization to ship rental items on internal orders..
        *  Author: Ram Talluri- TMS #20131212-00053 
        * Creation_date 12/12/2013
        * Last update Date 12/12/2013
        *   Parameter:
        *          IN p_debug_msg
        *
        * ************************************************************************/
FUNCTION XXWC_GET_USER_PROF_VAL(V_PROFILE_USER VARCHAR2)
 RETURN number
AS
v_errbuf CLOB;
V_VAL NUMBER;
BEGIN

BEGIN

select VALUE INTO V_VAL
from (SELECT PO.PROFILE_OPTION_NAME "NAME"
      ,PO.USER_PROFILE_OPTION_NAME
      ,DECODE (TO_CHAR (POV.LEVEL_ID)
              ,'10001', 'SITE'
              ,'10002', 'APP'
              ,'10003', 'RESP'
              ,'10005', 'SERVER'
              ,'10006', 'ORG'
              ,'10004', 'USER'
              ,'***')
           "LEVEL"
      ,DECODE (TO_CHAR (POV.LEVEL_ID)
              ,'10001', ''
              ,'10002', APP.APPLICATION_SHORT_NAME
              ,'10003', RSP.RESPONSIBILITY_KEY
              ,'10005', SVR.NODE_NAME
              ,'10006', ORG.NAME
              ,'10004', USR.USER_NAME
              ,'***')
           "CONTEXT"
      ,POV.PROFILE_OPTION_VALUE "VALUE"
  FROM APPS.FND_PROFILE_OPTIONS_VL PO
      ,APPS.FND_PROFILE_OPTION_VALUES POV
      ,APPS.FND_USER USR
      ,APPS.FND_APPLICATION APP
      ,APPS.FND_RESPONSIBILITY RSP
      ,APPS.FND_NODES SVR
      ,APPS.HR_OPERATING_UNITS ORG
WHERE     1 = 1
       AND POV.APPLICATION_ID = PO.APPLICATION_ID
       AND POV.PROFILE_OPTION_ID = PO.PROFILE_OPTION_ID
       AND USR.USER_ID(+) = POV.LEVEL_VALUE
       AND RSP.APPLICATION_ID(+) = POV.LEVEL_VALUE_APPLICATION_ID
       AND RSP.RESPONSIBILITY_ID(+) = POV.LEVEL_VALUE
       AND APP.APPLICATION_ID(+) = POV.LEVEL_VALUE
       AND SVR.NODE_ID(+) = POV.LEVEL_VALUE
       AND ORG.ORGANIZATION_ID(+) = POV.LEVEL_VALUE
       AND PO.PROFILE_OPTION_NAME = 'XXWC_OM_DEFAULT_SHIPPING_ORG') xxwc
       where xxwc.context=V_PROFILE_USER;

EXCEPTION
when no_data_found then
v_val:=0;
WHEN OTHERS THEN
v_errbuf :=    'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
                
                             
                             
 xxcus_error_pkg.xxcus_error_main_api(p_called_from            =>'XXWC_GEN_ROUTINES_PKG'
                                          ,p_calling           => 'XXWC_GET_USER_PROF_VAL'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(REGEXP_REPLACE(v_errbuf , '[[:cntrl:]]', NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running ' || 'XXWC_GEN_ROUTINES_PKG'|| '.' || 'XXWC_GET_USER_PROF_VAL'
                                          ,p_distribution_list => 'HDSOracleDevelopers@hdsupply.com'
                                          ,p_module            => 'ONT');
                         
                             
                             
                             
v_val:=0;

END;

dbms_output.put_line('v_val..'||v_val);




RETURN V_VAL;

 END  XXWC_GET_USER_PROF_VAL;

   
END XXWC_OM_PERSONALIZE_RTNS_PKG;
/