CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_ITEM_IMPORT_PKG
AS
   /**************************************************************************
    File Name:XXWC_EGO_ITEM_IMPORT_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Create NPR and trigger Change Order
    HISTORY
    -- Description   : Called from the trigger XXWC_ENG_NIR_TRG
    --
    --
    -- Dependencies Tables        : custom table XXWC_EGO_NPB_DEBUG
    -- Dependencies Views         : None
    -- Dependencies Sequences     : XXWC_EGO_NPB_DEBUG_S
    -- Dependencies Procedures    : XXWC_EGO_NPB_DBG_PKG
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    ================================================================
           Last Update Date : 01/11/2012
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     07-Aug-2012    Rajiv Rathod,    Initial creation of the package
						   Santhosh Louis
    1.1     11-Dec-2013    Praveen Pawar    NPBI changes
   **************************************************************************/

FUNCTION process_event (
				   p_subscription_guid IN RAW
				  ,p_event IN OUT NOCOPY WF_EVENT_T
	)
	RETURN VARCHAR2
	IS


	l_param_list               WF_PARAMETER_LIST_T;
	l_event_name            VARCHAR2(100);
	l_event_key             VARCHAR2(100);
	l_dml_type              VARCHAR2(100);
	l_category_name         VARCHAR2(100);
	l_category_id           VARCHAR2(100);
	l_category_set_id       VARCHAR2(100);
	l_parent_category_id    VARCHAR2(100);

	L_PARAM_NAME            VARCHAR2(100);
	L_PARAM_VALUE           VARCHAR2(100);

	l_param_name1           VARCHAR2(100);
	l_param_name2           VARCHAR2(100);
	l_param_name3           VARCHAR2(100);
	l_param_name4           VARCHAR2(100);
	l_param_name5           VARCHAR2(100);

--PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN


l_param_list        := P_EVENT.GETPARAMETERLIST();
l_event_name        := P_EVENT.GETEVENTNAME();
l_event_key         := substr(P_EVENT.GETEVENTKEY(),instr(P_EVENT.GETEVENTKEY(),'-')+1);

    IF l_param_list IS NOT NULL THEN

      FOR I IN l_param_list.FIRST .. l_param_list.LAST
      LOOP
        L_PARAM_NAME  := l_param_list(I).GETNAME;
        L_PARAM_VALUE := L_PARAM_LIST(I).GETVALUE;
      END LOOP;

    END IF;

RETURN 'SUCCESS';

EXCEPTION WHEN OTHERS THEN
dbms_output.put_line ('INSIDE EXCEPTION =>'||SQLERRM);
RETURN 'ERROR';

END process_event;

FUNCTION check_number (p_value IN VARCHAR2)
    RETURN VARCHAR2
IS
	v_dummy VARCHAR2(240):=null;
	l_num_flag  VARCHAR2(1):='Y';
BEGIN
    BEGIN
      v_dummy := to_number(p_value);
    EXCEPTION WHEN OTHERS THEN
      l_num_flag:='N';
    END;

    if l_num_flag='N' THEN
      v_dummy:= UPPER(p_value);
    else
      v_dummy:=LPAD(p_value,30,0);
    end if;

    RETURN v_dummy;
END;
   --------------------------------------------------------------------------------
   -- Function to check if the item is an Web Item--
   --------------------------------------------------------------------------------
 FUNCTION check_web_item (p_item_id IN number)
      RETURN NUMBER
   IS
      l_web_hier       varchar2 (200) := NULL;
      l_website_item   varchar2 (10) := NULL;
	  l_org_id 		    number := null;
BEGIN

l_org_id :=fnd_profile.value('XXWC_ITEM_MASTER_ORG');
  BEGIN
	 SELECT   mcb.segment1
	   INTO   l_website_item
	   FROM   MTL_CATEGORY_SETS MCS,
			  MTL_ITEM_CATEGORIES MIC,
			  MTL_CATEGORIES_B MCB,
			  MTL_SYSTEM_ITEMS_B msib
	  WHERE       MCS.CATEGORY_SET_NAME = 'Website Item'
			  AND MIC.CATEGORY_SET_ID = MCS.CATEGORY_SET_ID
			  AND MIC.INVENTORY_ITEM_ID = msib.inventory_item_id
			  AND MIC.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
			  AND mcb.CATEGORY_ID = MIC.CATEGORY_ID
			  AND MSIB.ORGANIZATION_ID = l_org_id
			  AND msib.inventory_item_id = p_item_id
			  AND mcb.segment1 = 'Y';
  EXCEPTION
	 WHEN OTHERS
	 THEN
		l_website_item := NULL;
  END;

  IF (l_website_item = 'Y')
  THEN
	 RETURN 1;
  ELSE
	 RETURN 0;
  END IF;
END check_web_item;
--==============================================================================
    -- Procedure to Create Related Item mapping
--==============================================================================
 procedure CREATE_RELATED_ITEMS ( P_ITEM_ID               IN  NUMBER,
                                  P_ORG_ID                IN  NUMBER,
                                  P_RELATED_ITEM_ID       IN  NUMBER,
                                  P_RELATIONSHIP_TYPE_ID  IN  NUMBER,
                                  p_user_id               IN  NUMBER)
as
	l_num_row_id   VARCHAR2 (100);

BEGIN
 apps.mtl_related_items_pkg.insert_row (
      x_rowid                   => l_num_row_id,
      x_inventory_item_id       => P_ITEM_ID,
      x_organization_id         => P_ORG_ID,
      X_RELATED_ITEM_ID         => P_RELATED_ITEM_ID,
      x_relationship_type_id    => P_RELATIONSHIP_TYPE_ID,
      x_reciprocal_flag         => 'N',
      x_planning_enabled_flag   => 'N',
      x_start_date              => NULL,
      x_end_date                => NULL,
      x_attr_context            => NULL,
      x_attr_char1              => NULL,
      x_attr_char2              => NULL,
      x_attr_char3              => NULL,
      x_attr_char4              => NULL,
      x_attr_char5              => NULL,
      x_attr_char6              => NULL,
      x_attr_char7              => NULL,
      x_attr_char8              => NULL,
      x_attr_char9              => NULL,
      x_attr_char10             => NULL,
      x_attr_num1               => NULL,
      x_attr_num2               => NULL,
      x_attr_num3               => NULL,
      x_attr_num4               => NULL,
      x_attr_num5               => NULL,
      x_attr_num6               => NULL,
      x_attr_num7               => NULL,
      x_attr_num8               => NULL,
      x_attr_num9               => NULL,
      x_attr_num10              => NULL,
      x_attr_date1              => NULL,
      x_attr_date2              => NULL,
      x_attr_date3              => NULL,
      x_attr_date4              => NULL,
      x_attr_date5              => NULL,
      x_attr_date6              => NULL,
      x_attr_date7              => NULL,
      x_attr_date8              => NULL,
      x_attr_date9              => NULL,
      x_attr_date10             => NULL,
      x_last_update_date        => SYSDATE,
      x_last_updated_by         => fnd_profile.VALUE ('USER_ID'),
      x_creation_date           => SYSDATE,
      x_created_by              => fnd_profile.VALUE ('USER_ID'),
      x_last_update_login       => fnd_profile.VALUE ('LOGIN_ID'),
      X_OBJECT_VERSION_NUMBER   => 1
   );

END CREATE_RELATED_ITEMS;

--==============================================================================
    -- Procedure to Create Cross References
--==============================================================================
  procedure CREATE_CROSS_REFERENCE (P_TRANSACTION_TYPE        in  varchar2,
                                    P_ITEM_ID                 in  number,
                                    p_org_id                  IN  number,
                                    P_CROSS_REF_TYPE          IN  varchar2,
                                    P_CROSS_REF_VALUE         IN  varchar2,
                                    p_org_independent_flag    IN  varchar2)
  as

		A_XRef_Rec        APPS.MTL_CROSS_REFERENCES_PUB.XRef_Rec_Type;
		A_XRef_Tbl        APPS.MTL_CROSS_REFERENCES_PUB.XRef_Tbl_Type;
		x_return_status   varchar2 (100);
		x_msg_count       number;
		X_MESSAGE_LIST    ERROR_HANDLER.ERROR_TBL_TYPE;
		error_msg                 error_handler.error_tbl_type;

BEGIN

		A_XRef_Rec.Transaction_Type := P_TRANSACTION_TYPE;
		A_XRef_Rec.X_Return_Status := NULL;
		-- Primary Key Columns          --
		A_XREF_REC.INVENTORY_ITEM_ID := TO_NUMBER(P_ITEM_ID);
		A_XREF_REC.ORGANIZATION_ID := TO_NUMBER(P_ORG_ID);
		A_XREF_REC.CROSS_REFERENCE_TYPE := TO_CHAR(P_CROSS_REF_TYPE);
		A_XRef_Rec.Cross_Reference := to_char(P_CROSS_REF_VALUE);
		-- As cross_reference_id is present,for uniquely identifying
		-- the row we have to use this.(Must be populated for Update and Delete)
		A_XRef_Rec.Cross_Reference_Id := NULL;
		A_XRef_Rec.Description := NULL;
		A_XRef_Rec.Org_Independent_Flag := p_org_independent_flag;
		--Who Columns
		A_XRef_Rec.Last_Update_Date := NULL;
		A_XRef_Rec.Last_Updated_By := NULL;
		A_XRef_Rec.Creation_Date := NULL;
		A_XRef_Rec.Created_By := NULL;
		A_XRef_Rec.Last_Update_Login := NULL;
		A_XRef_Rec.Request_id := NULL;
		A_XRef_Rec.Program_Application_Id := NULL;
		A_XRef_Rec.Program_Id := NULL;
		A_XRef_Rec.Program_Update_Date := NULL;
		-- DFF
		A_XRef_Rec.Attribute1 := NULL;
		A_XRef_Rec.Attribute2 := NULL;
		A_XRef_Rec.Attribute3 := NULL;
		A_XRef_Rec.Attribute4 := NULL;
		A_XRef_Rec.Attribute5 := NULL;
		A_XRef_Rec.Attribute6 := NULL;
		A_XRef_Rec.Attribute7 := NULL;
		A_XRef_Rec.Attribute8 := NULL;
		A_XRef_Rec.Attribute9 := NULL;
		A_XRef_Rec.Attribute10 := NULL;
		A_XRef_Rec.Attribute11 := NULL;
		A_XRef_Rec.Attribute12 := NULL;
		A_XRef_Rec.Attribute13 := NULL;
		A_XRef_Rec.Attribute14 := NULL;
		A_XRef_Rec.Attribute15 := NULL;
		A_XRef_Rec.Attribute_category := NULL;
		A_XRef_Rec.Uom_Code := NULL;
		A_XRef_Rec.Revision_Id := NULL;
		A_XREF_TBL (1) := A_XREF_REC;

		 apps.MTL_CROSS_REFERENCES_PUB.Process_XRef (
			p_api_version     => NULL,
			p_init_msg_list   => NULL,
			p_commit          => FND_API.g_TRUE,
			p_XRef_Tbl        => A_XRef_tbl,
			x_return_status   => x_return_status,
			x_msg_count       => x_msg_count,
			x_message_list    => x_message_list
		 );

		DBMS_OUTPUT.PUT_LINE ('x_return_Status'||X_RETURN_STATUS);

END CREATE_CROSS_REFERENCE;
--==============================================================================
    -- Procedure to Create Attachments for Item
--==============================================================================
  procedure CREATE_ATTACHMENTS(P_DESCRIPTION      in  varchar2,
                                P_FILE_NAME       IN  varchar2,
                                P_DOC_TEXT        IN  varchar2,
                                P_ITEM_ID         IN  varchar2,
                                P_ORG_ID          IN  varchar2,
                                P_REVISION_ID     in  varchar2,
                                p_attached_doc_id OUT number
                                )
  as
		l_document_id            NUMBER;
		l_attached_document_id   NUMBER;
		l_description            varchar2 (100) := P_DESCRIPTION;
		l_filename               varchar2 (100) := P_FILE_NAME;
		l_rowid                  number;
		l_media_id               number;
		l_category_id            number := 1;
		l_return_status          varchar2 (100);
		l_msg_count              number;
		l_msg_data               varchar2 (1000);
		l_data_type_id           fnd_documents.datatype_id%TYPE;
		--  l_category_id              fnd_documents.category_id%TYPE;
		l_security_type          fnd_documents.security_type%TYPE;
		l_security_id            fnd_documents.security_id%TYPE;
		l_publish_flag           fnd_documents.publish_flag%TYPE;
		l_image_type             fnd_documents.image_type%TYPE;
		l_storage_type           fnd_documents.storage_type%TYPE;
		l_usage_type             fnd_documents.usage_type%TYPE;
		l_start_date_active      fnd_documents.start_date_active%TYPE;
		l_end_date_active        fnd_documents.end_date_active%TYPE;
		L_URL                    FND_DOCUMENTS.URL%type;
BEGIN
	dbms_output.put_line ('Inside Attachement creation');
	   APPS.oe_fnd_attachments_pub.Create_Short_Text_Document (
		  p_api_version            => 1.0,
		  p_document_text          => P_DOC_TEXT,
		  p_document_category      => l_category_id,
		  p_document_description   => l_description,          --l_doc_description,
		  p_language               => NULL,
		  p_security_type          => 4,
		  p_security_id            => NULL,
		  p_publish_flag           => 'Y',
		  p_usage_type             => 'S',
		  p_start_date_active      => SYSDATE,
		  p_end_date_active        => null,
		  p_commit                 => 'Y',
		  x_document_id            => l_document_id,
		  x_return_status          => l_return_status,
		  x_msg_count              => l_msg_count,
		  x_msg_data               => l_msg_data
	   );

   SELECT   datatype_id,
            category_id,
            security_type,
            security_id,
            publish_flag,
            image_type,
            storage_type,
            usage_type,
            start_date_active,
            end_date_active,
            url,
            media_id
     INTO   l_data_type_id,
            l_category_id,
            l_security_type,
            l_security_id,
            l_publish_flag,
            l_image_type,
            l_storage_type,
            l_usage_type,
            l_start_date_active,
            l_end_date_active,
            l_url,
            l_media_id
     FROM   fnd_documents
    WHERE   document_id = l_document_id;

	   APPS.fnd_documents_pkg.Update_Row (
		  X_document_id         => l_document_id,
		  X_last_update_date    => SYSDATE,
		  X_last_updated_by     => fnd_profile.VALUE ('USER_ID'),
		  X_last_update_login   => fnd_profile.VALUE ('LOGIN_ID'),
		  X_datatype_id         => l_data_type_id,
		  X_category_id         => l_category_id,
		  X_security_type       => l_security_type,
		  X_security_id         => l_security_id,
		  X_publish_flag        => l_publish_flag,
		  X_image_type          => l_image_type,
		  X_storage_type        => l_storage_type,
		  X_usage_type          => l_usage_type,
		  X_start_date_active   => l_start_date_active,
		  X_end_date_active     => l_end_date_active,
		  X_language            => 'US',
		  X_description         => l_description,
		  X_file_name           => l_filename,
		  X_media_id            => l_media_id
	   );
	   dbms_output.put_line ('Adding attachement to item');
	   APPS.oe_fnd_attachments_pvt.Add_Attachment (
		  P_API_VERSION      => '1.0',
		  P_ENTITY_NAME      => 'MTL_ITEM_REVISIONS',
		  P_PK1_VALUE        =>  P_ORG_ID,
		  P_PK2_VALUE        =>  P_ITEM_ID,
		  p_pk3_value        =>  p_revision_id,
		  p_pk4_value        => NULL,
		  p_pk5_value        => NULL,
		  p_automatic_flag   => 'N',
		  p_document_id      => l_document_id,
		  p_validate_flag    => 'N',
		  x_attachment_id    => l_attached_document_id,
		  x_return_status    => l_return_status,
		  x_msg_count        => l_msg_count,
		  x_msg_data         => l_msg_data
	   );

	   UPDATE   FND_attached_documents
		  SET   status = 'APPROVED'
		where   ATTACHED_DOCUMENT_ID = L_ATTACHED_DOCUMENT_ID;

		P_ATTACHED_DOC_ID:=L_ATTACHED_DOCUMENT_ID;

  END create_attachments;


PROCEDURE INITIATE_WORKFLOW(l_item_Type   IN  varchar2,
                            l_item_key    IN  varchar2,
                            l_body_text   IN  varchar2,
                            l_sender_id     IN  varchar2
                            )
AS
	   l_chr_item_key    VARCHAR2 (200) := null;
	   l_chr_item_type   VARCHAR2 (15) := 'XXWCNOTF';
	   l_Subject  VARCHAR2 (2000) :=  null;
	   l_body varchar2(4000) := 'Hi,
									';--Donot remove the indentation
	   l_sender varchar2(50) :=  NULL;
	   L_RECEIVER VARCHAR2(50) := NULL;
       L_USER_NAME VARCHAR2(100):=NULL;

	   L_VERBAGE 	VARCHAR2(1000):=NULL;
     l_verbage1 	VARCHAR2(1000):=NULL;
BEGIN

SELECT  USER_NAME
  INTO  L_USER_NAME
  FROM  FND_USER
 WHERE  user_id =l_sender_id;

 L_SENDER   := L_USER_NAME;
 L_RECEIVER := L_USER_NAME;
 L_VERBAGE 	:='We are sorry, your ';
 l_verbage1 	:=' and has failed due to an unexpected system problem.See below for details.'||chr(10)||chr(13)||'The IT team has been notified and you may contact someemailgroup@hdsupply.net if you need assistance.'||chr(10)||chr(13);

	l_chr_item_key:=l_item_Type||'-'||l_item_key||to_char(sysdate, 'dd-mon-yyyy-hh-mi-ss');

	IF l_item_Type='ITEM' THEN
	  L_SUBJECT:= L_ITEM_TYPE||'-'||L_ITEM_KEY||' has errors';
	ELSIF L_ITEM_TYPE='DUPITEM' then
	  l_Subject:= l_item_key;
	ELSIF l_item_Type='NPR' THEN
	  l_Subject:= 'New Product Request '||'-'||l_item_key||' has errors';
	ELSIF l_item_Type='NPW' THEN
	  l_Subject:= 'New Product Workflow '||'-'||l_item_key||' has errors';
	ELSIF l_item_Type='NWP' THEN
	  l_Subject:= 'New Web Product '||'-'||l_item_key||' has errors';
	END IF;


		l_body:= l_body ||l_verbage||l_Subject|| l_verbage1;
		l_body := l_body ||l_body_text;


		apps.wf_engine.createprocess (itemtype   => l_chr_item_type,
									 itemkey    => l_chr_item_key,
									 process    => 'XX_SEND_NOTF');

		wf_engine.setitemattrtext (itemtype   => l_chr_item_type,
								  itemkey    => l_chr_item_key,
								  aname      => 'SENDER',
								  avalue     => l_sender);

		wf_engine.setitemattrtext (itemtype   => l_chr_item_type,
								  itemkey    => l_chr_item_key,
								  aname      => 'RECEIVER',
								  avalue     => l_receiver);

		wf_engine.setitemattrtext (itemtype   => l_chr_item_type,
								  itemkey    => l_chr_item_key,
								  aname      => 'SUBJECT',
								  avalue     => l_Subject);

		wf_engine.setitemattrtext (itemtype   => l_chr_item_type,
								  itemkey    => l_chr_item_key,
								  aname      => 'BODY',
								  avalue     => l_body);

		wf_engine.startprocess (itemtype   => l_chr_item_type,
							   itemkey    => l_chr_item_key);

		COMMIT;
END INITIATE_WORKFLOW;
--==============================================================================
    -- Procedure to Check if the item entered by user exists
--==============================================================================
procedure CHECK_ITEM_DUP (P_IN_NUM_CHANGE_ID in number,
                          p_STATUS_CODE in VARCHAR2)
AS

    PRAGMA AUTONOMOUS_TRANSACTION;
    L_ITEM_NO       varchar2(30):=null;
    l_change_notice varchar2(30):=null;
    L_COUNT         number;
    L_NUM_USER_ID   number;
    l_sumbit_prog   number:= 0;

BEGIN
XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Inside duplicate item check',P_IN_NUM_CHANGE_ID);
if(P_STATUS_CODE = 1) THEN
 select   CREATED_BY,CHANGE_NOTICE
   INTO   l_num_user_id,l_change_notice
   from   ENG_ENGINEERING_CHANGES
  where   change_id = P_IN_NUM_CHANGE_ID and rownum = 1;

SELECT  wc_pim_cr_item_no
  INTO  l_item_no
  FROM  ENG_WC_PIM_CR_ITEM_M_AGV
 WHERE  CHANGE_ID=P_IN_NUM_CHANGE_ID;

XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Item No got',L_ITEM_NO);

SELECT  COUNT(1)
  INTO  l_count
  FROM  MTL_SYSTEM_ITEMS_B
 WHERE  SEGMENT1=L_ITEM_NO;
XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Item No Duplicate count',l_count);

  if L_COUNT>0  then

      BEGIN
         SELECT   1
           INTO   l_sumbit_prog
           FROM   eng_engineering_changes a, eng_change_order_types_vl b
          WHERE   b.change_order_type_id = a.change_order_type_id
            AND   change_order_type IN ('TEST_CHANGE_REQUEST', 'New Product Request')
            AND   b.change_mgmt_type_code = 'CHANGE_REQUEST'
            AND   change_id = p_in_num_change_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_sumbit_prog := 0;
      END;
    IF L_SUMBIT_PROG=1 THEN
      INITIATE_WORKFLOW('DUPITEM','New Product Request '||'-'||L_CHANGE_NOTICE||' has errors','The Item # -"'||L_ITEM_NO ||'" already exists ,Please revisit the NPR -'||L_CHANGE_NOTICE||' and modify the Item # entered.',L_NUM_USER_ID);
    END IF;
  end if;

END IF;

end CHECK_ITEM_DUP;

--==============================================================================
    -- Procedure to import item and to trigger Change Order
--==============================================================================
PROCEDURE import_item_proc (p_in_num_change_id IN NUMBER)
AS

    PRAGMA AUTONOMOUS_TRANSACTION;

    l_extension_id            NUMBER;
      l_num_user_id             NUMBER;
      l_num_resp_id             NUMBER;
      l_num_app_id              NUMBER;
      l_num_set_process_id      NUMBER;
      l_num_request_id          NUMBER;
      l_chr_phase               VARCHAR2 (100);
      l_chr_status              VARCHAR2 (100);
      l_chr_dev_phase           VARCHAR2 (100);
      l_chr_dev_status          VARCHAR2 (100);
      l_chr_msg                 VARCHAR2 (1000);
      x_commit                  BOOLEAN := TRUE;
      l_num_total_records       NUMBER;
      l_num_organization_id     NUMBER;
      l_chr_user_name           VARCHAR2 (100);
      l_num_batch_id            NUMBER;
      l_return_status           VARCHAR2 (10);
      l_chr_error_msg           VARCHAR2 (200);
      l_request_id              NUMBER;
      L_REQUEST_WAIT            BOOLEAN;
      l_flag                    NUMBER :=0;
    -------------------------------------
        -- variables for change order creation
    -------------------------------------
      p_eco_rec                 eng_eco_pub.eco_rec_type;                   --
      p_eco_revision_tbl        eng_eco_pub.eco_revision_tbl_type;          --
      p_revised_item_tbl        eng_eco_pub.revised_item_tbl_type;          --
      p_rev_component_tbl       bom_bo_pub.rev_component_tbl_type;          --
      p_ref_designator_tbl      bom_bo_pub.ref_designator_tbl_type;         --
      p_sub_component_tbl       bom_bo_pub.sub_component_tbl_type;          --
      x_return_status           VARCHAR2 (10);                              --
      x_msg_count               NUMBER;                                     --
      x_message_list            error_handler.error_tbl_type;
      x_eco_rec                 eng_eco_pub.eco_rec_type;                   --
      x_eco_revision_tbl        eng_eco_pub.eco_revision_tbl_type;
      x_revised_item_tbl        eng_eco_pub.revised_item_tbl_type;
      x_rev_component_tbl       bom_bo_pub.rev_component_tbl_type;
      x_ref_designator_tbl      bom_bo_pub.ref_designator_tbl_type;
      x_sub_component_tbl       bom_bo_pub.sub_component_tbl_type;
      x_rev_operation_tbl       bom_rtg_pub.rev_operation_tbl_type;
      x_rev_op_resource_tbl     bom_rtg_pub.rev_op_resource_tbl_type;
      x_rev_sub_resource_tbl    bom_rtg_pub.rev_sub_resource_tbl_type;
      x_change_line_tbl         eng_eco_pub.change_line_tbl_type;           --
      l_change_line_tbl         eng_eco_pub.change_line_tbl_type;
      error_msg                 error_handler.error_tbl_type;
      l_eco_name                  eng_engineering_changes.change_notice%TYPE;

    -------------------------------------
    -- Hard coded variable which might change
    -------------------------------------
      l_change_name         VARCHAR2 (240)     := 'CO-' || TO_CHAR (SYSDATE, 'ddmmrrrrhh24miss') ;
      l_eco_name_desc         VARCHAR2 (1000)  :=    'CO-' || TO_CHAR (SYSDATE, 'ddmmrrrrhh24miss')|| ' Change Order' ;
      l_apprvl_sts_name     VARCHAR2 (1000):= 'Not submitted for approval' ;
      L_CHNG_TYPE           ENG_CHANGE_ORDER_TYPES_VL.TYPE_NAME%type:= 'New Product Workflow';
      l_chng_mgmt_code      VARCHAR2 (1000) := 'CHANGE_ORDER';


      l_chng_type_id            eng_change_order_types_vl.change_order_type_id%TYPE;
      l_change_id               eng_engineering_changes.change_id%TYPE;
      l_change_number                eng_engineering_changes.change_notice%TYPE := NULL ;

      l_org_code              VARCHAR2(100):='MST';
      l_chr_user              VARCHAR2 (100);
      l_sumbit_prog           number :=0;
      L_SUMBIT_ACTIVE_PROG    NUMBER :=0;
      l_check_Flag            NUMBER :=0;
      l_inventory_item_id     NUMBER;
      l_org_id                NUMBER :=null;
      l_related_item_id       NUMBER;
      l_string                   VARCHAR2(4000) :=null;
      l_description              VARCHAR2(200):=null;
      l_file_name              VARCHAR2(200):=null;
      l_revision_id              NUMBER;
      l_attached_doc_id          NUMBER;
    l_category_id         NUMBER := null;
    l_sp_item_cat_id      NUMBER :=null;
    -------------------------------------
    -- Record Type declarations to hold the attribute values entered by user while creating Change Request
    -------------------------------------

        main_details_rec         eng_wc_pim_cr_item_m_agv%ROWTYPE;
        phyl_details_rec        eng_wc_pim_cr_item_p_agv%ROWTYPE;
        hzmt_details_rec        eng_wc_pim_cr_item_h_agv%ROWTYPE;
        othr_details_rec        eng_wc_pim_cr_others_agv%ROWTYPE;
        purc_details_rec        eng_wc_pim_cr_purcha_agv%ROWTYPE;

        l_num_template_id       NUMBER;
        l_num_buyer_id          NUMBER;
		l_item_no               VARCHAR2(200):='';
		l_change_notice         eng_engineering_changes.change_notice%TYPE;

		l_notes                 VARCHAR2(4000):=null;

		l_item_error_cnt        NUMBER:=0;
		l_npr_error_cnt         NUMbER:=0;
		l_npw_error_cnt         NUMBER:=0;
		l_nwp_error_cnt         NUMBER:=0;

		l_item_error_msg        VARCHAR2(4000):=null;
		l_npr_error_msg         VARCHAR2(4000):=null;
		l_npw_error_msg         VARCHAR2(4000):=null;
		l_nwp_error_msg         VARCHAR2(4000):=null;

		l_cogs_acc              VARCHAR2(10);
		l_sales_acc             VARCHAR2(10);
		l_cogs_acc_val          NUMBER := null;
		l_sales_Acc_val         NUMBER := null;
		l_org_cogs_id           NUMBER;
		l_org_sales_id          NUMBER;
		l_sub_inv_val           VARCHAR2(10) := null;
		
		-- Added w.r.t. ver 1.1 on 12/11/2013 --		
		l_tbl_rec_cnt             NUMBER                                      := 0;
		ln_grp_identifier         NUMBER                                      := 0;
		ln_attr_identifier        NUMBER                                      := 0;
		ln_internal_attr_grp_id   NUMBER                                      := NULL;
		ln_wcm_attr_grp_id        NUMBER                                      := NULL;
		ln_purchase_attr_grp_id   NUMBER                                      := NULL;
		ln_msg_count              NUMBER                                      := 0;
		ln_errorcode              NUMBER                                      := 0;
		ln_attr_grp_id            NUMBER                                      := NULL;
		ln_pre_attr_grp_id        NUMBER                                      := NULL;		
		
		ln_item_id                MTL_SYSTEM_ITEMS_B.INVENTORY_ITEM_ID%TYPE   := NULL;
		
		lv_data_level             VARCHAR2(10)                                := NULL; 			
  lv_err_msg	               VARCHAR2(4000)                              := NULL;
		lv_return_status          VARCHAR2 (1)                                := NULL;		
		lv_msg_data               VARCHAR2 (4000)                             := NULL;		
		l_failed_row_id_list      VARCHAR2 (2000)                             := NULL;
		
		cv_attr_coo               VARCHAR2(40)  := 'XXWC_COUNTRY_ORIGIN_ATTR';
		cv_attr_a_f_1             VARCHAR2(40)  := 'XXWC_A_F_1_ATTR';
		cv_attr_a_t_1             VARCHAR2(40)  := 'XXWC_A_T_1_ATTR';
		cv_attr_vendor            VARCHAR2(40)  := 'XXWC_VENDOR_NUMBER_ATTR';
		cv_ego_itemmgmt_group     VARCHAR2 (60) := 'EGO_ITEMMGMT_GROUP';	
		-- End of declaration of additional variables w.r.t. ver 1.1 --		
	
		TYPE trec_uda_list IS RECORD (
					attr_group_name     VARCHAR2 (200),
					attr_group_id       NUMBER,
					attr_name           VARCHAR2 (200),
					attr_display_name   VARCHAR2 (200),
					attr_value_var      VARCHAR2 (400),
					attr_value_num      NUMBER,
					attr_value_dt       DATE,
					attr_data_type      VARCHAR2 (5),
					exception_flag      VARCHAR2 (1)
		);

 TYPE trec_uda_list_tbl IS TABLE OF trec_uda_list
    INDEX BY BINARY_INTEGER;

 t_uda_list_tbl                 trec_uda_list_tbl;		

 l_attributes_row_table     ego_user_attr_row_table
                                           := ego_user_attr_row_table
                                                                     ();
 l_attributes_data_table    ego_user_attr_data_table
                                          := ego_user_attr_data_table
                                                                     ();	
	
    -------------------------------------
    -- Cursor Declaration
    -- Cursor get to get all the Valid items
    -------------------------------------
      CURSOR items_cur
      IS

        SELECT    ORGANIZATION,ORGANIZATION_ID
        FROM (
         SELECT   xxwc_branch_name_disp ORGANIZATION,xxwc_branch_name organization_id ,rownum rr
           FROM   ENG_XXWC_PIM_CR_BRAN_AGV a
          WHERE   a.change_id = p_in_num_change_id
          UNION
         SELECT   'MST' ORGANIZATION ,fnd_profile.value('XXWC_ITEM_MASTER_ORG') ORGANIZATION_ID ,9999 RR FROM DUAL )
        ORDER by rr desc;


      CURSOR rev_items_cur
      IS
         SELECT   revised_item_id inventory_item_id,
                  b.organization_id,
                  b.segment1 item_number
           FROM   eng_revised_items a, mtl_system_items_b b
          WHERE   a.change_id = p_in_num_change_id
            AND   a.revised_item_id = b.inventory_item_id;

    begin

	--Find master Org and set it to l_org_id

	l_org_id:=fnd_profile.value('XXWC_ITEM_MASTER_ORG');
    -------------------------------------
    -- Perform Check to see if the request is for
    -- Change request process
    -------------------------------------
	  XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('At sTart of Item Import','');
      BEGIN
         SELECT   1
           INTO   l_sumbit_prog
           FROM   eng_engineering_changes a, eng_change_order_types_vl b
          WHERE   b.change_order_type_id = a.change_order_type_id
            AND   change_order_type IN ('TEST_CHANGE_REQUEST', 'New Product Request')
            AND   b.change_mgmt_type_code = 'CHANGE_REQUEST'
            AND   change_id = p_in_num_change_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_sumbit_prog := 0;
      END;
        XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('l_sumbit_prog',l_sumbit_prog);
    -------------------------------------
    -- Perform Check to see if the request is for
    -- Change Order process
    -------------------------------------
      BEGIN
         SELECT   1
           INTO   l_sumbit_active_prog
           FROM   eng_engineering_changes a, eng_change_order_types_vl b
          WHERE   b.change_order_type_id = a.change_order_type_id
            AND   change_order_type IN ('TEST_CO_TYPE', 'New Product Workflow')
            AND   b.change_mgmt_type_code = 'CHANGE_ORDER'
            AND   change_id = p_in_num_change_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_sumbit_active_prog := 0;
      end;
        XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('l_sumbit_active_prog',l_sumbit_active_prog);
      IF (l_sumbit_prog = 1)
      THEN
        BEGIN --Main BEgin for the IF (l_sumbit_prog = 1)

             BEGIN
                BEGIN
                   SELECT   responsibility_id, application_id
                     INTO   l_num_resp_id, l_num_app_id
                     FROM   fnd_responsibility
                    WHERE   responsibility_key = 'EGO_PIM_DATA_LIBRARIAN';
                EXCEPTION
                   WHEN OTHERS
                   THEN
                      NULL;
               XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Inside resp exception',l_num_resp_id);
                END;

                BEGIN
                   SELECT   created_by
                     INTO   l_num_user_id
                     FROM   eng_lifecycle_statuses
                    WHERE   entity_id1 = p_in_num_change_id AND ROWNUM = 1;
                EXCEPTION
                   WHEN OTHERS
                   THEN
                   NULL;
					XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Inside created exception',l_num_user_id);
                end;
                fnd_global.apps_initialize (user_id        => l_num_user_id,
                                            resp_id        => l_num_resp_id,
                                            resp_appl_id   => l_num_app_id);
             end;

    -------------------------------------
    -- Get all attribute Values that were entered while creating Change Request
    -------------------------------------
         BEGIN
			BEGIN
            SELECT   *
              INTO   main_details_rec
              FROM   eng_wc_pim_cr_item_m_agv
             WHERE   change_id = p_in_num_change_id;
               EXCEPTION
            WHEN OTHERS
            THEN
               XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Exception while retreiving Attribute Values main_details_rec  :-' , SQLERRM);
               DBMS_OUTPUT.put_line ('Exception while retreiving Attribute Values main_details_rec :-' || SQLERRM);
			END;

			BEGIN

            SELECT   template_id
              INTO   l_num_template_id
              from   MTL_ITEM_TEMPLATES_ALL_V
             where   UPPER (TEMPLATE_NAME) = UPPER (MAIN_DETAILS_REC.WC_PIM_CR_ITEM_TYPE);

                EXCEPTION
            WHEN OTHERS
            THEN
               XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Exception MAIN_DETAILS_REC.WC_PIM_CR_ITEM_TYPE  :-' , MAIN_DETAILS_REC.WC_PIM_CR_ITEM_TYPE);
               XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Exception l_num_template_id :-' , l_num_template_id);
               DBMS_OUTPUT.put_line ('Exception while retreiving Attribute Values main_details_rec :-' || SQLERRM);
			end;

-------------------------------------
    -- If special item selected then go with the details of Special Item
-------------------------------------
			if main_details_rec.WC_PIM_CR_SPL_ITEM_FLG ='Yes' then

				select   DECODE(COUNT(1),0,0,1)
				  into   L_FLAG
				  from   MTL_SYSTEM_ITEMS_B
				 where   segment1=MAIN_DETAILS_REC.XXWC_PIM_CR_SPECIAL_ITEM_NO;

			END IF;
			begin
				SELECT   *
				  INTO   phyl_details_rec
				  FROM   eng_wc_pim_cr_item_p_agv
				 WHERE   change_id = p_in_num_change_id;
			 EXCEPTION
				WHEN OTHERS
				THEN
				   XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Exception while retreiving Attribute Values phyl_details_rec  :-' , SQLERRM);
				   DBMS_OUTPUT.put_line ('Exception while retreiving Attribute Values phyl_details_rec :-' || SQLERRM);
			 end;

			begin
            SELECT   *
              INTO   othr_details_rec
              FROM   eng_wc_pim_cr_others_agv
             WHERE   change_id = p_in_num_change_id;

                EXCEPTION
            WHEN OTHERS
            THEN
               XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Exception while retreiving Attribute Values othr_details_rec :-' , SQLERRM);
               DBMS_OUTPUT.put_line ('Exception while retreiving Attribute Values othr_details_rec :-' || SQLERRM);
			end;

			begin
            SELECT   *
              INTO   hzmt_details_rec
              FROM   eng_wc_pim_cr_item_h_agv
             WHERE   change_id = p_in_num_change_id;

                EXCEPTION
            WHEN OTHERS
            THEN
               XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Exception while retreiving Attribute Values phyl_details_rec hzmt_details_rec :-' , SQLERRM);
               DBMS_OUTPUT.put_line ('Exception while retreiving Attribute Values phyl_details_rec hzmt_details_rec :-' || SQLERRM);
			end;

			begin
            SELECT   *
              INTO   purc_details_rec
              FROM   eng_wc_pim_cr_purcha_agv
             WHERE   change_id = p_in_num_change_id;

                EXCEPTION
            WHEN OTHERS
            THEN
               XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Exception while retreiving Attribute Values phyl_details_rec purc_details_rec :-' , SQLERRM);
               DBMS_OUTPUT.put_line ('Exception while retreiving Attribute Values phyl_details_rec purc_details_rec :-' || SQLERRM);
			end;


         EXCEPTION
            WHEN OTHERS
            THEN
               XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Exception while retreiving Attribute Values :-' , SQLERRM);
               DBMS_OUTPUT.put_line ('Exception while retreiving Attribute Values :-' || SQLERRM);
         end;

         BEGIN
            SELECT   person_id
              INTO   l_num_buyer_id
              from   PER_ALL_PEOPLE_F
             WHERE   FULL_NAME = PURC_DETAILS_REC.WC_PIM_CR_PC_BUYER
              AND    ROWNUM<2 ;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_num_buyer_id:=NULL;
         END;

		l_item_no :=MAIN_DETAILS_REC.WC_PIM_CR_ITEM_NO;

		  SELECT change_notice
			INTO  l_change_notice
			FROM eng_engineering_changes
		   WHERE change_id = p_in_num_change_id;
    -------------------------------------
    -- Get Batch ID
    -------------------------------------
         BEGIN
            SELECT   user_name
              INTO   l_chr_user_name
              FROM   fnd_user
             WHERE   user_id = fnd_global.user_id;

            SELECT   organization_id
              INTO   l_num_organization_id
              FROM   mtl_parameters
             where   ORGANIZATION_CODE = l_org_code;
			XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('l_chr_user_name=>'||l_chr_user_name , 'l_num_organization_id=>'||l_num_organization_id);

            apps.ego_import_batches_pkg.create_import_batch (
               p_source_system_code           => 'PIMDH',
               p_organization_id              => l_num_organization_id,
               p_batch_type_display_name      => 'item',
               p_assignee_name                => l_chr_user_name,
               p_batch_name                   => 'INTERFACE BATCH',
               p_match_on_data_load           => 'N',
               p_apply_def_match_rule_all     => 'N',
               p_confirm_single_match         => 'N',
               p_import_on_data_load          => 'N',
               p_import_xref_only             => 'N',
               p_revision_import_policy       => 'N',
               p_structure_name               => '',
               p_structure_effectivity_type   => NULL,
               p_effectivity_date             => NULL,
               p_from_end_item_unit_number    => '',
               p_structure_content            => '',
               p_change_order_creation        => '',
               p_add_all_to_change_flag       => '',
               p_change_mgmt_type_code        => '',
               p_change_type_id               => NULL,
               p_change_notice                => NULL,
               p_def_match_rule_cust_app_id   => NULL,
               p_def_match_rule_cust_code     => '',
               p_nir_option                   => NULL,
               p_enabled_for_data_pool        => NULL,
               x_batch_id                     => l_num_batch_id,
               X_RETURN_STATUS                => L_RETURN_STATUS,
               x_error_msg                    => l_chr_error_msg
            );
            COMMIT;
            DBMS_OUTPUT.put_line ('l_num_batch_id' || l_num_batch_id);
            DBMS_OUTPUT.put_line ('l_return_status' || l_return_status);
            DBMS_OUTPUT.put_line ('l_chr_error_msg' || l_chr_error_msg);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
              XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Inside Batch creation program exception' ,l_chr_error_msg);
         end;
dbms_output.put_line(l_return_status);
    -------------------------------------
    -- Insert item attributes into interface table with the batch id obtained
    -------------------------------------
			  XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('before category :- othr_details_rec.wc_pim_cr_ot_cc' , othr_details_rec.wc_pim_cr_ot_cc);
			  BEGIN
				  select category_id
				  into l_Category_id
				  from mtl_categories_kfv
				  where concatenated_segments=othr_details_rec.wc_pim_cr_ot_cc;
			  EXCEPTION WHEN OTHERS THEN
				  l_Category_id:=null;
				  XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Exception of category :-' , SQLERRM);
			  END;

   XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('after category :- l_Category_id' , l_Category_id);

  IF MAIN_DETAILS_REC.XXWC_PIM_CR_SPECIAL_ITEM_NO IS NOT NULL THEN

      SELECT /*nvl(OTHR_DETAILS_REC.WC_PIM_ICC,(select SEGMENT1
                        from MTL_ITEM_CATALOG_GROUPS_B
                        where ITEM_CATALOG_GROUP_ID=MSIV.ITEM_CATALOG_GROUP_ID)),*/ -- commented as ICC is hardcoded to "ICC Unclassified"
        nvl(othr_details_rec.wc_pim_cr_cc_tax_code,attribute22),
        nvl(main_details_rec.wc_pim_cr_description,msiv.description),
        nvl(main_details_rec.wc_pim_cr_long_desc,msiv.LONG_DESCRIPTION),
        nvl(main_details_rec.wc_pim_cr_shelf_life_days,msiv.SHELF_LIFE_DAYS),
        NVL(MAIN_DETAILS_REC.WC_PIM_CR_PRIMARY_UOM,MSIV.PRIMARY_UNIT_OF_MEASURE),
        nvl(phyl_details_rec.WC_PIM_CR_ITEM_PHY_SHIPPI_UUOM,msiv.WEIGHT_UOM_CODE),
        nvl(phyl_details_rec.wc_pim_cr_item_phy_shipping_w,msiv.UNIT_WEIGHT),
        nvl(hzmt_details_rec.wc_pim_cr_hz_flag,msiv.HAZARDOUS_MATERIAL_FLAG),
        nvl(hzmt_details_rec.wc_hazard_class,msiv.HAZARD_CLASS_ID),
        nvl(HZMT_DETAILS_REC.WC_PIM_CR_HZ_CONTAINER_TYPE,msiv.ATTRIBUTE18),
        nvl(hzmt_details_rec.wc_pim_cr_hz_ca_prop,msiv.attribute1),
        nvl(HZMT_DETAILS_REC.WC_PIM_CR_HZ_PST_FLG,msiv.attribute3),
        nvl(hzmt_details_rec.xxwc_un_number,msiv.un_number_id),
        nvl(hzmt_details_rec.WC_PIM_CR_PST_FLG_ST,msiv.attribute5),
        nvl(HZMT_DETAILS_REC.WC_PIM_CR_HZ_VOC_GL,msiv.attribute4),
        nvl(purc_details_rec.wc_pim_cr_pc_po_cost,msiv.list_price_per_unit),
        nvl(purc_details_rec.xxwc_pim_cr_pc_lead_time,msiv.full_lead_time),
        nvl(purc_details_rec.wc_pim_cr_pc_coo,msiv.attribute10),
        nvl(PURC_DETAILS_REC.XXWC_PIM_CR_PC_FIX_LT_MY,msiv.FIXED_LOT_MULTIPLIER),
        NVL(OTHR_DETAILS_REC.WC_PIM_CR_CC_SOURCED,MSIV.SOURCE_TYPE),
        NVL(HZMT_DETAILS_REC.XXWC_VOC_CATEGORY,MSIV.attribute6 ),
        NVL(HZMT_DETAILS_REC.WC_PIM_CR_HZ_VOC_GL ,MSIV.attribute4),
        NVL(HZMT_DETAILS_REC.XXWC_VOC_SUBCATEGORY,MSIV.attribute7 ),
        NVL(hzmt_details_rec.XXWC_MSDS_NUMBER, MSIV.attribute8),
        NVL(hzmt_details_rec.WC_PIM_CR_Packing_Grp,MSIV.attribute9)

    into     --OTHR_DETAILS_REC.WC_PIM_ICC, -- commented as ICC is hardcoded to "ICC Unclassified"
        othr_details_rec.wc_pim_cr_cc_tax_code,
        main_details_rec.wc_pim_cr_description,
        main_details_rec.wc_pim_cr_long_desc,
        main_details_rec.wc_pim_cr_shelf_life_days,
        main_details_rec.wc_pim_cr_primary_uom,
        PHYL_DETAILS_REC.WC_PIM_CR_ITEM_PHY_SHIPPI_UUOM,
        phyl_details_rec.wc_pim_cr_item_phy_shipping_w,
        hzmt_details_rec.wc_pim_cr_hz_flag,
        hzmt_details_rec.wc_hazard_class,
        HZMT_DETAILS_REC.WC_PIM_CR_HZ_CONTAINER_TYPE,
        hzmt_details_rec.wc_pim_cr_hz_ca_prop,
        HZMT_DETAILS_REC.WC_PIM_CR_HZ_PST_FLG,
        hzmt_details_rec.xxwc_un_number,
        hzmt_details_rec.WC_PIM_CR_PST_FLG_ST,
        HZMT_DETAILS_REC.WC_PIM_CR_HZ_VOC_GL,
        purc_details_rec.wc_pim_cr_pc_po_cost,
        purc_details_rec.xxwc_pim_cr_pc_lead_time,
        purc_details_rec.wc_pim_cr_pc_coo,
        PURC_DETAILS_REC.XXWC_PIM_CR_PC_FIX_LT_MY,
        othr_details_rec.wc_pim_cr_cc_sourced,
        HZMT_DETAILS_REC.XXWC_VOC_CATEGORY,
        HZMT_DETAILS_REC.WC_PIM_CR_HZ_VOC_GL ,
        HZMT_DETAILS_REC.XXWC_VOC_SUBCATEGORY  ,
        hzmt_details_rec.XXWC_MSDS_NUMBER,
        hzmt_details_rec.WC_PIM_CR_Packing_Grp
    FROM      MTL_SYSTEM_ITEMS_VL  MSIV
    WHERE     SEGMENT1=MAIN_DETAILS_REC.XXWC_PIM_CR_SPECIAL_ITEM_NO
    AND ROWNUM=1;


    BEGIN
      SELECT  category_id
      INTO    l_sp_item_cat_id
      FROM    mtl_item_categories mic,mtl_system_items_B msib,mtl_category_sets_b mcsb
      WHERE   mcsb.structure_id=101
      AND     mcsb.category_Set_id=mic.category_Set_id
      AND     mic.inventory_item_id=msib.inventory_item_id
      AND     mic.organization_id=msib.organization_id
      and     msib.SEGMENT1=MAIN_DETAILS_REC.XXWC_PIM_CR_SPECIAL_ITEM_NO
      AND     ROWNUM=1;
    EXCEPTION WHEN OTHERS THEN
      l_sp_item_cat_id := null;
    END;
     XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('after category :- apecial item' , l_sp_item_cat_id);
   END IF;


    IF othr_details_rec.WC_PIM_CR_OT_CC='99.99RR' OR othr_details_rec.WC_PIM_CR_OT_CC='99.99RT' THEN
      l_sub_inv_val:='Rental';
    ELSE
      l_sub_inv_val:='General';
    END IF;

         BEGIN
            for ITEMS_REC in ITEMS_CUR
            LOOP

            IF ITEMS_REC.ORGANIZATION !='MST' THEN

            l_cogs_acc 		:= null;
            l_sales_acc		:= null;
            l_org_cogs_id 	:= null;
            l_org_sales_id	:= null;
            L_COGS_ACC_VAL 	:=NULL;
            L_SALES_ACC_VAL :=NULL;

                BEGIN
                  SELECT 	a.attribute1, a.attribute2
                    INTO  l_cogs_acc, l_sales_acc
                    FROM  mtl_categories_kfv a
                   WHERE  a.concatenated_segments=othr_details_rec.WC_PIM_CR_OT_CC;
                EXCEPTION WHEN OTHERS THEN
                  l_cogs_acc := null;
                  l_sales_acc:= null;
                END;
                BEGIN
                  SELECT  cost_of_sales_account, sales_account
                    INTO  l_org_cogs_id, l_org_sales_id
                    FROM  mtl_parameters
                   WHERE  organization_id=ITEMS_REC.ORGANIZATION_ID;
                EXCEPTION WHEN OTHERS THEN
                  l_org_cogs_id := null;
                  l_org_sales_id:= null;
                END;

                IF l_cogs_acc is null then
                  l_cogs_acc_val:= l_org_cogs_id;
                ELSE
                    BEGIN
                      select c.code_combination_id
                      into l_cogs_acc_val
                      from gl_code_combinations b, gl_code_combinations c
                      where b.code_combination_id=l_org_cogs_id
                      and b.segment1 = c.segment1
                      and b.segment2 = c.segment2
                      and b.segment3 = c.segment3
                      and b.segment5 = c.segment5
                      and b.segment6 = c.segment6
                      and b.segment7 = c.segment7
                      and c.segment4 = l_cogs_acc;

                    EXCEPTION
                    when others then
                      l_cogs_acc_val:= l_org_cogs_id;
                    END;
                END IF;

                IF l_sales_acc is null then
                  l_sales_acc_val:= l_org_sales_id;
                ELSE
                    BEGIN
                      select c.code_combination_id
                      into l_sales_acc_val
                      from gl_code_combinations b, gl_code_combinations c
                      where b.code_combination_id=l_org_sales_id
                      and b.segment1 = c.segment1
                      and b.segment2 = c.segment2
                      and b.segment3 = c.segment3
                      and b.segment5 = c.segment5
                      and b.segment6 = c.segment6
                      and b.segment7 = c.segment7
                      and c.segment4 = l_sales_acc;

                    EXCEPTION
                    when others then
                    l_sales_acc_val:= l_org_sales_id;
                    END;
                END IF;
              END IF;

                INSERT INTO mtl_system_items_interface (
                             item_number,
                             set_process_id,
                             created_by,
                             creation_date,
                             last_update_date,
                             last_updated_by,
                             transaction_type,
                             process_flag,
                             organization_code,
                             ITEM_CATALOG_GROUP_NAME,
                             ATTRIBUTE22,
                             --Tax code
                             description,
                             long_description,
                             eng_item_flag,
                             shelf_life_days,
                             primary_unit_of_measure,
                             template_id,
                             WEIGHT_UOM_CODE,
                             unit_weight,
                             hazardous_material_flag,
                             hazard_class_id,
                             attribute18,
                             --CONTAINER TYPE
                             attribute9,
                             -- PACKAGE GROUP
                             attribute1,
                             -- CA PROP
                             attribute5,
                             --PESTICITE STATE FLAG
                             attribute6,
                             -- VOC CATEGORY
                             attribute8,
                             --MSDS NO
                             un_number_id,
                             attribute3,
                             --PESTICIDE FLAG
                             attribute4,
                             --VOC GL
                             attribute7,
                             --VOC SUB CAT
                             attribute_category,
                             inventory_item_status_code,
                             buyer_id,
                             list_price_per_unit,
                             full_lead_time,
                             ATTRIBUTE10,
                             --country of origin
                             FIXED_LOT_MULTIPLIER,
                             source_Type,
                             COST_OF_SALES_ACCOUNT,
                             SALES_ACCOUNT
                             )
                    VALUES   (main_details_rec.wc_pim_cr_item_no,
                              l_num_batch_id,
                              l_num_user_id,
                              SYSDATE,
                              SYSDATE,
                              l_num_user_id,
                              'CREATE',
                              1,
                              items_rec.ORGANIZATION,
                              'UNCLASSIFIED',-- commented as ICC is hardcoded
                              --OTHR_DETAILS_REC.WC_PIM_ICC,
                              othr_details_rec.wc_pim_cr_cc_tax_code,
                              main_details_rec.wc_pim_cr_description,
                              main_details_rec.wc_pim_cr_long_desc,
                              'N',
                              TO_NUMBER (main_details_rec.wc_pim_cr_shelf_life_days),
                              main_details_rec.wc_pim_cr_primary_uom,
                              l_num_template_id,
                              phyl_details_rec.WC_PIM_CR_ITEM_PHY_SHIPPI_UUOM,
                              phyl_details_rec.wc_pim_cr_item_phy_shipping_w,
                              SUBSTR (hzmt_details_rec.wc_pim_cr_hz_flag, 1, 1),
                              hzmt_details_rec.wc_hazard_class,
                              HZMT_DETAILS_REC.WC_PIM_CR_HZ_CONTAINER_TYPE,
                              hzmt_details_rec.WC_PIM_CR_Packing_Grp,  -- needs to be commented
                              hzmt_details_rec.wc_pim_cr_hz_ca_prop,
                              HZMT_DETAILS_REC.WC_PIM_CR_PST_FLG_ST,
                              check_number(HZMT_DETAILS_REC.XXWC_VOC_CATEGORY), -- needs to be commented
                              hzmt_details_rec.XXWC_MSDS_NUMBER, -- needs to be commented
                              hzmt_details_rec.xxwc_un_number,
                              decode(hzmt_details_rec.WC_PIM_CR_HZ_PST_FLG,'Yes','Y','No','N'),
                              check_number(HZMT_DETAILS_REC.WC_PIM_CR_HZ_VOC_GL),
                              check_number(hzmt_details_rec.XXWC_VOC_SUBCATEGORY),  -- needs to be commented
                              'WC',
                              'New Item',
                              l_num_buyer_id,
                              purc_details_rec.wc_pim_cr_pc_po_cost,
                              purc_details_rec.xxwc_pim_cr_pc_lead_time,
                              purc_details_rec.wc_pim_cr_pc_coo,
                              PURC_DETAILS_REC.XXWC_PIM_CR_PC_FIX_LT_MY,
                              decode(othr_details_rec.wc_pim_cr_cc_sourced,'DC','1','Branch Direct','2'),
                              l_cogs_acc_val,
                              l_sales_acc_val
                              );

            END LOOP;
      XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Inserting into mtl_item_cat_interface',l_Category_id);
              INSERT INTO mtl_item_categories_interface (
                      item_number,
                      set_process_id,
                      created_by,
                      creation_date,
                      last_update_date,
                      last_updated_by,
                      transaction_type,
                      process_flag,
                      organization_id,
                      category_set_name,
                      category_id,
                      transaction_id)
              VALUES (trim(main_details_rec.wc_pim_cr_item_no),
                      l_num_batch_id,
                      l_num_user_id,
                      SYSDATE,
                      SYSDATE,
                      l_num_user_id,
                      'CREATE',
                      1,
                      l_org_id,
                      'Inventory Category',
                      nvl(l_sp_item_cat_id,l_Category_id),
                      mtl_system_items_interface_s.NEXTVAL);

            COMMIT;
         end;
         XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Submitting incoin request' ,l_num_batch_id);
         l_request_id :=
            apps.fnd_request.submit_request ('INV',-- short name of the application
                                             'INCOIN',-- name of the concurrent pgm
                                             NULL,-- description.n of the request
                                             SYSDATE ,            -- start time
                                             FALSE,
                                             l_org_id ,     -- Master Organization ID
                                             '1' ,          -- all organization
                                             '1',-- validate items
                                             '1',-- process items
                                             '2', -- dont delete processed rows
                                             l_num_batch_id,--process set
                                             '1'-- UPDATE - if item exists then update else it will create item
                                             );
         commit;
            XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Before Wit for request' ,l_request_id);
       l_notes := 'Item No - '||main_details_rec.wc_pim_cr_item_no||', Request Id - '||l_request_id||' submitted for NPR '||l_change_notice||'.'||CHR(13) || CHR(10);
         l_request_wait :=
            fnd_concurrent.wait_for_request (l_request_id,
                                             30,       -- wait 30 sec interval
                                             9999,            -- max wait time
                                             l_chr_phase,
                                             l_chr_status,
                                             l_chr_dev_phase,
                                             l_chr_dev_status,
                                             L_CHR_MSG);

        l_notes :=l_notes || 'Request Completion status '|| l_chr_dev_phase||'.'||CHR(13) || CHR(10);

        for error_Rec in (
          select distinct message_name,column_name,error_message
          from  mtl_interface_Errors
          where request_id=l_request_id
          AND   table_name !='MTL_ITEM_REVISIONS_INTERFACE') loop
          l_item_error_cnt:=1;

          l_item_error_msg :=l_item_error_msg || 'Message Name - '|| error_Rec.message_name ||' on Column '|| error_REc.column_name  ||CHR(13) || CHR(10);
          l_item_error_msg :=l_item_error_msg || 'Error Message - '|| error_Rec.error_message  ||CHR(13) || CHR(10);

          l_notes :=l_notes || 'Message Name - '|| error_Rec.message_name ||' on Column '|| error_REc.column_name  ||CHR(13) || CHR(10);
          l_notes :=l_notes || 'Error Message - '|| error_Rec.error_message  ||CHR(13) || CHR(10);

        end loop;

        IF l_item_error_cnt=1 THEN
            INITIATE_WORKFLOW('ITEM',main_details_rec.wc_pim_cr_item_no||' for NPR '||l_change_notice,l_item_error_msg,l_num_user_id);
        END IF;
    -------------------------------------
    -- Item Creation program had finished
  -- Check if the creation of item was successful and create
        --1) cross ref
        --2) Related items
        --3) Item attachments
    -------------------------------------

      SELECT  DECODE(COUNT(1),0,0,1)
        INTO  L_CHECK_FLAG
        FROM  MTL_SYSTEM_ITEMS_B
       WHERE  SEGMENT1=MAIN_DETAILS_REC.WC_PIM_CR_ITEM_NO
           AND  organization_id=l_org_id;

                  XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC (' L_CHECK_FLAG= >' ,L_CHECK_FLAG);
    -------------------------------------
    -- IF the item has been created then create ref,attachments and related Items
    -------------------------------------
     IF l_check_Flag =1 THEN
       --------------------------
							-- Added w.r.t. ver 1.1 --
							--------------------------
						
       BEGIN
							
										SELECT inventory_item_id
												INTO ln_item_id
												FROM mtl_system_items_b
											WHERE segment1        = main_details_rec.wc_pim_cr_item_no
													AND organization_id = l_org_id;
									
										SELECT attr_group_id 
												INTO ln_internal_attr_grp_id
												FROM ego_attr_groups_v
											WHERE attr_group_name = 'XXWC_INTERNAL_ATTR_AG'
													AND attr_group_type = 'EGO_ITEMMGMT_GROUP';
														
										l_tbl_rec_cnt := l_tbl_rec_cnt + 1;
										t_uda_list_tbl (l_tbl_rec_cnt).attr_group_id  := ln_internal_attr_grp_id;
										t_uda_list_tbl (l_tbl_rec_cnt).attr_name      := cv_attr_vendor;
										t_uda_list_tbl (l_tbl_rec_cnt).attr_value_var := othr_details_rec.wc_pim_cr_cc_ven_no;
										t_uda_list_tbl (l_tbl_rec_cnt).attr_value_num := NULL;
										t_uda_list_tbl (l_tbl_rec_cnt).attr_value_dt  := NULL;
										t_uda_list_tbl (l_tbl_rec_cnt).exception_flag := 'N';
																			
										l_tbl_rec_cnt := l_tbl_rec_cnt + 1;
										t_uda_list_tbl (l_tbl_rec_cnt).attr_group_id  := ln_internal_attr_grp_id;
										t_uda_list_tbl (l_tbl_rec_cnt).attr_name      := cv_attr_coo;       
										t_uda_list_tbl (l_tbl_rec_cnt).attr_value_var := purc_details_rec.wc_pim_cr_pc_coo;
										t_uda_list_tbl (l_tbl_rec_cnt).attr_value_num := NULL;
										t_uda_list_tbl (l_tbl_rec_cnt).attr_value_dt  := NULL;
										t_uda_list_tbl (l_tbl_rec_cnt).exception_flag := 'N';

										IF UPPER (MAIN_DETAILS_REC.WC_PIM_CR_ITEM_TYPE) = 'NEW ITEM'
										THEN
							
													SELECT attr_group_id 
															INTO ln_wcm_attr_grp_id
															FROM ego_attr_groups_v
														WHERE attr_group_name = 'XXWC_WHITE_CAP_MST_AG'
																AND attr_group_type = 'EGO_ITEMMGMT_GROUP';
									
									
													l_tbl_rec_cnt := l_tbl_rec_cnt + 1;
													t_uda_list_tbl (l_tbl_rec_cnt).attr_group_id  := ln_wcm_attr_grp_id;
													t_uda_list_tbl (l_tbl_rec_cnt).attr_name      := cv_attr_a_f_1;       
													t_uda_list_tbl (l_tbl_rec_cnt).attr_value_var := 'f_'||main_details_rec.wc_pim_cr_item_no||'.jpg';
													t_uda_list_tbl (l_tbl_rec_cnt).attr_value_num := NULL;
													t_uda_list_tbl (l_tbl_rec_cnt).attr_value_dt  := NULL;
													t_uda_list_tbl (l_tbl_rec_cnt).exception_flag := 'N';
									
													l_tbl_rec_cnt := l_tbl_rec_cnt + 1;
													t_uda_list_tbl (l_tbl_rec_cnt).attr_group_id  := ln_wcm_attr_grp_id;
													t_uda_list_tbl (l_tbl_rec_cnt).attr_name      := cv_attr_a_t_1;       
													t_uda_list_tbl (l_tbl_rec_cnt).attr_value_var := 't_'||main_details_rec.wc_pim_cr_item_no||'.jpg';
													t_uda_list_tbl (l_tbl_rec_cnt).attr_value_num := NULL;
													t_uda_list_tbl (l_tbl_rec_cnt).attr_value_dt  := NULL;
													t_uda_list_tbl (l_tbl_rec_cnt).exception_flag := 'N';
										
										END IF;						
						
						
										---------------------------------------------------------------------
										-- Looping through all attributes to build ego_user_attr_data_obj  --
										---------------------------------------------------------------------
										IF NVL (t_uda_list_tbl.COUNT, 0) > 0
										THEN

													ln_attr_grp_id  := NULL;
													ln_pre_attr_grp_id := -9999;
										
													FOR ln_rec_num IN t_uda_list_tbl.FIRST .. t_uda_list_tbl.LAST
													LOOP       


																ln_attr_grp_id := t_uda_list_tbl(ln_rec_num).attr_group_id;
												
																IF ln_attr_grp_id <> ln_pre_attr_grp_id
																THEN												
													
																			ln_grp_identifier := ln_grp_identifier + 1;

																			lv_data_level := 'ITEM_LEVEL';
																			l_attributes_row_table.EXTEND ();
																			l_attributes_row_table (ln_grp_identifier) :=
																			ego_user_attrs_data_pub.build_attr_group_row_object
																						(p_row_identifier         => ln_grp_identifier,
																							p_attr_group_id          => ln_attr_grp_id,														
																							p_attr_group_app_id      => l_num_app_id,
																							p_attr_group_type        => cv_ego_itemmgmt_group,
																							p_attr_group_name        => NULL,
																							p_data_level             => lv_data_level,
																							p_data_level_1           => NULL,
																							p_data_level_2           => NULL,
																							p_data_level_3           => NULL,
																							p_data_level_4           => NULL,
																							p_data_level_5           => NULL,
																							p_transaction_type       => ego_user_attrs_data_pvt.g_sync_mode
																						);					

																END IF;
												
																ln_pre_attr_grp_id := t_uda_list_tbl(ln_rec_num).attr_group_id;																			
							
																BEGIN																			
																
																			ln_attr_identifier := ln_attr_identifier + 1;
																			l_attributes_data_table.EXTEND ();
																			l_attributes_data_table (ln_attr_identifier) :=
																			ego_user_attr_data_obj
																														(ln_grp_identifier,
																															t_uda_list_tbl (ln_rec_num).attr_name,
																															t_uda_list_tbl (ln_rec_num).attr_value_var,
																															t_uda_list_tbl (ln_rec_num).attr_value_num,
																															t_uda_list_tbl (ln_rec_num).attr_value_dt,
																															NULL,
																															NULL,
																															NULL 
																														);																															
																																
																EXCEPTION
																			WHEN OTHERS
																			THEN
																						
																						lv_err_msg := 'Error '||SQLERRM;
																						XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Error while setting UDA data object API :',lv_err_msg);

																END;																
										   END LOOP;
							   END IF;                                --NVL(t_uda_list_tbl.COUNT,0) > 0
							
										BEGIN
														x_message_list.DELETE;
														lv_return_status := NULL;
														ln_msg_count := NULL;
														lv_msg_data := NULL;
														ego_item_pub.process_user_attrs_for_item
																															(p_api_version                  => 1.0,
																																p_inventory_item_id            => ln_item_id,
																																p_organization_id              => l_org_id,
																																p_attributes_row_table         => l_attributes_row_table,
																																p_attributes_data_table        => l_attributes_data_table,
																																p_entity_id                    => NULL,
																																p_entity_index                 => NULL,
																																p_entity_code                  => NULL,
																																p_debug_level                  => 0,
																																p_init_error_handler           => fnd_api.g_true,
																																p_write_to_concurrent_log      => fnd_api.g_false,
																																p_init_fnd_msg_list            => fnd_api.g_false,
																																p_log_errors                   => fnd_api.g_true,
																																p_add_errors_to_fnd_stack      => fnd_api.g_false,
																																p_commit                       => fnd_api.g_false,
																																x_failed_row_id_list           => l_failed_row_id_list,
																																x_return_status                => lv_return_status,
																																x_errorcode                    => ln_errorcode,
																																x_msg_count                    => ln_msg_count,
																																x_msg_data                     => lv_msg_data
																															);
														COMMIT;

														IF (lv_return_status <> fnd_api.g_ret_sts_success)
														THEN
																	error_handler.get_message_list (x_message_list      => x_message_list);
																	
																	FOR l_num_loop_count IN 1 .. x_message_list.COUNT
																	LOOP
																				lv_err_msg :=
																							SUBSTR
																										(   'ego_item_pub.process_user_attrs_for_item Return Message '
																											|| l_num_loop_count
																											|| ' '
																											|| x_message_list (l_num_loop_count).MESSAGE_TEXT,
																											1,
																											4000
																										);
																	END LOOP;
																	
																	XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('UDA process API failed :',lv_err_msg);

														END IF;
											EXCEPTION
														WHEN OTHERS
														THEN
														
													    lv_err_msg :=	SQLERRM;
													    XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Error from UDA process block :',lv_err_msg);

											END;
										
							EXCEPTION
							
										WHEN OTHERS
										THEN
										
													lv_err_msg :=	SQLERRM;
													XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC ('Error from main block :',lv_err_msg);
							
							END;							
					---------------------------------------------
					-- End of code modification w.r.t. ver 1.1 --
					---------------------------------------------
					
              SELECT  inventory_item_id
                INTO  l_inventory_item_id
                FROM  mtl_system_items_b
               WHERE  SEGMENT1=MAIN_DETAILS_REC.WC_PIM_CR_ITEM_NO
                 AND    ORGANIZATION_ID=l_org_id;
                 XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC (' l_inventory_item_id= >' ,l_inventory_item_id);
                IF othr_details_rec.wc_pim_cr_cc_upc IS NOT NULL THEN
                XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC (' creating cross ref with = >' ,othr_details_rec.wc_pim_cr_cc_upc);
                CREATE_CROSS_REFERENCE(P_TRANSACTION_TYPE     => 'CREATE',
                                        P_ITEM_ID             => L_INVENTORY_ITEM_ID,
                                        P_ORG_ID              => null,-- if org id is selected org independant flag should be null
                                        P_CROSS_REF_TYPE      => 'UPC',
                                        P_CROSS_REF_VALUE     => othr_details_rec.wc_pim_cr_cc_upc,
                                        p_org_independent_flag=> 'Y');
                END IF;

                IF main_details_rec.wc_pim_cr_mfg_no_1 IS NOT NULL THEN
                XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC (' creating vendor ref with = >' ,main_details_rec.wc_pim_cr_mfg_no_1);
                CREATE_CROSS_REFERENCE(P_TRANSACTION_TYPE     => 'CREATE',
                                        P_ITEM_ID             => L_INVENTORY_ITEM_ID,
                                        P_ORG_ID              => null,  -- if org id is selected org independant flag should be null
                                        P_CROSS_REF_TYPE      => 'VENDOR',
                                        P_CROSS_REF_VALUE     => main_details_rec.wc_pim_cr_mfg_no_1,
                                        p_org_independent_flag=> 'Y');
                END IF;

                IF purc_details_rec.wc_pim_Cr_pc_rpl_item ='Yes'
                  AND purc_details_rec.wc_pim_cr_pc_rpl_itm_no IS NOT NULL THEN
                    XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC (' creating  related items with = >' ,purc_details_rec.wc_pim_cr_pc_rpl_itm_no);

                      SELECT  inventory_item_id
                        INTO  l_related_item_id
                        FROM  mtl_system_items_b
                       WHERE  segment1=purc_details_rec.wc_pim_cr_pc_rpl_itm_no
                         AND    organization_id=l_org_id;

                    CREATE_RELATED_ITEMS ( P_ITEM_ID               => l_inventory_item_id,
                                P_ORG_ID               => l_org_id,
                                P_RELATED_ITEM_ID      => l_related_item_id,
                                P_RELATIONSHIP_TYPE_ID => 2,
                                p_user_id              => l_num_user_id);
                  END IF;

                  IF othr_details_rec.wc_pim_cr_cc_ven_no IS NOT NULL THEN
                    l_string :='Vendor # -'||othr_details_rec.wc_pim_cr_cc_ven_no;
                  END IF;
                  IF othr_details_rec.wc_pim_cr_cc_price IS NOT NULL THEN
                    l_string := l_string || '  Price List - '|| othr_details_rec.wc_pim_cr_cc_price;
                  END IF;

                  IF l_string IS NOT NULL THEN

                  SELECT  revision_id
                    INTO  l_revision_id
                    FROM  mtl_item_revisions
                   WHERE  inventory_item_id=l_inventory_item_id
                     AND     organization_id= l_org_id;
                      XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC (' creating  attachments with = >' ,main_details_rec.wc_pim_cr_item_no||'_Attachment');
                    CREATE_ATTACHMENTS(P_DESCRIPTION              => main_details_rec.wc_pim_cr_item_no,
                                P_FILE_NAME           => main_details_rec.wc_pim_cr_item_no||'_Attachment',
                                P_DOC_TEXT             => l_string,
                                P_ITEM_ID           => l_inventory_item_id,
                                P_ORG_ID              => l_org_id,
                                P_REVISION_ID         => l_revision_id,
                                p_attached_doc_id     => l_attached_doc_id
                                );
                  END IF;


                  FOR ITEMS_REC IN ITEMS_CUR
                  LOOP

                    IF ITEMS_REC.ORGANIZATION!='MST' THEN
                      INSERT INTO MTL_ITEM_SUB_DEFAULTS
                          (INVENTORY_ITEM_ID,
                          ORGANIZATION_ID,
                          SUBINVENTORY_CODE,
                          DEFAULT_TYPE,
                          LAST_UPDATE_DATE,
                          LAST_UPDATED_BY,
                          CREATION_DATE,
                          CREATED_BY,
                          LAST_UPDATE_LOGIN)
                      VALUES
                          (L_INVENTORY_ITEM_ID,
                          ITEMS_REC.ORGANIZATION_ID,
                          l_sub_inv_val,
                          2,
                          SYSDATE,
                          L_NUM_USER_ID,
                          SYSDATE,
                          L_NUM_USER_ID,
                          L_NUM_USER_ID);
                    END IF;

                  END LOOP;
          COMMIT;
         --to get sequence no
         BEGIN
                SELECT   alpha_prefix || next_available_number,
                         b.change_order_type_id
                  INTO   l_change_number, l_chng_type_id
                  FROM   eng_auto_number_ecn a, eng_change_order_types_vl b
                 WHERE   a.change_type_id = b.change_order_type_id
                         AND b.change_order_type = l_chng_type
            FOR UPDATE   OF next_available_number;

            UPDATE   eng_auto_number_ecn
               SET   next_available_number = next_available_number + 1
             WHERE   change_type_id = l_chng_type_id;

            COMMIT;
         end;
                  XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC (' l_change_number' ,l_change_number);
         BEGIN
            SELECT   user_name
              INTO   l_chr_user
              FROM   fnd_user
             WHERE   user_id = fnd_global.user_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;


         --  l_eco_name                 := get_next_change_number (l_chng_type_id);
         p_eco_rec.eco_name := l_change_number;
         p_eco_rec.organization_code := l_org_code;
         P_ECO_REC.CHANGE_NAME := L_CHANGE_NUMBER;
         p_eco_rec.description :=  'New Product Workflow Change order -'|| l_change_number||' created for the NPR -'||l_change_notice ||'.Item No- '||l_item_no;
         p_eco_rec.requestor := l_chr_user;
         p_eco_rec.assignee := l_chr_user;
         p_eco_rec.approval_status_name := l_apprvl_sts_name;
         p_eco_rec.approval_date := SYSDATE;
         p_eco_rec.approval_request_date := SYSDATE;
         p_eco_rec.change_type_code := l_chng_type;
         p_eco_rec.transaction_type := 'CREATE';
         p_eco_rec.plm_or_erp_change := 'PLM';

         -----REVISED ITEMS
         IF p_revised_item_tbl.EXISTS (1) IS NOT NULL
         THEN
            p_revised_item_tbl.DELETE;
         END IF;

         p_revised_item_tbl (1).eco_name := l_change_number;
         P_REVISED_ITEM_TBL (1).ORGANIZATION_CODE := l_org_code;
         p_revised_item_tbl (1).revised_item_name := main_details_rec.wc_pim_cr_item_no;
         p_revised_item_tbl (1).start_effective_date := SYSDATE;
         p_revised_item_tbl (1).new_effective_date := SYSDATE + 2;
         p_revised_item_tbl (1).transaction_type := 'CREATE';
         XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC (' Before Change order creation' ,main_details_rec.wc_pim_cr_item_no);
         eng_eco_pub.process_eco (
            p_api_version_number     => 1.0,
            p_init_msg_list          => FALSE,
            x_return_status          => x_return_status,
            x_msg_count              => x_msg_count,
            p_bo_identifier          => 'ECO',
            p_eco_rec                => p_eco_rec,
            p_eco_revision_tbl       => p_eco_revision_tbl,
            p_change_line_tbl        => l_change_line_tbl,
            p_revised_item_tbl       => p_revised_item_tbl,
            p_rev_component_tbl      => p_rev_component_tbl,
            p_ref_designator_tbl     => p_ref_designator_tbl,
            p_sub_component_tbl      => p_sub_component_tbl,
            p_rev_operation_tbl      => x_rev_operation_tbl,
            p_rev_op_resource_tbl    => x_rev_op_resource_tbl,
            p_rev_sub_resource_tbl   => x_rev_sub_resource_tbl,
            x_eco_rec                => x_eco_rec,
            x_eco_revision_tbl       => x_eco_revision_tbl,
            x_change_line_tbl        => x_change_line_tbl,
            x_revised_item_tbl       => x_revised_item_tbl,
            x_rev_component_tbl      => x_rev_component_tbl,
            x_ref_designator_tbl     => x_ref_designator_tbl,
            x_sub_component_tbl      => x_sub_component_tbl,
            x_rev_operation_tbl      => x_rev_operation_tbl,
            x_rev_op_resource_tbl    => x_rev_op_resource_tbl,
            x_rev_sub_resource_tbl   => x_rev_sub_resource_tbl,
            p_debug                  => 'N',
            p_output_dir             => NULL,
            p_debug_filename         => 'ECO_BO_Debug.log'
         );
         COMMIT;

         l_notes:=l_notes || 'NPW Change order '||l_change_number ||'. Return status -'|| X_RETURN_STATUS ||'.'|| chr(13)||chr(10);
         XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Return Status ',X_RETURN_STATUS);
         IF (x_return_status <> fnd_api.g_ret_sts_success)
         THEN
            error_handler.get_message_list (x_message_list);

            FOR i IN 1 .. x_msg_count
            LOOP
                l_npw_error_cnt:=1;
               error_msg(i).MESSAGE_TEXT := x_message_list (i).MESSAGE_TEXT;
               l_npw_error_msg:=  l_npw_error_msg||error_msg (i).MESSAGE_TEXT  ||CHR(13) || CHR(10);
               XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Error Message',error_msg (i).MESSAGE_TEXT);
               DBMS_OUTPUT.put_line (error_msg (i).MESSAGE_TEXT);
            END LOOP;
         ELSE
            COMMIT;
             XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Successfully Created ECO ','');
            DBMS_OUTPUT.put_line ('Successfully Created ECO ');
         END IF;

        IF l_npw_error_cnt=1 THEN
            INITIATE_WORKFLOW('NPW',l_change_number,l_npw_error_msg,l_num_user_id);
        END IF;

           END IF; --l_check_Flag =1 , meaning if item is not created then do nothing

      ----------------------------------------------------------
      -- code to insert into notes starts here
      ----------------------------------------------------------
          SELECT  EGO_EXTFWK_S.NEXTVAL
            INTO  l_extension_id
            FROM  DUAL;

          INSERT INTO eng_changes_Ext_b
          (extension_id,
          change_id,
          change_type_id,
          attr_group_id,
          created_by,
          creation_Date,
          last_updated_by,
          last_update_Date,
          last_update_login)
          VALUES
          ( l_extension_id,
          p_in_num_change_id,
          (select change_order_type_id from eng_engineering_changes where change_id=p_in_num_change_id and rownum=1),
          (select attr_Group_id from ego_attr_groups_V where attr_group_type='ENG_CHANGEMGMT_GROUP' and attr_group_name='XXWC_NOTES'),
          l_num_user_id,
          sysdate,
          l_num_user_id,
          sysdate,
          l_num_user_id);

          insert into eng_changes_Ext_tl
          (extension_id,
          change_id,
          change_type_id,
          attr_group_id,
          source_lang,
          language,
          created_by,
          creation_Date,
          last_updated_by,
          last_update_Date,
          last_update_login,
          tl_ext_Attr1)
          values
          (l_extension_id ,
          p_in_num_change_id,
          (select change_order_type_id from eng_engineering_changes where change_id=p_in_num_change_id and rownum=1),
          (select attr_Group_id from ego_attr_groups_V where attr_group_type='ENG_CHANGEMGMT_GROUP' and attr_group_name='XXWC_NOTES'),
          'US',
          'US',
          l_num_user_id,
          sysdate,
          l_num_user_id,
          sysdate,
          l_num_user_id,
          substr(l_notes,1,1000));
          COMMIT;

      EXCEPTION WHEN OTHERS THEN

          SELECT  EGO_EXTFWK_S.NEXTVAL
            INTO  l_extension_id
            FROM  DUAL;

          INSERT INTO eng_changes_Ext_b
          (extension_id,
          change_id,
          change_type_id,
          attr_group_id,
          created_by,
          creation_Date,
          last_updated_by,
          last_update_Date,
          last_update_login)
          VALUES
          ( l_extension_id,
          p_in_num_change_id,
          (select change_order_type_id from eng_engineering_changes where change_id=p_in_num_change_id and rownum=1),
          (select attr_Group_id from ego_attr_groups_V where attr_group_type='ENG_CHANGEMGMT_GROUP' and attr_group_name='XXWC_NOTES'),
          l_num_user_id,
          sysdate,
          l_num_user_id,
          sysdate,
          l_num_user_id);

          insert into eng_changes_Ext_tl
          (extension_id,
          change_id,
          change_type_id,
          attr_group_id,
          source_lang,
          language,
          created_by,
          creation_Date,
          last_updated_by,
          last_update_Date,
          last_update_login,
          tl_ext_Attr1)
          values
          (l_extension_id ,
          p_in_num_change_id,
          (select change_order_type_id from eng_engineering_changes where change_id=p_in_num_change_id and rownum=1),
          (select attr_Group_id from ego_attr_groups_V where attr_group_type='ENG_CHANGEMGMT_GROUP' and attr_group_name='XXWC_NOTES'),
          'US',
          'US',
          l_num_user_id,
          sysdate,
          l_num_user_id,
          sysdate,
          l_num_user_id,
          substr(l_notes,1,1000));


            INITIATE_WORKFLOW('NPW',l_change_number,l_notes,l_num_user_id);

          COMMIT;
      END; --end of begin for IF (l_sumbit_prog = 1)

      END IF;         -- End of l_submit_prog =1

    -------------------------------------
    -- Code to change the status of Item to Active
  -- If the item is a WebItem raise a NWP change order
    -------------------------------------

      IF (l_sumbit_active_prog = 1) THEN

      --Once the Status for the NPW is approved , activate Item
      -- and create NWP change order if its a Web Item
      l_chng_type:='New Web Product';

      SELECT change_notice
        INTO  l_change_notice
        FROM eng_engineering_changes
       WHERE change_id = p_in_num_change_id;

       BEGIN
          BEGIN
             SELECT   responsibility_id, application_id
               INTO   l_num_resp_id, l_num_app_id
               FROM   fnd_responsibility
              WHERE   responsibility_key = 'EGO_PIM_DATA_LIBRARIAN';
          EXCEPTION
             WHEN OTHERS
             THEN
                NULL;
          END;
          BEGIN
             SELECT   created_by
               INTO   l_num_user_id
               FROM   eng_lifecycle_statuses
              WHERE   entity_id1 = p_in_num_change_id AND ROWNUM = 1;
          EXCEPTION
             WHEN OTHERS
             THEN
                NULL;
          END;
          fnd_global.apps_initialize (user_id        => l_num_user_id,
                                      resp_id        => l_num_resp_id,
                                      resp_appl_id   => l_num_app_id);
      END;
       BEGIN
          SELECT   mtl_system_items_intf_sets_s.NEXTVAL
            INTO   l_num_set_process_id
            FROM   DUAL;
       EXCEPTION
          WHEN OTHERS
          THEN
             NULL;
       END;

         BEGIN
            FOR rev_items_rec IN rev_items_cur
            LOOP
            --Capture Item no to be used for creating NWP
              l_inventory_item_id :=rev_items_rec.inventory_item_id;
              l_item_no:=rev_items_rec.item_number;
               BEGIN
                  INSERT INTO mtl_system_items_interface (
                               inventory_item_id,
                               item_number,
                               set_process_id,
                               created_by,
                               creation_date,
                               last_update_date,
                               last_updated_by,
                               transaction_type,
                               process_flag,
                               organization_id,
                               inventory_item_status_code
                             )
                    VALUES   (rev_items_rec.inventory_item_id,
                              rev_items_rec.item_number,
                              l_num_set_process_id,
                              l_num_user_id,
                              SYSDATE,
                              SYSDATE,
                              l_num_user_id,
                              'UPDATE',
                              1,
                              rev_items_rec.organization_id,
                              'Active');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;
            END LOOP;

            COMMIT;
            l_num_request_id :=
               apps.fnd_request.submit_request ('INV',-- short name of the application
                                                'INCOIN',-- name of the concurrent pgm
                                                NULL,-- description.n of the request
                                                SYSDATE,-- start time
                                                FALSE,
                                                l_org_id,-- Master Organization ID
                                                '1',-- all organization
                                                '1',-- validate items                          ,
                                                '1',-- process items                          ,
                                                '2',-- dont delete processed rows
                                                l_num_set_process_id,--process set                                                             ,
                                                '2'-- UPDATE - if item exists then update else it will create item
                                                );
            COMMIT;

    IF check_web_item(l_inventory_item_id)=1 THEN
         --to get sequence no
         BEGIN
                SELECT   alpha_prefix || next_available_number,
                         b.change_order_type_id
                  INTO   l_change_number, l_chng_type_id
                  FROM   eng_auto_number_ecn a, eng_change_order_types_vl b
                 WHERE   a.change_type_id = b.change_order_type_id
                         AND b.change_order_type = l_chng_type
            FOR UPDATE   OF next_available_number;

            UPDATE   eng_auto_number_ecn
               SET   next_available_number = next_available_number + 1
             WHERE   change_type_id = l_chng_type_id;

            COMMIT;
         end;
                  XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC (' l_change_number for NWP ' ,l_change_number);

         BEGIN
            SELECT   user_name
              INTO   l_chr_user
              FROM   fnd_user
             WHERE   user_id = fnd_global.user_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         p_eco_rec.eco_name := l_change_number;
         p_eco_rec.organization_code := l_org_code;
         p_eco_rec.change_name := l_change_number;
         p_eco_rec.description := 'New Web Product Change Order-'|| l_change_number ||' created for the NPW -'||l_change_notice||'. Item No - '||l_item_no;
         p_eco_rec.requestor := l_chr_user;
         p_eco_rec.assignee := l_chr_user;
         p_eco_rec.approval_status_name := l_apprvl_sts_name;
         p_eco_rec.approval_date := SYSDATE;
         p_eco_rec.approval_request_date := SYSDATE;
         p_eco_rec.change_type_code := l_chng_type;
         p_eco_rec.transaction_type := 'CREATE';
         p_eco_rec.plm_or_erp_change := 'PLM';

         -----REVISED ITEMS
         IF p_revised_item_tbl.EXISTS (1) IS NOT NULL
         THEN
            p_revised_item_tbl.DELETE;
         END IF;

         p_revised_item_tbl (1).eco_name := l_change_number;
         P_REVISED_ITEM_TBL (1).ORGANIZATION_CODE := l_org_code;
         p_revised_item_tbl (1).revised_item_name := l_item_no;
         p_revised_item_tbl (1).start_effective_date := SYSDATE;
         p_revised_item_tbl (1).new_effective_date := SYSDATE + 2;
         p_revised_item_tbl (1).transaction_type := 'CREATE';

         XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC (' Before Change order creationfor NWP' ,l_item_no);

         eng_eco_pub.process_eco (
            p_api_version_number     => 1.0,
            p_init_msg_list          => FALSE,
            x_return_status          => x_return_status,
            x_msg_count              => x_msg_count,
            p_bo_identifier          => 'ECO',
            p_eco_rec                => p_eco_rec,
            p_eco_revision_tbl       => p_eco_revision_tbl,
            p_change_line_tbl        => l_change_line_tbl,
            p_revised_item_tbl       => p_revised_item_tbl,
            p_rev_component_tbl      => p_rev_component_tbl,
            p_ref_designator_tbl     => p_ref_designator_tbl,
            p_sub_component_tbl      => p_sub_component_tbl,
            p_rev_operation_tbl      => x_rev_operation_tbl,
            p_rev_op_resource_tbl    => x_rev_op_resource_tbl,
            p_rev_sub_resource_tbl   => x_rev_sub_resource_tbl,
            x_eco_rec                => x_eco_rec,
            x_eco_revision_tbl       => x_eco_revision_tbl,
            x_change_line_tbl        => x_change_line_tbl,
            x_revised_item_tbl       => x_revised_item_tbl,
            x_rev_component_tbl      => x_rev_component_tbl,
            x_ref_designator_tbl     => x_ref_designator_tbl,
            x_sub_component_tbl      => x_sub_component_tbl,
            x_rev_operation_tbl      => x_rev_operation_tbl,
            x_rev_op_resource_tbl    => x_rev_op_resource_tbl,
            x_rev_sub_resource_tbl   => x_rev_sub_resource_tbl,
            p_debug                  => 'N',
            p_output_dir             => NULL,
            p_debug_filename         => 'ECO_BO_Debug.log'
         );

            COMMIT;

             XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Return Status for NWP1',X_RETURN_STATUS);

              XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('value for l_change_notice',l_change_notice);
              XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('value BEFORE UPDATE l_change_number',l_change_number);

              BEGIN

         Update eng_engineering_changes
         set    description=description||' NWP Change order no '||l_change_number
         where  change_notice=l_change_notice;
         EXCEPTION
         WHEN OTHERS THEN
          XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('ERROR IN UPDATE',sqlerrm);
          COMMIT;
         END;

          XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('after update',l_change_notice);

         COMMIT;

        XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Return Status for NWP',X_RETURN_STATUS);

         IF (x_return_status <> fnd_api.g_ret_sts_success)
         THEN
            error_handler.get_message_list (x_message_list);
            FOR i IN 1 .. x_msg_count
            LOOP
            l_nwp_error_cnt:=1;
               error_msg (i).MESSAGE_TEXT := x_message_list (i).MESSAGE_TEXT;
               l_nwp_error_msg:=l_nwp_error_msg ||error_msg (i).MESSAGE_TEXT  ||CHR(13) || CHR(10);
               XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Error Message for NWP',error_msg (i).MESSAGE_TEXT);
               DBMS_OUTPUT.put_line (error_msg (i).MESSAGE_TEXT);
            END LOOP;
         ELSE
            COMMIT;
             XXWC_EGO_NPB_DBG_PKG.DEBUG_PROC('Successfully Created ECO ','for NWP');
            DBMS_OUTPUT.put_line ('Successfully Created ECO ');
         END IF;

        IF l_nwp_error_cnt=1 THEN
            INITIATE_WORKFLOW('NWP',l_change_number,l_nwp_error_msg,l_num_user_id);
        END IF;


        END IF; -- code to raise a NWP change order request ends here
         END; -- end of begin block started just at  IF (l_sumbit_active_prog = 1)
      END IF;
    END IMPORT_ITEM_PROC;
END XXWC_EGO_ITEM_IMPORT_PKG;
/