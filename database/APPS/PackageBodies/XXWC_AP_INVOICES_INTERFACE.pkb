CREATE OR REPLACE package body APPS.XXWC_AP_INVOICES_INTERFACE as
/*************************************************************************
     $Header XXWC_AP_INVOICES_INTERFACE $
     Module Name: XXWC_AP_INVOICES_INTERFACE
     TMS # : 20140714-00052
     PURPOSE:   This package records the choices made in the PO Communication Form so that subsequent visits to the form defaults the correct values.

     REVISIONS:
     Ver        Date        Author                             Description
     ---------  ----------  -------------------------------    -------------------------
     1.0        05/19/2014  Jason Price (Focused E-commerce)   Initial Version
     1.1        09/25/2014    Veera C                               TMS# 20141001-00163 Modified code as per the Canada OU Test
     1.2        01/22/2015  Maharajan Shunmugam                TMS#20141024-00003 AP - EDI - Implement EDI Invoice Match Enhancement
   **************************************************************************/ 

procedure UPDATE_EDI_INVOICES (p_errbuf OUT varchar2, p_retcode OUT varchar2) is
  cursor cursor_po_line_numbers is 
     select aii.invoice_id, aili.invoice_line_id, aili.unit_price, aili.quantity_invoiced, poh.po_header_id
     from AP_INVOICES_INTERFACE aii, AP_INVOICE_LINES_INTERFACE aili, apps.PO_HEADERS poh
     where aii.org_id = 162 and aii.source = 'EDI GATEWAY' and status = 'REJECTED' 
       and aii.vendor_id          = poh.vendor_id        --and aii.vendor_site_id     = poh.vendor_site_id
       and aii.po_number          = poh.segment1
       and aii.invoice_id         = aili.invoice_id 
       and aili.po_line_number    is null
       and aili.unit_price is not null
       and aili.quantity_invoiced is not null;
   v_new_po_line_num_count number(10) := 0;
  
  
   -- We are stripping off the last zero from the AP po line number here.
  cursor cursor_strip_zero_po_line_num is 
  select aii.invoice_id, aii.group_id, aili.invoice_line_id, aili.unit_price, aili.quantity_invoiced, poh.po_header_id, poh.segment1 po_number, aili.po_line_number, min(pol.line_num) new_line_num
     from ap_invoices_interface aii, ap_invoice_lines_interface aili, apps.po_headers poh, apps.po_lines pol
     where aii.org_id = 162 and aii.source = 'EDI GATEWAY' and status = 'REJECTED' 
       and aii.vendor_id          = poh.vendor_id        --and aii.vendor_site_id     = poh.vendor_site_id
       and aii.po_number          = poh.segment1
       and poh.po_header_id       = pol.po_header_id
       and aii.invoice_id         = aili.invoice_id 
       --and aili.invoice_line_id in (select parent_id from AP_INTERFACE_REJECTIONS AIR where AIR.PARENT_TABLE = 'AP_INVOICE_LINES_INTERFACE') -- and air.reject_lookup_code = 'INVALID PO LINE NUM')
       and (aili.unit_price = pol.unit_price or aili.unit_price = round(pol.unit_price,2) or aili.unit_price between pol.unit_price - (pol.unit_price * .05) and  pol.unit_price + (pol.unit_price * .05))
      -- and aili.unit_price = pol.unit_price
       -- and aili.quantity_invoiced = pol.quantity             
       and decode(substr(aili.po_line_number,-1,1),'0', substr(aili.po_line_number,1, length(aili.po_line_number) -1))     = pol.line_num
   group by aii.invoice_id, aii.group_id, aili.invoice_line_id, aili.unit_price,  aili.po_line_number, aili.quantity_invoiced, poh.po_header_id, poh.segment1;
   v_trunc_zero_update_count number(10) := 0;


   -- We are replacing invalid line numbers from the invoice lines with the probable po line numbers. Example invoice po lines refer to 1 and 2 but po lines refer to 3 and 4.
  cursor cursor_different_po_line_num is 
  select aii.invoice_id, aii.group_id, aili.invoice_line_id, aili.unit_price, aili.quantity_invoiced, poh.po_header_id, poh.segment1 po_number, aili.po_line_number, min(pol.line_num) new_line_num
     from ap_invoices_interface aii, ap_invoice_lines_interface aili, apps.po_headers poh, apps.po_lines pol
     where aii.org_id = 162 and aii.source = 'EDI GATEWAY' and status = 'REJECTED' 
       and aii.vendor_id          = poh.vendor_id        --and aii.vendor_site_id     = poh.vendor_site_id
       and aii.po_number          = poh.segment1
       and poh.po_header_id       = pol.po_header_id
       and aii.invoice_id         = aili.invoice_id 
       and aili.invoice_line_id in (select parent_id from AP_INTERFACE_REJECTIONS AIR where AIR.PARENT_TABLE = 'AP_INVOICE_LINES_INTERFACE' and air.reject_lookup_code = 'INVALID PO LINE NUM')
       and aili.unit_price        = pol.unit_price
       and not exists (select 1 from ap_invoice_lines_interface aili2 where aili2.invoice_id = aii.invoice_id and pol.line_num = aili2.po_line_number)  
   group by aii.invoice_id, aii.group_id, aili.invoice_line_id, aili.unit_price,  aili.po_line_number, aili.quantity_invoiced, poh.po_header_id, poh.segment1;
    v_nonexistant_po_num_count number(10) := 0;
    
    --Added below cursor for ver 1.2
CURSOR update_edi_source IS
SELECT aii.invoice_id, aii.GROUP_ID, aii.source
  FROM ap_invoices_interface aii
 WHERE     invoice_id IN (SELECT aili.invoice_id
                            FROM ap_invoice_lines_interface aili,
                                 ap_interface_rejections air
                           WHERE     1 = 1
                                 AND aili.invoice_id = aii.invoice_id
                                 AND aili.invoice_line_id = air.parent_id
                                 AND air.reject_lookup_code = 'UNIT PRC NOT EQUAL TO PO'
                                 AND parent_table           = 'AP_INVOICE_LINES_INTERFACE')
                                 AND aii.source = 'EDI GATEWAY'
                                 AND aii.status = 'REJECTED';

   l_sec                    VARCHAR2 (200);
   l_msg                         VARCHAR2 (150);
   l_distro_list                 VARCHAR2 (75)   DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_req_id                   NUMBER := fnd_global.conc_request_id;


  
begin
l_sec := ' Entering into first cursor';
  for cur_po_line_numbers in cursor_po_line_numbers loop
    declare
      v_new_po_line_num  number(10);   
    begin   
      select min(pol.line_num) into v_new_po_line_num from apps.PO_LINES pol
       where cur_po_line_numbers.po_header_id = pol.po_header_id
         and ((cur_po_line_numbers.unit_price = pol.unit_price and cur_po_line_numbers.quantity_invoiced = pol.quantity))
         and not exists (select 1 from ap_invoice_lines_interface aili where cur_po_line_numbers.invoice_id = aili.invoice_id and pol.line_num = aili.po_line_number); --OR (aili.UNIT_PRICE        = pol.unit_price and aili.quantity_invoiced = pol.quantity)
   
      update AP_INVOICE_LINES_INTERFACE AILI set AILI.PO_LINE_NUMBER = v_new_po_line_num, description = substrb(description||' EDI PO Line Changed from '||po_line_number||' to '||v_new_po_line_num||' ',1, 240) where aili.invoice_line_id = cur_po_line_numbers.invoice_line_id;
      v_new_po_line_num_count := v_new_po_line_num_count + 1;
     l_msg := ' Error at updating PO line number and description ';
    exception 
       when no_data_found then null;  
       when others then FND_FILE.PUT_LINE(FND_FILE.LOG,'Error on PO header id: '||to_char(cur_po_line_numbers.po_header_id));
    end;
  end loop;
  FND_FILE.PUT_LINE(FND_FILE.LOG,'Number of invoice lines updated with PO Line Numbers: '||to_char(v_new_po_line_num_count));
  commit;

l_sec := ' Entering into second cursor';  

 for cur_strip_zero_po_line_num in cursor_strip_zero_po_line_num loop
   declare
   begin
      update AP_INVOICE_LINES_INTERFACE AILI set AILI.PO_LINE_NUMBER = cur_strip_zero_po_line_num.new_line_num, description = substrb(description||' Strip zero EDI PO Line Changed from '||po_line_number||' to '||cur_strip_zero_po_line_num.new_line_num||' ',1, 240) where aili.invoice_line_id = cur_strip_zero_po_line_num.invoice_line_id;
      v_trunc_zero_update_count := v_trunc_zero_update_count + 1;
     l_msg := ' Error at updating PO line number and description ';
    exception 
       --when no_data_found then null;  
       when others then FND_FILE.PUT_LINE(FND_FILE.LOG,'Error on PO header id: '||to_char(cur_strip_zero_po_line_num.po_header_id));
    end;
  end loop;
  FND_FILE.PUT_LINE(FND_FILE.LOG,'Number of invoice lines updated with truncated PO Line Numbers: '||to_char(v_trunc_zero_update_count));
  commit;

l_sec := ' Entering into third cursor'; 
  
 for cur_different_po_line_num in cursor_different_po_line_num loop
   declare
   begin
      update AP_INVOICE_LINES_INTERFACE AILI set AILI.PO_LINE_NUMBER = cur_different_po_line_num.new_line_num, description = substrb(description||' Non existant EDI PO Line Changed from '||po_line_number||' to '||cur_different_po_line_num.new_line_num||' ',1, 240) where aili.invoice_line_id = cur_different_po_line_num.invoice_line_id;
      v_nonexistant_po_num_count := v_nonexistant_po_num_count + 1;
     l_msg := ' Error at updating PO line number and description ';
    exception 
       --when no_data_found then null;  
       when others then FND_FILE.PUT_LINE(FND_FILE.LOG,'Error on PO header id: '||to_char(cur_different_po_line_num.po_header_id));
    end;
  end loop;
  FND_FILE.PUT_LINE(FND_FILE.LOG,'Number of invoice lines updated with PO Line Numbers that actually exist on the PO: '||to_char(v_nonexistant_po_num_count));
  commit;
  
  --Added below for ver 1.2
l_sec := 'opening cursor to update EDI source';
 FOR C4 IN update_edi_source 
 LOOP
 BEGIN
 UPDATE ap_invoices_interface
  SET source = 'EDI2'
 WHERE invoice_id = c4.invoice_id 
  AND  group_id = c4.group_id;
l_msg := ' Error at updating the source EDI GATEWAY to EDI2 ';
EXCEPTION
WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_AP_INVOICES_INTERFACE',
                  p_calling             => l_sec,
                  p_request_id          => l_req_id,
                  p_ora_error_msg       => l_msg||sqlerrm,
                  p_error_desc          => 'Error running XXWC_AP_INVOICES_INTERFACE.UPDATE_EDI_INVOICES WHEN OTHERS EXCEPTION',
                  p_distribution_list   => l_distro_list,
                  p_module              => 'AP');
 END;
 END LOOP;
 COMMIT;
  EXCEPTION
  WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_AP_INVOICES_INTERFACE',
                  p_calling             => l_sec,
                  p_request_id          => l_req_id,
                  p_ora_error_msg       => l_msg||sqlerrm,
                  p_error_desc          => 'Error running XXWC_AP_INVOICES_INTERFACE.UPDATE_EDI_INVOICES WHEN OTHERS EXCEPTION',
                  p_distribution_list   => l_distro_list,
                  p_module              => 'AP');

end;
end;
/
