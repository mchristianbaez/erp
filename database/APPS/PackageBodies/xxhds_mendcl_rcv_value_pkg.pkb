create or replace package body apps.xxhds_mendcl_rcv_value_pkg as
/*+
 -- Date: 12-DEC-2012
 -- Author: Balaguru Seshadri
 -- Scope: This package will enhance HD Supply to run the Oracle standard period close reconciliation report for multiple
 -- inventory organizations just by provicing the period alone.
 -- ESMS ticket 182083
 -- Parameters:
*/

 -- Global variables
 G_Application    Fnd_Application.Application_Short_Name%Type :='PO';
 G_Cp_Short_Code  Fnd_Concurrent_Programs.Concurrent_Program_Name%Type :='POXRRCVV'; --Period Close Reconcialiation Report

  procedure main (
    retcode               out number
   ,errbuf                out varchar2
   ,p_as_of_date          in  varchar2   
  ) is
    
    CURSOR wc_inv_orgs (n_org_id IN NUMBER) IS
      SELECT organization_id,
             organization_code,
             organization_name,
             set_of_books_id,
             chart_of_accounts_id,
             legal_entity
        FROM org_organization_definitions
       WHERE 1 = 1         
         AND operating_unit =n_org_id 
         AND disable_date IS NULL
         AND organization_code NOT IN ('WCC', 'MST') --WC Corporate Organization, WC Item Master
         --AND rownum <4    --testing for upto 7 organizations 
    ORDER BY organization_code ASC;
    
    type wc_inv_orgs_tbl is table of wc_inv_orgs%rowtype index by binary_integer;
    wc_inv_orgs_rec wc_inv_orgs_tbl;        
  
   lc_request_data VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   n_count_child_requests NUMBER :=0; 
   N_conc_pgm_id NUMBER :=0;
   N_running_count NUMBER :=0;
   b_keep_looping BOOLEAN;
   n_program_app_id NUMBER :=0;
   N_user_id NUMBER :=0;
   v_email       fnd_user.email_address%type :=Null;
   v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null; 
   v_cp_phase       Varchar2(40) :=Null;
   v_cp_status      Varchar2(40) :=Null; 
   v_child_requests Varchar2(240) :=Null;
   output_file_id   UTL_FILE.FILE_TYPE;
   v_path varchar2(80);
   p_period_id NUMBER :=0;
   
  begin  
  
   lc_request_data :=fnd_conc_global.request_data;
   
   n_req_id :=fnd_global.conc_request_id;
 
   if lc_request_data is null then     
       
       lc_request_data :='1';
  
       l_org_id :=mo_global.get_current_org_id;
      
       --n_req_id :=fnd_global.conc_request_id;     
       
       N_user_id :=fnd_global.user_id; 
       
        Begin
         Insert Into xxcus.xxcus_month_endclose_requests 
             (
               request_id
              ,creation_date
              ,output_emailed
              ,insert_source
             )
         Values 
             (
               n_req_id
              ,sysdate
              ,'N'
              ,'xxhds_mendcl_rcv_value_pkg.main'
             );
        End;        
       
       fnd_file.put_line(fnd_file.log, '');       
       fnd_file.put_line(fnd_file.log, 'p_as_of_date ='||p_as_of_date);              
       fnd_file.put_line(fnd_file.log, 'l_org_id ='||l_org_id);
       fnd_file.put_line(fnd_file.log, 'n_user_id ='||n_user_id);
       fnd_file.put_line(fnd_file.log, '');      
      
      Open wc_inv_orgs(l_org_id);
      Fetch wc_inv_orgs Bulk Collect Into wc_inv_orgs_rec;
      Close wc_inv_orgs;    
      
      If wc_inv_orgs_rec.count =0 Then
      
       fnd_file.put_line(fnd_file.log,'No active inventory organizations found for operating unit WhiteCap');
       
      Else --wc_inv_orgs_rec.count >0 
      
         -- We will kick off for every single inventory orgs and not wait to check how many are running at a time
          for indx in 1 .. wc_inv_orgs_rec.count loop                                   
                --
                -- If the concurrent program requires operating unit as a parameter then uncomment the below call
                -- fnd_request.set_org_id(mo_global.get_current_org_id);
                --          
                           
                --Except for arguments 1, 2 and 7 all other arguments are defaulted based on the parameter values
                --established from a sample run by the end user
                                   
                ln_request_id :=fnd_request.submit_request
                      (
                       application      =>G_Application,
                       program          =>G_Cp_Short_Code,
                       description      =>'',
                       start_time       =>'',
                       sub_request      =>TRUE,
                       argument1        =>wc_inv_orgs_rec(Indx).organization_code||' Receiving Value Report', --Title
                       argument2        =>wc_inv_orgs_rec(Indx).organization_id, --Organization Id
                       argument3        =>50328, --Chart of Accounts Id
                       argument4        =>1, --Report Option                       
                       argument5        =>'', --Sort Option
                       argument6        =>p_as_of_date, --As of Date
                       argument7        =>'', --Item From
                       argument8        =>'', --Item To
                       argument9       =>1100000062, --Category Set                                                                                                                                        
                       argument10       =>101, --Category Structure
                       argument11     =>'', --Category From
                       argument12       =>'', --Category To
                       argument13       =>'USD', --Currency
                       argument14       =>'N', --Display Inverse Rate
                       argument15       =>2, --Exchange Rate Type
                       argument16       =>1, --Exchange Rate
                       argument17       =>2, --Quantities by Revision
                       argument18       =>2, --Quantity Precision
                       argument19       =>2, --Include One Time Items
                       argument20       =>2  --Include Period End Accruals                                                                                                                                                                                                                                                                                  
                      ); 
                      
                  if ln_request_id >0 then 
                    fnd_file.put_line(fnd_file.log, 'Submitted Receiving Value report  for organization ='
                                                     ||wc_inv_orgs_rec(indx).organization_name
                                                     ||', Request Id ='
                                                     ||ln_request_id
                                                    );
                  else
                    fnd_file.put_line(fnd_file.log, 'Failed to submit Receiving Value report  for organization ='
                                                     ||wc_inv_orgs_rec(indx).organization_name                                                     
                                                    );
                  end if;     
                   
                 fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
           
                 lc_request_data :=to_char(to_number(lc_request_data)+1);      
          end loop;     
                              
      End If; -- checking if wc_inv_orgs_rec.count =0      
                             
   else     
   
        Begin  
            select a.user_concurrent_program_name
                  ,c.email_address
            into   v_cp_long_name
                  ,v_email
            from fnd_amp_requests_v a
                ,fnd_concurrent_programs d
                ,fnd_user c
            where 1 =1
              and a.request_id =n_req_id
              and a.concurrent_program_id =d.concurrent_program_id
              and a.requested_by =c.user_id;                                                  
                                                                                                          
        Exception                         
         When Others Then
           Null;
        End;    
         
        Begin
          select '/xx_iface/'||lower(name)||'/outbound' into v_path from v$database;
          output_file_id  :=UTL_FILE.FOPEN(v_path, 'zip_file_extract_'||n_req_id||'.txt', 'w');
          for rec in ( select outfile_name
                       from fnd_concurrent_requests
                       where 1 =1
                         and parent_request_id =n_req_id
                     )
          loop
            UTL_FILE.PUT_LINE(output_file_id, rec.outfile_name);
          end loop;
          UTL_FILE.FCLOSE(output_file_id);
          v_child_requests :='OK';
        Exception
         When Others Then
          v_child_requests :='NA';
        End;   
        
        If v_child_requests <>'NA' Then
        ln_request_id :=fnd_request.submit_request
              (
               application      =>'XXCUS',
               program          =>'XXCUS_PEREND_POSTPROCESSOR',
               description      =>'',
               start_time       =>'',
               sub_request      =>TRUE,
               argument1        =>n_req_id,
               argument2        =>v_path||'/zip_file_extract_'||n_req_id||'.txt', 
               argument3        =>v_cp_long_name,               
               argument4        =>v_email,
               argument5        =>'WC_Receiving_Value_Report_'||n_req_id||'.zip',
               argument6        =>'Receiving Value Report Bundle (All Orgs) -Status Update'                                                                         
              ); 
         --commit work;                         
          if ln_request_id >0 then 
            update xxcus.xxcus_month_endclose_requests
               set output_emailed ='Y'
                  ,update_source ='xxhds_mendcl_rcv_value_pkg.main'
             where 1 =1
               and request_id =n_req_id;

          else
             fnd_file.put_line(fnd_file.log, 'Failed to submit XXHDS Period End Post Processor Program');
             -- Lets leave the output_emailed field to N to show we were not able to email the user regarding the status
             -- of the request and the output of the zip file.
             --This condition can also be resolved by manually submitting a program to be devloped and to process
             -- all records where output_emailed ='N' 
          end if; 
        Else --v_child_requests ='NA'
          Null;        
        End If;
           
      retcode :=0;
      errbuf :='Child request[s] completed. Exit HDS Kickoff Receiving Value Report -Master';
      fnd_file.put_line(fnd_file.log,errbuf);
      --return;
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Error in caller apps.xxhds_mendcl_rcv_value_pkg.main ='||sqlerrm);
    rollback;
  end main;
  
end xxhds_mendcl_rcv_value_pkg;
/
