create or replace 
package body xxwc_po_freight_burden_pkg
as
   /*************************************************************************
   *   $Header xxwc_po_freight_burden_pkg $
   *   Module Name: xxwc_po_freight_burden_pkg
   *
   *   PURPOSE:   This package is used by the XXWC PO Freight Burden Upload ADI
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/01/2012  Shankar Hariharan             Initial Version
   *   2.0        05/13/2013  Lee Spitzer                   Updates to for HI Freight Burden
   *   3.0        12/10/2014  Manjula Chellappan 			TMS# 20141001-00254 - MultiOrg changes
   * ***************************************************************************/
   PROCEDURE load_freight_burden (i_org_code     IN VARCHAR2,
                                  i_vendor_num   IN VARCHAR2,
                                  i_freight_duty IN NUMBER,
                                  i_import_flag  IN VARCHAR2,
                                      --Added for HI Freight Burden
                                  i_freight_per_lb IN NUMBER,
                                  i_freight_basis IN VARCHAR2,
                                  i_vendor_type   IN VARCHAR2,
                                  i_source_org_code in VARCHAR2)
   IS
   lv_message varchar2(200);
   lv_vendor_num number;
   lv_site_num number;
   l_exists VARCHAR2(1);
   l_exception exception;
   l_organization_id NUMBER;
   l_source_organization_id NUMBER;
   l_transfer_cr_yes_no VARCHAR2(1);
--Added for Revision 3.0    
   l_org_id NUMBER ;
   BEGIN
   
--Added for Revision 3.0 
  l_org_id := FND_PROFILE.VALUE('ORG_ID');
  mo_global.set_policy_context('S',l_org_id);  
   
   BEGIN
    SELECT organization_id
    INTO   l_organization_id
    FROM   mtl_parameters
    WHERE  organization_code = i_org_code;
   END;

   
   IF i_vendor_num is null and i_vendor_type = 'EXTERNAL' then
   lv_message := 'Vendor Information is required';
   RAISE_APPLICATION_ERROR(-20001,lv_message);
   END IF;

   IF nvl(i_import_flag,'X') not in ('Y','N') then
   lv_message := 'Import flag should be Y or N';
   RAISE_APPLICATION_ERROR(-20001,lv_message);
   END IF;   
   
   IF l_organization_id is null then
   lv_message := 'Branch Information is required';
   RAISE_APPLICATION_ERROR(-20001,lv_message);
   END IF;
   
   IF nvl(i_freight_duty,0) < 0 then
   lv_message := 'Import duty should be a positive value';
   RAISE_APPLICATION_ERROR(-20001,lv_message);
   END IF;
   
   IF nvl(i_freight_per_lb,0) < 0 THEN
   lv_message := 'Freight per lb should be a positive value';
   RAISE_APPLICATION_ERROR(-20001,lv_message);
   END IF;
   
   IF i_freight_basis NOT IN ('W','C') THEN
   lv_message := 'Freight basis is not W or C';
   RAISE_APPLICATION_ERROR(-20001,lv_message);
   END IF;
   
   IF i_vendor_type NOT IN ('EXTERNAL','INTERNAL') THEN
   lv_message := 'Vendor Type is not EXTERNAL or INTERNAL';
   RAISE_APPLICATION_ERROR(-20001,lv_message);
   END IF;
   
   IF i_source_org_code IS NULL AND i_vendor_type = 'INTERNAL' THEN
   lv_message := 'Source organization code is required';
   RAISE_APPLICATION_ERROR(-20001,lv_message);
  
   END IF;

   IF i_source_org_code IS NOT NULL AND i_vendor_type = 'INTERNAL' THEN
      IF    l_source_organization_id = l_organization_id THEN
       lv_message := 'Source org and org can not be the same';
       RAISE_APPLICATION_ERROR(-20001,lv_message);
     END IF;
        
  END IF;
  


    --lv_message := '4';
     
    IF i_vendor_type = 'EXTERNAL' THEN
    
      
    
    BEGIN

      --lv_message := '1';
     BEGIN
      select to_number(trim(substr(i_vendor_num,1,instr(i_vendor_num,'-',1)-1)))
       into lv_vendor_num
       FROM dual;
     EXCEPTION
      WHEN OTHERS THEN
          lv_message := 'lv_vendor_num ' || lv_vendor_num || ' i_vendor_num ' || i_vendor_num;
          raise l_exception;
     END;
     --lv_message := '2';
     BEGIN
     select to_number(trim(substr(i_vendor_num,instr(i_vendor_num,'-',1)+1)))
       into lv_site_num
       FROM dual;
     EXCEPTION
      WHEN OTHERS THEN
          lv_message := 'lv_site_num ' || lv_site_num || ' i_vendor_num ' || i_vendor_num;
          raise l_exception;
     END;
   --lv_message := '3';
     IF lv_site_num=0 then
        lv_site_num := null;
     END IF;

     select 'x'
       into l_exists
       FROM xxwc_po_freight_burden_tbl
      where nvl(vendor_id,-1) = nvl(lv_vendor_num,-1)
        AND nvl(vendor_site_id,-1) = nvl(lv_site_num,-1)
        AND organization_id= l_organization_id;
        
     update xxwc_po_freight_burden_tbl
        set freight_duty  = nvl(i_freight_duty,0),
            import_flag = nvl(i_import_flag,'N'), 
            vendor_type = nvl(i_vendor_type,'EXTERNAL'),
            freight_per_lb = nvl(i_freight_per_lb,0),
            freight_basis = nvl(i_freight_basis,'C'),
            vendor_id = lv_vendor_num,
            vendor_site_id = lv_site_num,
            source_organization_id = l_source_organization_id,
            last_update_date   = sysdate,
            last_updated_by    = fnd_global.user_id,
            last_update_login  = FND_GLOBAL.LOGIN_ID
      where vendor_id = nvl(lv_vendor_num,-1)
        AND nvl(vendor_site_id,-1) = nvl(lv_site_num,-1)
        AND organization_id = l_organization_id
        AND vendor_type = i_vendor_type;
    EXCEPTION
      WHEN no_data_found then
      --lv_message := '6';
        insert 
          into xxwc_po_freight_burden_tbl
             ( vendor_id, vendor_site_id, organization_id, freight_duty,
               import_flag, creation_date, created_by,
               last_update_date, last_updated_by, last_update_login, vendor_type,
               freight_per_lb, freight_basis, source_organization_id
            )
        values
             ( lv_vendor_num, lv_site_num, l_organization_id, nvl(i_freight_duty,0),
               nvl(i_import_flag,'N'),SYSDATE, fnd_global.user_id,
               SYSDATE, fnd_global.user_id, fnd_global.login_id, i_vendor_type, 
               i_freight_per_lb, i_freight_basis, l_source_organization_id
             );
      WHEN OTHERS THEN
          lv_message := 'cound not update or insert vendor records' ||SQLERRM;
          RAISE l_exception;
    END;   
  
  
  ELSIF i_vendor_type = 'INTERNAL' THEN

  BEGIN
    SELECT organization_id
    INTO   l_source_organization_id
    FROM   mtl_parameters
    WHERE  organization_code = i_source_org_code;
   END;

  BEGIN
        SELECT nvl(attribute2,'N')
        INTO   l_transfer_cr_yes_no
        FROM   bom_resources
        WHERE  organization_id = l_organization_id
        AND    resource_code = 'PO Freight';
  EXCEPTION
    WHEN others THEN
        l_transfer_cr_yes_no := 'N';
  END;
  
  IF l_transfer_cr_yes_no = 'N' THEN
     lv_message := 'The org setting for '|| i_org_code || ' does not allow transfer charges.  Please set the PO Freight Duty Allow Transfer Charge to Yes';
     raise l_exception;
  END IF;
 
  BEGIN
    select 'x'
       into l_exists
       FROM xxwc_po_freight_burden_tbl
      WHERE source_organization_id = l_source_organization_id
        AND organization_id= l_organization_id;
   
     update xxwc_po_freight_burden_tbl
        set freight_duty  = nvl(i_freight_duty,0),
            import_flag = nvl(i_import_flag,'N'), 
            vendor_type = nvl(i_vendor_type,'EXTERNAL'),
            freight_per_lb = nvl(i_freight_per_lb,0),
            freight_basis = nvl(i_freight_basis,'C'),
            source_organization_id = l_source_organization_id,
            vendor_id = lv_vendor_num,
            vendor_site_id = lv_site_num,
            last_update_date   = sysdate,
            last_updated_by    = fnd_global.user_id,
            last_update_login  = FND_GLOBAL.LOGIN_ID
      where source_organization_id = l_source_organization_id
        AND organization_id = l_organization_id
         AND vendor_type = i_vendor_type;
    EXCEPTION
      WHEN no_data_found then
      --lv_message := '6';
       BEGIN
        insert 
          into xxwc_po_freight_burden_tbl
             ( organization_id, freight_duty,
               import_flag, creation_date, created_by,
               last_update_date, last_updated_by, last_update_login, vendor_type,
               freight_per_lb, freight_basis, source_organization_id
            )
        values
             ( l_organization_id, nvl(i_freight_duty,0),
               nvl(i_import_flag,'N'),SYSDATE, fnd_global.user_id,
               SYSDATE, fnd_global.user_id, fnd_global.login_id, i_vendor_type, 
               i_freight_per_lb, i_freight_basis, l_source_organization_id
             );
      EXCEPTION
        WHEN OTHERS THEN
            lv_message := 'error inserting internal org into freight burden table ' ||sqlcode || sqlerrm;
      END;
      WHEN OTHERS THEN
        lv_message := 'cound not update or insert internal org records' ||SQLERRM;
        raise l_exception;
      END;   
  ELSE
    
    RAISE_APPLICATION_ERROR(-20001,'Missing Vendor Type');
  END IF;
  
   EXCEPTION
    WHEN l_exception THEN
       RAISE_APPLICATION_ERROR(-20001,lv_message);
    WHEN no_data_found THEN
        RAISE_APPLICATION_ERROR(-20001,lv_message); 
    WHEN others THEN
     lv_message := i_org_code ||' | ' || i_source_org_code ||' | ' || i_vendor_num ||' | ' || lv_vendor_num ||' | ' || lv_site_num || ' | ' ||SQLCODE||SQLERRM; --||' Unhandled Exception '; --||i_vendor_num||' '||i_org_code;
     RAISE_APPLICATION_ERROR(-20001,lv_message);
   END load_freight_burden;                                
END xxwc_po_freight_burden_pkg;