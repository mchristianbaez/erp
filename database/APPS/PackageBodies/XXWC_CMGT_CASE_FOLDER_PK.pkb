--
-- XXWC_CMGT_CASE_FOLDER_PK  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.xxwc_cmgt_case_folder_pk
AS
   /*************************************************************************
    *  Copyright (c) 2011 Lucidity Consulting Group
    *  All rights reserved.
    **************************************************************************
    *   $Header XXWC_CMGT_CASE_FOLDER_PK.pkb $
    *   Module Name: XXWC_CMGT_CASE_FOLDER_PK.pkb
    *
    *   PURPOSE:   This package is used by the conc program -XXWC Close Case Folder
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
    *   1.0        09/19/2011  Vivek Lakaman             Initial Version
    *   1.1        10/06/2014  Maharajan Shunmugam       TMS# 20141001-00158 Canada Multi Org changes
    *   1.2        07/14/2015  Maharajan Shunmugam       TMS#20150909-00071 Credit - modify XXWC Close Case Folder concurrent program
    * ***************************************************************************/


   /*************************************************************************
    *   Procedure Name: Log_Msg
    *
    *************************************************************************
    *   Purpose       : To Log the debug message
    *   Parameter:
    *       IN
    *            p_debug_level  -Debug Level
    *            p_mod_name     -Module Name
    *            p_debug_msg    -Debug Message
    * ***********************************************************************/
   PROCEDURE LOG_MSG (p_debug_level   IN NUMBER
                     ,p_mod_name      IN VARCHAR2
                     ,P_DEBUG_MSG     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      --END IF;

      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      COMMIT;
   END LOG_MSG;

   /*************************************************************************
    *   Procedure Name: submit_request
    *
    *************************************************************************
    *   Purpose       : To Log the debug message
    *   Parameter:
    *       OUT
    *            errbuf       - Error Message
    *            retcode      - Error Code
    * ***********************************************************************/
   PROCEDURE submit_request (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_mod_name              VARCHAR2 (100)
                                 := 'xxwc_cmgt_case_folder_pk.submit_request :';

      l_wf_status             VARCHAR2 (100);
      l_reco_status           VARCHAR2 (100);
      lx_return_status        VARCHAR2 (10);
      lx_error_msg            VARCHAR2 (1000);
      l_released_by           NUMBER;
      l_hold_type_code        oe_hold_definitions.type_code%TYPE;
      l_order_header_id       oe_hold_sources_all.hold_entity_id%TYPE;
      l_hold_release_id       oe_hold_sources_all.hold_release_id%TYPE;
      l_notes                 VARCHAR2 (1000);
      l_error_msg             VARCHAR2 (1000);
      l_release_reason_code   oe_hold_releases.release_reason_code%TYPE;
      l_release_comment       oe_hold_releases.release_comment%TYPE;
      l_hold_release_reason   oe_lookups.meaning%TYPE;


      --Define user defined exception
      SKIP_RECORD             EXCEPTION;
      VALIDATION_ERROR        EXCEPTION;

      CURSOR C_CASE_FOLDER
      IS
         SELECT cr_req.credit_request_id credit_request_id
               ,case_folder.case_folder_id case_folder_id
               ,cr_req.source_column1 order_header_id
               ,cr_req.application_date review_date
               ,cr_req.credit_type
               ,cr_req.source_resp_appln_id appl_id
               ,cr_req.review_type                                                          --added this for ver#1.2
               ,DECODE(substr(cr_req.review_type,1,1),'O','O','C','C','S','B') review_type_code  --added this for ver#1.2
           FROM apps.ar_cmgt_credit_requests cr_req
               ,apps.ar_cmgt_case_folders case_folder
          WHERE     --cr_req.review_type = 'ORDER_HOLD'
                    cr_req.review_type IN ('ORDER_HOLD','SITE_HOLD','CUST_HOLD')    --commented above and added this for ver#1.2
                AND cr_req.credit_request_id = case_folder.credit_request_id
                AND cr_req.source_column3 = 'ORDER'
                AND case_folder.TYPE = 'CASE'
                AND case_folder.status <> 'CLOSED';

      --AND   ROWNUM < 11;

      CURSOR C_hold_release (
         p_hold_release_id IN NUMBER)
      IS
         SELECT rel.release_reason_code
               ,rel.release_comment
               ,lookup.meaning hold_release_reason
               ,rel.created_by released_by
           FROM apps.oe_hold_releases rel, apps.oe_lookups lookup
          WHERE     rel.hold_release_id = p_hold_release_id
                AND rel.release_reason_code = lookup.lookup_code
                AND lookup.lookup_type = 'RELEASE_REASON';
   BEGIN
      errbuf := NULL;
      retcode := '0';

      LOG_MSG (g_level_statement, l_mod_name, 'At Begin -submit_request');

      FOR i IN C_CASE_FOLDER
      LOOP
         BEGIN
            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'++++++++++++++++++++++++++++++++++++++++++++++++');
            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'credit_request_id =>' || i.credit_request_id);
            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'case_folder_id    =>' || i.case_folder_id);
            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'order_header_id   =>' || i.order_header_id);

            BEGIN
--commented below for ver#1.2
              /* SELECT ohd.type_code
                     ,ohs.hold_entity_id order_header_id
                     ,ohs.hold_release_id
                 INTO l_hold_type_code, l_order_header_id, l_hold_release_id
                 FROM apps.oe_hold_definitions ohd, apps.oe_hold_sources ohs
                WHERE     ohd.hold_id = ohs.hold_id 
                      AND ohs.hold_entity_code = 'O'                     
                      AND ohs.hold_entity_id = i.order_header_id
                      AND ohs.hold_release_id IS NOT NULL;*/
--Added below for ver#1.2 << Start
		 SELECT ohd.type_code
       			,holds.header_id order_header_id
      			,MAX(ohr.hold_release_id) hold_release_id
                 INTO l_hold_type_code 
                     ,l_order_header_id 
                     ,l_hold_release_id
	  	 FROM oe_order_holds_all holds,
     		      oe_hold_sources_all ohsa,
     		      oe_hold_releases ohr,
     		      oe_hold_definitions ohd,
     		      ar_cmgt_credit_requests cr_req,
     		      ar_cmgt_case_folders case_folder
		WHERE holds.hold_source_id = ohsa.hold_source_id
  		  AND ohsa.hold_id = ohd.hold_id              
  		  AND holds.hold_release_id = ohr.hold_release_id
  		  AND holds.hold_source_id = ohsa.hold_source_id
  		  AND ohd.name in ('Credit Check Failure')
  		  AND ohsa.hold_entity_code = i.review_type_code 
   		  AND cr_req.credit_request_id = case_folder.credit_request_id
   		  AND cr_req.credit_request_id = i.credit_request_id
  		  AND cr_req.source_column3 = 'ORDER'
   		  AND case_folder.review_type = i.review_type
  		  AND cr_req.source_column1 = holds.header_id
 --                 AND to_char(case_folder.creation_date,'DD-MON-YYYY HH24:MI:SS') 
 --                  BETWEEN to_char(holds.creation_date,'DD-MON-YYYY HH24:MI:SS') AND to_char(holds.creation_date+ 15/1440,'DD-MON-YYYY HH24:MI:SS') 
                  AND holds.header_id = i.order_header_id
                    GROUP BY ohd.type_code
       			,holds.header_id;
--Added for ver#1.2 << End
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  LOG_MSG (g_level_statement
                          ,l_mod_name
                          ,'Hold is not yet released.........');
                  RAISE SKIP_RECORD;
               WHEN OTHERS
               THEN
                  l_error_msg := SQLERRM;
                  RAISE VALIDATION_ERROR;
            END;

            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'l_hold_type_code     =>' || l_hold_type_code);
            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'l_hold_release_id    =>' || l_hold_release_id);

            IF l_hold_release_id IS NULL
            THEN
               LOG_MSG (g_level_statement
                       ,l_mod_name
                       ,'**********Hold is not yet released*********');
               RAISE SKIP_RECORD;
            ELSE
               OPEN C_hold_release (l_hold_release_id);

               FETCH C_hold_release
               INTO l_release_reason_code
                   ,l_release_comment
                   ,l_hold_release_reason
                   ,l_released_by;

               --Raise error message if not found
               IF C_hold_release%NOTFOUND
               THEN
                  FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_OM_HOLD_REL_MISSING');
                  FND_MESSAGE.SET_TOKEN ('XXWC_HOLD_REL_ID'
                                        ,l_hold_release_id);
                  FND_MSG_PUB.ADD;
                  RAISE VALIDATION_ERROR;
               END IF;

               --Close the cursor
               IF C_hold_release%ISOPEN
               THEN
                  CLOSE C_hold_release;
               END IF;

               LOG_MSG (g_level_statement
                       ,l_mod_name
                       ,'l_release_comment      =>' || l_release_comment);
               LOG_MSG (g_level_statement
                       ,l_mod_name
                       ,'l_hold_release_reason  =>' || l_hold_release_reason);

               l_notes :=
                     'Release Reason => ('
                  || l_hold_release_reason
                  || ')'
                  || CHR (10)
                  || 'Release Comments => ('
                  || l_release_comment
                  || ')';

               LOG_MSG (g_level_statement
                       ,l_mod_name
                       ,'l_notes =>' || l_notes);

               BEGIN
                  SELECT wf_fwkmon.getitemstatus (
                            workflowitemeo.item_type
                           ,workflowitemeo.item_key
                           ,workflowitemeo.end_date
                           ,workflowitemeo.root_activity
                           ,workflowitemeo.root_activity_version)
                            AS status_code
                    INTO l_wf_status
                    FROM apps.wf_items workflowitemeo
                        ,apps.wf_item_types_vl workflowitemtypeeo
                        ,apps.wf_activities_vl activityeo
                   WHERE     workflowitemeo.item_type =
                                workflowitemtypeeo.name
                         AND activityeo.item_type = workflowitemeo.item_type
                         AND activityeo.name = workflowitemeo.root_activity
                         AND activityeo.version =
                                workflowitemeo.root_activity_version
                         AND workflowitemeo.item_type = 'ARCMGTAP'
                         AND workflowitemeo.item_key = i.credit_request_id;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_WF_PROC_MISSING');
                     FND_MESSAGE.SET_TOKEN ('XXWC_ITEM_KEY'
                                           ,i.credit_request_id);
                     FND_MSG_PUB.ADD;
                     RAISE VALIDATION_ERROR;
                  WHEN OTHERS
                  THEN
                     l_error_msg := SQLERRM;
                     RAISE VALIDATION_ERROR;
               END;

               IF l_wf_status IN ('ACTIVE', 'ERROR')
               THEN
                  log_msg (
                     g_level_statement
                    ,l_mod_name
                    ,   'Aborting the workflow process..(Item Key) '
                     || i.credit_request_id);
                  wf_engine.AbortProcess (itemtype   => 'ARCMGTAP'
                                         ,itemkey    => i.credit_request_id);
               END IF;

               BEGIN
                  SELECT status
                    INTO l_reco_status
                    FROM apps.ar_cmgt_cf_recommends
                   WHERE case_folder_id = i.case_folder_id;

                  -- update the recommendation status to I
                  UPDATE ar_cmgt_cf_recommends
                     SET status = 'I'
                        ,last_update_date = SYSDATE
                        ,last_updated_by = fnd_global.user_id
                        ,last_update_login = fnd_global.login_id
                   WHERE case_folder_id = i.case_folder_id;

                  LOG_MSG (
                     g_level_statement
                    ,l_mod_name
                    ,   'No of Rows Updated (ar_cmgt_cf_recommends) =>'
                     || SQL%ROWCOUNT);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     INSERT INTO ar_cmgt_cf_recommends (Recommendation_id
                                                       ,case_folder_id
                                                       ,credit_request_id
                                                       ,credit_review_date
                                                       ,credit_recommendation
                                                       ,recommendation_value1
                                                       ,recommendation_value2
                                                       ,status
                                                       ,last_updated_by
                                                       ,last_update_date
                                                       ,last_update_login
                                                       ,creation_date
                                                       ,created_by
                                                       ,credit_type
                                                       ,recommendation_name
                                                       ,application_id)
                          VALUES (ar_cmgt_cf_recommends_s.NEXTVAL
                                 ,i.case_folder_id
                                 ,i.credit_request_id
                                 ,i.review_date
                                 ,'REMOVE_ORDER_HOLD'
                                 ,'USD'
                                 ,NULL
                                 ,                  --p_recommendation_value2,
                                  'I'
                                 ,                                 --p_status,
                                  fnd_global.user_id
                                 ,SYSDATE
                                 ,fnd_global.login_id
                                 ,SYSDATE
                                 ,fnd_global.user_id
                                 ,i.credit_type
                                 ,'AR_CMGT_RECOMMENDATIONS'
                                 ,i.appl_id);

                     LOG_MSG (
                        g_level_statement
                       ,l_mod_name
                       ,   'No of Rows inserted (ar_cmgt_cf_recommends)=>'
                        || SQL%ROWCOUNT);
                  WHEN OTHERS
                  THEN
                     NULL;
               END;

               LOG_MSG (g_level_statement
                       ,l_mod_name
                       ,'Closing the case folder.. ');

               UPDATE ar_cmgt_case_folders
                  SET status = 'CLOSED'
                     ,last_updated = SYSDATE
                     ,last_update_date = SYSDATE
                     ,last_updated_by = fnd_global.user_id
                     ,last_update_login = fnd_global.login_id
                WHERE case_folder_id = i.case_folder_id;

               LOG_MSG (
                  g_level_statement
                 ,l_mod_name
                 ,   'No of Rows Updated (ar_cmgt_case_folders)   =>'
                  || SQL%ROWCOUNT);

               UPDATE ar_cmgt_credit_requests
                  SET status = 'PROCESSED'
                     ,last_update_date = SYSDATE
                     ,last_updated_by = fnd_global.user_id
                     ,last_update_login = fnd_global.login_id
                WHERE credit_request_id = i.credit_request_id;

               LOG_MSG (
                  g_level_statement
                 ,l_mod_name
                 ,   'No of Rows Updated (ar_cmgt_credit_requests)=>'
                  || SQL%ROWCOUNT);

               LOG_MSG (g_level_statement
                       ,l_mod_name
                       ,'Adding Analyst Notes...');

               INSERT INTO AR_CMGT_CF_ANL_NOTES (ANALYSIS_NOTES_ID
                                                ,CASE_FOLDER_ID
                                                ,LAST_UPDATED_BY
                                                ,LAST_UPDATE_DATE
                                                ,LAST_UPDATE_LOGIN
                                                ,CREATION_DATE
                                                ,CREATED_BY
                                                ,TOPIC
                                                ,DISPLAY
                                                ,IMPORTANCE
                                                ,NOTES
                                                ,DATE_OPENED)
                  SELECT AR_CMGT_CF_ANL_NOTES_S.NEXTVAL
                        ,i.case_folder_id
                        ,l_released_by
                        ,                                --fnd_global.user_id,
                         SYSDATE
                        ,fnd_global.login_id
                        ,SYSDATE
                        ,l_released_by
                        ,                                --fnd_global.user_id,
                         'GENERAL'
                        ,'PUBLIC'
                        ,'HIGH'
                        ,l_notes
                        ,SYSDATE
                    FROM DUAL;


               log_msg (g_level_statement
                       ,l_mod_name
                       ,'****************Sucessfully processed************');
            END IF;                            --l_hold_release_id is not null
         EXCEPTION
            WHEN SKIP_RECORD
            THEN
               log_msg (g_level_statement
                       ,l_mod_name
                       ,'*********Skipping the record*********');
            WHEN VALIDATION_ERROR
            THEN
               l_error_msg := fnd_message.get;
               log_msg (g_level_statement
                       ,l_mod_name
                       ,'l_error_msg =>' || l_error_msg);

               --Close the cursor
               IF C_hold_release%ISOPEN
               THEN
                  CLOSE C_hold_release;
               END IF;

               errbuf := l_error_msg;
               retcode := '1';
            WHEN OTHERS
            THEN
               l_error_msg := SQLERRM;
               log_msg (g_level_statement
                       ,l_mod_name
                       ,'Others exception -l_error_msg =>' || l_error_msg);
               errbuf := l_error_msg;
               retcode := '1';
         END;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := SQLERRM;
         retcode := '2';
   END submit_request;
END xxwc_cmgt_case_folder_pk;
/

