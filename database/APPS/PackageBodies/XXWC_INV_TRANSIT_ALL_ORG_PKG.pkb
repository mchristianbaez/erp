CREATE OR REPLACE PACKAGE BODY APPS.xxwc_inv_transit_all_org_pkg AS

  /********************************************************************************
  FILE NAME: XXWC_INV_TRANSIT_ALL_ORG_PKG.pkg
  
  PROGRAM TYPE: PL/SQL Package Body
  
  PURPOSE: To Run the XXWC Inventory Intransit Report for All Organizations
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     03/04/2015    Pattabhi Avual  Initial creation 
  ********************************************************************************/

  --Email Defaults
  program_error EXCEPTION;
  g_err_callfrom VARCHAR2(75) DEFAULT 'XXWC_INV_TRANSIT_ALL_ORG_PKG';
  g_distro_list  VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';


PROCEDURE uc4_submit(  p_errbuf     OUT  VARCHAR2
                      ,p_retcode    OUT  VARCHAR2
                      ,p_resp_name  IN   VARCHAR2
                      ,p_user_name  IN   VARCHAR2
                      ,p_org_name   IN   VARCHAR2) AS
 /********************************************************************************
    FILE NAME: XXWC_INV_TRANSIT_ALL_ORG_PKG
    
    PROGRAM TYPE: PL/SQL Package Body
    
    PURPOSE: To submit the XXWC Inventory Intransit Report for All Orgs fron UC4
    
    HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)                DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     03/06/2015    Pattabhi Avual           TMS# 20150223-00230   - Create  
												   version of XXWC Inventory Intransit 
												   Report to run for all orgs (IT access only)
    ********************************************************************************/  
    ln_request_id        NUMBER;
	l_phase              VARCHAR2(80);
    l_status             VARCHAR2(80);
    l_dev_phase          VARCHAR2(80);
    l_dev_status         VARCHAR2(80);
    l_message            VARCHAR2(4000);
    l_interval           NUMBER := 60;
    l_max_time           NUMBER := 3600; --Version 1.1
    l_success            BOOLEAN;
	
	ln_child_request_id        NUMBER;
	l_child_phase              VARCHAR2(80);
    l_child_status             VARCHAR2(80);
    l_child_dev_phase          VARCHAR2(80);
    l_child_dev_status         VARCHAR2(80);
    l_child_message            VARCHAR2(4000);
    l_child_interval           NUMBER := 60;
    l_child_max_time           NUMBER := 600; --Version 1.1
  
    l_user_id NUMBER;
    l_resp_id NUMBER;
    l_appl_id NUMBER := 401; -- OrderManagement
    l_org_id  NUMBER := mo_global.get_current_org_id; -- HDS White Cap - Org
    l_exception EXCEPTION;
  
    -- File Variables
    l_data_file         VARCHAR2(200);
    l_data_file_path    VARCHAR2(200);
    l_control_file      VARCHAR2(200);
    l_control_file_path VARCHAR2(200);
    l_log_file_path     VARCHAR2(200);
    l_sec               VARCHAR2(100);
    
    l_directory_path   VARCHAR2 (1000);
  
    l_order_type VARCHAR2(100);
    l_dummy      VARCHAR2(100);
    l_order_num  VARCHAR2(100);
    l_delivery   VARCHAR2(100);
  
    l_procedure VARCHAR2(50) := 'UC4_SUBMIT';
    l_err_msg   CLOB;
  
  BEGIN
    p_errbuf  := 'Success';
    p_retcode := '0';
  
    l_sec := 'Derive UserId';
    ----------------------------------------------------------------------------------
    -- Derive UserId
    ----------------------------------------------------------------------------------
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE user_name = p_user_name;
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to find user id based on parm: ' ||
                     p_user_name;
        RAISE program_error;
    END;
  
    l_sec := 'Derive ResponsibilityId';
    ----------------------------------------------------------------------------------
    -- Derive ResponsibilityId
    ----------------------------------------------------------------------------------
    BEGIN
      SELECT responsibility_id
        INTO l_resp_id
        FROM fnd_responsibility_vl
       WHERE responsibility_name = p_resp_name
         AND application_id = l_appl_id;
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to find responsibility id based on parm: ' ||
                     p_resp_name;
        RAISE program_error;
		
    END;
  
    l_sec := 'Apps Initialize';
    ----------------------------------------------------------------------------------
    -- Apps Initialize
    ----------------------------------------------------------------------------------
    apps.fnd_global.apps_initialize(user_id      => l_user_id
                                   ,resp_id      => l_resp_id
                                   ,resp_appl_id => l_appl_id);
  
    apps.mo_global.set_policy_context('S', l_org_id);
    apps.mo_global.init('ONT');
  
    
      l_sec := 'Call Concurrent Program - XXWC Inventory Intransit Report for All Orgs'; 
      ----------------------------------------------------------------------------------
      -- Call Concurrent Program - XXWC Inventory Intransit Report for All Orgs
      ----------------------------------------------------------------------------------
      ln_request_id :=fnd_request.submit_request ( application   => 'XXWC',
                                                   program       => 'XXWC_INV_INTRANSIT_ALL',
                                                   description   => 'XXWC_INV_INTRANSIT',
                                                   start_time    => '',
                                                   sub_request   => FALSE,
                                                   argument1     => '',
                                                   argument2     => '',
                                                   argument3     => ''
												  );
    
    COMMIT;
  
    dbms_output.put_line('Concurrent Program Request ID: ' || ln_request_id);
  
    IF (ln_request_id != 0)
    THEN
    
        l_sec := 'Wait for Concurrent Program ' || 
	   						'XXWC Inventory Intransit Report for All Orgs';
        ----------------------------------------------------------------------------------
        -- Wait for Concurrent Program
        ----------------------------------------------------------------------------------
            IF fnd_concurrent.wait_for_request(request_id => ln_request_id
                                              ,INTERVAL   => l_interval
                                              ,max_wait   => l_max_time
                                              ,phase      => l_phase
                                              ,status     => l_status
                                              ,dev_phase  => l_dev_phase
                                              ,dev_status => l_dev_status
                                              ,message    => l_message)
            THEN
               dbms_output.put_line('Phase = ' || l_phase || ' and status = ' || l_status);
               dbms_output.put_line('Request Completion Message = ' || l_message);
                    
                 -- return completion information
                        IF l_dev_phase = 'COMPLETE' AND l_dev_status = 'NORMAL'
                        THEN
                          p_errbuf  := l_status;
                          p_retcode := '0';
		                  
		                  BEGIN
		                    SELECT  request_id
		                    INTO    ln_child_request_id
                            FROM    fnd_conc_req_summary_v 
                            WHERE   parent_request_id=ln_request_id;  
                          END;
                          COMMIT; 
		                  dbms_output.put_line('Concurrent Program Request ID: ' || ln_child_request_id);
                        
                                IF (ln_child_request_id != 0)
                                THEN
                                
                                  l_sec := 'Wait for Concurrent Program ' || 
	                            							'Bursting XXWC Inventory Intransit Report for All Orgs';
                                  ----------------------------------------------------------------------------------
                                  -- Wait for Child Bursting Concurrent Program
                                  ----------------------------------------------------------------------------------
                                        IF fnd_concurrent.wait_for_request(request_id => ln_child_request_id
                                                                      ,INTERVAL   => l_child_interval
                                                                      ,max_wait   => l_child_max_time
                                                                      ,phase      => l_child_phase
                                                                      ,status     => l_child_status
                                                                      ,dev_phase  => l_child_dev_phase
                                                                      ,dev_status => l_child_dev_status
                                                                      ,message    => l_child_message)
	                                    THEN
                                          dbms_output.put_line('Child Phase = ' || l_child_phase || ' and Child status = ' || l_child_status);
                                          dbms_output.put_line('Request Completion Message = ' || l_child_message);
                                                             
                                                 -- return completion information
                                                IF l_child_dev_phase = 'COMPLETE' AND l_child_dev_status = 'NORMAL'
                                                THEN
                                                  p_errbuf  := l_child_status;
                                                  p_retcode := '0'; 
		                                        ELSIF 	l_child_dev_phase = 'COMPLETE' AND l_child_dev_status = 'ERROR'  
		                                        THEN 	  
		                                          p_errbuf  := l_child_status;
                                                  p_retcode := '2';  
		                                        ELSIF 	l_child_dev_phase = 'COMPLETE' AND l_child_dev_status = 'WARNING'  
		                                        THEN   
		                                          p_errbuf  := l_child_status;
                                                  p_retcode := '1';	  
		                                        ELSE
		                                          dbms_output.put_line('Bursting Request Has Not Completed prior to wait process max_wait');
                                                  dbms_output.put_line('DEV Child Phase = ' || l_child_dev_phase ||
                                                                       ' and DEV Child status = ' || l_child_dev_status);
                                                  p_errbuf  := l_child_status;
                                                  p_retcode := '2';	  
		                                        END IF;	--  line 179 condition  
		                                ELSE                     
		                                    dbms_output.put_line('Request Has Not Completed prior to wait process max_wait');
                                            dbms_output.put_line('DEV Child Phase = ' || l_child_dev_phase ||
                                                                    ' and DEV Child status = ' || l_child_dev_status);
                                            p_errbuf  := l_child_status;
                                            p_retcode := '1';
		                                END IF;  -- line num 166 condition
								ELSE		
							        dbms_output.put_line('Unable to determine completion status of concurrent program');
                                      p_errbuf  := 'Unable to determine completion status';
                                      p_retcode := '2';
                                END IF; -- line num 158 condition			
										
										
			            ELSIF l_dev_phase = 'COMPLETE' AND l_dev_status = 'ERROR'
                        THEN                        
                           p_errbuf  := l_status;
                           p_retcode := '2';
						ELSIF l_dev_phase = 'COMPLETE' AND l_dev_status = 'WARNING'
                        THEN
                          p_errbuf  := l_status;
                          p_retcode := '1';
                        ELSE
                          p_errbuf  := l_status;
                          p_retcode := '2';
                        END IF;  -- line num 139 condition
            ELSE
              dbms_output.put_line('Request Has Not Completed prior to wait process max_wait');
              dbms_output.put_line('DEV Phase = ' || l_dev_phase ||
                                   ' and DEV status = ' || l_dev_status);
              p_errbuf  := l_status;
              p_retcode := '1';
            END IF; -- line num 126  condition
    ELSE
       dbms_output.put_line('Unable to determine completion status of concurrent program');
       p_errbuf  := 'Unable to determine completion status';
       p_retcode := '2';
    END IF;  -- line numn 118 condition
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END uc4_submit;

END xxwc_inv_transit_all_org_pkg;
