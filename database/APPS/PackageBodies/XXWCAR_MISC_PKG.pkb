CREATE OR REPLACE PACKAGE BODY xxwcar_misc_pkg IS

  -- Error DEBUG
  g_err_callfrom  VARCHAR2(75) DEFAULT 'XXWCAR_MISC_PKG';
  g_err_callpoint VARCHAR2(75) DEFAULT 'START';
  g_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  /*******************************************************************************
  * Procedure:   UC4_AUTOADJUSTMENT
  * Description: This is for UC4 to run the concurrent request to AutoAdjustment
  *              that will be part of the nightly schedule and need to run prior
  *              to GetPaid and BillTrust
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     2/22/2013     Kathy Poling    Initial creation of the procedure
  1.1     4/29/2013     Kathy Poling    Uncommented the retcode wrong version commited to SVN  
                                        Incident ticket 402072 (Task ID: 20130417-01451)
  1.2     3/20/2014     Kathy Poling    Added check for open period
                                        (Task ID: 20130908-00094)
  1.3     5/12/2014     Kathy Poling    corrected application ID and schema to tables used 
                                        RFC 40484
  1.4     10/08/2014    Pattabhi Avula  For TMS# 20141001-00058    
  1.5     12/22/2014    Maharajan Shunmugam TMS# 20141222-00045  Multi-Org Issues Resolution
  1.6     03/06/2017    Pattabhi Avula  For TMS#20170227-00028 --Increased the l_sec variable length
                                        in uc4_submit_receipt_writeoff procedure
  1.7     09/07/2017    Pattabhi Avula  For TMS#20170830-00129 - Automatic Receipt Write-off program 
                                        completed with Error -- Added few parameter details
                                        in Error message										
  ********************************************************************************/

  PROCEDURE uc4_autoadjustment(errbuf                OUT VARCHAR2
                              ,retcode               OUT NUMBER
                              ,p_user_name           IN VARCHAR2
                              ,p_responsibility_name IN VARCHAR2
                              ,p_amt_low             IN VARCHAR2
                              ,p_amt_high            IN VARCHAR2
                              ,p_pct_low             IN VARCHAR2
                              ,p_pct_hi              IN VARCHAR2
                              ,p_adj_type            IN VARCHAR2
                              ,p_act_code            IN VARCHAR2
                              ,p_reason_code         IN VARCHAR2
                              ,p_gl_date             IN DATE
                              ,p_currency            IN VARCHAR2
                              ,p_due_low             IN VARCHAR2
                              ,p_due_high            IN VARCHAR2
                              ,p_inv_type_low        IN VARCHAR2
                              ,p_inv_type_high       IN VARCHAR2
                              ,p_cust_name_low       IN VARCHAR2
                              ,p_cust_name_high      IN VARCHAR2
                              ,p_cust_num_low        IN VARCHAR2
                              ,p_cust_num_high       IN VARCHAR2
                              ,p_option_code         IN VARCHAR2) IS
    --
    -- Package Variables
    --
  
    l_req_id              NUMBER NULL;
    v_phase               VARCHAR2(50);
    v_status              VARCHAR2(50);
    v_dev_status          VARCHAR2(50);
    v_dev_phase           VARCHAR2(50);
    v_message             VARCHAR2(250);
    v_error_message       VARCHAR2(3000);
    v_interval            NUMBER := 30; -- In seconds
    v_max_time            NUMBER := 1500; -- In seconds
    l_err_msg             VARCHAR2(3000);
    l_sec                 VARCHAR2(255);
    l_statement           VARCHAR2(9000);
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_org_id              NUMBER;
    l_start_date DATE; --Version 1.2
    l_end_date   DATE; --Version 1.2 
    l_run_date   VARCHAR2(50); --Version 1.2
    l_date       DATE;  --Version 1.2
  
    l_application_name   VARCHAR2(30) := 'AR';
    l_program_short_name VARCHAR2(240) := 'ARXAAP';
    l_argument1          VARCHAR2(240);
    l_argument2          VARCHAR2(240);
    l_argument3          VARCHAR2(240);
    l_argument4          VARCHAR2(240);
    l_argument5          VARCHAR2(240);
    l_argument6          VARCHAR2(240);
    l_argument7          VARCHAR2(240);
    l_argument8          VARCHAR2(240);
    l_argument9          VARCHAR2(240);
    l_argument10         VARCHAR2(240);
    l_argument11         VARCHAR2(240);
    l_argument12         VARCHAR2(240);
    l_argument13         VARCHAR2(240);
    l_argument14         VARCHAR2(240);
    l_argument15         VARCHAR2(240);
    l_argument16         VARCHAR2(240);
    l_argument17         VARCHAR2(240);
    l_argument18         VARCHAR2(240);
  
    l_user_id fnd_user.user_id%TYPE;
  
  BEGIN
  
--Added below org id and initialization for MO changes by Maha on 12/22/14
--Get org id from responsibility_name
BEGIN
SELECT profile_option_value
INTO l_org_id
  FROM fnd_profile_option_values a, fnd_profile_options b
WHERE     a.profile_option_id = b.profile_option_id
       AND b.profile_option_name = 'ORG_ID'
       AND level_value =
              (SELECT responsibility_id
                 FROM fnd_responsibility_tl
                WHERE responsibility_name = p_responsibility_name)
       AND level_id = 10003;
EXCEPTION
WHEN OTHERS THEN
l_org_id := -1;
l_err_msg := 'Not able to get org id from responsibility in Oracle';
 RAISE program_error;
END; 

MO_GLOBAL.SET_POLICY_CONTEXT('S',l_org_id);

    --Version 1.2  
    --Deriving dates of open period
    BEGIN
      SELECT start_date, end_date, trunc(sysdate) 
        INTO l_start_date, l_end_date, l_date
        FROM gl.gl_period_statuses
       WHERE application_id = 222 --Receivables    --Version 1.3
         -- AND set_of_books_id = 2061 --HD Supply USD  -- Commented by Pattabhi and added below condition for TMS# 20141001-00058 Canada Testing
         AND set_of_books_id =(SELECT  set_of_books_id
                               FROM    hr_operating_units
                               WHERE   organization_id= l_org_id)
         AND closing_status = 'O'
         AND period_name NOT LIKE 'FYE%'
         AND period_year || lpad(period_num, 2, 0) IN
             (SELECT MAX(period_year || lpad(period_num, 2, 0))
                FROM gl.gl_period_statuses
               WHERE application_id = 222 --Receivables   --Version 1.3
                 -- AND set_of_books_id = 2061 --HD Supply USD  -- Commented by Pattabhi and added below condition for TMS# 20141001-00058 Canada Testing
               AND set_of_books_id =(SELECT  set_of_books_id
                               FROM    hr_operating_units
                               WHERE   organization_id= l_org_id)
                 AND closing_status = 'O'
                 AND period_name NOT LIKE 'FYE%');
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'No open period in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving open period ' ;
        RAISE program_error;
    END;
    ---3/30/2014
  
    -- Deriving Ids from initalization variables
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM apps.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName ' || p_user_name || ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;
  
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;
  
    --Version 1.2
    IF l_date BETWEEN l_start_date AND l_end_date
    THEN
      l_run_date := 'GL_DATE=' || to_char(l_date, 'YYYY/MM/DD') ||
                    ' 00:00:00';
    ELSE
      l_run_date := 'GL_DATE=' || to_char(l_start_date, 'YYYY/MM/DD') ||
                    ' 00:00:00';
    END IF;
  
    l_argument1  := p_amt_low;
    l_argument2  := p_amt_high;
    l_argument3  := p_currency;
    l_argument4  := p_pct_low;
    l_argument5  := p_pct_hi;
    l_argument6  := p_due_low;
    l_argument7  := p_due_high;
    l_argument8  := p_inv_type_low;
    l_argument9  := p_inv_type_high;
    l_argument10 := p_cust_name_low;
    l_argument11 := p_cust_name_high;
    l_argument12 := p_cust_num_low;
    l_argument13 := p_cust_num_high;
    l_argument14 := p_option_code;
    l_argument15 := p_adj_type;
    l_argument16 := p_act_code;
    l_argument17 := l_run_date;
    l_argument18 := p_reason_code;
  
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);
  
    l_sec := 'UC4 call to run concurrent request AutoAdjustment.';
  
    l_req_id := fnd_request.submit_request(application => l_application_name
                                          ,program     => l_program_short_name
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => l_argument1
                                          ,argument2   => l_argument2
                                          ,argument3   => l_argument3
                                          ,argument4   => l_argument4
                                          ,argument5   => l_argument5
                                          ,argument6   => l_argument6
                                          ,argument7   => l_argument7
                                          ,argument8   => l_argument8
                                          ,argument9   => l_argument9
                                          ,argument10  => l_argument10
                                          ,argument11  => l_argument11
                                          ,argument12  => l_argument12
                                          ,argument13  => l_argument13
                                          ,argument14  => l_argument14
                                          ,argument15  => l_argument15
                                          ,argument16  => l_argument16
                                          ,argument17  => l_argument17
                                          ,argument18  => l_argument18);
    COMMIT;
  
    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,v_interval
                                        ,v_max_time
                                        ,v_phase
                                        ,v_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
        THEN
          l_statement := 'An error occured in the running of the AutoAdjustment' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        
        END IF;
        -- Then Success!
        retcode := 0; --Version 1.1
      
      ELSE
        l_statement := 'An error occured running the AutoAdjustment' ||
                       v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    
    ELSE
      l_statement := 'An error occured when trying to submitting the AutoAdjustment';
      fnd_file.put_line(fnd_file.log, l_statement);
      fnd_file.put_line(fnd_file.output, l_statement);
      RAISE program_error;
    END IF;
  
    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);
  
  EXCEPTION
    WHEN program_error THEN
      retcode   := 2;
      l_err_msg := l_sec || l_err_msg || ' ERROR ' ||
                   substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('retcode:  ' || retcode);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      g_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'AR');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      retcode   := 2;
      l_err_msg := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                   substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('retcode:  ' || retcode);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      g_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'AR');
    
  END uc4_autoadjustment;

  /*******************************************************************************
  * Procedure:   WRITEOFF_DISC
  * Description: This is for writing off discounts not taken by customers.
  *              This will be part of the nightly schedule and need to run prior
  *              to GetPaid and BillTrust
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     2/28/2013    Kathy Poling    Initial creation of the procedure
  1.1     12/22/2014   Maharajan Shunmugam TMS# 20141222-00045  Multi-Org Issues Resolution                                   
  
  ********************************************************************************/

  PROCEDURE writeoff_disc(errbuf                OUT VARCHAR2
                         ,retcode               OUT NUMBER
                         ,p_user_name           IN VARCHAR2
                         ,p_responsibility_name IN VARCHAR2
                         ,p_org_nm              IN VARCHAR2
                         ,p_activity            IN VARCHAR2
                         ,p_reason              IN VARCHAR2) IS
  
    l_err_msg             VARCHAR2(3000);
    l_sec                 VARCHAR2(255);
    l_adj_rec             ar_adjustments%ROWTYPE;
    l_api_name            VARCHAR2(20);
    l_api_version         NUMBER;
    l_called_from         VARCHAR2(10);
    l_check_amount        VARCHAR2(1);
    l_chk_approval_limits VARCHAR2(1);
    l_commit_flag         VARCHAR2(1);
    l_init_msg_list       VARCHAR2(1);
    l_move_deferred_tax   VARCHAR2(10);
    l_msg_count           NUMBER;
    l_msg_data            VARCHAR2(2000);
    l_new_adjust_id       ar_adjustments.adjustment_id%TYPE;
    l_new_adjust_number   ar_adjustments.adjustment_number%TYPE;
    l_old_adjust_id       ar_adjustments.adjustment_id%TYPE;
    l_return_status       VARCHAR2(5);
    l_validation_level    NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_user_id             fnd_user.user_id%TYPE;
    l_org_id              NUMBER;
    l_sob_id              NUMBER;
    l_rec_trx_id          NUMBER;
    l_ccid                NUMBER;
    l_reason_code         VARCHAR2(100);
    l_type                VARCHAR2(25) := 'INVOICE';
    l_count               NUMBER;
  
  BEGIN
  
    l_count               := 0;
    l_adj_rec             := NULL;
    l_api_name            := NULL;
    l_api_version         := 1.0;
    l_called_from         := NULL;
    l_check_amount        := NULL;
    l_chk_approval_limits := NULL;
    l_commit_flag         := NULL;
    l_init_msg_list       := fnd_api.g_true;
    l_move_deferred_tax   := 'Y';
    l_msg_count           := 0;
    l_msg_data            := NULL;
    l_new_adjust_id       := NULL;
    l_new_adjust_number   := NULL;
    l_old_adjust_id       := NULL;
    l_return_status       := NULL;
    l_validation_level    := fnd_api.g_valid_level_full;
  
    -- Deriving Ids from initalization variables
    l_sec := 'Deriving variables';
  
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName ' || p_user_name || ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;
  
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;
  
    -- Deriving Ids for org and set of books   
    BEGIN
      SELECT hou.organization_id, hou.set_of_books_id
        INTO l_org_id, l_sob_id
        FROM apps.hr_operating_units hou
       WHERE 1 = 1
         AND hou.name = p_org_nm;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Invalid org name';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving SetOfBooksId and Org - ' || SQLERRM;
        RAISE program_error;
    END;
 

    --Initializing org_id for rev 1.1
    
    MO_GLOBAL.SET_POLICY_CONTEXT('S',l_org_id);

 
    -- Deriving receivables activity
    BEGIN
      SELECT receivables_trx_id, code_combination_id
        INTO l_rec_trx_id, l_ccid
        FROM apps.ar_receivables_trx
       WHERE NAME = p_activity
         AND set_of_books_id = l_sob_id
         AND org_id = l_org_id
         AND status = 'A'
         AND nvl(end_date_active, SYSDATE + 1) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Invalid Receivables Activity';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Receivables Activity - ' || SQLERRM;
        RAISE program_error;
    END;
  
    -- Validate Reason Code   'DISCOUNT'
    BEGIN
      SELECT lookup_code
        INTO l_reason_code
        FROM applsys.fnd_lookup_values
       WHERE lookup_type = 'ADJUST_REASON'
         AND enabled_flag = 'Y'
         AND lookup_code = p_reason
         AND nvl(end_date_active, SYSDATE + 1) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Invalid ReasonCode';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error validating Reason Code - ' || SQLERRM;
        RAISE program_error;
    END;
    
  
    SELECT COUNT(*)
      INTO l_count
      FROM apps.ar_payment_schedules psa
     WHERE org_id = l_org_id
       AND CLASS IN ('INV', 'DM')
       AND status = 'OP'
       AND amount_due_remaining = nvl(discount_remaining, 0)
       AND amount_due_remaining <> 0
       AND (payment_schedule_id, psa.customer_trx_id,
            discount_remaining * -1) NOT IN
           (SELECT payment_schedule_id, customer_trx_id, line_adjusted
              FROM apps.ar_adjustments aa
             WHERE aa.payment_schedule_id = psa.payment_schedule_id
               AND aa.customer_trx_id = psa.customer_trx_id
               AND aa.org_id = psa.org_id
               AND aa.type = l_type
               AND status = 'W');
    l_sec := 'Record count for writeoff = ' || l_count;
    dbms_output.put_line(l_sec);
    fnd_file.put_line(fnd_file.log, l_sec);
    IF l_count > 0
    THEN
    
      l_sec := 'Apps Initialize';
      --------------------------------------------------------------------------
      -- Apps Initialize
      --------------------------------------------------------------------------
      mo_global.set_policy_context('S', l_org_id);
      mo_global.init('AR');
      fnd_global.apps_initialize(l_user_id
                                ,l_responsibility_id
                                ,l_resp_application_id);
    
      l_sec := 'Starting loop for writeoff';
    
      FOR c_adj IN (SELECT trx_number
                          ,amount_due_original
                          ,amount_due_remaining
                          ,discount_remaining
                          ,discount_taken_earned
                          ,CLASS
                          ,payment_schedule_id
                          ,discount_taken_unearned
                          ,amount_applied
                          ,due_date
                          ,customer_trx_id
                      FROM apps.ar_payment_schedules psa
                     WHERE org_id = l_org_id
                       AND CLASS IN ('INV', 'DM')
                       AND status = 'OP'
                       AND amount_due_remaining = nvl(discount_remaining,0)
                       AND amount_due_remaining <> 0
                       AND (payment_schedule_id, psa.customer_trx_id,
                            discount_remaining * -1) NOT IN
                           (SELECT payment_schedule_id
                                  ,customer_trx_id
                                  ,line_adjusted
                              FROM apps.ar_adjustments aa
                             WHERE aa.payment_schedule_id =
                                   psa.payment_schedule_id
                               AND aa.customer_trx_id = psa.customer_trx_id
                               AND aa.org_id = psa.org_id
                               AND aa.type = l_type
                               AND status = 'W'))
      
      LOOP
      
        l_adj_rec.customer_trx_id     := c_adj.customer_trx_id;
        l_adj_rec.acctd_amount        := c_adj.discount_remaining * -1; --11.46; -- COLUMN A2
        l_adj_rec.amount              := c_adj.discount_remaining * -1; --11.46;  
        l_adj_rec.payment_schedule_id := c_adj.payment_schedule_id;
        l_adj_rec.apply_date          := SYSDATE;
        l_adj_rec.gl_date             := trunc(SYSDATE);
        l_adj_rec.adjustment_type     := 'A';
        l_adj_rec.posting_control_id  := -3;
        l_adj_rec.set_of_books_id     := l_sob_id;
        l_adj_rec.reason_code         := p_reason; --'DISCOUNT'
        l_adj_rec.status              := 'A';
        l_adj_rec.adjustment_id       := NULL;
        l_adj_rec.adjustment_number   := NULL;
        l_adj_rec.created_by          := l_user_id;
        l_adj_rec.created_from        := 'ADJ-API';
        l_adj_rec.creation_date       := SYSDATE;
        l_adj_rec.last_update_date    := SYSDATE;
        l_adj_rec.last_updated_by     := l_user_id;
        l_adj_rec.type                := l_type;
      
        l_adj_rec.receivables_trx_id  := l_rec_trx_id; -- this is rec activity  "405010-Disc Allowed-INV"
        l_adj_rec.code_combination_id := l_ccid; -- pass the CCID  0W.BW080.0000.405010.00000.00000.00000
      
        l_sec := 'API call to create adjust';
        ar_adjust_pub.create_adjustment(p_api_name            => l_api_name
                                       ,p_api_version         => l_api_version
                                       ,p_init_msg_list       => l_init_msg_list
                                       ,p_commit_flag         => l_commit_flag
                                       ,p_validation_level    => l_validation_level
                                       ,p_msg_count           => l_msg_count
                                       ,p_msg_data            => l_msg_data
                                       ,p_return_status       => l_return_status
                                       ,p_adj_rec             => l_adj_rec
                                       ,p_chk_approval_limits => l_chk_approval_limits
                                       ,p_check_amount        => l_check_amount
                                       ,p_move_deferred_tax   => l_move_deferred_tax
                                       ,p_new_adjust_number   => l_new_adjust_number
                                       ,p_new_adjust_id       => l_new_adjust_id
                                       ,p_called_from         => l_called_from
                                       ,p_old_adjust_id       => l_old_adjust_id);
      
        --dbms_output.put_line('New Adjustment Number: ' || l_new_adjust_number);
        --dbms_output.put_line('New Adjustment ID: ' || l_new_adjust_id);
        IF l_return_status = 'S'
        THEN
          fnd_file.put_line(fnd_file.output
                           ,'Adj For Trx Number: ' || c_adj.trx_number ||
                            ' - Adj ID:  ' || l_new_adjust_id);
          fnd_file.put_line(fnd_file.log
                           ,'Error Trx Number: ' || c_adj.trx_number ||
                            ' - Adj ID:  ' || l_new_adjust_id);
          IF l_msg_count >= 1
          THEN
            FOR i IN 1 .. l_msg_count
            LOOP
              dbms_output.put_line(i || '. ' || substr(fnd_msg_pub.get(p_encoded => fnd_api.g_false)
                                                      ,1
                                                      ,255));
              l_err_msg := l_msg_data || ' . ' ||
                           substr(fnd_msg_pub.get(p_encoded => fnd_api.g_false)
                                 ,1
                                 ,255);
              fnd_file.put_line(fnd_file.output, 'Pending: ' || l_err_msg);
              fnd_file.put_line(fnd_file.log, 'Pending: ' || l_err_msg);
            END LOOP;
            --RAISE program_error;
          END IF;
        END IF;
      
      END LOOP;
    END IF;
  EXCEPTION
    WHEN program_error THEN
      retcode   := 2;
      l_err_msg := l_sec || l_err_msg || ' ERROR ' ||
                   substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      dbms_output.put_line('retcode:  ' || retcode);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      g_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom
                                          ,p_calling     => g_err_callpoint
                                           --,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'AR');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      retcode   := 2;
      l_err_msg := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                   substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      dbms_output.put_line('retcode:  ' || retcode);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      g_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom
                                          ,p_calling     => g_err_callpoint
                                           --,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'AR');
    
  END writeoff_disc;

  /*******************************************************************************
  * Procedure:   UC4_SUBMIT_WRITEOFF
  * Description: This is for UC4 to run the concurrent request to Write off discounts
  *              not taken.  This will be part of the nightly schedule and need to run prior
  *              to GetPaid and BillTrust
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     3/5/2013      Kathy Poling    Initial creation of the procedure
  1.1     4/29/2013     Kathy Poling    Uncommented the retcode wrong version commited to SVN
                                        Incident ticket 402072 (Task ID: 20130417-01451)
  ********************************************************************************/

  PROCEDURE uc4_submit_writeoff(errbuf                OUT VARCHAR2
                               ,retcode               OUT NUMBER
                               ,p_user_name           IN VARCHAR2
                               ,p_responsibility_name IN VARCHAR2
                               ,p_org_nm              IN VARCHAR2
                               ,p_activity            IN VARCHAR2
                               ,p_reason              IN VARCHAR2
                               ,p_application         IN VARCHAR2
                               ,p_short_nm            IN VARCHAR2) IS
    --
    -- Package Variables
    --
  
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 1500; -- In seconds
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
    l_sec                VARCHAR2(255);
    l_statement          VARCHAR2(9000);
    l_user_id            fnd_user.user_id%TYPE;
  
  BEGIN
  
  begin
    SELECT user_id
      INTO l_user_id
      FROM applsys.fnd_user
     WHERE user_name = p_user_name;
         EXCEPTION
      WHEN no_data_found THEN
        l_sec := 'UserName ' || p_user_name || ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_sec := 'Error deriving user_id for UserName - ' || p_user_name;
        RAISE program_error;
    END;
  
    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(p_user_name
                                                             ,p_responsibility_name);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';
    
    ELSE
    
      l_sec := 'Global Variables are not set for the ' || p_user_name || '.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
    l_sec := 'UC4 call to run concurrent request WC AR WRITEOFF DISCOUNT NOT TAKEN.';
  
    l_req_id := fnd_request.submit_request(application => p_application
                                          ,program     => p_short_nm
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => p_user_name
                                          ,argument2   => p_responsibility_name
                                          ,argument3   => p_org_nm
                                          ,argument4   => p_activity
                                          ,argument5   => p_reason);
    COMMIT;
  
    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,v_interval
                                        ,v_max_time
                                        ,v_phase
                                        ,v_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
        THEN
          l_statement := 'An error occured in the running of the WC AR WRITEOFF DISCOUNT NOT TAKEN' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        
        END IF;
        -- Then Success!
        retcode := 0; --Version 1.1
      
      ELSE
        l_statement := 'An error occured running the WC AR WRITEOFF DISCOUNT NOT TAKEN' ||
                       v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    
    ELSE
      l_statement := 'An error occured when trying to submitting the WC AR WRITEOFF DISCOUNT NOT TAKEN';
      fnd_file.put_line(fnd_file.log, l_statement);
      fnd_file.put_line(fnd_file.output, l_statement);
      RAISE program_error;
    END IF;
  
    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);
  
  EXCEPTION
    WHEN program_error THEN
      retcode   := 2;
      l_err_msg := l_sec || l_err_msg || ' ERROR ' ||
                   substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('retcode:  ' || retcode);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      g_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'AR');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      retcode   := 2;
      l_err_msg := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                   substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('retcode:  ' || retcode);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      g_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'AR');
    
  END uc4_submit_writeoff;

  /*******************************************************************************
  * Procedure:   UC4_SUBMIT_WRITEOFF
  * Description: This is for UC4 to run the concurrent request "Automatic Receipt Write-off  Program"
  *              This will be part of the nightly schedule and need to run after 
  *              DCTM/BillTrust lockbox process and prior to the extracts for 
  *              GetPaid and BillTrust
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     12/12/2013    Kathy Poling    Initial creation of the procedure
                                        SR 222712 (Task ID: 20130908-00094)
  1.3     05/12/2014    Kathy Poling    corrected application ID and schema to tables used
                                        RFC 40484  
  1.4    12/22/2014    Maharajan Shunmugam TMS# 20141222-00045  Multi-Org Issues Resolution                                  
  1.6     03/06/2017    Pattabhi Avula  For TMS#20170227-00028 --Increased the l_sec variable length 
                                        in uc4_submit_receipt_writeoff procedure 
  1.7     09/07/2017    Pattabhi Avula  For TMS#20170830-00129 - Automatic Receipt Write-off program 
                                        completed with Error -- Added few parameter details
                                        in Error message										
  ********************************************************************************/

  PROCEDURE uc4_submit_receipt_writeoff(errbuf                 OUT VARCHAR2
                                       ,retcode                OUT NUMBER
                                       ,p_user_name            IN VARCHAR2
                                       ,p_responsibility_name  IN VARCHAR2
                                       ,p_currency_code        IN VARCHAR2
                                       ,p_unapp_amount         IN VARCHAR2
                                       ,p_unapp_amount_percent IN VARCHAR2
                                       ,p_receipt_date_from    IN VARCHAR2
                                       ,p_receipt_date_to      IN VARCHAR2
                                       ,p_gl_date_from         IN VARCHAR2
                                       ,p_gl_date_to           IN VARCHAR2
                                       ,p_payment_mthd_id      IN VARCHAR2
                                       ,p_customer_number      IN VARCHAR2
                                       ,p_receipt_number       IN VARCHAR2
                                       ,p_method_id            IN VARCHAR2
                                       ,p_apply_date           IN VARCHAR2
                                       ,p_gl_date              IN VARCHAR2
                                       ,p_comments             IN VARCHAR2) IS
    --
    -- Package Variables
    --
  
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 7200; -- In seconds
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
   -- l_sec                VARCHAR2(255);   -- Ver#1.6
	l_sec                VARCHAR2(1000);  -- Ver#1.6
    l_user_id            fnd_user.user_id%TYPE;
    l_procedure          VARCHAR2(50) := 'uc4_submit_receipt_writeoff';
  
    l_application_name   VARCHAR2(30) := 'AR';
    l_program_short_name VARCHAR2(240) := 'ARWRTCON';
    l_start_date         DATE;
    l_end_date           DATE;
    l_run_date           VARCHAR(50);
    l_date               DATE;  
    l_org_id             NUMBER;
  
  BEGIN
    retcode := 0;
  
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = p_user_name;
    EXCEPTION
      WHEN no_data_found THEN
        l_sec := 'UserName ' || p_user_name || ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_sec := 'Error deriving user_id for UserName - ' || p_user_name;
        RAISE program_error;
    END;
  
    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(p_user_name
                                                             ,p_responsibility_name);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';
    
    ELSE
    
      l_sec := 'Global Variables are not set for the ' || p_user_name || '.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
  --Added below org id and initialization for MO changes by Maha on 12/22/14
--Get org id from responsibility_name
BEGIN
SELECT profile_option_value
INTO l_org_id
  FROM fnd_profile_option_values a, fnd_profile_options b
WHERE     a.profile_option_id = b.profile_option_id
       AND b.profile_option_name = 'ORG_ID'
       AND level_value =
              (SELECT responsibility_id
                 FROM fnd_responsibility_tl
                WHERE responsibility_name = p_responsibility_name)
       AND level_id = 10003;
EXCEPTION
WHEN OTHERS THEN
l_org_id := -1;
l_err_msg := 'Not able to get org id from responsibility in Oracle';
 RAISE program_error;
END; 

MO_GLOBAL.SET_POLICY_CONTEXT('S',l_org_id);

 
    BEGIN
      SELECT start_date, end_date, trunc(sysdate)
        INTO l_start_date, l_end_date, l_date
        FROM gl.gl_period_statuses
       WHERE application_id = 222 -- Receivables   --Version 1.3
         --AND set_of_books_id = 2061 --HD Supply USD -- Commented by Maha and added below condition for TMS# 20141001-00058 Canada Testing
         AND set_of_books_id =(SELECT  set_of_books_id
                               FROM    hr_operating_units
                               WHERE   organization_id= l_org_id)
         AND closing_status = 'O'
         AND period_name NOT LIKE 'FYE%'
         AND period_year || lpad(period_num, 2, 0) IN
             (SELECT MAX(period_year || lpad(period_num, 2, 0))
                FROM gl.gl_period_statuses
               WHERE application_id = 222 -- Receivables    --Version 1.3
                 --AND set_of_books_id = 2061 --HD Supply USD
                  -- Commented by Maha and added below condition for TMS# 20141001-00058 Canada Testing
         AND set_of_books_id =(SELECT  set_of_books_id
                               FROM    hr_operating_units
                               WHERE   organization_id= l_org_id)
                 AND closing_status = 'O'
                 AND period_name NOT LIKE 'FYE%');
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'No open period in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving open period ' ;
        RAISE program_error;
    END; 
  
    IF l_date BETWEEN l_start_date AND l_end_date
    THEN
      l_run_date := to_char(l_date, 'YYYY/MM/DD') || ' 00:00:00';
    ELSE
      l_run_date := to_char(l_start_date, 'YYYY/MM/DD') || ' 00:00:00';
    END IF;
  
    l_sec := 'UC4 call to run concurrent request Automatic Receipt Write-off  Program.';
  
    l_req_id := fnd_request.submit_request(application => l_application_name
                                          ,program     => l_program_short_name
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => p_currency_code
                                          ,argument2   => p_unapp_amount
                                          ,argument3   => p_unapp_amount_percent
                                          ,argument4   => p_receipt_date_from
                                          ,argument5   => p_receipt_date_to
                                          ,argument6   => p_gl_date_from
                                          ,argument7   => p_gl_date_to
                                          ,argument8   => p_payment_mthd_id
                                          ,argument9   => p_customer_number
                                          ,argument10  => p_receipt_number
                                          ,argument11  => p_method_id
                                          ,argument12  => l_run_date
                                          ,argument13  => l_run_date
                                          ,argument14  => p_comments);
    COMMIT;
  
    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,v_interval
                                        ,v_max_time
                                        ,v_phase
                                        ,v_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
       -- v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' || -- Ver#1.7
	     v_error_message := chr(10) ||' '||'Error occured for the receipt num'||' '||p_receipt_number|| -- Ver#1.7
                    		 'and for customer num'||' '||p_customer_number||'.'||' Other details are: ReqID=' || l_req_id || ' DPhase ' || -- Ver#1.7
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                         --  chr(10) || ' MSG - ' || v_message;  -- Ver#1.6
						   chr(10) || ' MSG - ' || SUBSTR(v_message,1,300);  -- Ver#1.6
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
        THEN
          l_sec := 'An error occured in the running of the Automatic Receipt Write-off  Program' ||
                   v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE program_error;
        
        END IF;
        -- Then Success!
      
      ELSE
        l_sec := 'EBS timed out waiting on Automatic Receipt Write-off  Program' ||
                 v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        RAISE program_error;
      END IF;
    
    ELSE
     -- l_sec := 'An error occured when trying to submit Automatic Receipt Write-off  Program'; --  Ver#1.7
	  l_sec := 'An error occured when trying to submit Automatic Receipt Write-off  Program '||SQLERRM; --  Ver#1.7
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);
  
  EXCEPTION
    WHEN program_error THEN
      l_err_msg := l_sec || ' Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('retcode:  ' || retcode);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM ||
                                                                         regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running ARWRTCON with Program Error Exception'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'AR');
    
      retcode := 2;
      errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_sec || ' Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('retcode:  ' || retcode);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM ||
                                                                         regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running ARWRTCON with Exception'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'AR');
    
      retcode := 2;
      errbuf  := l_err_msg;
    
  END uc4_submit_receipt_writeoff;
END xxwcar_misc_pkg;
/