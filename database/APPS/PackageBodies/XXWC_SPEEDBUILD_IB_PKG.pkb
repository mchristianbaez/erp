CREATE OR REPLACE PACKAGE BODY APPS.XXWC_SPEEDBUILD_IB_PKG AS
   /*************************************************************************
      $Header XXWC_SPEEDBUILD_IB_PKG.PKG $
      Module Name: XXWC_SPEEDBUILD_IB_PKG.PKG
   
      PURPOSE:   This package is used for SpeedBuild SalesOrder Inbound Interface
   
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        11/18/2013  Gopi Damuluri             Initial Version   
      1.1        01/31/2014  Gopi Damuluri             TMS# 20140211-00049
                                                       PartySiteNumber UpperCase
   * ***************************************************************************/

   g_default_contact VARCHAR2(200) := 'SYSNAME';

   --Email Defaults
   pl_dflt_email              fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';

   /*************************************************************************
   *   PROCEDURE Name: create_contact
   *
   *   PURPOSE:   To create contact when contact does not exist in Oracle
   *
   * ***************************************************************************/
   PROCEDURE create_contact (p_customer_id   IN NUMBER
                           , p_first_name    IN VARCHAR2
                           , p_last_name     IN VARCHAR2
                           , p_contact_num   IN VARCHAR2
                           , p_cust_contact_id OUT NUMBER
                           , p_error_message OUT VARCHAR2)
   IS
   g_default_contact VARCHAR2(200) := 'SYSNAME';

     l_party_id                   NUMBER;
     p_create_person_rec HZ_PARTY_V2PUB.person_rec_type;
     x_party_id          NUMBER;
     x_party_number      VARCHAR2(2000);
     x_profile_id        NUMBER;
     x_return_status     VARCHAR2(2000);
     x_msg_count         NUMBER;
     x_msg_data          VARCHAR2(2000);

     p_org_contact_rec   HZ_PARTY_CONTACT_V2PUB.ORG_CONTACT_REC_TYPE;
     x_org_contact_id    NUMBER;
     x_party_rel_id      NUMBER;
     x_party_id2         NUMBER;
     x_party_number2     VARCHAR2(2000);

     p_cr_cust_acc_role_rec HZ_CUST_ACCOUNT_ROLE_V2PUB.cust_account_role_rec_type;
     x_cust_account_role_id NUMBER;
     l_exception            EXCEPTION;
     
     p_contact_point_rec HZ_CONTACT_POINT_V2PUB.CONTACT_POINT_REC_TYPE;

     p_edi_rec           HZ_CONTACT_POINT_V2PUB.EDI_REC_TYPE;
     p_email_rec         HZ_CONTACT_POINT_V2PUB.EMAIL_REC_TYPE;
     p_phone_rec         HZ_CONTACT_POINT_V2PUB.PHONE_REC_TYPE;
     p_telex_rec         HZ_CONTACT_POINT_V2PUB.TELEX_REC_TYPE;
     p_web_rec           HZ_CONTACT_POINT_V2PUB.WEB_REC_TYPE;

     x_contact_point_id  NUMBER;
   BEGIN

    ------------------------------------
    -- Derive Customer Party Id
    ------------------------------------
    SELECT party_id
      INTO l_party_id
      FROM hz_cust_accounts_all hca
     WHERE cust_account_id = p_customer_id;

    ------------------------------------
    -- 1. Create a definition contact
    ------------------------------------
--     p_create_person_rec.person_pre_name_adjunct := 'MR.';
     IF p_first_name IS NULL AND p_last_name IS NULL THEN
        p_create_person_rec.person_first_name := g_default_contact;
        p_create_person_rec.person_last_name  := g_default_contact;
     ELSE
        p_create_person_rec.person_first_name := p_first_name;
        p_create_person_rec.person_last_name  := p_last_name;
     END IF;
     p_create_person_rec.created_by_module := 'TCA_V1_API';

     HZ_PARTY_V2PUB.create_person('T',
     p_create_person_rec,
     x_party_id,
     x_party_number,
     x_profile_id,
     x_return_status,
     x_msg_count,
     x_msg_data);

     IF x_return_status != 'S' THEN
       p_error_message := x_msg_data;
       RAISE l_exception;
     END IF;

    ------------------------------------
    -- 2. Create a relation cont-org using party_id from step 1
    ------------------------------------
        p_org_contact_rec.created_by_module                := 'TCA_V1_API';
        p_org_contact_rec.party_rel_rec.subject_id         := x_party_id; --<<value for party_id from step 1>
        p_org_contact_rec.party_rel_rec.subject_type       := 'PERSON';
        p_org_contact_rec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
        p_org_contact_rec.party_rel_rec.object_id          := l_party_id; --<<value for party_id from step 0>
        p_org_contact_rec.party_rel_rec.object_type        := 'ORGANIZATION';
        p_org_contact_rec.party_rel_rec.object_table_name  := 'HZ_PARTIES';
        p_org_contact_rec.party_rel_rec.relationship_code  := 'CONTACT_OF';
        p_org_contact_rec.party_rel_rec.relationship_type  := 'CONTACT';
        p_org_contact_rec.party_rel_rec.start_date         := SYSDATE;
--         p_org_contact_rec.contact_number                   := p_contact_num;

        hz_party_contact_v2pub.create_org_contact('T',
        p_org_contact_rec,
        x_org_contact_id,
        x_party_rel_id,
        x_party_id2,
        x_party_number,
        x_return_status,
        x_msg_count,
        x_msg_data);

     IF x_return_status != 'S' THEN
       p_error_message := x_msg_data;
       RAISE l_exception;
     END IF;

    ------------------------------------------------------------------------------------------------------------
    -- 3. Create a Phone Contact Point using party_id
    ------------------------------------------------------------------------------------------------------------
       -- Initializing the Mandatory API parameters
       p_contact_point_rec.contact_point_type    := 'PHONE';
       p_contact_point_rec.owner_table_name      := 'HZ_PARTIES';
       p_contact_point_rec.owner_table_id        := x_party_id2;--l_party_id;
       p_contact_point_rec.primary_flag          := 'Y';
       p_contact_point_rec.contact_point_purpose := 'BUSINESS';
       p_contact_point_rec.created_by_module     := 'BO_API';

       p_phone_rec.phone_area_code               :=  NULL;
       p_phone_rec.phone_country_code            := '1';
       p_phone_rec.phone_number                  := p_contact_num;
       p_phone_rec.phone_line_type               := 'MOBILE';

       DBMS_OUTPUT.PUT_LINE('Calling the API hz_contact_point_v2pub.create_contact_point');

       HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT  (p_init_msg_list      => FND_API.G_TRUE
                                                 ,   p_contact_point_rec  => p_contact_point_rec
                                                 ,   p_edi_rec            => p_edi_rec
                                                 ,   p_email_rec          => p_email_rec
                                                 ,   p_phone_rec          => p_phone_rec
                                                 ,   p_telex_rec          => p_telex_rec
                                                 ,   p_web_rec            => p_web_rec
                                                 ,   x_contact_point_id   => x_contact_point_id
                                                 ,   x_return_status      => x_return_status
                                                 ,   x_msg_count          => x_msg_count
                                                 ,   x_msg_data           => x_msg_data);

     IF x_return_status != 'S' THEN
       p_error_message := x_msg_data;
       RAISE l_exception;
     END IF;
     
     
     ------------------------------------------------------------------------------------------------------------
    -- 4. Create a contact using party_id you get in step 3
    ------------------------------------------------------------------------------------------------------------
        p_cr_cust_acc_role_rec.party_id          := x_party_id2; --<<value for party_id from step 2>
        p_cr_cust_acc_role_rec.cust_account_id   := p_customer_id; --<<value for cust_account_id from step 0>
        --p_cr_cust_acc_role_rec.primary_flag    := 'Y';
        p_cr_cust_acc_role_rec.role_type         := 'CONTACT';
        p_cr_cust_acc_role_rec.created_by_module := 'TCA_V1_API';

        HZ_CUST_ACCOUNT_ROLE_V2PUB.CREATE_CUST_ACCOUNT_ROLE('T',
        p_cr_cust_acc_role_rec,
        x_cust_account_role_id,
        x_return_status,
        x_msg_count,
        x_msg_data);

     IF x_return_status != 'S' THEN
       p_error_message := x_msg_data;
       RAISE l_exception;
     ELSE
       p_cust_contact_id := x_cust_account_role_id;
     END IF;

   EXCEPTION
   WHEN l_exception THEN
     fnd_file.put_line(fnd_file.log,'p_customer_id - '||p_customer_id||' in L_EXCEPTION, p_error_message - '||p_error_message);
   WHEN OTHERS THEN
     ROLLBACK;
     p_cust_contact_id := NULL;
--     fnd_file.put_line(fnd_file.log,'l_sec - '||l_sec);      
     fnd_file.put_line(fnd_file.log,'p_customer_id - '||p_customer_id||' error in  CREATE_CONTACT procedure - '||SQLERRM);
   END create_contact;

   /*************************************************************************
   *   PROCEDURE Name: load_interface
   *
   *   PURPOSE:   To validate SpeedBuild Staging data and load the same into 
   *              Interface table.
   * ***************************************************************************/
   PROCEDURE load_interface (p_errbuf                 OUT  VARCHAR2,
                             p_retcode                OUT  NUMBER,
                             p_validate_only           IN  VARCHAR2
                             )
   IS

   ----------------------------------------------------------
   -- Order Header Cursor
   ----------------------------------------------------------
   CURSOR cur_so_hdr
       IS
   SELECT stg.quote_number
        , stg.ecomm_order_id
        , stg.ordered_date               
        , stg.po_number
        , stg.customer_number
        , UPPER(stg.customer_site_number) customer_site_number
        , lpad(stg.branch_number,3,'0') branch_number
        , stg.shipping_method
        , stg.shipping_instructions      
        , stg.tax_amount                 
        , stg.contact_first_name
        , stg.contact_last_name
        , stg.contact_number            
        , stg.shipping_charges          
     FROM xxwc.xxwc_qb_order_headers_int_tbl stg
    WHERE 1 = 1
      AND NVL(stg.process_flag,'N') IN ('V', 'N','E');

   ----------------------------------------------------------
   -- Order Lines Cursor
   ----------------------------------------------------------
   CURSOR cur_so_lines(p_ecomm_order_id    NUMBER)
       IS
   SELECT stg.line_number                
        , stg.item_number
        , stg.unit_of_measure            
        , stg.ordered_quantity
        , stg.unit_selling_price
        , stg.rowid
     FROM xxwc.xxwc_qb_order_lines_int_tbl stg
    WHERE 1 = 1
      AND stg.ecomm_order_id   = p_ecomm_order_id;


   l_order_source_id            NUMBER          := 1001; -- SpeedBuild
   l_dflt_ship_method_code      VARCHAR2(50);
   l_shipping_method_code       VARCHAR2(50);
   l_err_flag                   VARCHAR2(1)     := 'N';
   g_org_id                     NUMBER          := 162;
   l_order_type                 oe_transaction_types_tl.name%TYPE := 'STANDARD ORDER';
   l_cust_contact_id            NUMBER;
   l_inv_org_id                 NUMBER;
   l_ship_to_site_use_id        NUMBER;
   l_customer_id                NUMBER;
   l_bill_to_site_use_id        NUMBER;
   l_party_site_id              NUMBER;
   l_inv_item_id                NUMBER;
   l_shipping_item_id           NUMBER;
   l_shipping_line_number       NUMBER;
   l_order_cnt                  NUMBER := 0;
   l_error_message              VARCHAR2 (2000);
   l_line_error_message         VARCHAR2 (2000);
   l_attribute1                 VARCHAR2(240);
   l_line_number                NUMBER;
   l_quote_number               NUMBER;

   l_exception                  EXCEPTION;

   BEGIN
     fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
     fnd_file.put_line(fnd_file.log,'Start of Procedure : LOAD_INTERFACE');
     fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');

     ----------------------------------------------------------------------------------
     -- Derive Default Shipping Method
     ----------------------------------------------------------------------------------
     BEGIN
       SELECT flv.description
         INTO l_dflt_ship_method_code
         FROM fnd_lookup_values flv
        WHERE 1 = 1
          AND flv.lookup_type = 'XXWC_SPEEDBUILD_SHIP_METHOD'
          AND flv.tag         = 'DEFAULT';
     EXCEPTION
     WHEN OTHERS THEN
       fnd_file.put_line(fnd_file.log,'Error deriving Default Shipping Method - '||SQLERRM);
     END;

     ----------------------------------------------------------------------------------
     -- Order Header Cursor
     ----------------------------------------------------------------------------------
     FOR rec_so_hdr IN cur_so_hdr LOOP
        l_error_message := '.';
        l_err_flag      := 'N';
        l_order_cnt     := l_order_cnt + 1;

        BEGIN -- rec_so_hdr LOOP BEGIN

        ----------------------------------------------------------------------------------
        -- Derive Inventory Org Id
        ----------------------------------------------------------------------------------
        l_inv_org_id := NULL;
        BEGIN
          SELECT organization_id
            INTO l_inv_org_id
            FROM org_organization_definitions  ood
           WHERE organization_code = LPAD (rec_so_hdr.branch_number, 3, '0');
        EXCEPTION
        WHEN OTHERS THEN
          l_err_flag   := 'Y';
          l_error_message := l_error_message || '-- Invalid branch_number';
        END;

        l_ship_to_site_use_id     := NULL;
        l_customer_id             := NULL;
        l_bill_to_site_use_id     := NULL;
        l_party_site_id           := NULL;

        ----------------------------------------------------------------------------------
        -- Derive Ship-To Address using Prism PartySiteNumber - Validate Party Site
        ----------------------------------------------------------------------------------
          BEGIN
            SELECT hcsu.site_use_id
                 , hcas.cust_account_id
                 , hcsu.bill_to_site_use_id
                 , hcas.party_site_id
              INTO l_ship_to_site_use_id
                 , l_customer_id
                 , l_bill_to_site_use_id
                 , l_party_site_id
              FROM hz_cust_acct_sites_all   hcas
                 , hz_cust_site_uses_all    hcsu
                 , hz_party_sites           hps
             WHERE 1                           = 1
               AND hcas.status                 = 'A'
               AND hcsu.status                 = 'A'
               AND hps.status                  = 'A'
               AND hps.party_site_number       = to_char(rec_so_hdr.customer_site_number)
               AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
               AND hcas.party_site_id          = hps.party_site_id
               AND hcsu.site_use_code          = 'SHIP_TO'
               AND hcas.org_id                 = g_org_id;
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
            l_err_flag   := 'Y';
            l_error_message := l_error_message || '-- PRISM Party Site not in Oracle';
          WHEN TOO_MANY_ROWS THEN
            l_err_flag   := 'Y';
            l_error_message := l_error_message || '-- Multiple Oracle Sites have same PRISM Party Site';
          WHEN OTHERS THEN
            l_err_flag   := 'Y';
            l_error_message := l_error_message || '-- Error deriving SHIP-TO Site details';
          END;

        ----------------------------------------------------------------------------------
        -- Derive Contact Details 
        ----------------------------------------------------------------------------------
        IF l_customer_id IS NOT NULL 
        AND rec_so_hdr.contact_first_name IS NOT NULL 
        AND rec_so_hdr.contact_last_name IS NOT NULL THEN
          l_cust_contact_id  := NULL;
          create_contact(l_customer_id
                       , rec_so_hdr.contact_first_name
                       , rec_so_hdr.contact_last_name
                       , rec_so_hdr.contact_number
                       , l_cust_contact_id
                       , l_error_message);
        END IF;

/*
        BEGIN
          SELECT cust_account_role_id
            INTO l_cust_contact_id
            FROM hz_cust_account_roles hcar
           WHERE 1 = 1
             AND hcar.cust_account_id     = l_customer_id
             AND hcar.role_type           = 'CONTACT'
             AND hcar.current_role_state = 'A'
             AND hcar.cust_acct_site_id IS NULL
             AND hcar.last_update_date IN (SELECT MAX(hcar_s.last_update_date)
                                             FROM hz_cust_account_roles hcar_s
                                            WHERE 1 = 1
                                              AND hcar_s.cust_account_id     = l_customer_id
                                              AND hcar_s.role_type           = 'CONTACT'
                                              AND hcar.cust_acct_site_id IS NULL
                                              AND hcar_s.current_role_state = 'A')
             AND not exists (select '1'
                              from ar_contacts_v cont
                                 , hz_role_responsibility cont_role
                              where 1 =1
                                and cont.contact_id = cont_role.cust_account_role_id
                                and cont.customer_id = l_customer_id)
             AND rownum = 1;

        EXCEPTION
        WHEN NO_DATA_FOUND THEN
          create_contact(l_customer_id
                       , rec_so_hdr.contact_first_name
                       , rec_so_hdr.contact_last_name
                       , rec_so_hdr.contact_number
                       , l_cust_contact_id
                       , l_error_message);

          IF l_error_message IS NOT NULL THEN
            l_err_flag   := 'Y';
            l_error_message := l_error_message || '-- Customer has no contacts';
          END IF;

        WHEN OTHERS THEN
          l_err_flag   := 'Y';
          l_error_message := l_error_message || '-- Error deriving contact details';
        END;
*/
     ---------------------------------------------------------------------------------
     -- Validate Shipping_Method
     ---------------------------------------------------------------------------------
     IF rec_so_hdr.shipping_method IS NOT NULL THEN
       BEGIN
        SELECT flv.description
          INTO l_shipping_method_code
          FROM fnd_lookup_values flv
         WHERE 1 = 1
           AND flv.lookup_type = 'XXWC_SPEEDBUILD_SHIP_METHOD'
           AND flv.meaning     = rec_so_hdr.shipping_method;

         SELECT wcs.ship_method_code
           INTO l_shipping_method_code
           FROM wsh_carrier_services_v wcs
          WHERE wcs.ship_method_code = l_shipping_method_code
            AND wcs.enabled_flag     = 'Y';
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           l_shipping_method_code := l_dflt_ship_method_code;
       END;
     ELSE
       l_shipping_method_code := l_dflt_ship_method_code;
     END IF;

      ---------------------------------------------------------------------------------
      -- Derive Inventory Item details for SHIPPING Item
      ----------------------------------------------------------------------------------
      IF NVL(rec_so_hdr.shipping_charges,0) > 0 THEN
         l_shipping_item_id := NULL;
         BEGIN
           SELECT inventory_item_id
             INTO l_shipping_item_id
             FROM mtl_system_items_b  msib
            WHERE organization_id = l_inv_org_id
              AND enabled_flag    = 'Y'
              AND segment1        = 'SHIPPING';
         EXCEPTION
         WHEN OTHERS THEN
           l_err_flag   := 'Y';
           l_line_error_message := '-- SHIPPING Item is not setup for Org - '||LPAD (rec_so_hdr.branch_number, 3, '0');
           fnd_file.put_line(fnd_file.log,l_line_error_message);
           fnd_file.put_line(fnd_file.log,'Error validating item - SHIPPING. '||'   Error - '||SQLERRM);
         END;
      END IF;

      ---------------------------------------------------------------------------------
      -- Insert into OE_HEADERS_IFACE_ALL table
      ----------------------------------------------------------------------------------
     BEGIN

     IF p_validate_only = 'N' AND l_err_flag   != 'Y' THEN
        
        /*
        l_quote_number := NULL;
        BEGIN
          SELECT quote_number
            INTO l_quote_number
            FROM oe_order_headers_all
           WHERE 1 = 1
             AND header_id = rec_so_hdr.quote_number;
        EXCEPTION
        WHEN OTHERS THEN
          NULL;
        END;
        */

        INSERT INTO OE_HEADERS_IFACE_ALL(ORDER_SOURCE_ID,
                 CUSTOMER_PO_NUMBER,
                 SHIPPING_METHOD_CODE,
                 INVOICE_TO_CONTACT_ID,
                 SHIP_TO_CONTACT_ID,
                 SOLD_TO_CONTACT_ID,
                 ORIG_SYS_DOCUMENT_REF,
             --    QUOTE_NUMBER,
                 ORDERED_DATE,
                 ORDER_TYPE,
                 CUSTOMER_ID,
                 CREATED_BY,
                 CREATION_DATE,
                 LAST_UPDATED_BY,
                 LAST_UPDATE_DATE,
                 SOLD_TO_ORG_ID,
                 SHIP_FROM_ORG_ID,
                 SHIP_TO_ORG_ID,
                 INVOICE_TO_ORG_ID,
                 BOOKED_FLAG ,
                 SHIPPING_INSTRUCTIONS,
                 ATTRIBUTE7,
                 ORG_ID)
                 VALUES
                (l_order_source_id                     -- ORDER_SOURCE_ID,
               , rec_so_hdr.po_number                  -- CUSTOMER_PO_NUMBER,
               , l_shipping_method_code                -- SHIPPING_METHOD_CODE,
               , l_cust_contact_id                     -- INVOICE_TO_CONTACT_ID,
               , l_cust_contact_id                     -- SHIP_TO_CONTACT_ID,
               , l_cust_contact_id                     -- SOLD_TO_CONTACT_ID,
               , rec_so_hdr.ecomm_order_id             -- ORIG_SYS_DOCUMENT_REF,
            --   , l_quote_number                        -- QUOTE_NUMBER,
               , rec_so_hdr.ordered_date               -- ORDERED_DATE,
               , l_order_type                          -- ORDER_TYPE,
               , l_bill_to_site_use_id                 -- CUSTOMER_ID,
               , Fnd_Global.user_id                    -- CREATED_BY,
               , SYSDATE                               -- CREATION_DATE,
               , Fnd_Global.user_id                    -- LAST_UPDATED_BY,
               , SYSDATE                               -- LAST_UPDATE_DATE,
               , l_customer_id                         -- SOLD_TO_ORG_ID,
               , l_inv_org_id                          -- SHIP_FROM_ORG_ID,
               , l_ship_to_site_use_id                 -- SHIP_TO_ORG_ID,
               , l_bill_to_site_use_id                 -- INVOICE_TO_ORG_ID,
               , 'N'                                   -- BOOKED_FLAG ,
               , rec_so_hdr.shipping_instructions      -- SHIPPING_INSTRUCTIONS
               , 'XXWC_INT_SALESFULFILLMENT'           -- ATTRIBUTE7,
               , g_org_id);

/*
      ---------------------------------------------------------------------------------
      -- Insert into OE_ACTIONS_IFACE_ALL
      ----------------------------------------------------------------------------------
        INSERT INTO OE_ACTIONS_IFACE_ALL
        (ORDER_SOURCE_ID,
        ORIG_SYS_DOCUMENT_REF,
        OPERATION_CODE,
        ORG_ID)
        VALUES
        (l_order_source_id            -- ORDER_SOURCE_ID,
       , rec_so_hdr.ecomm_order_id    -- ORIG_SYS_DOCUMENT_REF,
       , 'BOOK_ORDER'                 -- OPERATION_CODE
       , g_org_id );
*/

      END IF; --      IF p_validate_only = 'N' THEN

   EXCEPTION
   WHEN OTHERS THEN
     l_err_flag := 'Y';
     l_error_message := l_error_message || '-- Error inserting into Order Header interface tables';
   END;

    IF l_err_flag = 'Y' THEN
      RAISE l_exception;
    END IF;

    l_line_number   := 0;
    FOR rec_so_lines IN cur_so_lines(rec_so_hdr.ecomm_order_id) LOOP

        l_line_error_message := NULL;

      ---------------------------------------------------------------------------------
      -- Derive Inventory Item details
      ----------------------------------------------------------------------------------
        l_inv_item_id := NULL;
        IF rec_so_lines.item_number IS NOT NULL THEN
          BEGIN
            SELECT inventory_item_id
              INTO l_inv_item_id
              FROM mtl_system_items_b  msib
             WHERE organization_id = l_inv_org_id
               AND enabled_flag    = 'Y'
               AND segment1        = rec_so_lines.item_number;
          EXCEPTION
          WHEN OTHERS THEN
            l_err_flag   := 'Y';
            l_line_error_message := '-- Invalid Item';
            fnd_file.put_line(fnd_file.log,'Error validating item - '||rec_so_lines.item_number||'   Error - '||SQLERRM);
          END;
        ELSE
          l_err_flag := 'Y';
          l_line_error_message := '-- Invalid Item';
        END IF;

      ---------------------------------------------------------------------------------
      -- Insert into OE_LINES_IFACE_ALL
      ----------------------------------------------------------------------------------
     IF p_validate_only = 'N' AND l_err_flag != 'Y' AND NVL(rec_so_lines.ordered_quantity,0) != 0 THEN
        
        IF l_line_number = 0 AND NVL(rec_so_hdr.shipping_charges,0) > 0 THEN

          SELECT COUNT(1)
            INTO l_shipping_line_number
            FROM xxwc.xxwc_qb_order_lines_int_tbl stg
           WHERE 1 = 1
             AND stg.ecomm_order_id   = rec_so_hdr.ecomm_order_id;
             
          l_shipping_line_number := l_shipping_line_number + 1;

          ---------------------------------------------------------------------------------
          -- Insert SHIPPING Charge Line
          ----------------------------------------------------------------------------------
          BEGIN
            INSERT INTO OE_LINES_IFACE_ALL
                (ORDER_SOURCE_ID,
                INVOICE_TO_CONTACT_ID,
                SHIP_TO_CONTACT_ID,
                ORIG_SYS_DOCUMENT_REF,
                ORIG_SYS_LINE_REF,
                LINE_NUMBER,
                LINE_TYPE,
                INVENTORY_ITEM_ID,
                ORDERED_QUANTITY,
                UNIT_SELLING_PRICE,
                UNIT_LIST_PRICE,
                CALCULATE_PRICE_FLAG,
                SOLD_TO_ORG_ID,
                SHIP_FROM_ORG_ID,
                SHIP_TO_ORG_ID,
                INVOICE_TO_ORG_ID,
                CREATED_BY,
                CREATION_DATE,
                LAST_UPDATED_BY,
                LAST_UPDATE_DATE,
                ORG_ID)
            VALUES(l_order_source_id                          --ORDER_SOURCE_ID,
               , l_cust_contact_id                            --INVOICE_TO_CONTACT_ID,
               , l_cust_contact_id                            --SHIP_TO_CONTACT_ID,
               , rec_so_hdr.ecomm_order_id                    --ORIG_SYS_DOCUMENT_REF,
               , rec_so_hdr.ecomm_order_id||'_'||0            --ORIG_SYS_LINE_REF,
               , l_shipping_line_number                       --LINE_NUMBER,
               , 'STANDARD LINE'                              --LINE_TYPE,
               , l_shipping_item_id                           --INVENTORY_ITEM_ID,
               , 1                                            --ordered_quantity,
               , rec_so_hdr.shipping_charges                  --UNIT_SELLING_PRICE,
               , rec_so_hdr.shipping_charges                  --UNIT_LIST_PRICE
               ,'N'                                           --CALCULATE_PRICE_FLAG,
               , l_customer_id                                --SOLD_TO_ORG_ID,,
               , l_inv_org_id                                 --SHIP_FROM_ORG_ID,
               , l_ship_to_site_use_id                        -- SHIP_TO_ORG_ID,
               , l_bill_to_site_use_id                        --INVOICE_TO_ORG_ID,
               , Fnd_Global.user_id                           --CREATED_BY,
               , SYSDATE                                      --CREATION_DATE,
               , Fnd_Global.user_id                           --LAST_UPDATED_BY,
               , SYSDATE                                      --LAST_UPDATE_DATE,
               , g_org_id);       
          EXCEPTION
          WHEN OTHERS THEN
             l_err_flag := 'Y';
             l_error_message := l_error_message || '-- Error inserting SHIPPING line into Order Lines interface table';
          END;
        END IF;

       l_line_number := l_line_number + 1;
       BEGIN
          ---------------------------------------------------------------------------------
          -- Insert Order Item Line
          ----------------------------------------------------------------------------------
          INSERT INTO OE_LINES_IFACE_ALL
              (ORDER_SOURCE_ID,
              INVOICE_TO_CONTACT_ID,
              SHIP_TO_CONTACT_ID,
              ORIG_SYS_DOCUMENT_REF,
              ORIG_SYS_LINE_REF,
              LINE_NUMBER,
              LINE_TYPE,
              INVENTORY_ITEM_ID,
              ORDERED_QUANTITY,
              UNIT_SELLING_PRICE,
              UNIT_LIST_PRICE,
              CALCULATE_PRICE_FLAG,
              SOLD_TO_ORG_ID,
              SHIP_FROM_ORG_ID,
              SHIP_TO_ORG_ID,
              INVOICE_TO_ORG_ID,
              CREATED_BY,
              CREATION_DATE,
              LAST_UPDATED_BY,
              LAST_UPDATE_DATE,
              ORG_ID)
          VALUES(l_order_source_id                          --ORDER_SOURCE_ID,
             , l_cust_contact_id                            --INVOICE_TO_CONTACT_ID,
             , l_cust_contact_id                            --SHIP_TO_CONTACT_ID,
             , rec_so_hdr.ecomm_order_id                    --ORIG_SYS_DOCUMENT_REF,
             , rec_so_hdr.ecomm_order_id||'_'||l_line_number  --ORIG_SYS_LINE_REF,
             , l_line_number                                --LINE_NUMBER,
             , 'STANDARD LINE'                              --LINE_TYPE,
             , l_inv_item_id                                --INVENTORY_ITEM_ID,
             , rec_so_lines.ordered_quantity                --ordered_quantity,
             , rec_so_lines.unit_selling_price              --UNIT_SELLING_PRICE,
             , rec_so_lines.unit_selling_price              --UNIT_LIST_PRICE
             ,'N'                                           --CALCULATE_PRICE_FLAG,
             , l_customer_id                                --SOLD_TO_ORG_ID,,
             , l_inv_org_id                                 --SHIP_FROM_ORG_ID,
             , l_ship_to_site_use_id                        -- SHIP_TO_ORG_ID,
             , l_bill_to_site_use_id                        --INVOICE_TO_ORG_ID,
             , Fnd_Global.user_id                           --CREATED_BY,
             , SYSDATE                                      --CREATION_DATE,
             , Fnd_Global.user_id                           --LAST_UPDATED_BY,
             , SYSDATE                                      --LAST_UPDATE_DATE,
             , g_org_id);
       EXCEPTION
       WHEN OTHERS THEN
          l_err_flag := 'Y';
          l_error_message := l_error_message || '-- Error inserting SHIPPING line into Order Lines interface table';
       END;
     END IF; -- p_validate_only = 'N' THEN

    IF l_err_flag = 'Y' THEN
      BEGIN
         ----------------------------------------------------------------------------------
         -- UPDATE staging table with Error Status
         ----------------------------------------------------------------------------------
         UPDATE xxwc.xxwc_qb_order_lines_int_tbl stg
            SET process_flag = 'E'
              , error_message  = l_line_error_message
          WHERE rowid = rec_so_lines.rowid;

         ----------------------------------------------------------------------------------
         -- Delete the Order details that have been inserted into Interface tables
         ----------------------------------------------------------------------------------
         DELETE FROM OE_HEADERS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = to_char(rec_so_hdr.ecomm_order_id);
--         DELETE FROM OE_ACTIONS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.ecomm_order_id;
         DELETE FROM OE_LINES_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = to_char(rec_so_hdr.ecomm_order_id);
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;

    ELSE
      BEGIN
         ----------------------------------------------------------------------------------
         -- UPDATE staging table with Success status
         ----------------------------------------------------------------------------------
         UPDATE xxwc.xxwc_qb_order_lines_int_tbl stg
            SET process_flag = 'V'
          WHERE rowid          = rec_so_lines.rowid;
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;
    END IF;

    END LOOP; -- rec_so_lines

    IF l_err_flag = 'Y' THEN
      BEGIN
      IF l_error_message IS NOT NULL THEN
        ----------------------------------------------------------------------------------
        -- UPDATE Header staging table with Error Status
        ----------------------------------------------------------------------------------
        UPDATE xxwc.xxwc_qb_order_headers_int_tbl stg
           SET process_flag = 'E'
             , error_message  = l_error_message
         WHERE ecomm_order_id = rec_so_hdr.ecomm_order_id;
      END IF;

      ----------------------------------------------------------------------------------
      -- Delete the Order details that have been inserted into Interface tables
      ----------------------------------------------------------------------------------
      DELETE FROM OE_HEADERS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = to_char(rec_so_hdr.ecomm_order_id);
--      DELETE FROM OE_ACTIONS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.ecomm_order_id;
      DELETE FROM OE_LINES_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = to_char(rec_so_hdr.ecomm_order_id);

      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;
    ELSE

      BEGIN
      ----------------------------------------------------------------------------------
      -- UPDATE staging table with Success status
      ----------------------------------------------------------------------------------
      UPDATE xxwc.xxwc_qb_order_headers_int_tbl stg
         SET process_flag  = 'S'
       WHERE ecomm_order_id  = rec_so_hdr.ecomm_order_id;
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;
    END IF;

   EXCEPTION
   WHEN l_exception THEN
      BEGIN
      ----------------------------------------------------------------------------------
      -- UPDATE staging table with Error Status
      ----------------------------------------------------------------------------------
      UPDATE xxwc.xxwc_qb_order_headers_int_tbl stg
         SET process_flag = 'E'
           , error_message  = l_error_message
       WHERE ecomm_order_id = rec_so_hdr.ecomm_order_id;
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;

      ----------------------------------------------------------------------------------
      -- Delete the Order details that have been inserted into Interface tables
      ----------------------------------------------------------------------------------
      DELETE FROM OE_HEADERS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = to_char(rec_so_hdr.ecomm_order_id);
--      DELETE FROM OE_ACTIONS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.ecomm_order_id;
      DELETE FROM OE_LINES_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = to_char(rec_so_hdr.ecomm_order_id);

   END; -- rec_so_hdr
   END LOOP; -- rec_so_hdr

   COMMIT;

   EXCEPTION
   WHEN l_exception THEN
     fnd_file.put_line(fnd_file.log,'In L_EXCEPTION, ErrorMessage - '||l_error_message);
   WHEN OTHERS THEN
     ROLLBACK;

     XXCUS_error_pkg.XXCUS_error_main_api (
        p_called_from         => 'XXWC_SPEEDBUILD_IB_PKG.LOAD_INTERFACE'
       ,p_calling             => 'LOAD_INTERFACE'
       ,p_request_id          => -1
       ,p_ora_error_msg       => SQLERRM
       ,p_error_desc          => 'Error in SpeedBuild LOAD_INTERFACE procedure, When Others Exception'
       ,p_distribution_list   => pl_dflt_email
       ,p_module              => 'XXWC');

     fnd_file.put_line(fnd_file.log,'Error in  LOAD_INTERFACE procedure - '||SQLERRM);
   END load_interface;

END XXWC_SPEEDBUILD_IB_PKG;
/