CREATE OR REPLACE PACKAGE BODY APPS.xxwc_edi_tp_extract_pkg
AS
/****************************************************************************************************
 * FUNCTION | PROCEDURE | CURSOR
 * XXWC_EDI_TP_EXTRACT_PKG
 *
 * DESCRIPTION
 * TMS # 20140418-00209 EDI -Create automated extract and email of XXWC_EDI_TP_V to Liaison
 *
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
 * ----------------- -------- ------------------------------------------------------------------------
 * p_file_path       <IN>     User Needs to pass the P_file_path for generating the files 
 * p_file_name1      <IN>     EDI extract first file name 
 * p_file_name2      <IN>     EDI extract Second file name 
 *
 * CALLED BY
 *   Which program, if any, calls this one
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ----------------------------------------------------------------
 * 1.0    19/08/2014   Veera C           TMS # 20140418-00209
******************************************************************************************************/
--Variable Declarations 
l_msg                    VARCHAR2 (150);
l_distro_list            VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
l_req_id                 NUMBER        ;
l_sec                    VARCHAR2(200);
PROCEDURE Generate_files(errbuf       OUT  VARCHAR2
                        ,retcode      OUT  NUMBER
                        ,p_file_path  IN   VARCHAR2
                        ,p_file_name1 IN   VARCHAR2
                        ,p_file_name2 IN   VARCHAR2)IS 
                        
--Variable Declarations                         
output_file_id           UTL_FILE.FILE_TYPE;
v_data_file_name         VARCHAR2 (1000);
v_header                 VARCHAR2(2000);
v_detail                 VARCHAR2 (4000);
n_req_id                 NUMBER := 0;
ln_count                 NUMBER:=0;

CURSOR cur_exract IS 
SELECT tp_group_code            ||','||
       tp_code                  ||','||
       tp_description           ||','||
       tp_reference_ext1        ||','||
       tp_reference_ext2        ||','|| 
       document_id              ||','||
       document_type            ||','||
       translator_code          ||','||
       document_standard        ||','||
       vendor_customer_name     ||','||
       vendor_customer_number   ||','||
       vendor_customer_site_code||','||
       purchasing_site_flag     ||','||
       pay_site_flag            ||','||
       ece_tp_location_code     ||','||
       document_id_location_code   extract_data
FROM xxwc_edi_tp_v;
    
BEGIN
  

  v_header :='TP_GROUP_CODE'            ||','||
             'TP_CODE'                  ||','||
             'TP_DESCRIPTION'           ||','||
             'TP_REFERENCE_EXT1'        ||','||
             'TP_REFERENCE_EXT2'        ||','||
             'DOCUMENT_ID'              ||','||
             'DOCUMENT_TYPE'            ||','||
             'TRANSLATOR_CODE'          ||','||
             'DOCUMENT_STANDARD'        ||','||
             'VENDOR_CUSTOMER_NAME'     ||','||
             'VENDOR_CUSTOMER_NUMBER'   ||','||
             'VENDOR_CUSTOMER_SITE_CODE'||','||
             'PURCHASING_SITE_FLAG'     ||','||
             'PAY_SITE_FLAG'            ||','||
             'ECE_TP_LOCATION_CODE'     ||','||
             'DOCUMENT_ID_LOCATION_CODE';
                  
  ln_count:=1;      
  FOR rec_records IN 1..2 LOOP
    IF ln_count=1 THEN 
      v_data_file_name:= p_file_name1; 
    ELSE 
      v_data_file_name:= p_file_name2;
    END IF;
    
    ln_count:=2;
       
    fnd_file.put_line (fnd_file.log, 'Data File Name :' || v_data_file_name);
    fnd_file.put_line (FND_FILE.LOG, 'Data File Path :'||p_file_path);
    
    fnd_file.put_line (FND_FILE.LOG, 'Opening the file to Write data into CSV file');
    
    output_file_id := UTL_FILE.fopen (p_file_path, v_data_file_name, 'W');
    fnd_file.put_line (FND_FILE.LOG, 'Writting the Header  into : '||v_data_file_name);
    UTL_FILE.put_line (output_file_id, v_header);  
    
    l_sec:='Writting the records into'||v_data_file_name;
    
    ----------------------------------------------------------------
    -- Open cursor for writing into output file
    ----------------------------------------------------------------
    fnd_file.put_line (FND_FILE.LOG, 'Writting the records into : '||v_data_file_name);
    FOR rec_exract in  cur_exract 
    LOOP 
      UTL_FILE.put_line (output_file_id, rec_exract.extract_data);
    END LOOP;
  END LOOP; 
    
  UTL_FILE.FCLOSE(output_file_id);
     
EXCEPTION
WHEN OTHERS
THEN
  l_msg := 'Error: ' || SQLERRM;
  fnd_file.put_line (fnd_file.log,'Error in XXWC_EDI_TP_EXTRACT_PKG');
    xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXWC_EDI_TP_EXTRACT_PKG',
          p_calling                => l_sec,
          p_request_id             => l_req_id,
          p_ora_error_msg          => l_msg,
          p_error_desc             => 'Error running XXWC_EDI_TP_EXTRACT_PKG with PROGRAM ERROR',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AP'
         );
END  Generate_files;

PROCEDURE submit_job(errbuf       OUT  VARCHAR2
                    ,retcode      OUT  NUMBER
                    ,p_file_path  IN   VARCHAR2
                ,p_file_name1 IN   VARCHAR2
            ,p_file_name2 IN   VARCHAR2
               ,p_resp_name  IN   VARCHAR2
               ,p_user_name  IN   VARCHAR2
              ,p_org_name   IN   VARCHAR2) IS 
                     
l_resp_id       NUMBER;
l_resp_app_id   NUMBER;
l_user_id       NUMBER;
l_org_id        NUMBER;
l_file1         VARCHAR2(240);
l_file2         VARCHAR2(240);
l_file_path     VARCHAR2(240);
ln_request_id   NUMBER;
l_call_status   BOOLEAN; 
l_rphase        VARCHAR2(80); 
l_rstatus       VARCHAR2(80); 
l_dphase        VARCHAR2(30); 
l_dstatus       VARCHAR2(30); 
l_message       VARCHAR2(500);

BEGIN

DBMS_OUTPUT.put_line ('Entering submit_job...');
DBMS_OUTPUT.put_line ('p_user_name: ' || p_user_name);
DBMS_OUTPUT.put_line ('p_responsibility_name: ' || p_resp_name);
DBMS_OUTPUT.put_line ('p_operating_unit_name: ' || p_org_name);


BEGIN
  SELECT application_id
  ,      responsibility_id  
  INTO   l_resp_app_id
  ,      l_resp_id
  FROM   fnd_responsibility_tl
  WHERE  responsibility_name =p_resp_name; 
EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_msg :=
                  'Responsibility '
               || p_resp_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_msg :=
                  'Error deriving Responsibility_id for '
               || p_resp_name;
            RAISE PROGRAM_ERROR;
      END;


BEGIN
  SELECT user_id 
  INTO   l_user_id 
  FROM   fnd_user 
  WHERE  user_name =p_user_name;
EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_msg :=
                       'UserName ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_msg :=
                      'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

BEGIN  
  SELECT organization_id 
  INTO   l_org_id
  FROM   hr_operating_units 
  WHERE   name =p_org_name;
EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_msg :=
                  'Operating unit '
               || p_org_name
               || ' is invalid in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_msg :=
                  'Other Operating unit validation error for '
               || p_org_name
               || '. Error: '
               || SUBSTR (SQLERRM, 1, 250);
            RAISE PROGRAM_ERROR;
      END;
  
  fnd_global.apps_initialize(l_user_id, l_resp_id, l_resp_app_id);

  mo_global.set_policy_context('S',l_org_id);
 
 
  SELECT UPPER(Instance_name)||'_UPTP_xref.csv' 
  INTO   l_file1 
  FROM   v$instance;            

  SELECT UPPER(Instance_name)||'_TPVENDOR_xref.csv' 
  INTO   l_file2
  FROM   v$instance;
 
BEGIN 
  SELECT directory_path
  INTO   l_file_path
  FROM   all_directories 
  WHERE  directory_name = p_file_path;
EXCEPTION
WHEN OTHERS THEN
l_file_path := NULL;
END;  

  l_sec:='Concurrent Program submiting';


  
  ln_request_id :=fnd_request.submit_request (
                  application   => 'XXWC',
                  program       => 'XXWC_EDI_TP_EXTRACT',
                  description   => 'XXWC EDI Trading Partner Extract Program',
                  start_time    => '',
                  sub_request   => FALSE,
                  argument1     => l_file_path,
                  argument2     => NVL(p_file_name1,l_file1),
                  argument3     => NVL(p_file_name2,l_file2));
  COMMIT;
  
DBMS_OUTPUT.put_line
                      (   'Concurrent Program Request Submitted. Request ID: '
                       || ln_request_id
                      );


  IF ln_request_id > 0 THEN 

    l_call_status:=fnd_concurrent.wait_for_request(request_id  => ln_request_id
                                                    ,INTERVAL    => 10 
                                                    ,max_wait    => 500 
                                                    ,phase       => l_rphase
                                                    ,status      => l_rstatus
                                                    ,dev_phase   => l_dphase
                                                    ,dev_status  => l_dstatus 
                                                    ,Message     => l_message);
 
    IF l_call_status 
    THEN 
      retcode:=0; 
    ELSE
      retcode:=1;
    END IF;
  ELSE 
    retcode:=1;
  END IF;
      
EXCEPTION
WHEN OTHERS
THEN
  l_msg := 'Error: ' || SQLERRM;
  fnd_file.put_line (fnd_file.log,'Error in XXWC_EDI_TP_EXTRACT_PKG');
    xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXWC_EDI_TP_EXTRACT_PKG',
          p_calling                => l_sec,
          p_request_id             => l_req_id,
          p_ora_error_msg          => l_msg,
          p_error_desc             => 'Error running XXWC_EDI_TP_EXTRACT_PKG with PROGRAM ERROR',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AP'
         );      
END submit_job;
END  xxwc_edi_tp_extract_pkg;
/
GRANT EXECUTE ON apps.xxwc_edi_tp_extract_pkg TO interface_prism
/
GRANT EXECUTE ON apps.xxwc_edi_tp_extract_pkg TO interface_xxcus
/