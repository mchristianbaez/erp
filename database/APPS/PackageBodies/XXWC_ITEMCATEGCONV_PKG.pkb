CREATE OR REPLACE PACKAGE BODY "XXWC_ITEMCATEGCONV_PKG"
IS
/***************************************************************************
        *    script name: xxwc_itemcategconv_pkg.pkb
        *
        *    interface / conversion name: Item conversion.
        *
        *    functional purpose: convert Items using Interface
        *
        *    history:
        *
        *    version    date              author             description
        ************************************************************************
        *    1.0        04-sep-2011   k.Tavva     initial development.
        *    1.5        06-Feb-2012   S Hariharan Changed template logic
        *                                         Used different classification table
        *                                         New code for deriving COGS and Sales account
        *    1.6        21-Mar-2012  S Hariharan  Changed the template application logic for Kit and Intangible
        *                                           to be applied ahead of discontinued
        *    1.7        12-Jul-2012  S Hariharan  Code change to not process items in master org if already exist
        *                                         Eliminate MST from child org insert to process only items
        *                                           in Master Org
        *    2.0        20-Sep-2012   G.Damuluri  Added a new parameter Sourcing From
        *    2.1        25-Apr-2013   S Spivey    remove consigned item template logic for consigned
        *                                           items in master org
        *    2.2        31-May-2013   G.Damuluri  TMS - 20130508-01291 Modified to fix the issue with Item Locators due to the new KeyFlexField
        ***********************************************************************/

    g_sourcing_from       VARCHAR2(150);

    PROCEDURE print_debug (p_print_str IN VARCHAR2)
    IS
    BEGIN
        --  IF g_debug = 'Y' THEN
        fnd_file.put_line (fnd_file.LOG, p_print_str);
        -- END IF;
        DBMS_OUTPUT.put_line (p_print_str);
    END print_debug;

    PROCEDURE validations
    IS
        l_error_message       VARCHAR2 (4000);
        l_record_status       VARCHAR2 (2) := 'N';
        l_organization_id     NUMBER;
        l_count               NUMBER := 0;
        l_processing_stg      NUMBER := 0;
        l_error_stg           NUMBER := 0;
        l_validated_stg       NUMBER := 0;
        l_organization_code   VARCHAR2 (10);
        l_uom_code            VARCHAR2 (10);
        l_category            VARCHAR2 (10);
        l_pl_category         VARCHAR2 (40); -- Shankar 20-Aug-2012
        l_buyer_id            NUMBER;
        l_planner_code        VARCHAR2(30);
        l_cost_of_sales_account          NUMBER;
        l_sales_account                  NUMBER; -- Shankar 15-Feb-2012
        l_new_cogs_account               NUMBER;
        l_new_sales_account              NUMBER; -- Shankar 15-Feb-2012

   l_coa_id          NUMBER;
   v_segment1        gl_code_combinations.segment1%TYPE := NULL;
   v_segment2        gl_code_combinations.segment2%TYPE := NULL;
   v_segment3        gl_code_combinations.segment3%TYPE := NULL;
   v_segment4        gl_code_combinations.segment4%TYPE := NULL;
   v_segment5        gl_code_combinations.segment5%TYPE := NULL;
   v_segment6        gl_code_combinations.segment6%TYPE := NULL;
   v_segment7        gl_code_combinations.segment7%TYPE := NULL;
   v_new_cogs_ccid   NUMBER;
   l_concat_segs     VARCHAR2(80);
   l_status          BOOLEAN;

        CURSOR c_item_stg
        IS
            SELECT   ROWID, xis.*
              FROM   xxwc_invitem_stg xis
             WHERE   NVL (status, 'R') IN ('R', 'E')
               AND sourcing_from = NVL(g_sourcing_from, sourcing_from);
    --FOR BOTH NEW RECORDS AND ALSO THE ERROR OUT RECORDS TO PROCESS REAGAIN
    --AND unitofmeasure = 'EA';
    BEGIN
        SELECT   COUNT (1)
          INTO   l_processing_stg
          FROM   xxwc_invitem_stg xis
         WHERE   NVL (status, 'R') IN ('R', 'E')
           AND sourcing_from = NVL(g_sourcing_from, sourcing_from);

        --UOM DECODE
        BEGIN
            UPDATE   xxwc_invitem_stg
               SET   unitofmeasure =
                         DECODE (unitofmeasure,
                                 'PAR', 'PR',
                                 'ROL', 'RL',
                                 'PK', 'PKG',
                                 'LB', 'LBS',
                                 'PAL', 'PL',
                                 'CAS', 'CS',
                                 unitofmeasure);
           -- To remove special characters in gtp indicator field
            update xxwc_invitem_stg
               set gtpindicator=trim(substr(gtpindicator,1,6));
            COMMIT;
        END;

        FOR r_item_stg IN c_item_stg
        LOOP
            l_error_message := NULL;
            l_record_status := NULL;
            l_category := NULL;

            --org code validation
            IF r_item_stg.organization_code IS NOT NULL
            THEN
                BEGIN
                    SELECT   organization_id
                      INTO   l_organization_id
                      FROM   org_organization_definitions
                     WHERE   UPPER (organization_code) =
                                 UPPER (r_item_stg.organization_code);
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        l_record_status := 'E';
                        l_error_message := 'Organization Code Does Not Exists';
                    -- print_debug (l_error_message);
                    WHEN TOO_MANY_ROWS
                    THEN
                        l_record_status := 'E';
                        l_error_message := 'Too Many Organization Exists';
                    -- print_debug (l_error_message);
                    WHEN OTHERS
                    THEN
                        l_record_status := 'E';
                        l_error_message := SQLCODE || '  ' || SQLERRM;
                -- print_debug (l_error_message);
                END;
            ELSE
                l_record_status := 'E';
                l_error_message := l_error_message || 'Org Code is null';
            END IF;


            --Buyer validation
            IF r_item_stg.buyer IS NOT NULL
            THEN
                BEGIN
                    SELECT   planner_code
                      INTO   l_planner_code
                      FROM   mtl_planners a, mtl_parameters b
                     WHERE   a.organization_id=b.organization_id
                       AND   b.organization_code=UPPER (r_item_stg.organization_code)
                       AND   planner_code=UPPER (r_item_stg.buyer) ;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        --l_record_status := 'E';
                        --l_error_message := 'Planner Code Does Not Exists';
                        null;
                    -- print_debug (l_error_message);
                    WHEN TOO_MANY_ROWS
                    THEN
                        l_record_status := 'E';
                        l_error_message := 'Too Many Planner Code Exists';
                    -- print_debug (l_error_message);
                    WHEN OTHERS
                    THEN
                        l_record_status := 'E';
                        l_error_message := SQLCODE || '  ' || SQLERRM;
                -- print_debug (l_error_message);
                END;
            END IF;

            -- Category validation
            IF r_item_stg.category IS NOT NULL and length(r_item_stg.category)<>7
            THEN
                BEGIN
                    SELECT   a.segment1 || '.' || a.segment2
                      INTO   l_category
                      FROM   mtl_categories_b a, mtl_category_sets_v b
                     WHERE   UPPER (a.segment1) || '.' || UPPER (a.segment2) =
                                 UPPER (SUBSTR (r_item_stg.category, 1, 2))
                                 || '.'
                                 || UPPER (r_item_stg.category)
                             AND a.structure_id = b.structure_id
                             AND b.category_set_name = 'Inventory Category';

                    UPDATE   xxwc_invitem_stg
                       SET   category = l_category
                     WHERE   ROWID = r_item_stg.ROWID;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        l_record_status := 'C';
                        --FOR TO POPULATE THE MESSAGE WHERE THE CATEGORY IS NOT EXISTED AND ITS DEFAULTED TO 'QQ.QQQQ'
                        l_error_message :=
                               l_error_message
                            || '-'
                            || 'Category name Does Not Exists';
                        print_debug (l_error_message);
                    WHEN TOO_MANY_ROWS
                    THEN
                        l_record_status := 'E';
                        l_error_message :=
                               l_error_message
                            || '-'
                            || 'Too Category names Exists';
                        print_debug (l_error_message);
                    WHEN OTHERS
                    THEN
                        l_record_status := 'E';
                        l_error_message :=
                               l_error_message
                            || ' - '
                            || SQLCODE
                            || '  '
                            || SQLERRM;
                        print_debug (l_error_message);
                END;
            ELSIF length(r_item_stg.category)=7 then
              null;
            ELSE
                l_record_status := 'E';
                l_error_message := l_error_message || 'Category is null';
            END IF;


            --  Private Label  category validation -- Shankar 20-Aug-2012
            IF r_item_stg.private_label_category IS NOT NULL
            THEN
                BEGIN
                    SELECT   a.segment1
                      INTO   l_pl_category
                      FROM   mtl_categories_b a, mtl_category_sets_v b
                     WHERE   UPPER (a.segment1) = UPPER (r_item_stg.private_label_category)
                             AND a.structure_id = b.structure_id
                             AND b.category_set_name = 'PRIVATE LABEL';

                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        l_record_status := 'E';
                        l_error_message :=
                               l_error_message
                            || '-'
                            || 'Category name Does Not Exist';
                        print_debug (l_error_message);
                    WHEN TOO_MANY_ROWS
                    THEN
                        l_record_status := 'E';
                        l_error_message :=
                               l_error_message
                            || '-'
                            || 'Too Many Category names Exists';
                        print_debug (l_error_message);
                    WHEN OTHERS
                    THEN
                        l_record_status := 'E';
                        l_error_message :=
                               l_error_message
                            || ' - '
                            || SQLCODE
                            || '  '
                            || SQLERRM;
                        print_debug (l_error_message);
                END;
            END IF;


            --uom code validation
            IF r_item_stg.unitofmeasure IS NOT NULL
            THEN
                BEGIN
                    SELECT   uom_code
                      INTO   l_uom_code
                      FROM   mtl_units_of_measure
                     WHERE   uom_code = r_item_stg.unitofmeasure;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        l_record_status := 'E';
                        l_error_message :=
                               l_error_message
                            || '-'
                            || 'Primary UOM Code Does Not Exists';
                    -- print_debug (l_error_message);
                    WHEN TOO_MANY_ROWS
                    THEN
                        l_record_status := 'E';
                        l_error_message :=
                               l_error_message
                            || '-'
                            || 'Too Many Primary UOM Code Exists';
                    -- print_debug (l_error_message);
                    WHEN OTHERS
                    THEN
                        l_record_status := 'E';
                        l_error_message :=
                               l_error_message
                            || ' - '
                            || SQLCODE
                            || '  '
                            || SQLERRM;
                -- print_debug (l_error_message);
                END;
            ELSE
                l_record_status := 'E';
                l_error_message := l_error_message || 'UOM Code  is null';
            END IF;

            -- COGS and Revenue Account Validation

                SELECT   cost_of_sales_account,
                         sales_account
                  INTO   l_cost_of_sales_account,
                         l_sales_account
                  FROM   mtl_parameters
                 WHERE   UPPER (organization_code) = UPPER (r_item_stg.organization_code);


/*
            BEGIN
                 select a.code_combination_id
                   into l_new_cogs_account
                   from gl_code_combinations a,gl_code_combinations b
                  where b.code_combination_id=l_cost_of_sales_account
                    and b.segment1=a.segment1
                    and b.segment2=a.segment2
                    and b.segment3=a.segment3
                    and a.segment4=r_item_stg.cogs_account
                    and b.segment5=a.segment5
                    and b.segment6=a.segment6
                    and b.segment7=a.segment7;
            EXCEPTION
              when others then
                l_record_status := 'E';
                l_error_message := l_error_message || 'COGS N/A';
            END;

            BEGIN
                 select a.code_combination_id
                   into l_new_sales_account
                   from gl_code_combinations a,gl_code_combinations b
                  where b.code_combination_id=l_sales_account
                    and b.segment1=a.segment1
                    and b.segment2=a.segment2
                    and b.segment3=a.segment3
                    and a.segment4=r_item_stg.revenue_account
                    and b.segment5=a.segment5
                    and b.segment6=a.segment6
                    and b.segment7=a.segment7;
            EXCEPTION
              when others then
                l_record_status := 'E';
                l_error_message := l_error_message || 'Sales Account N/A';
            END;
*/

     BEGIN
        -- Get chart of accounts ID.
        SELECT chart_of_accounts_id
          INTO l_coa_id
          FROM org_organization_definitions ood
         WHERE ood.organization_id = 222;
     END;

    ----------------------------------------------------------------------------------
    -- Derive COGS Account
    ----------------------------------------------------------------------------------
    BEGIN
      SELECT segment1, segment2, segment3, segment4, segment5 ,segment6, segment7
        INTO v_segment1, v_segment2, v_segment3, v_segment4, v_segment5 ,v_segment6, v_segment7
        FROM gl_code_combinations
       WHERE code_combination_id=l_cost_of_sales_account;
    EXCEPTION
    WHEN OTHERS THEN
      NULL;
    END;

    v_segment4 := r_item_stg.cogs_account;

    ----------------------------------------------------------------------------------
    -- Derive COGS Code Combination Id
    ----------------------------------------------------------------------------------
    BEGIN
      l_concat_segs := v_segment1||'.'||v_segment2||'.'||v_segment3||'.'||v_segment4||'.'||v_segment5||'.'||v_segment6||'.'||v_segment7;

      l_status := fnd_flex_keyval.validate_segs('CREATE_COMBINATION' --operation
                                               , 'SQLGL'
                                                --appl_short_name
                                               , 'GL#'
                                                --key_flex_code
                                               , l_coa_id
                                                --structure_number
                                               , l_concat_segs
                                                --concat_segments
                                               , 'V'
                                                --values_or_ids
                                               , SYSDATE
                                                --validation_date
                                               , 'ALL'
                                                --displayable
                                               , NULL
                                                --data_set
                                               , NULL
                                                --vrule
                                               , NULL
                                                --where_clause
                                               , NULL
                                                --get_columns
                                               , FALSE
                                                --allow_nulls
                                               , FALSE
                                                --allow_orphans
                                               , NULL
                                                --allow_orphans
                                               , NULL
                                                --resp_id
                                               , NULL
                                                --user_id
                                               , NULL
                                                --select_comb_from_view
                                               , NULL
                                                --no_combmsg
                                               , NULL
                                                --where_clause_msg
                                                );

    IF l_status THEN
      l_new_cogs_account   := fnd_flex_keyval.combination_id();
    ELSE
      l_record_status := 'E';
      l_error_message := l_error_message || 'COGS N/A';
    END IF;
    EXCEPTION
    WHEN no_data_found THEN
      l_record_status := 'E';
      l_error_message := l_error_message || 'COGS N/A';
    END;

    ----------------------------------------------------------------------------------
    -- Derive Revenue Account
    ----------------------------------------------------------------------------------
    BEGIN
      SELECT segment1, segment2, segment3, segment4, segment5 ,segment6, segment7
        INTO v_segment1, v_segment2, v_segment3, v_segment4, v_segment5 ,v_segment6, v_segment7
        FROM gl_code_combinations
       WHERE code_combination_id = l_sales_account;
    EXCEPTION
    WHEN OTHERS THEN
      NULL;
    END;

    v_segment4 := r_item_stg.revenue_account;

    ----------------------------------------------------------------------------------
    -- Derive Revenue Account Code Combination Id
    ----------------------------------------------------------------------------------
    BEGIN
      l_concat_segs := v_segment1||'.'||v_segment2||'.'||v_segment3||'.'||v_segment4||'.'||v_segment5||'.'||v_segment6||'.'||v_segment7;

      l_status := fnd_flex_keyval.validate_segs('CREATE_COMBINATION' --operation
                                               , 'SQLGL'
                                                --appl_short_name
                                               , 'GL#'
                                                --key_flex_code
                                               , l_coa_id
                                                --structure_number
                                               , l_concat_segs
                                                --concat_segments
                                               , 'V'
                                                --values_or_ids
                                               , SYSDATE
                                                --validation_date
                                               , 'ALL'
                                                --displayable
                                               , NULL
                                                --data_set
                                               , NULL
                                                --vrule
                                               , NULL
                                                --where_clause
                                               , NULL
                                                --get_columns
                                               , FALSE
                                                --allow_nulls
                                               , FALSE
                                                --allow_orphans
                                               , NULL
                                                --allow_orphans
                                               , NULL
                                                --resp_id
                                               , NULL
                                                --user_id
                                               , NULL
                                                --select_comb_from_view
                                               , NULL
                                                --no_combmsg
                                               , NULL
                                                --where_clause_msg
                                                );

    IF l_status THEN
      l_new_sales_account  := fnd_flex_keyval.combination_id();
    ELSE
      l_record_status := 'E';
      l_error_message := l_error_message || 'Sales Account N/A';
    END IF;
    EXCEPTION
    WHEN no_data_found THEN
      l_record_status := 'E';
      l_error_message := l_error_message || 'Sales Account N/A';
    END;

            --Processing Error Information
            IF NVL (l_record_status, 'V') = 'E'
            THEN
                -- Update Item Staging Table with error flag and error message
                UPDATE   xxwc_invitem_stg
                   SET   status = 'E',
                         error_message = SUBSTR (l_error_message, 1, 1000)
                 WHERE   ROWID = r_item_stg.ROWID;
            --COMMIT;
            ELSIF NVL (l_record_status, 'V') = 'C'
            THEN                                --IF THE CATEGEORY NOT EXISTED
                UPDATE   xxwc_invitem_stg
                   SET   status = 'V',
                         error_message = category||'-'||SUBSTR (l_error_message, 1, 80),
                         category = 'QQ.QQQQ'
                 WHERE   ROWID = r_item_stg.ROWID;
            -- COMMIT;
            ELSE
                UPDATE   xxwc_invitem_stg
                   SET   status = 'V'
                 --       error_message = SUBSTR (l_error_message, 1, 100)
                 WHERE   ROWID = r_item_stg.ROWID;
            END IF;

            COMMIT;
        END LOOP;

        print_debug ('Number of Records Processed: ' || l_processing_stg);

        SELECT   COUNT (1)
          INTO   l_error_stg
          FROM   xxwc_invitem_stg
         WHERE   status = 'E'
         AND sourcing_from = NVL(g_sourcing_from, sourcing_from);

        print_debug ('Number of Records Errored out: ' || l_error_stg);

        SELECT   COUNT (1)
          INTO   l_validated_stg
          FROM   xxwc_invitem_stg
         WHERE   status = 'V'
         AND sourcing_from = NVL(g_sourcing_from, sourcing_from);

        print_debug ('Number of Records Validated ' || l_validated_stg);
    END validations;

    PROCEDURE item_insert
    IS
        l_inventory_item_status_code     VARCHAR2 (10) := null; --'Active';
        l_days                           NUMBER := 30;
        l_shelf_days                     NUMBER := 0;
        l_shelf_life_code                NUMBER := 4;
        l_un_number_id                   NUMBER;
        l_hazard_class_id                NUMBER;
        l_category_set_name              VARCHAR2 (30);
        l_master_org_id                  NUMBER;
        l_organization_id                NUMBER;
        g_master_org_code                VARCHAR2 (10) := 'MST';
        l_interfacerec_count             NUMBER := 0;
        l_interfaceprocess_count         NUMBER := 0;
        l_expense_account                NUMBER;
        l_cost_of_sales_account          NUMBER;
        l_sales_account                  NUMBER; -- Shankar 15-Feb-2012
        l_new_cogs_account               NUMBER;
        l_new_sales_account              NUMBER; -- Shankar 15-Feb-2012
        l_template_name                  VARCHAR2 (30);
        g_process_flag                   NUMBER := 1;
        g_master_transaction_type        VARCHAR2 (30) := 'CREATE';
        --'UPDATE';
        g_transaction_type               VARCHAR2 (30) := 'CREATE';
        l_process_set_id                 NUMBER := 100;
        l_buyer_id                       NUMBER := 0;
        l_template_id                    NUMBER := 0;
        l_master_expense_account         NUMBER;
        l_master_cost_of_sales_account   NUMBER;
        ------master
        l_master_shelf_days              NUMBER := 0;
        l_master_buyer_id                NUMBER := 0;
        l_userid                         NUMBER := fnd_global.user_id;
        l_loginid                        NUMBER := fnd_global.login_id;
        l_date                           DATE := SYSDATE;
        l_lot_control_code               NUMBER := 0;
        l_serial_number_control_code     NUMBER := 0;
        l_error_message                  VARCHAR2 (1000);
        l_category                       VARCHAR2 (20) := 'QQ.QQQQ';
        l_def_cat_id                     NUMBER;

        l_planner_code                   VARCHAR2(30);
        l_sourcing_org_id                NUMBER;
        l_source_type                    NUMBER;
        l_supplier_code                  VARCHAR2(100);


        CURSOR c_item_int
        IS
            SELECT   ROWID, xis.*
              FROM   xxwc_invitem_stg xis
             WHERE   status = 'M'
               AND   organization_code<>'MST'
               AND sourcing_from = NVL(g_sourcing_from, sourcing_from);

        --IF THE ITEM IS PROCESSED FOR MASTER THEN ONLY WE PROCEED FOR CHILD

        --UPDATED THE FLAG FOR ONLY MASTER PROCESSED RECORDS
        CURSOR c_item_master_int
        IS
            SELECT   DISTINCT partnumber,
                              primspartnumber,
                              unitofmeasure,
                              unitweight,
                              containertype,
                              unitlength,
                              unitwidth,
                              unitheight,
                              salesvelocitystorecount,
                              salesvelocityyearlystorecount,
                              --gtpindicator,
                              lotserial,
                              hazardyn,
                              hazardclass,
                              description,
                              unnumber,
                              category,
                              --velocityclassification,
                              shelf_life_code,
                              intangible,
                              kitflag,
                              --caprop65,                                  --att1
                              --pesticideflag,
                              --vocnumber,
                              --pesticideflagstate,
                              --voccategory,
                              --vocsubcategory,
                              msdsnumber,
                              extendeddescription,
                              hazarddesc, --attribute17,
                              attribute9,                          --, leadtime
                              consigned_flag
                             , private_label_category -- Shankar 20-Aug-2012
              FROM   xxwc_invitem_stg a
             WHERE   status = 'V'
               AND sourcing_from = NVL(g_sourcing_from, sourcing_from)
             -- added by Shankar 12-Jul-2012 to select only those items that are not in master
               AND not exists (select 'x' from mtl_system_items b
                                where a.primspartnumber=b.segment1
                                  and b.organization_id=222);

             -- added by Shankar 12-Jul-2012 to update items that are already in master
        CURSOR c_item_master_upd
        IS
            SELECT distinct primspartnumber
              FROM   xxwc_invitem_stg a
             WHERE   status = 'V'
               AND sourcing_from = NVL(g_sourcing_from, sourcing_from)
               AND exists (select 'x' from mtl_system_items b
                                where a.primspartnumber=b.segment1
                                  and b.organization_id=222);

        --hazard class cursor
        CURSOR c_hazard_class (p_hazardclass VARCHAR2)
        IS
            SELECT   hazard_class_id --added to remove default hazard class id
              FROM   po_hazard_classes
             WHERE   UPPER (hazard_class) = UPPER (p_hazardclass);

        --un numbers cursor
        CURSOR c_unnumber (p_unnumber VARCHAR2)
        IS
            SELECT   un_number_id
              FROM   po_un_numbers
             WHERE   UPPER (un_number) = UPPER (p_unnumber);

        --buyers / vendors cursor
        CURSOR c_buyer (
            p_buyer VARCHAR2)
        IS
            SELECT   pa.agent_id agent_id
              FROM   per_all_people_f ppf, po_agents pa
             WHERE   ppf.person_id = pa.agent_id
                     AND UPPER (ppf.full_name) = TRIM (UPPER (p_buyer));
    BEGIN
        SELECT   COUNT (1)
          INTO   l_interfaceprocess_count
          FROM   xxwc_invitem_stg xis
         WHERE   status = 'V'
         AND sourcing_from = NVL(g_sourcing_from, sourcing_from);

        -- print_debug ('Before For Loop......');
        SELECT   expense_account, cost_of_sales_account, organization_id
          INTO   l_master_expense_account,
                 l_master_cost_of_sales_account,
                 l_master_org_id
          FROM   mtl_parameters
         WHERE   UPPER (organization_code) = g_master_org_code;

        --print_debug ('l_master_org_id ' || l_master_org_id);

        --Added by Shankar 12-Jul-2012
        for c1_rec in c_item_master_upd
          loop
             update xxwc_invitem_stg
                set status='M'
              where primspartnumber=c1_rec.primspartnumber;
          end loop;
          commit;

        -------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------
        FOR r_item_master_int IN c_item_master_int
        LOOP
            l_error_message := NULL;
            l_un_number_id := NULL;
            l_hazard_class_id := NULL;

            BEGIN
                -- print_debug ('Expense Accounts Verification.....');

                --hazard class information
                --                SELECT NVL (hazard_class_id, 0)            --MAKE TO CURSOR  CHANGE1.1
                --                    INTO l_hazard_class_id
                --                    FROM po_hazard_classes
                --                   WHERE UPPER (hazard_class) =
                --                                          UPPER (NVL (r_item_master_int.hazardclass, '10.11'));

                FOR r_hazard_class
                IN                           --hazard class id --TO FIX ISSUE6
                  c_hazard_class (r_item_master_int.hazardclass)
                LOOP
                    BEGIN
                        l_hazard_class_id := r_hazard_class.hazard_class_id;
                    EXCEPTION
                        WHEN OTHERS
                        THEN
                            l_hazard_class_id := NULL;
                            print_debug (
                                'l_hazard_class_id is assigned to null');
                    END;
                END LOOP;

                -- print_debug ('l_hazard_class_id ' || l_hazard_class_id);
                --l_hazard_class_id := null;
                FOR r_unnumber IN            --hazard class id --TO FIX ISSUE6
                                 c_unnumber (r_item_master_int.unnumber)
                LOOP
                    BEGIN
                        l_un_number_id := r_unnumber.un_number_id;
                    EXCEPTION
                        WHEN OTHERS
                        THEN
                            l_un_number_id := NULL;
                            print_debug (
                                'l_un_number_id is assigned to null');
                    END;
                END LOOP;

                SELECT   DECODE (
                             UPPER (
                                 NVL (r_item_master_int.shelf_life_code, 0)),
                             'UNLIMITED',
                             10000,
                             'UNLIMTED',
                             10000,
                             '24 MONTHS',
                             l_days * 24,
                             '09 MONTHS',
                             l_days * 9,
                             '14 MONTHS',
                             l_days * 14,
                             '23 MONTHS',
                             l_days * 23,
                             '11 MONTHS',
                             l_days * 11,
                             '10 MONTHS',
                             l_days * 10,
                             '9 MONTHS',
                             l_days * 9,
                             '3 MONTHS',
                             l_days * 3,
                             '36 MONTHS',
                             l_days * 36,
                             '06 MONTHS',
                             l_days * 6,
                             '240 MONTHS',
                             l_days * 240,
                             '6 MONTHS',
                             l_days * 6,
                             '15 MONTHS',
                             l_days * 15,
                             '18 MONTHS',
                             l_days * 18,
                             '72 MONTHS',
                             l_days * 72,
                             '30 MONTHS',
                             l_days * 30,
                             '12 MONTHS',
                             l_days * 12,
                             '02 MONTHS',
                             l_days * 2,
                             '65 MONTHS',
                             l_days * 65,
                             '45 MONTHS',
                             l_days * 45,
                             '60 MONTHS',
                             l_days * 60,
                             '08 MONTHS',
                             l_days * 8,
                             '120 MONTHS',
                             l_days * 120,
                             '03 MONTHS',
                             l_days * 3,
                             '48 MONTHS',
                             l_days * 48,
                             0)
                  INTO   l_shelf_days
                  FROM   DUAL;

                IF l_shelf_days > 0
                THEN
                    l_shelf_life_code := 1;
                ELSE
                    l_shelf_life_code := 4;
                END IF;

              --  remove consigned item template on master org item                      -- rev 2.1  4/26/2013
--                IF nvl(r_item_master_int.consigned_flag,'N') = 'Y'                     -- rev 2.1  4/26/2013
--                THEN                                                                   -- rev 2.1  4/26/2013
--                    l_template_name := 'Consigned - Std';                              -- rev 2.1  4/26/2013
                /*
                ELSIF NVL (r_item_master_int.velocityclassification, 'XYZ') = 'Z'
                   AND r_item_master_int.lotserial = 0
                THEN
                    l_template_name := 'Discontinued Item - General';
                ELSIF NVL (r_item_master_int.velocityclassification, 'XYZ') = 'Z'
                   AND r_item_master_int.lotserial = 1
                THEN
                    l_template_name := 'Discontinued Item - Lot';
                ELSIF NVL (r_item_master_int.velocityclassification, 'XYZ') = 'Z'
                   AND r_item_master_int.lotserial = 2
                THEN
                    l_template_name := 'Discontinued Item - Serial'; */
                IF NVL (r_item_master_int.kitflag, 'N') = 'Y'                            -- rev 2.1  4/26/2013
                THEN
                    l_template_name := 'Kit';
                ELSIF NVL (r_item_master_int.intangible, 'N') = 'Y'
                THEN
                    l_template_name := 'Intangible';
                ELSIF r_item_master_int.category='99.99RR' then
                  l_template_name := 'Re-Rental';
                ELSIF r_item_master_int.category ='99.99RT'
                  AND r_item_master_int.lotserial = 0
                THEN
                    l_template_name := 'Rental - Std';
                ELSIF r_item_master_int.category ='99.99RT'
                  AND r_item_master_int.lotserial = 1
                THEN
                    l_template_name := 'Rental - Lot';
                ELSIF r_item_master_int.category ='99.99RT'
                  AND r_item_master_int.lotserial = 2
                THEN
                    l_template_name := 'Rental - Serial';
                ELSIF (SUBSTR (r_item_master_int.partnumber, 0, 2)) = 'SP'
                THEN
                    l_template_name := 'Special';
                ELSIF r_item_master_int.category='TH.TH01'
                THEN
                    l_template_name := 'Repair';
                /*
                ELSIF NVL (r_item_master_int.velocityclassification, 'Y') = 'N'
                  AND r_item_master_int.lotserial = 0
                THEN
                    l_template_name := 'Non-Stock Item - General';
                ELSIF NVL (r_item_master_int.velocityclassification, 'Y') = 'N'
                  AND r_item_master_int.lotserial = 1
                THEN
                    l_template_name := 'Non-Stock Item - Lot';
                ELSIF NVL (r_item_master_int.velocityclassification, 'Y') = 'N'
                  AND r_item_master_int.lotserial = 2
                THEN
                    l_template_name := 'Non-Stock Item - Serial'; */
                ELSIF r_item_master_int.lotserial = 0
                THEN
                    l_template_name := 'Stock Item - General';
                ELSIF r_item_master_int.lotserial = 1
                THEN
                    l_template_name := 'Stock Item - Lot';
                ELSIF r_item_master_int.lotserial = 2
                THEN
                    l_template_name := 'Stock Item - Serial';
                ELSE
                    l_template_name := NULL;
                END IF;

                IF r_item_master_int.lotserial = 0
                THEN
                    l_lot_control_code := 1;
                    l_serial_number_control_code := 1;
                ELSIF r_item_master_int.lotserial = 1
                THEN
                    l_lot_control_code := 2;
                    -- commented by Shankar 15-Feb-2012 for issue 208
                    --l_shelf_days := NULL;                 --fix the issue no 5
                    l_serial_number_control_code := 1;
                ELSIF r_item_master_int.lotserial = 2
                THEN
                    l_lot_control_code := 1;
                    --l_serial_number_control_code := 6;  -- Changed from 5 by Shankar 22-Feb-2012
                    l_serial_number_control_code := 1;  -- Changed from 5 by Shankar 22-Mar-2012 based on discussion with Ted
                END IF;

                ---Inserting into the master org
                INSERT INTO mtl_system_items_interface (organization_code,
                                                        organization_id,
                                                        segment1,
                                                        description,
                                                        long_description,
                                                        expense_account,
                                                        cost_of_sales_account,
                                                        primary_uom_code,
                                                        weight_uom_code, --TO FIX ISSUE3
                                                        inventory_item_status_code,
                                                        process_flag,
                                                        transaction_type,
                                                        set_process_id,
                                                        unit_weight,
                                                        shelf_life_days, --shelf_life_code,--TO FIX ISSUE5
                                                        attribute18, --container_type_code,
                                                        un_number_id,
                                                        hazard_class_id,
                                                        list_price_per_unit,
                                                        tax_code,
                                                        min_minmax_quantity,
                                                        max_minmax_quantity,
                                                        lot_control_code,
                                                        serial_number_control_code,
                                                        hazardous_material_flag,
                                                        revision, --derive with buyer name
                                                        template_name,
                                                        attribute8,   --MSDS #
                                                        attribute12, --sales velocity
                                                        attribute14, --sales velocity early
                                                        attribute16,
                                                        --attribute19, -- preprocessing_lead_time
                                                        --TO FIX ISSUE2 --attribute30
                                                        --,
                                                        --safety_stock_bucket_days, --TO FIX ISSUE 11
                                                        last_update_date,
                                                        last_updated_by,
                                                        creation_date,
                                                        created_by,
                                                        last_update_login,
                                                        attribute_category --TO FIX ISSUE1
                                                       ,attribute17
                                                       ,attribute9
                                                        )
                  VALUES   (g_master_org_code,
                            l_master_org_id,
                            r_item_master_int.partnumber,
                            r_item_master_int.description,
                            r_item_master_int.extendeddescription,
                            l_master_expense_account,
                            l_master_cost_of_sales_account,
                            r_item_master_int.unitofmeasure,
                            'LBS',                             --TO FIX ISSUE3
                            l_inventory_item_status_code,
                            g_process_flag,
                            g_master_transaction_type,
                            l_process_set_id,
                            r_item_master_int.unitweight,
                            l_shelf_days,  --l_shelf_life_code,--TO FIX ISSUE5
                            NVL (r_item_master_int.containertype, ''), --r_item_master_int.unitlength,
                            l_un_number_id,
                            l_hazard_class_id,
                            0,
                            NULL,                --r_item_master_int.tax_code,
                            0,                  --r_item_master_int.minmaxmin,
                            0,                  --r_item_master_int.minmaxmax,
                            l_lot_control_code,  --r_item_master_int.lotserial
                            l_serial_number_control_code,
                            r_item_master_int.hazardyn,
                            --NVL (l_buyer_id, 37898),
                            0, --REVISION,
                            --  l_template_id,
                            l_template_name,
                            r_item_master_int.msdsnumber,
                            r_item_master_int.salesvelocitystorecount,
                            r_item_master_int.salesvelocityyearlystorecount,
                            r_item_master_int.primspartnumber,        --attr16
                            l_date,
                            l_userid,
                            l_date,
                            l_userid,
                            l_loginid,
                            'WC'                               --TO FIX ISSUE1
                            ,r_item_master_int.hazarddesc
                            ,r_item_master_int.attribute9
                            );

                --                print_debug ('After Inserting into MASTER');

                select default_category_id
                  into l_def_cat_id
                  From MTL_CATEGORY_SETS_V
                 where category_set_name='Inventory Category';

                ---Inserting into Item Categeries for master
                INSERT INTO mtl_item_categories_interface (organization_id,
                                                           organization_code,
                                                           item_number,
                                                           category_name,
                                                           process_flag,
                                                           transaction_type,
                                                           old_category_id,
                                                           set_process_id,
                                                           category_set_name,
                                                           last_update_date,
                                                           last_updated_by,
                                                           creation_date,
                                                           created_by,
                                                           last_update_login)
                  VALUES   (l_master_org_id,
                            g_master_org_code,
                            r_item_master_int.partnumber,        --l_category,
                            r_item_master_int.category,
                            g_process_flag,
                            'UPDATE', --g_master_transaction_type,
                            l_def_cat_id,
                            l_process_set_id,
                            NVL (l_category_set_name, 'Inventory Category'),
                            l_date,
                            l_userid,
                            l_date,
                            l_userid,
                            l_loginid);

                   -- Shankar 20-Aug-2012
               IF r_item_master_int.private_label_category is not null then
                ---Inserting into Item Categeries for master
                INSERT INTO mtl_item_categories_interface (organization_id,
                                                           organization_code,
                                                           item_number,
                                                           category_name,
                                                           process_flag,
                                                           transaction_type,
                                                           --old_category_id,
                                                           set_process_id,
                                                           category_set_name,
                                                           last_update_date,
                                                           last_updated_by,
                                                           creation_date,
                                                           created_by,
                                                           last_update_login)
                  VALUES   (l_master_org_id,
                            g_master_org_code,
                            r_item_master_int.partnumber,
                            r_item_master_int.private_label_category,
                            g_process_flag,
                            'CREATE',
                            --l_def_cat_id,
                            l_process_set_id,
                            'PRIVATE LABEL',
                            l_date,
                            l_userid,
                            l_date,
                            l_userid,
                            l_loginid);
               END IF;
                ---UPDATING THE STAGING TABLE WITH MASTER PROCESSED RECORDS INFO
                UPDATE   xxwc_invitem_stg xis
                   SET   status = 'M'
                 WHERE   partnumber = r_item_master_int.partnumber
                         AND primspartnumber =
                                r_item_master_int.primspartnumber
                         AND unitofmeasure = r_item_master_int.unitofmeasure
                         AND NVL (unitweight, 1) =
                                NVL (r_item_master_int.unitweight, 1)
                         AND NVL (containertype, 1) =
                                NVL (r_item_master_int.containertype, 1)
                         AND NVL (unitlength, 1) =
                                NVL (r_item_master_int.unitlength, 1)
                         AND NVL (unitwidth, 1) =
                                NVL (r_item_master_int.unitwidth, 1)
                         AND NVL (unitheight, 1) =
                                NVL (r_item_master_int.unitheight, 1)
                         AND NVL (salesvelocitystorecount, 1) =
                                NVL (
                                    r_item_master_int.salesvelocitystorecount,
                                    1)
                         AND NVL (salesvelocityyearlystorecount, 1) =
                                NVL (
                                    r_item_master_int.salesvelocityyearlystorecount,
                                    1)
                         AND NVL (lotserial, 1) =
                                NVL (r_item_master_int.lotserial, 1)
                         AND NVL (hazardyn, 1) =
                                NVL (r_item_master_int.hazardyn, 1)
                         --AND NVL (hazardclass, 1) =
                           --     NVL (r_item_master_int.hazardclass, 1)
                         AND NVL (description, 1) =
                                NVL (r_item_master_int.description, 1)
                         AND NVL (unnumber, 1) =
                                NVL (r_item_master_int.unnumber, 1)
                         AND NVL (category, 1) =
                                NVL (r_item_master_int.category, 1)
                         AND NVL (shelf_life_code, 1) =
                                NVL (r_item_master_int.shelf_life_code, 1)
                         --AND NVL (velocityclassification, 1) =
                           --     NVL (r_item_master_int.velocityclassification, 1)
                         AND NVL (intangible, 1) =
                                NVL (r_item_master_int.intangible, 1)
                         AND NVL (kitflag, 1) =
                                NVL (r_item_master_int.kitflag, 1);
                COMMIT;
            -- print_debug ('After insert of the Categories.......');
            EXCEPTION
                WHEN OTHERS
                THEN
                    print_debug (
                        'Master Exception ' || SQLCODE || '  ' || SQLERRM);
                    l_error_message :=
                        'Exception ' || SQLCODE || '  ' || SQLERRM;

                    UPDATE   xxwc_invitem_stg xis
                       SET   status = 'E', error_message = l_error_message
                     WHERE   partnumber = r_item_master_int.partnumber
                             AND primspartnumber =
                                    r_item_master_int.primspartnumber
                             AND unitofmeasure =
                                    r_item_master_int.unitofmeasure
                             AND NVL (unitweight, 1) =
                                    NVL (r_item_master_int.unitweight, 1)
                             AND NVL (containertype, 1) =
                                    NVL (r_item_master_int.containertype, 1)
                             AND NVL (unitlength, 1) =
                                    NVL (r_item_master_int.unitlength, 1)
                             AND NVL (unitwidth, 1) =
                                    NVL (r_item_master_int.unitwidth, 1)
                             AND NVL (unitheight, 1) =
                                    NVL (r_item_master_int.unitheight, 1)
                             AND NVL (salesvelocitystorecount, 1) =
                                    NVL (
                                        r_item_master_int.salesvelocitystorecount,
                                        1)
                             AND NVL (salesvelocityyearlystorecount, 1) =
                                    NVL (
                                        r_item_master_int.salesvelocityyearlystorecount,
                                        1)
                             AND NVL (lotserial, 1) =
                                    NVL (r_item_master_int.lotserial, 1)
                             AND NVL (hazardyn, 1) =
                                    NVL (r_item_master_int.hazardyn, 1)
                             --AND NVL (hazardclass, 1) =
                               --     NVL (r_item_master_int.hazardclass, 1)
                             AND NVL (description, 1) =
                                    NVL (r_item_master_int.description, 1)
                             AND NVL (unnumber, 1) =
                                    NVL (r_item_master_int.unnumber, 1)
                             AND NVL (category, 1) =
                                    NVL (r_item_master_int.category, 1)
                             AND NVL (shelf_life_code, 1) =
                                    NVL (r_item_master_int.shelf_life_code,
                                         1)
                             --AND NVL (velocityclassification, 1) =
                               --     NVL (r_item_master_int.velocityclassification, 1)
                             AND NVL (intangible, 1) =
                                    NVL (r_item_master_int.intangible, 1)
                             AND NVL (kitflag, 1) =
                                    NVL (r_item_master_int.kitflag, 1);
                             --AND NVL(hazarddesc,'x')=nvl(r_item_master_int.hazarddesc,'x')       ;

                    COMMIT;
            END;
        END LOOP;

        --      COMMIT;
        l_process_set_id := l_process_set_id + 1;
        l_shelf_days := 0;
        l_shelf_life_code := 4;
        l_un_number_id := 0;
        l_hazard_class_id := 0;
        l_buyer_id := 0;
        l_template_id := 0;
        l_template_name := '';
        print_debug ('Child Org Insertion......');

        ----------------------------------------------------------------------------------
        FOR rec_item_int IN c_item_int
        LOOP
            l_error_message := NULL;
            l_hazard_class_id := NULL;
            l_un_number_id := NULL;
            l_buyer_id := NULL;

            BEGIN
                --print_debug ('Expense Accounts Verification.....');

                --getting expence and cost of sales account and org id
                SELECT   expense_account,
                         cost_of_sales_account,
                         organization_id,
                         sales_account
                  INTO   l_expense_account,
                         l_cost_of_sales_account,
                         l_organization_id,
                         l_sales_account
                  FROM   mtl_parameters
                 WHERE   UPPER (organization_code) = UPPER (rec_item_int.organization_code);


                 select a.code_combination_id
                   into l_new_cogs_account
                   from gl_code_combinations a,gl_code_combinations b
                  where b.code_combination_id=l_cost_of_sales_account
                    and b.segment1=a.segment1
                    and b.segment2=a.segment2
                    and b.segment3=a.segment3
                    and a.segment4=rec_item_int.cogs_account
                    and b.segment5=a.segment5
                    and b.segment6=a.segment6
                    and b.segment7=a.segment7;


                 select a.code_combination_id
                   into l_new_sales_account
                   from gl_code_combinations a,gl_code_combinations b
                  where b.code_combination_id=l_sales_account
                    and b.segment1=a.segment1
                    and b.segment2=a.segment2
                    and b.segment3=a.segment3
                    and a.segment4=rec_item_int.revenue_account
                    and b.segment5=a.segment5
                    and b.segment6=a.segment6
                    and b.segment7=a.segment7;


                print_debug(   l_expense_account
                            || l_cost_of_sales_account
                            || l_organization_id);

                --getting shelf days and shelf code
                SELECT   DECODE (
                             UPPER (NVL (rec_item_int.shelf_life_code, 0)),
                             'UNLIMITED',
                             10000,
                             'UNLIMTED',
                             10000,
                             '24 MONTHS',
                             l_days * 24,
                             '09 MONTHS',
                             l_days * 9,
                             '14 MONTHS',
                             l_days * 14,
                             '23 MONTHS',
                             l_days * 23,
                             '11 MONTHS',
                             l_days * 11,
                             '10 MONTHS',
                             l_days * 10,
                             '9 MONTHS',
                             l_days * 9,
                             '3 MONTHS',
                             l_days * 3,
                             '36 MONTHS',
                             l_days * 36,
                             '06 MONTHS',
                             l_days * 6,
                             '240 MONTHS',
                             l_days * 240,
                             '6 MONTHS',
                             l_days * 6,
                             '15 MONTHS',
                             l_days * 15,
                             '18 MONTHS',
                             l_days * 18,
                             '72 MONTHS',
                             l_days * 72,
                             '30 MONTHS',
                             l_days * 30,
                             '12 MONTHS',
                             l_days * 12,
                             '02 MONTHS',
                             l_days * 2,
                             '65 MONTHS',
                             l_days * 65,
                             '45 MONTHS',
                             l_days * 45,
                             '60 MONTHS',
                             l_days * 60,
                             '08 MONTHS',
                             l_days * 8,
                             '120 MONTHS',
                             l_days * 120,
                             '03 MONTHS',
                             l_days * 3,
                             '48 MONTHS',
                             l_days * 48,
                             0)
                  INTO   l_shelf_days
                  FROM   xxwc_invitem_stg
                 WHERE   ROWID = rec_item_int.ROWID;

                print_debug ('bEFORE shelf life');

                IF NVL (l_shelf_days, -1) > 0
                THEN
                    l_shelf_life_code := 1;
                ELSE
                    l_shelf_life_code := 4;
                END IF;

                print_debug ('bEFORE HAZARD');

                FOR r_hazard_class
                IN                                           --hazard class id
                  c_hazard_class (rec_item_int.hazardclass)
                LOOP
                    BEGIN
                        l_hazard_class_id := r_hazard_class.hazard_class_id;
                    EXCEPTION
                        WHEN OTHERS
                        THEN
                            l_hazard_class_id := NULL;
                            print_debug (
                                'l_hazard_class_id is assigned to null');
                    END;
                END LOOP;

                print_debug ('bEFORE UN NUMBER');

                FOR r_unnumber IN                            --hazard class id
                                 c_unnumber (rec_item_int.unnumber)
                LOOP
                    BEGIN
                        l_un_number_id := r_unnumber.un_number_id;
                    EXCEPTION
                        WHEN OTHERS
                        THEN
                            l_un_number_id := NULL;
                            print_debug (
                                'l_un_number_id is assigned to null');
                    END;
                END LOOP;


                print_debug ('bEFORE TEMPLATE NAME');

                IF nvl(rec_item_int.consigned_flag,'N') = 'Y'
                THEN
                    l_template_name := 'Consigned - Std';
                ELSIF NVL (rec_item_int.kitflag, 'N') = 'Y'
                THEN
                    l_template_name := 'Kit';
                ELSIF NVL (rec_item_int.intangible, 'N') = 'Y'
                THEN
                    l_template_name := 'Intangible';
                ELSIF NVL (rec_item_int.velocityclassification, 'XYZ') = 'Z'
                   AND rec_item_int.lotserial = 0
                THEN
                    l_template_name := 'Discontinued Item - General';
                ELSIF NVL (rec_item_int.velocityclassification, 'XYZ') = 'Z'
                   AND rec_item_int.lotserial = 1
                THEN
                    l_template_name := 'Discontinued Item - Lot';
                ELSIF NVL (rec_item_int.velocityclassification, 'XYZ') = 'Z'
                   AND rec_item_int.lotserial = 2
                THEN
                    l_template_name := 'Discontinued Item - Serial';
                ELSIF rec_item_int.category='99.99RR' then
                  l_template_name := 'Re-Rental';
                ELSIF rec_item_int.category ='99.99RT'
                  AND rec_item_int.lotserial = 0
                THEN
                    l_template_name := 'Rental - Std';
                ELSIF rec_item_int.category ='99.99RT'
                  AND rec_item_int.lotserial = 1
                THEN
                    l_template_name := 'Rental - Lot';
                ELSIF rec_item_int.category ='99.99RT'
                  AND rec_item_int.lotserial = 2
                THEN
                    l_template_name := 'Rental - Serial';
                ELSIF (SUBSTR (rec_item_int.partnumber, 0, 2)) = 'SP'
                THEN
                    l_template_name := 'Special';
                ELSIF rec_item_int.category='TH.TH01'
                THEN
                    l_template_name := 'Repair';
                ELSIF NVL (rec_item_int.velocityclassification, 'Y') = 'N'
                  AND rec_item_int.lotserial = 0
                THEN
                    l_template_name := 'Non-Stock Item - General';
                ELSIF NVL (rec_item_int.velocityclassification, 'Y') = 'N'
                  AND rec_item_int.lotserial = 1
                THEN
                    l_template_name := 'Non-Stock Item - Lot';
                ELSIF NVL (rec_item_int.velocityclassification, 'Y') = 'N'
                  AND rec_item_int.lotserial = 2
                THEN
                    l_template_name := 'Non-Stock Item - Serial';
                ELSIF rec_item_int.lotserial = 0
                THEN
                    l_template_name := 'Stock Item - General';
                ELSIF rec_item_int.lotserial = 1
                THEN
                    l_template_name := 'Stock Item - Lot';
                ELSIF rec_item_int.lotserial = 2
                THEN
                    l_template_name := 'Stock Item - Serial';
                ELSE
                    l_template_name := NULL;
                END IF;

                print_debug ('bEFORE lot serial');

                IF rec_item_int.lotserial = 0
                THEN
                    l_lot_control_code := 1;
                    l_serial_number_control_code := 1;
                ELSIF rec_item_int.lotserial = 1
                THEN
                    l_lot_control_code := 2;
                    -- commented by Shankar 15-Feb-2012 for issue 208
                    --l_shelf_days := NULL;                 --fix the issue no 5
                    l_serial_number_control_code := 1;
                ELSIF rec_item_int.lotserial = 2
                THEN
                    l_lot_control_code := 1;
                    --l_serial_number_control_code := 6;
                    l_serial_number_control_code := 1;  -- Changed from 5 by Shankar 22-Mar-2012 based on discussion with Ted
                END IF;

               -- Added by Shankar 09-Jan-2012
               l_planner_code := null;
               l_buyer_id := null;
               BEGIN
                select employee_id
                  into l_buyer_id
                  from mtl_planners a, mtl_parameters b
                 where a.organization_id=b.organization_id
                   and b.organization_code=rec_item_int.organization_code
                   and a.planner_code=rec_item_int.buyer;
                   l_planner_code := rec_item_int.buyer;
               EXCEPTION
                 when others then
                select employee_id
                  into l_buyer_id
                  from mtl_planners a, mtl_parameters b
                 where a.organization_id=b.organization_id
                   and b.organization_code=rec_item_int.organization_code
                   and a.planner_code='QQQQQQ';
                   l_planner_code := 'QQQQQQ';
               END;


               -- Added by Shankar 17-Jan-2011
               IF rec_item_int.sourcing_From = 'INVENTORY' then
                l_source_type := 1;
                select organization_id
                  into l_sourcing_org_id
                  from org_organization_definitions
                 where organization_code = lpad(rec_item_int.inv_org,3,'0');
                l_supplier_code := null;

               ELSE
                l_source_type := 2;
                l_sourcing_org_id := null;
                l_supplier_code := rec_item_int.inv_org;
               END IF;


                ---Inserting into the Child org
                -- print_debug ('bEFORE child insert');
                INSERT INTO mtl_system_items_interface (organization_code,
                                                        organization_id,
                                                        segment1,
                                                        description,
                                                        long_description,
                                                        expense_account,
                                                        cost_of_sales_account,
                                                        sales_account,
                                                        primary_uom_code,
                                                        weight_uom_code, --TO FIX ISSUE3
                                                        --dimension_uom_code,--TO FIX ISSUE4
                                                        inventory_item_status_code,
                                                        process_flag,
                                                        transaction_type,
                                                        set_process_id,
                                                        unit_weight,
                                                        shelf_life_days, --shelf_life_code,
                                                        attribute18, --container_type_code,
                                                        un_number_id,
                                                        hazard_class_id,
                                                        list_price_per_unit,
                                                        lot_control_code,
                                                        tax_code,
                                                        min_minmax_quantity,
                                                        max_minmax_quantity,
                                                        serial_number_control_code,
                                                        hazardous_material_flag,
                                                        buyer_id,
                                                        planner_code,
                                                        revision, --template_id,
                                                        template_name,
                                                        attribute1,  --capro65
                                                        attribute3, --pestiside flag
                                                        attribute4,    --VOC #
                                                        attribute5, --Pesticide Flag State
                                                        attribute6, --VOC Category
                                                        attribute7, --VOC Sub Category
                                                        attribute8,   --MSDS #
                                                        attribute12, --sales velocity  Store Velocity
                                                        attribute14, --sales velocity early  --Yearly Store Velocity
                                                        attribute16, --PRISM Part Number
                                                        attribute19, --gto indicator
                                                        --attribute20, --template
                                                        --attribute21,    --loc1
                                                        attribute22,    --tax_code shankar 14-feb-2012
                                                        --attribute23,    --loc3 shankar 09-Jan-2012
                                                       -- attribute24, --package group
                                                        attribute20, --average units
                                                        attribute26, --product code
                                                        attribute27, --duty rate
                                                        --preprocessing_lead_time,     --TO FIX ISSUE2
                                                        --attribute30,                    --Calc Lead Time
                                                        safety_stock_bucket_days, --TO FIX ISSUE 11
                                                        --attribute2,--Last Lead Time
                                                        --attribute9,--Market Branch Cost
                                                        --attribute10,--Market Branch Cost Override
                                                        --attribute11,--Market Cost Override Date
                                                        --attribute13,--DC Velocity
                                                        --ATTRIBUTE15 --Yearly DC Velocity
                                                        last_update_date,
                                                        last_updated_by,
                                                        creation_date,
                                                        created_by,
                                                        last_update_login,
                                                        attribute_category, --TO FIX ISSUE1
                                                        full_lead_time --, unit_of_issue
                                                       ,attribute11
                                                       ,attribute10
                                                       ,attribute17
                                                       ,attribute9
                                                       ,attribute21
                                                       ,preprocessing_lead_time
                                                       ,fixed_lot_multiplier
                                                       ,source_type
                                                       ,source_organization_id
                                                       ,global_attribute11
                                                                      )
                  VALUES   (rec_item_int.organization_code,
                            l_organization_id,
                            rec_item_int.partnumber,
                            rec_item_int.description,
                            rec_item_int.extendeddescription,
                            l_expense_account,
                            l_new_cogs_account, --l_cost_of_sales_account,
                            l_new_sales_account, -- Shankar 15-Feb-2012
                            rec_item_int.unitofmeasure,
                            'LBS',                             --TO FIX ISSUE3
                            l_inventory_item_status_code,
                            g_process_flag,
                            g_transaction_type,
                            l_process_set_id,
                            rec_item_int.unitweight,
                            l_shelf_days, --l_shelf_life_code,
                            rec_item_int.containertype,
                            l_un_number_id,
                            l_hazard_class_id,
                            rec_item_int.listprice,
                            l_lot_control_code,
                            NULL,                     --rec_item_int.tax_code,
                            rec_item_int.minmaxmin,
                            rec_item_int.minmaxmax,
                            l_serial_number_control_code,
                            rec_item_int.hazardyn,
                            l_buyer_id,
                            l_planner_code,
                            0,                                     --REVISION,
                            --NVL(l_template_id,1),
                            --rec_item_int.template,
                            l_template_name,
                            rec_item_int.caprop65,                      --att1
                            rec_item_int.pesticideflag,
                            rec_item_int.vocnumber,
                            rec_item_int.pesticideflagstate,
                            rec_item_int.voccategory,
                            rec_item_int.vocsubcategory,
                            rec_item_int.msdsnumber,
                            rec_item_int.salesvelocitystorecount,      --att12
                            rec_item_int.salesvelocityyearlystorecount, --att14
                            rec_item_int.primspartnumber,              --att16
                            rec_item_int.gtpindicator,                 --att19
                            rec_item_int.tax_code, -- shankar 14-feb-2012
                            --rec_item_int.itemlocation1,
                            --rec_item_int.itemlocation2, shankar 09-Jan-2012
                            --rec_item_int.itemlocation3,shankar 09-Jan-2012
                            --rec_item_int.packagegroup,
                            rec_item_int.averageunits,
                            rec_item_int.productcode,
                            rec_item_int.dutyrate, --rec_item_int.leadtime,--TO FIX ISSUE2
                            rec_item_int.leadtime,           --TO FIX ISSUE 11
                            --r_item_master_int.lotserial
                            l_date,
                            l_userid,
                            l_date,
                            l_userid,
                            l_loginid,
                            'WC',                               --TO FIX ISSUE1
                            rec_item_int.leadtime--, rec_item_int.unitofmeasure
                           ,rec_item_int.attribute11
                           ,rec_item_int.attribute10
                           ,rec_item_int.hazarddesc
                           ,nvl(rec_item_int.packagegroup,rec_item_int.attribute9)
                           ,rec_item_int.reservedstock
                           ,rec_item_int.reviewtime
                           ,rec_item_int.fixed_lot
                           ,l_source_type
                           ,l_sourcing_org_id
                           ,l_supplier_code
                            );

                /* Populate data into interface table for child organization */
                --                SELECT category_set_name
                --                    INTO l_category_set_name
                --                    FROM mtl_category_sets
                --                   WHERE structure_id =
                --                  (SELECT DISTINCT structure_id
                --                   FROM mtl_categories_b
                --                   WHERE UPPER (segment1) =
                --                   UPPER (NVL (rec_item_int.CATEGORY,
                --                    '00'
                --                     )
                --                    ));
                --                print_debug
                -- ('Before Insert mtl_item_categories_interface.......for Child');

                ---Inserting into Item Categeries for Child orgs
                IF rec_item_int.velocityclassification IS NOT NULL
                THEN
                    INSERT INTO mtl_item_categories_interface (organization_id,
                                                               organization_code,
                                                               item_number,
                                                               category_name,
                                                               process_flag,
                                                               transaction_type,
                                                               set_process_id,
                                                               category_set_name,
                                                               last_update_date,
                                                               last_updated_by,
                                                               creation_date,
                                                               created_by,
                                                               last_update_login)
                      VALUES   (l_organization_id,
                                rec_item_int.organization_code, --g_master_org_code
                                rec_item_int.partnumber,          --l_category
                                rec_item_int.velocityclassification, --rec_item_int.CATEGORY,
                                g_process_flag,
                                g_transaction_type,
                                l_process_set_id,
                                'Sales Velocity', --NVL (l_category_set_name, 'Inventory Category'),
                                l_date,
                                l_userid,
                                l_date,
                                l_userid,
                                l_loginid);
                END IF;

                IF rec_item_int.purchasingflag IS NOT NULL
                THEN
                    INSERT INTO mtl_item_categories_interface (organization_id,
                                                               organization_code,
                                                               item_number,
                                                               category_name,
                                                               process_flag,
                                                               transaction_type,
                                                               set_process_id,
                                                               category_set_name,
                                                               last_update_date,
                                                               last_updated_by,
                                                               creation_date,
                                                               created_by,
                                                               last_update_login)
                      VALUES   (l_organization_id,
                                rec_item_int.organization_code, --g_master_org_code
                                rec_item_int.partnumber,          --l_category
                                rec_item_int.purchasingflag, --rec_item_int.CATEGORY,
                                g_process_flag,
                                g_transaction_type,
                                l_process_set_id,
                                'Purchase Flag', --NVL (l_category_set_name, 'Inventory Category'),
                                l_date,
                                l_userid,
                                l_date,
                                l_userid,
                                l_loginid);
                END IF;


                UPDATE   xxwc_invitem_stg xis
                   SET   status = 'P', template = l_template_name  --CHANGE1.1
                 --CATEGORY = l_category
                 WHERE   ROWID = rec_item_int.ROWID;
            EXCEPTION
                WHEN OTHERS
                THEN
                    -- ROLLBACK;
                    print_debug (
                        'Child Exception ' || SQLCODE || '  ' || SQLERRM);
                    l_error_message :=
                        'Child Exception ' || SQLCODE || '  ' || SQLERRM;

                    UPDATE   xxwc_invitem_stg xis
                       SET   status = 'E', error_message = l_error_message
                     WHERE   ROWID = rec_item_int.ROWID;

                    COMMIT;
            ---Inserting into Item Cross references
            END;
        END LOOP;

        COMMIT;
        print_debug (
            'Number Of Records Processing: ' || l_interfaceprocess_count);

        SELECT   COUNT (1)
          INTO   l_interfacerec_count
          FROM   xxwc_invitem_stg xis
         WHERE   status = 'P'
         AND sourcing_from = NVL(g_sourcing_from, sourcing_from);

        print_debug (
            'Number Of Records in Interface Table: ' || l_interfacerec_count);
    END item_insert;

    PROCEDURE process_item (errbuf               OUT VARCHAR2,
                            retcode              OUT NUMBER,
                            p_validate_only   IN     VARCHAR2, --,p_submit IN VARCHAR2 --consolidated to one parameter
                            p_sourcing_from   IN  VARCHAR2)
    IS
        l_count          NUMBER := 0;
        l_count_valid    NUMBER := 0;
        v_errorcode      NUMBER;
        v_errormessage   VARCHAR2 (240);

        CURSOR c_master
        IS
            SELECT   DISTINCT organization_code
              FROM   xxwc_invitem_stg
             WHERE   NVL (status, 'R') IN ('R', 'E')
              AND sourcing_from = NVL(g_sourcing_from, sourcing_from);

        CURSOR c_items (
            p_org VARCHAR2)
        IS
            SELECT   *
              FROM   xxwc_invitem_stg
             WHERE   UPPER (organization_code) = UPPER (p_org)
                     AND NVL (status, 'R') IN ('R', 'E')
         AND sourcing_from = NVL(g_sourcing_from, sourcing_from);
    BEGIN

       g_sourcing_from := p_sourcing_from;

      update xxwc_invitem_stg
         set category='x'
       where category is null;

      update xxwc_invitem_stg
         set lotserial=0
       where lotserial is null;

       commit;

        IF p_validate_only = 'Y'
        THEN
            SELECT   COUNT (1)
              INTO   l_count
              FROM   xxwc_invitem_stg
             WHERE   NVL (status, 'R') IN ('R', 'E')
               AND sourcing_from = NVL(g_sourcing_from, sourcing_from);

            IF l_count > 0
            THEN
                xxwc_itemcategconv_pkg.print_debug ('Performing Validations');
                xxwc_itemcategconv_pkg.validations;
            ELSE
                xxwc_itemcategconv_pkg.print_debug ('No Records To Process');
            END IF;
        ELSE
            SELECT   COUNT (1)
              INTO   l_count
              FROM   xxwc_invitem_stg
             WHERE   NVL (status, 'R') IN ('R', 'E')
               AND sourcing_from = NVL(g_sourcing_from, sourcing_from);

            IF l_count > 0
            THEN
                xxwc_itemcategconv_pkg.print_debug ('Performing Validations');
                xxwc_itemcategconv_pkg.validations;
            ELSE
                xxwc_itemcategconv_pkg.print_debug ('No Records To Process');
            END IF;

            SELECT   COUNT (1)
              INTO l_count_valid FROM xxwc_invitem_stg
             WHERE status = 'V'
               AND sourcing_from = NVL(g_sourcing_from, sourcing_from);

            IF l_count_valid > 0
            THEN
                xxwc_itemcategconv_pkg.print_debug ('Performing Insert');
                xxwc_itemcategconv_pkg.item_insert;
            ELSE
                xxwc_itemcategconv_pkg.print_debug ('No Records To Insert');
            END IF;
        END IF;
    END process_item;

    PROCEDURE process_item_locators (errbuf    OUT VARCHAR2,
                                     retcode   OUT VARCHAR2)
    IS
        l_return_status           VARCHAR2 (1) := fnd_api.g_ret_sts_success;
        l_msg_count               NUMBER;
        l_msg_data                VARCHAR2 (4000);
        l_subinventory_code       VARCHAR2(20);
        l_inventory_location_id   NUMBER;
        l_locator_exists          VARCHAR2 (1);
        l_processig_records       NUMBER;
        l_rejected_records        NUMBER;
        l_itemreturn_status       VARCHAR2 (1) := fnd_api.g_ret_sts_success;
        l_itemmsg_count           NUMBER DEFAULT 0;
        l_itemmsg_data            VARCHAR2 (4000) DEFAULT NULL;
        l_msg_index_out           NUMBER;
        l_error_msg               VARCHAR2 (2000) := NULL;

        CURSOR c_locator_stg
        IS
            SELECT   ROWID,
                     organization_code,
                     itemlocation1||'.' itemlocation,  -- Version 2.2
                     partnumber
              FROM   xxwc_invitem_stg xxwcis
             WHERE       itemlocation1 IS NOT NULL
                     AND status = 'P' 
                     AND nvl(process_status,'R') in ('R','E')
            UNION
            SELECT   ROWID,
                     organization_code,
                     itemlocation2||'.' itemlocation,  -- Version 2.2
                     partnumber
              FROM   xxwc_invitem_stg xxwcis
             WHERE       itemlocation2 IS NOT NULL
                     AND status = 'P' 
                     AND nvl(process_status,'R') in ('R','E')
            UNION
            SELECT   ROWID,
                     organization_code,
                     itemlocation3||'.' itemlocation,  -- Version 2.2
                     partnumber
              FROM   xxwc_invitem_stg xxwcis
             WHERE       itemlocation3 IS NOT NULL
                     AND status = 'P' 
                     AND nvl(process_status,'R') in ('R','E');

    BEGIN
        mo_global.init ('INV');

        SELECT   COUNT (1)
          INTO   l_processig_records
          FROM   xxwc_invitem_stg xxwcis
         WHERE   status = 'P'
           AND   nvl(process_status,'R') in ('R','E');

        --mo_global.set_org_context('7186','0','INV');
  --      print_debug ('Item Locator Process Started......!');
    --    print_debug ('Number of processing records :' || l_processig_records);

        FOR r_locator_stg IN c_locator_stg
        LOOP
            l_error_msg := NULL;
            l_inventory_location_id := NULL;
            l_locator_exists := NULL;
            l_msg_data := NULL;
            l_msg_count := NULL;
            l_return_status := NULL;

            inv_loc_wms_pub.create_locator (
                x_return_status              => l_return_status,
                x_msg_count                  => l_msg_count,
                x_msg_data                   => l_msg_data,
                x_inventory_location_id      => l_inventory_location_id,
                x_locator_exists             => l_locator_exists,
                p_organization_id            => NULL,
                p_organization_code          => r_locator_stg.organization_code, --'001',
                p_concatenated_segments      => r_locator_stg.itemlocation, -- as per cv40 combine all the BINLOCATION1, BINLOCATION2 and BINLOCATION3
                --'1.1.1...',     --'Testing13_9',
                p_description                => '', -- r_locator_stg.description,--'Locator 1.2.3',--'Locator Teseting Prupose 121',
                p_inventory_location_type    => NULL,                     --3,
                p_picking_order              => NULL,
                p_location_maximum_units     => NULL,
                p_subinventory_code          => 'General', --'001', --'General',
                p_location_weight_uom_code   => NULL,
                p_max_weight                 => NULL,
                p_volume_uom_code            => NULL,
                p_max_cubic_area             => NULL,
                p_x_coordinate               => NULL,
                p_y_coordinate               => NULL,
                p_z_coordinate               => NULL,
                p_physical_location_id       => NULL,
                p_pick_uom_code              => NULL,
                p_dimension_uom_code         => NULL,
                p_length                     => NULL,
                p_width                      => NULL,
                p_height                     => NULL,
                p_status_id                  => 1,
                p_dropping_order             => NULL,
                p_attribute_category         => '',
                p_attribute1                 => '',
                p_attribute2                 => '',
                p_attribute3                 => '',
                p_attribute4                 => '',
                p_attribute5                 => '',
                p_attribute6                 => '',
                p_attribute7                 => '',
                p_attribute8                 => '',
                p_attribute9                 => '',
                p_attribute10                => '',
                p_attribute11                => '',
                p_attribute12                => '',
                p_attribute13                => '',
                p_attribute14                => '',
                p_attribute15                => '',
                p_alias                      => '');
     --       print_debug ('Return Status: ' || l_return_status);

            IF (l_return_status = 'S')
            THEN
              NULL;
                --Success. Process Locator Id and check if locator exists
                --commit;
     --          print_debug ('Locator Id:' || l_inventory_location_id);
     --           print_debug ('Locator exists:' || l_locator_exists);
            ELSE
                FOR l_index IN 1 .. l_msg_count
                LOOP
                    l_msg_data := fnd_msg_pub.get (l_index, 'F');
     --            print_debug (REPLACE (l_msg_data, CHR (0), ' '));
                    --l_error_msg :=                         l_error_msg || '-' || SUBSTR (l_msg_data, 1, 50);
                END LOOP;
                l_error_msg :=                         l_error_msg || '-' || SUBSTR (l_msg_data, 1, 100);

                UPDATE   xxwc_invitem_stg
                   SET   error_message = l_error_msg, process_status = 'E'
                 WHERE   ROWID = r_locator_stg.ROWID;
            END IF;

            l_itemreturn_status := NULL;
            l_itemmsg_count := NULL;
            l_itemmsg_data := NULL;

            inv_loc_wms_pub.create_loc_item_tie (
                x_return_status           => l_itemreturn_status,
                x_msg_count               => l_itemmsg_count,
                x_msg_data                => l_itemmsg_data,
                p_inventory_item_id       => NULL,                --i.item_id,
                p_item                    => r_locator_stg.partnumber,
                p_organization_id         => NULL,        --i.organization_id,
                p_organization_code       => r_locator_stg.organization_code,
                p_subinventory_code       => 'General', --r_item_to_locator_stg.subinventory_code,
                p_inventory_location_id   => l_inventory_location_id, --i.locator_id,
                p_locator                 => r_locator_stg.itemlocation, -- ite may change further
                p_status_id               => 1 --p_par_level                          => i.
                                              );

     --       print_debug ('Return Status = ' || l_itemreturn_status);

            IF l_itemreturn_status = 'S'
            THEN
                UPDATE   xxwc_invitem_stg
                   SET   process_status = 'P'
                 WHERE   ROWID = r_locator_stg.ROWID;

           --     COMMIT;
            ELSE
                FOR ln_index IN 1 .. l_itemmsg_count
                LOOP
                    oe_msg_pub.get (p_msg_index       => ln_index,
                                    p_encoded         => fnd_api.g_false,
                                    p_data            => l_itemmsg_data,
                                    p_msg_index_out   => l_msg_index_out);
                    --l_error_msg :=                         l_error_msg || '-' || SUBSTR (l_itemmsg_data, 1, 100);
                END LOOP;
                l_error_msg :=                         l_error_msg || '-' || SUBSTR (l_itemmsg_data, 1, 100);


                UPDATE   xxwc_invitem_stg
                   SET   error_message = substr(l_error_msg,1,4000)
                   , process_status = 'E'
                 WHERE   ROWID = r_locator_stg.ROWID;
            END IF;
        END LOOP;

        SELECT   COUNT (1)
          INTO   l_rejected_records
          FROM   xxwc_invitem_stg xxwcis
         WHERE   process_status = 'E';

     --   print_debug ('Number of Error Out records :' || l_rejected_records);
     --    print_debug ('Item Locator Process Ends......!');


     --    print_debug ('Calling Item Defaults......!');

        for c1_rec in (select a.partnumber,b.organization_id, b.inventory_item_id, a.category
                         from xxwc_invitem_stg a, mtl_system_items b, mtl_parameters c
                        where a.partnumber=b.segment1
                          and a.organization_code=c.organization_code
                          and b.organization_id=c.organization_id
                          and a.status='P'
                          and not exists (select 'x' from mtl_item_sub_defaults d
                                           where d.inventory_item_id=b.inventory_item_id
                                             and d.organization_id=b.organization_id
                                             and d.default_type=2)
                      )
            LOOP
              IF c1_rec.category in ('99.99RR','99.99RT') then -- ,'RE.RESI','RE.RESO') then
                 l_subinventory_code := 'Rental';
              ELSE
                 l_subinventory_code := 'General';
              END IF;
            INSERT INTO mtl_item_sub_defaults
                  (inventory_item_id,
                   organization_id,
                   subinventory_code,
                   default_type,
                   last_update_date,
                   last_updated_by,
                   creation_date,
                   created_by,
                   last_update_login
                  )
           VALUES (c1_rec.inventory_item_id,
                   c1_Rec.organization_id,
                   l_subinventory_code,
                   2,               --receiving
                   SYSDATE,
                   fnd_global.user_id,
                   SYSDATE,
                   fnd_global.user_id,
                   -1
                  );
            END LOOP;

            COMMIT;
    END process_item_locators;

    PROCEDURE xx_item_validation
    IS
        l_errmsg              VARCHAR2 (4000);
        l_item_error          VARCHAR2 (2) := 'N';
        l_item_err_details    VARCHAR2 (3000);
        l_inventory_item_id   NUMBER;

        CURSOR c_item_exist
        IS
            SELECT   DISTINCT partnumber, vendorpartnumber, organization_code
              FROM   xxwc_invitem_stg xxwcixs
             WHERE   nvl(cross_ref_status,'E')='E'
               AND vendorpartnumber is not null;
    BEGIN
        FOR r_item_exist IN c_item_exist
        LOOP
            l_errmsg := NULL;
            l_item_error := NULL;

            IF r_item_exist.partnumber IS NOT NULL
            THEN
                BEGIN
                    SELECT   MAX (inventory_item_id)
                      INTO   l_inventory_item_id
                      FROM   mtl_system_items_b
                     WHERE   UPPER (segment1) =
                                 UPPER (r_item_exist.partnumber)
                             AND organization_id =
                                    (SELECT   organization_id
                                       FROM   mtl_parameters
                                      WHERE   organization_code =
                                                  r_item_exist.organization_code)
                             AND enabled_flag = 'Y'
                             AND SYSDATE BETWEEN NVL (start_date_active,
                                                      SYSDATE)
                                             AND  NVL (end_date_active,
                                                       SYSDATE);
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        l_item_error := 'Y';
                        l_errmsg :=
                               l_errmsg
                            || '\'
                            || 'Inventory Item Id doesnt exist';
                        l_item_err_details :=
                            '\' || ' ' || 'Inventory Item Id cannot be Null';
                    --  print_debug (l_item_err_details);
                    WHEN OTHERS
                    THEN
                        l_item_error := 'Y';
                        l_errmsg :=
                            l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
                        l_item_err_details :=
                            '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
                END;
            ELSE
                l_item_error := 'Y';
                l_errmsg := l_errmsg || '\' || 'Inventory Item Id is NULL';
                l_item_err_details :=
                    '\' || ' ' || 'Inventory Item Id cannot be Null';
            END IF;

            IF NVL (l_item_error, 'N') = 'Y'
            THEN
                -- Update Item Staging Table with error flag and error message
                UPDATE   xxwc_invitem_stg
                   SET   cross_ref_status = 'E',
                         cross_ref_message = SUBSTR (l_errmsg, 1, 1000)
                 WHERE   partnumber = r_item_exist.partnumber
                         AND vendorpartnumber = r_item_exist.vendorpartnumber;
            ELSE
                UPDATE   xxwc_invitem_stg
                   SET   cross_ref_status = 'V'
                 WHERE   partnumber = r_item_exist.partnumber
                         AND vendorpartnumber = r_item_exist.vendorpartnumber;
            END IF;

            COMMIT;
        END LOOP;
    END xx_item_validation;

    PROCEDURE xx_itemxref (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
    IS
        vn_index                 NUMBER := 0;
        vn_count1                NUMBER := 0;
        vt_xref_table            mtl_cross_references_pub.xref_tbl_type;
        vn_cross_reference_id    NUMBER;

        vv_crossreference_type   VARCHAR2 (100) := 'VENDOR';
        vv_crossreference_type1  VARCHAR2 (100) := 'WC_XREF';
        vv_crossreference_type2  VARCHAR2 (100) := 'UPC';

        description              VARCHAR2 (100) := '';
        vn_inventory_itemid      NUMBER;
        lv_part                  NUMBER;

        vv_return_status         VARCHAR2 (10);
        v_cross_reference        VARCHAR2 (10);
        vn_msg_count             NUMBER := 0;
        vt_message_list          error_handler.error_tbl_type;
        vv_error_message         VARCHAR2 (1000);
        v_vendor_val             VARCHAR2 (100) := NULL;
        vn_organziation_id       NUMBER;
        l_user                   NUMBER := fnd_profile.VALUE ('USER_ID');
        l_creation_date          DATE := SYSDATE;

        CURSOR c_xref_stg
        IS
            SELECT   DISTINCT partnumber, vendorpartnumber
              FROM   xxwc_invitem_stg xxwcixs
             WHERE   cross_ref_status = 'V';

        CURSOR c_item_xref_stg
        IS
            SELECT   DISTINCT partnumber, xref_part
              FROM   xxwc_invitem_xref_stg xxwcixs
             WHERE   nvl(cross_ref_status,'x') <> 'S'
               AND   exists (select 'x' from mtl_system_items b
                              where xxwcixs.partnumber=b.segment1
                              --added by Shankar on 08-May-2012 to improve performance
                                and organization_id=fnd_profile.value('XXWC_ITEM_MASTER_ORG')
                              );


    BEGIN
        --call the item validation procedure
        xx_item_validation;
        vn_index := 1;
        -- call to create xref for the vendorpartnumber
        FOR rc_xref_stg IN c_xref_stg
        LOOP
            vt_xref_table.delete;
            --print_debug ('Loop Starts...');
            vv_error_message := NULL;


            BEGIN
                SELECT   MAX (msi.inventory_item_id)
                  INTO   vn_inventory_itemid
                  FROM   mtl_system_items_b msi
                 WHERE   msi.segment1 = rc_xref_stg.partnumber;
            END;

            vt_xref_table (vn_index).cross_reference_id :=
                mtl_cross_references_b_s.NEXTVAL;
            vt_xref_table (vn_index).transaction_type := 'CREATE';
            vt_xref_table (vn_index).cross_reference_type :=
                vv_crossreference_type;
            vt_xref_table (vn_index).cross_reference :=
                rc_xref_stg.vendorpartnumber;
            vt_xref_table (vn_index).inventory_item_id := vn_inventory_itemid;
            vt_xref_table (vn_index).description := '';
            vt_xref_table (vn_index).org_independent_flag := 'Y';
            vt_xref_table (vn_index).creation_date := l_creation_date;
            vt_xref_table (vn_index).last_update_date := l_creation_date;
            vt_xref_table (vn_index).last_updated_by := l_user;
            vt_xref_table (vn_index).created_by := l_user;

            mtl_cross_references_pub.process_xref (
                p_api_version     => 1.0,
                p_init_msg_list   => fnd_api.g_true,
                p_commit          => fnd_api.g_false,
                p_xref_tbl        => vt_xref_table,
                x_return_status   => vv_return_status,
                x_msg_count       => vn_msg_count,
                x_message_list    => vt_message_list);

            IF (vv_return_status = fnd_api.g_ret_sts_success)
            THEN
                UPDATE   xxwc_invitem_stg xxwcixs
                   SET   xxwcixs.cross_ref_status = 'S'
                 WHERE   partnumber = rc_xref_stg.partnumber
                         AND vendorpartnumber = rc_xref_stg.vendorpartnumber;

                print_debug('Cross Reference Loaded Successfully for Item:'
                            || rc_xref_stg.partnumber
                            || ' with vendor part number:'
                            || rc_xref_stg.vendorpartnumber);
                COMMIT;
            ELSE
                --Error_Handler.GET_MESSAGE_LIST(x_message_list=>x_message_list);
                FOR i IN 1 .. vt_message_list.COUNT
                LOOP
                    DBMS_OUTPUT.put_line (vt_message_list (i).MESSAGE_TEXT);
                    vv_error_message :=
                           vv_error_message
                        || ' - '
                        || vt_message_list (i).MESSAGE_TEXT;
                    print_debug(   'Load Failed for Item:'
                                || rc_xref_stg.partnumber
                                || ' with vendor part number:'
                                || rc_xref_stg.vendorpartnumber
                                || '. Error message:'
                                || vv_error_message);
                END LOOP;

                UPDATE   xxwc_invitem_stg xxwcixs
                   SET   xxwcixs.cross_ref_status = 'E',
                         xxwcixs.cross_ref_message = vv_error_message
                 WHERE   partnumber = rc_xref_stg.partnumber
                         AND vendorpartnumber = rc_xref_stg.vendorpartnumber;

                print_debug ('Error message:' || vv_error_message);
                print_debug(   'Load Failed for Item:'
                            || rc_xref_stg.partnumber
                            || ' with vendor part number:'
                            || rc_xref_stg.vendorpartnumber);
                COMMIT;
            END IF;
        END LOOP;

        vn_index := 1;
        -- call to create xref for the UPC and WC_XREF
        FOR rc_item_xref_stg IN c_item_xref_stg
        LOOP
            vt_xref_table.delete;
            vv_error_message := NULL;


            BEGIN
                SELECT   MAX (msi.inventory_item_id)
                  INTO   vn_inventory_itemid
                  FROM   mtl_system_items_b msi
                 WHERE   msi.segment1 = rc_item_xref_stg.partnumber;
            END;

            vt_xref_table (vn_index).cross_reference_id := mtl_cross_references_b_s.NEXTVAL;
            vt_xref_table (vn_index).transaction_type := 'CREATE';
            IF length(rc_item_xref_stg.xref_part) between 10 and 13 then
            BEGIN
             select to_number(rc_item_xref_stg.xref_part)
               into lv_part
               from dual;
               vt_xref_table (vn_index).cross_reference_type := vv_crossreference_type2;
            EXCEPTION
             when others then
             vt_xref_table (vn_index).cross_reference_type := vv_crossreference_type1;
            END;
            ELSE
            vt_xref_table (vn_index).cross_reference_type := vv_crossreference_type1;
            END IF;
            vt_xref_table (vn_index).cross_reference :=
                rc_item_xref_stg.xref_part;
            vt_xref_table (vn_index).inventory_item_id := vn_inventory_itemid;
            vt_xref_table (vn_index).description := '';
            vt_xref_table (vn_index).org_independent_flag := 'Y';
            vt_xref_table (vn_index).creation_date := l_creation_date;
            vt_xref_table (vn_index).last_update_date := l_creation_date;
            vt_xref_table (vn_index).last_updated_by := l_user;
            vt_xref_table (vn_index).created_by := l_user;

            mtl_cross_references_pub.process_xref (
                p_api_version     => 1.0,
                p_init_msg_list   => fnd_api.g_true,
                p_commit          => fnd_api.g_false,
                p_xref_tbl        => vt_xref_table,
                x_return_status   => vv_return_status,
                x_msg_count       => vn_msg_count,
                x_message_list    => vt_message_list);

            IF (vv_return_status = fnd_api.g_ret_sts_success)
            THEN
                UPDATE   xxwc_invitem_xref_stg xxwcixs
                   SET   xxwcixs.cross_ref_status = 'S'
                 WHERE   partnumber = rc_item_xref_stg.partnumber
                         AND xref_part = rc_item_xref_stg.xref_part;

                print_debug('Cross Reference Loaded Successfully for Item:'
                            || rc_item_xref_stg.partnumber
                            || ' with vendor part number:'
                            || rc_item_xref_stg.xref_part);
                COMMIT;
            ELSE
                --Error_Handler.GET_MESSAGE_LIST(x_message_list=>x_message_list);
                FOR i IN 1 .. vt_message_list.COUNT
                LOOP
                    DBMS_OUTPUT.put_line (vt_message_list (i).MESSAGE_TEXT);
                    vv_error_message :=
                           vv_error_message
                        || ' - '
                        || vt_message_list (i).MESSAGE_TEXT;
                    print_debug(   'Load Failed for Item:'
                                || rc_item_xref_stg.partnumber
                                || ' with vendor part number:'
                                || rc_item_xref_stg.xref_part
                                || '. Error message:'
                                || vv_error_message);
                END LOOP;

                UPDATE   xxwc_invitem_xref_stg xxwcixs
                   SET   xxwcixs.cross_ref_status = 'E',
                         xxwcixs.cross_ref_message = vv_error_message
                 WHERE   partnumber = rc_item_xref_stg.partnumber
                         AND xref_part = rc_item_xref_stg.xref_part;

                print_debug ('Error message:' || vv_error_message);
                print_debug(   'Load Failed for Item:'
                            || rc_item_xref_stg.partnumber
                            || ' with vendor part number:'
                            || rc_item_xref_stg.xref_part);
                COMMIT;
            END IF;
        END LOOP;
    END xx_itemxref;
END XXWC_ITEMCATEGCONV_PKG;