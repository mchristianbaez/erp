CREATE OR REPLACE PACKAGE BODY APPS."XXCUSCE_BAI_LOB_EXTRACT_PKG" AS

  program_error EXCEPTION;

  g_err_callfrom VARCHAR2(75) DEFAULT 'XXCUSCE_BAI_LOB_EXTRACT_PKG';
  g_distro_list  VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  /*****************************************************************************
  -- File Name: XXCUS_AP_BAI_EXTRACT_PKG
  --
  -- HISTORY
  -- ==========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ------------------------------------
  -- 1.0     06-JUL-2011   Luong Vu        Created this package.
  -- 1.1     25-SEP-2014   Kathy Poling    Created bai_extract_power_solutions
  --                                       ESMS# 264959 
  -- 1.2     26-May-2015   Maharajan Shunmugam  ESMS#279817 Transaction date format change 
  ******************************************************************************/

  -- -----------------------------------------------------------------------------
  -- |----------------------------< bai_extract_eclipse >--------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   This program outputs various suntrust accounts to a flat file.
  --   These files are then to be picked up and sent to eclipse.
  --
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     06-JUL-2011   Luong Vu        Retrofit from 11i.
  --                                       This procedure takes 2 specific bank accounts which
  --                                       are stored in THE PARAMETERS TABLE,
  --                                       and extracts them.  A cron job places them in a share
  --                                       drive that Eclipse can retrieve from.
  --
  --                                       The two ACH bank accounts were removed late in the design.
  --                                       The structure was left intact in case this decision
  --                                       is repealed.

  PROCEDURE bai_extract_eclipse(errbuf     OUT VARCHAR2
                               ,retcode    OUT NUMBER
                               ,p_bankdate IN DATE) IS
  
    --Initialize Local Variables
    -- Error DEBUG
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);
  
    l_err_callfrom  VARCHAR2(175) DEFAULT 'XXCUS_BAI_LOB_EXTRACT_PKG';
    l_err_callpoint VARCHAR2(175) DEFAULT 'START';
    l_distro_list   VARCHAR2(175) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --l_distro_list VARCHAR2(175) DEFAULT 'luong.vu@hdsupply.com';
  
    c_trx_code       NUMBER DEFAULT 479; --BAI code for checks. constant variable
    l_bank_eledisb   applsys.fnd_lookup_values.description%TYPE;
    l_bank_eleach    applsys.fnd_lookup_values.description%TYPE;
    l_eclipse_distro applsys.fnd_lookup_values.description%TYPE;
  
    l_sid VARCHAR2(15);
  
    --Flat file writing variables
    l_file_dir    VARCHAR2(100);
    l_file_handle utl_file.file_type;
  
    -- Main BAI Cursor
  
    CURSOR banklines_cur(p_banknum VARCHAR2) IS
    
      SELECT a.bank_trx_number   check_number
            ,a.trx_text
            ,a.trx_date          check_clearing_date
            ,a.amount
            ,b.bank_account_id
            ,a.statement_line_id
            ,c.bank_account_name
            ,c.bank_account_num
            ,a.trx_code
        FROM ce.ce_statement_lines   a
            ,ce.ce_statement_headers b
            ,ce.ce_bank_accounts     c
       WHERE a.statement_header_id = b.statement_header_id(+)
         AND b.bank_account_id = c.bank_account_id(+)
         AND a.trx_date = trunc(nvl(p_bankdate, SYSDATE - 1)) -- date of bank files.
         AND a.trx_code = c_trx_code
         AND c.bank_account_num = p_banknum;
  
    --START
  BEGIN
    -- Get flat file directory parameters
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    --l_file_dir := '/xx_iface/' || l_sid || '/outbound';
  
    l_file_dir := 'ORACLE_INT_ECLIPSE_BAI_EXTRACT';
  
    -- Gathering accounts from parameters table.
  
    l_err_callpoint := 'Gathering seeded parameters of accounts and distribution group.';
  
    l_bank_eledisb   := xxcus_misc_pkg.get_description(p_proc_name => l_err_callfrom
                                                      ,p_parm_name => 'SUNTRUST_ELEC_DISB');
    l_eclipse_distro := xxcus_misc_pkg.get_description(p_proc_name => l_err_callfrom
                                                      ,p_parm_name => 'EMAIL_DISTRIBUTION');
  
    -- Suntrust Electrical Disbursments
    l_err_callpoint := 'Starting Main outer loop for ' || l_bank_eledisb;
    l_file_handle   := utl_file.fopen(l_file_dir
                                     ,'ECE_' || l_bank_eledisb || '.CHKR'
                                     ,'w');
  
    FOR file_spool_rec IN banklines_cur(l_bank_eledisb)
    LOOP
      utl_file.put_line(l_file_handle
                       ,file_spool_rec.check_number || '|' ||
                        to_char(file_spool_rec.check_clearing_date
                               ,'MM/DD/YYYY') || '|' ||
                        file_spool_rec.amount);
    END LOOP;
    utl_file.fclose(l_file_handle);
  
    /*-- Suntrust Electrical ACH's
    l_err_callpoint := 'Starting Main outer loop for '||l_bank_eleAch;
        l_file_handle := utl_file.fopen(l_file_dir, 'ECE_'||l_bank_eleAch||'.CHKR', 'w');
    
     FOR file_spool_rec IN banklines_cur(l_bank_eleAch)
     LOOP
      utl_file.put_line(l_file_handle,  file_spool_rec.check_number || '|' ||to_char(file_spool_rec.check_clearing_date,'MM/DD/YYYY') || '|' || file_spool_rec.amount);
     END LOOP;
      utl_file.fclose(l_file_handle);
         */
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Interface package with PROGRAM ERROR'
                                          ,p_distribution_list => l_eclipse_distro
                                           -- for eclipse team
                                          ,p_module => 'AP');
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Interface package with PROGRAM ERROR'
                                          ,p_distribution_list => l_distro_list
                                           -- for HDS Development
                                          ,p_module => 'AP');
    
    WHEN OTHERS THEN
      ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Interface package with PROGRAM ERROR'
                                          ,p_distribution_list => l_eclipse_distro
                                           -- for eclipse team
                                          ,p_module => 'AP');
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Interface package with PROGRAM ERROR'
                                          ,p_distribution_list => l_distro_list
                                           -- for HDS Development
                                          ,p_module => 'AP');
    
  END bai_extract_eclipse;

  /* -- -----------------------------------------------------------------------------
  -- |----------------------------< bai_extract_cti >--------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   This program create a bai extract for CTI
  --   The delivery will be dependent on UC4 strategy
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------------
  -- 1.0     06-JUL-2011   Luong Vu        Created this procedure.
  --------------------------------------------------------------------------------
  
  PROCEDURE bai_extract_cti(errbuf     OUT VARCHAR2
                           ,retcode    OUT NUMBER
                           ,p_bankacct IN VARCHAR2) IS
  
    --Initialize Local Variables
    -- Error DEBUG
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);
  
    l_err_callfrom  VARCHAR2(175) DEFAULT 'XXCUSCE_BAI_LOB_EXTRACT_PKG';
    l_err_callpoint VARCHAR2(175) DEFAULT 'START';
    --  l_distro_list   VARCHAR2(175) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_distro_list VARCHAR2(175) DEFAULT 'luong.vu@hdsupply.com';
  
    l_rownum   NUMBER := 1; -- first record row number
    l_rownum2  NUMBER; -- group header row number
    l_num      NUMBER;
    l_bankacct VARCHAR2(50);
  
    l_sid VARCHAR2(15);
  
    --Flat file writing variables
    l_file_dir    VARCHAR2(100);
    l_file_handle utl_file.file_type;
  
  
  
    --START
  BEGIN
  
    l_bankacct := p_bankacct;
  
    -- Truncate data table before data load
    EXECUTE IMMEDIATE 'truncate table xxcus.xxcusce_stmt_int_tmp_tbl';
  
    -- Get row number of first record
    SELECT rec_no
      INTO l_rownum
      FROM ce.ce_stmt_int_tmp a
     WHERE a.rec_id_no = '03'
       AND a.column1 = l_bankacct
       AND a.column2 LIKE 'USD%';
  
    -- Get flat file directory parameters
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
    l_file_dir := '/xx_iface/' || l_sid || '/outbound';
  
    -----< Extract data from ce temp table to custom temp table >-----
  
    INSERT INTO xxcus.xxcusce_stmt_int_tmp_tbl -- Insert file header row
      (SELECT *
         FROM ce.ce_stmt_int_tmp t
        WHERE rec_no = 1);
    COMMIT;
  
    l_rownum2 := l_rownum - 1; -- row number of group header.
  
    INSERT INTO xxcus.xxcusce_stmt_int_tmp_tbl \*APPEND*\ --Insert group header row
      (SELECT *
         FROM ce.ce_stmt_int_tmp t
        WHERE rec_no = l_rownum2);
    COMMIT;
  
    INSERT INTO xxcus.xxcusce_stmt_int_tmp_tbl \*APPEND*\ -- Insert all other records
      (SELECT *
         FROM ce.ce_stmt_int_tmp t
        WHERE rec_no >= l_rownum
          AND rec_no < (SELECT MIN(rec_no) -- identify the next 03 record where we want to stop reading.
                          FROM ce.ce_stmt_int_tmp b
                         WHERE b.rec_no > l_rownum
                           AND b.rec_id_no = '03'));
    COMMIT;
  
    -----< CLEAN UP DATA >-----
    DELETE xxcus.xxcusce_stmt_int_tmp_tbl --CTI does not want this line included
     WHERE rec_id_no = '16'
       AND column1 = '856';
    COMMIT;
  
    UPDATE xxcus.xxcusce_stmt_int_tmp_tbl --Change routing number from 061000052 to 061112788
       SET column2 = 061112788
     WHERE rec_id_no = '02';
    COMMIT;
  
    -----< Creating CTI Extract >-----
    l_err_callpoint := 'Starting Main outer loop for account number ' ||
                       l_bankacct;
    l_file_handle   := utl_file.fopen(l_file_dir, 'BOFA_Stmt', 'w');
  
    FOR b_rec IN (SELECT *
                    FROM xxcus.xxcusce_stmt_int_tmp_tbl)
    LOOP
      FOR l_num IN 1 .. 1
      LOOP
        utl_file.put(l_file_handle,
                     b_rec.rec_no || ',' || b_rec.rec_id_no || ',' ||
                      b_rec.column1 || ',');
      
        IF nvl(b_rec.column2, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column2, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column2);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column3, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column3, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column3);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column4, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column4, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column4);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column5, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column5, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column5);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column6, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column6, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column6);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column7, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column7, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column7);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column8, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column8, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column8);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column9, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column9, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column9);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column10, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column10, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column10);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column11, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column11, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column11);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column12, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column12, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column12);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column13, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column13, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column13);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column14, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column14, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column14);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column15, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column15, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column15);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column16, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column16, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column16);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column17, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column17, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column17);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column18, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column18, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column18);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column18, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column18, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column18);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column19, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column19, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column19);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column20, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column20, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column20);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column21, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column21, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column21);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column22, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column22, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column22);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column23, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column23, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column23);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column24, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column24, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column24);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column25, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column25, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column25);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column26, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column26, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column26);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column27, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column27, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column27);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column28, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column28, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column28);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column29, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column29, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column29);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column30, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column30, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column30);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column31, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column31, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column31);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column32, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column32, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column32);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column33, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column33, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column33);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column34, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column34, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column34);
          EXIT;
        END IF;
      
        IF nvl(b_rec.column35, ' ') NOT LIKE '%/%' THEN
          utl_file.put(l_file_handle, nvl(b_rec.column35, '') || ',');
        ELSE
          utl_file.put(l_file_handle, b_rec.column35);
          EXIT;
        END IF;
      
      END LOOP;
    
      utl_file.new_line(l_file_handle, 1);
    
    END LOOP;
  
    utl_file.fclose(l_file_handle);
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Interface package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list
                                           -- for HDS Development
                                          , p_module => 'AP');
    
  
    WHEN OTHERS THEN
      ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Interface package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list
                                           -- for HDS Development
                                          , p_module => 'AP');
    
  END bai_extract_cti;*/

  /*********************************************************************************
  -- Procedure:  bai_extract_power_solutions
  -- *******************************************************************************
  --
  -- PURPOSE: Extract a daily BAI file for Power Solutions to reconcile the checks in 
  --          SXE.  UC4 will pickup the file and deliver to \\utilsxeprd.hdsupply.net\utilreports
  -- HISTORY
  -- ==============================================================================
  -- VERSION DATE          AUTHOR(S)            DESCRIPTION
  -- ------- -----------   --------------- ----------------------------------------
  -- 1.0     20-May-2014   Kathy Poling         Created this package.  ESMS# 264959 
  -- 1.2     26-May-2015   Maharajan Shunmugam  ESMS#279817 Transaction date format change 
  **********************************************************************************/

  PROCEDURE bai_extract_ps(p_errbuf         OUT VARCHAR2
                          ,p_retcode        OUT NUMBER
                          ,p_bankdate       IN VARCHAR2
                          ,p_directory_name IN VARCHAR2
                          ,p_user_name      IN VARCHAR2
                          ,p_resp_name      IN VARCHAR2) IS
  
    l_err_msg   CLOB;
    l_sec       VARCHAR2(110) DEFAULT 'START';
    l_user_id   fnd_user.user_id%TYPE;
    l_procedure VARCHAR2(50) := 'BAI_EXTRACT_PS';
    l_delimiter CONSTANT CHAR(1) := chr(9);
    l_filename       VARCHAR2(100);
    l_file_dir       VARCHAR2(100) := 'XXCUS_CE_BAI_PS_OB_DIR';
    l_file_handle    utl_file.file_type;
    l_file_name_temp VARCHAR2(100);
    l_column_header  VARCHAR2(250);
  
    --
    CURSOR cur_bai_details IS
      SELECT ba.bank_account_num || l_delimiter || tr.trx_code ||
             l_delimiter || li.trx_type || l_delimiter || tr.description ||
             l_delimiter || li.trx_text || l_delimiter ||
             li.bank_trx_number || l_delimiter || NULL || l_delimiter || NULL ||
             l_delimiter || NULL || l_delimiter || NULL || l_delimiter ||
             --li.trx_date || l_delimiter ||                                               --Commented and added below for Ver#1.2
             TO_CHAR(li.trx_date,'MM/DD/YYYY') || l_delimiter ||
             to_char(li.amount, '999,999,999,999.99') "BAI_DETAILS"
        FROM ce.ce_statement_lines li
            ,ce.ce_statement_headers hd
            ,ce.ce_bank_accounts ba
            ,(SELECT trx_code, description, bank_account_id
                FROM ce.ce_transaction_codes tr
               GROUP BY trx_code, description, bank_account_id) tr
       WHERE li.statement_header_id = hd.statement_header_id
         AND hd.bank_account_id = ba.bank_account_id
         AND li.trx_code = tr.trx_code
         AND ba.bank_account_id = tr.bank_account_id
         AND li.cashflow_id IS NULL
         AND ba.bank_account_num IN
             (SELECT meaning
                FROM fnd_lookup_values
               WHERE 1 = 1
                 AND lookup_type = 'XXCUS_POWER_SOLUTIONS_EXTRACT'
                 AND enabled_flag = 'Y'
                 AND SYSDATE BETWEEN start_date_active AND
                     nvl(end_date_active, SYSDATE + 1))
         AND li.trx_date >= p_bankdate
      UNION ALL
      SELECT ba.bank_account_num || l_delimiter || tr.trx_code ||
             l_delimiter || li.trx_type || l_delimiter || tr.description ||
             l_delimiter || li.trx_text || l_delimiter ||
             li.bank_trx_number || l_delimiter || g.segment1 || l_delimiter ||
             g.segment2 || l_delimiter || g.segment4 || l_delimiter ||
            -- g.segment5 || l_delimiter || li.trx_date || l_delimiter ||                 --Commented and added below for Ver#1.2
               g.segment5 || l_delimiter || TO_CHAR(li.trx_date,'MM/DD/YYYY') || l_delimiter ||
             to_char(li.amount, '999,999,999,999.99') "BAI_DETAILS"
        FROM ce.ce_statement_lines li
            ,ce.ce_statement_headers hd
            ,ce.ce_bank_accounts ba
            ,ce.ce_cashflows ca
            ,gl.gl_code_combinations g
            ,(SELECT trx_code, description, bank_account_id
                FROM ce.ce_transaction_codes tr
               GROUP BY trx_code, description, bank_account_id) tr
       WHERE li.statement_header_id = hd.statement_header_id
         AND hd.bank_account_id = ba.bank_account_id
         AND li.trx_code = tr.trx_code
         AND ba.bank_account_id = tr.bank_account_id
         AND ca.cashflow_id = li.cashflow_id
         AND ca.offset_ccid = g.code_combination_id
         AND ba.bank_account_num IN
             (SELECT meaning
                FROM fnd_lookup_values
               WHERE 1 = 1
                 AND lookup_type = 'XXCUS_POWER_SOLUTIONS_EXTRACT'
                 AND enabled_flag = 'Y'
                 AND SYSDATE BETWEEN start_date_active AND
                     nvl(end_date_active, SYSDATE + 1))
         AND li.trx_date >= p_bankdate;
  
  BEGIN
  
    SELECT 'Bank Account Num' || chr(9) || 'Trx Code' || chr(9) ||
           'Trx Type' || chr(9) || 'Description' || chr(9) || 'Trx Text' ||
           chr(9) || 'Bank Trx Number' || chr(9) || 'Segment1' || chr(9) ||
           'Segment2' || chr(9) || 'Segment4' || chr(9) || 'Segment5' ||
           chr(9) || 'Trx Date' || chr(9) || 'Amount'
      INTO l_column_header
      FROM dual;
  
    fnd_file.put_line(fnd_file.log
                     ,'Begining BAI Extract for Power Solution File Generation ');
    fnd_file.put_line(fnd_file.log
                     ,'========================================================');
    fnd_file.put_line(fnd_file.log, '  ');
    --Initialize the Out Parameter
    p_errbuf  := '';
    p_retcode := '0';
  
    --Derive the file name 
    SELECT to_char(SYSDATE, 'MMDDYYYY') || '.txt'
      INTO l_filename
      FROM dual;
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || l_filename;
  
    fnd_file.put_line(fnd_file.log, 'File Name:  ' || l_filename);
  
    --Open the file handler
    l_file_handle := utl_file.fopen(location  => l_file_dir
                                   ,filename  => l_file_name_temp
                                   ,open_mode => 'w');
    fnd_file.put_line(fnd_file.log, ' Writing to the file ... ');
  
    --write column header into file
    utl_file.put_line(l_file_handle, l_column_header);
  
    l_sec := 'Writing file';
    -- Writting file 
    FOR rec_bai_details IN cur_bai_details
    LOOP
      utl_file.put_line(l_file_handle, rec_bai_details.bai_details);
    END LOOP;
  
    -- Close the file
    l_sec := 'Closing the File; ';
    utl_file.fclose(l_file_handle);
  
    --Rename file for pickup  
    utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir, l_filename);
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'End of Procedure : BAI_EXTRACT_PS');
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log
                     ,'**********************************************************************************');
  
  EXCEPTION
    WHEN utl_file.invalid_path THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'File Path is invalid.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.invalid_mode THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'The open_mode parameter in FOPEN is invalid.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.invalid_filehandle THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'File handle is invalid..';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.invalid_operation THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'File could not be opened or operated on as requested';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.write_error THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'Operating system error occurred during the write operation';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.internal_error THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'Unspecified PL/SQL error.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
    
    WHEN utl_file.file_open THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'The requested operation failed because the file is open.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.invalid_filename THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'The filename parameter is invalid.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.access_denied THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'Permission to access to the file location is denied.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END bai_extract_ps;

  /*********************************************************************************
  -- Procedure:  UC4_SUBMIT
  -- *******************************************************************************
  --
  -- PURPOSE: Use to submit the concurrent program from UC4
  --          
  -- HISTORY
  -- ==============================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ----------------------------------------
  -- 1.0     20-May-2014   Kathy Poling    Created this package.  ESMS# 264959 
  **********************************************************************************/
  PROCEDURE uc4_submit(p_errbuf         OUT VARCHAR2
                      ,p_retcode        OUT NUMBER
                      ,p_bankdate       IN VARCHAR2
                      ,p_directory_name IN VARCHAR2
                      ,p_user_name      IN VARCHAR2
                      ,p_resp_name      IN VARCHAR2
                      ,p_program        IN VARCHAR2
                      ,p_application    IN VARCHAR2) IS
    --
    l_package     VARCHAR2(50) := 'XXCUSCE_BAI_LOB_EXTRACT_PKG';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    l_req_id              NUMBER NULL;
    l_phase               VARCHAR2(50);
    l_status              VARCHAR2(50);
    l_dev_status          VARCHAR2(50);
    l_dev_phase           VARCHAR2(50);
    l_error_message       VARCHAR2(3000);
    l_supplier_id         NUMBER;
    l_rec_cnt             NUMBER := 0;
    l_interval            NUMBER := 30; -- In seconds
    l_max_time            NUMBER := 15000; -- In seconds
    l_message             VARCHAR2(2000);
    l_errormessage        CLOB;
    l_can_submit_request  BOOLEAN := TRUE;
    l_globalset           VARCHAR2(100);
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;
    l_sec                 VARCHAR2(2000);
    l_statement           CLOB;
    l_user_id             fnd_user.user_id%TYPE;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_procedure           VARCHAR2(50) := 'UC4_SUBMIT';
  
  BEGIN
    --For UC4
    p_errbuf  := 'Success';
    p_retcode := 0;
  
    -- Deriving User Id  
    l_sec := 'Derive UserId';
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
    END;
  
    -- Deriving Responsibility Id and Responsibility Application Id
    l_sec := 'Derive ResponsibilityId';
  
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_resp_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_resp_name ||
                     ' not defined in Oracle';
        RAISE program_error;
    END;
  
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    l_sec := 'Apps Initialize';
  
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);
  
    l_sec := 'Call Concurrent Program';
  
    l_req_id := fnd_request.submit_request(application => p_application
                                          ,program     => p_program
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => p_bankdate
                                          ,argument2   => p_directory_name
                                          ,argument3   => p_user_name
                                          ,argument4   => p_resp_name);
    COMMIT;
  
    IF (l_req_id != 0)
    THEN
      l_sec := 'Wait for Concurrent Program ' || p_program;
    
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,l_interval
                                        ,l_max_time
                                        ,l_phase
                                        ,l_status
                                        ,l_dev_phase
                                        ,l_dev_status
                                        ,l_message)
      THEN
        l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                     l_dev_phase || ' DStatus ' || l_dev_status || chr(10) ||
                     ' MSG - ' || l_message;
        -- Error Returned
        IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
        THEN
          l_err_msg := 'An error occured in the running of ' || p_program ||
                       l_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE program_error;
        END IF;
      END IF;
    
    ELSE
      l_err_msg := 'An error occured when trying to submit  ' || p_program;
      RAISE program_error;
    END IF;
  
    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_resp_name);
    dbms_output.put_line('User Name:  ' || p_user_name);
    dbms_output.put_line('Program Short Name:  ' || p_program);
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || p_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'CE');
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || p_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'CE');
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END uc4_submit;

END xxcusce_bai_lob_extract_pkg;
/
