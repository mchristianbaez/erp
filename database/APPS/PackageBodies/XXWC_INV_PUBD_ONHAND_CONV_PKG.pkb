CREATE OR REPLACE PACKAGE BODY APPS.xxwc_inv_pubd_onhand_conv_pkg
IS
/**********************************************************************************************************
  $Header xxwc_inv_pubd_onhand_conv_pkb $
  Module Name: xxwc_inv_pubd_onhand_conv_pkg.pkb

  PURPOSE:   TMS#20160107-00119 -convert PUBD On-hand Qty using Interface
             

  REVISIONS:
  Ver        Date        Author                  Description
  ---------  ----------  ---------------         --------------------------------------------------------
  1.0        03/30/2016  Rakesh Patel            Initial Version
  1.1        11/11/2016  P.Vamshidhar            TMS#20160427-00134 - PUBD Item Conversion-for Field use.
  1.2        05-APR-2018 Ashwin Sridhar          Added for TMS#20180404-00049--AH Harries Inv PUBD Conversion
***********************************************************************************************************/
    PROCEDURE print_debug (p_print_str IN VARCHAR2)
    IS
    BEGIN
        --  IF g_debug = 'Y' THEN
        fnd_file.put_line (fnd_file.LOG, p_print_str);
        -- END IF;
        DBMS_OUTPUT.put_line (p_print_str);
    END print_debug;

    PROCEDURE validations
    IS
        CURSOR c_onhadbal_stg
        IS
            SELECT   ROWID, xobc.*
              FROM   xxwc.xxwc_pubd_onhand_cnv xobc
             WHERE   NVL (status, 'R') in ('R','E');                     --onhand qty

        l_errmsg                   VARCHAR2 (4000);
        l_item_error               VARCHAR2 (2) := 'N';
        l_item_err_details         VARCHAR2 (3000);
        l_organization_id          NUMBER;
        l_inventory_item_id        NUMBER;
        l_uom                      NUMBER;
        l_uom_code                 VARCHAR2 (5);
        l_processing_stg           NUMBER := 0;
        l_error_stg                NUMBER := 0;
        l_validated_stg            NUMBER := 0;
    BEGIN
        SELECT   COUNT (1)
          INTO   l_processing_stg
          FROM   xxwc.xxwc_pubd_onhand_cnv xobc
         WHERE   NVL (status, 'R') in ('R','E');


        UPDATE xxwc.xxwc_pubd_onhand_cnv a
           SET transaction_uom =
                  (SELECT b.primary_uom_code
                     FROM mtl_system_items b, mtl_parameters c
                    WHERE     a.item_segment = b.segment1
                          AND b.organization_id = c.organization_id
                          AND a.organization_code = c.organization_code)
         WHERE transaction_uom IS NULL;

        UPDATE   xxwc.xxwc_pubd_onhand_cnv
           SET   transaction_uom =
                     DECODE (transaction_uom,
                             'PAR', 'PR',
                             'ROL', 'RL',
                             'PK', 'PKG',
                             'LB', 'LBS',
                             'PAL', 'PL',
                             'CAS', 'CS',
                             transaction_uom);

        COMMIT;

        FOR r_onhadbal_stg IN c_onhadbal_stg
        LOOP
            l_errmsg := NULL;
            l_item_error := NULL;
            l_organization_id := NULL;
            l_inventory_item_id := NULL;

            --Organization Validation
            IF r_onhadbal_stg.organization_code IS NOT NULL
            THEN
                BEGIN
                    SELECT   organization_id
                      INTO   l_organization_id
                      FROM   mtl_parameters
                     WHERE   organization_code =
                                 r_onhadbal_stg.organization_code;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        l_item_error := 'Y';
                        l_errmsg :=
                            l_errmsg || '\' || 'Invalid Organization Code';
                        l_item_err_details :=
                            '\' || ' ' || ' Organization Code is not valid';
                        print_debug (l_item_err_details);
                    WHEN OTHERS
                    THEN
                        l_item_error := 'Y';
                        l_errmsg :=
                            l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
                        l_item_err_details :=
                            '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
                        print_debug (l_item_err_details);
                END;
            ELSE
                l_item_error := 'Y';
                l_errmsg := l_errmsg || '\' || 'Organization Code is NULL';
                l_item_err_details :=
                    '\' || ' ' || 'Organization Code cannot be Null';
                print_debug (l_item_err_details);
            END IF;

            --Item Validation
            IF r_onhadbal_stg.item_segment IS NOT NULL
            THEN
                BEGIN
                    SELECT   inventory_item_id, primary_uom_code    --count(1)
                      INTO   l_inventory_item_id, l_uom_code
                      FROM   mtl_system_items_b
                     WHERE   UPPER (segment1) =
                                 UPPER (r_onhadbal_stg.item_segment)
                             AND organization_id = l_organization_id
                             AND enabled_flag = 'Y'
                             AND SYSDATE BETWEEN NVL (start_date_active,
                                                      SYSDATE)
                                             AND  NVL (end_date_active,
                                                       SYSDATE);
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        l_item_error := 'Y';
                        l_errmsg :=
                               l_errmsg
                            || '\'
                            || 'Inventory Item Id doesnt exist';
                        l_item_err_details :=
                            '\' || ' ' || 'Inventory Item Id cannot be Null';
                        print_debug (l_item_err_details);
                    WHEN OTHERS
                    THEN
                        l_item_error := 'Y';
                        l_errmsg :=
                            l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
                        l_item_err_details :=
                            '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
                        print_debug (l_item_err_details);
                END;
            ELSE
                l_item_error := 'Y';
                l_errmsg := l_errmsg || '\' || 'Inventory Item Id is NULL';
                l_item_err_details :=
                    '\' || ' ' || 'Inventory Item Id cannot be Null';
                print_debug (l_item_err_details);
            END IF;

            --UOM Validation
            IF r_onhadbal_stg.transaction_uom IS NOT NULL
            THEN
                BEGIN
                    SELECT   COUNT (1)
                      INTO   l_uom
                      FROM   mtl_units_of_measure
                     WHERE   UPPER (uom_code) =
                                 UPPER (r_onhadbal_stg.transaction_uom);
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        l_item_error := 'Y';
                        l_errmsg := l_errmsg || '\' || 'Item UOM doesnt exist';
                        l_item_err_details :=
                            '\' || ' ' || 'Item  UOM doesnt exist';
                        print_debug (l_item_err_details);
                    WHEN OTHERS
                    THEN
                        l_item_error := 'Y';
                        l_errmsg :=
                            l_errmsg || '\' || SQLCODE || '  ' || SQLERRM;
                        l_item_err_details :=
                            '\' || ' ' || '-' || SQLCODE || '  ' || SQLERRM;
                        print_debug (l_item_err_details);
                END;
            ELSE
                l_item_error := 'Y';
                l_errmsg := l_errmsg || '\' || 'Item UOM is NULL';
                l_item_err_details := '\' || ' ' || 'Item UOM is NULL';
                print_debug (l_item_err_details);
            END IF;

            --Transaction Qty Validation
            IF r_onhadbal_stg.EXPIRED_PUBD  >= 0
            THEN
                NULL;
            ELSE
                l_item_error := 'Y';
                l_errmsg :=
                    l_errmsg || '\' || 'TRANSACTION QUANTITY Does Not Exist';
                l_item_err_details :=
                    '\' || ' ' || 'TRANSACTION QUANTITY Does Not Exist';
                print_debug (l_item_err_details);
            END IF;
            
            --print_debug ('Updating Staging Tables');

            --Processing Error Information
            IF NVL (l_item_error, 'N') = 'Y'
            THEN
                -- Update Item Staging Table with error flag and error message
                UPDATE   xxwc.xxwc_pubd_onhand_cnv
                   SET   status = 'E',
                         error_message = SUBSTR (l_errmsg, 1, 2000)
                 WHERE   ROWID = r_onhadbal_stg.ROWID;
            ELSE
                UPDATE   xxwc.xxwc_pubd_onhand_cnv
                   SET   status = 'V'
                 WHERE   ROWID = r_onhadbal_stg.ROWID;
            END IF;

            COMMIT;
        END LOOP;

        print_debug ('Number of Records Processed: ' || l_processing_stg);

        SELECT   COUNT (1)
          INTO   l_error_stg
          FROM   xxwc.xxwc_pubd_onhand_cnv xobc
         WHERE   NVL (status, 'E') = 'E';

        print_debug ('Number of Records Errored out : ' || l_error_stg);

        SELECT   COUNT (1)
          INTO   l_validated_stg
          FROM   xxwc.xxwc_pubd_onhand_cnv xobc
         WHERE   status = 'V';

        print_debug ('Number of Records Validated ' || l_validated_stg);
    --display the processed information
    END validations;

    PROCEDURE process_statements
    IS
        CURSOR c_validrec_stg
        IS
            SELECT   ROWID,
                     organization_id,
                     xxonhand.EXPIRED_PUBD  transaction_quantity,
                     transaction_uom,
                     xxonhand.status status,
                     organization_code,
                     item_segment,
                     error_message
              FROM   xxwc.xxwc_pubd_onhand_cnv xxonhand
             WHERE   xxonhand.status = 'V'
            ORDER by organization_code, item_segment;

        l_transaction_type_id      NUMBER;
        l_organization_id          NUMBER;
        l_inventory_item_id        NUMBER;
        l_lotcontrol               NUMBER;
        l_srl_num_control          NUMBER;
        l_trans_action_id          NUMBER;
        l_trans_source_type_id     NUMBER;
        l_flow_schedule            VARCHAR2 (1) := 'Y';
        l_scheduled_flag           NUMBER := 2;
        l_transaction_mode         NUMBER := 3;
        l_process_flag             NUMBER := 1;
        l_lock_mode                NUMBER := 2;
        l_accts_pay_code_comb_id   VARCHAR2 (30);
        l_interfaceprocess_count   NUMBER := 0;
        l_interfaceerror_count     NUMBER := 0;
        l_interfacerec_count       NUMBER := 0;
        l_errormsg                 VARCHAR2 (1000);
        l_organization_code        VARCHAR2 (10);
        l_transaction_source_id    NUMBER;
        l_count                    NUMBER;
        l_total_qty                NUMBER;
        l_bal_qty                  NUMBER;
        l_trx_qty                  NUMBER;
        l_mat_account              NUMBER;
        l_cost_group_id            NUMBER;
    BEGIN
        SELECT   COUNT (1)
          INTO   l_interfaceprocess_count
          FROM   xxwc.xxwc_pubd_onhand_cnv xxobc
         WHERE   xxobc.status = 'V';

        print_debug ('Before Insertion Process.....!');

        FOR r_validrec_stg IN c_validrec_stg
        LOOP
            l_errormsg := NULL;
            l_mat_account      := NULL;
            l_cost_group_id    := NULL;
            l_transaction_type_id := 41;

            print_debug ('Before Insertion Loop....!');
            print_debug ('Transaction Type.....!');
            print_debug ('Org Code....!');

            --getting organization id
            SELECT   organization_id
              INTO   l_organization_id
              FROM   org_organization_definitions
             WHERE   organization_code = r_validrec_stg.organization_code
                     AND NVL (disable_date, TRUNC(SYSDATE)) >=
                            TRUNC (SYSDATE);

            print_debug ('Transaction Source....!');

            --getting transaction source and transaction id
            SELECT   transaction_source_type_id, transaction_action_id
              INTO   l_trans_source_type_id, l_trans_action_id
              FROM   mtl_transaction_types
             WHERE   transaction_type_id = l_transaction_type_id;

            print_debug ('Item Id....!');

            --getting inventory item id
            SELECT   inventory_item_id
              INTO   l_inventory_item_id
              FROM   mtl_system_items_b
             WHERE       segment1 = r_validrec_stg.item_segment
                     AND organization_id = l_organization_id
                     AND enabled_flag = 'Y'
                     AND SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                                     AND  NVL (end_date_active, SYSDATE);

            print_debug ('GL Code Comination ....!');

            SELECT   disposition_id, distribution_account
              INTO   l_transaction_source_id, l_accts_pay_code_comb_id
              FROM   mtl_generic_dispositions
             WHERE   organization_id = l_organization_id 
                     AND segment1 = 'TYPE 26';--'CONVERSION';

            print_debug ('Inserting into mtl_transactions_interface table');

            INSERT INTO mtl_transactions_interface (   source_code,
                                                       process_flag,
                                                       transaction_mode,
                                                       transaction_uom,
                                                       transaction_type_id,
                                                       flow_schedule,
                                                       scheduled_flag,
                                                       created_by,
                                                       last_updated_by,
                                                       organization_id,
                                                       subinventory_code,
                                                       transaction_date,
                                                       transaction_cost,
                                                       transaction_reference,
                                                       inventory_item_id,
                                                       source_header_id,
                                                       source_line_id,
                                                       transaction_quantity,
                                                       transaction_source_id,
                                                       distribution_account_id,
                                                       locator_name,
                                                       lock_flag,
                                                       transaction_action_id,
                                                       transaction_source_type_id,
                                                       creation_date,
                                                       last_update_date,
                                                       transaction_interface_id,
                                                       transaction_header_id
                                                                  )
                 VALUES   ('CONVERSION',--r_validrec_stg.transaction_source,
                           l_process_flag,
                           l_transaction_mode,
                           r_validrec_stg.transaction_uom,
                           l_transaction_type_id,
                           l_flow_schedule,
                           l_scheduled_flag,
                           fnd_profile.VALUE ('USER_ID'),                    --,0
                           fnd_profile.VALUE ('USER_ID'),                   --,-1
                           l_organization_id,
                           'PUBD',--NVL (r_validrec_stg.subinventory_code, 'General'),
                           sysdate, --add_months(last_day(SYSDATE), -1)-7,
                           /**decode(r_validrec_stg.how_priced,
                                  'E',r_validrec_stg.transaction_cost,
                                  'C',r_validrec_stg.transaction_cost/100,
                                  'M',r_validrec_stg.transaction_cost/1000,
                                   r_validrec_stg.transaction_cost), **/ -- transaction_cost
                           NULL, --transaction_cost       
                           NULL,-- r_validrec_stg.transaction_reference,
                           l_inventory_item_id,
                           99,                               --p_source_header_id
                           98,                                 --p_source_line_id
                           NVL (r_validrec_stg.transaction_quantity, 0),
                           l_transaction_source_id,
                           l_accts_pay_code_comb_id,
                           NULL,--NVL (r_validrec_stg.locator_name, ''),
                           l_lock_mode,                                     --add
                           l_trans_action_id,
                           l_trans_source_type_id,
                           SYSDATE,
                           SYSDATE,
                           mtl_material_transactions_s.NEXTVAL, --l_txn_int_id -- transaction_interface_id
                           TO_CHAR (r_validrec_stg.organization_id) || TO_CHAR (SYSDATE, 'ddmmyyyy')
                           );

            -- Update Item Staging Table with process status
            IF l_errormsg IS NOT NULL
            THEN
                --ROLLBACK;
                --print_debug ('Updating the Staging Table with status E');
                UPDATE   xxwc.xxwc_pubd_onhand_cnv
                   SET   status = 'E', error_message = l_errormsg
                 WHERE   ROWID = r_validrec_stg.ROWID;

                --COMMIT;
            ELSE
                -- print_debug ('Updating the Staging Table with status P');
                UPDATE   xxwc.xxwc_pubd_onhand_cnv
                   SET   status = 'P'
                 WHERE   ROWID = r_validrec_stg.ROWID;

                --COMMIT;
            END IF;

            COMMIT;
        --- FND_FILE.PUT_LINE(FND_FILE.OUTPUT, 'One record inserted into interface table -> mtl_transactions_interface');
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            print_debug (SQLCODE || '  ' || SQLERRM);
            print_debug ('Exception Part....!');
            --processing information
            print_debug (
                'Number Of Records being Processed: ' || l_interfaceprocess_count);

            SELECT   COUNT (1)
              INTO   l_interfaceerror_count
              FROM   xxwc.xxwc_pubd_onhand_cnv
             WHERE   status = 'E';

            print_debug('Number Of Records with Error in Interface Table: '
                        || l_interfaceerror_count);

            SELECT   COUNT (1)
              INTO   l_interfacerec_count
              FROM   xxwc.xxwc_pubd_onhand_cnv
             WHERE   status = 'P';

            print_debug('Number Of Valid Records in Interface Table: '
                        || l_interfacerec_count);
    END process_statements;

    PROCEDURE itemonhand_conv_proc (errbuf    OUT VARCHAR2,
                                    retcode   OUT VARCHAR2,
                                    p_validate_only IN VARCHAR2)
    IS
        l_count          NUMBER;
        l_count_valid    NUMBER;
        v_errorcode      NUMBER;
        v_errormessage   VARCHAR2 (240);

    BEGIN
        print_debug ('Performing .........!');

        SELECT   COUNT (1)
          INTO   l_count
          FROM   xxwc.xxwc_pubd_onhand_cnv
         WHERE   NVL (status, 'R') in ('R','E');

        IF l_count > 0
        THEN
            print_debug ('Performing Validations');
            xxwc_inv_pubd_onhand_conv_pkg.validations;
        ELSE
            print_debug ('No Records To Process');
        END IF;

      IF nvl(p_validate_only,'Y') = 'N' then
        SELECT   COUNT (1)
          INTO   l_count_valid
          FROM   xxwc.xxwc_pubd_onhand_cnv
         WHERE   status = 'V';

        IF l_count_valid > 0
        THEN
            print_debug ('Performing Insertion');
            xxwc_inv_pubd_onhand_conv_pkg.process_statements;
        ELSE
            print_debug ('No Records To Insert');
        END IF;
     END IF;
    END itemonhand_conv_proc;

/**********************************************************************************************************
  Procedure: update_born_on_date

  PURPOSE:   To update item DOB.

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         --------------------------------------------------------
  1.0        03/30/2016  Rakesh Patel            Initial Version
  1.1        11/11/2016  P.Vamshidhar            TMS#20160427-00134 - PUBD Item Conversion-for Field use.
***********************************************************************************************************/


PROCEDURE update_born_on_date (errbuf          OUT VARCHAR2
                             , retcode         OUT VARCHAR2
                             , i_trx_date   IN     VARCHAR2
                             , i_organization_id IN NUMBER)
IS
   l_trx_date date;
   ln_count NUMBER:=0; --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
   CURSOR c1 IS
SELECT mmt.organization_id
           , mmt.transaction_id
           ,mmt.transaction_date
           ,msib.SHELF_LIFE_DAYS
           --, TO_DATE (NVL (attribute15, TO_CHAR (transaction_date - 366, 'DD-MON-RR')), 'DD-MON-RR') trx_date
           ,(transaction_date-msib.shelf_life_days) trx_date
       FROM mtl_material_transactions mmt,
            mtl_system_items_b msib
      WHERE --TRUNC (mmt.creation_date) = TRUNC (l_trx_date) Commented in rev 1.1
            TRUNC (mmt.creation_date) = TRUNC (SYSDATE)  -- Added in Rev 1.1
        AND mmt.inventory_item_id = msib.inventory_item_id
        AND mmt.organization_id = msib.organization_id
        AND mmt.organization_id=nvl(i_organization_id, mmt.organization_id)
        AND subinventory_code = 'PUBD'
        AND source_code = 'CONVERSION'
        AND EXISTS
                 (SELECT 'x'
                    FROM mtl_onhand_quantities_detail b
                   WHERE b.create_transaction_id = mmt.transaction_id);                 
                 
BEGIN

fnd_file.put_line(fnd_file.output,'This is the output file'); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'-------------------------'); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Execution Start Time '||SYSTIMESTAMP); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049

fnd_file.put_line(fnd_file.output,'Starting the loop...'); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Parameter Transaction Date...'||i_trx_date); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Parameter Inventory Organization ID...'||i_organization_id); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Starting the loop...'); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049

   l_trx_date := FND_DATE.canonical_to_date(i_trx_date);
   print_debug ('Start updating born on date '||i_trx_date);
   
   IF l_trx_date > Sysdate THEN
      print_debug ('Future date is not allowed');
      retcode := 2;
      errbuf := 'Future date is not allowed';
   ELSE
     FOR c1_rec IN c1
     LOOP
	 
	 ln_count:=ln_count+1;

        UPDATE mtl_onhand_quantities_detail
           --SET orig_date_received = c1_rec.ld_orig_date -- Modified in Rev 1.1           
           SET orig_date_received = TRUNC(SYSDATE) - NVL(c1_rec.SHELF_LIFE_DAYS,0) -- Modified in Rev 1.1
         WHERE create_transaction_id = c1_rec.transaction_id;
       
         print_debug ('No of records updated : '||sql%rowcount);
     END LOOP;
	 
fnd_file.put_line(fnd_file.output,'Loop completed...'); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Count of records updated in table mtl_onhand_quantities_detail is...'||ln_count); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
fnd_file.put_line(fnd_file.output,'Execution End Time '||SYSTIMESTAMP); --Added by Ashwin.S on 05-Apr-2018 for TMS#20180404-00049
  
      print_debug ('updating born on date complete');
     
      print_debug ('Delete the stage table records');
      delete from xxwc.xxwc_pubd_onhand_cnv;
      print_debug ('No of records deleted : '||sql%rowcount);
   END IF;
END update_born_on_date;
END xxwc_inv_pubd_onhand_conv_pkg;
/