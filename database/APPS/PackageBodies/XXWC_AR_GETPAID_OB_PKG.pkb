CREATE OR REPLACE PACKAGE BODY XXWC_AR_GETPAID_OB_PKG AS

  /********************************************************************************
  FILE NAME: XXWC_AR_GETPAID_OB_PKG.pkg

  PROGRAM TYPE: PL/SQL Package Body

  PURPOSE: Package to create Log Files for GetPaid interfaces

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
  1.1     12/10/2012    Gopi Damuluri    Added new func - GET_BILL_TO_ADDR
  1.2     01/25/2013    Gopi Damuluri    get_bill_to_addr function is modified to
                                         consider Primary Bill-To address.
  ********************************************************************************/

  --Email Defaults
  pl_dflt_email fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';

  /********************************************************************************
  Procedure: CREATE_LOG_FILE

  PURPOSE: API to create Log Files for GetPaid interfaces

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
  ********************************************************************************/
PROCEDURE CREATE_LOG_FILE (p_errbuf           OUT     VARCHAR2,
                           p_retcode          OUT     NUMBER,
                           p_view_name         IN     VARCHAR2,
                           p_directory_path    IN     VARCHAR2,
                           p_file_name         IN     VARCHAR2,
                           p_org_name          IN     VARCHAR2
                           )
       IS

    -- Intialize Variables
    l_err_msg             VARCHAR2(2000);
    l_err_code            NUMBER;
    l_sec                 VARCHAR2(150);
    l_procedure_name      VARCHAR2(75) := 'xxwc_ar_getpaid_ob_pkg.CREATE_LOG_FILE';

    -- Reference Cursor Variables
    TYPE ref_cur IS REF CURSOR;
    view_cur              ref_cur;
    view_rec              xxwc_ob_common_pkg.xxcus_ob_file_rec;

    --File Variables
    l_file_handle         utl_file.file_type;
    l_log_file_name       VARCHAR2(150);
    l_file_dir            VARCHAR2(100);
    l_log_file_name_temp  VARCHAR2(150);
    l_org_id              NUMBER;
    l_org_name            VARCHAR2(240);
    l_rec_cnt             NUMBER := 0;
    l_date                VARCHAR2(15);

    l_fexists             BOOLEAN;
    l_file_length         NUMBER;
    l_block_size          INTEGER;
    l_rec_count           VARCHAR2(15);
    l_rec_balance         VARCHAR2(15);
    l_gp_file_name        VARCHAR2(50);
    l_dir_exists          NUMBER;
    l_program_error       EXCEPTION;

BEGIN

        fnd_file.put_line(fnd_file.log,'**********************************************************************************');
        fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.log,'Start of Procedure : CREATE_LOG_FILE');
        fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.log,'Input Parameters     :');
        fnd_file.put_line(fnd_file.log,'p_view_name          :'||p_view_name);
        fnd_file.put_line(fnd_file.log,'p_directory_path     :'||p_directory_path);
        fnd_file.put_line(fnd_file.log,'p_file_name          :'||p_file_name);
        fnd_file.put_line(fnd_file.log,'p_org_name           :'||p_org_name);
        fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');

        l_org_name := p_org_name;

        -- Check if directory path exists
        l_dir_exists := 0;
        BEGIN
          SELECT COUNT(1)
            INTO l_dir_exists
            FROM all_directories
           WHERE directory_name = p_directory_path;
        EXCEPTION
        WHEN OTHERS THEN
          l_err_msg := 'Error validating directory   :'||SQLERRM;
          RAISE l_program_error;
        END;

        -- Raise ProgramError if directory path does not exist
        IF l_dir_exists = 0 THEN
          l_err_msg := 'Directory does not exist in Oracle :'||p_directory_path;
          RAISE l_program_error;
        END IF;

        -- Derive Operating Unit details
        BEGIN
        SELECT organization_id
          INTO l_org_id
          FROM hr_operating_units
         WHERE name = l_org_name;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_err_msg := 'OperatingUnit does not exist :'||l_org_name;
          RAISE l_program_error;
        WHEN OTHERS THEN
          l_err_msg := 'Error deriving OperatingUnit details :'||l_org_name;
          RAISE l_program_error;
        END;

        -- Derive Log File Name
        l_log_file_name       := p_file_name;
        l_log_file_name_temp  := 'TEMP_'||l_log_file_name;
        l_gp_file_name        := REPLACE(p_file_name, '.log', '.txt');

        -- Delete if file already exists
        BEGIN
          UTL_FILE.FREMOVE (p_directory_path,l_log_file_name_temp);
          UTL_FILE.FREMOVE (p_directory_path,l_log_file_name);
        EXCEPTION
        WHEN OTHERS THEN
          NULL;
        END;

        -- Get the details of GetPaid Data File
        utl_file.fgetattr(p_directory_path,
                  l_gp_file_name,
                  l_fexists,
                  l_file_length ,
                  l_block_size);

        -- Start creating the file
        l_file_handle := utl_file.fopen(p_directory_path, l_log_file_name_temp, 'w', 32767);

        OPEN view_cur for 'SELECT * FROM '||p_view_name|| ' WHERE org_id = '||l_org_id;
        LOOP
           FETCH view_cur INTO view_rec;
           EXIT WHEN view_cur%NOTFOUND;

           l_rec_count := SUBSTR(view_rec.rec_line, 1, INSTR(view_rec.rec_line,'_') - 1);
           l_rec_balance := SUBSTR(view_rec.rec_line, INSTR(view_rec.rec_line,'_') + 1);

           -- Write Log file info
           utl_file.put_line (l_file_handle, 'FILE: '||l_gp_file_name|| chr(10) || chr(13));
           utl_file.put_line (l_file_handle, 'RECORDS: '||l_rec_count || chr(13) || chr(10));
           utl_file.put_line (l_file_handle, 'BYTES: '||l_file_length|| chr(10) || chr(13));

           IF SUBSTR(p_file_name,1,10) = 'wcc_armast' THEN
             utl_file.put_line (l_file_handle, 'BALANCE: '||l_rec_balance);
           END IF;
        END LOOP;

        -- 'Rename file for pickup';
        utl_file.frename(p_directory_path, l_log_file_name_temp, p_directory_path, l_log_file_name);

        -- Close the file
        utl_file.fclose(l_file_handle);
        p_retcode := 0;

        fnd_file.put_line(fnd_file.output,'----------------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.output,'**********************************************************************************');

        fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.log,'End of Procedure : CREATE_LOG_FILE');
        fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');

EXCEPTION -- this section traps my errors
    WHEN l_program_error THEN
      l_err_code := 2;
--      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);

      utl_file.fclose(l_file_handle);
      utl_file.fremove(l_file_dir, l_log_file_name);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error running Outbound Common package with Program Error Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AR');
    p_retcode := 2;
    WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);

      utl_file.fclose(l_file_handle);
      utl_file.fremove(l_file_dir, l_log_file_name);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Outbound File Create package with Program Error Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AR');
    p_retcode := 2;
END CREATE_LOG_FILE;

  /********************************************************************************
  Function: GET_INV_SOURCE

  PURPOSE: API to derive the Invoice Source

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/14/2012    Gopi Damuluri    Initial creation of the procedure
  ********************************************************************************/
FUNCTION get_inv_source(p_customer_trx_id IN NUMBER
                      , p_org_id          IN NUMBER)
                 RETURN VARCHAR2 IS
   l_batch_source_name    ra_batch_sources_all.name%TYPE;
   l_trx_number           ra_customer_trx_all.trx_number%TYPE;
BEGIN
  SELECT rbsa.name
       , trx_number
    INTO l_batch_source_name
       , l_trx_number
    FROM ra_batch_sources_all rbsa
       , ra_customer_trx_all rcta
   WHERE 1 = 1
     AND rcta.customer_trx_id = p_customer_trx_id
     AND rcta.batch_source_id = rbsa.batch_source_id
     AND rcta.org_id = rbsa.org_id
     AND rcta.org_id = p_org_id;

  IF UPPER(l_batch_source_name) like '%REBILL%' THEN
    SELECT rbsa.name
      INTO l_batch_source_name
      FROM ra_batch_sources_all rbsa
         , ra_customer_trx_all rcta
     WHERE 1 = 1
       AND rcta.trx_number = l_trx_number
       AND rcta.batch_source_id = rbsa.batch_source_id
       AND rbsa.name NOT LIKE '%REBILL%'
       AND rcta.org_id = rbsa.org_id
       AND rcta.org_id = p_org_id;
  END IF;

  RETURN (l_batch_source_name);

EXCEPTION
 WHEN OTHERS THEN
   RETURN (NULL);
END get_inv_source;

  /********************************************************************************
  Function: GET_CREDIT_LIMIT

  PURPOSE: API to derive the Customer Credit Limit

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/14/2012    Gopi Damuluri    Initial creation of the function
  ********************************************************************************/
FUNCTION get_credit_limit(p_customer_trx_id IN NUMBER
                        , p_org_id          IN NUMBER)
                   RETURN NUMBER IS

   l_credit_limit                  hz_cust_profile_amts.overall_credit_limit%TYPE;
   l_bill_to_site_use_id           ra_customer_trx_all.bill_to_site_use_id%TYPE;

BEGIN

  -- Derive Customer Bill-TO SiteUseId corresponding to the Ship-To Site
  BEGIN
  SELECT hcsu.bill_to_site_use_id
    INTO l_bill_to_site_use_id
    FROM ra_customer_trx_all   rcta
       , hz_cust_site_uses_all hcsu
   WHERE 1 = 1
     AND rcta.ship_to_site_use_id IS NOT NULL
     AND rcta.ship_to_site_use_id = hcsu.site_use_id
     AND rcta.customer_trx_id     = p_customer_trx_id
     AND rcta.org_id              = p_org_id;
  EXCEPTION
  WHEN OTHERS THEN
    l_bill_to_site_use_id := NULL;
  END;

  -- Invoice has no SHIP_TO Site
  -- Derive Primary BILL_TO SiteUseId of the Customer
  IF l_bill_to_site_use_id IS NULL THEN
     BEGIN
     SELECT hcsu.site_use_id
       INTO l_bill_to_site_use_id
       FROM ra_customer_trx_all    rcta
          , hz_cust_site_uses_all  hcsu
          , hz_cust_acct_sites_all hcas
          , hz_cust_accounts_all   hca
      WHERE 1 = 1
        AND rcta.ship_to_site_use_id IS NULL
        AND rcta.sold_to_customer_id = hca.cust_account_id
        AND hcas.cust_account_id     = hca.cust_account_id
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.site_use_code       = 'BILL_TO'
        AND hcsu.primary_flag        = 'Y'
        AND rcta.customer_trx_id     = p_customer_trx_id
        AND rcta.org_id              = hcsu.org_id
        AND rcta.org_id              = p_org_id;
     EXCEPTION
     WHEN OTHERS THEN
       NULL;
     END;
  END IF;

  IF l_bill_to_site_use_id IS NOT NULL THEN

    SELECT NVL(hcpa_s.overall_credit_limit, 0)
      INTO l_credit_limit
      FROM hz_cust_profile_amts hcpa_s
     WHERE hcpa_s.site_use_id = l_bill_to_site_use_id;

    RETURN (l_credit_limit);
  ELSE
    RETURN (NULL);
  END IF;

EXCEPTION
 WHEN OTHERS THEN
   RETURN (NULL);
END get_credit_limit;


  /********************************************************************************
  Function: GET_FLEXNUM3

  PURPOSE: API to derive the Number of Lien Days

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/11/2012    Gopi Damuluri    Initial creation of the function
  ********************************************************************************/
FUNCTION get_flexnum3(p_org_id          IN NUMBER
                    , p_trx_class       IN VARCHAR2
                    , p_party_site_number IN VARCHAR2
                    , p_ship_to_site_use_id IN NUMBER
                    , p_location_id     IN NUMBER
                    , p_hdr_attr2       IN VARCHAR2
                    , p_hdr_attr8       IN VARCHAR2)
                   RETURN NUMBER IS

   l_program_error                EXCEPTION;
   l_trx_date                     DATE;
   l_rental_inv_cnt               NUMBER;
   l_lien_num                     NUMBER;
   l_flex_num3                    NUMBER;

BEGIN
  IF p_ship_to_site_use_id IS NULL THEN
    RAISE l_program_error;
  END IF;

  -- Filter PRISM Rental Invoices
  IF p_hdr_attr8 = 'RENTAL INVOICE' THEN
    RAISE l_program_error;
  END IF;

  IF p_trx_class = 'INV' OR p_trx_class = 'DM' THEN

   -- Filter Order Management Rental Invoices
   l_rental_inv_cnt := 0;
   BEGIN
     SELECT COUNT(1)
       INTO l_rental_inv_cnt
       FROM fnd_lookup_values flv
      WHERE 1 = 1
        AND flv.lookup_type = 'XXWC_RENTAL_ORDERTYPES'
        AND flv.enabled_flag = 'Y'
        AND flv.lookup_code = p_hdr_attr2
        AND SYSDATE BETWEEN flv.start_date_active and NVL(flv.end_date_active,SYSDATE + 1);
   EXCEPTION
   WHEN OTHERS THEN
    RAISE l_program_error;
   END;

   IF l_rental_inv_cnt > 0 THEN
    RAISE l_program_error;
   END IF;

    -- Derive Maximum TransactionDate associated with the ShipTo PartySite
    BEGIN
      SELECT MAX(trx_date)
        INTO l_trx_date
        FROM ra_customer_trx_all       rcta
           , ra_cust_trx_types_all     rctt
           , hz_cust_site_uses_all     hcsu_s
           , hz_cust_acct_sites_all    hcas_s
           , hz_party_sites            hps_s
       WHERE 1 = 1
         AND rcta.cust_trx_type_id   = rctt.cust_trx_type_id
         AND rcta.org_id = rctt.org_id
         AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
         AND NVL(rcta.interface_header_attribute8,'INVOICE') NOT IN ('RENTAL INVOICE')
         AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
         AND hcas_s.party_site_id     = hps_s.party_site_id
         AND rctt.type          IN ('INV','DM')
         AND NOT EXISTS (SELECT '1'
                           FROM fnd_lookup_values flv
                          WHERE 1 = 1
                            AND flv.lookup_type  = 'XXWC_RENTAL_ORDERTYPES'
                            AND flv.enabled_flag = 'Y'
                            AND flv.lookup_code  = rcta.interface_header_attribute2
                            AND SYSDATE BETWEEN flv.start_date_active and NVL(flv.end_date_active,SYSDATE + 1))
         AND hps_s.party_site_number  = p_party_site_number;
    EXCEPTION
    WHEN OTHERS THEN
      RAISE l_program_error;
    END;

    -- LIEN number of days
    BEGIN
      SELECT TO_NUMBER(ar_l.externally_visible_flag)
        INTO l_lien_num
        FROM ar_lookups ar_l
       WHERE ar_l.lookup_type = 'XXWC_LIEN_BY_STATE'
         AND ar_l.lookup_code IN (SELECT hl_s.state
                                    FROM hz_locations hl_s
                                   WHERE hl_s.location_id = p_location_id
                                  )
         AND ar_l.enabled_flag = 'Y'
         AND SYSDATE BETWEEN ar_l.start_date_active and NVL(ar_l.end_date_active,SYSDATE + 1);
    EXCEPTION
    WHEN OTHERS THEN
      RAISE l_program_error;
    END;
    l_flex_num3 := TRUNC(l_trx_date) + l_lien_num - TRUNC(SYSDATE);
    RETURN (l_flex_num3);
  ELSE
    RETURN (NULL);
  END IF;

EXCEPTION
 WHEN l_program_error THEN
   RETURN (NULL);
 WHEN OTHERS THEN
   RETURN (NULL);
END get_flexnum3;

FUNCTION get_bill_to_addr(p_org_id                  IN NUMBER
                        , p_cust_acct_id            IN NUMBER
                        , p_bill_to_site_use_id     IN NUMBER)
                   RETURN VARCHAR2
IS

  l_bill_to_addr      VARCHAR2(250);
BEGIN

    ---------------------------------------------------------------------
    -- Derive Primary Bill-To address for the customer
    ---------------------------------------------------------------------
    SELECT RPAD(NVL(hl_b.address1, ' '),50,' ')             --  BILL_TO_ADDRESS_1
        || RPAD(NVL(TRIM(hl_b.address2), ' '),50,' ')       --  BILL_TO_ADDRESS_2
        || RPAD(NVL(hl_b.city, ' '),50,' ')                 --  BILL_TO_ADDRESS_3
        || RPAD(NVL(hl_b.state, ' '),50,' ')                --  BILL_TO_ADDRESS_4
        || RPAD(NVL(hl_b.postal_code, ' '),50,' ')          --  BILL_TO_ADDRESS_5
      INTO l_bill_to_addr
      FROM hz_cust_site_uses_all hcsu_b
         , hz_cust_acct_sites_all hcas_b
         , hz_party_sites hps_b
         , hz_locations hl_b
     WHERE 1 =1
       AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
       AND hcsu_b.site_use_code     = 'BILL_TO'
       AND hcsu_b.primary_flag      = 'Y'
       AND hcas_b.org_id            = p_org_id
       AND hcas_b.cust_account_id   = p_cust_acct_id       
       AND hcas_b.party_site_id     = hps_b.party_site_id
       AND hps_b.location_id        = hl_b.location_id;

/* 
    ---------------------------------------------------------------------
    -- Derive Bill-To address associated with the Ship-To address
    ---------------------------------------------------------------------
  IF p_bill_to_site_use_id IS NOT NULL THEN
    SELECT RPAD(NVL(hl_b.address1, ' '),50,' ')             --  BILL_TO_ADDRESS_1
        || RPAD(NVL(TRIM(hl_b.address2), ' '),50,' ')       --  BILL_TO_ADDRESS_2
        || RPAD(NVL(hl_b.city, ' '),50,' ')                 --  BILL_TO_ADDRESS_3
        || RPAD(NVL(hl_b.state, ' '),50,' ')                --  BILL_TO_ADDRESS_4
        || RPAD(NVL(hl_b.postal_code, ' '),50,' ')          --  BILL_TO_ADDRESS_5
      INTO l_bill_to_addr
      FROM hz_cust_site_uses_all hcsu_b
         , hz_cust_acct_sites_all hcas_b
         , hz_party_sites hps_b
         , hz_locations hl_b
     WHERE 1 =1
       AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
       AND hcsu_b.site_use_code     = 'BILL_TO'
       AND hcas_b.org_id            = p_org_id
       AND hcas_b.cust_account_id   = p_cust_acct_id
       AND hcas_b.party_site_id     = hps_b.party_site_id
       AND hps_b.location_id        = hl_b.location_id
       AND hcsu_b.site_use_id       = p_bill_to_site_use_id;
  ELSE
    SELECT RPAD(NVL(hl_b.address1, ' '),50,' ')             --  BILL_TO_ADDRESS_1
        || RPAD(NVL(TRIM(hl_b.address2), ' '),50,' ')       --  BILL_TO_ADDRESS_2
        || RPAD(NVL(hl_b.city, ' '),50,' ')                 --  BILL_TO_ADDRESS_3
        || RPAD(NVL(hl_b.state, ' '),50,' ')                --  BILL_TO_ADDRESS_4
        || RPAD(NVL(hl_b.postal_code, ' '),50,' ')          --  BILL_TO_ADDRESS_5
      INTO l_bill_to_addr
      FROM hz_cust_site_uses_all hcsu_b
         , hz_cust_acct_sites_all hcas_b
         , hz_party_sites hps_b
         , hz_locations hl_b
     WHERE 1 =1
       AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
       AND hcsu_b.site_use_code     = 'BILL_TO'
       AND hcsu_b.primary_flag      = 'Y'
       AND hcas_b.org_id            = p_org_id
       AND hcas_b.cust_account_id   = p_cust_acct_id       
       AND hcas_b.party_site_id     = hps_b.party_site_id
       AND hps_b.location_id        = hl_b.location_id;
  END IF;
*/
RETURN l_bill_to_addr;

EXCEPTION
 WHEN OTHERS THEN
   RETURN (NULL);
END get_bill_to_addr;

FUNCTION get_lien_by_state(p_bill_to_site_use_id    IN NUMBER)
                   RETURN VARCHAR2
IS
   l_lien_by_state   ar_lookups.description%TYPE;
BEGIN
    SELECT ar_l.description
      INTO l_lien_by_state
      FROM hz_cust_site_uses_all hcsu_b
         , hz_cust_acct_sites_all hcas_b
         , hz_party_sites hps_b
         , hz_locations hl_b
         , ar_lookups ar_l
     WHERE 1 =1
       AND hcsu_b.site_use_id       = p_bill_to_site_use_id
       AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
       AND hcas_b.party_site_id     = hps_b.party_site_id
       AND hps_b.location_id        = hl_b.location_id
       AND ar_l.lookup_type         = 'XXWC_LIEN_BY_STATE'
       AND ar_l.lookup_code         = hl_b.state;

RETURN l_lien_by_state;

EXCEPTION
 WHEN OTHERS THEN
   RETURN (NULL);
END get_lien_by_state;

END xxwc_ar_getpaid_ob_pkg;
/
