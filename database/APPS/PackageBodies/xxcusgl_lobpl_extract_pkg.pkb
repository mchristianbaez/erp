CREATE OR REPLACE PACKAGE BODY APPS.xxcusgl_lobpl_extract_pkg IS

  /*******************************************************************************
  * Procedure:   create_elec_budget_extract
  * Description: Loads budget information in table XXCUS.XXCUS_ELEC_BUDGET_TBL
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/30/2011    Mani Kumar      Initial creation of the package
  ********************************************************************************/
  l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  /* PROCEDURE CREATE_ELEC_BUDGET_EXTRACT(p_errbuf       OUT VARCHAR2,
                                       p_retcode      OUT NUMBER,
                                       p_budgetname IN VARCHAR2,
                                       p_lob        IN VARCHAR2) IS
  
    --Intialize Variables        
    l_location             VARCHAR2(150);
    l_req_id               NUMBER:=0;--This procedure is called remotely by DW. Hence no Concurrent Request ID    
    l_procedure_name VARCHAR2(75) := 'XXCUSGL_LOBPL_EXTRACT_PKG.CREATE_ELEC_BUDGET_EXTRACT';
    l_lob_segments   VARCHAR2(200):='HDS_'||p_lob||'BUDEXTRACT';
    l_ledger_id CONSTANT gl_ledgers.ledger_id%TYPE := 2042; --HD Supply USD/Main Set of Books
    l_coa_id        CONSTANT GL_CODE_COMBINATIONS.CHART_OF_ACCOUNTS_ID%TYPE := 50328; --HD Supply USD/Main Chart of Accounts
    l_budgetname     gl_budget_versions.budget_name%TYPE := p_budgetname; --Budget Name to be loaded        
        
  BEGIN
    
    l_budgetname   := p_budgetname;
    l_lob_segments := 'HDS_'||p_lob||'BUDEXTRACT';
  
    --Truncate the Summary Table
    BEGIN
      l_location := 'Truncate table before loading specified time frame.';      
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_ELEC_EXTRACT_TBL';
  
    EXCEPTION
      WHEN OTHERS THEN
      p_retcode := 900021;
      p_errbuf  := 'XXCUS: ' || p_retcode ||
                   ': Error during blances extract in ' || l_procedure_name ||
                   ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for XXCUSGL_LOBPL_EXTRACT_PKG'
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => p_retcode
                                          ,p_error_desc        => p_errbuf
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'XXCUS'
                                          ,p_argument1         => p_budgetname
                                          ,p_argument2         => p_lob);
        RAISE program_error;
    END;
  
    --Insert Global Header GTT
    l_location := 'Loading the GL Budgets; ';
  
    Insert /*+ APPEND */
  /*  into XXCUS.XXCUS_ELEC_EXTRACT_TBL
    (Select p_budgetname,
            p_lob,
            'Detail',
            'Detail',
            B.SEGMENT1,
            B.SEGMENT2,              
            (Select DISTINCT s.fru from XXCUS_LOCATION_CODE_VW s where s.ENTRP_ENTITY = B.SEGMENT1 and s.ENTRP_LOC = B.SEGMENT2 and rownum < 2) ,
            B.SEGMENT3,
            B.SEGMENT4,
            B.SEGMENT5,
            NULL,
            NULL,
            A.PERIOD_NUM,
            (A.PERIOD_YEAR - 1),
            A.PERIOD_NET_DR,
            A.PERIOD_NET_CR
       FROM gl_balances           a,
            gl_code_combinations  b,
            GL.GL_BUDGET_VERSIONS C
      WHERE a.code_combination_id = b.code_combination_id
        AND A.CURRENCY_CODE <> 'STAT'
        AND a.ledger_id = l_ledger_id
        AND a.actual_flag = 'B'
        AND b.SEGMENT1 in
            (select flv.description from fnd_lookup_values flv
                    where upper(flv.lookup_type)=upper(l_lob_segments)
                    and flv.VIEW_APPLICATION_ID = 3
                    and upper(flv.LOOKUP_CODE) like upper('SEG1_%'))
        AND b.CHART_OF_ACCOUNTS_ID = l_coa_id
        AND A.BUDGET_VERSION_ID = C.BUDGET_VERSION_ID
        AND C.BUDGET_NAME = l_budgetname);
  
  COMMIT;
  
  INSERT /*+ APPEND */
  /* INTO XXCUS.XXCUS_ELEC_EXTRACT_TBL
      Select p_budgetname,
          p_lob,
          'P ',
          pnl_hierarchy,
          NULL,
          NULL,
          LOB_BRANCH,
          NULL,
          NULL,
          NULL,
          ROLLUP_LEVEL1,
          ROLLUP_LEVEL2,
          period,
          period_year,
          Period_Debit,
          Period_Credit
     from (Select a.period_year,
                  a.period,
                  a.lob_branch,
                  f.pnl_order,
                  f.pnl_hierarchy,
                  f.ROLLUP_LEVEL1,
                  f.ROLLUP_LEVEL2,
                  SUM(a.Period_Debit) Period_Debit,
                  SUM(a.Period_Credit) Period_Credit
             FROM XXCUS.XXCUS_ELEC_EXTRACT_TBL    a,
                  XXCUS.XXCUSGL_LOB_PL_RANGE_TBL f
            WHERE F.LOB = p_lob
              and to_number(a.segment4) >= f.from_account
              and to_number(a.segment4) <= f.thru_account
            group by a.period_year,
                     a.period,
                     a.lob_branch,
                     f.pnl_order,
                     f.pnl_hierarchy,
                     f.ROLLUP_LEVEL1,
                     f.ROLLUP_LEVEL2) a
    order by period_year, period, LOB_BRANCH, pnl_order;
  
    COMMIT;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      p_retcode := 900022;
      p_errbuf  := 'XXCUS: ' || p_retcode ||
                   ': Error during blances extract in ' || l_procedure_name ||
                   ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for XXCUSGL_LOBPL_EXTRACT_PKG'
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => p_retcode
                                          ,p_error_desc        => p_errbuf
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'XXCUS'
                                          ,p_argument1         => p_budgetname
                                          ,p_argument2         => p_lob);
  
    WHEN OTHERS THEN
      p_retcode := 900023;
      p_errbuf  := 'XXCUS: ' || p_retcode ||
                   ': Error during blances extract in ' || l_procedure_name ||
                   ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for XXCUSGL_LOBPL_EXTRACT_PKG'
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => p_retcode
                                          ,p_error_desc        => p_errbuf
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'XXCUS'
                                          ,p_argument1         => p_budgetname
                                          ,p_argument2         => p_lob);
  
  
  END CREATE_ELEC_BUDGET_EXTRACT;*/

  /*******************************************************************************
  * Procedure:   create_elec_scos_extract
  * Description: Loads budget information in table XXCUS.XXCUS_ELEC_SCOS_EXTRACT_TBL
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/30/2011    Mani Kumar      Initial creation of the package
  1.1     04/03/2012    Luong Vu        Change period_year -1 - RFC 33287
  ********************************************************************************/

  PROCEDURE create_elec_scos_extract(p_errbuf      OUT VARCHAR2
                                    ,p_retcode     OUT NUMBER
                                    ,p_fiscal_year IN VARCHAR2
                                    ,p_lob         IN VARCHAR2) IS
  
    --Intialize Variables        
    l_location       VARCHAR2(150);
    l_req_id         NUMBER := 0; --This procedure is called remotely by DW. Hence no Concurrent Request ID
    l_procedure_name VARCHAR2(75) := 'XXCUSGL_LOBPL_EXTRACT_PKG.CREATE_ELEC_SCOS_EXTRACT';
    l_lob_segments   VARCHAR2(200) := 'HDS_' ||
                                      substr(p_lob, 1,
                                             (instr(p_lob, '-') - 1)) ||
                                      'BUDEXTRACT';
    l_ledger_id CONSTANT gl_ledgers.ledger_id%TYPE := 2061; --HD Supply USD/Main Set of Books
    l_fy_year gl_balances.period_year%TYPE := p_fiscal_year; --Will hold the year for the extract to be run for        
    l_coa_id CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --HD Supply USD/Main Chart of Accounts
  
  BEGIN
  
    --l_fy_year   := p_fiscal_year;
    --l_lob_segments := 'HDS_' || substr(p_lob,1,(instr(p_lob,'-')-1)) || 'BUDEXTRACT';
  
    --Truncate the Summary Table
    BEGIN
      l_location := 'Truncate table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_ELEC_SCOS_EXTRACT_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        p_retcode := 900024;
        p_errbuf  := 'XXCUS: ' || p_retcode ||
                     ': Error during blances extract in ' ||
                     l_procedure_name || ' at ' || l_location || ' - ' ||
                     substr(SQLERRM, 1, 1900);
        fnd_file.put_line(fnd_file.log, p_errbuf);
        xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                             p_calling => 'Exception for XXCUSGL_LOBPL_EXTRACT_PKG',
                                             p_request_id => l_req_id,
                                             p_ora_error_msg => p_retcode,
                                             p_error_desc => p_errbuf,
                                             p_distribution_list => l_distro_list,
                                             p_module => 'XXCUS',
                                             p_argument1 => p_fiscal_year,
                                             p_argument2 => p_lob);
        RAISE program_error;
    END;
  
    --Insert Global Header GTT
    l_location := 'Loading GL Fiscal year SCOS Balances ';
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcus_elec_scos_extract_tbl
      (SELECT l_fy_year
             ,p_lob
             ,'Detail'
             ,'Detail'
             ,b.segment1
             ,b.segment2
             ,(SELECT DISTINCT s.fru
                FROM xxcus_location_code_vw s
               WHERE s.entrp_entity = b.segment1
                 AND s.entrp_loc = b.segment2
                 AND rownum < 2)
             ,b.segment3
             ,b.segment4
             ,b.segment5
             ,NULL
             ,NULL
             ,a.period_num
             ,a.period_year
             ,a.period_net_dr
             ,a.period_net_cr
         FROM gl_balances          a
             ,gl_code_combinations b
        WHERE a.code_combination_id = b.code_combination_id
          AND a.currency_code <> 'STAT'
          AND a.ledger_id = l_ledger_id
          AND a.actual_flag = 'A'
          AND a.period_year = l_fy_year
          AND b.segment1 IN
              (SELECT flv.description
                 FROM fnd_lookup_values flv
                WHERE upper(flv.lookup_type) = upper(l_lob_segments)
                  AND flv.view_application_id = 3
                  AND upper(flv.lookup_code) LIKE upper('SEG1_%'))
          AND b.chart_of_accounts_id = l_coa_id);
  
    COMMIT;
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcus_elec_scos_extract_tbl
      SELECT l_fy_year
            ,p_lob
            ,'SCOS'
            ,pnl_hierarchy
            ,NULL
            ,NULL
            ,lob_branch
            ,NULL
            ,NULL
            ,NULL
            ,rollup_level1
            ,rollup_level2
            ,period
            ,period_year
            ,period_debit
            ,period_credit
        FROM (SELECT a.period_year
                    ,a.period
                    ,a.lob_branch
                    ,f.pnl_order
                    ,f.pnl_hierarchy
                    ,f.rollup_level1
                    ,f.rollup_level2
                    ,SUM(a.period_debit) period_debit
                    ,SUM(a.period_credit) period_credit
                FROM xxcus.xxcus_elec_scos_extract_tbl a
                    ,xxcus.xxcusgl_lob_pl_range_tbl    f
               WHERE f.lob = p_lob
                 AND to_number(a.segment4) >= f.from_account
                 AND to_number(a.segment4) <= f.thru_account
               GROUP BY a.period_year
                       ,a.period
                       ,a.lob_branch
                       ,f.pnl_order
                       ,f.pnl_hierarchy
                       ,f.rollup_level1
                       ,f.rollup_level2) a
       ORDER BY period_year
               ,period
               ,lob_branch
               ,pnl_order;
  
    COMMIT;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      p_retcode := 900025;
      p_errbuf  := 'XXCUS: ' || p_retcode ||
                   ': Error during blances extract in ' || l_procedure_name ||
                   ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSGL_LOBPL_EXTRACT_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS',
                                           p_argument1 => p_fiscal_year,
                                           p_argument2 => p_lob);
    WHEN OTHERS THEN
      p_retcode := 900026;
      p_errbuf  := 'XXCUS: ' || p_retcode ||
                   ': Error during blances extract in ' || l_procedure_name ||
                   ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSGL_LOBPL_EXTRACT_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS',
                                           p_argument1 => p_fiscal_year,
                                           p_argument2 => p_lob);
    
  END create_elec_scos_extract;

  /*******************************************************************************
  * Procedure:   create_elec_shrink_extract
  * Description: Loads budget information in table XXCUS.XXCUS_ELEC_SHRINK_EXTRACT_TBL
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/30/2011    Mani Kumar      Initial creation of the package
  1.1     04/03/2012    Luong Vu        Change period_year -1 - RFC 33287
  ********************************************************************************/

  PROCEDURE create_elec_shrink_extract(p_errbuf      OUT VARCHAR2
                                      ,p_retcode     OUT NUMBER
                                      ,p_fiscal_year IN VARCHAR2
                                      ,p_lob         IN VARCHAR2) IS
  
    --Intialize Variables        
    l_location       VARCHAR2(150);
    l_req_id         NUMBER := 0; --This procedure is called remotely by DW. Hence no Concurrent Request ID
    l_procedure_name VARCHAR2(75) := 'XXCUSGL_LOBPL_EXTRACT_PKG.CREATE_ELEC_SHRINK_EXTRACT';
    l_lob_segments   VARCHAR2(200) := 'HDS_' ||
                                      substr(p_lob, 1,
                                             (instr(p_lob, '-') - 1)) ||
                                      'BUDEXTRACT';
    l_ledger_id CONSTANT gl_ledgers.ledger_id%TYPE := 2061; --HD Supply USD/Main Set of Books
    l_fy_year gl_balances.period_year%TYPE := p_fiscal_year; --Will hold the year for the extract to be run for        
    l_coa_id CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --HD Supply USD/Main Chart of Accounts
  
  BEGIN
  
    --l_fy_year   := p_fiscal_year;
    --l_lob_segments := 'XXHSI' || substr(p_lob,1,(instr(p_lob,'-')-1)) || 'BUDEXTRACT';
  
    BEGIN
      l_location := 'Truncate table before loading specified time frame';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_ELEC_SHRINK_EXTRACT_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        p_retcode := 900027;
        p_errbuf  := 'XXCUS: ' || p_retcode ||
                     ': Error during blances extract in ' ||
                     l_procedure_name || ' at ' || l_location || ' - ' ||
                     substr(SQLERRM, 1, 1900);
        fnd_file.put_line(fnd_file.log, p_errbuf);
        xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                             p_calling => 'Exception for XXCUSGL_LOBPL_EXTRACT_PKG',
                                             p_request_id => l_req_id,
                                             p_ora_error_msg => p_retcode,
                                             p_error_desc => p_errbuf,
                                             p_distribution_list => l_distro_list,
                                             p_module => 'XXCUS',
                                             p_argument1 => p_fiscal_year,
                                             p_argument2 => p_lob);
        RAISE program_error;
    END;
  
    --Insert Global Header GTT
    l_location := 'Loading GL SHRINK Balances ';
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcus_elec_shrink_extract_tbl
      (SELECT l_fy_year
             ,p_lob
             ,'Detail'
             ,'Detail'
             ,b.segment1
             ,b.segment2
             ,(SELECT DISTINCT s.fru
                FROM xxcus_location_code_vw s
               WHERE s.entrp_entity = b.segment1
                 AND s.entrp_loc = b.segment2
                 AND rownum < 2)
             ,b.segment3
             ,b.segment4
             ,b.segment5
             ,NULL
             ,NULL
             ,a.period_num
             ,a.period_year
             ,a.period_net_dr
             ,a.period_net_cr
         FROM gl_balances          a
             ,gl_code_combinations b
        WHERE a.code_combination_id = b.code_combination_id
          AND a.currency_code <> 'STAT'
          AND a.ledger_id = l_ledger_id
          AND a.actual_flag = 'A'
          AND a.period_year = l_fy_year
          AND b.segment1 IN
              (SELECT flv.description
                 FROM fnd_lookup_values flv
                WHERE upper(flv.lookup_type) = upper(l_lob_segments)
                  AND flv.view_application_id = 3
                  AND upper(flv.lookup_code) LIKE upper('SEG1_%'))
          AND b.chart_of_accounts_id = l_coa_id);
  
    COMMIT;
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcus_elec_shrink_extract_tbl
      SELECT l_fy_year
            ,p_lob
            ,'SHRINK'
            ,pnl_hierarchy
            ,NULL
            ,NULL
            ,lob_branch
            ,NULL
            ,NULL
            ,NULL
            ,rollup_level1
            ,rollup_level2
            ,period
            ,period_year
            ,period_debit
            ,period_credit
        FROM (SELECT a.period_year
                    ,a.period
                    ,a.lob_branch
                    ,f.pnl_order
                    ,f.pnl_hierarchy
                    ,f.rollup_level1
                    ,f.rollup_level2
                    ,SUM(a.period_debit) period_debit
                    ,SUM(a.period_credit) period_credit
                FROM xxcus.xxcus_elec_shrink_extract_tbl a
                    ,xxcus.xxcusgl_lob_pl_range_tbl      f
               WHERE f.lob = p_lob
                 AND to_number(a.segment4) >= f.from_account
                 AND to_number(a.segment4) <= f.thru_account
               GROUP BY a.period_year
                       ,a.period
                       ,a.lob_branch
                       ,f.pnl_order
                       ,f.pnl_hierarchy
                       ,f.rollup_level1
                       ,f.rollup_level2) a
       ORDER BY period_year
               ,period
               ,lob_branch
               ,pnl_order;
  
    COMMIT;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      p_retcode := 900028;
      p_errbuf  := 'XXCUS: ' || p_retcode ||
                   ': Error during blances extract in ' || l_procedure_name ||
                   ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSGL_LOBPL_EXTRACT_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS',
                                           p_argument1 => p_fiscal_year,
                                           p_argument2 => p_lob);
    
    WHEN OTHERS THEN
      p_retcode := 900029;
      p_errbuf  := 'XXCUS: ' || p_retcode ||
                   ': Error during blances extract in ' || l_procedure_name ||
                   ' at ' || l_location || ' - ' ||
                   substr(SQLERRM, 1, 1900);
      fnd_file.put_line(fnd_file.log, p_errbuf);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => 'Exception for XXCUSGL_LOBPL_EXTRACT_PKG',
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => p_retcode,
                                           p_error_desc => p_errbuf,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'XXCUS',
                                           p_argument1 => p_fiscal_year,
                                           p_argument2 => p_lob);
    
  
  END create_elec_shrink_extract;

END xxcusgl_lobpl_extract_pkg;
/
