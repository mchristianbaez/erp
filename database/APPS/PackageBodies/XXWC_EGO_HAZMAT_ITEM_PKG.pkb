CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_HAZMAT_ITEM_PKG
AS
   /*********************************************************************************************************************************************
   -- File Name: XXWC_EGO_HAZMAT_ITEM_PKG.pkb
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:
   -- HISTORY
   -- ===========================================================================================================================================
   -- ===========================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -----------------------------------------------------------------------------------------------------
   -- 1.0     26-JAN-2016   P.Vamshidhar    TMS#20150717-00041 - 2015 PLM - EHS Attribute Related changes - Org Attributes Moving to Master UDA
   --                                       Initial Version
   *********************************************************************************************************************************************/

   g_err_callfrom   VARCHAR2 (100) := 'XXWC_EGO_HAZMAT_ITEM_PKG';
   g_distro_list    VARCHAR2 (100) := 'HDSOracleDevelopers@hdsupply.com';
   gsec             VARCHAR2 (1000);



   FUNCTION HAZMAT_ATTR (p_item_id IN NUMBER, p_attr_name IN VARCHAR2)
      /************************************************************************************************************************************************
       FUNCTION : HAZMAT_ATTR

       REVISIONS:
       Ver        Date            Author                 Description
       ---------  ----------    ---------------    ----------------------------------------------------------------------------------------------------
       1.0        26-JAN-2016   P.Vamshidhar       TMS#20150717-00041 - 2015 PLM - EHS Attribute Related changes - Org Attributes Moving to Master UDA
                                                   Parameters: p_item_id    --> Inventory_item_id
                                                               p_attr_name  --> PDH Attribute Internal Name

                                                   PDH Attribute name      PDH Internal Name            Old Inventory Attribute
                                                   ------------------    --------------------------     -------------------------
                                                   MSDS#                 XXWC_MSDS_No_ATTR               Attribute8
                                                   Package Group         XXWC_PKG_GROUP_ATTR             Attribute9
                                                   Hazmat Description    XXWC_DOT_S_N_ATTR               Attribute17
                                                   Limited Qty Flag      XXXWC_LTD_QTY_ATTR              Attribute11
                                                   Container Type        XXWC_CONTAINER_TYPE_ATTR        Attribute18
                                                   CA Prop 65            XXWC_CA_PROP65_ATTR             Attribute1
                                                   VOC Content           XXWC_VOC_CONTENT_ATTR           Attribute4
                                                   VOC Category          XXWC_VOC_CAT_ATTR               Attribute6
                                                   VOC Sub_Categopry     XXWC_VOC_SUB_CAT_ATTR           Attribute7
                                                   Pesticide Flag        XXWC_PESTICIDE_FLG_ATTR         Attribute3
                                                   Pesticide Flag State  XXWC_PESTICIDE_FLG_ST_ATTR      Attribute5
       ***************************************************************************************************************************************************/
      RETURN VARCHAR2
   IS
      -- Variables declaration
      lvc_return            VARCHAR2 (1000);
      l_err_msg             VARCHAR2 (2000);
      l_procedure           VARCHAR2 (100) := 'ITEM_HAZMAT_ATTR';
      lvc_group_name        VARCHAR2 (100) := 'XXWC_EHS_AG';
      lvc_attr_group_type   VARCHAR2 (100) := 'EGO_ITEMMGMT_GROUP';
      lvc_column_name       VARCHAR2 (100);
      lvc_sql_stat          VARCHAR2 (1000);
   BEGIN
      gsec := ' Main Section ';

      IF p_attr_name IS NOT NULL AND p_item_id IS NOT NULL
      THEN
         gsec := ' Deriving DB column Name ';

         SELECT DATABASE_COLUMN
           INTO lvc_column_name
           FROM EGO_ATTRS_V
          WHERE ATTR_GROUP_NAME = lvc_group_name AND ATTR_NAME = p_attr_name;

         gsec := ' SQL Statement Preparation ';

         IF SUBSTR (lvc_column_name, 1, 2) = 'TL'
         THEN
            gsec := ' Deriving DB column Name ';

            lvc_sql_stat :=
                  'SELECT eira.'
               || lvc_column_name
               || ' FROM apps.ego_mtl_sy_items_ext_TL EIRA, apps.ego_attr_groups_v ag WHERE ag.attr_group_id = eira.attr_group_id AND eira.organization_id = 222 and ag.attr_group_name= :1 AND'
               || ' ag.attr_group_type = :2 AND eira.inventory_item_id= :3 ';
         ELSE
            lvc_sql_stat :=
                  'SELECT eira.'
               || lvc_column_name
               || ' FROM apps.EGO_MTL_SY_ITEMS_EXT_B EIRA, apps.ego_attr_groups_v ag WHERE ag.attr_group_id = eira.attr_group_id AND eira.organization_id = 222 and ag.attr_group_name= :1 AND'
               || ' ag.attr_group_type = :2 AND eira.inventory_item_id= :3 ';
         END IF;

         gsec := ' Deriving attribute value ';

         EXECUTE IMMEDIATE lvc_sql_stat
            INTO lvc_return
            USING lvc_group_name, lvc_attr_group_type, p_item_id;
      ELSE
         lvc_return := NULL;
      END IF;

      RETURN lvc_return;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         lvc_return := NULL;
         RETURN lvc_return;
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (
                                          gsec
                                       || ' Item Id:'
                                       || p_item_id
                                       || ' Attr Name:'
                                       || p_attr_name,
                                       1,
                                       240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         lvc_return := NULL;
         RETURN lvc_return;
   END;
END XXWC_EGO_HAZMAT_ITEM_PKG;
/