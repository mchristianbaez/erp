CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OPN_PM_WEB_EXTRACT_PKG as
/*
 -- Implementation Date: 25-JUN-2013
 -- Author: Balaguru Seshadri
 -- Scope: This package has some common routines used by the HDS OPN Lease PM Web Extract process.
 -- ESMS ticket 194475
 -- Modification History
 -- Date          ESMS           Notes
 -- ----------------------------------------------------------
    06/27/2013   194475      1)BU description is a combination of the dff's on the flex rather than the flex description.
                             2)Added date format to option date fields 
   07/15/2013   194475       3)Added regexp_replace to remove the line feeds that triggered the records to break up.
   09/09/2013  221642        4)Add sort by to the custom table xxcus.xxcus_opn_pm_web_extract_b before creating the text file
                               Sort by: COUNTRY, STATE_PROVINCE, CITY and RER_ID   
   07/03/2014   253936       New routines to populate lease contact role custom tables.
                             a)procedure extract_lea_cont_roles
                             b)procedure Insert_Lease_Cont_Roles
                             c)procedure create_lease_cont_role_tables
                             d)procedure store_table_matrix
  08/20/2014   253936        a)Extract additional TAB's for a lease like detail, locations, rights/obligations, options, payments and notes.
  08/28/2014   253936        a)Modify xxcus_opn_lease_tab_dtl to fetch location code and location name from pn_locations_all and not pn_leases_v
  12/11/2014   272717        Remove the routines and their references as we are moving them to the package body xxcus_opn_master_extract
                               1)  xxcus_opn_lease_tab_dtl
                               2)  xxcus_opn_lease_tab_loc                                                                
                               3)  xxcus_opn_lease_tab_insr
                               4)  xxcus_opn_lease_tab_rights
                               5)  xxcus_opn_lease_tab_oblig
                               6)  xxcus_opn_lease_tab_options                                                                                                                          
                               7)  xxcus_opn_lease_tab_notes
                               8)  xxcus_opn_lease_tab_billings 
  05/01/2015  283790       Add new routine rename_file and invoke it in the main routine                                                                                            
 --
 --
*/
 --
 g_flexfield_exists  varchar2(1) :='N';
 b_create boolean;
 --  
 procedure print_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_log;
 -- 
 procedure store_table_matrix
   (
     p_caller             in varchar2
    ,p_lookup_type        in varchar2
    ,p_lookup_code        in varchar2
    ,p_table_name         in varchar2
    ,p_table_description  in varchar2
    ,p_flexfield_exists   in varchar2
    ,p_created_by         in number
    ,p_creation_date      in date
   ) is
 begin
  savepoint start_here;
  insert into xxcus.xxcus_hds_pn_lookup_objects
   (
      lookup_type
     ,lookup_code
     ,table_name
     ,table_description
     ,flexfield_exists
     ,created_by
     ,creation_date
   )
  values
   (
     p_lookup_type
    ,p_lookup_code
    ,p_table_name
    ,p_table_description
    ,p_flexfield_exists
    ,p_created_by
    ,p_creation_date
   );
 exception
  when others then
   rollback to start_here;
   print_log ('When Others @store_table_matrix: p_caller =>'
              ||p_caller
              ||', p_lookup_type ='
              ||p_lookup_type
              ||', lookup_code ='
              ||p_lookup_code
              ||', table_name ='
              ||p_table_name
              ||', table_description ='
              ||p_table_description
             );
   print_log('When Others @store_table_matrix: message stack: '||sqlerrm);
 end store_table_matrix;
 --
 function create_lease_cont_role_tables (p_tbl_prepend in varchar2) return boolean is
 --
  cursor get_feature_lookup_codes is
  select lookup_code
        ,meaning table_description
        ,SUBSTR(p_tbl_prepend||upper(replace(replace(replace(lookup_code, ' - ', '_'), ' ', '_'),'-','')), 1, 30) table_name
  from   fnd_lookup_values_vl
  where  1 =1
    and  lookup_type ='PN_LEASE_ROLE_TYPE'
  order by lookup_code asc;
 --
    cursor get_dff_columns (p_feature_lookup_meaning in varchar2) is
    select ', '||upper(replace(replace(replace(c.FORM_LEFT_PROMPT, ' - ', '_'), ' ', '_'),'-',''))||' VARCHAR2(150)' DFF_COL
    from   fnd_descriptive_flexs_vl a--fnd_descr_flex_contexts_vl
          ,fnd_descr_flex_contexts_vl b
          ,fnd_descr_flex_col_usage_vl c
    where  1 =1
      and  a.application_id =240
      and  a.application_table_name ='PN_CONTACT_ASSIGNMENTS_ALL'
      and  a.descriptive_flexfield_name ='PN_CONTACT_ASSIGNMENTS'
      and  a.context_column_name ='ATTRIBUTE_CATEGORY'
      and  b.descriptive_flexfield_name =a.descriptive_flexfield_name
      and  b.descriptive_flex_context_name =p_feature_lookup_meaning
      and  c.descriptive_flex_context_code  =b.descriptive_flex_context_code
      and  c.descriptive_flexfield_name =a.descriptive_flexfield_name;
 --
 v_tbl_create        varchar2(4000);
 v_dff               varchar2(4000);
 v_tbl_name          varchar2(30);
 v_lookup_code       varchar2(80);
 v_table_description varchar2(240);
 v_exec              BOOLEAN;
 v_posn              number :=1;
 n_count             number :=0;
 v_comment           varchar2(4000);
 v_enter             varchar2(1) :='
';
 --
 begin
  --
  g_flexfield_exists :='N';
  --
  print_log('');
  print_log('Begin -Create lease contact role custom tables.');
  --
  for rec in get_feature_lookup_codes loop
   --
   v_tbl_name :=rec.table_name;
   --
   v_lookup_code :=rec.lookup_code;
   --
   v_table_description :=rec.table_description;
   --
   print_log('');
   print_log('Table Name ='||v_tbl_name||' , Lookup Code ='||v_lookup_code||' , Lookup Description ='||v_table_description);
   --
   begin
     --
     v_posn :=1;
     for dff_rec in get_dff_columns (p_feature_lookup_meaning =>v_table_description) loop
      if v_posn =1 then
       v_dff :=dff_rec.DFF_COL||v_enter;
       v_posn :=2;
      else
       v_dff :=v_dff||dff_rec.DFF_COL||v_enter;
      end if;
     end loop;
     --
       v_tbl_create
        := 'CREATE TABLE '
           ||v_tbl_name
           ||v_enter
           ||'( '
           ||v_enter
           ||'  LEASE_NUM VARCHAR2(30)'
           ||v_enter
           ||', PRIMARY_LOC_CODE VARCHAR2(40)'
           ||v_enter
           ||', COMPANY_NAME VARCHAR2(80)'
           ||v_enter           
           ||', SITE_NAME VARCHAR2(40)'
           ||v_enter
           ||', LEASE_ROLE_TYPE VARCHAR2(30)'
           ||v_enter
           ||', LEASE_ROLE VARCHAR2(80)'
           ||v_enter
           ||', CONTACT_NAME VARCHAR2(150)'
           ||v_enter
           ||', ADDRESS_LINE1 VARCHAR2(150)'
           ||v_enter
           ||', ADDRESS_LINE2 VARCHAR2(150)'
           ||v_enter
           ||', CITY VARCHAR2(150)'
           ||v_enter
           ||', COUNTY VARCHAR2(150)'
           ||v_enter
           ||', POSTAL_CODE VARCHAR2(150)'
           ||v_enter                             
           ||', STATE VARCHAR2(150)'
           ||v_enter
           ||', PROVINCE VARCHAR2(150)'
           ||v_enter
           ||', COUNTRY VARCHAR2(150)'
           ||v_enter                                                                                        
           ||', STATUS VARCHAR2(1)'
           ||v_enter
           ||', CONTACT_ASSIGNMENT_ID number'
           ||v_enter
           ||', COMPANY_ID number'
           ||v_enter
           ||', COMPANY_SITE_ID number'
           ||v_enter
           ||', LEASE_ID number'
           ||v_enter            
           ||', LEASE_CHANGE_ID number'
           ||v_enter            
           ||', LOCATION_ID number'
           ||v_enter                                                                  
           ||', ADDRESS_ID number'
           ||v_enter
           ||', ORG_ID NUMBER'
           ||
           case
              when v_dff is not null then
                 v_enter
               ||v_dff
               ||' )'
              else
                 v_enter
               ||' )' end;
     --               
         begin                 
          select case when v_dff is not null then 'Y' else 'N' end
          into   g_flexfield_exists
          from   dual;
         exception
           when others then
           g_flexfield_exists :='N';
         end;
     --               
     v_dff :=null;
     --
   exception
    when others then
      print_log('Build feature table SQL for lookup code ='||v_lookup_code||', message ='||sqlerrm);
      v_exec :=FALSE;
   end;
   --
   --print_log(v_tbl_create);
   --print_log('');
   --
   begin
    select count(1)
    into   n_count
    from   all_tables
    where  1 =1
      and  owner||'.'||table_name =v_tbl_name;
   exception
    when no_data_found then
     n_count :=0;
    when others then
     n_count :=0;
   end;
   --
    if n_count >0 then
     --print_log('@feature, Inside n_count >0');
     -- truncate old data, drop and recreate the table
     begin
      --
      begin
        execute immediate 'TRUNCATE TABLE '||v_tbl_name;
          begin
            execute immediate 'DROP TABLE '||v_tbl_name;
              begin
                execute immediate v_tbl_create;
                  begin
                   v_comment :='COMMENT ON TABLE '||v_tbl_name||' IS '||''''||' Table to hold HDS lease contact role for '||v_table_description||'''';
                   --print_log('COMMENT SQL: '||v_comment);
                   execute immediate v_comment;
                  exception
                   when others then
                    print_log ('Comment on table '||v_tbl_name||' failed, Error =>'||sqlerrm);
                    v_exec :=FALSE;
                  end;
                --print_log ('Table: '||v_tbl_name||' created.');
                  store_table_matrix
                   (
                     p_caller             =>'PN_CONTACT_ASSIGNMENTS' --in varchar2
                    ,p_lookup_type        =>'LEASE_CONT_ROLES_ASGN' --in varchar2
                    ,p_lookup_code        =>v_lookup_code --in varchar2
                    ,p_table_name         =>v_tbl_name --in varchar2
                    ,p_table_description  =>v_table_description --in varchar2
                    ,p_flexfield_exists   =>g_flexfield_exists --in varchar2
                    ,p_created_by         =>fnd_global.user_id --in number
                    ,p_creation_date      =>sysdate --in date
                   );
                  --
                  v_exec :=TRUE;
                  --
              exception
               when others then
                print_log ('Create table '||v_tbl_name||'failed, Error =>'||sqlerrm);
                v_exec :=FALSE;
              end;
          exception
           when others then
            print_log ('Drop table '||v_tbl_name||'failed, Error =>'||sqlerrm);
            v_exec :=FALSE;
          end;
      exception
       when others then
        print_log ('Truncate table '||v_tbl_name||'failed, Error =>'||sqlerrm);
        v_exec :=FALSE;
      end;
      --
     exception
      when others then
       print_log ('Table: '||v_tbl_name||' creation failed, Error =>'||sqlerrm);
       v_exec :=FALSE;
     end;
     print_log('');
    else
     --print_log('@feature, Inside n_count =0');
     -- first time create new feature table
     begin
      --
      execute immediate v_tbl_create;
      execute immediate 'COMMENT ON TABLE '||v_tbl_name||' IS '||''''||' Table to hold HDS lease contact role for '||v_table_description||'''';
      --print_log ('Table: '||v_tbl_name||' created.');
      --
      store_table_matrix
       (
         p_caller             =>'PN_CONTACT_ASSIGNMENTS' --in varchar2
        ,p_lookup_type        =>'LEASE_CONT_ROLES_ASGN' --in varchar2
        ,p_lookup_code        =>v_lookup_code --in varchar2
        ,p_table_name         =>v_tbl_name --in varchar2
        ,p_table_description  =>v_table_description --in varchar2
        ,p_flexfield_exists   =>g_flexfield_exists --in varchar2
        ,p_created_by         =>fnd_global.user_id --in number
        ,p_creation_date      =>sysdate --in date
       );
      --
       v_exec :=TRUE;
      --
     exception
      when others then
       print_log ('Table: '||v_tbl_name||' creation failed, Error =>'||SQLERRM);
       v_exec :=FALSE;
     end;
     print_log('');
    end if;
   --
   v_exec :=TRUE;
   --
  end loop;
  --
  print_log('End -Create lease contact role custom tables.');
  print_log('');
  --
  RETURN v_exec;
  --
 exception
  --
  when others then
    print_log('');
     return FALSE;
  --
 end create_lease_cont_role_tables;
 -- 
 --
 procedure Insert_Lease_Cont_Roles 
  (
   p_loc_id      in number
  ,p_loc_code    in varchar2
  ,p_lease_id    in number
  ,p_lease_num   in varchar2
  ,p_feature     in varchar2
  ,p_table       in varchar2
  ,p_dff_exists  in varchar2  
  ) is
  --
  g_loc_id    NUMBER :=p_loc_id;  
  g_loc_code  VARCHAR(40);
  g_lease_num VARCHAR(40);
  g_lease_id  NUMBER :=p_lease_id;
  g_feature   VARCHAR2(80);  
  v_cr VARCHAR2(2) :='
';
  v_final_sql VARCHAR2(10000) :=Null;
  --
  cursor get_dff (p_alias in varchar2, p_feature_lookup_meaning in varchar2) is
    select '    ,'||p_alias||'.'||c.application_column_name||' '||upper(replace(replace(replace(c.FORM_LEFT_PROMPT, ' - ', '_'), ' ', '_'),'-','')) dff_dtl
           ,','||upper(replace(replace(replace(c.FORM_LEFT_PROMPT, ' - ', '_'), ' ', '_'),'-','')) dff_hdr
    from   fnd_descriptive_flexs_vl a--fnd_descr_flex_contexts_vl
          ,fnd_descr_flex_contexts_vl b
          ,fnd_descr_flex_col_usage_vl c
    where  1 =1
      and  a.application_id =240
      and  a.application_table_name ='PN_CONTACT_ASSIGNMENTS_ALL'
      and  a.descriptive_flexfield_name ='PN_CONTACT_ASSIGNMENTS'
      and  a.context_column_name ='ATTRIBUTE_CATEGORY'
      and  b.descriptive_flexfield_name =a.descriptive_flexfield_name
      and  b.descriptive_flex_context_name =p_feature_lookup_meaning
      and  c.descriptive_flex_context_code  =b.descriptive_flex_context_code
      and  c.descriptive_flexfield_name =a.descriptive_flexfield_name
      order by c.column_seq_num;     
  --  
 CURSOR get_features is
    select b.table_description cont_role_feature, b.table_name, b.flexfield_exists
    --a.location_feature, b.table_name, flexfield_exists
    from  xxcus.xxcus_hds_pn_lookup_objects B
    where  1 =2 --do not change this. we just need the field names and data type for reference in the plsql table
     and   b.lookup_type        ='LEASE_CONT_ROLES_ASGN'
     and   b.table_description  ='LL Notice To:' --this condition will not affect as we are using the cursor just as a ref for fields.
    group by b.table_description, b.table_name, b.flexfield_exists;
 --
 type g_fea_tbl is table of get_features%rowtype index by binary_integer;
 g_fea_rec g_fea_tbl; 
 --
  b_proceed   BOOLEAN :=FALSE;
  v_dff_hdr   VARCHAR2(5000) :=Null;
  v_dff_dtl   VARCHAR2(5000) :=Null;     
  --
  --
  v_dff varchar2(2000) :=Null;
  v_sql_hdr2 varchar2(10000);
  v_sql_dtl varchar2(10000);
  n_loc number :=100; 
  --
  v_sql_hdr1 VARCHAR2(200) :=  
  'INSERT INTO ';     
  --
  v_sql_tbl varchar2(10000) := 
    'from pn_contact_assignments_v Cont
    ,xxcus.xxcus_opn_pm_web_extract_b  lea --pn_leases_v lea 
    ,pn_addresses_all addr
where 1 =1
  and lea.lease_id =cont.lease_id 
  and addr.address_id =cont.address_id
  and lea.lease_id ='||G_lease_id||v_cr;  
  --       
 begin  
  --
  begin
   -- 
   --mo_global.set_policy_context('S', 163);
   --
   n_loc        :=101;
   G_loc_code   :=p_loc_code;
   G_lease_id   :=p_lease_id;
   G_lease_num  :=p_lease_num;
   -- 
   n_loc :=102;
   --   
  v_sql_hdr2 :='
  (
   lease_num
  ,primary_loc_code
  ,company_name
  ,site_name
  ,lease_role_type
  ,lease_role
  ,contact_name
  ,address_line1
  ,address_line2
  ,city
  ,county
  ,postal_code
  ,state
  ,province
  ,country
  ,status
  ,contact_assignment_id
  ,company_id
  ,company_site_id
  ,lease_id
  ,lease_change_id
  ,location_id
  ,address_id
  ,org_id';
  --
  v_sql_dtl :=
  '  '||'
  (
   SELECT '||''''||g_lease_num||''''||' lease_num '||'
  ,'||''''||g_loc_code||''''||' primary_loc_code '||'
      ,cont.company_name
      ,cont.site_name
      ,cont.lease_role_type
      ,cont.lease_role
      ,cont.contact_name
      ,addr.address_line1
      ,addr.address_line2
      ,addr.city
      ,addr.county
      ,addr.zip_code postal_code
      ,addr.state
      ,addr.province
      ,addr.country
      ,cont.status
      ,cont.contact_assignment_id
      ,cont.company_id
      ,cont.company_site_id
      ,cont.lease_id
      ,cont.lease_change_id
      ,cont.location_id
      ,cont.address_id
      ,cont.org_id ';
   --
      n_loc :=103;
   --
   g_fea_rec.delete;
   --
   --print_log('@ 1.'||n_loc);
   g_fea_rec(1).cont_role_feature  :=p_feature; --''LL Notice To:' 
   g_fea_rec(1).table_name         :=p_table; --'XXCUS.HDS_PN_LFT_VALUE'
   g_fea_rec(1).flexfield_exists   :=p_dff_exists; --'Y'; --'N';   
   --
   if g_fea_rec.count >0 then 
    --
    n_loc :=104;
    --
    --print_log('@'||n_loc);
    --
    for idx1 in 1 .. g_fea_rec.count loop 
     --
     n_loc :=105;
     --
     --print_log('@'||n_loc);
     --     
     g_feature :=g_fea_rec(idx1).cont_role_feature;
     --
     --print_log('g_feature ='||g_feature);
     --
      if g_fea_rec(idx1).flexfield_exists ='Y' then
       --
       n_loc :=106;
       --
       --print_log('@'||n_loc);
       --       
       begin 
          --
          for rec in get_dff (p_alias =>'Cont', p_feature_lookup_meaning =>g_fea_rec(idx1).cont_role_feature) loop
           -- 
            n_loc :=107;
           --
            --print_log('@'||n_loc);
           --  
           v_dff_hdr :=v_dff_hdr||v_cr||'  '||rec.dff_hdr;
           v_dff_dtl :=v_dff_dtl||v_cr||'  '||rec.dff_dtl;
          --
          end loop;
          --
      exception
       when others then
        print_log('@1001, msg ='||sqlerrm);
       --
      end; 
       --
      end if;
     --
     v_final_sql :=Null;
     --   
       --print_log('@108');
       v_final_sql :=v_final_sql
                  ||v_sql_hdr1
                  ||' '
                  ||g_fea_rec(idx1).table_name
                  ||v_sql_hdr2
                  ||case when g_fea_rec(idx1).flexfield_exists ='Y' then v_dff_hdr||v_cr||' )' else  ' ) '||v_cr end
                  ||v_sql_dtl
                  ||case when g_fea_rec(idx1).flexfield_exists ='Y' then v_dff_dtl||v_cr else v_cr end
                  ||v_sql_tbl
                  ||'and cont.lease_role  ='||''''||g_feature||''''
                  ||v_cr
                  ||' )';
     --
      print_log(' ');
      print_log('@'||g_feature||', SQL Text:');
      print_log('============================');
      print_log(v_final_sql);      
     --
     print_log('Before calling dynamic sql, insert of records for lease '||p_lease_num||', lease_id ='||p_lease_id||', contact_role ='||g_feature);
     execute immediate v_final_sql;
     print_log('After calling dynamic sql, insert of records for lease '||p_lease_num||', lease_id ='||p_lease_id||', contact_role ='||g_feature||', records inserted ='||sql%rowcount);
      
     commit;    
    --
    end loop; --for idx1 in 1 .. g_fea_rec.count loop  
   --
   else
    print_log('@Insert_Lease_Cont_Roles, plsql table is empty for lease_id '||p_lease_id);
   end if;
   --
  exception  
   when others then
    print_log ('@Insert_Lease_Cont_Roles -When Others, p_lease_id ='||p_lease_id||', msg ='||sqlerrm);
  end;
  --
  commit;
  --
 exception
  when others then
   print_log ('Outer block, @Insert_Lease_Cont_Roles, p_lease_id ='||p_lease_id||', msg ='||sqlerrm);
 end Insert_Lease_Cont_Roles; 
 --
 procedure extract_lea_cont_roles is
  cursor c_fea is
    select a.lease_id                    lease_id
          ,a.rer_id                      lease_num
          ,a.le_loc_id                   primary_loc_id
          ,a.primary_loc                 primary_loc_code
          ,b.table_name                  le_cont_dff_tbl_name
          ,b.flexfield_exists            le_cont_dff_exists
          ,c.lease_role                  lease_role      
    from   xxcus.xxcus_opn_pm_web_extract_b   a
          ,xxcus.xxcus_hds_pn_lookup_objects  b
          ,pn_contact_assignments_v           c
    where  1 =1
     --and   a.lease_id           =1749 
     and   b.lookup_type        ='LEASE_CONT_ROLES_ASGN'
     and   b.table_description  =c.lease_role
     and   c.lease_id(+)        =a.lease_id  
    group by a.lease_id
          ,a.rer_id 
          ,a.le_loc_id
          ,a.primary_loc 
          ,b.table_name
          ,b.flexfield_exists
          ,c.lease_role       
    order by a.lease_id asc;
  --
  type c_fea_tbl is table of c_fea%rowtype index by binary_integer;
  c_fea_rec c_fea_tbl; 
  --
  v_final_sql varchar2(2000);
  --  
  --  
 begin
  --   
  open  c_fea;
  fetch c_fea bulk collect into c_fea_rec;
  close c_fea;
  --
  print_log('Contact_Roles.Total_Records ='||c_fea_rec.count);
  if c_fea_rec.count >0 then  
  nULL;
  --
   for idx in 1 .. c_fea_rec.count loop 
   --
    begin 
    --
     --Null;
     Insert_Lease_Cont_Roles 
      (
       p_loc_id      =>c_fea_rec(idx).primary_loc_id 
      ,p_loc_code    =>c_fea_rec(idx).primary_loc_code
      ,p_lease_id    =>c_fea_rec(idx).lease_id
      ,p_lease_num   =>c_fea_rec(idx).lease_num
      ,p_feature     =>c_fea_rec(idx).lease_role --'LL Notice To:'
      ,p_table       =>c_fea_rec(idx).le_cont_dff_tbl_name --'XXCUS.HDS_LE_CONT_ROLE_LLN' 
      ,p_dff_exists  =>c_fea_rec(idx).le_cont_dff_exists --'Y'  
      );   
    -- 
    exception
     when others then
      print_log('Issue in calling Insert_Lease_Cont_Roles, msg ='||sqlerrm);
    end;
   --
   end loop; 
  --
  end if;
  --
 exception
  when others then
   print_log('@extract_lea_cont_roles, message ='||sqlerrm);
   rollback;
 end extract_lea_cont_roles;
 --     
 procedure fetch_rights_ref_comments (p_lease_id in number, p_right_type_code in varchar2, p_ref out varchar2, p_comments out varchar2) is
 begin
    select right_reference
          ,right_comments
    into   p_ref
          ,p_comments 
    from   pn_rights
    where  1 =1
      and  lease_id =p_lease_id
      and  right_type_code =p_right_type_code --IN ('VPD', 'SECDP', 'SURREQ', 'HLDOVR', 'SUBASGN')
      and  rowid =
        (
          select max(rowid)
          from   pn_rights
          where  1 =1
            and  lease_id =p_lease_id
            and  right_type_code =p_right_type_code
        );
 exception
  when no_data_found then
   p_ref      :=Null;
   p_comments :=Null;
  when too_many_rows then
   print_log ('@fetch_rights_ref_comments, lease_id ='||p_lease_id||', right_type_code ='||p_right_type_code);  
   print_log ('@fetch_rights_ref_comments, when too_many_rows exception,  -'||sqlerrm);  
   p_ref      :=Null;
   p_comments :=Null;   
  when others then
   print_log ('@fetch_rights_ref_comments, lease_id ='||p_lease_id||', right_type_code ='||p_right_type_code);  
   print_log ('@fetch_rights_ref_comments, when others exception,  -'||sqlerrm);
   p_ref      :=Null;
   p_comments :=Null;  
 end fetch_rights_ref_comments;
    
 function xxcus_opn_get_hds_entities (p_lease_id in number, p_lease_type in varchar2) return varchar2 as
     v_non_legal_entity  varchar2(2000) :='';
     v_legal_entity      varchar2(2000) :='';
     n_lease_id          number; 
  begin
     if p_lease_type='LSEE' then --Expense lease then
      --
      begin 
        select ndet.text
              ,nhdr.lease_id
        into   v_legal_entity
              ,n_lease_id
        from   pn_note_details ndet
              ,pn_note_headers nhdr
        where 1 =1
          and  nhdr.lease_id              =p_lease_id
          and  ndet.note_header_id        =nhdr.note_header_id
          and  nhdr.note_type_lookup_code ='TCUR'
          and  ndet.last_update_date =
           (
             select max(last_update_date)
             from   pn_note_details 
             where  1 =1
               and  note_header_id =ndet.note_header_id
           )
          ; --Tenant Current Entity  
      exception
       when no_data_found then
          v_legal_entity :='NONE';
          n_lease_id     :=to_number(null);
       when others then
          v_legal_entity :='NA';
          n_lease_id     :=to_number(null);
      end;
      --
      if v_legal_entity ='NONE' then 
          --
          begin 
            select ndet.text
                  ,nhdr.lease_id
            into   v_legal_entity
                  ,n_lease_id
            from   pn_note_details ndet
                  ,pn_note_headers nhdr
            where 1 =1
              and  nhdr.lease_id              =p_lease_id
              and  ndet.note_header_id        =nhdr.note_header_id
              and  nhdr.note_type_lookup_code ='TORIG'
              and  ndet.last_update_date =
               (
                 select max(last_update_date)
                 from   pn_note_details 
                 where  1 =1
                   and  note_header_id =ndet.note_header_id
               )
              ; --Tenant Original Entity  
          exception
           when no_data_found then
            v_legal_entity :='NONE';
            n_lease_id     :=to_number(null);
           when others then
            v_legal_entity :='NA';
            n_lease_id     :=to_number(null);
          end; 
          --  
      else
       Null; 
       --What this means is we have found the hds legal entity value based on the tenant current entity [TCUR] OR 
       --some sort of when other exception happened and the value is set to NA
      end if; 
      --
      -- The below logic will fetch the Non HDS Legal Entity lease option information 
      --  
      begin 
        select ndet.text
              ,nhdr.lease_id
        into   v_non_legal_entity
              ,n_lease_id
        from   pn_note_details ndet
              ,pn_note_headers nhdr
        where 1 =1
          and  nhdr.lease_id              =p_lease_id
          and  ndet.note_header_id        =nhdr.note_header_id
          and  nhdr.note_type_lookup_code ='LLCUR'
          and  ndet.last_update_date =
           (
             select max(last_update_date)
             from   pn_note_details 
             where  1 =1
               and  note_header_id =ndet.note_header_id
           )          
          ; --Landlord Current Entity  
      exception
       when no_data_found then
        v_non_legal_entity :='NONE';
        n_lease_id     :=to_number(null);
       when others then
        v_non_legal_entity :='NA';
        n_lease_id     :=to_number(null);
      end; 
      -- 
      if v_non_legal_entity ='NONE' then 
      --
          begin 
            select ndet.text
                  ,nhdr.lease_id
            into   v_non_legal_entity
                  ,n_lease_id
            from   pn_note_details ndet
                  ,pn_note_headers nhdr
            where 1 =1
              and  nhdr.lease_id              =p_lease_id
              and  ndet.note_header_id        =nhdr.note_header_id
              and  nhdr.note_type_lookup_code ='LLORIG'
              and  ndet.last_update_date =
               (
                 select max(last_update_date)
                 from   pn_note_details 
                 where  1 =1
                   and  note_header_id =ndet.note_header_id
               )              
              ; --Landlord Original Entity  
          exception
           when no_data_found then
            v_non_legal_entity :='NONE';
            n_lease_id     :=to_number(null);
           when others then
            v_non_legal_entity :='NA';
            n_lease_id     :=to_number(null);
          end;
       --  
      else
       Null; 
       --What this means is we have found the hds legal entity value based on the tenant current entity [TCUR] OR 
       --some sort of when other exception happened and the value is set to NA
      end if;    
      --     
     elsif p_lease_type ='OWNO' then --Owned
      --
      begin 
        select ndet.text
              ,nhdr.lease_id
        into   v_legal_entity
              ,n_lease_id
        from   pn_note_details ndet
              ,pn_note_headers nhdr
        where 1 =1
          and  nhdr.lease_id              =p_lease_id
          and  ndet.note_header_id        =nhdr.note_header_id
          and  nhdr.note_type_lookup_code ='OCUR'
          and  ndet.last_update_date =
           (
             select max(last_update_date)
             from   pn_note_details 
             where  1 =1
               and  note_header_id =ndet.note_header_id
           )          
          ; --Tenant Current Entity  
      exception
       when no_data_found then
          v_legal_entity :='NONE';
          n_lease_id     :=to_number(null);
       when others then
          v_legal_entity :='NA';
          n_lease_id     :=to_number(null);
      end;
      --
      if v_legal_entity ='NONE' then 
          --
          begin 
            select ndet.text
                  ,nhdr.lease_id
            into   v_legal_entity
                  ,n_lease_id
            from   pn_note_details ndet
                  ,pn_note_headers nhdr
            where 1 =1
              and  nhdr.lease_id              =p_lease_id
              and  ndet.note_header_id        =nhdr.note_header_id
              and  nhdr.note_type_lookup_code ='OORIG'
              and  ndet.last_update_date =
               (
                 select max(last_update_date)
                 from   pn_note_details 
                 where  1 =1
                   and  note_header_id =ndet.note_header_id
               )              
              ; --Tenant Original Entity  
          exception
           when no_data_found then
            v_legal_entity :='NONE';
            n_lease_id     :=to_number(null);
           when others then
            v_legal_entity :='NA';
            n_lease_id     :=to_number(null);
          end; 
          --  
      else
       Null; 
       --What this means is we have found the hds legal entity value based on the owned current entity [OCUR] OR 
       --some sort of when other exception happened and the value is set to NA
      end if; 
      --
      v_non_legal_entity :='NONE'; 
      --
     elsif p_lease_type IN ('LSE', 'ISUB') then --Income Lease and Income Sublease
      --
      begin 
        select ndet.text
              ,nhdr.lease_id
        into   v_legal_entity
              ,n_lease_id
        from   pn_note_details ndet
              ,pn_note_headers nhdr
        where 1 =1
          and  nhdr.lease_id              =p_lease_id
          and  ndet.note_header_id        =nhdr.note_header_id
          and  nhdr.note_type_lookup_code ='LLCUR'
          and  ndet.last_update_date =
           (
             select max(last_update_date)
             from   pn_note_details 
             where  1 =1
               and  note_header_id =ndet.note_header_id
           )          
          ; --Tenant Current Entity  
      exception
       when no_data_found then
          v_legal_entity :='NONE';
          n_lease_id     :=to_number(null);
       when others then
          v_legal_entity :='NA';
          n_lease_id     :=to_number(null);
      end;
      --
      if v_legal_entity ='NONE' then 
          --
          begin 
            select ndet.text
                  ,nhdr.lease_id
            into   v_legal_entity
                  ,n_lease_id
            from   pn_note_details ndet
                  ,pn_note_headers nhdr
            where 1 =1
              and  nhdr.lease_id              =p_lease_id
              and  ndet.note_header_id        =nhdr.note_header_id
              and  nhdr.note_type_lookup_code ='LLORIG'
              and  ndet.last_update_date =
               (
                 select max(last_update_date)
                 from   pn_note_details 
                 where  1 =1
                   and  note_header_id =ndet.note_header_id
               )              
              ; --Tenant Original Entity  
          exception
           when no_data_found then
            v_legal_entity :='NONE';
            n_lease_id     :=to_number(null);
           when others then
            v_legal_entity :='NA';
            n_lease_id     :=to_number(null);
          end; 
          --  
      else
       Null; 
       --What this means is we have found the hds legal entity value based on the tenant current entity [TCUR] OR 
       --some sort of when other exception happened and the value is set to NA
      end if;  
      --  
      begin 
        select ndet.text
              ,nhdr.lease_id
        into   v_non_legal_entity
              ,n_lease_id
        from   pn_note_details ndet
              ,pn_note_headers nhdr
        where 1 =1
          and  nhdr.lease_id              =p_lease_id
          and  ndet.note_header_id        =nhdr.note_header_id
          and  nhdr.note_type_lookup_code ='TCUR'
          and  ndet.last_update_date =
           (
             select max(last_update_date)
             from   pn_note_details 
             where  1 =1
               and  note_header_id =ndet.note_header_id
           )          
          ; --Landlord Current Entity  
      exception
       when no_data_found then
        v_non_legal_entity :='NONE';
        n_lease_id     :=to_number(null);
       when others then
        v_non_legal_entity :='NA';
        n_lease_id     :=to_number(null);
      end; 
      -- 
      if v_non_legal_entity ='NONE' then 
      --
          begin 
            select ndet.text
                  ,nhdr.lease_id
            into   v_non_legal_entity
                  ,n_lease_id
            from   pn_note_details ndet
                  ,pn_note_headers nhdr
            where 1 =1
              and  nhdr.lease_id              =p_lease_id
              and  ndet.note_header_id        =nhdr.note_header_id
              and  nhdr.note_type_lookup_code ='TORIG'
              and  ndet.last_update_date =
               (
                 select max(last_update_date)
                 from   pn_note_details 
                 where  1 =1
                   and  note_header_id =ndet.note_header_id
               )              
              ; --Landlord Original Entity  
          exception
           when no_data_found then
            v_non_legal_entity :='NONE';
            n_lease_id     :=to_number(null);
           when others then
            v_non_legal_entity :='NA';
            n_lease_id     :=to_number(null);
          end;
       --  
      else
       Null; 
       --What this means is we have found the hds legal entity value based on the tenant current entity [TCUR] OR 
       --some sort of when other exception happened and the value is set to NA
      end if;    
      --
     elsif p_lease_type IN ('NON', 'NONL', 'ISA') then --Non Leased, License Agreement and Internal Sharing Agreement
      v_legal_entity     :='NONE';
      v_non_legal_entity :='NONE'; 
      n_lease_id         :=to_number(null); 
      --
     else
      v_legal_entity     :=Null;
      v_non_legal_entity :=Null; 
      n_lease_id         :=to_number(null);
      --
     end if;
     --
     return v_legal_entity||'|'||v_non_legal_entity;
     -- 
  exception
     when others then
      fnd_file.put_line(fnd_file.log, 'Outer block, xxcus_opn_get_hds_entities -'||sqlerrm);
      return v_legal_entity||'|'||v_non_legal_entity; 
  end xxcus_opn_get_hds_entities;    
  --              
  function beforereport return boolean as  
     cursor lease (p_lease_term_date in date, p_req_id in number) is 
        select 
          le.lease_num RER_ID
         ,case
          when pnaddr.country <>'US' then pnaddr.province
          else pnaddr.state
         end ||'_'||le.name RER_NAME               
        ,pnl.location_type_lookup_code location_type
        ,locdir.fru           fru
        ,pnl.attribute2       latitude
        ,pnl.attribute1       longitude
        ,pnaddr.address_line1 primary_loc_address_line1
        ,pnaddr.city          city
         ,case
           when pnaddr.country <>'US' then pnaddr.province
           else pnaddr.state
          end                 state_province
        ,pnaddr.zip_code      postal_code
        ,pnaddr.county        county
        ,pnaddr.country       country
        ,look1.meaning        rer_type
        ,look2.meaning        rer_status 
        ,look3.description    pmweb_status
        ,ten.bldg_total_sqft  bldg_total_sqft
        ,ten.land_total_sqft  land_total_sqft
        ,(
         select a.description  
         from   pn_location_features_all a
         where  1 =1
           and  a.location_id =le.location_id
           and  a.location_feature_lookup_code ='YEAR BLT'
           and  a.rowid =
                (
                    select max(rowid)
                    from   pn_location_features_all
                    where  1 =1
                      and  location_id =a.location_id
                      and  location_feature_lookup_code ='YEAR BLT'   
                )      
        )                      year_built
        ,case
          when to_char(trunc(ldet.lease_termination_date), 'DD-MON-YYYY') ='31-DEC-4712' then to_date(null) 
          else trunc(ldet.lease_termination_date)
         end  lease_expiration_date
        --,to_char(trunc(ldet.lease_termination_date), 'DD-Mon-YYYY')   lease_expiration_date
        ,rem_user.description   rem_name
        ,rem_user.user_name     rem_ntid
        ,exp_glcc.segment1      fps  
        ,to_char(null)          bu 
        ,to_char(null)          bu_description       
        ,locdir.lob_branch      lob_branch
        ,exp_glcc.segment2      oracle_id 
        ,pnl.location_code      primary_loc
        ,master_le.lease_num    master_rer_id  
        ,ldet.attribute6        sales_leaseback
        ,ldet.attribute7        guarantor
        ,(
         select b.description  
         from   fnd_flex_value_sets a
               ,fnd_flex_values_vl  b
         where  1 =1
           and  a.flex_value_set_name  ='xxcus_pn_rel_party'
           and  b.flex_value_set_id    =a.flex_value_set_id
           and  b.flex_value           =ldet.attribute3
        )                      related_party
        ,ldet.attribute8        rer_docs_link
        ,pnl.attribute6         loc_docs_link
        ,case
          when ten.bldg_count =0 then ten.partial_count
          else ten.bldg_count   
         end                    bldg_count
        ,Null docrev
        ,Null ablease
        ,Null docs
        ,Null hldovr_rights_ref  
        ,Null hldovr_rights_comments  
        ,Null secdp_rights_ref 
        ,Null secdp_rights_comments  
        ,Null vpd_rights_ref 
        ,Null vpd_rights_comments
        ,Null surreq_rights_ref 
        ,Null surreq_rights_comments
        ,Null subasgn_rights_ref 
        ,Null subasgn_rights_comments        
        --,ldet.attribute3 related_party_code
        ,Null hds_notes_entity
        ,Null non_hds_notes_entity
        ,to_date(Null) options_renew_exer_end
        ,Null options_renew_comments
        ,Null options_renew_reference
        ,to_date(Null) options_term_exer_begins
        ,Null options_term_comments
        ,Null options_term_reference
        ,Null ceiling_clear_height
        ,Null high_pile_storage
        ,Null sprinklers
        ,Null cctv
        ,Null relamp
        ,Null alarms_burlgar
        ,Null alarms_fire
        ,le.parent_lease_id parent_lease_id
        ,le.lease_type_code lease_type_code
        ,le.payment_term_proration_rule le_pmt_term_prule
        ,le.abstracted_by_user abstracted_by_user
        ,le.status le_status
        ,le.lease_class_code lease_class_code
        ,le.lease_status lease_status
        ,exp_glcc.segment3 expense_gl_cost_center
        ,exp_glcc.segment4 expense_gl_account       
        ,le.location_id le_loc_id
        ,pnaddr.address_id pnl_address_id
        ,le.lease_id lease_id      
        ,ldet.lease_detail_id lease_detail_id
        ,ldet.lease_change_id lease_change_id
        ,p_req_id request_id
        from  pn_leases_all le
             ,pn_lease_details_all           ldet
             ,fnd_user                       rem_user
             ,pn_locations_all               pnl
             ,pn_addresses_all               pnaddr
             ,gl_code_combinations_kfv       exp_glcc
             ,xxcus.xxcus_location_code_tbl  locdir
             ,fnd_lookups                    look1
             ,fnd_lookups                    look2 
             ,fnd_lookups                    look3             
             ,pn_leases_all                  master_le 
              ,(
                 select nvl(sum(bldg_rentable_area), 0) bldg_total_sqft
                       ,nvl(sum(land_rentable_area), 0) land_total_sqft
                       ,to_char(count(case
                         when tenancy_loc_type_code ='BUILDING' then 1
                        end)) bldg_count
                      ,case 
                         when (count(case
                         when tenancy_loc_type_code IN ('FLOOR', 'OFFICE') then 1
                        end)) >=1 then 'PORTION'
                        else '' 
                        end partial_count                        
                       ,tenancy_lease_id
                from   apps.xxcus_tenancy_lease_v
                where  1 =1
                group by tenancy_lease_id                  
               ) ten        
        where  1 =1
          and  ldet.lease_id(+)                 =le.lease_id
          and  pnl.location_id(+)               =le.location_id
          and  trunc(sysdate) between pnl.active_start_date(+) and pnl.active_end_date(+)
          and  pnaddr.address_id(+)             =pnl.address_id 
          and  exp_glcc.code_combination_id(+)  =ldet.expense_account_id
          and  ( 
                 (xxcus_opn_pm_web_extract_pkg.p_fps is null and 2 =2) 
                   OR 
                 (xxcus_opn_pm_web_extract_pkg.p_fps is not null and exp_glcc.segment1 =xxcus_opn_pm_web_extract_pkg.p_fps)
               )
          and  ( 
                 (
                       p_lease_term_date is null 
                   and 3 =3
                 ) 
                   OR 
                 (
                      (p_lease_term_date is not null) 
                  and (
                          (ldet.lease_termination_date >p_lease_term_date) and (le.lease_status IN ('TER', 'PCOM')) 
                        OR 
                          (3 =3) and (le.lease_status NOT IN ('TER', 'PCOM'))                  
                      )
                  --and (to_char(ldet.lease_termination_date, 'DD-MM-YYYY') >to_char(p_lease_term_date, 'DD-MM-YYYY') and (le.lease_status NOT IN ('TER', 'PCOM')))                  
                   --Completed and Pending Completion
                 )
               )               
          and  locdir.entrp_loc(+)              =exp_glcc.segment2  
          and  locdir.inactive(+)               ='N'  
          and  look1.lookup_type(+)              ='PN_LEASE_TYPE'
          and  look1.lookup_code(+)              =le.lease_type_code
          and  look2.lookup_type(+)              ='PN_LEASESTATUS_TYPE'
          and  look2.lookup_code(+)              =le.lease_status
          and  look3.lookup_type(+)              ='HDS_OPN_PMW_RERSTATUS'
          and  look3.lookup_code(+)              =le.lease_status            
          and  ten.tenancy_lease_id(+)           =le.lease_id 
          and  rem_user.user_id(+)               =ldet.responsible_user
          and  master_le.lease_id(+)             =le.parent_lease_id;
        --  and  ( 
        --        (nvl(ldet.lease_termination_date, trunc(sysdate)) between '01-JAN-13' and to_date('12/31/4712', 'mm/dd/yyyy'))        
        --       )
     
     type my_lease_tbl is table of lease%rowtype index by binary_integer; 
     my_lease_rec my_lease_tbl; 
     
     -- ========================
      b_move_fwd         boolean :=FALSE;
      n_fetch            number :=0;
      p_limit            number :=1000;
      v_detail_exists    varchar2(1) :=Null;
      n_header_no        number :=0;
      n_total_ytd        number :=0;
      n_total_rebates    number :=0; 
      v_entities         varchar2(4010) :=Null;
      ex_dml_errors      exception;
      PRAGMA EXCEPTION_INIT(ex_dml_errors, -24381);  
      l_error_count      number;   
      v_portion          varchar2(10) :=Null;
     -- ========================
 Begin
   execute immediate 'truncate table xxcus.xxcus_opn_pm_web_extract_b';     
      print_log('');                
      print_log('xxcus_opn_pm_web_extract_pkg.p_fps ='||xxcus_opn_pm_web_extract_pkg.p_fps);      
      print_log('xxcus_opn_pm_web_extract_pkg.p_date ='||xxcus_opn_pm_web_extract_pkg.p_date);
      print_log('');
      
      open lease (trunc(apps.fnd_conc_date.string_to_date(xxcus_opn_pm_web_extract_pkg.p_date)), fnd_global.conc_request_id);
      --
      loop 
       --
       fetch lease bulk collect into my_lease_rec limit p_limit;
       n_fetch :=n_fetch +1;
       exit when my_lease_rec.count =0;
       print_log('@ Data set '||n_fetch||', total_records ='||my_lease_rec.count);
       --
       if my_lease_rec.count >0 then 
       --
        for idx in my_lease_rec.first .. my_lease_rec.last loop 
              -- fetch fru again bcoz the main query did not as it could be inactive
              if my_lease_rec(idx).fru is null then
               begin 
                 select fru
                 into   my_lease_rec(idx).fru
                 from   xxcus.xxcus_location_code_tbl
                 where  1 =1
                   and  entrp_loc =my_lease_rec(idx).oracle_id
                   and  rownum <2;
               exception
                when no_data_found then
                  my_lease_rec(idx).fru :=Null;
                when too_many_rows then
                  print_log ('@fetch fru again, when too_many_rows, lease_detail_id ='||my_lease_rec(idx).lease_detail_id||', lease_id ='||my_lease_rec(idx).lease_id);                
                  my_lease_rec(idx).fru :=Null;
                when others then
                  print_log ('@fetch fru again, when others, lease_detail_id ='||my_lease_rec(idx).lease_detail_id||', lease_id ='||my_lease_rec(idx).lease_id);          
                  print_log ('@fetch fru again, when others OERR =>'||sqlerrm);
                  my_lease_rec(idx).fru :=Null;
               end;
              end if;
              --          
              -- get bldg_total_sqft and land_total_sqft
              if my_lease_rec(idx).lease_status IN ('TER', 'PCOM') then
                v_portion :=Null;
                  begin 
                    select 
                           sum(case
                             when ten_loc.location_type_lookup_code IN ('FLOOR', 'OFFICE') then (ten_loc.rentable_area)
                             when ten_loc.location_type_lookup_code ='BUILDING' then (ten_loc.gross_area)
                             else to_number(null) 
                           end) bldg_rentable_area
                          ,sum(case
                            when ten_loc.location_type_lookup_code ='LAND' then (ten_loc.gross_area)
                            when ten_loc.location_type_lookup_code IN ('SECTION', 'PARCEL') then ten_loc.rentable_area
                            else to_number(null) 
                           end) land_rentable_area
                          ,to_char(count(case
                             when ten_loc.location_type_lookup_code ='BUILDING' then 1
                            end)) bldg_count
                          ,case 
                             when (count(case
                             when ten_loc.location_type_lookup_code IN ('FLOOR', 'OFFICE') then 1
                            end)) >=1 then 'PORTION'
                            else '' 
                            end partial_count
                    into my_lease_rec(idx).bldg_total_sqft
                        ,my_lease_rec(idx).land_total_sqft
                        ,my_lease_rec(idx).bldg_count
                        ,v_portion                                   
                    from pn_tenancies_all ten
                        ,pn_locations_all ten_loc
                    where 1 =1
                    and ten.lease_id =my_lease_rec(idx).lease_id
                    and ten_loc.location_id(+) =ten.location_id
                    and ten_loc.active_end_date =
                         (
                          select max(active_end_date)
                          from   pn_locations_all
                          where  location_id =ten_loc.location_id     
                         ); 
                    --
                      if my_lease_rec(idx).bldg_count ='0' then 
                       my_lease_rec(idx).bldg_count :=v_portion;
                      else
                       null;
                      end if;
                  exception
                   when no_data_found then
                    Null;
                   when others then
                    print_log('@301, '||sqlerrm);
                  end;
              else
                v_portion :=Null;
                  begin 
                    select 
                           nvl(sum(case 
                             when ten_loc.location_type_lookup_code IN ('FLOOR', 'OFFICE') then (ten_loc.rentable_area)
                             when ten_loc.location_type_lookup_code ='BUILDING' then (ten_loc.gross_area)
                             else to_number(null) 
                           end), 0) bldg_total_sqft
                          --
                          ,nvl(sum(case
                            when ten_loc.location_type_lookup_code ='LAND' then (ten_loc.gross_area)
                            when ten_loc.location_type_lookup_code IN ('SECTION', 'PARCEL') then ten_loc.rentable_area
                            else to_number(null) 
                           end), 0) land_total_sqft
                          ,to_char(count(case
                             when ten_loc.location_type_lookup_code ='BUILDING' then 1
                            end)) bldg_count
                          ,case 
                             when (count(case
                             when ten_loc.location_type_lookup_code IN ('FLOOR', 'OFFICE') then 1
                            end)) >=1 then 'PORTION'
                            else '' 
                            end partial_count
                    into my_lease_rec(idx).bldg_total_sqft
                        ,my_lease_rec(idx).land_total_sqft
                        ,my_lease_rec(idx).bldg_count
                        ,v_portion
                    from pn_locations_all ten_loc
                    where 1 =1
                    and ten_loc.location_id(+) =my_lease_rec(idx).le_loc_id
                    and ten_loc.active_end_date =
                           (
                             select max(x.active_end_date)
                             from   pn_locations_all x
                             where  1 =1
                               and  x.location_id =ten_loc.location_id
                           ); 
                      if my_lease_rec(idx).bldg_count ='0' then 
                       my_lease_rec(idx).bldg_count :=v_portion;
                      else
                       null;
                      end if;           
                  exception
                   when no_data_found then
                    Null;
                   when others then
                    print_log('@302, '||sqlerrm);
                  end;              
              end if;              
              --  
           -- If the STATE_PROVINCE field is blank, then we just grab the address info again bcoz 
           -- the location may be inactive or with some status that caused the value missing in first place.          
           if my_lease_rec(idx).state_province is null then
            begin
                select b.address_line1
                      ,b.city
                      ,case
                           when b.country <>'US' then b.province
                           else b.state
                       end
                      ,b.zip_code
                      ,b.county
                      ,b.country
                      ,b.address_id                   --pnl_address_id
                      ,a.attribute6                   --loc_docs_link
                      ,a.location_type_lookup_code    --location_type
                      ,a.attribute2                   --latitude
                      ,a.attribute1                   --longiture
                      ,a.location_code                --primary_loc
                into  my_lease_rec(idx).primary_loc_address_line1
                     ,my_lease_rec(idx).city      
                     ,my_lease_rec(idx).state_province
                     ,my_lease_rec(idx).postal_code
                     ,my_lease_rec(idx).county
                     ,my_lease_rec(idx).country  
                     ,my_lease_rec(idx).pnl_address_id
                     ,my_lease_rec(idx).loc_docs_link
                     ,my_lease_rec(idx).location_type
                     ,my_lease_rec(idx).latitude
                     ,my_lease_rec(idx).longitude
                     ,my_lease_rec(idx).primary_loc                                                                                   
                from pn_locations_all a
                    ,pn_addresses_all b
                where 1 =1
                  and a.location_id =my_lease_rec(idx).le_loc_id
                  and a.active_end_date =
                   (
                     select max(x.active_end_date)
                     from   pn_locations_all x
                     where  1 =1
                       and  x.location_id =a.location_id
                   )
                  and  b.address_id =a.address_id;
                  
                  --
                  my_lease_rec(idx).rer_name :=my_lease_rec(idx).state_province||my_lease_rec(idx).rer_name;
                  --
            exception
              when no_data_found then
               null;
              when too_many_rows then          
                my_lease_rec(idx).primary_loc_address_line1 :='OERR =>MORE THAN 1 ROW';
                print_log('@Feature ceiling clear height, location_id ='||my_lease_rec(idx).le_loc_id||', lease_id ='||my_lease_rec(idx).lease_id);            
              when others then
                print_log ('@rerun get_loc_address, location_id ='||my_lease_rec(idx).le_loc_id||', lease_id ='||my_lease_rec(idx).lease_id);          
                print_log ('@rerun get_loc_address, OERR =>'||sqlerrm);
             end;
            --               
           end if;  --my_lease_rec(idx).state_province is null condition.            
         -- Get Holdover rights reference and comments
          fetch_rights_ref_comments 
            (
              p_lease_id        =>my_lease_rec(idx).lease_id
             ,p_right_type_code =>'HLDOVR'
             ,p_ref             =>my_lease_rec(idx).hldovr_rights_ref
             ,p_comments        =>my_lease_rec(idx).hldovr_rights_comments
            );
         --
         -- Get Security Deposit rights reference and comments
          fetch_rights_ref_comments 
            (
              p_lease_id        =>my_lease_rec(idx).lease_id
             ,p_right_type_code =>'SECDP'
             ,p_ref             =>my_lease_rec(idx).secdp_rights_ref
             ,p_comments        =>my_lease_rec(idx).secdp_rights_comments
            );
         --
         -- Get Sublease rights reference and comments
          fetch_rights_ref_comments 
            (
              p_lease_id        =>my_lease_rec(idx).lease_id
             ,p_right_type_code =>'SUBASGN'
             ,p_ref             =>my_lease_rec(idx).subasgn_rights_ref
             ,p_comments        =>my_lease_rec(idx).subasgn_rights_comments
            );
         --  
         -- Get Surrender Requirements rights reference and comments
          fetch_rights_ref_comments 
            (
              p_lease_id        =>my_lease_rec(idx).lease_id
             ,p_right_type_code =>'SURREQ'
             ,p_ref             =>my_lease_rec(idx).surreq_rights_ref
             ,p_comments        =>my_lease_rec(idx).surreq_rights_comments
            );
         --
         -- Get Vacating Premises Default rights reference and comments
          fetch_rights_ref_comments 
            (
              p_lease_id        =>my_lease_rec(idx).lease_id
             ,p_right_type_code =>'VPD'
             ,p_ref             =>my_lease_rec(idx).vpd_rights_ref
             ,p_comments        =>my_lease_rec(idx).vpd_rights_comments
            );
         --  
         -- DOCREV
         begin 
            select ndet.text 
            into   my_lease_rec(idx).docrev
            from   pn_note_details ndet
                  ,pn_note_headers nhdr
            where 1 =1
              and  nhdr.lease_id =my_lease_rec(idx).lease_id
              and  nhdr.note_type_lookup_code ='DOCREV'
              and  ndet.note_header_id =nhdr.note_header_id  
              and  ndet.note_detail_id =
                    (
                      select max(note_detail_id)
                      from   pn_note_details
                      where  1 =1
                        and  note_header_id =ndet.note_header_id 
                    );           
         exception
          when no_data_found then
            my_lease_rec(idx).docrev :=Null;
          when too_many_rows then          
            my_lease_rec(idx).docrev :='OERR =>MORE THAN 1 ROW';
          when others then
            print_log('@DOCREV, Lease Id ='||my_lease_rec(idx).lease_id);          
            my_lease_rec(idx).docrev :=substr('OERR =>, '||sqlerrm, 1, 2000);
         end;
         --
         --ABLEASE
         begin 
            select ndet.text 
            into   my_lease_rec(idx).ablease
            from   pn_note_details ndet
                  ,pn_note_headers nhdr
            where 1 =1
              and  nhdr.lease_id =my_lease_rec(idx).lease_id
              and  nhdr.note_type_lookup_code ='ABLEASE'
              and  ndet.note_header_id =nhdr.note_header_id  
              and  ndet.note_detail_id =
                    (
                      select max(note_detail_id)
                      from   pn_note_details
                      where  1 =1
                        and  note_header_id =ndet.note_header_id 
                    );           
         exception
          when no_data_found then
            my_lease_rec(idx).ablease :=Null;
          when too_many_rows then          
            my_lease_rec(idx).ablease :='OERR =>MORE THAN 1 ROW';
          when others then
            print_log('@ABLEASE, Lease Id ='||my_lease_rec(idx).lease_id);          
            my_lease_rec(idx).ablease :=substr('OERR =>, '||sqlerrm, 1, 2000);
         end;
         --
         -- DOCS
         begin
            select ndet.text 
            into   my_lease_rec(idx).docs
            from   pn_note_details ndet
                  ,pn_note_headers nhdr
            where 1 =1
              and  nhdr.lease_id =my_lease_rec(idx).lease_id
              and  nhdr.note_type_lookup_code ='DOCS'
              and  ndet.note_header_id =nhdr.note_header_id  
              and  ndet.note_detail_id =
                    (
                      select max(note_detail_id)
                      from   pn_note_details
                      where  1 =1
                        and  note_header_id =ndet.note_header_id 
                    );           
         exception
          when no_data_found then
            my_lease_rec(idx).docs :=Null;
          when too_many_rows then                    
            my_lease_rec(idx).docs :='OERR =>MORE THAN 1 ROW';
          when others then
            print_log('@DOCS, Lease Id ='||my_lease_rec(idx).lease_id);
            my_lease_rec(idx).docs :=substr('OERR =>, '||sqlerrm, 1, 2000);
         end;
         --  
         --HDS and NON HDS Entities
         begin
          v_entities :=Null;
          v_entities :=xxcus_opn_pm_web_extract_pkg.xxcus_opn_get_hds_entities (my_lease_rec(idx).lease_id ,my_lease_rec(idx).lease_type_code);
          my_lease_rec(idx).hds_notes_entity     :=TRIM(SUBSTR(v_entities, 1, instr(v_entities, '|')-1));
          my_lease_rec(idx).non_hds_notes_entity :=TRIM(SUBSTR(v_entities, instr(v_entities, '|')+1));
          /* print_log('lease_id ='||my_lease_rec(idx).lease_id);
          print_log('len for hds entity ='||length(my_lease_rec(idx).hds_notes_entity));
          print_log('len for non hds entity ='||length(my_lease_rec(idx).non_hds_notes_entity)); */
         exception
          when no_data_found then
           Null;
          when too_many_rows then          
           my_lease_rec(idx).hds_notes_entity     :='OERR =>MORE THAN 1 ROW';
           my_lease_rec(idx).non_hds_notes_entity :='OERR =>MORE THAN 1 ROW';
          when others then
           print_log('@HDS_NON_HDS_Entities, Lease Id ='||my_lease_rec(idx).lease_id);
           my_lease_rec(idx).hds_notes_entity     :=substr('OERR =>, '||sqlerrm, 1, 2000);
           my_lease_rec(idx).non_hds_notes_entity :=substr('OERR =>, '||sqlerrm, 1, 2000);          
         end;
         --
         --Options Renewal Details
         begin
            select 
                   case
                     when (to_char(a.option_exer_end_date, 'DD-MON-YYYY') ='31-DEC-4712') then to_date(null)
                     else trunc(a.option_exer_end_date)
                   end 
                  ,a.option_comments
                  ,a.option_reference
            into   my_lease_rec(idx).options_renew_exer_end
                  ,my_lease_rec(idx).options_renew_comments
                  ,my_lease_rec(idx).options_renew_reference
            from   pn_options_all a
            where  1 =1
              and  lease_id                  =my_lease_rec(idx).lease_id
              and  lease_change_id           =my_lease_rec(idx).lease_change_id
              and  option_type_code          ='RENEW'
              and  option_status_lookup_code NOT IN ('EXERCISED', 'NOTEXERCISED', 'CANCEL')
              and  option_exer_end_date =
                    (
                      select min(option_exer_end_date)
                      from   pn_options_all
                      where  1 =1
                        and  lease_id =a.lease_id
                        and  lease_change_id =a.lease_change_id                         
                        and  option_type_code ='RENEW'                    
                        and  option_status_lookup_code NOT IN ('EXERCISED', 'NOTEXERCISED', 'CANCEL')          
                    );           
         exception
          when no_data_found then
            Null;
          when too_many_rows then 
            my_lease_rec(idx).options_renew_exer_end   :=Null;
            my_lease_rec(idx).options_renew_comments   :='OERR =>MORE THAN 1 ROW';
            my_lease_rec(idx).options_renew_reference  :='OERR, 2MANYROWS';
          when others then
            print_log('@Options renewal details, Lease Id ='||my_lease_rec(idx).lease_id);
            my_lease_rec(idx).options_renew_exer_end   :=Null;
            my_lease_rec(idx).options_renew_comments   :=substr('OERR =>, '||sqlerrm, 1, 2000);
            my_lease_rec(idx).options_renew_reference  :='OERR, OTHERS';
         end;
         -- 
         -- Options Termination Details
         begin 
            select 
                   case
                    when (to_char(a.option_exer_start_date, 'DD-MON-YYYY') ='31-DEC-4712') then to_date(null)
                    else trunc(a.option_exer_start_date)
                   end 
                  ,a.option_comments
                  ,a.option_reference
            into   my_lease_rec(idx).options_term_exer_begins
                  ,my_lease_rec(idx).options_term_comments
                  ,my_lease_rec(idx).options_term_reference
            from   pn_options_all a
            where  1 =1 
              and  lease_id                  =my_lease_rec(idx).lease_id
              and  lease_change_id           =my_lease_rec(idx).lease_change_id            
              and  option_type_code          ='TERM'
              and  option_status_lookup_code NOT IN ('EXERCISED', 'NOTEXERCISED', 'CANCEL')
              and  option_exer_start_date =
                    (
                      select min(option_exer_start_date)
                      from   pn_options_all
                      where  1 =1
                        and  lease_id =a.lease_id 
                        and  lease_change_id =a.lease_change_id
                        and  option_type_code ='TERM'
                        and  option_status_lookup_code NOT IN ('EXERCISED', 'NOTEXERCISED', 'CANCEL')          
                    );          
         exception
          when no_data_found then
           Null;
          when too_many_rows then          
           my_lease_rec(idx).options_term_exer_begins :=Null;
           my_lease_rec(idx).options_term_comments    :='OERR =>MORE THAN 1 ROW';
           my_lease_rec(idx).options_term_reference   :='OERR, 2MANYROWS';     
          when others then
           print_log('@Options termination details, Lease Id ='||my_lease_rec(idx).lease_id);          
           my_lease_rec(idx).options_term_exer_begins :=Null;
           my_lease_rec(idx).options_term_comments    :=substr('OERR =>, '||sqlerrm, 1, 2000);
           my_lease_rec(idx).options_term_reference   :='OERR, OTHERS';          
         end;
         --   
         -- Location Feature - CEILING CLEAR HEIGHT
         begin 
          select  nvl(( select a.description
                     from   pn_location_features_all a
                     where  1 =1  
                       and  a.location_id =my_lease_rec(idx).le_loc_id
                       and  a.location_feature_lookup_code ='CEILH'
                       and  rowid =
                            (
                                select max(rowid)
                                from   pn_location_features_all
                                where  1 =1
                                  and  location_id =a.location_id
                                  and  location_feature_lookup_code ='CEILH'   
                            )            
                   ), '')
          into my_lease_rec(idx).ceiling_clear_height
          from dual;
         exception
          when no_data_found then
           Null;
          when too_many_rows then          
           my_lease_rec(idx).ceiling_clear_height :='OERR =>MORE THAN 1 ROW';
          when others then
           print_log('@Feature ceiling clear height, location_id ='||my_lease_rec(idx).le_loc_id||', lease_id ='||my_lease_rec(idx).lease_id);
           my_lease_rec(idx).ceiling_clear_height :=substr('OERR =>, '||sqlerrm, 1, 240);
         end;
         -- 
         -- Location Feature HIGH PILE STORAGE 
         begin 
          select  nvl(( select a.description
                     from   pn_location_features_all a
                     where  1 =1  
                       and  a.location_id =my_lease_rec(idx).le_loc_id
                       and  a.location_feature_lookup_code ='PILE'
                       and  rowid =
                            (
                                select max(rowid)
                                from   pn_location_features_all
                                where  1 =1
                                  and  location_id =a.location_id
                                  and  location_feature_lookup_code ='PILE'   
                            )            
                   ), '')
          into my_lease_rec(idx).high_pile_storage
          from dual;
         exception
          when no_data_found then
           Null;
          when too_many_rows then          
           my_lease_rec(idx).high_pile_storage :='OERR =>MORE THAN 1 ROW';
          when others then
           print_log('@Feature high_pile_storage, location_id ='||my_lease_rec(idx).le_loc_id||', lease_id ='||my_lease_rec(idx).lease_id);
           my_lease_rec(idx).high_pile_storage :=substr('OERR =>, '||sqlerrm, 1, 240);
         end;
         --
         -- Location Feature SPRINKLERS
         begin 
          select  nvl(( select a.description
                     from   pn_location_features_all a
                     where  1 =1  
                       and  a.location_id =my_lease_rec(idx).le_loc_id
                       and  a.location_feature_lookup_code ='SPRINK'
                       and  rowid =
                            (
                                select max(rowid)
                                from   pn_location_features_all
                                where  1 =1
                                  and  location_id =a.location_id
                                  and  location_feature_lookup_code ='SPRINK'   
                            )            
                   ), '')
          into my_lease_rec(idx).sprinklers
          from dual;
         exception
          when no_data_found then
           Null;
          when too_many_rows then          
           my_lease_rec(idx).sprinklers :='OERR =>MORE THAN 1 ROW';
          when others then
           print_log('@Feature sprinklers, location_id ='||my_lease_rec(idx).le_loc_id||', lease_id ='||my_lease_rec(idx).lease_id);
           my_lease_rec(idx).sprinklers :=substr('OERR =>, '||sqlerrm, 1, 240);
         end;
         --
         -- Location Feature CCTV
         begin 
          select  nvl(( select 'Y'
                     from   pn_location_features_all a
                     where  1 =1  
                       and  a.location_id =my_lease_rec(idx).le_loc_id
                       and  a.location_feature_lookup_code ='CCT'
                       and  rowid =
                            (
                                select max(rowid)
                                from   pn_location_features_all
                                where  1 =1
                                  and  location_id =a.location_id
                                  and  location_feature_lookup_code ='CCT'   
                            )            
                   ), 'N')
          into my_lease_rec(idx).cctv
          from dual;
         exception
          when no_data_found then
           Null;
          when too_many_rows then          
           my_lease_rec(idx).cctv :='OERR =>MORE THAN 1 ROW';
          when others then
           print_log('@Feature CCTV, location_id ='||my_lease_rec(idx).le_loc_id||', lease_id ='||my_lease_rec(idx).lease_id);
           my_lease_rec(idx).cctv :=substr('OERR =>, '||sqlerrm, 1, 240);
         end;
         --
         -- Location Feature RELAMP
         begin 
          select  nvl(( select a.description
                     from   pn_location_features_all a
                     where  1 =1  
                       and  a.location_id =my_lease_rec(idx).le_loc_id
                       and  a.location_feature_lookup_code ='LAMP'
                       and  rowid =
                            (
                                select max(rowid)
                                from   pn_location_features_all
                                where  1 =1
                                  and  location_id =a.location_id
                                  and  location_feature_lookup_code ='LAMP'   
                            )            
                   ), '')
          into my_lease_rec(idx).cctv
          from dual;
         exception
          when no_data_found then
           Null;
          when too_many_rows then          
           my_lease_rec(idx).cctv :='OERR =>MORE THAN 1 ROW';
          when others then
           print_log('@Feature RELAMP, location_id ='||my_lease_rec(idx).le_loc_id||', lease_id ='||my_lease_rec(idx).lease_id);
           my_lease_rec(idx).cctv :=substr('OERR =>, '||sqlerrm, 1, 240);
         end;
         --
         -- Location Feature ALARM BURGLAR
         begin 
          select  nvl(( select a.description
                     from   pn_location_features_all a
                     where  1 =1  
                       and  a.location_id =my_lease_rec(idx).le_loc_id
                       and  a.location_feature_lookup_code ='ALARMB'
                       and  rowid =
                            (
                                select max(rowid)
                                from   pn_location_features_all
                                where  1 =1
                                  and  location_id =a.location_id
                                  and  location_feature_lookup_code ='ALARMB'   
                            )            
                   ), '')
          into my_lease_rec(idx).alarms_burlgar
          from dual;
         exception
          when no_data_found then
           Null;
          when too_many_rows then          
           my_lease_rec(idx).alarms_burlgar :='OERR =>MORE THAN 1 ROW';
          when others then
           print_log('@Feature ALARMS BURGLAR, location_id ='||my_lease_rec(idx).le_loc_id||', lease_id ='||my_lease_rec(idx).lease_id);
           my_lease_rec(idx).alarms_burlgar :=substr('OERR =>, '||sqlerrm, 1, 240);
         end;
         --
         -- Location Feature ALARM BURGLAR
         begin 
          select  nvl(( select a.description
                     from   pn_location_features_all a
                     where  1 =1  
                       and  a.location_id =my_lease_rec(idx).le_loc_id
                       and  a.location_feature_lookup_code ='ALARMF'
                       and  rowid =
                            (
                                select max(rowid)
                                from   pn_location_features_all
                                where  1 =1
                                  and  location_id =a.location_id
                                  and  location_feature_lookup_code ='ALARMF'   
                            )            
                   ), '')
          into my_lease_rec(idx).alarms_fire
          from dual;
         exception
          when no_data_found then
           Null;
          when too_many_rows then          
           my_lease_rec(idx).alarms_fire :='OERR =>MORE THAN 1 ROW';
          when others then
           print_log('@Feature ALARMS FIRE, location_id ='||my_lease_rec(idx).le_loc_id||', lease_id ='||my_lease_rec(idx).lease_id);
           my_lease_rec(idx).alarms_fire :=substr('OERR =>, '||sqlerrm, 1, 240);
         end;
         --                              
        end loop; --end loop for idx in my_lease_rec.first .. my_lease_rec.last loop 
         --Begin to push each data set that has all the data elements fetched so far by bulk inserting them into the custom table
         --      
         begin 
          forall idx in my_lease_rec.first .. my_lease_rec.last --save exceptions
            insert into  xxcus.xxcus_opn_pm_web_extract_b values my_lease_rec(idx);
            print_log('Successfully bulk inserted data set '||n_fetch);
         exception
          when ex_dml_errors then                       
           l_error_count := SQL%BULK_EXCEPTIONS.count;
           print_log ('@ data set '||n_fetch||', number of failures: ' || l_error_count);
              for i in 1 .. l_error_count loop 
                print_log ('Error: ' || i || 
                  ' Error Index: ' || sql%bulk_exceptions(i).error_index ||
                  ' Message: ' || sqlerrm(-sql%bulk_exceptions(i).error_code));
              end loop; 
           when others then
             print_log('Issue in bulk insert @ data set '||n_fetch||', message ='||sqlerrm);
         end;
         --        
       else
       --
        Null; --We are either done with the records or no data returned from the cursor lease
        -- 
       end if;        
      --
      end loop;
      close lease;
      --
      print_log('Before update of BU value.');
      --
      begin 
        merge into xxcus.xxcus_opn_pm_web_extract_b t
        using (
                select flex_value, attribute3, attribute4 
                from   fnd_flex_values 
                where  1 =1
                  and  flex_value_set_id =1014547 
               ) s
        on (t.fps = s.flex_value)
        when matched then update set bu =s.attribute3, bu_description =s.attribute4;       
      exception
       when others then
        print_log('Error in updating BU field, Message ='||sqlerrm);
      end;
      --
      print_log('After update of BU value.');      
      --
      commit;
      --
      return TRUE;
      --
 Exception
     when others then
      print_log('Error in xxcus_opn_pm_web_extract_pkg.beforereport '||sqlerrm);
      return FALSE;
 End beforereport;  
 --
/*
 -- Implementation Date: 04/30/2015
 -- Author: Balaguru Seshadri
 -- Scope: private
 -- Modification History
 -- Date          ESMS           Notes
 -- ----------------------------------------------------------
  04/30/2015  283790       Add new routine rename_file and invoke it in the main routine                                                                                            
 --
 --
*/ 
 procedure rename_file (p_temp_file IN VARCHAR2, p_current_folder IN VARCHAR2) IS
  --
  v_perm_file_name VARCHAR2(150) :='HDS_OPN_PMWEB_EXTRACT.txt';
  v_command        VARCHAR2(2000) :=Null;
  v_result         VARCHAR2(2000) :=Null;  
  --
 BEGIN 
    v_command :='chmod 777'
               ||' '
             ||p_current_folder
             ||'/' 
             ||p_temp_file;  
    --
    -- Make the temp file to 777
    --
    print_log(' ');
    print_log('Begin: Change permission of temp file '||p_temp_file||' to 777');
    --
      select xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
      into   v_result 
      from   dual;     
    -- rename the zip file to the final resting file name [v_final_zip_file ]
    --
    print_log('End: Permission of temp file '||p_temp_file||'changed to 777');
    --
    print_log('Begin: Rename file '||p_temp_file||' to HDS_OPN_PMWEB_EXTRACT.txt');
    --    
     v_command :='mv'
               ||' '
               ||p_current_folder
               ||'/' 
               ||p_temp_file --temp zip file
               ||' '
               ||p_current_folder
               ||'/' 
               ||v_perm_file_name; --temp zip file
    --                    
      select xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
      into   v_result 
      from   dual;
    --
    print_log('End: File rename completed successfully');
    print_log(' ');
      --         
 EXCEPTION
  WHEN OTHERS THEN
   PRINT_LOG('Main: rename_file: message ='||SQLERRM);
   RAISE;
 END;
 --
 procedure main (retcode out varchar2, errbuf out varchar2, p_fps in varchar2, p_date in varchar2) as
   --
   b_result boolean  :=FALSE;
   n_tbl_count        Number :=0;
   v_email            fnd_user.email_address%type :=Null;
   --
   v_cp_long_name     fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;   
   v_path             VARCHAR2(80);
   --
   ln_request_id      NUMBER :=0;
   v_child_requests   VARCHAR2(2);
   --
   output_file_id     UTL_FILE.FILE_TYPE;  
   v_file_name        VARCHAR2(80);
   --
   v_line             VARCHAR2(32767) :=Null; 
   v_header           VARCHAR2(2000) :=Null;
   --
   l_extract_date     DATE :=sysdate;
   l_user_id          NUMBER :=FND_GLOBAL.USER_ID;
   --
  --
   CURSOR PMWEB_EXTRACT (P_REQUEST_ID IN NUMBER) IS
    SELECT
    'RER_ID' ||chr(9)|| 
    'RER_NAME' ||chr(9)|| 
    'LOCATION_TYPE' ||chr(9)|| 
    'FRU' ||chr(9)|| 
    'LATITUDE' ||chr(9)|| 
    'LONGITUDE' ||chr(9)|| 
    'PRIMARY_LOC_ADDRESS_LINE1' ||chr(9)|| 
    'CITY' ||chr(9)|| 
    'STATE_PROVINCE' ||chr(9)|| 
    'POSTAL_CODE' ||chr(9)|| 
    'COUNTY' ||chr(9)|| 
    'COUNTRY' ||chr(9)|| 
    'RER_TYPE' ||chr(9)|| 
    'RER_STATUS' ||chr(9)|| 
    'PMWEB_STATUS' ||chr(9)|| 
    'BLDG_TOTAL_SQFT' ||chr(9)|| 
    'LAND_TOTAL_SQFT' ||chr(9)|| 
    'YEAR_BUILT' ||chr(9)|| 
    'LEASE_EXPIRATION_DATE' ||chr(9)|| 
    'REM_NAME' ||chr(9)|| 
    'REM_NTID' ||chr(9)|| 
    'FPS' ||chr(9)|| 
    'BU' ||chr(9)|| 
    'BU_DESCRIPTION' ||chr(9)|| 
    'LOB_BRANCH' ||chr(9)|| 
    'ORACLE_ID' ||chr(9)|| 
    'PRIMARY_LOC' ||chr(9)|| 
    'MASTER_RER_ID' ||chr(9)|| 
    'SALES_LEASEBACK' ||chr(9)|| 
    'GUARANTOR' ||chr(9)|| 
    'RELATED_PARTY' ||chr(9)|| 
    'RER_DOCS_LINK' ||chr(9)|| 
    'LOC_DOCS_LINK' ||chr(9)|| 
    'BLDG_COUNT' ||chr(9)  FIELD_1, 
    'DOCREV' ||chr(9)|| 
    'ABLEASE' ||chr(9)|| 
    'DOCS' ||chr(9)|| 
    'HLDOVR_RIGHTS_REF' ||chr(9)|| 
    'HLDOVR_RIGHTS_COMMENTS' ||chr(9)|| 
    'SECDP_RIGHTS_REF' ||chr(9)|| 
    'SECDP_RIGHTS_COMMENTS' ||chr(9) FIELD_2, 
    'VPD_RIGHTS_REF' ||chr(9)|| 
    'VPD_RIGHTS_COMMENTS' ||chr(9)|| 
    'SURREQ_RIGHTS_REF' ||chr(9) FIELD_3, 
    'SURREQ_RIGHTS_COMMENTS' ||chr(9)|| 
    'SUBASGN_RIGHTS_REF' ||chr(9)|| 
    'SUBASGN_RIGHTS_COMMENTS' ||chr(9)|| 
    'HDS_NOTES_ENTITY' ||chr(9)|| 
    'NON_HDS_NOTES_ENTITY' ||chr(9) FIELD_4,
    'OPTIONS_RENEW_EXER_END' ||chr(9)|| 
    'OPTIONS_RENEW_COMMENTS' ||chr(9)|| 
    'OPTIONS_RENEW_REFERENCE' ||chr(9)|| 
    'OPTIONS_TERM_EXER_BEGINS' ||chr(9) FIELD_5,
    'OPTIONS_TERM_COMMENTS' ||chr(9)|| 
    'OPTIONS_TERM_REFERENCE' ||chr(9)|| 
    'CEILING_CLEAR_HEIGHT' ||chr(9) FIELD_6,
    'HIGH_PILE_STORAGE' ||chr(9)|| 
    'SPRINKLERS' ||chr(9)|| 
    'CCTV' ||chr(9)|| 
    'RELAMP' ||chr(9)|| 
    'ALARMS_BURLGAR' ||chr(9)|| 
    'ALARMS_FIRE' ||chr(9)|| 
    'PARENT_LEASE_ID' ||chr(9)|| 
    'LEASE_TYPE_CODE' ||chr(9)|| 
    'LE_PMT_TERM_PRULE' ||chr(9)|| 
    'ABSTRACTED_BY_USER' ||chr(9)|| 
    'LE_STATUS' ||chr(9)|| 
    'LEASE_CLASS_CODE' ||chr(9)|| 
    'LEASE_STATUS' ||chr(9)|| 
    'EXPENSE_GL_COST_CENTER' ||chr(9)|| 
    'EXPENSE_GL_ACCOUNT' ||chr(9)|| 
    'LE_LOC_ID' ||chr(9)|| 
    'PNL_ADDRESS_ID' ||chr(9)|| 
    'LEASE_ID' ||chr(9)|| 
    'LEASE_DETAIL_ID' ||chr(9)|| 
    'LEASE_CHANGE_ID' ||chr(9)|| 
    'REQUEST_ID' ||chr(9) FIELD_7 
    FROM DUAL 
   UNION ALL  
   SELECT
        RER_ID||CHR(9)||
        RER_NAME||CHR(9)||
        LOCATION_TYPE||CHR(9)||
        FRU||CHR(9)||
        LATITUDE||CHR(9)||
        LONGITUDE||CHR(9)||
        PRIMARY_LOC_ADDRESS_LINE1||CHR(9)||
        CITY||CHR(9)||
        STATE_PROVINCE||CHR(9)||
        POSTAL_CODE||CHR(9)||
        COUNTY||CHR(9)||
        COUNTRY||CHR(9)||
        RER_TYPE||CHR(9)||
        RER_STATUS||CHR(9)||
        PMWEB_STATUS||CHR(9)||
        BLDG_TOTAL_SQFT||CHR(9)||
        LAND_TOTAL_SQFT||CHR(9)||
        YEAR_BUILT||CHR(9)||
        LEASE_EXPIRATION_DATE||CHR(9)||
        REM_NAME||CHR(9)||
        REM_NTID||CHR(9)||
        FPS||CHR(9)||
        BU||CHR(9)||
        BU_DESCRIPTION||CHR(9)||
        LOB_BRANCH||CHR(9)||
        ORACLE_ID||CHR(9)||
        PRIMARY_LOC||CHR(9)||
        MASTER_RER_ID||CHR(9)||
        SALES_LEASEBACK||CHR(9)||
        GUARANTOR||CHR(9)||
        RELATED_PARTY||CHR(9)||
        RER_DOCS_LINK||CHR(9)||
        LOC_DOCS_LINK||CHR(9)||
        BLDG_COUNT||CHR(9) FIELD_1,
        /*
        DOCREV||CHR(9)||
        ABLEASE||CHR(9)||
        DOCS||CHR(9)||
        HLDOVR_RIGHTS_REF||CHR(9)||
        HLDOVR_RIGHTS_COMMENTS||CHR(9)||
        SECDP_RIGHTS_REF||CHR(9)||
        SECDP_RIGHTS_COMMENTS||CHR(9) FIELD_2,
        VPD_RIGHTS_REF||CHR(9)||
        VPD_RIGHTS_COMMENTS||CHR(9)||
        SURREQ_RIGHTS_REF||CHR(9)||
        SURREQ_RIGHTS_COMMENTS||CHR(9)||
        SUBASGN_RIGHTS_REF||CHR(9) FIELD_3,
        SUBASGN_RIGHTS_COMMENTS||CHR(9)||
        HDS_NOTES_ENTITY||CHR(9)||
        NON_HDS_NOTES_ENTITY||CHR(9) FIELD_4,
        OPTIONS_RENEW_EXER_END||CHR(9)||
        OPTIONS_RENEW_COMMENTS||CHR(9)||
        OPTIONS_RENEW_REFERENCE||CHR(9)||
        OPTIONS_TERM_EXER_BEGINS||CHR(9) FIELD_5, 
        OPTIONS_TERM_COMMENTS||CHR(9)||
        OPTIONS_TERM_REFERENCE||CHR(9)|| 
        */       
        regexp_replace(DOCREV, '([^[:print:]])',' ')||CHR(9)|| -- DOCREV||CHR(9)||
        regexp_replace(ABLEASE, '([^[:print:]])',' ')||CHR(9)|| --ABLEASE||CHR(9)||
        regexp_replace(DOCS, '([^[:print:]])',' ')||CHR(9)|| --DOCS||CHR(9)||
        regexp_replace(HLDOVR_RIGHTS_REF, '([^[:print:]])',' ')||CHR(9)|| -- HLDOVR_RIGHTS_REF||CHR(9)||
        regexp_replace(HLDOVR_RIGHTS_COMMENTS, '([^[:print:]])',' ')||CHR(9)|| --HLDOVR_RIGHTS_COMMENTS||CHR(9)||
        regexp_replace(SECDP_RIGHTS_REF, '([^[:print:]])',' ')||CHR(9)|| --SECDP_RIGHTS_REF||CHR(9)||
        regexp_replace(SECDP_RIGHTS_COMMENTS, '([^[:print:]])',' ')||CHR(9) FIELD_2, --SECDP_RIGHTS_COMMENTS||CHR(9) FIELD_2,
        regexp_replace(VPD_RIGHTS_REF, '([^[:print:]])',' ')||CHR(9)|| --VPD_RIGHTS_REF||CHR(9)||
        regexp_replace(VPD_RIGHTS_COMMENTS, '([^[:print:]])',' ')||CHR(9)|| --VPD_RIGHTS_COMMENTS||CHR(9)||
        regexp_replace(SURREQ_RIGHTS_REF, '([^[:print:]])',' ')||CHR(9)|| --SURREQ_RIGHTS_REF||CHR(9)||
        regexp_replace(SURREQ_RIGHTS_COMMENTS, '([^[:print:]])',' ')||CHR(9)|| --SURREQ_RIGHTS_COMMENTS||CHR(9)||
        regexp_replace(SUBASGN_RIGHTS_REF, '([^[:print:]])',' ')||CHR(9) FIELD_3, --SUBASGN_RIGHTS_REF||CHR(9) FIELD_3,
        regexp_replace(SUBASGN_RIGHTS_COMMENTS, '([^[:print:]])',' ')||CHR(9)|| --SUBASGN_RIGHTS_COMMENTS||CHR(9)||
        regexp_replace(HDS_NOTES_ENTITY, '([^[:print:]])',' ')||CHR(9)|| --HDS_NOTES_ENTITY||CHR(9)||
        regexp_replace(NON_HDS_NOTES_ENTITY, '([^[:print:]])',' ')||CHR(9) FIELD_4, --NON_HDS_NOTES_ENTITY||CHR(9) FIELD_4,
        OPTIONS_RENEW_EXER_END||CHR(9)||
        regexp_replace(OPTIONS_RENEW_COMMENTS, '([^[:print:]])',' ')||CHR(9)|| --OPTIONS_RENEW_COMMENTS||CHR(9)||
        regexp_replace(OPTIONS_RENEW_REFERENCE, '([^[:print:]])',' ')||CHR(9)|| --OPTIONS_RENEW_REFERENCE||CHR(9)||
        OPTIONS_TERM_EXER_BEGINS||CHR(9) FIELD_5, 
        regexp_replace(OPTIONS_TERM_COMMENTS, '([^[:print:]])',' ')||CHR(9)|| --OPTIONS_TERM_COMMENTS||CHR(9)||
        regexp_replace(OPTIONS_TERM_REFERENCE, '([^[:print:]])',' ')||CHR(9)|| --OPTIONS_TERM_REFERENCE||CHR(9)||
        CEILING_CLEAR_HEIGHT||CHR(9) FIELD_6,
        HIGH_PILE_STORAGE||CHR(9)||
        SPRINKLERS||CHR(9)||
        CCTV||CHR(9)||
        RELAMP||CHR(9)||
        ALARMS_BURLGAR||CHR(9)||
        ALARMS_FIRE||CHR(9)||
        PARENT_LEASE_ID||CHR(9)||
        LEASE_TYPE_CODE||CHR(9)||
        LE_PMT_TERM_PRULE||CHR(9)||
        ABSTRACTED_BY_USER||CHR(9)||
        LE_STATUS||CHR(9)||
        LEASE_CLASS_CODE||CHR(9)||
        LEASE_STATUS||CHR(9)||
        EXPENSE_GL_COST_CENTER||CHR(9)||
        EXPENSE_GL_ACCOUNT||CHR(9)||
        LE_LOC_ID||CHR(9)||
        PNL_ADDRESS_ID||CHR(9)||
        LEASE_ID||CHR(9)||
        LEASE_DETAIL_ID||CHR(9)||
        LEASE_CHANGE_ID||CHR(9)||
        REQUEST_ID||CHR(9) FIELD_7
    FROM (
            SELECT *
            FROM   XXCUS.XXCUS_OPN_PM_WEB_EXTRACT_B
            WHERE 1 =1
              AND REQUEST_ID =P_REQUEST_ID 
            ORDER BY COUNTRY, STATE_PROVINCE, CITY, RER_ID    
         );  
  --
 begin 
      print_log('');
      print_log('calling main...');
      print_log('');            
      print_log('Parameters:');
      print_log('===========');                
      print_log('Param2, p_fps ='||p_fps);      
      print_log('Param1, p_date ='||p_date);
      print_log('');
  --      
  xxcus_opn_pm_web_extract_pkg.p_fps  :=p_fps;
  xxcus_opn_pm_web_extract_pkg.p_date :=p_date;
  --
  b_result :=xxcus_opn_pm_web_extract_pkg.beforereport;
  --
  if (b_result) then
   --
   print_log('xxcus_opn_pm_web_extract_pkg.beforereport returned TRUE.'); 
   print_log(' '); --ESMS 283790
   --    
      begin 
       -- 
         select count(1)
         into   n_tbl_count
         from   xxcus.xxcus_opn_pm_web_extract_b
         where  1 =1
           and  request_id =fnd_global.conc_request_id;
       --    
        if n_tbl_count >0 then
         --
          Begin
            --  
            select a.user_concurrent_program_name
                  ,c.email_address
            into   v_cp_long_name
                  ,v_email
            from fnd_amp_requests_v a
                ,fnd_concurrent_programs d
                ,fnd_user c
            where 1 =1
              and a.request_id =fnd_global.conc_request_id
              and a.concurrent_program_id =d.concurrent_program_id
              and a.requested_by =c.user_id;                                                  
            --                                                                                                      
          Exception                         
           When Others Then
               v_cp_long_name :=Null;
               v_email :=Null;
          End;     
         --
         if (v_email is not null) then
          --
            --v_file_name :='HDS_OPN_PMWEB_EXTRACT_'||fnd_global.conc_request_id||'.txt'; --ESMS 283790
            v_file_name :='tmp_opn_pmweb_'||fnd_global.conc_request_id||'.txt'; --ESMS 283790
          --
            --select '/usr/tmp' into v_path from v$database;
            select '/xx_iface/'||lower(name)||'/outbound' into v_path from v$database;
          --
            output_file_id :=utl_file.fopen
                                (
                                   location      =>v_path
                                  ,filename      =>v_file_name
                                  ,open_mode     =>'w'
                                  ,max_linesize  =>32767
                                );
            print_log('After opening file handle for '||v_path||'/'||v_file_name);
          --
            --utl_file.put_line(output_file_id, v_header);
            --print_log('Header record copied into the file '||v_path||'/'||v_file_name);
          --
            for rec in pmweb_extract (p_request_id =>fnd_global.conc_request_id) loop  
             --v_line :=v_line||rec.field_1||rec.field_2||rec.field_3||rec.field_4||rec.field_5||rec.field_6||rec.field_7;
             utl_file.put_line(output_file_id, rec.field_1||rec.field_2||rec.field_3||rec.field_4||rec.field_5||rec.field_6||rec.field_7); 
            end loop; 
            --
            print_log('All PM WEB extract records are copied into the file '||v_path||'/'||v_file_name);
            --              
            utl_file.fclose(output_file_id);
            --                
            print_log('After close of file '||v_path||'/'||v_file_name);
            --          
          --
         end if;
         --
        end if; --n_tbl_count >0 then
       --
      exception
       when others then
        print_log('Issue in fetching total records from table '||sqlerrm);
      end;
     --
     -- ESMS 283790 rename the temp file to a fixed file name
     --
     rename_file (p_temp_file =>v_file_name, p_current_folder =>v_path);
     --
     --
     --
      if n_tbl_count >0 then
         -- 
         print_log('Begin processing lease contact role custom tables');
         --
         begin 
           savepoint start_lease_cont_role;
           delete from  xxcus.xxcus_hds_pn_lookup_objects where lookup_type ='LEASE_CONT_ROLES_ASGN';
             --
             begin 
               b_create :=create_lease_cont_role_tables ('XXCUS.HDS_LE_CONT_ROLE_');
               commit;
                 --
                 begin
                   extract_lea_cont_roles;
                   commit;
                 exception
                  when others then
                   print_log('Failed to insert lease contact role custom tables, msg ='||SQLERRM);
                 end;     
                 --           
             exception
              when others then
               print_log('Failed to drop and recreate lease contact role custom tables, msg ='||SQLERRM);
             end;
             --       
         exception
          when others then
           print_log('Unable to delete Lease Contact Role lookup matrix table');
           rollback to start_lease_cont_role;     
         end;
         --
         print_log('End processing lease contact role custom tables');
         --
      else
       print_log('Unable to populate Lease Contact Role custom tables as variable n_tbl_count returned zero');    
      end if;
     --                                            
  else --NOT (b_result) then
   print_log('xxcus_opn_pm_web_extract_pkg.beforereport returned FALSE.');  
  end if;  --(b_result) then 
  --
 exception
  when others then
   rollback;
   print_log('Error in xxcus_opn_pm_web_extract_pkg.main, message ='||sqlerrm);
 end main;
 --
 function afterreport return boolean as 
 --          
 begin 
 --
   commit;
   return TRUE;
   --
 exception
 --
  when others then
  --
   print_log('Error in xxcus_opn_pm_web_extract_pkg.afterreport '||sqlerrm);
   return FALSE;
   --
 end afterreport;    
  
end xxcus_opn_pm_web_extract_pkg;
/