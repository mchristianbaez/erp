CREATE OR REPLACE PACKAGE BODY APPS.xxcus_process_pbowes_pkg as
   /*************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************

     PURPOSE:   

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ---------------       -------------------------
     1.0        11/16/2012  Balaguru Seshadri     Use the incoming scrubbed pitney bowes address data and update AR location data
                                                  that includes the county and sales_tax_geocode fields.
    2.0        12/16/2013  Balaguru Seshadri     Change logic to update address and geo code info
    3.0       03/07/2014  Balaguru Seshadri     Change logic to update address and geo code info
    4.0      04/06/2014   Balaguru Seshadri     To fix geocode issues, we are going to run geocode slam update
                                               which is basically comment out all address elements except geocode and
                                               as soon as we are done with the geocode sla, uncomment the logic back.                                                                                                 
   ESMS History:
   Ticket#        Date
   -------        -------------------
   194657         03/16/2013 
   237747         03/07/2014   
   242855         04/06/2014 
   237747         04/06/2014                                        
   **************************************************************************/

  procedure print_log(p_message in varchar2) as
  begin 
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  end print_log;
  
  function get_field (v_delimiter in varchar2, n_field_no in number ,v_line_read in varchar2, p_which_line in number ) return varchar2 is
       n_start_field_pos number;
       n_end_field_pos   number;
       v_get_field       varchar2(2000);
  begin
       if n_field_no = 1 then
          n_start_field_pos := 1;
       else
          n_start_field_pos := instr(v_line_read,v_delimiter,1,n_field_no-1)+1;
       end if;
      
       n_end_field_pos   := instr(v_line_read,v_delimiter,1,n_field_no) -1;
       if n_end_field_pos > 0 then
          v_get_field := substr(v_line_read,n_start_field_pos,(n_end_field_pos - n_start_field_pos)+1);
       else
          v_get_field := substr(v_line_read,n_start_field_pos); 
       end if;

       return v_get_field;

  exception
    when others then
     print_log ('Line# ='||p_which_line);
     print_log ('Line ='||v_line_read);
     print_log ('get field: '||sqlerrm);
  end get_field;  

  procedure main
   (
     retcode out varchar2
    ,errbuf  out varchar2
    ,p_file_path in varchar2
    ,p_file_name in varchar2
   ) is   
  -- =================================================================================             
           
  -- local variables   
  -- =================================================================================  
  input_file_id   UTL_FILE.FILE_TYPE;
  line_read       VARCHAR2(2000):= NULL;
  line_count      NUMBER := 1;
  lines_processed NUMBER :=0;
  print_count     NUMBER :=0;
  b_move_fwd      BOOLEAN;
  n_location_id   NUMBER;
  v_unprocessed_flag VARCHAR2(1) :='Z'; 
  -- =================================================================================
  v_address1      xxcus.xxcus_address_validation_b.address_line1%type;
  v_address2      xxcus.xxcus_address_validation_b.address_line2%type;
  v_city          xxcus.xxcus_address_validation_b.city%type;
  v_state         xxcus.xxcus_address_validation_b.state%type;
  v_zip           xxcus.xxcus_address_validation_b.zip%type;
  v_zip4          xxcus.xxcus_address_validation_b.zip4%type;
  v_pb_address1      xxcus.xxcus_address_validation_b.address_line1%type;
  v_pb_address2      xxcus.xxcus_address_validation_b.address_line2%type;
  v_pb_city          xxcus.xxcus_address_validation_b.city%type;
  v_pb_state         xxcus.xxcus_address_validation_b.state%type;
  v_pb_zip           xxcus.xxcus_address_validation_b.zip%type;
  v_pb_zip4          xxcus.xxcus_address_validation_b.zip4%type;  
  v_county        xxcus.xxcus_address_validation_b.county%type;
  v_geocode       xxcus.xxcus_address_validation_b.geocode_override%type;
  v_geo_diff      xxcus.xxcus_address_validation_b.geo_diff%type;
  v_county_diff     xxcus.xxcus_address_validation_b.county_diff%type;
  v_old_county        xxcus.xxcus_address_validation_b.county%type;
  v_old_geocode       xxcus.xxcus_address_validation_b.geocode_override%type;
  v_old_city      xxcus.xxcus_address_validation_b.city%type;
  v_old_state     xxcus.xxcus_address_validation_b.state%type;
  v_old_zip       xxcus.xxcus_address_validation_b.zip%type;       
  -- =================================================================================
  l_location_rec            HZ_LOCATION_V2PUB.LOCATION_REC_TYPE;  
  n_obj_ver_number          NUMBER;
  o_return_status           VARCHAR2 (2000);
  o_msg_count               NUMBER;
  o_msg_data                VARCHAR2 (2000);  
  v_api_message             VARCHAR2 (4000); 
  p_org_id                  NUMBER :=mo_global.get_current_org_id;
  n_count                   NUMBER :=0;
  p_limit                   NUMBER :=5000;
  b_city_y_n boolean :=FALSE;
  b_st_y_nb  boolean :=FALSE;
  b_zip_y_n  boolean :=FALSE;
  b_cnty_y_n boolean :=FALSE;
  b_geo_y_n  boolean :=FALSE; 
  -- =================================================================================
begin  
   print_log ('');
   print_log (' ***** Process PITNEY BOWES inbound address data *****');
   print_log ('');      
   print_log ('Parameters');
   print_log ('======================================');      
   print_log ('  1. p_file_path ='||p_file_path);
   print_log ('  2. p_file_name ='||p_file_name);   
   print_log ('  3. p_org_id ='||p_org_id);
   
   begin 
    execute immediate ('truncate table xxcus.xxcus_address_validation_b');
   exception
    when others then
     print_log ('Issue in cleaning up table xxcus.xxcus_address_validation_b');
     print_log ('Error ='||sqlerrm);     
   end; 
   --
    mo_global.set_policy_context('S', p_org_id);
    print_log ('After set operating unit');   
   --
    input_file_id  := utl_file.fopen(p_file_path, p_file_name, 'R');
    print_log ('After file open.');
   -- 
  loop                           
       begin
        utl_file.get_line(input_file_id, line_read); 
          if (xxcus_process_pbowes_pkg.g_header_found ='Y' and line_count =1) then
             Null; --ignore the header line
             line_count:= line_count+1; --set the line number to the next one in the file             
          else                 
                 begin 
                     
                  Savepoint start1;
                      
                    begin
                   
                      v_old_county  :=Null;
                      v_old_geocode :=Null;
                      n_location_id :=Null;
                      v_county_diff :=Null;
                      v_geo_diff    :=Null;
                      n_obj_ver_number :=0;
                                              
                    -- get all the address related fields extracted from the line sent by pitney bowes 
                        
                      n_location_id :=to_number(get_field(xxcus_process_pbowes_pkg.g_delimiter, 1, line_read, line_count));
                      v_address1    :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 2, line_read, line_count));
                      v_address2    :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 3, line_read, line_count));
                      v_city        :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 4, line_read, line_count));
                      v_state       :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 5, line_read, line_count));
                      v_zip         :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 6, line_read, line_count));
                      v_zip4        :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 7, line_read, line_count));
                      v_county      :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 8, line_read, line_count));
                      v_geocode     :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 9, line_read, line_count));
                      v_pb_address1 :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 10, line_read, line_count));
                      v_pb_address2 :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 11, line_read, line_count));
                      v_pb_city     :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 12, line_read, line_count));
                      v_pb_state    :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 13, line_read, line_count));
                      v_pb_zip      :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 14, line_read, line_count));
                      v_pb_zip4     :=trim(get_field(xxcus_process_pbowes_pkg.g_delimiter, 15, line_read, line_count));
                     
                       Begin 
                         select  nvl(county, ''), nvl(sales_tax_geocode, ''), nvl(city, ''), nvl(state, ''), nvl(postal_code, '')
                         into    v_old_county, v_old_geocode, v_old_city, v_old_state, v_old_zip
                         from    hz_locations
                         where   1 =1
                           and   location_id =n_location_id; 
                       Exception
                        When No_Data_Found then
                         v_old_county :=Null;
                         v_old_geocode :=Null;                        
                        When Others then
                         v_old_county :=Null;
                         v_old_geocode :=Null;                        
                       End;                   
                    exception
                      when others then
                        print_log ('Error in get fields ='||sqlerrm);
                    end;                     
                      
                  Insert Into xxcus.xxcus_address_validation_b
                       (
                         hds_unique_id
                        ,address_line1
                        ,address_line2
                        ,city
                        ,state
                        ,zip
                        ,zip4
                        ,county
                        ,geocode_override 
                        ,pb_address_line1
                        ,pb_address_line2
                        ,pb_city
                        ,pb_state
                        ,pb_zip
                        ,pb_zip4
                        ,old_county
                        ,old_geocode  
                        ,county_diff
                        ,geo_diff        
                       )
                  Values
                       (
                         n_location_id
                        ,v_address1
                        ,v_address2
                        ,v_old_city --v_city
                        ,v_old_state --v_state
                        ,v_old_zip --v_zip
                        ,v_zip4
                        ,v_county
                        ,v_geocode 
                        ,v_pb_address1
                        ,v_pb_address2
                        ,v_pb_city
                        ,v_pb_state
                        ,v_pb_zip
                        ,v_pb_zip4
                        ,v_old_county
                        ,v_old_geocode
                        ,v_county_diff
                        ,v_geo_diff                            
                       );
                       
                  b_move_fwd :=TRUE; --if this happens then we did not have an issue with the insert 
                      
                 exception
                  when others then
                   rollback to start1;
                   b_move_fwd :=FALSE;             
                 end;
                 
                 
                 if (b_move_fwd) then --insert is a success lets move further   
                   --
                    b_city_y_n :=FALSE;
                    b_st_y_nb  :=FALSE;
                    b_zip_y_n  :=FALSE;
                    b_cnty_y_n :=FALSE;
                    b_geo_y_n  :=FALSE;                    
                   --
                   l_location_rec :=null;
                   --                 
                                            
                      --If (v_county Is Null And v_geocode Is Null) Then                                                                   
                      --Else  --Either v_county is not null or V_geocode is not null or both of them not null
                      
                          --we need the latest object version  number for the location id before invoking the hz_location api
                          Begin 
                           Select object_version_number
                           Into   n_obj_ver_number
                           From   Hz_Locations
                           Where  1 =1
                             And  Location_Id =n_location_id;
                          Exception
                           When No_Data_Found Then
                            n_obj_ver_number :=0;
                           When Others Then
                            n_obj_ver_number :=0;
                          End;                      
                                  
                          --                                   
                               l_location_rec.location_id        :=Null;                               
                               --no matter what we need location id to update using the api
                               l_location_rec.location_id        :=n_location_id;
                          --  
                               -- 
                               if v_pb_city is not null and v_old_city is null then
                                l_location_rec.city :=v_pb_city;
                                B_CITY_Y_N :=TRUE;
                               else
                                B_CITY_Y_N :=FALSE;
                               end if;
                               --
                               if v_pb_state is not null and v_old_state is null then
                                l_location_rec.state :=v_pb_state;
                                B_ST_Y_NB:=TRUE;
                               else
                                B_ST_Y_NB:=FALSE;
                               end if;
                               -- 
                               if v_pb_zip is not null and v_old_zip is null then
                                l_location_rec.postal_code :=v_pb_zip;
                                B_ZIP_Y_N :=TRUE;
                               else
                                B_ZIP_Y_N :=FALSE;
                               end if;
                               --
                               --print_log('');
                               --print_log('v_county ='||v_county||', v_old_county ='||v_old_county);
                               --print_log('v_geocode ='||v_geocode||', v_old_geocode ='||v_old_geocode);
                               --print_log('');                               
                               --                                                             
                               -- Pass the incoming county value only when we have some value
                               If v_county Is Not Null Then                               
                                if (v_old_county is not null and (v_old_county <>v_county)) then
                                     l_location_rec.county :=v_county; 
                                     B_CNTY_Y_N :=TRUE;                               
                                else
                                     null;                                
                                end if;                                                                                                   
                               Else
                                B_CNTY_Y_N :=FALSE;
                               End if;                               

                               -- Pass the incoming geocode value only when we have some value  
                               if v_geocode is not null then
                                b_geo_y_n :=TRUE;
                                l_location_rec.sales_tax_geocode :=v_geocode;
                               else
                                b_geo_y_n :=FALSE;
                               end if;                        
                                -- 
                        --uncomment after geocode slam process is over
                        --
                        if --new code
                         (
                              (b_city_y_n)  
                           OR (b_st_y_nb)  
                           OR (b_zip_y_n)  
                           OR (b_cnty_y_n) 
                           OR (b_geo_y_n)  
                         ) then
                           hz_location_v2pub.update_location 
                             (
                               p_init_msg_list           => FND_API.G_TRUE,
                               p_location_rec            => l_location_rec,
                               p_object_version_number   => n_obj_ver_number,
                               x_return_status           => o_return_status,
                               x_msg_count               => o_msg_count,
                               x_msg_data                => o_msg_data
                             ); 
                             --print_log ('API return status ='||o_return_status);                                    
                               v_api_message :=Null;
                                       
                                if o_return_status <>fnd_api.g_ret_sts_success Then --API returned either ERROR or WARNING
                                
                                  for i in 1 .. o_msg_count loop
                                    o_msg_data :=i||') '||substr(fnd_msg_pub.get(p_encoded =>fnd_api.g_false ), 1, 255);
                                    if v_api_message is  null then
                                        v_api_message :=substr(o_msg_data, 1, 4000);                            
                                    else
                                        v_api_message :=substr(v_api_message||', '||o_msg_data, 1 , 4000);                            
                                    end if;
                                   -- 
                                  end loop; 
                                 --                        
                                else --API returned a status of Succcess.                                
                                  v_api_message :='Successfully updated';                                           
                                end if; 
                                        
                                --print_log ('Before updating xxcus_address_validation_b with the API return status');
                                Begin 
                                 --
                                 Update xxcus.xxcus_address_validation_b
                                    set api_status  =o_return_status
                                       ,api_message =substr(v_api_message, 1, 4000) --field max width is 4000 characters 
                                  Where 1 =1
                                    And HDS_Unique_Id =n_location_id; 
                                    
                                    n_count :=n_count +1;
                                    if n_count >p_limit then                                 
                                      commit;
                                      n_count :=0;
                                    else                                 
                                      n_count :=n_count +1;
                                    end if;
                                  --                             
                                Exception
                                 --
                                 When Others Then
                                 --
                                  print_log ('Error in updating xxcus_address_validation_b for location_id ='||n_location_id);
                                 --
                                End; 
                                
                                --print_log ('After updating xxcus_address_validation_b with the API return status');

                                  
                            print_log ('');                        
                        else --new code
                         --
                            Begin 
                             Update xxcus.xxcus_address_validation_b
                                set api_status  =v_unprocessed_flag
                                   ,api_message =substr('Record excluded as none of our requirements are met.', 1, 4000) --field max width is 4000 characters 
                              Where 1 =1
                                And HDS_Unique_Id =n_location_id;                            
                            Exception
                             When Others Then
                              print_log ('@Both county and geocode blank, Location_id ='||n_location_id);                         
                              print_log ('Error ='||sqlerrm);
                            End; 
                         --                         
                        end if; --nwe code
                      --                       
                      --End if;
                                                                   
                 else --insert was not successful, we cannot proceed further
                   Null;                   
                 end if;
                 --                          
             line_count:= line_count+1; --set the line number to the next one in the file
             --
          end if;            
       exception
             when no_data_found then
               exit;
             when others then
               raise_application_error(-20010,'Line: '||line_read);
               raise_application_error(-20010,' Unknown Errors: '||sqlerrm);
               exit;
       end;         
  end loop;
       
    utl_file.fclose(input_file_id);          
    print_log ('After file close.');    
   
exception
     when utl_file.invalid_path then
      raise_application_error(-20010,'File: '||p_file_name||' Invalid Path: '||sqlerrm);
     when utl_file.invalid_mode then
      raise_application_error(-20010,'File: '||p_file_name||' Invalid Mode: '||sqlerrm);
     when utl_file.invalid_operation then
      raise_application_error(-20010,'File: '||p_file_name||' Invalid Operation: '||sqlerrm);
     when utl_file.invalid_filehandle then
      raise_application_error(-20010,'File: '||p_file_name||' Invalid File Handle: '||sqlerrm);
     when utl_file.read_error then
      raise_application_error(-20010,'File: '||p_file_name||' File Read Error: '||sqlerrm);
     when utl_file.internal_error then
      raise_application_error(-20010,'File: '||p_file_name||' File Internal Error: '||sqlerrm);  
     when others then
       print_log ('Error in caller apps.xxcus_process_pbowes_pkg.main ='||sqlerrm);
       rollback;
end main;
  
end xxcus_process_pbowes_pkg;
/
