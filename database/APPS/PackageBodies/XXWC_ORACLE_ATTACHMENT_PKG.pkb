create or replace package body APPS.XXWC_ORACLE_ATTACHMENT_PKG as
  -----------------------------------------------------------------------------
  -- � Copyright 2008, Nancy Pahwa
  -- All Rights Reserved
  --
  -- Name           : XXWC_ORACLE_ATTACHMENT_PKG- Cleanup Orphan attachment records in Oracle EBS
  -- Date Written   : 11-May-2016
  -- Author         : Nancy Pahwa
  --
  -- Modification History:
  --
  -- Version When         Who        Did what
  -- ------- -----------  --------   -----------------------------------------------------
  -- 1.1    11-July-2017  nancypahwa   Initially Created TMS# 20170616-00237
  ---------------------------------------------------------------------------------
  /*PROCEDURE Write_log(p_debug_msg IN VARCHAR2) AS
  BEGIN
    DBMS_OUTPUT.PUT_LINE(p_debug_msg);
    fnd_file.put_line(fnd_file.LOG, p_debug_msg);
  END Write_log;*/
  procedure attachment_wrapper(errbuf           OUT VARCHAR2,
                               retcode          OUT NUMBER,
                               p_entity_name    varchar2,
                               p_from_date      varchar2,
                               p_to_date        varchar2,
                               p_operating_unit varchar2) as
    -----------------------------------------------------------------------------
    -- � Copyright 2008, Nancy Pahwa
    -- All Rights Reserved
    --
    -- Name           : attachment_wrapper - Add data  to staging table
    -- Date Written   : 11-May-2016
    -- Author         : Nancy Pahwa
    --
    -- Modification History:
    --
    -- Version When         Who        Did what
    -- ------- -----------  --------   -----------------------------------------------------
    -- 1.1    11-July-2017  nancypahwa   Initially Created TMS# 20170616-00237
    ---------------------------------------------------------------------------------
    v_RECEIPT_NUMBER AR_CASH_RECEIPTS_ALL.RECEIPT_NUMBER%TYPE;
    v_ACCOUNT_NUMBER HZ_CUST_ACCOUNTS_ALL.ACCOUNT_NUMBER%TYPE;
    v_count          NUMBER;
    v_from_date      date;
    v_to_date        date;
    l_count          number;
    --  i number :=0;
  Begin
    errbuf  := NULL;
    retcode := '0';
    /*  Write_log('p_entity_name ' || p_entity_name);
    Write_log('p_from_date ' || p_from_date);
    Write_log('p_to_date ' || p_to_date);
    Write_log('p_operating_unit ' || p_operating_unit);*/
    -- fnd_file.put_line(fnd_file.log,'p_from_date  '||p_from_date);
    --  fnd_file.put_line(fnd_file.log,'p_to_date  '||p_to_date);
    v_from_date := to_date(p_from_date, 'YYYY/MM/DD HH24:MI:SS');
    -- fnd_file.put_line(fnd_file.log,'v_from_date  '||to_char(v_from_date,'dd-mon-yyyy hh24:mi:ss'));
    v_to_date := to_date(p_to_date, 'YYYY/MM/DD HH24:MI:SS');
    -- fnd_file.put_line(fnd_file.log,'v_to_date  '||to_char(v_to_date,'dd-mon-yyyy hh24:mi:ss'));
    for rec_DETAIL in (select AD.SEQ_NUM SEQ_NUM,
                              DCT.CATEGORY_ID CATEGORY_ID,
                              nvl(DT.DESCRIPTION, 'NULL') DOCUMENT_DESCRIPTION,
                              DAT.DATATYPE_ID DATATYPE_ID,
                              DT.TITLE TITLE,
                              nvl(L.FILE_NAME, 'NULL') FILE_NAME,
                              nvl(D.URL, 'NULL') URL,
                              MSSQL.DOCUMENT_URL MSSQL_URL,
                              AD.ENTITY_NAME ENTITY_NAME,
                              AD.PK1_VALUE PK1_VALUE,
                              AD.PK2_VALUE PK2_VALUE,
                              AD.PK3_VALUE PK3_VALUE,
                              AD.PK4_VALUE PK4_VALUE,
                              AD.PK5_VALUE PK5_VALUE,
                              d.created_by USER_ID,
                              'O' USAGE_TYPE,
                              D.DOCUMENT_ID DOCUMENT_ID,
                              AD.ATTACHED_DOCUMENT_ID ATTACHED_DOCUMENT_ID,
                              nvl(L.FILE_ID, -1) FILE_ID,
                              nvl(D.MEDIA_ID, -1) MEDIA_ID,
                              D.Creation_Date,
                              d.last_update_date
                         FROM FND_DOCUMENT_DATATYPES     DAT,
                              FND_DOCUMENT_ENTITIES_TL   DET,
                              FND_DOCUMENTS_TL           DT,
                              FND_DOCUMENTS              D,
                              FND_DOCUMENT_CATEGORIES_TL DCT,
                              FND_ATTACHED_DOCUMENTS     AD,
                              FND_LOBS                   L,
                              cvw_ebs_fnd_lobs@          SQLATTACH MSSQL
                        WHERE D.DOCUMENT_ID = AD.DOCUMENT_ID
                          AND DT.DOCUMENT_ID = D.DOCUMENT_ID
                          AND DCT.CATEGORY_ID = D.CATEGORY_ID
                          AND D.DATATYPE_ID = DAT.DATATYPE_ID
                          AND AD.ENTITY_NAME = DET.DATA_OBJECT_CODE
                          AND L.FILE_ID(+) = D.MEDIA_ID
                          AND AD.ENTITY_NAME =
                              NVL(p_entity_name, AD.ENTITY_NAME)
                          and d.creation_date between v_from_date and
                              v_to_date
                          AND L.FILE_ID = MSSQL.FILE_ID
                          AND DAT.DATATYPE_ID = 6) loop
      insert into XXWC.XXWC_ORACLE_ATTACHMENT_TBL
        (seq_num,
         category_id,
         document_description,
         datatype_id,
         text,
         file_name,
         url,
         entity_name,
         pk1_value,
         pk2_value,
         pk3_value,
         pk4_value,
         pk5_value,
         media_id,
         user_id,
         usage_type,
         title,
         mssql_url,
         OLD_ATTACHED_DOCUMENT_ID,
         OLD_DOCUMENT_ID,
         OLD_CREATION_DATE,
         OLD_UPDATED_DATE,
         FLAG,
         OLD_FILE_ID,
         fnd_flag)
      values
        (rec_DETAIL.SEQ_NUM,
         rec_DETAIL.CATEGORY_ID,
         rec_DETAIL.DOCUMENT_DESCRIPTION,
         5,
         null,
         rec_DETAIL.FILE_NAME,
         rec_DETAIL.MSSQL_URL,
         rec_DETAIL.ENTITY_NAME,
         rec_DETAIL.PK1_VALUE,
         rec_DETAIL.PK2_VALUE,
         rec_DETAIL.PK3_VALUE,
         rec_DETAIL.PK4_VALUE,
         rec_DETAIL.PK5_VALUE,
         null,
         rec_DETAIL.USER_ID,
         rec_DETAIL.USAGE_TYPE,
         rec_DETAIL.TITLE,
         rec_detail.mssql_url,
         rec_DETAIL.Attached_Document_Id,
         rec_detail.document_id,
         rec_detail.creation_date,
         rec_detail.last_update_date,
         'N',
         rec_detail.file_id,
         'N');
      --   test_prc('inserting of data');
    
    /*   i := i+1;
              IF mod(i, 200) = 0 THEN    -- Commit every 10000 records
                 COMMIT;
              END IF;*/
    END LOOP;
    commit;
    begin
      select count(1) into l_count from XXWC.XXWC_ORACLE_ATTACHMENT_TBL;
      --  test_prc('count' || l_count);
    
      if l_count > 0 then
        add_new_attachment(errbuf, retcode);
      else
        null;
      end if;
    end;
  exception
    when others then
      retcode := '1';
      errbuf  := sqlerrm;
  END;
  procedure add_new_attachment(errbuf OUT VARCHAR2, retcode OUT NUMBER) as
    -----------------------------------------------------------------------------
    -- � Copyright 2008, Nancy Pahwa
    -- All Rights Reserved
    --
    -- Name           : add_new_attachment - delete/insert attachment in oracle with new URL
    -- Date Written   : 11-May-2016
    -- Author         : Nancy Pahwa
    --
    -- Modification History:
    --
    -- Version When         Who        Did what
    -- ------- -----------  --------   -----------------------------------------------------
    -- 1.1    11-July-2017  nancypahwa   Initially Created TMS# 20170616-00237
    ---------------------------------------------------------------------------------
    CURSOR table_with_227_rows_cur IS
      select seq_num,
             category_id,
             document_description,
             datatype_id,
             text,
             file_name,
             url,
             entity_name,
             pk1_value,
             pk2_value,
             pk3_value,
             pk4_value,
             pk5_value,
             media_id,
             user_id,
             usage_type,
             title,
             receipt_number,
             account_number,
             mssql_url,
             OLD_ATTACHED_DOCUMENT_ID,
             OLD_DOCUMENT_ID,
             old_creation_date,
             old_updated_date,
             old_file_id,
             rowid row_id
        from XXWC.XXWC_ORACLE_ATTACHMENT_TBL
       where flag = 'N';
    TYPE table_with_227_rows_aat IS TABLE OF table_with_227_rows_cur%ROWTYPE INDEX BY PLS_INTEGER;
    l_table_with_227_rows table_with_227_rows_aat;
  BEGIN
    errbuf  := NULL;
    retcode := '0';
    -- test_prc('Open cursor');
    OPEN table_with_227_rows_cur;
    LOOP
      FETCH table_with_227_rows_cur BULK COLLECT
        INTO l_table_with_227_rows LIMIT 1000;
      FOR indx IN 1 .. l_table_with_227_rows.COUNT LOOP
        begin
          --   test_prc('Calling delete api');
          fnd_attached_documents3_pkg.delete_row(X_attached_document_id => l_table_with_227_rows(indx)
                                                                           .OLD_ATTACHED_DOCUMENT_ID,
                                                 X_datatype_id          => l_table_with_227_rows(indx)
                                                                           .DATATYPE_ID,
                                                 delete_document_flag   => 'Y');
        exception
          when no_data_found then
            null;
        end;
      
        begin
          --   test_prc('Calling attach api');
          fnd_webattch.add_attachment(seq_num              => l_table_with_227_rows(indx)
                                                              .SEQ_NUM,
                                      category_id          => l_table_with_227_rows(indx)
                                                              .CATEGORY_ID,
                                      document_description => l_table_with_227_rows(indx)
                                                              .DOCUMENT_DESCRIPTION,
                                      datatype_id          => 5,
                                      text                 => NULL,
                                      file_name            => l_table_with_227_rows(indx)
                                                              .FILE_NAME,
                                      url                  => l_table_with_227_rows(indx)
                                                              .MSSQL_URL,
                                      function_name        => null,
                                      entity_name          => l_table_with_227_rows(indx)
                                                              .ENTITY_NAME,
                                      pk1_value            => l_table_with_227_rows(indx)
                                                              .PK1_VALUE,
                                      pk2_value            => l_table_with_227_rows(indx)
                                                              .PK2_VALUE,
                                      pk3_value            => l_table_with_227_rows(indx)
                                                              .PK3_VALUE,
                                      pk4_value            => l_table_with_227_rows(indx)
                                                              .PK4_VALUE,
                                      pk5_value            => l_table_with_227_rows(indx)
                                                              .PK5_VALUE,
                                      media_id             => NULL,
                                      user_id              => l_table_with_227_rows(indx)
                                                              .USER_ID,
                                      usage_type           => l_table_with_227_rows(indx)
                                                              .USAGE_TYPE,
                                      title                => l_table_with_227_rows(indx)
                                                              .TITLE);
        exception
          when no_data_found then
            null;
        end;
      END LOOP;
      --   test_prc('before for all');
      forall indx IN 1 .. l_table_with_227_rows.COUNT
        update XXWC.XXWC_ORACLE_ATTACHMENT_TBL t
           set flag = DECODE(flag, 'N', 'Y', flag)
         where ROWID = l_table_with_227_rows(indx).row_id;
      --  test_prc('number of rows updated' || sql%rowcount);
      commit;
      l_table_with_227_rows.delete;
      EXIT WHEN table_with_227_rows_cur%NOTFOUND; /* cause of missing rows */
    END LOOP;
    CLOSE table_with_227_rows_cur;
    add_data_to_archive(errbuf, retcode);
  exception
    when others then
      retcode := '1';
      errbuf  := sqlerrm;
  END;
  procedure add_data_to_archive(errbuf OUT VARCHAR2, retcode OUT NUMBER) as
    -----------------------------------------------------------------------------
    -- � Copyright 2008, Nancy Pahwa
    -- All Rights Reserved
    --
    -- Name           : add_data_to_archive - add data to archive table and truncate
    -- Date Written   : 11-May-2016
    -- Author         : Nancy Pahwa
    --
    -- Modification History:
    --
    -- Version When         Who        Did what
    -- ------- -----------  --------   -----------------------------------------------------
    -- 1.1    11-July-2017  nancypahwa   Initially Created TMS# 20170616-00237
    ---------------------------------------------------------------------------------
    CURSOR table_stage_rows_cur IS
      SELECT a.seq_num,
             a.category_id,
             a.document_description,
             a.datatype_id,
             a.text,
             a.file_name,
             a.url,
             a.entity_name,
             a.pk1_value,
             a.pk2_value,
             a.pk3_value,
             a.pk4_value,
             a.pk5_value,
             a.media_id,
             a.user_id,
             a.usage_type,
             a.title,
             a.receipt_number,
             a.account_number,
             a.mssql_url,
             a.OLD_ATTACHED_DOCUMENT_ID,
             a.OLD_DOCUMENT_ID,
             a.old_creation_date,
             a.old_updated_date,
             a.old_file_id,
             a.flag,
             a.fnd_flag,
             a.ROWID                    row_id,
             b.ROWID                    OLD_ROW_ID
        FROM XXWC.XXWC_ORACLE_ATTACHMENT_TBL A, FND_LOBS B
       WHERE A.OLD_FILE_ID = B.FILE_ID(+)
         and a.flag = 'Y';
  
    CURSOR table_stage_rows1_cur IS
      SELECT a.seq_num,
             a.category_id,
             a.document_description,
             a.datatype_id,
             a.text,
             a.file_name,
             a.url,
             a.entity_name,
             a.pk1_value,
             a.pk2_value,
             a.pk3_value,
             a.pk4_value,
             a.pk5_value,
             a.media_id,
             a.user_id,
             a.usage_type,
             a.title,
             a.receipt_number,
             a.account_number,
             a.mssql_url,
             a.OLD_ATTACHED_DOCUMENT_ID,
             a.OLD_DOCUMENT_ID,
             a.old_creation_date,
             a.old_updated_date,
             a.old_file_id,
             a.flag,
             a.fnd_flag,
             a.ROWID row_id /*,
                         b.ROWID                    OLD_ROW_ID*/
        FROM XXWC.XXWC_ORACLE_ATTACHMENT_TBL A /*, FND_LOBS B*/
       WHERE /* A.OLD_FILE_ID = B.FILE_ID(+)
                     and*/
       a.flag = 'Y'
       and a.fnd_flag = 'Y';
    TYPE table_stage_rows_aat IS TABLE OF table_stage_rows_cur%ROWTYPE INDEX BY PLS_INTEGER;
    TYPE table_stage_rows1_aat IS TABLE OF table_stage_rows1_cur%ROWTYPE INDEX BY PLS_INTEGER;
    l_table_stage_rows  table_stage_rows_aat;
    l_table_stage_rows1 table_stage_rows1_aat;
    lvc_row_lock        VARCHAR2(1);
  BEGIN
    errbuf  := NULL;
    retcode := '0';
    -- test_prc('Open cursor');
    OPEN table_stage_rows_cur;
    LOOP
      FETCH table_stage_rows_cur BULK COLLECT
        INTO l_table_stage_rows LIMIT 1000;
      --   test_prc('before for all');
      FOR indx IN 1 .. l_table_stage_rows.COUNT loop
        begin
          lvc_row_lock := NULL;
          lvc_row_lock := apps.xxwc_om_force_ship_pkg.is_row_locked(l_table_stage_rows(indx)
                                                                    .OLD_ROW_ID,
                                                                    'FND_LOBS');
        
          IF lvc_row_lock = 'N' THEN
            UPDATE APPS.FND_LOBS
               SET file_data = EMPTY_BLOB()
             WHERE ROWID = l_table_stage_rows(indx).old_row_id;
            UPDATE xxwc.XXWC_ORACLE_ATTACHMENT_TBL
               SET fnd_flag = 'Y'
             WHERE ROWID = l_table_stage_rows(indx).row_id;
            commit;
          END IF;
        end;
      END LOOP;
      l_table_stage_rows.delete;
      EXIT WHEN table_stage_rows_cur%NOTFOUND; /* cause of missing rows */
    END LOOP;
    CLOSE table_stage_rows_cur;
  
    OPEN table_stage_rows1_cur;
  
    LOOP
      FETCH table_stage_rows1_cur BULK COLLECT
        INTO l_table_stage_rows1 LIMIT 1000;
      FORALL indx IN 1 .. l_table_stage_rows1.COUNT
        INSERT /*+ APPEND */
        INTO XXWC.XXWC_ORACLE_ATTACH_ARCH_TBL
          (seq_num,
           category_id,
           document_description,
           file_name,
           url,
           entity_name,
           pk1_value,
           pk2_value,
           pk3_value,
           pk4_value,
           pk5_value,
           media_id,
           user_id,
           usage_type,
           title,
           receipt_number,
           account_number,
           mssql_url,
           old_attached_document_id,
           old_document_id,
           old_creation_date,
           old_updated_date,
           flag,
           fnd_flag,
           old_file_id)
        VALUES
          (l_table_stage_rows1(indx).seq_num,
           l_table_stage_rows1(indx).category_id,
           l_table_stage_rows1(indx).document_description,
           l_table_stage_rows1(indx).file_name,
           l_table_stage_rows1(indx).url,
           l_table_stage_rows1(indx).entity_name,
           l_table_stage_rows1(indx).pk1_value,
           l_table_stage_rows1(indx).pk2_value,
           l_table_stage_rows1(indx).pk3_value,
           l_table_stage_rows1(indx).pk4_value,
           l_table_stage_rows1(indx).pk5_value,
           l_table_stage_rows1(indx).media_id,
           l_table_stage_rows1(indx).user_id,
           l_table_stage_rows1(indx).usage_type,
           l_table_stage_rows1(indx).title,
           l_table_stage_rows1(indx).receipt_number,
           l_table_stage_rows1(indx).account_number,
           l_table_stage_rows1(indx).mssql_url,
           l_table_stage_rows1(indx).OLD_ATTACHED_DOCUMENT_ID,
           l_table_stage_rows1(indx).OLD_DOCUMENT_ID,
           l_table_stage_rows1(indx).old_creation_date,
           l_table_stage_rows1(indx).old_updated_date,
           l_table_stage_rows1(indx).flag,
           l_table_stage_rows1(indx).fnd_flag,
           l_table_stage_rows1(indx).old_file_id);
      --  test_prc('number of rows updated' || sql%rowcount);
      --   COMMIT;
      forall indx IN 1 .. l_table_stage_rows1.COUNT
        delete from xxwc.XXWC_ORACLE_ATTACHMENT_TBL
         where rowid = l_table_stage_rows1(indx).row_id;
      /*WHERE flag = 'Y'
      and fnd_flag = 'Y';*/
      commit;
      l_table_stage_rows1.delete;
      EXIT WHEN table_stage_rows1_cur%NOTFOUND; /* cause of missing rows */
    END LOOP;
    CLOSE table_stage_rows1_cur;
  exception
    when others then
      retcode := '1';
      errbuf  := sqlerrm;
  END;
end;
/