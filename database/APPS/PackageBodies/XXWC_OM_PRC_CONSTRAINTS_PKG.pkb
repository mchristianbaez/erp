CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_PRC_CONSTRAINTS_PKG
AS
   /********************************************************************************
   FILE NAME: XXWC_OM_PRC_CONSTRAINTS_PKG.pkg

   PROGRAM TYPE: PL/SQL Package Body

   PURPOSE: Restrict sales order cancellation,updation using processing constraints

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     1/07/2014       Harsha         Initial creation of the procedure
   1.1     10/30/2014    Pattabhi Avula   TMS# 20141002-00060 -- Canada Changes removed
                                          _ALL for org specific tables
   1.2     05/23/2017    Rakesh Patel     TMS# 20170323-00225 --Custom SOE entry - Phase2
   1.3     7/20/2017     Neha Saini        TMSTask ID: 20170719-00212 added new procedure
   1.4     11/02/2017    Damuluri Gopi     TMS#20171101-00003 - Sales Order 
                                           negative inventory issues
   1.5     06/14/2017    Niraj K Ranjan    TMS#20171213-00260   SOE Phase-3 Limit the Associates who have the security in Oracle to cancel lines on orders
   ********************************************************************************/

   l_pilot_user    VARCHAR2(1):= fnd_profile.value('XXWC_OM_USER_AUTH_PILOT');

   --This function is to check the end user profile organization.

   FUNCTION XXWC_GET_USER_PROF_VAL (V_PROFILE_USER VARCHAR2)
      RETURN NUMBER
   IS
      v_errbuf   CLOB;
      V_VAL      NUMBER;
   BEGIN
      BEGIN
         SELECT VALUE
           INTO V_VAL
           FROM (SELECT PO.PROFILE_OPTION_NAME "NAME",
                        PO.USER_PROFILE_OPTION_NAME,
                        DECODE (TO_CHAR (POV.LEVEL_ID),
                                '10001', 'SITE',
                                '10002', 'APP',
                                '10003', 'RESP',
                                '10005', 'SERVER',
                                '10006', 'ORG',
                                '10004', 'USER',
                                '***')
                           "LEVEL",
                        DECODE (TO_CHAR (POV.LEVEL_ID),
                                '10001', '',
                                '10002', APP.APPLICATION_SHORT_NAME,
                                '10003', RSP.RESPONSIBILITY_KEY,
                                '10005', SVR.NODE_NAME,
                                '10006', ORG.NAME,
                                '10004', USR.USER_NAME,
                                '***')
                           "CONTEXT",
                        POV.PROFILE_OPTION_VALUE "VALUE"
                   FROM APPS.FND_PROFILE_OPTIONS_VL PO,
                        APPS.FND_PROFILE_OPTION_VALUES POV,
                        APPS.FND_USER USR,
                        APPS.FND_APPLICATION APP,
                        APPS.FND_RESPONSIBILITY RSP,
                        APPS.FND_NODES SVR,
                        APPS.HR_OPERATING_UNITS ORG
                  WHERE     1 = 1
                        AND POV.APPLICATION_ID = PO.APPLICATION_ID
                        AND POV.PROFILE_OPTION_ID = PO.PROFILE_OPTION_ID
                        AND USR.USER_ID(+) = POV.LEVEL_VALUE
                        AND RSP.APPLICATION_ID(+) =
                               POV.LEVEL_VALUE_APPLICATION_ID
                        AND RSP.RESPONSIBILITY_ID(+) = POV.LEVEL_VALUE
                        AND APP.APPLICATION_ID(+) = POV.LEVEL_VALUE
                        AND SVR.NODE_ID(+) = POV.LEVEL_VALUE
                        AND ORG.ORGANIZATION_ID(+) = POV.LEVEL_VALUE
                        AND PO.PROFILE_OPTION_NAME =
                               'XXWC_OM_DEFAULT_SHIPPING_ORG') xxwc
          WHERE xxwc.context = V_PROFILE_USER;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_val := 0;
         WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'XXWC_GET_USER_PROF_VAL',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'XXWC_GET_USER_PROF_VAl',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');


            v_val := 0;
      END;

      DBMS_OUTPUT.put_line ('v_val..' || v_val);


      RETURN V_VAL;
   END XXWC_GET_USER_PROF_VAL;
   
   --This function is to check if order header / line can be cancelled
   /********************************************************************************
   PROCEDURE NAME: XXWC_CAN_ORDER_CANCEL

   PURPOSE: Check if order header or line are eligible for cancellation

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.5     06/14/2018    Niraj K Ranjan    TMS#20171213-00260   SOE Phase-3 Limit the Associates who have the security in Oracle to cancel lines on orders
   ********************************************************************************/
   FUNCTION XXWC_CAN_ORDER_CANCEL (p_header_id  NUMBER,
                                   p_line_id       NUMBER,
								   p_user_org_id   NUMBER,
								   p_context       VARCHAR2)
   RETURN VARCHAR2
   IS
      l_auth_user     VARCHAR2(1):= fnd_profile.value('XXWC_OM_USER_AUTHORIZATION');
      l_order_line_canbe_canceled    VARCHAR2(1);
      l_order_type_id       oe_order_headers.order_type_id%TYPE;
	  --l_header_id           oe_order_headers.header_id%TYPE;
	  l_ship_from_org_id    oe_order_headers.ship_from_org_id%TYPE;
	  l_other_branch_line_exists          NUMBER;
	  l_delivery_not_exists     NUMBER;   
      l_flow_status_code	  oe_order_headers.flow_status_code%TYPE;
	  l_ordered_quantity      oe_order_lines.ordered_quantity%TYPE;
      le_proc_exception	  EXCEPTION;
	  l_delivery_exists   NUMBER;
   BEGIN
      --l_header_id := NULL;
	  l_order_type_id := NULL;
	  l_ship_from_org_id := NULL;
	  l_other_branch_line_exists := NULL;
	  l_delivery_not_exists := NULL;
	  l_order_line_canbe_canceled := NULL;
	  l_delivery_exists := NULL;
      	  
         -- To Check order type, default shipping branch
		 BEGIN
            SELECT oh.order_type_id,oh.ship_from_org_id,oh.flow_status_code,ol.ordered_quantity
               INTO l_order_type_id,l_ship_from_org_id,l_flow_status_code,l_ordered_quantity
               FROM  apps.oe_order_headers oh, apps.oe_order_lines ol
              WHERE  oh.header_id = p_header_id
			  AND    oh.header_id = ol.header_id
			  AND    ol.line_id = NVL(p_line_id,ol.line_id)
			  AND    ROWNUM = 1;
		 EXCEPTION
		    WHEN OTHERS THEN
			  l_order_line_canbe_canceled := 'N';
			  RAISE le_proc_exception;
		 END;
		     
         -- To Check default shipping branch
         SELECT COUNT (1)
           INTO l_other_branch_line_exists
           FROM apps.oe_order_lines oel
          WHERE oel.header_id = p_header_id
		    AND  oel.line_id  = NVL(p_line_id,oel.line_id)
            AND OEL.FLOW_STATUS_CODE !='CANCELLED'
            AND oel.SHIP_FROM_ORG_ID != p_user_org_id;
		    
         --1001=Standard Order and 1011 = Internal Order
		 IF l_order_type_id IN (1001, 1011) THEN
		    --To Check if delivery doc printed
			SELECT COUNT(1) INTO l_delivery_exists
			 FROM XXWC.XXWC_WSH_SHIPPING_STG xwss
               WHERE xwss.header_id = p_header_id
			   AND   nvl(xwss.line_id,-1) = NVL(p_line_id,nvl(xwss.line_id,-1));
			IF l_delivery_exists > 0 THEN
               SELECT COUNT(1) INTO l_delivery_not_exists
			    FROM XXWC.XXWC_WSH_SHIPPING_STG xwss
                 WHERE xwss.header_id = p_header_id
                  AND   xwss.line_id = NVL(p_line_id,xwss.line_id)
                  AND   xwss.DELIVERY_ID IS NULL;
            ELSE
			   l_delivery_not_exists := 1;
            END IF;			
		 END IF;
      IF p_context = 'CANCEL_HEADER' THEN
		  IF l_flow_status_code = 'BOOKED' THEN
			 --Set l_can_order_cancel flag according to different conditions  
			 --If order type id Standard / Internal Order
			 IF l_order_type_id IN (1001, 1011) THEN
				--If Default user org id match with header and all lines
				IF l_ship_from_org_id = p_user_org_id AND l_other_branch_line_exists = 0
				THEN
				   --If delivery is created for all lines
				   IF l_delivery_not_exists = 0 THEN
					  --If user is authorized
					  IF l_auth_user = 'Y' THEN
						 l_order_line_canbe_canceled := 'Y';
					  ELSE
						 l_order_line_canbe_canceled := 'N';
					  END IF;			   
				   ELSE
					  l_order_line_canbe_canceled := 'Y';
				   END IF;
				ELSE
				   l_order_line_canbe_canceled := 'N';
				END IF;
			 ELSE
				--If Default user org id match with header and all lines
				IF l_ship_from_org_id = p_user_org_id AND l_other_branch_line_exists = 0
				THEN
				   --If user is authorized
				   IF l_auth_user = 'Y' THEN
					  l_order_line_canbe_canceled := 'Y';
				   ELSE
					  l_order_line_canbe_canceled := 'N';
				   END IF;
				ELSE
				   l_order_line_canbe_canceled := 'N';
				END IF;
			 END IF;
		  ELSE
			 l_order_line_canbe_canceled := 'Y';
		  END IF;
      END IF; --End If p_context = 'CANCEL_HEADER'
	  
	  IF p_context = 'CANCEL_LINE' THEN
		  IF l_flow_status_code = 'BOOKED' THEN
			 --Set l_can_order_cancel flag according to different conditions  
			 --If order type id Standard / Internal Order
			 IF l_order_type_id IN (1001, 1011) THEN
				--If Default user org id match with  lines
				IF l_other_branch_line_exists = 0 THEN
				   --If delivery is created for line
				   IF l_delivery_not_exists = 0 THEN
					  --If user is authorized
					  IF l_auth_user = 'Y' THEN
						 l_order_line_canbe_canceled := 'Y';
					  ELSE
						 l_order_line_canbe_canceled := 'N';
					  END IF;			   
				   ELSE
					  l_order_line_canbe_canceled := 'Y';
				   END IF;
				ELSE
				   l_order_line_canbe_canceled := 'N';
				END IF;
			 ELSE
				--If Default user org id match with line
				IF l_other_branch_line_exists = 0 THEN
				   --If user is authorized
				   IF l_auth_user = 'Y' THEN
					  l_order_line_canbe_canceled := 'Y';
				   ELSE
					  l_order_line_canbe_canceled := 'N';
				   END IF;
				ELSE
				   l_order_line_canbe_canceled := 'N';
				END IF;
			 END IF;
		  ELSE
			 l_order_line_canbe_canceled := 'Y';
		  END IF;
	  END IF; --End If p_context = 'CANCEL_LINE'
	  
	  IF p_context = 'UPDATE_LINE' THEN
		  IF l_flow_status_code = 'BOOKED' THEN
			 --Set l_can_order_cancel flag according to different conditions  
			 --If order type id Standard / Internal Order
			 IF l_order_type_id IN (1001, 1011) THEN
				--If Default user org id match with  lines
				IF l_other_branch_line_exists = 0 THEN
				   --If delivery is created for line
				   IF l_delivery_not_exists = 0 THEN
					  --If user is authorized
					  IF l_auth_user = 'Y' THEN
						 l_order_line_canbe_canceled := 'Y';
					  ELSE
						 l_order_line_canbe_canceled := 'N';
					  END IF;			   
				   ELSE
					  l_order_line_canbe_canceled := 'Y';
				   END IF;
				ELSE
				   IF oe_line_security.g_record.ORDERED_QUANTITY != l_ordered_quantity
				   THEN
					  l_order_line_canbe_canceled := 'N';
				   ELSE
					  l_order_line_canbe_canceled := 'Y';
				   END IF;
				END IF;
			 ELSE
				--If Default user org id match with line
				IF l_other_branch_line_exists = 0 THEN
				   --If user is authorized
				   IF l_auth_user = 'Y' THEN
					  l_order_line_canbe_canceled := 'Y';
				   ELSE
					  l_order_line_canbe_canceled := 'N';
				   END IF;
				ELSE
				   IF oe_line_security.g_record.ORDERED_QUANTITY != l_ordered_quantity
				   THEN
					  l_order_line_canbe_canceled := 'N';
				   ELSE
					  l_order_line_canbe_canceled := 'Y';
				   END IF;
				END IF;
			 END IF;
		  ELSE
			 l_order_line_canbe_canceled := 'Y';
		  END IF;
	  END IF; --End If p_context = 'UPDATE_LINE'
	  
      RETURN l_order_line_canbe_canceled;
   EXCEPTION
      WHEN le_proc_exception THEN
	     RETURN l_order_line_canbe_canceled;
   END XXWC_CAN_ORDER_CANCEL;

   --This Procedure is for restricting order header cancellation if enduser org id<> sales order line org id and order status is booked
   /********************************************************************************
   PROCEDURE NAME: CANCEL_HEADER

   PURPOSE: This Procedure is for restricting order header cancellation if it is eligible for cancellation

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.5     06/14/2018    Niraj K Ranjan    TMS#20171213-00260   SOE Phase-3 Limit the Associates who have the security in Oracle to cancel lines on orders
   ********************************************************************************/
   PROCEDURE CANCEL_HEADER (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
   AS
      v_val           NUMBER := 0;
      v_flow_status   VARCHAR2 (30);
	  --Ver 1.5 - added below variables
	  l_pilot_user    VARCHAR2(1):= fnd_profile.value('XXWC_OM_USER_AUTH_PILOT');
	  l_pilot_branch_count  NUMBER := 0;
	  l_user_org_id   NUMBER;
	  l_order_line_canbe_canceled    VARCHAR2(1);
   BEGIN
      --Start Ver 1.5
	  l_user_org_id := XXWC_GET_USER_PROF_VAL (FND_GLOBAL.USER_NAME);
      IF l_pilot_user = 'Y' THEN
	     SELECT count(1)
		 INTO l_pilot_branch_count
		 FROM fnd_lookup_values  
         WHERE  lookup_type = 'XXWC_OM_SO_CANCEL_PILOT_BRANCH'
         AND    lookup_code = l_user_org_id
         AND    ENABLED_FLAG = 'Y'
         AND    TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE_ACTIVE,SYSDATE)) AND TRUNC(NVL(END_DATE_ACTIVE,SYSDATE));
	  END IF;
	  --End Ver 1.5
	  
	  --If pilot check profile enabled and branch is not pilot branch then execute old logic else new logic
	  IF l_pilot_user = 'Y' AND l_pilot_branch_count = 0 THEN --OLD LOGIC Ver 1.5
	     
         SELECT COUNT (1)
           INTO v_val
           FROM apps.oe_order_lines oel, apps.oe_order_headers oeh
          WHERE     oeh.order_number = oe_header_security.g_record.ORDER_NUMBER
                AND oel.header_id = oeh.header_id
                AND OEL.FLOW_STATUS_CODE!='CANCELLED'
                AND oel.SHIP_FROM_ORG_ID != 
                       XXWC_GET_USER_PROF_VAL (FND_GLOBAL.USER_NAME);
         BEGIN
            SELECT oh.flow_status_code
              INTO v_flow_status
              FROM apps.oe_order_headers oh
             WHERE     oh.header_id = oe_header_security.g_record.header_id
              AND oh.org_id = oe_header_security.g_record.org_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_flow_status := null;
            WHEN OTHERS
            THEN
               v_errbuf :=
                     'Error_Stack...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_stack ()
                  || CHR (10)
                  || ' Error_Backtrace...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_backtrace ();
               
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
                  p_calling             => 'CANCEL_HEADER',
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                             REGEXP_REPLACE (v_errbuf,
                                                             '[[:cntrl:]]',
                                                             NULL),
                                             1,
                                             2000),
                  p_error_desc          =>    'Error running '
                                           || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                           || '.'
                                           || 'CANCEL_HEADER',
                  p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
                  p_module              => 'ONT');
               v_flow_status := null;
         END;
	  
         IF v_flow_status = 'BOOKED'
         THEN
            IF v_val = 0
            THEN
         
               x_result := 0;
            ELSE
         
               x_result := 1;
            END IF;
         ELSE
         
            x_result := 0;
         END IF;
     
      ELSE --NEW LOGIC Ver 1.5
	     l_order_line_canbe_canceled := XXWC_CAN_ORDER_CANCEL(
			                       p_header_id  => oe_header_security.g_record.header_id,
                                   p_line_id       => null,
								   p_user_org_id   => l_user_org_id,
								   p_context       => 'CANCEL_HEADER');
		
            IF l_order_line_canbe_canceled = 'Y' THEN
		       x_result := 0;
		    ELSE
		       x_result := 1;
		    END IF;
	  END IF;
   EXCEPTION
      WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'CANCEL_HEADER',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'CANCEL_HEADER',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');
   END CANCEL_HEADER;


   --This Procedure is for restricting order line cancellation if enduser org id<> sales order line org id and order status is booked
   /********************************************************************************
   PROCEDURE NAME: CANCEL_LINE

   PURPOSE: This Procedure is for restricting order line cancellation if it is eligible for cancellation

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.5     06/14/2018    Niraj K Ranjan    TMS#20171213-00260   SOE Phase-3 Limit the Associates who have the security in Oracle to cancel lines on orders
   ********************************************************************************/
   PROCEDURE CANCEL_LINE (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
   AS
      v_val           NUMBER := 0;
      v_flow_status   VARCHAR2 (30);
      v_qty           NUMBER := 0;
	  --Ver 1.5 - added below variables
	  l_pilot_user    VARCHAR2(1):= fnd_profile.value('XXWC_OM_USER_AUTH_PILOT');
	  l_pilot_branch_count  NUMBER := 0;
	  l_user_org_id   NUMBER := XXWC_GET_USER_PROF_VAL (FND_GLOBAL.USER_NAME);
	  l_order_line_canbe_canceled    VARCHAR2(1);
   BEGIN
      --Start Ver 1.5
      IF l_pilot_user = 'Y' THEN
	     SELECT count(1)
		 INTO l_pilot_branch_count
		 FROM fnd_lookup_values  
         WHERE  lookup_type = 'XXWC_OM_SO_CANCEL_PILOT_BRANCH'
         AND    lookup_code = l_user_org_id
         AND    ENABLED_FLAG = 'Y'
         AND    TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE_ACTIVE,SYSDATE)) AND TRUNC(NVL(END_DATE_ACTIVE,SYSDATE));
	  END IF;
	  --End Ver 1.5
	  --If pilot check profile enabled and branch is not pilot branch then execute old logic else new logic
	  IF l_pilot_user = 'Y' AND l_pilot_branch_count = 0 THEN --OLD LOGIC Ver 1.5
         SELECT COUNT (1)
           INTO v_val
           FROM apps.oe_order_lines oel
          WHERE     oel.SHIP_FROM_ORG_ID !=
                       XXWC_GET_USER_PROF_VAL (FND_GLOBAL.USER_NAME)
                AND OEL.line_id = oe_line_security.g_record.line_id
                AND OEL.org_id = oe_line_security.g_record.org_id;

         BEGIN
            SELECT ol.ORDERED_QUANTITY, oh.flow_status_code
              INTO v_qty, v_flow_status
              FROM apps.oe_order_headers oh, apps.oe_order_lines ol
             WHERE     oh.header_id = oe_line_security.g_record.header_id
                   AND oh.header_id = ol.header_id
                   AND ol.line_id = oe_line_security.g_record.line_id
                   AND ol.org_id = oe_line_security.g_record.org_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                v_qty := 0;
                v_flow_status:=null;

            WHEN OTHERS
            THEN
               v_errbuf :=
                     'Error_Stack...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_stack ()
                  || CHR (10)
                  || ' Error_Backtrace...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_backtrace ();

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
                  p_calling             => 'CANCEL_LINE',
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                             REGEXP_REPLACE (v_errbuf,
                                                             '[[:cntrl:]]',
                                                             NULL),
                                             1,
                                             2000),
                  p_error_desc          =>    'Error running '
                                           || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                           || '.'
                                           || 'CANCEL_LINE',
                  p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
                  p_module              => 'ONT');
               v_qty := 0;
               v_flow_status:=null;


         END;

         IF v_flow_status = 'BOOKED'
         THEN
            IF v_val = 0
            THEN
         
               x_result := 0;
            ELSE
         
               x_result := 1;
            END IF;
         ELSE
         
            x_result := 0;
         END IF;
      ELSE --NEW LOGIC Ver 1.5
	     l_order_line_canbe_canceled := XXWC_CAN_ORDER_CANCEL(
		                           p_header_id  => oe_line_security.g_record.header_id,
                                   p_line_id       => oe_line_security.g_record.line_id,
								   p_user_org_id   => l_user_org_id,
								   p_context       => 'CANCEL_LINE');
         IF l_order_line_canbe_canceled = 'Y' THEN
		    x_result := 0;
		 ELSE
		    x_result := 1;
		 END IF;
	  END IF;
   EXCEPTION
       WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'CANCEL_LINE',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'CANCEL_LINE',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');
   END CANCEL_LINE;


   --This procedure is to restrict order line quantity updation if enduser org id <> sales order line org id and order status is booked
   /********************************************************************************
   PROCEDURE NAME: UPDATE_LINE

   PURPOSE: This procedure is to restrict order line quantity updation if it is eligible for updation

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.5     06/14/2018    Niraj K Ranjan    TMS#20171213-00260   SOE Phase-3 Limit the Associates who have the security in Oracle to cancel lines on orders
   ********************************************************************************/
   PROCEDURE UPDATE_LINE (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
   AS
      v_val           NUMBER := 0;
      v_errbuf        VARCHAR2 (2000);
      v_flow_status   VARCHAR2 (50);
	  --Ver 1.5 - added below variables
	  l_pilot_user    VARCHAR2(1):= fnd_profile.value('XXWC_OM_USER_AUTH_PILOT');
	  l_pilot_branch_count  NUMBER := 0;
	  l_user_org_id   NUMBER := XXWC_GET_USER_PROF_VAL (FND_GLOBAL.USER_NAME);
	  l_order_line_canbe_canceled    VARCHAR2(1);
   BEGIN

      --Start Ver 1.5
      IF l_pilot_user = 'Y' THEN
	     SELECT count(1)
		 INTO l_pilot_branch_count
		 FROM fnd_lookup_values  
         WHERE  lookup_type = 'XXWC_OM_SO_CANCEL_PILOT_BRANCH'
         AND    lookup_code = l_user_org_id
         AND    ENABLED_FLAG = 'Y'
         AND    TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE_ACTIVE,SYSDATE)) AND TRUNC(NVL(END_DATE_ACTIVE,SYSDATE));
	  END IF;
      --End Ver 1.5
	  
	  --If pilot check profile enabled and branch is not pilot branch then execute old logic else new logic
	  IF l_pilot_user = 'Y' AND l_pilot_branch_count = 0 THEN --OLD LOGIC Ver 1.5
         BEGIN
           SELECT ol.ORDERED_QUANTITY, oh.flow_status_code
             INTO v_val, v_flow_status
             FROM apps.oe_order_headers oh, apps.oe_order_lines ol
            WHERE     oh.header_id = oe_line_security.g_record.header_id
                  AND oh.header_id = ol.header_id
                  AND ol.line_id = oe_line_security.g_record.line_id
                  AND ol.org_id = oe_line_security.g_record.org_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_val := 0;
               v_flow_status:=NULL;
            
            WHEN OTHERS
            THEN
               v_errbuf :=
                     'Error_Stack...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_stack ()
                  || CHR (10)
                  || ' Error_Backtrace...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_backtrace ();

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
                  p_calling             => 'UPDATE_LINE',
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                             REGEXP_REPLACE (v_errbuf,
                                                             '[[:cntrl:]]',
                                                             NULL),
                                             1,
                                             2000),
                  p_error_desc          =>    'Error running '
                                           || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                           || '.'
                                           || 'UPDATE_LINE',
                  p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
                  p_module              => 'ONT');
               
                 v_val := 0;
                 v_flow_status:=NULL;
         END;

         IF v_flow_status = 'BOOKED'
         THEN
            IF oe_line_security.g_record.ship_from_org_id !=
                  XXWC_GET_USER_PROF_VAL (FND_GLOBAL.USER_NAME)
            THEN
               IF oe_line_security.g_record.ORDERED_QUANTITY != v_val
               THEN
         
                  x_result := 1;
               ELSE
         
                  x_result := 0;
               END IF;
            ELSE                                             --USER PROFILE VALUE
         
               x_result := 0;
            END IF;
         ELSE
         
            x_result := 0;
         END IF;
      ELSE --NEW LOGIC Ver 1.5
	     l_order_line_canbe_canceled := XXWC_CAN_ORDER_CANCEL(
		                           p_header_id  => oe_line_security.g_record.header_id,
                                   p_line_id       => oe_line_security.g_record.line_id,
								   p_user_org_id   => l_user_org_id,
								   p_context       => 'UPDATE_LINE');
         IF l_order_line_canbe_canceled = 'Y' THEN
		    x_result := 0;
		 ELSE
		    x_result := 1;
		 END IF;
	  END IF;
 EXCEPTION
   WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'UPDATE_LINE',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'UPDATE_LINE',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');


   END update_LINE;

   --Ver 1.2 <Start
   /*******************************************************************************
   Procedure Name   :   CHANGE_LINE_WAREHOUSE
   Description      :   This procedure is to restrict wharehouse line updation for counter order

   Change History  :
    Ver       DATE         NAME              Modification
    ---       --------     -------           ------------------------------------------
              15-Mar-17    Rakesh Patel      TMS# 20170323-00225 --Custom SOE entry - Phase2
   1.4        11/02/2017   Damuluri Gopi     TMS#20171101-00003 - Sales Order 
                                           negative inventory issues
   ******************************************************************************/
   PROCEDURE CHANGE_LINE_WAREHOUSE (
         p_application_id                 IN            NUMBER,
         p_entity_short_name              IN            VARCHAR2,
         p_validation_entity_short_name   IN            VARCHAR2,
         p_validation_tmplt_short_name    IN            VARCHAR2,
         p_record_set_short_name          IN            VARCHAR2,
         p_scope                          IN            VARCHAR2,
         x_result                            OUT NOCOPY NUMBER)
      AS
         v_ship_from_org_id     apps.oe_order_headers.ship_from_org_id%TYPE;
         v_order_type_id        apps.oe_order_headers.order_type_id%TYPE;
         v_errbuf               VARCHAR2 (2000);
         v_flow_status_code     apps.oe_order_headers.flow_status_code%TYPE; -- Ver#1.4 
         v_ln_ship_from_org_id  apps.oe_order_lines.ship_from_org_id%TYPE;
      BEGIN
         BEGIN
            SELECT oh.ship_from_org_id, oh.order_type_id
                 , oh.flow_status_code -- Ver#1.4 
              INTO v_ship_from_org_id, v_order_type_id
                 , v_flow_status_code -- Ver#1.4
              FROM apps.oe_order_headers oh
             WHERE oh.header_id = oe_line_security.g_record.header_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_ship_from_org_id := 0;
               v_order_type_id    := 0;
            WHEN OTHERS
            THEN
               v_errbuf :=
                     'Error_Stack...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_stack ()
                  || CHR (10)
                  || ' Error_Backtrace...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_backtrace ();



               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
                  p_calling             => 'CHANGE_LINE_WAREHOUSE',
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                             REGEXP_REPLACE (v_errbuf,
                                                             '[[:cntrl:]]',
                                                             NULL),
                                             1,
                                             2000),
                  p_error_desc          =>    'Error running '
                                           || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                           || '.'
                                           || 'CHANGE_LINE_WAREHOUSE',
                  p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
                  p_module              => 'ONT');

               v_ship_from_org_id := 0;
               v_order_type_id    := 0;
         END;

--<START>-- Ver#1.4  
         BEGIN
            SELECT ool.ship_from_org_id
              INTO v_ln_ship_from_org_id
              FROM apps.oe_order_lines ool
             WHERE ool.line_id = oe_line_security.g_record.line_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_ln_ship_from_org_id := 0;
            WHEN OTHERS
            THEN
               v_errbuf :=
                     'Error_Stack...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_stack ()
                  || CHR (10)
                  || ' Error_Backtrace...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_backtrace ();

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
                  p_calling             => 'CHANGE_LINE_WAREHOUSE',
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                             REGEXP_REPLACE (v_errbuf,
                                                             '[[:cntrl:]]',
                                                             NULL),
                                             1,
                                             2000),
                  p_error_desc          =>    'Error running '
                                           || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                           || '.'
                                           || 'CHANGE_LINE_WAREHOUSE',
                  p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
                  p_module              => 'ONT');

               v_ln_ship_from_org_id := 0;
         END;
--<END> -- Ver#1.4 

         IF oe_line_security.g_record.ship_from_org_id != v_ln_ship_from_org_id 
         AND v_order_type_id = 1004 
         AND v_flow_status_code = 'BOOKED' 
         THEN -- Ver#1.4 
            x_result := 1;
         ELSE
            x_result := 0;
         END IF;

      EXCEPTION
         WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'CHANGE_LINE_WAREHOUSE',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'CHANGE_LINE_WAREHOUSE',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');
   END CHANGE_LINE_WAREHOUSE;
   --Ver 1.2 >End
--Neha changes for TMSTask ID: 20170719-00212 starts ver1.3
--This Procedure is for restricting order line cancellation if order quantity length is greater than 8
PROCEDURE CANCEL_LINE_QUANTITY (
   p_application_id                 IN            NUMBER,
   p_entity_short_name              IN            VARCHAR2,
   p_validation_entity_short_name   IN            VARCHAR2,
   p_validation_tmplt_short_name    IN            VARCHAR2,
   p_record_set_short_name          IN            VARCHAR2,
   p_scope                          IN            VARCHAR2,
   x_result                            OUT NOCOPY NUMBER)
AS
   v_qty           NUMBER := 0;
   v_val           NUMBER := 0;
   v_errbuf        VARCHAR2 (2000);
   v_flow_status   VARCHAR2 (50);
BEGIN
   BEGIN
      SELECT ol.ORDERED_QUANTITY, oh.flow_status_code
        INTO v_qty, v_flow_status
        FROM apps.oe_order_headers oh, apps.oe_order_lines ol
       WHERE     oh.header_id = oe_line_security.g_record.header_id
             AND oh.header_id = ol.header_id
             AND ol.line_id = oe_line_security.g_record.line_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_qty := 0;
         v_flow_status := NULL;
      WHEN OTHERS
      THEN
         v_errbuf :=
               'Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
            p_calling             => 'CANCEL_LINE_QUANTITY',
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                       REGEXP_REPLACE (v_errbuf,
                                                       '[[:cntrl:]]',
                                                       NULL),
                                       1,
                                       2000),
            p_error_desc          =>    'Error running '
                                     || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                     || '.'
                                     || 'CANCEL_LINE_QUANTITY',
            p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
            p_module              => 'ONT');

         v_qty := 0;
         v_flow_status := NULL;
   END;


   IF    (LENGTH (ROUND (oe_line_security.g_record.ORDERED_QUANTITY)) > 7)
      OR (LENGTH (ROUND (v_qty)) > 7)
   THEN
      x_result := 1;
   ELSE
      x_result := 0;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      v_errbuf :=
            'Error_Stack...'
         || CHR (10)
         || DBMS_UTILITY.format_error_stack ()
         || CHR (10)
         || ' Error_Backtrace...'
         || CHR (10)
         || DBMS_UTILITY.format_error_backtrace ();



      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
         p_calling             => 'CANCEL_LINE_QUANTITY',
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SUBSTR (
                                    REGEXP_REPLACE (v_errbuf,
                                                    '[[:cntrl:]]',
                                                    NULL),
                                    1,
                                    2000),
         p_error_desc          =>    'Error running '
                                  || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                  || '.',
         p_distribution_list   =>    'HDSOracleDevelopers@hdsupply.com'
                                  || 'CANCEL_LINE_QUANTITY',
         p_module              => 'ONT');
END CANCEL_LINE_QUANTITY;
   --Neha changes for TMSTask ID: 20170719-00212 ends

-- Ver#1.4 > Start
   /*******************************************************************************
   Procedure Name   :   CHANGE_HEADER_WAREHOUSE
   Description      :   This procedure is to restrict header wharehouse updation 
                         for counter order

   Change History  :
   Version  DATE         NAME              Modification
   -------  -------      ---------------   -------------------------------------
   1.4      11/02/2017   Damuluri Gopi     TMS#20171101-00003 - Sales Order 
                                           negative inventory issues
   ******************************************************************************/
   PROCEDURE CHANGE_HEADER_WAREHOUSE (
         p_application_id                 IN            NUMBER,
         p_entity_short_name              IN            VARCHAR2,
         p_validation_entity_short_name   IN            VARCHAR2,
         p_validation_tmplt_short_name    IN            VARCHAR2,
         p_record_set_short_name          IN            VARCHAR2,
         p_scope                          IN            VARCHAR2,
         x_result                            OUT NOCOPY NUMBER)
      AS
         v_ship_from_org_id     apps.oe_order_headers.ship_from_org_id%TYPE;
         v_order_type_id        apps.oe_order_headers.order_type_id%TYPE;
         v_errbuf               VARCHAR2 (2000);
         v_flow_status_code     apps.oe_order_headers.flow_status_code%TYPE; 
      BEGIN
         BEGIN
            SELECT oh.ship_from_org_id, oh.order_type_id
                 , oh.flow_status_code 
              INTO v_ship_from_org_id, v_order_type_id
                 , v_flow_status_code 
              FROM apps.oe_order_headers oh
             WHERE oh.header_id = oe_header_security.g_record.header_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_ship_from_org_id := 0;
               v_order_type_id    := 0;
            WHEN OTHERS
            THEN
               v_errbuf :=
                     'Error_Stack...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_stack ()
                  || CHR (10)
                  || ' Error_Backtrace...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_backtrace ();



               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
                  p_calling             => 'CHANGE_HEADER_WAREHOUSE',
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                             REGEXP_REPLACE (v_errbuf,
                                                             '[[:cntrl:]]',
                                                             NULL),
                                             1,
                                             2000),
                  p_error_desc          =>    'Error running '
                                           || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                           || '.'
                                           || 'CHANGE_HEADER_WAREHOUSE',
                  p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
                  p_module              => 'ONT');

               v_ship_from_org_id := 0;
               v_order_type_id    := 0;
         END;

         IF oe_header_security.g_record.ship_from_org_id != v_ship_from_org_id 
         AND v_order_type_id = 1004 
         AND v_flow_status_code = 'BOOKED' 
         THEN 
            x_result := 1;
         ELSE
            x_result := 0;
         END IF;

      EXCEPTION
         WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'CHANGE_HEADER_WAREHOUSE',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'CHANGE_HEADER_WAREHOUSE',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');
   END CHANGE_HEADER_WAREHOUSE;
-- Vern#1.4 < End

END XXWC_OM_PRC_CONSTRAINTS_PKG;
/