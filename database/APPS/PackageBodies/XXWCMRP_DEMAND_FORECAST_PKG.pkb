CREATE OR REPLACE 
package body           APPS.XXWCMRP_DEMAND_FORECAST_PKG As

  -- global declarations
  XXWC_ERROR                           EXCEPTION;


/*******************************************************************************************/
/* Direct_Upload : procedure to upload user-supplied data into custom staging table        */
/*******************************************************************************************/
Procedure Direct_Upload(p_org_code               IN  VARCHAR2
                       ,p_forecast_designator    IN  VARCHAR2
                       ,p_item_number            IN  VARCHAR2
                       ,p_bucket_type            IN  VARCHAR2
                       ,p_forecast_date          IN  DATE
                       ,p_forecast_end_date      IN  DATE DEFAULT NULL
                       ,p_number_buckets         IN  NUMBER DEFAULT 1
                       ,p_quantity               IN  NUMBER) IS

  l_item_id                            NUMBER;
  l_org_id                             NUMBER;
  l_trx_id                             NUMBER;
  l_forecast_date                      DATE;
  l_forecast_end_date                  DATE;
  l_bucket_type                        NUMBER;
  l_dummy                              VARCHAR2(1);

  l_error_message                      VARCHAR2(80);

Begin

  -- translate item number / organization code into inventory-item-id and organization-id
  Begin
    Select MSI.inventory_item_id,
           MP.organization_id
      Into l_item_id,
           l_org_id
      From MTL_SYSTEM_ITEMS_B MSI,
           MTL_PARAMETERS MP
     Where MSI.segment1 = p_item_number
       And MP.organization_code = p_Org_code
       And MP.organization_id = MSI.organization_id;
  Exception
    When NO_DATA_FOUND Then
      l_error_message := 'Invalid item number for organization';
      RAISE XXWC_ERROR;
  End;

  Begin
    Select 'x'
      Into l_dummy
      From MRP_FORECAST_DESIGNATORS
     Where organization_id = l_org_id
       And forecast_designator = p_forecast_designator;
  Exception
    When NO_DATA_FOUND Then
      l_error_message := 'Invalid organization/forecast designator combination';
      RAISE XXWC_ERROR;
  End;

  -- validate bucket type
  Case Upper(p_bucket_type)
    When 'DAYS' Then l_bucket_type := 1;
    When 'WEEKS' Then l_bucket_type := 2;
    When 'PERIODS' Then l_bucket_type := 3;
    Else
      l_error_message := 'Invalid forecast bucket type';
      RAISE XXWC_ERROR;
  End Case;

  -- force forecast date to period start date
  l_forecast_date := MRP_CALENDAR.Date_Offset(l_org_id, l_bucket_type, MRP_CALENDAR.Date_Offset(l_org_id, l_bucket_type, p_forecast_date, -1),1);

  If Nvl(p_number_buckets,1) > 1
  And p_forecast_end_date Is NULL Then
    l_forecast_end_date := MRP_CALENDAR.Date_Offset(l_org_id, l_bucket_type, l_forecast_date, p_number_buckets)-1;
  Else
    l_forecast_end_Date := p_forecast_end_date;
  End If;

  -- validate record not already in interface table
  Begin
    Delete From XXWC.XXWCINV_DEMAND_FORECAST_IFACE
     Where organization_id = l_org_id
       And inventory_item_Id = l_item_id
       And forecast_designator = p_forecast_designator
       And forecast_date = l_forecast_date;
  Exception
    When NO_DATA_FOUND Then
      NULL;
  End;

  -- populate custom staging table
  Insert Into XXWC.XXWCINV_DEMAND_FORECAST_IFACE
         (organization_code
         ,organization_id
         ,forecast_designator
         ,item_number
         ,inventory_item_id
         ,bucket_type
         ,forecast_date
         ,forecast_end_date
         ,number_buckets
         ,quantity
         ,confidence_percentage
         )
  Values (p_org_code
         ,l_org_id
         ,p_forecast_designator
         ,p_item_number
         ,l_item_id
         ,l_bucket_type
         ,l_forecast_date
         ,l_forecast_end_date
         ,p_number_buckets
         ,p_quantity
         ,100
         );

Exception
  When XXWC_ERROR Then
    Raise_Application_Error(-20001, SUBSTR (l_error_message, 1, 100));
  When OTHERS Then
    Raise_Application_Error(-20002, 'Error:'||SQLERRM);
End Direct_Upload;


/*******************************************************************************************/
/* Load_Forecast : procedure to migrate user-supplied data into base MRP tables            */
/*******************************************************************************************/
Procedure Load_Forecast(errbuf   OUT VARCHAR2
                       ,retcode  OUT NUMBER) Is

  -- retrieve user-supplied forecast date from custom staging table
  Cursor C1 Is
    Select DF.item_number,
           MSI.inventory_item_id,
           DF.organization_code,
           MP.organization_id,
           DF.bucket_type,
           DF.forecast_designator,
           DF.forecast_date,
           DF.forecast_end_date,
           DF.number_buckets,
           DF.quantity,
           DF.confidence_percentage,
           DF.rowid row_id
      From XXWC.XXWCINV_DEMAND_FORECAST_IFACE DF,
           MTL_SYSTEM_ITEMS_B MSI,
           MTL_PARAMETERS MP,
           MRP_FORECAST_DESIGNATORS MFD
     Where DF.organization_code = MP.organization_code
       And DF.item_number = MSI.segment1
       And MP.organization_id = MSI.organization_id
       And DF.forecast_designator = MFD.forecast_designator
       And MP.organization_id = MFD.organization_id
     Order By DF.organization_code, DF.forecast_designator, DF.item_number;

  Cursor C_Errors Is
    Select DF.item_number,
           DF.organization_code,
           DF.bucket_type,
           DF.forecast_designator,
           DF.forecast_date,
           DF.quantity,
           DF.error_message
      From XXWC.XXWCINV_DEMAND_FORECAST_IFACE DF
     Where DF.error_message Is Not NULL
     Order By DF.organization_code, DF.forecast_designator, DF.item_number;

  l_success                            BOOLEAN := TRUE;
  l_found                              BOOLEAN := FALSE;
  l_forecast_interface                 MRP_FORECAST_INTERFACE_PK.t_forecast_interface;
  l_forecast_designator                MRP_FORECAST_INTERFACE_PK.t_forecast_designator;

  l_ndx                                NUMBER := 0;
  l_dndx                               NUMBER := 0;

  l_transaction_id                     NUMBER;
  l_bucket_type                        NUMBER;
  l_message                            VARCHAR2(256);

  l_step                               NUMBER := 0;
  l_cnt                                NUMBER := 0;
  l_cnt_err                            NUMBER := 0;

Begin

  l_step := 1.0;

  l_step := 2.0;
  FND_FILE.Put_Line(FND_FILE.Log, 'Processing -----');
  For R1 In C1 Loop
    Begin

      FND_FILE.Put_Line(FND_FILE.Log, R1.organization_id||'|'||R1.forecast_designator||'|'||R1.inventory_item_id||'|'||R1.bucket_type||'|'||
                                      R1.forecast_date||'|'||R1.quantity||'|'||R1.confidence_percentage);

      l_cnt := l_cnt + 1;
      l_step := 5.0;
      -- confirm that item / org are valid
      If R1.inventory_item_id Is NULL
      Or R1.organization_id Is NULL Then
        l_message := 'Invalid item number / organization code';
        Raise XXWC_ERROR;
      End If;

      -- determine if transaction already exists, if so, then allow for update
      Begin
        l_step := 6.0;
        Select transaction_id
          Into l_transaction_id
          From MRP_FORECAST_DATES
         Where inventory_item_id = R1.inventory_item_id
           And organization_id = R1.organization_id
           And forecast_designator = R1.forecast_designator
           And forecast_date = R1.forecast_date;

      Exception
        When NO_DATA_FOUND Then
          l_transaction_id := NULL;
      End;

      l_step := 7.0;
      -- validate forecast date provided
      If R1.forecast_date Is NULL Then
        l_message := 'Missing forecast date';
        Raise XXWC_ERROR;
      End If;

      l_step := 9.0;
      l_ndx := l_ndx + 1;
      -- populate API record type
      l_forecast_interface(l_ndx).inventory_item_id := R1.inventory_item_id;
      l_forecast_interface(l_ndx).forecast_designator := R1.forecast_designator;
      l_forecast_interface(l_ndx).organization_id := R1.organization_id;
      l_forecast_interface(l_ndx).forecast_date := R1.forecast_date;

      If R1.forecast_end_date Is Not NULL Then
        l_forecast_interface(l_ndx).forecast_end_date := R1.forecast_end_date;
      End If;

      l_forecast_interface(l_ndx).quantity := R1.quantity;
      l_forecast_interface(l_ndx).confidence_percentage := R1.confidence_percentage;
      l_forecast_interface(l_ndx).transaction_id := l_transaction_id;

      l_forecast_interface(l_ndx).process_status := 2;
      l_forecast_interface(l_ndx).workday_control := 2;          -- 1=reject  2=shift forword  3=shift backwards
      l_forecast_interface(l_ndx).bucket_type:= R1.bucket_type;   -- 1=days  2=weeks  3=periods

      l_forecast_interface(l_ndx).comments := NULL;
      l_forecast_interface(l_ndx).source_code := NULL;
      l_forecast_interface(l_ndx).source_line_id := NULL;

      l_forecast_interface(l_ndx).last_update_date := SYSDATE;
      l_forecast_interface(l_ndx).last_updated_by := FND_GLOBAL.user_id;
      l_forecast_interface(l_ndx).creation_date := SYSDATE;
      l_forecast_interface(l_ndx).created_by := FND_GLOBAL.user_id;
      l_forecast_interface(l_ndx).last_update_login := NULL;

    Exception
      When XXWC_ERROR Then
        FND_FILE.Put_Line(FND_FILE.Log, l_message);
        l_step := 10.0;
        errbuf := l_message;
        retcode := 1;
        Update XXWC.XXWCINV_DEMAND_FORECAST_IFACE
           Set error_message = l_message
         Where rowid = R1.row_id;
        COMMIT;
    End;
  End Loop;

  l_step := 11.0;
  If l_ndx = 0 Then
    FND_FILE.Put_Line(FND_FILE.Log, 'No Rows Processed');
    errbuf := 'No Rows Processed';
    retcode := 1;
    Raise XXWC_ERROR;
  End If;

/*  -- comment out this section for time being, as it would be used to remove existing forecast data before loading user-supplied information
  l_step := 12.0;
  -- build forecast designator variable from forecast interface variable values
  For i In 1..l_ndx Loop
    l_step := 13.0;
    l_found := FALSE;
    l_step := 13.5;
    For x In 1..l_dndx Loop
      l_step := 14.0;
      If l_forecast_designator(x).organization_id = l_forecast_interface(i).organization_id
      And l_forecast_designator(x).forecast_designator = l_forecast_interface(i).forecast_designator
      And l_forecast_designator(x).inventory_item_id = l_forecast_interface(i).inventory_item_id Then
        l_found := TRUE;
      End If;
    End Loop;

    l_step := 15.0;
    If Not l_found Then
      l_dndx := l_dndx + 1;
      l_forecast_designator(l_dndx).organization_id := l_forecast_interface(i).organization_id;
      l_forecast_designator(l_dndx).forecast_designator := l_forecast_interface(i).forecast_designator;
      l_forecast_designator(l_dndx).inventory_item_id := l_forecast_interface(i).inventory_item_id;
    End If;
  End Loop;
  l_success := MRP_FORECAST_INTERFACE_PK.MRP_Forecast_Interface(forecast_interface  => l_forecast_interface
                                                               ,forecast_designator => l_forecast_designator);
*/

  l_step := 16.0;
  l_success := MRP_FORECAST_INTERFACE_PK.MRP_Forecast_Interface(forecast_interface  => l_forecast_interface);

  l_step := 17.0;
  If Not l_success Then
    l_step := 18.0;
    FND_FILE.Put_Line(FND_FILE.Log, 'Errors from call to MRP_FORECAST_INTERFACE_PK.MRP_Forecast_Interface');
    For i In 1..l_ndx Loop
      l_step := 19.0;
      If l_forecast_interface(i).error_message Is Not NULL Then
        l_step := 20.0;
        Update XXWC.XXWCINV_DEMAND_FORECAST_IFACE
           Set error_message = l_forecast_interface(i).error_message
         Where organization_id = l_forecast_interface(i).organization_id
           And forecast_designator = l_forecast_interface(i).forecast_designator
           And inventory_item_id = l_forecast_interface(i).inventory_item_id
           And forecast_date = l_forecast_interface(i).forecast_date;
        FND_FILE.Put_Line(FND_FILE.Log, l_forecast_interface(i).organization_id||' '||
                                        l_forecast_interface(i).forecast_designator||' '||
                                        l_forecast_interface(i).inventory_item_id||' '||
                                        l_forecast_interface(i).forecast_date||' '||
                                        l_forecast_interface(i).error_message);
      End If;
    End Loop;
    COMMIT;
    errbuf := 'Error in API Call';
    retcode := 2;
  End If;

  -- generate error report for users
  l_cnt_err := 0;
  FND_FILE.Put_Line(FND_FILE.Output, 'Org Code  Forecast Designator  Item Number    Bucket Type  Forecast Date  Quantity  Error Message');
  FND_FILE.Put_Line(FND_FILE.Output, '--------  -------------------  -------------  -----------  -------------  --------  --------------------------------');
  For R2 in C_Errors Loop
    l_cnt_err := l_cnt_err + 1;
  FND_FILE.Put_Line(FND_FILE.Output, R2.item_number||'  '||
                                     R2.organization_code||'  '||
                                     R2.bucket_type||'  '||
                                     R2.forecast_designator||'  '||
                                     R2.forecast_date||'  '||
                                     R2.quantity||'  '||
                                     R2.error_message);
  End Loop;

  FND_FILE.Put_Line(FND_FILE.Log, l_cnt_err||' records has errors');
  If l_cnt_err = 0 Then
    FND_FILE.Put_Line(FND_FILE.Output, 'No Errors Found');
  End If;

  -- clear out interface table
  l_step := 21.0;
  Begin
    Delete From XXWC.XXWCINV_DEMAND_FORECAST_IFACE DF;
    COMMIT;
  End;
  errbuf := 'Success';
  retcode := 0;

  FND_FILE.Put_Line(FND_FILE.Log, l_cnt||' records processed');
Exception
  When XXWC_ERROR Then
    ROLLBACK;
  When OTHERS Then
    FND_FILE.Put_Line(FND_FILE.Log, 'Error in Load_Forecast at '||l_step);
    FND_FILE.Put_Line(FND_FILE.Log, SQLERRM);
    errbuf := SQLERRM;
    retcode := 2;
End Load_Forecast;


/*******************************************************************************************/
/* Get_Demand : internal function to determine historical demand average                   */
/*******************************************************************************************/
Function Get_Avg_Demand(p_org_id          IN  NUMBER,
                        p_item_id         IN  NUMBER,
                        p_start_date      IN  DATE,
                        p_end_date        IN  DATE, --added 1/9/2013 Lee Spitzer
                        p_dc_mode         IN  VARCHAR2,
                        x_src_orgs        OUT VARCHAR2) RETURN NUMBER Is

--Sourcing Rules if using ASCP
/*
  Cursor C_Sourced_Items Is
    Select SRA.organization_id,
           MP.organization_code
      from MRP_SOURCING_RULES SR,
           MRP_SR_RECEIPT_ORG_V SRO,
           MRP_SR_SOURCE_ORG_V SSO,
           MRP_SR_ASSIGNMENTS_V SRA,
           MTL_PARAMETERS MP
     Where SR.sourcing_rule_id = SRO.sourcing_rule_id
       And Trunc(SYSDATE) Between SRO.effective_date and Nvl(disable_date, SYSDATE+1)
       And SRO.sr_receipt_id = SSO.sr_receipt_id
       And SRA.sourcing_rule_id = SR.sourcing_rule_id
       And SRA.inventory_item_id = p_item_id
       And SSO.source_type = 1    -- 1=transfer-from  3=buy-from
       And SRA.assignment_type = 6
       And SRA.organization_id = MP.organization_id
       And SSO.source_organization_id = p_org_id
     Order By SRA.organization_id;

*/
--Sourcing from Item Master
    Cursor C_Sourced_Items Is
    select msib.organization_id,
           mp.organization_code
    from   mtl_system_items_b msib,
           mtl_parameters mp
    where  msib.source_organization_id = p_org_id
    and    msib.source_type = 1 --Internally_Sourced
    and    msib.inventory_item_id = p_item_id
    AND    msib.source_organization_id = mp.organization_id
    AND    msib.source_organization_id != msib.organization_id; --added 2/26/2013 to prevent duplication of self source items     


  l_demand                             NUMBER := 0;
  l_src_demand                         NUMBER := 0;
    --l_src_orgs                           VARCHAR2(80) := NULL; --removed 4/11/2013 due to pl/sql buffer string too small
  l_src_orgs                           VARCHAR2(10000) := NULL; --added 4/11/2013 to fix pl/sql buffer string

Begin

  BEGIN
    --Select Nvl(Sum(sales_order_demand),0) --removed 2/26/2013 to include WIP usage
    Select Nvl(Sum(sales_order_demand),0) + NVL(sum(std_wip_usage),0) --added 2/26/2013 to include WIP Usage
      Into l_demand
      From MTL_DEMAND_HISTORIES
     Where organization_id = p_org_id
       AND inventory_item_id = p_item_id
       AND period_start_date >= p_start_date
       AND period_start_date < p_end_date; --Added 1/9/2013 Lee Spitzer
        
  Exception
    When OTHERS Then
      l_demand := 0;
  End;

  FND_FILE.Put_Line(FND_FILE.Log, 'p_item_id '|| p_item_id || ' p_org_id ' || p_org_id || ' l_demand '|| l_demand); --Used for Debugging
          

  If p_dc_mode = 'DC' Then
    For R1 in C_Sourced_Items Loop
      BEGIN
        --Select nvl(Sum(sales_order_demand),0) --added 11/30/2012 --removed 2/26/2013 to include WIP Usage
               --sum(sales_order_demand),0) --removed 11/30/2012
              select Nvl(Sum(sales_order_demand),0) + NVL(sum(std_wip_usage),0) --added 2/26/2013 to include WIP Usage
                Into l_src_demand
                From MTL_DEMAND_HISTORIES
               Where organization_id = R1.organization_id
                 AND inventory_item_id = p_item_id
                 AND period_start_date >= p_start_date
                 AND period_start_date < p_end_date; --Added 1/9/2013 Lee Spitzer


        If l_src_orgs Is NULL Then
          l_src_orgs := R1.organization_code;
        Else
          l_src_orgs := l_src_orgs||','||R1.organization_code;
        End If;
      Exception
        When NO_DATA_FOUND Then
          l_src_demand := 0;
      End;

      FND_FILE.Put_Line(FND_FILE.Log, 'p_item_id '|| p_item_id || ' r1.organization_id ' || r1.organization_id || ' l_src_demand '|| l_src_demand); --Used for Debugging
 
      l_demand := l_demand + l_src_demand;

    End Loop;

    x_src_orgs := l_src_orgs;
  End If;

  If l_demand <= 0 Then
    Return 0;
  Else
    Return(l_demand);
  End If;
Exception
  When OTHERS Then
    Return 0;
End Get_Avg_Demand;

/*******************************************************************************************/
/* Get_History_Start_Date : function to determine start date for demand history            */
/*******************************************************************************************/
Function Get_History_Start_Date(p_calendar_code  IN  VARCHAR2,
                                p_start_date     IN  DATE,
                                p_bucket_type    IN  NUMBER,
                                p_prev_periods   IN  NUMBER) Return Date Is

  -- use 3 selects based upon bucket type in single cursor
  Cursor C_Dates Is
    Select period_start_date
      From BOM_PERIOD_START_DATES
     Where calendar_code = p_calendar_code
       And period_start_date <= p_start_date
       And p_bucket_type = 3
    UNION
    Select week_start_date
      From BOM_CAL_WEEK_START_DATES
     Where calendar_code = p_calendar_code
       And week_start_date <= p_start_date
       And p_bucket_type = 2
    UNION
    Select calendar_date
      From BOM_CALENDAR_DATES
     Where calendar_code = p_calendar_code
       And calendar_date <= p_start_date
       And p_bucket_type = 1
     Order by 1 DESC;

  l_hist_date                          DATE := p_start_date;  -- default starting history date to p_start_date
  l_date                               DATE;

Begin

  -- open cursor based upon bucket type
  Open C_Dates;

  -- for the necessary number of iterations (skip current bucket date range), loop thru fetched records
  For indx In 1..p_prev_periods+1 Loop
    Fetch C_Dates Into l_date;

    -- if number of previous buckets exceeds number available, return earliest date found
    Exit When C_Dates%NOTFOUND;

    -- assign fetched date into return variable after successful fetch
    l_hist_date := l_date;
  End Loop;

  Close C_Dates;

  Return l_hist_date;

End Get_History_Start_Date;

/*******************************************************************************************/
/* Generate_Forecast : procedure to generate demand forecast based upon historical demand  */
/*******************************************************************************************/
Procedure Generate_Forecast(errbuf            OUT VARCHAR2,
                            retcode           OUT VARCHAR2,
                            p_org_id          IN  NUMBER,
                            p_forecast        IN  VARCHAR2,
                            p_dc_mode         IN  VARCHAR2,
                            p_item_range      IN  VARCHAR2,
                            p_item_category   IN  VARCHAR2,
                            p_item_id         IN  NUMBER DEFAULT NULL,
                            p_period_type     IN  NUMBER DEFAULT 3,   -- 3=PERIODS
                            p_start_date      IN  VARCHAR2,
                            p_forecast_periods IN  NUMBER DEFAULT 12,
                            p_prev_periods    IN  NUMBER DEFAULT 12,
                            p_seas_constant   IN  NUMBER DEFAULT 0, --Only Used Parameters to help default Seas_fact 1 through 12
                            p_seas_factor1    IN  NUMBER DEFAULT 0,
                            p_seas_factor2    IN  NUMBER DEFAULT 0,
                            p_seas_factor3    IN  NUMBER DEFAULT 0,
                            p_seas_factor4    IN  NUMBER DEFAULT 0,
                            p_seas_factor5    IN  NUMBER DEFAULT 0,
                            p_seas_factor6    IN  NUMBER DEFAULT 0,
                            p_seas_factor7    IN  NUMBER DEFAULT 0,
                            p_seas_factor8    IN  NUMBER DEFAULT 0,
                            p_seas_factor9    IN  NUMBER DEFAULT 0,
                            p_seas_factor10   IN  NUMBER DEFAULT 0,
                            p_seas_factor11   IN  NUMBER DEFAULT 0,
                            p_seas_factor12   IN  NUMBER DEFAULT 0) Is

  Cursor C_Forecast_Items Is
    -- retrieve items within forecast designator
    Select MSI.inventory_item_id,
           MSI.segment1 item_number
      From MRP_FORECAST_ITEMS MFI,
           MTL_SYSTEM_ITEMS_B MSI,
           MTL_ITEM_CATEGORIES MIC,
           MTL_CATEGORIES_KFV MC,
           MTL_CATEGORY_SETS MCS
     Where MFI.organization_id = p_org_id
       And MFI.forecast_designator = p_forecast
       And MFI.organization_id = MSI.organization_id
       And MFI.inventory_item_id = MSI.inventory_item_id
       And MFI.inventory_item_id = Nvl(p_item_id, MFI.inventory_item_id)
       And MSI.inventory_item_id = MIC.inventory_item_id
       And MSI.organization_id = MIC.organization_id
       And MIC.category_id = MC.category_id
       And MIC.category_set_id = MCS.category_set_id
       And MCS.category_set_name = 'Inventory Category'
       And MCS.structure_id = MC.structure_id
       And MC.concatenated_segments = Nvl(p_item_category, MC.concatenated_segments)
       And p_item_range = 'FORECAST'
    UNION
    -- retrieve items within branch
    Select MSI.inventory_item_id,
           MSI.segment1 item_number
      From MTL_SYSTEM_ITEMS_B MSI,
           MTL_ITEM_CATEGORIES MIC,
           MTL_CATEGORIES_KFV MC,
           MTL_CATEGORY_SETS MCS
     Where MSI.organization_id = p_org_id
       And MSI.inventory_item_id = Nvl(p_item_id, MSI.inventory_item_id)
       And MSI.inventory_item_id = MIC.inventory_item_id
       And MSI.organization_id = MIC.organization_id
       And MIC.category_id = MC.category_id
       And MIC.category_set_id = MCS.category_set_id
       And MCS.category_set_name = 'Inventory Category'
       And MCS.structure_id = MC.structure_id
       And MC.concatenated_segments = Nvl(p_item_category, MC.concatenated_segments)
       And p_item_range = 'BRANCH'
     Order By 2;

  l_period_avg                         NUMBER := 0;
  l_start_date                         DATE := FND_DATE.Canonical_To_Date(p_start_date);
  l_forecast_periods                   NUMBER := p_forecast_periods;
  l_prev_periods                       NUMBER := p_prev_periods;
  l_first_forecast_date                DATE;
  l_last_forecast_date                 DATE;
  l_next_forecast_date                 DATE;
  l_hist_date                          DATE;
    --l_src_orgs                           VARCHAR2(80) := NULL; --removed 4/11/2013 due to pl/sql buffer string too small
  l_src_orgs                           VARCHAR2(10000) := NULL; --added 4/11/2013 to fix pl/sql buffer string

  -- global variables for MRP Calendar functions
  l_calendar_code                      VARCHAR2(10);
  l_exception_set_id                   NUMBER;
  l_min_date                           DATE;
  l_max_date                           DATE;

  -- variables for MRP_FORECAST API call
  l_success                            BOOLEAN := TRUE;
  l_forecast_interface                 MRP_FORECAST_INTERFACE_PK.t_forecast_interface;
  l_forecast_designator                MRP_FORECAST_INTERFACE_PK.t_forecast_designator;

  l_ndx                                NUMBER := 0;
  l_cnt                                NUMBER := 0;
  l_pos                                NUMBER := 0;

  l_average_total                      NUMBER := 0;
Begin

  l_pos := 1.0;
  FND_FILE.Put_Line(FND_FILE.Log, 'P_ORG_ID = '||p_org_id);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_FORECAST = '||p_forecast);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_DC_MODE = '||p_dc_mode);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_ITEM_CATEGORY = '||p_item_category);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_ITEM_RANGE = '||p_item_range);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_ITEM_ID = '||p_item_id);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_PERIOD_TYPE = '||p_period_type);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_START_DATE = '||l_start_date);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_FORECAST_PERIODS= '||p_forecast_periods);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_PREV_PERIODS = '||p_prev_periods);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR1 = '||p_seas_factor1);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR2 = '||p_seas_factor2);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR3 = '||p_seas_factor3);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR4 = '||p_seas_factor4);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR5 = '||p_seas_factor5);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR6 = '||p_seas_factor6);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR7 = '||p_seas_factor7);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR8 = '||p_seas_factor8);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR9 = '||p_seas_factor9);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR10 = '||p_seas_factor10);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR11 = '||p_seas_factor11);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR12 = '||p_seas_factor12);
  FND_FILE.Put_Line(FND_FILE.Log, '======================');

  l_pos := 2.0;

  -- initialize inventory calendar information for organization
  MRP_CALENDAR.Select_Calendar_Defaults(p_org_id, l_calendar_code, l_exception_set_id);

  -- call function to determine demand history start date
  l_hist_date := Get_History_Start_Date(l_calendar_code, l_start_date, p_period_type, p_prev_periods);

  -- call function to set start date to working date
  l_start_date := MRP_CALENDAR.Prev_Work_Day(p_org_id, 1, l_start_date);

  -- validate cutoff date is within 12 weeks or periods
  l_pos := 3.0;
  If p_forecast_periods > 12 Then
    FND_FILE.Put_Line(FND_FILE.Log, 'Forecast Periods exceeds 12 periods, resetting to 12 periods');
    l_forecast_periods := 12;
  ElsIf p_forecast_periods < 1 Then
    FND_FILE.Put_Line(FND_FILE.Log, 'Forecast Periods less than 1 period, resetting to 1 period');
    l_forecast_periods := 1;
  Else
    l_forecast_periods := p_forecast_periods;
  End If;

  l_pos := 4.0;
  If p_prev_periods < 1 Then
    FND_FILE.Put_Line(FND_FILE.Log, 'Previous Periods less than 1 period, resetting to 1 period');
    l_prev_periods := 1;
  Else
    l_prev_periods := p_prev_periods;
  End If;

  FND_FILE.Put_Line(FND_FILE.Log, '======================');
  FND_FILE.Put_Line(FND_FILE.Log, 'Adjusted Start Date = '||l_start_date);
  FND_FILE.Put_Line(FND_FILE.Log, 'History start date = '||l_hist_date);
  FND_FILE.Put_Line(FND_FILE.Log, '======================');

  l_first_forecast_date := MRP_CALENDAR.Date_Offset(p_org_id, p_period_type, MRP_CALENDAR.Date_Offset(p_org_id, p_period_type, l_start_date, -1), 1);

  FND_FILE.Put_Line(FND_FILE.Output, Lpad('XXWC Generate Forecast',19));
  FND_FILE.Put_Line(FND_FILE.Output, ' ');
  FND_FILE.Put_Line(FND_FILE.Output, 'Forecast : '||p_forecast);
  FND_FILE.Put_Line(FND_FILE.Output, ' ');
  FND_FILE.Put_Line(FND_FILE.Output,Rpad('Item Number',40)||' '||
                                    Lpad('AMU',8)||' '||
                                    Rpad('Source Organizations',40));
  FND_FILE.Put_Line(FND_FILE.Output,Rpad('-----',40,'-')||' '||
                                    Lpad('-',8,'-')||' '||
                                    Rpad('-----',40,'-'));

  -- for all items selected in org/forecast
  l_pos := 5.0;
  For R_Item in C_Forecast_Items Loop

    -- get start date of current period
    l_pos := 6.0;
    l_next_forecast_date := l_first_forecast_date;

    -- determine monthly average demand for item
    l_pos := 7.0;
    l_period_avg := nvl(Round(Get_Avg_Demand(p_org_id, R_Item.inventory_item_id, l_hist_date, l_start_date, p_dc_mode, l_src_orgs) / l_prev_periods),0); --Added 1/9/2013 for end date parameter 
    --l_period_avg := nvl(Round(Get_Avg_Demand(p_org_id, R_Item.inventory_item_id, l_hist_date, p_dc_mode, l_src_orgs) / l_prev_periods),0); --Removed 1/9/2013 and replaced with above line , Added 11/29/2012 to fix blank AMU 
    --l_period_avg := (Round(Get_Avg_Demand(p_org_id, R_Item.inventory_item_id, l_hist_date, p_dc_mode, l_src_orgs) / l_prev_periods); --Removed 11/29/2012 to fix blank AMU

     FND_FILE.Put_Line(FND_FILE.Log, 'l_period_avg before filter factor ' || l_period_avg);
          
        --Added 12/17/2012 for AMU Filter Factor, 1/9/2013 with end date parameter
        
        l_period_avg := nvl(Round(AMU_FILTER_FACTOR(p_org_id, r_item.inventory_item_id, l_hist_date, l_start_date, p_dc_mode, l_period_avg)/l_prev_periods),0);
        
     
     FND_FILE.Put_Line(FND_FILE.Log, 'l_period_avg after filter factor ' || l_period_avg);   
    
    l_forecast_interface.DELETE;
    l_forecast_designator.DELETE;

    -- generate demand forecast records based upon periods between start date and cutoff date
    l_pos := 8.0;

    --Resetting Average Total --added 11/30/2012
    l_average_total := 0; 
    
    For l_ndx In 1..l_forecast_periods Loop

      l_cnt := l_cnt + 1;

      l_pos := 9.0;
      -- populate API record type
      l_forecast_interface(l_ndx).inventory_item_id := R_Item.inventory_item_id;
      l_forecast_interface(l_ndx).forecast_designator := p_forecast;
      l_forecast_interface(l_ndx).organization_id := p_org_id;
      l_forecast_interface(l_ndx).forecast_date := l_next_forecast_date;
      
      
      l_pos := 10.0;  
      Case l_ndx
        When 1 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor1)/100)),0);
        When 2 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor2)/100)),0);
        When 3 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor3)/100)),0);
        When 4 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor4)/100)),0);
        When 5 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor5)/100)),0);
        When 6 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor6)/100)),0);
        When 7 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor7)/100)),0);
        When 8 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor8)/100)),0);
        When 9 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor9)/100)),0);
        When 10 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor10)/100)),0);
        When 11 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor11)/100)),0);
        When 12 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor12)/100)),0);
      End Case;
  
      --FND_FILE.Put_Line(FND_FILE.Log, 'l_forecast_interface('||l_ndx||').quantity '|| l_forecast_interface(l_ndx).quantity); --Added 11/30/2012
       
      l_average_total := nvl(l_average_total,0) + nvl(l_forecast_interface(l_ndx).quantity,0); --Added 11/30/2012
      
      l_pos := 11.0;
      l_forecast_interface(l_ndx).confidence_percentage := 100;
--      l_forecast_interface(l_ndx).transaction_id := l_transaction_id;

      l_forecast_interface(l_ndx).process_status := 2;
      l_forecast_interface(l_ndx).workday_control := 2;           -- 1=reject  2=shift forword  3=shift backwards
      l_forecast_interface(l_ndx).bucket_type := p_period_type;   -- 1=days  2=weeks  3=periods

      l_forecast_interface(l_ndx).comments := 'Created from demand history';
      l_forecast_interface(l_ndx).source_code := NULL;
      l_forecast_interface(l_ndx).source_line_id := NULL;

      l_forecast_interface(l_ndx).last_update_date := SYSDATE;
      l_forecast_interface(l_ndx).last_updated_by := FND_GLOBAL.user_id;
      l_forecast_interface(l_ndx).creation_date := SYSDATE;
      l_forecast_interface(l_ndx).created_by := FND_GLOBAL.user_id;
      l_forecast_interface(l_ndx).last_update_login := NULL;

      -- setup information for next loop iteration
      l_pos := 12.0;
      l_next_forecast_date := MRP_CALENDAR.Date_Offset(p_org_id, p_period_type, l_next_forecast_date, 1);

    End Loop;  -- forecast period loop

    l_pos := 13.0;
    --FND_FILE.Put_Line(FND_FILE.Log, 'Generated forecast date records for '||R_Item.item_number||' with avg demand '||l_period_avg); --Removed 11/30/2012
    FND_FILE.Put_Line(FND_FILE.Output,Rpad(R_Item.item_number,40)||' '||
                                      Lpad(To_Char(l_period_avg,'999,990'),8)||' '||
                                      Rpad(l_src_orgs,40));

    -- if any records processed, then execute MRP API to generate demand forecast
    l_pos := 14.0;
    -- this API call will remove current forecast date information and replace with new forecast date information
    l_forecast_designator(1).organization_id := p_org_id;
    l_forecast_designator(1).forecast_designator := p_forecast;
    l_forecast_designator(1).inventory_item_id := R_Item.inventory_item_id;

    l_success := MRP_FORECAST_INTERFACE_PK.MRP_Forecast_Interface(forecast_interface  => l_forecast_interface
                                                                 ,forecast_designator => l_forecast_designator);

    -- if any error occured, generate error messages
    l_pos := 15.0;
      For i In 1..l_ndx Loop
        If l_forecast_interface(i).error_message Is Not NULL Then
          l_success := FALSE;
          FND_FILE.Put_Line(FND_FILE.Log, l_forecast_interface(i).organization_id||' '||
                                          l_forecast_interface(i).forecast_designator||' '||
                                          l_forecast_interface(i).inventory_item_id||' '||
                                          l_forecast_interface(i).forecast_date||' '||
                                          l_forecast_interface(i).error_message);
        End If;
      End Loop;
    If Not l_success Then
      FND_FILE.Put_Line(FND_FILE.Log, 'Errors from call to MRP_FORECAST_INTERFACE_PK.MRP_Forecast_Interface');
      FND_FILE.Put_Line(FND_FILE.Log, 'Terminating process');
      errbuf := 'Error in API Call';
      retcode := 2;
      Raise XXWC_ERROR;
    Else
      
      --Added 11/30/2012
      Case 
        When L_Forecast_Periods <= 0 Then L_Period_Avg := 0 ;
        When L_Forecast_Periods > 0 THEN  l_period_avg := round(L_Average_Total / L_Forecast_Periods) ;
      END CASE;
      --End Added 11/30/2012
      
      Case p_period_type
        When 1 Then l_period_avg := l_period_avg * 28 ;       -- Days
        When 2 Then l_period_avg := l_period_avg * 4 ;        -- Weeks
        When 3 Then NULL;                                     -- Periods
      End Case;
      -- sucessful call to API, so update item with average demand quantity
      FND_FILE.Put_Line(FND_FILE.Log, 'Generated forecast date records for '||R_Item.item_number||' with avg demand '||l_period_avg); --Added 11/30/2012
      
      Update MTL_SYSTEM_ITEMS_B
         Set attribute20 = l_period_avg
       Where inventory_item_id = R_Item.inventory_item_id
         And organization_id = p_org_id;
      
      COMMIT;
    End If;

    l_pos := 16.0;

  End Loop;  -- item loop

  errbuf := 'Success';
  retcode := 0;

  FND_FILE.Put_Line(FND_FILE.Log, l_cnt||' records processed');
  FND_FILE.Put_Line(FND_FILE.Output,  ' ');
  FND_FILE.Put_Line(FND_FILE.Output,  l_cnt||' records processed');
Exception
  When XXWC_ERROR Then
    ROLLBACK;
  When OTHERS Then
    ROLLBACK;
    errbuf := 'Error';
    retcode := 2;
    FND_FILE.Put_Line(FND_FILE.Log, 'Error at position '||l_pos);
    FND_FILE.Put_Line(FND_FILE.Log, SQLERRM);
End Generate_Forecast;

/*******************************************************************************************/
/* Generate_Forecast : procedure to generate demand forecast based upon historical demand  */
/*******************************************************************************************/
Procedure Copy_Forecast(errbuf            OUT VARCHAR2,
                        retcode           OUT VARCHAR2,
                        p_org_id          IN  NUMBER,
                        p_forecast        IN  VARCHAR2,
                        p_dc_mode         IN  VARCHAR2,
                        p_src_item_id     IN  NUMBER,
                        p_dest_item_id    IN  NUMBER,
                        p_period_type     IN  NUMBER DEFAULT 3,   -- 3=PERIODS
                        p_start_date      IN  VARCHAR2,
                        p_forecast_periods IN  NUMBER DEFAULT 12,
                        p_prev_periods    IN  NUMBER DEFAULT 12,
                        p_seas_factor1    IN  NUMBER DEFAULT 0,
                        p_seas_factor2    IN  NUMBER DEFAULT 0,
                        p_seas_factor3    IN  NUMBER DEFAULT 0,
                        p_seas_factor4    IN  NUMBER DEFAULT 0,
                        p_seas_factor5    IN  NUMBER DEFAULT 0,
                        p_seas_factor6    IN  NUMBER DEFAULT 0,
                        p_seas_factor7    IN  NUMBER DEFAULT 0,
                        p_seas_factor8    IN  NUMBER DEFAULT 0,
                        p_seas_factor9    IN  NUMBER DEFAULT 0,
                        p_seas_factor10   IN  NUMBER DEFAULT 0,
                        p_seas_factor11   IN  NUMBER DEFAULT 0,
                        p_seas_factor12   IN  NUMBER DEFAULT 0) Is

  l_period_avg                         NUMBER := 0;
  l_start_date                         DATE := FND_DATE.Canonical_To_Date(p_start_date);
  l_last_forecast_date                 DATE;
  l_next_forecast_date                 DATE;
  l_hist_date                          DATE;
  l_prev_periods                       NUMBER := p_prev_periods;
  l_forecast_periods                   NUMBER := p_forecast_periods;
    --l_src_orgs                           VARCHAR2(80) := NULL; --removed 4/11/2013 due to pl/sql buffer string too small
  l_src_orgs                           VARCHAR2(10000) := NULL; --added 4/11/2013 to fix pl/sql buffer string

  -- global variables for MRP Calendar functions
  l_calendar_code                      VARCHAR2(10);
  l_exception_set_id                   NUMBER;
  l_min_date                           DATE;
  l_max_date                           DATE;

  -- variables for MRP_FORECAST API call
  l_success                            BOOLEAN := TRUE;
  l_forecast_interface                 MRP_FORECAST_INTERFACE_PK.t_forecast_interface;
  l_forecast_designator                MRP_FORECAST_INTERFACE_PK.t_forecast_designator;

  l_cnt                                NUMBER := 0;

Begin

  FND_FILE.Put_Line(FND_FILE.Log, 'P_ORG_ID = '||p_org_id);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_FORECAST = '||p_forecast);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SRC_ITEM_ID = '||p_src_item_id);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_DEST_ITEM_ID = '||p_dest_item_id);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_PERIOD_TYPE = '||p_period_type);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_START_DATE = '||l_start_date);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_FORECAST_PERIODS = '||p_forecast_periods);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_PREV_PERIODS = '||p_prev_periods);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR1 = '||p_seas_factor1);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR2 = '||p_seas_factor2);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR3 = '||p_seas_factor3);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR4 = '||p_seas_factor4);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR5 = '||p_seas_factor5);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR6 = '||p_seas_factor6);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR7 = '||p_seas_factor7);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR8 = '||p_seas_factor8);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR9 = '||p_seas_factor9);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR10 = '||p_seas_factor10);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR11 = '||p_seas_factor11);
  FND_FILE.Put_Line(FND_FILE.Log, 'P_SEAS_FACTOR12 = '||p_seas_factor12);
  FND_FILE.Put_Line(FND_FILE.Log, '================================');

  -- ensure that source item and destination item are difference
  If p_src_item_id = p_dest_item_id Then
    FND_FILE.Put_Line(FND_FILE.Log, 'ERROR: Source Item must be different than Destination Item !!!');
    FND_FILE.Put_Line(FND_FILE.Log, '****  Process Terminating  ****');
    errbuf := 'Source Item must be different than Destination Item';
    retcode := 1;
    RAISE XXWC_ERROR;
  End If;

  If p_forecast_periods > 12 Then
    FND_FILE.Put_Line(FND_FILE.Log, 'Forecast Periods exceeds 12 periods, resetting to 12 periods');
    l_forecast_periods := 12;
  ElsIf p_forecast_periods < 1 Then
    FND_FILE.Put_Line(FND_FILE.Log, 'Forecast Periods less than 1 period, resetting to 1 period');
    l_forecast_periods := 1;
  Else
    l_forecast_periods := p_forecast_periods;
  End If;

  If p_prev_periods < 1 Then
    FND_FILE.Put_Line(FND_FILE.Log, 'Previous Periods less than 1 period, resetting to 1 period');
    l_prev_periods := 1;
  Else
    l_prev_periods := p_prev_periods;
  End If;

  MRP_CALENDAR.Select_Calendar_Defaults(p_org_id, l_calendar_code, l_exception_set_id);
  -- quick check to determine if prev_periods goes beyond calendar start
  --  a call to MRP_CALENDAR.Date_Offset that attempts to go beyond the calendar range will HANG
  Begin
    Select   /*+ index_ffs(bom) */ min(calendar_date), max(calendar_date)
        Into l_min_date, l_max_date
        From BOM_CALENDAR_DATES BOM
       Where calendar_code = l_calendar_code
         And seq_num is not null
       And exception_set_id = l_exception_set_id;

    If l_min_date > Add_Months(l_start_date, p_prev_periods*-1) Then
      FND_FILE.Put_Line(FND_FILE.Log, 'Number of Previous Periods pre-dates '||l_calendar_code||' calendar');
      FND_FILE.Put_Line(FND_FILE.Log, 'Change the number of previous periods to begin '||l_min_date||' or later');
      errbuf := 'Number of Previous Periods pre-dates '||l_calendar_code||' calendar';
      retcode := 1;
      RAISE XXWC_ERROR;
    End If;
  End;
  l_hist_date := MRP_CALENDAR.Date_Offset(p_org_id, p_period_type, l_start_date, l_prev_periods*-1);
  l_start_date := MRP_CALENDAR.Prev_Work_Day(p_org_id, 1, l_start_date);

  FND_FILE.Put_Line(FND_FILE.Log, '======================');
  FND_FILE.Put_Line(FND_FILE.Log, 'Adjusted Start Date = '||l_start_date);
  FND_FILE.Put_Line(FND_FILE.Log, 'History start date = '||l_hist_date);
  FND_FILE.Put_Line(FND_FILE.Log, '======================');


  -- determine monthly average demand for item
  l_period_avg := Round(Get_Avg_Demand(p_org_id, p_src_item_id, l_hist_date, l_start_date, p_dc_mode, l_src_orgs) / l_prev_periods); -- added 1/9/2013 for new end date parameter
  --l_period_avg := Round(Get_Avg_Demand(p_org_id, p_src_item_id, l_hist_date, p_dc_mode, l_src_orgs) / l_prev_periods); - removed 1/9/2013 for new end date parameter
  If l_src_orgs is Not NULL Then
    FND_FILE.Put_Line(FND_FILE.Log, 'Demand Usage computed based upon additional organizations: '||l_src_orgs);
  End If;
  -- get start date of current period
  l_next_forecast_date := MRP_CALENDAR.Date_Offset(p_org_id, p_period_type, MRP_CALENDAR.Date_Offset(p_org_id, p_period_type, l_start_date, -1), 1);

  -- generate demand forecast records based upon periods between start date and cutoff date
  For l_ndx In 1..l_forecast_periods Loop

    l_cnt := l_cnt + 1;

    -- populate API record type
    l_forecast_interface(l_ndx).inventory_item_id := p_dest_item_id;
    l_forecast_interface(l_ndx).forecast_designator := p_forecast;
    l_forecast_interface(l_ndx).organization_id := p_org_id;
    l_forecast_interface(l_ndx).forecast_date := l_next_forecast_date;
--    l_forecast_interface(l_ndx).forecast_end_date := MRP_CALENDAR.Date_Offset(p_org_id, p_period_type, l_next_forecast_date, 1) - 1;

    Case l_ndx
      When 1 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor1)/100)),0);
      When 2 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor2)/100)),0);
      When 3 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor3)/100)),0);
      When 4 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor4)/100)),0);
      When 5 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor5)/100)),0);
      When 6 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor6)/100)),0);
      When 7 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor7)/100)),0);
      When 8 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor8)/100)),0);
      When 9 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor9)/100)),0);
      When 10 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor10)/100)),0);
      When 11 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor11)/100)),0);
      When 12 Then l_forecast_interface(l_ndx).quantity := Greatest(Round(l_period_avg * ((100+p_seas_factor12)/100)),0);
    End Case;

    l_forecast_interface(l_ndx).confidence_percentage := 100;
--    l_forecast_interface(l_ndx).transaction_id := l_transaction_id;
    l_forecast_interface(l_ndx).process_status := 2;
    l_forecast_interface(l_ndx).workday_control := 2;           -- 1=reject  2=shift forword  3=shift backwards
    l_forecast_interface(l_ndx).bucket_type := p_period_type;   -- 1=days  2=weeks  3=periods

    l_forecast_interface(l_ndx).comments := NULL;
    l_forecast_interface(l_ndx).source_code := NULL;
    l_forecast_interface(l_ndx).source_line_id := NULL;

    l_forecast_interface(l_ndx).last_update_date := SYSDATE;
    l_forecast_interface(l_ndx).last_updated_by := FND_GLOBAL.user_id;
    l_forecast_interface(l_ndx).creation_date := SYSDATE;
    l_forecast_interface(l_ndx).created_by := FND_GLOBAL.user_id;
    l_forecast_interface(l_ndx).last_update_login := FND_GLOBAL.login_id;

    -- setup information for next loop iteration
    l_next_forecast_date := MRP_CALENDAR.Date_Offset(p_org_id, p_period_type, l_next_forecast_date, 1);

  End Loop;  -- forecast period loop

  FND_FILE.Put_Line(FND_FILE.Log, 'Generated '||p_forecast_periods||' forecast date records with avg demand '||Round(l_period_avg));

  -- this API call will remove current forecast date information and replace with new forecast date information
  l_forecast_designator(1).organization_id := p_org_id;
  l_forecast_designator(1).forecast_designator := p_forecast;
  l_forecast_designator(1).inventory_item_id := p_dest_item_id;

  l_success := MRP_FORECAST_INTERFACE_PK.MRP_Forecast_Interface(forecast_interface  => l_forecast_interface
                                                               ,forecast_designator => l_forecast_designator);

  -- if any error occured, generate error messages
    For i In 1..l_cnt Loop
      If l_forecast_interface(i).error_message Is Not NULL Then
        l_success := FALSE;
        FND_FILE.Put_Line(FND_FILE.Log, l_forecast_interface(i).organization_id||' '||
                                        l_forecast_interface(i).forecast_designator||' '||
                                        l_forecast_interface(i).inventory_item_id||' '||
                                        l_forecast_interface(i).forecast_date||' '||
                                        l_forecast_interface(i).error_message);
      End If;
    End Loop;
  If Not l_success Then
    FND_FILE.Put_Line(FND_FILE.Log, 'Errors from call to MRP_FORECAST_INTERFACE_PK.MRP_Forecast_Interface');
    errbuf := 'Error in API Call';
    retcode := 2;
    Raise XXWC_ERROR;
  Else
    Case p_period_type
      When 1 Then l_period_avg := l_period_avg * 28 ;       -- Days
      When 2 Then l_period_avg := l_period_avg * 4 ;        -- Weeks
      When 3 Then NULL;                                     -- Periods
    End Case;
    -- sucessful call to API, so update item with average demand quantity
    Update MTL_SYSTEM_ITEMS_B
       Set attribute20 = l_period_avg
     Where inventory_item_id = p_dest_item_id
       And organization_id = p_org_id;

    COMMIT;
  End If;

  errbuf := 'Success';
  retcode := 0;
  FND_FILE.Put_Line(FND_FILE.Log, 'Processing Complete');

Exception
  When XXWC_ERROR Then
    errbuf := 'Warning';
    retcode := 1;
    ROLLBACK;
  When OTHERS Then
    ROLLBACK;
    errbuf := 'Error';
    retcode := 2;
    FND_FILE.Put_Line(FND_FILE.Log, SQLERRM);
End Copy_Forecast;

 Function AMU_FILTER_FACTOR
                       (p_org_id          IN  NUMBER,
                        p_item_id         IN  NUMBER,
                        p_start_date      IN  DATE,
                        p_end_date        IN  DATE,
                        p_dc_mode         IN  VARCHAR2,
                        p_amu             IN NUMBER
                        ) RETURN NUMBER Is
 
 l_amu_filter_factor NUMBER := nvl(fnd_profile.value('XXWC_AMU_FILTER_FACTOR'),1);

--Sourcing Rules if using ASCP
/*
  Cursor C_Sourced_Items Is
    Select SRA.organization_id,
           MP.organization_code
      from MRP_SOURCING_RULES SR,
           MRP_SR_RECEIPT_ORG_V SRO,
           MRP_SR_SOURCE_ORG_V SSO,
           MRP_SR_ASSIGNMENTS_V SRA,
           MTL_PARAMETERS MP
     Where SR.sourcing_rule_id = SRO.sourcing_rule_id
       And Trunc(SYSDATE) Between SRO.effective_date and Nvl(disable_date, SYSDATE+1)
       And SRO.sr_receipt_id = SSO.sr_receipt_id
       And SRA.sourcing_rule_id = SR.sourcing_rule_id
       And SRA.inventory_item_id = p_item_id
       And SSO.source_type = 1    -- 1=transfer-from  3=buy-from
       And SRA.assignment_type = 6
       And SRA.organization_id = MP.organization_id
       And SSO.source_organization_id = p_org_id
     Order By SRA.organization_id;

*/

    

   cursor c_demand (lv_dc_mode in VARCHAR2)is
       select demand_history.PERIOD_START_DATE,
        SUM (demand_history.sales_order_demand) sales_order_demand
        FROM (
        --SELECT  'BRANCH' DC_MODE, msib.segment1, mp.organization_code, mdh.period_start_date, nvl(mdh.sales_order_demand,0) sales_order_demand --removed 2/26/2013
        SELECT  'BRANCH' DC_MODE, msib.segment1, mp.organization_code, mdh.period_start_date, nvl(mdh.sales_order_demand,0) + nvl(mdh.std_wip_usage,0) sales_order_demand --added 2/26/2013
        FROM MTL_DEMAND_HISTORIES MDH,
             mtl_parameters mp,
             mtl_system_items_b msib
        WHERE  mp.organization_id = msib.organization_id
        AND  mp.organization_id = p_org_id
        AND  msib.inventory_item_id = p_item_id
        and  mdh.organization_id = mp.organization_Id
        AND  mdh.inventory_item_id = msib.inventory_item_id
        AND  mdh.period_start_date >= p_start_date
        AND  mdh.period_start_date < p_end_date
        UNION
        --SELECT  'DC' DC_MODE, msib.segment1, mp2.organization_code, mdh.period_start_date, nvl(mdh.sales_order_demand,0) sales_order_demand --removed 2/26/2013
        SELECT  'DC' DC_MODE, msib.segment1, mp2.organization_code, mdh.period_start_date, nvl(mdh.sales_order_demand,0) + nvl(mdh.std_wip_usage,0) sales_order_demand --removed 2/26/2013
        FROM MTL_DEMAND_HISTORIES MDH,
             mtl_parameters mp,
             mtl_system_items_b msib,
             mtl_parameters mp2
        WHERE  mp.organization_id = msib.source_organization_id
        AND  mp.organization_id = p_org_id
        AND  msib.inventory_item_id = p_item_id
        AND  msib.organization_id = mp2.organization_id
        and  mp.organization_id != mp2.organization_id
        AND  msib.source_type = 1 --Internally Sourced
        and  mdh.organization_id = mp2.organization_Id
        AND  mdh.inventory_item_id = msib.inventory_item_id
        AND  mdh.period_start_date >= p_start_date
        AND  mdh.period_start_date < p_end_date) demand_history
        WHERE  demand_history.dc_mode in nvl(lv_dc_mode, demand_history.dc_mode) 
        group by demand_history.PERIOD_START_DATE;


  l_demand                             NUMBER DEFAULT 0;
  lv_dc_mode                           VARCHAR2(10);
  l_amu_max                            NUMBER DEFAULT 0;
  --l_amu_min                            NUMBER DEFAULT 0;
  l_total_demand                       NUMBER DEFAULT 0;

Begin


    if p_dc_mode = 'DC' then 
    
          lv_dc_mode := NULL;
          
    else 
    
          lv_dc_mode := 'BRANCH';
          
    end if;


    l_amu_max := nvl(p_amu,0) * nvl(l_amu_filter_factor,0);
    
    --l_amu_min := nvl(p_amu,0)- (nvl(l_amu_max,0) - nvl(p_amu,0)); 
  
    FND_FILE.Put_Line(FND_FILE.Log, 'p_item_id '|| p_item_id || ' p_org_id ' || p_org_id || ' l_amu_max '|| l_amu_max); --Used for Debugging
   -- FND_FILE.Put_Line(FND_FILE.Log, 'p_item_id '|| p_item_id || ' p_org_id ' || p_org_id || ' l_amu_min '|| l_amu_min); --Used for Debugging
          

    For R1 in c_demand (lv_dc_mode)  Loop
      Begin


      --  FND_FILE.Put_Line(FND_FILE.Log, 'period start date ' || r1.PERIOD_START_DATE);

      
            if r1.sales_order_demand > l_amu_max then
            
                l_demand := l_amu_max; 
                
            else
            
                l_demand := r1.sales_order_demand;
                
            end if;
             
           
      EXCEPTION
      
            WHEN OTHERS THEN
            
                 l_demand := 0;
      
      END;
          
        --FND_FILE.Put_Line(FND_FILE.Log, 'demand = ' || l_demand);

      
            l_total_demand := l_demand + l_total_demand;
            
        
       -- FND_FILE.Put_Line(FND_FILE.Log, 'total demand = ' || l_total_demand);

            
    end loop;
             
                    
    --FND_FILE.Put_Line(FND_FILE.Log, 'p_item_id '|| p_item_id || ' r1.organization_id ' || r1.organization_id || ' l_src_demand '|| l_src_demand); --Used for Debuggin


  If l_total_demand <= 0 Then
    Return 0;
  Else
    Return(l_total_demand);
  End If;
  Exception
  When OTHERS Then
    Return 0;
 
  END AMU_FILTER_FACTOR;


End XXWCMRP_DEMAND_FORECAST_PKG;