CREATE OR REPLACE PACKAGE BODY APPS.xxcus_ar_trx_exemption_pkb
  -- ------------------------------------------------------------------------------
  -- Copyright 2010 HD Supply  Inc (Orlando, FL) - All rights reserved
  --
  --
  --    NAME:       XXCUS_TM_MISC_PKG.pck
  --
  --    REVISIONS:
  --  ESMS/RFC          Ver        Date        Author           Description
  -- ----------         ---------  ----------  ---------------  -------------------------------
  --  ESMS 195714       1.0        06/17/2013  Balaguru Seshadri Installed. 
  --  Incident 557826    1.1       03/03/2015  Balaguru Seshadri Bug fix [Delete custom table runs as soon as the main routine
  --                                                             is invoked and handled exception no_data_found  @ location 301. 
  -- ------------------------------------------------------------------------------
AS
  PROCEDURE print_log(p_message in varchar2) IS
  BEGIN
   IF fnd_global.conc_request_id >0 THEN
    fnd_file.put_line(fnd_file.log, p_message);
   ELSE
    dbms_output.put_line(p_message);
   END IF;
  EXCEPTION
   WHEN OTHERS THEN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Issue in print_log routine ='||sqlerrm);
  END print_log;
  
  function get_exempt_amount (p_invoice_id in number, p_line_id in number) return number is
   n_exempt_amount number :=0;
  begin  
    select nvl(sum(prod_lines.extended_amount), 0)
    into   n_exempt_amount
    from   ra_customer_trx_lines prod_lines   
    where  1 =1
      and  prod_lines.customer_trx_id =p_invoice_id
      and  prod_lines.customer_trx_line_id =nvl(p_line_id, prod_lines.customer_trx_line_id)
      and  line_type ='LINE'
      and not exists
         (
           select customer_trx_id
           from   ra_customer_trx_lines tax_lines
           where  1 =1
             and  tax_lines.customer_trx_id          =p_invoice_id
             and  tax_lines.link_to_cust_trx_line_id =prod_lines.customer_trx_line_id
             and  tax_lines.line_type                ='TAX'
             and  tax_lines.extended_amount          <>0
         );
     return n_exempt_amount;
  exception
   when no_data_found then
    return n_exempt_amount;
   when others then
    print_log('Issue while fetching get_exempt_amount for customer_trx_id ='||p_invoice_id);
    return n_exempt_amount; 
  end get_exempt_amount;  
  
 procedure get_geo_maxtaxrate 
   (
     p_report_type    in varchar2
    ,p_invoice_source in varchar2   
    ,p_invoice_id     in number   --customer_trx_id
    ,p_prism_branch   in varchar2 --prism branch number
    ,p_state          in varchar2 --customer ship to state
    ,p_cityname       in varchar2 --customer ship to city
    ,p_zipcode        in varchar2 --customer ship to postal code 
    ,p_county         in varchar2 --customer ship to county 
    ,p_geo                out varchar2
    ,p_point_of_tax       out varchar2
    ,p_ship_via           out varchar2
    ,p_branch             out varchar2  
   ) is
  cursor get_txw_info (p_state in varchar2, p_cityname in varchar2, p_zipcode in varchar2, p_county in varchar2) is
  select min(geo_code) txw_geo, combined_rate*100 max_tax_rate, STNAME
    from (
    SELECT h.stalpha as stcode, h.stname AS STNAME, s.stname AS STATE, c.cntyname AS COUNTY, l.cityname AS CITY, 
      l.zipcode AS "ZIP CODE", l.geocode AS "GEO_CODE", x.stalphacode||l.zipcode||l.geocode as "OVERRIDE GEO",
      (s.cursalerate + s.curspecrate) AS "STATE RATE", 
      (c.cursalerate + c.curspecrate) AS "COUNTY RATE", 
      (l.cursalerate + l.curspecrate) AS "LOCAL RATE",
      (s.cursalerate + s.curspecrate + c.cursalerate + 
      c.curspecrate + l.cursalerate + l.curspecrate ) AS "COMBINED_RATE"
    FROM taxsttax s
      JOIN taxware.taxcntytax c USING (stcode)
      JOIN taxlocltax l USING (stcode, cntycode)
      join taxware.stepstcd_tbl x using(stcode)
      join TAXSTINFO h using (stcode)
       WHERE  1 = 1 
        and h.stalpha    =p_state
        and cityname     =p_cityname
        and l.zipcode    =p_zipcode        
        and c.cntyname   =nvl(p_county, c.cntyname) 
        order by s.stname, c.cntyname,l.cityname,l.zipcode,l.geocode
    )
    group by combined_rate*100, stname
    order by to_number(min(geo_code));
    
    i                  number       :=1; 
    v_point_of_tax     varchar2(40) :=Null;
    v_checkpoint_flag  varchar2(1)  :='N';
    v_ship_method      varchar2(80) :=Null;
    v_branch           varchar2(4)  :=Null;
    v_state            varchar2(60) :=Null;
    v_cityname         varchar2(60) :=Null;
    v_zipcode          varchar2(20) :=Null;
    v_county           varchar2(60) :=Null;
    v_geocode          varchar2(40) :=Null;
    n_header_id        number       :=Null;
    n_order_number     number       :=Null;
    G_state            varchar2(60) :=Null;
    G_cityname         varchar2(60) :=Null;
    G_zipcode          varchar2(20) :=Null;
    G_county           varchar2(60) :=Null; 
    G_run              varchar2(1)  :='N';
    v_ct_reference     ra_customer_trx_all.ct_reference%type :=Null; 
    p_tax              number;
    p_checkpoint_flag  varchar2(1);
    p_branch_city      varchar2(80);
    p_branch_state     varchar2(80);
    p_branch_zip       varchar2(80);
    p_header_id        number;
    p_order_number     number;
    p_branch_geocode   varchar2(80);             
  begin 
       -- get ship method, point of taxation, branch address info and branch geo code info. 
    If p_invoice_source IN ('PRISM', 'CONVERSION') Then
       Null;
    Elsif p_invoice_source IN ('ORDER MANAGEMENT', 'REPAIR OM SOURCE', 'STANDARD OM SOURCE') Then
       begin 

        select a.ship_method_meaning
              ,e.organization_code
              ,g.town_or_city
              ,g.region_2
              ,g.postal_code
              ,to_char(null) --have not seen a record in hr_locations view with county info for branch addresses
              ,g.loc_information13 --this is the geocode for the branch 
              ,nvl(
                     (
                       select 'ORIGIN'
                       from   fnd_lookup_values_vl
                       where  1 =1
                         and  lookup_type ='HDS_WILLCALL_METHODS'
                         and  meaning     =a.ship_method_meaning
                     )
                   ,'DESTINATION'
                  ) point_of_tax
               ,'Y' --We have retrieved either a destination based or origin based, lets just assign a flag for success
               ,b.header_id
               ,b.order_number 
        into  v_ship_method
             ,v_branch
             ,v_cityname             
             ,v_state
             ,v_zipcode
             ,v_county
             ,v_geocode
             ,v_point_of_tax
             ,v_checkpoint_flag
             ,n_header_id
             ,n_order_number
        from  ra_customer_trx_all d 
             ,oe_order_headers b
             ,wsh_carrier_services a         
             ,org_organization_definitions e 
             ,hr_all_organization_units f
             ,hr_locations_all g                
        where 1 =1
          and d.customer_trx_id          =p_invoice_id --in (375504, 1425351) --Invoice 50000017443
          and d.interface_header_context ='ORDER ENTRY'
          and b.order_number             =d.interface_header_attribute1 --use invoice header flexfield attribute1 which is order_number      
          and a.ship_method_code         =b.shipping_method_code
          and e.organization_id          =b.ship_from_org_id
          and f.organization_id          =e.organization_id
          and g.location_id              =f.location_id;
          
         -- assign output variables
         p_point_of_tax       :=v_point_of_tax;
         p_checkpoint_flag    :=v_checkpoint_flag;
         p_ship_via           :=v_ship_method;  
         p_branch             :=v_branch;
         p_branch_city        :=v_cityname;
         p_branch_state       :=v_state;
         p_branch_zip         :=v_zipcode;
         p_branch_geocode     :=v_geocode;
         p_header_id          :=n_header_id;
         p_order_number       :=n_order_number; 
               
       exception
        when no_data_found then
         p_point_of_tax       :='POT UNKNOWN';
         p_checkpoint_flag    :='N';
         p_ship_via           :=Null;  
         p_branch             :=Null;
         p_branch_city        :=Null;
         p_branch_state       :=Null;
         p_branch_zip         :=Null;
         p_branch_geocode     :=Null;
         p_geo                :='oerr1';
         p_tax                :=Null;
         p_header_id          :=Null;
         p_order_number       :=Null;                  
        when too_many_rows then
         p_point_of_tax       :='POT ERR1';
         p_checkpoint_flag    :='N';
         p_ship_via           :=Null;  
         p_branch             :=Null;
         p_branch_city        :=Null;
         p_branch_state       :=Null;
         p_branch_zip         :=Null;
         p_branch_geocode     :=Null;
         p_geo                :='oerr2';
         p_tax                :=Null; 
         p_header_id          :=Null;
         p_order_number       :=Null;                 
         print_log ('@get_geo_maxtaxrate, too many records in trying to find the point of tax');
        when others then
         p_point_of_tax       :='POT ERR2';
         p_checkpoint_flag    :='N';
         p_ship_via           :=Null;  
         p_branch             :=Null;
         p_branch_city        :=Null;
         p_branch_state       :=Null;
         p_branch_zip         :=Null;
         p_branch_geocode     :=Null;
         p_geo                :='oerr3';
         p_tax                :=Null;
         p_header_id          :=Null;
         p_order_number       :=Null;         
         print_log ('@get_geo_maxtaxrate trying to find the point of tax, when others '||sqlerrm);     
       end;  
       If p_point_of_tax ='DESTINATION' then
             
             If p_county ='0' Then
              G_county :=Null;
             Else
              G_county :=p_county;
             End If;
                         
             G_state              :=p_state;    --customer ship to state
             G_cityname           :=p_cityname; --customer ship to city
             G_zipcode            :=p_zipcode;  --customer ship to zipcode
             --G_county             :=p_county;   --customer ship to county
             G_run                :='Y';
       Elsif p_point_of_tax ='ORIGIN' then  --tax is origin based, so pass the branch address details
             G_state              :=UPPER(v_state);     --state where branch is located 
             G_cityname           :=UPPER(v_cityname);  --city where branch is located
             G_zipcode            :=v_zipcode;   --zipcode where branch is located
             G_county             :=UPPER(v_county);    --county where branch is located, verfied this field is not available in hr_locations_all table 
             G_run                :='Y';                              
       Else
             G_run                :='N';
       End If;
            
     --get tax rate
       begin 
          if G_run ='Y' then
                       for rec in get_txw_info(G_state, G_cityname, G_zipcode, G_county) loop 
                            if i =1 then 
                             p_geo :=rec.txw_geo;
                             p_tax :=rec.max_tax_rate;
                             i :=i+1;
                            else
                             null;
                            end if;
                                if i>1 then
                                 exit;
                                end if;
                       end loop;                             
                  else --this means p_point_of_tax is something other than ORIGIN or DESTINATION when report is sales tax recovery
                    p_geo :=Null;
                    p_tax :=Null;                               
                  end if; --end if for G_run ='Y' condition        
       exception
        when others then
          print_log ('@when looping to get maxtaxrate, other errors '||sqlerrm);   
             p_point_of_tax       :='NA';
             p_checkpoint_flag    :='N';
             p_ship_via           :=Null;  
             p_branch             :=Null;
             p_branch_city        :=Null;
             p_branch_state       :=Null;
             p_branch_zip         :=Null;
             p_branch_geocode     :=Null;
             p_geo                :='oerr8';
             p_tax                :=Null; 
             p_header_id          :=Null;
             p_order_number       :=Null;                             
       end;             
    Else
             p_point_of_tax       :=Null;
             p_checkpoint_flag    :=Null;
             p_ship_via           :=Null;  
             p_branch             :=Null;
             p_branch_city        :=Null;
             p_branch_state       :=Null;
             p_branch_zip         :=Null;
             p_branch_geocode     :=Null;
             p_geo                :=Null;
             p_tax                :=Null;
             p_header_id          :=Null;
             p_order_number       :=Null;
    End If;   
  exception
   when others then
    print_log ('@get_geo_maxtaxrate when others '||sqlerrm);   
         p_point_of_tax       :='NA';
         p_checkpoint_flag    :='N';
         p_ship_via           :=Null;  
         p_branch             :=Null;
         p_branch_city        :=Null;
         p_branch_state       :=Null;
         p_branch_zip         :=Null;
         p_branch_geocode     :=Null;
         p_geo                :='oerr9';
         p_tax                :=Null;
         p_header_id          :=Null;
         p_order_number       :=Null;               
  end get_geo_maxtaxrate ;

  PROCEDURE main (
    retcode               out number
   ,errbuf                out varchar2
   ,p_ledger_id           in  number
   ,p_period_name         in  varchar2   
  ) IS
  --
    CURSOR inv_ids (p_period in varchar2, p_from_gl_date in date, p_to_gl_date in date) is 
    select
         p_period      period
        ,to_char(null) div
        ,trx.trx_number inv_no
        ,trunc(trx.trx_date) inv_date
        ,a.account_number cust_num
        ,b.party_name customer_name
        ,to_number(null) inv_total
        ,to_number(null) freight
        ,to_number(null) sales_tx 
        ,to_number(null) non_txbl_amount
        ,to_char(null) exemption_reason
        ,to_char(null) exemption_product_details
        ,to_char(null) ship_to_name
        ,to_char(null) address1
        ,to_char(null) address2
        ,to_char(null) city
        ,to_char(null) state
        ,to_char(null) zip_code
        ,to_char(null) county
        ,to_char(null) geo
        ,to_char(null) pot
        ,to_char(null) ship_via
        ,trunc(trx.creation_date) clsd_date
        ,c.name terms
        ,trx.customer_trx_id customer_trx_id
        ,trx.ship_to_customer_id ship_to_customer_id
        ,trx.ship_to_site_use_id ship_to_site_use_id
        ,trx.bill_to_customer_id bill_to_customer_id
        ,trx.bill_to_site_use_id bill_to_site_use_id
        ,trx.cust_trx_type_id    cust_trx_type_id
        ,rbs.name      invoice_source          
    from  ra_customer_trx trx
         ,ra_batch_sources rbs 
         ,hz_cust_accounts a
         ,hz_parties b
         ,ra_terms_tl c
    where 1 =1
      and trx.batch_source_id not in (1004, 1005, 1006, 1007) --Exclude PRISM, PRISM REFUND, REBILL and REBILL-CM invoices 
      and a.cust_account_id =trx.bill_to_customer_id
      and b.party_id        =a.party_id
      and c.term_id(+)      =trx.term_id
      and rbs.batch_source_id =trx.batch_source_id      
      and exists
             (
                select 1
                from   apps.ar_payment_schedules_all
                where  1 =1
                  and  customer_trx_id  =trx.customer_trx_id
                  and  gl_date between p_from_gl_date and p_to_gl_date  --we need invoices that matches the period requested 
                  and  nvl(tax_original, 0) =0        
             );
    
    TYPE inv_ids_tbl IS TABLE OF inv_ids%ROWTYPE INDEX BY BINARY_INTEGER;
    inv_ids_row inv_ids_tbl;

    CURSOR my_exemptions IS
    SELECT exempt_line
    FROM   apps.xxcus_ar_trx_exempt_v;    
    
  lc_request_data VARCHAR2(20) :='';
  n_req_id NUMBER :=0;
  p_limit NUMBER :=5000;
  sql_text VARCHAR2(20000);
  --
  v_wrtoff_file_name varchar2(40);
  n_user_id NUMBER :=0;
  v_email       fnd_user.email_address%type :=Null;
  v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;
  --
  v_path VARCHAR2(80);
  ln_request_id NUMBER :=0;
  v_child_requests VARCHAR2(2);
  n_tbl_count Number :=0;
  --
  output_file_id   UTL_FILE.FILE_TYPE;
  v_audit_missing varchar2(1) :=Null;
  v_oracle_branch varchar2(3);
  n_ship_siteuse_id  number :=0;
  --
  n_ship_cust_id     number :=0;
  g_checkpoint_flag varchar2(1) :='N';
  v_trx_source ra_batch_sources_all.name%type;
  v_prism_branch varchar2(40);
  --
  v_prism_ship_method varchar2(40);
  b_move_fwd boolean;
  n_total_fetch number :=0;
  n_customer_trx_id number :=0;
  --
  v_wc_branch   Varchar2(10) :=Null;
  g_user_id number :=fnd_global.user_id;
  g_creation_date date :=sysdate;
  l_dollar_amount number;
  --
  l_payment_amount number;
  n_prev_cash_receipt_id number :=0;
  n_rec_app_id number :=0;
  n_rec_trx_id number :=0;
  --
  v_rcpt_wrt_off_name ar_receivable_applications_v.trx_number%type :=Null;
  v_rcpt_wrt_off_activity ar_receivable_applications_v.rec_activity_name%type :=Null;
  d_gl_from_date date;
  d_gl_to_date date;
  --
  v_ship_site_number varchar2(150);
  v_bill_site_number varchar2(150);
  v_enter varchar2(1) :='
';
  --
  l_err_msg  VARCHAR2(3000);
  l_err_code NUMBER;
  l_sec      VARCHAR2(255);
  --    
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_AR_TRX_EXEMPTION_PKB.MAIN';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';  
  --
  begin
   --
   -- =========================
   -- cleanup the current table
   -- =========================       
   begin 
     delete xxcus.xxcus_ar_trx_exempt_b;
   exception
     when others then
      l_err_callpoint :='@Delete of xxcus.xxcus_ar_trx_exempt_b';
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();      
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'WC AR TAX'); 
      --                                             
      print_log ('issue during clean up of the table xxcus.xxcus_ar_trx_exempt_b, '||sqlerrm);
      --
   end;   
   --
   begin 
    select gps.start_date, gps.end_date
    into   d_gl_from_date, d_gl_to_date
    from   gl_period_statuses gps
    where 1 =1
      and gps.application_id =222 --GL owned periods only
      and gps.period_name    =p_period_name
      and gps.ledger_id      =p_ledger_id;
      b_move_fwd :=TRUE;  
   exception
    when no_data_found then
     d_gl_from_date :=Null;
     d_gl_to_date   :=Null;
     b_move_fwd :=FALSE;
    when too_many_rows then
     d_gl_from_date :=Null;
     d_gl_to_date   :=Null;
     b_move_fwd :=FALSE;    
    when others then
     b_move_fwd :=FALSE;
     print_log('Issue in Period ='||p_period_name);
     raise_application_error(-20010, 'Error in fetching start and end dates for the period ='||sqlerrm);
   end;
   --
   n_req_id :=fnd_global.conc_request_id;
   --
   print_log('Inside main, p_ledger_id ='||p_ledger_id);
   print_log('Inside main, p_period_name ='||p_period_name);   
   print_log('');
   --    
   if (b_move_fwd) then
       open inv_ids(p_period_name, d_gl_from_date, d_gl_to_date); 
       fetch inv_ids bulk collect into inv_ids_row; 
       close inv_ids;
               
       print_log('Total AR Invoices with no taxes for '||p_period_name||' ='||inv_ids_row.count);
                 
         if inv_ids_row.count >0 then 
               --             
               for idx in inv_ids_row.first .. inv_ids_row.last loop 
               
                    Begin   
                        v_trx_source          :=inv_ids_row(idx).invoice_source;
                        n_ship_cust_id        :=inv_ids_row(idx).ship_to_customer_id;
                        n_ship_siteuse_id     :=inv_ids_row(idx).ship_to_site_use_id;
                        v_prism_branch        :=Null;
                        v_prism_ship_method   :=Null;                                                                                                                                            
                                         
                    Exception                                                
                        When Others Then
                         v_trx_source         :='*NO INV SRC*';
                         inv_ids_row(idx).invoice_source :=v_trx_source;
                         n_ship_cust_id       :=Null;
                         n_ship_siteuse_id    :=Null;
                         v_prism_branch       :=Null;
                         v_prism_ship_method  :=Null;                         
                    End;                                                   
               
                    begin
                       inv_ids_row(idx).non_txbl_amount :=get_exempt_amount(inv_ids_row(idx).customer_trx_id, Null);
                       select      a.cust_bill_to_number
                                  ,a.cust_bill_to_name
                                  ,a.whs_branch_name
                                  ,a.total_freight
                                  ,a.total_tax                  
                                  ,a.total_invoice 
                                  --,a.total_line_items
                                  ,a.cust_ship_to_name
                                  ,a.cust_ship_to_address1
                                  ,a.cust_ship_to_address2
                                  ,a.cust_ship_to_city
                                  ,a.cust_ship_to_state_prov
                                  ,a.cust_ship_to_zip_code 
                                  ,a.cust_ship_to_county
                                  ,a.cust_ship_to_site_number
                                  ,a.cust_bill_to_site_number
                                  ,a.customer_trx_id               
                       into   inv_ids_row(idx).cust_num
                                  ,inv_ids_row(idx).customer_name
                                  ,inv_ids_row(idx).div
                                  ,inv_ids_row(idx).freight
                                  ,inv_ids_row(idx).sales_tx
                                  ,inv_ids_row(idx).inv_total
                                  --,inv_ids_row(idx).non_txbl_amount
                                  ,inv_ids_row(idx).ship_to_name
                                  ,inv_ids_row(idx).address1
                                  ,inv_ids_row(idx).address2                                    
                                  ,inv_ids_row(idx).city
                                  ,inv_ids_row(idx).state
                                  ,inv_ids_row(idx).zip_code
                                  ,inv_ids_row(idx).county
                                  ,v_ship_site_number
                                  ,v_bill_site_number
                                  ,n_customer_trx_id                                                                                                            
                       from   xxwc.xxwcar_taxware_audit_tbl a            
                       where  1 =1
                         and  a.customer_trx_id =inv_ids_row(idx).customer_trx_id;                  
                    exception 
                     When No_Data_Found Then
                        n_customer_trx_id :=inv_ids_row(idx).customer_trx_id; 
                        inv_ids_row(idx).non_txbl_amount :=get_exempt_amount(n_customer_trx_id, Null); 
                          begin 
                             select hzp.party_name 
                                  ,case
                                    when a.interface_header_context ='ORDER ENTRY' 
                                      then
                                        (
                                          select organization_code
                                          from   org_organization_definitions
                                          where  1 =1
                                            and  to_char(organization_id) =a.interface_header_attribute10
                                        )
                                    when a.interface_header_context ='PRISM INVOICES' then (lpad(a.interface_header_attribute7, 3,'0'))
                                    when a.interface_header_context ='CONVERSION' 
                                           then (substr(lpad(substr(a.trx_number, 1, (instr(a.trx_number,'-')-1)), 9, '0'), 1, 3))
                                    else 'NA' 
                                   end whs_branch_name
                                  ,apsa.freight_original
                                  ,apsa.tax_original                 
                                  ,apsa.amount_due_original 
                                  --,apsa.amount_line_items_original
                                  ,hzl.address1 
                                  ,hzl.address2
                                  ,hzl.city
                                  ,hzl.state
                                  ,hzl.postal_code
                                  ,hzl.county
                                  ,hzps.party_site_number
                                  ,bhzps.party_site_number
                                  ,a.customer_trx_id 
                             into  inv_ids_row(idx).ship_to_name
                                  ,inv_ids_row(idx).div
                                  ,inv_ids_row(idx).freight
                                  ,inv_ids_row(idx).sales_tx
                                  ,inv_ids_row(idx).inv_total
                                  --,inv_ids_row(idx).non_txbl_amount
                                  ,inv_ids_row(idx).address1
                                  ,inv_ids_row(idx).address2
                                  ,inv_ids_row(idx).city
                                  ,inv_ids_row(idx).state
                                  ,inv_ids_row(idx).zip_code
                                  ,inv_ids_row(idx).county
                                  ,v_ship_site_number
                                  ,v_bill_site_number
                                  ,n_customer_trx_id                                  
                             from  ra_customer_trx_all a
                                   ,ar_payment_schedules_all apsa 
                                   ,hz_cust_accounts hzca
                                   ,hz_parties hzp
                                   ,hz_cust_site_uses_all hzcsu
                                   ,hz_party_sites hzps
                                   ,hz_cust_acct_sites_all hzcas
                                   ,hz_locations hzl  
                                   ,hz_cust_site_uses_all bhzcsu
                                   ,hz_party_sites bhzps
                                   ,hz_cust_acct_sites_all bhzcas
                                   ,hz_locations bhzl                                                                        
                             where  1 =1 
                               and  a.customer_trx_id         =n_customer_trx_id    
                               and  apsa.customer_trx_id(+)    =a.customer_trx_id
                               and  apsa.class(+)              ='INV'
                               and  hzca.cust_account_id(+)    =a.ship_to_customer_id  
                               and  hzp.party_id(+)            =hzca.party_id 
                               and  hzcsu.site_use_id(+)       =a.ship_to_site_use_id                                                       
                               and  hzcas.cust_account_id      =hzca.cust_account_id         
                               and  hzcas.cust_acct_site_id(+) =hzcsu.cust_acct_site_id          
                               and  hzps.party_site_id(+)     =hzcas.party_site_id
                               and  hzl.location_id(+)         =hzps.location_id
                               and  bhzcsu.site_use_id          =a.bill_to_site_use_id                                                       
                               and  bhzcas.cust_account_id      =a.bill_to_customer_id         
                               and  bhzcas.cust_acct_site_id    =bhzcsu.cust_acct_site_id          
                               and  bhzps.party_site_id         =bhzcas.party_site_id
                               and  bhzl.location_id            =bhzps.location_id;                                                                                                                                             
                          exception
                            when no_data_found then
                             Null;
                            when others then                            
                             print_log('@301, Customer_Trx_Id ='||n_customer_trx_id);
                             print_log('@301, Customer_Trx_Id, Message ='||sqlerrm);
                          end;                                           
                     When Too_many_rows then 
                        n_customer_trx_id :=inv_ids_row(idx).customer_trx_id;                    
                             print_log('@401, Customer_Trx_Id ='||n_customer_trx_id);
                             print_log('@401, Customer_Trx_Id, Message ='||sqlerrm);
                     When others then
                        n_customer_trx_id :=inv_ids_row(idx).customer_trx_id;                        
                             print_log('@501, Customer_Trx_Id ='||n_customer_trx_id);
                             print_log('@501, Customer_Trx_Id, Message ='||sqlerrm);                                          
                    end;
                    
                    If (inv_ids_row(idx).city Is Not Null) Then 
                         Null; --keep moving, the audit table has ship to info 
                    Else
                        --Looks like the custom table has no ship address info
                        -- use the fields from ar tables to get the ship to info
                         If (n_ship_siteuse_id Is Not Null) Then 
                           --print_log('n_ship_siteuse_id is not null, ship_city is blank, requery AR ['||n_ship_siteuse_id||'.'||n_ship_cust_id||']');
                          --If () Then --(v_audit_missing ='N') Then
                             Begin 
                                select hzl.address1
                                      ,hzl.address2
                                      ,hzl.city
                                      ,hzl.state
                                      ,hzl.postal_code
                                      ,hzl.county
                                      ,hzps.party_site_number
                                into   inv_ids_row(idx).address1
                                      ,inv_ids_row(idx).address2                                    
                                      ,inv_ids_row(idx).city
                                      ,inv_ids_row(idx).state
                                      ,inv_ids_row(idx).zip_code
                                      ,inv_ids_row(idx).county
                                      ,v_ship_site_number
                                from   hz_cust_site_uses_all hzcsu
                                      ,hz_party_sites hzps
                                      ,hz_cust_acct_sites_all hzcas
                                      ,hz_locations hzl
                                where  1 =1 
                                  and  hzcsu.site_use_id       =n_ship_siteuse_id                                                       
                                  and  hzcas.cust_account_id   =n_ship_cust_id --in ( 29738,92507)         
                                  and  hzcas.cust_acct_site_id =hzcsu.cust_acct_site_id          
                                  and  hzps.party_site_id      =hzcas.party_site_id
                                  and  hzl.location_id         =hzps.location_id;                                                                                     
                             Exception
                              When No_Data_Found Then
                               Null;
                              When Others Then
                               print_log('@601, Customer_Trx_Id ='||inv_ids_row(idx).customer_trx_id);
                               print_log('@601, Ship_to_customer_id ='||n_ship_cust_id); 
                               print_log('@601, Ship_to_site_use_id ='||n_ship_siteuse_id);
                               print_log('@601, Message ='||sqlerrm);
                             End;                          
                          --End If;
                         Else
                           inv_ids_row(idx).address1 :=Null;
                           inv_ids_row(idx).address2 :=Null; 
                           inv_ids_row(idx).city     :=Null;
                           inv_ids_row(idx).state    :=Null; 
                           inv_ids_row(idx).zip_code :=Null;
                           inv_ids_row(idx).county   :=Null; 
                         End If;
                    End If; 
                    
                    Begin
                        select 
                              --a.staxcertifnum "Certificate_Number" 
                              e.reasonname
                             ,d.productentries -- "Exempt for Taxware Codes"
                        into  inv_ids_row(idx).exemption_reason
                             ,inv_ids_row(idx).exemption_product_details
                        from taxware.steptec_tbl a,
                        taxware.TAXSTINFO b,
                        taxware.stepprod_tbl d
                        ,taxware.stepreas_tbl e
                        where 1=1
                        and a.scustomerstatecode = b.stalpha
                        and a.key_1 =d.key_1(+)
                        and a.key_1 =nvl(v_ship_site_number, v_bill_site_number)
                        and e.reasoncode(+) =a.sreasoncode
                        and rownum <2
                        order by a.cjurislevel desc;
                    Exception
                     When no_data_found then
                        Null;
                     When others then
                       print_log('@601, Customer_Trx_Id ='||inv_ids_row(idx).customer_trx_id);
                       print_log('@601, Ship Site Number ='||v_ship_site_number);                                        
                       print_log('@601, Bill Site Number ='||v_bill_site_number);                       
                       print_log('@601, Message ='||sqlerrm);                       
                    End;
                         
                       get_geo_maxtaxrate 
                        (
                             p_report_type     =>'TAX EXEMPTION REPORT'                           --in report type
                            ,p_invoice_source  =>inv_ids_row(idx).invoice_source       --in invoice source
                            ,p_invoice_id      =>inv_ids_row(idx).customer_trx_id                       --in customer_trx_id
                            ,p_prism_branch    =>v_prism_branch                          --prism branch    
                            ,p_state           =>UPPER(inv_ids_row(idx).state)    --in customer ship to state
                            ,p_cityname        =>UPPER(inv_ids_row(idx).city)     --in customer ship to city
                            ,p_zipcode         =>inv_ids_row(idx).zip_code         --in customer ship to postal code 
                            ,p_county          =>UPPER(inv_ids_row(idx).county)   --in customer ship to county 
                            ,p_geo             =>inv_ids_row(idx).geo              --out varchar2
                            ,p_point_of_tax    =>inv_ids_row(idx).pot    --out varchar2
                            ,p_ship_via        =>inv_ids_row(idx).ship_via             --out varchar2
                            ,p_branch          =>v_oracle_branch                         --out varchar2                                  
                        );  
                        
                        If inv_ids_row(idx).invoice_source IN ('ORDER MANAGEMENT', 'REPAIR OM SOURCE', 'STANDARD OM SOURCE') Then 
                             inv_ids_row(idx).div :=v_oracle_branch;                        
                        Elsif inv_ids_row(idx).invoice_source IN ('CONVERSION', 'PRISM', 'PRISM REFUND') Then
                             inv_ids_row(idx).div :=Null;                        
                        Else
                             inv_ids_row(idx).div :='NA';                        
                        End If;                                                                   
               end loop;
           --   
               begin 
                    print_log('Before Bulk Insert');
                    print_log('inv_ids_row.count ='||inv_ids_row.count);
                     begin
                       forall indx in inv_ids_row.first .. inv_ids_row.last
                          insert into xxcus.xxcus_ar_trx_exempt_b values inv_ids_row(indx);                      
                     exception
                      when others then
                       print_log('@201, Failed while inserting records for summarized invoices');
                       print_log('@201, Message ='||sqlerrm);                       
                     end;

                    print_log('After Bulk Insert');                      
               exception
                when others then
                 print_log('@101, Issue in forall bulk insert ='||sqlerrm);
               end; 
           --              
         else
          print_log('No invoices to fetch, summarize and add supporting information');
         end if;  
            
            
   else
    print_log('Error in deleting PLSQL table inv_ids_row, '||sqlerrm);       
   end if;      

    begin 
     select count(1)
     into   n_tbl_count
     from   xxcus.xxcus_ar_trx_exempt_b
     where  1 =1;
     print_log('Total records from xxcus.xxcus_ar_trx_exempt_b ='||n_tbl_count);  
        Begin  
            select a.user_concurrent_program_name
                  ,c.email_address
            into   v_cp_long_name
                  ,v_email
            from fnd_amp_requests_v a
                ,fnd_concurrent_programs d
                ,fnd_user c
            where 1 =1
              and a.request_id =n_req_id
              and a.concurrent_program_id =d.concurrent_program_id
              and a.requested_by =c.user_id;                                                  
                                                                                                          
        Exception                         
         When Others Then
           v_cp_long_name :=Null;
           v_email :=Null;
        End;          
     if (n_tbl_count >0) then  
     
        if (v_email is not null) then
            begin 
             --create the data file with the tax write off records 
              v_wrtoff_file_name :='HDS_AR_INVOICE_EXEMPTIONS'||'_'||n_req_id||'.txt';
              
               select '/xx_iface/'||lower(name)||'/outbound' into v_path from v$database;
               
               output_file_id  :=utl_file.fopen(v_path, v_wrtoff_file_name, 'w');
               
               print_log('After opening fileid for file '||v_path||'/'||v_wrtoff_file_name);
               
               for rec in my_exemptions loop 
                utl_file.put_line(output_file_id, rec.exempt_line); 
               end loop; 

              print_log('All detail records are copied into the file '||v_path||'/'||v_wrtoff_file_name);              
              utl_file.fclose(output_file_id); 
               
              print_log('After close of file '||v_path||'/'||v_wrtoff_file_name);
              
             --create a file to point us where the data file is located along with the absolute path.              
              v_wrtoff_file_name :='zip_file_extract_'||n_req_id||'.txt';
              
              output_file_id  :=utl_file.fopen(v_path, v_wrtoff_file_name, 'w');
              print_log('After opening fileid for file '||v_path||'/'||v_wrtoff_file_name);
                      
              utl_file.put_line(output_file_id, v_path||'/'||'HDS_AR_INVOICE_EXEMPTIONS'||'_'||n_req_id||'.txt');
               
              utl_file.fclose(output_file_id);
              print_log('After close of file '||v_path||'/'||v_wrtoff_file_name);
              
              v_child_requests :='OK';
                             
            exception
              when others then
                v_child_requests :='NA';
            end;
          
            if v_child_requests <>'NA' Then
                
              ln_request_id :=fnd_request.submit_request
                  (
                   application      =>'XXCUS',
                   program          =>'XXCUS_PEREND_POSTPROCESSOR',
                   description      =>'Send Tax Exemptions Email',
                   start_time       =>'',
                   sub_request      =>FALSE,
                   argument1        =>n_req_id,
                   argument2        =>v_path||'/zip_file_extract_'||n_req_id||'.txt', 
                   argument3        =>v_cp_long_name,               
                   argument4        =>v_email,
                   argument5        =>'AR_TAX_EXEMPTIONS'||'_'||p_period_name||'.zip',
                   argument6        =>'HDS AR Invoice Exemptions Summary Report -Status Update'                                                                         
                  ); 
             --commit work;                         
              if ln_request_id >0 then 
                 fnd_file.put_line(fnd_file.log, 'Successfully submitted program XXHDS Period End Post Processor Program to email the tax exemption file.');
              else
                 fnd_file.put_line(fnd_file.log, 'Failed to submit program XXHDS Period End Post Processor Program to email the tax exemption file.');
              end if; 
                  
            else --v_child_requests ='NA'
              Null;                      
            end if;      
        else --email id is null
           print_log('Email id for the user is blank');
        end if;              
      
     else --no data in custom table
      print_log('Cannot send zip file as there is no data for request id '||n_req_id);
     end if;  
   
    exception
     when others then
      print_log('Other errors while checking zip file data for request id '||n_req_id);
    end;

  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Error in caller XXCUS_AR_TRX_EXEMPTION_PKB.main ='||sqlerrm);
    --rollback;
  end main; 

end xxcus_ar_trx_exemption_pkb;
/