CREATE OR REPLACE PACKAGE BODY APPS.XXWC_VENDOR_CONT_FRM_PKG
AS
/*************************************************************************
 Copyright (c)  2013 HD Supply
 All rights reserved.
**************************************************************************
  $Header XXWC_VENDOR_CONT_FRM_PKG $
  Module Name: XXWC_VENDOR_CONT_FRM_PKG.pks

  PURPOSE:

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        11/19/2013  HARSHAVARDHAN YEDLA      Initial Version
 
**************************************************************************/

   /*************************************************************************
     Function : xxwc_edi_doctype

     PURPOSE:   This function concatenates all the edi enabled document types 
     Parameter:
            IN
                p_tp_hdr_id      -- Header Id
   ************************************************************************/

  Function xxwc_edi_doctype(p_tp_hdr_id number)
  RETURN varchar2
  as

  cursor c1
  is
  SELECT   distinct decode( etd.document_id, 'INI', 'Invoice', 'POO','PO', 'POCO', 'PO Change', null) DOCUMENT
      FROM ece_tp_details etd,ece_tp_headers eth
            WHERE etd.TP_header_ID = eth.tp_header_id
            and  etd.edi_flag='Y'
            and eth.tp_header_id   =p_tp_hdr_id;
  v_val varchar2(100);
  l_err_code  number;
  l_err_msg   varchar2(3000);
  l_req_id          NUMBER        := fnd_global.conc_request_id;
  l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_VENDOR_CONT_FRM_PKG';
  l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
  l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  j                  NUMBER :=1;

  BEGIN

  for i in c1
  LOOP

  v_val:=i.DOCUMENT||','||v_val;

  END LOOP;
  
   select substr(V_VAL,1,length(V_VAL)-1) into V_VAL from dual;
  

  return v_val;
exception
 WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ((l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000
                   );
         DBMS_OUTPUT.put_line (l_err_msg);
         xxcus_error_pkg.xxcus_error_main_api
                                        (p_called_from            => l_err_callfrom,
                                         p_calling                => l_err_callpoint,
                                         p_request_id             => l_req_id,
                                         p_ora_error_msg          => SUBSTR
                                                                        (SQLERRM,
                                                                         1,
                                                                         2000
                                                                        ),
                                         p_error_desc             => SUBSTR
                                                                        (l_err_msg,
                                                                         1,
                                                                         2000
                                                                        ),
                                         p_distribution_list      => l_distro_list,
                                         p_module                 => 'AP'
                                        );


END xxwc_edi_doctype;



END XXWC_VENDOR_CONT_FRM_PKG;
/