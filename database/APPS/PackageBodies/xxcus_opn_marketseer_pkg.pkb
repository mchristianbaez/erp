CREATE OR REPLACE PACKAGE BODY APPS.xxcus_opn_marketseer_pkg AS

  /********************************************************************************
  PROGRAM TYPE: Procedure
  PURPOSE:
  ****  This is Run every 5 minutes by UC4  *****
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     08/02/2012    Manny           Initial creation of the procedure
  1.1     08/07/2012    Manny           Generalizing the lease data
  2       02/20/2012    Manny           changed to meaning and added the lookup values table
                                        Adding countryfips,MSA, and MSA_NAME column
  4       03/07/2013    Manny           Adding new rules to function type           
  5       04/22/2013    Manny           Adding new columns and revised logic                             
  ********************************************************************************/
  PROCEDURE hds_marketseer(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  l_trunc   VARCHAR2(150)  DEFAULT 'truncate table xxcus.xxcus_pn_marketseer_tbl';
  l_submit  BOOLEAN   DEFAULT TRUE;
  l_dummy   VARCHAR2(50);
  
  
  -- column inserts
  l_ops_type1              VARCHAR2(50);
  l_ops_type2              VARCHAR2(50);
  l_long                   VARCHAR2(50);
  l_lat                    VARCHAR2(50);
  l_lob                    VARCHAR2(50);
  l_legacy                 VARCHAR2(1000);
  l_fru                    VARCHAR2(1000);
  l_loc_code               VARCHAR2(20);
  l_addr                   VARCHAR2(100);
  l_city                   VARCHAR2(100);
  l_state                  VARCHAR2(100);
  l_zip                    VARCHAR2(100);
  l_country                VARCHAR2(50);
  l_primary_loc            VARCHAR2(100);
  l_rer                    VARCHAR2(100);
  l_rer_type               VARCHAR2(100);
  l_rer_status             VARCHAR2(100);
  l_expir                  DATE;
  l_rem_name               VARCHAR2(100);
  l_prp_total              NUMBER;
  l_vac                    NUMBER;
  l_lnd_total              NUMBER;
  l_oracle                 VARCHAR2(1000);
  l_fru_desc               VARCHAR2(1000);
  l_cbsa_number            VARCHAR2(50);
  l_cbsa_name              VARCHAR2(250);
  l_evp                    VARCHAR2(100);
  l_function               VARCHAR2(150);
  l_openmarket             VARCHAR2(50);
  l_countyfips             VARCHAR2(10);
  l_msa                    VARCHAR2(50);
  l_msa_name               VARCHAR2(255);
  l_yardacreage            NUMBER;
  
BEGIN
  
  EXECUTE IMMEDIATE l_trunc;


FOR c_main in (

--marketseer view (779 rows)
  SELECT  DISTINCT prop.property_code PRP_ID
                 ,roll.bu_id_ms||'-'||prop.property_code SBU_PRP
                 ,roll.bu_id_ms
                 ,prop.attribute1
--FROM clause*********************************************************************************

  FROM pn.pn_properties_all prop
      ,pn_space_assign_emp_all empsp
      ,hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com Roll      
      ,xxcus.xxcuspn_ld_gldesc_tbl gldcr


      --Bld (lvl1)
      ,(SELECT loc1.location_id
              ,loc1.parent_location_id
              ,gross_area
              ,address_id
              ,loc1.property_id
              ,loc1.building
              ,loc1.attribute1
              ,loc1.attribute2
              ,loc1.location_code
              ,Nvl(loc1.active_end_date,to_date('12/31/4712' ,'MM/DD/YYYY')) ACTIVE_END_DATE
              ,loc1.space_type_lookup_code
          FROM pn.pn_locations_all loc1
          WHERE  loc1.location_type_lookup_code IN ('BUILDING', 'LAND')             
          AND loc1.active_end_date >= SYSDATE              
          AND loc1.active_end_date =
               (SELECT MAX(la.active_end_date)
                  FROM pn.pn_locations_all la
                 WHERE substr(la.location_code, 1,decode(instr(la.location_code,'.',1),0,length(la.location_code),instr(la.location_code,'.',1)-1)) = substr(loc1.location_code, 1,decode(instr(loc1.location_code,'.',1),0,length(loc1.location_code),instr(loc1.location_code,'.',1)-1))  
       ) )loc1


      -- Floor (lvl2)
               ,(SELECT loc2.location_id
              ,loc2.parent_location_id
              ,Nvl(loc2.active_end_date,to_date('12/31/4712' ,'MM/DD/YYYY')) ACTIVE_END_DATE
              ,loc2.location_code
              ,loc2.space_type_lookup_code
          FROM pn.pn_locations_all loc2
           WHERE loc2.location_type_lookup_code IN ('FLOOR', 'PARCEL')    
           AND loc2.active_end_date >= SYSDATE              
           AND loc2.active_end_date =
               (SELECT MAX(la.active_end_date)
                  FROM pn.pn_locations_all la
                 WHERE substr(la.location_code, 1,decode(instr(la.location_code,'.',1),0,length(la.location_code),instr(la.location_code,'.',1)-1)) = substr(loc2.location_code, 1,decode(instr(loc2.location_code,'.',1),0,length(loc2.location_code),instr(loc2.location_code,'.',1)-1)) 
                )
               ) loc2
               
    --Section (lvl3)
      ,(SELECT loc3.location_id
              ,loc3.parent_location_id
              ,loc3.address_id
              ,loc3.property_id
              ,loc3.building
              ,loc3.location_code
              ,loc3.lease_or_owned
              ,loc3.active_START_date
              ,loc3.active_end_date ACTIVE_END_DATE
              ,loc3.attribute1
              ,loc3.attribute2
              ,space_type_lookup_code
              ,function_type_lookup_code
          FROM pn.pn_locations_all loc3
         WHERE loc3.location_type_lookup_code IN ('OFFICE', 'SECTION')
         AND   nvl(loc3.SPACE_type_lookup_code,'x') !='LAWSONADMIN' 
        AND loc3.active_end_date >= SYSDATE              
       AND loc3.active_end_date =
               (SELECT MAX(la.active_end_date)
                  FROM pn.pn_locations_all la
                 WHERE substr(la.location_code, 1,decode(instr(la.location_code,'.',1),0,length(la.location_code),instr(la.location_code,'.',1)-1)) = substr(loc3.location_code, 1,decode(instr(loc3.location_code,'.',1),0,length(loc3.location_code),instr(loc3.location_code,'.',1)-1))
                 ) )loc3




--Main Where clause******************************

 WHERE  loc1.property_id = prop.property_id(+)
   AND loc3.parent_location_id = loc2.location_id
   AND loc2.parent_location_id = loc1.location_id
   AND loc3.active_end_date >= SYSDATE
   AND loc3.location_id = empsp.location_id(+) 

 
   AND empsp.cost_center_code = gldcr.segment2(+)
   AND empsp.attribute1 = gldcr.l_corp_id 
   AND roll.fps_id = gldcr.segment1 
                                      ) LOOP
                                      

--begin main loop
--clear out values from prev loop
l_submit   :=TRUE;
l_dummy            :=NULL;
l_ops_type1        :=NULL;
  l_long           :=NULL;                
  l_lat            :=NULL;                
  l_lob            :=NULL;   
  l_legacy         :=NULL;    
  l_fru            :=NULL;     
  l_loc_code       :=NULL;    
  l_addr           :=NULL;
  l_city           :=NULL;
  l_state          :=NULL;
  l_zip            :=NULL; 
  l_country        :=NULL;
  l_primary_loc    :=NULL; 
  l_rer            :=NULL;
  l_rer_type       :=NULL; 
  l_rer_status     :=NULL;
  l_expir          :=NULL; 
  l_rem_name       :=NULL;   
  l_prp_total      :=NULL;
  l_vac            :=NULL;
  l_lnd_total      :=NULL;
  l_oracle         :=NULL;
  l_fru_desc       :=NULL;
  l_ops_type2      :=NULL;
  l_cbsa_number    :=NULL;
  l_cbsa_name      :=NULL;
  l_evp            :=NULL;
  l_function       :=NULL;




--long and lat
BEGIN
  -- look for buildings
  SELECT DISTINCT  bld.attribute2 lat, bld.attribute1 "long"
  INTO  l_lat, l_long
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code = 'BUILDING'  
                       AND bld.active_end_date >= SYSDATE                                                       
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
--                        AND bld.location_code = prop.attribute4

                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id);
EXCEPTION
  WHEN no_data_found THEN
--no buildings found, look for land
BEGIN
  SELECT DISTINCT  bld.attribute2 lat, bld.attribute1 "long"
  INTO  l_lat, l_long
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code = 'LAND'     
                       AND bld.active_end_date >= SYSDATE                                                     
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
--                        AND bld.location_code = prop.attribute4

                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id);
EXCEPTION
  WHEN too_many_rows THEN  
    -- Too many Buildings, so select the one from the PRoperty form.
 SELECT DISTINCT  bld.attribute2 lat, bld.attribute1 "long"
  INTO  l_lat, l_long
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.location_type_lookup_code = 'LAND'
                       AND bld.active_end_date >= SYSDATE     
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)
                        AND bld.location_code = (select attribute4
                                                 FROM pn.pn_properties_all
                                                 where property_code = c_main.prp_id) ;

  WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log,'OTHERS error getting long/lat at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);  
end ;    


--too many rows searching for buildings.  Get unique building from property form. 
  WHEN too_many_rows THEN
    
BEGIN
  SELECT DISTINCT  bld.attribute2 lat, bld.attribute1 "long"
  INTO  l_lat, l_long
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.location_type_lookup_code = 'BUILDING'
                       AND bld.active_end_date >= SYSDATE     
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)
                        AND bld.location_code = (select attribute4
                                                 FROM pn.pn_properties_all
                                                 where property_code = c_main.prp_id) ;
EXCEPTION
  WHEN no_data_found THEN
    NULL;  
  
  WHEN too_many_rows THEN  
      fnd_file.put_line(fnd_file.log,'too many rows getting long/lat at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);  
END;                                
END;




--lob
BEGIN

select DISTINCT lob_ms
INTO l_lob
FROM   hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
WHERE bu_id_ms =  c_main.bu_id_ms;


END;


-- legacy
BEGIN
  
SELECT  LISTAGG( leg_seg_one ,', ') WITHIN GROUP (ORDER BY leg_seg_one) --AS "MANNY"
INTO l_legacy
  from (
SELECT DISTINCT gl.leg_seg_one
                      FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                        and (spaced.emp_assign_end_date>sysdate or spaced.emp_assign_end_date is null)                        
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)) ;
                                  
EXCEPTION
  WHEN no_data_found THEN
    NULL;
  


END;




-- FRU
BEGIN
  
SELECT  LISTAGG( l_corp_id ,', ') WITHIN GROUP (ORDER BY l_corp_id) --AS "MANNY"
INTO l_fru
  from (
SELECT DISTINCT gl.l_corp_id
                      FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                        and (spaced.emp_assign_end_date>sysdate or spaced.emp_assign_end_date is null)                        
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)) ;
                                  
EXCEPTION
  WHEN no_data_found THEN
NULL;



END;




--loc code and EVP 
BEGIN
  --Search for buildings.
  SELECT DISTINCT  bld.location_code, rollups2.evp
  INTO  l_loc_code, l_evp
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code = 'BUILDING'       
                       AND bld.active_end_date >= SYSDATE                                                 
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id);
EXCEPTION
   WHEN too_many_rows THEN
BEGIN     
  SELECT DISTINCT  bld.location_code, rollups2.evp
  INTO  l_loc_code, l_evp
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code = 'BUILDING'  
                       AND bld.active_end_date >= SYSDATE                                                       
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)     
                        AND bld.location_code = (select attribute4
                                                 FROM pn.pn_properties_all
                                                 where property_code = c_main.prp_id)  ; 

EXCEPTION
  WHEN no_data_found THEN
    NULL;
  WHEN too_many_rows THEN
           fnd_file.put_line(fnd_file.log,'TOO many rows getting loc evp function at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);      
    
END;

  WHEN no_data_found THEN
    
BEGIN
  SELECT DISTINCT  bld.location_code, rollups2.evp
  INTO  l_loc_code, l_evp
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code = 'LAND'      
                       AND bld.active_end_date >= SYSDATE                                                  
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id);
EXCEPTION
   WHEN too_many_rows THEN

 SELECT DISTINCT  bld.location_code, rollups2.evp
  INTO  l_loc_code, l_evp
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code = 'LAND'  
                       AND bld.active_end_date >= SYSDATE                                                       
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)     
                        AND bld.location_code = (select attribute4
                                                 FROM pn.pn_properties_all
                                                 where property_code = c_main.prp_id)  ;        

   WHEN OTHERS  THEN
      fnd_file.put_line(fnd_file.log,'OTHERS ERROR getting loc evp function at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);                                   
END;

END;








BEGIN
-- Primary ops type1
select  DISTINCT ops_type1.description, ops_type1.meaning
INTO l_ops_type1, l_ops_type2

From 
(SELECT min(to_number(looky.tag)) tag, rollups.bu_id_ms,substr(loca.location_code, 1, 5) loc_code
      FROM  pn.pn_locations_all loca, pn.pn_space_assign_emp_all spc, apps.xxcus_location_code_vw loco, hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS, fnd_lookup_values_vl looky
      WHERE loca.location_id = spc.location_id
      AND loca.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id = loca.location_id)      
      AND loca.SPACE_type_lookup_code !='LAWSONADMIN'
      and looky.enabled_flag ='Y'
      and looky.lookup_type ='PN_SPACE_TYPE'
      AND loca.active_end_date >= SYSDATE               
      and loca.space_type_lookup_code = looky.lookup_code
      AND spc.attribute1 = loco.fru
      AND loco.entrp_entity = rollups.fps_id
     GROUP BY rollups.bu_id_ms, substr(loca.location_code, 1, 5)
  )  ops_type
  
,(select lookup_code, DESCRIPTION, nvl(to_number(tag),999) tag, meaning
FROM fnd_lookup_values_vl
WHERE lookup_type='PN_SPACE_TYPE'
AND enabled_flag ='Y'
) ops_type1
    
Where  ops_type1.tag = ops_type.tag   
   AND ops_type.loc_code=c_main.prp_id
   AND ops_type.bu_id_ms = c_main.bu_id_ms;
EXCEPTION
  WHEN no_data_found THEN
    NULL;
END;















--add,city,state,zip
BEGIN
  --Look for buildings
select          addr.address_line1||' '||addr.address_line2
               ,addr.city
               ,nvl(addr.state,addr.province)
               ,addr.Zip_Code 
               ,addr.country
INTO 
l_addr,
l_city,
l_state,
l_zip,
l_country
FROM pn.pn_addresses_all addr
WHERE addr.Address_Id IN (
  SELECT DISTINCT  bld.address_id
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code = 'BUILDING'  
                       AND bld.active_end_date >= SYSDATE                                                    
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id));
EXCEPTION
   --narrow down location to one from property form
   WHEN too_many_rows THEN
     
BEGIN
select          addr.address_line1||' '||addr.address_line2
               ,addr.city
               ,nvl(addr.state,addr.province)
               ,addr.Zip_Code 
               ,addr.country
INTO 
l_addr,
l_city,
l_state,
l_zip,
l_country
FROM pn.pn_addresses_all addr
WHERE addr.Address_Id IN(     
  SELECT DISTINCT  bld.address_id
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.location_type_lookup_code = 'BUILDING'   
                       AND bld.active_end_date >= SYSDATE                     
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)     
                        AND bld.location_code = (select attribute4
                                                 FROM pn.pn_properties_all
                                                 where property_code = c_main.prp_id))  ; 
EXCEPTION
     WHEN too_many_rows THEN
    fnd_file.put_line(fnd_file.log,'too many rows getting addr 2 at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);                                                                   
     WHEN OTHERS THEN 
    fnd_file.put_line(fnd_file.log,'unknown error getting addr 2 at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);                                                                   
           
END;
  

  WHEN no_data_found THEN
  --no buildings found, so search for LAND.
BEGIN
select          addr.address_line1||' '||addr.address_line2
               ,addr.city
               ,nvl(addr.state,addr.province)
               ,addr.Zip_Code 
               ,addr.country
INTO 
l_addr,
l_city,
l_state,
l_zip,
l_country
FROM pn.pn_addresses_all addr
WHERE addr.Address_Id IN(     
  SELECT DISTINCT  bld.address_id
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.location_type_lookup_code = 'LAND'        
                       AND bld.active_end_date >= SYSDATE                                            
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)     
                                                                                                )  ; 
EXCEPTION
     WHEN too_many_rows THEN

-- Too many LANDS ,use property form to get unique one.

select          addr.address_line1||' '||addr.address_line2
               ,addr.city
               ,nvl(addr.state,addr.province)
               ,addr.Zip_Code 
               ,addr.country
INTO 
l_addr,
l_city,
l_state,
l_zip,
l_country
FROM pn.pn_addresses_all addr
WHERE addr.Address_Id IN(     
  SELECT DISTINCT  bld.address_id
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.location_type_lookup_code = 'LAND'   
                       AND bld.active_end_date >= SYSDATE                     
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)     
                        AND bld.location_code = (select attribute4
                                                 FROM pn.pn_properties_all
                                                 where property_code = c_main.prp_id))  ;        
           
END;
                                                                   
     WHEN OTHERS THEN 
    fnd_file.put_line(fnd_file.log,'unknown error getting addr land at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);                                                                   
  
/*  WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.log,'unknown error getting addr 1 at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);  */                                                                 
      
                                  
END;









--lease data
BEGIN


 SELECT DISTINCT  
b.location_code primary_loc
               ,a.lease_num              rer
               ,lease_type.meaning       rer_type
               ,c.lease_termination_date expiration
               ,remgr.description        rem_name
               ,lease_status.meaning     rer_status  --v5
          
INTO
l_primary_loc,
l_rer,
l_rer_type,
l_expir,
l_rem_name,
l_rer_status
   
FROM pn.pn_leases_all a
    ,pn.pn_locations_all b
    ,pn.pn_lease_details_all c
    ,pn.pn_properties_all prop
    ,apps.fnd_user remgr
    ,pn.pn_tenancies_all ten
    ,(SELECT meaning, lookup_code
      FROM apps.fnd_lookup_values
      WHERE lookup_type = 'PN_LEASE_TYPE'
      AND enabled_flag = 'Y') lease_type
    , (SELECT meaning, lookup_code
      FROM apps.fnd_lookup_values
      WHERE lookup_type = 'PN_LEASESTATUS_TYPE'
      AND enabled_flag = 'Y') lease_status
WHERE a.lease_type_code = lease_type.lookup_code(+)
AND ten.location_id = b.location_id
AND a.lease_id= ten.lease_id
AND b.location_type_lookup_code IN ('BUILDING','LAND')
AND a.lease_id = c.lease_id(+)
AND b.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id = a.location_id)
AND c.responsible_user = remgr.user_id(+)
AND a.lease_status NOT in ('PCOM','TER')
AND b.property_id = prop.property_id
AND   a.lease_status = lease_status.lookup_code
     
AND a.lease_num 

IN 
(SELECT b.lease_num
FROM pn.pn_leases_all b
WHERE lease_id in (
SELECT DISTINCT a.lease_id
from pn.pn_tenancies_all a
WHERE   (SELECT DISTINCT  substr(location_code, 1,decode(instr(location_code,'.',1),0,length(location_code),instr(location_code,'.',1)-1)) from pn.pn_locations_all WHERE location_id = a.location_id AND active_end_date >SYSDATE) = l_loc_code
AND  a.primary_flag = 'Y'
)
AND b.lease_status NOT in ('PCOM','TER'))
            /*      (SELECT lse.lease_num 
                     FROM pn.pn_tenancies_all ab , pn.pn_leases_all lse, pn.pn_locations_all bc
                     WHERE ab.lease_id = lse.lease_id 
                     AND ab.primary_flag ='Y'  
                     AND lse.lease_status NOT in ('PCOM','TER')
                     --AND bc.location_type_lookup_code = 'BUILDING'
                     AND bc.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id = ab.location_id)
                     AND ab.lease_id = lse.lease_id
                     AND ab.location_id = bc.location_id
                     AND  substr(bc.location_code, 1,decode(instr(bc.location_code,'.',1),0,length(bc.location_code),instr(bc.location_code,'.',1)-1)) = substr(b.location_code, 1,decode(instr(b.location_code,'.',1),0,length(b.location_code),instr(b.location_code,'.',1)-1))
                     GROUP BY lse.lease_num)*/
AND prop.property_code  =c_main.prp_id
AND b.location_code =l_loc_code;
                    
EXCEPTION
  WHEN too_many_rows THEN
    dbms_output.put_line('too many rows getting lease data at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);      
      fnd_file.put_line(fnd_file.log,'too many rows getting lease data at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);                                 
  
  WHEN no_data_found THEN
    NULL;                    

END;



--Landsize
BEGIN

-- Property SQ ft
        Select 
            nvl(loc_sft.prp_total_sft,0) PRP_Total_SFT
         -- , decode(SIGN(nvl(loc_sft.prp_total_sft,0)-(nvl(sec_sft.prp_allocated_sft,0)+nvl(sub_sft.Sub_PRP_SFT,0))),-1,0,(nvl(loc_sft.prp_total_sft,0)-(nvl(sec_sft.prp_allocated_sft,0)+nvl(sub_sft.Sub_PRP_SFT,0)))) PRP_VAC_SFT
          , Round(nvl(loc_sft.LND_total_sft, 0)/43560,1) LND_Total_Acre
         INTO
         l_prp_total,
       --  l_vac,
         l_lnd_total 
          
        From 
        --Building Land Level
        (Select 
            prp.Property_Code
            , nvl(sum(case when loc.location_type_lookup_code = 'BUILDING' then loc.gross_area end),0) PRP_Total_SFT
            , nvl(sum(case when loc.location_type_lookup_code = 'LAND' then loc.gross_area end),0) LND_Total_SFT
        From 
            pn.pn_properties_ALL prp
          , pn.pn_locations_all loc
        where 
          prp.property_id=loc.property_id
          and (loc.active_end_date>sysdate or loc.active_end_date is null)
        group by prp.property_code
        order by prp.property_code) LOC_SFT
        ,
        --Office, Section Level
        (Select 
            prp.property_code
          , nvl(sum(case when sec.location_type_lookup_code='OFFICE' then spa.allocated_area end),0) PRP_Allocated_SFT
          , nvl(sum(case when sec.location_type_lookup_code='SECTION' then spa.allocated_area end),0) LND_Allocated_SFT
        from 
            pn.pn_properties_ALL prp
          , pn.pn_locations_all loc
          , pn.pn_locations_all flr
          , pn.pn_locations_all sec
          , pn.pn_space_assign_emp_all spa
        where 
          prp.property_id=loc.property_id(+)
          and loc.location_id=flr.parent_location_id(+)
          and flr.location_id=sec.parent_location_id(+)
          and sec.location_id=spa.location_id(+)
          
          and sec.space_type_lookup_code !='LAWSONADMIN'
          and sec.standard_type_lookup_code!='ASSET'
          and (loc.active_end_date>sysdate or loc.active_end_date is null)
          and (sec.active_end_date>sysdate or sec.active_end_date is null)
          and (spa.emp_assign_end_date>sysdate or spa.emp_assign_end_date is null) 
        Group by prp.property_code) SEC_SFT
        , 
       
        (Select
                 substr(loc.location_code, 1,decode(instr(loc.location_code,'.',1),0,length(loc.location_code),instr(loc.location_code,'.',1)-1)) Property_Code
          , nvl(Sum(case when loc.location_type_lookup_code ='BUILDING' then loc.gross_area end),0) 
          + nvl(Sum(case when loc.location_type_lookup_code ='OFFICE' then loc.Assignable_Area end),0) Sub_PRP_SFT
          , nvl(Sum(case when loc.location_type_lookup_code ='LAND' then loc.gross_area end),0) 
          + nvl(Sum(case when loc.location_type_lookup_code ='SECTION' then loc.Assignable_Area end),0) Sub_LND_SFT
        From 
          pn.pn_leases_All lse
          , pn.pn_Lease_Details_All lsd
          , pn.pn_tenancies_all ten
          , pn.pn_locations_all loc
          
        where 
          lse.lease_id=ten.lease_id(+)
          and lse.lease_id=lsd.lease_id(+)
          and ten.location_id= loc.location_id(+)
          
          and lse.lease_type_code in('LSE', 'ISUB','ISA','NONL') and lse.lease_class_code='SUB_LEASE'
          and lse.lease_Status not in('TER','SGN')
          and (loc.active_end_date>sysdate or loc.active_end_date is null)
        Group By substr(loc.location_code, 1,decode(instr(loc.location_code,'.',1),0,length(loc.location_code),instr(loc.location_code,'.',1)-1))) SUB_SFT

        Where 
          loc_sft.property_code=sec_sft.property_code(+)
          and loc_sft.property_Code=sub_sft.Property_Code(+)
          AND             loc_sft.property_code = c_main.prp_id;
EXCEPTION
  WHEN too_many_rows THEN
    fnd_file.put_line(fnd_file.log,'too many rows getting landsizes at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);                                  
    
  WHEN no_data_found THEN
    NULL;          

END;




-- getting vacant sections
BEGIN
 select   sum(assignable_area) gross_area
  INTO l_vac
  FROM pn.pn_locations_all loc
 WHERE substr(loc.location_code, 1,5)   = c_main.prp_id
  AND loc.location_type_lookup_code = 'OFFICE'
  AND loc.space_type_lookup_code ='SUR'
  AND loc.active_end_date >= SYSDATE 
 GROUP BY    substr(loc.location_code, 1,5);

EXCEPTION
  WHEN too_many_rows THEN
    fnd_file.put_line(fnd_file.log,'too many rows getting section landsize at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);                                  
    
  WHEN no_data_found THEN
    NULL;          
 
END;




-- getting openmarket
BEGIN
 select  attribute5
  INTO l_openmarket
  FROM pn.pn_properties_all
 WHERE property_code   = c_main.prp_id
;
EXCEPTION
  WHEN too_many_rows THEN
    fnd_file.put_line(fnd_file.log,'too many rows getting section property openmarket at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);                                  
    
  WHEN no_data_found THEN
    NULL;          
 
END;






--oracle list
BEGIN
SELECT  LISTAGG( segment2 ,', ') WITHIN GROUP (ORDER BY segment2) --AS "MANNY"
INTO l_oracle
  from (
SELECT DISTINCT  gl.segment2
                      FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                       -- AND    rollups2.bu_id_ms = 'CAN'
                        and (spaced.emp_assign_end_date>sysdate or spaced.emp_assign_end_date is null) 

                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id));
                                  
EXCEPTION
  WHEN too_many_rows THEN
    fnd_file.put_line(fnd_file.log,'too many rows getting oracle list at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);                                  
    
  WHEN no_data_found THEN
    NULL;                                 
  
END;








--FRU DESC list
BEGIN
SELECT  LISTAGG( location ,', ') WITHIN GROUP (ORDER BY location) AS "MANNY"
INTO l_fru_desc
  from (
SELECT DISTINCT  gl.location
                      FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                       AND    rollups2.bu_id_ms = c_main.bu_id_ms
                        and (spaced.emp_assign_end_date>sysdate or spaced.emp_assign_end_date is null) 

                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id));
                                  
EXCEPTION
  WHEN no_data_found THEN
    NULL;                                 
  
END;




-- cbsanamer stuff

BEGIN
  SELECT DISTINCT cbsanamer.cbsa, replace(cbsanamer.cbsaname,'"'), lpad(statefips,2,'0')|| lpad(countyfips,3,'0'), msa,msaname
  INTO l_cbsa_number, l_cbsa_name,l_countyfips,l_msa,l_msa_name
from xxcuspn_metro_area_zips_tbl cbsanamer
where cbsanamer.zipcode = (select attribute1 from pn.pn_properties_all where property_code =c_main.prp_id)
and   cbsanamer.primaryrecord = 'P';
EXCEPTION
  WHEN too_many_rows THEN
    fnd_file.put_line(fnd_file.log,'TOO MANY ROWS error getting CBSA info at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);
  WHEN no_data_found THEN
    NULL; --normal
  WHEN OTHERS THEN
    fnd_file.put_line(fnd_file.log,'OTHERS error getting CBSA info at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);                                  
    
  END;





-- Location type 1
--logic is : only 3 types of functions exist (primary,support, surplus).  If multiples, then both are present
--rules dicate that if both, use primary
--rules dicate that no_data_found is unlikely scenario.
--v3 changed to meaning and added the lookup values table  
--v4 New condition rules
BEGIN
  SELECT DISTINCT  bb.meaning
  INTO  l_function
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2,
                            apps.fnd_lookup_values_vl bb
                        WHERE  1=1
                        AND  bb.LOOKUP_TYPE = 'PN_FUNCTION_TYPE'
                        AND  loced.function_type_lookup_code = bb.LOOKUP_CODE
                        AND bb.enabled_flag = 'Y'
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code in ('BUILDING','LAND') 
                       AND bld.active_end_date >= SYSDATE    
                        AND loced.active_end_date >= SYSDATE                                                                           
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
--                        AND bld.location_code = prop.attribute4

                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id);  


EXCEPTION
  WHEN too_many_rows THEN  -- new rules say when dups, go with tag order
    
BEGIN
  
  SELECT DISTINCT  bb.meaning
  INTO  l_function
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2,
                            apps.fnd_lookup_values_vl bb
                        WHERE  1=1
                        AND  bb.LOOKUP_TYPE = 'PN_FUNCTION_TYPE'
                        AND  loced.function_type_lookup_code = bb.LOOKUP_CODE
                        AND bb.enabled_flag = 'Y'
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code in ('BUILDING','LAND')  
                       AND bld.active_end_date >= SYSDATE    
                        AND loced.active_end_date >= SYSDATE                                                                           
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
--                        AND bld.location_code = prop.attribute4

                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)
                         AND bb.tag='1';  
 -- If #1 found, then use that and get out.
 EXCEPTION
   WHEN no_data_found THEN  --If #1 not found, then look for #2
     BEGIN
  
  SELECT DISTINCT  bb.meaning
  INTO  l_function
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2,
                            apps.fnd_lookup_values_vl bb
                        WHERE  1=1
                        AND  bb.LOOKUP_TYPE = 'PN_FUNCTION_TYPE'
                        AND  loced.function_type_lookup_code = bb.LOOKUP_CODE
                        AND bb.enabled_flag = 'Y'
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code in ('BUILDING','LAND')
                       AND bld.active_end_date >= SYSDATE    
                        AND loced.active_end_date >= SYSDATE                                                                           
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
--                        AND bld.location_code = prop.attribute4

                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)
                         AND bb.tag='2';  
     EXCEPTION
       WHEN no_data_found THEN -- If #2 not found, pick up #3

       BEGIN
  
  SELECT DISTINCT  bb.meaning
  INTO  l_function
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2,
                            apps.fnd_lookup_values_vl bb
                        WHERE  1=1
                        AND  bb.LOOKUP_TYPE = 'PN_FUNCTION_TYPE'
                        AND  loced.function_type_lookup_code = bb.LOOKUP_CODE
                        AND bb.enabled_flag = 'Y'
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code in ('BUILDING','LAND') 
                       AND bld.active_end_date >= SYSDATE    
                        AND loced.active_end_date >= SYSDATE                                                                           
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
--                        AND bld.location_code = prop.attribute4

                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id)
                         AND bb.tag='3';  
       EXCEPTION
       WHEN OTHERS THEN  --Must be a tagging problem. 
        dbms_output.put_line('Others error trying to get tag order at function type at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms||'.  For l_loc_code of :'||l_loc_code);
        fnd_file.put_line(fnd_file.log,'Others error trying to get tag order at function type at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms||'.  For l_loc_code of :'||l_loc_code);
       END;
         
     END;         


END;








--    l_function:='PRIMARY';
   
/*  WHEN no_data_found THEN
    l_function:='PRIMARY';*/
   WHEN no_data_found THEN

    
  dbms_output.put_line('No data found at function type at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms||'.  For l_loc_code of :'||l_loc_code);
  fnd_file.put_line(fnd_file.log,'No data found at function type at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms||'.  For l_loc_code of :'||l_loc_code);
  
END;



--yard/acreage
NULL;




--Yard/acreage

  SELECT round((SUM(DISTINCT bld.gross_area)/43560),1)
  INTO  l_yardacreage
                        FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                         AND bld.location_type_lookup_code = 'LAND'      
                       AND bld.active_end_date >= SYSDATE                                                  
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id);
                 






-- FILTER #1.  Remove row when No active frus found and opstype NOT in ('Surplus','Sublet')
IF l_submit THEN
BEGIN
SELECT DISTINCT gl.l_corp_id
INTO l_dummy
                      FROM pn.pn_locations_all loced,
                             pn.pn_locations_all flr,
                              pn.pn_locations_all  bld,
                             pn.pn_space_assign_emp_all spaced,
                             xxcus.xxcuspn_ld_gldesc_tbl gl,
                            hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com ROLLUPS2
 
                        WHERE  1=1
                        AND loced.location_type_lookup_code IN ('OFFICE', 'SECTION')
                        AND loced.office !='PEOPLE'
                        AND flr.location_id = loced.parent_location_id
                        AND bld.location_id = flr.parent_location_id
                        AND loced.location_id = spaced.Location_Id
                        AND substr(loced.location_code,1,5) =  c_main.prp_id
                       AND SUBSTR(bld.location_code,1,5) = c_main.prp_id
                        AND    rollups2.bu_id_ms = c_main.bu_id_ms
                        and (spaced.emp_assign_end_date>sysdate or spaced.emp_assign_end_date is null)                        
                       AND bld.active_end_date =               
                       (SELECT MAX(la.active_end_date)
                          FROM pn.pn_locations_all la
                         WHERE SUBSTR(la.location_code,1,5)=  SUBSTR(BLD.LOCATION_CODE,1,5)
                           AND la.location_type_lookup_code in ('BUILDING','LAND'))                        
                        AND spaced.Cost_Center_Code = gl.segment2
                        AND gl.segment1 = rollups2.fps_id
                              AND nvl(spaced.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
                                (SELECT MAX(nvl(spc.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')))
                                  FROM pn.pn_space_assign_emp_all spc
                                  WHERE spc.attribute1 = spaced.attribute1
                                  AND spc.location_id = spaced.location_id) ;
                                  
EXCEPTION
  WHEN no_data_found THEN
/*   fnd_file.put_line(fnd_file.log,'Checking to see if skip at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);
   fnd_file.put_line(fnd_file.log,'Opstype2: '||l_ops_type2);
  dbms_output.put_line('Checking to see if skip at loc: '||c_main.prp_id||' and BU: '||c_main.bu_id_ms);
   dbms_output.put_line('Opstype2: '||l_ops_type2);*/

  IF l_ops_type2 = 'Sublet' OR l_ops_type2='Surplus' THEN
    l_submit :=TRUE;
   ELSE
     l_submit :=FALSE;
  end IF;
  
  WHEN OTHERS THEN
    NULL;

END;

END IF;



-- FILTER #2.  Only US and Canada
IF l_submit THEN
  
IF l_country = 'US' OR l_country='CA'
  THEN l_submit:=TRUE;
 ELSE
   l_submit :=FALSE;
 -- fnd_file.put_line(fnd_file.log,'Removing non CA or US country LOC: '||c_main.prp_id||'.  With Country of :'||l_country);
end IF;

END IF;






IF l_submit THEN
 
insert INTO xxcus.xxcus_pn_marketseer_tbl 
(prp_id,
sbu_prp,
lat,
longitude,
lob,
sbu,
prime_ops_type_1,
prime_ops_type_2,
legacy,
fru,
loc_id,
address,
city,
st_prov,
postal_code,
prime_lease_rer,
rer_type,
rer_status,
exp_date,
rem,
prp_total_sft,
prp_vac_sft,
lnd_total_acre,
oracle_ids,
fru_description,
cbsa_number,
metroarea,
evp,
location_type_1,
One_team_selling_area,
countyfips,
msa,
msa_name,
YARD_ACREAGE
)
VALUES
(c_main.prp_id,  --prp_id
c_main.sbu_prp,  --sbu_prp
l_lat,           --lat
l_long,          --long
l_lob,           --lob
c_main.bu_id_ms, --sbu
l_ops_type1,     --prime ops 1
l_ops_type2,     --prime ops 2
l_legacy,        --legacy
l_fru,           --fru
l_loc_code,      --locid
l_addr,          --address
l_city,          --city
l_state,         --state
l_zip,           --zip
l_rer,           --prime lease
l_rer_type,      --rer type
l_rer_status,     --rer status
l_expir,         --exp date
l_rem_name,      --rem
l_prp_total,     --prop total
l_vac,           --pro vac
l_lnd_total,     --land total
l_oracle,        --oracle ids
l_fru_desc,      --fru descr
l_cbsa_number,   --cbsa num
l_cbsa_name,     --metroarea
l_evp,           --evp
l_function,       --location type 1
l_openmarket,     --openmarket
l_countyfips,
l_msa,
l_msa_name,
l_yardacreage
);

END IF;



                                             

END LOOP;


END;

END xxcus_opn_marketseer_pkg;
/
