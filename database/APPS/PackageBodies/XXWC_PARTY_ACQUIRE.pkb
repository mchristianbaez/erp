CREATE OR REPLACE package BODY APPS.XXWC_PARTY_ACQUIRE AS

----------------------------------------------------------------------------------------------
-- Filename:     XXWC_PARTY_ACQUIRE.pkb  
--
-- Purpose:      This package is developed to support search functionality from DQM.
--
-- Source Table: 1. NA 
--
-- Destination Table: 1. NA  
--
-- Developer: Sandeep Surapaneni 
--
-- Date:       05/23/2013   
--
-- Modification History
--
--  Version    Person                Date            Comments
-- --------  -------------------  -----------    -----------------------------------------------
--  1.0      Sandeep Surapaneni    05/23/2013      Program written  
--  1.1      Pattabhi Avula        10/14/2014      TMS# 20141001-00170 Multi-Org Changes made for Canada OU
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
-- F U N C T I O N  # 1 
--
-- Procedure: get_prism_account  
--
-- Purpose:  1.To derive customer record based on  Prism Account
------------------------------------------------------------------------------------------------

 FUNCTION get_prism_account (
                             p_party_id      IN      NUMBER,
                             p_entity        IN      VARCHAR2,
                             p_attribute     IN      VARCHAR2,
                             p_context       IN      VARCHAR2
                             )
   RETURN VARCHAR2 IS

   CURSOR cust_prism_account_info 
       IS
   SELECT attribute6 prism_account
     FROM hz_cust_accounts
    WHERE party_id = p_party_id;

  l_prism_account VARCHAR2(240);
  l_prism_account_ret VARCHAR2(1999); 

 BEGIN

      OPEN cust_prism_account_info ;
      LOOP
      FETCH cust_prism_account_info  INTO l_prism_account;
      EXIT WHEN cust_prism_account_info %NOTFOUND;

      l_prism_account_ret := l_prism_account_ret || ' ' || l_prism_account;
    
      END LOOP;
      CLOSE cust_prism_account_info;
  
      RETURN l_prism_account_ret;

 EXCEPTION
 WHEN OTHERS THEN
 IF (sqlcode=-6502) THEN
 RETURN 'SYNC';
 END IF;
 FND_MESSAGE.SET_NAME('AR', 'HZ_ACQUIRE_PROC_ERROR');
 FND_MESSAGE.SET_TOKEN('PROC' ,'XXWC_PARTY_ACQUIRE.get_prism_account');
 FND_MESSAGE.SET_TOKEN('ERROR' ,SQLERRM);
 FND_MSG_PUB.ADD;
 RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
 END get_prism_account;

------------------------------------------------------------------------------------------------
-- F U N C T I O N  # 2 
--
-- Procedure: get_location_info   
--
-- Purpose:  1.To derive customer record based on Location 
------------------------------------------------------------------------------------------------

 FUNCTION get_location_info(
                            p_party_site_id    IN    NUMBER,
                            p_entity           IN    VARCHAR2,
                            p_attribute        IN    VARCHAR2,
                            p_context          IN    VARCHAR2 
                            )
   RETURN VARCHAR2 IS

   CURSOR cust_location_info 
       IS
   SELECT hzsu.location
     FROM apps.hz_cust_acct_sites hzas,
          apps.hz_cust_site_uses hzsu
    WHERE hzas.PARTY_SITE_ID = p_party_site_id 
      AND hzsu.cust_acct_site_id = hzas.cust_acct_site_id
    ORDER BY hzsu.STATUS,hzsu.CREATION_DATE;

    l_location VARCHAR2(240);
    l_location_ret VARCHAR2(1999); 
    
    
 BEGIN

      OPEN cust_location_info;
      LOOP
      FETCH cust_location_info INTO l_location;
      EXIT WHEN cust_location_info%NOTFOUND;

      l_location_ret := l_location_ret || ' ' || l_location;
    
      END LOOP;
      CLOSE cust_location_info;
      
      RETURN l_location_ret;

 EXCEPTION
 WHEN OTHERS THEN
 IF (sqlcode=-6502) THEN
 RETURN 'SYNC';
 END IF;
 FND_MESSAGE.SET_NAME('AR', 'HZ_ACQUIRE_PROC_ERROR');
 FND_MESSAGE.SET_TOKEN('PROC' ,'XXWC_PARTY_ACQUIRE.get_location_info');
 FND_MESSAGE.SET_TOKEN('ERROR' ,SQLERRM);
 FND_MSG_PUB.ADD;
 RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
 END get_location_info;

------------------------------------------------------------------------------------------------
-- F U N C T I O N  # 3 
--
-- Procedure: get_prism_site_info  
--
-- Purpose:  1.To derive customer record based on Prism Site  
------------------------------------------------------------------------------------------------
 FUNCTION get_prism_site_info(
                              p_party_site_id    IN    NUMBER,
                              p_entity           IN    VARCHAR2,
                              p_attribute        IN    VARCHAR2,
                              p_context          IN    VARCHAR2 
                             )
   RETURN VARCHAR2 IS


   CURSOR cust_prism_site_info 
       IS
   SELECT hzas.attribute17 prism_site
     FROM apps.hz_cust_acct_sites hzas
    WHERE hzas.PARTY_SITE_ID = p_party_site_id 
    ORDER BY hzas.STATUS,hzas.CREATION_DATE;

    l_prism_site VARCHAR2(240);
    l_prism_site_ret VARCHAR2(1999);

 BEGIN

      OPEN cust_prism_site_info;
      LOOP
      FETCH cust_prism_site_info INTO l_prism_site;
      EXIT WHEN cust_prism_site_info%NOTFOUND;

      l_prism_site_ret := l_prism_site_ret || ' ' || l_prism_site;
    
      END LOOP;  
      CLOSE cust_prism_site_info;
      
      RETURN l_prism_site_ret;

 EXCEPTION
 WHEN OTHERS THEN
 IF (sqlcode=-6502) THEN
 RETURN 'SYNC';  
 END IF;
 FND_MESSAGE.SET_NAME('AR', 'HZ_ACQUIRE_PROC_ERROR');
 FND_MESSAGE.SET_TOKEN('PROC' ,'XXWC_PARTY_ACQUIRE.get_prism_site_info');
 FND_MESSAGE.SET_TOKEN('ERROR' ,SQLERRM);
 FND_MSG_PUB.ADD;
 RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
 END get_prism_site_info;


END;
/
