CREATE OR REPLACE PACKAGE BODY APPS.xxcus_ar_set_geo_val_level as
/*+
 -- Date: 03-APR-2013
 -- Author: Balaguru Seshadri
 -- Scope: This package will set the AR address validation to ERROR OR WARNING based on the user input
 -- RFC# 36581
 --Modification History
-- Date      ESMS       Notes
-- ======================================================================================================
-- 06/24/13  209775    Before update of address validation flag set the address style as follows.
--                     When WARNING, set it to POSTAL_ADDR_US_INT.
--                     When ERROR, set it to POSTAL_ADDR_US.
-- ======================================================================================================
*/

 procedure print_log(p_message in varchar2) is
    begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
    end print_log;

 procedure main (
    retcode               out number
   ,errbuf                out varchar2
   ,p_level               in  varchar2 
  ) is

   v_addr_val_level VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   serious_error exception;
   v_location varchar2(2000);
   l_err_msg  varchar2(3000);
  begin  
        begin 
           if upper(p_level) not in ('ERROR', 'WARNING') then
             l_err_msg := 'Allowed values for parameter p_level are WARNING and ERROR';
             raise program_error;
           else
            --we are good to go with the parameter p_level
            begin             
             update fnd_territories
                set address_style =(
                   case
                    when p_level ='ERROR' then 'POSTAL_ADDR_US'
                    else 'POSTAL_ADDR_US_INT'
                   end 
                 )
              where 1 =1
                and territory_code ='US';
            exception
                when program_error then
                 v_location :='100';        
                 print_log('v_location ='||v_location||', message ='||sqlerrm); 
                when others then
                 v_location :='101';
                 print_log('v_location ='||v_location||', message ='||sqlerrm);            
            end;         
           end if;
        exception
         when others then
           v_location :='102';
           l_err_msg := 'Issue in checking allowed values for p_level,  message ='||sqlerrm;
           raise program_error;
        end;   
    fnd_file.put_line(fnd_file.log, 'Before update of geography validation level');        
      v_location :='0001';     
       begin 
        v_location :='0002';       
        update hz_geo_structure_levels 
           set addr_val_level    =p_level
              ,last_update_date  =sysdate
              ,last_updated_by   =fnd_global.user_id
         where 1 =1
           and geography_id           =1
           and geography_type         ='STATE'
           and parent_geography_type  ='COUNTRY'
           and country_code           ='US';
        v_location :='0003';            
       exception
        when program_error then
         v_location :='0004';        
         print_log('v_location ='||v_location||', message ='||sqlerrm); 
        when others then
         v_location :='0005';
         print_log('v_location ='||v_location||', message ='||sqlerrm);
       end;
    v_location :='0005';       
    fnd_file.put_line(fnd_file.log, 'After update of geography validation level');   
  exception
   when program_error then
    rollback;
    print_log('v_location ='||v_location||', message ='||sqlerrm);
   when others then
    fnd_file.put_line(fnd_file.log, 'Outer block, location ='||v_location);   
    fnd_file.put_line(fnd_file.log, 'Error in caller apps.xxcus_ar_set_geo_val_level.main ='||sqlerrm);
    rollback;
  end main; 
  
   procedure submit_job(  errbuf                 out varchar2
                        , retcode                out varchar2
                        , p_level                in  varchar2
                        , p_user_name            in  varchar2
                        , p_responsibility_name  in  varchar2
                       )
   is
           -- Variable definitions
        l_package    VARCHAR2(50) := 'xxcus_ar_set_geo_val_level.submit_job';
        l_email     VARCHAR2(200) := 'HDSOracleDevelopers@hdsupply.com';

        l_req_id                NUMBER NULL;
        v_phase                 VARCHAR2(50);
        v_status                VARCHAR2(50);
        v_dev_status            VARCHAR2(50);
        v_dev_phase             VARCHAR2(50);
        v_message               VARCHAR2(250);
        v_error_message         VARCHAR2(3000);
        l_err_msg               VARCHAR2(3000);
        l_err_code              NUMBER;
        l_statement             VARCHAR2(9000);
        l_user_id               NUMBER;
        l_responsibility_id     NUMBER;
        l_resp_application_id   NUMBER;

          l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_ERROR_PKG';
          l_err_callpoint VARCHAR2(75) DEFAULT 'START';
          l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   begin
        BEGIN
           if upper(p_level) NOT IN ('ERROR', 'WARNING') then
             l_err_msg := 'Allowed values for parameter p_level are WARNING and ERROR';
             RAISE program_error;
           else
             Null; --we are good to go with the parameter p_level         
           end if;
        EXCEPTION
         WHEN OTHERS THEN
           l_err_msg := 'Issue in checking allowed values for p_level,  message ='||sqlerrm;
           RAISE program_error;
        END;

          -- Deriving Ids from variables
        BEGIN
            SELECT    user_id
              INTO     l_user_id
              FROM     fnd_user
              WHERE     user_name = UPPER(p_user_name)
            AND     SYSDATE BETWEEN start_date AND NVL(end_date, TRUNC(SYSDATE) + 1);
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
           l_err_msg := 'UserName '||p_user_name||' not defined in Oracle';
           RAISE program_error;
        WHEN OTHERS THEN
           l_err_msg := 'Error deriving user_id for UserName - '||p_user_name;
           RAISE program_error;
        END;

          BEGIN
            SELECT     responsibility_id
                     , application_id
              INTO     l_responsibility_id
                     , l_resp_application_id
              FROM     fnd_responsibility_vl
             WHERE     responsibility_name = p_responsibility_name
               AND     SYSDATE BETWEEN start_date AND NVL(end_date, TRUNC(SYSDATE) + 1);
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
             l_err_msg := 'Responsibility '||p_responsibility_name||' not defined in Oracle';
             RAISE program_error;
          WHEN OTHERS THEN
             l_err_msg := 'Error deriving Responsibility_id for '||p_responsibility_name;
             RAISE program_error;
          END;

          -- Apps Initialize
          FND_GLOBAL.APPS_INITIALIZE (l_user_id, l_responsibility_id, l_resp_application_id);

          -- Submitting program XXWC Prism-EBS Customer Interface Processing
          l_req_id := fnd_request.submit_request(application =>'XXCUS'
                                               , program     =>'XXCUS_AR_SET_GEO_VAL_LEVEL'
                                               , description => NULL
                                               , start_time  => SYSDATE
                                               , sub_request => FALSE
                                               , argument1   => upper(p_level)
                                               );

          COMMIT;

          dbms_output.put_line('Concurrent Program Request Submitted. Request ID: '||l_req_id);
          IF (l_req_id != 0)
          THEN

            IF fnd_concurrent.wait_for_request(l_req_id
                                          ,6
                                          ,3600 -- 04/17/2012 Changed from 15000
                                          ,v_phase
                                          ,v_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
            THEN

                v_error_message := 'ReqID:' || l_req_id || '-DPhase:' || v_dev_phase || '-DStatus:' || v_dev_status ||
                                    chr(10) || 'MSG:' || v_message;

                -- Error Returned
                IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
                THEN
                    l_statement := 'EBS Conc Prog Completed with problems ' || v_error_message || '.';
                    l_err_msg := l_statement;
                    dbms_output.put_line(l_statement);
                    RAISE program_error;
                ELSE
                    retcode := 1;
                END IF;

            ELSE
                  l_statement := 'EBS Conc Program Wait timed out';
                l_err_msg := l_statement;
                  dbms_output.put_line(l_statement);
                  RAISE program_error;
            END IF;

          ELSE
            l_statement := 'EBS Conc Program not initated';
            l_err_msg := l_statement;
            dbms_output.put_line(l_statement);
            RAISE program_error;
          END IF;

   EXCEPTION
   WHEN program_error THEN
       ROLLBACK;
    l_err_code := 2;
    l_err_msg  := substr ( (l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000)), 1, 3000);
    dbms_output.put_line(l_err_msg);

    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(SQLERRM,1,2000)
                                        ,p_error_desc        => substr(l_err_msg,1,2000)
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AR');
       retcode := l_err_code;
    errbuf := l_err_msg;
   WHEN OTHERS THEN
    l_err_code := 2;
    l_err_msg  := substr ( (l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000)), 1, 3000);
    dbms_output.put_line(l_err_msg);

    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(SQLERRM, 1, 2000)
                                        ,p_error_desc        => substr(l_err_msg,1,2000)
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AR');

       retcode := l_err_code;
    errbuf := l_err_msg;
   end submit_job;  
  
end xxcus_ar_set_geo_val_level;
/
