

CREATE OR REPLACE PACKAGE BODY apps.xxwc_wf_util_pkg
-- This procedure will assign the WC Role - Associate
-- role to all White Cap associates (GL segment1 = 0W) who
-- do not already have it via an indirect assignment.
-- It will also revoke it from anyone who has received it
-- through an indirect assignment.
-- The intent is to ensure all White Cap associates have
-- this role while also ensuring we do not double-assign
-- the role for a long period of time (that can cause odd behavior
-- in the apps, like responsibilities showing up twice or three times in lists).
-- This can be run ad-hoc or be scheduled as a daily concurrent program
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- Rasikha Galimova created by
-- Andre Rivas 24-MAY-2004
-- Rasikha Galimova 1/14/2013 remove responsibilities with word LIKE '%inquiry%'
IS
    l_err_code         VARCHAR2 (24) := 0;

    l_errbuf           CLOB;
    l_package_name     VARCHAR2 (128) := UPPER ('xxwc_wf_util_pkg');
    l_procedure_name   VARCHAR2 (128);
    l_distro_list      VARCHAR2 (75) DEFAULT 'andre.rivas@hdsupply.com';
    l_req_id           NUMBER := fnd_global.conc_request_id;

    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    PROCEDURE xxwc_assign_default_role (p_errbuf               OUT VARCHAR2
                                       ,p_retcode              OUT VARCHAR2
                                       ,p_expiration_date          DATE DEFAULT NULL
                                       ,p_admin_user        IN     VARCHAR2
                                       ,p_start_date        IN     DATE)
    IS
        v_admin_user               VARCHAR2 (64);
        v_admin_user_id            NUMBER;
        v_adm_responsibility_key   VARCHAR2 (256) := 'XXWC_IT_SECURITY_ADMINISTRATOR';
        v_responsibility_id        NUMBER;
        v_resp_application_id      NUMBER;
        v_default_role_name        VARCHAR2 (256) := 'UMX|XXWC_ROLE_ASSOCIATE';
        v_expiration_date          DATE;
        v_start_date               DATE := TRUNC (SYSDATE);
    BEGIN
        v_admin_user := p_admin_user;
        l_procedure_name := 'xxwc_assign_default_role';
        l_err_code := '0';
        v_expiration_date := p_expiration_date;
        v_start_date := p_start_date;

        BEGIN
            --find admin user_id
            SELECT user_id
              INTO v_admin_user_id
              FROM fnd_user
             WHERE user_name = v_admin_user AND SYSDATE BETWEEN start_date AND NVL (end_date, TRUNC (SYSDATE) + 1);
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                l_errbuf := 'UserName ' || v_admin_user || ' not defined in Oracle';
                RAISE;
            WHEN OTHERS
            THEN
                l_errbuf := 'Error deriving user_id for UserName - ' || v_admin_user;
                RAISE;
        END;

        BEGIN
            SELECT responsibility_id, application_id
              INTO v_responsibility_id, v_resp_application_id
              FROM fnd_responsibility_vl
             WHERE     responsibility_key = v_adm_responsibility_key
                   AND SYSDATE BETWEEN start_date AND NVL (end_date, TRUNC (SYSDATE) + 1);
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                l_errbuf := 'Responsibility key ' || v_adm_responsibility_key || ' not defined in Oracle';
                RAISE;
            WHEN OTHERS
            THEN
                l_errbuf := 'Error deriving Responsibility_id for  ' || v_adm_responsibility_key;
                RAISE;
        END;

        -- Apps Initialize
        fnd_global.apps_initialize (v_admin_user_id, v_responsibility_id, v_resp_application_id);

        FOR r
            IN (  SELECT DISTINCT
                         UPPER (u.user_name) user_name
                        ,per.full_name
                        ,gc.segment1
                        ,per.employee_number
                        ,ass.effective_start_date assignment_start_date
                        ,DECODE (ass.effective_end_date
                                ,TO_DATE ('31-Dec-4712', 'DD-MON-YYYY'), NULL
                                ,ass.effective_end_date)
                             assignment_end_date
                    --count(*)
                    FROM apps.per_people_f per
                         INNER JOIN apps.per_all_assignments_f ass
                             ON ass.person_id = per.person_id
                         INNER JOIN apps.gl_code_combinations gc
                             ON gc.code_combination_id = ass.default_code_comb_id
                         INNER JOIN apps.fnd_user u
                             ON u.employee_id = per.person_id
                   WHERE                                          /* Only include White Cap associates, segment1='0W' */
                        gc   .segment1 = '0W'
                         AND NVL (per.effective_end_date, SYSDATE + 1) > SYSDATE
                         AND NVL (u.end_date, SYSDATE + 1) > SYSDATE
                         AND /* Exclude users who have already been directly granted one of our
                                approved roles. They all start with UMX|XXWC_ROLE. */
                            u.user_name NOT IN
                                 (SELECT wa.user_name
                                    FROM apps.wf_all_user_role_assignments wa
                                   WHERE     wa.role_name LIKE 'UMX|XXWC_ROLE%'
                                         AND u.user_name = wa.user_name
                                         AND wa.assignment_type = 'DIRECT'
                                         AND wa.start_date <= SYSDATE
                                         AND NVL (wa.end_date, SYSDATE + 1) > SYSDATE)
                         AND u.user_name NOT IN
                                 (SELECT wa.user_name
                                    FROM apps.wf_all_user_role_assignments wa
                                   WHERE     wa.role_name = 'UMX|XXWC_ROLE_ASSOCIATE'
                                         AND u.user_name = wa.user_name
                                         AND wa.assignment_type = 'INHERITED'
                                         AND wa.start_date <= SYSDATE
                                         AND NVL (wa.end_date, SYSDATE + 1) > SYSDATE)
                ORDER BY full_name ASC)
        LOOP
            xxwc_wf_util_pkg.xxwc_assign_role (p_user_name         => r.user_name
                                              ,p_role_name         => v_default_role_name
                                              ,p_start_date        => v_start_date
                                              ,p_expiration_date   => NVL (v_expiration_date, r.assignment_end_date));
        END LOOP;

        --  If the user has any other directly assigned XXWC_ROLE role,
        --and also has the XXWC_ROLE_ASSOCIATE inherited role, then revoke the XXWC_ROLE_ASSOCIATE directly assigned role
        --will expire it  SYSDATE - 1 / 24

        FOR r IN (  SELECT tu.user_name, COUNT (assignment_type)
                      FROM (SELECT DISTINCT wa.user_name, assignment_type
                              FROM apps.wf_all_user_role_assignments wa
                             WHERE     wa.role_name LIKE '%XXWC_ROLE_ASSOCIATE%'
                                   AND user_name NOT LIKE 'XXWC%'
                                   AND wa.start_date <= SYSDATE
                                   AND NVL (wa.end_date, SYSDATE + 1) > SYSDATE) tu
                  GROUP BY tu.user_name
                    HAVING COUNT (assignment_type) > 1)
        LOOP
            wf_local_synch.propagateuserrole (p_user_name            => r.user_name
                                             ,                                                --            in varchar2,
                                              p_role_name            => 'UMX|XXWC_ROLE_ASSOCIATE'
                                             ,                                                                        --
                                              p_role_orig_system     => 'UMX'
                                             ,                                                                        --
                                              p_expiration_date      => SYSDATE - 1 / 24
                                             ,                                                                        --
                                              p_parent_orig_system   => 'UMX'
                                             ,p_updatewho            => TRUE);
            COMMIT;
        END LOOP;

        l_err_code := '0';
        p_retcode := l_err_code;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            l_err_code := '2';
            p_retcode := l_err_code;

            l_procedure_name := UPPER ('xxwc_assign_default_role');
            l_errbuf :=
                   l_errbuf
                || 'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_log (l_errbuf);
            p_errbuf := SUBSTR (l_errbuf, 1, 2000);
    END;

    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    PROCEDURE xxwc_assign_role (p_user_name          VARCHAR2
                               ,p_role_name          VARCHAR2
                               ,p_start_date         DATE
                               ,p_expiration_date    DATE DEFAULT NULL)
    IS
        v_user_name         VARCHAR2 (128);
        v_start_date        DATE := TRUNC (SYSDATE);
        v_expiration_date   DATE;
        v_role_name         VARCHAR2 (512);
    BEGIN
        -- local
        l_err_code := '0';
        l_procedure_name := 'xxwc_assign_role';
        v_user_name := UPPER (p_user_name);
        v_start_date := p_start_date;
        v_role_name := p_role_name;
        v_expiration_date := p_expiration_date;

        wf_local_synch.propagateuserrole (p_user_name          => v_user_name
                                         ,p_role_name          => v_role_name
                                         ,p_assignmentreason   => 'Base Associate role'
                                         ,p_start_date         => v_start_date
                                         ,p_updatewho          => TRUE
                                         ,p_expiration_date    => v_expiration_date);

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            l_err_code := '2';

            l_procedure_name := UPPER ('xxwc_assign_role');
            l_errbuf :=
                   l_errbuf
                || 'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_log (l_errbuf);
    END;

    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    PROCEDURE xxwc_log (p_message VARCHAR2)
    IS
    BEGIN
        DBMS_OUTPUT.put_line ('*Message from ' || l_procedure_name || ' ' || p_message);

        -- fnd_file.put_line (fnd_file.LOG, '*Message from ' || l_procedure_name || ' ' || p_message);

        IF l_err_code <> 0
        THEN
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => l_package_name
               ,p_calling             => l_procedure_name
               ,p_request_id          => l_req_id
               ,p_ora_error_msg       => SUBSTR (p_message, 1, 2000)
               ,p_error_desc          => 'Error running ' || l_package_name || '.' || l_procedure_name
               ,p_distribution_list   => l_distro_list
               ,p_module              => 'XXWC');
        END IF;

        l_errbuf := NULL;
    END;

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    -- PROCEDURE xxwc_security_roles_report;
    --******************************************************************************
    --   Include one row for every active User record that meets these criteria
    --    User name
    --    First name and Last name
    --    User Email from User record
    --    Start date / End Date
    --    Supervisor Name from employee
    --    Employee Number
    --    A column named "Alert"
    -- Criteria
    --   The user is either
    --   a. In the White Cap organization based on the employee record's GL segment1 = 0W
    --   b. Assigned any XXWC_ROLE_% role or XXWC_% responsibility directly or indirectly
    --        that is active (end/start date) because some users might not even have
    --         an Employee record or might not be assigned to the WC organization
    --     Then any of the following can be true
    --    1. The user has been assigned more than one XXWC_ROLE% role directly,
    --       excluding the XXWC_ROLE_ASSOCIATE role (Alert = "Multiple Roles")
    --    2. The user has been assigned any responsibility directly other than
    --      iExpense (Alert = "Direct Responsibility"")
    --    The purpose is to run this report every day to see if any support staff
    --    has more rights than in WC Role and responsibility assignment policy.
    --   After executing:  begin xxwc_wf_util_pkg.xxwc_security_roles_report; end;
    --    ,run below select for the Report, it can be a APPS Concurrent Program:
    /*   SELECT DISTINCT user_name_oracle,
                        first_name,
                        last_name,
                        email_address,
                        employee_number,
                        full_name,
                        assignment_start_date,
                        assignment_end_date,
                        manager_first_name,
                        manager_last_name,
                        manager_email,
                        segment1,
                        alert1,
                        alert2
          FROM xxwc.xxwc_security_roles_report
        WHERE alert1 IS NOT NULL OR alert2 IS NOT NULL;


    */
    PROCEDURE refresh_security_role_report
    IS
        s_string   CLOB;
    BEGIN
        EXECUTE IMMEDIATE 'alter table xxwc.xxwc_security_roles_report nologging';

        EXECUTE IMMEDIATE 'truncate table xxwc.xxwc_security_roles_report';

        COMMIT;

        s_string :=
            'insert /*+append*/ into xxwc.xxwc_security_roles_report( user_name_oracle, first_name, last_name, email_address,
       employee_number, full_name, assignment_start_date,
       assignment_end_date, manager_first_name, manager_last_name,
       manager_email, segment1, responsibility_id,
       assignment_type, user_name, role_name, relationship_id,
       assigning_role, start_date, end_date, created_by,
       creation_date, last_updated_by, last_update_date,
       last_update_login, user_start_date, role_start_date,
       assigning_role_start_date, user_end_date, role_end_date,
       assigning_role_end_date, partition_id,
       effective_start_date, effective_end_date, user_orig_system,
       user_orig_system_id, role_orig_system, role_orig_system_id,
       parent_orig_system, parent_orig_system_id, owner_tag,
       assignment_reason) SELECT DISTINCT
       UPPER (u.user_name) user_name_oracle,per.first_name,per.last_name,
       per.email_address,per.employee_number,per.full_name,ass.effective_start_date assignment_start_date,
       DECODE (ass.effective_end_date, TO_DATE (''31-Dec-4712'', ''DD-MON-YYYY''), NULL, ass.effective_end_date)assignment_end_date,
       (SELECT first_name FROM per_all_people_f
         WHERE person_id = ass.supervisor_id AND effective_end_date > SYSDATE and effective_start_date <= SYSDATE) manager_first_name,
       (SELECT last_name FROM per_all_people_f
         WHERE person_id = ass.supervisor_id AND effective_end_date > SYSDATE and effective_start_date <= SYSDATE) manager_last_name,
       (SELECT email_address FROM per_all_people_f
         WHERE person_id = ass.supervisor_id AND effective_end_date > SYSDATE and effective_start_date <= SYSDATE) manager_email,
       (SELECT segment1
          FROM apps.gl_code_combinations
         WHERE code_combination_id = ass.default_code_comb_id)
           segment1,
       wur.role_orig_system_id responsibility_id, wur.assignment_type,
       waf.*
  FROM apps.per_people_f per,
       apps.per_all_assignments_f ass,
       apps.fnd_user u,
       (SELECT wa.user_name
          FROM apps.wf_all_user_roles wa
         WHERE     wa.role_name LIKE ''UMX|XXWC_ROLE%''
               AND wa.start_date <= SYSDATE
               AND NVL (wa.expiration_date, SYSDATE + 1) > SYSDATE) xxwa,
      wf_user_role_assignments waf,
     wf_all_user_roles wur
WHERE     ass.person_id(+) = per.person_id
       AND xxwa.user_name = u.user_name
       AND waf.user_name = xxwa.user_name
       AND u.employee_id = per.person_id
       AND NVL (per.effective_end_date, SYSDATE + 1) > SYSDATE
       AND NVL (u.end_date, SYSDATE + 1) > SYSDATE
       AND wur.role_orig_system = ''FND_RESP''
       AND NOT waf.role_name LIKE ''FND_RESP|%|ANY''
       AND waf.role_name = wur.role_name
       AND waf.user_name = wur.user_name
       and NVL (waf.end_date, SYSDATE + 1) > SYSDATE';

        -- dbms_output.put_line (s_string);

        EXECUTE IMMEDIATE s_string;

        COMMIT;
        --    EXECUTE IMMEDIATE 'alter table xxwc.xxwc_security_roles_report add(alert1 varchar2(256),alert2 varchar2(256))';

        --   EXECUTE IMMEDIATE 'alter table xxwc.xxwc_security_roles_report add(responsibility_key varchar2(112))';

        s_string := 'DECLARE
    v_delete   NUMBER := 0;
BEGIN
    FOR r IN (SELECT ROWID b, responsibility_id FROM xxwc.xxwc_security_roles_report)
    LOOP
        BEGIN
            v_delete := 0;

            FOR rinner
                IN (SELECT g.responsibility_id
                      FROM fnd_responsibility_vl g
                     WHERE     (   LOWER (g.description) LIKE ''%inquiry%''
                                OR LOWER (g.responsibility_name) LIKE ''%inquiry%''
                                OR LOWER (g.responsibility_name) LIKE ''%read only%''
                                OR LOWER (g.responsibility_name) LIKE ''%read-only%'')
                           AND g.responsibility_id = r.responsibility_id)
            LOOP
                DELETE FROM xxwc.xxwc_security_roles_report
                      WHERE ROWID = r.b;

                v_delete := 1;
            END LOOP;

            IF v_delete = 0
            THEN
                UPDATE xxwc.xxwc_security_roles_report t
                   SET responsibility_key =
                           (SELECT responsibility_key
                              FROM fnd_responsibility
                             WHERE     responsibility_id = r.responsibility_id
                                   AND NVL (end_date, SYSDATE + 1) > SYSDATE
                                   AND ROWNUM = 1)
                 WHERE ROWID = r.b;
            END IF;
        EXCEPTION
            WHEN OTHERS
            THEN
                DBMS_OUTPUT.put_line (r.responsibility_id);
        END;
    END LOOP;
END;';

        --dbms_output.put_line (s_string);
        EXECUTE IMMEDIATE s_string;

        COMMIT;
        s_string :=
            'BEGIN
    FOR r IN (  SELECT COUNT (assignment_type) rt, user_name
                  FROM xxwc.xxwc_security_roles_report
                 WHERE assignment_type = ''D'' AND NVL(responsibility_key,''x'') <>''HDS_OIE_USER''-- and role_name
              GROUP BY user_name
                HAVING COUNT (assignment_type) > 0)
    LOOP
        UPDATE xxwc.xxwc_security_roles_report
           SET alert1 =
                      ''The user has been assigned to ''
                   || r.rt
                   || '' responsibility directly other than iExpense (Alert =  Direct Responsibility)''
         WHERE user_name = r.user_name;
    END LOOP;
END;';

        --dbms_output.put_line (s_string);
        EXECUTE IMMEDIATE s_string;

        COMMIT;
        s_string :=
            'BEGIN
    FOR r
        IN ( SELECT COUNT (DISTINCT assigning_role) rt, user_name,segment1
    FROM xxwc.xxwc_security_roles_report
   WHERE     NVL (assigning_role, ''x'') <> ''UMX|XXWC_ROLE_ASSOCIATE''
         AND assigning_role LIKE ''UMX|XXWC%''
GROUP BY user_name,segment1
  HAVING COUNT (DISTINCT assigning_role) > 1
)
    LOOP
        UPDATE xxwc.xxwc_security_roles_report
           SET alert2 =
                      '' The user has been assigned more than one ''
                   || r.rt
                   || '' XXWC_ROLE% role directly, excluding the XXWC_ROLE_ASSOCIATE role (Alert =  Multiple Roles )''
         WHERE user_name = r.user_name;
    END LOOP;
END;';

        --dbms_output.put_line (s_string);
        EXECUTE IMMEDIATE s_string;

        COMMIT;
    /*--report is here
    SELECT DISTINCT user_name_oracle,
                    first_name,
                    last_name,
                    email_address,
                    employee_number,
                    full_name,
                    assignment_start_date,
                    assignment_end_date,
                    manager_first_name,
                    manager_last_name,
                    manager_email,
                    segment1,
                    alert1,
                    alert2
      FROM xxwc.xxwc_security_roles_report
    WHERE alert1 IS NOT NULL OR alert2 IS NOT NULL;
    */
    END;

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    -- PROCEDURE xxwc_format_role_report (p_report OUT xxwc_wf_role_report)
    --will return cursor which can be used by Oracle CP  with sql executable
    --VARIABLE s_role_rep  REFCURSOR
    --EXECUTE xxwc_wf_util_pkg.xxwc_security_roles_report;
    --EXECUTE xxwc_wf_util_pkg.xxwc_wf_util_pkg(:s_role_rep);
    --put here some formatting
    --print s_role_rep
    --******************************************************************************
    PROCEDURE xxwc_format_role_report (p_report OUT xxwc_wf_role_report)
    IS
        v_rec_count   NUMBER;
    BEGIN
        SELECT COUNT ('a')
          INTO v_rec_count
          FROM xxwc.xxwc_security_roles_report
         WHERE alert1 IS NOT NULL OR alert2 IS NOT NULL;

        IF v_rec_count = 0
        THEN
            OPEN p_report FOR SELECT '***** No Data Found *****' FROM DUAL;
        ELSE
            OPEN p_report FOR
                SELECT DISTINCT user_name_oracle
                               ,first_name
                               ,last_name
                               ,email_address
                               ,employee_number
                               ,full_name
                               ,assignment_start_date
                               ,assignment_end_date
                               ,manager_first_name || ' ' || manager_last_name manager
                               ,manager_email
                               ,segment1 location
                               ,alert1
                               ,alert2
                  FROM xxwc.xxwc_security_roles_report
                 WHERE alert1 IS NOT NULL OR alert2 IS NOT NULL;
        END IF;
    END;
END;
/

-- Grants for Package Body
GRANT EXECUTE ON apps.xxwc_wf_util_pkg TO xxwc_prism_execute_role
/
GRANT EXECUTE ON apps.xxwc_wf_util_pkg TO bs006141
/
GRANT EXECUTE ON apps.xxwc_wf_util_pkg TO xxwc_dev_admin WITH GRANT OPTION
/
GRANT EXECUTE ON apps.xxwc_wf_util_pkg TO interface_prism
/

-- End of DDL Script for Package Body APPS.XXWC_WF_UTIL_PKG
