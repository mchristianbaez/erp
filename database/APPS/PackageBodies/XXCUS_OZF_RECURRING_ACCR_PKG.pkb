/**************************************************************************
   $Header xxcus_ozf_recurring_accr_pkg $
   Module Name: ORACLE TRADE MANAGEMENT

   PURPOSE:   RECURRING ACCRUAL PROCESS.
   REVISIONS:
   Ver        Date        Author             	Description
   ---------  ----------  ---------------   	-------------------------
    1.0       10/23/2018  Bala Seshadri   	Initial Build - Task ID: 20180116-00033
/*************************************************************************/
CREATE OR REPLACE PACKAGE BODY APPS.xxcus_ozf_recurring_accr_pkg is
  --
  g_adj_rec OZF_FUND_UTILIZED_PUB.adjustment_rec_type; 
  --
  type g_recurr_accrual_log_type is table of xxcus.xxcus_ozf_recurr_accrual_log%rowtype index by binary_integer;
  g_recurr_accrual_log_rec g_recurr_accrual_log_type;
  --
  type g_ozf_utils_stg_type is table of xxcus.xxcus_ozf_utils_stg%rowtype index by binary_integer;
  g_ozf_utils_stg_rec g_ozf_utils_stg_type;
  --  
  type g_ozf_misc_utils_type is table of xxcus.xxcus_ozf_misc_accruals%rowtype index by binary_integer;  
  g_ozf_misc_utils_rec g_ozf_misc_utils_type;
  --
  g_run_id Number :=0;
  g_debug varchar2(1) :=Null;
  g_final varchar2(10) :='FINAL';
  g_draft varchar2(10) :='DRAFT';
  --
  g_max_line_num number :=9999999;
  g_mode varchar2(10) :=Null;
  g_log_idx number :=0;
  g_stg_idx number :=0;
  g_misc_idx number :=0;
  --
  g_validated_No varchar2(1) :='N';
  g_validated_Yes  varchar2(1) :='Y'; 
  g_Zero Number :=0;
  g_adj_date date :=trunc(sysdate);
  --
  g_TRUE boolean :=TRUE;
  g_FALSE boolean :=FALSE;
  g_err_msg varchar2(2000) :=Null;
  --
  g_status_New xxcus.xxcus_ozf_recurring_accruals.status_code%type :='NEW';
  g_status_Ok    xxcus.xxcus_ozf_recurring_accruals.status_code%type :='COMPLETE';
  g_status_Err   xxcus.xxcus_ozf_recurring_accruals.status_code%type :='ERROR'; 
  g_line_type_standard   varchar2(20) :='STANDARD_ACCRUAL';
  g_line_type_variance    varchar2(20) :='VARIANCE_ACCRUAL';
  g_recurr_accrual_adj varchar2(30) :='RECURRING_ACCRUAL_ADJ'; 
  -- fndLookup_values_vl. lookup_type ='XXCUS_REB_ADJ_REASON'
  g_recurr_accrual_desc varchar2(60) :='Recurring Accrual Adjustments';
  --
  g_stage1 varchar2(240) :='EBS: Stage1 : Picked up for processing';
  g_stage2 varchar2(240) :='EBS: Stage2 : Total purchases fetch complete.';  
  g_stage3 varchar2(240) :='EBS: Stage3 : Branch level allocated accruals fetch complete.';
  g_stage40 varchar2(240) :='EBS: Stage4 : Recurring accrual created.';
  g_stage41 varchar2(240) :='EBS: Stage4 : Recurring accrual failed.';
  g_stage40a  varchar2(240) :='EBS: Stage4 : Recurring accrual created for all receipts for the branch.';
  g_stage40b  varchar2(240) :='EBS: Stage4 : One or more receipt lines failed for the branch.';  
  --
  type g_hdr_tbl is table of xxcus.xxcus_ozf_recurring_accruals%rowtype index by binary_integer;
  g_hdr_rec    g_hdr_tbl;
  --
  type g_line_util_tbl is table of xxcus.xxcus_ozf_recurr_accr_utils%rowtype index by binary_integer;
  g_line_rec    g_line_util_tbl;
  --  
  g_err_Email1 varchar2(200) := 'HDSOracleDevelopers@hdsupply.com';
  g_err_Email2 varchar2(200) :='HDSRebateManagementSystemNotifications@hdsupply.com'; --1.8
  --
  g_branch_lines_success number :=0;
  g_branch_lines_failed number :=0;  
  g_dtl_seq_success number :=0;
  g_dtl_seq_failed number :=0;
  --
  -- The 99999999 variables holds the numbers that are required for the variance accrual to be created that will hit the BU with max purchase.
  --
  g_99999999_max_detail_seq number :=0;
  g_99999999_receipt_amount number :=0;
  g_99999999_br_total_receipt number :=0;
  g_99999999_total_recurr_amount number :=0; 
  g_99999999_fund_id number :=0;
  --
  --
  procedure print_output(p_message in varchar2) is
  begin
       if fnd_global.conc_request_id >0 then
        fnd_file.put_line(fnd_file.output, p_message);
       else
        dbms_output.put_line(p_message);
       end if;   
  exception
   when others then
    dbms_output.put_line( 'Issue in xxcus_ozf_recurring_accr_pkg.print_output routine ='||sqlerrm);
  end print_output;
  --  
  procedure print_log(p_message in varchar2) is
  begin
   if g_debug =g_validated_Yes then
       if fnd_global.conc_request_id >0 then
        fnd_file.put_line(fnd_file.log, p_message);
       else
        dbms_output.put_line(p_message);
       end if;   
   else 
     Null;
   end if;
  exception
   when others then
    dbms_output.put_line( 'Issue in xxcus_ozf_recurring_accr_pkg.print_log routine ='||sqlerrm);
  end print_log;
  --
    procedure Variance_Utilization_Log
      (
           p_ozf_misc_utils_rec in g_ozf_misc_utils_type
      ) is
     --
     PRAGMA AUTONOMOUS_TRANSACTION;
     --
    begin
         --
         If p_ozf_misc_utils_rec.Count >0 then 
           --
           Forall indx in 1 .. p_ozf_misc_utils_rec.Count
           Insert Into xxcus.xxcus_ozf_misc_accruals Values p_ozf_misc_utils_rec(indx);       
           --
          Commit; --Since this procedure is declared as a pragma auto transaction, we will commit here which is not going to affect the main block
           --           
         End if;
         --
    exception
     when others then
      print_log ('@xxcus_ozf_recurring_accr_pkg.Variance_Utilization_Log, '||'Message ='||sqlerrm);
    end Variance_Utilization_Log;
    --    
    procedure Utilization_Log
      (
           p_ozf_utils_stg_rec in g_ozf_utils_stg_type
      ) is
     --
     PRAGMA AUTONOMOUS_TRANSACTION;
     --
    begin
         --
         If p_ozf_utils_stg_rec.Count >0 then 
           --
           Forall indx in 1 .. p_ozf_utils_stg_rec.Count
           Insert Into xxcus.xxcus_ozf_utils_stg Values p_ozf_utils_stg_rec(indx);       
           --
          Commit; --Since this procedure is declared as a pragma auto transaction, we will commit here which is not going to affect the main block
           --           
         End if;
         --
    exception
     when others then
      print_log ('@xxcus_ozf_recurring_accr_pkg.Utilization_Log, '||'Message ='||sqlerrm);
    end Utilization_Log;
    --   
    procedure Appln_Log
      (
           p_recurr_accrual_log_rec in g_recurr_accrual_log_type
      ) is
     --
     PRAGMA AUTONOMOUS_TRANSACTION;
     --
    begin
         --
         If p_recurr_accrual_log_rec.Count >0 then 
           --
           Forall indx in 1 .. p_recurr_accrual_log_rec.Count
           Insert Into xxcus.xxcus_ozf_recurr_accrual_log Values p_recurr_accrual_log_rec(indx);       
           --
          Commit; --Since this procedure is declared as a pragma auto transaction, we will commit here which is not going to affect the main block
           --           
         End if;
         --
    exception
     when others then
      print_log ('@xxcus_ozf_recurring_accr_pkg.Appln_Log, '||'Message ='||sqlerrm);
    end Appln_Log;
    --  
  procedure update_header_status
      (
          p_rec_seq in number
         ,p_status_code in varchar2
         ,p_status_desc in varchar2
         ,p_ebs_updated_by in number   
      ) is  
   --PRAGMA AUTONOMOUS_TRANSACTION;
  begin
        --
         update xxcus.xxcus_ozf_recurring_accruals
         set
                ebs_update_date =sysdate
               ,status_code = case when p_status_code is not null then p_status_code else status_code end 
               ,status_desc = case when p_status_desc is not null then p_status_desc else status_desc end               
               ,ebs_updated_by = case when p_ebs_updated_by is not null then p_ebs_updated_by else ebs_updated_by end       
         where 1 =1
               and record_seq =p_rec_seq
         ;        
         --
         commit;
         --  
  exception
   when others then
    dbms_output.put_line( 'Issue in xxcus_ozf_recurring_accr_pkg.update_header_status, routine ='||sqlerrm);
  end update_header_status;
  --  
  procedure update_alloc_util_status 
      (
          p_rec_seq in number
         ,p_line_num in number
         ,p_status_code in varchar2
         ,p_status_desc in varchar2
         ,p_ebs_updated_by in number  
      ) is
   --PRAGMA AUTONOMOUS_TRANSACTION;
  begin
        --
         update xxcus.xxcus_ozf_recurr_accr_utils
         set
                ebs_update_date =sysdate
               ,status_code = case when p_status_code is not null then p_status_code else status_code end 
               ,status_desc = case when p_status_desc is not null then p_status_desc else status_desc end               
               ,ebs_updated_by = case when p_ebs_updated_by is not null then p_ebs_updated_by else ebs_updated_by end                      
         where 1 =1
               and record_seq =p_rec_seq
               and line_num =p_line_num
         ;        
         --
         --print_log('@ after update utils status to stage 4, record seq '||p_rec_seq||', line_num ='||p_line_num||', utilization_id ='||p_util_id);
         --
         commit;
         --      
  exception
   when others then
    dbms_output.put_line( 'Issue in xxcus_ozf_recurring_accr_pkg.update_alloc_util_status, routine ='||sqlerrm);
  end update_alloc_util_status;
  -- 
  procedure log_error (p_error_rec in xxcus.xxcus_ozf_recurring_accr_err%rowtype) is
   PRAGMA AUTONOMOUS_TRANSACTION;
  begin
        insert into xxcus.xxcus_ozf_recurring_accr_err
         (
           record_seq
           ,line_num
          ,err_seq
          ,err_message
          ,run_id
          ,created_by
          ,detail_seq
         )
        values
         (
           p_error_rec.record_seq
          ,p_error_rec.line_num
          ,p_error_rec.err_seq
          ,p_error_rec.err_message
          ,p_error_rec.run_id
          ,p_error_rec.created_by
          ,p_error_rec.detail_seq          
         )
        ;
         --
         commit;
         --        
  exception
   when others then
    dbms_output.put_line( 'Issue in xxcus_ozf_recurring_accr_pkg.log_error, routine ='||sqlerrm);
  end log_error;
  --     
  function get_offer_fund_id (p_offer_id in number, p_offer_name in varchar2, p_rec_seq in number) return number is
    l_fund_id number :=0;
  begin  
         --
       select budget_source_id
       into     l_fund_id
       from  ozf_offers
       where 1 =1
            and offer_id =p_offer_id;   
        --
        return l_fund_id;
        --
  exception
   when no_data_found then
    print_log('Recurring accrual record seq : '||p_rec_seq||', failed to get fund id from function get_offer_fund_id for offer id =>'||p_offer_id||', name ='||p_offer_name);   
    return l_fund_id;
   when others then
    print_log( 'Issue in function get_offer_fund_id : msg =>'||sqlerrm);
    return l_fund_id;
  end get_offer_fund_id;
  --  
 procedure Validate (p_ok OUT boolean, p_rec_seq IN number) is
  --
  cursor c_new is
  select *
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and  run_id =g_run_id  
      and  validated_flag =g_validated_No
      and  status_code =g_status_New      
      and  p_rec_seq is not null
      and  record_seq =p_rec_seq
   union all
  select *
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and  run_id =g_run_id
      and  validated_flag =g_validated_No
      and  status_code =g_status_New
      and  p_rec_seq is null    
  ; 
  --
  type c_new_type is table of c_new%rowtype index by binary_integer;
  c_new_recs c_new_type;
  --
  l_user_id number :=fnd_global.user_id;
  l_bu_rec xxcus.xxcus_pam_bu_name_tbl%rowtype :=Null;
  --
  l_customer_id    xxcus.xxcus_ozf_recurring_accruals.cust_account_id%type :=0;
  l_plan_id              xxcus.xxcus_ozf_recurring_accruals.plan_id%type :=0;
  l_reason_code   xxcus.xxcus_ozf_recurring_accruals.reason_code%type :=Null;
  --
  l_mvid_ok BOOLEAN :=Null;
  l_offer_ok BOOLEAN :=Null;
  l_currency_ok BOOLEAN :=Null;
  --
  l_prereq_status  xxcus.xxcus_ozf_recurring_accruals.status_code%type :=Null;
  --
  l_reason_ok BOOLEAN :=Null;
  l_date_ok BOOLEAN :=Null;        
  l_BU_ok BOOLEAN :=Null;  
  --
  l_seq number :=0;
  l_currency_code varchar2(10) :=Null;
  --
  l_USD  varchar2(10) :='USD';   --US currency code
  l_CAD  varchar2(10) :='CAD';   --Canadian currency code
  --
  l_from_date date :=Null;
  l_to_date date :=Null;  
  --
 begin
      --
      begin 
        --
        open c_new;
        fetch c_new bulk collect into c_new_recs;
        close c_new;
        --
        if (c_new_recs.count >g_Zero) then
             --
             print_log( 'Check pre requisites and update critical information, found new records, total : '||c_new_recs.count);
             --  
             for idx1 in 1 .. c_new_recs.count loop  
                  --
                  l_seq :=idx1;
                  --
                  begin 
                        --
                        savepoint sqr1; -- 02/02/2018
                        --
                        -- Check if mvid is correct
                        --
                        begin
                                select customer_id 
                                into    l_customer_id
                                from xxcus.xxcus_rebate_customers
                                where 1 =1
                                    and  customer_attribute1 ='HDS_MVID'
                                    and  customer_attribute2 =c_new_recs(idx1).mvid
                                ;
                                --
                                update xxcus.xxcus_ozf_recurring_accruals a 
                                set a.cust_account_id =l_customer_id
                                where 1 =1
                                and a.record_seq =c_new_recs(idx1).record_seq   
                                and l_customer_id >0
                                ; 
                            --
                            if (sql%rowcount >0) then 
                                --
                                l_prereq_status :=substr('MVID valid ', 1, 240);
                                l_mvid_ok :=TRUE;
                                print_log('l_prereq_status : '||l_prereq_status);
                                --                        
                            else
                                --
                                l_prereq_status :=substr('MVID invalid ', 1, 240);
                                l_mvid_ok :=FALSE;                            
                                --
                            end if;
                            --
                        exception
                         when no_data_found then
                            --
                            l_prereq_status :=substr('MVID check : Invalid MVID ', 1, 240);
                            l_mvid_ok :=FALSE;                            
                            --                                   
                         when too_many_rows then  
                            --
                            l_prereq_status :=substr('MVID check : More than 1 MVID ', 1, 240);
                            l_mvid_ok :=FALSE;                            
                            --                                                         
                         when others then
                            --
                            l_prereq_status :=substr('MVID check : Misc error ', 1, 240);
                            l_mvid_ok :=FALSE;
                            --                                                             
                        end;
                        --      
                        --print_log('@ MVID check, l_prereq_status : '||l_prereq_status);                                    
                        --
                        -- Check if offer name is correct
                        --
                        begin
                                select plan_id 
                                into    l_plan_id
                                from xxcus.xxcus_pam_program_list_tbl
                                where 1 =1
                                and agreement_name =c_new_recs(idx1).offer_name
                                and cust_id                     =l_customer_id
                                ;
                                --
                                update xxcus.xxcus_ozf_recurring_accruals a 
                                set a.plan_id =l_plan_id
                                where 1 =1
                                     and a.record_seq =c_new_recs(idx1).record_seq  
                                     and l_plan_id >0
                                ;
                                --
                                if (sql%rowcount >0) then
                                    --
                                    l_prereq_status :=substr(l_prereq_status||', Offer valid ', 1, 240);
                                    l_offer_ok :=TRUE;
                                    --                        
                                else
                                    --
                                    l_prereq_status :=substr(l_prereq_status||', Offer invalid ', 1, 240);
                                    l_offer_ok :=FALSE;
                                    --                         
                                end if;
                                --
                        exception
                         when no_data_found then
                            --
                            l_prereq_status :=substr('Offer check : Invalid ', 1, 240);
                            l_offer_ok :=FALSE;                            
                            --                                   
                         when too_many_rows then  
                            --
                            l_prereq_status :=substr('Offer check : More than 1 Offer ', 1, 240);
                            l_offer_ok :=FALSE;                            
                            --                                                         
                         when others then
                            --
                            l_prereq_status :=substr('Offer check : Misc error ', 1, 240);
                            l_offer_ok :=FALSE;
                            --                                                             
                        end;
                        --
                        --print_log('@ Offer check, l_prereq_status : '||l_prereq_status);                                    
                        --                        
                        -- Check if currency code is correct
                        --
                        begin       
                            --
                            if (c_new_recs(idx1).currency IN (l_USD, l_CAD)) then
                                --
                                l_prereq_status :=substr(l_prereq_status||', Currency valid, ', 1, 240);
                                l_currency_ok :=TRUE;
                                --                        
                            else
                                --
                                l_prereq_status :=substr(l_prereq_status||', Currency invalid, ', 1, 240);
                                l_currency_ok :=FALSE;
                                --                         
                            end if;                     
                            --                                              
                        exception
                         when others then
                            --
                            l_prereq_status :=substr(l_prereq_status||'Currency : Misc error ', 1, 240);
                            l_currency_ok :=FALSE;
                            --
                        end;
                        --
                        --print_log('@ Currency check, l_prereq_status : '||l_prereq_status);                                    
                        --                        
                        -- Check if reason name is correct
                        --
                        begin
                                select lookup_code
                                into    l_reason_code
                                from  fnd_lookup_values_vl
                                where 1 =1
                                and lookup_type ='XXCUS_REB_ADJ_REASON'
                                and upper(meaning) =upper(c_new_recs(idx1).reason_name)
                                ;
                                --
                                update xxcus.xxcus_ozf_recurring_accruals a 
                                set a.reason_code =l_reason_code
                                where 1 =1
                                     and a.record_seq =c_new_recs(idx1).record_seq   
                                     and l_reason_code is not null
                                ;
                                --
                                if (sql%rowcount >0) then
                                    --
                                    l_prereq_status :=substr(l_prereq_status||', Reason valid ', 1, 240);
                                    l_reason_ok :=TRUE;
                                    --                        
                                else
                                    --
                                    l_prereq_status :=substr(l_prereq_status||', Reason invalid ', 1, 240);
                                    l_reason_ok :=FALSE;
                                    --                  
                                end if;
                                --
                        exception
                         when no_data_found then
                            --
                            l_prereq_status :=substr(l_prereq_status||'Reason name check : Invalid ', 1, 240);
                            l_reason_ok :=FALSE;                            
                            --                                   
                         when too_many_rows then
                            --
                            l_prereq_status :=substr(l_prereq_status||'Reason name check : More than 1 reason ', 1, 240);
                            l_reason_ok :=FALSE;                            
                            --                                                         
                         when others then
                            --
                            l_prereq_status :=substr(l_prereq_status||'Reason name check : Misc error ', 1, 240);
                            l_reason_ok :=FALSE;
                            --                                                             
                        end;
                        --
                        --print_log('@ reason check, l_prereq_status : '||l_prereq_status);                    
                        --
                        -- Check if receipt end date is less than receipt start date
                        --
                        if ( c_new_recs(idx1).receipt_end_date >= c_new_recs(idx1).receipt_start_date ) then
                            --
                            l_prereq_status :=substr(l_prereq_status||', End date check : Ok, ', 1, 240);
                            l_date_ok :=TRUE;
                            --                        
                        else
                            --
                            l_prereq_status :=substr(l_prereq_status||', End date check : Not ok, ', 1, 240);
                            l_date_ok :=FALSE;                            
                            --                         
                        end if;                     
                        --     
                        --print_log('@ end date check, l_prereq_status : '||l_prereq_status);                                    
                        --                        
                        -- Check if BU name is correct
                        --
                        begin
                            select *
                            into     l_bu_rec
                            from   xxcus.xxcus_pam_bu_name_tbl
                            where 1 =1
                                 and upper(bu_name) =upper(c_new_recs(idx1).bu_name)
                            ;  
                            --
                            l_prereq_status :=substr(l_prereq_status||', BU valid ', 1, 240);
                            l_BU_ok :=TRUE;
                            --
                        exception
                         when no_data_found then
                            --
                            l_prereq_status :=substr('BU check : Invalid ', 1, 240);
                            l_BU_ok :=FALSE;                            
                            --                                   
                         when too_many_rows then  
                            --
                            l_prereq_status :=substr('BU check : More than 1 BU ', 1, 240);
                            l_BU_ok :=FALSE;                            
                            --                                                         
                         when others then
                            --
                            l_prereq_status :=substr('BU check : Misc error ', 1, 240);
                            l_BU_ok :=FALSE;
                            --                                                             
                        end;                  
                        --                   
                        --   
                        print_log('Prereq Status : '||l_prereq_status);                                        
                        --
                        -- if all good update validated_flag with Y else leave it as it is because the default value is always N
                        if 
                           (
                                      (l_BU_ok) -- BU Ok
                             AND (l_date_ok) -- Dates Ok 
                             AND (l_reason_ok) -- Reason Ok                             
                             AND (l_currency_ok) -- Currency Ok                             
                             AND (l_offer_ok) -- Offer Ok                             
                             AND (l_mvid_ok) -- MVID Ok                             
                           ) then 
                            --
                             begin
                               savepoint precheck;
                               update xxcus.xxcus_ozf_recurring_accruals a
                                    set status_desc =l_prereq_status -- status_code =l_prereq_status IMPORTANT 
                                          ,status_code ='STAGE_1'
                                          ,validated_flag =g_validated_Yes
                                          ,lob_party_id =l_bu_rec.lob_id
                                          ,bu_party_id =l_bu_rec.bu_id
                                          ,lob_name =l_bu_rec.lob_name
                                          ,run_id =g_run_id
                                         ,fund_id =
                                                             (
                                                                   select budget_source_id
                                                                   from   ozf_offers
                                                                   where 1 =1
                                                                         and qp_list_header_id =a.plan_id                                                                         
                                                             )
                               where 1 =1
                                    and record_seq =c_new_recs(idx1).record_seq
                                ;
                             exception
                              when others then
                               print_log('Error in updating validated flag for record seq :'||c_new_recs(idx1).record_seq||', msg ='||sqlerrm);
                               rollback to precheck;
                             end;
                        else
                          print_log('prereq status failed : '); 
                            --
                             begin
                               savepoint precheck;
                               update xxcus.xxcus_ozf_recurring_accruals a
                                    set status_desc =l_prereq_status
                                          ,lob_party_id =l_bu_rec.lob_id
                                          ,bu_party_id =l_bu_rec.bu_id
                                          ,lob_name =l_bu_rec.lob_name
                                         ,fund_id =
                                                             (
                                                                   select budget_source_id
                                                                   from   ozf_offers
                                                                   where 1 =1
                                                                         and qp_list_header_id =a.plan_id                                                                         
                                                             )
                               where 1 =1
                                    and record_seq =c_new_recs(idx1).record_seq
                                ;
                             exception
                              when others then
                               print_log('Error in updating validated flag for record seq :'||c_new_recs(idx1).record_seq||', msg ='||sqlerrm);
                               rollback to precheck;
                             end;                          
                        end if; 
                        --
                        --     
                        -- Update fund id for all the current record sequence with no fund id after the first attempt.
                        --
                        begin
                                --
                                print_log('Begin: Update fund id using ozf funds utilized all in second attempt. ');
                                --                                 
                                update xxcus.xxcus_ozf_recurring_accruals a 
                                set a.fund_id =
                                                             (
                                                                select distinct b.fund_id
                                                                from  ozf_funds_utilized_all_b b
                                                                where 1 =1
                                                                     and b.plan_id =a.plan_id
                                                             )
                                where 1 =1   
                                     and a.record_seq =c_new_recs(idx1).record_seq
                                     and (a.fund_id is null or a.fund_id =0)
                                ;
                                --
                                if (sql%rowcount >0) then
                                    --
                                    print_log('Updated fund id using ozf funds utilized all in second attempt for record seq :'||c_new_recs(idx1).record_seq);
                                    --                                                       
                                else
                                    --
                                    Null;
                                    --
                                end if;
                                --
                                print_log('End: Update fund id using ozf funds utilized all in second attempt. ');
                                --                                  
                        exception                                                  
                         when others then
                            --
                            print_log(
                                                'Failed to get fund_id in second attempt, message ='||sqlerrm             
                                             );
                        end;
                        --                            
                        --                                                            
                  exception
                   when others then
                        --
                        g_err_msg :=substr(sqlerrm, 1, 2000);
                        --
                        insert into xxcus.xxcus_ozf_recurring_accr_err
                         (
                           record_seq
                          ,err_seq
                          ,err_message
                          ,run_id
                          ,created_by
                         )
                        values
                         (
                           c_new_recs(idx1).record_seq
                          ,l_seq
                          ,'Cannot update stage1 status : '||g_err_msg
                          ,g_run_id
                          ,l_user_id
                         )
                        ;
                        --
                        --rollback to sqr1; 02/02/2018
                        --
                  end;
                  --
             end loop;
             --              
             p_ok :=g_TRUE; 
             --                 
        else
             --
             print_log('No new records, total : '||c_new_recs.count);
             --     
             p_ok :=g_FALSE;
             --            
        end if;
        --        
         p_ok :=g_TRUE; 
         --
      exception
       when no_data_found then
        p_ok :=g_FALSE;
       when others then
        p_ok :=g_FALSE;
      end;
     --
     commit;
     --
 exception
  when others then
   print_log('Issue in xxcus_ozf_recurring_accr_pkg.validate routine, msg ='||sqlerrm);  
   p_ok :=g_FALSE;   
 end Validate;
  --
 procedure extract_recurring_accruals 
   ( 
      p_rec_seq in number
   ) is
  --
  cursor c_recurring_accruals is
  select 
             mvid
            ,mvid_name
            ,offer_name
            ,currency
            ,reason_name
            ,amount
            ,lob_name
            ,bu_name
            ,branch_name
            ,receipt_start_date
            ,receipt_end_date
            ,validated_flag
            ,status_code
            ,cust_account_id
            ,plan_id
            ,reason_code
            ,apex_created_by
            ,apex_creation_date
            ,ebs_updated_by
            ,ebs_update_date
            ,record_seq
            ,total_purchases 
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and  run_id =g_run_id  
      and p_rec_seq is not null
      and  validated_flag =g_validated_Yes
      and  record_seq =p_rec_seq
      and status_code ='STAGE_1'
  union all
  select 
             mvid
            ,mvid_name
            ,offer_name
            ,currency
            ,reason_name
            ,amount
            ,lob_name
            ,bu_name
            ,branch_name
            ,receipt_start_date
            ,receipt_end_date
            ,validated_flag
            ,status_code
            ,cust_account_id
            ,plan_id
            ,reason_code
            ,apex_created_by
            ,apex_creation_date
            ,ebs_updated_by
            ,ebs_update_date
            ,record_seq
            ,total_purchases  
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and  run_id =g_run_id  
      and p_rec_seq is null
      and  validated_flag =g_validated_Yes  
      and status_code ='STAGE_1'            
  ; 
  --
  type c_recurring_accruals_type is table of c_recurring_accruals%rowtype index by binary_integer;
  c_recurring_accruals_rec   c_recurring_accruals_type;
  --   
  cursor c_get_purchases (p_cust_id in number, p_bu in varchar2, p_branch in varchar2, p_from_date in date, p_to_date in date) is
    select line_attribute6 branch,
                bill_to_cust_account_id branch_cust_acct_id, 
                sum(quantity * selling_price) purchases
     from ozf.ozf_resale_lines_all
     where 1 = 1
       and p_branch is not null -- for a specific ship to branch code
       and line_attribute2 =p_bu
       and date_ordered between p_from_date and p_to_date
       and sold_from_cust_account_id = p_cust_id
       and line_attribute6 =p_branch
    group by line_attribute6, bill_to_cust_account_id
    union all
    select line_attribute6 branch, 
                bill_to_cust_account_id branch_cust_acct_id,
                sum(quantity * selling_price) purchases         
     from ozf.ozf_resale_lines_all
     where 1 = 1
       and p_branch is null -- for all branches
       and line_attribute2 =p_bu       
       and date_ordered between p_from_date and p_to_date
       and sold_from_cust_account_id = p_cust_id
    group by line_attribute6, bill_to_cust_account_id
    ;     
  --
  type c_purchases_type is table of c_get_purchases%rowtype index by binary_integer;
  c_purchases_rec   c_purchases_type;
  --    
  cursor c_detail_spend 
  ( 
     p_cust_id in number
    ,p_bu in varchar2
    ,p_branch in varchar2
    ,p_from_date in date
    ,p_to_date in date
    ,p_rec_seq in number
    ,p_line_num in number
    ,p_total_purchase in number
    ,p_total_recurr_amount in number
    ,p_status_code in varchar2
    ,p_status_desc in varchar2
    ,p_user_id in number
    ,p_plan_id in number
  ) 
  is
    select 
                p_rec_seq record_seq,
                g_run_id run_id,
                p_line_num,                
                p_cust_id cust_account_id,
                p_plan_id plan_id,
                line_attribute6 branch_name,
                bill_to_cust_account_id branch_cust_acct_id,
                (quantity * selling_price) line_receipt_amount,                
                'TP_ORDER' object_type,
                resale_line_id object_id,
                p_total_purchase branch_total_purchase,
                p_total_recurr_amount total_recurring_amount,
                ((quantity * selling_price)/p_total_purchase)*round(p_total_recurr_amount,2) line_recurring_amount ,
                p_status_code status_code,
                p_status_desc status_desc,    
                0 utilization_id,
                p_user_id ebs_updated_by,
                sysdate ebs_update_date,
                Null api_status,
                Null api_message,
                rownum detail_seq,
                 --xxcus.xxcus_ozf_raccr_purdtls_seq.nextval detail_seq,
                 g_mode run_mode                            
     from ozf.ozf_resale_lines_all
     where 1 = 1
       and p_branch is not null -- for a specific ship to branch code
       and line_attribute2 =p_bu
       and date_ordered between p_from_date and p_to_date
       and sold_from_cust_account_id = p_cust_id
       and line_attribute6 =p_branch
       and (quantity <>0 and selling_price <>0) --08/22/2018
    ;  
  --
  type c_SPEND_type is table of c_detail_spend%rowtype index by binary_integer;
  c_SPEND_rec   c_SPEND_type;
  --      
  l_move_fwd boolean :=Null; 
  l_line_num number :=0;
  l_user_id number :=fnd_global.user_id;
  --
  l_total_purchases number :=0;
  l_purchase_percent number :=0;
  l_recurr_accrual_amt number :=0;
  --  
 begin 
   --
   execute immediate 'alter session set nls_date_format ='||''''||'DD-MON-YY'||'''';
   --
   print_log('Begin :  extract_recurring_accruals');
   --
   begin 
    --
     open c_recurring_accruals;
     fetch c_recurring_accruals bulk collect into c_recurring_accruals_rec;
     close c_recurring_accruals;
    --
     if c_recurring_accruals_rec.count >0 then
          --
          for idx1 in 1 .. c_recurring_accruals_rec.count loop
               -- Derive branch level accrual and purchases
               begin
                --
                print_log ('c_recurring_accruals_rec(idx1).cust_account_id ='||c_recurring_accruals_rec(idx1).cust_account_id);
                print_log ('c_recurring_accruals_rec(idx1).bu_name ='||c_recurring_accruals_rec(idx1).bu_name);
                print_log ('c_recurring_accruals_rec(idx1).branch_name ='||c_recurring_accruals_rec(idx1).branch_name);
                print_log ('c_recurring_accruals_rec(idx1).receipt_start_date ='||c_recurring_accruals_rec(idx1).receipt_start_date);
                print_log ('c_recurring_accruals_rec(idx1).receipt_end_date ='||c_recurring_accruals_rec(idx1).receipt_end_date);                                                                
                --
                c_purchases_rec.delete;
                l_total_purchases :=0;
                --
                open c_get_purchases
                  (
                     p_cust_id =>c_recurring_accruals_rec(idx1).cust_account_id,
                     p_bu =>c_recurring_accruals_rec(idx1).bu_name,
                     p_branch =>c_recurring_accruals_rec(idx1).branch_name,
                     p_from_date =>c_recurring_accruals_rec(idx1).receipt_start_date,
                     p_to_date =>c_recurring_accruals_rec(idx1).receipt_end_date
                  )
                  ;
                fetch c_get_purchases bulk collect into c_purchases_rec;
                     print_log('01....Purchases found for customer :'||c_purchases_rec.count);                                
                close c_get_purchases;
                --
                    if c_purchases_rec.count >0 then
                     -- get the total purchases first so we can derive the purchase percent by branch further and the actual accrual amount
                        for idx2 in 1 .. c_purchases_rec.count loop
                              --
                              l_total_purchases := l_total_purchases + c_purchases_rec(idx2).purchases; 
                              --
                              begin
                                  --
                                  savepoint sqr2;
                                  --
                                  update xxcus.xxcus_ozf_recurring_accruals 
                                  set total_purchases =l_total_purchases
                                       ,branch_cust_acct_id =case when branch_name is not null then c_purchases_rec(idx2).branch_cust_acct_id else null end
                                       ,status_code ='STAGE_2'
                                       ,status_desc =g_stage2                                       
                                       ,ebs_updated_by=l_user_id
                                       ,ebs_update_date =sysdate
                                  where 1 =1
                                        and record_seq =c_recurring_accruals_rec(idx1).record_seq
                                  ;
                                  --                     
                              exception
                               when others then
                                print_log('Stage 2....error :'||c_recurring_accruals_rec(idx1).record_seq||', '||sqlerrm);                                     
                                rollback to sqr2;
                              end;
                              --
                        end loop;
                        --
                        print_log('Move to IDX3 :  c_purchases_rec.count = '|| c_purchases_rec.count);
                        -- run through each branch to get the individual recurring amount based on purchase percent
                        --
                        l_line_num :=0;
                        --
                        for idx3 in 1 .. c_purchases_rec.count loop 
                          --
                          if (c_purchases_rec(idx3).purchases =0) then
                              print_log('Line# '||idx3||', c_recurring_accruals_rec(idx3).branch_name ='||c_purchases_rec(idx3).branch
                                               ||', l_purchases =0, Skip to next record...'
                                             );            
                          else
                           -- 08/22/2018
                                         --                                 
                                          l_line_num := l_line_num + 1;
                                          --
                                          begin 
                                            --
                                              l_purchase_percent :=(c_purchases_rec(idx3).purchases/l_total_purchases);                                    
                                              --                                    
                                              l_recurr_accrual_amt :=(c_recurring_accruals_rec(idx1).amount * l_purchase_percent);                                    
                                              --
                                              print_log('Inside IDX3 - get % and recurring amount, Line# '||idx3||', c_recurring_accruals_rec(idx3).branch_name ='||c_purchases_rec(idx3).branch
                                                               ||', l_purchase_percent ='||l_purchase_percent||', l_recurr_accrual_amt ='||l_recurr_accrual_amt
                                                             );
                                               --                                  
                                          exception
                                           when others then
                                            l_purchase_percent :=0;
                                            l_recurr_accrual_amt :=0;
                                            print_log('ERROR Inside IDX3 - get % and recurring amount : '||idx3||', c_recurring_accruals_rec(idx3).branch_name ='||c_purchases_rec(idx3).branch);                                    
                                          end;
                                          --
                                          begin
                                           savepoint sqr3;
                                            insert into xxcus.xxcus_ozf_recurr_accr_utils
                                            (
                                              record_seq,  
                                              line_num,
                                              cust_account_id,
                                              plan_id,
                                              branch_cust_acct_id,
                                              branch_name,
                                              purchase_percent,
                                              recurring_accrual_amount, 
                                              status_code,
                                              status_desc,  
                                              ebs_updated_by,
                                              ebs_update_date,
                                              run_id
                                            )
                                            values
                                            (
                                              c_recurring_accruals_rec(idx1).record_seq,  
                                              l_line_num,
                                              c_recurring_accruals_rec(idx1).cust_account_id,
                                              c_recurring_accruals_rec(idx1).plan_id,
                                              c_purchases_rec(idx3).branch_cust_acct_id,
                                              c_purchases_rec(idx3).branch,
                                              l_purchase_percent, --round((c_purchases_rec(idx3).purchases/l_total_purchases) * 100, 2),
                                              l_recurr_accrual_amt, --(c_recurring_accruals_rec(idx1).amount * round((c_purchases_rec(idx3).purchases/l_total_purchases) * 100, 2))/100, 
                                              'STAGE_3',
                                              g_stage3,  
                                              l_user_id,
                                              sysdate,
                                              g_run_id                            
                                            );     
                                                --
                                                if sql%rowcount >0 then 
                                                        --
                                                        -- Copy detailed spend info  
                                                        --
                                                        begin  
                                                         --
                                                          open c_detail_spend
                                                              (
                                                                 p_cust_id =>c_recurring_accruals_rec(idx1).cust_account_id,
                                                                 p_bu =>c_recurring_accruals_rec(idx1).bu_name,
                                                                 p_branch =>c_purchases_rec(idx3).branch,
                                                                 p_from_date =>c_recurring_accruals_rec(idx1).receipt_start_date,
                                                                 p_to_date =>c_recurring_accruals_rec(idx1).receipt_end_date,
                                                                 p_rec_seq =>c_recurring_accruals_rec(idx1).record_seq,
                                                                 p_line_num =>l_line_num,
                                                                 p_total_purchase =>c_purchases_rec(idx3).purchases,
                                                                 p_total_recurr_amount =>l_recurr_accrual_amt, 
                                                                 p_status_code =>'STAGE_3',
                                                                 p_status_desc =>g_stage3,
                                                                 p_user_id =>l_user_id,
                                                                 p_plan_id =>c_recurring_accruals_rec(idx1).plan_id                                                                                                        
                                                              );                    
                                                         fetch c_detail_spend bulk collect into c_SPEND_rec;
                                                         close c_detail_spend;
                                                         --
                                                          print_log('c_SPEND_rec.count  =>'||c_SPEND_rec.count);
                                                          --                                             
                                                             if c_SPEND_rec.count >0 then 
                                                              --
                                                               forall idx10 in 1 .. c_SPEND_rec.count 
                                                               insert into xxcus.xxcus_ozf_recurr_accr_purdtls values c_SPEND_rec(idx10);
                                                              --
                                                             end if;
                                                             --
                                                       exception
                                                        when others then
                                                         print_log('Issue in copying detailed spend info : '||sqlerrm);
                                                       end;
                                                        --
                                                      begin
                                                          --
                                                          savepoint sqr4;
                                                          --
                                                          update xxcus.xxcus_ozf_recurring_accruals 
                                                          set status_desc =g_stage3
                                                               ,status_code ='STAGE_3'
                                                               ,ebs_updated_by=l_user_id
                                                               ,ebs_update_date =sysdate                                                  
                                                          where 1 =1
                                                                and record_seq =c_recurring_accruals_rec(idx1).record_seq
                                                          ;
                                                          --                     
                                                      exception
                                                       when others then
                                                        print_log('Stage 3 status code update failed....error :'||c_recurring_accruals_rec(idx1).record_seq||', '||sqlerrm);                                     
                                                        rollback to sqr4;
                                                      end;
                                                      --
                                                end if;
                                                --                              
                                          exception
                                           when others then
                                            rollback to sqr3;
                                            print_log('Stage 3 status code update failed...record seq '||c_recurring_accruals_rec(idx1).record_seq);                                                                      
                                            print_log('Stage 3 status code update failed...error ='||sqlerrm);
                                          end;
                                      --                           
                           -- 08/22/2018
                          end if;
                          --
                        end loop;
                         --
                    else
                     print_log('No purchase found for customer :'||c_recurring_accruals_rec(idx1).cust_account_id);
                              --
                              begin
                                  --
                                  savepoint sqr2;
                                  --
                                  update xxcus.xxcus_ozf_recurring_accruals 
                                  set status_code ='STAGE_2'
                                       ,status_desc ='No purchases for customer.'                                       
                                       ,ebs_updated_by=l_user_id
                                       ,ebs_update_date =sysdate
                                  where 1 =1
                                        and record_seq =c_recurring_accruals_rec(idx1).record_seq
                                  ;
                                  --                     
                              exception
                               when others then
                                print_log('Stage 2....error :'||c_recurring_accruals_rec(idx1).record_seq||', '||sqlerrm);                                     
                                rollback to sqr2;
                              end;
                              --                     
                    end if;
                --
                --
               exception
                when no_data_found then
                 print_log('Purchases not found for the recurring accrual record seq :'||c_recurring_accruals_rec(idx1).record_seq);
                when others then
                 print_log('When other errrors in extract_recurring_accrulals, record seq '||c_recurring_accruals_rec(idx1).record_seq);
                 raise program_error;           
               end;
               --
          end loop;      
          --
     end if;
    --
    --
   exception
    when no_data_found then
     print_log('No data found in extract_recurring_accrulals');
     raise program_error;
    when others then
     print_log('When other errrors in extract_recurring_accrulals');
     raise program_error;
   end;
   --
   print_log('End :  extract_recurring_accruals');
   --   
 exception
  when others then  
   print_log('Issue in xxcus_ozf_recurring_accr_pkg.extract_recurring_accruals routine, msg ='||sqlerrm);
   raise program_error;
 end extract_recurring_accruals;
  --
 procedure process_adj (p_rec_seq in number) is
 --
 l_adj_rec OZF_FUND_UTILIZED_PUB.adjustment_rec_type; --adj_rec_type; 
  --
  cursor c_accruals_master_set is  
  select 
             mvid
            ,mvid_name
            ,offer_name
            ,currency
            ,reason_name
            --,round(amount, 2) amount            
            ,amount 
            ,lob_name
            ,bu_name
            ,branch_name
            ,receipt_start_date
            ,receipt_end_date
            ,validated_flag
            ,status_code
            ,cust_account_id
            ,plan_id
            ,reason_code
            ,apex_created_by
            ,apex_creation_date
            ,ebs_updated_by
            ,ebs_update_date
            ,record_seq
            ,total_purchases
            ,lob_party_id
            ,bu_party_id
            ,branch_cust_acct_id
            ,fund_id
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and  run_id =g_run_id  
      and  validated_flag =g_validated_Yes
      and  status_code ='STAGE_3'
      and  record_seq =p_rec_seq                  
      and p_rec_seq is not null
      and total_purchases >0
  union all
  select 
             mvid
            ,mvid_name
            ,offer_name
            ,currency
            ,reason_name
            --,round(amount, 2) amount
            ,amount            
            ,lob_name
            ,bu_name
            ,branch_name
            ,receipt_start_date
            ,receipt_end_date
            ,validated_flag
            ,status_code
            ,cust_account_id
            ,plan_id
            ,reason_code
            ,apex_created_by
            ,apex_creation_date
            ,ebs_updated_by
            ,ebs_update_date
            ,record_seq
            ,total_purchases
            ,lob_party_id
            ,bu_party_id
            ,branch_cust_acct_id
            ,fund_id
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and  run_id =g_run_id
      and  validated_flag =g_validated_Yes  
      and  status_code ='STAGE_3'        
      and p_rec_seq is null       
      and total_purchases >0                 
  ; 
  --
  type c_recurring_accruals_mst  is table of c_accruals_master_set%rowtype index by binary_integer;
  c_accruals_header   c_recurring_accruals_mst;
  --   
  cursor c_accrual_lines_set  (p_rec_seq in number) is
    select 
             line_num
            ,cust_account_id
            ,plan_id
            ,branch_name
            ,purchase_percent
            ,round(recurring_accrual_amount, 2) recurring_amount --rounding was changed to 2 from 4 on 03/12/2018
            ,status_code
            ,status_desc
            ,branch_cust_acct_id    
    from xxcus.xxcus_ozf_recurr_accr_utils 
    where 1 =1
         and run_id =g_run_id  
         and status_code ='STAGE_3'
         and record_seq =p_rec_seq
         --and line_num =2
    ;     
  --
  type c_accrual_lines_type is table of c_accrual_lines_set%rowtype index by binary_integer;
  c_accrual_lines   c_accrual_lines_type;
  --   
  cursor c_purdtls (p_record_seq in number, p_line_num in number) is
    select 
             record_seq
            ,run_id
            ,line_num
            ,cust_account_id
            ,plan_id
            ,branch_name
            ,branch_cust_acct_id
            ,line_receipt_amount
            ,object_type
            ,object_id
            ,branch_total_purchase
            ,total_recurring_amount
            ,line_recurring_amount
            ,status_code
            ,status_desc
            ,utilization_id
            ,ebs_updated_by
            ,ebs_update_date
            ,api_status
            ,api_message
            ,detail_seq
            ,run_mode    
    from xxcus.xxcus_ozf_recurr_accr_purdtls
    where 1 =1
         and run_id =g_run_id
         and record_seq =p_record_seq
         and line_num =p_line_num
    ;  
  -- 
  type c_purdtls_type is table of  xxcus.xxcus_ozf_recurr_accr_purdtls%rowtype index by binary_integer;
  c_purdtls_rec c_purdtls_type;
  --
  cursor c_total_utils (p_record_seq in number) is
  select count(ofu.utilization_id) total_utils, sum(ofu.acctd_amount) util_amt
  from  ozf_funds_utilized_all_b ofu
  where 1 =1 
        and exists 
         (
            select 1
            from   xxcus.xxcus_ozf_utils_stg
            where 1 =1
                  and record_seq =p_record_seq
                  and run_id =g_run_id
                  and utilization_id =ofu.utilization_id
         );
  --
  cursor get_max_purchase_branch (p_run_id in number, p_record_seq in number) is
    select line_num
    from xxcus.xxcus_ozf_recurr_accr_utils  a, xxcus.xxcus_ozf_recurring_accruals b
    where 1 =1
    and b.run_id =p_run_id
    and b.record_seq =p_record_seq
    and a.run_id =b.run_id
    and a.record_seq =b.record_seq
    and round(purchase_percent, 2) =
     (
        select max(round(purchase_percent, 2))
        from xxcus.xxcus_ozf_recurr_accr_utils  a, xxcus.xxcus_ozf_recurring_accruals b
        where 1 =1
        and b.run_id =p_run_id
        and b.record_seq =p_record_seq
        and a.run_id =b.run_id
        and a.record_seq =b.record_seq     
     );  
  --
  l_move_fwd boolean :=Null; 
  l_line_num number :=0;
  l_user_id number :=fnd_global.user_id;
  --
  l_total_purchases number :=0;
  l_purchase_percent number :=0;
  l_recurr_accrual_amt number :=0;
  --
  l_adjustment_type varchar2(30) :=Null;
  --
  l_error_rec xxcus.xxcus_ozf_recurring_accr_err%rowtype :=Null;  
  --
  v_message          VARCHAR2(250);
  l_message          VARCHAR2(150);
  l_err_msg          VARCHAR2(3000);
  l_err_code         NUMBER;
  l_sec              VARCHAR2(500);
  l_req_id           NUMBER := FND_GLOBAL.CONC_REQUEST_ID;

   l_return_status             VARCHAR2(2000) :=Null;
   l_msg_count                 NUMBER :=Null;
   l_msg_data                 VARCHAR2(2000) :=Null;
   --  
   l_idx number :=0; 
   l_api_failed number :=0;
   l_api_success number :=0;
   l_utilization_id number :=0;
   l_max_branch_cust_acct_id number :=0;
   --
   l_total_utils number :=0;
   l_util_amt number :=0;
   l_mx_pur_br_line_num number :=0;
   l_idx99 number :=0;
   l_idx89 number :=0;
   l_util_id_count number :=0;
  --
 begin 
   --
   print_log('Begin :  process_adj (p_rec_seq =>'||p_rec_seq||');');
   --
   begin 
    --
     open c_accruals_master_set;
     fetch c_accruals_master_set bulk collect into c_accruals_header;
     close c_accruals_master_set;
    --
     if c_accruals_header.count >0 then
        --
        print_log ('c_accruals_header.count ='||c_accruals_header.count);                                                                
        --          
          for idx1 in 1 .. c_accruals_header.count loop
               --
               savepoint start_here;
               --       
               g_branch_lines_failed :=0; 
               g_branch_lines_success :=0;
               --
               begin
                --
                print_log ('c_accruals_header(idx1).record_seq ='||c_accruals_header(idx1).record_seq);                                                                
                --
                open c_accrual_lines_set (p_rec_seq =>c_accruals_header(idx1).record_seq);
                fetch c_accrual_lines_set bulk collect into c_accrual_lines;
                print_log('01....Found '||c_accrual_lines.count||' accrual utilization lines for record_seq :'||c_accruals_header(idx1).record_seq);                                
                close c_accrual_lines_set;
                --
                    if c_accrual_lines.count >0 then
                        --
                        g_adj_rec :=Null;
                        --
                        g_branch_lines_success :=0;
                        g_branch_lines_failed :=0;                         
                        --
                        begin
                         open get_max_purchase_branch (g_run_id, c_accruals_header(idx1).record_seq);
                         fetch get_max_purchase_branch into l_mx_pur_br_line_num;
                         close get_max_purchase_branch;
                        exception
                         when no_data_found then
                          l_mx_pur_br_line_num :=0;
                         when others then
                          l_mx_pur_br_line_num :=0;
                        end;
                        --
                        l_adj_rec :=Null;
                        --   
                        for idx2 in 1 .. c_accrual_lines.count loop 
                              --                             
                              print_log('02....xxcus.xxcus_ozf_recurr_accr_utils.line_num :'||c_accrual_lines(idx2).line_num);                              
                              --
                              begin 
                               open c_purdtls (p_record_seq =>c_accruals_header(idx1).record_seq, p_line_num =>c_accrual_lines(idx2).line_num);
                               fetch c_purdtls bulk collect into c_purdtls_rec;
                               close c_purdtls;
                              exception
                               when others then
                                print_log('Issue in fetch of purchase details for record_seq and  line_num '||sqlerrm);
                              end;
                              --
                              l_idx99 :=l_idx99+1;
                              l_adj_rec :=Null;
                              --    
                             --
                              begin -- Begin assign variables 
                                     l_adj_rec.adjustment_type := 'STANDARD';                                                           
                                     l_adj_rec.fund_id :=c_accruals_header(idx1).fund_id;                                     
                                     l_adj_rec.currency_code :=c_accruals_header(idx1).currency;
                                     l_adj_rec.adjustment_date :=g_adj_date;
                                     l_adj_rec.gl_date:=g_adj_date;                   
                                     l_adj_rec.activity_id :=c_accrual_lines(idx2).plan_id;
                                     l_adj_rec.activity_type  :='OFFR';
                                     l_adj_rec.cust_account_id :=c_accrual_lines(idx2).cust_account_id;
                                     l_adj_rec.attribute1 :='LOB : '||c_accruals_header(idx1).lob_name;
                                     l_adj_rec.attribute2 :='BU : '||c_accruals_header(idx1).bu_name;                                                                          
                                     l_adj_rec.attribute3 :='Branch : '||c_accrual_lines(idx2).branch_name;                                     
                                     l_adj_rec.attribute4 :='Rcpt Start Date : '||to_char(c_accruals_header(idx1).receipt_start_date, 'MM/DD/YYYY');
                                     l_adj_rec.attribute5 :='Rcpt End Date : '||to_char(c_accruals_header(idx1).receipt_end_date, 'MM/DD/YYYY');
                                     l_adj_rec.attribute6 :='RUN ID : '||to_char(g_run_id);
                                     l_adj_rec.attribute10 :=c_accruals_header(idx1).reason_code;                                     
                                     l_adj_rec.attribute11 :='Reason Name : '||c_accruals_header(idx1).reason_name;
                                     l_adj_rec.attribute14 :='Branch ID :'||c_accrual_lines(idx2).branch_cust_acct_id;                                     
                                     --l_adj_rec.attribute15 :=c_accrual_lines(idx2).purchase_percent;                                     
                                     --                                  
                              exception
                               when others then
                                print_log ('Error in assigning adjusment collection table variables, msg ='||sqlerrm);                                                          
                              end; -- End assign variables
                             --                                                                                              
                              --
                              l_api_failed :=0; -- Reset this for every line number  of the record seq once
                              l_util_id_count :=0;
                              g_ozf_utils_stg_rec.delete; -- clean up collections for each branch of the record seq
                              g_stg_idx :=0;
                              --
                              if c_purdtls_rec.count >0 then 
                                     --
                                     -- get recurring line amount for each branch level stock receipt
                                     --
                                     for idx90 in 1 .. c_purdtls_rec.count loop
                                          -- idx90 08/13/2018
                                         if l_mx_pur_br_line_num = c_accrual_lines(idx2).line_num then --assign only for branch with max purchase percent
                                            --
                                            g_adj_rec :=l_adj_rec;
                                            --
                                            l_max_branch_cust_acct_id :=c_accrual_lines(idx2).branch_cust_acct_id;
                                            --
                                            g_adj_rec.attribute10 :=g_recurr_accrual_adj; 
                                            --                                    
                                            g_adj_rec.attribute11 :='Reason Name : '||g_recurr_accrual_desc;                                        
                                            --
                                              g_99999999_receipt_amount :=c_purdtls_rec(idx90).line_receipt_amount;
                                              g_99999999_br_total_receipt :=c_purdtls_rec(idx90).branch_total_purchase;
                                              g_99999999_total_recurr_amount :=c_purdtls_rec(idx90).total_recurring_amount; 
                                              g_99999999_fund_id :=c_accruals_header(idx1).fund_id;                                        
                                            --
                                         end if;
                                         --                                       
                                         --
                                          begin -- Begin assign variables 
                                                   print_log 
                                                   ('Before calling Adjustment API, Detail Seq ='||g_run_id||'.'||c_accruals_header(idx1).record_seq||'.'||c_accrual_lines(idx2).line_num||'.'||c_purdtls_rec(idx90).detail_seq||                                               
                                                  ', Line Receipt Amount ='||c_purdtls_rec(idx90).line_receipt_amount||
                                                  ', Branch Total Purchase ='||c_purdtls_rec(idx90).branch_total_purchase||
                                                  ', Branch Recurring Amount Allocated ='||c_purdtls_rec(idx90).total_recurring_amount||
                                                  ', Line Recurring Amount ='||c_purdtls_rec(idx90).line_recurring_amount                                                                                                                    
                                                   );
                                                 l_adj_rec.document_number :=c_purdtls_rec(idx90).object_id;
                                                 l_adj_rec.document_type :=c_purdtls_rec(idx90).object_type;                                                                                                    
                                                 l_adj_rec.amount :=c_purdtls_rec(idx90).line_recurring_amount;                                     
                                                 l_adj_rec.attribute13 :='Detail Seq# =>'||g_run_id||'.'||c_accruals_header(idx1).record_seq||'.'||c_accrual_lines(idx2).line_num||'.'||c_purdtls_rec(idx90).detail_seq;                                     
                                                 --
                                          exception
                                           when others then
                                            print_log ('Error in assigning adjusment collection table variables, msg ='||sqlerrm);                                                          
                                          end; -- End assign variables
                                         --           
                                         -- 08/14/2018
                                              --
                                              begin
                                                  --
                                                  fnd_msg_pub.Delete_Msg; -- Clear message stack
                                                  --
                                                      ozf_fund_utilized_pub.create_fund_adjustment
                                                              (
                                                                  p_api_version      => 1.0,
                                                                  p_init_msg_list    => fnd_api.g_true,
                                                                  p_commit           => fnd_api.g_false,
                                                                  p_validation_level => fnd_api.g_valid_level_full,
                                                                  p_adj_rec          => l_adj_rec,
                                                                  x_return_status    => l_return_status,
                                                                  x_msg_count        => l_msg_count,
                                                                  x_msg_data         => l_msg_data,
                                                                  x_utilization_id       =>l_utilization_id
                                                              );
                                                  --
                                                  if l_utilization_id >0 then
                                                        --
                                                        g_stg_idx :=g_stg_idx +1;
                                                        --
                                                        begin
                                                            g_ozf_utils_stg_rec(g_stg_idx).record_seq :=c_accruals_header(idx1).record_seq;
                                                            g_ozf_utils_stg_rec(g_stg_idx).run_id :=g_run_id;
                                                            g_ozf_utils_stg_rec(g_stg_idx).utilization_id :=l_utilization_id;
                                                            g_ozf_utils_stg_rec(g_stg_idx).request_id :=l_req_id;
                                                            g_ozf_utils_stg_rec(g_stg_idx).run_mode :=g_mode;     
                                                            g_ozf_utils_stg_rec(g_stg_idx).ebs_updated_by :=l_user_id;                                   
                                                            g_ozf_utils_stg_rec(g_stg_idx).line_num :=c_accrual_lines(idx2).line_num;
                                                            g_ozf_utils_stg_rec(g_stg_idx).object_id :=c_purdtls_rec(idx90).object_id;                                                        
                                                            g_ozf_utils_stg_rec(g_stg_idx).detail_seq :=c_purdtls_rec(idx90).detail_seq;                                                           
                                                        exception
                                                         when others then
                                                          print_log (
                                                                                'Record Seq# '||g_ozf_utils_stg_rec(g_stg_idx).record_seq||
                                                                                ', Line# '||g_ozf_utils_stg_rec(g_stg_idx).line_num||
                                                                                ', Detail Seq# '||g_ozf_utils_stg_rec(g_stg_idx).detail_seq||
                                                                                ', object_id '||g_ozf_utils_stg_rec(g_stg_idx).object_id||
                                                                               ', error : '||sqlerrm
                                                                             );
                                                        end;                                                     
                                                        --
                                                        l_util_id_count :=l_util_id_count+1;
                                                        --
                                                  end if;
                                                  --
                                                  if l_return_status =fnd_api.g_ret_sts_success then
                                                     --
                                                     print_log('Recurring accrual created for Detail Seq# '||c_purdtls_rec(idx90).detail_seq||', Utilization_id ='||l_utilization_id);
                                                     g_dtl_seq_success :=g_dtl_seq_success+1;
                                                     --                                         
                                                  else
                                                     --
                                                     print_log('Recurring accrual failed for Detail Seq# '||c_accrual_lines(idx2).line_num);
                                                     g_dtl_seq_failed :=g_dtl_seq_failed+1;    
                                                     --
                                                     -- 08/14/2018
                                                     --                              
                                                          begin
                                                              --
                                                              if (l_msg_count >0 and l_return_status <>fnd_api.g_ret_sts_success)   then
                                                                   fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false,
                                                                                      p_count   => l_msg_count,
                                                                                      p_data    => l_msg_data);
                                                                    for err_idx in 1 .. l_msg_count loop 
                                                                        --
                                                                        l_error_rec :=Null;
                                                                        --
                                                                        l_error_rec.record_seq :=c_accruals_header(idx1).record_seq;
                                                                        l_error_rec.line_num :=c_accrual_lines(idx2).line_num;
                                                                        l_error_rec.err_seq :=err_idx;
                                                                        l_error_rec.err_message :=substr(fnd_msg_pub.get(p_msg_index => err_idx, p_encoded => 'F'), 1, 254);
                                                                        l_error_rec.run_id :=g_run_id;
                                                                        l_error_rec.created_by :=l_user_id;
                                                                        l_error_rec.detail_seq :=  c_purdtls_rec(idx90).detail_seq;
                                                                        --
                                                                        log_error(l_error_rec);
                                                                        --
                                                                    end loop;
                                                                      --                                   
                                                              end if;
                                                              --
                                                          exception
                                                           when others then
                                                            print_log('@process_adj, stage4 - log error. record_seq : '||c_accruals_header(idx1).record_seq
                                                                               ||', line_num : '||c_accrual_lines(idx2).line_num
                                                                               ||', detail_seq : '||c_purdtls_rec(idx90).detail_seq                                                                      
                                                                               ||sqlerrm
                                                                             );
                                                          end;
                                                          --                                                       
                                                     -- 08/14/2018                                     
                                                  end if;
                                                      --
                                                      -- Log all activities into the table so we can print in the BI Publisher report.
                                                      --
                                                      begin
                                                            --
                                                            g_log_idx :=g_log_idx+1;
                                                            --      
                                                            -- new one
                                                                --
                                                                g_recurr_accrual_log_rec(g_log_idx).request_id :=l_req_id;
                                                                g_recurr_accrual_log_rec(g_log_idx).line_type :=g_line_type_standard;                                        
                                                                g_recurr_accrual_log_rec(g_log_idx).record_seq :=c_accruals_header(idx1).record_seq;
                                                                g_recurr_accrual_log_rec(g_log_idx).line_num :=c_accrual_lines(idx2).line_num;
                                                                g_recurr_accrual_log_rec(g_log_idx).detail_seq :=c_purdtls_rec(idx90).detail_seq;                                        
                                                                g_recurr_accrual_log_rec(g_log_idx).resale_line_id :=c_purdtls_rec(idx90).object_id;  
                                                                g_recurr_accrual_log_rec(g_log_idx).utilization_id :=l_utilization_id;  
                                                                --                                                                            
                                                                g_recurr_accrual_log_rec(g_log_idx).mvid :=c_accruals_header(idx1).mvid;
                                                                g_recurr_accrual_log_rec(g_log_idx).mvid_name :=c_accruals_header(idx1).mvid_name;
                                                                g_recurr_accrual_log_rec(g_log_idx).offer_name :=c_accruals_header(idx1).offer_name;
                                                                g_recurr_accrual_log_rec(g_log_idx).currency :=c_accruals_header(idx1).currency;
                                                                g_recurr_accrual_log_rec(g_log_idx).receipt_start_date :=c_accruals_header(idx1).receipt_start_date;
                                                                g_recurr_accrual_log_rec(g_log_idx).receipt_end_date :=c_accruals_header(idx1).receipt_end_date;
                                                                --
                                                                g_recurr_accrual_log_rec(g_log_idx).reason_name :=g_recurr_accrual_desc;
                                                                g_recurr_accrual_log_rec(g_log_idx).lob_name :=c_accruals_header(idx1).lob_name;
                                                                g_recurr_accrual_log_rec(g_log_idx).bu_name :=c_accruals_header(idx1).bu_name;
                                                                g_recurr_accrual_log_rec(g_log_idx).branch_name :=substr(g_adj_rec.attribute3, 10);
                                                                g_recurr_accrual_log_rec(g_log_idx).receipt_amount :=c_purdtls_rec(idx90).line_receipt_amount;
                                                                g_recurr_accrual_log_rec(g_log_idx).branch_total_receipt :=c_purdtls_rec(idx90).branch_total_purchase;
                                                                g_recurr_accrual_log_rec(g_log_idx).applied_recurring_amount :=c_purdtls_rec(idx90).line_recurring_amount;             
                                                                g_recurr_accrual_log_rec(g_log_idx).total_recurring_amount :=c_purdtls_rec(idx90).total_recurring_amount;                                              
                                                                --
                                                                g_recurr_accrual_log_rec(g_log_idx).cust_account_id :=c_accruals_header(idx1).cust_account_id;
                                                                g_recurr_accrual_log_rec(g_log_idx).plan_id :=c_accruals_header(idx1).plan_id;
                                                                g_recurr_accrual_log_rec(g_log_idx).lob_party_id :=c_accruals_header(idx1).lob_party_id;
                                                                g_recurr_accrual_log_rec(g_log_idx).bu_party_id :=c_accruals_header(idx1).bu_party_id;
                                                                g_recurr_accrual_log_rec(g_log_idx).branch_cust_acct_id :=l_max_branch_cust_acct_id;
                                                                --
                                                                g_recurr_accrual_log_rec(g_log_idx).run_id :=g_run_id;
                                                                g_recurr_accrual_log_rec(g_log_idx).fund_id :=c_accruals_header(idx1).fund_id;
                                                                g_recurr_accrual_log_rec(g_log_idx).ebs_updated_by :=l_user_id;
                                                                g_recurr_accrual_log_rec(g_log_idx).ebs_update_date :=sysdate;
                                                                g_recurr_accrual_log_rec(g_log_idx).run_mode :=g_draft;                
                                                                --
                                                                g_recurr_accrual_log_rec(g_log_idx).status_code :='STAGE_4';
                                                                g_recurr_accrual_log_rec(g_log_idx).status_desc :=case when l_return_status =fnd_api.g_ret_sts_success  then g_stage40 else g_stage41 end;                                                            
                                                            -- new one                                
                                                       --                                   
                                                  exception
                                                   when others then
                                                     print_log('Error in assigning log record to plsql table, detail_seq :'||c_purdtls_rec(idx90).detail_seq||', message ='||sqlerrm);
                                                  end;                     
                                              exception
                                               when others then            
                                                g_dtl_seq_failed := g_dtl_seq_failed+1;
                                                print_log('@process_adj, g_dtl_seq_failed variable updated, stage 4 -api call....error detail seq  :'||c_purdtls_rec(idx90).detail_seq||', msg =>'||sqlerrm);
                                              end;
                                              --                                         
                                         -- 08/14/2018                           
                                      -- idx90 08/13/2018
                                     end loop; -- for idx90 in 1 .. c_purdtls_rec.count loop 
                                     -- 08/14/2018
                                       -- Insert the utilizations created so far    
                                       --
                                           begin
                                                if g_ozf_utils_stg_rec.count >0 then        
                                                    print_log(
                                                                        'Begin Utilization_Log : @ Record Seq# '||c_accruals_header(idx1).record_seq||
                                                                        ', Line  Num ='||c_accrual_lines(idx2).line_num||
                                                                        ', OFU Utilization ID Count ='||g_ozf_utils_stg_rec.count
                                                                     );
                                                       Utilization_Log ( p_ozf_utils_stg_rec =>g_ozf_utils_stg_rec);
                                                    print_log('End Utilization_Log.');                                 
                                                end if;
                                           exception
                                             when others then
                                              print_log('Error in updating line utilization status, : @ Record Seq# '||c_accruals_header(idx1).record_seq||', Line Num '||c_accrual_lines(idx2).line_num||', message ='||sqlerrm);
                                           end;        
                                       --
                                     -- 08/14/2018
                              end if; --  if c_purdtls_rec.count >0 then 
                              --
                              if (g_dtl_seq_failed =0 and g_dtl_seq_success >0) then
                               g_branch_lines_success :=g_branch_lines_success +1;                         
                              else
                               g_branch_lines_failed :=g_branch_lines_failed +1;                              
                              end if;
                              --
                              print_log(' ');                              
                              print_log('g_branch_lines_success =>'||g_branch_lines_success||',g_branch_lines_failed =>'||g_branch_lines_failed);
                              print_log(' ');
                              --
                              begin 
                                  -- gather line utils table record status for each record seq and line number
                                          g_line_rec(l_idx99).record_seq :=c_accruals_header(idx1).record_seq;
                                          g_line_rec(l_idx99).line_num :=c_accrual_lines(idx2).line_num;                                  
                                          g_line_rec(l_idx99).status_code :='STAGE_4';
                                          g_line_rec(l_idx99).ebs_updated_by :=l_user_id;
                                          g_line_rec(l_idx99).ebs_update_date :=sysdate;
                                          g_line_rec(l_idx99).status_desc :=
                                                    case 
                                                      when (g_branch_lines_success >0 and g_branch_lines_failed =0) then g_stage40a                                                      
                                                      else g_stage40b 
                                                    end;    
                                  --               
                              exception
                               when others then
                                print_log('@process_adj, stage4 collect utilizations -api_status_catch....error :'
                                                      ||c_accruals_header(idx1).record_seq
                                                      ||', Line# '
                                                      ||c_accrual_lines(idx2).line_num
                                                      ||',  '
                                                      ||sqlerrm
                                                 );
                              end;
                              --                            
                        end loop; -- for idx2 in 1 .. c_accrual_lines.count loop 
                        --
                    else
                     print_log('No accrual utilizations found for record seq  :'||c_accruals_header(idx1).record_seq);
                    end if;
                --
                --
               exception
                when no_data_found then
                 print_log('Purchases not found for the recurring accrual record seq :'||c_accruals_header(idx1).record_seq);
                when others then
                 print_log('When other errrors in extract_recurring_accrulals, record seq '||c_accruals_header(idx1).record_seq);
                 raise program_error;           
               end;
               -- 
--               if (g_branch_lines_success >0 and g_branch_lines_failed =0) then
--                    l_api_success := l_api_success +1;
--               else
--                    l_api_failed := l_api_failed +1;               
--               end if;
               --
               -- gather header table record status for each record seq
               --
              --
              Null;
              --
              begin
                  --
                          g_hdr_rec(idx1).record_seq :=c_accruals_header(idx1).record_seq;
                          g_hdr_rec(idx1).status_code :='STAGE_4';
                          g_hdr_rec(idx1).status_desc :='Status for all Branches, Success : '||g_branch_lines_success||', Failed : '||g_branch_lines_failed;
                          g_hdr_rec(idx1).ebs_updated_by :=l_user_id;                  
              exception
               when others then
                print_log('@process_adj, stage4 -collect header status....error :'||c_accruals_header(idx1).record_seq||', '||sqlerrm);
              end;
              --
             if (g_branch_lines_failed >0) then --( l_api_failed >0 ) then
                print_log('Decision point : g_branch_lines_failed =>'||g_branch_lines_failed||',G_mode :'||g_mode);    
                rollback to start_here; -- go back to square1
             else
                  -- we have to look at the variance and see if we need to add an adjustment or not. If an adjustment is required then it has to be assigned to the
                  -- branch with the highest purchase                 
                 --
                 begin
                   open c_total_utils (c_accruals_header(idx1).record_seq);
                   fetch c_total_utils into l_total_utils, l_util_amt;
                   close c_total_utils;
                   --
                        print_log('Record_seq ='||c_accruals_header(idx1).record_seq||', l_total_utils ='||l_total_utils||', l_util_amt ='||l_util_amt);
                   --                   
                   if l_total_utils >0 then
                        --
                        if ( abs(c_accruals_header(idx1).amount) <> abs(l_util_amt)) then
                              --
                              if (sign(c_accruals_header(idx1).amount) =1) then
                                       g_adj_rec.amount :=c_accruals_header(idx1).amount - l_util_amt;                          
                              else
                                       g_adj_rec.amount :=(-1) *(abs(c_accruals_header(idx1).amount) - abs(l_util_amt));                          
                              end if;
                              --
                                       print_log('@ variance : g_adj_rec.amount ='||g_adj_rec.amount);
                              --                              
                          -- ^^^^
                              begin
                                  --
                                  fnd_msg_pub.Delete_Msg; -- Clear message stack
                                  --
                                      ozf_fund_utilized_pub.create_fund_adjustment
                                              (
                                                  p_api_version      => 1.0,
                                                  p_init_msg_list    => fnd_api.g_true,
                                                  p_commit           => fnd_api.g_false,
                                                  p_validation_level => fnd_api.g_valid_level_full,
                                                  p_adj_rec          => g_adj_rec,
                                                  x_return_status    => l_return_status,
                                                  x_msg_count        => l_msg_count,
                                                  x_msg_data         => l_msg_data,
                                                  x_utilization_id       =>l_utilization_id
                                              );
                                  --
                                  print_log('@ variance g_adj_rec with l_return_status ='||l_return_status); 
                                  --
                                         begin
                                                 --
                                                 g_misc_idx :=g_misc_idx +1;
                                                 --
                                                 g_ozf_misc_utils_rec(g_misc_idx).record_seq  :=c_accruals_header(idx1).record_seq;                                                                           
                                                 g_ozf_misc_utils_rec(g_misc_idx).line_num :=l_mx_pur_br_line_num;
                                                 g_ozf_misc_utils_rec(g_misc_idx).cust_account_id :=g_adj_rec.cust_account_id;
                                                 g_ozf_misc_utils_rec(g_misc_idx).plan_id :=g_adj_rec.activity_id;
                                                 g_ozf_misc_utils_rec(g_misc_idx).branch_name :=g_adj_rec.attribute3;
                                                 g_ozf_misc_utils_rec(g_misc_idx).purchase_percent :=1.00;
                                                 g_ozf_misc_utils_rec(g_misc_idx).recurring_accrual_amount :=g_adj_rec.amount;
                                                 g_ozf_misc_utils_rec(g_misc_idx).status_code :='STAGE_4';
                                                 g_ozf_misc_utils_rec(g_misc_idx).status_desc :=g_stage40;
                                                 g_ozf_misc_utils_rec(g_misc_idx).utilization_id :=l_utilization_id;
                                                 g_ozf_misc_utils_rec(g_misc_idx).ebs_updated_by :=l_user_id;
                                                 g_ozf_misc_utils_rec(g_misc_idx).ebs_update_date :=sysdate;
                                                 g_ozf_misc_utils_rec(g_misc_idx).branch_cust_acct_id :=Null;
                                                 g_ozf_misc_utils_rec(g_misc_idx).run_id :=g_run_id;
                                                 g_ozf_misc_utils_rec(g_misc_idx).api_status :=l_return_status;    
                                                 g_ozf_misc_utils_rec(g_misc_idx).request_id :=l_req_id;
                                                 g_ozf_misc_utils_rec(g_misc_idx).run_mode :=g_mode; 
                                                 --                                         
                                         exception
                                          when others then
                                           print_log('Error in assigning misc utils plsql table collection, message ='||sqlerrm);
                                         end;                                    
                                      --
                                  -- Log all activities into the table so we can print in the BI Publisher report.
                                  --
                                  begin
                                        --  
                                        g_log_idx :=g_log_idx+1;
                                        --
                                        g_recurr_accrual_log_rec(g_log_idx).request_id :=l_req_id;
                                        g_recurr_accrual_log_rec(g_log_idx).line_type :=g_line_type_variance;                                        
                                        g_recurr_accrual_log_rec(g_log_idx).record_seq :=c_accruals_header(idx1).record_seq;
                                        g_recurr_accrual_log_rec(g_log_idx).line_num :=g_max_line_num;
                                        g_recurr_accrual_log_rec(g_log_idx).detail_seq :=g_99999999_max_detail_seq;                                        
                                        g_recurr_accrual_log_rec(g_log_idx).resale_line_id :=g_adj_rec.document_number;  
                                        g_recurr_accrual_log_rec(g_log_idx).utilization_id :=l_utilization_id;  
                                        --                                                                            
                                        g_recurr_accrual_log_rec(g_log_idx).mvid :=c_accruals_header(idx1).mvid;
                                        g_recurr_accrual_log_rec(g_log_idx).mvid_name :=c_accruals_header(idx1).mvid_name;
                                        g_recurr_accrual_log_rec(g_log_idx).offer_name :=c_accruals_header(idx1).offer_name;
                                        g_recurr_accrual_log_rec(g_log_idx).currency :=c_accruals_header(idx1).currency;
                                        g_recurr_accrual_log_rec(g_log_idx).receipt_start_date :=c_accruals_header(idx1).receipt_start_date;
                                        g_recurr_accrual_log_rec(g_log_idx).receipt_end_date :=c_accruals_header(idx1).receipt_end_date;
                                        --
                                        g_recurr_accrual_log_rec(g_log_idx).reason_name :=g_recurr_accrual_desc;
                                        g_recurr_accrual_log_rec(g_log_idx).lob_name :=c_accruals_header(idx1).lob_name;
                                        g_recurr_accrual_log_rec(g_log_idx).bu_name :=c_accruals_header(idx1).bu_name;
                                        g_recurr_accrual_log_rec(g_log_idx).branch_name :=substr(g_adj_rec.attribute3, 10);
                                        g_recurr_accrual_log_rec(g_log_idx).receipt_amount :=g_99999999_receipt_amount;
                                        g_recurr_accrual_log_rec(g_log_idx).branch_total_receipt :=g_99999999_br_total_receipt;
                                        g_recurr_accrual_log_rec(g_log_idx).applied_recurring_amount :=g_adj_rec.amount;             
                                        g_recurr_accrual_log_rec(g_log_idx).total_recurring_amount :=g_99999999_total_recurr_amount;                                              
                                        --
                                        g_recurr_accrual_log_rec(g_log_idx).cust_account_id :=c_accruals_header(idx1).cust_account_id;
                                        g_recurr_accrual_log_rec(g_log_idx).plan_id :=c_accruals_header(idx1).plan_id;
                                        g_recurr_accrual_log_rec(g_log_idx).lob_party_id :=c_accruals_header(idx1).lob_party_id;
                                        g_recurr_accrual_log_rec(g_log_idx).bu_party_id :=c_accruals_header(idx1).bu_party_id;
                                        g_recurr_accrual_log_rec(g_log_idx).branch_cust_acct_id :=l_max_branch_cust_acct_id;
                                        --
                                        g_recurr_accrual_log_rec(g_log_idx).run_id :=g_run_id;
                                        g_recurr_accrual_log_rec(g_log_idx).fund_id :=g_99999999_fund_id;
                                        g_recurr_accrual_log_rec(g_log_idx).ebs_updated_by :=l_user_id;
                                        g_recurr_accrual_log_rec(g_log_idx).ebs_update_date :=sysdate;    
                                        g_recurr_accrual_log_rec(g_log_idx).run_mode :=g_draft;                                                            
                                        --
                                        g_recurr_accrual_log_rec(g_log_idx).status_code :='STAGE_4';
                                        g_recurr_accrual_log_rec(g_log_idx).status_desc :=case 
                                                                                                                                                    when l_return_status =fnd_api.g_ret_sts_success  
                                                                                                                                                                   then 'Variance of $'
                                                                                                                                                                              ||to_char(g_adj_rec.amount, 'FM999,999,990.90') 
                                                                                                                                                                              ||' adjusted towards Line# '|| l_mx_pur_br_line_num 
                                                                                                                                                                              ||', Detail Seq# '|| g_99999999_max_detail_seq                                                                                                                                                                              
                                                                                                                                                    else g_stage41 
                                                                                                                                           end;
                                        --                                   
                              exception
                                 when others then
                                     print_log('Rounding Adjustment, Error in assigning log record to plsql table, message ='||sqlerrm);
                                     print_log(
                                                        'Rounding Adjustment Failed, c_accruals_header(idx1).record_seq ='
                                                        ||c_accruals_header(idx1).record_seq
                                                        ||', Line# '||l_mx_pur_br_line_num
                                                      );                                     
                              end;
                              --                                
                              if l_return_status =fnd_api.g_ret_sts_success then
                                 print_log
                                   (
                                     'Rounding adjustment : Recurring accrual created for record seq :'||c_accruals_header(idx1).record_seq||
                                     ', line num : '||l_mx_pur_br_line_num||
                                     ', Detail Seq : '||g_adj_rec.attribute13||
                                     ', utilization_id ='||l_utilization_id);
                                 --                                                                                                                                                          
                               else
                                 print_log(
                                                     'Rounding adjustment : Recurring accrual failed for record seq :'||c_accruals_header(idx1).record_seq||
                                                     ', line num : '||l_mx_pur_br_line_num
                                                   );
                                  rollback to start_here;
                              end if;
                                  --                                
                              exception
                               when others then            
                                --l_api_failed :=l_api_failed+1;
                                print_log('@Process -Rounding adjustment, l_api_failed variable updated, stage 4 -api call....error :'||c_accruals_header(idx1).record_seq||', msg =>'||sqlerrm);
                              end;
                              --                          
                          -- ^^^^
                        else
                          --
                          Null; -- The total amount requested is same as the ones accrued. 
                          --
                        end if;
                   end if;
                   --
                 exception
                  when others then
                   rollback to start_here;
                   print_log('Issue in checking total utilization amount for record_seq '||c_accruals_header(idx1).record_seq||', message ='||sqlerrm);
                 end;
                --
                 if g_mode =g_draft then 
                   Rollback to start_here; -- all lines for the header will be rolled back at all cost..
                 else
                   Null;
                 end if;
                 --
             end if;   
             --                                                                     
          end loop; --  for idx1 in 1 .. c_accruals_header.count loop 
          --
     end if;
     --
     -- Capture all activities that happened so far whether the mode is Draft or Final.
     --
       begin
            if g_ozf_misc_utils_rec.count >0 then        
                print_log('Begin Variance_Utilization_Log, Count ='||g_ozf_misc_utils_rec.count);
                Variance_Utilization_Log
                      (
                           p_ozf_misc_utils_rec =>g_ozf_misc_utils_rec
                      );                  
                print_log('End Variance_Utilization_Log.');                                   
            end if;
       exception
         when others then
          print_log('Error in Variance_Utilization_Log, message ='||sqlerrm);
       end;           
        --
        -- update header table status
        --
        begin
            if g_hdr_rec.count >0 then
                print_log('Begin: update header table status, Count ='||g_hdr_rec.count);
                 for idx5 in 1 .. g_hdr_rec.count loop
                    update_header_status
                          (
                              p_rec_seq =>g_hdr_rec(idx5).record_seq
                             ,p_status_code =>g_hdr_rec(idx5).status_code
                             ,p_status_desc =>g_hdr_rec(idx5).status_desc
                             ,p_ebs_updated_by =>g_hdr_rec(idx5).ebs_updated_by   
                          ) 
                          ;
                 end loop;
                print_log('End: update header table status');                 
            end if;
        exception
         when others then
          print_log('Error in updating header status message ='||sqlerrm);
       end;
    --
    -- update line accrual utils table status
    --
       begin
            if g_line_rec.count >0 then
                print_log('Begin: update line accrual utils table status, Count ='||g_line_rec.count);            
                 --print_log('g_line_rec.count  =>'||g_line_rec.count );        
                 for idx6 in 1 .. g_line_rec.count loop
                    update_alloc_util_status 
                          (
                              p_rec_seq =>g_line_rec(idx6).record_seq
                             ,p_line_num =>g_line_rec(idx6).line_num
                             ,p_status_code =>g_line_rec(idx6).status_code
                             ,p_status_desc =>g_line_rec(idx6).status_desc
                             ,p_ebs_updated_by =>g_line_rec(idx6).ebs_updated_by
                          )
                          ;     
                 end loop;
                print_log('End: update line accrual utils table status, Count =');                 
            end if;
       exception
         when others then
          print_log('Error in updating line utilization status message ='||sqlerrm);
       end;        
    --    
    -- Capture all activities that happened so far whether the mode is Draft or Final.
    --
       begin
            if g_recurr_accrual_log_rec.count >0 then        
                print_log('Begin Appln_Log, Count ='||g_recurr_accrual_log_rec.count);
                   Appln_log ( p_recurr_accrual_log_rec =>g_recurr_accrual_log_rec);
                print_log('End Appln_Log.');                                   
            end if;
       exception
         when others then
          print_log('Error in Appln_Log, message ='||sqlerrm);
       end;        
    --      
   exception
    when no_data_found then
     print_log('No data found in process_adj, msg :'||sqlerrm);
     raise program_error;
    when others then
     print_log('When other errrors in process_adj : msg '||sqlerrm);
     raise program_error;
   end;
   --
   print_log('End :  process_adj (p_rec_seq =>'||p_rec_seq||', l_util_id_count ='||l_util_id_count||');');
   --
   --commit;
   --
 exception
  when others then  
   print_log('Issue in xxcus_ozf_recurring_accr_pkg.process_adj routine, msg ='||sqlerrm);
   raise program_error;
 end process_adj;
  --  
 procedure Main
   ( 
      errbuff           out      varchar2,
      retcode          out      number,
      p_mode         in varchar2,
      p_record_seq in number default null,
      p_debug in varchar2 default 'N'
   ) is
  --
  l_req_id number :=fnd_global.conc_request_id;
  l_user_id number :=fnd_global.user_id;
  --
  l_move_fwd boolean :=Null; 
  --  
  cursor c_report is
    select 
            'RUN_MODE'||'|'||
            'REQUEST_ID'||'|'||
            'LINE_TYPE'||'|'||
            'RECORD_SEQ'||'|'||
            'LINE_NUM'||'|'||
            'MVID'||'|'||
            'MVID_NAME'||'|'||
            'OFFER_NAME'||'|'||
            'CURRENCY'||'|'||
            'RECEIPT_START_DATE'||'|'||
            'RECEIPT_END_DATE'||'|'||
            'REASON_NAME'||'|'||
            'LOB_NAME'||'|'||
            'BU_NAME'||'|'||
            'BRANCH_NAME'||'|'||
            'RECEIPT_AMOUNT'||'|'||
            'BRANCH_TOTAL_RECEIPT'||'|'||
            'AMOUNT_PROCESSED'||'|'||
            'STATUS_DESC'||'|' my_report
           ,record_seq
           ,line_num
           ,1 pull_seq            
    from  xxcus.xxcus_ozf_recurr_accrual_log
    where 1 =1
    and request_id =l_req_id
    and rownum =1
    union all
    select 
            run_mode||'|'||
            request_id||'|'||
            line_type||'|'||
            record_seq||'|'||
            line_num||'|'||
            mvid||'|'||
            mvid_name||'|'||
            offer_name||'|'||
            currency||'|'||
            to_char(receipt_start_date, 'mm/dd/yyyy')||'|'||
            to_char(receipt_end_date, 'mm/dd/yyyy')||'|'||
            reason_name||'|'||
            lob_name||'|'||
            bu_name||'|'||
            branch_name||'|'||
            to_char(receipt_amount, 'FM999,990.90')||'|'||
            to_char(branch_total_receipt, 'FM999,990.90')||'|'||
            to_char(applied_recurring_amount, 'FM999,990.90')||'|'||
            status_desc||'|' my_report
           ,record_seq
           ,line_num
           ,2 pull_seq
    from  xxcus.xxcus_ozf_recurr_accrual_log
    where 1 =1
    and request_id =l_req_id
    order by 4 asc, 2 asc, 3 asc
     ;  
  --
 begin 
      --
      g_debug :=p_debug;
      g_mode :=p_mode;
      --      
      print_log(' ');
      print_log('Parameters :');
      print_log('===========');      
      print_log('   1. Run Mode : '||p_mode);
      print_log('   2. Record Sequence :'||p_record_seq);
      print_log('   3. Debug Flag : '||p_debug);                              
      print_log(' ');      
      --
      begin -- Get run id
        --
        delete xxcus.xxcus_ozf_utils_stg where run_mode =g_draft and ebs_updated_by =l_user_id;
        print_log('Clean up xxcus.xxcus_ozf_utils_stg staging table for draft records for current user.');        
        --
        delete xxcus.xxcus_ozf_misc_accruals where run_mode =g_draft and ebs_updated_by =l_user_id;
        print_log('Clean up xxcus.xxcus_ozf_misc_accruals table for draft records for current user.');        
        --
        delete xxcus.xxcus_ozf_recurr_accrual_log where run_mode =g_draft and ebs_updated_by =l_user_id;
        print_log('Clean up xxcus.xxcus_ozf_recurr_accrual_log table for draft records for current user.'); 
        --
        delete xxcus.xxcus_ozf_recurr_accr_purdtls where run_mode =g_draft and ebs_updated_by =l_user_id;
        print_log('Clean up xxcus.xxcus_ozf_recurr_accr_purdtls table for draft records for current user.');        
        --        
        select xxcus.xxcus_ozf_recurr_accr_runid_s.nextval
        into    g_run_id
        from  dual;
        --
        print_log('Successfully fetched global run id : '||g_run_id);
        --
      exception
       when others then
        g_run_id :=to_char(sysdate, 'YYYYMMDDHH24MISS');
      end;
      --    
      begin -- update  run id
        --
        update xxcus.xxcus_ozf_recurring_accruals set run_id =g_run_id where run_id =0; -- only for the current batch that was just uploaded
        --
        print_log('Successfully updated run id from default 0 to the latest sequence value : '||g_run_id||', records updated :'||sql%rowcount);
        --
        if (sql%rowcount >0) then
          commit;
       else
        Null;       
       end if;
         --
         l_move_fwd :=TRUE;
        --                  
      exception
       when others then
        l_move_fwd :=FALSE;  
        g_run_id :=to_char(sysdate, 'YYYYMMDDHH24MISS');
        print_log('Error in updating global run id, message : '||sqlerrm);
      end;
      --
      if (l_move_fwd) then
         Validate (p_ok =>l_move_fwd, p_rec_seq =>p_record_seq);
      else
        print_log('l_move_fwd is false, failed to invoke validate routine'||sqlerrm);       
      end if;
      --
      if (l_move_fwd) then
           --
           print_log('Validate routine returned true');  
           extract_recurring_accruals (p_rec_seq =>p_record_seq);
           l_move_fwd :=TRUE;
           --
      else
           --
           print_log('Validate routine returned false');
           l_move_fwd :=FALSE;           
           --
      end if;
      --
      if (l_move_fwd) then
           --
           print_log('Extract accruals routine returned true, Begin: Adjustments');  
           process_adj (p_rec_seq =>p_record_seq);
           --
      else
           --
           print_log('Extract accruals routine returned false, failed to proceed to adjustments.');
           --
      end if;
      -- 
      -- Print all the activities that happened so far in the concurrent job output file
      --
      begin   
         --
         print_log(' ');
         print_log('Begin: Print report to Concurrent Job Output');      
         --
         for rec in c_report loop
          print_output(rec.my_report); 
         end loop;
         --
         print_log('End: Print report to Concurrent Job Output');   
         --
      exception
       when others then 
        print_log('Issue in generating report for user, message =>'||sqlerrm);
      end;
      --
 exception
  when others then
   print_log('Issue in xxcus_ozf_recurring_accr_pkg.Main routine, msg ='||sqlerrm);
 end Main;
  --
end xxcus_ozf_recurring_accr_pkg;
/