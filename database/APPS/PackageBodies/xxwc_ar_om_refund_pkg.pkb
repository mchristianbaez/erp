CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ar_om_refund_pkg
AS
   /*************************************************************************
   *   $Header xxwc_ar_om_refund_pkg.sql $
   *   Module Name: xxwc OM Counter Order Refund package
   *
   *   PURPOSE:   Used in extension  Counter Order Refunds process
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/03/2012  Shankar Hariharan           Initial Version
   *   1.1        09/10/2012  Shankar Hariharan  Changed the batch source derive logic while submitting auto invoice
   *   1.2        04/04/2013  Shankar Hariharan  Added new procedure refund_co_cash
   *   1.3        04/11/2013  Shankar Hariharan  Oracle Credit Card Disconnect Project
   *   1.4        07/01/2013  Shankar Hariharan  TMS Ticket 20130618-01124--include check payment method for auto w/o
   *   1.5        23/09/2014  Veera C            TMS# 20141001-00163 Modified code as per the Canada OU Test
   *   1.6        09/26/2016  Neha Saini TMS# 20150527-00318 Forego Two AR Triggers
   * ***************************************************************************/
   PROCEDURE launch_auto_remittance (l_cash_receipt_id   IN     NUMBER,
                                     o_return_message       OUT VARCHAR2)
   IS
      /*
      Launched from the "Process Refund" button action, if the receipt is a credit card receipt
      and not in remitted status. Oracle do not allow a refund to a unremitted credit card receipt
      */
      l_request_id          NUMBER;
      l_receipt_class_id    NUMBER;
      l_receipt_method_id   NUMBER;
      l_branch_party_id     NUMBER;
      l_bank_acct_use_id    NUMBER;
   BEGIN
      -- Derive parameter values for concurrent program submission
      SELECT rm.receipt_method_id, rm.receipt_class_id
        INTO l_receipt_method_id, l_receipt_class_id
        FROM ar_receipt_methods rm, ar_cash_receipts cr
       WHERE     rm.receipt_method_id = cr.receipt_method_id
             AND cr.cash_receipt_id = l_cash_receipt_id;

      SELECT ba.bank_acct_use_id, br.branch_party_id
        INTO l_bank_acct_use_id, l_branch_party_id
        FROM ce_bank_acct_uses ba,
             ce_bank_accounts cba,
             ce_bank_branches_v br,
             ar_cash_receipts cr
       WHERE     ba.bank_account_id = cba.bank_account_id
             AND cba.bank_branch_id = br.branch_party_id
             AND ba.bank_acct_use_id = cr.remit_bank_acct_use_id
             AND cr.cash_receipt_id = l_cash_receipt_id;


      l_request_id :=
         fnd_request.submit_request (
            --REMIT, 2012/04/06 00:00:00, 2012/04/06 00:00:00, Y, Y, Y, , , USD, , , , STANDARD, 2003, 5000, , 8103, 10081, , , , , , , , , , , , , , , , , , , , , , , , , 1
            application   => 'AR',
            program       => 'ARAREMMB',
            description   => 'Automatic Remittances Master Program from OM',
            start_time    => SYSDATE,
            sub_request   => NULL,
            argument1     => 'REMIT',
            argument2     => TO_CHAR (TRUNC (SYSDATE),
                                      'YYYY/MM/DD HH24:MI:SS'),
            argument3     => TO_CHAR (TRUNC (SYSDATE),
                                      'YYYY/MM/DD HH24:MI:SS'),
            argument4     => 'Y',
            argument5     => 'Y',
            argument6     => 'Y',
            argument7     => NULL,
            argument8     => NULL,
            argument9     => 'USD',
            argument10    => NULL,
            argument11    => NULL,
            argument12    => NULL,
            argument13    => 'STANDARD',
            argument14    => TO_CHAR (l_receipt_class_id),
            argument15    => TO_CHAR (l_receipt_method_id),
            argument16    => NULL,
            argument17    => TO_CHAR (l_branch_party_id),
            argument18    => TO_CHAR (l_bank_acct_use_id),
            argument19    => NULL,
            argument20    => NULL,
            argument21    => NULL,
            argument22    => NULL,
            argument23    => NULL,
            argument24    => NULL,
            argument25    => NULL,
            argument26    => NULL,
            argument27    => NULL,
            argument28    => NULL,
            argument29    => NULL,
            argument30    => NULL,
            argument31    => NULL,
            argument32    => NULL,
            argument33    => NULL,
            argument34    => NULL,
            argument35    => NULL,
            argument36    => NULL,
            argument37    => NULL,
            argument38    => NULL,
            argument39    => NULL,
            argument40    => NULL,
            argument41    => NULL,
            argument42    => NULL,
            argument43    => '1');

      IF l_request_id <> 0
      THEN
         o_return_message :=
               'Remittance Process submitted successfully with request id='
            || l_request_id
            || ' Do not press this button again until the process has completed. Monitor processing through the Requests Concurrent Manager form';
         COMMIT;
         RETURN;
      ELSE
         o_return_message := 'Issue submitting Remittance Process';
         ROLLBACK;
         RETURN;
      END IF;
   END launch_auto_remittance;


   PROCEDURE check_rec_in_doubt (p_cash_receipt_id   IN            NUMBER,
                                 x_rec_in_doubt         OUT NOCOPY VARCHAR2,
                                 x_rid_reason           OUT NOCOPY VARCHAR2)
   IS
   BEGIN
      ---Shankar-- Copied from ar_prepayments package
      ---Called from the XXWC_OM_REFUND refund from to check whether a credit card receipt
      -- is in a remitted state
      x_rec_in_doubt := 'N';
      x_rid_reason := NULL;

      ---
      --- For CC receipts, receipt should be remitted
      ---
      BEGIN
         SELECT 'Y',
                'Receipt is not in Remitted or Cleared Status. Please wait for few minutes ' --arp_standard.fnd_message('AR_RID_NOT_REMITTED_OR_CLEARED')
           INTO x_rec_in_doubt, x_rid_reason
           FROM DUAL
          WHERE (NOT EXISTS
                    (SELECT 1
                       FROM AR_CASH_RECEIPT_HISTORY crh
                      WHERE     crh.cash_receipt_id = p_cash_receipt_id
                            AND crh.status IN ('REMITTED', 'CLEARED')));
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
         WHEN OTHERS
         THEN
            arp_standard.debug (
                  'Unexpected error '
               || SQLERRM
               || ' occurred in ar_prepayments.check_rec_in_doubt');
            RAISE;
      END;

      ---
      --- There should not be any Claims Investigation or CB special application
      ---
      BEGIN
         SELECT 'Y',
                arp_standard.fnd_message ('AR_RID_CLAIM_OR_CB_APP_EXISTS')
           INTO x_rec_in_doubt, x_rid_reason
           FROM DUAL
          WHERE EXISTS
                   (SELECT 1
                      FROM ar_receivable_applications ra
                     WHERE     ra.cash_receipt_id = p_cash_receipt_id
                           AND applied_payment_schedule_id IN (-4, -5)
                           AND display = 'Y');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
         WHEN OTHERS
         THEN
            arp_standard.debug (
                  'Unexpected error '
               || SQLERRM
               || ' occurred in ar_prepayments.check_rec_in_doubt');
            RAISE;
      END;

      ---
      --- Receipt should not be reversed
      ---
      BEGIN
         SELECT 'Y', arp_standard.fnd_message ('AR_RID_RECEIPT_REVERSED')
           INTO x_rec_in_doubt, x_rid_reason
           FROM DUAL
          WHERE EXISTS
                   (SELECT 1
                      FROM ar_cash_receipts cr1
                     WHERE     cr1.cash_receipt_id = p_cash_receipt_id
                           AND cr1.reversal_date IS NOT NULL);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
         WHEN OTHERS
         THEN
            arp_standard.debug (
                  'Unexpected error '
               || SQLERRM
               || ' occurred in ar_prepayments.check_rec_in_doubt');
            RAISE;
      END;
   ---
   EXCEPTION
      WHEN OTHERS
      THEN
         arp_standard.debug (
               'Unexpected error '
            || SQLERRM
            || ' occurred in arp_process_returns.check_rec_in_doubt');
         RAISE;
   END check_rec_in_doubt;

   PROCEDURE cash_refund_process (i_header_id             IN     NUMBER,
                                  i_return_header_id      IN     NUMBER,
                                  i_cash_receipt_id       IN     NUMBER,
                                  i_refund_amount         IN     NUMBER,
                                  i_check_refund_amount   IN     NUMBER,
                                  i_payment_type_code     IN     VARCHAR2,
                                  o_return_status            OUT VARCHAR2,
                                  o_return_message           OUT VARCHAR2)
   IS
      /*
       Counter Order  Refund Core process -- Only amount in On Account status is refunded

      Credit CArd Refund -- a receivable activity of refund is applied against the receipt for the refund amount and Oracle Payments
                         automatically refunds the money back to the original credit card

     Check Refund --Refund amount is left as On Account against the receipt and a notification sent for manual
                   check refund processing

    Cash Refund -- A receivable activity identified by custom profile of type "receipt writeoff" is applied against
                 the receipt to record the actual cash refund. Since the activity is of type "reciept writeoff",
                 approval limits needs to be setup on a individual user basis for this to work.
      */
      l_applied_amt                    NUMBER;
      l_line_amt                       NUMBER;
      l_tax_amt                        NUMBER;
      l_new_applied_amt                NUMBER;
      l_ps_applied_amt                 NUMBER;
      l_return_status                  VARCHAR2 (1);
      l_msg_count                      NUMBER;
      l_msg_data                       VARCHAR2 (32000);
      l_app_ps_id                      NUMBER;
      p_count                          NUMBER;
      l_application_ref_type           ar_receivable_applications.application_ref_type%TYPE;
      l_application_ref_id             ar_receivable_applications.application_ref_id%TYPE;
      l_application_ref_num            ar_receivable_applications.application_ref_num%TYPE;
      l_secondary_application_ref_id   ar_receivable_applications.secondary_application_ref_id%TYPE;
      l_receivable_application_id      ar_receivable_applications.receivable_application_id%TYPE;
      l_receivable_application_id1     ar_receivable_applications.receivable_application_id%TYPE;
      l_cc_refund_ps_id                NUMBER := -6;
      l_rec_wo_ps_id                   NUMBER := -3;
      l_prepay_ps_id                   NUMBER := -7;
      l_refund_rec_trx_id              NUMBER;
      l_rec_wo_rec_trx_id              NUMBER;
      l_acc_app_ps_id                  NUMBER;
      l_acc_app_rec_trx_id             NUMBER;
      l_debug                          VARCHAR2 (240);
      l_adj_ccid                       NUMBER;
      l_pay_count                      NUMBER;
      l_inv_count                      NUMBER;
      l_cust_trx_line_id               NUMBER;
      l_return_Number                  NUMBER; -- used in return value of email
      l_refund_email                   VARCHAR2 (100)
         := fnd_Profile.VALUE ('XXWC_OM_REFUND_REQ_EMAIL');
      --Variables for Notification
      l_orig_order_number              VARCHAR2 (100);
      l_orig_order_date                DATE;
      l_return_order_number            VARCHAR2 (100);
      l_return_order_date              DATE;
      l_receipt_number                 VARCHAR2 (100);
      l_receipt_date                   DATE;
      l_receipt_amount                 NUMBER;
      l_customer_number                VARCHAR2 (100);
      l_customer_name                  VARCHAR2 (240);
      l_bal_due_remaining              NUMBER;
      l_payment_set_id                 NUMBER;
      l_receivable_trx_id              NUMBER;
      l_cc_type                        VARCHAR2 (1);
      l_cc_rec_wo_rec_trx_id           NUMBER;
      l_attribute_rec                  AR_RECEIPT_API_PUB.attribute_rec_type;
   BEGIN
      -- Go through the process, only if a refund is requested
      IF i_refund_amount IS NOT NULL OR i_check_refund_amount IS NOT NULL -- IF 1
      THEN
         -- Get refund rec trx id
         BEGIN
            SELECT receivables_trx_id
              INTO l_refund_rec_trx_id
              FROM ar_receivables_trx
             WHERE TYPE = 'CCREFUND' AND status = 'A' AND ROWNUM = 1;

            l_rec_wo_rec_trx_id :=
               fnd_profile.VALUE ('XXWC_OM_CASH_REFUND_ACTIVITY');
            l_cc_rec_wo_rec_trx_id :=
               fnd_profile.VALUE ('XXWC_OM_CC_REFUND_ACTIVITY');
         EXCEPTION
            WHEN OTHERS
            THEN
               o_return_message :=
                  'Missing Receivable Trx Setup or Branch Specific CCID';
               o_return_status := 'E';
               RETURN;
         END;

         -- Check if the receipt is applied against an invoice
         SELECT NVL (SUM (amount_applied), 0)
           INTO l_applied_amt
           FROM ar_receivable_applications_v
          WHERE     cash_receipt_id = i_cash_receipt_id
                AND (applied_payment_schedule_id = -1);

         IF l_applied_amt = 0
         THEN
            o_return_message := 'Amount not available for Refund.';
            o_return_status := 'E';
            RETURN;
         END IF;

         -- Oracle CC Disconnect Project Shankar Hariharan 04/11/2013
         IF i_payment_type_code = 'CHECK'
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_cc_type
                 FROM apps.ar_cash_receipts a, ar_receipt_methods b
                WHERE     a.receipt_method_id = b.receipt_method_id
                      AND a.cash_receipt_id = i_cash_receipt_id
                      AND (b.name LIKE '%AMX%' OR b.name LIKE '%VMD%');
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_cc_type := 'N';
            END;
         END IF;

         -- Oracle CC Disconnect Project Shankar Hariharan 04/11/2013 END
         --=====================================================
         -- Amount available for unapply and reapply
         IF l_applied_amt >=
               NVL (i_refund_amount, 0) + NVL (i_check_refund_amount, 0)
         THEN                                                          -- IF 2
            -- Check the number of payment method
            SELECT COUNT (1)
              INTO l_pay_count
              FROM oe_payments
             WHERE header_id = i_header_id;

            -- Get information from id for reporting
            SELECT order_number, ordered_date
              INTO l_return_order_number, l_return_order_date
              FROM apps.oe_order_headers
             WHERE header_id = i_return_header_id;

            SELECT order_number,
                   ordered_date,
                   customer_number,
                   sold_to
              INTO l_orig_order_number,
                   l_orig_order_date,
                   l_customer_number,
                   l_customer_name
              FROM oe_order_headers_v
             WHERE header_id = i_header_id;

            SELECT receipt_number, receipt_date, amount
              INTO l_receipt_number, l_receipt_date, l_receipt_amount
              FROM ar_cash_receipts_v
             WHERE cash_receipt_id = i_cash_receipt_id;

            -- Get the PS ID for the invoice applied to the receipt
            BEGIN
               SELECT applied_payment_schedule_id,
                      amount_applied,
                      receivable_application_id
                 INTO l_app_ps_id,
                      l_ps_applied_amt,
                      l_receivable_application_id
                 FROM ar_receivable_applications_v
                WHERE     cash_receipt_id = i_cash_receipt_id
                      AND applied_payment_schedule_id = -1
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_return_status := 'E';
                  o_return_message :=
                     'Unable to determine the On Account Amount';
            END;

            -- Unapply the On Account from the receipt
            AR_RECEIPT_API_PUB.UNAPPLY_ON_ACCOUNT (
               p_api_version                 => 1.0,
               p_init_msg_list               => 'T',
               p_commit                      => 'F',
               p_validation_level            => 100,
               x_return_status               => l_return_status,
               x_msg_count                   => l_msg_count,
               x_msg_data                    => l_msg_data,
               p_cash_receipt_id             => i_cash_receipt_id,
               p_receivable_application_id   => l_receivable_application_id,
               p_reversal_gl_date            => SYSDATE);

            l_debug :=
                  l_debug
               || 'Unapply status='
               || l_return_status
               || ' Unapply Amount='
               || l_ps_applied_amt;
            -- IF success then proceed further
            l_new_applied_amt :=
                 l_ps_applied_amt
               - NVL (i_refund_amount, 0)
               - NVL (i_check_refund_amount, 0);

            -- Reapply the net amount
            IF l_return_status = 'S'
            THEN
               IF l_new_applied_amt > 0
               THEN
                  AR_RECEIPT_API_PUB.APPLY_ON_ACCOUNT (
                     p_api_version        => 1.0,
                     p_init_msg_list      => 'T',
                     p_commit             => 'F',
                     p_validation_level   => 100,
                     p_cash_receipt_id    => i_cash_receipt_id-- , p_applied_payment_schedule_id   => l_app_ps_id
                     ,
                     p_amount_applied     => l_new_applied_amt,
                     x_return_status      => l_return_status,
                     x_msg_count          => l_msg_count,
                     x_msg_data           => l_msg_data);
               ELSE
                  l_return_status := 'S';
               END IF;

               l_debug :=
                     l_debug
                  || 'Reapply status='
                  || l_return_status
                  || ' Reapply Amount='
                  || l_new_applied_amt;

               IF l_return_status = 'S'
               THEN                                                    -- IF 4
                  IF i_payment_type_code = 'CASH'
                  THEN                                                 -- IF 5
                     l_acc_app_ps_id := l_rec_wo_ps_id;
                     l_acc_app_rec_trx_id := l_rec_wo_rec_trx_id;
                  ELSIF i_payment_type_code = 'CHECK'
                  THEN                     --and nvl(l_cc_type,'N') = 'Y' then
                     l_acc_app_ps_id := l_rec_wo_ps_id;
                     l_acc_app_rec_trx_id := l_cc_rec_wo_rec_trx_id;
                  ELSIF i_payment_type_code = 'CREDIT_CARD'
                  THEN                                               -- ELSE 5
                     l_acc_app_ps_id := l_cc_refund_ps_id;
                     l_acc_app_rec_trx_id := l_refund_rec_trx_id;
                  END IF;                                          -- END IF 5

                  -- if refund type is cash or credit card, apply the receivable trx type
                  -- No need to do anything for check refund request
                  IF    (    i_payment_type_code IN ('CASH', 'CREDIT_CARD')
                         AND NVL (i_refund_amount, 0) <> 0)
                     OR (    i_payment_type_code = 'CHECK'
                         AND NVL (i_refund_amount, 0) <> 0) --and nvl(l_cc_type,'N') = 'Y')
                  THEN
                     l_receivable_application_id1 := NULL;
                     l_application_ref_type := NULL;
                     l_application_ref_id := NULL;
                     l_application_ref_num := NULL;
                     l_payment_set_id := NULL;
                     l_attribute_rec.attribute1 := i_header_id;
                     l_attribute_rec.attribute2 := l_orig_order_number;

                     AR_RECEIPT_API_PUB.ACTIVITY_APPLICATION (
                        p_api_version                    => 1.0,
                        p_init_msg_list                  => 'T',
                        p_commit                         => 'F',
                        p_validation_level               => 100,
                        x_return_status                  => l_return_status,
                        x_msg_count                      => l_msg_count,
                        x_msg_data                       => l_msg_data,
                        p_cash_receipt_id                => i_cash_receipt_id,
                        p_applied_payment_schedule_id    => l_acc_app_ps_id,
                        p_receivables_trx_id             => l_acc_app_rec_trx_id,
                        p_amount_applied                 => i_refund_amount,
                        p_attribute_rec                  => l_attribute_rec,
                        p_receivable_application_id      => l_receivable_application_id,
                        p_application_ref_type           => l_application_ref_type,
                        p_application_ref_id             => l_application_ref_id,
                        p_application_ref_num            => l_application_ref_num,
                        p_secondary_application_ref_id   => l_secondary_application_ref_id);

                     l_debug :=
                        l_debug || 'Acc app status=' || l_return_status;
                  END IF;

                  -- If refund type is check or original payment method is check then notify
                  IF     (   i_payment_type_code = 'CHECK'
                          OR NVL (i_check_refund_amount, 0) <> 0)
                     AND NVL (l_cc_type, 'N') = 'N'
                  THEN
                     UTL_MAIL.send (
                        sender       => l_refund_email,
                        recipients   => l_refund_email,
                        cc           => NULL,
                        bcc          => NULL,
                        subject      =>    'Check Refund Request for Order '
                                        || l_return_order_number,
                        MESSAGE      =>    'A Check Refund Request has been processed by '
                                        || fnd_global.user_name
                                        || ' for Customer '
                                        || l_customer_number
                                        || ':'
                                        || l_customer_name
                                        || CHR (10)
                                        || CHR (10)
                                        || 'Order Information:'
                                        || CHR (10)
                                        || '    Original Order Number: '
                                        || l_orig_order_number
                                        || CHR (10)
                                        || '    Original Order Date: '
                                        || l_orig_order_date
                                        || CHR (10)
                                        || '    Return Order Number: '
                                        || l_return_order_number
                                        || CHR (10)
                                        || '    Return Order Date: '
                                        || l_return_order_date
                                        || CHR (10)
                                        || CHR (10)
                                        || 'Receipt Information:'
                                        || CHR (10)
                                        || '    Receipt Number: '
                                        || l_receipt_number
                                        || CHR (10)
                                        || '    Receipt Date: '
                                        || l_receipt_date
                                        || CHR (10)
                                        || '    Receipt Amount: '
                                        || l_receipt_amount
                                        || CHR (10)
                                        || CHR (10)
                                        || 'Requested Check Refund: '
                                        || NVL (i_check_refund_amount,
                                                i_refund_amount),
                        mime_type    => 'text/plain; charset=us-ascii',
                        priority     => 3,
                        replyto      => NULL);
                  END IF;

                  -- Record the refund activity in the custom table
                  IF l_return_status = 'S'
                  THEN                                                 -- IF 6
                     INSERT
                       INTO apps.xxwc_om_cash_refund_tbl (header_id,
                                                          return_header_id,
                                                          cash_receipt_id,
                                                          refund_date,
                                                          refund_amount,
                                                          check_refund_amount,
                                                          payment_type_code,
                                                          process_flag,
                                                          creation_date,
                                                          created_by,
                                                          last_update_date,
                                                          last_updated_by,
                                                          last_update_login)
                     VALUES (i_header_id,
                             i_return_header_id,
                             i_cash_receipt_id,
                             SYSDATE,
                             NVL (i_refund_amount, 0),
                             NVL (i_check_refund_amount, 0),
                             i_payment_type_code,
                             'Y',
                             SYSDATE,
                             fnd_global.user_id,
                             SYSDATE,
                             fnd_global.user_id,
                             -1);

                     o_return_status := 'S';
                     o_return_message := 'Refund Processed Sucessfully';
                  ELSE                                               -- ELSE 6
                     IF l_msg_count = 1
                     THEN
                        NULL;
                     ELSIF l_msg_count > 1
                     THEN
                        LOOP
                           p_count := p_count + 1;
                           l_msg_data :=
                              FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, 'F');

                           IF l_msg_data IS NULL
                           THEN
                              EXIT;
                           END IF;
                        END LOOP;
                     END IF;

                     o_return_status := l_return_status;
                     o_return_message := l_debug || ' ' || l_msg_data;
                  END IF;                                          -- END IF 6
               ELSE                                                  -- ELSE 4
                  --===============================================
                  IF l_msg_count = 1
                  THEN
                     NULL;
                  ELSIF l_msg_count > 1
                  THEN
                     LOOP
                        p_count := p_count + 1;
                        l_msg_data :=
                           FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, 'F');

                        IF l_msg_data IS NULL
                        THEN
                           EXIT;
                        END IF;
                     END LOOP;
                  END IF;

                  --===============================================
                  o_return_status := l_return_status;
                  o_return_message := l_debug || ' ' || l_msg_data;
               END IF;                                             -- END IF 4
            ELSE
               IF l_msg_count = 1
               THEN
                  NULL;
               ELSIF l_msg_count > 1
               THEN
                  LOOP
                     p_count := p_count + 1;
                     l_msg_data := FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, 'F');

                     IF l_msg_data IS NULL
                     THEN
                        EXIT;
                     END IF;
                  END LOOP;
               END IF;

               o_return_status := l_return_status;
               o_return_message := l_debug || ' ' || l_msg_data;
            END IF;
         --===================================================================================
         ELSE                                                        -- ELSE 2
            o_return_message :=
                  'Refund Amount of '
               || i_refund_amount
               || ' is greater than receipt applied amount '
               || l_applied_amt;
            o_return_status := 'E';
         END IF;                                                   -- END IF 2
      END IF;                                                      -- END IF 1
   END cash_refund_process;
   /*************************************************************************
   *   $Header xxwc_ar_om_refund_pkg.sql $
   *   Module Name: xxwc OM Counter Order Refund package
   *   procedure: launch_auto_invoice
   *   PURPOSE:   Used in extension  Counter Order Refunds process
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   1.0        08/03/2012  Shankar Hariharan           Initial Version
   *   1.6        09/26/2016  Neha Saini       TMS# 20150527-00318 Forego Two AR Triggers
   * ***************************************************************************/

   PROCEDURE launch_auto_invoice (i_sales_order      IN     VARCHAR2,
                                  o_return_message      OUT VARCHAR2)
   IS
      l_request_id          NUMBER;
      l_source_id           NUMBER;
      l_set_print_options   BOOLEAN;
      l_batch_source_name   VARCHAR2 (100);
      l_pre_request_id      NUMBER;                       -- Added for Ver 1.6
   -- Launched from XXWC_OM_REFUND from for an order that is being returned the same day
   -- Invoices need to be created first before the refunds can be processed.
   BEGIN
      /*
        begin
          select batch_source_id
            into l_source_id
            from RA_BATCH_SOURCES_ALL
           where name = 'ORDER MANAGEMENT'
             and org_id=xxwc_ascp_scwb_pkg.get_wc_org_id;  */
      -- Added by Shankar 09-Sep-2012
       --Added for Ver 1.6 Begin
      l_pre_request_id :=
         fnd_request.submit_request ('XXWC',
                                     'XXWC_AR_INV_PREPROCESS',
                                     NULL,
                                     NULL,
                                     FALSE,
                                     NULL);

      IF l_pre_request_id = 0
      THEN
         o_return_message :=
            'Failed to Submit XXWC AR Invoice Preprocessor, Autoinoice will not be submitted';
      ELSE
         COMMIT;
         
      BEGIN
         SELECT a.invoice_source_id, c.name
           INTO l_source_id, l_batch_source_name
           FROM apps.oe_transaction_types a,
                apps.oe_order_headers b,
                apps.ra_batch_sources c
          WHERE     b.order_number = i_sales_order
                AND a.transaction_type_id = b.order_type_id
                AND a.invoice_source_id = c.batch_source_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_source_id := NULL;
      END;

      -- ADded to avoid printing invoices at the branch
      l_set_print_options :=
         FND_Request.set_print_options (printer => 'noprint', copies => 0);

      l_request_id :=
         fnd_request.submit_request (
            --1, -99, 1002, ORDER MANAGEMENT, 2012/03/28 00:00:00, , , , , , , , , , , , , 10001627, 10001627, , , , , , , Y,
            application   => 'AR',
            program       => 'RAXMTR',
            description   => 'Autoinvoice master program from OM',
            start_time    => SYSDATE,
            sub_request   => NULL,
            argument1     => '1',
            argument2     => '-99',
            argument3     => TO_CHAR (l_source_id),
            argument4     => l_batch_source_name,
            argument5     => TO_CHAR (SYSDATE, 'YYYY/MM/DD HH24:MI:SS'),
            argument6     => NULL,
            argument7     => NULL,
            argument8     => NULL,
            argument9     => NULL,
            argument10    => NULL,
            argument11    => NULL,
            argument12    => NULL,
            argument13    => NULL,
            argument14    => NULL,
            argument15    => NULL,
            argument16    => NULL,
            argument17    => NULL,
            argument18    => i_sales_order,
            argument19    => i_sales_order,
            argument20    => NULL,
            argument21    => NULL,
            argument22    => NULL,
            argument23    => NULL,
            argument24    => NULL,
            argument25    => NULL,
            argument26    => 'Y',
            argument27    => NULL);

      IF l_request_id <> 0
      THEN
         o_return_message :=
               'Invoice Process submitted successfully with request id='
            || l_request_id
            || '. Do not press this button again until the process has completed. Monitor processing through the Requests Concurrent Manager form';
         COMMIT;
         RETURN;
      ELSE
         o_return_message := 'Issue submitting Invoice Process';
         ROLLBACK;
         RETURN;
      END IF;
      END IF;
      --Added for Ver 1.6 Ends
   END launch_auto_invoice;

   -- Function called from personalization the sales order from to restrict the
   -- message to appear only for those orders for which prepayment exists
   FUNCTION prepayment_exists (i_header_id IN NUMBER)
      RETURN VARCHAR
   IS
      l_header_id   NUMBER;
      l_line_id     NUMBER;
      l_exist       VARCHAR2 (1);
   BEGIN
      IF i_header_id IS NULL
      THEN
         RETURN ('N');
      ELSE
         BEGIN
            SELECT 'Y'
              INTO l_exist
              FROM oe_payments
             WHERE header_id = i_header_id AND ROWNUM = 1;

            RETURN ('Y');
         EXCEPTION
            WHEN OTHERS
            THEN
               RETURN ('N');
         END;
      END IF;
   END prepayment_exists;

   --
   -- Procedure registered as a concurrent program to process cash refunds on
   -- counter orders
   --
   PROCEDURE refund_co_cash (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_return_status   VARCHAR2 (1);
      l_msg_data        VARCHAR2 (2000);

      CURSOR c1
      IS
         SELECT a.ROWID, a.*
           FROM apps.xxwc_om_co_refunds_tbl a
          WHERE process_flag = 'N';
   BEGIN
      FOR c1_rec IN c1
      LOOP
         xxwc_ar_om_refund_pkg.cash_refund_process (
            i_header_id             => c1_rec.header_id,
            i_return_header_id      => c1_rec.header_id,
            i_cash_receipt_id       => c1_rec.cash_receipt_id,
            i_refund_amount         => c1_rec.refund_amount,
            i_check_refund_amount   => 0,
            i_payment_type_code     => c1_rec.payment_type_code,
            o_return_status         => l_return_status,
            o_return_message        => l_msg_data);

         IF l_return_status = 'S'
         THEN
            UPDATE apps.xxwc_om_co_refunds_tbl
               SET process_flag = 'Y', process_msg = NULL
             WHERE ROWID = c1_rec.ROWID;

            COMMIT;
         --dbms_output.put_line(l_return_status||'='||l_msg_data);
         ELSE
            ROLLBACK;

            UPDATE apps.xxwc_om_co_refunds_tbl
               SET process_msg = l_msg_data
             WHERE ROWID = c1_rec.ROWID;

            COMMIT;
         --dbms_output.put_line(l_return_status||'='||l_msg_data);
         END IF;
      END LOOP;

      FOR c2_rec
         IN (SELECT b.order_number, c.receipt_number, a.process_msg
               FROM apps.xxwc_om_co_refunds_tbl a,
                    apps.oe_order_headers b,
                    apps.ar_cash_receipts c
              WHERE     a.header_id = b.header_id
                    AND a.process_flag = 'N'
                    AND a.cash_receipt_id = c.cash_receipt_id)
      LOOP
         fnd_file.put_line (
            fnd_file.OUTPUT,
               'Order='
            || c2_Rec.order_number
            || ' Recipt='
            || c2_rec.receipt_number
            || ' Error='
            || c2_rec.process_msg);
      END LOOP;
   END refund_co_cash;
END xxwc_ar_om_refund_pkg;
/