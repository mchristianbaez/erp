CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ar_credit_profiles_pkg
AS
   /*************************************************************************
   *   $Header xxwc_ar_credit_profiles_pkg.pks $
   *   Module Name: xxwc update credit profiles
   *
   *   PURPOSE:   Procedure used to update profile classes and customer account
   *              for cash on the fly customers
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        10/09/2012  Shankar Hariharan       Initial Version
   *   1.1        10/17/2013  Shankar Hariharan       Create another cursor to update
   *                                                 all customers with COD profile
   *                                                 and credit class <> CSALE to CSALE
   *   1.2        10/31/2014   Maharajan Shunmugam   TMS#20140129-00081
   *   1.3        23/06/2015   Pattabhi Avula        TMS#20150310-00194 -- Added substring
   *												 for location and added commit 
   *   1.4        09/07/2015   Pattabhi Avula        TMS#20150814-00025 -- Added terms 
   *                                                 condition in cursor3  
   * ***************************************************************************/

   PROCEDURE update_customer_profile (
      errbuf                     OUT VARCHAR2,
      retcode                    OUT VARCHAR2,
      i_profile_class_id      IN     NUMBER,
      i_customer_class_code   IN     VARCHAR2,
      i_remit_to_code         IN     VARCHAR2)
   IS
      p_customer_profile_rec          HZ_CUSTOMER_PROFILE_V2PUB.customer_profile_rec_type;
      x_cust_account_profile_id       NUMBER;
      x_return_status                 VARCHAR2 (2000);
      x_msg_count                     NUMBER;
      x_msg_data                      VARCHAR2 (2000);
      p_obj_version_number            NUMBER;
      p_cust_account_rec              HZ_CUST_ACCOUNT_V2PUB.CUST_ACCOUNT_REC_TYPE;
      x_profile_class_id              NUMBER;
      l_account_name                  VARCHAR2 (2000);
      l_location_name                 VARCHAR2 (2000);
      l_party_name                    VARCHAR2 (2000);
      p_cust_site_use_rec             HZ_CUST_ACCOUNT_SITE_V2PUB.cust_site_use_rec_type;
      p_organization_rec              HZ_PARTY_V2PUB.ORGANIZATION_REC_TYPE;
      p_party_rec                     HZ_PARTY_V2PUB.PARTY_REC_TYPE;
      l_object_version_number         NUMBER;
      l_party_object_version_number   NUMBER;
      l_profile_id                    NUMBER;
      l_org_id                        NUMBER;
      l_sec          		      VARCHAR2 (200);
      l_msg               	      VARCHAR2 (150);
      l_distro_list       	      VARCHAR2 (75)   DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_req_id                        NUMBER := fnd_global.conc_request_id;

      CURSOR c1
      IS
         SELECT a.cust_account_profile_id,
                a.object_Version_number,
                b.account_number,
                a.standard_terms
           FROM hz_customer_profiles a, hz_cust_accounts b
          WHERE     a.profile_class_id = 0
                AND a.created_by_module = 'ONT_UI_ADD_CUSTOMER'
                AND a.cust_account_id = b.cust_account_id;


      CURSOR c2
      IS
         SELECT b.cust_account_id,
                b.account_number,
                b.creation_date,
                b.object_version_number,
                a.party_name,
                a.party_id,
                a.object_version_number party_object_version_number
           FROM hz_cust_accounts b, hz_parties a
          WHERE     b.created_by_module = 'ONT_UI_ADD_CUSTOMER'
                AND b.account_name IS NULL
                AND b.attribute4 IS NULL
                AND b.account_established_date IS NULL
                AND a.party_id = b.party_id;

      CURSOR c3
      IS
         SELECT a.cust_account_profile_id,
                a.object_Version_number,
                b.account_number,
				a.standard_terms  -- Added for ver#1.4
           FROM hz_customer_profiles a, hz_cust_accounts b
          WHERE     a.profile_class_id = NVL (i_profile_class_id, 1040)
                AND a.credit_classification <> 'CSALE'
                AND a.cust_account_id = b.cust_account_id;


      CURSOR c4 (
         p_account_id    NUMBER)                           --Added for ver#1.2
      IS
         SELECT hcsu.object_version_number,
                hcsu.site_use_id,
                hcsu.cust_acct_site_id,
                hcsu.location,
                hcsu.primary_salesrep_id
           FROM hz_cust_acct_sites hcas, hz_cust_site_uses hcsu
          WHERE     hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND hcas.cust_account_id = p_account_id;
   BEGIN

   SELECT   mo_global.get_current_org_id () INTO l_org_id FROM DUAL;

   fnd_file.put_line (
               fnd_file.LOG,
                  'Current MO Org_id=' 
               || l_org_id);

      --Initialize the apps
      --mo_global.init ('AR');
      mo_global.set_policy_context ('S', l_org_id);
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id,
                                  0);

      l_sec := 'Opening Cursor C1';
      FOR c1_rec IN c1
      LOOP
         p_customer_profile_rec.cust_account_profile_id :=
            c1_rec.cust_account_profile_id;
         p_customer_profile_rec.profile_class_id := i_profile_class_id;
         p_customer_profile_rec.attribute2 := i_remit_to_code;          --'2';
         p_customer_profile_rec.attribute3 := 'N';
         p_customer_profile_rec.account_status := 'ACTIVE';
         p_obj_version_number := c1_rec.object_version_number;

         IF c1_rec.standard_terms = 1019 --PFRDCASH                                        --Added for ver#1.4 <<START>>
         THEN
         p_customer_profile_rec.standard_terms := c1_rec.standard_terms;
         END IF;                                                     -- <<END>>

        BEGIN
         HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile (
            p_init_msg_list           => 'T',
            p_customer_profile_rec    => p_customer_profile_rec,
            p_object_version_number   => p_obj_version_number,
            x_return_status           => x_return_status,
            x_msg_count               => x_msg_count,
            x_msg_data                => x_msg_data);
			
			p_customer_profile_rec.standard_terms :=NULL;  --Added for ver#1.4
			
         IF x_return_status <> 'S'
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Issue updating Profile for customer '
               || c1_rec.account_number
               || ' '
               || x_msg_data);
            DBMS_OUTPUT.put_line (
                  'Issue updating Profile for customer '
               || c1_rec.account_number
               || ' '
               || x_msg_data);
         ELSE
            fnd_file.put_line (
               fnd_file.LOG,
                  'Successfully updated Profile for customer '
               || c1_rec.account_number
               || ' '
               || x_msg_data);
            DBMS_OUTPUT.put_line (
                  'Successfully updated Profile for customer '
               || c1_rec.account_number
               || ' '
               || x_msg_data);
         END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_msg := 'Error: ' || SQLERRM;
               fnd_file.put_line (
                  FND_FILE.LOG,
                  'When others exception in HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile ');
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_AR_CREDIT_PROFILES_PKG',
                  p_calling             => l_sec,
                  p_request_id          => l_req_id,
                  p_ora_error_msg       => l_msg,
                  p_error_desc          => 'Error running HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile WHEN OTHERS EXCEPTION',
                  p_distribution_list   => l_distro_list,
                  p_module              => 'AR');

         END;
      END LOOP;                                                           --C1

      l_sec := 'Opening Cursor C2';
      FOR c2_rec IN c2
      LOOP
         p_cust_account_rec.cust_account_id := c2_rec.cust_account_id;
         p_cust_account_rec.account_established_date := c2_rec.creation_date;

         --Added below for ver#1.2 <<Start>>
         IF    SUBSTR (UPPER (c2_rec.party_name), 1, 5) NOT LIKE 'CASH/%'
            OR SUBSTR (UPPER (c2_rec.party_name), 1, 6) NOT LIKE 'CASH /%'
         THEN
            SELECT REPLACE (
                      REPLACE (
                            'CASH/'
                         || TRIM (
                               REPLACE (
                                  REGEXP_REPLACE (UPPER (c2_rec.party_name),
                                                  '[/,%,;,*,?,\,]',
                                                  ' '),
                                  'CASH',
                                  '')),
                         '/.',
                         '/'),
                      '/-',
                      '/')
              INTO l_account_name
              FROM DUAL;

            p_cust_account_rec.account_name := l_account_name;
         ELSE
            p_cust_account_rec.account_name := c2_rec.party_name;
         END IF;

         --         p_cust_account_rec.account_name := c2_rec.party_name;


         p_cust_account_rec.attribute4 := 'WCI';
         p_cust_account_rec.customer_class_code := i_customer_class_code; --'GEN_CONTRACT';

         BEGIN
         HZ_CUST_ACCOUNT_V2PUB.update_cust_account (
            p_init_msg_list           => 'T',
            p_cust_account_rec        => p_cust_account_rec,
            p_object_version_number   => c2_rec.object_version_number,
            x_return_status           => x_return_status,
            x_msg_count               => x_msg_count,
            x_msg_data                => x_msg_data);

         IF x_return_status <> 'S'
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Issue updating Account for customer '
               || c2_rec.account_number
               || ' '
               || x_msg_data);
            DBMS_OUTPUT.put_line (
                  'Issue updating Account for customer '
               || c2_rec.account_number
               || ' '
               || x_msg_data);
         ELSE
            fnd_file.put_line (
               fnd_file.LOG,
                  'Successfully updated Account for customer '
               || c2_rec.account_number
               || ' '
               || x_msg_data);
            DBMS_OUTPUT.put_line (
                  'Successfully updated Account for customer '
               || c2_rec.account_number
               || ' '
               || x_msg_data);
         END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_msg := 'Error: ' || SQLERRM;
               fnd_file.put_line (
                  FND_FILE.LOG,
                  'When others exception in HZ_CUST_ACCOUNT_V2PUB.update_cust_account ');
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_AR_CREDIT_PROFILES_PKG',
                  p_calling             => l_sec,
                  p_request_id          => l_req_id,
                  p_ora_error_msg       => l_msg,
                  p_error_desc          => 'Error running HZ_CUST_ACCOUNT_V2PUB.update_cust_account WHEN OTHERS EXCEPTION',
                  p_distribution_list   => l_distro_list,
                  p_module              => 'AR');

         END;

         BEGIN
            p_party_rec.party_id := c2_rec.party_id;
            p_organization_rec.party_rec := p_party_rec;
            l_party_object_version_number :=
               c2_rec.party_object_version_number;

            IF    SUBSTR (UPPER (c2_rec.party_name), 1, 5) NOT LIKE 'CASH/%'
               OR SUBSTR (UPPER (c2_rec.party_name), 1, 6) NOT LIKE 'CASH /%'
            THEN
               SELECT REPLACE (
                         REPLACE (
                               'CASH/'
                            || TRIM (
                                  REPLACE (
                                     REGEXP_REPLACE (
                                        UPPER (c2_rec.party_name),
                                        '[/,%,;,*,?,\,]',
                                        ' '),
                                     'CASH',
                                     '')),
                            '/.',
                            '/'),
                         '/-',
                         '/')
                 INTO l_party_name
                 FROM DUAL;

               p_organization_rec.organization_name := l_party_name;
            ELSE
               p_organization_rec.organization_name := l_party_name;
            END IF;

            BEGIN
            HZ_PARTY_V2PUB.update_organization (
               p_init_msg_list                 => FND_API.G_FALSE,
               p_organization_rec              => p_organization_rec,
               p_party_object_version_number   => l_party_object_version_number,
               x_profile_id                    => l_profile_id,
               x_return_status                 => x_return_status,
               x_msg_count                     => x_msg_count,
               x_msg_data                      => x_msg_data);


            IF x_return_status <> 'S'
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Issue updating Organization name '
                  || c2_rec.party_name
                  || ' '
                  || x_msg_data);
               DBMS_OUTPUT.put_line (
                     'Issue updating Organization name  '
                  || c2_rec.party_name
                  || ' '
                  || x_msg_data);
            ELSE
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Successfully updated Organization name  '
                  || c2_rec.party_name
                  || ' '
                  || x_msg_data);
               DBMS_OUTPUT.put_line (
                     'Successfully updated Organization name  '
                  || c2_rec.party_name
                  || ' '
                  || x_msg_data);
            END IF;

           EXCEPTION
            WHEN OTHERS
            THEN
               l_msg := 'Error: ' || SQLERRM;
               fnd_file.put_line (
                  FND_FILE.LOG,
                  'When others exception in HZ_PARTY_V2PUB.update_organization  ');
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_AR_CREDIT_PROFILES_PKG',
                  p_calling             => l_sec,
                  p_request_id          => l_req_id,
                  p_ora_error_msg       => l_msg,
                  p_error_desc          => 'Error running HZ_PARTY_V2PUB.update_organization WHEN OTHERS EXCEPTION',
                  p_distribution_list   => l_distro_list,
                  p_module              => 'AR');
         END;

         END;

         l_sec := 'Opening Cursor C4';

         FOR c4_rec IN c4 (c2_rec.cust_account_id) --Added for ver#1.2 <<Start>>
         LOOP
            BEGIN
               p_cust_site_use_rec.site_use_id := c4_rec.site_use_id;
               p_cust_site_use_rec.cust_acct_site_id :=
                  c4_rec.cust_acct_site_id;
               --p_cust_site_use_rec.location       := c4_rec.location;
               l_object_version_number := c4_rec.object_version_number;

               IF    SUBSTR (UPPER (c4_rec.location), 1, 5) NOT LIKE 'CASH/%'
                  OR SUBSTR (UPPER (c4_rec.location), 1, 6) NOT LIKE
                        'CASH /%'
               THEN
                  SELECT SUBSTR(REPLACE (
                            REPLACE (
                                  'CASH/'
                               || TRIM (
                                     REPLACE (
                                        REGEXP_REPLACE (
                                           UPPER (c4_rec.location),
                                           '[/,%,;,*,?,\,]',
                                           ' '),
                                        'CASH',
                                        '')),
                               '/.',
                               '/'),
                            '/-',
                            '/'),1,40)  -- Added for Ver# 1.3
                    INTO l_location_name
                    FROM DUAL;

                  p_cust_site_use_rec.location := l_location_name;
               ELSE
                  p_cust_site_use_rec.location := c4_rec.location;
               END IF;

               BEGIN
               HZ_CUST_ACCOUNT_SITE_V2PUB.update_cust_site_use (
                  p_init_msg_list           => FND_API.G_FALSE,
                  p_cust_site_use_rec       => p_cust_site_use_rec,
                  p_object_version_number   => l_object_version_number,
                  x_return_status           => x_return_status,
                  x_msg_count               => x_msg_count,
                  x_msg_data                => x_msg_data);

               IF x_return_status <> 'S'
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG,
                        'Issue updating location '
                     || c4_rec.location
                     || ' '
                     || x_msg_data);
                  DBMS_OUTPUT.put_line (
                        'Issue updating location '
                     || c4_rec.location
                     || ' '
                     || x_msg_data);
               ELSE
                  fnd_file.put_line (
                     fnd_file.LOG,
                        'Successfully updated location '
                     || c4_rec.location
                     || ' '
                     || x_msg_data);
                  DBMS_OUTPUT.put_line (
                        'Successfully updated location '
                     || c4_rec.location
                     || ' '
                     || x_msg_data);
               END IF;
            EXCEPTION
            WHEN OTHERS THEN
               l_msg := 'Error: ' || SQLERRM;
               fnd_file.put_line (
                  FND_FILE.LOG,
                  'When others exception in HZ_CUST_ACCOUNT_SITE_V2PUB.update_cust_site_use');
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_AR_CREDIT_PROFILES_PKG',
                  p_calling             => l_sec,
                  p_request_id          => l_req_id,
                  p_ora_error_msg       => l_msg,
                  p_error_desc          => 'Error running HZ_CUST_ACCOUNT_SITE_V2PUB.update_cust_site_use WHEN OTHERS EXCEPTION',
                  p_distribution_list   => l_distro_list,
                  p_module              => 'AR');
         END;
            END;
         END LOOP;                                                        --C4
      END LOOP;                                                           --C2

      -- ADded by Shankar 21-Jan-2013 to update credit class for COD customers
      -- coming through interface from PRISM
       l_sec := 'Opening Cursor C3';
      FOR c3_rec IN c3
      LOOP
         p_customer_profile_rec.cust_account_profile_id :=
            c3_rec.cust_account_profile_id;
         p_customer_profile_rec.credit_classification := 'CSALE';
         p_obj_version_number := c3_rec.object_version_number;
		 
		 IF c3_rec.standard_terms = 1019 --PFRDCASH                                        --Added for ver#1.4 <<START>>
         THEN
 		   p_customer_profile_rec.standard_terms:=c3_rec.standard_terms;
         END IF;          --  <<END>>
		 
         BEGIN
         HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile (
            p_init_msg_list           => 'T',
            p_customer_profile_rec    => p_customer_profile_rec,
            p_object_version_number   => p_obj_version_number,
            x_return_status           => x_return_status,
            x_msg_count               => x_msg_count,
            x_msg_data                => x_msg_data);
			
			p_customer_profile_rec.standard_terms:=NULL;   --Added for ver#1.4

         IF x_return_status <> 'S'
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Issue updating Profile for customer '
               || c3_rec.account_number
               || ' '
               || x_msg_data);
            DBMS_OUTPUT.put_line (
                  'Issue updating Profile for customer '
               || c3_rec.account_number
               || ' '
               || x_msg_data);
         ELSE
            fnd_file.put_line (
               fnd_file.LOG,
                  'Successfully updated Profile for customer '
               || c3_rec.account_number
               || ' '
               || x_msg_data);
            DBMS_OUTPUT.put_line (
                  'Successfully updated Profile for customer '
               || c3_rec.account_number
               || ' '
               || x_msg_data);
         END IF;
            EXCEPTION
            WHEN OTHERS THEN
               l_msg := 'Error: ' || SQLERRM;
               fnd_file.put_line (
                  FND_FILE.LOG,
                  'When others exception in HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile');
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_AR_CREDIT_PROFILES_PKG',
                  p_calling             => l_sec,
                  p_request_id          => l_req_id,
                  p_ora_error_msg       => l_msg,
                  p_error_desc          => 'Error running HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile WHEN OTHERS EXCEPTION',
                  p_distribution_list   => l_distro_list,
                  p_module              => 'AR');
         END;
      END LOOP;
	  COMMIT;  -- Added for Ver# 1.3
   END update_customer_profile;
END xxwc_ar_credit_profiles_pkg;
/