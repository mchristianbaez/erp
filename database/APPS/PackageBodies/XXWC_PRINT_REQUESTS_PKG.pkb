CREATE OR REPLACE 
PACKAGE BODY      APPS.XXWC_PRINT_REQUESTS_PKG AS 


/******************************************************************************
   NAME:       XXWC_PRINT_REQUESTS_PKG

   PURPOSE:    Package to truncate data from the Printer Form tables that are no longer needed.  
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        29-JUL-2013  Lee Spitzer       1. Create the PL/SQL Package
******************************************************************************/

  /*************************************************************************
 *   Procedure : Write_Log
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/
   -- add debug message to log table and concurrent log file
   PROCEDURE Write_Log (p_debug_level   IN NUMBER,
                        p_mod_name      IN VARCHAR2,
                        p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;
      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      --DBMS_OUTPUT.put_line (P_DEBUG_MSG);
      COMMIT;
   END Write_Log;

  /*************************************************************************
 *   Procedure : Write_Error
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_Error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT G_PACKAGE_NAME ;
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START' ;
      l_distro_list VARCHAR2 (75)
            DEFAULT 'HDSOracleDevelopers@hdsupply.com'  ;
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running '|| G_PACKAGE_NAME ||' with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'INV'
      );
   END Write_Error;

   /*************************************************************************
    *   Procedure : Write_output
    *
    *   PURPOSE:   This procedure logs message into concurrent output
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;

   /*************************************************************************
    *   Procedure : Write_log_output
    *
    *   PURPOSE:   This procedure logs message into concurrent logfile
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/

   PROCEDURE Write_log_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
   END write_log_output;

/*PROCEDURE DELETE_TEMP_TABLES
    No parameters required
    This program truncates the tables xxwc.xxwc_print_requests_temp and xxwc.xxwc_print_requests_arg_temp tables
    Tables should be purged weekly on Sundays   
*/   
  PROCEDURE  DELETE_TEMP_TABLES
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ) IS
    
  l_count     NUMBER;
              
BEGIN

 
      G_NAME := 'DELETE_TEMP_TABLES';
      G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;
  
      write_log_output('******************************************************************');
      write_log_output('Starting Program '||G_PROGRAM_NAME);
      write_log_output('******************************************************************');


    
    BEGIN
    
        SELECT count(*)
        INTO   l_count
        FROM   xxwc.xxwc_print_requests_temp;
    EXCEPTION
        WHEN OTHERS THEN
            g_message := 'Could not count records on XXWC.XXWC_PRINT_REQUESTS_TEMP ' || SQLCODE || SQLERRM;
            raise g_exception;
    END;
        
    
    write_log_output('XXWC.XXWC_PRINT_REQUESTS_TEMP number of records ' || l_count);
    
    --if there are records in the backup table, then truncate those records
    IF l_count > 0 THEN
        
      write_log_output('Truncating table XXWC.XXWC_PRINT_REQUESTS_TEMP');
        
        BEGIN      
            EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_PRINT_REQUESTS_TEMP';
        EXCEPTION
            WHEN OTHERS THEN
              g_message := 'Could not truncate XXWC.XXWC_PRINT_REQUESTS_TEMP ' || SQLCODE || SQLERRM;
              raise g_exception;
        END;
        
      write_log_output('Truncated table XXWC.XXWC_PRINT_REQUESTS_TEMP');
    
    END IF;
  
    BEGIN
    
        SELECT count(*)
        INTO   l_count
        FROM   xxwc.xxwc_print_requests_arg_temp;
    EXCEPTION
        WHEN OTHERS THEN
            g_message := 'Could not count records on XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP ' || SQLCODE || SQLERRM;
            raise g_exception;
    END;
        
    
    write_log_output('XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP number of records ' || l_count);
    
    --if there are records in the backup table, then truncate those records
    IF l_count > 0 THEN
        
      write_log_output('Truncating table XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP');
        
        BEGIN      
            EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP';
        EXCEPTION
            WHEN OTHERS THEN
              g_message := 'Could not truncate XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP ' || SQLCODE || SQLERRM;
              raise g_exception;
        END;
        
      write_log_output('Truncated table XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP');
    
    END IF;
  
  
  retcode :=0 ;
  
  
EXCEPTION

    WHEN g_exception THEN
    
        write_log_output(g_message);
        retcode :=2;
  
END DELETE_TEMP_TABLES;
              
END XXWC_PRINT_REQUESTS_PKG;