CREATE OR REPLACE PACKAGE BODY  APPS.XXWC_INV_ITEM_AVGCOST_UPD_PKG
AS
   /*****************************************************************************************************************************************
   -- File Name: XXWC_INV_ITEM_AVGCOST_UPD_PKG.pkb
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: To update Average cost information for items.
   --
   -- HISTORY
   -- ========================================================================================================================================
   -- ========================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------------------------------------
   -- 1.0     11-SEP-2015   P.vamshidhar    TMS#20150609-00199 - Cost - Create mass update tool for average cost updates
   --                                       Initial Version.
   -- 1.1     02-Mar-2018   A.Pattabhi    TMS#20171024-00015 - Initial Version

   ******************************************************************************************************************************************/


   PROCEDURE IMPORT_ADI_DATA_PRC (p_org_code    IN VARCHAR2,
                                  p_account     IN VARCHAR2,
                                  p_Item_num    IN VARCHAR2,
                                  p_cost_type   IN VARCHAR2,
                                  p_new_cost    IN NUMBER)
   IS
      /******************************************************************************************************************************************
        PROCEDURE : IMPORT_ADI_DATA_PRC

        REVISIONS:
        Ver        Date            Author                     Description
        ---------  -----------    ---------------    --------------------------------------------------------------------------------------
        1.0        11-SEP-2015    P.vamshidhar       TMS#20150609-00199 - Cost - Create mass update tool for average cost updates
                                                     Initial Version.
      ******************************************************************************************************************************************/
      l_transaction_uom               MTL_TRANSACTIONS_INTERFACE.TRANSACTION_UOM%TYPE;
      l_tran_type_id                  MTL_TRANSACTIONS_INTERFACE.TRANSACTION_TYPE_ID%TYPE;
      l_mast_org_id                   MTL_PARAMETERS.ORGANIZATION_ID%TYPE;
      l_org_id                        MTL_PARAMETERS.ORGANIZATION_ID%TYPE;
      l_user_id                       FND_USER.USER_ID%TYPE;
      l_trans_source_type_id          NUMBER := 24;
      l_item_id                       MTL_SYSTEM_ITEMS_B.INVENTORY_ITEM_ID%TYPE;
      l_transaction_source_id         NUMBER := 24;
      l_return_status                 VARCHAR2 (100);
      l_msg_count                     NUMBER;
      l_msg_data                      VARCHAR2 (1000);
      l_trans_count                   NUMBER;
      ln_retcode                      VARCHAR2 (1000);
      l_account_period                ORG_ACCT_PERIODS.ACCT_PERIOD_ID%TYPE;
      ln_cost_group_id                mtl_parameters.default_cost_group_id%TYPE;
      ln_material_account             GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_material_overhead_account    GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_resource_account             GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_overhead_account             GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_outside_processing_account   GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      l_cost_element_id               CST_COST_ELEMENTS.COST_ELEMENT_ID%TYPE;
      l_tran_interface_id             MTL_TRANSACTIONS_INTERFACE.TRANSACTION_INTERFACE_ID%TYPE;
      l_transaction_header_id_ex      MTL_TRANSACTIONS_INTERFACE.TRANSACTION_HEADER_ID%TYPE;
      l_login_id                      MTL_TRANSACTIONS_INTERFACE.LAST_UPDATE_LOGIN%TYPE;
      l_level_type                    MTL_CST_TXN_COST_DETAILS.LEVEL_TYPE%TYPE;
      l_primary_uom_code              MTL_SYSTEM_ITEMS_B.PRIMARY_UOM_CODE%TYPE;
      l_error_mess                    VARCHAR2 (1000);
      ln_new_ccid                     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_old_ccid                     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      l_acc_flag                      VARCHAR2 (1);
      l_cost_type                     CST_COST_ELEMENTS.COST_ELEMENT%TYPE;
      l_new_cost                      NUMBER;
      l_mate_over_cost                NUMBER;

      bne_exception                   EXCEPTION;
   BEGIN
      -- Initializing Variables
      l_login_id := FND_GLOBAL.LOGIN_ID;
      l_user_id := FND_GLOBAL.USER_ID;
      l_mast_org_id := FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');

      l_level_type := '1'; --  Level Type defaulted to 1 (1 means --> 'This' as per MFG_LOOKUPS)

      -- Input Variables Validation
      -- Organization Code Validation

      BEGIN
         SELECT organization_id,
                default_cost_group_id,
                material_account,
                material_overhead_account,
                resource_account,
                overhead_account,
                outside_processing_account
           INTO l_org_id,
                ln_cost_group_id,
                ln_material_account,
                ln_material_overhead_account,
                ln_resource_account,
                ln_overhead_account,
                ln_outside_processing_account
           FROM apps.mtl_parameters
          WHERE organization_code = p_org_code;

         l_error_mess := NULL;

         IF l_org_id IS NULL
         THEN
            l_error_mess := 'Invalid Organization Code';
         ELSIF ln_cost_group_id IS NULL
         THEN
            l_error_mess := 'Invalid Organization Cost Group';
         ELSIF (   ln_material_account IS NULL
                OR ln_material_overhead_account IS NULL
                OR ln_resource_account IS NULL
                OR ln_overhead_account IS NULL
                OR ln_outside_processing_account IS NULL)
         THEN
            l_error_mess :=
               'Material/Overhead/.. Account information missed at Org Level';
         END IF;

         IF l_error_mess IS NOT NULL
         THEN
            RAISE bne_exception;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_mess := 'Organization Code Not Found/Valid';
            RAISE bne_exception;
         WHEN OTHERS
         THEN
            l_error_mess :=
                  ' Error occured while Deriving Org Info: '
               || SUBSTR (SQLERRM, 1, 100);
            RAISE bne_exception;
      END;

      -- Input Cost Amount Validation

      IF NVL (p_new_cost, 0) = 0
      THEN
         l_error_mess := 'Invalid Cost Amount ';
         RAISE bne_exception;
      END IF;

      -- Item Number Validation

      BEGIN
         SELECT inventory_item_id, primary_uom_code
           INTO l_item_id, l_primary_uom_code
           FROM APPS.MTL_SYSTEM_ITEMS_B
          WHERE     SEGMENT1 = p_Item_num
                AND ORGANIZATION_ID = l_org_id
                AND ENABLED_FLAG = 'Y'
                AND NVL (END_DATE_ACTIVE, SYSDATE + 1) >= SYSDATE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_mess :=
               'Item Not Found/Valid (For Org Code ' || p_org_code || ' )';
            RAISE bne_exception;
         WHEN OTHERS
         THEN
            l_error_mess :=
                  'Error Occured while Deriving Item Info: '
               || SUBSTR (SQLERRM, 1, 100);
            RAISE bne_exception;
      END;

      -- Deriving Cost_type and Material cost.
      IF p_cost_type = 'Average Cost'
      THEN
         l_cost_type := 'Material';

         BEGIN
            SELECT MATERIAL_OVERHEAD_COST
              INTO l_mate_over_cost
              FROM APPS.CST_ITEM_COST_TYPE_V
             WHERE     INVENTORY_ITEM_ID = l_item_id
                   AND ORGANIZATION_ID = l_org_id
                   AND cost_type = 'Average';

            l_new_cost := NVL (p_new_cost, 0) - NVL (l_mate_over_cost, 0);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_new_cost := p_new_cost;
            WHEN OTHERS
            THEN
               l_new_cost := p_new_cost;
         END;
      ELSE
         l_cost_type := p_cost_type;
         l_new_cost := p_new_cost;
      END IF;

      -- Costing Element Validation
      BEGIN
         l_error_mess := NULL;

         SELECT cost_element_id
           INTO l_cost_element_id
           FROM CST_COST_ELEMENTS
          WHERE UPPER (COST_ELEMENT) = UPPER (l_cost_type);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_mess := l_cost_type || ' Cost Type Not Found/Valid';
            RAISE bne_exception;
         WHEN OTHERS
         THEN
            l_error_mess :=
                  'Error Occured While Deriving Cost Element: '
               || SUBSTR (SQLERRM, 1, 100);
            RAISE bne_exception;
      END;

      -- Account Validation

      BEGIN
         l_acc_flag := 'N';
         l_error_mess := NULL;

         SELECT 'Y'
           INTO l_acc_flag
           FROM APPS.FND_FLEX_VALUES_VL FFVV, APPS.FND_FLEX_VALUE_SETS FFVS
          WHERE     FFVV.FLEX_VALUE_SET_ID = FFVS.FLEX_VALUE_SET_ID
                AND FFVS.FLEX_VALUE_SET_NAME = 'XXCUS_GL_ACCOUNT'
                AND FFVV.ENABLED_FLAG = 'Y'
                AND NVL (FFVV.END_DATE_ACTIVE, SYSDATE + 1) >= SYSDATE
                AND FFVV.FLEX_VALUE = p_account;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_acc_flag := 'N';
            l_error_mess :=
               p_account || ' Account Not Defined/Valid in XXCUS_GL_ACCOUNT';
            RAISE bne_exception;
         WHEN OTHERS
         THEN
            l_acc_flag := 'N';
            l_error_mess :=
                  'Error Occured while Deriving '
               || p_account
               || ' Account Info: '
               || SUBSTR (SQLERRM, 1, 100);
            RAISE bne_exception;
      END;

      -- Deriving Account Combination

      IF UPPER (l_cost_type) = 'MATERIAL'
      THEN
         ln_old_ccid := ln_material_account;
      ELSIF UPPER (l_cost_type) = 'MATERIAL OVERHEAD'
      THEN
         ln_old_ccid := ln_material_overhead_account;
      ELSIF UPPER (l_cost_type) = 'RESOURCE'
      THEN
         ln_old_ccid := ln_resource_account;
      ELSIF UPPER (l_cost_type) = 'OUTSIDE PROCESSING'
      THEN
         ln_old_ccid := ln_outside_processing_account;
      ELSIF UPPER (l_cost_type) = 'OVERHEAD'
      THEN
         ln_old_ccid := ln_overhead_account;
      END IF;


      IF l_acc_flag = 'Y'
      THEN
         BEGIN
            l_error_mess := NULL;

            SELECT GCC1.CODE_COMBINATION_ID
              INTO ln_new_ccid
              FROM APPS.GL_CODE_COMBINATIONS_KFV GCC1,
                   (SELECT    A.SEGMENT1
                           || '.'
                           || A.SEGMENT2
                           || '.'
                           || A.SEGMENT3
                           || '.'
                           || p_account
                           || '.'
                           || A.SEGMENT5
                           || '.'
                           || A.SEGMENT6
                           || '.'
                           || A.SEGMENT7
                              AS NEW_ACCOUNT
                      FROM APPS.GL_CODE_COMBINATIONS_KFV A
                     WHERE     CODE_COMBINATION_ID = ln_old_ccid
                           AND A.ENABLED_FLAG = 'Y') GCC2
             WHERE     GCC1.CONCATENATED_SEGMENTS = GCC2.NEW_ACCOUNT
                   AND GCC1.ENABLED_FLAG = 'Y';


            IF UPPER (l_cost_type) = 'MATERIAL'
            THEN
               ln_material_account := ln_new_ccid;
            ELSIF UPPER (l_cost_type) = 'MATERIAL OVERHEAD'
            THEN
               ln_material_overhead_account := ln_new_ccid;
            ELSIF UPPER (l_cost_type) = 'RESOURCE'
            THEN
               ln_resource_account := ln_new_ccid;
            ELSIF UPPER (l_cost_type) = 'OUTSIDE PROCESSING'
            THEN
               ln_outside_processing_account := ln_new_ccid;
            ELSIF UPPER (l_cost_type) = 'OVERHEAD'
            THEN
               ln_overhead_account := ln_new_ccid;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_error_mess :=
                     'Account combination not exists with Provided Account '
                  || p_account;
               RAISE bne_exception;
            WHEN OTHERS
            THEN
               l_error_mess :=
                     'Error Occured while deriving Account Combination: '
                  || SUBSTR (SQLERRM, 1, 100);
               RAISE bne_exception;
         END;
      END IF;

      -- Deriving Tranaction Type Id

      BEGIN
         l_error_mess := NULL;

         SELECT TRANSACTION_TYPE_ID
           INTO l_tran_type_id
           FROM APPS.MTL_TRANSACTION_TYPES
          WHERE TRANSACTION_TYPE_NAME = 'Average cost update';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_mess := 'Average cost update Transaction Type Not Found';
            RAISE bne_exception;
         WHEN OTHERS
         THEN
            l_error_mess :=
                  'Error Occured While Deriving Transaction Type: '
               || SUBSTR (SQLERRM, 1, 100);
            RAISE bne_exception;
      END;

      -- Deriving Account Period

      BEGIN
         l_error_mess := NULL;

         SELECT acct_period_id
           INTO l_account_period
           FROM ORG_ACCT_PERIODS_V
          WHERE     ORGANIZATION_ID = l_org_id
                AND TRUNC (SYSDATE) BETWEEN START_DATE AND END_DATE
                AND REC_TYPE = 'ORG_PERIOD'
                AND STATUS = 'Open';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_mess := 'Transaction Date Period Not Opened/Found ';
            RAISE bne_exception;
         WHEN OTHERS
         THEN
            l_error_mess :=
                  'Error Occured While Deriving Priod: '
               || SUBSTR (SQLERRM, 1, 100);
            RAISE bne_exception;
      END;

      l_transaction_header_id_ex :=
         TO_CHAR (l_org_id) || TO_CHAR (SYSDATE, 'HH24MISS');

      l_tran_interface_id := mtl_material_transactions_s.NEXTVAL;

      -- Inserting into Transactions Interface Table
      BEGIN
         l_error_mess := NULL;

         INSERT INTO MTL_TRANSACTIONS_INTERFACE (source_code,
                                                 process_flag,
                                                 transaction_mode,
                                                 transaction_uom,
                                                 transaction_type_id,
                                                 created_by,
                                                 last_updated_by,
                                                 organization_id,
                                                 transaction_date,
                                                 new_average_cost,
                                                 inventory_item_id,
                                                 source_header_id,
                                                 source_line_id,
                                                 transaction_quantity,
                                                 transaction_source_id,
                                                 transaction_source_type_id,
                                                 creation_date,
                                                 last_update_date,
                                                 last_update_login,
                                                 transaction_interface_id,
                                                 transaction_header_id,
                                                 material_account,
                                                 cost_group_id,
                                                 acct_period_id,
                                                 material_overhead_account,
                                                 resource_account,
                                                 overhead_account,
                                                 outside_processing_account)
              VALUES ('ACU',                            -- transaction_source,
                      1,                                    -- l_process_flag,
                      3,                                -- l_transaction_mode,
                      l_primary_uom_code,                   -- transaction_uom
                      l_tran_type_id,
                      l_user_id,                                 -- created_by
                      l_user_id,                            -- last_updated_by
                      l_org_id,                             -- organization_id
                      SYSDATE,                             -- transaction_date
                      l_new_cost,                          -- new_average_cost
                      l_item_id,                          -- inventory_item_id
                      1,                                   -- source_header_id
                      1,                                     -- source_line_id
                      0,                               -- transaction_quantity
                      l_transaction_source_id,        -- transaction_source_id
                      l_trans_source_type_id,    -- transaction_source_type_id
                      SYSDATE,                                -- creation_date
                      SYSDATE,                             -- last_update_date
                      l_login_id,                                  -- Login id
                      l_tran_interface_id,         -- transaction_interface_id
                      l_transaction_header_id_ex,
                      ln_material_account,
                      ln_cost_group_id,
                      l_account_period,
                      ln_material_overhead_account,
                      ln_resource_account,
                      ln_overhead_account,
                      ln_outside_processing_account);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_mess :=
                  'Error Occured While Inserting Data into Tran Intf Table: '
               || SUBSTR (SQLERRM, 1, 100);
            RAISE bne_exception;
      END;


      -- Inserting into Transactions Cost Interface Table

      BEGIN
         INSERT INTO MTL_TXN_COST_DET_INTERFACE (transaction_interface_id,
                                                 last_update_date,
                                                 last_updated_by,
                                                 creation_date,
                                                 created_by,
                                                 last_update_login,
                                                 organization_id,
                                                 cost_element_id,
                                                 level_type,
                                                 new_average_cost)
              VALUES (l_tran_interface_id,
                      SYSDATE,
                      l_user_id,
                      SYSDATE,
                      l_user_id,
                      l_login_id,
                      l_org_id,
                      l_cost_element_id,
                      l_level_type,
                      l_new_cost);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_mess :=
                  'Error Occured While Inserting Data into Cost Intf Table: '
               || SUBSTR (SQLERRM, 1, 100);
            RAISE bne_exception;
      END;

      COMMIT;

      -- Submitting Transaction Processor
      ln_retcode :=
         INV_TXN_MANAGER_PUB.process_transactions (
            p_api_version     => 1.0,
            p_init_msg_list   => fnd_api.g_true,
            x_return_status   => l_return_status,
            x_msg_count       => l_msg_count,
            x_msg_data        => l_msg_data,
            x_trans_count     => l_trans_count,
            p_header_id       => l_transaction_header_id_ex);

      COMMIT;

      IF l_return_status = 'E'
      THEN
         SELECT ERROR_EXPLANATION
           INTO l_error_mess
           FROM APPS.MTL_TRANSACTIONS_INTERFACE
          WHERE TRANSACTION_INTERFACE_ID = l_tran_interface_id;

         l_error_mess :=
               'Transaction Import Failed due to '
            || SUBSTR (l_error_mess, 1, 100);
         RAISE bne_exception;
      END IF;
   EXCEPTION
      WHEN bne_exception
      THEN
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_mess);
         ROLLBACK;
      WHEN OTHERS
      THEN
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_mess);

         ROLLBACK;
   END;
   
PROCEDURE AVG_COST_UPDATE_PRC(p_org_code    IN VARCHAR2,
                                  p_account     IN VARCHAR2,
                                  p_Item_num    IN VARCHAR2,
                                  p_cost_type   IN VARCHAR2,
                                  p_new_cost    IN NUMBER,
                                  o_ret_msg     OUT VARCHAR2)  
   IS
      /******************************************************************************************************************************************
        PROCEDURE : XXWC_AVG_COST_UPDATE_FUNC

        REVISIONS:
        Ver        Date            Author                     Description
        ---------  -----------    ---------------    --------------------------------------------------------------------------------------
        1.1        02-Mar-2018    A.Pattabhi        Initial Version - TMS#20171024-00015
      ******************************************************************************************************************************************/
      l_transaction_uom               MTL_TRANSACTIONS_INTERFACE.TRANSACTION_UOM%TYPE;
      l_tran_type_id                  MTL_TRANSACTIONS_INTERFACE.TRANSACTION_TYPE_ID%TYPE;
      l_mast_org_id                   MTL_PARAMETERS.ORGANIZATION_ID%TYPE;
      l_org_id                        MTL_PARAMETERS.ORGANIZATION_ID%TYPE;
      l_user_id                       FND_USER.USER_ID%TYPE;
      l_trans_source_type_id          NUMBER := 24;
      l_item_id                       MTL_SYSTEM_ITEMS_B.INVENTORY_ITEM_ID%TYPE;
      l_transaction_source_id         NUMBER := 24;
      l_return_status                 VARCHAR2 (100);
      l_msg_count                     NUMBER;
      l_msg_data                      VARCHAR2 (1000);
      l_trans_count                   NUMBER;
      ln_retcode                      VARCHAR2 (1000);
      l_account_period                ORG_ACCT_PERIODS.ACCT_PERIOD_ID%TYPE;
      ln_cost_group_id                mtl_parameters.default_cost_group_id%TYPE;
      ln_material_account             GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_material_overhead_account    GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_resource_account             GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_overhead_account             GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_outside_processing_account   GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      l_cost_element_id               CST_COST_ELEMENTS.COST_ELEMENT_ID%TYPE;
      l_tran_interface_id             MTL_TRANSACTIONS_INTERFACE.TRANSACTION_INTERFACE_ID%TYPE;
      l_transaction_header_id_ex      MTL_TRANSACTIONS_INTERFACE.TRANSACTION_HEADER_ID%TYPE;
      l_login_id                      MTL_TRANSACTIONS_INTERFACE.LAST_UPDATE_LOGIN%TYPE;
      l_level_type                    MTL_CST_TXN_COST_DETAILS.LEVEL_TYPE%TYPE;
      l_primary_uom_code              MTL_SYSTEM_ITEMS_B.PRIMARY_UOM_CODE%TYPE;
      l_error_mess                    VARCHAR2 (1000);
      ln_new_ccid                     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_old_ccid                     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      l_acc_flag                      VARCHAR2 (1);
      l_cost_type                     CST_COST_ELEMENTS.COST_ELEMENT%TYPE;
      l_new_cost                      NUMBER;
      l_mate_over_cost                NUMBER;

 
   BEGIN
      -- Initializing Variables
      l_login_id := FND_GLOBAL.LOGIN_ID;
      l_user_id := FND_GLOBAL.USER_ID;
      l_mast_org_id := FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');

      l_level_type := '1'; --  Level Type defaulted to 1 (1 means --> 'This' as per MFG_LOOKUPS)

      -- Input Variables Validation
      -- Organization Code Validation

      BEGIN
         SELECT organization_id,
                default_cost_group_id,
                material_account,
                material_overhead_account,
                resource_account,
                overhead_account,
                outside_processing_account
           INTO l_org_id,
                ln_cost_group_id,
                ln_material_account,
                ln_material_overhead_account,
                ln_resource_account,
                ln_overhead_account,
                ln_outside_processing_account
           FROM apps.mtl_parameters
          WHERE organization_code = p_org_code;

         IF l_org_id IS NULL
         THEN
            o_ret_msg := 'Invalid Organization Code';
         ELSIF ln_cost_group_id IS NULL
         THEN
            o_ret_msg := 'Invalid Organization Cost Group';
         ELSIF (   ln_material_account IS NULL
                OR ln_material_overhead_account IS NULL
                OR ln_resource_account IS NULL
                OR ln_overhead_account IS NULL
                OR ln_outside_processing_account IS NULL)
         THEN
            o_ret_msg :=
               'Material/Overhead/.. Account information missed at Org Level';
         END IF;

         IF o_ret_msg IS NOT NULL
         THEN
            o_ret_msg := l_error_mess;
            GOTO LAST_STEP;            
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            o_ret_msg := 'Organization Code Not Found/Valid';
             GOTO LAST_STEP;                          
         WHEN OTHERS
         THEN
            o_ret_msg :=
                  ' Error occured while Deriving Org Info: '
               || SUBSTR (SQLERRM, 1, 100);
            GOTO LAST_STEP;
      END;

      -- Input Cost Amount Validation

      IF NVL (p_new_cost, 0) = 0
      THEN
         o_ret_msg := 'Invalid Cost Amount ';
         GOTO LAST_STEP;
      END IF;

      -- Item Number Validation

      BEGIN
         SELECT inventory_item_id, primary_uom_code
           INTO l_item_id, l_primary_uom_code
           FROM APPS.MTL_SYSTEM_ITEMS_B
          WHERE     SEGMENT1 = p_Item_num
                AND ORGANIZATION_ID = l_org_id
                AND ENABLED_FLAG = 'Y'
                AND NVL (END_DATE_ACTIVE, SYSDATE + 1) >= SYSDATE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            o_ret_msg :=
               'Item Not Found/Valid (For Org Code ' || p_org_code || ' )';
         GOTO LAST_STEP;
         WHEN OTHERS
         THEN
            o_ret_msg :=
                  'Error Occured while Deriving Item Info: '
               || SUBSTR (SQLERRM, 1, 100);
         GOTO LAST_STEP;
      END;

      -- Deriving Cost_type and Material cost.
      IF p_cost_type = 'Average Cost'
      THEN
         l_cost_type := 'Material';

         BEGIN
            /*SELECT MATERIAL_OVERHEAD_COST
              INTO l_mate_over_cost
              FROM APPS.CST_ITEM_COST_TYPE_V
             WHERE     INVENTORY_ITEM_ID = l_item_id
                   AND ORGANIZATION_ID = l_org_id
                   AND cost_type = 'Average'; */
		    SELECT material_cost
              INTO l_mate_over_cost
              FROM APPS.CST_ITEM_COST_TYPE_V
             WHERE INVENTORY_ITEM_ID = l_item_id
               AND MATERIAL_COST IS NOT NULL
               AND ORGANIZATION_ID = l_org_id
               AND cost_type = 'Average'; 

            l_new_cost := NVL (p_new_cost, 0) - NVL (l_mate_over_cost, 0);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_new_cost := p_new_cost;
            WHEN OTHERS
            THEN
               l_new_cost := p_new_cost;
         END;
      ELSE
         l_cost_type := p_cost_type;
         l_new_cost := p_new_cost;
      END IF;

      -- Costing Element Validation
      BEGIN
         l_error_mess := NULL;

         SELECT cost_element_id
           INTO l_cost_element_id
           FROM CST_COST_ELEMENTS
          WHERE UPPER (COST_ELEMENT) = UPPER (l_cost_type);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            o_ret_msg:= l_cost_type || ' Cost Type Not Found/Valid';
            
            GOTO LAST_STEP;
         WHEN OTHERS
         THEN
            o_ret_msg:=
                  'Error Occured While Deriving Cost Element: '
               || SUBSTR (SQLERRM, 1, 100);
            GOTO LAST_STEP;
      END;

      -- Account Validation

      BEGIN
         l_acc_flag := 'N';
         l_error_mess := NULL;

         SELECT 'Y'
           INTO l_acc_flag
           FROM APPS.FND_FLEX_VALUES_VL FFVV, APPS.FND_FLEX_VALUE_SETS FFVS
          WHERE     FFVV.FLEX_VALUE_SET_ID = FFVS.FLEX_VALUE_SET_ID
                AND FFVS.FLEX_VALUE_SET_NAME = 'XXCUS_GL_ACCOUNT'
                AND FFVV.ENABLED_FLAG = 'Y'
                AND NVL (FFVV.END_DATE_ACTIVE, SYSDATE + 1) >= SYSDATE
                AND FFVV.FLEX_VALUE = p_account;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_acc_flag := 'N';
            o_ret_msg :=
               p_account || ' Account Not Defined/Valid in XXCUS_GL_ACCOUNT';
            GOTO LAST_STEP;
         WHEN OTHERS
         THEN
            l_acc_flag := 'N';
            o_ret_msg :=
                  'Error Occured while Deriving '
               || p_account
               || ' Account Info: '
               || SUBSTR (SQLERRM, 1, 100);
            GOTO LAST_STEP;
      END;

      -- Deriving Account Combination

      IF UPPER (l_cost_type) = 'MATERIAL'
      THEN
         ln_old_ccid := ln_material_account;
      ELSIF UPPER (l_cost_type) = 'MATERIAL OVERHEAD'
      THEN
         ln_old_ccid := ln_material_overhead_account;
      ELSIF UPPER (l_cost_type) = 'RESOURCE'
      THEN
         ln_old_ccid := ln_resource_account;
      ELSIF UPPER (l_cost_type) = 'OUTSIDE PROCESSING'
      THEN
         ln_old_ccid := ln_outside_processing_account;
      ELSIF UPPER (l_cost_type) = 'OVERHEAD'
      THEN
         ln_old_ccid := ln_overhead_account;
      END IF;


      IF l_acc_flag = 'Y'
      THEN
         BEGIN
            l_error_mess := NULL;

            SELECT GCC1.CODE_COMBINATION_ID
              INTO ln_new_ccid
              FROM APPS.GL_CODE_COMBINATIONS_KFV GCC1,
                   (SELECT    A.SEGMENT1
                           || '.'
                           || A.SEGMENT2
                           || '.'
                           || A.SEGMENT3
                           || '.'
                           || p_account
                           || '.'
                           || A.SEGMENT5
                           || '.'
                           || A.SEGMENT6
                           || '.'
                           || A.SEGMENT7
                              AS NEW_ACCOUNT
                      FROM APPS.GL_CODE_COMBINATIONS_KFV A
                     WHERE     CODE_COMBINATION_ID = ln_old_ccid
                           AND A.ENABLED_FLAG = 'Y') GCC2
             WHERE     GCC1.CONCATENATED_SEGMENTS = GCC2.NEW_ACCOUNT
                   AND GCC1.ENABLED_FLAG = 'Y';


            IF UPPER (l_cost_type) = 'MATERIAL'
            THEN
               ln_material_account := ln_new_ccid;
            ELSIF UPPER (l_cost_type) = 'MATERIAL OVERHEAD'
            THEN
               ln_material_overhead_account := ln_new_ccid;
            ELSIF UPPER (l_cost_type) = 'RESOURCE'
            THEN
               ln_resource_account := ln_new_ccid;
            ELSIF UPPER (l_cost_type) = 'OUTSIDE PROCESSING'
            THEN
               ln_outside_processing_account := ln_new_ccid;
            ELSIF UPPER (l_cost_type) = 'OVERHEAD'
            THEN
               ln_overhead_account := ln_new_ccid;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               o_ret_msg :=
                     'Account combination not exists with Provided Account '
                  || p_account;
               GOTO LAST_STEP;
            WHEN OTHERS
            THEN
               o_ret_msg :=
                     'Error Occured while deriving Account Combination: '
                  || SUBSTR (SQLERRM, 1, 100);
               GOTO LAST_STEP;
         END;
      END IF;

      -- Deriving Tranaction Type Id

      BEGIN
         l_error_mess := NULL;

         SELECT TRANSACTION_TYPE_ID
           INTO l_tran_type_id
           FROM APPS.MTL_TRANSACTION_TYPES
          WHERE TRANSACTION_TYPE_NAME = 'Average cost update';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            o_ret_msg := 'Average cost update Transaction Type Not Found';
            GOTO LAST_STEP;
         WHEN OTHERS
         THEN
            o_ret_msg :=
                  'Error Occured While Deriving Transaction Type: '
               || SUBSTR (SQLERRM, 1, 100);
            GOTO LAST_STEP;
      END;

      -- Deriving Account Period

      BEGIN
         l_error_mess := NULL;

         SELECT acct_period_id
           INTO l_account_period
           FROM ORG_ACCT_PERIODS_V
          WHERE     ORGANIZATION_ID = l_org_id
                AND TRUNC (SYSDATE) BETWEEN START_DATE AND END_DATE
                AND REC_TYPE = 'ORG_PERIOD'
                AND STATUS = 'Open';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            o_ret_msg := 'Transaction Date Period Not Opened/Found ';
            GOTO LAST_STEP;
         WHEN OTHERS
         THEN
            o_ret_msg :=
                  'Error Occured While Deriving Priod: '
               || SUBSTR (SQLERRM, 1, 100);
            GOTO LAST_STEP;
      END;

      l_transaction_header_id_ex :=
         TO_CHAR (l_org_id) || TO_CHAR (SYSDATE, 'HH24MISS');

      l_tran_interface_id := mtl_material_transactions_s.NEXTVAL;

      -- Inserting into Transactions Interface Table
      BEGIN
         l_error_mess := NULL;

         INSERT INTO MTL_TRANSACTIONS_INTERFACE (source_code,
                                                 process_flag,
                                                 transaction_mode,
                                                 transaction_uom,
                                                 transaction_type_id,
                                                 created_by,
                                                 last_updated_by,
                                                 organization_id,
                                                 transaction_date,
                                                 new_average_cost,
                                                 inventory_item_id,
                                                 source_header_id,
                                                 source_line_id,
                                                 transaction_quantity,
                                                 transaction_source_id,
                                                 transaction_source_type_id,
                                                 creation_date,
                                                 last_update_date,
                                                 last_update_login,
                                                 transaction_interface_id,
                                                 transaction_header_id,
                                                 material_account,
                                                 cost_group_id,
                                                 acct_period_id,
                                                 material_overhead_account,
                                                 resource_account,
                                                 overhead_account,
                                                 outside_processing_account)
              VALUES ('ACU',                            -- transaction_source,
                      1,                                    -- l_process_flag,
                      3,                                -- l_transaction_mode,
                      l_primary_uom_code,                   -- transaction_uom
                      l_tran_type_id,
                      l_user_id,                                 -- created_by
                      l_user_id,                            -- last_updated_by
                      l_org_id,                             -- organization_id
                      SYSDATE,                             -- transaction_date
                      l_new_cost,                          -- new_average_cost
                      l_item_id,                          -- inventory_item_id
                      1,                                   -- source_header_id
                      1,                                     -- source_line_id
                      0,                               -- transaction_quantity
                      l_transaction_source_id,        -- transaction_source_id
                      l_trans_source_type_id,    -- transaction_source_type_id
                      SYSDATE,                                -- creation_date
                      SYSDATE,                             -- last_update_date
                      l_login_id,                                  -- Login id
                      l_tran_interface_id,         -- transaction_interface_id
                      l_transaction_header_id_ex,
                      ln_material_account,
                      ln_cost_group_id,
                      l_account_period,
                      ln_material_overhead_account,
                      ln_resource_account,
                      ln_overhead_account,
                      ln_outside_processing_account);
      EXCEPTION
         WHEN OTHERS
         THEN
            o_ret_msg :=
                  'Error Occured While Inserting Data into Tran Intf Table: '
               || SUBSTR (SQLERRM, 1, 100);
            GOTO LAST_STEP;
      END;


      -- Inserting into Transactions Cost Interface Table

      BEGIN
         INSERT INTO MTL_TXN_COST_DET_INTERFACE (transaction_interface_id,
                                                 last_update_date,
                                                 last_updated_by,
                                                 creation_date,
                                                 created_by,
                                                 last_update_login,
                                                 organization_id,
                                                 cost_element_id,
                                                 level_type,
                                                 new_average_cost)
              VALUES (l_tran_interface_id,
                      SYSDATE,
                      l_user_id,
                      SYSDATE,
                      l_user_id,
                      l_login_id,
                      l_org_id,
                      l_cost_element_id,
                      l_level_type,
                      l_new_cost);
      EXCEPTION
         WHEN OTHERS
         THEN
            o_ret_msg :=
                  'Error Occured While Inserting Data into Cost Intf Table: '
               || SUBSTR (SQLERRM, 1, 100);
             GOTO LAST_STEP;
      END;

      COMMIT;

      -- Submitting Transaction Processor
      ln_retcode :=
         INV_TXN_MANAGER_PUB.process_transactions (
            p_api_version     => 1.0,
            p_init_msg_list   => fnd_api.g_true,
            x_return_status   => l_return_status,
            x_msg_count       => l_msg_count,
            x_msg_data        => l_msg_data,
            x_trans_count     => l_trans_count,
            p_header_id       => l_transaction_header_id_ex);

      COMMIT;
    <<LAST_STEP>>
    NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
      o_ret_msg := substr(sqlerrm,1,250);
      ROLLBACK;
   END;   
   
END XXWC_INV_ITEM_AVGCOST_UPD_PKG;
/