CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_PAYMENTS_PKG IS

  -----------------------------------------------------------------------------
  -- Copyright 2012 HD Supply  Inc (Orlando, FL) - All rights reserved
  --
  --    Overview: This package contains procedure for creating the receipts
  --              through the lockbox program.
  --
  --    NAME:      XXCUS_OZF_PAYMENTS_PKG.pck
  --
  --    REVISIONS:
  --    Ver        Date        Author           Description
  --    ---------  ----------  ---------------  -------------------------------
  --    1.0        06/06/2012  Nithya Sampath   1. Created this package.
  --    1.1        07/21/2014  Kathy Poling     RFC 41043 SR  Change in procedure
  --                                            submit_lockbox_pgm      
  --                                            New parameter 'Apply unearn discounts' 
  --                                            handling in process lockbox added from
  --                                            patching that was done on 7/20/14
  --    1.2     07/31/2014 Maharajan Shunmugam  RFC 41107 SR  Change in procedure
  --                                            submit_lockbox_pgm      
  --                                            New parameter 'No of instances' 
  --                                            handling in process lockbox added from
  --                                            patching that was done on 7/29/14
  --   1.3       06/17/2015  Nancy Pahwa       Service 291010 New rebate payment fields
  --  1.4        07/08/2015  Balaguru Seshadri timeframe dff logic change, ESMS 294318
  --  1.5        10/07/205    Balaguru Seshadri Add agreement name and code / TMS 20151001-00046
  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------
  PROCEDURE printLog(p_message IN VARCHAR2) IS
  BEGIN
    apps.FND_FILE.PUT_LINE(apps.FND_FILE.LOG, p_message);
    dbms_output.put_line(p_message);
  END printLog;
  --
  PROCEDURE printOutput(p_message IN VARCHAR2) IS
  BEGIN
    apps.FND_FILE.PUT_LINE(apps.FND_FILE.OUTPUT, p_message);
    dbms_output.put_line(p_message);
  END printOutput;
  /*
   -- procedure: create_receipts
    -- Modification History:
    --
    -- Ver            When                  Who                               Did what
        -----------  --------                -------------------             ----------------------------------
        1.5             10/07/2015   Balaguru Seshadri      TMS 20151001-00046 
*/    
  PROCEDURE create_receipts(p_err_buf        OUT VARCHAR2,
                            p_ret_code       OUT VARCHAR2,
                            p_org_id         IN NUMBER,
                            p_payment_Method IN VARCHAR2) IS
  
    l_transmission_id    NUMBER;
    l_transmission_reqid NUMBER;
    l_payment_amount     NUMBER;
    l_user_id            VARCHAR2(20);
    l_invoice_number     VARCHAR2(30);
    l_status_flag        VARCHAR2(1);
    v_return_val         NUMBER;
    v_request_id         NUMBER;
    x_status_flag        BOOLEAN;
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(500);
    l_err_callfrom       VARCHAR2(75) DEFAULT 'XXCUSOZF_INVOICE_PKG';
    l_err_callpoint      VARCHAR2(75) DEFAULT 'START';
    l_distro_list        VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_req_id             NUMBER NULL;
    l_message            VARCHAR2(150);
  
    x_status     BOOLEAN;
    l_request_id NUMBER;
    l_payment_time_frame Varchar2(20); --Ver 1.4
  
  BEGIN
  
    SAVEPOINT create_receipts;
    l_request_id := fnd_global.conc_request_id;
    l_user_id    := fnd_global.user_id;
    --fnd_global.apps_initialize(l_user_id, 50622, 222);
    mo_global.set_policy_context('S', p_org_id);
    fnd_request.set_org_id(p_org_id);
  
    IF p_payment_method IN ('REBATE_CHECK', 'REBATE_CREDIT') AND
       p_org_id = 102 THEN
    
      printlog('REBATE_CHECK OR REBATE_CREDIT For Canada');
      /*
      FOR c_payments_ext_data IN (SELECT receipt_method,
                                         receipt_number,
                                         payment_date,
                                         gl_date,
                                         deposit_date,
                                         currency,
                                         payment_amount,
                                         creditmemo_check_image_link,
                                         mvid_vendor_name,
                                         lob,
                                         additional_data1
                                    FROM xxcus.xxcusozf_payments_ext_tbl) LOOP
      
      INSERT INTO XXCUS.XXCUSOZF_RBT_PAYMENTS_TBL --XXCUSOZF_RBT_PAYMENTS_TBL
           (rebate_payment_id,
            payment_method,
            payment_number,
            payment_date,
            deposit_date,
            customer_number,
            customer_trx_id,
            payment_amount,
            gl_date,
            creation_date,
            created_by,
            last_updated_date,
            currency_code,
            image_url,
            line_of_business,
            org_id,
            payment_time_frame,
            ar_invoice_num,
            status)
         VALUES
           (XXCUSOZF_RBT_PAYMENT_S.NEXTVAL,
            c_payments_ext_data.receipt_method,
            c_payments_ext_data.receipt_number,
            c_payments_ext_data.payment_date,
            c_payments_ext_data.deposit_date,
            c_payments_ext_data.mvid_vendor_name,
            NULL,
            c_payments_ext_data.payment_amount,
            c_payments_ext_data.gl_date,
            SYSDATE,
            l_user_id,
            SYSDATE,
            c_payments_ext_data.currency,
            c_payments_ext_data.creditmemo_check_image_link,
            c_payments_ext_data.lob,
            p_org_id,
            c_payments_ext_data.additional_data1,
            NULL,
            'N');
      END LOOP;
      
      printlog('Inserted CAD Payments into Staging Table');
      
      
      
      printlog('Call to Payments Host File');
      
      
      v_request_id := Fnd_Request.Submit_Request(Application => 'XXCUS',
                                                  Program     => 'XXCUSPAYMENTS',
                                                  Description => NULL,
                                                  Sub_Request => FALSE,
                                                  Argument1   => NULL,
                                                  Argument2   => NULL,
                                                  Argument3   => '',
                                                  Argument4   => '');
      
      printlog('End Call to Payments Host File');
      */
    
    END IF;
  
    IF p_payment_method = 'REBATE_DEDUCTION' THEN
    
      FOR c_deduction_dtls IN (SELECT rcta.trx_date              Deduction_Date,
                                      SYSDATE                    Deposit_Date,
                                      rcta.trx_number            Payment_number,
                                      rcta.attribute1            BU_Id,
                                      hp.party_name              BU_Name,
                                      rcta.invoice_currency_code currency_code,
                                      rcta.org_id                Org_id,
                                      rcta.attribute1            lob_id,
                                      rcta.customer_trx_id,
                                      rcta.attribute15,
                                      hca.account_number         customer_number,
                                      apsa.amount_due_remaining  Payment_Amount,
                                      --1.3 start
                                      decode(replace(substr(rcta.trx_number,
                                                            1,
                                                            4),
                                                     '-',
                                                     ''),
                                             'REB',
                                             'REBATE',
                                             'COOP') rebate_type,
                                        qpvl.attribute7 offer_year, --Ver 1.5
                                      /* --Begin Ver 1.5      
                                      (select attribute7
                                         from qp_list_headers_vl
                                        where list_header_id in
                                              (select distinct activity_id
                                                 from APPS.OZF_CLAIM_LINES_ALL
                                                where claim_id in
                                                      (select claim_id
                                                         from APPS.OZF_CLAIMS_ALL
                                                        where claim_number =
                                                              rcta.interface_header_attribute1))) offer_year, --Ver 1.4 change alias year to offer_year
                                        */ --End Ver 1.5
                                     /*  -- Begin Ver 1.4 
                                      to_char(rcta.creation_date, 'MON') || '-' ||
                                      (select attribute7
                                         from qp_list_headers_vl
                                        where list_header_id in
                                              (select distinct activity_id
                                                 from APPS.OZF_CLAIM_LINES_ALL
                                                where claim_id in
                                                      (select claim_id
                                                         from APPS.OZF_CLAIMS_ALL
                                                        where claim_number =
                                                              rcta.interface_header_attribute1))) payment_time_frame,
                                     */ -- End Ver 1.4
                                       -- Begin Ver 1.4                                                              
                                       (
                                         select glp.period_year
                                         from   gl_periods glp
                                         where  1 =1
                                           and  apsa.gl_date between glp.start_date and glp.end_date
                                           and  adjustment_period_flag ='N'
                                         
                                       )                                               inv_cal_year 
                                       -- End Ver 1.4                                                            
                               --1.3 end
                                   ,qpvl.name agreement_code --Ver 1.5
                                   ,qpvl.description agreement_name --Ver 1.5
                                 FROM ra_customer_trx_all      rcta,
                                      ra_cust_trx_types_all    rctta,
                                      hz_cust_accounts         hca,
                                      ar_payment_schedules_all apsa,
                                      hz_parties               hp
                                      ,ozf_claims_all ozfc --Ver 1.5
                                      ,ozf_claim_lines_all ozfcl --Ver 1.5
                                      ,qp_list_headers_vl qpvl--Ver 1.5
                                WHERE 1 = 1
                                  AND apsa.customer_trx_id =
                                      rcta.customer_trx_id
                                  AND apsa.class = 'INV'
                                  AND hca.cust_account_id =
                                      rcta.bill_to_customer_id
                                  AND rctta.cust_trx_type_id =
                                      rcta.cust_trx_type_id
                                  AND rctta.name = p_payment_method
                                  AND TO_CHAR(hp.party_id) = rcta.attribute1
                                  AND hp.attribute1 = 'HDS_BU'
                                  AND rcta.attribute15 IS NULL
                                  AND rctta.org_id = rcta.org_id
                                  AND ozfc.claim_id =to_number(rcta.interface_header_attribute2) --Ver 1.5
                                  AND ozfcl.claim_id =ozfc.claim_id --Ver 1.5
                                  AND qpvl.list_header_id =ozfcl.activity_id --Ver 1.5
                                  AND rcta.org_id = p_org_id) LOOP
        -- Begin Ver 1.4
        begin
         if c_deduction_dtls.inv_cal_year =c_deduction_dtls.offer_year then 
           l_payment_time_frame :=to_char(add_months(c_deduction_dtls.deduction_date, -1), 'MON-YYYY'); -- Ver 1.4 
           --For current year agreements, print invoice month minus 1 --ver 1.4
         else
           l_payment_time_frame :=c_deduction_dtls.offer_year; --Ver 1.4 --for prior year agreements print the agreement year only.
         end if;
        end;
        -- End Ver 1.4
        INSERT INTO XXCUS.XXCUSOZF_RBT_PAYMENTS_TBL --XXCUSOZF_RBT_PAYMENTS_TBL
          (rebate_payment_id,
           payment_method,
           payment_number,
           payment_date,
           deposit_date,
           customer_number,
           customer_trx_id,
           payment_amount,
           gl_date,
           creation_date,
           created_by,
           last_updated_date,
           currency_code,
           image_url,
           line_of_business,
           org_id,
           payment_time_frame,
           ar_invoice_num,
           status,
           rebate_type, --1.3
           --year) --1.3
            year --Ver 1.5
           ,agreement_code --Ver 1.5
           ,agreement_name --Ver 1.5
           ) --Ver 1.5           
        VALUES
          (XXCUS.XXCUSOZF_RBT_PAYMENT_S.NEXTVAL,
           p_payment_method,
           c_deduction_dtls.payment_number,
           c_deduction_dtls.deduction_date,
           trunc(c_deduction_dtls.deposit_date),
           c_deduction_dtls.customer_number,
           c_deduction_dtls.customer_trx_id,
           c_deduction_dtls.payment_amount,
           trunc(SYSDATE),
           SYSDATE,
           l_user_id,
           SYSDATE,
           c_deduction_dtls.currency_code,
           NULL,
           c_deduction_dtls.BU_Name,
           c_deduction_dtls.org_id,
           --NULL, --1.3
           l_payment_time_frame, --c_deduction_dtls.payment_time_frame, --1.3 --Ver 1.4
           c_deduction_dtls.payment_number,
           'N',
           c_deduction_dtls.rebate_type, --1.3
           --c_deduction_dtls.offer_year); --Ver 1.4 year); --1.3 --Ver 1.5
           c_deduction_dtls.offer_year,  --Ver 1.5
           c_deduction_dtls.agreement_code,  --Ver 1.5
           c_deduction_dtls.agreement_name  --Ver 1.5                      
           ); --Ver 1.5          
      
        UPDATE ra_customer_trx_all rcta
           SET rcta.attribute15 = 'Y'
         WHERE customer_trx_id = c_deduction_dtls.customer_trx_id;
      END LOOP;
    END IF;
  
    XXCUS_OZF_PAYMENTS_PKG.load_interface_table(p_org_id         => p_org_id,
                                                p_payment_Method => p_payment_method);
  
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.LOG, 'l_err_msg' || l_err_msg);
      p_ret_code      := l_err_code;
      p_err_buf       := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUSOZF_UTIL_PKG.create_deduction_document',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
    
      ROLLBACK TO create_receipts;
      p_ret_code := 1;
    WHEN fnd_api.g_exc_unexpected_error THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      p_ret_code      := l_err_code;
      p_err_buf       := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUS_OZF_PAYMENTS_PKG.create_receipt package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
    
      ROLLBACK TO create_receipts;
      p_ret_code := 1;
    WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      p_ret_code      := l_err_code;
      p_err_buf       := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                           p_calling           => l_err_callpoint,
                                           p_request_id        => l_req_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error Running XXCUS_OZF_PAYMENTS_PKG.create_receipt package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module            => 'TM');
    
      ROLLBACK TO create_receipts;
      p_ret_code := 1;
  END create_receipts;
  /*******************************************************************************
      PROCEDURE : Load_interface_table
  --    REVISIONS:
  --    Ver        Date        Author           Description
  --    ---------  ----------  ---------------  -------------------------------
  --  1.5        10/07/205    Balaguru Seshadri Add agreement name and code : TMS 20151001-00046
  ------------------------------------------------------------------------------      
  *******************************************************************************/
  PROCEDURE load_interface_table(p_org_id         IN NUMBER,
                                 p_payment_Method IN VARCHAR2) IS
  
    CURSOR transmission_reqid IS
      SELECT Fnd_Concurrent_Requests_s.nextval FROM DUAL;
  
    CURSOR transmission_id IS
      SELECT ar_transmissions_s.NEXTVAL FROM DUAL;
  
    CURSOR get_transmission_line(cv_payment_method IN VARCHAR2,
                                 cv_trns_seq       IN NUMBER) IS
      SELECT *
        FROM ar_transmissions_all
       where transmission_name =
             cv_payment_method || '_' || SYSDATE || '_' || cv_trns_seq;
  
    CURSOR get_payment_amount(p_payment_method IN VARCHAR2,
                              p_org_id         IN NUMBER) IS
      SELECT payment_method,
             org_id,
             sum(payment_amount) payment_amount,
             count(*) receipt_count
        FROM XXCUS.XXCUSOZF_RBT_PAYMENTS_TBL crpt
       WHERE crpt.payment_method = p_payment_method
         AND crpt.status = 'N'
         AND crpt.org_id = p_org_id
       GROUP BY crpt.payment_method, crpt.org_id;
  
    CURSOR get_lockbox_id(p_name IN VARCHAR2, p_org_id IN NUMBER) IS
      SELECT ar.lockbox_id
        FROM ar_lockboxes_all ar
       WHERE lockbox_number = p_name
         AND org_id = p_org_id;
  
    CURSOR get_rcpt_method_id(p_name IN VARCHAR2) IS
      SELECT rm.receipt_method_id
        FROM ar_receipt_methods rm, ar_receipt_classes rc
       WHERE rm.receipt_Class_id = rc.receipt_class_id
         AND rm.name = p_name;
  
    CURSOR get_tr_name(p_name IN VARCHAR2) IS
      SELECT transmission_format_id
        FROM ar_transmission_formats
       WHERE format_name = p_name;
  
    l_payment_number     NUMBER;
    l_receipt_method_id  NUMBER;
    l_lockbox_id         NUMBER;
    l_payment_count      NUMBER := 0;
    l_transmission_id    NUMBER;
    l_transmission_reqid NUMBER;
    l_payment_method     varchar2(30);
    l_invoice_number     VARCHAR2(30);
    l_user_id            NUMBER;
    x_status_flag        BOOLEAN;
    l_trns_seq           NUMBER;
    l_receipt_method     VARCHAR2(30);
    l_trans_name         VARCHAR2(100);
    l_request_id         NUMBER;
    l_format_id          NUMBER;
  
  BEGIN
  
    printlog('check1');
    l_request_id := fnd_global.conc_request_id;
    l_user_id    := fnd_global.user_id;
  
    printlog('User ID' || l_user_id);
    printlog('Request ID' || l_request_id);
    /*SELECT get_trns_seq.NEXTVAL INTO l_trns_seq FROM DUAL;*/
  
    IF p_payment_method = 'REBATE_DEDUCTION' THEN
      l_payment_method := 'DEDUCTION';
    END IF;
  
    IF p_payment_method = 'REBATE_CREDIT' THEN
      l_payment_method := 'CREDIT';
    END IF;
  
    IF p_payment_method = 'REBATE_CHECK' THEN
      l_payment_method := 'CHECK';
    END IF;
  
    FOR c_get_pmt_method IN get_payment_amount(p_payment_Method, p_org_id) LOOP
    
      OPEN get_lockbox_id('REBATES', p_org_id);
      FETCH get_lockbox_id
        INTO l_lockbox_id;
      CLOSE get_lockbox_id;
    
      l_format_id := NULL;
    
      OPEN get_tr_name('REBATES');
      FETCH get_tr_name
        INTO l_format_id;
      CLOSE get_tr_name;
    
      OPEN get_rcpt_method_id(p_payment_method);
      FETCH get_rcpt_method_id
        INTO l_receipt_method_id;
      CLOSE get_rcpt_method_id;
    
      printlog('l_lockbox_id :' || l_lockbox_id);
      printlog('l_receipt_method_id :' || l_receipt_method_id);
    
      OPEN transmission_reqid;
      FETCH transmission_reqid
        INTO l_transmission_reqid;
      CLOSE transmission_reqid;
    
      OPEN transmission_id;
      FETCH transmission_id
        INTO l_transmission_id;
      CLOSE transmission_id;
    
      printlog('l_transmission_reqid :' || l_transmission_reqid);
      printlog('l_transmission_id :' || l_transmission_id);
    
      l_payment_number := c_get_pmt_method.receipt_count;
    
      printlog('l_payment_number :' || l_payment_number);
    
      l_trans_name := l_payment_method || '_' || SYSDATE || '_' ||
                      l_transmission_id;
    
      printlog('l_trans_name :' || l_trans_name);
    
      INSERT INTO ar_transmissions_all
        (transmission_request_id,
         transmission_id,
         transmission_name,
         trans_date,
         count,
         amount,
         status,
         requested_lockbox_id,
         requested_gl_date,
         requested_trans_format_id,
         org_id,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date)
      VALUES
        (l_transmission_reqid,
         l_transmission_id,
         l_trans_name,
         SYSDATE,
         l_payment_number,
         c_get_pmt_method.payment_amount,
         'NB',
         l_lockbox_id,
         trunc(SYSDATE),
         l_format_id,
         c_get_pmt_method.org_id,
         l_user_id,
         SYSDATE,
         l_user_id,
         SYSDATE);
    
      --  FOR c_get_transmission_line IN get_transmission_line(p_payment_method,l_trns_seq) LOOP
    
      FOR c_get_payment_data IN (SELECT rebate_payment_id,
                                        payment_method,
                                        payment_number,
                                        payment_date,
                                        deposit_date,
                                        customer_number,
                                        customer_trx_id,
                                        payment_amount, --* 100 Payment_Amount,
                                        org_id,
                                        gl_date,
                                        crpt.creation_date,
                                        crpt.created_by,
                                        crpt.last_updated_date,
                                        currency_code,
                                        image_url,
                                        line_of_business       BU_Name,
                                        hp.party_id            BU_Id,
                                        payment_time_frame,
                                        ar_invoice_num,
                                        rebate_type, --1.3
                                        year --1.3
                                        ,agreement_code --Ver 1.5
                                        ,agreement_name --Ver 1.5
                                   FROM XXCUS.XXCUSOZF_RBT_PAYMENTS_TBL crpt,
                                        hz_parties                      hp
                                  WHERE 1 = 1
                                    AND crpt.payment_method =
                                        p_payment_Method
                                    AND crpt.org_id = p_org_id
                                    AND crpt.line_of_business =
                                        hp.party_name
                                    AND hp.attribute1 = 'HDS_BU'
                                    AND crpt.status = 'N') LOOP
      
        l_payment_count := l_payment_count + 1;
      
        IF p_payment_method = 'REBATE_DEDUCTION' THEN
          l_invoice_number := c_get_payment_data.payment_number;
        ELSE
          l_invoice_number := NULL;
        END IF;
      
        printlog('l_invoice_number' || l_invoice_number);
      
        INSERT into ar_payments_interface_all
          (transmission_request_id,
           transmission_id,
           transmission_amount,
           record_type,
           org_id,
           attribute_category,
           customer_number,
           receipt_date,
           receipt_method_id,
           receipt_method,
           check_number,
           item_number,
           remittance_amount,
           gl_date,
           creation_date,
           last_update_date,
           deposit_date,
           transmission_record_id,
           currency_code,
           transmission_record_count,
           cpg_postmark_date,
           invoice1,
           attribute1,
           attribute2,
           attribute3,
           comments,
           attribute10, --1.3
           attribute11 --1.3
           ,attribute12 --Ver 1.5
           ,attribute13 --Ver 1.5
           )
        values
          (l_transmission_reqid,
           l_transmission_id,
           c_get_payment_data.Payment_Amount,
           6,
           c_get_payment_data.org_id,
           c_get_payment_data.org_id,
           c_get_payment_data.customer_number, --768~MSTR 
           c_get_payment_data.payment_date,
           l_receipt_method_id,
           p_payment_method,
           c_get_payment_data.payment_number,
           l_payment_count,
           c_get_payment_data.Payment_Amount,
           c_get_payment_data.gl_date,
           SYSDATE,
           SYSDATE,
           c_get_payment_data.deposit_date,
           ar_payments_interface_s.nextval,
           c_get_payment_data.currency_code,
           l_payment_count,
           SYSDATE,
           l_invoice_number,
           c_get_payment_data.BU_Id,
           c_get_payment_data.payment_time_frame,
           c_get_payment_data.customer_trx_id,
           c_get_payment_data.image_url,
           c_get_payment_data.rebate_type, --1.3
           --c_get_payment_data.year); --1.3 --Ver 1.5
           c_get_payment_data.year,  --Ver 1.5
           c_get_payment_data.agreement_code,  --Ver 1.5
           c_get_payment_data.agreement_name  --Ver 1.5
           ); --Ver 1.5           
      
        UPDATE XXCUS.XXCUSOZF_RBT_PAYMENTS_TBL crpt
           SET crpt.status = 'Y'
         WHERE crpt.rebate_payment_id =
               c_get_payment_data.rebate_payment_id;
      
        printlog('Data Inserted into payments interface :' ||
                 c_get_payment_data.payment_number);
      
      END LOOP; ---Payments Interface line
    
      printlog('Before Calling Lockbox Proram :');
    
      submit_lockbox_pgm(p_transmission_name => l_trans_name,
                         p_transmission_id   => l_transmission_id,
                         p_lockbox_id        => l_lockbox_id,
                         p_format_id         => l_format_id,
                         p_org_id            => p_org_id);
    
      printlog('After Calling Lockbox Program');
    
    -- END LOOP; ---Transmission Line
    END LOOP; -- Payment Method
  
  END load_interface_table;

  /*******************************************************************************
      PROCEDURE : Submit_Lockbox_Program
   -----------------------------------------------------------------------------
   -- Copyright 2012 HD Supply  Inc (Orlando, FL) - All rights reserved
   --
   --    Overview: This package contains procedure for creating the receipts
   --              through the lockbox program.
   --
   --    NAME:      XXCUS_OZF_PAYMENTS_PKG.pck
   --
   --    REVISIONS:
   --    Ver        Date        Author           Description
   --    ---------  ----------  ---------------  -------------------------------
   --    1.1        07/21/2014  Kathy Poling     RFC 41043 SR  Change in procedure
   --                                            submit_lockbox_pgm      
   --                                            New parameter 'Apply unearn discounts' 
   --                                            handling in process lockbox added from
   --                                            patching that was done on 7/20/14     
   --    1.2     07/31/2014 Maharajan Shunmugam  RFC 41107 SR  Change in procedure
   --                                            submit_lockbox_pgm      
   --                                            New parameter 'No of instances' 
   --                                            handling in process lockbox added from
   --                                            patching that was done on 7/29/14
  *******************************************************************************/

  PROCEDURE submit_lockbox_pgm(p_transmission_name IN VARCHAR2,
                               p_transmission_id   IN NUMBER,
                               p_lockbox_id        IN NUMBER,
                               p_format_id         IN NUMBER,
                               p_org_id            IN NUMBER) IS
  
    v_request_id NUMBER := NULL;
    --l_transmission_name          VARCHAR2(30):= 'CREDIT_MEMO_21-JUN-12';               
  
    l_err_msg        VARCHAR2(3000);
    l_err_code       NUMBER;
    l_sec            VARCHAR2(500);
    v_request_status BOOLEAN;
    v_phase          VARCHAR2(2000);
    v_wait_status    VARCHAR2(2000);
    v_dev_phase      VARCHAR2(2000);
    v_dev_status     VARCHAR2(2000);
    v_message        VARCHAR2(2000);
    l_request_id     NUMBER;
  BEGIN
  
    printlog('Enter Lockbox Program');
    printlog('p_transmission_name' || p_transmission_name);
    printlog('p_lockbox_id' || p_lockbox_id);
    printlog('p_org_id' || p_org_id);
  
    v_request_id := fnd_request.submit_request('AR', -- Application short name 
                                               'ARLPLB', -- program short name 
                                               NULL, -- program name 
                                               NULL, -- start date 
                                               FALSE, -- sub-request 
                                               'N', -- NEW_TRANSMISSION -- argument1 
                                               p_transmission_id, -- LB_TRANSMISSION_ID 
                                               '', -- ORIG_REQUEST_ID 
                                               p_transmission_name, -- TRANSMISSION_NAME 
                                               'N', -- SUBMIT_IMPORT 
                                               NULL, -- DATA_FILE 
                                               NULL, -- CNTRL_FILE 
                                               p_format_id, -- TRANSMISSION_FORMAT_ID 
                                               'Y', -- SUBMIT_VALIDATION 
                                               'N', -- PAY_UNRELATED_INVOICES 
                                               p_lockbox_id, -- LOCKBOX_ID 
                                               NULL, -- GL_DATE 
                                               'A', -- REPORT_FORMAT 
                                               'N', -- COMPLETE_BATCHES_ONLY 
                                               'Y', -- SUBMIT_POSTBATCH 
                                               'N', -- ALTERNATE_NAME_SEARCH 
                                               'N', -- IGNORE_INVALID_TXN_NUM 
                                               '', -- USSGL_TRANSACTION_CODE 
                                               p_org_id, -- ORG_ID 
                                               'N', -- Apply unearn discount  --Version 1.1 added 7/24/14 
                                               1, --No of instances         --Version 1.2 added 7/30/14
                                               'L', -- SUBMISSION_TYPE 
                                               NULL -- SCORING_MODEL 
                                               );
  
    fnd_file.put_line(fnd_file.LOG,
                      'Concurrent Request Submitted Successfully:' ||
                      v_request_id);
    COMMIT;
  
    IF v_request_id IS NOT NULL THEN
      --Wait for the request to complete successfully---
      v_request_status := fnd_concurrent.wait_for_request(request_id => v_request_id,
                                                          interval   => 10,
                                                          max_wait   => 0,
                                                          phase      => v_phase,
                                                          status     => v_wait_status,
                                                          dev_phase  => v_dev_phase,
                                                          dev_status => v_dev_status,
                                                          message    => v_message);
      v_dev_phase      := NULL;
      v_dev_status     := NULL;
    
    END IF;
  
  END submit_lockbox_pgm;

end XXCUS_OZF_PAYMENTS_PKG;
/