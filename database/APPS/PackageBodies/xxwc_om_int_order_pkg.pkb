CREATE OR REPLACE PACKAGE BODY APPS.xxwc_om_int_order_pkg
AS
   /*************************************************************************
     $Header xxwc_om_int_orders_pkg $
     Module Name: xxwc_om_int_orders_pkg.pkb

     PURPOSE:   This package is used for changes to internal orders

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        08/07/2012  Shankar Hariharan        Initial Version
     2.0        7/24/2014   Ram Talluri              added procedure ISO_accepted_BY- TMS #20140130-00144
     3.0        10/31/2014  Pattabhi Avula           TMS# 20141002-00060 Multi Org Changes for Canada
     4.0        02/03/2016  Rakesh Patel             TMS# 20150316-00051 internal order pick report performance issue
     5.0        01/15/2018  Rakesh Patel             TMS#20180105-00394-Order not eligible for booking- Happens nearly every other order to multiple users--Severely impacti
     6.0        01/23/2018  Rakesh Patel             TMS#20180123-00028-unbookable order
     7.0        02/20/2018  Rakesh Patel             TMS#20180220-00071-DMS 2.0 Pilot enhancements based on branch feedback  
   **************************************************************************/

   /*************************************************************************
  Procedure : Write_Log

  PURPOSE:   This procedure logs debug message in Concurrent Log file
  Parameter:
         IN
             p_debug_msg      -- Debug Message
************************************************************************/
   g_pkg_name        VARCHAR2(100):='XXWC_OM_INT_ORDER_PKG';  -- Version# 4.0, TMS# 20150316-00051

   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/
   PROCEDURE write_error (p_debug_msg IN VARCHAR2, p_call_point IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_OM_INT_ORDERS_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
       , p_calling             => l_err_callpoint
       , p_request_id          => l_req_id
       , p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000)
       , p_error_desc          => 'Error running xxwc_om_int_orders_pkg with PROGRAM ERROR'
       , p_distribution_list   => l_distro_list
       , p_module              => 'WSH');
   END write_error;


   PROCEDURE cancel_bo_line (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      v_api_version_number           NUMBER := 1;
      v_return_status                VARCHAR2 (2000);
      v_msg_count                    NUMBER;
      v_msg_data                     VARCHAR2 (2000);

      -- IN Variables --
      v_header_rec                   oe_order_pub.header_rec_type;
      v_line_tbl                     oe_order_pub.line_tbl_type;
      v_action_request_tbl           oe_order_pub.request_tbl_type;
      v_line_adj_tbl                 oe_order_pub.line_adj_tbl_type;

      -- OUT Variables --
      v_header_rec_out               oe_order_pub.header_rec_type;
      v_header_val_rec_out           oe_order_pub.header_val_rec_type;
      v_header_adj_tbl_out           oe_order_pub.header_adj_tbl_type;
      v_header_adj_val_tbl_out       oe_order_pub.header_adj_val_tbl_type;
      v_header_price_att_tbl_out     oe_order_pub.header_price_att_tbl_type;
      v_header_adj_att_tbl_out       oe_order_pub.header_adj_att_tbl_type;
      v_header_adj_assoc_tbl_out     oe_order_pub.header_adj_assoc_tbl_type;
      v_header_scredit_tbl_out       oe_order_pub.header_scredit_tbl_type;
      v_header_scredit_val_tbl_out   oe_order_pub.header_scredit_val_tbl_type;
      v_line_tbl_out                 oe_order_pub.line_tbl_type;
      v_line_val_tbl_out             oe_order_pub.line_val_tbl_type;
      v_line_adj_tbl_out             oe_order_pub.line_adj_tbl_type;
      v_line_adj_val_tbl_out         oe_order_pub.line_adj_val_tbl_type;
      v_line_price_att_tbl_out       oe_order_pub.line_price_att_tbl_type;
      v_line_adj_att_tbl_out         oe_order_pub.line_adj_att_tbl_type;
      v_line_adj_assoc_tbl_out       oe_order_pub.line_adj_assoc_tbl_type;
      v_line_scredit_tbl_out         oe_order_pub.line_scredit_tbl_type;
      v_line_scredit_val_tbl_out     oe_order_pub.line_scredit_val_tbl_type;
      v_lot_serial_tbl_out           oe_order_pub.lot_serial_tbl_type;
      v_lot_serial_val_tbl_out       oe_order_pub.lot_serial_val_tbl_type;
      v_action_request_tbl_out       oe_order_pub.request_tbl_type;

      CURSOR c1
      IS
         SELECT distinct
                a.order_number
              , a.ordered_date
              , b.line_number
              , b.ordered_item
              , c.released_status
              , a.header_id
              , b.line_id
              , c.subinventory
           FROM apps.oe_order_headers a
              , apps.oe_order_lines b
              , wsh_delivery_details c
              , oe_transaction_types_vl d
          WHERE     a.header_id = b.header_id
                AND a.header_id = c.source_header_id
                AND b.line_id = c.source_line_id
                AND b.header_id = c.source_header_id
                AND b.line_type_id = d.transaction_type_id
                AND d.transaction_type_code = 'LINE'
                AND d.name = 'INTERNAL LINE'
                AND b.flow_status_code = 'AWAITING_SHIPPING'
                AND a.flow_status_code = 'BOOKED'
                AND c.released_status = 'B';
                --AND a.header_id=25235;
   BEGIN
      FOR c1_rec IN c1
      LOOP
         v_action_request_tbl (1) := oe_order_pub.g_miss_request_rec;

         -- Cancel a Line Record --
         v_line_tbl (1) := oe_order_pub.g_miss_line_rec;
         v_line_tbl (1).operation := OE_GLOBALS.G_OPR_UPDATE;
         v_line_tbl (1).header_id := c1_rec.header_id;
         v_line_tbl (1).line_id := c1_rec.line_id;
         v_line_tbl (1).ordered_quantity := 0;
         v_line_tbl (1).cancelled_flag := 'Y';
         v_line_tbl (1).change_reason := 'Admin Error';

         OE_ORDER_PUB.PROCESS_ORDER (
            p_api_version_number       => v_api_version_number
          , p_header_rec               => v_header_rec
          , p_line_tbl                 => v_line_tbl
          , p_action_request_tbl       => v_action_request_tbl
          , p_line_adj_tbl             => v_line_adj_tbl
          -- OUT variables
          , x_header_rec               => v_header_rec_out
          , x_header_val_rec           => v_header_val_rec_out
          , x_header_adj_tbl           => v_header_adj_tbl_out
          , x_header_adj_val_tbl       => v_header_adj_val_tbl_out
          , x_header_price_att_tbl     => v_header_price_att_tbl_out
          , x_header_adj_att_tbl       => v_header_adj_att_tbl_out
          , x_header_adj_assoc_tbl     => v_header_adj_assoc_tbl_out
          , x_header_scredit_tbl       => v_header_scredit_tbl_out
          , x_header_scredit_val_tbl   => v_header_scredit_val_tbl_out
          , x_line_tbl                 => v_line_tbl_out
          , x_line_val_tbl             => v_line_val_tbl_out
          , x_line_adj_tbl             => v_line_adj_tbl_out
          , x_line_adj_val_tbl         => v_line_adj_val_tbl_out
          , x_line_price_att_tbl       => v_line_price_att_tbl_out
          , x_line_adj_att_tbl         => v_line_adj_att_tbl_out
          , x_line_adj_assoc_tbl       => v_line_adj_assoc_tbl_out
          , x_line_scredit_tbl         => v_line_scredit_tbl_out
          , x_line_scredit_val_tbl     => v_line_scredit_val_tbl_out
          , x_lot_serial_tbl           => v_lot_serial_tbl_out
          , x_lot_serial_val_tbl       => v_lot_serial_val_tbl_out
          , x_action_request_tbl       => v_action_request_tbl_out
          , x_return_status            => v_return_status
          , x_msg_count                => v_msg_count
          , x_msg_data                 => v_msg_data);

         IF v_return_status = fnd_api.g_ret_sts_success
         THEN
            COMMIT;
            write_log ('Line Cancelation in Existing Order is Success '||c1_rec.order_number||','||c1_rec.line_number);
         ELSE
            ROLLBACK;

            FOR i IN 1 .. v_msg_count
            LOOP
               v_msg_data := oe_msg_pub.get (p_msg_index => i, p_encoded => 'F');
               write_log (
                     '  1.2.Order.'
                  || c1_rec.order_number
                  || ','
                  || c1_rec.line_number
                  || ','
                  || v_return_status
                  || '='
                  || v_msg_data
                  || '='
                  || v_msg_count);
            END LOOP;
         END IF;
      END LOOP;
   END cancel_bo_line;
/*************************************************************************
 *   Procedure : iso_accepted_by
 *
 *   PURPOSE:   This procedure updates accepted by and accepted date to internal order lines. .
 *  
 *   REVISIONS:
 *    Ver        Date        Author                     Description
 *     ---------  ----------  ---------------         -------------------------
 *    1.0        7/24/2014  Ram Talluri               Initial Version- TMS #20140130-00144
 *    1.1        10/22/2014 Pattabhi Avula            Hard coded values are replaced with Profile Options
 *       
 * ************************************************************************/
PROCEDURE iso_accepted_by (errbuf              OUT VARCHAR2,
                           retcode             OUT NUMBER,
                           p_last_delta_date       VARCHAR2,
                           p_header_id             NUMBER)
IS
   l_header_rec               oe_order_pub.header_rec_type;
   o_header_rec               oe_order_pub.header_rec_type;
   o_header_val_rec           oe_order_pub.header_val_rec_type;
   o_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
   o_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type;
   o_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type;
   o_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type;
   o_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type;
   o_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type;
   o_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type;
   l_line_tbl                 oe_order_pub.line_tbl_type;
   o_line_tbl                 oe_order_pub.line_tbl_type;
   o_line_val_tbl             oe_order_pub.line_val_tbl_type;
   o_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
   o_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type;
   o_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type;
   o_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type;
   o_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type;
   o_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
   o_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type;
   o_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type;
   o_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type;
   l_action_request_tbl       oe_order_pub.request_tbl_type;
   o_action_request_tbl       oe_order_pub.request_tbl_type;
   l_return_status            VARCHAR2 (240);
   l_msg_count                NUMBER;
   l_msg_data                 VARCHAR2 (4000);
   l_exception                EXCEPTION;

   l_locked_line              VARCHAR2 (1) := 'N';
   l_locked_header            VARCHAR2 (1) := 'N';
   l_locked_fulfil            VARCHAR2 (1) := 'N';
   l_running_message          VARCHAR2 (2048);
   l_msg_dummy                VARCHAR2 (1024);
   l_user_desc                VARCHAR2 (200);

   -- local error handling variables
   l_err_callfrom             VARCHAR2 (75)
                                 := 'XXWC_OM_INT_ORDER_PKG.ISO_ACCEPTED_BY';
   l_err_callpoint            VARCHAR2 (75) := 'START';
   l_distro_list              VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_message                  VARCHAR2 (2000);
BEGIN
   FOR c1_rec
      IN (SELECT /*+ INDEX (ola XXWC_OE_ORDER_LN_LUD1) */
                ola.ship_from_org_id,
                 ola.header_id,
                 oha.order_type_id,
                 oha.order_number,
                 ola.line_id,
                 ola.line_number || '.' || ola.shipment_number line_num,
                 xofa.ACCEPTED_BY,
                 xofa.ACCEPTED_DATE,
                 xofa.ACCEPTANCE_COMMENTS,
                 ola.ROWID line_rowid,
                 oha.ROWID header_rowid,
                 xofa.ROWID fulfil_rowid
            FROM apps.oe_order_headers oha,
                 apps.oe_order_lines ola,
                 xxwc.xxwc_om_fulfill_acceptance xofa
           WHERE     1 = 1
                 AND ola.line_type_id = FND_PROFILE.VALUE('XXWC_INTERNAL_ORDER_LINE_TYPE')  -- V 1.1 Commented by Pattabhi on 10/22/2014 for TMS# 20141002-00060  --  1012                   --Internal line
                 AND oha.order_type_id = FND_PROFILE.VALUE('XXWC_INTERNAL_ORDER_TYPE') -- V 1.1 Commented by Pattabhi on 10/22/2014 for TMS# 20141002-00060  --1011                 --Internal Order
                 AND oha.header_id = ola.header_id
                 AND ola.line_id = xofa.line_id
                 AND ola.header_id = xofa.header_id
                 AND ola.flow_status_code = 'CLOSED'
                 AND ola.accepted_by IS NULL
                 AND xofa.process_flag = 'N'
                 AND oha.header_id = NVL (p_header_id, oha.header_id)
                 AND TRUNC (ola.last_update_date) BETWEEN TRUNC (
                                                               NVL (
                                                                  TO_DATE (
                                                                     p_last_delta_date,
                                                                     'YYYY/MM/DD HH24:MI:SS'),
                                                                  SYSDATE)
                                                             - 1.01)
                                                      AND TRUNC (
                                                               NVL (
                                                                  TO_DATE (
                                                                     p_last_delta_date,
                                                                     'YYYY/MM/DD HH24:MI:SS'),
                                                                  SYSDATE)
                                                             + 0.9))
   LOOP
      l_err_callpoint := 'Inside loop';

      BEGIN
         l_locked_line :=
            XXWC_OM_PERSONALIZE_RTNS_PKG.is_row_locked (c1_rec.line_rowid,
                                                        'OE_ORDER_LINES_ALL');
         l_locked_header :=
            XXWC_OM_PERSONALIZE_RTNS_PKG.is_row_locked (
               c1_rec.header_rowid,
               'OE_ORDER_HEADERS_ALL');

         l_locked_fulfil :=
            XXWC_OM_PERSONALIZE_RTNS_PKG.is_row_locked (
               c1_rec.fulfil_rowid,
               'XXWC_OM_FULFILL_ACCEPTANCE');

         l_err_callpoint := 'Check locking';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_locked_line := 'Y';
            l_locked_header := 'Y';
            l_locked_fulfil := 'Y';
      END;


      IF     l_locked_line = 'N'
         AND l_locked_header = 'N'
         AND l_locked_fulfil = 'N'
      THEN
         l_err_callpoint := 'Populate API record';

         l_line_tbl (1) := oe_order_pub.g_miss_line_rec;
         l_line_tbl (1).line_id := c1_rec.line_id;
         l_line_tbl (1).header_id := c1_rec.header_id;
         l_line_tbl (1).accepted_by := c1_rec.ACCEPTED_BY;
         l_line_tbl (1).revrec_signature_date := c1_rec.ACCEPTED_DATE;
         l_line_tbl (1).revrec_comments := c1_rec.ACCEPTANCE_COMMENTS;
         l_line_tbl (1).operation := oe_globals.g_opr_update;

         --oe_msg_pub.initialize;

         l_err_callpoint := 'Before API call';

         oe_order_pub.process_order (
            p_api_version_number       => 1.0,
            p_header_rec               => l_header_rec,
            p_line_tbl                 => l_line_tbl,
            p_action_request_tbl       => l_action_request_tbl,
            x_return_status            => l_return_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data,
            x_header_rec               => o_header_rec,
            x_header_val_rec           => o_header_val_rec,
            x_header_adj_tbl           => o_header_adj_tbl,
            x_header_adj_val_tbl       => o_header_adj_val_tbl,
            x_header_price_att_tbl     => o_header_price_att_tbl,
            x_header_adj_att_tbl       => o_header_adj_att_tbl,
            x_header_adj_assoc_tbl     => o_header_adj_assoc_tbl,
            x_header_scredit_tbl       => o_header_scredit_tbl,
            x_header_scredit_val_tbl   => o_header_scredit_val_tbl,
            x_line_tbl                 => o_line_tbl,
            x_line_val_tbl             => o_line_val_tbl,
            x_line_adj_tbl             => o_line_adj_tbl,
            x_line_adj_val_tbl         => o_line_adj_val_tbl,
            x_line_price_att_tbl       => o_line_price_att_tbl,
            x_line_adj_att_tbl         => o_line_adj_att_tbl,
            x_line_adj_assoc_tbl       => o_line_adj_assoc_tbl,
            x_line_scredit_tbl         => o_line_scredit_tbl,
            x_line_scredit_val_tbl     => o_line_scredit_val_tbl,
            x_lot_serial_tbl           => o_lot_serial_tbl,
            x_lot_serial_val_tbl       => o_lot_serial_val_tbl,
            x_action_request_tbl       => o_action_request_tbl);

         l_err_callpoint := 'After API call';


         IF (l_return_status != Fnd_Api.g_ret_sts_success)
         THEN
            IF (l_msg_count > 0)
            THEN
            
               l_msg_data:=NULL;
               
               FOR i IN 1 .. l_msg_count
               LOOP
                
                  l_msg_data :=
                     SUBSTR (
                           l_msg_data
                        || Oe_Msg_Pub.get (p_msg_index => i, p_encoded => 'F'),
                        1,
                        2000);
                  write_log (
                        '1.0 Internal order accepted by update is errored for order number ='
                     || c1_rec.order_number
                     || ', '
                     || ' LINE_NUMBER='
                     || c1_rec.line_num
                     || ', '
                     || 'LINE_ID='
                     || c1_rec.line_id
                     || l_return_status
                     || '='
                     || l_msg_data
                     || '='
                     || l_msg_count);
               END LOOP;
            --ELSE
            --   write_log ('  2.0.Unable to Update the Order.' || l_msg_data);
            END IF;

            UPDATE xxwc_om_fulfill_acceptance
               SET error_msg = SUBSTR (l_msg_data, 1, 2000)
             WHERE ROWID = c1_rec.fulfil_rowid;
         ELSIF l_return_status = 'S'
         THEN
            l_err_callpoint := 'API Success';

            UPDATE xxwc_om_fulfill_acceptance
               SET process_flag = 'Y',
                   last_update_date = SYSDATE,
                   last_updated_by = fnd_global.user_id
             WHERE ROWID = c1_rec.fulfil_rowid;

            write_log (
                  '3.0 Internal order accepted by update is success for order number ='
               || c1_rec.order_number
               || ', '
               || ' LINE_NUMBER='
               || c1_rec.line_num
               || ', '
               || 'LINE_ID='
               || c1_rec.line_id);
         END IF;
      END IF;
   END LOOP;

   COMMIT;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      NULL;
   WHEN OTHERS
   THEN
      NULL;
      write_error ('Error in updating internal order accepted by procedure',
                   l_err_callpoint);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => l_message,
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');
END iso_accepted_by;

-- Version# 4.0 > Start
/*************************************************************************
*   Function : submit_internal_order_pick_rpt
*
*   PURPOSE:   This procedure is called from Standard Header WF Node at header Level.
*   Checks if it is interneal order then submit the report else do nothing.
*  Ver        Date        Author                     Description
*  ---------  ----------  ---------------         -------------------------
*  4.0        02/03/2016  Rakesh Patel            Initial Version MS Ticket 20150316-00051
* ************************************************************************/
PROCEDURE submit_internal_order_pick_rpt (itemtype    IN            VARCHAR2,
                                          itemkey     IN            VARCHAR2,
                                          actid       IN            NUMBER,
                                          funcmode    IN            VARCHAR2,
                                          resultout   IN OUT NOCOPY VARCHAR2)
IS
   l_header_id              NUMBER;
   l_proc_name              VARCHAR2 (100) := 'submit_internal_order_pick_rpt';
   l_debug_level   CONSTANT NUMBER := oe_debug_pub.g_debug_level;
   l_arg3                   VARCHAR2 (50);
   l_internal_order_type    VARCHAR2 (1);
   l_group_id               NUMBER;
   l_from_batch_id          NUMBER;
   l_to_batch_id            NUMBER;
   l_from_org_printer       VARCHAR2 (30);
   l_to_org_printer         VARCHAR2 (30);
   l_request_id             NUMBER;
   l_copies                 NUMBER := 1;
   l_org_code               VARCHAR2(3);
   l_ship_to_org_id         NUMBER;
   l_shp_org_count          NUMBER;  --Rev#7.0
   
   l_message                VARCHAR2 (2000); --Rev#6.0

   --#Rev 5.0 Start <
   -------------------------------------------------------------------------------
   -- Cursor to rowid from order header table 
   -------------------------------------------------------------------------------
   CURSOR cur_header_rowid (p_header_id   IN NUMBER)
   IS
      SELECT ooh.ROWID header_rowid
        FROM apps.oe_order_headers_all ooh --Rev#6.0
       WHERE 1 = 1
         AND ooh.header_id = p_header_id
         AND (ooh.flow_status_code = 'ENTERED' OR booked_flag= 'N');
   --#Rev 5.0 > End 
            
   -------------------------------------------------------------------------------
   -- Cursor to derive Ship From and Ship To Organization details
   -------------------------------------------------------------------------------
   CURSOR cur_get_orgs (p_header_id   IN NUMBER)
   IS
      SELECT DISTINCT oola.header_id
           , oola.ship_from_org_id
           , oola.sold_to_org_id
        FROM apps.oe_order_lines      oola
       WHERE 1 = 1
         AND oola.header_id         = p_header_id
         AND oola.line_type_id      = fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE')
         ;

BEGIN
     IF l_debug_level > 0 THEN
      oe_debug_pub.add ('*** Beging of API  xxwc_om_int_order_pkg.submit_internal_order_pick_rpt for item type/item key'|| ITEMTYPE || '/'|| ITEMKEY,1);
     END IF;

     l_header_id := TO_NUMBER (itemkey);
     DBMS_OUTPUT.put_line ('l_header_id ' || l_header_id);

     --#Rev 5.0 Start <
     IF FND_PROFILE.VALUE ('XXWC_OM_UPDATE_FLOW_STATUS_CODE') = 'Y' THEN
        FOR cur_header_rowid_rec IN cur_header_rowid(l_header_id) LOOP
          --#Rev 6.0 Start <
          FOR i IN 1..10 LOOP   
             BEGIN
	             SELECT header_id
	               INTO l_header_id
	               FROM oe_order_headers_all
	              WHERE header_id = l_header_id
	              FOR UPDATE NOWAIT;
   
                 IF l_debug_level  > 0 THEN
                    oe_debug_pub.add('lock successful for update', 1);
                    l_message := 'lock successful for update';
                 END IF;
                 
                 UPDATE oe_order_headers_all
                    SET flow_status_code    = 'BOOKED'
                      , booked_flag         = 'Y'
                      , last_update_date    = SYSDATE
                      , last_updated_by     = FND_GLOBAL.USER_ID
                      , last_update_login   = FND_GLOBAL.LOGIN_ID
                  WHERE 1 = 1
                    AND header_id = l_header_id;
                  
                  l_message := 'flow_status_code update successful';
                  
                  COMMIT;
                  EXIT;-- exit from the loop
              EXCEPTION
              WHEN APP_EXCEPTIONS.RECORD_LOCK_EXCEPTION THEN
                 -- some one else is currently working on this record
                 IF l_debug_level  > 0 THEN
                    oe_debug_pub.add('in lock exception');
                 END IF;
                 l_message := 'in lock exception';
              WHEN NO_DATA_FOUND THEN
                 IF l_debug_level  > 0 THEN
                    oe_debug_pub.add('no_data_found, lock exception');
                 END IF;
                 l_message := 'no_data_found, lock exception';
              WHEN OTHERS THEN
                 IF l_debug_level  > 0 THEN
                    oe_debug_pub.add('lock exception, others');
                 END IF;
                 l_message := 'no_data_found, lock exception';
              END;
              DBMS_LOCK.sleep(5); -- sleep for 10 sec.
          END LOOP; --loop 10 times
        
          INSERT INTO xxwc_om_orders_datafix_log_tbl ( ORDER_HEADER_ID
                                         ,DATA_FIX_SCRIPT
			                             ,ERROR_MESSAGE
			                             ,CREATION_DATE
			                             ,CREATED_BY
			                             ,LAST_UPDATE_DATE
			                             ,LAST_UPDATED_BY
			                            )
                                 VALUES (l_header_id,
                                         'WF_UPDATE_HEADER_STATUS',
									     l_message,
                                         SYSDATE,
                                         FND_GLOBAL.USER_ID,
                                         SYSDATE,
                                         FND_GLOBAL.USER_ID);
	      COMMIT;
          --#Rev 6.0 > End
        END LOOP; --header cursor to select only order with entered status
     END IF;--profile value check
     --#Rev 5.0 > End
   
        -------------------------------------------------------------------------------
        -- Loop CUR_GET_ORGS
        -------------------------------------------------------------------------------
        FOR c1 IN cur_get_orgs (l_header_id) LOOP
           EXIT WHEN cur_get_orgs%NOTFOUND;

          --IF (funcmode = 'RUN') THEN
          oe_standard_wf.set_msg_context (actid);
          l_arg3      := TO_CHAR (actid);

          IF l_debug_level > 0 THEN
             oe_debug_pub.ADD ('Starting XXWC Print Internal Sales Orders Pick Slip Process');
             oe_debug_pub.ADD ('Initializing variables');
          END IF;

          -------------------------------------------------------------------------------
          -- Derive Order Type
          -------------------------------------------------------------------------------
          l_internal_order_type := xxwc_ont_routines_pkg.internal_order_check (p_header_id   => l_header_id);
          DBMS_OUTPUT.put_line ('l_internal_order_type ' || l_internal_order_type);

             fnd_global.apps_initialize (fnd_global.user_id,
                                         fnd_global.resp_id,
                                         fnd_global.resp_appl_id);
             mo_global.init ('ONT');

          -------------------------------------------------------------------------------
          -- Initialize Group ID
          -------------------------------------------------------------------------------
          l_group_id := NULL;

          SELECT xxwc_print_request_groups_s.NEXTVAL
            INTO l_group_id
            FROM DUAL;

           -------------------------------------------------------------------------------
           -- Derive Ship To Org Code
           -------------------------------------------------------------------------------
           l_org_code := NULL;
           BEGIN
             SELECT SUBSTR (account_name, 1, 3)
               INTO l_org_code
               FROM hz_cust_accounts_all         hcaa
              WHERE cust_account_id = c1.sold_to_org_id
                AND attribute4      = 'WCI'
                AND ROWNUM          = 1;
           EXCEPTION
           WHEN OTHERS THEN
             NULL;
           END;

           -------------------------------------------------------------------------------
           -- Derive Ship To Org Id
           -------------------------------------------------------------------------------
           l_ship_to_org_id := NULL;
           BEGIN
             SELECT organization_id
               INTO l_ship_to_org_id
               FROM org_organization_definitions ood_to
              WHERE organization_code = l_org_code
                AND ROWNUM            = 1;
           EXCEPTION
           WHEN OTHERS THEN
             NULL;
           END;

           -------------------------------------------------------------------------------
           -- Initialize batch ID
           -------------------------------------------------------------------------------
           SELECT xxwc_print_requests_s.NEXTVAL
             INTO l_from_batch_id
             FROM DUAL;

           SELECT xxwc_print_requests_s.NEXTVAL
             INTO l_to_batch_id
             FROM DUAL;

           -------------------------------------------------------------------------------
           -- Derive Org Printer for From Org
           -------------------------------------------------------------------------------
           l_from_org_printer := NULL;

           BEGIN
              SELECT xxwc_ont_routines_pkg.default_org_printer ('XXWC',
                                                                'XXWC_INT_ORD_PICK',
                                                                c1.ship_from_org_id,
                                                                fnd_global.user_id,
                                                                fnd_global.resp_id,
                                                                fnd_global.resp_appl_id)
                INTO l_from_org_printer
                FROM DUAL;
           EXCEPTION
           WHEN OTHERS THEN
              l_from_org_printer := NULL;
           END;

           -------------------------------------------------------------------------------
           -- Pull Org Printer for To Org
           -------------------------------------------------------------------------------
           l_to_org_printer := NULL;

           BEGIN
              SELECT xxwc_ont_routines_pkg.default_org_printer ('XXWC',
                                                                 'XXWC_INT_ORD_PICK',
                                                                 l_ship_to_org_id, --c1.destination_organization_id,
                                                                 fnd_global.user_id,
                                                                 fnd_global.resp_id,
                                                                 fnd_global.resp_appl_id)
                INTO l_to_org_printer
                FROM DUAL;
           EXCEPTION
           WHEN OTHERS THEN
              l_to_org_printer := NULL;
           END;

           IF l_debug_level > 0
           THEN
              oe_debug_pub.ADD ('From Org id/Printer/Batch Id: '|| c1.ship_from_org_id|| '/'|| l_from_org_printer|| '/'|| l_from_batch_id);
              oe_debug_pub.ADD ('To Org id/Printer/Batch Id: '|| l_ship_to_org_id|| '/'|| l_to_org_printer|| '/'|| l_to_batch_id);
           END IF;

           oe_debug_pub.ADD ('From Org id/Printer/Batch Id: '|| c1.ship_from_org_id|| '/'|| l_from_org_printer|| '/'|| l_from_batch_id);
           oe_debug_pub.ADD ('To Org id/Printer/Batch Id: '|| l_ship_to_org_id|| '/'|| l_to_org_printer|| '/'|| l_to_batch_id);

           -------------------------------------------------------------------------------
           -- Insert into XXWC.XXWC_PRINT_REQUESTS_TEMP and
           -- XXWC_PRINT_REQUESTS_ARG_TEMP for From Org
           -------------------------------------------------------------------------------
           IF l_from_org_printer IS NOT NULL
           THEN
              INSERT INTO xxwc.xxwc_print_requests_temp (created_by,
                                                    creation_date,
                                                    last_updated_by,
                                                    last_update_date,
                                                    batch_id,
                                                    process_flag,
                                                    GROUP_ID,
                                                    application,
                                                    program,
                                                    description,
                                                    start_time,
                                                    sub_request,
                                                    printer,
                                                    style,
                                                    copies,
                                                    save_output,
                                                    print_together,
                                                    validate_printer,
                                                    template_appl_name,
                                                    template_code,
                                                    template_language,
                                                    template_territory,
                                                    output_format,
                                                    nls_language)
              VALUES (fnd_global.user_id                     -- created_by
                                        ,
                      SYSDATE,
                      fnd_global.user_id                 -- last_udated_by
                                        ,
                      SYSDATE,
                      l_from_batch_id                          -- BATCH_ID
                                     ,
                      '1'                                  -- PROCESS_FLAG
                         ,
                      l_group_id                               -- GROUP_ID
                                ,
                      'XXWC'                                -- APPLICATION
                            ,
                      'XXWC_INT_ORD_PICK'                       -- PROGRAM
                                         ,
                      'XXWC Internal Order Pick Slip'       -- DESCRIPTION
                                                     ,
                      NULL                                   -- START_TIME
                          ,
                      'FALSE'                               -- SUB_REQUEST
                             ,
                      l_from_org_printer                        -- PRINTER
                                        ,
                      NULL                                        -- STYLE
                          ,
                      --1                                           -- COPIES --removed 08142012
                      l_copies                 --COPIES   --added 08142012
                              ,
                      'TRUE'                                -- SAVE_OUTPUT
                            ,
                      'N'                                -- PRINT_TOGETHER
                         ,
                      'RESOLVE'                        -- VALIDATE_PRINTER
                               ,
                      'XXWC'                         -- TEMPLATE_APPL_NAME
                            ,
                      'XXWC_INT_ORD_PICK'                 -- TEMPLATE_CODE
                                         ,
                      'en'                            -- TEMPLATE_LANGUAGE
                          ,
                      'US'                           -- TEMPLATE_TERRITORY
                          ,
                      'PDF'                               -- OUTPUT_FORMAT
                           ,
                      'en'                                 -- NLS_LANGUAGE
                          );

              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_from_batch_id,
                           1,
                           l_group_id,
                           1,
                           c1.ship_from_org_id);           -- Organization

              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_from_batch_id,
                           1,
                           l_group_id,
                           2,
                           c1.header_id);

              -- Order Number
              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_from_batch_id,
                           1,
                           l_group_id,
                           3,
                           2);

              -- Print Pricing
              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_from_batch_id,
                           1,
                           l_group_id,
                           4,
                           NULL);                               -- Reprint

              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_from_batch_id,
                           1,
                           l_group_id,
                           5,
                           NULL);

              -- Print Kit Details
              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_from_batch_id,
                           1,
                           l_group_id,
                           6,
                           2);

              -- Send to Rightfax Yes or No?
              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_from_batch_id,
                           1,
                           l_group_id,
                           7,
                           NULL);

              -- Fax Comment --Updated 2/13/2013
              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_from_batch_id,
                           1,
                           l_group_id,
                           8,
                           NULL);

              --Hazmat --Updated 2/13/2013 changed argument from 8 to 9
              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_from_batch_id,
                           1,
                           l_group_id,
                           9,
                           2);

           COMMIT;

           -------------------------------------------------------------------------------
           -- Submit Print Batch for From Org
           -------------------------------------------------------------------------------
           SELECT xxwc_ont_routines_pkg.submit_print_batch (l_from_batch_id             -- p_batch_id
                                                          , l_group_id                  -- p_group_id
                                                          , 1                           -- p_process_flag
                                                          , fnd_global.user_id          -- p_user_id
                                                          , fnd_global.resp_id          -- p_resp_id
                                                          , fnd_global.resp_appl_id)    -- p_resp_appl_id
             INTO l_request_id
             FROM DUAL;
             
             --Rev#7.0 <Start
             SELECT count(1) 
              INTO l_shp_org_count 
              FROM apps.fnd_lookup_values flv,
                   org_organization_definitions od
             WHERE 1=1
               AND flv.lookup_type    = 'XXWC_DMS2_BRANCHES_LOOKUP'
               AND flv.enabled_flag   = 'Y'
               AND flv.lookup_code    = od.organization_code
               AND od.organization_id = c1.ship_from_org_id
               AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
            
             IF l_shp_org_count >0 THEN
                XXWC_OM_DMS2_OB_PKG.Send_to_DMS_INT_PICKUP( p_order_header_id   => l_header_id
                                                           ,p_sch_delivery_date => NULL
                                                           ,p_request_id        => l_request_id
                                                           );
             END IF;
             --Rev#7.0 >End
           ELSE
              IF l_debug_level > 0 THEN
                 oe_debug_pub.ADD ('Ship from org printer not defined...');
              END IF;
           END IF;

           -------------------------------------------------------------------------------
           -- Insert into XXWC.XXWC_PRINT_REQUESTS_TEMP and
           -- XXWC_PRINT_REQUESTS_ARG_TEMP for - To Org
           ------------------------------------------------------------------------------
           IF l_to_org_printer IS NOT NULL AND l_internal_order_type IN ('S') THEN
              INSERT INTO xxwc.xxwc_print_requests_temp (created_by,
                                                    creation_date,
                                                    last_updated_by,
                                                    last_update_date,
                                                    batch_id,
                                                    process_flag,
                                                    GROUP_ID,
                                                    application,
                                                    program,
                                                    description,
                                                    start_time,
                                                    sub_request,
                                                    printer,
                                                    style,
                                                    copies,
                                                    save_output,
                                                    print_together,
                                                    validate_printer,
                                                    template_appl_name,
                                                    template_code,
                                                    template_language,
                                                    template_territory,
                                                    output_format,
                                                    nls_language)
              VALUES (fnd_global.user_id                     -- created_by
                                        ,
                      SYSDATE,
                      fnd_global.user_id                 -- last_udated_by
                                        ,
                      SYSDATE,
                      l_to_batch_id                            -- BATCH_ID
                                   ,
                      '1'                                  -- PROCESS_FLAG
                         ,
                      l_group_id                               -- GROUP_ID
                                ,
                      'XXWC'                                -- APPLICATION
                            ,
                      'XXWC_INT_ORD_PICK'                       -- PROGRAM
                                         ,
                      'XXWC Internal Order Pick Slip'       -- DESCRIPTION
                                                     ,
                      NULL                                   -- START_TIME
                          ,
                      'FALSE'                               -- SUB_REQUEST
                             ,
                      l_to_org_printer                          -- PRINTER
                                      ,
                      NULL                                        -- STYLE
                          ,
                      --1                                           -- COPIES --removed 08142012
                      l_copies                 --COPIES   --added 08142012
                              ,
                      'TRUE'                                -- SAVE_OUTPUT
                            ,
                      'N'                                -- PRINT_TOGETHER
                         ,
                      'RESOLVE'                        -- VALIDATE_PRINTER
                               ,
                      'XXWC'                         -- TEMPLATE_APPL_NAME
                            ,
                      'XXWC_INT_ORD_PICK'                 -- TEMPLATE_CODE
                                         ,
                      'en'                            -- TEMPLATE_LANGUAGE
                          ,
                      'US'                           -- TEMPLATE_TERRITORY
                          ,
                      'PDF'                               -- OUTPUT_FORMAT
                           ,
                      'en'                                 -- NLS_LANGUAGE
                          );

              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_to_batch_id,
                           1,
                           l_group_id,
                           1,
                           c1.ship_from_org_id);

              -- Organization
              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_to_batch_id,
                           1,
                           l_group_id,
                           2,
                           c1.header_id);

              -- Order Number
              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_to_batch_id,
                           1,
                           l_group_id,
                           3,
                           2);                            -- Print Pricing

              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_to_batch_id,
                           1,
                           l_group_id,
                           4,
                           NULL);                               -- Reprint

              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_to_batch_id,
                           1,
                           l_group_id,
                           5,
                           NULL);

              -- Print Kit Details
              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_to_batch_id,
                           1,
                           l_group_id,
                           6,
                           2);

              -- Send to Rightfax Yes or No?
              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_to_batch_id,
                           1,
                           l_group_id,
                           7,
                           NULL);                            -- Fax Number

              INSERT INTO xxwc.xxwc_print_requests_arg_temp
                   VALUES (l_to_batch_id,
                           1,
                           l_group_id,
                           8,
                           2);                             -- Print Hazmat

              COMMIT;

           -------------------------------------------------------------------------------
           -- Submit Print Batch for To Org
           -------------------------------------------------------------------------------
           SELECT xxwc_ont_routines_pkg.submit_print_batch (l_to_batch_id               -- p_batch_id
                                                          , l_group_id                  -- p_group_id
                                                          , 1                           -- p_process_flag
                                                          , fnd_global.user_id          -- p_user_id
                                                          , fnd_global.resp_id          -- p_resp_id
                                                          , fnd_global.resp_appl_id)    -- p_resp_appl_id
             INTO l_request_id
             FROM DUAL;

           ELSE
              IF l_debug_level > 0 THEN
                 oe_debug_pub.ADD ('Ship to org printer not defined...');
              END IF;
           END IF;
        END LOOP;

        resultout := 'COMPLETE:Y';
        RETURN;

EXCEPTION
   WHEN OTHERS THEN
      IF l_debug_level > 0
      THEN
         oe_debug_pub.add (
            'Others exception in xxwc_om_int_order_pkg.submit_internal_order_pick_rpt');
         oe_debug_pub.add ('Exception is ' || SQLERRM, 0.5);
      END IF;

      -- The line below records this function call in the error system
      -- in the case of an exception.
      wf_core.CONTEXT (g_pkg_name,                   --'XXWC_OM_INT_ORDER_PKG'
                       l_proc_name,         --'submit_internal_order_pick_rpt'
                       itemtype,
                       itemkey,
                       l_arg3,                               --TO_CHAR (actid)
                       funcmode);

      oe_standard_wf.add_error_activity_msg (p_actid      => actid,
                                             p_itemtype   => itemtype,
                                             p_itemkey    => itemkey);
      oe_standard_wf.save_messages;
      oe_standard_wf.clear_msg_context;
      RAISE;
END submit_internal_order_pick_rpt;   
-- Version# 4.0 < End

END xxwc_om_int_order_pkg;
/