/* Formatted on 17-Mar-2014 13:00:07 (QP5 v5.206) */
-- Start of DDL Script for Package Body APPS.XXWC_AP_HOLD_NOTIFICATIONS_PKG
-- Generated 17-Mar-2014 13:00:05 from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE BODY apps.xxwc_ap_hold_notifications_pkg
IS

 /*************************************************************************
   *   $Header XXWC_AP_HOLD_NOTIFICATIONS_PKG $
   *   Module Name: xxwc OM Automatic Receipt allocation package
   *
   *   Purpose: To send AP hold notification to recipient
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        2/15/2014  Rasikha Galimova       Initial Version
   *   1.1        11/24/2014 Maharajan Shunmugam    TMS#20141001-00163  Canada Multi Org changes
   * ***************************************************************************/


    -- Enter procedure, function bodies as shown below
    l_error_message                    CLOB;
    g_current_runtime_level   CONSTANT NUMBER := fnd_log.g_current_runtime_level;
    g_level_statement         CONSTANT NUMBER := fnd_log.level_statement;
    g_module_name             CONSTANT VARCHAR2 (100) := 'XXWC_AP_INVOICE_HOLD_NOTIFY';
    g_level_procedure         CONSTANT NUMBER := fnd_log.level_procedure;
    l_req_id                           NUMBER := fnd_global.conc_request_id;

    PROCEDURE refresh_main (p_errbuf OUT varchar2, p_retcode OUT varchar2)             --Added parameters for conc program by Maha for ver 1.1
    IS
        l_role    VARCHAR2 (124);
        lx_role   VARCHAR2 (124);
        l_org_id NUMBER;        --Added Org id initialization by Maha for TMS#20141001-00163 

    BEGIN

    l_org_id := MO_GLOBAL.GET_CURRENT_ORG_ID; 
    
    mo_global.set_policy_context('S',l_org_id);


        BEGIN
            FOR t IN (SELECT hold_id FROM apps.xxwc_apinvoicelines_hold
                      MINUS
                      SELECT hold_id
                        FROM xxwc.ap_hold_original_recipient
                       WHERE NVL (orig_recipient, 'X') = 'XXWC_APINVHDN')
            LOOP
                -- delete if exists:
                BEGIN
                    DELETE FROM xxwc.ap_hold_original_recipient
                          WHERE hold_id = t.hold_id;

                    COMMIT;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                END;

                INSERT INTO xxwc.ap_hold_original_recipient (hold_id, orig_recipient, new_recipient)
                    SELECT t.hold_id, name, 'XXWC_APINVHDN'
                      FROM wf_roles
                     WHERE     orig_system = 'PER'
                           AND orig_system_id IN
                                   (SELECT number_value
                                      FROM wf_notification_attributes
                                     WHERE     name = 'INTERNAL_REP_PERSON_ID'
                                           AND notification_id IN
                                                   (SELECT notification_id
                                                      FROM wf_notifications
                                                     WHERE     message_name = 'POMATCHED_HLD_NOTIF_MSG'
                                                           AND MESSAGE_TYPE = 'APINVHDN'
                                                           AND item_key = t.hold_id
                                                           AND status = 'OPEN'
                                                           AND ROWNUM = 1));
            END LOOP;

            COMMIT;
        END;

        DELETE FROM xxwc.xxwc_ap_hold_notifications t
              WHERE t.ROWID NOT IN (SELECT MAX (b.ROWID)
                                      FROM apps.xxwc_ap_hold_notifications b
                                     WHERE b.hold_id = t.hold_id AND b.po_number = t.po_number);

        COMMIT;

        FOR r_exp IN (SELECT hold_id, notification_id, recipient_role FROM apps.xxwc_ap_hold_notifications
                      MINUS
                      SELECT hold_id, notification_id, recipient_role FROM apps.xxwc_apinvoicelines_hold)
        LOOP
            DELETE FROM xxwc.xxwc_ap_hold_notifications                                  --will delete "locked rows' too
                  WHERE     hold_id = r_exp.hold_id
                        AND notification_id = r_exp.notification_id
                        AND recipient_role = r_exp.recipient_role;
        END LOOP;

        COMMIT;

        BEGIN
            FOR rw IN (SELECT ROWID row_id
                             ,hold_id
                             ,orig_recipient
                             ,new_recipient
                         FROM xxwc.ap_hold_original_recipient
                        WHERE orig_recipient = 'XXWC_APINVHDN')
            LOOP
                FOR rx
                    IN (SELECT DISTINCT name
                          FROM wf_roles
                         WHERE     orig_system = 'PER'
                               AND orig_system_id IN
                                       (SELECT number_value
                                          FROM wf_notification_attributes
                                         WHERE     name = 'INTERNAL_REP_PERSON_ID'
                                               AND notification_id IN (SELECT notification_id
                                                                         FROM apps.xxwc_ap_hold_notifications
                                                                        WHERE hold_id = rw.hold_id AND ROWNUM = 1)))
                LOOP
                    lx_role := rx.name;
                END LOOP;

                UPDATE xxwc.ap_hold_original_recipient
                   SET orig_recipient = lx_role
                 WHERE ROWID = rw.row_id;
            END LOOP;

            COMMIT;
        END;

        INSERT INTO xxwc.xxwc_ap_hold_notifications (hold_id
                                                    ,hold_lookup_code
                                                    ,hold_reason
                                                    ,agent_id
                                                    ,vendor_id
                                                    ,org_id
                                                    ,invoice_num
                                                    ,invoice_date
                                                    ,shipto_org
                                                    ,invoice_id
                                                    ,inv_line_number
                                                    ,inv_line_amount
                                                    ,inv_line_qty_invoiced
                                                    ,uom
                                                    ,inv_unit_price
                                                    ,line_location_id
                                                    ,last_updated_by
                                                    ,disputable_flag
                                                    ,line_type_lookup_code
                                                    ,inventory_item_id
                                                    ,inv_item_desc
                                                    ,supplier_item_number
                                                    ,vendor_item_desc
                                                    ,matching_basis
                                                    ,po_number
                                                    ,quantity_overbilled
                                                    ,quantity_not_received
                                                    ,unit_price_variance
                                                    ,amount_overbilled
                                                    ,po_matched
                                                    ,shipment_number
                                                    ,po_line_number
                                                    ,po_line_shipment_qty
                                                    ,po_line_shipment_qty_rcved
                                                    ,po_unit_price
                                                    ,po_line_amount
                                                    ,po_line_item_description
                                                    ,po_line_need_by_date
                                                    ,invoice_line_type
                                                    ,original_amount
                                                    ,original_quantity_invoiced
                                                    ,original_unit_price
                                                    ,po_distribution_id
                                                    ,branch_manager
                                                    ,ship_org_id
                                                    ,notification_id
                                                    ,notification_status
                                                    ,recipient_role
                                                    ,notif_mail_status
                                                    ,notif_begin_date
                                                    ,notif_end_date
                                                    ,notif_due_date
                                                    ,notif_subject
                                                    ,item_number
                                                    ,recepient_name
                                                    ,supplier_name
                                                    ,hdr_invoice_total
                                                    ,second_page_link
                                                    ,wf_item_start_date
                                                    ,branch_man_name
                                                    ,branch_man_role
                                                    ,buyer_name
                                                    ,buyer_role
                                                    ,original_recipient)
            SELECT hold_id
                  ,hold_lookup_code
                  ,hold_reason
                  ,agent_id
                  ,vendor_id
                  ,org_id
                  ,invoice_num
                  ,invoice_date
                  ,shipto_org
                  ,invoice_id
                  ,inv_line_number
                  ,inv_line_amount
                  ,inv_line_qty_invoiced
                  ,uom
                  ,inv_unit_price
                  ,line_location_id
                  ,last_updated_by
                  ,disputable_flag
                  ,line_type_lookup_code
                  ,inventory_item_id
                  ,inv_item_desc
                  ,supplier_item_number
                  ,vendor_item_desc
                  ,matching_basis
                  ,po_number
                  ,quantity_overbilled
                  ,quantity_not_received
                  ,unit_price_variance
                  ,amount_overbilled
                  ,po_matched
                  ,shipment_number
                  ,po_line_number
                  ,po_line_shipment_qty
                  ,po_line_shipment_qty_rcved
                  ,po_unit_price
                  ,po_line_amount
                  ,po_line_item_description
                  ,po_line_need_by_date
                  ,invoice_line_type
                  ,original_amount
                  ,original_quantity_invoiced
                  ,original_unit_price
                  ,po_distribution_id
                  ,branch_manager
                  ,ship_org_id
                  ,notification_id
                  ,notification_status
                  ,recipient_role
                  ,notif_mail_status
                  ,notif_begin_date
                  ,notif_end_date
                  ,notif_due_date
                  ,notif_subject
                  , (SELECT segment1
                       FROM mtl_system_items_b
                      WHERE inventory_item_id = r.inventory_item_id AND organization_id = r.ship_org_id)
                  , (SELECT wfr.display_name
                       FROM wf_roles wfr
                      WHERE wfr.name = r.recipient_role)
                  , (SELECT text_value supplier_name
                       FROM wf_notification_attributes
                      WHERE notification_id = r.notification_id AND name = '#HDR_INVOICE_SUPPLIER_NAME')
                  , (SELECT number_value
                       FROM wf_notification_attributes
                      WHERE notification_id = r.notification_id AND name = '#HDR_INVOICE_TOTAL')
                  , (   '&INVOICEID='
                     || r.invoice_id
                     || '&HOLDID='
                     || r.hold_id
                     || '&NOTIFICATIONCONTEXT=HOLDNEGOTIABLE&ISINTERNAL=Y&ITEMTYPE=APINVHDN&ITEMKEY='
                     || r.hold_id)
                  , (SELECT begin_date
                       FROM wf_items
                      WHERE item_key = TO_CHAR (r.hold_id) AND item_type = 'APINVHDN' AND root_activity = 'HOLD_MAIN')
                  , (SELECT description branch_man_name
                       FROM fnd_user
                      WHERE employee_id = r.branch_manager AND NVL (end_date, SYSDATE + 1440) > SYSDATE)
                  , (SELECT user_name branch_man_role
                       FROM fnd_user
                      WHERE employee_id = r.branch_manager AND NVL (end_date, SYSDATE + 1440) > SYSDATE)
                  , (SELECT description buyer_name
                       FROM fnd_user
                      WHERE employee_id = r.agent_id AND NVL (end_date, SYSDATE + 1440) > SYSDATE)
                  , (SELECT user_name buyer_rolee
                       FROM fnd_user
                      WHERE employee_id = r.agent_id AND NVL (end_date, SYSDATE + 1440) > SYSDATE)
                  , (SELECT orig_recipient
                       FROM xxwc.ap_hold_original_recipient
                      WHERE hold_id = r.hold_id AND ROWNUM = 1)
              FROM apps.xxwc_apinvoicelines_hold r
             WHERE r.notification_id IN (SELECT notification_id
                                           FROM (SELECT notification_id FROM apps.xxwc_apinvoicelines_hold
                                                 MINUS
                                                 SELECT notification_id FROM apps.xxwc_ap_hold_notifications));

        COMMIT;

        -- old workflow notifications should be converted to new

        FOR r IN (SELECT hold_id
                        ,recipient_role new_original_recipient
                        ,'XXWC_APINVHDN' new_recipient
                        ,notification_id
                    FROM apps.xxwc_ap_hold_notifications
                   WHERE original_recipient IS NULL OR original_recipient = 'XXWC_APINVHDN')
        LOOP
            BEGIN
                DELETE FROM xxwc.ap_hold_original_recipient
                      WHERE hold_id = r.hold_id;

                COMMIT;
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            l_role := NULL;

            FOR rx
                IN (SELECT DISTINCT name
                      FROM wf_roles
                     WHERE     orig_system = 'PER'
                           AND orig_system_id IN
                                   (SELECT number_value
                                      FROM wf_notification_attributes
                                     WHERE     name = 'INTERNAL_REP_PERSON_ID'
                                           AND notification_id IN (SELECT notification_id
                                                                     FROM apps.xxwc_ap_hold_notifications
                                                                    WHERE hold_id = r.hold_id AND ROWNUM = 1)))
            LOOP
                l_role := rx.name;
            END LOOP;

            INSERT INTO xxwc.ap_hold_original_recipient
                 VALUES (r.hold_id, l_role, r.new_recipient);

            UPDATE wf_notifications
               SET status = 'OPEN', recipient_role = 'XXWC_APINVHDN'
             WHERE notification_id = r.notification_id;

            UPDATE wf_notification_attributes
               SET text_value = 'XXWC_APINVHDN'
             WHERE name = 'INTERNAL_REP_ROLE' AND notification_id = r.notification_id;

            UPDATE xxwc.xxwc_ap_hold_notifications
               SET original_recipient = l_role, recipient_role = 'XXWC_APINVHDN'
             WHERE hold_id = r.hold_id;

            COMMIT;
        END LOOP;

        DELETE FROM xxwc.xxwc_ap_hold_notifications t
              WHERE t.ROWID NOT IN (SELECT MAX (b.ROWID)
                                      FROM apps.xxwc_ap_hold_notifications b
                                     WHERE b.hold_id = t.hold_id AND b.po_number = t.po_number);

        COMMIT;

        BEGIN
            DELETE FROM xxwc.ap_hold_original_recipient o
                  WHERE NOT EXISTS
                            (SELECT 1
                               FROM apps.xxwc_ap_hold_notifications i
                              WHERE o.hold_id = i.hold_id);

            COMMIT;

            FOR rd IN (  SELECT hold_id
                           FROM xxwc.ap_hold_original_recipient
                       GROUP BY hold_id
                         HAVING COUNT (hold_id) > 1)
            LOOP
                DELETE FROM xxwc.ap_hold_original_recipient t
                      WHERE t.ROWID NOT IN (SELECT MAX (b.ROWID)
                                              FROM xxwc.ap_hold_original_recipient b
                                             WHERE b.hold_id = t.hold_id AND b.hold_id = rd.hold_id);
            END LOOP;

            COMMIT;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        resend_combined_notification;
    END;

    --*************************************************
    PROCEDURE assign_form_attribute (p_notification_id NUMBER, p_recipient VARCHAR2)
    IS
        v_resp_key               VARCHAR2 (123);
        v_new_attribute_string   VARCHAR2 (214);
        v_old_attribute_string   VARCHAR2 (214);
    BEGIN
        FOR r
            IN (SELECT DISTINCT status, notification_id, recipient_role
                  FROM wf_notifications
                 WHERE     MESSAGE_TYPE = 'XXWCAPHN'
                       AND message_name = 'XXWC_APMG'
                       AND notification_id = p_notification_id
                       AND recipient_role = p_recipient)
        LOOP
            wf_notification.setattrtext (nid => r.notification_id, aname => 'HLDFRM', avalue => 'XXWC_AP_HOLD_NOTIF');
            COMMIT;
        END LOOP;
    END;

    --*************************************************
    PROCEDURE assign_resp
    IS
    BEGIN
        FOR ro
            IN (SELECT DISTINCT recipient_role
                  FROM apps.xxwc_apinvoicelines_hold
                 WHERE NOT EXISTS
                               (SELECT 1
                                  FROM fnd_application fa
                                      ,fnd_responsibility fr
                                      ,fnd_user fu
                                      ,fnd_user_resp_groups_all furga
                                      ,fnd_security_groups fsg
                                 WHERE     1 = 1
                                       AND fu.user_name = recipient_role
                                       AND fu.user_id = furga.user_id
                                       AND fa.application_id = fr.application_id
                                       AND furga.responsibility_id = fr.responsibility_id
                                       AND furga.responsibility_application_id = fa.application_id
                                       AND fsg.security_group_id = furga.security_group_id
                                       AND fr.responsibility_key = 'XXWC_INV_HLD_NOTIF'
                                       --  AND furga.end_date IS NULL OR trunc(furga.end_date) > trunc(SYSDATE)
                                       AND NVL (furga.end_date, SYSDATE + 1 / 1440) > SYSDATE))
        LOOP
            BEGIN
                fnd_user_pkg.addresp (username         => ro.recipient_role
                                     ,resp_app         => 'PO'
                                     ,resp_key         => 'XXWC_INV_HLD_NOTIF'
                                     ,security_group   => 'STANDARD'
                                     ,description      => NULL
                                     ,start_date       => SYSDATE
                                     ,end_date         => NULL);
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            --     DBMS_OUTPUT.put_line ('Error while Adding Responsibility: ' || SQLERRM);

            END;

            COMMIT;
        END LOOP;
    END;

    --*************************************************
    PROCEDURE main
    IS
     l_org_id NUMBER;        --Added Org id initialization by Maha for TMS#20141001-00163 
    BEGIN

    l_org_id := MO_GLOBAL.GET_CURRENT_ORG_ID; 
    
    mo_global.set_policy_context('S',l_org_id);
    
    -- assign_resp;

        EXECUTE IMMEDIATE 'truncate table xxwc.xxwc_ap_hold_working ';

        INSERT INTO xxwc.xxwc_ap_hold_working (hold_id
                                              ,hold_lookup_code
                                              ,hold_reason
                                              ,agent_id
                                              ,vendor_id
                                              ,org_id
                                              ,invoice_num
                                              ,invoice_date
                                              ,shipto_org
                                              ,invoice_id
                                              ,inv_line_number
                                              ,inv_line_amount
                                              ,inv_line_qty_invoiced
                                              ,uom
                                              ,inv_unit_price
                                              ,line_location_id
                                              ,last_updated_by
                                              ,disputable_flag
                                              ,line_type_lookup_code
                                              ,inventory_item_id
                                              ,inv_item_desc
                                              ,supplier_item_number
                                              ,vendor_item_desc
                                              ,matching_basis
                                              ,po_number
                                              ,quantity_overbilled
                                              ,quantity_not_received
                                              ,unit_price_variance
                                              ,amount_overbilled
                                              ,po_matched
                                              ,shipment_number
                                              ,po_line_number
                                              ,po_line_shipment_qty
                                              ,po_line_shipment_qty_rcved
                                              ,po_unit_price
                                              ,po_line_amount
                                              ,po_line_item_description
                                              ,po_line_need_by_date
                                              ,invoice_line_type
                                              ,original_amount
                                              ,original_quantity_invoiced
                                              ,original_unit_price
                                              ,po_distribution_id
                                              ,branch_manager
                                              ,ship_org_id
                                              ,notification_id
                                              ,notification_status
                                              ,recipient_role
                                              ,notif_mail_status
                                              ,notif_begin_date
                                              ,notif_end_date
                                              ,notif_due_date
                                              ,notif_subject
                                              ,item_number
                                              ,recepient_name
                                              ,supplier_name
                                              ,hdr_invoice_total
                                              ,second_page_link
                                              ,wf_item_start_date
                                              ,branch_man_name
                                              ,branch_man_role
                                              ,buyer_name
                                              ,buyer_role
                                              ,original_recipient)
            SELECT hold_id
                  ,hold_lookup_code
                  ,hold_reason
                  ,agent_id
                  ,vendor_id
                  ,org_id
                  ,invoice_num
                  ,invoice_date
                  ,shipto_org
                  ,invoice_id
                  ,inv_line_number
                  ,inv_line_amount
                  ,inv_line_qty_invoiced
                  ,uom
                  ,inv_unit_price
                  ,line_location_id
                  ,last_updated_by
                  ,disputable_flag
                  ,line_type_lookup_code
                  ,inventory_item_id
                  ,inv_item_desc
                  ,supplier_item_number
                  ,vendor_item_desc
                  ,matching_basis
                  ,po_number
                  ,quantity_overbilled
                  ,quantity_not_received
                  ,unit_price_variance
                  ,amount_overbilled
                  ,po_matched
                  ,shipment_number
                  ,po_line_number
                  ,po_line_shipment_qty
                  ,po_line_shipment_qty_rcved
                  ,po_unit_price
                  ,po_line_amount
                  ,po_line_item_description
                  ,po_line_need_by_date
                  ,invoice_line_type
                  ,original_amount
                  ,original_quantity_invoiced
                  ,original_unit_price
                  ,po_distribution_id
                  ,branch_manager
                  ,ship_org_id
                  ,notification_id
                  ,notification_status
                  ,recipient_role
                  ,notif_mail_status
                  ,notif_begin_date
                  ,notif_end_date
                  ,notif_due_date
                  ,notif_subject
                  , (SELECT segment1
                       FROM mtl_system_items_b
                      WHERE inventory_item_id = r.inventory_item_id AND organization_id = r.ship_org_id)
                  , (SELECT wfr.display_name
                       FROM wf_roles wfr
                      WHERE wfr.name = r.recipient_role)
                  , (SELECT text_value supplier_name
                       FROM wf_notification_attributes
                      WHERE notification_id = r.notification_id AND name = '#HDR_INVOICE_SUPPLIER_NAME')
                  , (SELECT number_value
                       FROM wf_notification_attributes
                      WHERE notification_id = r.notification_id AND name = '#HDR_INVOICE_TOTAL')
                  , (   '&INVOICEID='
                     || r.invoice_id
                     || '&HOLDID='
                     || r.hold_id
                     || '&NOTIFICATIONCONTEXT=HOLDNEGOTIABLE&ISINTERNAL=Y&ITEMTYPE=APINVHDN&ITEMKEY='
                     || r.hold_id)
                  , (SELECT begin_date
                       FROM wf_items
                      WHERE item_key = TO_CHAR (r.hold_id) AND item_type = 'APINVHDN' AND root_activity = 'HOLD_MAIN')
                  , (SELECT description branch_man_name
                       FROM fnd_user
                      WHERE employee_id = r.branch_manager AND NVL (end_date, SYSDATE + 1 / 1440) > SYSDATE)
                  , (SELECT user_name branch_man_role
                       FROM fnd_user
                      WHERE employee_id = r.branch_manager AND NVL (end_date, SYSDATE + 1 / 1440) > SYSDATE)
                  , (SELECT description buyer_name
                       FROM fnd_user
                      WHERE employee_id = r.agent_id AND NVL (end_date, SYSDATE + 1 / 1440) > SYSDATE)
                  , (SELECT user_name buyer_rolee
                       FROM fnd_user
                      WHERE employee_id = r.agent_id AND NVL (end_date, SYSDATE + 1 / 1440) > SYSDATE)
                  , (SELECT orig_recipient
                       FROM xxwc.ap_hold_original_recipient
                      WHERE hold_id = r.hold_id AND ROWNUM = 1)
              FROM apps.xxwc_apinvoicelines_hold r;

        COMMIT;

        EXECUTE IMMEDIATE 'alter session set DDL_LOCK_TIMEOUT=30';

        EXECUTE IMMEDIATE 'truncate table xxwc.xxwc_ap_hold_notifications ';

        EXECUTE IMMEDIATE 'alter table xxwc.xxwc_ap_hold_notifications nologging';

        INSERT /*+append */
              INTO  xxwc.xxwc_ap_hold_notifications
            SELECT * FROM apps.xxwc_ap_hold_working;

        COMMIT;

        /* FOR r
             IN (SELECT notification_id
                   FROM wf_notifications
                  WHERE     notification_id IN (SELECT notification_id FROM xxwc.xxwc_ap_hold_notifications)
                        AND status = 'OPEN'
                 FOR UPDATE
                     SKIP LOCKED)
         LOOP
             UPDATE wf_notifications
                SET status = 'XXWC', end_date = NULL
              WHERE notification_id = r.notification_id;
         END LOOP;*/
        DELETE FROM xxwc.xxwc_ap_hold_notifications t
              WHERE t.ROWID NOT IN (SELECT MAX (b.ROWID)
                                      FROM apps.xxwc_ap_hold_notifications b
                                     WHERE b.hold_id = t.hold_id AND b.po_number = t.po_number);

        COMMIT;

        resend_combined_notification;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            INSERT INTO xxwc.ap_hold_sent_to_ap_log (error_message, sent_date)
                 VALUES (SUBSTR (l_error_message, 1, 500), SYSDATE);

            DELETE FROM xxwc.ap_hold_sent_to_ap_log
                  WHERE sent_date < SYSDATE - 40;

            COMMIT;
    END;

    /*DECLARE
        v_job   NUMBER;
        v_ex    CLOB;
    BEGIN
        v_ex := ' begin apps.xxwc_ap_hold_notifications_pkg.main; end ;';
        DBMS_JOB.submit (v_job
                        ,v_ex
                        ,SYSDATE
                        ,'sysdate +5/(24*60)'                                                                        --5 min
                                             );
        COMMIT;
    END;*/
    --******************************************
    --*************************************************

    --******************************************

    --******************************************

    PROCEDURE process_sent_to_ap_button (pl_hold_id              NUMBER
                                        ,p_note                  VARCHAR2
                                        ,p_resolution_code       VARCHAR2
                                        ,p_notification_id       NUMBER
                                        ,p_responder             VARCHAR2
                                        ,p_return_status     OUT VARCHAR2)
    IS
    BEGIN
        p_return_status := NULL;

        --RESPOND TO notification
        --  SELECT *  FROM wf_item_attribute_values WHERE item_type = 'APINVHDN' AND item_key = 361713
        -- SELECT * FROM  WF_NOTIFICATION_ATTRIBUTES WHERE notification_id=24500751
        --set all notification attribute
        --first open notification

        FOR r IN (SELECT 1
                    FROM wf_notifications
                   WHERE notification_id = p_notification_id AND status = 'OPEN')
        LOOP
            wf_notification.setattrtext (p_notification_id, 'WF_RESOLUTION_CODE', p_resolution_code);
            wf_notification.setattrtext (p_notification_id, 'WF_NOTE', p_note);
            wf_notification.setattrtext (p_notification_id, 'RESULT', 'RELEASE');
            wf_engine.setitemattrtext ('APINVHDN'
                                      ,pl_hold_id
                                      ,'XXWC_RESOLUTION_CODE'
                                      ,p_resolution_code);
            wf_notification.respond (nid => p_notification_id, responder => p_responder);
            COMMIT;
        END LOOP;

        --refresh driving table - if notification processed

        DELETE FROM xxwc.xxwc_ap_hold_notifications
              WHERE hold_id = pl_hold_id;                                    -- AND notification_id = p_notification_id;

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();
            p_return_status := l_error_message;

            INSERT INTO xxwc.ap_hold_sent_to_ap_log (error_message, sent_date)
                 VALUES (SUBSTR (l_error_message, 1, 500), SYSDATE);

            COMMIT;
    END;

    --******************************************************
    --******************************************************

    PROCEDURE delete_notification (p_notification_id NUMBER)
    IS
    BEGIN
        DELETE FROM wf_notifications
              WHERE notification_id = p_notification_id;

        DELETE FROM wf_notification_attributes
              WHERE notification_id = p_notification_id;

        DELETE FROM wf_comments
              WHERE notification_id = p_notification_id;

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            NULL;
    END;

    --******************************************************
    --******************************************************
    --******************************************************
    --******************************************************

    PROCEDURE resend_combined_notification
    IS
        v_message_body       VARCHAR2 (3900);
        v_old_message_body   VARCHAR2 (3900);
        v_counter            NUMBER := 0;
        v_open_notif         NUMBER := 0;
        v_existing_nid       NUMBER;
        v_notification_id    NUMBER;
        vr_status            VARCHAR2 (16);
    BEGIN
        -- select  hold_id,  orig_recipient, new_recipient From xxwc.ap_hold_original_recipient
        --delete old notifications- resolved ones

        FOR cr
            IN (SELECT status, notification_id, recipient_role
                  FROM wf_notifications
                 WHERE     MESSAGE_TYPE = 'XXWCAPHN'
                       AND message_name = 'XXWC_APMG'
                       AND status IN ('XXWC', 'OPEN')
                       AND recipient_role NOT IN
                               (SELECT DISTINCT rr.orig_recipient
                                  FROM apps.xxwc_ap_hold_notifications o, xxwc.ap_hold_original_recipient rr
                                 WHERE     o.notification_status IN ('XXWC', 'OPEN')
                                       AND o.original_recipient = rr.orig_recipient
                                       AND o.hold_id = rr.hold_id))
        LOOP
            delete_notification (cr.notification_id);
        END LOOP;

        ---***************************************************************************
        COMMIT;

        FOR r1 IN (SELECT DISTINCT rr.orig_recipient recipient_role
                     FROM apps.xxwc_ap_hold_notifications o, xxwc.ap_hold_original_recipient rr
                    WHERE     o.notification_status IN ('XXWC', 'OPEN')
                          AND o.original_recipient = rr.orig_recipient
                          AND o.hold_id = rr.hold_id
                          AND EXISTS
                                  (SELECT 1
                                     FROM wf_roles wr
                                    WHERE wr.name = rr.orig_recipient))
        LOOP
            BEGIN
                --CREATE  MEGGAGE   BODY
                v_message_body := NULL;

                v_message_body := 'Invoices with holds, for Purchase Orders:';

                BEGIN
                    FOR r3
                        IN (  SELECT DISTINCT po_number
                                FROM apps.xxwc_ap_hold_notifications
                               WHERE notification_status IN ('XXWC', 'OPEN') AND original_recipient = r1.recipient_role
                            ORDER BY 1)
                    LOOP
                        v_message_body := SUBSTR ( (v_message_body || r3.po_number || ', '), 1, 3800);
                    END LOOP;

                    v_message_body :=
                        SUBSTR ( (SUBSTR (v_message_body, 1, LENGTH (v_message_body) - 2) || ' require your response.')
                               ,1
                               ,3900);
                -- DBMS_OUTPUT.put_line (v_message_body);
                END;

                v_existing_nid := NULL;

                DECLARE
                    v   NUMBER := 0;
                BEGIN
                    FOR x
                        IN (  SELECT status
                                    ,notification_id
                                    ,recipient_role
                                    ,mail_status
                                    ,end_date
                                FROM wf_notifications
                               WHERE     MESSAGE_TYPE = 'XXWCAPHN'
                                     AND message_name = 'XXWC_APMG'
                                     AND recipient_role = r1.recipient_role
                            ORDER BY status DESC)
                    LOOP
                        v := v + 1;

                        IF v <> 1
                        THEN
                            --delete dups
                            xxwc_ap_hold_notifications_pkg.delete_notification (x.notification_id);
                        ELSE
                            v_existing_nid := x.notification_id;
                            vr_status := x.status;
                        END IF;
                    END LOOP;
                END;

                COMMIT;

                --
                --if notification not found, then send new one
                IF v_existing_nid IS NULL -- OR vr_status <> 'OPEN'                        --user has not combined notification
                THEN
                    v_notification_id := NULL;

                    v_notification_id :=
                        wf_notification.send (role       => r1.recipient_role                                  --to role
                                             ,msg_type   => 'XXWCAPHN' --custom workflow type created for this message purpose
                                             ,msg_name   => 'XXWC_APMG'                                  -- message name
                                             ,priority   => 1);                           -- And set message attributes.
                    wf_notification.setattrdate (nid => v_notification_id, aname => 'START_DATE', avalue => SYSDATE);

                    wf_notification.setattrtext (nid      => v_notification_id
                                                ,aname    => 'SUBJECT'
                                                ,avalue   => 'WC Combined AP invoice hold message');
                    wf_notification.setattrtext (nid => v_notification_id, aname => 'BODY', avalue => v_message_body);

                    wf_notification.setattrtext (nid      => v_notification_id
                                                ,aname    => 'SENDER'
                                                ,avalue   => 'Accounts Payable');
                    assign_form_attribute (v_notification_id, r1.recipient_role);

                    COMMIT;
                --DBMS_OUTPUT.put_line ('Notification has been sent. Notification ID:' || v_notification_id);
                ELSIF v_existing_nid > 0 AND vr_status = 'OPEN'
                THEN
                    assign_form_attribute (v_existing_nid, r1.recipient_role);                                    --????
                    --find old message header
                    v_old_message_body := wf_notification.getattrtext (nid => v_existing_nid, aname => 'BODY');

                    IF NVL (v_old_message_body, 'n') <> NVL (v_message_body, 'c')
                    THEN
                        --Reopen notification if it not open
                        IF NVL (vr_status, 's') <> 'OPEN'
                        THEN
                            FOR t IN (SELECT notification_id
                                        FROM wf_notifications
                                       WHERE notification_id = v_existing_nid AND NVL (status, 's') <> 'OPEN'
                                      FOR UPDATE
                                          SKIP LOCKED)
                            LOOP
                                UPDATE wf_notifications                                           -- CUSTOM notification
                                   SET status = 'OPEN', end_date = NULL
                                 WHERE notification_id = v_existing_nid;
                            END LOOP;

                            COMMIT;
                        END IF;

                        wf_notification.setattrdate (nid => v_existing_nid, aname => 'START_DATE', avalue => SYSDATE);

                        wf_notification.setattrtext (nid      => v_existing_nid
                                                    ,aname    => 'SUBJECT'
                                                    ,avalue   => 'WC Combined AP invoice hold message');
                        wf_notification.setattrtext (nid => v_existing_nid, aname => 'BODY', avalue => v_message_body);

                        wf_notification.setattrtext (nid      => v_existing_nid
                                                    ,aname    => 'SENDER'
                                                    ,avalue   => 'Accounts Payable');
                        assign_form_attribute (v_existing_nid, r1.recipient_role);

                        --resend email to user

                        xxwc_resent_notif_one_email (v_existing_nid);
                    END IF;
                END IF;

                COMMIT;

                FOR t
                    IN (SELECT notification_id
                          FROM wf_notifications
                         WHERE     notification_id IN (NVL (v_notification_id, 0), NVL (v_existing_nid, 0))
                               AND status <> 'OPEN'
                        FOR UPDATE
                            SKIP LOCKED)
                LOOP
                    UPDATE wf_notifications                                                       -- CUSTOM notification
                       SET status = 'OPEN', end_date = NULL
                     WHERE notification_id = t.notification_id;
                END LOOP;

                COMMIT;
            EXCEPTION
                WHEN OTHERS
                THEN
                    l_error_message :=
                           'Errors inside loop recipient_role ='
                        || r1.recipient_role
                        || 'Error_Stack...'
                        || DBMS_UTILITY.format_error_stack ()
                        || ' Error_Backtrace...'
                        || DBMS_UTILITY.format_error_backtrace ();

                    INSERT INTO xxwc.ap_hold_sent_to_ap_log (error_message, sent_date)
                         VALUES (SUBSTR (l_error_message, 1, 500), SYSDATE);

                    COMMIT;
            END;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            INSERT INTO xxwc.ap_hold_sent_to_ap_log (error_message, sent_date)
                 VALUES (SUBSTR (l_error_message, 1, 500), SYSDATE);

            COMMIT;
    END;

    --******************************************************
    --******************************************************
    --******************************************************
    --******************************************************
    --**********************

    PROCEDURE transfer (p_notification_id NUMBER, p_new_role VARCHAR2, p_comments VARCHAR2)
    IS
        l_hist_id    NUMBER;
        v_comments   VARCHAR2 (240);
        v_run        CLOB;
    BEGIN
        FOR rt IN (SELECT b.name, b.orig_system_id person_id, b.display_name display_name
                     FROM wf_roles b
                    WHERE b.name = p_new_role AND ROWNUM = 1)
        LOOP
            v_comments := 'Reassign to ' || rt.display_name || ' from ';
        END LOOP;

        --*******************
        FOR r IN (SELECT a.hold_id
                        , (SELECT org_id
                             FROM ap_holds
                            WHERE hold_id = a.hold_id)
                             org_id
                        ,a.invoice_id
                        ,a.original_recipient
                        ,b.orig_system_id person_id
                        ,b.display_name display_name
                        ,a.inv_line_number line_number
                    FROM apps.xxwc_ap_hold_notifications a, wf_roles b
                   WHERE a.notification_id = p_notification_id AND a.original_recipient = b.name)
        LOOP
            v_comments := SUBSTR (v_comments || r.display_name || ': ' || p_comments, 1, 240);

            SELECT ap_inv_aprvl_hist_s.NEXTVAL INTO l_hist_id FROM DUAL;

            INSERT INTO ap_inv_aprvl_hist_all (approval_history_id
                                              ,history_type
                                              ,invoice_id
                                              ,iteration
                                              ,response
                                              ,approver_id
                                              ,approver_name
                                              ,created_by
                                              ,creation_date
                                              ,last_update_date
                                              ,last_updated_by
                                              ,last_update_login
                                              ,org_id
                                              ,amount_approved
                                              ,hold_id
                                              ,line_number
                                              ,approver_comments
                                              ,notification_order)
                 VALUES (l_hist_id
                        ,'HOLDAPPROVAL'                                                        --p_hist_rec.HISTORY_TYPE
                        ,r.invoice_id                                                            --p_hist_rec.INVOICE_ID
                        ,NULL                                                                     --p_hist_rec.ITERATION
                        ,'SENT'                                                                    --p_hist_rec.RESPONSE
                        ,r.person_id                                                            --p_hist_rec.APPROVER_ID
                        ,r.display_name                                                       --p_hist_rec.APPROVER_NAME
                       -- ,NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1)                    -- p_hist_rec.created_by
                        ,NVL (TO_NUMBER(fnd_global.user_id), -1)
                        ,SYSDATE                                                              --p_hist_rec.CREATION_DATE
                        ,SYSDATE                                                           --p_hist_rec.LAST_UPDATE_DATE
                        --,NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1)                --p_hist_rec.LAST_UPDATED_BY
                         ,NVL (TO_NUMBER(fnd_global.user_id), -1)
                        --,NVL (TO_NUMBER (fnd_profile.VALUE ('LOGIN_ID')), -1)            ---p_hist_rec.LAST_UPDATE_LOGIN
                         ,NVL (TO_NUMBER(fnd_global.login_id), -1)
                        ,r.org_id                                                                    --p_hist_rec.ORG_ID
                        ,'0'
                        ,r.hold_id                                                                  --p_hist_rec.HOLD_ID
                        ,NULL                                                                   --p_hist_rec.LINE_NUMBER
                        ,v_comments                                                       --p_hist_rec.approver_comments
                        ,NULL                                                            --p_hist_rec.NOTIFICATION_ORDER
                             );
        END LOOP;

        FOR ry IN (SELECT b.orig_system_id, b.display_name
                     FROM wf_roles b
                    WHERE name = p_new_role)
        LOOP
            IF ry.orig_system_id IS NOT NULL
            THEN
                wf_notification.setattrnumber (p_notification_id, 'INTERNAL_REP_PERSON_ID', ry.orig_system_id);
            END IF;
        END LOOP;

        FOR ri IN (SELECT notification_id, original_recipient, item_key
                     FROM wf_notifications
                    WHERE notification_id = p_notification_id AND status = 'OPEN')
        LOOP
            BEGIN
                UPDATE wf_notifications
                   SET original_recipient = p_new_role
                 WHERE notification_id = p_notification_id;

                UPDATE xxwc.ap_hold_original_recipient
                   SET orig_recipient = p_new_role
                 WHERE hold_id = ri.item_key;
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;
        END LOOP;

        COMMIT;

        --********************
        DELETE FROM xxwc.xxwc_ap_hold_notifications
              WHERE notification_id = p_notification_id;

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'nid='
                || p_notification_id
                || ' role='
                || p_new_role
                || 'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            INSERT INTO xxwc.ap_hold_sent_to_ap_log (error_message, sent_date)
                 VALUES (SUBSTR (l_error_message, 1, 500), SYSDATE);

            COMMIT;
    END;

    ------

    PROCEDURE get_approver (itemtype    IN            VARCHAR2
                           ,itemkey     IN            VARCHAR2
                           ,actid       IN            NUMBER
                           ,funcmode    IN            VARCHAR2
                           ,resultout      OUT NOCOPY VARCHAR2)
    IS
        l_invoice_id          NUMBER;
        l_hold_id             NUMBER;
        l_next_approver       ame_util.approverrecord;
        l_api_name   CONSTANT VARCHAR2 (200) := 'Get_Approver';
        l_debug_info          VARCHAR2 (2000);
        l_org_id              NUMBER;
        l_role                VARCHAR2 (50);
        l_role_display        VARCHAR2 (150);
        l_display_name        VARCHAR2 (150);
        l_hist_id             ap_inv_aprvl_hist.approval_history_id%TYPE;
        l_name                wf_users.name%TYPE;                                                          --bug 8620671
        l_hist_rec            ap_inv_aprvl_hist%ROWTYPE;
        l_iteration           NUMBER;
        l_notf_iteration      NUMBER;
        v_line_location_id    NUMBER;
    BEGIN
        l_org_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'ORG_ID');

        l_invoice_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'INVOICE_ID');

        l_hold_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'HOLD_ID');
        l_iteration := wf_engine.getitemattrnumber (itemtype, itemkey, 'ITERATION');

        l_notf_iteration := wf_engine.getitemattrnumber (itemtype, itemkey, 'NOTF_ITERATION');

        --find line_location_id
        FOR r IN (SELECT NVL (line_location_id, 0) line_location_id
                    FROM ap_holds
                   WHERE hold_id = l_hold_id)
        LOOP
            v_line_location_id := r.line_location_id;
        END LOOP;

        l_debug_info :=
               l_api_name
            || ': get variables from workflow'
            || ', l_invoice_id = '
            || l_invoice_id
            || ', l_hold_id = '
            || l_hold_id
            || ', l_org_id = '
            || l_org_id;

        IF (g_level_statement >= g_current_runtime_level)
        THEN
            fnd_log.string (g_level_statement, g_module_name || l_api_name, l_debug_info);
        END IF;

        --get the next approver

        ame_api.getnextapprover (applicationidin     => 200
                                ,transactiontypein   => 'APHLD'
                                ,transactionidin     => TO_CHAR (l_hold_id)
                                ,nextapproverout     => l_next_approver);
        l_debug_info := l_api_name || ': after call to ame';

        IF (g_level_statement >= g_current_runtime_level)
        THEN
            fnd_log.string (g_level_statement, g_module_name || l_api_name, l_debug_info);
        END IF;

        IF l_next_approver.approval_status = ame_util.exceptionstatus
        THEN
            l_debug_info := 'Error in AME_API.getNextApprover call';

            IF (g_level_statement >= g_current_runtime_level)
            THEN
                fnd_log.string (g_level_statement, g_module_name || l_api_name, l_debug_info);
            END IF;

            app_exception.raise_exception;
        END IF;

        IF l_next_approver.person_id IS NULL
        THEN                                                                                 /*no approver on the list*/
            -- added for bug 8671976
            UPDATE ap_holds_all
               SET wf_status = 'TERMINATED'
             WHERE hold_id = l_hold_id;

            -- added for bug 8671976
            resultout := wf_engine.eng_completed || ':' || 'N';
        ELSE
            IF v_line_location_id = 0
            THEN
                wf_directory.getrolename ('PER'
                                         ,l_next_approver.person_id
                                         ,l_role
                                         ,l_role_display);
                wf_directory.getusername ('PER'
                                         ,l_next_approver.person_id
                                         ,l_name
                                         ,l_display_name);
                wf_engine.setitemattrtext (itemtype
                                          ,itemkey
                                          ,'INTERNAL_REP_ROLE'
                                          ,l_role);

                wf_engine.setitemattrtext (itemtype
                                          ,itemkey
                                          ,'ORIG_SYSTEM'
                                          ,'PER');

                wf_engine.setitemattrtext (itemtype
                                          ,itemkey
                                          ,'INTERNAL_REP_DISPLAY_NAME'
                                          ,l_display_name);

                wf_engine.setitemattrnumber (itemtype
                                            ,itemkey
                                            ,'INTERNAL_REP_PERSON_ID'
                                            ,l_next_approver.person_id);
            ELSIF v_line_location_id > 0
            THEN                                                                                        -- have approver
                wf_directory.getrolename ('PER'
                                         ,l_next_approver.person_id
                                         ,l_role
                                         ,l_role_display);
                wf_directory.getusername ('PER'
                                         ,l_next_approver.person_id
                                         ,l_name
                                         ,l_display_name);
                wf_engine.setitemattrtext (itemtype
                                          ,itemkey
                                          ,'INTERNAL_REP_ROLE'
                                          ,'XXWC_APINVHDN');
                -- ,l_role);
                --dummy role for combined notifications
                wf_engine.setitemattrtext (itemtype
                                          ,itemkey
                                          ,'REP_ROLE_COPY'
                                          ,'XXWC_APINVHDN');

                wf_engine.setitemattrtext (itemtype
                                          ,itemkey
                                          ,'ORIG_SYSTEM'
                                          ,'PER');

                wf_engine.setitemattrtext (itemtype
                                          ,itemkey
                                          ,'INTERNAL_REP_DISPLAY_NAME'
                                          ,l_display_name);

                wf_engine.setitemattrnumber (itemtype
                                            ,itemkey
                                            ,'INTERNAL_REP_PERSON_ID'
                                            ,l_next_approver.person_id);

                BEGIN
                    DELETE FROM xxwc.ap_hold_original_recipient
                          WHERE hold_id = l_hold_id;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                END;

                IF l_role = 'XXWC_APINVHDN'
                THEN
                    FOR rx
                        IN (SELECT DISTINCT name
                              FROM wf_roles
                             WHERE     orig_system = 'PER'
                                   AND orig_system_id IN
                                           (SELECT number_value
                                              FROM wf_notification_attributes
                                             WHERE     name = 'INTERNAL_REP_PERSON_ID'
                                                   AND notification_id IN (SELECT notification_id
                                                                             FROM apps.xxwc_ap_hold_notifications
                                                                            WHERE hold_id = l_hold_id AND ROWNUM = 1)))
                    LOOP
                        l_role := rx.name;
                    END LOOP;
                END IF;

                IF l_role IS NOT NULL
                THEN
                    INSERT INTO xxwc.ap_hold_original_recipient
                         VALUES (l_hold_id, l_role, 'XXWC_APINVHDN');

                    COMMIT;
                END IF;
            END IF;

            --Now set the environment
            mo_global.init ('SQLAP');
            mo_global.set_policy_context ('S', l_org_id);

            l_hist_rec.history_type := 'HOLDAPPROVAL';
            l_hist_rec.invoice_id := l_invoice_id;
            l_hist_rec.iteration := l_iteration;
            l_hist_rec.response := 'SENT';
            l_hist_rec.approver_id := l_next_approver.person_id;
            l_hist_rec.approver_name := l_display_name;
            --l_hist_rec.created_by := NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1);
            l_hist_rec.created_by := NVL (TO_NUMBER (fnd_global.user_id), -1);
            l_hist_rec.creation_date := SYSDATE;
            l_hist_rec.last_update_date := SYSDATE;
           -- l_hist_rec.last_updated_by := NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1);
            l_hist_rec.last_updated_by := NVL (TO_NUMBER (fnd_global.user_id), -1);
            --l_hist_rec.last_update_login := NVL (TO_NUMBER (fnd_profile.VALUE ('LOGIN_ID')), -1);
            l_hist_rec.last_update_login := NVL (TO_NUMBER (fnd_global.login_id), -1);
            l_hist_rec.org_id := l_org_id;
            l_hist_rec.amount_approved := 0;
            l_hist_rec.hold_id := l_hold_id;
            l_hist_rec.notification_order := l_notf_iteration;

            ap_workflow_pkg.insert_history_table (p_hist_rec => l_hist_rec);

            resultout := wf_engine.eng_completed || ':' || 'Y';
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            wf_core.context ('APINVHDN'
                            ,'GET_APPROVER'
                            ,itemtype
                            ,itemkey
                            ,TO_CHAR (actid)
                            ,funcmode);
            RAISE;
    END get_approver;

    ---

    PROCEDURE watch_the_job
    IS
        v_broken   VARCHAR2 (1) := 'Y';
        v_job      NUMBER;
        r_job      NUMBER;
        v_ex       CLOB := 'begin apps.xxwc_ap_hold_notifications_pkg.refresh_main; end;';
    BEGIN
        FOR r IN (SELECT broken, job
                    FROM user_jobs
                   WHERE what LIKE '%apps.xxwc_ap_hold_notifications_pkg.refresh_main;%')
        LOOP
            v_broken := r.broken;
            v_job := r.job;
        END LOOP;

        IF v_broken = 'Y' AND v_job IS NOT NULL
        THEN
            DBMS_JOB.remove (v_job);
            COMMIT;
            DBMS_JOB.submit (r_job
                            ,v_ex
                            ,SYSDATE
                            ,'sysdate +1/(24*120)'                                                               --5 min
                                                  );
            COMMIT;
        ELSIF v_broken = 'Y' AND v_job IS NULL
        THEN
            DBMS_JOB.submit (r_job
                            ,v_ex
                            ,SYSDATE
                            ,'sysdate +1/(24*120)'                                                               --5 min
                                                  );
            COMMIT;
        END IF;
    END;

    /*
    DECLARE
         v_job   NUMBER;
         v_ex    CLOB;
     BEGIN
         v_ex := ' begin apps.xxwc_ap_hold_notifications_pkg.watch_the_job; end ;';
         DBMS_JOB.submit (v_job
                         ,v_ex
                         ,SYSDATE
                         ,'sysdate +1/(24)'                                                                        --5 min
                                              );
         COMMIT;
     END;*/
    PROCEDURE close_notications
    IS
    BEGIN
        FOR r IN (SELECT notification_id
                    FROM wf_notifications
                   WHERE MESSAGE_TYPE = 'XXWCAPHN' AND message_name = 'XXWC_APMG')
        LOOP
            wf_notification.close (nid => r.notification_id);
        END LOOP;

        COMMIT;
    END;

    -----****************************************************************
    --will resent one email
    PROCEDURE xxwc_resent_notif_one_email (p_nid IN NUMBER)
    IS
    BEGIN
        -- DELETE FROM wf_notification_out
        FOR r
            IN (SELECT msg_id, notification_id
                  FROM (SELECT msg_id
                              ,o.enq_time
                              ,o.deq_time
                              ,msg_state
                              , (SELECT str_value
                                   FROM TABLE (o.user_data.header.properties)
                                  WHERE name = 'NOTIFICATION_ID')
                                   notification_id
                              , (SELECT str_value
                                   FROM TABLE (o.user_data.header.properties)
                                  WHERE name = 'ROLE')
                                   role
                              , (SELECT str_value
                                   FROM TABLE (o.user_data.header.properties)
                                  WHERE name = 'Q_CORRELATION_ID')
                                   corrid
                          FROM applsys.aq$wf_notification_out o) enq_time
                 WHERE     msg_state = 'PROCESSED'
                       AND deq_time IS NOT NULL
                       AND enq_time IS NOT NULL
                       AND corrid = 'XXWCAPHN:XXWC_APMG'
                       AND notification_id = p_nid)
        LOOP
            DELETE FROM wf_notification_out
                  WHERE msgid = r.msg_id;
        END LOOP;

        COMMIT;

        -- DELETE FROM  wf_deferred
        FOR r IN (SELECT msgid
                    FROM (SELECT TO_NUMBER ( (SELECT VALUE
                                                FROM TABLE (d.user_data.parameter_list)
                                               WHERE name = 'NOTIFICATION_ID'))
                                     nid
                                ,d.msgid
                            FROM wf_deferred d
                           WHERE     (SELECT VALUE
                                        FROM TABLE (d.user_data.parameter_list)
                                       WHERE name = 'Q_CORRELATION_ID') = 'XXWCAPHN:XXWC_APMG'
                                 AND d.state >= 2
                                 AND deq_time IS NOT NULL
                                 AND enq_time IS NOT NULL)
                   WHERE nid = p_nid)
        LOOP
            DELETE FROM wf_deferred
                  WHERE msgid = r.msgid;
        END LOOP;

        COMMIT;

        BEGIN
            UPDATE wf_notifications                                                               -- CUSTOM notification
               SET mail_status = NULL
             WHERE notification_id = p_nid AND status = 'OPEN';

            COMMIT;
            wf_notification.resend (p_nid);
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   l_error_message
                || ' '
                || 'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            IF l_req_id > 0
            THEN
                fnd_file.put_line (
                    fnd_file.LOG
                   , (   'Error running '
                      || 'xxwc_ap_hold_notifications_pkg'
                      || '.'
                      || 'xxwc_resent_notif_email'
                      || ' '
                      || l_error_message));
            ELSE
                DBMS_OUTPUT.put_line (
                       'Error running '
                    || 'xxwc_ap_hold_notifications_pkg'
                    || '.'
                    || 'xxwc_resent_notif_email'
                    || ' '
                    || l_error_message);
            END IF;
    END;

    ---*****************************************************************
    --will resent all active notifications emails
    PROCEDURE xxwc_resent_all_notif_email (p_errbuf OUT NOCOPY VARCHAR2, p_retcode OUT NOCOPY VARCHAR2) --corrid = 'XXWCAPHN:XXWC_APMG'
    IS
    BEGIN
        -- DELETE FROM wf_notification_out
        DELETE FROM wf_notification_out
              WHERE msgid IN
                        (SELECT msg_id
                           FROM (SELECT msg_id
                                       ,o.enq_time
                                       ,o.deq_time
                                       ,msg_state
                                       , (SELECT str_value
                                            FROM TABLE (o.user_data.header.properties)
                                           WHERE name = 'NOTIFICATION_ID')
                                            notification_id
                                       , (SELECT str_value
                                            FROM TABLE (o.user_data.header.properties)
                                           WHERE name = 'ROLE')
                                            role
                                       , (SELECT str_value
                                            FROM TABLE (o.user_data.header.properties)
                                           WHERE name = 'Q_CORRELATION_ID')
                                            corrid
                                   FROM applsys.aq$wf_notification_out o) enq_time
                          WHERE     corrid = 'XXWCAPHN:XXWC_APMG'
                                AND msg_state = 'PROCESSED'
                                AND deq_time IS NOT NULL
                                AND enq_time IS NOT NULL
                                AND corrid = 'XXWCAPHN:XXWC_APMG');

        COMMIT;

        -- DELETE FROM  wf_deferred
        EXECUTE IMMEDIATE ' TRUNCATE TABLE xxwc.xxwc_ap_hold_deferred';

        EXECUTE IMMEDIATE ' alter TABLE xxwc.xxwc_ap_hold_deferred nologging';

        -- CREATE TABLE  xxwc.xxwc_ap_hold_deferred    AS
        INSERT /*+APPEND*/
              INTO  xxwc.xxwc_ap_hold_deferred
            SELECT TO_NUMBER ( (SELECT VALUE
                                  FROM TABLE (d.user_data.parameter_list)
                                 WHERE name = 'NOTIFICATION_ID'))
                       nid
                  ,msgid
              FROM wf_deferred d
             WHERE (SELECT VALUE
                      FROM TABLE (d.user_data.parameter_list)
                     WHERE name = 'Q_CORRELATION_ID') = 'XXWCAPHN:XXWC_APMG';

        COMMIT;

        DELETE FROM wf_deferred
              WHERE msgid IN (SELECT a.msgid
                                FROM xxwc.xxwc_ap_hold_deferred a, wf_notifications b
                               WHERE a.nid = b.notification_id(+) AND b.mail_status(+) = 'SENT');

        COMMIT;

        FOR cr
            IN (SELECT status, notification_id, recipient_role
                  FROM wf_notifications
                 WHERE     MESSAGE_TYPE = 'XXWCAPHN'
                       AND message_name = 'XXWC_APMG'
                       AND status IN ('XXWC', 'OPEN')
                       AND recipient_role IN
                               (SELECT DISTINCT rr.orig_recipient
                                  FROM apps.xxwc_ap_hold_notifications o, xxwc.ap_hold_original_recipient rr
                                 WHERE     o.notification_status IN ('XXWC', 'OPEN')
                                       AND o.original_recipient = rr.orig_recipient
                                       AND o.hold_id = rr.hold_id))
        LOOP
            BEGIN
                UPDATE wf_notifications                                                           -- CUSTOM notification
                   SET mail_status = NULL
                 WHERE notification_id = cr.notification_id AND status = 'OPEN';
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            COMMIT;
            wf_notification.resend (cr.notification_id);
        END LOOP;

        p_retcode := 0;
        p_errbuf := NULL;
    EXCEPTION
        WHEN OTHERS
        THEN
            p_retcode := 2;
            l_error_message :=
                   l_error_message
                || ' '
                || 'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            p_errbuf := l_error_message;

            IF l_req_id > 0
            THEN
                fnd_file.put_line (
                    fnd_file.LOG
                   , (   'Error running '
                      || 'xxwc_ap_hold_notifications_pkg'
                      || '.'
                      || 'xxwc_resent_all_notif_email'
                      || ' '
                      || l_error_message));
            ELSE
                DBMS_OUTPUT.put_line (
                       'Error running '
                    || 'xxwc_ap_hold_notifications_pkg'
                    || '.'
                    || 'xxwc_resent_all_notif_email'
                    || ' '
                    || l_error_message);
            END IF;
    END;
END;
/

-- End of DDL Script for Package Body APPS.XXWC_AP_HOLD_NOTIFICATIONS_PKG
