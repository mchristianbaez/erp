CREATE OR REPLACE PACKAGE BODY XXWC_ASL_CONV_PKG
/********************************************************************************

FILE NAME: APPS.XXWC_ASL_CONV_PKG.pkg

PROGRAM TYPE: PL/SQL Package Body

PURPOSE: API to load Approved Supplier List into Oracle.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     02/10/2013    Gopi Damuluri    Initial version.
********************************************************************************/
AS

   -- global error handling variables
   g_err_callfrom        VARCHAR2 (75) := 'XXWC_ASL_CONV_PKG.Main';
   g_err_callpoint       VARCHAR2 (75) := 'START';
   g_distro_list         VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_message             VARCHAR2 (2000);

PROCEDURE main (p_errbuf       OUT VARCHAR2,
                p_retcode      OUT VARCHAR2,
                p_validate_only IN VARCHAR2)
IS
   CURSOR cur_validate is
   SELECT x1.rowid row_id
        , x1.*
     FROM xxwc.xxwc_asl_conv_stg x1
    WHERE NVL(x1.status,'N') IN ('N','E');

   CURSOR cur_supplier_sites (p_vendor_id number) is
   SELECT assa.vendor_site_id
     FROM ap_supplier_sites_all assa
    WHERE assa.vendor_id = p_vendor_id
      AND nvl(assa.purchasing_site_flag,'N') = 'Y';

   CURSOR cur_load_asl is
   SELECT x1.rowid row_id
        , x1.*
     FROM xxwc.xxwc_asl_conv_stg x1
    WHERE x1.status = 'V';

   v_organization_id               number;
   v_operating_unit_id             number;
   v_inventory_item_id             number;
   v_vendor_id                     number;
   v_vendor_site_id                number;
   v_owner_org_id                  number;
   v_count                         number;
   v_existing_asl                  number;
   v_user_id                       NUMBER;
   v_bpa_id                        NUMBER;
   v_bpa_line_id                   NUMBER;
   v_user_name                     VARCHAR2(10) := 'SYSADMIN';
   v_resp_id                       NUMBER;
   v_resp_appl_id                  NUMBER;
   v_responsibility                VARCHAR2(30) := 'LB Purchasing Superuser';

   l_err_flag                      VARCHAR2(1);
   l_err_msg                       VARCHAR2(2000);

begin

/*
     begin
       select user_id
         into v_user_id
         from fnd_user
        where user_name = v_user_name;
     exception
            when others then
                   v_user_id := -1;
     end;


    begin
            select responsibility_id, application_id
            into   v_resp_id, v_resp_appl_id
            from   fnd_responsibility_tl
            where  responsibility_name = v_responsibility
            and    language = 'US';

    exception
            when others then
                   v_resp_id := 50644;
                   v_resp_appl_id := 201;
    end;

--    fnd_global.APPS_INITIALIZE(v_user_id, v_resp_id, v_resp_appl_id);
*/

    DBMS_OUTPUT.PUT_LINE ('Creating Approved Suppliers List');
    DBMS_OUTPUT.PUT_LINE ('==============================================');
    DBMS_OUTPUT.PUT_LINE ('Initiating validations...');

    v_bpa_id            := null;
    FOR c1 IN cur_validate LOOP

      l_err_flag    := NULL;
      l_err_msg     := NULL;

        exit when cur_validate%notfound;

        v_organization_id   := null;
        v_inventory_item_id := null;
        v_vendor_id         := null;
        v_vendor_site_id    := null;
        v_owner_org_id      := null;
        v_bpa_line_id       := null;

     --   if c1.asl_type = 'ITEM' then

            --------------------------------------------------------------------------
            -- Check if item exists in Inventory Org
            --------------------------------------------------------------------------
            BEGIN
              SELECT msib.organization_id
                   , msib.inventory_item_id
                INTO v_organization_id
                   , v_inventory_item_id
                FROM mtl_system_items_b msib
                   , mtl_parameters     mp
               WHERE msib.segment1        = c1.item_number
                 AND msib.organization_id = mp.organization_id
                 AND mp.organization_code = LPAD(TRIM(c1.organization_code),3,'0');
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'ITEM_ORG_ERR';
            WHEN OTHERS THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'OTHER_ITEM_ORG_ERR';
            END;

            --------------------------------------------------------------------------
            -- Check if item has sufficient attirbutes to create ASL
            --------------------------------------------------------------------------
            BEGIN
              SELECT msib.organization_id
                   , msib.inventory_item_id
                INTO v_organization_id
                   , v_inventory_item_id
                FROM mtl_system_items_b                      msib
                   , mtl_parameters                          mp
               WHERE msib.segment1                         = c1.item_number
                 AND msib.organization_id                  = mp.organization_id
                 AND nvl(msib.purchasing_enabled_flag,'N') = 'Y'
                 AND nvl(msib.purchasing_item_flag, 'N')   = 'Y'
                 AND mp.organization_code                  = LPAD(TRIM(c1.organization_code),3,'0');
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'ITEM_BUY_FG_ERR';
            WHEN OTHERS THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'OTHR_ITEM_BUY_FG_ERR';
            END;

            --------------------------------------------------------------------------
            -- Validate Inventory Organization
            --------------------------------------------------------------------------
            BEGIN
              SELECT ood.organization_id
                INTO v_owner_org_id
                FROM org_organization_definitions ood
               WHERE ood.organization_code             = LPAD(TRIM(c1.organization_code),3,'0')
                 AND NVL(TRUNC(ood.user_definition_enable_date),TRUNC(sysdate)) <= TRUNC(sysdate)
                 AND NVL(TRUNC(ood.disable_date),trunc(sysdate)) >= TRUNC(sysdate);
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'INV_ORG_ERR';
            WHEN OTHERS THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'OTHR_INV_ORG_ERR';
            END;

            --------------------------------------------------------------------------
            -- Validate Vendor Details
            --------------------------------------------------------------------------
            BEGIN
              SELECT asa.vendor_id
                INTO v_vendor_id
                FROM ap_suppliers asa
               WHERE upper(asa.vendor_name) = upper(c1.supplier_name);
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'VENDOR_ERR';
            WHEN OTHERS THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'OTHR_VENDOR_ERR';
            END;

            --------------------------------------------------------------------------
            -- Validate Vendor Site Details
            --------------------------------------------------------------------------
            BEGIN
              SELECT vendor_site_id
                INTO v_vendor_site_id
                FROM ap_supplier_sites_all assa
               WHERE assa.vendor_id = v_vendor_id
                 AND UPPER(assa.vendor_site_code) = UPPER(c1.supplier_site_code)
                 AND org_id = 162;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'INVALID_SUP_ERR';
            WHEN TOO_MANY_ROWS THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'MULT_SUP_ERR';
            WHEN OTHERS THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'OTHR_SUP_ERR';
            END;

            IF v_bpa_id IS NULL THEN
            --------------------------------------------------------------------------
            -- Validate Blanket Purchasing Agreement
            --------------------------------------------------------------------------
            BEGIN
              SELECT po_header_id
                INTO v_bpa_id
                FROM po_headers_all pha
               WHERE 1 = 1
                 AND segment1 = UPPER(c1.BLANKET_PA_NUM)
                 AND org_id = 162;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'INVALID_BPA';
            WHEN TOO_MANY_ROWS THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'MULT_BPA_ERR';
            WHEN OTHERS THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'OTHR_BPA_ERR';
            END;
            END IF;

            --------------------------------------------------------------------------
            -- Derive Blanket Purchasing Agreement for the line
            --------------------------------------------------------------------------
            BEGIN
              SELECT po_line_id
                INTO v_bpa_line_id
                FROM po_lines_all pha
               WHERE 1 = 1
                 AND po_header_id      = v_bpa_id
                 AND item_id           = v_inventory_item_id
                 AND org_id            = 162;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'INVALID_BPA_LINE';
            WHEN TOO_MANY_ROWS THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'MULT_BPA_LINE_ERR';
            WHEN OTHERS THEN
                l_err_flag := 'Y';
                l_err_msg  := l_err_msg||' ~ '||'OTHR_BPA_LINE_ERR';
            END;

            IF l_err_flag = 'Y' THEN
            --------------------------------------------------------------------------
            -- Update staging table with error details
            --------------------------------------------------------------------------

               UPDATE xxwc.xxwc_asl_conv_stg
                  SET error_message     = l_err_msg
                    , status            = 'E'
                WHERE rowid             = c1.row_id;
            ELSE
            --------------------------------------------------------------------------
            -- Update staging table that the record is valid.
            --------------------------------------------------------------------------

               UPDATE xxwc.xxwc_asl_conv_stg
                  SET organization_id   = v_organization_id
                    , owning_org_id     = v_organization_id
                    , inventory_item_id = v_inventory_item_id
                    , vendor_site_id    = v_vendor_site_id
                    , vendor_id         = v_vendor_id
                    , bpa_id            = v_bpa_id
                    , bpa_line_id       = v_bpa_line_id
                    , status            = 'V'
                WHERE rowid             = c1.row_id;
            END IF;
    END LOOP;

    COMMIT;

    DBMS_OUTPUT.PUT_LINE ('Creating ASLs...');
    v_count := 0;
    for c4 in cur_load_asl
    loop
        exit when cur_load_asl%notfound;

        v_existing_asl := 0;
            --------------------------------------------------------------------------
            -- Check if ASL already exists
            --------------------------------------------------------------------------
        BEGIN
           SELECT COUNT (*)
             INTO v_existing_asl
             FROM po_approved_supplier_list
            WHERE 1 = 1
              AND using_organization_id       = c4.organization_id
              AND item_id                     = c4.inventory_item_id
              AND vendor_id                   = c4.vendor_id
              AND vendor_site_id              = c4.vendor_site_id
              AND owning_organization_id      = c4.owning_org_id
              AND UPPER(vendor_business_type) = 'DIRECT'
              AND asl_status_id               = (SELECT status_id
                                                   FROM po_asl_statuses
                                                  WHERE upper(status) = 'NEW'
                                                    AND rownum = 1);
        EXCEPTION
          WHEN OTHERS THEN
            v_existing_asl := 0;
        END;

        IF v_existing_asl = 0
        THEN

            --------------------------------------------------------------------------
            -- Create Approved Supplier List
            --------------------------------------------------------------------------
           INSERT INTO po.po_approved_supplier_list (
                        asl_id,
                        using_organization_id,
                        owning_organization_id,
                        vendor_business_type,
                        asl_status_id,
                        vendor_id,
                        vendor_site_id,
                        item_id,
                        primary_vendor_item,
                        last_update_date,
                        last_updated_by,
                        creation_date,
                        created_by,
                        last_update_login)
                 VALUES ( po.po_approved_supplier_list_s.NEXTVAL                         -- asl_id
                        , c4.organization_id                                             -- using_organization_id
                        , c4.owning_org_id                                               -- owning_organization_id
                        , 'DIRECT'                                                       -- vendor_business_type
                        , (select status_id from po_asl_statuses where upper(status) = 'NEW' and rownum = 1)         -- asl_status_id
                        , c4.vendor_id                                                   -- vendor_id
                        , c4.vendor_site_id                                              -- vendor_site_id
                        , c4.inventory_item_id                                           -- item_id
                        , substr(c4.supplier_item,1,25)                                  -- primary_vendor_item
                        , SYSDATE                                                        -- last_update_date
                        , -1                                                             -- last_updated_by
                        , SYSDATE                                                        -- creation_date
                        , -1                                                             -- created_by
                        , -1                                                             -- last_update_login
            );

            --------------------------------------------------------------------------
            -- Populate ASL Attributes
            --------------------------------------------------------------------------
           INSERT INTO po.po_asl_attributes (
                        asl_id,
                        using_organization_id,
                        last_update_date,
                        last_updated_by,
                        creation_date,
                        created_by,
                        document_sourcing_method,
                        enable_plan_schedule_flag,
                        enable_ship_schedule_flag,
                        enable_autoschedule_flag,
                        enable_authorizations_flag,
                        vendor_id,
                        vendor_site_id,
                        item_id,
                        last_update_login,
                        enable_vmi_flag,
                        enable_vmi_auto_replenish_flag,
                        consigned_from_supplier_flag,
                        consume_on_aging_flag
            ) VALUES (
                        po.po_approved_supplier_list_s.CURRVAL                  -- ASL_ID
                        , c4.organization_id                                    -- USING_ORGANIZATION_ID
                        , SYSDATE                                               -- LAST_UPDATE_DATE
                        , -1                                                    -- LAST_UPDATED_BY
                        , SYSDATE                                               -- CREATION_DATE
                        , -1                                                    -- CREATED_BY
                        , 'ASL'                                                 -- DOCUMENT_SOURCING_METHOD
                        , 'N'                                                   -- ENABLE_PLAN_SCHEDULE_FLAG
                        , 'N'                                                   -- ENABLE_SHIP_SCHEDULE_FLAG
                        , 'N'                                                   -- ENABLE_AUTOSCHEDULE_FLAG
                        , 'N'                                                   -- ENABLE_AUTHORIZATIONS_FLAG
                        , c4.vendor_id                                          -- VENDOR_ID
                        , c4.vendor_site_id                                     -- VENDOR_SITE_ID
                        , c4.inventory_item_id                                  -- ITEM_ID
                        , -1                                                    -- LAST_UPDATE_LOGIN
                        , 'N'                                                   -- ENABLE_VMI_FLAG
                        , 'N'                                                   -- ENABLE_VMI_AUTO_REPLENISH_FLAG
                        , 'Y'                                                   -- CONSIGNED_FROM_SUPPLIER_FLAG
                        , 'N'                                                   -- CONSUME_ON_AGING_FLAG
            );

            --------------------------------------------------------------------------
            -- Create ASL Document details
            --------------------------------------------------------------------------
           INSERT INTO po.po_asl_documents (asl_id
                                          , using_organization_id
                                          , sequence_num
                                          , document_type_code
                                          , document_header_id
                                          , document_line_id
                                          , last_update_date
                                          , last_updated_by
                                          , creation_date
                                          , created_by
                                          , last_update_login
                                          , org_id)
                                    VALUES (po.po_approved_supplier_list_s.CURRVAL -- asl_id
                                          , c4.organization_id                     -- using_organization_id
                                          , '1'                                    -- sequence_num
                                          , 'BLANKET'                              -- document_type_code
                                          , c4.bpa_id                              -- document_header_id
                                          , c4.bpa_line_id                         -- document_line_id
                                          , SYSDATE                                -- last_update_date
                                          , -1                                     -- last_updated_by
                                          , SYSDATE                                -- creation_date
                                          , -1                                     -- created_by
                                          , -1                                     -- last_update_login
                                          , 162                                    -- org_id
                                          );

            v_count := v_count + 1;

            --------------------------------------------------------------------------
            -- Update staging table the record is successfully processed.
            --------------------------------------------------------------------------
            UPDATE xxwc.xxwc_asl_conv_stg x1
               SET asl_id = po.po_approved_supplier_list_s.CURRVAL
                 , status = 'S'
             WHERE rowid  = c4.row_id;

        else

            update  xxwc.xxwc_asl_conv_stg x1
            set     status = 'A'
            where   rowid = c4.row_id;

        end if;

    end loop;

    DBMS_OUTPUT.PUT_LINE ('Defined '||v_count||' ASLs.');

    commit;

   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom
           ,p_calling             => g_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => g_message
           ,p_distribution_list   => g_distro_list
           ,p_module              => 'XXWC');
 END;

END XXWC_ASL_CONV_PKG;

/
