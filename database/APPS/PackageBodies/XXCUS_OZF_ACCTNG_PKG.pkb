CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_ACCTNG_PKG IS
  --    Ver        ESMS / RFC                Date         Author              Description
  --    ---------  ----------                -----------  ----------------    -------------------------------
  --    1.0                                 05/17/2012    Chandra Gadge        1. Created this package body.
  --    2.0       ESMS 239557 /RFC 39911    04/01/2014    Balaguru Seshadri    2. New routines to fetch product and location segments
  --    3.0       ESMS 246904 /RFC 40169    04/17/2014    Balaguru Sesahdri    3. Oracle standard view ar_receipt_method_accouts is not pulling the correct
  --                                                                             org_id based record due to some issue with the MO operating unit setup.We will replace the view with a base table instead.
  --    Notes:
  PROCEDURE printlog(p_message IN VARCHAR2) IS
  BEGIN
    apps.fnd_file.put_line(apps.fnd_file.log, p_message);
    --dbms_output.put_line(p_message);
  END printlog;
  --
  PROCEDURE printoutput(p_message IN VARCHAR2) IS
  BEGIN
    apps.fnd_file.put_line(apps.fnd_file.output, p_message);
    -- dbms_output.put_line(p_message);
  END printoutput;
  --
  FUNCTION create_ccid(p_concat_segs IN VARCHAR2, p_sob_id IN NUMBER)
    RETURN NUMBER IS
    l_concat_segs   VARCHAR2(240) := p_concat_segs; --75.21879.0.505301.0.0.0
    l_keyval_status BOOLEAN;
    l_coa_id        NUMBER;
    l_ccid          NUMBER;
  BEGIN
    BEGIN
      SELECT chart_of_accounts_id
        INTO l_coa_id
        FROM gl_sets_of_books
       WHERE set_of_books_id = p_sob_id; --fnd_profile.VALUE_SPECIFIC('GL_SET_OF_BKS_ID',NULL,50623,682);
    EXCEPTION
      WHEN no_data_found THEN
        printlog('Chart of Accounts ID not found from profile option GL_SET_OF_BKS_ID');
        printlog('Try setting up your environment with fnd_global.apps_initialize');
        RAISE;
    END;
    -- keyval_mode can be one of CREATE_COMBINATION CHECK_COMBINATION FIND_COMBINATION
    -- create will only work if dynamic inserts on and cross validation rules not broken
    l_keyval_status := fnd_flex_keyval.validate_segs('CREATE_COMBINATION'
                                                    ,'SQLGL'
                                                    ,'GL#'
                                                    ,l_coa_id
                                                    ,l_concat_segs
                                                    ,'V'
                                                    ,SYSDATE
                                                    ,'ALL'
                                                    ,NULL
                                                    ,NULL
                                                    ,NULL
                                                    ,NULL
                                                    ,FALSE
                                                    ,FALSE
                                                    ,NULL
                                                    ,NULL
                                                    ,NULL);
    IF l_keyval_status
    THEN
      SELECT gcv.code_combination_id
        INTO l_ccid
        FROM gl_code_combinations_kfv gcv
       WHERE gcv.padded_concatenated_segments = l_concat_segs;
      printoutput('S - ' || l_ccid);
    ELSE
      l_ccid := -1;
      printoutput('F - ' || l_ccid);
    END IF;
  
    RETURN l_ccid;
  EXCEPTION
    WHEN no_data_found THEN
      printlog('Unexpected Error');
      l_ccid := -1;
      RETURN l_ccid;
  END create_ccid;

  --------------------------------------------------------------------------------
  -- Copyright 2012 HD Supply  Inc (Orlando, FL) - All rights reserved
  --
  --
  --    NAME:       XXCUS_OZF_ACCTNG_PKG.pck
  --
  --    REVISIONS:
  --    Ver        Date        Author           Description
  --    ---------  ----------  ---------------  -------------------------------
  --    1.0        05/17/2012  Chandra Gadge    1. Created this package.
  --    1.1        12/26/2013  Kathy Poling     Changes to dervive adjustment
  --                                            accounting for SLA from checkbook
  --                                            SR 234229 RFC 39026
  --    Overview: This package contains procedure for derviving the account in SLA 
  ---------------------------------------------------------------------------------
  ---------------------------------------------------------------------------------

  FUNCTION get_income_acct(p_utilization_id IN NUMBER) RETURN NUMBER IS
    l_acc_rec_acct    VARCHAR2(250);
    l_rec_ccid        NUMBER;
    l_create_rec_ccid NUMBER;
    l_product         VARCHAR2(3);
    --p_utilization_id  NUMBER;
    l_income_ccid      NUMBER;
    l_sob_id           NUMBER;
    l_adj_bu_id        NUMBER; --Verison 1.1
    l_adj_us_location  VARCHAR2(30); --Verison 1.1
    l_adj_cad_location VARCHAR2(30); --Verison 1.1
    l_adj_us_product   VARCHAR2(30); --Verison 1.1
    l_adj_cad_product  VARCHAR2(30); --Verison 1.1   
    -- Get Default Income Account from Sys Parameters ----
  
    CURSOR get_dflt_inc_acct(p_utilization_id IN NUMBER) IS
      SELECT gl_id_accr_promo_liab, set_of_books_id
        FROM ozf_sys_parameters_all sys, ozf_funds_utilized_all_b ofu
       WHERE ofu.utilization_id = p_utilization_id
         AND sys.org_id = ofu.org_id;
  
    --Get the Product ,Locationa and BU--
  
    CURSOR get_prod_loc_acct(v_utilization_id IN NUMBER) IS
    --Verison 1.1  change to get account for adjustment at checkbook
    /*SELECT orl.line_attribute11 product,
                   orl.line_attribute12 location,
                   orl.end_cust_party_id BU_ID
            FROM   ozf_funds_utilized_all_b ofu,
                   ozf_resale_lines_all orl
            WHERE  ofu.object_id = orl.resale_line_id
            AND    ofu.utilization_id = v_utilization_id;
            */
      SELECT (CASE
               WHEN orl.line_attribute11 IS NULL AND ofu.org_id = 101 THEN
                l_adj_us_product
               WHEN orl.line_attribute11 IS NULL AND ofu.org_id = 102 THEN
                l_adj_cad_product
               ELSE
                orl.line_attribute11
             END) product
            ,(CASE
               WHEN orl.line_attribute12 IS NULL AND ofu.org_id = 101 THEN
                l_adj_us_location
               WHEN orl.line_attribute12 IS NULL AND ofu.org_id = 102 THEN
                l_adj_cad_location
               ELSE
                orl.line_attribute12
             END) location
            ,(CASE
               WHEN orl.end_cust_party_id IS NULL THEN
               l_adj_bu_id  
               ELSE
                orl.end_cust_party_id
             END) bu_id
        FROM ozf_funds_utilized_all_b ofu, ozf_resale_lines_all orl
       WHERE ofu.object_id = orl.resale_line_id(+)
         AND ofu.utilization_id = v_utilization_id;
  
    --Get the location,product,Cost Center and  Natural account for the BU--   
    CURSOR cost_natural_acct_csr(v_bu_id IN NUMBER) IS
      SELECT hp.attribute8  cost_center
            ,hp.attribute6  coop_natural_acct
            ,hp.attribute7  rebate_natural_acct
            ,hp.attribute5  deflt_loc_segment
            ,hp.attribute11 coop_flag
        FROM hz_parties hp
       WHERE party_id = v_bu_id; ----BU ID
  
    ---Get the General Acct,project,cost center,future use,future use2---
    CURSOR get_segment_values_csr(p_ccid IN NUMBER) IS
      SELECT segment5 project_code
            ,segment6 future_use
            ,segment7 future_use2
        FROM apps.gl_code_combinations_kfv glc
       WHERE code_combination_id = p_ccid;
  
    l_cost_center  VARCHAR2(20);
    l_account      VARCHAR2(20);
    l_project_code VARCHAR2(20);
    l_future_use   VARCHAR2(20);
    l_future_use2  VARCHAR2(20);
    l_location     VARCHAR2(20);
    l_bu_id        NUMBER;
    l_coop_acct    VARCHAR2(20);
    l_rebate_acct  VARCHAR2(20);
    l_coop_flag    VARCHAR2(1);
    l_default_loc  VARCHAR2(20);
    l_offr_type    VARCHAR2(50);
    l_loc_acct     VARCHAR2(20);
    l_acct_seg_val VARCHAR2(100);
    l_natural_acct VARCHAR2(240);
  
    ----
  
    CURSOR get_offr_type_csr(v_utilization_id IN NUMBER) IS
      SELECT amv.description
        FROM apps.ams_media_vl        amv
            ,ozf_offers               oof
            ,ozf_funds_utilized_all_b ofu
       WHERE amv.media_id = oof.activity_media_id
         AND oof.qp_list_header_id = ofu.plan_id
         AND ofu.utilization_id = v_utilization_id;
  
    -----Get the Code Combination ID-----------
    CURSOR csr_get_ccid(p_code_combination IN VARCHAR2) IS
      SELECT glc.code_combination_id
        FROM apps.gl_code_combinations_kfv glc
       WHERE glc.concatenated_segments = p_code_combination;
  
    CURSOR csr_get_ccid1(p_location IN VARCHAR2
                        ,p_account  IN VARCHAR2
                        ,p_project  IN VARCHAR2
                        ,p_cost_ctr IN VARCHAR2
                        ,p_product  IN VARCHAR2
                        ,p_future1  IN VARCHAR2
                        ,p_future2  IN VARCHAR2) IS
      SELECT glc.code_combination_id
        FROM apps.gl_code_combinations_kfv glc
       WHERE glc.segment2 = p_location
         AND glc.segment4 = p_account
         AND glc.segment5 = p_project
         AND glc.segment3 = p_cost_ctr
         AND glc.segment1 = p_product
         AND glc.segment6 = p_future1
         AND glc.segment7 = p_future2;
  
    l_acct_ccid NUMBER;
  
  BEGIN
  
    --Version 1.1 lookups for adjustments done at checkbook
    BEGIN
      SELECT meaning
        INTO l_adj_bu_id
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_OZF_ACCTG_PARMS'
         AND lookup_code = 'BU_ID'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
        l_adj_bu_id := NULL;
    END;
  
    BEGIN
      SELECT meaning
        INTO l_adj_us_location
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_OZF_ACCTG_PARMS'
         AND lookup_code = 'US_ACCT'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
        l_adj_us_location := NULL;
    END;
  
    BEGIN
      SELECT meaning
        INTO l_adj_cad_location
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_OZF_ACCTG_PARMS'
         AND lookup_code = 'CAD_ACCT'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
        l_adj_cad_location := NULL;
    END;
  
    BEGIN
      SELECT meaning
        INTO l_adj_us_product
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_OZF_ACCTG_PARMS'
         AND lookup_code = 'US_PRODUCT'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
        l_adj_us_product := NULL;
    END;
  
    BEGIN
      SELECT meaning
        INTO l_adj_cad_product
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_OZF_ACCTG_PARMS'
         AND lookup_code = 'CAD_PRODUCT'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
        l_adj_cad_product := NULL;
    END;
    --Version 1.1 end lookups   
  
    ---
    OPEN get_prod_loc_acct(p_utilization_id);
    FETCH get_prod_loc_acct
      INTO l_product, l_location, l_bu_id;
    CLOSE get_prod_loc_acct;
  
    ---
    OPEN cost_natural_acct_csr(l_bu_id);
    FETCH cost_natural_acct_csr
      INTO l_cost_center
          ,l_coop_acct
          ,l_rebate_acct
          ,l_default_loc
          ,l_coop_flag;
    CLOSE cost_natural_acct_csr;
  
    OPEN get_dflt_inc_acct(p_utilization_id);
    FETCH get_dflt_inc_acct
      INTO l_income_ccid, l_sob_id;
    CLOSE get_dflt_inc_acct;
  
    OPEN get_segment_values_csr(l_income_ccid);
    FETCH get_segment_values_csr
      INTO l_project_code, l_future_use, l_future_use2;
    CLOSE get_segment_values_csr;
  
    --PrintLog('l_project_code' || l_project_code);
    --PrintLog('l_future_use' || l_future_use);
    --PrintLog('l_future_use2' || l_future_use2);
    ----
  
    OPEN get_offr_type_csr(p_utilization_id);
    FETCH get_offr_type_csr
      INTO l_offr_type;
    CLOSE get_offr_type_csr;
  
    --PrintLog('l_offr_type' || l_offr_type);
    --PrintLog('l_Utilication' || p_utilization_id);  
  
    IF l_offr_type IN ('COOP', 'COOP_MIN')
    THEN
      l_natural_acct := l_coop_acct;
      IF nvl(l_coop_flag, 'N') = 'Y' OR l_product = '11'
      THEN
        l_loc_acct := l_default_loc;
      ELSE
        l_loc_acct := l_location;
      END IF;
    ELSE
      l_natural_acct := l_rebate_acct;
      IF l_product = '11'
      THEN
        l_loc_acct := l_default_loc;
      ELSE
        l_loc_acct := l_location;
      END IF;
    END IF;
  
    l_acct_seg_val := l_product || '.' || l_loc_acct || '.' ||
                      l_cost_center || '.' || l_natural_acct || '.' ||
                      l_project_code || '.' || l_future_use || '.' ||
                      l_future_use2;
  
    printoutput('l_acct_seg_val' || l_acct_seg_val);
  
    --Get the code_combination_id for the Rev_acct
    IF l_acct_seg_val IS NOT NULL
    THEN
    
      OPEN csr_get_ccid1(l_loc_acct
                        ,l_natural_acct
                        ,l_project_code
                        ,l_cost_center
                        ,l_product
                        ,l_future_use
                        ,l_future_use2);
      FETCH csr_get_ccid1
        INTO l_acct_ccid;
      CLOSE csr_get_ccid1;
    
      --printOutput('l_acct_ccid' || l_acct_ccid);
      --printOutput('SOB' || l_sob_id);
    
      IF l_acct_ccid IS NULL
      THEN
        l_acct_ccid := apps.xxcus_ozf_acctng_pkg.create_ccid(p_concat_segs => l_acct_seg_val
                                                            ,p_sob_id      => l_sob_id);
      END IF;
    END IF;
  
    printoutput('l_acct_ccid' || l_acct_ccid);
  
    -- return the ccid or -1 when nothing found
  
    IF l_acct_ccid IS NOT NULL
    THEN
      RETURN l_acct_ccid;
    ELSE
      RETURN - 1;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      printoutput('CCID Other Error');
      RETURN - 1;
  END get_income_acct;

  FUNCTION get_account_segment(p_cash_receipt_id IN NUMBER) RETURN VARCHAR2 IS
    l_cash_receipt_id NUMBER := p_cash_receipt_id;
    l_bu_id           NUMBER;
    l_segment         VARCHAR2(50) :=Null;       
    l_party_name      hz_parties.party_name%type :=Null;
    l_cash_loc_seg    gl_code_combinations.segment2%type :=Null;
    l_cash_prod_seg   gl_code_combinations.segment1%type :=Null;
    --
    CURSOR c_get_lob (p_cash_receipt_id IN NUMBER) IS
    select /*+ RESULT_CACHE */ 
         arcr.attribute1
        ,gcc.segment1
        ,gcc.segment2    
    from  ar_cash_receipts_all           arcr
         ,ar_receipt_method_accounts_all arma
         ,gl_code_combinations_kfv       gcc
    where 1 =1
      and cash_receipt_id             =p_cash_receipt_id
      and arma.receipt_method_id(+)   =arcr.receipt_method_id
      and arma.org_id(+)              =arcr.org_id
      and gcc.code_combination_id(+)  =arma.cash_ccid;
    --
    CURSOR c_lob_details (p_bu_id IN NUMBER) IS
      SELECT /*+ RESULT_CACHE */ 
             hp.party_name  lob_name
            ,hp.attribute4  def_prod_seg
            ,hp.attribute5  def_loc_seg
            ,NVL(hp.attribute12, 'N') pmt_override
            ,hp.attribute13 def_pmt_account
            ,hp.attribute10 ic_receivable_acc
        FROM apps.hz_parties hp
       WHERE hp.party_id = p_bu_id;
  --       
   l_lob_details c_lob_details%rowtype :=Null;
  -- 
  BEGIN
  
    OPEN c_get_lob (l_cash_receipt_id);
    FETCH c_get_lob
      INTO l_bu_id, l_cash_prod_seg, l_cash_loc_seg;
    CLOSE c_get_lob;
  
    IF l_bu_id IS NOT NULL THEN
     --
     BEGIN 
     --
      OPEN c_lob_details (l_bu_id);
      FETCH c_lob_details INTO l_lob_details;
      CLOSE c_lob_details;
      --
      -- (l_lob_details.lob_name IN ('ELECTRICAL', 'UTILISERV', 'WHITECAP', 'WHITECAP CONSTRUCTION')) AND 
      IF ((l_lob_details.pmt_override ='Y') ) THEN
       l_segment :=l_lob_details.def_pmt_account;
      ELSE
       l_segment :=l_lob_details.ic_receivable_acc;
       -- What this means is we are going to use the existing intercompany receivable account  [attribute10] from hz_parties for that LOB
      END IF;
      --    
     EXCEPTION
      WHEN OTHERS THEN
       l_party_name :=Null;
       l_segment :=Null;
     END;
     --
    END IF;
  
    RETURN l_segment;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END get_account_segment;
  --
  FUNCTION get_prod_segment (p_cash_receipt_id IN NUMBER) RETURN VARCHAR2 IS
    l_cash_receipt_id NUMBER := p_cash_receipt_id;
    l_bu_id           NUMBER;
    l_segment         VARCHAR2(50) :=Null;       
    l_party_name      hz_parties.party_name%type :=Null;
    l_cash_loc_seg    gl_code_combinations.segment2%type :=Null;
    l_cash_prod_seg   gl_code_combinations.segment1%type :=Null;
    --
    CURSOR c_get_lob (p_cash_receipt_id IN NUMBER) IS
    select /*+ RESULT_CACHE */ 
         arcr.attribute1
        ,gcc.segment1
        ,gcc.segment2    
    from  ar_cash_receipts_all            arcr
         ,ar_receipt_method_accounts_all  arma
         ,gl_code_combinations_kfv        gcc
    where 1 =1
      and cash_receipt_id             =p_cash_receipt_id
      and arma.receipt_method_id(+)   =arcr.receipt_method_id
      and arma.org_id(+)              =arcr.org_id      
      and gcc.code_combination_id(+)  =arma.cash_ccid;
    --
    CURSOR c_lob_details (p_bu_id IN NUMBER) IS
      SELECT /*+ RESULT_CACHE */ 
             hp.party_name  lob_name
            ,hp.attribute4  def_prod_seg
            ,hp.attribute5  def_loc_seg
            ,NVL(hp.attribute12, 'N') pmt_override
            ,hp.attribute13 def_pmt_account
            ,hp.attribute10 ic_receivable_acc
        FROM apps.hz_parties hp
       WHERE hp.party_id = p_bu_id;
  --       
   l_lob_details c_lob_details%rowtype :=Null;
  -- 
  BEGIN
  
    OPEN c_get_lob (l_cash_receipt_id);
    FETCH c_get_lob
      INTO l_bu_id, l_cash_prod_seg, l_cash_loc_seg;
    CLOSE c_get_lob;
  
    IF l_bu_id IS NOT NULL THEN
     --
     BEGIN 
     --
      OPEN c_lob_details (l_bu_id);
      FETCH c_lob_details INTO l_lob_details;
      CLOSE c_lob_details;
      --
      -- (l_lob_details.lob_name IN ('ELECTRICAL', 'UTILISERV', 'WHITECAP', 'WHITECAP CONSTRUCTION')) AND 
      IF ( (l_lob_details.pmt_override ='Y') ) THEN
       l_segment :=l_lob_details.def_prod_seg;
      ELSE
       l_segment :=l_cash_prod_seg;
       -- What this means is we are going to use the existing value of product from the cash account using the receipt payment method setup
      END IF;
      --    
     EXCEPTION
      WHEN OTHERS THEN
       l_party_name :=Null;
       l_segment :=Null;
     END;
     --
    END IF;
  
    RETURN l_segment;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END get_prod_segment;  
  -- 
  FUNCTION get_loc_segment (p_cash_receipt_id IN NUMBER) RETURN VARCHAR2 IS
    l_cash_receipt_id NUMBER := p_cash_receipt_id;
    l_bu_id           NUMBER;
    l_segment         VARCHAR2(50) :=Null;       
    l_party_name      hz_parties.party_name%type :=Null;
    l_cash_loc_seg    gl_code_combinations.segment2%type :=Null;
    l_cash_prod_seg   gl_code_combinations.segment1%type :=Null;
    --
    CURSOR c_get_lob (p_cash_receipt_id IN NUMBER) IS
    select /*+ RESULT_CACHE */ 
         arcr.attribute1
        ,gcc.segment1
        ,gcc.segment2    
    from  ar_cash_receipts_all            arcr
         ,ar_receipt_method_accounts_all  arma
         ,gl_code_combinations_kfv        gcc
    where 1 =1
      and cash_receipt_id             =p_cash_receipt_id
      and arma.receipt_method_id(+)   =arcr.receipt_method_id
      and arma.org_id(+)              =arcr.org_id      
      and gcc.code_combination_id(+)  =arma.cash_ccid;
    --
    CURSOR c_lob_details (p_bu_id IN NUMBER) IS
      SELECT /*+ RESULT_CACHE */ 
             hp.party_name  lob_name
            ,hp.attribute4  def_prod_seg
            ,hp.attribute14  def_loc_seg
            ,NVL(hp.attribute12, 'N') pmt_override
            ,hp.attribute13 def_pmt_account
            ,hp.attribute10 ic_receivable_acc
        FROM apps.hz_parties hp
       WHERE hp.party_id = p_bu_id;
  --       
   l_lob_details c_lob_details%rowtype :=Null;
  -- 
  BEGIN
  
    OPEN c_get_lob (l_cash_receipt_id);
    FETCH c_get_lob
      INTO l_bu_id, l_cash_prod_seg, l_cash_loc_seg;
    CLOSE c_get_lob;
  
    IF l_bu_id IS NOT NULL THEN
     --
     BEGIN 
     --
      OPEN c_lob_details (l_bu_id);
      FETCH c_lob_details INTO l_lob_details;
      CLOSE c_lob_details;
      --
      -- (l_lob_details.lob_name IN ('ELECTRICAL', 'UTILISERV', 'WHITECAP', 'WHITECAP CONSTRUCTION')) AND
      IF ( (l_lob_details.pmt_override ='Y') ) THEN
       l_segment :=l_lob_details.def_loc_seg;
      ELSE
       l_segment :=l_cash_loc_seg;
       -- What this means is we are going to use the existing value of location from the cash account using the receipt payment method setup
      END IF;
      --    
     EXCEPTION
      WHEN OTHERS THEN
       l_party_name :=Null;
       l_segment :=Null;
     END;
     --
    END IF;
  
    RETURN l_segment;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END get_loc_segment;
  --  
END xxcus_ozf_acctng_pkg;
/
