CREATE OR REPLACE package body APPS.XXWC_PER_CLOSE_PEN_TRANS_PKG as
/*+
 -- Date: 26-FEB-2013 
 -- Author: Dheeresh Chintala 
 -- Scope: TThis package is to run Period Close Pending Transactions Report for multiple inventory organizations.
 -- TMS ticket 
 -- Update History: 
 -- ESMS ticket:224510   Date:10/22/2013 
 -- Version     Updated by          Update Date     Task (TMS or ESMS) 
 -- -------     ------------        ------------    ------------------  
 -- Ver 1.1     Dheeresh Chintala   22-OCT-2013     TMS-20131029-00408 - Period Close Pending Transactions Report - not sending email attachments
 -- Ver 1.2     Manjula Chellappan  07-Nov-2014     TMS-20141001-00254 - MultiOrg Changes
 --
 --
 -- Parameters
*/

 -- Global variables
 G_Application    Fnd_Application.Application_Short_Name%Type :='BOM';
 G_Cp_Short_Code  Fnd_Concurrent_Programs.Concurrent_Program_Name%Type :='CSTPNTXN'; --Period Close Reconcialiation Report

  procedure main (
    retcode               out number
   ,errbuf                out VARCHAR2,
   --p_period              IN  NUMBER --removed LHS
   p_region               in VARCHAR2, --added Ver 1.1   
   p_period              in  VARCHAR2   
  ) is
    
  --- ****** Ver 1.1 Start ****** 
    CURSOR wc_inv_orgs (n_org_id IN NUMBER) IS
          SELECT ood.organization_id,
             ood.organization_code,
             ood.organization_name,
             ood.set_of_books_id,
             ood.chart_of_accounts_id,
             ood.legal_entity,
             mp.attribute9 region
        FROM org_organization_definitions ood,
             mtl_parameters mp
       WHERE 1 = 1         
         AND ood.operating_unit = n_org_id 
         AND ood.disable_date IS NULL
--TMS-20141001-00254 - MultiOrg Changes By Manjula Chellappan on 7-Nov-2014 
         AND ood.organization_code NOT IN (fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION') , 'MST') --WC Corporate Organization, WC Item Master
         --AND organization_code = '710'
         AND ood.organization_id = mp.organization_id
         AND mp.attribute9 = nvl(p_region,mp.attribute9)
    ORDER BY ood.organization_code ASC;
--- ****** Ver 1.1 Start ****** and commented below cursor query 

      /*SELECT organization_id,
             organization_code,
             organization_name,
             set_of_books_id,
             chart_of_accounts_id,
             legal_entity
        FROM org_organization_definitions
       WHERE 1 = 1         
         AND operating_unit =n_org_id 
         AND disable_date IS NULL
         AND organization_code NOT IN ('WCC', 'MST') --WC Corporate Organization, WC Item Master
         --AND rownum <3    --testing for upto 7 organizations 
    ORDER BY organization_code ASC;*/
    
    type wc_inv_orgs_tbl is table of wc_inv_orgs%rowtype index by binary_integer;
    wc_inv_orgs_rec wc_inv_orgs_tbl;        
  
   lc_request_data VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   n_count_child_requests NUMBER :=0; 
   N_conc_pgm_id NUMBER :=0;
   N_running_count NUMBER :=0;
   b_keep_looping BOOLEAN;
   n_program_app_id NUMBER :=0;
   N_user_id NUMBER :=0;
   v_email       fnd_user.email_address%type :=Null;
   v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null; 
   v_cp_phase       Varchar2(40) :=Null;
   v_cp_status      Varchar2(40) :=Null; 
   v_child_requests Varchar2(240) :=Null;
   
   --v_resolution_type    Varchar2(50) :='All'; --added LHS
   v_resolution_type    NUMBER :=1; --1 = All --removed LHS
   --v_transaction_type    Varchar2(50) :='All'; --removed LHS
   v_transaction_type    NUMBER :=1; --1 = All --Added LHS
   
   output_file_id   UTL_FILE.FILE_TYPE;
   v_path varchar2(80);
   p_period_id NUMBER :=0;
   
   l_layout boolean; --added LHS
   
  begin  
  
   lc_request_data :=fnd_conc_global.request_data;
   
   n_req_id :=fnd_global.conc_request_id;
 
   if lc_request_data is null then     
       
       lc_request_data :='1';
  
       l_org_id :=mo_global.get_current_org_id;
      
       --n_req_id :=fnd_global.conc_request_id;     
       
       N_user_id :=fnd_global.user_id; 
       
       /* Begin
         Insert Into xxcus.xxcus_month_endclose_requests 
             (
               request_id
              ,creation_date
              ,output_emailed
              ,insert_source
             )
         Values 
             (
               n_req_id
              ,sysdate
              ,'N'
              ,'XXWC_PER_CLOSE_PEN_TRANS_PKG.main'
             );
        End;        
       */
       fnd_file.put_line(fnd_file.log, '');              
       fnd_file.put_line(fnd_file.log, 'l_org_id ='||l_org_id);
       fnd_file.put_line(fnd_file.log, 'n_user_id ='||n_user_id);
       fnd_file.put_line(fnd_file.log, '');  

--TMS-20141001-00254 - MultiOrg Changes By Manjula Chellappan on 7-Nov-2014 
      MO_GLOBAL.set_policy_context('S',l_org_id);

    
      
      Open wc_inv_orgs(l_org_id);
      Fetch wc_inv_orgs Bulk Collect Into wc_inv_orgs_rec;
      Close wc_inv_orgs;    
      
      If wc_inv_orgs_rec.count =0 Then
      
       fnd_file.put_line(fnd_file.log,'No active inventory organizations found for operating unit WhiteCap');
       
      Else --wc_inv_orgs_rec.count >0 
      
         -- We will kick off for every single inventory orgs and not wait to check how many are running at a time
          for indx in 1 .. wc_inv_orgs_rec.count loop  
          
               Begin
                select acct_period_id
                into   p_period_id
                from   org_acct_periods
                where  1 =1
                  and  organization_id =wc_inv_orgs_rec(Indx).organization_id
                  and  period_name =p_period;
                  --and open_flag ='Y'; --Commented on 1/9/2013. ESMS ticket 184365
               Exception
                When NO_Data_Found Then
                 p_period_id :=0;                
                When Too_Many_Rows Then 
                 p_period_id :=0;               
                When Others Then
                 p_period_id :=0;
               End; 
               
               fnd_file.put_line(fnd_file.log, 'org ='||wc_inv_orgs_rec(indx).organization_name||', p_period_id ='||p_period_id);
         
                --
                -- If the concurrent program requires operating unit as a parameter then uncomment the below call
                -- fnd_request.set_org_id(mo_global.get_current_org_id);
                --          
                           
                --Except for arguments 1, 2 and 7 all other arguments are defaulted based on the parameter values
                --established from a sample run by the end user
              
              --Added LHS
              l_layout :=
         fnd_request.add_layout (template_appl_name      => 'BOM',
                                 template_code           => 'CSTPNTXNT',
                                 template_language       => 'ENG',
                                 template_territory      => 'US',
                                 output_format           => 'PDF' --'RTF'
                                );
                                                 
                ln_request_id :=fnd_request.submit_request
                      (
                       application      =>G_Application,
                       program          =>G_Cp_Short_Code,
                       description      =>'',
                       start_time       =>'',
                       sub_request      =>TRUE,
                       argument1        =>wc_inv_orgs_rec(Indx).organization_id, --Organization Id
                       argument2        =>p_period_id, --Chart of Accounts Id
                       argument3        =>v_resolution_type, --Report Type
                       argument4        =>v_transaction_type --Sort Option                                                                                                                                                                                                                                                                           
                      ); 
                      
                  if ln_request_id >0 then 
                    fnd_file.put_line(fnd_file.log, 'Submitted Period Close Pending Transactions report  for organization ='
                                                     ||wc_inv_orgs_rec(indx).organization_name
                                                     ||', Request Id ='
                                                     ||ln_request_id
                                                    );
                  else
                    fnd_file.put_line(fnd_file.log, 'Failed to submit Period Close Pending Transactions report for organization ='
                                                     ||wc_inv_orgs_rec(indx).organization_name                                                     
                                                    );
                  end if;     
                   
                 fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
           
                 lc_request_data :=to_char(to_number(lc_request_data)+1);      
          end loop;     
                              
      End If; -- checking if wc_inv_orgs_rec.count =0      
                             
   else     
   
        Begin  
            select a.user_concurrent_program_name
                  ,c.email_address
            into   v_cp_long_name
                  ,v_email
            from fnd_amp_requests_v a
                ,fnd_concurrent_programs d
                ,fnd_user c
            where 1 =1
              and a.request_id =n_req_id
              and a.concurrent_program_id =d.concurrent_program_id
              and a.requested_by =c.user_id;                                                  
                                                                                                          
        Exception                         
         When Others Then
           Null;
        End;    
       
        Begin
          SELECT '/xx_iface/'||lower(NAME)||'/outbound' INTO v_path FROM v$database;
          output_file_id  :=UTL_FILE.FOPEN(v_path, 'zip_file_extract_'||n_req_id||'.txt', 'w');
          FOR rec IN (
                      /*select outfile_name
                       from fnd_concurrent_requests
                       where 1 =1
                         and parent_request_id =n_req_id
                     */
                      SELECT fcro.file_name outfile_name
                       FROM fnd_concurrent_requests fcr,
                            fnd_concurrent_programs fcp,
                            fnd_conc_req_outputs fcro
                       WHERE 1 =1
                         AND fcr.parent_request_id =n_req_id
                         AND fcr.concurrent_program_id = fcp.concurrent_program_id
                         AND fcr.request_id = fcro.concurrent_request_id
                     )
          loop
            UTL_FILE.PUT_LINE(output_file_id, rec.outfile_name);
          end loop;
          UTL_FILE.FCLOSE(output_file_id);
          v_child_requests :='OK';
        Exception
         When Others Then
          v_child_requests :='NA';
        End;   
        
       If v_child_requests <>'NA' Then
        ln_request_id :=fnd_request.submit_request
              (
               application      =>'XXCUS',
               program          =>'XXCUS_PEREND_POSTPROCESSOR',
               description      =>'',
               start_time       =>'',
               sub_request      =>TRUE,
               argument1        =>n_req_id,
               argument2        =>v_path||'/zip_file_extract_'||n_req_id||'.txt', 
               argument3        =>v_cp_long_name,               
               argument4        =>v_email,
               argument5        =>'WC_Period_Close_Pending_Transactions_'||n_req_id||'.zip',
               argument6        =>'Period Close Pending Transactions Report Bundle (All Orgs) -Status Update'                                                                         
              ); 
         --commit work;                         
          if ln_request_id >0 then 
            update xxcus.xxcus_month_endclose_requests
               set output_emailed ='Y'
                  ,update_source ='xxhds_per_close_recon_pkg.main'
             where 1 =1
               and request_id =n_req_id;

          else
             fnd_file.put_line(fnd_file.log, 'Failed to submit XXHDS Period End Post Processor Program');
             -- Lets leave the output_emailed field to N to show we were not able to email the user regarding the status
             -- of the request and the output of the zip file.
             --This condition can also be resolved by manually submitting a program to be devloped and to process
             -- all records where output_emailed ='N' 
          end if; 

        Else --v_child_requests ='NA'
          Null;        
        End If;
        
      retcode :=0;
      errbuf :='Child request[s] completed. Exit XXWC Period Close Pending Transactions Report -Master';
      fnd_file.put_line(fnd_file.log,errbuf);
      --return;
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Error in caller apps.XXWC_PER_CLOSE_PEN_TRANS_PKG.main ='||sqlerrm);
    rollback;
  end main;
  
end XXWC_PER_CLOSE_PEN_TRANS_PKG;
/