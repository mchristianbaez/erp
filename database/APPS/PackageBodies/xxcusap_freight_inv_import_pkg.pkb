CREATE OR REPLACE PACKAGE BODY APPS.xxcusap_freight_inv_import_pkg IS

  /*================================================================================
  Last Update Date : 12-Aug-2013
  ==================================================================================
  ==================================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  -----------------------------------------
  1.0     12-Aug-2013   Luong Vu           Creation this procedure
  1.1     09-Oct-2013   Luong Vu           Add validation for invoice amount line 1.
                                           Mod update invoice status in staging table
                                           to handle invoice line error.
                                           Add BOL to attribute6 per reporting
                                           requirement.
                                           SR 225571, RFC 38389
  1.2     27-JAN-2014   Manny              Added 1.1 in load_inv proc
  1.3     16-SEP-2014   Manny              modified for FRT move to eaapxprd RFC41641
                                           recompiled (no changes) per     269621?
  1.4     02-JAN-2014   Manny              Discrepency in load_inv adjustments. 259338
                                           rfc 42640
  1.5     07-MAY-2015   Gopi Damuluri      ESMS# 259338 Resolve TMS Freight .01 Cent Discrepancy
  1.6     08/02/2015    Balaguru/Maha  TMS#20150811-00132 ESMS I605812 Freight invoice batches not processing completely
  2.0     08/20/2015    Balaguru/Maha  TMS 20150820-00081, Add cast function to the vendor queries and few other exception handlers.
  2.1     09/14/2015   Balaguru Seshadri TMS 20150909-00153 Add logic to populate project code for Power Solutions sale
  2.2     09/14/2015   Maharajan Shunmugam TMS#20150602-00007 Freight - Including a SO/PO number from TMS in Oracle
  ==================================================================================
  *********************************************************************************/
 --
  /*=============================================================================
  Procedure: invoice_validation
  Version   Date                  Author                                   Description
  -------       -----------            -----------------                      ------------------------------------
  1.0           12-Aug-2013   Luong Vu                           last working version
  2.0           20-Aug-2015  Balaguru Seshadri           TMS 20150820-00081,  Add cast function to the vendor queries and few other exception handlers.
  =============================================================================
  *****************************************************************************/
  PROCEDURE invoice_validation(errbuf  OUT VARCHAR2
                              ,retcode OUT NUMBER) IS

    --l_scac           VARCHAR2(10);
    l_vendor_site_id NUMBER;
    l_vendor_id      NUMBER;
    l_exist          NUMBER;
    l_status         VARCHAR2(25);
    l_numval         NUMBER :=0; --Ver 2.0
    l_dup            VARCHAR2(3);
    l_site_type      VARCHAR2(10) :='UNKNOWN'; --ver 2.0
   -- begin ver 2.0
   b_vendor_can_usd   boolean :=Null;
   b_vendor_can_cad   boolean :=Null;
   b_vendor_dcm           boolean :=Null;
   -- end ver 2.0
    --  Error handling variables
    l_sec            VARCHAR2(2000);
    l_error_msg      VARCHAR2(2000);
    l_err_code       NUMBER;
    l_err_msg        VARCHAR2(1000);
    l_procedure_name VARCHAR2(75) := 'INVOICE_VALIDATION';
    l_err_callfrom   VARCHAR2(75) DEFAULT 'XXCUSAP_FREIGHT_INV_IMPORT_PKG.INVOICE_VALIDATION';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --l_distro_list VARCHAR2(75) DEFAULT 'luong.vu@hdsupply.com';

  BEGIN
   -- dbms_output.enable(1000000000);
    l_exist := 0;

    l_sec := 'Set batch number';
    -- Set batch number
    UPDATE xxcus.xxcusap_tms_inv_stg_tbl
       SET batch_no = 'TMS_' || to_char(SYSDATE, 'DD-MON-YYYY') || '-' ||
                      to_char(SYSDATE, 'HH24MI')
     WHERE status = 'NEW'
       AND batch_no IS NULL;
    COMMIT;

    l_sec := 'Begin data validation';
    -- Begin data validation
    FOR inv_cur IN (SELECT stg.*
                      FROM xxcus.xxcusap_tms_inv_stg_tbl stg
                     WHERE status = 'NEW')
    LOOP
      l_error_msg := NULL;
      l_dup       := 'No';
      l_status    := NULL;

      ------------------------
      -- Invoice Validation --
      ------------------------
      l_sec := 'Invoice Validation';

      ---------------------
      -- SCAC Validation --
      ---------------------
      l_sec := 'SCAC Validation';
      --dbms_output.put_line('SCAC Validation');
      -- BEGIN
      -- SCAC Missing
      IF inv_cur.carrier_scac IS NULL THEN
        l_error_msg := Substr('SCAC code is missing for invoice number: ' ||inv_cur.invoice_num || '<BR>', 1, 2000);  -- ver 2.0
        l_status    := 'REJECTED';

      ELSE
        -- Does SCAC exist in XREF
        --begin ver 1.1
        begin
            SELECT COUNT(scac_cd)
              INTO l_numval
              FROM ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM
             WHERE scac_cd = inv_cur.carrier_scac;
        exception
         when others then
          l_numval :=0;
        end;
        -- end ver 1.1.
        --dbms_output.put_line('l_numval 1 ' || l_numval);
        IF l_numval = 0 THEN
          l_error_msg := Substr(l_error_msg ||'SCAC code not found in XREF.  SCAC code: ' ||inv_cur.carrier_scac || '<BR>', 1, 2000);  -- ver 2.0
          l_status    := 'REJECTED';

        ELSE
          -- Does CURRENCY CODE exist
          IF inv_cur.currency_code IS NULL THEN
            l_error_msg := Substr(l_error_msg ||'Currency code is missing for invoice number: ' ||inv_cur.invoice_num || '<BR>', 1, 2000);   -- ver 2.0
            l_status    := 'REJECTED';

          ELSE
            -- Vendor Site ID lookup (SCAC XREF)
            BEGIN
              SELECT 1
                INTO l_numval
                FROM xxcus.xxcus_location_code_tbl
               WHERE fru = substr(inv_cur.gl_code1, 1, 4)
                 AND substr(entrp_entity, 2) IN
                     (SELECT h.description
                        FROM apps.fnd_lookup_values h
                       WHERE h.lookup_type LIKE ('HDS_CANGLEXTR')
                         AND enabled_flag = 'Y'
                         AND nvl(end_date_active, SYSDATE) >= SYSDATE
                         AND h.lookup_code LIKE 'SEG1_%');
            EXCEPTION
              WHEN no_data_found THEN
                l_numval := 0;
              when others then  -- ver 2.0
                l_numval := 0;       -- ver 2.0
            END;

            IF l_numval = 1 THEN
                  SELECT
                         CASE inv_cur.currency_code
                           WHEN 'CAD' THEN
                            'CAN_CAD'
                           WHEN 'USD' THEN
                            'CAN_USD'
                           ELSE 'UNKNOWN' -- ver 2.0
                         END
                    INTO l_site_type
                    FROM dual;

                  IF l_site_type = 'CAN_USD' THEN
                    BEGIN
                      SELECT xref.can_usd
                        INTO l_vendor_site_id
                        FROM ap.ap_supplier_sites_all                                    ssa
                            ,ap.ap_suppliers                                             sup
                            ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
                       WHERE sup.vendor_id = ssa.vendor_id
                         AND ssa.vendor_site_id =cast(regexp_replace(xref.can_usd, '[^0-9]+', '') as number) --xref.can_usd -- ver 2.0
                         AND xref.scac = inv_cur.carrier_scac;
                         b_vendor_can_usd :=TRUE; -- ver 2.0
                    EXCEPTION
                      WHEN no_data_found THEN
                        l_vendor_site_id := 0;
                        -- begin ver 2.0
                        b_vendor_can_usd :=FALSE;
                       WHEN others THEN
                        l_vendor_site_id := 0;
                        b_vendor_can_usd :=FALSE;
                        -- end ver 2.0
                    END;

                  ELSIF l_site_type = 'CAN_CAD' THEN
                    BEGIN
                      SELECT xref.can_cad
                        INTO l_vendor_site_id
                        FROM ap.ap_supplier_sites_all                                    ssa
                            ,ap.ap_suppliers                                             sup
                            ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
                       WHERE sup.vendor_id = ssa.vendor_id
                         AND ssa.vendor_site_id =cast(regexp_replace(xref.can_cad, '[^0-9]+', '') as number) --xref.can_cad  ver 2.0
                         AND xref.scac = inv_cur.carrier_scac;
                         b_vendor_can_cad :=TRUE; --ver 2.0
                    EXCEPTION
                      WHEN no_data_found THEN
                        l_vendor_site_id := 0;
                        -- begin ver 2.0
                        b_vendor_can_cad :=FALSE;
                       WHEN others THEN
                        l_vendor_site_id := 0;
                        b_vendor_can_cad :=FALSE;
                        -- end ver 2.0
                    END;
                  END IF;
            ELSE --All UNKNOWN site types... --ver 2.0
              BEGIN
                SELECT xref.dcm
                  INTO l_vendor_site_id
                  FROM ap.ap_supplier_sites_all                                    ssa
                      ,ap.ap_suppliers                                             sup
                      ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
                 WHERE sup.vendor_id = ssa.vendor_id
                   AND ssa.vendor_site_id =cast(regexp_replace(xref.dcm, '[^0-9]+', '') as number) --xref.dcm ver 2.0
                   AND xref.scac = inv_cur.carrier_scac;
                    b_vendor_dcm :=TRUE; --ver 2.0
              EXCEPTION
                WHEN no_data_found THEN
                    -- begin ver 2.0
                     l_vendor_site_id := 0;
                    b_vendor_dcm :=FALSE;
                 WHEN others THEN
                     l_vendor_site_id := 0;
                     b_vendor_dcm :=FALSE;
                    -- end ver 2.0
              END;
            END IF;

            IF l_vendor_site_id = 0 THEN
                    -- begin ver 2.0
                    if (NOT b_vendor_dcm) then
                      l_error_msg := Substr(l_error_msg ||', Invalid DCM [oracle vendor site id] value for SCAC code ' ||inv_cur.carrier_scac ||' in SCAC XREF table.<BR>', 1, 2000);
                    elsif (NOT b_vendor_can_cad) then
                      l_error_msg := Substr(l_error_msg ||', Invalid CAN_CAD [oracle vendor site id] value for SCAC code ' ||inv_cur.carrier_scac ||' in SCAC XREF table.<BR>',1 , 2000);
                    elsif (NOT b_vendor_can_usd) then
                      l_error_msg := Substr(l_error_msg ||', Invalid CAN_USD [oracle vendor site id] value for SCAC code ' ||inv_cur.carrier_scac ||' in SCAC XREF table.<BR>',  1, 2000);
                    else
                      l_error_msg := Substr(l_error_msg ||', Invalid oracle vendor site id value for SCAC code ' ||inv_cur.carrier_scac ||' in SCAC XREF table.<BR>', 1, 2000);
                    end if;
                    -- end ver 2.0
              l_status    := 'REJECTED';

            ELSE
              -- Validate vendor_site_id exist and active
                    -- begin ver 2.0
              BEGIN
                    SELECT COUNT(vendor_site_id)
                    INTO l_numval
                    FROM ap.ap_supplier_sites_all
                   WHERE vendor_site_id = l_vendor_site_id
                     AND inactive_date IS NULL;
              EXCEPTION
                WHEN no_data_found THEN
                     l_numval :=0;
                WHEN others THEN
                     l_numval :=0;
              END;
                    -- end ver 2.0
              IF l_numval = 0 THEN
                l_error_msg := Substr(l_error_msg ||
                               'Oracle supplier site not found or not active in Oracle table.  Site ID: ' ||
                               l_vendor_site_id || '<BR>', 1, 2000); --ver 2.0
                l_status    := 'REJECTED';
              END IF; -- SCAC EXIST IN XREF
            END IF; --vendor_site_is
          END IF; --DCM exist
        END IF; -- CURRENCY CODE
      END IF; --SCAC code

      l_sec := 'Invoice Validation';
      IF inv_cur.carrier_scac IS NOT NULL THEN
        -- Missing Invoice number
        IF inv_cur.invoice_num IS NULL THEN
          l_error_msg := Substr(l_error_msg || 'Invoice number not found.<BR>', 1, 2000);  --ver 2.0
          l_status    := 'REJECTED';
        ELSE
          -- Duplicate Invoice
          BEGIN
            -- Get vendor info
            SELECT ssa.vendor_id
                  ,xref.dcm
              INTO l_vendor_id
                  ,l_vendor_site_id
              FROM ap.ap_supplier_sites_all                                    ssa
                  ,ap.ap_suppliers                                             sup
                  ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
             WHERE sup.vendor_id = ssa.vendor_id
               AND ssa.vendor_site_id =cast(regexp_replace(xref.dcm, '[^0-9]+', '') as number) --xref.dcm ver 2.0
               AND xref.scac = inv_cur.carrier_scac;
            -- begin ver 1.1
            begin
                SELECT COUNT(i.invoice_num)
                  INTO l_exist
                  FROM ap.ap_invoices_all i
                 WHERE i.invoice_num = inv_cur.invoice_num
                   AND i.invoice_date = inv_cur.invoice_date
                   AND i.vendor_id = l_vendor_id
                   AND i.vendor_site_id = l_vendor_site_id
                   AND i.invoice_amount = inv_cur.net;

                  IF l_exist <> 0 THEN
                    l_error_msg := Substr(l_error_msg || 'Duplicate invoice found.<BR>', 1, 2000);  -- ver 2.0
                    l_status    := 'REJECTED';
                    l_dup       := 'Yes';
                  END IF;
            exception
                when others then
                    l_exist := 0;
                    l_status    := 'REJECTED';
                    l_error_msg := Substr(l_error_msg || ' Issue in checking duplicate scenario using dcm value. <BR>', 1, 2000);  -- ver 2.0
                    l_dup       := 'No';    -- ver 2.0
            end;
               -- end ver 1.1
          EXCEPTION
            WHEN no_data_found THEN
              l_exist := 0;
              l_dup      := 'No';  -- ver 2.0
            -- begin ver 2.0
            WHEN OTHERS THEN
                l_exist := 0;
                l_status    := 'REJECTED';
                l_error_msg := Substr(l_error_msg || ' Issue in checking duplicate scenario using dcm value. <BR>', 1, 2000);  --ver 2.0
                l_dup       := 'No';    -- ver .1.1
            -- end ver 2.0
          END;
          --begin ver 2.0
          /*
          IF l_exist <> 0 THEN
            l_error_msg := Substr(l_error_msg || 'Duplicate invoice found.<BR>', 1, 2000);
            l_status    := 'REJECTED';
            l_dup       := 'Yes';
          END IF;
          */
          --end ver 2.0
        END IF;
      END IF; -- if scac not null
      ----------------------------------
      -- Settlement Amount Validation --
      ----------------------------------
      l_sec := 'Settlement Amount Validation';

      IF inv_cur.net = 0
      OR inv_cur.net IS NULL THEN

        l_error_msg := Substr(l_error_msg ||
                       'Settlement amount is missing or equal to 0.<BR>', 1, 2000);  -- ver 2.0
        l_status    := 'REJECTED';
      END IF;

      ----------------------------------
      -- Amount line 1 Validation --
      ----------------------------------
      l_sec := 'Amount line 1 Validation'; -- v1.1

      IF inv_cur.amount1 IS NULL THEN
        l_error_msg := Substr(l_error_msg || 'Amount line 1 is missing.<BR>', 1, 2000);  -- ver 2.0
        l_status    := 'REJECTED';
      END IF;

      ------------------------
      -- GL Code Validation --
      ------------------------
      l_sec := 'GL Code Validation';

      -- VALIDATE AMOUNT1, GL_CODE1
      IF inv_cur.amount1 IS NOT NULL THEN
        IF inv_cur.gl_code1 IS NULL THEN
          -- glcode1 does not exists
          l_error_msg := Substr(l_error_msg ||
                         'GL_Code not found for invoice distribution line 1.<BR>', 1, 2000);  -- ver 2.0
          l_status    := 'REJECTED';

        ELSE
          -- BRANCH1 Exist VALIDATION
          SELECT COUNT(fru)
            INTO l_exist
            FROM xxcus.xxcus_location_code_tbl
           WHERE fru = substr(inv_cur.gl_code1, 1, 4);
          IF l_exist = 0 THEN
            l_error_msg := Substr(l_error_msg ||
                           'Invalid Branch for invoice distribution line 1.<BR>', 1, 2000);  -- ver 2.0
            l_status    := 'REJECTED';
          ELSE
            -- BRANCH1 AP Enable VALIDATION
            SELECT COUNT(fru)
              INTO l_exist
              FROM xxcus.xxcus_location_code_tbl
             WHERE fru = substr(inv_cur.gl_code1, 1, 4)
            -- AND ap_docum_flag = 'Y'    -- reactivate after testing
            ;

            IF l_exist = 0 THEN
              l_error_msg := Substr(l_error_msg ||
                             'AP_Flag for branch is not enabled.<BR>', 1, 2000);  -- ver 2.0
              l_status    := 'REJECTED';
            END IF;
          END IF;

        END IF;
      END IF;

      -- VALIDATE AMOUNT2, GL_CODE2
      IF inv_cur.amount2 IS NOT NULL THEN
        IF inv_cur.gl_code2 IS NULL THEN
          -- glcode2 does not exist
          l_error_msg := Substr(l_error_msg ||
                         'GL_Code not found for invoice distribution line 2.<BR>', 1, 2000);  --ver 2.0
          l_status    := 'REJECTED';
        ELSE
          -- BRANCH2 VALIDATION
          SELECT COUNT(fru)
            INTO l_exist
            FROM xxcus.xxcus_location_code_tbl
           WHERE fru = substr(inv_cur.gl_code2, 1, 4);
          IF l_exist = 0 THEN
            l_error_msg := Substr(l_error_msg ||
                           'Invalid Branch for invoice distribution 2.<BR>', 1, 2000);  -- ver 2.0
            l_status    := 'REJECTED';
          END IF;
        END IF;
      END IF;

      -- VALIDATE AMOUNT3, GL_CODE3
      IF inv_cur.amount3 IS NOT NULL THEN
        IF inv_cur.gl_code3 IS NULL THEN
          -- glcode3 does not exist.
          l_error_msg := Substr(l_error_msg ||
                         'GL_Code not found for invoice distribution line 3.<BR>', 1, 2000);  -- ver 2.0
          l_status    := 'REJECTED';
        ELSE
          -- BRANCH3 VALIDATION
          SELECT COUNT(fru)
            INTO l_exist
            FROM xxcus.xxcus_location_code_tbl
           WHERE fru = substr(inv_cur.gl_code3, 1, 4);
          IF l_exist = 0 THEN
            l_error_msg := Substr(l_error_msg ||
                           'Invalid Branch for invoice distribution line 3.<BR>', 1, 2000);  -- ver 2.0
            l_status    := 'REJECTED';
          END IF;
        END IF;
      END IF;

      IF inv_cur.amount4 IS NOT NULL THEN
        IF inv_cur.gl_code4 IS NULL THEN
          --gl4 does not exist
          l_error_msg := Substr(l_error_msg ||
                         'GL_Code not found for invoice distribution line 4.<BR>', 1, 2000);  -- ver 2.0
          l_status    := 'REJECTED';
        ELSE
          -- BRANCH4 VALIDATION
          SELECT COUNT(fru)
            INTO l_exist
            FROM xxcus.xxcus_location_code_tbl
           WHERE fru = substr(inv_cur.gl_code4, 1, 4);
          IF l_exist = 0 THEN
            l_error_msg := Substr(l_error_msg ||
                           'Invalid Branch for invoice distribution line 4.<BR>', 1, 2000);  -- ver 2.0
            l_status    := 'REJECTED';
          END IF;
        END IF;
      END IF;

      IF inv_cur.amount5 IS NOT NULL THEN
        IF inv_cur.gl_code5 IS NULL THEN
          l_error_msg := Substr(l_error_msg ||
                         'GL_Code not found for invoice distribution line 5.<BR>', 1, 2000);  -- ver 2.0
          l_status    := 'REJECTED';
        ELSE
          -- BRANCH5 VALIDATION
          SELECT COUNT(fru)
            INTO l_exist
            FROM xxcus.xxcus_location_code_tbl
           WHERE fru = substr(inv_cur.gl_code5, 1, 4);
          IF l_exist = 0 THEN
            l_error_msg := Substr(l_error_msg ||
                           'Invalid Branch for invoice distribution line 5.<BR>', 1, 2000);  --ver 2.0
            l_status    := 'REJECTED';
          END IF;
        END IF;
      END IF; -- GL_CODE5

      ----------------------------------
      -- Weight Validation --
      ----------------------------------
      l_sec := 'Weight Validation';

      IF inv_cur.weight < 0
         OR inv_cur.weight IS NULL THEN
        l_error_msg := Substr(l_error_msg ||
                       'Weight value is missing or less than 0.<BR>', 1, 2000);  -- ver 2.0
        l_status    := 'REJECTED';
      END IF;

      ----------------------------------
      -- Quantity Validation --
      ----------------------------------
      l_sec := 'Quantity Validation';

      IF inv_cur.pieces < 0
         OR inv_cur.pieces IS NULL THEN
        l_error_msg := Substr(l_error_msg ||
                       'Quantity value is missing or less than 0.<BR>', 1, 2000);  -- ver 2.0
        l_status    := 'REJECTED';
      END IF;

      ----------------------------------
      -- Invoice Date Validation      --
      -- no future date               --
      ----------------------------------
      l_sec := 'Invoice Date Validation';

      IF inv_cur.invoice_date > trunc(SYSDATE) THEN
        l_error_msg := Substr(l_error_msg || 'Invoice date is in the future.<BR>', 1, 2000);  --ver 2.0
        l_status    := 'REJECTED';
      END IF;

      -- FINALLY, update record if status is REJECTED
      l_sec := 'Update record status and error message';

      IF l_status = 'REJECTED' THEN
        UPDATE xxcus.xxcusap_tms_inv_stg_tbl
           SET status    = 'REJECTED'
              ,error_msg = Substr(l_error_msg, 1, 2000)  -- ver 2.0
         WHERE stg_invoice_id = inv_cur.stg_invoice_id;
        COMMIT;
      END IF;
    END LOOP;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := Substr('Error in program ' || l_procedure_name || ' Error: ' ||l_sec, 1, 2000);  -- ver 2.0
      l_err_msg  := Substr(l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000), 1, 2000);  -- ver 2.0

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for xxcusap_freight_inv_import_pkg.invoice_validation',
                                           p_request_id => g_request_id,
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error during procedure invoice_validation',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');
    WHEN OTHERS THEN

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := Substr(l_sec || l_err_msg || ' ERROR ' || SQLCODE ||substr(SQLERRM, 1, 2000), 1, 2000); -- ver 2.0
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for xxcusap_freight_inv_import_pkg.invoice_validation',
                                           p_request_id => g_request_id,
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error during procedure invoice_validation',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');
  END;
  --
  /*=============================================================================
  Last Update Date : 12-Aug-2013
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)                              DESCRIPTION
  ------- -----------   -----------------                                ------------------------------------
  1.0     12-Aug-2013   Luong Vu                              Creation this package
  1.1     27-JAN-2014   Manny                                  Added error handling to show invoices
  1.3     16-SEP-2014   Manny                                  modified for FRT move to eaapxprd RFC41641
  1.4     16-JAN-1015   Manny                                 .01 issue rfc 42640
  1.5     07-MAY-2015   Gopi Damuluri                  ESMS# 259338 Resolve TMS Freight .01 Cent discrepancy
  1.6    08/02/2015     Balaguru/Maha                 TMS#20150811-00132 ESMS I605812 Freight invoice batches not processing completely
  2.0    08/20/2015     Balaguru  Seshadri           TMS 20150820-00081. Add cast function to the vendor queries
  2.1     09/14/2015   Balaguru Seshadri            TMS 20150909-00153 Add logic to populate project code for Power Solutions sale 
  2.2     09/14/2015   Maharajan Shunmugam TMS#20150602-00007 Freight - Including a SO/PO number from TMS in Oracle
  =============================================================================
  *****************************************************************************/

  PROCEDURE load_inv(errbuf  OUT VARCHAR2
                    ,retcode OUT NUMBER
                     --,p_group_id IN VARCHAR2
                     ) IS

    v_req_id        NUMBER := fnd_global.conc_request_id;
    v_req_phase     VARCHAR2(80);
    v_req_status    VARCHAR2(80);
    v_dev_phase     VARCHAR2(30);
    v_dev_status    VARCHAR2(30);
    v_message       VARCHAR2(255);
    v_interval      NUMBER := 10; -- In seconds
    v_max_time      NUMBER := 15000; -- In seconds
    v_error_message VARCHAR2(3000);

    l_source          VARCHAR2(15) DEFAULT 'TMS';
    l_invoice_line_id NUMBER;
    l_err_invoice     NUMBER;
    l_freight_type    VARCHAR2(25) DEFAULT 'FRTEXP';
    l_vendor_number   NUMBER;
    l_vendor_id       NUMBER;
    l_vendor_site_id  NUMBER;
    l_org_id          NUMBER;
    l_product         VARCHAR2(2);
    l_loc             VARCHAR2(6);
    l_fru             VARCHAR2(4);
    l_glacct          VARCHAR2(25);
    l_code_combo1     VARCHAR2(250);
    l_code_combo      VARCHAR2(250);
    l_line_num        NUMBER;
    l_line_type       VARCHAR2(25) DEFAULT 'ITEM';
    l_line_amt        NUMBER;
    l_line_amt_sum    NUMBER DEFAULT 0;
    l_diff            NUMBER DEFAULT 0;
    l_numval          NUMBER :=0; --ver 1.6
    l_site_type       VARCHAR2(10);
    l_tax_prov        VARCHAR2(3);
    l_adjust_desc     VARCHAR2(240) DEFAULT 'TMS Adjustment';
    l_tax_code        VARCHAR2(6);
    l_seg3            VARCHAR2(4) DEFAULT '0000';
    l_seg567          VARCHAR2(25) DEFAULT '00000.00000.00000';
    l_ps_seg5        VARCHAR2(10) :='P0086'; --Ver 2.1
    l_ps_seg67          VARCHAR2(25) DEFAULT '00000.00000';   --Ver 2.1 
    l_PS_check_flag  VARCHAR2(1) :='N'; --Ver 2.1
    l_can_submit_request BOOLEAN := TRUE;
    l_globalset          VARCHAR2(100);
    l_err_msg            VARCHAR2(2000);
    l_err_code           NUMBER;
    l_fru_failed boolean :=Null; --vER 1.6
    l_sec                VARCHAR2(2000);
    l_statement          VARCHAR2(9000);
    l_err_callfrom       VARCHAR2(75) DEFAULT 'XXCUSAP_FREIGHT_INV_IMPORT_PKG.LOAD_INV';
    l_errcode            VARCHAR2(250);
    l_errdesc            VARCHAR2(750);
    l_err_callpoint      VARCHAR2(75) DEFAULT 'START';
    l_distro_list        VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --l_distro_list    VARCHAR2(75) DEFAULT 'luong.vu@hdsupply.com';
    l_procedure_name VARCHAR2(75) := 'LOAD_INV';
    l_amt_val_failed BOOLEAN :=Null; --Ver 1.6
    l_vendor_check_failed BOOLEAN :=Null; -- Ver 1.6
  BEGIN
    --dbms_output.enable(10000000);

    -- Load invoice header
    BEGIN
         -- Begin Ver 1.6
         fnd_file.put_line(fnd_file.log, '');
         l_sec :='Begin processing staging records...';
         fnd_file.put_line(fnd_file.log, '');
         -- End Ver 1.6
--      l_sec := 'Insert invoices header into AP_INVOICES_INTERFACE'; -- Ver 1.6
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      dbms_output.put_line(l_sec);
      FOR c_inv_hdr IN (SELECT invoice_id
                              ,invoice_num
                              ,po_number                               --Added for ver#2.2
                               -- ,invoice_type_lookup_code
                              ,nvl(invoice_date, trunc(SYSDATE)) invoice_date
                               -- ,vendor_num
                              ,carrier_name
                              ,carrier_scac
                              ,net invoice_amount
                              , gross --Version# 1.5
                              , net -- Version# 1.5
                              , stg_invoice_id -- Version# 1.5
                              ,nvl(currency_code, 'USD') currency_code
                               --  ,description
                               --  ,org_id
                               --  ,SOURCE
                              ,consignee_st
                              ,batch_no --group_id
                              ,substr(gl_code1, 1, 4) fru
                              ,attribute1
                              ,attribute2
                              ,attribute3
                              ,attribute4
                              ,attribute5
                              ,bol --attribute6      --v1.1
                              ,attribute7
                              ,attribute8
                              ,attribute9
                              ,attribute10
                              ,attribute11
                              ,attribute12
                              ,attribute13
                              ,attribute14
                              ,attribute15
                          FROM xxcus.XXCUSAP_TMS_INV_STG_TBL
                         WHERE status = 'NEW')
      LOOP
      BEGIN -- Version# 1.5
        l_vendor_check_failed :=FALSE; -- Ver 1.6
        l_amt_val_failed :=FALSE; -- Ver 1.6
        l_fru_failed :=FALSE; --Ver 1.6
    l_err_invoice :=c_inv_hdr.invoice_id;
        -- Get vendor info
        -- Is CAN invoice?
        BEGIN
          SELECT 1
            INTO l_numval
            FROM xxcus.xxcus_location_code_tbl
           WHERE fru = c_inv_hdr.fru
             AND substr(entrp_entity, 2) IN ('17', '18', '37', '38');
        EXCEPTION
          WHEN no_data_found THEN
            l_numval := 0;
            l_fru_failed :=TRUE; --vER 1.6
           --  FND_FILE.PUT_LINE(FND_FILE.LOG, 'no data found, Check xxcus.xxcus_location_code_tbl, @ invoice_num ='||c_inv_hdr.invoice_num   
           --  ||', c_inv_hdr.fru =' ||c_inv_hdr.fru);  --Ver 1.6  --commented ver 2.1
          -- Begin Ver 1.6
          WHEN OTHERS THEN
            FND_FILE.PUT_LINE(FND_FILE.LOG, 'Check if xxcus.xxcus_location_code_tbl, @ invoice_num ='||c_inv_hdr.invoice_num||', msg ='||SQLERRM);  --Ver 1.6
            l_numval := 0;
            l_fru_failed :=TRUE;
          -- End Ver 1.6
        END;

        IF l_numval = 1 THEN
          SELECT CASE c_inv_hdr.currency_code
                   WHEN 'CAD' THEN
                    'CAN_CAD'
                   WHEN 'USD' THEN
                    'CAN_USD'
                 END
            INTO l_site_type
            FROM dual;
          ELSE --VER1.6
           l_site_type := 'INVALID';     ---Ver1.6
        END IF;
        dbms_output.put_line('HERE 1');
        IF l_site_type = 'CAN_USD' THEN
         -- Begin Ver 1.6
          begin
              SELECT ssa.vendor_id
                    ,xref.can_usd
                    ,sup.segment1
                    ,ssa.org_id
                INTO l_vendor_id
                    ,l_vendor_site_id
                    ,l_vendor_number
                    ,l_org_id
                FROM ap.ap_supplier_sites_all ssa
                    ,ap.ap_suppliers                        sup
                    ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
               WHERE sup.vendor_id = ssa.vendor_id
                 AND ssa.vendor_site_id =cast(regexp_replace(xref.can_usd, '[^0-9]+', '') as number) --ver 2.0
                 AND xref.scac = c_inv_hdr.carrier_scac;
          exception
           when no_data_found then
                 FND_FILE.PUT_LINE(FND_FILE.LOG, 'No Data found, Check if l_site_type = CAN_USD, @ invoice_num ='||c_inv_hdr.invoice_num||', msg ='||SQLERRM); 
                 l_vendor_id :=Null;
                 l_vendor_site_id :=Null;
                 l_vendor_number :=Null;
                 l_org_id :=Null;
                 l_vendor_check_failed :=TRUE;
           when others then
               FND_FILE.PUT_LINE(FND_FILE.LOG, 'When others, Check if l_site_type = CAN_USD, @ invoice_num ='||c_inv_hdr.invoice_num||', msg ='||SQLERRM);
                 l_vendor_id :=Null;
                 l_vendor_site_id :=Null;
                 l_vendor_number :=Null;
                 l_org_id :=Null;
                 l_vendor_check_failed :=TRUE;
          end;
          -- End Ver 1.6
          /* -- Begin Ver 1.6
          SELECT ssa.vendor_id
                ,xref.can_usd
                ,sup.segment1
                ,ssa.org_id
            INTO l_vendor_id
                ,l_vendor_site_id
                ,l_vendor_number
                ,l_org_id
            FROM ap.ap_supplier_sites_all                                    ssa
                ,ap.ap_suppliers                                             sup
                ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
           WHERE sup.vendor_id = ssa.vendor_id
             AND ssa.vendor_site_id = xref.can_usd
             AND xref.scac = c_inv_hdr.carrier_scac;
             */  -- End Ver 1.6
          dbms_output.put_line('HERE 2');
        ELSIF l_site_type = 'CAN_CAD' THEN
         -- Begin Ver 1.6
          begin
              SELECT ssa.vendor_id
                    ,xref.can_cad
                    ,sup.segment1
                    ,ssa.org_id
                INTO l_vendor_id
                    ,l_vendor_site_id
                    ,l_vendor_number
                    ,l_org_id
                FROM ap.ap_supplier_sites_all  ssa
                    ,ap.ap_suppliers                         sup
                    ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
               WHERE sup.vendor_id = ssa.vendor_id
                 AND ssa.vendor_site_id =cast(regexp_replace(xref.can_cad, '[^0-9]+', '') as number) --ver 2.0
                 AND xref.scac = c_inv_hdr.carrier_scac;
          exception
           when no_data_found then
                  FND_FILE.PUT_LINE(FND_FILE.LOG, 'No data found,Check if l_site_type = CAN_CAD, @ invoice_num ='||c_inv_hdr.invoice_num||', msg ='||SQLERRM);
                l_vendor_id :=Null;
                 l_vendor_site_id :=Null;
                 l_vendor_number :=Null;
                 l_org_id :=Null;
                 l_vendor_check_failed :=TRUE;
           when others then
                FND_FILE.PUT_LINE(FND_FILE.LOG, 'When others,Check if l_site_type = CAN_CAD, @ invoice_num ='||c_inv_hdr.invoice_num||', msg ='||SQLERRM);
                 l_vendor_id :=Null;
                 l_vendor_site_id :=Null;
                 l_vendor_number :=Null;
                 l_org_id :=Null;
                 l_vendor_check_failed :=TRUE;
          end;
          -- End Ver 1.6
          -- Begin Ver 1.6
          /*
          SELECT ssa.vendor_id
                ,xref.can_cad
                ,sup.segment1
                ,ssa.org_id
            INTO l_vendor_id
                ,l_vendor_site_id
                ,l_vendor_number
                ,l_org_id
            FROM ap.ap_supplier_sites_all                                    ssa
                ,ap.ap_suppliers                                             sup
                ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
           WHERE sup.vendor_id = ssa.vendor_id
             AND ssa.vendor_site_id = xref.can_cad
             AND xref.scac = c_inv_hdr.carrier_scac;
         */
         -- End Ver 1.6
          --END IF;
          dbms_output.put_line('HERE 3');
        ELSE
         -- Begin Ver 1.6
          begin
              SELECT ssa.vendor_id
                    ,xref.dcm
                    ,sup.segment1
                    ,ssa.org_id
                INTO l_vendor_id
                    ,l_vendor_site_id
                    ,l_vendor_number
                    ,l_org_id
                FROM ap.ap_supplier_sites_all  ssa
                    ,ap.ap_suppliers                         sup
                    ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
               WHERE sup.vendor_id = ssa.vendor_id
                 AND ssa.vendor_site_id =cast(regexp_replace(xref.dcm, '[^0-9]+', '') as number) --ver 2.0
                 AND xref.scac = c_inv_hdr.carrier_scac;
          exception
           when no_data_found then
                 FND_FILE.PUT_LINE(FND_FILE.LOG, 'NO data found,Check if l_site_type other than CAN_USD OR CAN_CAD, @ invoice_num ='||c_inv_hdr.invoice_num||', msg ='||SQLERRM);
                 l_vendor_id :=Null;
                 l_vendor_site_id :=Null;
                 l_vendor_number :=Null;
                 l_org_id :=Null;
                 l_vendor_check_failed :=TRUE;
           when others then
                FND_FILE.PUT_LINE(FND_FILE.LOG, 'When others,Check if l_site_type other than CAN_USD OR CAN_CAD, @ invoice_num ='||c_inv_hdr.invoice_num||', msg ='||SQLERRM);
                 l_vendor_id :=Null;
                 l_vendor_site_id :=Null;
                 l_vendor_number :=Null;
                 l_org_id :=Null;
                 l_vendor_check_failed :=TRUE;
          end;
          -- End Ver 1.6
          /* -Begin Ver 1.6
          SELECT ssa.vendor_id
                ,xref.dcm
                ,sup.segment1
                ,ssa.org_id
            INTO l_vendor_id
                ,l_vendor_site_id
                ,l_vendor_number
                ,l_org_id
            FROM ap.ap_supplier_sites_all                                    ssa
                ,ap.ap_suppliers                                             sup
                ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
           WHERE sup.vendor_id = ssa.vendor_id
             AND ssa.vendor_site_id = xref.dcm
             AND xref.scac = c_inv_hdr.carrier_scac;
             */
           --End Ver 1.6
        END IF;
        -- Populate tax province for CAN invoice
        --IF c_inv_hdr.currency_code = 'CAD' THEN
        IF l_org_id = 167 THEN
          l_tax_prov := c_inv_hdr.consignee_st;
        ELSE
          l_tax_prov := NULL;
        END IF;
        dbms_output.put_line('HERE 4');
        l_sec := 'INSERT INTO ap.ap_invoices_interface';
        -- Version# 1.5 > Start
        l_line_amt_sum := 0;
        l_diff         := 0;
        BEGIN
        SELECT (NVL(amount1,0) + NVL(amount2,0) + NVL(amount3,0) + NVL(amount4,0) + NVL(amount5,0))
          INTO l_line_amt_sum
          FROM xxcus.xxcusap_tms_inv_stg_tbl stg
         WHERE 1 = 1
           AND stg.invoice_id = c_inv_hdr.invoice_id
           AND stg.status = 'NEW';
        EXCEPTION
        WHEN NO_DATA_FOUND THEN -- Ver 1.6
           l_line_amt_sum := 0;   -- Ver 1.6
        WHEN OTHERS THEN
          FND_FILE.PUT_LINE(FND_FILE.LOG, '@ Error in calculating variable l_line_amt_sum, @ invoice_num ='||c_inv_hdr.invoice_num||',  msg ='||SQLERRM); -- Ver 1.6
           l_line_amt_sum := 0;
        END;

        l_diff := c_inv_hdr.net - l_line_amt_sum;
--        fnd_file.put_line(fnd_file.log, '*** Invoice Num - '||c_inv_hdr.invoice_num||' l_diff -'||l_diff); --Ver 1.6

        IF ABS(l_diff) > 0.01 AND c_inv_hdr.net > l_line_amt_sum THEN -- $$$$$
       fnd_file.put_line(fnd_file.log, '*** BatchNo_Invoice# - '||c_inv_hdr.batch_no||'_'||c_inv_hdr.invoice_num||' , l_diff -'||l_diff||', variable l_amt_val_failed IS TRUE'); --Ver 1.6
          -- l_errdesc := 'Invoice Amount does not match sum of Invoice Line Amounts.';
           --RAISE program_error; --
           l_amt_val_failed :=TRUE; --Ver 1.6
        ELSE
           l_amt_val_failed :=FALSE; --Ver 1.6
        END IF;
        -- Version# 1.5 < End
        -- Begin Ver 1.6
        SAVEPOINT square1; -- Ver 1.6
        IF ((l_amt_val_failed) OR (l_vendor_check_failed)) then  --STOP, flag the staging record as rejected and move on to next record
                     --
                     begin
                          if ((l_amt_val_failed) AND (l_vendor_check_failed)) then
                           l_errdesc :=substr('Failed to fetch vendor info and Invoice Amount does not match sum of Invoice Line Amounts', 1, 1000);
                          elsif ((l_amt_val_failed) and  (NOT l_vendor_check_failed)) then
                           l_errdesc :=substr('Invoice Amount does not match sum of Invoice Line Amounts', 1, 1000);
                          elsif ((NOT l_amt_val_failed) AND (l_vendor_check_failed)) then
                           l_errdesc :=substr('Failed to fetch vendor info', 1, 1000);
                          elsif ((l_fru_failed) and (l_vendor_check_failed)) then
                           l_errdesc :=substr('Failed to fetch fru from xxcus_location_code_tbl and vendor info using DCM field', 1, 1000);
                          else
                           l_errdesc :=Substr('Failed to get status of variables l_amt_val_failed and l_vendor_check_failed', 1, 1000);
                          end if;
                     end;
                     --
                     begin
                      update xxcus.xxcusap_tms_inv_stg_tbl
                         set status    = 'REJECTED'
                           , error_msg = l_errdesc
                       where stg_invoice_id = c_inv_hdr.stg_invoice_id;
                     exception
                      when others then
                          fnd_file.put_line(fnd_file.log, '@ Check of variable  l_amt_val_failed, BATCHNO_INVOICE# ='|| c_inv_hdr.batch_no||'_'||c_inv_hdr.invoice_num||', Note: Staging record 
rejected');
                          l_err_msg  := substr(SQLERRM, 1, 2000);
                          xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                                               p_calling => 'xxcusap_freight_inv_import_pkg.load_inv',
                                                               p_request_id => fnd_global.conc_request_id,  -- Ver 1.
                                                               p_argument1 => '@ l_amt_val_failed, update staging record to rejected, BATCHNO_INVOICE#' ,           -- Ver 1.6
                                                               p_argument2 => c_inv_hdr.batch_no||'_'||c_inv_hdr.invoice_num,  -- Ver 1.6
                                                               p_ora_error_msg => substr(' Error_Stack =>' ||
                                                                                       dbms_utility.format_error_stack() ||
                                                                                       ' Error_Backtrace =>' ||
                                                                                       dbms_utility.format_error_backtrace()
                                                                                      ,1
                                                                                      ,2000), -- Ver 1.6
                                                               p_error_desc =>l_err_msg,
                                                               p_distribution_list => l_distro_list,
                                                               p_module => 'AP');

                     end;
        ELSE --Good to go...Insert ap invoice  header
               begin
                    INSERT INTO ap.ap_invoices_interface
                      (invoice_id
                      ,invoice_num
                       --  ,invoice_type_lookup_code
                      ,invoice_date
                      ,vendor_num
                      ,vendor_id
                      ,vendor_site_id
                      ,invoice_amount
                      ,invoice_currency_code
                       --  ,description
                      ,org_id
                      ,SOURCE
                      ,group_id
                      ,last_updated_by
                      ,last_update_date
                      ,last_update_login
                      ,creation_date
                      ,created_by
                      ,attribute_category
                      ,attribute1
                      ,attribute2
                      ,attribute3
                      ,attribute4         
                      ,attribute5
                      ,attribute6
                      ,attribute7
                      ,attribute8
                      ,attribute9
                      ,attribute10
                      ,attribute11
                      ,attribute12
                      ,attribute13
                      ,attribute14
                      ,attribute15)
                    VALUES
                      (c_inv_hdr.invoice_id
                      ,c_inv_hdr.invoice_num
                      ,c_inv_hdr.invoice_date
                      ,l_vendor_number
                      ,l_vendor_id
                      ,l_vendor_site_id
                      ,c_inv_hdr.invoice_amount
                      ,c_inv_hdr.currency_code
                      ,l_org_id
                      ,l_source
                      ,c_inv_hdr.batch_no
                      ,g_login_id
                      ,SYSDATE
                      ,g_login_id
                      ,SYSDATE
                      ,g_login_id
                      ,l_org_id
                      ,c_inv_hdr.attribute1
                      ,c_inv_hdr.fru --c_inv_hdr.attribute2
                      ,c_inv_hdr.attribute3
                     -- ,c_inv_hdr.attribute4 					--commented and added below for ver#2.2
		      ,NVL(c_inv_hdr.po_number,c_inv_hdr.attribute4) 
                      ,l_tax_prov --c_inv_hdr.attribute5
                      ,c_inv_hdr.bol --c_inv_hdr.attribute6
                      ,c_inv_hdr.attribute7
                      ,c_inv_hdr.attribute8
                      ,c_inv_hdr.attribute9
                      ,c_inv_hdr.attribute10
                      ,l_freight_type --c_inv_hdr.attribute11
                      ,c_inv_hdr.attribute12
                      ,c_inv_hdr.attribute13
                      ,c_inv_hdr.attribute14
                      ,c_inv_hdr.attribute15
                      );
               exception
                when others then
                   fnd_file.put_line(fnd_file.log, 'Error in insert of ap invoice header, @ Batchno_Invoice# :'||c_inv_hdr.batch_no||'_'||c_inv_hdr.invoice_num||', msg ='||sqlerrm);
                  rollback to square1 ;
               end;
               /*
                INSERT INTO ap.ap_invoices_interface
                  (invoice_id
                  ,invoice_num
                   --  ,invoice_type_lookup_code
                  ,invoice_date
                  ,vendor_num
                  ,vendor_id
                  ,vendor_site_id
                  ,invoice_amount
                  ,invoice_currency_code
                   --  ,description
                  ,org_id
                  ,SOURCE
                  ,group_id
                  ,last_updated_by
                  ,last_update_date
                  ,last_update_login
                  ,creation_date
                  ,created_by
                  ,attribute_category
                  ,attribute1
                  ,attribute2
                  ,attribute3
                  ,attribute4
                  ,attribute5
                  ,attribute6
                  ,attribute7
                  ,attribute8
                  ,attribute9
                  ,attribute10
                  ,attribute11
                  ,attribute12
                  ,attribute13
                  ,attribute14
                  ,attribute15)
                VALUES
                  (c_inv_hdr.invoice_id
                  ,c_inv_hdr.invoice_num
                  ,c_inv_hdr.invoice_date
                  ,l_vendor_number
                  ,l_vendor_id
                  ,l_vendor_site_id
                  ,c_inv_hdr.invoice_amount
                  ,c_inv_hdr.currency_code
                  ,l_org_id
                  ,l_source
                  ,c_inv_hdr.batch_no
                  ,g_login_id
                  ,SYSDATE
                  ,g_login_id
                  ,SYSDATE
                  ,g_login_id
                  ,l_org_id
                  ,c_inv_hdr.attribute1
                  ,c_inv_hdr.fru --c_inv_hdr.attribute2
                  ,c_inv_hdr.attribute3
                  ,c_inv_hdr.attribute4
                  ,l_tax_prov --c_inv_hdr.attribute5
                  ,c_inv_hdr.bol --c_inv_hdr.attribute6
                  ,c_inv_hdr.attribute7
                  ,c_inv_hdr.attribute8
                  ,c_inv_hdr.attribute9
                  ,c_inv_hdr.attribute10
                  ,l_freight_type --c_inv_hdr.attribute11
                  ,c_inv_hdr.attribute12
                  ,c_inv_hdr.attribute13
                  ,c_inv_hdr.attribute14
                  ,c_inv_hdr.attribute15
                  );
                  */
        END IF;
        -- End Ver 1.6
      --  COMMIT; -- Version# 1.5
-- Version# 1.5 > Start
      EXCEPTION
       /* --Begin Ver 1.6
      WHEN program_error THEN
          ROLLBACK;
          UPDATE xxcus.xxcusap_tms_inv_stg_tbl
             SET status    = 'REJECTED'
               , error_msg = l_errdesc
           WHERE stg_invoice_id = c_inv_hdr.stg_invoice_id;
           */ --End Ver 1.6
      WHEN OTHERS THEN
          ROLLBACK to square1;
          l_errdesc := SQLERRM;
          fnd_file.put_line(fnd_file.log, 'Error before check of l_amt_val_failed and l_vendor_check_failed, @ Batchno_Invoice# :'||c_inv_hdr.batch_no||'_'||c_inv_hdr.invoice_num||', msg ='||
sqlerrm);
          UPDATE xxcus.xxcusap_tms_inv_stg_tbl
             SET status    = 'REJECTED'
               , error_msg = l_errdesc
           WHERE stg_invoice_id = c_inv_hdr.stg_invoice_id;
      END;
-- Version# 1.5 < End

      END LOOP;

    EXCEPTION
      WHEN OTHERS THEN
      dbms_output.put_line('Error at cursor c_inv_hdr, current invoice_id  '||l_err_invoice);
        retcode := SQLCODE;
        errbuf  := substr(SQLERRM, 1, 2000);
        fnd_file.put_line(fnd_file.log,
                          'Error in ' || l_procedure_name || l_sec);
        fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
        fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
        -- Calling ERROR API
    END;
    -- Begin Ver 2.1
     if (g_PS_logic_flag ='Y' and trunc(sysdate) >= g_PS_sale_start_date) then
      l_PS_check_flag :='Y';     
     else
      l_PS_check_flag :='N';
     end if;
    -- End Ver 2.1
    -------------------------
    -- LOAD INVOICE LINES  --
    -------------------------
    BEGIN
      dbms_output.put_line('HERE 5');
      l_sec := 'Insert invoices lines into ap_invoice_lines_interface';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      --Load invoice line to interface

      FOR c_invoice IN (SELECT stg.carrier_scac
                              ,stg.invoice_id
                              ,stg.stg_invoice_id
                              ,stg.invoice_num
                              ,aii.group_id
                              ,aii.org_id
                              ,aii.vendor_id
                              ,aii.vendor_site_id
                              ,aii.vendor_num
                              ,stg.gross
                              ,stg.net
                              ,stg.amount1
                              ,stg.gl_code1
                              ,stg.amount2
                              ,stg.gl_code2
                              ,stg.amount3
                              ,stg.gl_code3
                              ,stg.amount4
                              ,stg.gl_code4
                              ,stg.amount5
                              ,stg.gl_code5
                              ,stg.attribute1
                              ,stg.attribute2
                              ,stg.attribute3
                              ,stg.attribute4
                              ,stg.attribute5
                              ,stg.attribute6
                              ,stg.attribute7
                              ,stg.attribute8
                              ,stg.attribute9
                              ,stg.attribute10
                              ,stg.attribute11
                              ,stg.attribute12
                              ,stg.attribute13
                              ,stg.attribute14
                              ,stg.attribute15
                          FROM xxcus.xxcusap_tms_inv_stg_tbl stg
                              ,ap.ap_invoices_interface      aii
                         WHERE stg.invoice_id = aii.invoice_id
                           AND stg.status = 'NEW'
                           AND aii.status IS NULL)
      LOOP
        BEGIN -- Version# 1.5
        dbms_output.put_line('HERE 6 -  ' || c_invoice.invoice_num);
        /*        -- Get vendor info
        SELECT ssa.vendor_id
              ,xref.dcm
              ,sup.segment1
              ,ssa.org_id
          INTO l_vendor_id
              ,l_vendor_site_id
              ,l_vendor_number
              ,l_org_id
          FROM ap.ap_supplier_sites_all                                    ssa
              ,ap.ap_suppliers                                             sup
              ,ea_apps.frt_carrier_xref@APXPRD_EAAPXPRD.HSI.HUGHESSUPPLY.COM xref
         WHERE sup.vendor_id = ssa.vendor_id
           AND ssa.vendor_site_id = xref.dcm
           AND xref.scac = c_invoice.carrier_scac;*/
        dbms_output.put_line('HERE 6.1');
        -- Line 1
        l_line_num        := 1;
        l_invoice_line_id := ap_invoice_lines_interface_s.nextval;
        dbms_output.put_line('HERE 6.2');
        -- Get fru
        SELECT substr(gl_code1, 1, 4)
          INTO l_fru
          FROM xxcus.xxcusap_tms_inv_stg_tbl
         WHERE invoice_id = c_invoice.invoice_id;
        -- Get GL segments
        SELECT substr(lc.entrp_entity, 2, 2)
              ,lc.entrp_loc
          INTO l_product
              ,l_loc
          FROM xxcus.xxcus_location_code_tbl lc
         WHERE fru = l_fru;

        SELECT substr(gl_code1, 5, 6)
          INTO l_glacct
          FROM xxcus.xxcusap_tms_inv_stg_tbl
         WHERE invoice_id = c_invoice.invoice_id;
        dbms_output.put_line('HERE 6.3');
        --Begin Ver 2.1
             --l_product =16 => Utilities
             --l_product =21 => Electrical
        if ( l_PS_check_flag ='Y' and l_product  IN ('16', '21') ) then 
           l_code_combo1 := l_product || '.' || l_loc || '.' || l_seg3 || '.' ||l_glacct || '.' || l_ps_seg5||'.'||l_ps_seg67;
        else
           l_code_combo1 := l_product || '.' || l_loc || '.' || l_seg3 || '.' ||l_glacct || '.' || l_seg567;           
        end if;     
       --End Ver 2.1                         
               --l_code_combo1 := l_product || '.' || l_loc || '.' || l_seg3 || '.' || --Ver 2.1
                         --l_glacct || '.' || l_seg567;  --Ver 2.1
        dbms_output.put_line('HERE 7');
        -- Insert line 1
        l_sec := 'Insert line 1';
        dbms_output.put_line('invoice num: ' || c_invoice.invoice_id ||
                             ' org id: ' || l_org_id);
        INSERT INTO ap.ap_invoice_lines_interface
          (invoice_id
          ,invoice_line_id
          ,line_number
          ,line_type_lookup_code
          ,amount
           -- ,description
          ,dist_code_concatenated
          ,last_updated_by
          ,last_update_date
          ,last_update_login
          ,creation_date
          ,created_by
          ,attribute_category
          ,attribute1
          ,attribute2
          ,attribute3
          ,attribute4
          ,attribute5
          ,attribute6
          ,attribute7
          ,attribute8
          ,attribute9
          ,attribute10
          ,attribute11
          ,attribute12
          ,attribute13
          ,attribute14
          ,attribute15
          ,org_id)
        VALUES
          (c_invoice.invoice_id
          ,l_invoice_line_id
          ,l_line_num
          ,l_line_type
          ,c_invoice.amount1
           --  ,l_tax_code
          ,l_code_combo1
           --c_inv_line.dist_code_concatenated
          ,g_user_id
          ,SYSDATE
          ,g_login_id
          ,SYSDATE
          ,g_user_id
          ,c_invoice.org_id
           --l_org_id --c_inv_line.attribute_category
          ,c_invoice.attribute1
          ,c_invoice.attribute2
          ,c_invoice.attribute3
          ,c_invoice.attribute4
          ,c_invoice.attribute5
          ,c_invoice.attribute6
          ,c_invoice.attribute7
          ,c_invoice.attribute8
          ,c_invoice.attribute9
          ,c_invoice.attribute10
          ,c_invoice.attribute11
          ,c_invoice.attribute12
          ,c_invoice.attribute13
          ,c_invoice.attribute14
          ,c_invoice.attribute15
          ,c_invoice.org_id
           --l_org_id
           );
        --  COMMIT; -- Version# 1.5
        l_line_num := l_line_num + 1;
        -- l_line_amt_sum :=c_invoice.Amount1;      -- Version# 1.5
        ------------ LINE 2 ---------------------
        IF c_invoice.amount2 IS NOT NULL THEN

          l_invoice_line_id := ap_invoice_lines_interface_s.nextval;
          -- l_line_amt_sum :=l_line_amt_sum+c_invoice.Amount2;            -- Version# 1.5
          -- Get fru
          SELECT substr(gl_code2, 1, 4)
            INTO l_fru
            FROM xxcus.xxcusap_tms_inv_stg_tbl
           WHERE invoice_id = c_invoice.invoice_id;
          -- Get GL segments
          SELECT substr(lc.entrp_entity, 2, 2)
                ,lc.entrp_loc
            INTO l_product
                ,l_loc
            FROM xxcus.xxcus_location_code_tbl lc
           WHERE fru = l_fru;

          SELECT substr(gl_code2, 5, 6)
            INTO l_glacct
            FROM xxcus.xxcusap_tms_inv_stg_tbl
           WHERE invoice_id = c_invoice.invoice_id;
        --Begin Ver 2.1
             --l_product =16 => Utilities
             --l_product =21 => Electrical
        if ( l_PS_check_flag ='Y' and l_product  IN ('16', '21') ) then 
           l_code_combo  := l_product || '.' || l_loc || '.' || l_seg3 || '.' ||l_glacct || '.' || l_ps_seg5||'.'||l_ps_seg67;
        else
           l_code_combo  := l_product || '.' || l_loc || '.' || l_seg3 || '.' ||l_glacct || '.' || l_seg567;           
        end if;     
       --End Ver 2.1 
           --          l_code_combo := l_product || '.' || l_loc || '.' || l_seg3 || '.' || --Ver 2.1 
            --                          l_glacct || '.' || l_seg567;  --Ver 2.1      
          -- Get line type
          BEGIN
            SELECT lookup_code
              INTO l_tax_code
              FROM applsys.fnd_lookup_values flv
             WHERE flv.meaning = l_glacct;
          EXCEPTION
            WHEN no_data_found THEN
              l_tax_code := NULL;
          END;

          --dbms_output.put_line('l_line_type ' || l_line_type);
          --dbms_output.put_line('l_tax_code ' || l_tax_code);
          -------------------------------

          --dbms_output.put_line('Insert line 2');
          -- Insert line 2
          INSERT INTO ap.ap_invoice_lines_interface
            (invoice_id
            ,invoice_line_id
            ,line_number
            ,line_type_lookup_code
            ,amount
            ,description
            ,dist_code_concatenated
            ,last_updated_by
            ,last_update_date
            ,last_update_login
            ,creation_date
            ,created_by
            ,attribute_category
            ,attribute1
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7
            ,attribute8
            ,attribute9
            ,attribute10
            ,attribute11
            ,attribute12
            ,attribute13
            ,attribute14
            ,attribute15
            ,org_id)
          VALUES
            (c_invoice.invoice_id
            ,l_invoice_line_id
            ,l_line_num
            ,l_line_type
            ,c_invoice.amount2
            ,l_tax_code
            ,l_code_combo --c_inv_line.dist_code_concatenated
            ,g_user_id
            ,SYSDATE
            ,g_login_id
            ,SYSDATE
            ,g_user_id
            ,c_invoice.org_id --l_org_id --c_inv_line.attribute_category
            ,c_invoice.attribute1
            ,c_invoice.attribute2
            ,c_invoice.attribute3
            ,c_invoice.attribute4
            ,l_tax_code --c_invoice.attribute5
            ,c_invoice.attribute6
            ,c_invoice.attribute7
            ,c_invoice.attribute8
            ,c_invoice.attribute9
            ,c_invoice.attribute10
            ,c_invoice.attribute11
            ,c_invoice.attribute12
            ,c_invoice.attribute13
            ,c_invoice.attribute14
            ,c_invoice.attribute15
            ,c_invoice.org_id --l_org_id
             );
        --  COMMIT; -- Version# 1.5
          l_line_num := l_line_num + 1;
        END IF;

        ------------ LINE 3 ---------------------
        IF c_invoice.amount3 IS NOT NULL THEN
          -- l_line_amt_sum :=l_line_amt_sum+c_invoice.Amount3;        -- Version# 1.5
          l_invoice_line_id := ap_invoice_lines_interface_s.nextval;

          -- Get fru
          SELECT substr(gl_code3, 1, 4)
            INTO l_fru
            FROM xxcus.xxcusap_tms_inv_stg_tbl
           WHERE invoice_id = c_invoice.invoice_id;
          -- Get GL segments
          SELECT substr(lc.entrp_entity, 2, 2)
                ,lc.entrp_loc
            INTO l_product
                ,l_loc
            FROM xxcus.xxcus_location_code_tbl lc
           WHERE fru = l_fru;

          SELECT substr(gl_code3, 5, 6)
            INTO l_glacct
            FROM xxcus.xxcusap_tms_inv_stg_tbl
           WHERE invoice_id = c_invoice.invoice_id;
        --Begin Ver 2.1
             --l_product =16 => Utilities
             --l_product =21 => Electrical
        if ( l_PS_check_flag ='Y' and l_product  IN ('16', '21') ) then 
           l_code_combo  := l_product || '.' || l_loc || '.' || l_seg3 || '.' ||l_glacct || '.' || l_ps_seg5||'.'||l_ps_seg67;
        else
           l_code_combo  := l_product || '.' || l_loc || '.' || l_seg3 || '.' ||l_glacct || '.' || l_seg567;           
        end if;
       --End Ver 2.1
            --          l_code_combo := l_product || '.' || l_loc || '.' || l_seg3 || '.' || -- Ver 2.1
            --                           l_glacct || '.' || l_seg567;   -- Ver 2.1                                     
          -- Get line type
          BEGIN
            SELECT lookup_code
              INTO l_tax_code
              FROM applsys.fnd_lookup_values flv
             WHERE flv.meaning = l_glacct;
          EXCEPTION
            WHEN no_data_found THEN
              l_tax_code := NULL;
          END;

          -- Insert line 3
          INSERT INTO ap.ap_invoice_lines_interface
            (invoice_id
            ,invoice_line_id
            ,line_number
            ,line_type_lookup_code
            ,amount
            ,description
            ,dist_code_concatenated
            ,last_updated_by
            ,last_update_date
            ,last_update_login
            ,creation_date
            ,created_by
            ,attribute_category
            ,attribute1
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7
            ,attribute8
            ,attribute9
            ,attribute10
            ,attribute11
            ,attribute12
            ,attribute13
            ,attribute14
            ,attribute15
            ,org_id)
          VALUES
            (c_invoice.invoice_id
            ,l_invoice_line_id
            ,l_line_num
            ,l_line_type
            ,c_invoice.amount3
            ,l_tax_code
            ,l_code_combo --c_inv_line.dist_code_concatenated
            ,g_user_id
            ,SYSDATE
            ,g_login_id
            ,SYSDATE
            ,g_user_id
            ,c_invoice.org_id --l_org_id --c_inv_line.attribute_category
            ,c_invoice.attribute1
            ,c_invoice.attribute2
            ,c_invoice.attribute3
            ,c_invoice.attribute4
            ,l_tax_code --c_invoice.attribute5
            ,c_invoice.attribute6
            ,c_invoice.attribute7
            ,c_invoice.attribute8
            ,c_invoice.attribute9
            ,c_invoice.attribute10
            ,c_invoice.attribute11
            ,c_invoice.attribute12
            ,c_invoice.attribute13
            ,c_invoice.attribute14
            ,c_invoice.attribute15
            ,c_invoice.org_id --l_org_id
             );

          l_line_num := l_line_num + 1;
        --  COMMIT; -- Version# 1.5
        END IF;

        ------------ LINE 4 ---------------------
        IF c_invoice.amount4 IS NOT NULL THEN
          -- l_line_amt_sum :=l_line_amt_sum+c_invoice.Amount4;            -- Version# 1.5
          l_invoice_line_id := ap_invoice_lines_interface_s.nextval;

          -- Get fru
          SELECT substr(gl_code4, 1, 4)
            INTO l_fru
            FROM xxcus.xxcusap_tms_inv_stg_tbl
           WHERE invoice_id = c_invoice.invoice_id;
          -- Get GL segments
          SELECT substr(lc.entrp_entity, 2, 2)
                ,lc.entrp_loc
            INTO l_product
                ,l_loc
            FROM xxcus.xxcus_location_code_tbl lc
           WHERE fru = l_fru;

          SELECT substr(gl_code4, 5, 6)
            INTO l_glacct
            FROM xxcus.xxcusap_tms_inv_stg_tbl
           WHERE invoice_id = c_invoice.invoice_id;
        --Begin Ver 2.1
             --l_product =16 => Utilities
             --l_product =21 => Electrical
        if ( l_PS_check_flag ='Y' and l_product  IN ('16', '21') ) then 
           l_code_combo := l_product || '.' || l_loc || '.' || l_seg3 || '.' ||l_glacct || '.' || l_ps_seg5||'.'||l_ps_seg67;
        else
           l_code_combo  := l_product || '.' || l_loc || '.' || l_seg3 || '.' ||l_glacct || '.' || l_seg567;           
        end if;     
       --End Ver 2.1 
            --          l_code_combo := l_product || '.' || l_loc || '.' || l_seg3 || '.' || --Ver 2.1
            --                            l_glacct || '.' || l_seg567;  --Ver 2.1                                       
          -- Get line type
          BEGIN
            SELECT lookup_code
              INTO l_tax_code
              FROM applsys.fnd_lookup_values flv
             WHERE flv.meaning = l_glacct;
          EXCEPTION
            WHEN no_data_found THEN
              l_tax_code := NULL;
          END;
          -- Insert line 4
          INSERT INTO ap.ap_invoice_lines_interface
            (invoice_id
            ,invoice_line_id
            ,line_number
            ,line_type_lookup_code
            ,amount
            ,description
            ,dist_code_concatenated
            ,last_updated_by
            ,last_update_date
            ,last_update_login
            ,creation_date
            ,created_by
            ,attribute_category
            ,attribute1
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7
            ,attribute8
            ,attribute9
            ,attribute10
            ,attribute11
            ,attribute12
            ,attribute13
            ,attribute14
            ,attribute15
            ,org_id)
          VALUES
            (c_invoice.invoice_id
            ,l_invoice_line_id
            ,l_line_num
            ,l_line_type
            ,c_invoice.amount4
            ,l_tax_code
            ,l_code_combo --c_inv_line.dist_code_concatenated
            ,g_user_id
            ,SYSDATE
            ,g_login_id
            ,SYSDATE
            ,g_user_id
            ,c_invoice.org_id --l_org_id --c_inv_line.attribute_category
            ,c_invoice.attribute1
            ,c_invoice.attribute2
            ,c_invoice.attribute3
            ,c_invoice.attribute4
            ,l_tax_code --c_invoice.attribute5
            ,c_invoice.attribute6
            ,c_invoice.attribute7
            ,c_invoice.attribute8
            ,c_invoice.attribute9
            ,c_invoice.attribute10
            ,c_invoice.attribute11
            ,c_invoice.attribute12
            ,c_invoice.attribute13
            ,c_invoice.attribute14
            ,c_invoice.attribute15
            ,c_invoice.org_id --l_org_id
             );

        --  COMMIT; -- Version# 1.5
          l_line_num := l_line_num + 1;
        END IF;

        ------------ LINE 5 ---------------------
        IF c_invoice.amount5 IS NOT NULL THEN

          l_invoice_line_id := ap_invoice_lines_interface_s.nextval;
          -- l_line_amt_sum :=l_line_amt_sum+c_invoice.Amount5;                 -- Version# 1.5
          -- Get fru
          SELECT substr(gl_code5, 1, 4)
            INTO l_fru
            FROM xxcus.xxcusap_tms_inv_stg_tbl
           WHERE invoice_id = c_invoice.invoice_id;
          -- Get GL segments
          SELECT substr(lc.entrp_entity, 2, 2)
                ,lc.entrp_loc
            INTO l_product
                ,l_loc
            FROM xxcus.xxcus_location_code_tbl lc
           WHERE fru = l_fru;

          SELECT substr(gl_code5, 5, 6)
            INTO l_glacct
            FROM xxcus.xxcusap_tms_inv_stg_tbl
           WHERE invoice_id = c_invoice.invoice_id;
        --Begin Ver 2.1
             --l_product =16 => Utilities
             --l_product =21 => Electrical
        if ( l_PS_check_flag ='Y' and l_product  IN ('16', '21') ) then 
           l_code_combo := l_product || '.' || l_loc || '.' || l_seg3 || '.' ||l_glacct || '.' || l_ps_seg5||'.'||l_ps_seg67;
        else
           l_code_combo  := l_product || '.' || l_loc || '.' || l_seg3 || '.' ||l_glacct || '.' || l_seg567;           
        end if;     
       --End Ver 2.1
            --          l_code_combo :=  l_code_combo := l_product || '.' || l_loc || '.' || l_seg3 || '.' || --Ver 2.1
            --                            l_glacct || '.' || l_seg567; --Ver 2.1                                         
          -- Get line type
          BEGIN
            SELECT lookup_code
              INTO l_tax_code
              FROM applsys.fnd_lookup_values flv
             WHERE flv.meaning = l_glacct;
          EXCEPTION
            WHEN no_data_found THEN
              l_tax_code := NULL;
          END;
          -- Insert line 5
          INSERT INTO ap.ap_invoice_lines_interface
            (invoice_id
            ,invoice_line_id
            ,line_number
            ,line_type_lookup_code
            ,amount
            ,description
            ,dist_code_concatenated
            ,last_updated_by
            ,last_update_date
            ,last_update_login
            ,creation_date
            ,created_by
            ,attribute_category
            ,attribute1
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7
            ,attribute8
            ,attribute9
            ,attribute10
            ,attribute11
            ,attribute12
            ,attribute13
            ,attribute14
            ,attribute15
            ,org_id)
          VALUES
            (c_invoice.invoice_id
            ,l_invoice_line_id
            ,l_line_num
            ,l_line_type
            ,c_invoice.amount5
            ,l_tax_code
            ,l_code_combo --c_inv_line.dist_code_concatenated
            ,g_user_id
            ,SYSDATE
            ,g_login_id
            ,SYSDATE
            ,g_user_id
            ,c_invoice.org_id --l_org_id --c_inv_line.attribute_category
            ,c_invoice.attribute1
            ,c_invoice.attribute2
            ,c_invoice.attribute3
            ,c_invoice.attribute4
            ,l_tax_code --c_invoice.attribute5
            ,c_invoice.attribute6
            ,c_invoice.attribute7
            ,c_invoice.attribute8
            ,c_invoice.attribute9
            ,c_invoice.attribute10
            ,c_invoice.attribute11
            ,c_invoice.attribute12
            ,c_invoice.attribute13
            ,c_invoice.attribute14
            ,c_invoice.attribute15
            ,c_invoice.org_id --l_org_id
             );

        --  COMMIT; -- Version# 1.5
          l_line_num := l_line_num + 1;
        END IF;

        ------------ ADJUSTMENT LINE ---------------------
-- Modified for v1.4
/*        IF c_invoice.gross <> c_invoice.net THEN

          l_line_amt        := (c_invoice.gross - c_invoice.net) * -1;*/
         -- 1.4
--- Modified for v1.4
          -- l_diff := abs(c_invoice.net - l_line_amt_sum); -- Version# 1.5
          l_invoice_line_id := ap_invoice_lines_interface_s.nextval;
          l_line_amt_sum := 0;
          l_diff         := 0;

          BEGIN
          SELECT (NVL(amount1,0) + NVL(amount2,0) + NVL(amount3,0) + NVL(amount4,0) + NVL(amount5,0))
            INTO l_line_amt_sum
            FROM xxcus.xxcusap_tms_inv_stg_tbl stg
           WHERE 1 = 1
             AND stg.invoice_id = c_invoice.invoice_id
             AND stg.status = 'NEW';
          EXCEPTION
          WHEN OTHERS THEN
             l_line_amt_sum := 0;
          END;

          l_diff := c_invoice.net - l_line_amt_sum;

        IF l_diff = 0 THEN
           NULL;
        ELSIF ABS(l_diff) > 0.01 AND c_invoice.net > l_line_amt_sum THEN -- $$$$$
           NULL;
        ELSE

          -- Insert adjustment line
          INSERT INTO ap.ap_invoice_lines_interface
            (invoice_id
            ,invoice_line_id
            ,line_number
            ,line_type_lookup_code
            ,amount
            ,description
            ,dist_code_concatenated
            ,last_updated_by
            ,last_update_date
            ,last_update_login
            ,creation_date
            ,created_by
            ,attribute_category
            ,attribute1
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7
            ,attribute8
            ,attribute9
            ,attribute10
            ,attribute11
            ,attribute12
            ,attribute13
            ,attribute14
            ,attribute15
            ,org_id)
          VALUES
            (c_invoice.invoice_id
            ,l_invoice_line_id
            ,l_line_num
            ,l_line_type
            -- ,(c_invoice.net - l_line_amt_sum) --c_invoice.amount5, -- Version# 1.5
            , l_diff
            ,l_adjust_desc
            ,l_code_combo1 --c_inv_line.dist_code_concatenated
            ,g_user_id
            ,SYSDATE
            ,g_login_id
            ,SYSDATE
            ,g_user_id
            ,c_invoice.org_id --l_org_id --c_inv_line.attribute_category
            ,c_invoice.attribute1
            ,c_invoice.attribute2
            ,c_invoice.attribute3
            ,c_invoice.attribute4
            ,c_invoice.attribute5
            ,c_invoice.attribute6
            ,c_invoice.attribute7
            ,c_invoice.attribute8
            ,c_invoice.attribute9
            ,c_invoice.attribute10
            ,c_invoice.attribute11
            ,c_invoice.attribute12
            ,c_invoice.attribute13
            ,c_invoice.attribute14
            ,c_invoice.attribute15
            ,c_invoice.org_id --l_org_id
             );
        END IF;

      -- Version# 1.5 > Start
      EXCEPTION
      WHEN program_error THEN
          ROLLBACK;
          UPDATE xxcus.xxcusap_tms_inv_stg_tbl
             SET status    = 'REJECTED'
               , error_msg = l_errdesc
           WHERE stg_invoice_id = c_invoice.stg_invoice_id;
      WHEN OTHERS THEN
          ROLLBACK;

          l_errdesc := 'Error - '||SQLERRM;

          UPDATE xxcus.xxcusap_tms_inv_stg_tbl
             SET status    = 'REJECTED'
               , error_msg = l_errdesc
           WHERE stg_invoice_id = c_invoice.stg_invoice_id;
      END;
      COMMIT;
      -- Version# 1.5 < End

      END LOOP;

    EXCEPTION
      WHEN OTHERS THEN
        retcode := SQLCODE;
        errbuf  := substr(SQLERRM, 1, 2000);
        fnd_file.put_line(fnd_file.log,
                          'Error in ' || l_procedure_name || l_sec);
        fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
        fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);

    END;

    BEGIN
      --
      -- Submit the invoice import
      --
      FOR c_group IN (SELECT DISTINCT group_id
                                     ,org_id
                        FROM ap.ap_invoices_interface
                       WHERE status IS NULL
                         AND group_id LIKE 'TMS%')
      LOOP

        IF c_group.org_id <> 167 THEN
          --  Setup parameters for running FND JOBS!
          l_can_submit_request := xxcus_misc_pkg.set_responsibility('HDSAPINTERFACE',
                                                                    'HDS Payables Manager (No Suppliers) - US GSC');
          IF l_can_submit_request THEN
            l_globalset := 'Global Variables are set.';

            v_message := 'Submitting Payables Invoice Import';
            fnd_file.put_line(fnd_file.log, v_message);
            v_req_id := fnd_request.submit_request('SQLAP', 'APXIIMPT', NULL,
                                                   '', FALSE, c_group.org_id
                                                   --Operating Unit HD Supply US GSC
                                                  , 'TMS'
                                                   --Source
                                                  , c_group.group_id
                                                   --Group
                                                  , NULL
                                                   --Batch Name
                                                  , NULL
                                                   --NULL --Hold Name
                                                  , NULL
                                                   --Hold Reason
                                                  , NULL
                                                   --GL Date
                                                  , 'N'
                                                   --Purge
                                                  , 'N'
                                                   --Trace Switch
                                                  , 'N'
                                                   --Debug Switch
                                                  , 'N'
                                                   --Summarize Report
                                                  , 1000
                                                   --Commit batch size
                                                   );
            COMMIT;

          ELSE
            l_globalset := 'Global Variables are not set.';
            l_sec       := 'Global Variables are not set for the Responsibility of XXCUS_CON and the User.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            RAISE program_error;
          END IF;
        ELSE
          --  Setup parameters for running FND JOBS!
          l_can_submit_request := xxcus_misc_pkg.set_responsibility('HDSAPINTERFACE',
                                                                    'HDS Payables Transaction Entry - CAD GSC');
          -- Version# 1.5 > Start
          IF l_can_submit_request THEN
            l_globalset := 'Global Variables are set.';

            v_message := 'Submitting Payables Invoice Import';
            fnd_file.put_line(fnd_file.log, v_message);
            v_req_id := fnd_request.submit_request('SQLAP', 'APXIIMPT', NULL,
                                                   '', FALSE, c_group.org_id
                                                   --Operating Unit HD Supply US GSC
                                                  , 'TMS'
                                                   --Source
                                                  , c_group.group_id
                                                   --Group
                                                  , NULL
                                                   --Batch Name
                                                  , NULL
                                                   --NULL --Hold Name
                                                  , NULL
                                                   --Hold Reason
                                                  , NULL
                                                   --GL Date
                                                  , 'N'
                                                   --Purge
                                                  , 'N'
                                                   --Trace Switch
                                                  , 'N'
                                                   --Debug Switch
                                                  , 'N'
                                                   --Summarize Report
                                                  , 1000
                                                   --Commit batch size
                                                   );
            COMMIT;

          ELSE
            l_globalset := 'Global Variables are not set.';
            l_sec       := 'Global Variables are not set for the Responsibility of XXCUS_CON and the User.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            RAISE program_error;
          END IF;
          -- Version# 1.5 < End
        END IF;

        -- get the status of the process.
        IF v_req_id != 0 THEN
          --
          -- Wait for the previous concurrent request to complete
          --
          IF fnd_concurrent.wait_for_request(v_req_id, v_interval,
                                             v_max_time, v_req_phase,
                                             v_req_status, v_dev_phase,
                                             v_dev_status, v_message) THEN
            v_error_message := chr(10) || 'ReqID=' || v_req_id ||
                               ' DPhase ' || v_dev_phase || ' DStatus ' ||
                               v_dev_status || chr(10) || ' MSG - ' ||
                               v_message;
            -- Error Returned
            IF v_dev_phase != 'COMPLETE'
               OR v_dev_status != 'NORMAL' THEN
              l_statement := 'An error occured in the running the invoice import' ||
                             v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_statement);
              fnd_file.put_line(fnd_file.output, l_statement);
              RAISE program_error;
            END IF;

            l_sec := 'Payables Import Completed Request ID:  ' || v_req_id;
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);

            -- Then Success!  Update invoice status in staging table
            l_sec := 'Updated xxcusap_tms_inv_stg_tbl invoice status to PROCESSED';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);

            FOR c_invoice_p IN (SELECT invoice_id
                                  FROM ap.ap_invoices_interface
                                 WHERE group_id = c_group.group_id
                                   AND status = 'PROCESSED')
            LOOP
              UPDATE xxcus.xxcusap_tms_inv_stg_tbl
                 SET status = 'PROCESSED'
               WHERE invoice_id = c_invoice_p.invoice_id;
              COMMIT;
            END LOOP;

            l_sec := 'Updated xxcusap_tms_inv_stg_tbl invoice status to REJECTION';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);

            FOR c_invoice_r IN (SELECT invoice_id
                                  FROM ap.ap_invoices_interface
                                 WHERE group_id = c_group.group_id
                                   AND status = 'REJECTED')
            LOOP
              /*   dbms_output.put_line('group_id: ' || c_group.group_id);
              dbms_output.put_line('invoice_id: ' ||
                                   c_invoice_r.invoice_id);*/
           l_err_invoice :=c_invoice_r.invoice_id;

              BEGIN
                SELECT nvl(ir.displayed_field, 'n/a') --v 1.1
                      ,nvl(ir.description, 'n/a')
                  INTO l_errcode
                      ,l_errdesc
                  FROM apps.ap_interface_rejections_v ir
                 WHERE ir.invoice_id = c_invoice_r.invoice_id
                   AND line_number = 'Invoice'
                   AND rownum = 1; -- handle dup issue.  --v1.1
              EXCEPTION
                WHEN no_data_found THEN
                  l_errcode := 'Invoice line error';
                  l_errdesc := 'See open interface for invoice line error';
              END;

              /*   dbms_output.put_line('UPDATE invoice_id: ' ||
              c_invoice_r.invoice_id);*/
              UPDATE xxcus.xxcusap_tms_inv_stg_tbl
                 SET status    = 'REJECTED'
                    ,error_msg = 'Invoice rejected in Oracle Open Interface.  Request ID: ' ||
                                 v_req_id || ' Error: ' || l_errcode ||
                                 ' - ' || l_errdesc
               WHERE invoice_id = c_invoice_r.invoice_id;
              COMMIT;

            END LOOP;

          END IF;
        END IF;
      END LOOP;
    END;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for xxcusap_freight_inv_import_pkg.load_inv',
                                           p_request_id => g_request_id,
                                           p_argument1 => 'INVOICE #' ,           --v1.1
                                           p_argument2 => l_err_invoice,  --v1.1
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error during procedure load_inv',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');
    WHEN OTHERS THEN

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for xxcusap_freight_inv_import_pkg.load_inv',
                                           p_request_id => g_request_id,
                                           p_argument1 => 'INVOICE #' ,           --v1.1
                                           p_argument2 => l_err_invoice,  --v1.1
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error during procedure load_inv',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');

  END load_inv;

  /*=============================================================================
  Last Update Date : 12-Aug-2013
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     12-Aug-2013   Luong Vu           Creation - Program to cleanup interface
                                           invoices.
  =============================================================================
  *****************************************************************************/

  PROCEDURE intf_maint(errbuf  OUT VARCHAR2
                      ,retcode OUT NUMBER) IS

    l_err_msg       VARCHAR2(3000);
    l_err_code      NUMBER;
    l_sec           VARCHAR2(2000);
    l_err_invoice     NUMBER;
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUSAP_FREIGHT_INV_IMPORT_PKG.inft_maint';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --l_distro_list    VARCHAR2(75) DEFAULT 'luong.vu@hdsupply.com';
    l_procedure_name VARCHAR2(75) := 'intf_maint';
    --l_org            hr_all_organization_units.organization_id%TYPE;

  BEGIN

    l_sec := 'Update invoice status in staging table';

    FOR mrec IN (SELECT stg.invoice_id
                       ,aii.status
                   FROM xxcus.xxcusap_tms_inv_stg_tbl stg
                       ,ap.ap_invoices_interface      aii
                  WHERE stg.invoice_id = aii.invoice_id
                    AND aii.group_id LIKE 'TMS%'
                    AND stg.status = 'REJECTED'
                    AND aii.status = 'PROCESSED')
    LOOP

--v.1.1
l_err_invoice := mrec.invoice_id;

    l_sec := 'Update xxcusap_tms_inv_stg_tbl';

      UPDATE xxcus.xxcusap_tms_inv_stg_tbl
         SET status = mrec.status
       WHERE invoice_id = mrec.invoice_id;
    END LOOP;
    COMMIT;

    l_sec := 'Delete TMS invoice from stagint table';
    DELETE FROM xxcus.xxcusap_tms_inv_stg_tbl
     WHERE creation_date < add_months(trunc(SYSDATE), -24);

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for xxcusap_freight_inv_import_pkg.intf_maint',
                                           p_request_id => g_request_id,
                                           p_argument1 => 'INVOICE #' ,           --v1.1
                                           p_argument2 => l_err_invoice,  --v1.1
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error during procedure interface_maintenance at '||l_sec,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');
    WHEN OTHERS THEN

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for xxcusap_freight_inv_import_pkg.intf_maint',
                                           p_request_id => g_request_id,
                                           p_argument1 => 'INVOICE #' ,           --v1.1
                                           p_argument2 => l_err_invoice,  --v1.1
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error during procedure interface_maintenanceat '||l_sec,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');
  END intf_maint;

  /*=============================================================================
  Last Update Date : 12-Aug-2013
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     12-Aug-2013   Luong Vu           Creation this package - Main freight
                                           invoice import program.

  =============================================================================
  *****************************************************************************/


  PROCEDURE main(errbuf  OUT VARCHAR2
                ,retcode OUT NUMBER) IS

    v_req_id        NUMBER := fnd_global.conc_request_id;
    v_req_phase     VARCHAR2(80);
    v_req_status    VARCHAR2(80);
    v_dev_phase     VARCHAR2(30);
    v_dev_status    VARCHAR2(30);
    v_interval      NUMBER := 10; -- In seconds
    v_max_time      NUMBER := 15000; -- In seconds
    v_error_message VARCHAR2(3000);

    l_can_submit_request BOOLEAN := TRUE;
    l_user               VARCHAR2(50) DEFAULT 'HDSAPINTERFACE';
    l_responsibility     VARCHAR2(250) DEFAULT 'XXCUS_CON';
    l_globalset          VARCHAR2(100);
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(2000) DEFAULT NULL;
    l_statement          VARCHAR2(9000);
    l_err_callfrom       VARCHAR2(75) DEFAULT 'XXCUSAP_FREIGHT_INV_IMPORT_PKG.MAIN';
    --l_distro_list        VARCHAR2(75) DEFAULT 'luong.vu@hdsupply.com';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_procedure_name VARCHAR2(75) := 'MAIN';


  BEGIN
    --dbms_output.enable(1000000);

    BEGIN
      l_sec := 'Setup parameters for running FND JOBS!';
      --  Setup parameters for running FND JOBS!
      l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user,
                                                                l_responsibility);
      IF l_can_submit_request THEN
        l_globalset := 'Global Variables are set.';

      ELSE

        l_globalset := 'Global Variables are not set.';
        l_sec       := 'Global Variables are not set for the Responsibility of XXCUS_CON and the User.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        RAISE program_error;
      END IF;
    END;

    BEGIN
      --
      -- Submit TMS invoice import
      --
      l_sec := 'Submitting TMS Invoice Import';
      fnd_file.put_line(fnd_file.log, l_sec);

      v_req_id := fnd_request.submit_request('XXCUS', 'XXCUSAP_TMS_INV_LOAD');
      COMMIT;

      -- get the status of the process.
      IF v_req_id != 0 THEN
        --
        -- Wait for the previous concurrent request to complete
        --
        IF fnd_concurrent.wait_for_request(v_req_id, v_interval, v_max_time,
                                           v_req_phase, v_req_status,
                                           v_dev_phase, v_dev_status, l_sec) THEN
          v_error_message := chr(10) || 'ReqID=' || v_req_id || ' DPhase ' ||
                             v_dev_phase || ' DStatus ' || v_dev_status ||
                             chr(10) || ' MSG - ' || l_sec;
          -- Error Returned
          IF v_dev_phase != 'COMPLETE'
             --OR v_dev_status != 'NORMAL'
             THEN
            l_statement := 'An error occured in the running TMS invoice import' ||
                           v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          END IF;
        END IF;
      END IF;

      l_sec := 'TMS Invoice Import Completed Request ID:  ' || v_req_id;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    END;

    BEGIN
      --
      -- Submit TMS invoice validation
      --
      l_sec := 'Initiate TMS Invoice Validation Program';
      fnd_file.put_line(fnd_file.log, 'l_sec ='||l_sec);

      v_req_id := fnd_request.submit_request('XXCUS', 'XXCUS_TMS_INV_VALID');
      COMMIT;
       fnd_file.put_line(fnd_file.log, '@ Initiate TMS Invoice Validation Program, v_req_id ='||v_req_id);
      -- get the status of the process.
      IF v_req_id != 0 THEN
        --
        -- Wait for the previous concurrent request to complete
        --
        IF fnd_concurrent.wait_for_request(v_req_id, v_interval, v_max_time,
                                           v_req_phase, v_req_status,
                                           v_dev_phase, v_dev_status, l_err_msg) THEN
          v_error_message := chr(10) || 'ReqID=' || v_req_id || ' DPhase ' ||
                             v_dev_phase || ' DStatus ' || v_dev_status ||
                             chr(10) || ' MSG - ' || l_err_msg;
          fnd_file.put_line(fnd_file.log, '@ Initiate TMS Invoice Validation Program, v_error_message ='||v_error_message);
          -- Error Returned
          IF v_dev_phase != 'COMPLETE'
             OR v_dev_status != 'NORMAL' THEN
            l_statement := 'An error occured in the running the TMS invoice validation' ||
                           v_error_message || '.';
                           fnd_file.put_line(fnd_file.log, '@ B4 Initiate TMS Invoice Validation Program, l_statement ='||l_statement);
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
            fnd_file.put_line(fnd_file.log, '@ After Initiate TMS Invoice Validation Program, l_statement ='||l_statement);
          END IF;
        END IF;
      END IF;

      l_sec := 'TMS invoice validation Completed Request ID:  ' || v_req_id;
      fnd_file.put_line(fnd_file.log, '@ TMS invoice validation Completed Request ID:  ' || v_req_id);
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    END;

    BEGIN
      --
      -- Submit freight invoice import
      --
      l_sec := 'Initiate Freight Invoice Import Program';
      fnd_file.put_line(fnd_file.log, l_sec);

      v_req_id := fnd_request.submit_request('XXCUS', 'XXCUS_FRT_INV_IMPORT');
      COMMIT;

      -- get the status of the process.
      IF v_req_id != 0 THEN
        --
        -- Wait for the previous concurrent request to complete
        --
        IF fnd_concurrent.wait_for_request(v_req_id, v_interval, v_max_time,
                                           v_req_phase, v_req_status,
                                           v_dev_phase, v_dev_status, l_err_msg) THEN
          v_error_message := chr(10) || 'ReqID=' || v_req_id || ' DPhase ' ||
                             v_dev_phase || ' DStatus ' || v_dev_status ||
                             chr(10) || ' MSG - ' || l_err_msg;
          -- Error Returned
          IF v_dev_phase != 'COMPLETE'
             OR v_dev_status != 'NORMAL' THEN
            l_statement := 'An error occured in the running the freight invoice import program' ||
                           v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          END IF;
        END IF;
      END IF;

      l_sec := 'Freight invoice import Completed Request ID:  ' || v_req_id;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    END;

    --
    -- TMS Staging table maintenance
    --
    l_sec := 'TMS Staging table maintenance';
    xxcusap_freight_inv_import_pkg.intf_maint(errbuf, retcode);
    COMMIT;


  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for xxcusap_freight_inv_import_pkg.main',
                                           p_request_id => g_request_id,
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error in interface_maintenance at '||l_sec,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');
    WHEN OTHERS THEN

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => 'Concurrent Program exception for xxcusap_freight_inv_import_pkg.main',
                                           p_request_id => g_request_id,
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error in interface_maintenance'||l_sec,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');

  END;

END xxcusap_freight_inv_import_pkg;
/