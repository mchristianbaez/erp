CREATE OR REPLACE PACKAGE BODY APPS.XXWC_VENDOR_QUOTE_TMP_PKG  as


   /*************************************************************************
     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        11/10/2014   Vijaysrinivasan      TMS#20141002-00054 Multi org changes
   *************************************************************************/
      
    procedure GET_HEADER_ID (P_PROCESS_ID in number,P_INV_START_DATE in date,P_INV_END_DATE in date) is

    Type t_header_rec is record
    (
     t_header_id number,
     t_line_id number
     );

    Type t_header is table of t_header_rec;
     V_HEADER_ID t_header;
   l_count number;
  cursor xxcur_header_id is
  SELECT DISTINCT PROCESS_ID,HEADER_ID,LINE_ID
  FROM 
        --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
        XXWC_VENDOR_QUOTE_TEMP
  WHERE PROCESS_ID = P_PROCESS_ID;
 cursor c_insert_temp is

     SELECT OL.HEADER_ID,LINE_ID
       FROM   OE_ORDER_LINES OL,
      RA_CUSTOMER_TRX_LINES RCTL,
      RA_CUST_TRX_LINE_GL_DIST RCTLGL
     WHERE ol.flow_status_code             = 'CLOSED'
    AND RCTL.INTERFACE_LINE_CONTEXT     = 'ORDER ENTRY'
    AND RCTL.INTERFACE_LINE_ATTRIBUTE6  = TO_CHAR (OL.LINE_ID)
  --  AND rctl.sales_order                = oh.order_number
    AND RCTL.LINE_TYPE                  = 'LINE'
    AND rctlgl.customer_trx_line_id = rctl.customer_trx_line_id
  --  AND RCT.CUSTOMER_TRX_ID IN (6248691,6266238)
   -- and TRUNC(RCT.TRX_DATE) between '26-Sep-2013' and '04-Oct-2013';
    and rctlgl.GL_DATE between P_INV_START_DATE and P_INV_END_DATE;

     CURSOR XX_CURR_DET(P_HEADER_ID NUMBER,p_line_id number)
IS
SELECT NAME,
    salesperson_name,
    salespeson_number,
    master_account_name,
    master_account_number,
    job_name,
    job_number,
    vendor_number,
    vendor_name,
    oracle_quote_number,
    invoice_number,
    invoice_date,
    part_number,
    uom,
    description,
    bpa,
    po_cost po_cost,
    average_cost average_cost,
    special_cost special_cost,
    (po_cost - special_cost) unit_claim_value,
    (po_cost - special_cost) * qty rebate,
    gl_coding,
    gl_string,
    loc,
    created_by,
    customer_trx_id,
    order_number,
    line_number,
    creation_date,
    location,
    QTY
  FROM
    (SELECT
      /*+ index (QPL XXWC_QP_LIST_LINES_N2) */
      qll.start_date_active ,
      qlh.start_date_active ,
      qll.end_date_active,
      qlh.end_date_active,
      RCT.TRX_DATE,
      QLH. NAME,
     ( select NAME
      FROM RA_SALESREPS
      where SALESREP_ID =oh.SALESREP_ID) SALESPERSON_NAME,
    --  jrs.salesrep_number salespeson_number,
     ( select salesrep_number
      FROM RA_SALESREPS
      where SALESREP_ID =oh.SALESREP_ID) salespeson_number,
      hcs.account_name master_account_name,
      hcs.account_number master_account_number,
      hcsu.LOCATION job_name,
      hps.party_site_number job_number,
      pov.segment1 vendor_number,
      pov.vendor_name vendor_name,
      xxcpl.vendor_quote_number oracle_quote_number,
      rct.trx_number invoice_number,
      TRUNC(rct.trx_date) invoice_date,
      msi.concatenated_segments part_number,
      msi.primary_unit_of_measure uom,
      msi.description description,
      CASE
        WHEN upper (pov.vendor_name) LIKE 'SIMPSON%'
        THEN xxeis.eis_rs_xxwc_com_util_pkg.get_best_buy ( msi.inventory_item_id, msi.organization_id, pov.vendor_id)
        ELSE NULL
      END bpa,
      NVL ( xxeis.eis_rs_xxwc_com_util_pkg.get_order_line_po_cost ( msi.inventory_item_id, msi.organization_id, ol.creation_date), msi.list_price_per_unit) po_cost,
      xxcpl.average_cost average_cost,
      xxcpl.special_cost special_cost,
      '400-900'
      || '-'
      || gcc.segment2 gl_coding,
      gcc.concatenated_segments gl_string,
      gcc.segment2 loc,
      ppf1.full_name created_by,
      rct.customer_trx_id customer_trx_id,
      oh.order_number order_number,
      ol.line_number line_number,
      oh.creation_date creation_date,
      mp.organization_code location,
      NVL(rctl.quantity_invoiced, rctl.quantity_credited) QTY,
        (ROW_NUMBER ()
             OVER (
                PARTITION BY OH.HEADER_ID,
                             OL.LINE_ID
                           --  QQ.QUALIFIER_ATTRIBUTE
                ORDER BY QQ.QUALIFIER_ATTRIBUTE)) list_num
    FROM oe_order_headers oh,
      oe_order_lines ol,
      qp_qualifiers qq,
      qp_list_headers qlh,
      apps.xxwc_om_contract_pricing_hdr xxcph,
      apps.xxwc_om_contract_pricing_lines xxcpl,
      qp_list_lines qll,
      mtl_system_items_b_kfv msi,
      fnd_user fu,
      per_all_people_f ppf1,
      PO_VENDORS POV,
      --ra_salesreps jrs,
      ra_customer_trx rct,
      ra_customer_trx_lines rctl,
      ra_cust_trx_line_gl_dist rctlgl,
      gl_code_combinations_kfv gcc,
      hz_cust_accounts hcs,
      hz_parties hp,
      hz_cust_site_uses hcsu,
      hz_cust_acct_sites hcas,
      hz_party_sites hps,
      MTL_PARAMETERS MP
      --      XXEIS.XXWC_VENDOR_QUOTE_TEMP TEMP
    WHERE 1                         =1
    AND oh.header_id                = ol.header_id
    AND ol.flow_status_code         = 'CLOSED'
    AND qq.qualifier_context        = 'CUSTOMER'
    AND qq.comparison_operator_code = '='
    AND qq.active_flag              = 'Y'
    AND ( (qlh.attribute10          = ('Contract Pricing')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.ship_to_org_id)) )
    OR (qlh.attribute10             = ('Contract Pricing')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.sold_to_org_id)) )
    OR (qlh.attribute10             = ('CSP-NATIONAL')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.sold_to_org_id)) ) )
    AND qlh.list_header_id          = qq.list_header_id
    AND qlh.active_flag             = 'Y'
    AND xxcph.agreement_id          = TO_CHAR(qlh.attribute14)
      --AND xxcph.revision_number           = TO_CHAR(qlh.attribute15)
    AND xxcpl.agreement_id              = xxcph.agreement_id
    AND xxcpl.agreement_type            = 'VQN'
    AND qlh.list_header_id              = qll.list_header_id
    AND xxcpl.product_value             = msi.segment1
    AND xxcpl.agreement_line_id         = TO_CHAR(qll.attribute2)
    AND msi.inventory_item_id           = ol.inventory_item_id
    AND msi.organization_id             = ol.ship_from_org_id
    AND fu.user_id                      = oh.created_by
    AND (fu.employee_id(+)               = ppf1.person_id
    AND SYSDATE BETWEEN ppf1.effective_start_date AND ppf1.effective_end_date)
    AND pov.vendor_id                   = xxcpl.vendor_id
--    AND oh.salesrep_id                  = jrs.salesrep_id(+)
--    AND oh.org_id                       = jrs.org_id(+)
    AND rct.interface_header_context    = 'ORDER ENTRY'
    AND rct.interface_header_attribute1 = TO_CHAR(oh.order_number)
    AND rctl.interface_line_context     = 'ORDER ENTRY'
    AND rctl.interface_line_attribute6  = TO_CHAR (ol.line_id)
      --AND rctl.sales_order                = oh.order_number
    AND rctl.customer_trx_id        = rct.customer_trx_id
    AND rctl.line_type              = 'LINE'
    AND rctlgl.customer_trx_id      = rctl.customer_trx_id
    AND rctlgl.customer_trx_line_id = rctl.customer_trx_line_id
    AND rctlgl.account_class        = 'REV'
    AND gcc.code_combination_id     = rctlgl.code_combination_id
    AND hcs.cust_account_id         = rct.ship_to_customer_id
    AND hcs.party_id                = hp.party_id
    AND hcsu.site_use_id            = rct.ship_to_site_use_id
    AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
    AND hcas.party_site_id          = hps.party_site_id
    AND MP.ORGANIZATION_ID          = MSI.ORGANIZATION_ID
      --AND QLL.START_DATE_ACTIVE          <= NVL(QLL.END_DATE_ACTIVE,RCT.TRX_DATE)
   -- AND (TRUNC(NVL(QLL.START_DATE_ACTIVE,RCT.TRX_DATE))<=TRUNC(RCT.TRX_DATE)
   -- AND TRUNC(NVL(QLL.END_DATE_ACTIVE,RCT.TRX_DATE))   >= TRUNC(RCT.TRX_DATE))
     -- and (rctlgl.GL_DATE) between P_INV_START_DATE and P_INV_END_DATE
      /*AND (TRUNC(QLL.END_DATE_ACTIVE)  >=TRUNC(RCT.TRX_DATE)
      OR QLL.END_DATE_ACTIVE           IS NULL)*/
    AND OH.HEADER_ID = P_HEADER_ID
    and Ol.LINE_ID  = p_line_id
    )
   WHERE list_num =1;

BEGIN
BEGIN
    FND_FILE.PUT_LINE(FND_FILE.log,'Start of the Process '||to_char(sysdate,'DD-MON-YYYY HH24-MI-SS'));
OPEN c_insert_temp;
LOOP
FETCH c_insert_temp BULK COLLECT INTO v_header_id LIMIT 1000;

FORALL i IN 1 .. v_header_id.COUNT
INSERT INTO 
    --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
    XXWC_VENDOR_QUOTE_TEMP
(PROCESS_ID,HEADER_ID,line_id)
VALUES
(p_process_id,v_header_id(i).t_header_id,v_header_id(i).t_line_id);

EXIT WHEN c_insert_temp%NOTFOUND;
END LOOP;
CLOSE C_INSERT_TEMP;
COMMIT;
FND_FILE.PUT_LINE(FND_FILE.log,'After insertion of headers into temp table'||to_char(sysdate,'DD-MON-YYYY HH24-MI-SS'));

  EXCEPTION
  when OTHERS then
    FND_FILE.PUT_LINE(FND_FILE.LOG,SQLERRM);
end;

BEGIN
for i in xxcur_header_id
LOOP
 l_count:=0;

   SELECT COUNT(OH.HEADER_ID)
   INTO   l_count
      FROM  OE_ORDER_HEADERS OH,
            OE_ORDER_LINES OL,
            QP_QUALIFIERS QQ,
            QP_LIST_HEADERS QLH,
            QP_LIST_LINES QLL,
            MTL_SYSTEM_ITEMS_B_KFV MSI,
            MTL_PARAMETERS MP,
            APPS.XXWC_OM_CONTRACT_PRICING_HDR XXCPH,
            APPS.XXWC_OM_CONTRACT_PRICING_LINES XXCPL,
            PO_VENDORS POV
    WHERE 1=1
        AND OH.HEADER_ID                = OL.HEADER_ID
        AND MP.ORGANIZATION_ID          = MSI.ORGANIZATION_ID
        AND MSI.INVENTORY_ITEM_ID           = OL.INVENTORY_ITEM_ID
        AND MSI.ORGANIZATION_ID             = OL.SHIP_FROM_ORG_ID
        AND qq.qualifier_context        = 'CUSTOMER'
    AND QQ.COMPARISON_OPERATOR_CODE = '='
   --  AND ol.flow_status_code         = 'CLOSED'
    AND qq.active_flag              = 'Y'
    AND ( (qlh.attribute10          = ('Contract Pricing')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.ship_to_org_id)) )
    OR (qlh.attribute10             = ('Contract Pricing')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.sold_to_org_id)) )
    OR (qlh.attribute10             = ('CSP-NATIONAL')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.sold_to_org_id)) ) )
    AND QLH.LIST_HEADER_ID          = QQ.LIST_HEADER_ID
    AND QLH.ACTIVE_FLAG             = 'Y'
    AND QLH.LIST_HEADER_ID          = QLL.LIST_HEADER_ID
    AND XXCPH.AGREEMENT_ID          = TO_CHAR(QLH.ATTRIBUTE14)
     AND XXCPL.AGREEMENT_ID              = XXCPH.AGREEMENT_ID
    AND XXCPL.AGREEMENT_TYPE            = 'VQN'
    AND XXCPL.PRODUCT_VALUE             = DECODE(ol.ordered_item,MSI.SEGMENT1,ol.ordered_item,MSI.SEGMENT1)
  --  AND XXCPL.PRODUCT_VALUE             = ol.ordered_item
    AND XXCPL.AGREEMENT_LINE_ID         = TO_CHAR(QLL.ATTRIBUTE2)
        AND POV.VENDOR_ID                   = XXCPL.VENDOR_ID
   -- AND PRODUCT_VALUE='137HTT5'
    AND oh.HEADER_id =i.header_id
    and ol.line_id   = i.line_id;


  if  l_count<>0
  THEN

 FOR XX_REC_DET IN XX_curr_DET(i.header_id,i.line_id)
 LOOP
 begin
 insert into xxeis.EIS_XXWC_OM_VENDOR_QUOTE_V
      values(
      xx_rec_det.NAME,
    xx_rec_det.salesperson_name,
    xx_rec_det.salespeson_number,
    xx_rec_det.master_account_name,
    xx_rec_det.master_account_number,
    xx_rec_det.job_name,
    xx_rec_det.job_number,
    xx_rec_det.vendor_number,
    xx_rec_det.vendor_name,
    xx_rec_det.oracle_quote_number,
    xx_rec_det.invoice_number,
    xx_rec_det.invoice_date,
    xx_rec_det.part_number,
    xx_rec_det.uom,
    xx_rec_det.description,
    xx_rec_det.bpa,
    xx_rec_det.po_cost,
    xx_rec_det.average_cost ,
    xx_rec_det.special_cost ,
    xx_rec_det.unit_claim_value,
    xx_rec_det. rebate,
    xx_rec_det.gl_coding,
    xx_rec_det.gl_string,
    xx_rec_det.loc,
    xx_rec_det.created_by,
    xx_rec_det.customer_trx_id,
    xx_rec_det.order_number,
    xx_rec_det.line_number,
    xx_rec_det.creation_date,
    xx_rec_det.location,
    XX_REC_DET.QTY,
    P_PROCESS_ID,
    xxeis.eis_rs_common_outputs_s.nextval
    );
COMMIT;
EXCEPTION
WHEN OTHERS THEN
FND_FILE.PUT_LINE(FND_FILE.LOG,SQLERRM);
end;
END LOOP;
end if;
END LOOP;
EXCEPTION WHEN OTHERS THEN
FND_FILE.PUT_LINE(FND_FILE.LOG,SQLERRM);
END;

END;
end XXWC_VENDOR_QUOTE_TMP_PKG;
/