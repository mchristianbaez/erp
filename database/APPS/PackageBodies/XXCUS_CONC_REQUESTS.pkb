CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_CONC_REQUESTS AS

   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************

     PURPOSE:   

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        02/11/2013  Balaguru Seshadri      Initial Version
   
    ESMS Ticket Info
    ===============
    189210 -2/11/2013 -Migrate to production   
   ************************************************************************* */
   
   Function check_instance Return Varchar2 Is
      v_flag varchar2(1) :='N';      
   Begin 
        SELECT 'Y'
        INTO   v_flag
        FROM   xxcus.xxcus_monitor_cp_instance A
              ,V$DATABASE B      
        WHERE  1 =1
          AND  B.NAME =A.DB_NAME;
        
        RETURN v_flag;
   Exception
      When No_Data_Found Then
       Return v_flag;
      When Too_Many_Rows Then
       Return v_flag;
      When Others Then    
       Return v_flag;
   End check_instance;   
   
   Procedure Notify
     (
       p_request_id in number
      ,p_program in varchar2
      ,p_requested_by in varchar2
      ,p_request_status in varchar2
      ,p_email in varchar2      
     ) Is
      ln_request_id Number :=0;
   Begin 
        ln_request_id :=fnd_request.submit_request
              (
               application      =>'XXCUS',
               program          =>'XXCUS_NOTIFY_CP_STATUS',
               description      =>'',
               start_time       =>'',
               sub_request      =>FALSE,
               argument1        =>p_request_id,
               argument2        =>p_program,
               argument3        =>p_requested_by,
               argument4        =>p_request_status, 
               argument5        =>p_email --'Start: '||p_started_at||' End:'||p_ended_at                                                                            
              );
          
          if ln_request_id >0 then 
             commit work; 
          else
             Null; 
          end if;    
   Exception
      When Others Then    
       Rollback;
   End Notify;   
   
   Function CP_Req_Monitor
                     (
                        P_Subscription_Guid In Raw
                       ,P_Event             In Out Nocopy Wf_Event_T
                     )
      Return Varchar2 Is
      L_Api_Name    Constant Varchar2(30) := 'CP_Req_Monitor';
      L_Api_Version Constant Number := 1.0;
      L_Rule           Varchar2(20);
      L_Parameter_List Wf_Parameter_List_T := Wf_Parameter_List_T();
      L_Parameter_T    Wf_Parameter_T      := Wf_Parameter_T(Null, Null);
      L_Parameter_Name Varchar2(2000);
      Param_Index      Pls_Integer;
      L_Event_Key      Varchar2(240);
      L_Event_Name     Varchar2(50);
      L_Receipt_Id     Number;
      V_Error_Message  Varchar2(2000) :=Null;
      v_user           fnd_user.user_name%type :=Null;
      v_email_id       fnd_user.email_address%type :=Null;
      v_warning_email_id fnd_user.email_address%type :=Null;
      v_error_email_id   fnd_user.email_address%type :=Null;
      v_resp_name      fnd_responsibility_vl.responsibility_name%type :=Null;
      v_program_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null; 
      v_cp_status      Varchar2(40) :=Null;
      v_status         Varchar2(40) :=Null;
      n_request_id     Number :=0;
      d_started_at     Varchar2(20) :=Null;
      d_ended_at       Varchar2(20) :=Null;
      n_cp_id          Number :=0;
      v_warning        Varchar2(1) :=Null;
      v_error          Varchar2(1) :=Null;
      v_request_set_name Varchar2(150) :=Null;
      
   cursor my_request(n_request_id in number) is
   select a.user_concurrent_program_name
          ,a.concurrent_program_id
          ,a.phase||' -'||a.status
          ,a.status 
          ,a.user_name
          ,nvl(b.email_when_warning, 'Z')
          ,nvl(b.email_when_error, 'Z')
          ,c.email_address
          ,b.warning_email_address
          ,b.error_email_address
    from fnd_amp_requests_v a
        ,xxcus.xxcus_monitor_cp_b b
        ,fnd_user c
    where 1 =1 
     /*    
      -- 75400, 66413, 70401 are programs owned by SALES and FULFILLMENT
      -- 36119, 37122, 66426, 32766 are programs owned by SUPPLY CHAIN
      -- 59379, 58383 are finance programs
      and b.concurrent_program_id in (75400, 66413, 70401, 36119, 37122, 66426, 32766, 59379, 58383)
      and c.user_name in ( 'RV003897', 'BS006141', 'RT008057', 'SS026369','DC005370', 'ID020048'
                           ,'MM027735', 'GLINTERFACE', 'HDSHRINTERFACE', 'HDSAPINTERFACE', 'XXWC_INT_FINANCE'                        
                         ) --for now we will focus on few users
     */                         
      and a.user_concurrent_program_name !='Report Set'
      and a.request_id =n_request_id
      and a.program_application_id =b.application_id
      and a.concurrent_program_id =b.concurrent_program_id
      and a.requested_by =c.user_id
      and a.requested_by =(
                            case 
                              when b.user_id is null then a.requested_by
                              else b.user_id
                            end
                          )
  UNION
    select a.program
          ,a.concurrent_program_id
          ,a.phase||' -'||a.status
          ,a.status 
          ,a.user_name
          ,nvl(b.email_when_warning, 'Z')
          ,nvl(b.email_when_error, 'Z')
          ,c.email_address
          ,b.warning_email_address
          ,b.error_email_address
    from fnd_amp_requests_v a
        ,xxcus.xxcus_monitor_cp_b b
        ,fnd_user c
        ,fnd_request_sets_tl rset
        ,fnd_concurrent_requests fndcr                                
    where 1 =1
    /*
      and c.user_name in ( 'RV003897', 'BS006141', 'RT008057', 'SS026369','DC005370', 'ID020048'
                           ,'MM027735', 'GLINTERFACE', 'HDSHRINTERFACE', 'HDSAPINTERFACE', 'XXWC_INT_FINANCE'                        
                         ) --for now we will focus on few users
    */                         
      and a.user_concurrent_program_name ='Report Set'
      and fndcr.request_id =n_request_id
      and a.request_id =fndcr.request_id
      and a.program_application_id =b.application_id
      and rset.application_id =fndcr.argument1 --argument1 is request set application id 
      and rset.request_set_id =fndcr.argument2 --argument2 is request set id   
      and b.request_set_id  =rset.request_set_id 
      and fndcr.requested_by =c.user_id
      and fndcr.requested_by =(
                            case 
                              when b.user_id is null then fndcr.requested_by
                              else b.user_id
                            end
                          );         
      
   Begin 
      If (check_instance ='Y') Then           
   
              L_Parameter_List := P_Event.Getparameterlist();
              L_Event_Key      := P_Event.Geteventkey();
           
              If L_Parameter_List Is Not Null Then
              
                 Param_Index := L_Parameter_List.First; 
                              
                 While (Param_Index <= L_Parameter_List.Last) 
                 
                 Loop         
                      If L_Parameter_List(Param_Index).Getname() ='REQUEST_ID' then
                      
                        n_request_id :=to_number(L_Parameter_List(Param_Index).Getvalue());
                        
                        Begin   
                            open my_request(n_request_id =>to_number(L_Parameter_List(Param_Index).Getvalue()));
                            fetch my_request into v_program_name
                                      ,n_cp_id
                                      ,v_cp_status
                                      ,v_status
                                      ,v_user
                                      ,v_warning
                                      ,v_error
                                      ,v_email_id
                                      ,v_warning_email_id
                                      ,v_error_email_id;
                            close my_request;                                                                                                                               
                        Exception                         
                         When Others Then
                           v_program_name      :=Null;
                           n_cp_id             :=Null;
                           v_cp_status         :=Null;
                           v_status            :=Null;
                           v_user              :=Null;
                           v_warning           :=Null;
                           v_error             :=Null;
                           v_email_id          :=Null;
                           v_warning_email_id  :=Null;
                           v_error_email_id    :=Null;
                        End;
                        
                      Else
                       --We do not require other parameters
                       Null;
                       
                      End If;                      
                                                        
                    Param_Index :=L_Parameter_List.Next(Param_Index);
                    
                 End Loop;                                 

                 If (v_status ='Warning' And v_warning ='Y') Then                 
                 
                   Notify (n_request_id, v_program_name, v_user, v_cp_status, nvl(v_warning_email_id, v_email_id));  
                 
                 ElsIf (v_status ='Error' And v_error ='Y') Then

                   Notify (n_request_id, v_program_name, v_user, v_cp_status, nvl(v_error_email_id, v_email_id));
 
                 Else
                 
                   Null;
                   
                 End If;

              End If;              
              
          Return 'SUCCESS'; 
      Else
        Null; --We are in one of those instance that does not require concurrent program monitoring
      End If;
   Exception
      When Others Then
       WF_CORE.CONTEXT('oracle.apps.fnd.concurrent.request.completed',
                             p_event.getEventName( ), p_subscription_guid);
       WF_EVENT.setErrorInfo(p_event, 'ERROR');      
         Return 'ERROR';
   End CP_Req_Monitor;    

END XXCUS_CONC_REQUESTS;
/
