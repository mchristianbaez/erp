CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OZF_CRE_UPD_API AS
/**************************************************************************
   $Header XXWC_OZF_CRE_UPD_API $
   Module Name: XXWC_OZF_CRE_UPD_API.pks

   PURPOSE:   This package body is called by the concurrent programs
              XXWC Create/Update OZF FND Objects for creating and updating the request group 
              used for Trade Management module in EBS system.
   REVISIONS:
   Ver        Date        Author            Description
   ---------  ----------  ---------------   -------------------------
    1.0       03/08/2018  Ashwin Sridhar    Initial Build - Task ID: TMS#20180713-00060
/*************************************************************************/

--Global Variables...
g_loc VARCHAR2(20000);
g_request_group_name     VARCHAR2(100):='Trade Management Request Group';
g_request_group_code     VARCHAR2(50) :='OZF_CONCURRENT_PROGRAMS_11510';
g_application            VARCHAR2(10) :='OZF';
g_new_request_group_code VARCHAR2(50) :='HDS_OZF_CONCURRENT_PRGS_11510';

/*************************************************************************
  Procedure : Write_Log

  PURPOSE:   This procedure logs debug message in Concurrent Log file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author            Description
  ---------  ----------  ---------------   -------------------------
   1.0       03/08/2018  Ashwin Sridhar    Initial Build - Task ID: TMS#20180713-00060
************************************************************************/
PROCEDURE write_log (p_debug_msg IN VARCHAR2) IS

BEGIN
      
  fnd_file.put_line (fnd_file.LOG, p_debug_msg);
  DBMS_OUTPUT.put_line (p_debug_msg);

END write_log;

/*************************************************************************
  Procedure : write_error

  PURPOSE:   This procedure logs debug message in Concurrent Out file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author            Description
  ---------  ----------  ---------------   -------------------------
   1.0       03/08/2018  Ashwin Sridhar    Initial Build - Task ID: TMS#20180713-00060
************************************************************************/
--Add message to concurrent output file
PROCEDURE write_error (p_debug_msg IN VARCHAR2) IS

l_req_id          NUMBER := fnd_global.conc_request_id;
l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_OZF_CRE_UPD_API';
l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

BEGIN

  xxcus_error_pkg.xxcus_error_main_api (
      p_called_from         => l_err_callfrom,
      p_calling             => l_err_callpoint,
      p_request_id          => l_req_id,
      p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
      p_error_desc          => 'Error running XXWC_COSTAR_AP_EXPORT with PROGRAM ERROR',
      p_distribution_list   => l_distro_list,
      p_module              => 'HRMS');

END write_error;

PROCEDURE CREATE_NEW_REQGROUP_P(p_errbuf  OUT VARCHAR2
                               ,p_retcode OUT VARCHAR2
                                ) IS

--Cursor to fetch the existing request group...
CURSOR cu_req_group IS
SELECT b.CONCURRENT_PROGRAM_NAME PROGRAMNAME,c.APPLICATION_NAME APPNAME,'Program' program_type
FROM   apps.FND_REQUEST_GROUP_UNITS a 
,      apps.fnd_concurrent_programs b 
,      apps.fnd_application_tl c 
,      apps.FND_REQUEST_GROUPS d
WHERE  a.request_Group_id=d.request_Group_id 
AND    a.REQUEST_UNIT_ID=b.CONCURRENT_PROGRAM_ID 
AND    B.APPLICATION_ID=C.APPLICATION_ID 
AND    D.REQUEST_GROUP_NAME =g_request_group_name
AND    D.REQUEST_GROUP_CODE =g_request_group_code
AND    c.language = 'US'
UNION ALL
SELECT b.REQUEST_SET_NAME PROGRAMNAME,c.APPLICATION_SHORT_NAME APPNAME,'Set' program_type
FROM   apps.FND_REQUEST_GROUP_UNITS a 
,      apps.FND_REQUEST_SETS b 
,      apps.fnd_application_vl c 
,      apps.FND_REQUEST_GROUPS d
WHERE  a.request_Group_id   = d.request_Group_id 
AND    a.REQUEST_UNIT_ID    = b.request_set_id 
AND    B.APPLICATION_ID     = C.APPLICATION_ID 
AND    D.REQUEST_GROUP_NAME = g_request_group_name
AND    D.REQUEST_GROUP_CODE = g_request_group_code;

ln_group_count      NUMBER:=0;
ln_group_unit_count NUMBER:=0;
lv_group_name       VARCHAR2(100):='HDSXX Trade Mgmt Request Group';
lv_group_desc       VARCHAR2(100):='HDSXX Trade Mgmt Request Group';

BEGIN

  write_log('Program Started...');

  --Create New Request Group...
  FND_PROGRAM.REQUEST_GROUP(request_group    => lv_group_name
                           ,application      => g_application
                           ,code             => g_new_request_group_code
                           ,description      => lv_group_desc
                           );

  COMMIT;
  
  SELECT COUNT(1)
  INTO   ln_group_count
  FROM   apps.FND_REQUEST_GROUPS
  WHERE  REQUEST_GROUP_NAME=lv_group_name;
  
  IF ln_group_count>0 THEN
  
    write_log('New Request group HDSXX Trade Mgmt Request Group created successfully ...');
  
  END IF;
           
  --Adding the programs to the new group from old request group...
  FOR rec_req_group IN cu_req_group LOOP 
  
    write_log('Program name added...'||rec_req_group.PROGRAMNAME);
    write_log('Program Type...'||rec_req_group.program_type);
    write_log('Request Group to attach...'||lv_group_name);
  
    IF rec_req_group.program_type='Program' THEN
  
      BEGIN
      
        FND_PROGRAM.ADD_TO_GROUP(program_short_name  =>rec_req_group.PROGRAMNAME
                                ,program_application =>rec_req_group.APPNAME
                                ,request_group       =>lv_group_name
                                ,group_application   =>g_application
                                );
        
      EXCEPTION
      WHEN others THEN
      
        write_log('Error in adding the program to request group program name '||rec_req_group.PROGRAMNAME
                 ||' Error Details-'||SQLERRM);
      
      END;
  
    --For Request Set
    ELSE
  
      BEGIN
      
        FND_SET.add_set_to_group(request_set       =>rec_req_group.PROGRAMNAME
                                ,set_application   =>rec_req_group.APPNAME
                                ,request_group     =>lv_group_name
                                ,group_application =>g_application
                                );
        
      EXCEPTION
      WHEN others THEN
      
        write_log('Error in adding the program to request group program name '||rec_req_group.PROGRAMNAME
                 ||' Error Details-'||SQLERRM);
      
      END;

    END IF;

  END LOOP;

  COMMIT;
  
  SELECT COUNT(1)
  INTO   ln_group_unit_count
  FROM   apps.FND_REQUEST_GROUP_UNITS FDG
  ,      apps.FND_REQUEST_GROUPS      FRG
  WHERE  FDG.REQUEST_GROUP_ID         = FRG.REQUEST_GROUP_ID
  AND    FDG.APPLICATION_ID           = FRG.APPLICATION_ID
  AND    REQUEST_GROUP_NAME           = lv_group_name
  AND    frg.REQUEST_GROUP_CODE       = g_new_request_group_code;
  
  --Removing the specific request sets from the new group...
  IF ln_group_unit_count>0 THEN
  
    write_log('Request group units added to the new group successfully with count.'||ln_group_unit_count);

    write_log('Calling the PROCEDURE REMOVE_REQSET_FROM_REQGROUP_P');

    REMOVE_REQSET_FROM_REQGROUP_P(lv_group_name);
  
  END IF;
  
  --Updating the responsibilities with the new group...
  IF ln_group_unit_count>0 THEN
  
    write_log('Calling the PROCEDURE UPDATE_RESP_WITH_REQGROUP_P');

    UPDATE_RESP_WITH_REQGROUP_P(lv_group_name);
  
  END IF;
    
  write_log ('XXWC Create/Update OZF FND Objects - Program Successfully completed');
  
EXCEPTION 
WHEN others THEN

  write_log('Exception in procedure CREATE_NEW_REQGROUP_P '||SQLERRM);

END CREATE_NEW_REQGROUP_P;

PROCEDURE REMOVE_REQSET_FROM_REQGROUP_P(p_group_name IN VARCHAR2) IS

--Cursor to find the programs to be removed from the new group...
CURSOR cu_nreq_group(cp_group_name IN VARCHAR2) IS
SELECT b.REQUEST_SET_NAME PROGRAMNAME,c.APPLICATION_SHORT_NAME APPNAME
FROM   apps.FND_REQUEST_GROUP_UNITS a 
,      apps.FND_REQUEST_SETS b 
,      apps.fnd_application_vl c 
,      apps.FND_REQUEST_GROUPS d
WHERE  a.request_Group_id   = d.request_Group_id 
AND    a.REQUEST_UNIT_ID    = b.request_set_id 
AND    B.APPLICATION_ID     = C.APPLICATION_ID 
AND    D.REQUEST_GROUP_NAME = cp_group_name
AND    D.REQUEST_GROUP_CODE = g_new_request_group_code
AND EXISTS
(SELECT 1
 FROM  apps.FND_REQUEST_SETS_TL FRS
 WHERE B.REQUEST_SET_ID=FRS.REQUEST_SET_ID
 AND   frs.user_request_set_name LIKE'HDS%Creat%');

BEGIN

  FOR rec_nreq_group IN cu_nreq_group(p_group_name) LOOP
  
    write_log('Request Set Name '||rec_nreq_group.PROGRAMNAME);
    write_log('Application Name '||rec_nreq_group.APPNAME);
  
    BEGIN
  
      fnd_set.remove_set_from_group
           (request_set       => rec_nreq_group.PROGRAMNAME
           ,set_application   => rec_nreq_group.APPNAME
           ,request_group     => p_group_name
           ,group_application => g_application
            );
  
    END;
  
  END LOOP;
  
  COMMIT;

EXCEPTION 
WHEN others THEN

  write_log('Exception in procedure REMOVE_REQSET_FROM_REQGROUP_P '||SQLERRM);

END REMOVE_REQSET_FROM_REQGROUP_P;

PROCEDURE UPDATE_RESP_WITH_REQGROUP_P(p_group_name IN VARCHAR2) IS

--Cursor to find the responsibilities to be updated with new group...
CURSOR cu_resp_name IS
SELECT frv.responsibility_id,
               FRV.APPLICATION_ID,
               FRV.DATA_GROUP_APPLICATION_ID,
               FRV.DATA_GROUP_ID,
               FRV.MENU_ID,
               frv.web_host_name,
               FRV.WEB_AGENT_NAME,
               FRV.GROUP_APPLICATION_ID,
               FRV.REQUEST_GROUP_ID,
               FRV.RESPONSIBILITY_KEY,
               frv.responsibility_name,
               frv.description,
               FRV.START_DATE,
               frv.version
FROM  apps.FND_RESPONSIBILITY_VL FRV
,     apps.FND_REQUEST_GROUPS    FRG
WHERE FRG.REQUEST_GROUP_NAME = g_request_group_name
AND   FRG.REQUEST_GROUP_CODE = g_request_group_code
AND   FRV.APPLICATION_ID     = FRG.APPLICATION_ID
AND   FRV.REQUEST_GROUP_ID   = FRG.REQUEST_GROUP_ID
AND   FRV.responsibility_name LIKE'HDS%'
AND  (FRV.END_DATE IS NULL OR FRV.END_DATE>=SYSDATE);

ln_application_id   NUMBER;
ln_request_group_id NUMBER;

BEGIN

  BEGIN 

    SELECT REQUEST_GROUP_ID
    ,      APPLICATION_ID
    INTO   ln_request_group_id
    ,      ln_application_id
    FROM   apps.FND_REQUEST_GROUPS
    WHERE  REQUEST_GROUP_NAME =p_group_name
    AND    REQUEST_GROUP_CODE =g_new_request_group_code;

  EXCEPTION
  WHEN no_data_found THEN
  
    ln_request_group_id:=NULL;
    ln_application_id:=NULL;
  
  WHEN others THEN
  
    ln_request_group_id:=NULL;
    ln_application_id:=NULL;
  
  END;

  IF ln_request_group_id IS NOT NULL THEN
  
    FOR rec_resp_name IN cu_resp_name LOOP
    
      BEGIN
    
        FND_RESPONSIBILITY_PKG.UPDATE_ROW (
           X_RESPONSIBILITY_ID           => rec_resp_name.responsibility_id,
           X_APPLICATION_ID              => rec_resp_name.application_id,
           X_WEB_HOST_NAME               => rec_resp_name.web_host_name,
           X_WEB_AGENT_NAME              => rec_resp_name.web_agent_name,
           X_DATA_GROUP_APPLICATION_ID   => rec_resp_name.data_group_application_id,
           X_DATA_GROUP_ID               => rec_resp_name.data_group_id,
           X_MENU_ID                     => rec_resp_name.menu_id,
           X_START_DATE                  => rec_resp_name.start_date,
           X_END_DATE                    => NULL,
           X_GROUP_APPLICATION_ID        => ln_application_id,
           X_REQUEST_GROUP_ID            => ln_request_group_id,
           X_VERSION                     => rec_resp_name.version,
           X_RESPONSIBILITY_KEY          => rec_resp_name.responsibility_key,
           X_RESPONSIBILITY_NAME         => rec_resp_name.responsibility_name,
           X_DESCRIPTION                 => rec_resp_name.description,
           X_LAST_UPDATE_DATE            => SYSDATE,
           X_LAST_UPDATED_BY             => -1,
           X_LAST_UPDATE_LOGIN           => 0
           );
    
        COMMIT;
    
          write_log(rec_resp_name.responsibility_name || ' has been updated !!!');
        
      EXCEPTION
      WHEN others THEN
    
        write_log('Inner Exception: ' || SQLERRM);
    
      END;
    
    END LOOP;

  END IF;

EXCEPTION 
WHEN others THEN

  write_log('Exception in procedure UPDATE_RESP_WITH_REQGROUP_P '||SQLERRM);
  
END UPDATE_RESP_WITH_REQGROUP_P;

END XXWC_OZF_CRE_UPD_API;
/