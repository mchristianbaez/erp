CREATE OR REPLACE PACKAGE BODY APPS.xxcusfa_tax_interface_pkg AS
  --/* $Header: XXCUSFA_TAX_INTERFACE_PKG.pkg 12.1.1 2011/05/03 12:00:00 vjayasee ship $ */

  -- ****************************************************************
  -- ****************************************************************
  -- *
  -- * Application : HDSFA
  -- * Module      : FA TAX Book Interface
  -- * Version     : 
  -- * Creation    : 03-May-2011  V.Jayaseelan
  -- *
  -- * Purpose     : Used for FA Books Interface
  -- *
  -- *  Change History
  -- *
  -- *  Developer      Date         Description
  -- *  ------------  -----------  ----------------------------------
  -- *  V.Jayaseelan  03-May-2011   Initial Version
  -- *
  -- ****************************************************************
  PROCEDURE print_out(p_message IN VARCHAR2) IS
  BEGIN
    IF fnd_global.conc_request_id > -1 THEN
      fnd_file.put_line(fnd_file.output, p_message);
    ELSE
      dbms_output.put_line(p_message);
    END IF;
  END print_out;
  PROCEDURE print_log(p_message IN VARCHAR2) IS
  BEGIN
    IF fnd_global.conc_request_id > -1 THEN
      fnd_file.put_line(fnd_file.log, p_message);
    ELSE
      dbms_output.put_line(p_message);
    END IF;
  END print_log;
  /*********************************************************************************************************
     Procedue submit_request will submit "Upload Tax Book Interface" concurrent program to process record from
     interface tables for given batch id.
     If any plsql error occur then an error message is assigned to x_err_msg
  *********************************************************************************************************/
  PROCEDURE submit_request(p_book_type_code IN VARCHAR2
                          ,p_rcv_request_id OUT NOCOPY NUMBER
                          ,p_error_msg      OUT NOCOPY VARCHAR2) AS
    v_interval    NUMBER := 60; --Time to wait between checks. This is the number of seconds to sleep.
    v_max_wait    NUMBER := 1200; --The maximum time in seconds to wait for the requests completion
    v_rphase      VARCHAR2(30);
    v_rstatus     VARCHAR2(30);
    v_dstatus     VARCHAR2(30);
    v_dphase      VARCHAR2(30) := 'PENDING';
    v_message     VARCHAR2(240);
    v_wait_status BOOLEAN;
  BEGIN
    p_rcv_request_id := fnd_request.submit_request('OFA',
                                                    -- APPLICATION SHORT NAME
                                                   'FATAXUP',
                                                    -- CONC PROGRAM NAME
                                                   NULL, NULL, FALSE,
                                                   p_book_type_code);
  
    IF p_rcv_request_id = 0 THEN
      p_error_msg := 'ERROR WHILE SUBMITING RECEIVING TRANSACTION PROCESSOR.';
    ELSE
      COMMIT;
      v_wait_status := fnd_concurrent.wait_for_request(request_id => p_rcv_request_id,
                                                       INTERVAL => v_interval,
                                                       max_wait => v_max_wait,
                                                       phase => v_rphase,
                                                       status => v_rstatus,
                                                       dev_phase => v_dphase,
                                                       dev_status => v_dstatus,
                                                       message => v_message);
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      p_error_msg := 'PLSQL ERROR WHILE SUBMITING RECEIVING TRANSACTION PROCESSOR ' ||
                     ' - ' || substr(SQLERRM, 1, 75);
  END submit_request;
  PROCEDURE load_fa_tax_book(p_errbuf    OUT VARCHAR2
                            ,p_retcode   OUT VARCHAR2
                            ,p_book_type IN VARCHAR2
                            ,p_period    IN VARCHAR2) IS
  
    CURSOR cur_ast_val(p_asset_number VARCHAR2
                      ,p_bk_type      VARCHAR2) IS
      SELECT a.deprn_method_code
        FROM fa.fa_books       a
            ,apps.fa_additions b
       WHERE a.asset_id = b.asset_id
         AND b.asset_number = p_asset_number
         AND a.book_type_code = p_bk_type
         AND a.date_ineffective IS NULL;
  
    CURSOR cur_exists(p_asset_number IN VARCHAR2
                     ,p_bk_type      IN VARCHAR2) IS
      SELECT 'X'
        FROM fa_tax_interface fa
       WHERE fa.book_type_code = p_book_type
         AND fa.asset_number = p_asset_number;
  
    CURSOR cur_main_data IS
      SELECT fa.asset_number asset_number
            ,book.book_type_code book_type_code
            ,book.original_cost original_cost
            ,book.cost new_cost
            ,fa.attribute1 attribute1
            ,fa.attribute2 attribute2
            ,fa.attribute3 attribute3
            ,fa.attribute4 attribute4
            ,fa.attribute5 attribute5
            ,fa.attribute6 attribute6
            ,fa.attribute7 attribute7
            ,fa.attribute8 attribute8
            ,'POST' posting_status
            ,book.date_placed_in_service date_placed_in_service
            ,book.life_in_months life_in_months
        FROM apps.fa_additions@r12_to_fin          fa
            ,fa.fa_distribution_history@r12_to_fin dh
            ,fa.fa_deprn_detail@r12_to_fin         ds
            ,fa.fa_books@r12_to_fin                book
            ,fa.fa_deprn_periods@r12_to_fin        fdp
            ,fa.fa_locations@r12_to_fin            loc
            ,fa.fa_categories_b@r12_to_fin         cat
            ,gl.gl_code_combinations@r12_to_fin    bk
       WHERE fa.asset_id = dh.asset_id
         AND dh.units_assigned = fa.current_units
         AND ds.distribution_id = dh.distribution_id
         AND dh.transaction_header_id_in IS NOT NULL
         AND dh.transaction_header_id_out IS NULL
         AND dh.date_ineffective IS NULL
         AND dh.asset_id = ds.asset_id
         AND ds.book_type_code = p_book_type
         AND book.book_type_code = p_book_type
         AND fdp.book_type_code = p_book_type
         AND book.asset_id = fa.asset_id
         AND book.date_ineffective IS NULL
         AND ds.period_counter =
             (SELECT MAX(period_counter)
                FROM apps.fa_financial_inquiry_deprn_v@r12_to_fin ot
               WHERE ot.asset_id = fa.asset_id
                 AND ot.book_type_code = p_book_type)
         AND fdp.period_counter = ds.period_counter
         AND loc.location_id = dh.location_id
         AND fa.asset_category_id = cat.category_id
         AND dh.code_combination_id = bk.code_combination_id;
  
    CURSOR cur_cst_val(p_asset_number VARCHAR2
                      ,p_bk_type      VARCHAR2
                      ,p_new_cost     NUMBER) IS
      SELECT 'X'
        FROM fa.fa_books       a
            ,apps.fa_additions b
       WHERE a.asset_id = b.asset_id
         AND b.asset_number = p_asset_number
         AND a.book_type_code = p_bk_type
         AND a.cost = p_new_cost;
  
    v_exists     VARCHAR2(1);
    v_report     NUMBER;
    v_request_id NUMBER;
    v_deprn_mtd  VARCHAR2(12);
    v_valid_cost VARCHAR2(1);
    v_err_msg    VARCHAR2(5000);
  BEGIN
    v_request_id := NULL;
    v_err_msg    := NULL;
    print_out(rpad('Book Type Code', 20, ' ') ||
              rpad('Asset Number', 15, ' ') || 'Asset Cost');
    print_out(rpad('-', length('Book Type Code'), '-') ||
              rpad(' ', 20 - length('Book Type Code'), ' ') ||
              rpad('-', length('Asset Number'), '-') ||
              rpad(' ', 15 - length('Asset Number'), ' ') ||
              rpad('-', length('Asset Cost'), '-'));
    FOR lp_dat IN cur_main_data
    LOOP
      v_exists     := NULL;
      v_deprn_mtd  := NULL;
      v_valid_cost := NULL;
      OPEN cur_ast_val(lp_dat.asset_number, lp_dat.book_type_code);
      FETCH cur_ast_val
        INTO v_deprn_mtd;
      IF cur_ast_val%NOTFOUND THEN
        print_log('Asset Number Not Found: ' || lp_dat.asset_number ||
                  ' For Tax Book: ' || lp_dat.book_type_code);
      ELSE
        OPEN cur_cst_val(lp_dat.asset_number, lp_dat.book_type_code,
                         lp_dat.new_cost);
        FETCH cur_cst_val
          INTO v_deprn_mtd;
        IF cur_cst_val%FOUND THEN
          print_out(rpad(lp_dat.book_type_code, 20, ' ') ||
                    rpad(lp_dat.asset_number, 15, ' ') || lp_dat.new_cost);
        ELSE
          OPEN cur_exists(lp_dat.asset_number, lp_dat.book_type_code);
          FETCH cur_exists
            INTO v_exists;
          IF cur_exists%NOTFOUND THEN
            v_report := 1;
            INSERT INTO apps.fa_tax_interface
              (asset_number
              ,book_type_code
              ,original_cost
              ,cost
              ,attribute1
              ,attribute2
              ,attribute3
              ,attribute4
              ,attribute5
              ,attribute6
              ,attribute7
              ,attribute8
              ,posting_status
              ,date_placed_in_service
              ,life_in_months
              ,deprn_method_code)
            VALUES
              (lp_dat.asset_number
              ,lp_dat.book_type_code
              ,lp_dat.original_cost
              ,lp_dat.new_cost
              ,lp_dat.attribute1
              ,lp_dat.attribute2
              ,lp_dat.attribute3
              ,lp_dat.attribute4
              ,lp_dat.attribute5
              ,lp_dat.attribute6
              ,lp_dat.attribute7
              ,lp_dat.attribute8
              ,lp_dat.posting_status
              ,lp_dat.date_placed_in_service
              ,lp_dat.life_in_months
              ,v_deprn_mtd);
          END IF;
          CLOSE cur_exists;
        END IF;
        CLOSE cur_cst_val;
      END IF;
      CLOSE cur_ast_val;
    END LOOP;
    COMMIT;
    IF v_report = 1 THEN
      print_out(chr(10) ||
                '********************* END OF REPORT *********************' ||
                chr(10));
      submit_request(p_book_type_code => p_book_type,
                     p_rcv_request_id => v_request_id,
                     p_error_msg => v_err_msg);
      print_out('Submitted Request ID ' || v_request_id || '.');
      print_log('Submitted Request ID ' || v_request_id || '.');
      print_log('View log details for Conc. Request ' || v_request_id ||
                ' for more information.');
      print_log(v_err_msg);
      p_errbuf := v_err_msg;
    ELSE
      print_log(chr(10) || chr(10) ||
                '********************* No Data Found *********************');
    END IF;
  END load_fa_tax_book;
END xxcusfa_tax_interface_pkg;
/
