CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_CUST_ACCT_UPDATE_PKG
AS
   /**************************************************************************
    *
    * PACKAGE
    * XXWC_AR_CUST_ACCT_UPDATE_PKG
    *
    * DESCRIPTION
    *  FIN / Mass update of accounts that haven't had activity within the last XX month
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
    * ----------------- -------- ---------------------------------------------
    *
    *
    *
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
     * HISTORY
    * =======
    *
    * VERSION DATE           AUTHOR(S)               DESCRIPTION
    * ------- ----------- ---------------------- ------------------------------------
    * 1.0    12/17/2013   HARSHAVARDHAN YEDLA            Creation
    * 1.1    04/28/2014   MAHARAJAN SHUNMUGAM        TMS#20140422-00290
    * 1.2    09/15/2014   PATTABHI AVULA 	         TMS#20141001-00029
    **********************************************************************************/

   PROCEDURE UPDATE_CACCT_PTRADE_CUSTCLASS (v_errbuf     NUMBER,
                                            v_retcode    VARCHAR2)
   AS
      CURSOR c1
      IS
         SELECT xxwc.ROWID rid, xxwc.*
           FROM XXWC_CACCT_ATTR_UPDATE_STG xxwc
          WHERE process_flag = 'N';


      r_organizationrec           apps.hz_party_v2pub.organization_rec_type;
      r_blan_organizationrec      apps.hz_party_v2pub.organization_rec_type;
      r_partyrec                  apps.hz_party_v2pub.party_rec_type;
      r_blankpartyrec             apps.hz_party_v2pub.party_rec_type;
      v_returnstatus              VARCHAR2 (100) := NULL;
      v_msgcount                  NUMBER (10) := NULL;
      v_msgdata                   VARCHAR2 (32000) := NULL;
      v_partyid                   NUMBER := NULL;
      v_partynumber               VARCHAR2 (2000) := NULL;
      v_orgprofileid              NUMBER (10) := NULL;
      v_errordetails              VARCHAR2 (32000) := NULL;
      v_recerrorflag              CHAR (1) := 'N';
      v_party_name                VARCHAR2 (100);
      v_party_number              NUMBER (10);
      -------------------------------------------CUSTOMER ACCOUNTS----------------------------
      r_custprofilerec            hz_customer_profile_v2pub.customer_profile_rec_type;
      r_blancustprofilerec        hz_customer_profile_v2pub.customer_profile_rec_type;
      r_custaccountrec            hz_cust_account_v2pub.cust_account_rec_type;
      r_blankcustaccountrec       hz_cust_account_v2pub.cust_account_rec_type;
      -- v_cust_account_id         NUMBER;
      -- v_account_number          VARCHAR2 (20);
      v_profile_id                NUMBER;
      v_exist                     VARCHAR2 (2);
      v_party_exist               VARCHAR2 (2);
      v_party_id                  NUMBER;
      v_collector_id              NUMBER;
      v_credit_analyst_id         NUMBER;
      v_profile_class_id          NUMBER;
      v_object_ver_no             NUMBER;
      v_cust_account_profile_id   NUMBER;
      v_account_name              VARCHAR2 (100);
      v_account_number            NUMBER;
      v_cust_account_id           NUMBER;
      v_dat_file                  VARCHAR2 (100);
      v_dat_file_path             VARCHAR2 (100);
      v_ctl_file                  VARCHAR2 (100);
      v_ctl_file_path             VARCHAR2 (100);
      v_log_file_path             VARCHAR2 (100);
      ln_req_id                   NUMBER;
      l_dev_phase                 VARCHAR2 (50);
      l_dev_status                VARCHAR2 (50);
      l_get_request_status        BOOLEAN;
      l_phase                     VARCHAR2 (50);
      l_status                    VARCHAR2 (50);
      l_message                   VARCHAR2 (3000);
      ------------------------------------------------------------------------------------------
      v_object_version_number     NUMBER;
      --v_account_name            VARCHAR2 (200);
      v_pexist                    VARCHAR2 (2);
      l_user_id                   NUMBER := fnd_global.user_id;   -- 17/09/2014 Commented by Pattabhi 20141001-00029 Canada OU testing 
      l_resp_id                   NUMBER;
      l_appl_id                   NUMBER ; -- := 222;                -- Receivables
      l_ou_id                     NUMBER;  --:= 162;  -- 17/09/2014 Commented by Pattabhi 20141001-00029 Canada OU testing 
	  

   BEGIN
   
     /* -- commented by pattabhi from here for 20141001-00029 Canada OU testing
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = 'XXWC_INT_SALESFULFILLMENT';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_user_id := 3;
      END;

      BEGIN
         SELECT responsibility_id
           INTO l_resp_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_key = 'XXWC_CUSTOMER_MAINTENANCE'
                AND application_id = l_appl_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_resp_id := 50992;    -- XXWC_CUSTOMER_MAINTENANCE responsibility
      END;


   
      BEGIN
         SELECT organization_id
           INTO l_ou_id
           FROM hr_operating_units
          WHERE short_code = 'HDSWCORG';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_ou_id := 162;                             -- HDS White Cap - Org
      END;
    -----   -- commented by pattabhi ends here for 20141001-00029 Canada OU testing */
	 
	 l_ou_id      :=mo_global.get_current_org_id;     -- 17/09/2014 Added by Pattabhi for 20141001-00029 Canada OU testing
     l_resp_id    :=fnd_global.resp_id;               -- 17/09/2014 Added by Pattabhi for 20141001-00029 Canada OU testing
     l_appl_id    :=fnd_global.resp_appl_id;          -- 17/09/2014 Added by Pattabhi for 20141001-00029 Canada OU testing
	 
      -- set Oracle Applications environment
      apps.fnd_global.apps_initialize (user_id        => l_user_id,
                                       resp_id        => l_resp_id,
                                       resp_appl_id   => l_appl_id);

      -- 161 = HDS White Cap operating unit
      apps.mo_global.set_policy_context ('S', l_ou_id);
      apps.mo_global.init ('AR');

      BEGIN
         SELECT attribute1 DAT_FILE,
                attribute2 DAT_FILE_PATH,
                attribute3 CTL_FILE,
                attribute4 CTL_FILE_PATH,
                attribute5 LOG_FILE_PATH
           INTO V_DAT_FILE,
                V_DAT_FILE_PATH,
                V_CTL_FILE,
                V_CTL_FILE_PATH,
                V_LOG_FILE_PATH
           FROM fnd_lookup_values
          WHERE     lookup_type = 'XXWC_LOADER_COMMON_LOOKUP'
                AND LANGUAGE = USERENV ('LANG')
                AND MEANING = 'XXWCCAAUP'			--Added and commented below for ver 1.1
--                       (SELECT CONCURRENT_PROGRAM_NAME
--                          FROM FND_CONCURRENT_PROGRAMS
--                         WHERE CONCURRENT_PROGRAM_ID =
--                                  FND_GLOBAL.CONC_PROGRAM_ID)
                AND ENABLED_FLAG = 'Y'
                AND TRUNC (SYSDATE) BETWEEN TRUNC (START_DATE_ACTIVE)
                                        AND TRUNC (
                                               NVL (END_DATE_ACTIVE, SYSDATE));
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'In Others Of Common Lookup..' || SQLERRM);
            v_errordetails := 'In Others Of Common Lookup..' || SQLERRM;
      END;


      fnd_file.put_line (
         fnd_file.LOG,
         'V_DAT_FILE,V_DAT_FILE_PATH,V_CTL_FILE,V_CTL_FILE_PATH,V_LOG_FILE_PATH..');
      fnd_file.put_line (
         fnd_file.LOG,
            v_dat_file
         || ','
         || v_dat_file_path
         || ','
         || v_ctl_file
         || ','
         || v_ctl_file_path
         || ','
         || v_log_file_path);


      Fnd_file.put_line (fnd_file.LOG, 'Calling Genreic Loader Program...');

      BEGIN
         ln_req_id :=
            fnd_request.submit_request (application   => 'XXWC',
                                        program       => 'XXWCGLP',
                                        description   => NULL,
                                        start_time    => SYSDATE,
                                        sub_request   => FALSE,
                                        argument1     => v_dat_file,
                                        argument2     => v_dat_file_path,
                                        argument3     => v_ctl_file,
                                        argument4     => v_ctl_file_path,
                                        argument5     => v_log_file_path);
         COMMIT;



         fnd_file.put_line (fnd_file.LOG,
                            'Request for batch submit..' || ln_req_id);



         IF ln_req_id > 0
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
               'In Submitting the XXWC Genric Loader Program');
         END IF;

         l_get_request_status :=
            FND_CONCURRENT.WAIT_FOR_REQUEST (phase        => l_phase,
                                             request_id   => ln_req_id,
                                             INTERVAL     => 10,
                                             max_wait     => 0,
                                             STATUS       => l_status,
                                             dev_phase    => l_dev_phase,
                                             dev_status   => l_dev_status,
                                             MESSAGE      => l_message);
         fnd_file.put_line (fnd_file.LOG, 'Program Phase :' || l_dev_phase);
         fnd_file.put_line (fnd_file.LOG, 'Program Status :' || l_dev_status);
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'In Others Of wait for request...' || SQLERRM);
            v_recerrorflag := 'Y';
      END;

      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         'Account Number| Account Name | Predominant Trade | Customer Class ');

      FOR i IN c1
      LOOP
         r_custaccountrec := r_blankcustaccountrec;

         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            'Processing Customer Account...' || i.account_number);

         BEGIN
            SELECT hp.PARTY_ID,
                   hp.PARTY_NAME,
                   hca.cust_account_id,
                   hca.object_version_number
              INTO V_PARTY_ID,
                   v_party_name,
                   v_cust_account_id,
                   v_object_version_number
              FROM HZ_CUST_ACCOUNTS HCA, hz_parties hp
             WHERE     HP.PARTY_ID = HCA.PARTY_ID
                   AND hca.ACCOUNT_NUMBER = i.account_number
--                   AND NVL (UPPER(hca.account_name), '--xx--') =		--commented condition as per confirmation from Steve for ver 1.1
--                          NVL (UPPER(i.account_name), '--xx--')
                   AND hca.status = 'A';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               V_PARTY_ID := NULL;
               v_party_name := NULL;
               v_cust_account_id := NULL;
               v_object_version_number := NULL;
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'In Others Of validating customer account details..'
                  || SQLERRM);
               v_recerrorflag := 'Y';
               v_errordetails :=
                     'In Others Of validating customer account details..'
                  || SQLERRM;
         END;



         IF    (i.PRED_TRADE IS NOT NULL
            OR i.CUST_CLASS IS NOT NULL)		--Modified AND to OR for ver 1.1
            AND V_PARTY_ID IS NOT NULL
         THEN

        IF i.CUST_CLASS IS NOT NULL THEN		--Added IF clause for ver 1.1
            BEGIN
               SELECT lookup_code
                 INTO r_custaccountrec.customer_class_code
                 FROM ar_lookups
                WHERE     lookup_type = 'CUSTOMER CLASS'
                      AND UPPER (meaning) =
                             REPLACE (UPPER (i.cust_class), CHR (13))
                      AND TRUNC (SYSDATE) BETWEEN TRUNC (START_DATE_ACTIVE)
                                              AND TRUNC (
                                                     NVL (END_DATE_ACTIVE,
                                                          SYSDATE))
                      AND enabled_flag = 'Y';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'In No Data Found of customer_class code');
                  r_custaccountrec.customer_class_code := NULL;

                  v_recerrorflag := 'Y';
                  v_errordetails :=
                     'In no data found of customer_class code' || SQLERRM;
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'In others of customer_class code' || SQLERRM);
                  r_custaccountrec.customer_class_code := NULL;
                  v_recerrorflag := 'Y';
                  v_errordetails :=
                     'In others of customer_class code' || SQLERRM;
            END;
         END IF;


            fnd_file.put_line (
               fnd_file.LOG,
                  'r_custaccountrec.customer_class_code...'
               || r_custaccountrec.customer_class_code);

     IF i.PRED_TRADE IS NOT NULL THEN					--Added IF clause for ver 1.1
            BEGIN
               SELECT ffvl.FLEX_VALUE
                 INTO r_custaccountrec.attribute9
                 FROM FND_FLEX_VALUE_SETS FFLV, FND_FLEX_VALUES_VL FFVL
                WHERE     FFLV.FLEX_VALUE_SET_NAME =
                             'XXCUS_PREDOMINANT_TRADE'
                      AND FFLV.FLEX_VALUE_SET_ID = FFVL.FLEX_VALUE_SET_ID
                      AND ffvl.enabled_flag = 'Y'
                      AND FFVL.DESCRIPTION =
                             REPLACE (UPPER (i.pred_trade), CHR (13));
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (fnd_file.LOG,
                                     'In No Data Found of Predomanent Trade');
                  r_custaccountrec.attribute9 := NULL;

                  v_recerrorflag := 'Y';
                  v_errordetails :=
                     'In no data found of Predomanent Trade' || SQLERRM;
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'In others of Predomanent Trade' || SQLERRM);
                  r_custaccountrec.attribute9 := NULL;
                  v_recerrorflag := 'Y';
                  v_errordetails :=
                     'In others of Predomanent Trade' || SQLERRM;
            END;
        END IF;

            fnd_file.put_line (
               fnd_file.LOG,
                  'r_custaccountrec.attribute9...'
               || r_custaccountrec.attribute9);
            fnd_file.put_line (
               fnd_file.LOG,
                  'r_custaccountrec.attribute9...'
               || r_custaccountrec.attribute9);
            fnd_file.put_line (fnd_file.LOG,
                               'v_recerrorflag...' || v_recerrorflag);
            fnd_file.put_line (
               fnd_file.LOG,
               'v_object_version_number...' || v_object_version_number);
            fnd_file.put_line (fnd_file.LOG,
                               'v_recerrorflag...' || v_recerrorflag);

            IF v_recerrorflag != 'Y'
            THEN
               r_custaccountrec.cust_account_id := v_cust_account_id;



               ------------ cust acc---------------------------------------------------------
               hz_cust_account_v2pub.update_cust_account (
                  p_init_msg_list           => fnd_api.g_true,
                  p_cust_account_rec        => r_custaccountrec,
                  p_object_version_number   => v_object_version_number,
                  x_return_status           => v_returnstatus,
                  x_msg_count               => v_msgcount,
                  x_msg_data                => v_msgdata);

               COMMIT;

               fnd_file.put_line (fnd_file.LOG,
                                  'v_returnstatus....' || v_returnstatus);
               fnd_file.put_line (fnd_file.LOG,
                                  'v_msgcount....' || v_msgcount);
               fnd_file.put_line (fnd_file.LOG, 'v_msgdata....' || v_msgdata);
               fnd_file.put_line (fnd_file.LOG,
                                  'v_party_id....' || v_partyid);
               fnd_file.put_line (
                  fnd_file.LOG,
                  'V_cust_account_id ....' || v_cust_account_id);
               fnd_file.put_line (fnd_file.LOG,
                                  'v_partynumber....' || v_partynumber);
               fnd_file.put_line (fnd_file.LOG,
                                  'v_profile_id....' || v_profile_id);

               IF v_returnstatus <> 'S'
               THEN
                  v_recerrorflag := 'Y';

                  FOR i IN 1 .. v_msgcount
                  LOOP
                     v_errordetails :=
                        SUBSTR (
                              v_errordetails
                           || 'creating party'
                           || SUBSTR (
                                 apps.fnd_msg_pub.get (
                                    p_msg_index   => i,
                                    p_encoded     => apps.fnd_api.g_false),
                                 1,
                                 255)
                           || CHR (10),
                           1,
                           10000);
                  END LOOP;
               END IF;
            ELSE
               fnd_file.put_line (
                  fnd_file.LOG,
                     'customer class code/Predomanent trade v_errordetails....'
                  || v_errordetails);
            END IF;
         ELSE
            fnd_file.put_line (
               fnd_file.LOG,
               'Either  Predominant Trade is null/Customer Classification is null/Both are null');
            v_recerrorflag := 'Y';
            v_errordetails :=
               'Either  Predominant Trade is null/Customer Classification is null/Both are null';
         END IF;


         fnd_file.put_line (
            fnd_file.output,
               i.account_number
            || '|'
            || i.account_name
            || '|'
            || i.PRED_TRADE
            || '|'
            || i.CUST_CLASS);


         IF v_recerrorflag = 'Y'
         THEN
            UPDATE XXWC_CACCT_ATTR_UPDATE_STG
               SET process_flag = 'E', ERROR_MESSAGE = v_errordetails
             WHERE ROWID = i.rid;
         ELSE
            UPDATE XXWC_CACCT_ATTR_UPDATE_STG
               SET process_flag = 'S', ERROR_MESSAGE = 'Success'
             WHERE ROWID = i.rid;
         END IF;

         COMMIT;

         v_msgdata := NULL;
         v_errordetails := NULL;
         v_recerrorflag := 'N';
         v_returnstatus := 'N';
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errordetails :=
               'Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();



         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_CUST_ACCT_UPDATE_PKG',
            p_calling             => 'UPDATE_CACCT_PTRADE_CUSTCLASS',
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                       REGEXP_REPLACE (v_errordetails,
                                                       '[[:cntrl:]]',
                                                       NULL),
                                       1,
                                       2000),
            p_error_desc          =>    'Error running '
                                     || 'XXWC_AR_CUST_ACCT_UPDATE_PKG'
                                     || '.'
                                     || 'UPDATE_CACCT_PTRADE_CUSTCLASS',
            p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
            p_module              => 'ONT');
   END UPDATE_CACCT_PTRADE_CUSTCLASS;
END XXWC_AR_CUST_ACCT_UPDATE_PKG;
/
