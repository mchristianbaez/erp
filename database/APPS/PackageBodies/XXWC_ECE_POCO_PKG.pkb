CREATE OR REPLACE PACKAGE BODY APPS.XXWC_ECE_POCO_PKG
IS /*************************************************************************
   $Header XXWC_ECE_POCO_PKG $
   Module Name: XXWC_ECE_POCO_PKG.pkb

   PURPOSE: EDI - Add ability to bypass EDI when transmitting a canceled PO, and EDI Overriden PO

   REVISIONS:
   Ver        Date        Author                Description
   ---------  ----------  ------------------    ----------------
   1.0        12-Mar-15   Manjula Chellappan    Initial Version TMS # 20141113-00118
 **************************************************************************/
   g_distribution_list   fnd_user.email_address%TYPE
                            := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE bypass_poco (p_po_header_id IN NUMBER, p_po_number IN NUMBER)
   /*************************************************************************
    $Header XXWC_ECE_POCO_PKG $
    Module Name: XXWC_ECE_POCO_PKG.bypass_poco

    PURPOSE: Procedure to maintain the list os POs to be be bypassed from EDI

    REVISIONS:
    Ver        Date        Author                Description
    ---------  ----------  ------------------    ----------------
    1.0        12-Mar-15   Manjula Chellappan    Initial Version TMS # 20141113-00118
  **************************************************************************/
   IS
      l_po_header_id   NUMBER := p_po_header_id;
      l_edi_flag       VARCHAR2 (1) := 'N';
      l_error_msg      VARCHAR2 (2000);
      l_sec            VARCHAR2 (100);
   BEGIN
      BEGIN
         l_sec := 'Check if POCO to be Bypassed';

         SELECT 'Y'
           INTO l_edi_flag
           FROM po_headers ph
          WHERE     po_header_id = l_po_header_id
                AND edi_processed_flag = 'Y'
                AND EXISTS
                       (SELECT 'x'
                          FROM ece_tp_details tpd
                         WHERE     tp_header_id =
                                      (SELECT tp_header_id
                                         FROM ap_supplier_sites
                                        WHERE vendor_site_id =
                                                 ph.vendor_site_id)
                               AND document_id = 'POCO'
                               AND edi_flag = 'Y');


         BEGIN
            l_sec := 'Bypass POCO';

            INSERT
              INTO XXWC_ECE_POCO_BYPASS_TBL (po_header_id,
                                             po_number,
                                             insert_date)
               VALUES (
                         p_po_header_id,
                         p_po_number,
                         SYSDATE);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                  SUBSTR (
                     (   'PO is not Bypassed for EDI for PO Header ID '
                      || l_po_header_id
                      || ' - '
                      || SQLERRM),
                     1,
                     2000);
         END;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
         WHEN OTHERS
         THEN
            l_error_msg :=
               SUBSTR (
                  (   'PO is not Bypassed for EDI for PO Header ID '
                   || l_po_header_id
                   || ' - '
                   || SQLERRM),
                  1,
                  2000);
      END;

      IF l_error_msg IS NOT NULL
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_ECE_POCO_PKG.BYPASS_POCO',
            p_calling             => l_sec,
            p_request_id          => NULL,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace ()
                                       || l_error_msg,
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_distribution_list,
            p_module              => 'EC');
      END IF;
   END bypass_poco;

   PROCEDURE extract_poco_outbound (
      errbuf                  OUT VARCHAR2,
      retcode                 OUT VARCHAR2,
      p_output_path        IN     VARCHAR2,
      p_po_number_from     IN     VARCHAR2,
      p_po_number_to       IN     VARCHAR2,
      p_crdate_from        IN     VARCHAR2,
      p_crdate_to          IN     VARCHAR2,
      p_pc_type            IN     VARCHAR2,
      p_vendor_name        IN     VARCHAR2,
      p_vendor_site_code   IN     VARCHAR2,
      p_debug_mode         IN     NUMBER DEFAULT 0)
   /*************************************************************************
    $Header XXWC_ECE_POCO_PKG $
    Module Name: XXWC_ECE_POCO_PKG.extract_poco_outbound

    PURPOSE: Wrapper to the standard EDI Purchase Order outbound Program

    REVISIONS:
    Ver        Date        Author                Description
    ---------  ----------  ------------------    ----------------
    1.0        12-Mar-15   Manjula Chellappan    Initial Version TMS # 20141113-00118
  **************************************************************************/
   IS
      l_line                    VARCHAR2 (50) := '*-----------------------------------*';
      l_po_header_id            NUMBER;
      l_cancel_po_header_id     NUMBER;
      l_override_po_header_id   NUMBER;
      l_po_number               VARCHAR2 (30);
      l_sec                     VARCHAR2 (400);
      l_parent_request_id       NUMBER;
      l_program_name            VARCHAR2 (100) := 'ECEPOCO';
      l_request_id              NUMBER;
      l_file_name               VARCHAR2 (100);
      l_edi_processed_flag      VARCHAR2 (1);
      l_print_count             NUMBER;


      CURSOR cur_edi_poco
      IS
           SELECT po_number, po_header_id
             FROM ECE_POCO_HEADERS_V eph
            WHERE     1 = 1
                  AND po_number BETWEEN NVL (p_po_number_from, po_number)
                                    AND NVL (p_po_number_to, po_number)
                  AND revised_date BETWEEN NVL (
                                              TO_DATE (p_crdate_from,
                                                       'YYYY/MM/DD HH24:MI:SS'),
                                              revised_date)
                                       AND NVL (
                                              TO_DATE (p_crdate_to,
                                                       'YYYY/MM/DD HH24:MI:SS'),
                                              revised_date)
                  AND po_type = NVL (p_pc_type, po_type)
                  AND supplier_name = NVL (p_vendor_name, supplier_name)
                  AND supplier_site_code =
                         NVL (p_vendor_site_code, supplier_site_code)
         ORDER BY 1;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, l_line);
      fnd_file.put_line (fnd_file.LOG, l_line);

      fnd_file.put_line (fnd_file.LOG, 'Parameters');

      fnd_file.put_line (fnd_file.LOG, 'p_output_path : ' || p_output_path);
      fnd_file.put_line (fnd_file.LOG,
                         'p_po_number_from : ' || p_po_number_from);
      fnd_file.put_line (fnd_file.LOG, 'p_po_number_to : ' || p_po_number_to);
      fnd_file.put_line (fnd_file.LOG, 'p_crdate_from : ' || p_crdate_from);
      fnd_file.put_line (fnd_file.LOG, 'p_crdate_to : ' || p_crdate_to);
      fnd_file.put_line (fnd_file.LOG, 'p_pc_type : ' || p_pc_type);
      fnd_file.put_line (fnd_file.LOG, 'p_vendor_name : ' || p_vendor_name);
      fnd_file.put_line (fnd_file.LOG,
                         'p_vendor_site_code : ' || p_vendor_site_code);

      fnd_file.put_line (fnd_file.LOG, l_line);
      fnd_file.put_line (fnd_file.LOG, l_line);

      l_sec := 'Set the Operating unit for Multi Org';

      fnd_file.put_line (
         fnd_file.LOG,
         l_sec || ':' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      fnd_request.set_org_id (fnd_global.org_id);

      fnd_file.put_line (fnd_file.LOG, 'Current Org Id' || fnd_global.org_id);



      l_parent_request_id := fnd_global.conc_request_id;

      FOR rec_edi_poco IN cur_edi_poco
      LOOP
         l_sec :=
               'Process PO '
            || cur_edi_poco%ROWCOUNT
            || ' : '
            || rec_edi_poco.po_number;

         fnd_file.put_line (fnd_file.LOG, l_line);

         fnd_file.put_line (
            fnd_file.LOG,
            l_sec || ':' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));


         fnd_file.put_line (fnd_file.LOG, l_line);

         l_sec := 'Validate POCO ';

         fnd_file.put_line (
            fnd_file.LOG,
            l_sec || ':' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));


         l_po_header_id := rec_edi_poco.po_header_id;

         -- Bypass EDI for cancelled / Overriden POs


         BEGIN
            l_cancel_po_header_id := NULL;
            l_override_po_header_id := NULL;

            BEGIN
               SELECT po_header_id
                 INTO l_cancel_po_header_id
                 FROM XXWC_ECE_POCO_BYPASS_TBL
                WHERE po_header_id = l_po_header_id AND ROWNUM = 1;

               fnd_file.put_line (fnd_file.LOG,
                                  'PO to be Bypassed ' || SQLERRM);
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (fnd_file.LOG,
                                     'PO not to be Bypassed ' || SQLERRM);
            END;

            BEGIN
               SELECT po_header_id
                 INTO l_override_po_header_id
                 FROM XXWC_PO_COMM_HIST
                WHERE     po_header_id = l_po_header_id
                      AND creation_date =
                             (SELECT MAX (creation_date)
                                FROM XXWC.XXWC_PO_COMM_HIST
                               WHERE po_header_id = l_po_header_id)
                      AND XML_OR_EDI <> 'EDI'
                      AND ROWNUM = 1;

               fnd_file.put_line (fnd_file.LOG,
                                  'PO EDI Overriden ' || SQLERRM);
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (fnd_file.LOG,
                                     'PO Not EDI Overriden ' || SQLERRM);
            END;
         END;

         IF    l_cancel_po_header_id IS NOT NULL
            OR l_override_po_header_id IS NOT NULL
         THEN
            fnd_file.put_line (fnd_file.LOG, 'POCO not submitted');

            BEGIN
                   SELECT edi_processed_flag, print_count
                     INTO l_edi_processed_flag, l_print_count
                     FROM PO_HEADERS_ARCHIVE
                    WHERE     latest_external_flag = 'Y'
                          AND NVL (edi_processed_flag, 'N') = 'N'
                          AND po_header_id = l_po_header_id
               FOR UPDATE NOWAIT;

               UPDATE PO_HEADERS_ARCHIVE
                  SET edi_processed_flag = 'Y', print_count = print_count + 1
                WHERE     latest_external_flag = 'Y'
                      AND NVL (edi_processed_flag, 'N') = 'N'
                      AND po_header_id = l_po_header_id;

               fnd_file.put_line (fnd_file.LOG, 'PO_HEADERS_ARCHIVE Updated');

               BEGIN
                  DELETE FROM XXWC_ECE_POCO_BYPASS_TBL
                        WHERE po_header_id = l_po_header_id;

                  fnd_file.put_line (
                     fnd_file.LOG,
                     'Deleted From XXWC_ECE_POCO_BYPASS_TBL ');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG,
                           'Record not deleted from XXWC_ECE_POCO_BYPASS_TBL '
                        || SQLERRM);
               END;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'Failed updating PO_HEADERS_ARCHIVE ' || SQLERRM);
            END;
         ELSE
            l_sec := 'Generate File Name';

            BEGIN
               SELECT    'POCO'
                      || LPAD (
                            SUBSTR (
                               TO_CHAR (ECE_OUTPUT_RUNS_S.NEXTVAL),
                               DECODE (LENGTH (ECE_OUTPUT_RUNS_S.NEXTVAL),
                                       1, -1,
                                       2, -2,
                                       3, -3,
                                       -4),
                               4),
                            4,
                            '0')
                      || '.tmp'
                 INTO l_file_name
                 FROM DUAL;

               l_sec := 'EDI File Name : ' || l_file_name;

               fnd_file.put_line (
                  fnd_file.LOG,
                  l_sec || ':' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_file_name := NULL;
            END;


            l_request_id :=
               fnd_request.submit_request ('EC',
                                           l_program_name,
                                           NULL,
                                           NULL,
                                           FALSE,
                                           p_output_path,
                                           l_file_name,
                                           rec_edi_poco.po_number,
                                           rec_edi_poco.po_number,
                                           p_crdate_from,
                                           p_crdate_to,
                                           p_pc_type,
                                           p_vendor_name,
                                           p_vendor_site_code,
                                           p_debug_mode);


            l_sec := 'POCO Submitted, Request Id : ' || l_request_id;

            fnd_file.put_line (
               fnd_file.LOG,
               l_sec || ':' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         END IF;

         COMMIT;
      END LOOP;

      fnd_file.put_line (fnd_file.LOG, l_line);

      l_sec := 'Delete Invalid Records from XXWC_ECE_POCO_BYPASS_TBL';


      BEGIN
         DELETE FROM XXWC_ECE_POCO_BYPASS_TBL a
               WHERE NOT EXISTS
                        (SELECT 'X'
                           FROM ECE_POCO_HEADERS_V
                          WHERE po_header_id = a.po_header_id);

         fnd_file.put_line (
            fnd_file.LOG,
            'Deleted Invalid Records From XXWC_ECE_POCO_BYPASS_TBL ');

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Error Deleting Invalid Records From XXWC_ECE_POCO_BYPASS_TBL '
               || SQLERRM);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, SQLERRM);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_ECE_POCO_PKG.EXTRACT_POCO_OUTBOUND',
            p_calling             => l_sec,
            p_request_id          => l_parent_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_distribution_list,
            p_module              => 'EC');
   END extract_poco_outbound;
END xxwc_ece_poco_pkg;