-- Start of DDL Script for Java Source APPS.XXWC_FTP_UTIL
-- Generated 6/20/2012 5:34:24 PM from APPS@EBIZCON

CREATE OR REPLACE AND RESOLVE JAVA SOURCE NAMED "XXWC_FTP_UTIL"
AS import ftp.*;
import java.sql.*;
import java.io.*;
import java.util.*;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import java.io.IOException;
public class XXWC_Ftp{
private static FtpBean ftp = null;
  public static void ftpConnect(String server, String user, String password )
        throws  IOException,FtpException
   {
      ftp = new FtpBean();
      ftp.ftpConnect(server,user,password);
   }
  public static  void setDirectory(String directory) throws  IOException,FtpException
   {
    ftp.setDirectory(directory);
   }
   /*public static FtpListResult getDirectoryContent() throws  IOException,FtpException
      {
        FtpListResult ftplist = new FtpListResult();
        ftplist = ftp.getDirectoryContent();
       return ftplist;
   }*/
   //
    public static int getDirectoryContent (ARRAY r1[]) throws SQLException,IOException,FtpException
     {
       // This first example returns a simple array of VARCHAR2s
       Connection conn = new OracleDriver().defaultConnection();
       FtpListResult ftplist = new FtpListResult();
       Statement    findType =  conn.createStatement();
       String TYPE_NAME ="NOT FOUND";
       String query = "select OWNER||'.'||OBJECT_NAME type_name from all_objects where upper(object_name) like 'XXWC_FILE_LIST' AND OBJECT_TYPE ='TYPE'";
       ResultSet rs = findType.executeQuery(query);
       while (rs.next()) {
            
            TYPE_NAME =rs.getString("type_name");
                 }
       int noFile = 0;
        ftplist = ftp.getDirectoryContent();
        Vector fileList = new Vector();
        while(ftplist.next())
                   {
                       noFile = 1;
                       fileList.addElement(  ftplist.getName());
                   }
       // Create the data to be returned:
       if (! fileList.isEmpty())
       {
       Iterator iter = fileList.iterator();
       String [] fileListArray = new String[fileList.size()];
       for (int i = 0; i <fileList.size();i++)
          {
          fileListArray[i] =(String)iter.next();
          }
       //  Now declare a descriptor to associate the host array type with the
       //  array type in the database
       ArrayDescriptor desc1=ArrayDescriptor.createDescriptor(TYPE_NAME,conn);
       // Now create the ARRAY object to be returned
       r1[0]= new ARRAY(desc1,conn,fileListArray);
       }
        return noFile;
     }
   //
   public static void getAsciiFile (String ftpfile, String localfile, String separator)
       throws  IOException,FtpException
       {
        ftp.getAsciiFile( ftpfile,  localfile,separator);
       }
   public static void putAsciiFile(String filename, String content, String separator)
        throws IOException, FtpException
        {
        ftp.putAsciiFile(filename,content,separator);
        }
   public static void fileDelete(String filename)
        throws IOException, FtpException
        {
        ftp.fileDelete(filename);
        }
   public static void fileRename(String oldfilename, String newfilename)
   throws IOException, FtpException
        {
        ftp.fileRename(oldfilename,newfilename);
        }
        
   public static void getBinaryFile (String ftpfile, String localfile)
       throws  IOException,FtpException
       {
        ftp.getBinaryFile( ftpfile, localfile);
       }
   public static void putBinaryFile (String local_file, String remote_file)
   throws IOException, FtpException
       {
       ftp.putBinaryFile(local_file, remote_file);
       }
   public static void makeDirectory(String directory)
        throws IOException, FtpException
        {
        ftp.makeDirectory(directory);
        }
   public static  void close()
     throws  IOException,FtpException
   {
    ftp.close();
   }
}


-- End of DDL Script for Java Source APPS.XXWC_FTP_UTIL

