SET VERIFY OFF;
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
WHENEVER OSERROR EXIT FAILURE ROLLBACK;
create or replace
PACKAGE BODY             dls_connector_pvt IS
  -- +================================================================================+
  -- |Description : DLS_CONNECTOR_PVT encapsulates functionality used to integrate    |
  -- |              the Datalens server with Oracle PIM.                              |
  -- |                                                                                |
  -- |Change Record:                                                                  |
  -- |===============                                                                 |
  -- |Version   Date         Author          Remarks                                  |
  -- |========  ===========  =============== =========================================|
  -- |1.0       10/9/2009    D. Savolainen   Initial
  -- |1.1       12/03/2014   Anand Lonkar    TMS# 20141110-00037
  -- |                                       PROCEDURE insertitemudas
  -- |					     attr_value_str commented and 
  -- |                                       replaced with NULL for fixing 
  -- |                                       table based attributes.
  -- +================================================================================+
  -- Global Structures.
  g_semantic_model    VARCHAR2(2000);
  g_organization_id   INTEGER;
  g_import_src_system VARCHAR2(200);
  g_ext_src_system    VARCHAR2(200);
  g_dls_user          VARCHAR2(20);
  g_resp_key          VARCHAR2(40);
  g_language          VARCHAR2(80);
  g_user_id           NUMBER;
  g_resp_id           NUMBER;
  g_application_id    NUMBER;
  g_lastitemclassid   INTEGER;
  g_lastitemclasstype VARCHAR2(1);
  g_lastsemanticattrs dls_semantic_attributes_tbl;
  g_ego_attr_tbl      ego_attr_group_request_table;
  g_lastprodcatalog   INTEGER;
  g_sample_size       INTEGER;
  g_batch_size        INTEGER;
  g_pkgname           VARCHAR2(18);
  g_dlsjobid          INTEGER;
  g_highvaldate CONSTANT DATE := to_date('30000101', 'yyyymmdd');
  g_datefmtstr        VARCHAR2(50);
  g_datetimefmtstr    VARCHAR2(50);
  g_attrextractmethod VARCHAR2(20);

  TYPE t_attrvaluerec IS RECORD(
    attr_value VARCHAR2(4000),
    attr_uom   VARCHAR2(3));
  TYPE t_attrvaluetype IS TABLE OF t_attrvaluerec INDEX BY BINARY_INTEGER;
  TYPE t_itemattrs IS TABLE OF VARCHAR2(4000) INDEX BY VARCHAR2(40);
  -- +================================================================================+
  -- |PROCEDURE: LogMessage
  -- +================================================================================+
  -- |Description : Write a message to the PIM output log using FND_LOG
  -- |
  -- +================================================================================+
  PROCEDURE logmessage(p_text        IN VARCHAR2,
                       p_log_level   IN NUMBER DEFAULT fnd_log.level_statement,
                       p_module_name IN VARCHAR2 DEFAULT NULL) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    lc_module_name VARCHAR2(250);
  BEGIN
    lc_module_name := 'dls.' || p_module_name;
    fnd_log.string(p_log_level, lc_module_name, p_text);
    IF p_log_level = fnd_log.level_unexpected OR
       p_log_level = fnd_log.level_procedure THEN
      INSERT INTO dls_job_messages
        (job_id, module_name, message)
      VALUES
        (g_dlsjobid, p_module_name, p_text);
      COMMIT;
    END IF;
  END logmessage;
  -- +================================================================================+
  -- |PROCEDURE: GetItemClassHierarchy
  -- +================================================================================+
  -- |Description : This procedure loads the passed x_ItemClassCategories object
  -- |              with the ICC hierarchy for the passed in ICC ID.
  -- +================================================================================+
  PROCEDURE getitemclasshierarchy(p_itemclassid         IN INTEGER,
                                  x_itemclasscategories IN OUT NOCOPY dls_item_catalog_group_tbl) IS
    CURSOR c_itemclasslist IS
      SELECT parent_catalog_group_id,
             parent_catalog_group,
             catalog_group_id,
             catalog_group
        FROM ego_catalog_groups_v
       WHERE nvl(end_date, g_highvaldate) > SYSDATE
      CONNECT BY PRIOR catalog_group_id = parent_catalog_group_id
       START WITH catalog_group_id = p_itemclassid
       ORDER SIBLINGS BY catalog_group;
    v_itemclass dls_item_catalog_group_obj := dls_item_catalog_group_obj(NULL,
                                                                         NULL,
                                                                         NULL,
                                                                         NULL);
  BEGIN
    FOR rec IN c_itemclasslist
    LOOP
      v_itemclass.parent_catalog_group_id := rec.parent_catalog_group_id;
      v_itemclass.parent_catalog_group    := rec.parent_catalog_group;
      v_itemclass.catalog_group_id        := rec.catalog_group_id;
      v_itemclass.catalog_group           := rec.catalog_group;
      x_itemclasscategories.extend(1);
      x_itemclasscategories(x_itemclasscategories.last) := v_itemclass;
    END LOOP;
    IF x_itemclasscategories.count = 0 THEN
      logmessage(p_text        => 'ICC ID-> ' || p_itemclassid ||
                                  ' not found',
                 p_log_level   => fnd_log.level_unexpected,
                 p_module_name => g_pkgname || '.getItemClassHierarchy');
    END IF;
  END getitemclasshierarchy;
  -- +================================================================================+
  -- |PROCEDURE: GetItemClassHierarchy
  -- +================================================================================+
  -- |Description : This procedure loads the passed x_ItemClassCategories object
  -- |              with the ICC hierarchy for the passed in ICC Name.
  -- +================================================================================+
  PROCEDURE getitemclasshierarchy(p_itemclass           IN VARCHAR2,
                                  x_itemclasscategories IN OUT NOCOPY dls_item_catalog_group_tbl) IS
    v_itemclassid INTEGER;
    CURSOR c_itemclassid IS
      SELECT catalog_group_id
        FROM ego_catalog_groups_v
       WHERE upper(catalog_group) = upper(p_itemclass)
         AND nvl(end_date, g_highvaldate) > SYSDATE;
  BEGIN
    OPEN c_itemclassid;
    FETCH c_itemclassid
      INTO v_itemclassid;
    CLOSE c_itemclassid;
    IF v_itemclassid IS NULL THEN
      logmessage(p_text        => 'Item Class -> ' || p_itemclass ||
                                  ' not found.',
                 p_log_level   => fnd_log.level_unexpected,
                 p_module_name => g_pkgname || '.getItemClassHierarchy');
    ELSE
      getitemclasshierarchy(p_itemclassid         => v_itemclassid,
                            x_itemclasscategories => x_itemclasscategories);
    END IF;
  END getitemclasshierarchy;
  -- +================================================================================+
  -- |PROCEDURE: GetItemClassSemanticAttrs
  -- +================================================================================+
  -- |Description : This procedure returns the attributes that have been associated
  -- |              with the display format identified by the
  -- |              DLS_CATALOG_UDA_DISPLAY_TYPE profile option.
  -- +================================================================================+
  PROCEDURE getitemclassattrs(p_itemclassid   IN INTEGER,
                              p_attrtype      IN INTEGER,
                              x_semanticattrs IN OUT NOCOPY dls_semantic_attributes_tbl) IS
    TYPE t_attrcursor IS REF CURSOR;
    c_attrlist t_attrcursor;
  
    v_tablemethod CONSTANT VARCHAR2(4000) := 'SELECT attr_group_id, attr_group_name, attrs.attr_group_type, ' ||
                                             'attr_id, attr_name, value_set_id, value_set_name, data_type_code, ' ||
                                             'database_column, order_of_importance FROM (SELECT parent_catalog_group_id, ' ||
                                             'catalog_group_id, catalog_group, LEVEL tree_level FROM ego_catalog_groups_v ' ||
                                             'WHERE nvl(end_date, :1) > SYSDATE ' ||
                                             'CONNECT BY catalog_group_id = PRIOR parent_catalog_group_id ' ||
                                             'START WITH catalog_group_id = :2 ORDER SIBLINGS BY catalog_group) q1 ' ||
                                             'JOIN dls_extract_attributes extattr USING (catalog_group_id) ' ||
                                             'JOIN ego_attr_groups_v attrgrp USING (attr_group_id) JOIN ego_attrs_v attrs ' ||
                                             'USING (attr_group_name, attr_id) WHERE bitand(extract_type, :3) > 0';
  
    v_displaymethod CONSTANT VARCHAR2(4000) := 'SELECT attr_group_id, attr_group_name, attr_group_type, attr_id, attr_name, value_set_id, ' ||
                                               'value_set_name, data_type_code, database_column, display_sequence ' ||
                                               'FROM (SELECT regexp_substr(attribute_code, ''\d+'', 1, 1) attr_group_id, ' ||
                                               'to_number(regexp_substr(attribute_code, ''\d+'', 1, 2)) attr_id, display_sequence ' ||
                                               'FROM ego_results_format_columns_v t1 JOIN ego_results_format_v ' ||
                                               'USING (customization_application_id, customization_code, region_application_id, region_code) ' ||
                                               'WHERE NAME = :1 AND classification1 = to_char(:2) ' ||
                                               'AND region_code = ''EGO_ITEM_RESULT_DUMMY_REGION'' AND customization_application_id = 431 ' ||
                                               'AND customization_level_id = 60 AND region_application_id = 431) JOIN ego_attrs_v ' ||
                                               'USING (attr_id)';
  
    v_attrs dls_semantic_attributes_obj := dls_semantic_attributes_obj(NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL);
  BEGIN
    IF p_attrtype IN (dls_connector_pub.g_semanticattr,
                      dls_connector_pub.g_operationattr,
                      dls_connector_pub.g_sem_and_oper) THEN
      IF nvl(g_lastitemclassid, 0) != p_itemclassid OR
         nvl(g_lastitemclasstype, 0) != p_attrtype THEN
        IF g_lastsemanticattrs IS NULL THEN
          g_lastsemanticattrs := dls_semantic_attributes_tbl();
        ELSIF g_lastsemanticattrs.count != 0 THEN
          g_lastsemanticattrs.delete;
        END IF;
        IF g_attrextractmethod = 'Display Format' AND
           p_attrtype = dls_connector_pub.g_semanticattr THEN
          OPEN c_attrlist FOR v_displaymethod using g_semantic_model, p_itemclassid;
        ELSIF g_attrextractmethod = 'DLS Table' THEN
          OPEN c_attrlist FOR v_tablemethod using g_highvaldate, p_itemclassid, p_attrtype;
        ELSIF g_attrextractmethod = 'Display Format' AND
              p_attrtype != dls_connector_pub.g_semanticattr THEN
          logmessage(p_text        => 'Operational parameters not supported with Display Formats.  Switch to ' ||
                                      ' DLS Table extract method',
                     p_log_level   => fnd_log.level_unexpected,
                     p_module_name => g_pkgname || '.getItemClassAttrs');
          RETURN;
        END IF;
        LOOP
          FETCH c_attrlist
            INTO v_attrs.attr_group_id,
                 v_attrs.attr_group_name,
                 v_attrs.attr_group_type,
                 v_attrs.attr_id,
                 v_attrs.attr_name,
                 v_attrs.value_set_id,
                 v_attrs.value_set_name,
                 v_attrs.data_type_code,
                 v_attrs.database_column,
                 v_attrs.display_sequence;
          exit when c_attrlist%NOTFOUND;
          g_lastsemanticattrs.extend(1);
          g_lastsemanticattrs(g_lastsemanticattrs.last) := v_attrs;
        END LOOP;
        CLOSE c_attrlist;
        g_lastitemclassid   := p_itemclassid;
        g_lastitemclasstype := p_attrtype;
      END IF;
      x_semanticattrs := g_lastsemanticattrs;
      IF x_semanticattrs.count = 0 THEN
        logmessage(p_text        => 'Semantic attributes for ICC ID -> ' ||
                                    p_itemclassid || ' not found',
                   p_log_level   => fnd_log.level_unexpected,
                   p_module_name => g_pkgname || '.getItemClassAttrs');
      END IF;
    ELSE
      logmessage(p_text        => 'Invalid parameter value for p_attrtype => ' ||
                                  p_attrtype,
                 p_log_level   => fnd_log.level_unexpected,
                 p_module_name => g_pkgname || '.getItemClassAttrs');
    END IF;
  END getitemclassattrs;

  -- +================================================================================+
  -- |PROCEDURE: CreateWorkbenchBatch
  -- +================================================================================+
  -- |Description : This function creates and returns a Work Bench batch ID.  The type
  -- |              type of batch is determined by the parameter p_batch_type which
  -- |              can be either ITEM or STRUCTURE.
  -- +================================================================================+
  FUNCTION createworkbenchbatch(p_batch_name IN VARCHAR2,
                                p_batch_type IN VARCHAR2 DEFAULT dls_connector_pub.g_item_batch_type,
                                p_src_system IN VARCHAR2 DEFAULT g_import_src_system)
    RETURN INTEGER IS
    v_batch_id      INTEGER;
    v_return_status VARCHAR2(1);
    v_error_msg     VARCHAR2(2000);

  BEGIN
  
    ego_import_batches_pkg.create_import_batch(p_source_system_code      => p_src_system,
                                               p_organization_id         => g_organization_id,
                                               p_batch_type_display_name => p_batch_type,
                                               p_assignee_name           => g_dls_user,
                                               p_batch_name              => p_batch_name,
                                               x_batch_id                => v_batch_id,
                                               x_return_status           => v_return_status,
                                               x_error_msg               => v_error_msg);
    IF v_return_status <> fnd_api.g_ret_sts_success THEN
      logmessage(p_text        => v_error_msg,
                 p_log_level   => fnd_log.level_procedure,
                 p_module_name => g_pkgname || '.CreateWorkbenchBatch');
      RETURN NULL;
    END IF;
    INSERT INTO DLS_JOB_BATCHES (JOB_ID, BATCH_ID, BATCH_COUNT, SOURCE_SYSTEM, CURRENT_BATCH, CREATION_DATE)
          VALUES (g_dlsjobid, v_batch_id, 0, p_src_system, 'Y', sysdate);
    RETURN v_batch_id;
  END;
  -- +================================================================================+
  -- |PROCEDURE: GetProdAttributes
  -- +================================================================================+
  -- |Description : This procedure will get the production usr attributes using the
  -- |              ego_user_attrs_data_pub.Get_User_Attrs_Data procedure.
  -- |
  -- |
  -- |
  -- +================================================================================+
  PROCEDURE getprodattributes(p_catalogid    IN INTEGER,
                              p_inventoryid  IN INTEGER,
                              p_orgid        IN VARCHAR2,
                              p_attrtype     IN INTEGER,
                              x_attr_row_tb  IN OUT NOCOPY ego_user_attr_row_table,
                              x_attr_data_tb IN OUT NOCOPY ego_user_attr_data_table) IS
    v_pk_value_pairs   ego_col_name_value_pair_array := ego_col_name_value_pair_array();
    v_semanticattrs    dls_semantic_attributes_tbl := dls_semantic_attributes_tbl();
    v_usr_object_privs ego_varchar_tbl_type := NULL;
    v_ego_attr_tbl     ego_attr_group_request_table := ego_attr_group_request_table();
    x_return_status    VARCHAR2(10);
    x_errorcode        NUMBER;
    x_msg_count        NUMBER;
    x_msg_data         VARCHAR2(255);
    CURSOR c_groupattrs IS
      SELECT attr_group_id,
             attr_group_type,
             attr_group_name,
             data_level_internal_name,
             ltrim(sys_connect_by_path(attr_name, ','), ',') || ',' attr_names
        FROM (SELECT t1.attr_group_id,
                     t1.attr_group_type,
                     t1.attr_group_name,
                     attr_name,
                     data_level_internal_name,
                     row_number() over(PARTITION BY t1.attr_group_id ORDER BY attr_name) rn,
                     COUNT(*) over(PARTITION BY t1.attr_group_id) cnt
                FROM TABLE(v_semanticattrs) t1
                JOIN ego_attr_groups_dl_v t2
                  ON (to_number(t1.attr_group_id) = t2.attr_group_id)
               WHERE t1.attr_group_id != 'Primary'
               ORDER BY attr_name)
       WHERE rn = cnt
       START WITH rn = 1
      CONNECT BY PRIOR attr_group_id = attr_group_id
             AND PRIOR rn = rn - 1
       ORDER BY attr_group_id;
  BEGIN
    v_pk_value_pairs.extend(2);
    v_pk_value_pairs(1) := ego_col_name_value_pair_obj('INVENTORY_ITEM_ID',
                                                       to_char(p_inventoryid));
    v_pk_value_pairs(2) := ego_col_name_value_pair_obj('ORGANIZATION_ID',
                                                       to_char(p_orgid));
    IF nvl(g_lastprodcatalog, 0) != p_catalogid THEN
      IF g_ego_attr_tbl IS NULL THEN
        g_ego_attr_tbl := ego_attr_group_request_table();
      END IF;
      getitemclassattrs(p_itemclassid   => p_catalogid,
                        p_attrtype      => p_attrtype,
                        x_semanticattrs => v_semanticattrs);
      FOR rec IN c_groupattrs
      LOOP
        v_ego_attr_tbl.extend();
        v_ego_attr_tbl(v_ego_attr_tbl.last) := ego_attr_group_request_obj(attr_group_id   => rec.attr_group_id,
                                                                          application_id  => 431,
                                                                          attr_group_type => rec.attr_group_type,
                                                                          attr_group_name => rec.attr_group_name,
                                                                          data_level      => rec.data_level_internal_name,
                                                                          data_level_1    => NULL,
                                                                          data_level_2    => NULL,
                                                                          data_level_3    => NULL,
                                                                          data_level_4    => NULL,
                                                                          data_level_5    => NULL,
                                                                          attr_name_list  => rec.attr_names);
      END LOOP;
      g_lastprodcatalog := p_catalogid;
      g_ego_attr_tbl    := v_ego_attr_tbl;
    END IF;
    IF g_ego_attr_tbl.count > 0 THEN
      ego_user_attrs_data_pub.get_user_attrs_data(p_api_version                => 1.0,
                                                  p_object_name                => 'EGO_ITEM',
                                                  p_pk_column_name_value_pairs => v_pk_value_pairs,
                                                  p_attr_group_request_table   => g_ego_attr_tbl,
                                                  p_user_privileges_on_object  => v_usr_object_privs,
                                                  p_debug_level                => 3,
                                                  p_init_error_handler         => fnd_api.g_true,
                                                  p_commit                     => fnd_api.g_false,
                                                  x_attributes_row_table       => x_attr_row_tb,
                                                  x_attributes_data_table      => x_attr_data_tb,
                                                  x_return_status              => x_return_status,
                                                  x_errorcode                  => x_errorcode,
                                                  x_msg_count                  => x_msg_count,
                                                  x_msg_data                   => x_msg_data);
      IF x_return_status <> fnd_api.g_ret_sts_success THEN
        logmessage(p_text        => x_msg_data,
                   p_log_level   => fnd_log.level_procedure,
                   p_module_name => g_pkgname || '.GetProdAttributes');
      END IF;
    END IF;
  END getprodattributes;
  -- +================================================================================+
  -- |FUNCTION: SemanticModelDefined
  -- +================================================================================+
  -- |Description : This function will return true if a semantic model is defined
  -- |              for the passed in ICC.
  -- +================================================================================+
  FUNCTION semanticmodeldefined(p_catalogid IN INTEGER) RETURN BOOLEAN IS
    CURSOR c_modeldefined IS
      SELECT customization_application_id
        FROM ego_results_format_v
       WHERE NAME = g_semantic_model
         AND classification1 = to_char(p_catalogid);
    v_return_cd BOOLEAN := FALSE;
    v_custom_id INTEGER;
  BEGIN
    OPEN c_modeldefined;
    FETCH c_modeldefined
      INTO v_custom_id;
    IF c_modeldefined%NOTFOUND THEN
      v_return_cd := FALSE;
    ELSE
      v_return_cd := TRUE;
    END IF;
    CLOSE c_modeldefined;
    RETURN v_return_cd;
  END semanticmodeldefined;
  -- +================================================================================+
  -- |FUNCTION: getdefaultattruom
  -- +================================================================================+
  -- |Description : This function will return the default uom for the passed in
  -- |              attribute.
  -- +================================================================================+
  FUNCTION getdefaultattruom(p_attr_group IN VARCHAR2,
                             p_attr_name  IN VARCHAR2) RETURN VARCHAR2 IS
    CURSOR c_def_uom IS
      SELECT
      /*+ CHOOSE */
       muom.uom_code
        FROM ego_attrs_v attr, mtl_units_of_measure muom
       WHERE attr.attr_group_type = 'EGO_ITEMMGMT_GROUP'
         AND attr.attr_group_name = p_attr_group
         AND attr.attr_name = p_attr_name
         AND muom.uom_class = attr.uom_class
         AND muom.base_uom_flag = 'Y';
    v_uom_code mtl_units_of_measure.uom_code%TYPE;
  BEGIN
    OPEN c_def_uom;
    FETCH c_def_uom
      INTO v_uom_code;
    CLOSE c_def_uom;
    RETURN v_uom_code;
  END getdefaultattruom;
  -- +================================================================================+
  -- |PROCEDURE: GetPrimaryValues
  -- +================================================================================+
  -- |Description : This procedure returns a distinct list of values for the passed in
  -- |              Supported Primary attribute for the passed in ICC.  Supported
  -- |              primary attributes are listed in the dls_supported_primary_attrs
  -- |              table.
  -- |
  -- |
  -- +================================================================================+
  PROCEDURE getprimaryvalues(p_attr_name        IN VARCHAR,
                             p_catalog_group_id IN INTEGER,
                             x_attrvalues       IN OUT NOCOPY t_attrvaluetype) IS
    CURSOR c_supportedprimaryattr IS
      SELECT table_name
        FROM dls_supported_primary_attrs
       WHERE attr_name = p_attr_name;
    v_tablename VARCHAR2(30);
    v_sql       VARCHAR2(500);
  BEGIN
    OPEN c_supportedprimaryattr;
    FETCH c_supportedprimaryattr
      INTO v_tablename;
    IF c_supportedprimaryattr%FOUND THEN
      CLOSE c_supportedprimaryattr;
      v_sql := 'Select distinct ' || p_attr_name || ', NULL uom from ' ||
               v_tablename || ' where item_catalog_group_id = :1 and ' ||
               p_attr_name || ' is not null';
      EXECUTE IMMEDIATE v_sql BULK COLLECT
        INTO x_attrvalues
        USING p_catalog_group_id;
    ELSE
      CLOSE c_supportedprimaryattr;
    END IF;
  END getprimaryvalues;
  -- +================================================================================+
  -- |PROCEDURE: GetCharValues
  -- +================================================================================+
  -- |Description : This procedure returns a distinct list of values for the passed in
  -- |              Character Data Attribute for the passed in ICC.
  -- |
  -- |
  -- |
  -- |
  -- +================================================================================+
  PROCEDURE getcharvalues(p_column_name      IN VARCHAR2,
                          p_attr_group_id    IN INTEGER,
                          p_catalog_group_id IN INTEGER,
                          x_attrvalues       IN OUT NOCOPY t_attrvaluetype) IS
    v_sql VARCHAR2(500);
  BEGIN
    v_sql := 'select distinct ' || p_column_name || ', NULL uom ' ||
             ' from EGO_MTL_SY_ITEMS_EXT_VL ' ||
             '  join mtl_system_items_vl using (inventory_item_id, item_catalog_group_id) ' ||
             ' where attr_group_id = :1' ||
             '   and item_catalog_group_id = :2 ' || '  and ' ||
             p_column_name || ' is not null ' ||
             '   and NVL(ENABLED_FLAG, ''Y'') = ''Y'' ' ||
             '   and NVL(start_date_active, SYSDATE) <= SYSDATE ' ||
             '   and NVL(end_date_active, to_date(''30000101'', ''YYYYMMDD''))  > SYSDATE';
    EXECUTE IMMEDIATE v_sql BULK COLLECT
      INTO x_attrvalues
      USING p_attr_group_id, p_catalog_group_id;
  END getcharvalues;
  -- +================================================================================+
  -- |PROCEDURE: GetNumValues
  -- +================================================================================+
  -- |Description : This procedure returns a distinct list of values for the passed in
  -- |              Numeric Data Attribute for the passed in ICC.
  -- |
  -- |
  -- |
  -- |
  -- +================================================================================+
  PROCEDURE getnumvalues(p_column_name      IN VARCHAR2,
                         p_attr_group_id    IN INTEGER,
                         p_catalog_group_id IN INTEGER,
                         x_attrvalues       IN OUT NOCOPY t_attrvaluetype) IS
    v_sql VARCHAR2(500);
  BEGIN
    v_sql := 'select distinct ' || p_column_name || ', UOM' ||
             substr(p_column_name, 2, length(p_column_name)) ||
             ' from EGO_MTL_SY_ITEMS_EXT_VL ' ||
             '  join mtl_system_items_vl using (inventory_item_id, item_catalog_group_id) ' ||
             ' where attr_group_id = :1' ||
             '   and item_catalog_group_id = :2 ' || '  and ' ||
             p_column_name || ' is not null ' ||
             '   and NVL(ENABLED_FLAG, ''Y'') = ''Y'' ' ||
             '   and NVL(start_date_active, SYSDATE) <= SYSDATE ' ||
             '   and NVL(end_date_active, to_date(''30000101'', ''YYYYMMDD''))  > SYSDATE';
    EXECUTE IMMEDIATE v_sql BULK COLLECT
      INTO x_attrvalues
      USING p_attr_group_id, p_catalog_group_id;
  END getnumvalues;
  -- +================================================================================+
  -- |PROCEDURE: GetValueSetValues
  -- +================================================================================+
  -- |Description : This procedure returns a distinct list of values for the passed in
  -- |              Value Set.
  -- |
  -- |
  -- |
  -- |
  -- +================================================================================+
  PROCEDURE getvaluesetvalues(p_value_set_id IN INTEGER,
                              x_attrvalues   IN OUT NOCOPY t_attrvaluetype) IS
    CURSOR c_valuesettype IS
      SELECT validation_meaning
        FROM ego_value_sets_v
       WHERE value_set_id = p_value_set_id;
    CURSOR c_flexvalueset IS
      SELECT table_name, value_column_name, additional_where_clause
        FROM ego_val_set_value_tables_v
       WHERE value_set_id = p_value_set_id;
    CURSOR c_tablevalueset IS
      SELECT display_name, NULL uom
        FROM ego_value_set_values_v
       WHERE value_set_id = p_value_set_id
         AND nvl(enabled_code, 'Y') = 'Y'
         AND nvl(start_date, SYSDATE) <= SYSDATE
         AND nvl(end_date, g_highvaldate) > SYSDATE;
    v_value_set_type ego_value_sets_v.validation_meaning%TYPE;
    v_table_name     ego_val_set_value_tables_v.table_name%TYPE;
    v_column_name    ego_val_set_value_tables_v.value_column_name%TYPE;
    v_where_clause   ego_val_set_value_tables_v.additional_where_clause%TYPE;
    v_sql            VARCHAR2(500);
  BEGIN
    OPEN c_valuesettype;
    FETCH c_valuesettype
      INTO v_value_set_type;
    IF c_valuesettype%FOUND THEN
      IF v_value_set_type = 'Table' THEN
        -- This is a table based value set so build the dynamic SQL and populate the
        -- collection with the values
        OPEN c_flexvalueset;
        FETCH c_flexvalueset
          INTO v_table_name, v_column_name, v_where_clause;
        CLOSE c_flexvalueset;
        v_sql := 'select ' || v_column_name || ', NULL uom from ' ||
                 v_table_name;
        IF v_where_clause IS NOT NULL THEN
          v_sql := v_sql || ' where ' || v_where_clause;
        END IF;
        EXECUTE IMMEDIATE v_sql BULK COLLECT
          INTO x_attrvalues;
      ELSE
        -- This is a normal value set so populate the values using the defined cursor c_TableValueSet.
        OPEN c_tablevalueset;
        FETCH c_tablevalueset BULK COLLECT
          INTO x_attrvalues;
        CLOSE c_tablevalueset;
      END IF;
    END IF;
    CLOSE c_valuesettype;
  END getvaluesetvalues;
  -- +================================================================================+
  -- |PROCEDURE: LoadProdAttributes
  -- +================================================================================+
  -- |Description :  This procedure will copy production attributes of items that are in
  -- |               the batches created by the CreateProductionBatch procedure.
  -- +================================================================================+
  PROCEDURE loadprodattributes(p_jobid IN NUMBER, p_attrtype IN INTEGER) IS
    CURSOR c_batchitemlist IS
      SELECT transaction_id,
             inventory_item_id,
             organization_id,
             item_catalog_group_id,
             set_process_id,
             item_number
        FROM mtl_system_items_interface t1
        JOIN dls_job_batches t2
          ON (t1.set_process_id = t2.batch_id)
       WHERE job_id = p_jobid;
    x_attributes_row_table  ego_user_attr_row_table := ego_user_attr_row_table();
    x_attributes_data_table ego_user_attr_data_table := ego_user_attr_data_table();
  BEGIN
    FOR rec IN c_batchitemlist
    LOOP
      getprodattributes(p_catalogid    => rec.item_catalog_group_id,
                        p_inventoryid  => rec.inventory_item_id,
                        p_orgid        => rec.organization_id,
                        p_attrtype     => p_attrtype,
                        x_attr_row_tb  => x_attributes_row_table,
                        x_attr_data_tb => x_attributes_data_table);
      INSERT INTO ego_itm_usr_attr_intrfc
        (transaction_id,
         data_set_id,
         item_number,
         attr_group_int_name,
         attr_int_name,
         attr_value_str,
         attr_value_num,
         attr_value_date,
         attr_disp_value,
         transaction_type,
         organization_id,
         inventory_item_id,
         item_catalog_group_id,
         attr_group_type,
         attr_value_uom,
         process_status,
         data_level_name,
         row_identifier)
        (SELECT rec.transaction_id,
                rec.set_process_id,
                rec.item_number,
                r.attr_group_name,
                d.attr_name,
                d.attr_value_str,
                d.attr_value_num,
                d.attr_value_date,
                d.attr_disp_value,
                '',
                rec.organization_id,
                rec.inventory_item_id,
                rec.item_catalog_group_id,
                r.attr_group_type,
                d.attr_unit_of_measure,
                ego_item_user_attrs_cp_pub.g_ps_to_be_processed,
                r.data_level,
                r.row_identifier
           FROM TABLE(x_attributes_row_table) r,
                TABLE(x_attributes_data_table) d
          WHERE r.row_identifier = d.row_identifier
            AND d.attr_disp_value IS NOT NULL);
      IF x_attributes_row_table IS NULL THEN
        x_attributes_row_table := ego_user_attr_row_table();
      END IF;
      IF x_attributes_data_table IS NULL THEN
        x_attributes_data_table := ego_user_attr_data_table();
      END IF;
      x_attributes_row_table.delete;
      x_attributes_data_table.delete;
    END LOOP;
    COMMIT;
  END loadprodattributes;

  -- +================================================================================+
  -- |FUNCTION: GetNodeCount
  -- +================================================================================+
  -- |Description : This utility function is used to get the entiry count within an
  -- |              XML document.
  -- |
  -- +================================================================================+
  FUNCTION getnodecount(p_xmltype IN xmltype, p_nodepath IN VARCHAR2)
    RETURN NUMBER IS
    v_nodecnt NUMBER;
  BEGIN
    SELECT COUNT(1)
      INTO v_nodecnt
      FROM TABLE(xmlsequence(extract(p_xmltype, p_nodepath)));
    RETURN v_nodecnt;
  END getnodecount;

  -- +================================================================================+
  -- |FUNCTION: GetSourceSystemCd
  -- +================================================================================+
  -- |Description : This utility function returns the source system code or the passed
  -- |              in id.
  -- |
  -- +================================================================================+
  FUNCTION getsourcesystemcd(p_srcsysid IN integer)
    RETURN VARCHAR2 IS

    CURSOR c_sourcesystem IS
    SELECT orig_system 
      FROM hz_orig_systems_vl
      WHERE orig_system_id = p_srcsysid;

    v_srcsyscd VARCHAR2(150);
   
  BEGIN
  
    IF p_srcsysid IS NULL THEN
      v_srcsyscd := g_import_src_system;
    ELSE
       OPEN c_sourcesystem;
       FETCH c_sourcesystem INTO v_srcsyscd;
       IF c_sourcesystem%NOTFOUND THEN
          logmessage(p_text        => 'Invalid source system id passed -> ' || 
                                       p_srcsysid,
                     p_log_level   => fnd_log.level_procedure,
                     p_module_name => g_pkgname || '.GetSourceSystemCd');
          CLOSE c_sourcesystem;
          RETURN NULL;
       END IF;
       CLOSE c_sourcesystem;
    END IF;
    RETURN v_srcsyscd;
  END;
  -- +================================================================================+
  -- |FUNCTION: GetBatchID
  -- +================================================================================+
  -- |Description : This function will return a batch id.  
  -- |              
  -- |
  -- +================================================================================+
  FUNCTION getbatchid(p_sourcesystem IN VARCHAR2)
    RETURN NUMBER IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    v_lhandle VARCHAR2(200);
    v_rc      NUMBER;
    v_batchid DLS_JOB_BATCHES.BATCH_ID%TYPE := 0;
    v_batchcnt DLS_JOB_BATCHES.BATCH_COUNT%TYPE := 0;
    v_srcsyscd VARCHAR2(150);
    
    CURSOR c_batchid IS
      SELECT batch_id, batch_count
        FROM dls_job_batches
       WHERE job_id = g_dlsjobid
         AND nvl(source_system, 'null') = nvl(v_srcsyscd, 'null')
         and current_batch = 'Y';
         
  BEGIN
    v_srcsyscd := getsourcesystemcd(p_sourcesystem);
    IF v_srcsyscd IS NULL then
       RETURN 0;
    END IF;
    
    dbms_lock.allocate_unique(g_dlsjobid, v_lhandle);
    v_rc := dbms_lock.request(v_lhandle, dbms_lock.x_mode);
    IF v_rc <> 0 THEN
      logmessage(p_text        => 'Lock Request Failed',
                 p_log_level   => fnd_log.level_unexpected,
                 p_module_name => g_pkgname || '.getBatchID');
      RETURN 0;
    END IF;
    
    OPEN c_batchid;
    FETCH c_batchid INTO v_batchid, v_batchcnt;
    IF c_batchid%NOTFOUND THEN
       v_batchcnt := 0;
       v_batchid := createworkbenchbatch(p_batch_name => 'External Batch - JobID: ' || g_dlsjobid,
                                         p_batch_type => dls_connector_pub.g_item_batch_type,
                                         p_src_system => v_srcsyscd);
       IF v_batchid IS NULL THEN
          logmessage(p_text        => 'Faled to create batch.  Exiting',
                     p_log_level   => fnd_log.level_unexpected,
                     p_module_name => g_pkgname || '.getbatchid');
          v_rc := dbms_lock.release(v_lhandle);
          return 0;
       END IF;
    END IF;
    CLOSE c_batchid;
    
    IF v_batchcnt > g_batch_size THEN
      UPDATE DLS_JOB_BATCHES
         SET CURRENT_BATCH = 'N'
       WHERE BATCH_ID = v_batchid;
      v_batchid := createworkbenchbatch(p_batch_name => 'External Batch - JobID: ' || g_dlsjobid,
                                        p_batch_type => dls_connector_pub.g_item_batch_type,
                                        p_src_system => v_srcsyscd);
       IF v_batchid IS NULL THEN
          logmessage(p_text        => 'Faled to create batch.  Exiting',
                     p_log_level   => fnd_log.level_unexpected,
                     p_module_name => g_pkgname || '.getbatchid');
          v_rc := dbms_lock.release(v_lhandle);
          return 0;
       END IF;
    ELSE
      UPDATE DLS_JOB_BATCHES
         SET BATCH_COUNT = BATCH_COUNT + 1
       WHERE batch_id = v_batchid;
    END IF;
    
    v_rc := dbms_lock.release(v_lhandle);
    -- IF we failed to release the current lock log it.
    IF v_rc <> 0 THEN
      logmessage(p_text        => 'Lock Release Failed',
                 p_log_level   => fnd_log.level_unexpected,
                 p_module_name => g_pkgname || '.getBatchID');
    END IF;
    COMMIT;
    RETURN v_batchid;
    EXCEPTION 
      WHEN OTHERS THEN
         v_rc := dbms_lock.release(v_lhandle);
         raise;
  END getbatchid;
  

  -- +================================================================================+
  -- |PROCEDURE: ParseItem
  -- +================================================================================+
  -- |Description :  This procedure will parse the XML passed from the DSA
  -- |Version   Date         Author          Remarks                                  |
  -- |========  ===========  =============== =========================================|
  -- |1.1       12/03/2014   Anand Lonkar    TMS# 20141110-00037
  -- |                                       PROCEDURE parseitem
  -- |					     Commented lines for Alternate Catalog 
  -- |					     updates, Removed the counter for Params
  -- +================================================================================+
  PROCEDURE parseitem(p_itemxml    IN CLOB,
                      p_itemparams IN OUT NOCOPY t_itemattrs) IS
    v_itemcnt      INTEGER := 0;
    v_xmltype      xmltype;
    v_paramname    VARCHAR2(40);
    v_paramvalue   VARCHAR2(4000);
    v_setprocessid mtl_system_items_interface.set_process_id%TYPE := NULL;
    v_itemnumber   mtl_system_items_interface.item_number%TYPE := NULL;
    v_orgid        mtl_system_items_interface.organization_id%TYPE := NULL;
    v_inventoryid  mtl_system_items_interface.inventory_item_id%TYPE := NULL;
    v_transid      mtl_system_items_interface.transaction_id%TYPE := NULL;
    v_srcsysid     mtl_system_items_interface.source_system_id%TYPE := NULL;
    v_srcsysref    mtl_system_items_interface.source_system_reference%TYPE := NULL;
  BEGIN
    v_xmltype := xmltype(p_itemxml);
    -- Parse the XML into Item parameters, attributes and Alternate Catalog entries.
    v_itemcnt := getnodecount(v_xmltype, '/items/item/params/param');
    -- Parse Item Parameters.
    FOR c IN 1 .. v_itemcnt
    LOOP
      SELECT extractvalue(v_xmltype,
                          '/items/item/params/param[' || to_char(c) ||
                          ']/@name') param_name,
             extractvalue(v_xmltype,
                          '/items/item/params/param[' || to_char(c) ||
                          ']/text()') param_value
        INTO v_paramname, v_paramvalue
        FROM dual;
      p_itemparams(upper(v_paramname)) := v_paramvalue;
    END LOOP;
    IF p_itemparams.exists('INTERFACE_TABLE_UNIQUE_ID') = TRUE THEN
      SELECT set_process_id,
             item_number,
             organization_id,
             inventory_item_id,
             transaction_id,
             source_system_id,
             source_system_reference
        INTO v_setprocessid,
             v_itemnumber,
             v_orgid,
             v_inventoryid,
             v_transid,
             v_srcsysid,
             v_srcsysref
        FROM mtl_system_items_interface
       WHERE interface_table_unique_id =
             to_number(p_itemparams('INTERFACE_TABLE_UNIQUE_ID'));
    END IF;
    IF p_itemparams.exists('ITEM_NUMBER') = FALSE THEN
      p_itemparams('ITEM_NUMBER') := v_itemnumber;
    END IF;
    IF p_itemparams.exists('ORGANIZATION_ID') = FALSE THEN
      p_itemparams('ORGANIZATION_ID') := nvl(v_orgid, g_organization_id);
    END IF;
    IF p_itemparams.exists('INVENTORY_ITEM_ID') = FALSE THEN
      p_itemparams('INVENTORY_ITEM_ID') := v_inventoryid;
    END IF;
    IF p_itemparams.exists('TRANSACTION_ID') = FALSE THEN
      IF v_transid IS NULL THEN
        v_transid := dls_transaction_id_seq.nextval;
      END IF;
      p_itemparams('TRANSACTION_ID') := v_transid;
    END IF;
    IF p_itemparams.exists('SOURCE_SYSTEM_ID') = FALSE THEN
      p_itemparams('SOURCE_SYSTEM_ID') := v_srcsysid;
    END IF;
    IF p_itemparams.exists('SOURCE_SYSTEM_REFERENCE') = FALSE THEN
      p_itemparams('SOURCE_SYSTEM_REFERENCE') := v_srcsysref;
    END IF;
    IF p_itemparams.exists('SET_PROCESS_ID') = FALSE THEN
      IF v_setprocessid IS NULL THEN
         p_itemparams('SET_PROCESS_ID') := getbatchid(p_sourcesystem => p_itemparams('SOURCE_SYSTEM_ID'));
      ELSE
         p_itemparams('SET_PROCESS_ID') := v_setprocessid;
      END IF;
    END IF;
    -- Parse Item Attributes.
    v_itemcnt := getnodecount(v_xmltype, '/items/item/attrs/attr');
    FOR c IN 1 .. v_itemcnt
    LOOP
      INSERT INTO dls_usr_attr_values
        (data_set_id,
         organization_id,
         item_number,
         inventory_item_id,
         transaction_id,
         source_system_id,
         source_system_reference,
         attr_group_int_name,
         attr_int_name,
         attr_value_str,
         attr_value_uom,
         attr_order)
        (SELECT to_number(p_itemparams('SET_PROCESS_ID')),
                to_number(p_itemparams('ORGANIZATION_ID')),
                p_itemparams('ITEM_NUMBER'),
                to_number(p_itemparams('INVENTORY_ITEM_ID')),
                to_number(p_itemparams('TRANSACTION_ID')),
                to_number(p_itemparams('SOURCE_SYSTEM_ID')),
                p_itemparams('SOURCE_SYSTEM_REFERENCE'),
                extractvalue(v_xmltype,
                             '/items/item/attrs/attr[' || to_char(c) ||
                             ']/attr_param' || '[1]/text()') attr_group,
                extractvalue(v_xmltype,
                             '/items/item/attrs/attr[' || to_char(c) ||
                             ']/attr_param' || '[2]/text()') attr,
                extractvalue(v_xmltype,
                             '/items/item/attrs/attr[' || to_char(c) ||
                             ']/attr_param' || '[3]/text()') attr_val,
                extractvalue(v_xmltype,
                             '/items/item/attrs/attr[' || to_char(c) ||
                             ']/attr_param' || '[4]/text()') attr_uom,
                c
           FROM dual);
    END LOOP;
    -- Parse Item Catalog Info.
    v_itemcnt := getnodecount(v_xmltype,
                              '/items/item/alt_catalogs/alt_catalog');
    FOR c IN 1 .. v_itemcnt
    LOOP
      INSERT INTO dls_alt_item_categories
        (set_process_id,
         organization_id,
         item_number,
         inventory_item_id,
         transaction_id,
         category_set_name,
         category_name)
        (SELECT to_number(p_itemparams('SET_PROCESS_ID')),
                to_number(p_itemparams('ORGANIZATION_ID')),
                p_itemparams('ITEM_NUMBER'),
                to_number(p_itemparams('INVENTORY_ITEM_ID')),
                to_number(p_itemparams('TRANSACTION_ID')),
--                extractvalue(v_xmltype,
--                             '/items/item/alt_catalogs/alt_catalog[' ||
--                             to_char(c) || ']/alt_catalog_param[' ||
--                             to_char(c) || ']/@name'),
--                extractvalue(v_xmltype,
--                             '/items/item/alt_catalogs/alt_catalog[' ||
--                             to_char(c) || ']/alt_catalog_param[' ||
--                             to_char(c) || ']/text()')
-- Added code starts TMS# 20141110-00037
                extractvalue(v_xmltype,
                             '/items/item/alt_catalogs/alt_catalog[' ||
                             to_char(c) || ']/alt_catalog_param[1]/@name'),
                extractvalue(v_xmltype,
                             '/items/item/alt_catalogs/alt_catalog[' ||
                             to_char(c) || ']/alt_catalog_param[1]/text()')
-- Added Code ends TMS# 20141110-00037
           FROM dual);
    END LOOP;
  END parseitem;
  -- +================================================================================+
  -- |PROCEDURE: ProcessItemParams
  -- +================================================================================+
  -- |Description :  This procedure will update or insert a new row into the
  -- |               MTL_SYSTEM_ITEMS_INTERFACE table based on the value in the
  -- |               p_operation parameter.
  -- +================================================================================+
  PROCEDURE processitemparams(p_operation  IN VARCHAR2,
                              p_itemparams IN OUT NOCOPY t_itemattrs) IS
    e_invalid_column EXCEPTION;
    PRAGMA EXCEPTION_INIT(e_invalid_column, -19210);
    v_xmldoc   xmldom.domdocument;
    v_mainnode xmldom.domnode;
    v_rootnode xmldom.domnode;
    v_rownode  xmldom.domnode;
    v_itemnode xmldom.domnode;
    v_rootelmt xmldom.domelement;
    v_element  xmldom.domelement;
    v_textnode xmldom.domtext;
    v_index    VARCHAR2(40);
    v_doc      VARCHAR2(4000);
    v_rows     NUMBER := 0;
    v_context  dbms_xmlstore.ctxtype;
  BEGIN
    -- If the source_system_reference is null then we have an external batch so set the process_flag to 1
    -- so we can view the records in the interface.  If the source_system_reference is not null then
    -- we have an internal batch which requires the process_flag set to 0 inorder to view the items in the
    -- interface.
    IF p_itemparams('SOURCE_SYSTEM_REFERENCE') IS NULL THEN
      p_itemparams('PROCESS_FLAG') := '1';
    ELSE
      p_itemparams('PROCESS_FLAG') := '0';
    END IF;
    p_itemparams('TRANSACTION_TYPE') := 'SYNC';
    v_xmldoc := xmldom.newdomdocument;
    v_mainnode := xmldom.makenode(v_xmldoc);
    v_rootelmt := xmldom.createelement(doc => v_xmldoc, tagname => 'ROWSET');
    v_rootnode := xmldom.appendchild(v_mainnode,
                                     xmldom.makenode(v_rootelmt));
    v_element := xmldom.createelement(doc => v_xmldoc, tagname => 'ROW');
    v_rownode := xmldom.appendchild(v_rootnode, xmldom.makenode(v_element));
    v_index := p_itemparams.first;
    LOOP
      IF p_itemparams(v_index) IS NOT NULL THEN
        v_element  := xmldom.createelement(doc     => v_xmldoc,
                                           tagname => v_index);
        v_itemnode := xmldom.appendchild(v_rownode,
                                         xmldom.makenode(v_element));
        v_textnode := xmldom.createtextnode(doc  => v_xmldoc,
                                            data => p_itemparams(v_index));
        v_itemnode := xmldom.appendchild(v_itemnode,
                                         xmldom.makenode(v_textnode));
      END IF;
      v_index := p_itemparams.next(v_index);
      EXIT WHEN v_index IS NULL;
    END LOOP;
    xmldom.writetobuffer(v_xmldoc, v_doc);
    xmldom.freedocument(v_xmldoc);
    v_context := dbms_xmlstore.newcontext('INV.MTL_SYSTEM_ITEMS_INTERFACE');
    IF p_operation = 'INSERT' THEN
      v_index := p_itemparams.first;
      LOOP
        IF p_itemparams(v_index) IS NOT NULL THEN
          BEGIN
            dbms_xmlstore.setupdatecolumn(ctxhdl  => v_context,
                                          colname => v_index);
          EXCEPTION
            WHEN e_invalid_column THEN
              logmessage(p_text        => SQLERRM,
                         p_log_level   => fnd_log.level_unexpected,
                         p_module_name => g_pkgname || '.processitemparams');
            WHEN OTHERS THEN
              logmessage(p_text        => SQLERRM,
                         p_log_level   => fnd_log.level_unexpected,
                         p_module_name => g_pkgname || '.processitemparams');
              RAISE;
          END;
        END IF;
        v_index := p_itemparams.next(v_index);
        EXIT WHEN v_index IS NULL;
      END LOOP;
      v_rows := dbms_xmlstore.insertxml(ctxhdl => v_context, xdoc => v_doc);
    ELSE
      dbms_xmlstore.setkeycolumn(ctxhdl  => v_context,
                                 colname => 'INTERFACE_TABLE_UNIQUE_ID');
      v_index := p_itemparams.first;
      LOOP
        IF v_index NOT IN ('SET_PROCESS_ID',
                           'ITEM_NUMBER',
                           'ORGANIZATION_ID',
                           'TRANSACTION_ID',
                           'INVENTORY_ITEM_ID',
                           'INTERFACE_TABLE_UNIQUE_ID',
                           'SOURCE_SYSTEM_ID',
                           'SOURCE_SYSTEM_REFERENCE') THEN
          BEGIN
            dbms_xmlstore.setupdatecolumn(ctxhdl  => v_context,
                                          colname => v_index);
          EXCEPTION
            WHEN e_invalid_column THEN
              logmessage(p_text        => SQLERRM,
                         p_log_level   => fnd_log.level_unexpected,
                         p_module_name => g_pkgname || '.processitemparams');
            WHEN OTHERS THEN
              logmessage(p_text        => SQLERRM,
                         p_log_level   => fnd_log.level_unexpected,
                         p_module_name => g_pkgname || '.processitemparams');
              RAISE;
          END;
        END IF;
        v_index := p_itemparams.next(v_index);
        EXIT WHEN v_index IS NULL;
      END LOOP;
      v_rows := dbms_xmlstore.updatexml(ctxhdl => v_context, xdoc => v_doc);
    END IF;
    IF v_rows < 1 THEN
      logmessage('Insert into MTL_SYSTEM_ITEMS_INTERFACE failed',
                 fnd_log.level_unexpected,
                 g_pkgname || '.processitemparams');
    END IF;
    dbms_xmlstore.closecontext(v_context);
    --Check to see if item is to be excluded and all required keys exist in item data.
    --If the supporting data exists then insert record into the EGO_IMPORT_EXCLUDED_SS_ITEMS table.
    IF p_itemparams.exists('CONFIRM_STATUS') = TRUE AND
       p_itemparams.exists('SOURCE_SYSTEM_REFERENCE') = TRUE AND
       p_itemparams.exists('SOURCE_SYSTEM_ID') = TRUE THEN
      IF p_itemparams('CONFIRM_STATUS') = 'EX' AND
         p_itemparams('SOURCE_SYSTEM_REFERENCE') IS NOT NULL THEN
        MERGE INTO ego_import_excluded_ss_items t1
        USING (SELECT p_itemparams('SOURCE_SYSTEM_ID') source_system_id,
                      p_itemparams('SOURCE_SYSTEM_REFERENCE') source_system_reference
                 FROM dual) q1
        ON (t1.source_system_id = q1.source_system_id AND t1.source_system_reference = q1.source_system_reference)
        WHEN MATCHED THEN
          UPDATE
             SET last_updated_by   = fnd_global.user_id,
                 last_update_login = fnd_global.login_id
        WHEN NOT MATCHED THEN
          INSERT
            (source_system_id,
             source_system_reference,
             object_version_number,
             created_by,
             creation_date,
             last_updated_by,
             last_update_date,
             last_update_login)
          VALUES
            (to_number(p_itemparams('SOURCE_SYSTEM_ID')),
             p_itemparams('SOURCE_SYSTEM_REFERENCE'),
             1,
             fnd_global.user_id,
             SYSDATE,
             fnd_global.user_id,
             SYSDATE,
             fnd_global.login_id);
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      dbms_xmlstore.closecontext(v_context);
      RAISE;
  END processitemparams;
  -- +================================================================================+
  -- |PROCEDURE: validateattrvalues
  -- +================================================================================+
  -- |Description :  Validate usr attribute data in the DLS_USR_ATTR_VALUES table.
  -- +================================================================================+
  PROCEDURE validateattrvalues IS
    CURSOR c_attrlist IS
      SELECT attr_group_int_name,
             attr_int_name,
             attr_value_str,
             attr_value_uom
        FROM dls_usr_attr_values
         FOR UPDATE;
    CURSOR c_attrdetails(p_attr_group_name VARCHAR2, p_attr_name VARCHAR2) IS
      SELECT data_type_code, uom_class, nvl(value_set_id, 0) value_set_id
        FROM ego_attrs_v
       WHERE attr_group_name = p_attr_group_name
         AND attr_name = p_attr_name
         AND application_id = g_application_id;
    v_attrdetails c_attrdetails%ROWTYPE;
    v_attrvalues  dls_usr_attr_values%ROWTYPE;
  BEGIN
    FOR rec IN c_attrlist
    LOOP
      OPEN c_attrdetails(rec.attr_group_int_name, rec.attr_int_name);
      FETCH c_attrdetails
        INTO v_attrdetails;
      IF c_attrdetails%NOTFOUND THEN
        logmessage(p_text        => 'Attribute not found. ATTR GROUP -> ' ||
                                    rec.attr_group_int_name ||
                                    ' ATTR NAME -> ' || rec.attr_int_name ||
                                    ' VALUE -> ' || rec.attr_value_str,
                   p_log_level   => fnd_log.level_unexpected,
                   p_module_name => g_pkgname || '.validateattrvalues');
        CLOSE c_attrdetails;
      ELSE
        CLOSE c_attrdetails;
        v_attrvalues.attr_value_str  := NULL;
        v_attrvalues.attr_value_num  := NULL;
        v_attrvalues.attr_value_date := NULL;
        v_attrvalues.attr_value_uom  := NULL;
        v_attrvalues.attr_disp_value := rec.attr_value_str;
        IF v_attrdetails.data_type_code = 'N' THEN
          IF length(TRIM(translate(rec.attr_value_str,
                                   ' +-.0123456789',
                                   ' '))) IS NULL THEN
            v_attrvalues.attr_value_num := to_number(rec.attr_value_str);
          ELSE
            logmessage(p_text        => 'Non Numeric value for numeric attribute ATTR GROUP -> ' ||
                                        rec.attr_group_int_name ||
                                        ' ATTR NAME -> ' ||
                                        rec.attr_int_name || ' VALUE -> ' ||
                                        rec.attr_value_str,
                       p_log_level   => fnd_log.level_unexpected,
                       p_module_name => g_pkgname || '.validateattrvalues');
            v_attrvalues.attr_disp_value := rec.attr_value_str;
          END IF;
          IF rec.attr_value_uom IS NOT NULL THEN
            v_attrvalues.attr_value_uom := rec.attr_value_uom;
          END IF;
        ELSIF v_attrdetails.data_type_code = 'X' THEN
          dbms_output.put_line('Got a date.  Column Name -> ' || rec.attr_int_name);
          dbms_output.put_line('Date Format Str -> ' || g_datetimefmtstr || '  Date Str -> ' ||  rec.attr_value_str);
          v_attrvalues.attr_value_date := to_date(rec.attr_value_str,
                                                  g_datefmtstr);
          v_attrvalues.attr_disp_value := null;
        ELSIF v_attrdetails.data_type_code = 'Y' THEN
          dbms_output.put_line('Got a date time.  Column Name -> ' || rec.attr_int_name);
          dbms_output.put_line('Date Format Str -> ' || g_datetimefmtstr || '  Date Str -> ' ||  rec.attr_value_str);
          v_attrvalues.attr_value_date := to_date(rec.attr_value_str,
                                                  g_datetimefmtstr);
          v_attrvalues.attr_disp_value := null;
        ELSIF v_attrdetails.data_type_code = 'C' THEN
          IF rec.attr_value_uom IS NOT NULL THEN
            v_attrvalues.attr_value_str := rec.attr_value_str || ' ' ||
                                           rec.attr_value_uom;
          ELSE
            v_attrvalues.attr_value_str := rec.attr_value_str;
          END IF;
        END IF;
        UPDATE dls_usr_attr_values
           SET attr_value_str  = v_attrvalues.attr_value_str,
               attr_value_num  = v_attrvalues.attr_value_num,
               attr_value_date = v_attrvalues.attr_value_date,
               attr_value_uom  = v_attrvalues.attr_value_uom,
               attr_disp_value = v_attrvalues.attr_disp_value
         WHERE CURRENT OF c_attrlist;
      END IF;
    END LOOP;
  END validateattrvalues;
  -- +================================================================================+
  -- |PROCEDURE: checkrowidentifiers
  -- +================================================================================+
  -- |Description :  Populate the ROW_IDENTIFIER column using the value from an existing
  -- |               row in the EGO_ITM_USR_ATTR_INTRFC table or the next value in the
  -- |               sequence DLS_UDA_ROW_IDENTIFIER_S
  -- +================================================================================+
  PROCEDURE checkrowidentifiers IS
    CURSOR c_attrgrouplist IS
      SELECT data_set_id,
             organization_id,
             item_number,
             inventory_item_id,
             attr_group_int_name,
             source_system_id,
             source_system_reference
        FROM dls_usr_attr_values
       GROUP BY data_set_id,
                organization_id,
                item_number,
                inventory_item_id,
                attr_group_int_name,
                source_system_id,
                source_system_reference;
    CURSOR c_existingrowidentifier1(p_datasetid   NUMBER,
                                    p_orgid       NUMBER,
                                    p_invitemid   NUMBER,
                                    p_attrgrpname VARCHAR2) IS
      SELECT row_identifier
        FROM ego_itm_usr_attr_intrfc
       WHERE data_set_id = p_datasetid
         AND organization_id = p_orgid
         AND inventory_item_id = p_invitemid
         AND attr_group_int_name = p_attrgrpname;
    CURSOR c_existingrowidentifier2(p_datasetid   NUMBER,
                                    p_orgid       NUMBER,
                                    p_srcsysid    NUMBER,
                                    p_srcsysref   VARCHAR2,
                                    p_attrgrpname VARCHAR2) IS
      SELECT row_identifier
        FROM ego_itm_usr_attr_intrfc
       WHERE data_set_id = p_datasetid
         AND organization_id = p_orgid
         AND source_system_id = p_srcsysid
         AND source_system_reference = p_srcsysref
         AND attr_group_int_name = p_attrgrpname;
    CURSOR c_existingrowidentifier3(p_datasetid   NUMBER,
                                    p_orgid       NUMBER,
                                    p_itemnumber  VARCHAR2,
                                    p_attrgrpname VARCHAR2) IS
      SELECT row_identifier
        FROM ego_itm_usr_attr_intrfc
       WHERE data_set_id = p_datasetid
         AND organization_id = p_orgid
         AND item_number = p_itemnumber
         AND attr_group_int_name = p_attrgrpname;
    v_rowidentifier NUMBER(38);
  BEGIN
    FOR rec IN c_attrgrouplist
    LOOP
      IF rec.inventory_item_id IS NOT NULL THEN
        OPEN c_existingrowidentifier1(rec.data_set_id,
                                      rec.organization_id,
                                      rec.inventory_item_id,
                                      rec.attr_group_int_name);
        FETCH c_existingrowidentifier1
          INTO v_rowidentifier;
        CLOSE c_existingrowidentifier1;
      ELSIF rec.source_system_id IS NOT NULL AND
            rec.source_system_reference IS NOT NULL THEN
        OPEN c_existingrowidentifier2(rec.data_set_id,
                                      rec.organization_id,
                                      rec.source_system_id,
                                      rec.source_system_reference,
                                      rec.attr_group_int_name);
        FETCH c_existingrowidentifier2
          INTO v_rowidentifier;
        CLOSE c_existingrowidentifier2;
      ELSIF rec.item_number IS NOT NULL THEN
        OPEN c_existingrowidentifier3(rec.data_set_id,
                                      rec.organization_id,
                                      rec.item_number,
                                      rec.attr_group_int_name);
        FETCH c_existingrowidentifier3
          INTO v_rowidentifier;
        CLOSE c_existingrowidentifier3;
      END IF;
      IF v_rowidentifier IS NULL THEN
        SELECT dls_uda_row_identifier_s.nextval
          INTO v_rowidentifier
          FROM dual;
      END IF;
      UPDATE dls_usr_attr_values
         SET row_identifier = v_rowidentifier
       WHERE attr_group_int_name = rec.attr_group_int_name;
    END LOOP;
  END checkrowidentifiers;
  -- +================================================================================+
  -- |PROCEDURE: setrowidentifiers
  -- +================================================================================+
  -- |Description :  Populate the ROW_IDENTIFIER column using the value from an existing
  -- |               row in the EGO_ITM_USR_ATTR_INTRFC table or the next value in the
  -- |               sequence DLS_UDA_ROW_IDENTIFIER_S
  -- +================================================================================+
  PROCEDURE setrowidentifiers IS
    CURSOR c_attrgrouplist IS
      SELECT attr_group_int_name
        FROM dls_usr_attr_values
       GROUP BY attr_group_int_name;
    v_rowident NUMBER;
  BEGIN
    FOR rec IN c_attrgrouplist
    LOOP
      v_rowident := dls_uda_row_identifier_s.nextval;
      UPDATE dls_usr_attr_values
         SET row_identifier = v_rowident
       WHERE attr_group_int_name = rec.attr_group_int_name;
    END LOOP;
  END setrowidentifiers;
  -- +================================================================================+
  -- |PROCEDURE: InsertItemAttrs
  -- +================================================================================+
  -- |Description :  This procedure will insert new rows into the
  -- |               EGO_USR_ITM_ATTR_INTRFC table.
  --
  --     REVISIONS:
  --     Ver        Date        Author                     Description
  --     ---------  ----------  ---------------         -------------------------
  --     1.1        12/03/2014  Anand Lonkar           TMS# 20141110-00037
  --                                                   attr_value_str commented and 
  --                                                   replaced with NULL for fixing 
  --                                                   table based attributes.
  --
  -- +================================================================================+
  PROCEDURE insertitemudas IS
  BEGIN
    INSERT INTO
    /*+ append */
    ego_itm_usr_attr_intrfc
      (data_set_id,
       transaction_id,
       inventory_item_id,
       item_number,
       organization_id,
       source_system_id,
       source_system_reference,
       attr_group_int_name,
       attr_int_name,
       row_identifier,
       attr_value_str,
       attr_value_num,
       attr_value_date,
       attr_disp_value,
       transaction_type,
       attr_value_uom,
       process_status)
      (SELECT data_set_id,
              transaction_id,
              inventory_item_id,
              item_number,
              organization_id,
              source_system_id,
              source_system_reference,
              attr_group_int_name,
              attr_int_name,
              row_identifier,
              -- attr_value_str,
              -- Added code starts TMS# 20141110-00037
              NULL,
              -- Added code ends TMS# 20141110-00037
              attr_value_num,
              attr_value_date,
              attr_disp_value,
              'SYNC',
              attr_value_uom,
              decode(source_system_reference, NULL, 1, 0)
         FROM dls_usr_attr_values);
  END insertitemudas;
  -- +================================================================================+
  -- |PROCEDURE: ProcessItemAttrs
  -- +================================================================================+
  -- |Description :  This procedure will update or insert new rows into the
  -- |               EGO_USR_ITM_ATTR_INTRFC table based on the value in the
  -- |               p_operation parameter
  -- +================================================================================+
  PROCEDURE upsertitemudas IS
  BEGIN
    MERGE INTO ego_itm_usr_attr_intrfc t1
    USING dls_usr_attr_values t2
    ON (t1.data_set_id = t2.data_set_id AND t1.row_identifier = t2.row_identifier AND t1.attr_int_name = t2.attr_int_name AND t2.attr_group_int_name != 'Primary')
    WHEN MATCHED THEN
      UPDATE
         SET attr_value_str   = t2.attr_value_str,
             attr_value_num   = t2.attr_value_num,
             attr_disp_value  = t2.attr_disp_value,
             transaction_type = 'SYNC',
             attr_value_uom   = t2.attr_value_uom,
             process_status   = decode(t2.source_system_reference,
                                       NULL,
                                       1,
                                       0)
    WHEN NOT MATCHED THEN
      INSERT
        (data_set_id,
         transaction_id,
         inventory_item_id,
         item_number,
         organization_id,
         source_system_id,
         source_system_reference,
         attr_group_int_name,
         attr_int_name,
         row_identifier,
         attr_value_str,
         attr_value_num,
         attr_value_date,
         attr_disp_value,
         transaction_type,
         attr_value_uom,
         process_status)
      VALUES
        (t2.data_set_id,
         t2.transaction_id,
         t2.inventory_item_id,
         t2.item_number,
         t2.organization_id,
         t2.source_system_id,
         t2.source_system_reference,
         t2.attr_group_int_name,
         t2.attr_int_name,
         t2.row_identifier,
         t2.attr_value_str,
         t2.attr_value_num,
         t2.attr_value_date,
         t2.attr_disp_value,
         'SYNC',
         t2.attr_value_uom,
         decode(t2.source_system_reference, NULL, 1, 0));
  END upsertitemudas;
  -- +================================================================================+
  -- |PROCEDURE: ProcessItemCatalog
  -- +================================================================================+
  -- |Description :  This procedure will update or insert new records into the
  -- |               MTL_ITEM_CATEGORIES_INTERFACE table based on the value in the
  -- |               p_operation parameter.
  -- +================================================================================+
  PROCEDURE processitemcatalog IS
  BEGIN
    MERGE INTO mtl_item_categories_interface t1
    USING dls_alt_item_categories t2
    ON (t1.set_process_id = t2.set_process_id AND t1.item_number = t2.item_number AND t1.organization_id = t2.organization_id AND t1.category_set_name = t2.category_set_name)
    WHEN NOT MATCHED THEN
      INSERT
        (inventory_item_id,
         transaction_id,
         set_process_id,
         item_number,
         organization_id,
         transaction_type,
         process_flag,
         category_set_name,
         category_name)
      VALUES
        (t2.inventory_item_id,
         t2.transaction_id,
         t2.set_process_id,
         t2.item_number,
         t2.organization_id,
         'SYNC',
         1,
         t2.category_set_name,
         t2.category_name);
  END processitemcatalog;
  -- ****************************************************************************************
  --  Public procedures and functions
  -- ****************************************************************************************
  -- +================================================================================+
  -- |PROCEDURE: AutoBuild
  -- +================================================================================+
  -- |Description : This procedure inserts a distinct set of attribute/value pairs
  -- |              for the given ICC and it's decendants into the table
  -- |              dls_attribute_info.
  -- |
  -- |
  -- |
  -- +================================================================================+
  PROCEDURE produceautobuild(p_jobid IN NUMBER, p_itemclass IN VARCHAR2) IS
    v_semanticattrs dls_semantic_attributes_tbl := dls_semantic_attributes_tbl();
    v_itemclasses   dls_item_catalog_group_tbl := dls_item_catalog_group_tbl();
    CURSOR c_itemclasses IS
      SELECT parent_catalog_group_id,
             parent_catalog_group,
             catalog_group_id,
             catalog_group
        FROM TABLE(v_itemclasses);
    CURSOR c_itemattrs IS
      SELECT attr_group_id,
             attr_group_name,
             attr_name,
             value_set_id,
             value_set_name,
             data_type_code,
             database_column,
             display_sequence
        FROM TABLE(v_semanticattrs)
       ORDER BY display_sequence;
    v_attrvalues t_attrvaluetype;
    v_valueset   CHAR(1);
    v_locale     VARCHAR2(20) := userenv('LANG');
    v_attrvalrec t_attrvaluerec;
    v_defaultuom mtl_units_of_measure.uom_code%TYPE;
  BEGIN
    g_dlsjobid := p_jobid;
    getitemclasshierarchy(p_itemclass           => p_itemclass,
                          x_itemclasscategories => v_itemclasses);
    FOR rec IN c_itemclasses
    LOOP
      -- For each item in the ICC Hierarchy get the attributes that are members of the
      -- semantic model display format.
      v_semanticattrs.delete;
      getitemclassattrs(rec.catalog_group_id,
                        dls_connector_pub.g_semanticattr,
                        v_semanticattrs);
      IF v_semanticattrs.count = 0 THEN
        -- There are no attributes defined for this ICC so
        -- to maintain the hierarchy insert the ICC into the dls_attribute_info table.
        INSERT INTO dls_attribute_info
          (job_id,
           parent_catalog_group_id,
           parent_catalog_group,
           catalog_group_id,
           catalog_group,
           locale)
        VALUES
          (p_jobid,
           rec.parent_catalog_group_id,
           rec.parent_catalog_group,
           rec.catalog_group_id,
           rec.catalog_group,
           v_locale);
      ELSE
        FOR rec2 IN c_itemattrs
        LOOP
          v_attrvalues.delete;
          -- For each attribute get a list of distinct values.
          IF rec2.attr_group_name = 'Primary' THEN
            getprimaryvalues(p_attr_name        => rec2.attr_name,
                             p_catalog_group_id => rec.catalog_group_id,
                             x_attrvalues       => v_attrvalues);
          ELSIF nvl(rec2.value_set_id, 0) != 0 THEN
            getvaluesetvalues(p_value_set_id => rec2.value_set_id,
                              x_attrvalues   => v_attrvalues);
            v_valueset := '1';
          ELSIF rec2.data_type_code = 'C' THEN
            getcharvalues(p_column_name      => rec2.database_column,
                          p_attr_group_id    => rec2.attr_group_id,
                          p_catalog_group_id => rec.catalog_group_id,
                          x_attrvalues       => v_attrvalues);
          ELSIF rec2.data_type_code = 'N' THEN
            getnumvalues(p_column_name      => rec2.database_column,
                         p_attr_group_id    => rec2.attr_group_id,
                         p_catalog_group_id => rec.catalog_group_id,
                         x_attrvalues       => v_attrvalues);
            IF v_attrvalues.count = 0 THEN
              v_defaultuom := getdefaultattruom(p_attr_group => rec2.attr_group_name,
                                                p_attr_name  => rec2.attr_name);
              IF v_defaultuom IS NOT NULL THEN
                v_attrvalrec.attr_value := 0;
                v_attrvalrec.attr_uom := v_defaultuom;
                v_attrvalues(1) := v_attrvalrec;
              END IF;
            END IF;
          END IF;
          IF v_attrvalues.count = 0 THEN
            INSERT INTO dls_attribute_info
              (job_id,
               parent_catalog_group_id,
               parent_catalog_group,
               catalog_group_id,
               catalog_group,
               attr_id,
               attr_name,
               attr_value,
               locale)
            VALUES
              (p_jobid,
               rec.parent_catalog_group_id,
               rec.parent_catalog_group,
               rec.catalog_group_id,
               rec.catalog_group,
               rec2.attr_group_name || '|' || rec2.attr_name,
               rec2.attr_name,
               NULL,
               v_locale);
          ELSE
            FORALL x IN v_attrvalues.first .. v_attrvalues.last
              INSERT INTO dls_attribute_info
                (job_id,
                 parent_catalog_group_id,
                 parent_catalog_group,
                 catalog_group_id,
                 catalog_group,
                 attr_id,
                 attr_name,
                 attr_value,
                 attr_uom,
                 valueset,
                 locale)
              VALUES
                (p_jobid,
                 rec.parent_catalog_group_id,
                 rec.parent_catalog_group,
                 rec.catalog_group_id,
                 rec.catalog_group,
                 rec2.attr_group_name || '|' || rec2.attr_name,
                 rec2.attr_name,
                 v_attrvalues(x).attr_value,
                 v_attrvalues(x).attr_uom,
                 rec2.value_set_name,
                 v_locale);
          END IF;
        END LOOP;
      END IF;
    END LOOP;
    COMMIT;
  END produceautobuild;
  -- +================================================================================+
  -- |PROCEDURE: ProduceSampleData
  -- +================================================================================+
  -- |Description : This procedure inserts a set of sample data into the table
  -- |              dls_item_sample_data.  It uses the passed in ICC and will pull
  -- |              production items for the passed in ICC and all children ICC.
  -- |              The Profile option DLS_AUTOBUILD_SAMPLE_SIZE can be used to limit
  -- |              number of rows to per ICC.
  -- +================================================================================+
  PROCEDURE producesampledata(p_jobid IN NUMBER, p_itemclass IN VARCHAR2) IS
    v_itemclasses dls_item_catalog_group_tbl := dls_item_catalog_group_tbl(NULL);
    CURSOR c_itemclasses IS
      SELECT catalog_group_id, catalog_group FROM TABLE(v_itemclasses);
    CURSOR c_itemlist(p_cataloggrpid INTEGER) IS
      SELECT catalog_group, item_number, description
        FROM (SELECT catalog_group,
                     mtl.concatenated_segments item_number,
                     mtl.description,
                     dbms_random.random        rnd
                FROM mtl_system_items_vl mtl
                JOIN ego_catalog_groups_v
                  ON (catalog_group_id = item_catalog_group_id)
               WHERE catalog_group_id = p_cataloggrpid
                 AND nvl(style_item_flag, 'N') = 'N'
               ORDER BY rnd);
    TYPE t_itemdata IS TABLE OF VARCHAR2(240) INDEX BY PLS_INTEGER;
    v_cataloggroups t_itemdata;
    v_itemids       t_itemdata;
    v_descriptions  t_itemdata;
  BEGIN
    g_dlsjobid := p_jobid;
    getitemclasshierarchy(p_itemclass           => p_itemclass,
                          x_itemclasscategories => v_itemclasses);
    FOR rec IN c_itemclasses
    LOOP
      IF semanticmodeldefined(rec.catalog_group_id) = TRUE THEN
        OPEN c_itemlist(rec.catalog_group_id);
        FETCH c_itemlist BULK COLLECT
          INTO v_cataloggroups,
               v_itemids,
               v_descriptions LIMIT g_sample_size;
        FORALL x IN v_cataloggroups.first .. v_cataloggroups.last
          INSERT INTO dls_item_sample_data
            (jobid, catalog_group, item_number, description)
          VALUES
            (p_jobid, v_cataloggroups(x), v_itemids(x), v_descriptions(x));
        CLOSE c_itemlist;
      END IF;
    END LOOP;
    COMMIT;
  END producesampledata;
  -- +================================================================================+
  -- |PROCEDURE: ProductionPull
  -- +================================================================================+
  -- |Description :  This procedure will select and insert all active and effective
  -- |               production records for the passed in Item Class and it's children
  -- |               into a temporary table.  The items UDA's will be concatenated into
  -- |               a single column of name/value pairs.
  -- +================================================================================+
  PROCEDURE productionpull(p_jobid IN NUMBER, p_itemclass IN VARCHAR2) IS
    l_sql_insert VARCHAR2(1000) := 'insert /* APPEND */ into dls_production_pull ' ||
                                   'SELECT to_char(:JobID), ' ||
                                   'organization_id, ' ||
                                   'inventory_item_id, ' ||
                                   'organization_name, ' ||
                                   'catalog_group, ' ||
                                   'concatenated_segments sku, ' ||
                                   'description, ' || 'long_description, ' ||
                                   'dls_connector_pub.concatprodattributes(item_catalog_group_id, ' ||
                                   'inventory_item_id, ' ||
                                   'organization_id) attrs ' ||
                                   'FROM mtl_system_items_vl item ' ||
                                   'JOIN (SELECT catalog_group_id item_catalog_group_id, catalog_group ' ||
                                   'FROM ego_catalog_groups_v ' ||
                                   'START WITH upper(catalog_group) = upper(:ItemClass) ' ||
                                   'CONNECT BY PRIOR catalog_group_id = parent_catalog_group_id) ' ||
                                   'USING (item_catalog_group_id) ' ||
                                   'JOIN mtl_organizations ' ||
                                   'USING (organization_id) ' ||
                                   'WHERE nvl(enabled_flag, ''Y'') = ''Y'' ' ||
                                   'AND nvl(item.style_item_flag, ''N'') = ''N'' ' ||
                                   'AND nvl(start_date_active, SYSDATE) <= SYSDATE ' ||
                                   'AND nvl(end_date_active, to_date(''30000101'', ''YYYYMMDD'')) > SYSDATE ';
  BEGIN
    g_dlsjobid := p_jobid;
    EXECUTE IMMEDIATE l_sql_insert
      USING p_jobid, p_itemclass;
    COMMIT;
  END productionpull;
  -- +================================================================================+
  -- |PROCEDURE: CreateProductionBatch
  -- +================================================================================+
  -- |Description :  This procedure will select and insert all active and effective
  -- |               production records for the passed in Item Class and it's children
  -- |               and insert them into the Open Interface Tables in n number of batches.
  -- |               The number of records in each batch is controlled by the Profile Option
  -- |               DLS Import Batch Size.
  -- +================================================================================+
  PROCEDURE createproductionbatch(p_jobid     IN NUMBER,
                                  p_itemclass IN VARCHAR2,
                                  p_attrtype  IN INTEGER) IS
    e_invalidicc EXCEPTION;
    PRAGMA EXCEPTION_INIT(e_invalidicc, -20002);
    CURSOR c_proditems IS
      SELECT inventory_item_id,
             item.organization_id,
             concatenated_segments item_number,
             org.organization_code,
             description,
             long_description,
             cat_hier.catalog_group_id,
             cat_hier.catalog_group,
             primary_uom_code
        FROM mtl_system_items_vl item,
             mtl_parameters org,
             (SELECT catalog_group,
                     catalog_group_id,
                     parent_catalog_group,
                     parent_catalog_group_id,
                     segment1
                FROM ego_catalog_groups_v
               START WITH upper(catalog_group) = upper(p_itemclass)
              CONNECT BY PRIOR catalog_group_id = parent_catalog_group_id
               ORDER SIBLINGS BY catalog_group) cat_hier
       WHERE cat_hier.catalog_group_id = item.item_catalog_group_id
         AND org.organization_id = item.organization_id
         AND nvl(item.style_item_flag, 'N') = 'N'
         AND nvl(item.enabled_flag, 'Y') = 'Y'
         AND nvl(item.start_date_active, SYSDATE) <= SYSDATE
         AND nvl(item.end_date_active, g_highvaldate) > SYSDATE
         AND org.organization_id IN
             (SELECT org1.organization_id
                FROM mtl_parameters org1
               WHERE org1.master_organization_id = g_organization_id
              UNION
              SELECT org1.organization_id
                FROM mtl_parameters org1
               WHERE org1.organization_id = g_organization_id);
    TYPE t_proditems IS TABLE OF c_proditems%ROWTYPE INDEX BY PLS_INTEGER;
    v_productionitems t_proditems;
    l_proc_name       VARCHAR2(200) := g_pkgname || '.' ||
                                       'CreateProductionBatch';
    v_batchid         INTEGER;
    v_cnt             INTEGER := 0;
    v_rowcount        INTEGER := 0;
    v_batchrowcnt     NUMBER := 0;
  BEGIN
    g_dlsjobid := p_jobid;
    OPEN c_proditems;
    LOOP
      FETCH c_proditems BULK COLLECT
        INTO v_productionitems LIMIT g_batch_size;
      IF v_productionitems.count > 0 THEN
        v_cnt     := v_cnt + 1;
        v_batchid := createworkbenchbatch('Production Batch - JobID: ' ||
                                          p_jobid || ' Batch Number ' ||
                                          v_cnt);
        IF v_batchid IS NULL THEN
          logmessage(p_text        => 'Faled to create batch.  Exiting',
                     p_log_level   => fnd_log.level_unexpected,
                     p_module_name => g_pkgname || '.CreateProductionBatch');
          EXIT;
        END IF;
        FORALL c IN 1 .. v_productionitems.count
          INSERT INTO mtl_system_items_interface
            (inventory_item_id,
             organization_id,
             item_number,
             organization_code,
             description,
             long_description,
             item_catalog_group_id,
             item_catalog_group_name,
             primary_uom_code,
             transaction_id,
             set_process_id,
             process_flag,
             transaction_type,
             creation_date,
             created_by,
             last_update_date,
             last_updated_by)
          VALUES
            (v_productionitems(c).inventory_item_id,
             v_productionitems(c).organization_id,
             v_productionitems(c).item_number,
             v_productionitems(c).organization_code,
             v_productionitems(c).description,
             v_productionitems(c).long_description,
             v_productionitems(c).catalog_group_id,
             v_productionitems(c).catalog_group,
             v_productionitems(c).primary_uom_code,
             mtl_system_items_interface_s.nextval,
             v_batchid,
             1,
             '',
             SYSDATE,
             fnd_global.user_id,
             SYSDATE,
             fnd_global.user_id);
        -- Update batch information
        v_batchrowcnt := v_productionitems.count;
        UPDATE dls_job_batches
           SET batch_count = v_batchrowcnt
         WHERE batch_id = v_batchid;
                      
        COMMIT;
      END IF;
      v_rowcount := v_rowcount + v_productionitems.count;
      EXIT WHEN v_productionitems.count < g_batch_size;
      v_productionitems.delete;
    END LOOP;
    CLOSE c_proditems;
    IF v_rowcount > 0 THEN
      loadprodattributes(p_jobid, p_attrtype);
      COMMIT;
    ELSE
      logmessage(p_text        => l_proc_name || ' (-) ' ||
                                  'Zero Rows Returned using ICC -> ' ||
                                  p_itemclass,
                 p_log_level   => fnd_log.level_unexpected,
                 p_module_name => l_proc_name);
      RAISE e_invalidicc;
    END IF;
  END createproductionbatch;
  -- +================================================================================+
  -- |PROCEDURE: UpdateItem
  -- +================================================================================+
  -- |Description :  This procedure will update an existing item in the interface
  -- |               tables.
  -- +================================================================================+
  PROCEDURE updateitem(p_jobid IN NUMBER, p_itemxml IN CLOB) IS
    v_itemparams t_itemattrs;
  BEGIN
    g_dlsjobid := p_jobid;
    DELETE dls_usr_attr_values;
    parseitem(p_itemxml, v_itemparams);
    processitemparams('UPDATE', v_itemparams);
    validateattrvalues;
    checkrowidentifiers;
    upsertitemudas;
    processitemcatalog;
  END updateitem;
  -- +================================================================================+
  -- |PROCEDURE: InsertItem
  -- +================================================================================+
  -- |Description :  This procedure will Insert an item in the interface tables.
  -- +================================================================================+
  PROCEDURE insertitem(p_jobid IN NUMBER, p_itemxml IN CLOB) IS
    v_itemparams t_itemattrs;
  BEGIN
    g_dlsjobid := p_jobid;
    DELETE dls_usr_attr_values;
    parseitem(p_itemxml, v_itemparams);
    processitemparams('INSERT', v_itemparams);
    validateattrvalues;
    setrowidentifiers;
    insertitemudas;
    processitemcatalog;
  END insertitem;
  -- +================================================================================+
  -- |PROCEDURE: Initialize
  -- +================================================================================+
  -- |Description : This procedure will initialize all package global variables.
  -- |
  -- +================================================================================+
  PROCEDURE initialize IS
    CURSOR c_userlangpref(p_levelvalue INTEGER) IS
      SELECT profile_option_value
        FROM fnd_profile_option_values
       WHERE profile_option_id =
             (SELECT profile_option_id
                FROM fnd_profile_options
               WHERE profile_option_name = 'ICX_LANGUAGE')
         AND level_value = p_levelvalue
       ORDER BY level_value ASC;
    v_default INTEGER := 0;
  BEGIN
    g_pkgname           := 'DLS_CONNECTOR_PVT';
    g_semantic_model    := nvl(fnd_profile.value('DLS_CATALOG_UDA_DISPLAY_TYPE'),
                               'SemanticModel');
    g_import_src_system := nvl(fnd_profile.value('DLS_IMPORT_SRC_SYSTEM'),
                               'Product Information Management Data Hub');
    g_attrextractmethod := nvl(fnd_profile.value('DLS_ATTRIBUTE_EXTRACT_METHOD'),
                               'Display Format');
    g_ext_src_system    := nvl(fnd_profile.value('DLS_EXTERNAL_SRC_SYSTEM'),
                               'Product Information Management Data Hub');
    g_dls_user          := nvl(fnd_profile.value('DLS_PIMMGR_USER'),
                               'PIMMGR');
    g_resp_key          := nvl(fnd_profile.value('DLS_PIMMGR_RESP'),
                               'EGO_PIM_DATA_LIBRARIAN');
    g_sample_size       := nvl(fnd_profile.value('DLS_AUTOBUILD_SAMPLE_SIZE'),
                               100);
    g_batch_size        := nvl(fnd_profile.value('DLS_EGO_IMPORT_BATCH_SIZE'),
                               10000);
    g_datefmtstr        := nvl(fnd_profile.value('DLS_DATE_IMPORT_FORMAT'),
                               'YYYYMMDD');
    g_datetimefmtstr    := nvl(fnd_profile.value('DLS_DATETIME_IMPORT_FORMAT'),
                               'YYYYMMDD HH24:MI:SS');
    SELECT user_id
      INTO g_user_id
      FROM fnd_user
     WHERE user_name = g_dls_user;
    SELECT application_id, responsibility_id
      INTO g_application_id, g_resp_id
      FROM fnd_responsibility
     WHERE responsibility_key = g_resp_key;
    fnd_global.apps_initialize(g_user_id, g_resp_id, g_application_id);
    IF g_application_id IS NULL OR g_resp_id IS NULL THEN
      raise_application_error(-20000,
                              'Connector configuration incomplete. ' ||
                              'DLS_PIMMGR_RESP profile option not configured correctly.');
    END IF;
    g_organization_id := fnd_profile.value('EGO_USER_ORGANIZATION_CONTEXT');
    OPEN c_userlangpref(g_user_id);
    FETCH c_userlangpref
      INTO g_language;
    CLOSE c_userlangpref;
    IF g_language IS NULL THEN
      OPEN c_userlangpref(v_default);
      FETCH c_userlangpref
        INTO g_language;
      CLOSE c_userlangpref;
      IF g_language IS NULL THEN
        raise_application_error(-20000,
                                'Connector configuration incomplete. ' ||
                                'Langange not set at default or user level');
      ELSE
        logmessage(p_text        => 'Default application language not set at ' ||
                                    'user level using using default system level -> ' ||
                                    g_language,
                   p_log_level   => fnd_log.level_unexpected,
                   p_module_name => g_pkgname || '.Initialize');
      END IF;
    END IF;
    g_lastitemclassid := 0;
    g_lastprodcatalog := 0;
    EXECUTE IMMEDIATE 'alter session set NLS_LANGUAGE = ''' || g_language ||
                      ''' ';
  EXCEPTION
    WHEN no_data_found THEN
      IF g_user_id IS NULL THEN
        raise_application_error(-20000,
                                'Connector configuration incomplete. ' ||
                                'DLS_PIMMGR_USER profile option not configured correctly.');
      END IF;
  END initialize;
  -- +================================================================================+
  -- |PROCEDURE: ImportBatch
  -- +================================================================================+
  -- |Description : This procedure submits an import request to the concurrent queue
  -- |              for the passed in batch id.
  -- +================================================================================+
  PROCEDURE importbatch(p_jobid IN NUMBER, p_batchid IN INTEGER) IS
    v_rc    NUMBER;
    err_msg VARCHAR2(2000);
  BEGIN
    g_dlsjobid := p_jobid;
    fnd_message.clear;
    v_rc := fnd_request.submit_request(application => 'EGO',
                                       program     => 'EGOIJAVA',
                                       sub_request => FALSE,
                                       argument1   => NULL,
                                       --Result Format Usage Id (Non-Required numeric value set)
                                       argument2 => fnd_global.user_id,
                                       --User Id (Non-Required numeric value set)
                                       argument3 => 'US',
                                       --Language (value set FND_LANGUAGES, Language Codes)
                                       argument4 => fnd_global.resp_id,
                                       --Responsibility Id(Non-Required numeric value set)
                                       argument5 => fnd_global.prog_appl_id,
                                       --Application Id(Non-Required numeric value set)
                                       argument6 => 3,
                                       --Run From (Integers Between 1 and 9)
                                       argument7 => 'N',
                                       --Create New Batch (EGO_YES_NO)
                                       argument8 => p_batchid,
                                       --Batch Id (Non-Required numeric value set)
                                       argument9 => '',
                                       --Batch Name (240 characters, unvalidated)
                                       argument10 => 'N',
                                       --Auto Import On Data Load (EGO_YES_NO)
                                       argument11 => 'N',
                                       --Auto Match On Data Load (EGO_YES_NO)
                                       argument12 => 'N',
                                       --Change Order Option (EGO_YES_NO)
                                       argument13 => 'Y',
                                       --Add All Imported Items To CO (EGO_YES_NO)
                                       argument14 => '',
                                       --Change Order Category (240 characters, unvalidated)
                                       argument15 => '',
                                       --Change Order Type (240 characters, unvalidated)
                                       argument16 => '',
                                       --Change Order Name (240 characters, unvalidated)
                                       argument17 => '',
                                       --Change Order Number (240 characters, unvalidated)
                                       argument18 => ''
                                       --Change Order Description (240 characters, unvalidated)
                                       );
    IF v_rc = 0 THEN
      fnd_message.retrieve(err_msg);
      logmessage(p_text        => err_msg,
                 p_log_level   => fnd_log.level_unexpected,
                 p_module_name => g_pkgname || '.ImportBatch');
    ELSE
      COMMIT;
      logmessage(p_text        => 'Import request ID -> ' || v_rc ||
                                  ' issued for batch id -> ' || p_batchid,
                 p_log_level   => fnd_log.level_procedure,
                 p_module_name => g_pkgname || '.ImportBatch');
    END IF;
  END importbatch;
  -- +================================================================================+
  -- |PROCEDURE: ImportJobBatches
  -- +================================================================================+
  -- |Description : This procedure will invoke the batch import process using the
  -- |              FND_REQUEST.SEND_MESSAGE procedure.
  -- +================================================================================+
  PROCEDURE importjobbatches(p_jobid IN NUMBER) IS
    CURSOR c_batchlist IS
      SELECT batch_id FROM dls_job_batches WHERE job_id = p_jobid;
  BEGIN
    FOR rec IN c_batchlist
    LOOP
      importbatch(p_jobid => p_jobid, p_batchid => rec.batch_id);
    END LOOP;
  END importjobbatches;
  -- +================================================================================+
  -- |Function: PrepBatch
  -- +================================================================================+
  -- |Description : This procedure returns the attributes that have been associated
  -- |              with the display format identified by the
  -- |              DLS_CATALOG_UDA_DISPLAY_TYPE profile option.
  -- +================================================================================+
  PROCEDURE prepbatch(p_jobid IN NUMBER, p_batchid IN INTEGER) IS
    v_return_status VARCHAR2(10);
    v_error_code    NUMBER;
    v_msg_count     NUMBER;
    v_msg_data      VARCHAR2(10000);
  BEGIN
    g_dlsjobid := p_jobid;
    ego_item_pub.prep_batch_data_for_import_ui(p_api_version   => 1.0,
                                               p_batch_id      => p_batchid,
                                               x_return_status => v_return_status,
                                               x_errorcode     => v_error_code,
                                               x_msg_count     => v_msg_count,
                                               x_msg_data      => v_msg_data);
    IF v_return_status <> 'S' THEN
      logmessage(p_text        => v_msg_data,
                 p_log_level   => fnd_log.level_procedure,
                 p_module_name => g_pkgname || '.prepBatch');
      RETURN;
    END IF;
    RETURN;
  END prepbatch;
  -- +================================================================================+
  -- |PROCEDURE: PrepJobBatches
  -- +================================================================================+
  -- |Description : This procedure will invoke PrepBatch API for each batch created 
  -- |              by the passed in JOB ID
  -- +================================================================================+
  PROCEDURE prepjobbatches(p_jobid IN NUMBER) IS
  CURSOR c_batchlist IS
      SELECT batch_id FROM dls_job_batches WHERE job_id = p_jobid;
  BEGIN
    FOR rec in c_batchlist
    LOOP
      prepbatch(p_jobid => p_jobid, p_batchid => rec.batch_id);
    END LOOP;
  END prepjobbatches;
  -- +================================================================================+
  -- |FUNCTION: ConcatProdAttributes
  -- +================================================================================+
  -- |Description : This function will return a concatenated list of the passed in
  -- |              Items Semantic Attributes from the production tables.
  -- +================================================================================+
  FUNCTION concatprodattributes(p_catalogid   IN INTEGER,
                                p_inventoryid IN INTEGER,
                                p_orgid       IN VARCHAR2) RETURN VARCHAR2 IS
    x_attributes_row_table  ego_user_attr_row_table := ego_user_attr_row_table();
    x_attributes_data_table ego_user_attr_data_table := ego_user_attr_data_table();
    v_attrs                 VARCHAR2(4000) := '';
    CURSOR c_attr_info_table IS
      SELECT r.attr_group_id,
             r.row_identifier,
             r.attr_group_name,
             r.attr_group_type,
             r.data_level,
             d.attr_name,
             d.attr_value_str,
             d.attr_value_num,
             d.attr_value_date,
             d.attr_disp_value,
             d.attr_unit_of_measure
        FROM TABLE(x_attributes_row_table) r,
             TABLE(x_attributes_data_table) d
       WHERE r.row_identifier = d.row_identifier;
  BEGIN
    IF p_catalogid IS NOT NULL AND p_catalogid != 0 THEN
      getprodattributes(p_catalogid    => p_catalogid,
                        p_inventoryid  => p_inventoryid,
                        p_orgid        => p_orgid,
                        p_attrtype     => dls_connector_pub.g_semanticattr,
                        x_attr_row_tb  => x_attributes_row_table,
                        x_attr_data_tb => x_attributes_data_table);
      FOR rec IN c_attr_info_table
      LOOP
        v_attrs := v_attrs || rec.attr_name || ' ' || rec.attr_disp_value || ' ' ||
                   nvl(rec.attr_unit_of_measure,
                       getdefaultattruom(rec.attr_group_name, rec.attr_name)) || ' ';
      END LOOP;
    END IF;
    RETURN v_attrs;
  END concatprodattributes;
  -- +================================================================================+
  -- |FUNCTION: ConcatIntrfcAttributes
  -- +================================================================================+
  -- |Description : This function will return a concatenated list of the passed in
  -- |              Items Semantic Attributes from the interface tables.
  -- +================================================================================+
  FUNCTION concatintrfcattributes(p_itemclassid   IN INTEGER,
                                  p_batchid       IN NUMBER,
                                  p_transactionid IN NUMBER) RETURN VARCHAR2 IS
    v_semanticattrs dls_semantic_attributes_tbl := dls_semantic_attributes_tbl();
    CURSOR c_attrlist IS
      SELECT attr_group_name, attr_name, data_type_code
        FROM TABLE(v_semanticattrs)
       WHERE attr_group_name != 'PRIMARY'
       ORDER BY display_sequence;
  
    CURSOR c_attrinrfcvalue(p_attrgroupname VARCHAR2, p_attrname VARCHAR2) IS
      SELECT attr_disp_value
        FROM ego_itm_usr_attr_intrfc
       WHERE transaction_id = p_transactionid
         AND data_set_id = p_batchid
         AND attr_group_int_name = p_attrgroupname
         AND attr_int_name = p_attrname;
  
    v_attrvalue VARCHAR2(1000);
    v_attrstr   VARCHAR2(4000) := '';
  BEGIN
    IF p_itemclassid IS NOT NULL AND p_itemclassid != 0 THEN
      getitemclassattrs(p_itemclassid   => p_itemclassid,
                        p_attrtype      => dls_connector_pub.g_semanticattr,
                        x_semanticattrs => v_semanticattrs);
      FOR rec IN c_attrlist
      LOOP
        v_attrvalue := NULL;
        OPEN c_attrinrfcvalue(rec.attr_group_name, rec.attr_name);
        FETCH c_attrinrfcvalue
          INTO v_attrvalue;
        CLOSE c_attrinrfcvalue;
      
        IF v_attrvalue IS NOT NULL THEN
          v_attrstr := v_attrstr || rec.attr_name || ' ' || v_attrvalue || ' ' ||
                       getdefaultattruom(rec.attr_group_name, rec.attr_name) || ' ';
        END IF;
      END LOOP;
    END IF;
    RETURN v_attrstr;
  END concatintrfcattributes;
  -- +================================================================================+
  -- |PROCEDURE: UpdateBatchStatus
  -- +================================================================================+
  -- |Description : This procedure will update the batch status passed in batch id
  -- |              to the passed in status.
  -- +================================================================================+
  PROCEDURE updatebatchstatus(p_jobid       IN NUMBER,
                              p_batchid     IN NUMBER,
                              p_batchstatus IN VARCHAR2) IS
    v_returnstatus  VARCHAR2(1);
    v_returnmessage VARCHAR2(2000);
    v_batchname     VARCHAR2(200);
    CURSOR c_batchname IS
      SELECT NAME FROM ego_import_batches_tl WHERE batch_id = p_batchid;
  BEGIN
    g_dlsjobid := p_jobid;
    OPEN c_batchname;
    FETCH c_batchname
      INTO v_batchname;
    CLOSE c_batchname;
    IF v_batchname IS NOT NULL THEN
      ego_import_batches_pkg.update_batch_status(p_batch_id      => p_batchid,
                                                 p_batch_name    => v_batchname,
                                                 p_batch_status  => p_batchstatus,
                                                 x_return_status => v_returnstatus,
                                                 x_error_msg     => v_returnmessage);
      IF v_returnstatus <> fnd_api.g_ret_sts_success THEN
        FOR m IN 1 .. fnd_msg_pub.count_msg
        LOOP
          v_returnmessage := v_returnmessage || chr(10) ||
                             fnd_msg_pub.get(m, 'F');
        END LOOP;
        logmessage(p_text        => v_returnmessage,
                   p_log_level   => fnd_log.level_unexpected,
                   p_module_name => g_pkgname || '.updatebatchstatus');
      END IF;
    ELSE
      logmessage(p_text        => 'Batch not found for Batch ID => ' ||
                                  p_batchid,
                 p_log_level   => fnd_log.level_unexpected,
                 p_module_name => g_pkgname || '.updatebatchstatus');
    END IF;
    COMMIT;
  END updatebatchstatus;

  -- +================================================================================+
  -- |PROCEDURE: FinalizeItemBatch
  -- +================================================================================+
  -- |Description : This procedure will finalize an item external dataload by creating (n)
  -- |              of batches and moving the newly loaded items into the created batch.
  -- |              The profile option "DLS_EGO_IMPORT_BATCH_SIZE" is used to contol
  -- |              the number of records inserted into a batch.
  -- |              The interface tables this procedure works are 
  -- |              1)  MTL_SYSTE_ITEMS_INTERFACE
  -- |              2)  EGO_ITM_USR_ATTR_INTRFC
  -- |              3)  MTL_ITEM_CATEGORIES_INTERFACE
  -- +================================================================================+
  PROCEDURE finalizeitembatch(p_jobid IN INTEGER) IS
    CURSOR c_mtlitemlist(p_batchid NUMBER) IS
      SELECT ROWID, transaction_id
        FROM mtl_system_items_interface
       WHERE set_process_id = p_batchid;
  
    TYPE t_mtlitemrecs IS TABLE OF c_mtlitemlist%ROWTYPE INDEX BY PLS_INTEGER;
    v_mtlitemrecs t_mtlitemrecs;
    v_cnt         INTEGER := 0;
    v_batchid     INTEGER;
    v_batchrowcnt NUMBER := 0;
    v_tmpbatchid  NUMBER := p_jobid * -1;
  
  BEGIN
    LOOP
      OPEN c_mtlitemlist(v_tmpbatchid);
      FETCH c_mtlitemlist BULK COLLECT
        INTO v_mtlitemrecs LIMIT g_batch_size;
      IF v_mtlitemrecs.count > 0 THEN
        v_cnt     := v_cnt + 1;
        v_batchid := createworkbenchbatch(p_batch_name => 'External Batch - JobID: ' ||
                                                          p_jobid ||
                                                          ' Batch Number ' ||
                                                          v_cnt,
                                          p_batch_type => dls_connector_pub.g_item_batch_type,
                                          p_src_system => g_ext_src_system);
        IF v_batchid IS NULL THEN
          logmessage(p_text        => 'Faled to create batch.  Exiting',
                     p_log_level   => fnd_log.level_unexpected,
                     p_module_name => g_pkgname || '.setitembatchid');
          EXIT;
        END IF;
        FORALL c IN 1 .. v_mtlitemrecs.count
          UPDATE mtl_system_items_interface
             SET set_process_id = v_batchid
           WHERE ROWID = v_mtlitemrecs(c).rowid;
        FORALL d IN 1 .. v_mtlitemrecs.count
          UPDATE ego_itm_usr_attr_intrfc
             SET data_set_id = v_batchid
           WHERE data_set_id = v_tmpbatchid
             AND transaction_id = v_mtlitemrecs(d).transaction_id;
        FORALL e IN 1 .. v_mtlitemrecs.count
          UPDATE mtl_item_categories_interface
             SET set_process_id = v_batchid
           WHERE set_process_id = v_tmpbatchid
             AND transaction_id = v_mtlitemrecs(e).transaction_id;
        v_batchrowcnt := v_mtlitemrecs.count;
        
        UPDATE dls_job_batches
           SET batch_count = v_batchrowcnt
         WHERE batch_id = v_batchid;

        COMMIT;
      END IF;
      EXIT WHEN v_mtlitemrecs.count < g_batch_size;
      v_mtlitemrecs.delete;
      CLOSE c_mtlitemlist;
    END LOOP;
    CLOSE c_mtlitemlist;
  END finalizeitembatch;

  -- +================================================================================+
  -- |PROCEDURE: finalizestructbatch
  -- +================================================================================+
  -- |Description : This procedure will finalize a Bill Of Materials external dataload 
  -- |              by creating (n) of batches and moving the newly loaded items into 
  -- |              the created batch.  The profile option "DLS_EGO_IMPORT_BATCH_SIZE" 
  -- |              is used to contol the number of records inserted into a batch.
  -- |              
  -- +================================================================================+
  PROCEDURE finalizestructbatch(p_jobid IN INTEGER) IS
    CURSOR c_bomitemlist(p_batchid NUMBER) IS
      SELECT ROWID, transaction_id
        FROM bom_inventory_comps_interface
       WHERE batch_id = p_batchid;
    TYPE t_bomitemrevs IS TABLE OF c_bomitemlist%ROWTYPE INDEX BY PLS_INTEGER;
  
    v_bomitemrecs t_bomitemrevs;
    v_cnt         INTEGER := 0;
    v_batchid     INTEGER;
    v_batchrowcnt NUMBER := 0;
    v_tmpbatchid  NUMBER := p_jobid * -1;
  
  BEGIN
    LOOP
      OPEN c_bomitemlist(v_tmpbatchid);
      FETCH c_bomitemlist BULK COLLECT
        INTO v_bomitemrecs LIMIT g_batch_size;
      IF v_bomitemrecs.count > 0 THEN
        v_cnt     := v_cnt + 1;
        v_batchid := createworkbenchbatch(p_batch_name => 'External Structure Batch - JobID: ' ||
                                                          p_jobid ||
                                                          ' Batch Number ' ||
                                                          v_cnt,
                                          p_batch_type => dls_connector_pub.g_struct_batch_type,
                                          p_src_system => g_ext_src_system);
        IF v_batchid IS NULL THEN
          logmessage(p_text        => 'Faled to create batch.  Exiting',
                     p_log_level   => fnd_log.level_unexpected,
                     p_module_name => g_pkgname || '.setstructbatchid');
          EXIT;
        END IF;
        FORALL c IN 1 .. v_bomitemrecs.count
          UPDATE bom_inventory_comps_interface
             SET batch_id = v_batchid
           WHERE ROWID = v_bomitemrecs(c).rowid;
        FORALL d IN 1 .. v_bomitemrecs.count
          UPDATE bom_cmp_usr_attr_interface
             SET data_set_id = v_batchid
           WHERE data_set_id = v_tmpbatchid
             AND transaction_id = v_bomitemrecs(d).transaction_id;
        v_batchrowcnt := v_bomitemrecs.count;
      
        UPDATE dls_job_batches
           SET batch_count = v_batchrowcnt
         WHERE batch_id = v_batchid;

        prepbatch(p_jobid => g_dlsjobid, p_batchid => v_batchid);
        
        COMMIT;
      END IF;
      EXIT WHEN v_bomitemrecs.count < g_batch_size;
      v_bomitemrecs.delete;
      CLOSE c_bomitemlist;
    END LOOP;
    CLOSE c_bomitemlist;
  END finalizestructbatch;

  -- +================================================================================+
  -- |PROCEDURE: FinalizeLoad
  -- +=====================:wq===========================================================+
  -- |Description : This procedure will finalize an external dataload by creating (n)
  -- |              of batches and moving the newly loaded items into the created batch.
  -- |              The profile option "DLS_EGO_IMPORT_BATCH_SIZE" is used to contol
  -- |              the number of records inserted into a batch.
  -- +================================================================================+
  PROCEDURE finalizeload(p_jobid IN INTEGER, p_loadtype IN VARCHAR2) IS
  BEGIN
    g_dlsjobid := p_jobid;
    IF p_loadtype = dls_connector_pub.g_item_batch_type THEN
      finalizeitembatch(p_jobid);
    ELSIF p_loadtype = dls_connector_pub.g_struct_batch_type THEN
      finalizestructbatch(p_jobid);
    END IF;
  END finalizeload;
END dls_connector_pvt;
/
SHOW ERRORS;


------------------------------------------------------
--      Granting permissions to the Apps Schema   --
------------------------------------------------------
GRANT ALL ON dls_connector_pvt TO &&3 WITH GRANT OPTION;

------------------------------------------------------
--      Connecting to the Apps Schema             --
------------------------------------------------------
CONNECT &&3/&&4;

------------------------------------------------------
--      Creating synonym in Apps schema           --
------------------------------------------------------

CREATE OR REPLACE SYNONYM dls_connector_pvt FOR &&1..dls_connector_pvt;

SHOW ERRORS;

COMMIT
/
EXIT;

