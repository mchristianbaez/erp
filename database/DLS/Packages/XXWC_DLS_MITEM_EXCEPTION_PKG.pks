CREATE OR REPLACE PACKAGE DLS.XXWC_DLS_MITEM_EXCEPTION_PKG
IS
   /*******************************************************************************************************************************
        $Header XXWC_DLS_MITEM_EXCEPTION_PKG $
        Module Name: XXWC_DLS_MITEM_EXCEPTION_PKG.pks

        PURPOSE:    To handle exceptions in DSA

        REVISIONS:
        Ver        Date        Author                     Description
        ---------  ----------  ---------------    -------------------------------------------------------------------------------
        1.0        03/27/2015  Vamshidhar         Initial Version TMS# 20150223-00026 - Improve exception error handling in EDQP

        1.1        10/08/2015  Vamshidhar         TMS#20150817-00105 - EDQP Bundle changes -- Added Validation to validate Vendor
		
        1.2        05/24/2017  P.Vamshidhar       TMS#20170421-00146 - UPC Validation(EDQP)		

    ******************************************************************************************************************************/


   FUNCTION IS_UPC_VALID_FUNC (P_JOB_ID          IN VARCHAR2,
                               P_COLUMN_NAME     IN VARCHAR2,
                               P_COLUMN_VALUE    IN VARCHAR2,
                               P_STATUS          IN VARCHAR2,
                               P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION EXCEP_BUNDLE_FUNC (P_JOB_ID IN VARCHAR2, P_ID IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION MANDATORY_VALUE_FUNC (P_JOB_ID          IN VARCHAR2,
                                  P_COLUMN_NAME     IN VARCHAR2,
                                  P_COLUMN_VALUE    IN VARCHAR2,
                                  P_STATUS          IN VARCHAR2,
                                  P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION hazmat_value_func (P_JOB_ID          IN VARCHAR2,
                               P_COLUMN_NAME     IN VARCHAR2,
                               P_COLUMN_VALUE    IN VARCHAR2,
                               P_STATUS          IN VARCHAR2,
                               P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION VALIDATE_BRAND_FUNC (P_JOB_ID          IN VARCHAR2,
                                 P_COLUMN_NAME     IN VARCHAR2,
                                 P_COLUMN_VALUE    IN VARCHAR2,
                                 P_STATUS          IN VARCHAR2,
                                 P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION IS_COUNTRY_CODE_VALID_FUNC (P_JOB_ID          IN VARCHAR2,
                                        P_COLUMN_NAME     IN VARCHAR2,
                                        P_COLUMN_VALUE    IN VARCHAR2,
                                        P_STATUS          IN VARCHAR2,
                                        P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION IS_UOM_VALID_FUNC (P_JOB_ID          IN VARCHAR2,
                               P_COLUMN_NAME     IN VARCHAR2,
                               P_COLUMN_VALUE    IN VARCHAR2,
                               P_STATUS          IN VARCHAR2,
                               P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION IS_ITEM_VALID_FUNC (P_JOB_ID          IN VARCHAR2,
                                P_COLUMN_NAME     IN VARCHAR2,
                                P_COLUMN_VALUE    IN VARCHAR2,
                                P_STATUS          IN VARCHAR2,
                                P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION IS_ICC_CAT_VALID_FUNC (P_JOB_ID          IN VARCHAR2,
                                   P_COLUMN_NAME     IN VARCHAR2,
                                   P_COLUMN_VALUE    IN VARCHAR2,
                                   P_STATUS          IN VARCHAR2,
                                   P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2;

   -- Added below function in TMS#20150817-00105 -- Rev 1.1

   FUNCTION IS_XREF_VENDOR_FUNC (P_JOB_ID          IN VARCHAR2,
                                 P_COLUMN_NAME     IN VARCHAR2,
                                 P_COLUMN_VALUE    IN VARCHAR2,
                                 P_STATUS          IN VARCHAR2,
                                 P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2;
	  
-- Added below code in Rev 1.2
 PROCEDURE EDQP_UPC_VALIDATION (P_CROSS_REF   IN     VARCHAR2,
                                X_RET_MESS       OUT VARCHAR2);
 
END XXWC_DLS_MITEM_EXCEPTION_PKG;
/