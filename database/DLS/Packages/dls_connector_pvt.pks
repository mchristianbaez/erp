SET VERIFY OFF;
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
WHENEVER OSERROR EXIT FAILURE ROLLBACK;
create or replace
PACKAGE     dls_connector_pvt AUTHID CURRENT_USER IS

  -- Author  : DSAVOLAINEN
  -- Created : 10/9/2009 3:24:40 PM
  -- Purpose : Rework Original parkage

  -- +================================================================================+
  -- |PROCEDURE: AutoBuild
  -- +================================================================================+
  -- |Description : This procedure inserts a distinct set of attribute/value pairs
  -- |              for the given ICC and it's decendants into the table
  -- |              dls_attribute_info.
  -- +================================================================================+
  PROCEDURE produceautobuild(p_jobid IN NUMBER, p_itemclass IN VARCHAR2);
  -- +================================================================================+
  -- |PROCEDURE: ProduceSampleData
  -- +================================================================================+
  -- |Description : This procedure inserts a set of sample data into the table
  -- |              dls_item_sample_data.  It uses the passed in ICC and will pull
  -- |              production items for the passed in ICC and all children ICC.
  -- |              The Profile option DLS_AUTOBUILD_SAMPLE_SIZE can be used to limit
  -- |              number of rows to per ICC.
  -- +================================================================================+
  PROCEDURE producesampledata(p_jobid IN NUMBER, p_itemclass IN VARCHAR2);
  -- +================================================================================+
  -- |PROCEDURE: ProductionPull
  -- +================================================================================+
  -- |Description :  This procedure will select and insert all active and effective
  -- |               production records for the passed in Item Class and it's children
  -- |               into a temporary table.  The items UDA's will be concatenated into
  -- |               a single column of name/value pairs.
  -- +================================================================================+
  PROCEDURE productionpull(p_jobid IN NUMBER, p_itemclass IN VARCHAR2);

  -- +================================================================================+
  -- |PROCEDURE: CreateProductionBatch
  -- +================================================================================+
  -- |Description :  This procedure will select and insert all active and effective
  -- |               production records for the passed in Item Class and it's children
  -- |               and insert them into the Open Interface Tables in n number of batches.
  -- |               The number of records in each batch is controlled by the Profile Option
  -- |               DLS Import Batch Size.  The parameter p_attrtype controls which type
  -- |               of attributes are extracted for insert.
  -- |               Valid values for p_attrtype
  -- |                      S   Semantic attributes
  -- |                      O   Operational attributes
  -- |                      B   Both
  -- +================================================================================+
  PROCEDURE createproductionbatch(p_jobid     IN NUMBER,
                                  p_itemclass IN VARCHAR2,
                                  p_attrtype  IN INTEGER DEFAULT dls_connector_pub.g_semanticattr);

  -- +================================================================================+
  -- |PROCEDURE: UpdateItem
  -- +================================================================================+
  -- |Description :  This procedure will update and existing item in the interface tables.
  -- +================================================================================+
  PROCEDURE updateitem(p_jobid IN NUMBER, p_itemxml IN CLOB);

  -- +================================================================================+
  -- |PROCEDURE: InsertItem
  -- +================================================================================+
  -- |Description :  This procedure will Insert and item in the interface tables.
  -- +================================================================================+
  PROCEDURE insertitem(p_jobid IN NUMBER, p_itemxml IN CLOB);

  -- +================================================================================+
  -- |PROCEDURE: Initialize
  -- +================================================================================+
  -- |Description :  Sets internal variables to the values set in the DLS profile options
  -- +================================================================================+
  PROCEDURE initialize;

  -- +================================================================================+
  -- |PROCEDURE: ImportBatch
  -- +================================================================================+
  -- |Description : This procedure submits an import request to the concurrent queue
  -- |              for the passed in batch id.
  -- +================================================================================+
  PROCEDURE importbatch(p_jobid IN NUMBER, p_batchid IN INTEGER);

  -- +================================================================================+
  -- |PROCEDURE: ImportJobBatchs
  -- +================================================================================+
  -- |Description : This procedure will invoke the batch import process using the
  -- |              FND_REQUEST.SEND_MESSAGE procedure.
  -- +================================================================================+
  PROCEDURE importjobbatches(p_jobid IN NUMBER);

  -- +================================================================================+
  -- |Function: Prep_Batch_for_Import
  -- +================================================================================+
  -- |Description : This procedure returns the attributes that have been associated
  -- |              with the display format identified by the
  -- |              DLS_CATALOG_UDA_DISPLAY_TYPE profile option.
  -- +================================================================================+
  PROCEDURE prepbatch(p_jobid IN NUMBER, p_batchid IN INTEGER);

  -- +================================================================================+
  -- |PROCEDURE: PrepJobBatchs
  -- +================================================================================+
  -- |Description : This procedure will invoke the batch import process using the
  -- |              FND_REQUEST.SEND_MESSAGE procedure.
  -- +================================================================================+
  PROCEDURE prepjobbatches(p_jobid IN NUMBER);

  -- +================================================================================+
  -- |FUNCTION: ConcatProdAttributes
  -- +================================================================================+
  -- |Description : This function will return a concatenated list of the passed in
  -- |              Items Semantic Attributes.
  -- +================================================================================+
  FUNCTION concatprodattributes(p_catalogid   IN INTEGER,
                                p_inventoryid IN INTEGER,
                                p_orgid       IN VARCHAR2) RETURN VARCHAR2;

  -- +================================================================================+
  -- |FUNCTION: ConcatIntrfcAttributes
  -- +================================================================================+
  -- |Description : This function will return a concatenated list of the passed in
  -- |              Items Semantic Attributes from the interface tables.
  -- +================================================================================+
  FUNCTION concatintrfcattributes(p_itemclassid   IN INTEGER,
                                  p_batchid       IN NUMBER,
                                  p_transactionid IN NUMBER) RETURN VARCHAR2;
  -- +================================================================================+
  -- |PROCEDURE: UpdateBatchStatus
  -- +================================================================================+
  -- |Description : This procedure will update the batch status passed in batch id
  -- |              to the passed in status.
  -- +================================================================================+
  PROCEDURE updatebatchstatus(p_jobid       IN NUMBER,
                              p_batchid     IN NUMBER,
                              p_batchstatus IN VARCHAR2);

  -- +================================================================================+
  -- |PROCEDURE: FinalizeLoad
  -- +================================================================================+
  -- |Description : This procedure will finalize an external dataload by creating (n)
  -- |              of batches and moving the newly loaded items into the created batch.
  -- |              The profile option "DLS_EGO_IMPORT_BATCH_SIZE" is used to contol
  -- |              the number of records inserted into a batch.
  -- +================================================================================+
  PROCEDURE finalizeload(p_jobid IN INTEGER, p_loadtype IN VARCHAR2);

END dls_connector_pvt;
/
show errors
exit
