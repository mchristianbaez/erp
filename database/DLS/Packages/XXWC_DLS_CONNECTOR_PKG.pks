CREATE OR REPLACE PACKAGE DLS.XXWC_DLS_CONNECTOR_PKG
AS
   /****************************************************************************************************************
        $Header XXWC_DLS_CONNECTOR_PKG $
        Module Name: XXWC_DLS_CONNECTOR_PKG.pks

        PURPOSE:    Extension of DLS Connector PUB

        REVISIONS:
        Ver        Date        Author                     Description
        ---------  ----------  ---------------    -------------------------------------------------------------------
        1.0        11/01/2014  RK                 Initial Version TMS # 20141110-00037 -Item Mass Import Tool

        1.1        03/02/2015  Vamshidhar         TMS#20150226-00196 -- Added new function (is_category_class_valid)
                                                  to package to validate Category Class

        1.2        09/04/2015  Vamshidhar         TMS#20150310-00048-EDQP - Mass Item Uploads showing Interface User

        1.3        10/08/2015  P.Vamshidhar       TMS#20150817-00105  - EDQP Bundle changes

    *****************************************************************************************************************/


   TYPE lookup_rec_type IS RECORD
   (
      lookup_code   VARCHAR2 (40),
      meaning       VARCHAR2 (300)
   );

   TYPE lookup_tbl_type IS TABLE OF lookup_rec_type
      INDEX BY BINARY_INTEGER;

   PROCEDURE get_taxware_codes (
      x_taxware_codes_tbl      OUT NOCOPY xxwc_dls_connector_pkg.lookup_tbl_type,
      x_return_status          OUT NOCOPY VARCHAR2,
      x_msg_count              OUT NOCOPY NUMBER,
      x_msg_data               OUT NOCOPY VARCHAR2);



   PROCEDURE get_country_codes (
      x_country_codes_tbl      OUT NOCOPY xxwc_dls_connector_pkg.lookup_tbl_type,
      x_return_status          OUT NOCOPY VARCHAR2,
      x_msg_count              OUT NOCOPY NUMBER,
      x_msg_data               OUT NOCOPY VARCHAR2);

   PROCEDURE get_vendor_codes (
      x_vendor_codes_tbl      OUT NOCOPY xxwc_dls_connector_pkg.lookup_tbl_type,
      x_return_status         OUT NOCOPY VARCHAR2,
      x_msg_count             OUT NOCOPY NUMBER,
      x_msg_data              OUT NOCOPY VARCHAR2);

   FUNCTION is_item_number_valid (p_item_number       IN VARCHAR2,
                                  p_organization_id   IN NUMBER)
      RETURN VARCHAR2;


   FUNCTION is_icc_valid (p_icc_name IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION is_country_code_valid (p_country_code IN VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION is_uom_valid (p_primary_uom_code IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION is_acc_valid (p_acc_name IN VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION process_xref (p_item_number         IN VARCHAR2,
                          p_organization_id     IN NUMBER,
                          p_upc_xref_type       IN VARCHAR2,
                          p_upc_xref_value      IN VARCHAR2,
                          p_upc_ref_trxn_type   IN VARCHAR2,
                          p_upc_xref_vendor     IN VARCHAR2) -- Added in 1.3 Rev by Vamshi
      RETURN VARCHAR2;


   --TMS#20150226-00196 -- Vamshidhar --Added below function to validate category class.

   FUNCTION is_category_class_valid (p_cat_class IN VARCHAR2)
      RETURN VARCHAR2;


   -- TMS#20150310-00048 -- Added below function 1.2 to trigger to import batches.

   FUNCTION importjobbatches (p_user_id IN VARCHAR2, p_job_id IN VARCHAR2)
      RETURN VARCHAR2;


   -- TMS#20150310-00048 -- Added below function 1.2 to trigger import job.

   PROCEDURE importbatch (p_user_id   IN VARCHAR2,
                          p_jobid     IN NUMBER,
                          p_batchid   IN INTEGER);


   -- TMS#20150310-00048 -- Added below function 1.2 to store log messages.

   PROCEDURE logmessage (
      p_text          IN VARCHAR2,
      p_log_level     IN NUMBER DEFAULT fnd_log.level_statement,
      p_module_name   IN VARCHAR2 DEFAULT NULL);

   -- Below Procedure added by Vamshi in TMS#20150817-00105 - Rel 1.3

   PROCEDURE process_xref (p_item_number         IN            VARCHAR2,
                           p_organization_id     IN            NUMBER,
                           p_upc_xref_type       IN            VARCHAR2,
                           p_upc_xref_value      IN            VARCHAR2,
                           p_upc_ref_trxn_type   IN            VARCHAR2,
                           p_upc_xref_vendor     IN            VARCHAR2,
                           x_return_status       IN OUT NOCOPY VARCHAR2);
END XXWC_DLS_CONNECTOR_PKG;
/