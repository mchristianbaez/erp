--TMS#20151009-00049

/*SYNONYMS FOR BUYER_HIERARCHY*/

CREATE SYNONYM INTERFACE_OBI.PO_CONTROL_FUNCTIONS
FOR PO.PO_CONTROL_FUNCTIONS;

CREATE SYNONYM INTERFACE_OBI.PO_CONTROL_RULES
FOR PO.PO_CONTROL_RULES;

CREATE SYNONYM INTERFACE_OBI.PER_POSITIONS
FOR APPS.PER_POSITIONS;

CREATE SYNONYM INTERFACE_OBI.PO_EMPLOYEE_HIERARCHIES_ALL
FOR PO.PO_EMPLOYEE_HIERARCHIES_ALL;

CREATE SYNONYM INTERFACE_OBI.PER_ASSIGNMENTS_V7
FOR APPS.PER_ASSIGNMENTS_V7;

CREATE SYNONYM INTERFACE_OBI.PO_POSITION_CONTROLS_ALL
FOR PO.PO_POSITION_CONTROLS_ALL;

CREATE SYNONYM INTERFACE_OBI.PO_CONTROL_GROUPS_ALL
FOR PO.PO_CONTROL_GROUPS_ALL;

/*SYNONYM Script FOR PURCHASE_COST*/

CREATE SYNONYM INTERFACE_OBI.PO_REQUISITIONS_INTERFACE_ALL
FOR PO.PO_REQUISITIONS_INTERFACE_ALL;
  
/*SYNONYM Script FOR PURCHASE_RCPT*/

CREATE SYNONYM INTERFACE_OBI.PO_ACCEPTANCES
FOR PO.PO_ACCEPTANCES;

CREATE SYNONYM INTERFACE_OBI.ECE_TP_DETAILS
FOR EC.ECE_TP_DETAILS;

CREATE SYNONYM INTERFACE_OBI.ECE_TP_HEADERS
FOR EC.ECE_TP_HEADERS;

CREATE SYNONYM INTERFACE_OBI.XXWC_PO_LINE_ADDTIONAL_ATTR_T 
FOR XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T ;


/*SYNONYM Script FOR PURCHASE_REQUISITION*/

CREATE SYNONYM INTERFACE_OBI.HR_LOCATIONS
FOR APPS.HR_LOCATIONS;

CREATE SYNONYM INTERFACE_OBI.OE_ORDER_LINES_ALL
FOR APPS.OE_ORDER_LINES_ALL;

CREATE SYNONYM INTERFACE_OBI.XXWC_WSH_SHIPPING_STG
FOR XXWC.XXWC_WSH_SHIPPING_STG;
