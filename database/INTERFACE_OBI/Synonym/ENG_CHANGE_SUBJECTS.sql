/********************************************************************************
   $Header ENG_CHANGE_SUBJECTS.sql $
   Module Name: ENG_CHANGE_SUBJECTS

   PURPOSE:  Synonym for ENG_CHANGE_SUBJECTS

   REVISIONS:
   Ver        Date        Author                  Description
   ---------  ----------  ---------------         -------------------------
   1.0        02/17/2016  Manjula Chellappan      TMS# 20160202-00288 - Create synonyms on EBS for OBIEE
********************************************************************************/
CREATE SYNONYM interface_obi.ENG_CHANGE_SUBJECTS FOR apps.ENG_CHANGE_SUBJECTS;