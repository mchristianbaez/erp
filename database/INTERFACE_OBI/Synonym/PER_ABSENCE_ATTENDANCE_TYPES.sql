CREATE OR REPLACE SYNONYM INTERFACE_OBI.PER_ABSENCE_ATTENDANCE_TYPES FOR APPS.PER_ABSENCE_ATTENDANCE_TYPES;


CREATE OR REPLACE PUBLIC SYNONYM PER_ABSENCE_ATTENDANCE_TYPES FOR HR.PER_ABSENCE_ATTENDANCE_TYPES;
