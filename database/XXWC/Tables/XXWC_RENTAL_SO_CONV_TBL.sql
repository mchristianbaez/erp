-------------------------------------------------------------
--
--   XXWC.XXWC_RENTAL_SO_CONV_TBL
--
--  Rev.   Date        Programmer         Comments
--  1.1  4/22/2013    Lucidity CG        Added SHIPPING_METHOD_CODE column  (task 20130408-03457 )
-------------------------------------------------------------------

CREATE TABLE XXWC.XXWC_RENTAL_SO_CONV_TBL
(
  ORDER_TYPE                VARCHAR2(30 BYTE),
  LEGACY_PARTY_SITE_NUMBER  VARCHAR2(50 BYTE),
  SHIP_TO_ADDRESS           VARCHAR2(240 BYTE),
  SHIP_TO_CITY              VARCHAR2(60 BYTE),
  SHIP_TO_STATE             VARCHAR2(60 BYTE),
  SHIP_TO_ZIP               VARCHAR2(60 BYTE),
  SHIP_TO_CONTACT           VARCHAR2(383 BYTE),
  SHIP_TO_CONTACT_LN        VARCHAR2(383 BYTE),
  BILL_TO_ADDRESS           VARCHAR2(240 BYTE),
  BILL_TO_CITY              VARCHAR2(60 BYTE),
  BILL_TO_STATE             VARCHAR2(60 BYTE),
  BILL_TO_ZIP               VARCHAR2(60 BYTE),
  BILL_TO_CONTACT           VARCHAR2(383 BYTE),
  BILL_TO_CONTACT_LN        VARCHAR2(383 BYTE),
  CUST_PO_NUMBER            VARCHAR2(50 BYTE),
  WAREHOUSE                 VARCHAR2(3 BYTE),
  ITEM_NUMBER               VARCHAR2(40 BYTE),
  QUANTITY                  NUMBER,
  RENTAL_CHARGE             NUMBER,
  RENTAL_START_DATE         DATE,
  PRISM_SALES_ORDER_NUM     VARCHAR2(50 BYTE),
  SHIPPING_METHOD           VARCHAR2(30),              -- rev 1.1 4/22/2013  task 20130408-03457
  CREATION_DATE             DATE,
  CREATED_BY                NUMBER,
  LAST_UPDATE_DATE          DATE,
  LAST_UPDATED_BY           NUMBER,
  STATUS                    VARCHAR2(1 BYTE),
  ERROR_MESSAGE             VARCHAR2(2000 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


