 /********************************************************************************
  FILE NAME: "XXWC"."XXWC_AR_INVOICES_CONV_EXT_TBL" 
  PROGRAM TYPE: Alter Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
-- Add/modify columns 
alter table XXWC.XXWC_AR_INVOICES_CONV_EXT_TBL add comments VARCHAR2(240);