CREATE TABLE XXWC.XXWC_ASL_CONV_STG
--**********************************************************************
--*File Name		: XXWC_ASL_CONV_STG.sql                    
--*Author		: Gopi Damuluri                                  
--*Creation Date	: 10-Feb-2013                             
--*Last Updated Date	: 10-Feb-2013                         
--*Description		: Staging Table used to load ASL information.    
--**********************************************************************
(
  ORGANIZATION_CODE      VARCHAR2(3 BYTE),
  ASL_TYPE               VARCHAR2(30 BYTE),
  ITEM_NUMBER            VARCHAR2(40 BYTE),
  ITEM_DESCRIPTION       VARCHAR2(240 BYTE),
  BUSINESS               VARCHAR2(80 BYTE),
  SUPPLIER_NUMBER        VARCHAR2(150 BYTE),
  SUPPLIER_NAME          VARCHAR2(240 BYTE),
  SUPPLIER_SITE_CODE     VARCHAR2(15 BYTE),
  ASL_STATUS             VARCHAR2(50 BYTE),
  SUPPLIER_ITEM          VARCHAR2(25 BYTE),
  BLANKET_PA_NUM         VARCHAR2(20 BYTE),
  GLOBAL                 VARCHAR2(3 BYTE),
  OWNING_ORGANIZATION    VARCHAR2(240 BYTE),
  COMMENTS               VARCHAR2(240 BYTE),
  ORGANIZATION_ID        NUMBER,
  INVENTORY_ITEM_ID      NUMBER,
  VENDOR_ID              NUMBER,
  VENDOR_SITE_ID         NUMBER,
  OWNING_ORG_ID          NUMBER,
  ASL_ID                 NUMBER,
  BPA_ID                 NUMBER,
  BPA_LINE_ID            NUMBER,
  STATUS                 VARCHAR2(1 BYTE),
  ERROR_MESSAGE          VARCHAR2(240 BYTE),
  CREATED_BY             NUMBER,
  CREATION_DATE          DATE,
  LAST_UPDATED_BY        NUMBER,
  LAST_UPDATE_DATE       DATE
  );