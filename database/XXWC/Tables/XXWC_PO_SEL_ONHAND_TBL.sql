DROP TABLE XXWC.XXWC_PO_SEL_ONHAND_TBL CASCADE CONSTRAINTS;

   /*************************************************************************
      $Header XXWC_PO_SEL_ONHAND_TBL $
      Module Name: XXWC_PO_SEL_ONHAND_TBL.SQL
   
      PURPOSE:   Table used for loading on-hand information
   
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        02/12/2015  Gopi Damuluri           TMS# 20150209-00077 Initial Version
   ****************************************************************************/
CREATE TABLE XXWC.XXWC_PO_SEL_ONHAND_TBL
(
  INVENTORY_ITEM_ID      NUMBER                 NOT NULL,
  ORGANIZATION_ID        NUMBER                 NOT NULL,
  SEGMENT1               VARCHAR2(40 BYTE),
  DESCRIPTION            VARCHAR2(240 BYTE),
  AMU                    VARCHAR2(240 BYTE),
  ORGANIZATION_CODE      VARCHAR2(3 BYTE),
  PRICING_REGION         VARCHAR2(150 BYTE),
  ON_HAND_QTY            NUMBER,
  AVAILABLE_TO_RESERVE   NUMBER,
  AVAILABLE_TO_TRANSACT  NUMBER,
  OPEN_SALES_ORDERS      NUMBER,
  AVERAGE_COST           NUMBER,
  ON_HAND_GT_90          NUMBER,
  ON_HAND_GT_180         NUMBER,
  ON_HAND_GT_270         NUMBER,
  LAST_RECEIPT_DATE      DATE,
  MIN                    NUMBER,
  MAX                    NUMBER,
  primary_unit_of_measure  VARCHAR2(25)
);
