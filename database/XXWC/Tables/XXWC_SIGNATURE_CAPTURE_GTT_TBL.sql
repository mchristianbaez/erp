  /*********************************************************************************
  -- Table Name XXWC_SIGNATURE_CAPTURE_GTT_TBL
  -- *******************************************************************************
  --
  -- PURPOSE: To process Will Call orders with Signature Capture
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0    04-Apr-2015   Gopi Damuluri    TMS# 20150226-00084 - Peformance Tuning of SHIP_CONFIRM_SIG_CAPT process
  *******************************************************************************/

DROP TABLE XXWC.XXWC_SIGNATURE_CAPTURE_GTT_TBL CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_SIGNATURE_CAPTURE_GTT_TBL
(
  ID                      NUMBER,
  SIGNATURE_IMAGE_CLOB    CLOB,
  SIGNATURE_IMAGE_BLOB    BLOB,
  CREATION_DATE           DATE,
  CREATED_BY              NUMBER,
  HEADER_ID               NUMBER,
  SIGNATURE_NAME          VARCHAR2(60 BYTE),
  REQUEST_ID              NUMBER,
  DELIVERY_ID             NUMBER,
  RIGHT_FAX               VARCHAR2(1 BYTE),
  FAX                     VARCHAR2(100 BYTE),
  FAX_COMMENT             VARCHAR2(240 BYTE),
  GROUP_ID                NUMBER,
  SHIP_CONFIRM_STATUS     VARCHAR2(10 BYTE),
  SHIP_CONFIRM_EXCEPTION  VARCHAR2(240 BYTE),
  LAST_UPDATE_DATE        DATE,
  LAST_UPDATED_BY         NUMBER
)
ON COMMIT PRESERVE ROWS
NOCACHE;