DROP TABLE XXWC.XXWC_INACTIVE_USER_LOAD_TBL CASCADE CONSTRAINTS;
/*******************************************************************************************************
  -- Table Name XXWC_INACTIVE_USER_LOAD_TBL
  -- ***************************************************************************************************
  --
  -- PURPOSE: External table used to store user detail which need to be inactivated
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     23-Jun-2016   Niraj K Ranjan   Initial Version
********************************************************************************************************/
CREATE TABLE XXWC.XXWC_INACTIVE_USER_LOAD_TBL
   ( USER_NAME  VARCHAR2(100 BYTE),
     FIRST_NAME VARCHAR2(150 BYTE),
     LAST_NAME  VARCHAR2(150 BYTE)
   )
   ORGANIZATION EXTERNAL
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AD_INACTIVE_USERS"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE
        FIELDS TERMINATED BY '|'
      )
      LOCATION
       ( 'wc_contractors.txt'
       )
    )
   REJECT LIMIT UNLIMITED ;
