/* Formatted on 11/28/2012 6:09:15 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_ACCOUNT_MASS_UPDATE_ARCH
-- Generated 11/28/2012 6:09:07 PM from XXWC@EBIZDEV

CREATE TABLE xxwc.xxwc_account_mass_update_arch
(
    table_rowid         VARCHAR2 (128 BYTE)
   ,table_name          VARCHAR2 (64 BYTE)
   ,run_id              NUMBER
   ,change_name         VARCHAR2 (64 BYTE)
   ,old_name            VARCHAR2 (128 BYTE)
   ,new_name            VARCHAR2 (128 BYTE)
   ,old_id              NUMBER
   ,new_id              NUMBER
   ,last_updated_by     NUMBER
   ,last_update_date    DATE
   ,responsibility_id   NUMBER
   ,org_id              NUMBER
   ,error_message       CLOB
   ,insert_date         DATE
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
LOB ("ERROR_MESSAGE") STORE AS sys_lob0000799024c00013$$
    (TABLESPACE xxwc_data
     STORAGE (INITIAL 65536
              NEXT 1048576
              MINEXTENTS 1
              MAXEXTENTS 2147483645)
     NOCACHE LOGGING
     CHUNK 8192)
PARALLEL (DEGREE 8)
NOLOGGING
/

-- End of DDL Script for Table XXWC.XXWC_ACCOUNT_MASS_UPDATE_ARCH
