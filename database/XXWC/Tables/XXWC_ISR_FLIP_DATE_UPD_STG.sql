/*************************************************************************
  $Header XXWC_ISR_FLIP_DATE_UPD_STG.sql $
  Module Name: XXWC_ISR_FLIP_DATE_UPD_STG

  PURPOSE: Table to maintain the Flip date for updating the ISR Table

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        08-Oct-2015  Manjula Chellappan    Initial Version TMS # 20151008-00048

**************************************************************************/ 
 CREATE TABLE XXWC.XXWC_ISR_FLIP_DATE_UPD_STG
  (
    ORG         	    VARCHAR2(10) NOT NULL ,
    ITEM_NUMBER       	VARCHAR2(30) NOT NULL,
	FLIP_DATE           DATE NOT NULL,
    CREATION_DATE       DATE NOT NULL    
  );  
	
    CREATE UNIQUE INDEX XXWC.XXWC_ISR_FLIP_DATE_UPD_STG_U1 ON XXWC.XXWC_ISR_FLIP_DATE_UPD_STG(org,item_number) ;
       
	GRANT ALL ON XXWC.XXWC_ISR_FLIP_DATE_UPD_STG TO RT008057;
	GRANT ALL ON XXWC.XXWC_ISR_FLIP_DATE_UPD_STG TO MC019514;