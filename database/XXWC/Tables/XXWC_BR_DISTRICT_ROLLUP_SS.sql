CREATE TABLE xxwc.XXWC_BR_DISTRICT_ROLLUP_SS
(
   DIST_HIER_ID       NUMBER NOT NULL,
   SS_DATE            DATE NOT NULL,
   DIST_CODE          VARCHAR2 (10),
   MGT_LVL            NUMBER,
   MGT_DESC           VARCHAR2 (80),
   MGR_EEID           NUMBER,
   MGR_NAME           VARCHAR2 (80),
   START_DATE         DATE,
   END_DATE           DATE,
   LAST_UPDATE_DATE   DATE,
   LAST_UPDATE_USER   VARCHAR2 (30)
)
/

CREATE SYNONYM apps.XXWC_BR_DISTRICT_ROLLUP_SS FOR xxwc.XXWC_BR_DISTRICT_ROLLUP_SS
/

GRANT ALL ON xxwc.XXWC_BR_DISTRICT_ROLLUP_SS TO interface_dstage;
/

GRANT ALL ON xxwc.XXWC_BR_DISTRICT_ROLLUP_SS TO interface_apexwc;
/