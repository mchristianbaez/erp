/******************************************************************************
      $Header XXWC_EDI_IB_FILES_EXTRNL_TBL.sql $
      Module Name: XXWC_EDI_IB_FILES_EXTRNL_TBL.sql

      PURPOSE:   This XXWC_EDI_IB_FILES_EXTRNL_TBL table is used to load the
	             Customer Integration - Order Import Modifications for ecomm

      REVISIONS:
      Ver        Date         Author              Description 
      ---------  ----------   ---------------     --------------------------
      1.0        12-04-2018  Pattabhi Avula       TMS#20180308-00104
	                                              Initial version
*******************************************************************************/
CREATE TABLE XXWC.XXWC_EDI_IB_FILES_EXTRNL_TBL
(
  FILE_PERMISSIONS  VARCHAR2(50 BYTE),
  FILE_TYPE         VARCHAR2(64 BYTE),
  FILE_OWNER        VARCHAR2(64 BYTE),
  FILE_GROUP        VARCHAR2(64 BYTE),
  FILE_SIZE         VARCHAR2(64 BYTE),
  FILE_MONTH        VARCHAR2(64 BYTE),
  FILE_DAY          VARCHAR2(64 BYTE),
  FILE_TIME         VARCHAR2(64 BYTE),
  FILE_NAME         VARCHAR2(612 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY EDI_INBOUND
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE

    PREPROCESSOR EDI_INBOUND: 'XXWC_EDI_IB_FILES_DTLS.sh'
    NOBADFILE
    NODISCARDFILE
    NOLOGFILE

    FIELDS TERMINATED BY WHITESPACE
           )
     LOCATION (EDI_INBOUND:'list_files_dummy_source.txt')
  )
REJECT LIMIT UNLIMITED;
/