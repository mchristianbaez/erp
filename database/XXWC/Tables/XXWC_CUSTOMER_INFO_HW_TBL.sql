  /********************************************************************************
  FILE NAME: XXWC_CUSTOMER_INFO_HW_TBL.sql
  
  PROGRAM TYPE: Customer Info table for online sales order form
  
  PURPOSE: Customer Info table for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  *******************************************************************************************/
CREATE TABLE XXWC.XXWC_CUSTOMER_INFO_HW_TBL
(
  CUSTOMER_NAME        VARCHAR2(360 BYTE),
  CUST_ACCOUNT_ID      NUMBER(15)               NOT NULL,
  PARTY_ID             NUMBER(15)               NOT NULL,
  CUST_ACCOUNT_CODE    VARCHAR2(30 BYTE)        NOT NULL,
  MAIN_PHONE_NUMBER    VARCHAR2(200 BYTE),
  MAIN_EMAIL           VARCHAR2(3000 BYTE),
  CUST_ACCOUNT_STATUS  VARCHAR2(100 BYTE)       NOT NULL,
  SALES_REP_ID         NUMBER(15), -- New
  SALES_REP_NAME       VARCHAR2(2000 BYTE) ,       
  payment_term_id      NUMBER(15), -- New
  payment_term_name    VARCHAR2(2000 BYTE), -- New 
  AUTH_BUYER_REQUIRED  VARCHAR2(100 BYTE), -- New (--Auth Buyer required)
  CREATION_DATE        DATE                     NOT NULL,
  CREATED_BY           NUMBER(15)               NOT NULL,
  LAST_UPDATE_DATE     DATE                     NOT NULL,
  LAST_UPDATED_BY      NUMBER(15)               NOT NULL,
  REQUEST_ID           NUMBER(15),
  ATTRIBUTE1           VARCHAR2(150 BYTE),
  ATTRIBUTE2           VARCHAR2(150 BYTE),
  ATTRIBUTE3           VARCHAR2(150 BYTE),
  ATTRIBUTE4           VARCHAR2(150 BYTE),
  ATTRIBUTE5           VARCHAR2(150 BYTE),
  ATTRIBUTE6           VARCHAR2(150 BYTE),
  ATTRIBUTE7           VARCHAR2(150 BYTE),
  ATTRIBUTE8           VARCHAR2(150 BYTE),
  ATTRIBUTE9           VARCHAR2(150 BYTE),
  ATTRIBUTE10          VARCHAR2(150 BYTE)
);

GRANT SELECT ON XXWC.XXWC_CUSTOMER_INFO_HW_TBL TO INTERFACE_MSSQL;