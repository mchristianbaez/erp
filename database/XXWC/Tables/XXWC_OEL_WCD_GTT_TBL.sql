CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_OEL_WCD_GTT_TBL
(
  HEADER_ID             NUMBER                  NOT NULL,
  LINE_CATEGORY_CODE    VARCHAR2(30 BYTE)       NOT NULL,
  ORDERED_QUANTITY      NUMBER,
  UNIT_SELLING_PRICE    NUMBER,
  TAX_VALUE             NUMBER,
  LINE_ID               NUMBER                  NOT NULL,
  ACTUAL_SHIPMENT_DATE  DATE,
  SHIPPING_METHOD_CODE  VARCHAR2(30 BYTE),
  SHIP_FROM_ORG_ID      NUMBER
)
ON COMMIT PRESERVE ROWS;

GRANT ALL ON xxwc.xxwc_oel_wcd_gtt_tbl TO XXEIS;

CREATE INDEX xxwc.xxwc_oel_wcd_gtt_tbl_n1 ON xxwc.xxwc_oel_wcd_gtt_tbl(header_id, ship_from_org_id, shipping_method_code);

-- GRANT ALL ON xxwc.xxwc_oel_wcd_gtt_tbl_n1 TO XXEIS;