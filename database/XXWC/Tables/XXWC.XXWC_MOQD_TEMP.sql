CREATE GLOBAL TEMPORARY TABLE xxwc.xxwc_moqd_temp
  (inventory_item_id NUMBER
  ,organization_Id   NUMBER
  ,update_transaction_id NUMBER
  ,create_transaction_id NUMBER
  ,primary_transaction_quantity NUMBER
  ,onhand_quantities_id NUMBER
  ,orig_date_received DATE
  ,creation_date  DATE
  ,moqd_rowid VARCHAR2(100)
  )
ON COMMIT PRESERVE ROWS;