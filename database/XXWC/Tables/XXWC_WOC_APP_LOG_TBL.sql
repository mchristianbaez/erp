/*************************************************************************
      Table Name: XXWC_WOC_APP_LOG_TBL

      PURPOSE:   This is to create table

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        01/16/2018    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
   ****************************************************************************/
 DROP TABLE XXWC.XXWC_WOC_APP_LOG_TBL;
 CREATE TABLE XXWC.XXWC_WOC_APP_LOG_TBL
   (SEQUENCE_ID NUMBER, 
	API_NAME VARCHAR2(100 BYTE), 
	LOG_TIME DATE, 
	BRANCH_ID VARCHAR2(100),
	ORDER_HEADER_ID NUMBER, 
	ORDER_NUMBER NUMBER, 
	ITEM_ID NUMBER,
    ITEM_NUMBER VARCHAR2(2000),	
	JOB_SITE_ID NUMBER, 
	CUSTOMER_ID NUMBER, 
	CUSTOMER_NUMBER VARCHAR2(1000),
	SEARCH_STRING VARCHAR2(1000),
	DEBUG_TEXT VARCHAR2(4000 BYTE),
	CREATION_DATE DATE,
	CREATED_BY NUMBER,
	UPDATE_DATE DATE,
	UPDATED_BY NUMBER
   );
 
  CREATE INDEX "XXWC"."XXWC_WOC_APP_LOG_TBL_N1" ON "XXWC"."XXWC_WOC_APP_LOG_TBL" ("SEQUENCE_ID");
 