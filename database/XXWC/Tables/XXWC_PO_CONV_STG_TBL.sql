CREATE TABLE XXWC.XXWC_PO_CONV_STG_TBL
--**********************************************************************
--*File Name		: XXWC_PO_CONV_STG_TBL.sql                    
--*Author		: Gopi Damuluri                                  
--*Creation Date	: 10-Feb-2013                             
--*Last Updated Date	: 10-Feb-2013                         
--*Description		: Staging Table used to load ASL PO information.    
--**********************************************************************
( ORGANIZATION_CODE    VARCHAR2(3)
, VENDOR_NAME          VARCHAR2(50)
, VENDOR_NUMBER        VARCHAR2(50)
, VENDOR_SITE_CODE     VARCHAR2(50)
, PARTNUMBER           VARCHAR2(50)
, ONHAND_QTY           NUMBER
, AVG_COST             NUMBER
, STATUS               VARCHAR2(1)
, ERROR_MESSAGE        VARCHAR2(200)
, CREATION_DATE        DATE
, CREATED_BY           NUMBER
, LAST_UPDATE_DATE     DATE
, LAST_UPDATED_BY      NUMBER
);