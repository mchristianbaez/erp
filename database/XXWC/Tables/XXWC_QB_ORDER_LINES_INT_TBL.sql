CREATE TABLE XXWC.XXWC_QB_ORDER_LINES_INT_TBL (QUOTE_NUMBER		NUMBER
                                             , ECOMM_ORDER_ID           NUMBER
                                             , LINE_NUMBER		NUMBER
                                             , ITEM_NUMBER		VARCHAR2(40)
                                             , UNIT_OF_MEASURE		VARCHAR2(25)
                                             , ORDERED_QUANTITY		NUMBER
                                             , UNIT_SELLING_PRICE	NUMBER
                                             , CREATED_BY		NUMBER
                                             , CREATION_DATE		DATE
                                             , LAST_UPDATED_BY		NUMBER
                                             , LAST_UPDATE_DATE		DATE
                                             , PROCESS_FLAG             VARCHAR2(1)
                                             , ERROR_MESSAGE            VARCHAR2(2000)
                                             , ATTRIBUTE1		VARCHAR2(250)
                                             , ATTRIBUTE2		VARCHAR2(250)
                                             , ATTRIBUTE3		VARCHAR2(250)
                                             , ATTRIBUTE4		VARCHAR2(250)
                                             , ATTRIBUTE5		VARCHAR2(250));

GRANT ALL ON xxwc.xxwc_qb_order_lines_int_tbl TO interface_wcs;
