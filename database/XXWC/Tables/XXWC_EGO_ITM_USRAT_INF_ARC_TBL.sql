/**********************************************************************************************
-- Table XXWC_EGO_ITM_USRAT_INF_ARC_TBL
-- ********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     05-Feb-2015   P.Vamshidhar    Created this table to archive data before deleting
--                                       from standard interface table.
--                                       TMS: 20141120-00077
-- 2.0     12-Feb-2015   P.vamshidhar    Added insert_date column 
-- 3.0     17-Feb-2015   P.vamshidhar    script with column names.
*/

CREATE TABLE XXWC.XXWC_EGO_ITM_USRAT_INF_ARC_TBL
(
  TRANSACTION_ID             NUMBER(15),
  PROCESS_STATUS             NUMBER,
  DATA_SET_ID                NUMBER(15)         NOT NULL,
  ORGANIZATION_CODE          VARCHAR2(3 BYTE),
  ITEM_NUMBER                VARCHAR2(81 BYTE),
  REVISION                   VARCHAR2(3 BYTE),
  ROW_IDENTIFIER             NUMBER(38)         NOT NULL,
  ATTR_GROUP_INT_NAME        VARCHAR2(30 BYTE)  NOT NULL,
  ATTR_INT_NAME              VARCHAR2(30 BYTE)  NOT NULL,
  ATTR_VALUE_STR             VARCHAR2(1000 BYTE),
  ATTR_VALUE_NUM             NUMBER,
  ATTR_VALUE_DATE            DATE,
  ATTR_DISP_VALUE            VARCHAR2(1000 BYTE),
  TRANSACTION_TYPE           VARCHAR2(10 BYTE),
  ORGANIZATION_ID            NUMBER(15),
  INVENTORY_ITEM_ID          NUMBER(15),
  ITEM_CATALOG_GROUP_ID      NUMBER(15),
  REVISION_ID                NUMBER(15),
  ATTR_GROUP_ID              NUMBER(15),
  REQUEST_ID                 NUMBER(15),
  PROGRAM_APPLICATION_ID     NUMBER(15),
  PROGRAM_ID                 NUMBER(15),
  PROGRAM_UPDATE_DATE        DATE,
  CREATED_BY                 NUMBER(15),
  CREATION_DATE              DATE,
  LAST_UPDATED_BY            NUMBER(15),
  LAST_UPDATE_DATE           DATE,
  LAST_UPDATE_LOGIN          NUMBER(15),
  ATTR_GROUP_TYPE            VARCHAR2(30 BYTE),
  CHANGE_ID                  NUMBER,
  CHANGE_LINE_ID             NUMBER,
  SOURCE_SYSTEM_ID           NUMBER,
  SOURCE_SYSTEM_REFERENCE    VARCHAR2(255 BYTE),
  ATTR_UOM_DISP_VALUE        VARCHAR2(25 BYTE),
  ATTR_VALUE_UOM             VARCHAR2(3 BYTE),
  INTERFACE_TABLE_UNIQUE_ID  NUMBER,
  PROG_INT_CHAR1             VARCHAR2(255 BYTE),
  PROG_INT_CHAR2             VARCHAR2(255 BYTE),
  PROG_INT_NUM1              NUMBER,
  PROG_INT_NUM2              NUMBER,
  PROG_INT_NUM3              NUMBER,
  PROG_INT_NUM4              NUMBER,
  DATA_LEVEL_ID              NUMBER,
  DATA_LEVEL_NAME            VARCHAR2(30 BYTE),
  PK1_VALUE                  NUMBER,
  PK2_VALUE                  NUMBER,
  PK3_VALUE                  NUMBER,
  PK4_VALUE                  NUMBER,
  PK5_VALUE                  NUMBER,
  USER_DATA_LEVEL_NAME       VARCHAR2(240 BYTE),
  BUNDLE_ID                  NUMBER,
  INSERT_DATE                DATE
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
/
GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_EGO_ITM_USRAT_INF_ARC_TBL TO APPS
/

