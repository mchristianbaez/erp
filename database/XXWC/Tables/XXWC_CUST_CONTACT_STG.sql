CREATE TABLE XXWC.XXWC_CUST_CONTACT_STG
(
  CUSTOMER_NAME    VARCHAR2(240 BYTE)               NULL,
  ACCOUNT_NUMBER   VARCHAR2(30 BYTE)                NULL,
  FIRST_NAME       VARCHAR2(240 BYTE)               NULL,
  LAST_NAME        VARCHAR2(240 BYTE)               NULL,
  PHONE_NUMBER     VARCHAR2(240 BYTE)               NULL,
  EMAIL_ADDRESS    VARCHAR2(240 BYTE)               NULL,
  CUST_ACCOUNT_ID  NUMBER                           NULL,
  PARTY_ID         NUMBER                           NULL,
  CONTACT_ID       NUMBER                           NULL,
  STATUS           VARCHAR2(100 BYTE)               NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


