  /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_SUPPLIER_INT.sql

  PROGRAM TYPE: Alter Table script

  PURPOSE: Conversion purpose in APEX application

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/08/2018    Nancy Pahwa     TMS#  -- Initial Version
  ********************************************************************************/
ALTER TABLE XXWC.XXWC_AP_SUPPLIER_INT ADD(AHH_VENDOR_ID         NUMBER,	
                                          CATEGORY_SBA_OWNER    VARCHAR2(20),
										  TAX_EXEMPT_FLAG       VARCHAR2(10),
										  SUPPLIER_STATUS       VARCHAR2(20),
										  AHH_VENDOR_NUMBER     VARCHAR2(30));
/										  