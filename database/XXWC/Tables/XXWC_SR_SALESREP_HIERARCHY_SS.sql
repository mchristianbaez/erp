CREATE TABLE xxwc.XXWC_SR_SALESREP_HIERARCHY_SS
(
   SR_HIER_ID         NUMBER NOT NULL,
   SS_DATE            DATE NOT NULL,
   SR_ID              NUMBER,
   MGT_LVL            NUMBER,
   MGT_DESC           VARCHAR2 (80),
   MGR_EEID           NUMBER,
   MGR_NAME           VARCHAR2 (360),
   START_DATE         DATE,
   END_DATE           DATE,
   LAST_UPDATE_DATE   DATE,
   LAST_UPDATE_USER   VARCHAR2 (30)
)
/

CREATE SYNONYM apps.XXWC_SR_SALESREP_HIERARCHY_SS FOR xxwc.XXWC_SR_SALESREP_HIERARCHY_SS
/

GRANT ALL ON xxwc.xxwc_sr_salesrep_hierarchy_ss TO interface_dstage;
/

GRANT ALL ON xxwc.xxwc_sr_salesrep_hierarchy_ss TO interface_apexwc;
/