/*******************************************************************************************************
  -- Table Name XXWC_AHH_OE_AIS_LPP_TBL
  -- ***************************************************************************************************
  --
  -- PURPOSE: table used to store Last Price Paid details
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     21-Sep-2018   Niraj K Ranjan   TMS#20180926-00003   AHH LLP price upload
********************************************************************************************************/
CREATE TABLE XXWC.XXWC_AHH_OE_AIS_LPP_TBL
(
  INVENTORY_ITEM_ID          NUMBER,
  ORCL_PART_NBR              VARCHAR2(40),  
  ORCL_CUSTOMER_NBR          VARCHAR2(30),
  ORCL_CUSTOMER_SITE_NBR     VARCHAR2(30), 
  SHIP_TO_ORG_ID             NUMBER, 
  CUST_ACCOUNT_ID            NUMBER,
  AHH_CUSTOMER_NBR           VARCHAR2(100), 
  AHH_SITE_NBR               VARCHAR2(100), 
  AHH_PART_NBR               VARCHAR2(500), 
  LAST_PRICE_PAID            NUMBER, 
  LINE_CREATION_DATE         DATE,
  LAST_PRICE_PAID_EXCL_FLAG  VARCHAR2(1),
  START_DATE_ACTIVE          DATE,
  END_DATE_ACTIVE            DATE,
  REQUEST_ID                 NUMBER,
  CREATED_BY                 NUMBER,
  LAST_UPDATED_BY            NUMBER,
  CREATION_DATE              DATE,
  LAST_UPDATE_DATE           DATE,
  LAST_UPDATE_LOGIN          NUMBER
);