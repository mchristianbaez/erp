--
-- XXWC_OM_CSP_HEADER_ARCHIVE  (Table) 
--
CREATE TABLE XXWC.XXWC_OM_CSP_HEADER_ARCHIVE
(
  AGREEMENT_ID           NUMBER                 NOT NULL,
  PRICE_TYPE             VARCHAR2(30 BYTE)      NOT NULL,
  CUSTOMER_ID            NUMBER                     NULL,
  CUSTOMER_SITE_ID       NUMBER                     NULL,
  AGREEMENT_STATUS       VARCHAR2(30 BYTE)      NOT NULL,
  REVISION_NUMBER        NUMBER                     NULL,
  ORGANIZATION_ID        NUMBER                     NULL,
  GROSS_MARGIN           NUMBER                     NULL,
  INCOMPATABILITY_GROUP  VARCHAR2(30 BYTE)      NOT NULL,
  ATTRIBUTE_CATEGORY     VARCHAR2(30 BYTE)          NULL,
  ATTRIBUTE1             VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE2             VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE3             VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE4             VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE5             VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE6             VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE7             VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE8             VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE9             VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE10            VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE11            VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE12            VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE13            VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE14            VARCHAR2(150 BYTE)         NULL,
  ATTRIBUTE15            VARCHAR2(150 BYTE)         NULL,
  CREATION_DATE          DATE                   NOT NULL,
  CREATED_BY             NUMBER                 NOT NULL,
  LAST_UPDATE_DATE       DATE                   NOT NULL,
  LAST_UPDATED_BY        NUMBER                 NOT NULL,
  LAST_UPDATE_LOGIN      NUMBER                 NOT NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


