   /*************************************************************************
    *  Copyright (c) 2016 HD Supply
    *  All rights reserved.
    **************************************************************************
    *   $Header XXWC_RA_INTEFACE_LINES_TEMP.sql $
    *   Module Name: XXWC_RA_INTEFACE_LINES_TEMP.sql
    *
    *   PURPOSE:   This table is to capture interface lin eid that need error to be fixed in
    *               rainterface lines all before running Autoinvoicing.
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
    *   2.0        04/20/2016  Neha Saini             Initial Version TMS# 20150106-00014
    * ***************************************************************************/
DROP TABLE XXWC.XXWC_RA_INTEFACE_LINES_TEMP CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_RA_INTEFACE_LINES_TEMP
(
  INTERFACE_LINE_ID             NUMBER(15),
  INTERFACE_LINE_ATTRIBUTE1     VARCHAR2(240 BYTE),
  TRX_NUMBER                    VARCHAR2(20 BYTE),
  REASON_CODE                   VARCHAR2(30 BYTE),
  PAYMENT_SET_ID                NUMBER(15),
  TRX_DATE                      DATE,
  PRIMARY_SALESREP_ID           NUMBER(15),
  ORIG_SYSTEM_BILL_ADDRESS_ID   VARCHAR2(240 BYTE),
  ORIG_SYSTEM_BILL_CONTACT_ID   VARCHAR2(240 BYTE),
  ORIG_SYSTEM_BILL_CUSTOMER_ID  VARCHAR2(240 BYTE),
  ORIG_SYSTEM_SOLD_CUSTOMER_ID  VARCHAR2(240 BYTE),
  ORIG_SYSTEM_SHIP_CONTACT_ID   VARCHAR2(240 BYTE),
  ORIG_SYSTEM_SHIP_ADDRESS_ID   VARCHAR2(240 BYTE),
  ORIG_SYSTEM_SHIP_CUSTOMER_ID  VARCHAR2(240 BYTE),
  ERROR                         NUMBER,
  CREATION_DATE                 DATE,
  CREATED_BY                    NUMBER,
  STATUS                        VARCHAR2(20 BYTE)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
