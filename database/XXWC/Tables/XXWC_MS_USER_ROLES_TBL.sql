create table XXWC.XXWC_MS_USER_ROLES_TBL
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_USER_ROLES_TBL $
  Module Name: XXWC_MS_USER_ROLES_TBL

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
(
  user_role_id NUMBER not null,
  user_id      VARCHAR2(50),
  role_id      NUMBER,
  app_id       NUMBER,
  owner_flag   VARCHAR2(1),
  created_by   VARCHAR2(50),
  created_on   DATE,
  updated_by   VARCHAR2(50),
  updated_on   DATE
)
tablespace XXWC_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );