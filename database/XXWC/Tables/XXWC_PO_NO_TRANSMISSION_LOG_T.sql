/*************************************************************************
  $Header XXWC_PO_NO_TRANSMISSION_LOG_T.sql $
  Module Name: XXWC_PO_NO_TRANSMISSION_LOG_T

  PURPOSE: Table to maintain PO no transmission log.
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.0        13-Sep-2017  P.Vamshidhar         TMS#20170207-00206 - Research the issue where users are able to enter
                                               invalid email address and Fax numbers while app
*******************************************************************************************************************************************/ 
CREATE TABLE XXWC.XXWC_PO_NO_TRANSMISSION_LOG_T
(
  ITEM_KEY       VARCHAR2(100 BYTE),
  USER_ID        NUMBER,
  RESP_ID        NUMBER,
  RESP_APPL_ID   NUMBER,
  CREATION_DATE  DATE
);
/