-- ******************************************************************************************************
-- *   $Header XXWC.XXWC_AR_RECEIPTS_CONV.sql $
-- *   Module Name: XXWC.XXWC_AR_RECEIPTS_CONV
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXWC_AR_RECEIPTS_CONV
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------
-- *   1.0        10/11/2011  Vivek Lakaman             Initial Version
-- *   1.1        03/04/2018  Ashwin Sridhar       Added for TMS#20180319-00242
-- * *****************************************************************************************************
DROP TABLE XXWC.XXWC_AR_RECEIPTS_CONV CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_AR_RECEIPTS_CONV
(
  RECEIPT_NUMBER        VARCHAR2(50 BYTE)       NOT NULL,
  RECEIPT_METHOD        VARCHAR2(50 BYTE),
  RECEIPT_DATE          DATE,
  RECEIPT_AMOUNT        NUMBER,
  CURRENCY_CODE         VARCHAR2(10 BYTE),
  LEGACY_CUSTOMER_NUM   VARCHAR2(20 BYTE),
  LEGACY_CUSTOMER_SITE  VARCHAR2(30 BYTE),
  BANK_BRANCH_NAME      VARCHAR2(50 BYTE),
  BANK_ACCOUNT_NUMBER   VARCHAR2(50 BYTE),
  COMMENTS              VARCHAR2(1000 BYTE),
  GL_DATE               DATE,
  CREATION_DATE         DATE                    NOT NULL,
  CREATED_BY            NUMBER                  NOT NULL,
  LAST_UPDATED_BY       NUMBER                  NOT NULL,
  LAST_UPDATE_DATE      DATE                    NOT NULL,
  LAST_UPDATE_LOGIN     NUMBER,
  PROCESS_STATUS        VARCHAR2(1 BYTE),
  ERROR_MESSAGE         VARCHAR2(2000 BYTE),
  RECEIPT_REFERENCE     VARCHAR2(100 BYTE),
  DISPUTE_CODE          VARCHAR2(100 BYTE),
  REQUEST_ID            NUMBER,
  ORG_ID                NUMBER                  DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),162)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE OR REPLACE SYNONYM APPS.XXWC_AR_RECEIPTS_CONV FOR XXWC.XXWC_AR_RECEIPTS_CONV;


GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_RECEIPTS_CONV TO EA_APEX;
