CREATE TABLE XXWC.XXWC_TRX_TMP
(
  TRX_NUMBER   VARCHAR2(25 BYTE),
  ATTRIBUTE12  VARCHAR2(50 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


