  /*******************************************************************************
  Table: XXWC.XXWC_B2B_POD_SO_INFO_TBL
  Description: This table is used to maintain B2B POD information of Sales Orders
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-Dec-2015        Gopi Damuluri   TMS# 20160120-00169 - B2B POD Enhancement
  ********************************************************************************/
DROP TABLE XXWC.XXWC_B2B_POD_SO_INFO_TBL;
/

CREATE TABLE XXWC.XXWC_B2B_POD_SO_INFO_TBL
(
  HEADER_ID         NUMBER,
  ORDER_NUMBER      NUMBER,
  DELIVERY_ID       NUMBER,
  FILE_NAME         VARCHAR2(200 BYTE),
  POD_DATE          DATE,
  CUST_ACCOUNT_ID   NUMBER,
  PARTY_ID          NUMBER,
  CREATION_DATE     DATE,
  CREATED_BY        NUMBER,
  LAST_UPDATE_DATE  DATE,
  LAST_UPDATED_BY   NUMBER,
  STATUS            VARCHAR2(20 BYTE),
  SITE_USE_ID       NUMBER
);