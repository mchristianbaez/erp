CREATE TABLE XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL
/*************************************************************************
 $Header XXWC_MD_SEARCH_PRODUCTS_TBL $
 Module Name: XXWC_MD_SEARCH_PRODUCTS_TBL.sql

 PURPOSE:   This table is used to backup AIS MVs: XXWC_MD_SEARCH_PRODUCTS_MV

 REVISIONS:
 Ver        Date        Author           Description
 ---------  ----------  ---------------  -------------------------
 1.0        06/28/2016  Gopi Damuluri    Initial Version TMS# 20160628-00001
**************************************************************************/
(
  INVENTORY_ITEM_ID       NUMBER,
  ORGANIZATION_ID         NUMBER,
  PARTNUMBER              VARCHAR2(40 BYTE),
  TYPE                    CHAR(7 BYTE),
  MANUFACTURERPARTNUMBER  VARCHAR2(150 BYTE),
  MANUFACTURER            VARCHAR2(150 BYTE),
  SEQUENCE                NUMBER,
  CURRENCYCODE            CHAR(3 BYTE),
  NAME                    VARCHAR2(150 BYTE),
  SHORTDESCRIPTION        VARCHAR2(240 BYTE),
  LONGDESCRIPTION         VARCHAR2(1000 BYTE),
  THUMBNAIL               VARCHAR2(4000 BYTE),
  FULLIMAGE               VARCHAR2(4000 BYTE),
  QUANTITYMEASURE         CHAR(3 BYTE),
  WEIGHTMEASURE           CHAR(3 BYTE),
  WEIGHT                  NUMBER,
  BUYABLE                 CHAR(1 BYTE),
  KEYWORD                 VARCHAR2(1000 BYTE),
  CREATION_DATE           DATE,
  ITEM_TYPE               VARCHAR2(30 BYTE),
  CROSS_REFERENCE         VARCHAR2(4000 BYTE),
  DUMMY                   CHAR(1 BYTE),
  PRIMARY_UOM_CODE        VARCHAR2(3 BYTE)
);
/