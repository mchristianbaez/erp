--
-- XXWC_QP_WGHT_NTL_AVG_COST_STG  (Table) 
--
CREATE TABLE XXWC.XXWC_QP_WGHT_NTL_AVG_COST_STG
(
  INVENTORY_ITEM_ID  NUMBER                         NULL,
  WGHT_NTL_AVG_COST  NUMBER                         NULL,
  LIST_HEADER_ID     NUMBER                         NULL,
  CREATION_DATE      DATE                           NULL,
  CREATED_BY         NUMBER                         NULL,
  LAST_UPDATE_DATE   DATE                           NULL,
  LAST_UPDATED_BY    NUMBER                         NULL,
  LAST_UPDATE_LOGIN  NUMBER                         NULL,
  REQUEST_ID         NUMBER                         NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


--
-- XXWC_QP_WGT_NTL_AVG_CST_STG_N1  (Index) 
--
CREATE UNIQUE INDEX XXWC.XXWC_QP_WGT_NTL_AVG_CST_STG_N1 ON XXWC.XXWC_QP_WGHT_NTL_AVG_COST_STG
(INVENTORY_ITEM_ID)
NOLOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


