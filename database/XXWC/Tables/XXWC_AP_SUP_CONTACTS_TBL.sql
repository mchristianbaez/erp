/*************************************************************************
  $Header XXWC_AP_SUP_CONTACTS_TBL $
  Module Name: XXWC_AP_SUP_CONTACTS_TBL

  PURPOSE: Table to store the Supplier Contacts information

  TMS Task Id :  20141211-00121

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        28-Jan-2015  Manjula Chellappan    Initial Version
**************************************************************************/
CREATE TABLE XXWC.XXWC_AP_SUP_CONTACTS_TBL
(
   SUPPLIER_CONTACT_ID   NUMBER UNIQUE NOT NULL,
   SUPPLIER_ID           NUMBER NOT NULL ,
   CONTACT_TYPE          VARCHAR2 (150) NOT NULL ,
   FIRST_NAME            VARCHAR2 (150) NOT NULL ,
   LAST_NAME             VARCHAR2 (150) NOT NULL ,
   TITLE                 VARCHAR2 (60),
   OFFICE_PHONE          VARCHAR2 (20),
   CELL_PHONE            VARCHAR2 (20),
   EMAIL                 VARCHAR2 (240),
   ORG_ID                NUMBER  DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),-99) NOT NULL,
   CREATED_BY            NUMBER NOT NULL ,
   CREATION_DATE         DATE NOT NULL ,
   LAST_UPDATED_BY       NUMBER NOT NULL ,
   LAST_UPDATE_DATE      DATE NOT NULL ,
   LAST_UPDATE_LOGIN     NUMBER NOT NULL ,
   ATTRIBUTE1            VARCHAR2 (240),
   ATTRIBUTE2            VARCHAR2 (240),
   ATTRIBUTE3            VARCHAR2 (240),
   ATTRIBUTE4            VARCHAR2 (240),
   ATTRIBUTE5            VARCHAR2 (240),
   ATTRIBUTE6            VARCHAR2 (240),
   ATTRIBUTE7            VARCHAR2 (240),
   ATTRIBUTE8            VARCHAR2 (240),
   ATTRIBUTE9            VARCHAR2 (240),
   ATTRIBUTE10           VARCHAR2 (240),
   ATTRIBUTE11           VARCHAR2 (240),
   ATTRIBUTE12           VARCHAR2 (240),
   ATTRIBUTE13           VARCHAR2 (240),
   ATTRIBUTE14           VARCHAR2 (240),
   ATTRIBUTE15           VARCHAR2 (240),
   CONSTRAINT XXWC_AP_SUP_CONTACTS_TBL_U1 UNIQUE (supplier_id, org_id, contact_type, first_name, last_name)
)
/
CREATE SYNONYM APPS.XXWC_AP_SUP_CONTACTS_TBL FOR XXWC.XXWC_AP_SUP_CONTACTS_TBL
/
     BEGIN
       DBMS_RLS.ADD_POLICY ('APPS',                            -- SCHEMA NAME
                            'XXWC_AP_SUP_CONTACTS_TBL',        -- SYNONYM NAME
                            'ORG_SEC',                         -- USE POLICY_NAME 'ORG_SEC' –STANDARD POLICY
                            'APPS',                            -- FUNCTION_SCHEM
                            'MO_GLOBAL.ORG_SECURITY',          -- STANDARD MO VPD POLICY FUN
                            'SELECT, INSERT, UPDATE, DELETE',  -- STATEMENT_TYPE
                            TRUE,                              -- UPDATE_CHECK
                            TRUE                               -- POLICY ENABLE
                           );
       COMMIT;
END;
/
