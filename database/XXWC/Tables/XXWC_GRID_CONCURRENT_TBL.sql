create table xxwc.XXWC_GRID_CONCURRENT_TBL 
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header xxwc.XXWC_GRID_CONCURRENT_TBL $
  Module Name: xxwc.XXWC_GRID_CONCURRENT_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-APR-2018   Pahwa, Nancy                Initially Created 
TMS# 20180319-00169
**************************************************************************/
(
  request_id  NUMBER(15),
  status_flag VARCHAR2(1) default 'N',
  status      VARCHAR2(1)
)
tablespace XXWC_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );