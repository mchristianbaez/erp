DROP TABLE XXWC.XXWC_QP_MODIFIERS_STG;

CREATE TABLE XXWC.XXWC_QP_MODIFIERS_STG
(
  MODIFIER_NAME           VARCHAR2(240 BYTE)        NULL,
  LIST_LINE_ID            NUMBER                    NULL,
  START_DATE              DATE                      NULL,
  END_DATE                DATE                      NULL,
  PRODUCT_ATTRIBUTE       VARCHAR2(240 BYTE)        NULL,
  PRODUCT_VALUE           VARCHAR2(240 BYTE)        NULL,
  APPLICATION_METHOD      VARCHAR2(240 BYTE)        NULL,
  LINE_VALUE              NUMBER                    NULL,
  INCOMPATIBILITY         VARCHAR2(80 BYTE)         NULL,
  PRICE_BREAK_TYPE        VARCHAR2(240 BYTE)        NULL,
  PRODUCT_UOM             VARCHAR2(3 BYTE)          NULL,
  PRICE_BREAK_VALUE_FROM  NUMBER                    NULL,
  PRICE_BREAK_VALUE_TO    NUMBER                    NULL,
  LIST_HEADER_ID          NUMBER                    NULL,
  PRODUCT_ATTRIBUTE_CODE  VARCHAR2(240 BYTE)        NULL,
  PRODUCT_ATTR_VALUE_ID   NUMBER                    NULL,
  FORMULA_ID              NUMBER                    NULL,
  PROD_PRECEDENCE         NUMBER                    NULL,
  STATUS                  VARCHAR2(50 BYTE)         NULL,
  ERR_MESSAGE             VARCHAR2(4000 BYTE)       NULL,
  REQUEST_ID              NUMBER                    NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


