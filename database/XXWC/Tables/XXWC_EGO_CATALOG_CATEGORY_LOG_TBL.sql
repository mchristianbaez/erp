CREATE TABLE XXWC.XXWC_EGO_CATALOG_CATEGORY_LOG
   /**************************************************************************
    File Name:XXWC_EGO_CATALOG_CATEGORY_LOG
    TYPE: Table
    PURPOSE: 
    HISTORY
    -- Description   : 
    --                 
    ================================================================
           Last Update Date : 08-Jan-2014
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     08-Jan-2014   Praveen Pawar   					   
   **************************************************************************/
			(ITEM_NUMBER          VARCHAR2(40)
			,ORGANIZATION_CODE    VARCHAR2(3)
			,INVENTORY_ITEM_ID    NUMBER
			,ORGANIZATION_ID      NUMBER
			,CATALOG_NAME         VARCHAR2(30)
			,CATALOG_ID           NUMBER
			,CATEGORY             VARCHAR2(40)
			,CATEGORY_ID          NUMBER
			,TRANSACTION_TYPE     VARCHAR2(40)
			,CREATED_BY           NUMBER
			,CREATION_DATE        DATE
			);			
			
			CREATE OR REPLACE PUBLIC SYNONYM XXWC_EGO_CATALOG_CATEGORY_LOG FOR XXWC.XXWC_EGO_CATALOG_CATEGORY_LOG;