/*************************************************************************
 $Header XXWC_AR_AHH_IB_FILES_TBL $
 Module Name: XXWC_AR_AHH_IB_FILES_TBL.sql

 REVISIONS:
 Ver        Date        Author                     Description
 ---------  ----------  ---------------         -------------------------
 1.0        05/04/2018  P.Vamshidhar            20180503-00140-AH Harries Cash Receipts Interface
***********************************************************************************************/
CREATE TABLE XXWC.XXWC_AR_AHH_IB_FILES_TBL
(
  FILE_PERMISSIONS  VARCHAR2(50 BYTE),
  FILE_TYPE         VARCHAR2(64 BYTE),
  FILE_OWNER        VARCHAR2(64 BYTE),
  FILE_GROUP        VARCHAR2(64 BYTE),
  FILE_SIZE         VARCHAR2(64 BYTE),
  FILE_MONTH        VARCHAR2(64 BYTE),
  FILE_DAY          VARCHAR2(64 BYTE),
  FILE_TIME         VARCHAR2(64 BYTE),
  FILE_NAME         VARCHAR2(612 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_AR_AHH_LOCKBOX_IB_DIR
     ACCESS PARAMETERS
       ( RECORDS DELIMITED BY NEWLINE

    PREPROCESSOR XXWC_AR_AHH_LOCKBOX_IB_DIR: 'list_files.sh'
    NOBADFILE
    NODISCARDFILE
    NOLOGFILE

    FIELDS TERMINATED BY WHITESPACE
            )
     LOCATION (XXWC_AR_AHH_LOCKBOX_IB_DIR:'list_files_dummy_source.txt')
  )
REJECT LIMIT UNLIMITED
NOPARALLEL
NOMONITORING;