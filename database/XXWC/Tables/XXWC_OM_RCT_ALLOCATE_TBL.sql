CREATE TABLE XXWC.XXWC_OM_RCT_ALLOCATE_TBL
(
  REQUEST_ID          NUMBER                        NULL,
  SHIPMENT_HEADER_ID  NUMBER                        NULL,
  SHIPMENT_LINE_ID    NUMBER                        NULL,
  CREATED_BY          NUMBER                        NULL,
  HEADER_ID           NUMBER                        NULL,
  LINE_ID             NUMBER                        NULL,
  SHIP_QTY            NUMBER                        NULL,
  REQUEST_DATE        DATE                          NULL,
  CREATION_DATE       DATE                          NULL,
  SHIP_FROM_ORG_ID    NUMBER                        NULL,
  PROCESS_FLAG        VARCHAR2(1 BYTE)              NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE SYNONYM APPS.XXWC_OM_RCT_ALLOCATE_TBL FOR XXWC.XXWC_OM_RCT_ALLOCATE_TBL;


