/*********************************************************************************
Copyright (c) 2017 HD Supply
All Rights Reserved

HEADER:
	XXWC_LOC_ORACLE_LKP_TBL

PROGRAM NAME:
	EComm Google Product Mapping - APEX EBS

DESCRIPTION:
	TMS# 20161104-00076

LAST UPDATE DATE: 11-FEB-2017

HISTORY
=======
VERSION	DATE          	 AUTHOR(S)    		 DESCRIPTION
-------	---------------	------------------- -------------------------------------
1.0		11-FEB-2017	     Christian Baez		 Creation

*********************************************************************************/
CREATE TABLE "XXWC"."XXWC_LOC_ORACLE_LKP_TBL" 
   (
   	  "ORACLE_CATEGORY_ID" NUMBER, 
	"ORACLE_PROD_CATEGORY" VARCHAR2(100 BYTE), 
	              "ACTIVE" VARCHAR2(3 BYTE)
   )
/
CREATE OR REPLACE PUBLIC SYNONYM EA_APEX.XXWC_LOC_ORACLE_LKP_TBL FOR XXWC.XXWC_LOC_ORACLE_LKP_TBL;
/
GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_LOC_ORACLE_LKP_TBL TO EA_APEX;
/