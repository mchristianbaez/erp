/*******************************************************************************************************
  -- Table Name XXWC_AR_CASH_RECEIPTS_URL_STG
  -- ***************************************************************************************************
  --
  -- PURPOSE: To load image link data for Ar Cash Receipt's attached images
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     09-Sep-2016   Niraj K Ranjan   Initial Version - TMS#20150903-00017   IT - data script for new URL on lockbox receipts
********************************************************************************************************/
DROP TABLE xxwc.xxwc_ar_cash_receipts_url_stg CASCADE CONSTRAINTS;

CREATE TABLE xxwc.xxwc_ar_cash_receipts_url_stg
             ( APEX_OBJECT_ID        VARCHAR2(50)
              ,APEX_ENCRYPTED_ID     VARCHAR2(100)
              ,APEX_URL              VARCHAR2(500)
              ,DCTM_OBJECT_ID        VARCHAR2(50)
              ,DCTM_URL              VARCHAR2(500)
			  ,STATUS                VARCHAR2(30)
			  ,ERROR_MESSAGE         VARCHAR2(2000)
			  ,CREATION_DATE         DATE
			  ,CREATED_BY            NUMBER
			  ,LAST_UPDATE_DATE      DATE
			  ,LAST_UPDATED_BY       NUMBER
			  ,REQUEST_ID            NUMBER
			 );

