/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_ROLES_JOURNAL_TBL $
  Module Name: XXWC_MS_ROLES_JOURNAL_TBL

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
alter table XXWC.XXWC_MS_ROLES_JOURNAL_TBL
  add constraint PK_JOURNAL_ID primary key (JOURNAL_ID)
  using index 
  tablespace XXWC_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );