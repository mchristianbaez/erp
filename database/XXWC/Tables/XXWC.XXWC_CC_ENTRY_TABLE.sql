CREATE TABLE XXWC.XXWC_CC_ENTRY_TABLE 
  (creation_date          DATE
  ,created_by             NUMBER
  ,last_update_date       DATE
  ,last_updated_by        NUMBER
  ,employee_id            NUMBER
  ,cycle_count_header_id  NUMBER
  ,cycle_count_entry_id   NUMBER
  ,wc_count_list_sequence NUMBER
  ,count_list_sequence    NUMBER
  ,organization_id        NUMBER
  ,inventory_item_id      NUMBER
  ,revision               VARCHAR2(80)
  ,quantity               NUMBER
  ,primary_uom_code       VARCHAR2(3) 
  ,subinventory           VARCHAR2(10)
  ,lot_number             VARCHAR2(30)
  ,locator_id             NUMBER
  ,cost_group_id          NUMBER
  ,process_flag           NUMBER
  ,batch_id               NUMBER
  ,return_status          VARCHAR2(1)
  ,interface_id           NUMBER
  ,error_code             VARCHAR2(2000)
  ,message_count          NUMBER
  ,message                VARCHAR2(2000))
  NOLOGGING;
  
CREATE INDEX XXWC.XXWC_CC_ENTRY_TABLE_N1 ON XXWC.XXWC_CC_ENTRY_TABLE
 (cycle_count_header_id
 ,cycle_count_entry_id
 ,wc_count_list_sequence);
  