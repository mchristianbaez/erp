/* Formatted on 26-Nov-2012 23:32:10 (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_AR_CUST_MAINT_OTHER_ERROR
-- Generated 26-Nov-2012 23:32:04 from XXWC@EBIZCON

CREATE TABLE xxwc.xxwc_ar_cust_maint_other_error
(
    insert_date     DATE
   ,error_message   VARCHAR2 (2048 BYTE)
   ,insert_id       NUMBER
)

NOCACHE
MONITORING
NOPARALLEL
noLOGGING
/

-- End of DDL Script for Table XXWC.XXWC_AR_CUST_MAINT_OTHER_ERROR
