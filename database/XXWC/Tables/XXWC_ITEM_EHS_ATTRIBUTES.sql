  /********************************************************************************
  FILE NAME: XXWC_ITEM_EHS_ATTRIBUTES.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: XXWC_ITEM_EHS_ATTRIBUTES Staging table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     08/09/2018    Naveen Kalidindi TMS#20180723-00096 -Prop 65 Oracle Compliance changes - CFD, e-Comm
  *******************************************************************************************/
-- DROP TABLE
--DROP TABLE XXWC.XXWC_ITEM_EHS_ATTRIBUTES;

-- CREATE TABLE
CREATE TABLE XXWC.XXWC_ITEM_EHS_ATTRIBUTES
(
  ITEM_NUMBER                    VARCHAR2(40),
  INVENTORY_ITEM_ID              NUMBER,
  ORGANIZATION_ID                NUMBER,  
  PROP_65_FLAG                   VARCHAR2(1), 
  PROP_65_CANCER                 VARCHAR2(4000),    
  PROP_65_REPRODUCTIVE           VARCHAR2(4000),
  PROP_65_SHORT_FORM             VARCHAR2(4000), 
  PROP_65_DISPLAY_MESSAGE        VARCHAR2(4000),
  PROP_65_MESSAGE                VARCHAR2(4000),
  PROP_65_WOOD_DUST_MSG          VARCHAR2(4000),
  PROP_65_CANCER_MSG             VARCHAR2(4000),
  PROP_65_REPRODUCTIVE_MSG       VARCHAR2(4000),
  PROP_65_SHORT_FORM_MSG         VARCHAR2(4000),
  CREATION_DATE                  DATE,
  CREATED_BY                     NUMBER,
  LAST_UPDATE_DATE               DATE,
  LAST_UPDATED_BY                NUMBER
)
/

-- CREATE/RECREATE INDEXES 
CREATE UNIQUE INDEX XXWC.XXWC_ITEM_EHS_ATTRIBUTES_U1 ON XXWC.XXWC_ITEM_EHS_ATTRIBUTES (INVENTORY_ITEM_ID, ORGANIZATION_ID)
/

CREATE INDEX XXWC.XXWC_ITEM_EHS_ATTRIBUTES_N1 ON XXWC.XXWC_ITEM_EHS_ATTRIBUTES(ITEM_NUMBER)
/

GRANT SELECT, INSERT, UPDATE, DELETE ON XXWC.XXWC_ITEM_EHS_ATTRIBUTES TO EA_APEX
/