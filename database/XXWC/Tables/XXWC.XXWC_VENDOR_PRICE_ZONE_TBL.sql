CREATE TABLE XXWC.XXWC_VENDOR_PRICE_ZONE_TBL
  (creation_date          DATE   NOT NULL
  ,created_by             NUMBER NOT NULL
  ,last_update_date       DATE   NOT NULL
  ,last_updated_by        NUMBER NOT NULL
  ,vendor_id              NUMBER NOT NULL
  ,vendor_site_id         NUMBER NOT NULL
  ,organization_id        NUMBER NOT NULL
  ,price_zone             NUMBER
  );