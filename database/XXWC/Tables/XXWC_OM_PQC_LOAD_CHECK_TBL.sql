/*************************************************************************
  $Header XXWC_OM_PQC_LOAD_CHECK_TBL.sql $
  Module Name: XXWC_OM_PQC_LOAD_CHECK_TBL.sql

  PURPOSE:   Created New table

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        12/11/2017  Ram Talluri          TMS#20171011-00108 - Picking QC
                                              project - EBS - order information
   									          to APEX & Pick ticket changes -
                                              Initial Version
**************************************************************************/
--DROP TABLE XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL CASCADE CONSTRAINTS;
CREATE TABLE XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL
    (HEADER_ID 					NUMBER, 
	ORDER_NUMBER 				NUMBER, 
	PICK_TICKET_VERSION 		VARCHAR2(30), 
	CUSTOMER_NAME 				VARCHAR2(2000), 
	CUSTOMER_NUMBER 			NUMBER, 
	CUSTOMER_CITY               VARCHAR2(2000), 
	CUST_ACCOUNT_ID             NUMBER, 
	SHIP_FROM_ORG_ID            NUMBER, 
	SHIP_FROM_ORG_CODE          VARCHAR2(3), 
	REQUEST_DATE                DATE, 
	FREIGHT_CARRIER_CODE        VARCHAR2(30), 
	TOTAL_NUMBER_OF_LINES       NUMBER, 
	NUMBER_OF_LINES_PULLED      NUMBER, 
	NO_OF_LNS_LOAD_CHECKED      NUMBER, 
	LOAD_CHECK_STATUS           VARCHAR2(30), 
	PICKED_BY_USER_NTID         VARCHAR2(30), 
	PICKED_BY_USER_NAME         VARCHAR2(200), 
	LOAD_CHECKED_BY_USER_NTID   VARCHAR2(30), 
	LOAD_CHECKED_BY_USER_NAME   VARCHAR2(200), 
	LOAD_CHECK_START_DATE       DATE, 
	LOAD_CHECK_COMPLETION_DATE  DATE, 
	PALLETS                     NUMBER, 
	BUNDLES                     NUMBER, 
	TOOLS                       NUMBER, 
	UNITS                       NUMBER, 
	NOTES                       VARCHAR2(2000), 
	CREATION_DATE               DATE, 
	CREATED_BY                  NUMBER, 
	LAST_UPDATE_DATE            DATE, 
	LAST_UPDATED_BY             NUMBER,
	LINE_ERRORS	                NUMBER,
	REQUEST_ID                  NUMBER
	)
/
CREATE INDEX XXWC.XXWC_OM_PQC_LOAD_CHECK_HDR_ID ON XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL(HEADER_ID)
/
CREATE OR REPLACE SYNONYM APPS.XXWC_OM_PQC_LOAD_CHECK_TBL FOR XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL
/
GRANT SELECT, INSERT, UPDATE, DELETE ON APPS.XXWC_OM_PQC_LOAD_CHECK_TBL TO EA_APEX
/