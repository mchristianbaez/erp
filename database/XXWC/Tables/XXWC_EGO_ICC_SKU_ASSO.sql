CREATE TABLE XXWC.XXWC_EGO_ICC_SKU_ASSO
(
  ITEM_NUMBER    VARCHAR2(150 BYTE),
  ICC            VARCHAR2(150 BYTE),
  WEB_HIERARCHY  VARCHAR2(200 BYTE),
  MANUF_PART_NO  VARCHAR2(200 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


