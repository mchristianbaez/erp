  
 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_TRANSACT_TYPE_EXT_TBL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AP_TRANSACT_TYPE_EXT_TBL
   (AHH_TRANSACTION_TYPE		 VARCHAR2(50), 
	ORACLE_TRANSACTION_TYPE		 VARCHAR2(25)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AP_TRANSACTION_TYPE_CONV_LOAD.bad'
    DISCARDFILE 'XXWC_AP_TRANSACTION_TYPE_CONV_LOAD.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AP_TRANSACTION_TYPE_CONV_LOAD.csv'
       )
    )
   REJECT LIMIT UNLIMITED;
   /