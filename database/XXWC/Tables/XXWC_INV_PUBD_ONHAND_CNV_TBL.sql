/**************************************************************************
$FIle Name: XXWC_INV_PUBD_ONHAND_CNV_TBL.sql  $
PURPOSE:   This table will store data for the PUBD on hand conversion
REVISIONS:
 Ver        Date         Author                  Description
 ---------  -----------  ---------------         -------------------------
 1.0        28-Mar-2016  Rakesh Patel            TMS#20160107-00119-PUBD - On-Hand Conversion
**************************************************************************/
Drop Table XXWC.XXWC_PUBD_ONHAND_CNV ;

CREATE TABLE XXWC.XXWC_PUBD_ONHAND_CNV
(
  ORGANIZATION_CODE              VARCHAR2(10 BYTE),
  ITEM_SEGMENT                   VARCHAR2(40 BYTE),
  DESCRIPTION                    VARCHAR2(4000 BYTE),
  GEN_QTY_OH                     VARCHAR2(150 BYTE),
  BIN_1                          VARCHAR2(4000 BYTE),
  BIN_2                          VARCHAR2(4000 BYTE),
  BIN_3                          VARCHAR2(4000 BYTE),
  GEN_QTY_STAY_GEN               VARCHAR2(150 BYTE),
  GEN_QTY_MOVE_PUBD              NUMBER,
  GEN_QTY_HAZWASTE               VARCHAR2(150 BYTE),
  EXPIRED_PUBD                   VARCHAR2(150 BYTE),
  EXPIRED_HAZWASTE               VARCHAR2(150 BYTE),
  TRANSACTION_UOM                VARCHAR2(3 BYTE),
  ORGANIZATION_ID                NUMBER,
  STATUS                         VARCHAR2(10 BYTE),
  ERROR_MESSAGE                  VARCHAR2(4000 BYTE),
  ATTRIBUTE1                     VARCHAR2(150 BYTE),
  ATTRIBUTE2                     VARCHAR2(150 BYTE),
  ATTRIBUTE3                     VARCHAR2(150 BYTE),
  ATTRIBUTE4                     VARCHAR2(150 BYTE),
  ATTRIBUTE5                     VARCHAR2(150 BYTE),
  CREATION_DATE                  DATE                    NOT NULL,
  CREATED_BY                     NUMBER                  NOT NULL,
  LAST_UPDATED_BY                NUMBER                  NOT NULL,
  LAST_UPDATE_DATE               DATE                    NOT NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX XXWC.XXWC_PUBD_ONHAND_CNV_N1 ON XXWC.XXWC_PUBD_ONHAND_CNV
(ORGANIZATION_ID, STATUS)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

SHOW ERRORS; 
COMMIT;
EXIT; 
