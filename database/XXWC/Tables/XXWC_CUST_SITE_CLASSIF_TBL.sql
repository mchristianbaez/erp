  /********************************************************************************
  FILE NAME: XXWC_CUST_SITE_CLASSIF_TBL.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Archive Table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)         DESCRIPTION
  ------- -----------   ----------------  ----------------------------------------------------
  1.0     07/09/2018    Vamshi Palavarapu Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- Drop Table
-- DROP TABLE XXWC.XXWC_CUST_SITE_CLASSIF_TBL;

-- Create table
CREATE TABLE XXWC.XXWC_CUST_SITE_CLASSIF_TBL
(
  AHH_SITE_CLASSIFICATION VARCHAR2(10),
  CUSTOMER_NAME           VARCHAR2(240),
  CUST_SITE_NAME          VARCHAR2(30),
  CUSTOMER_NUMBER         VARCHAR2(20),
  CUST_SITE_NUM           VARCHAR2(30),
  CITY                    VARCHAR2(50),
  STATE                   VARCHAR2(50),
  ENTER_DATE              DATE,
  TERMS_TYPE              VARCHAR2(20),
  SELL_TYPE               VARCHAR2(10),
  HIGH_BALANCE            NUMBER(10,2),
  AVG_PAY_DAYS            NUMBER,
  TOTAL_CURR_BALANCE      NUMBER(10,2),
  LAST_SALE_DT            DATE,
  LAST_PAY_DT             DATE,
  NAICS                   VARCHAR2(20),
  SLSREPOUT               VARCHAR2(20),
  HOLDPER_CD              VARCHAR2(20),
  SIC_1                   VARCHAR2(20),
  SIC_2                   VARCHAR2(20),
  SIC_3                   VARCHAR2(20),
  SALES_YTD               NUMBER(10,2),
  LAST_SALES_YTD          NUMBER(10,2),
  CREDIT_LIMIT            NUMBER,
  COLLECTOR_ID            NUMBER,
  SITE_CLASS              VARCHAR2(50)
);
/
GRANT ALL ON XXWC.XXWC_CUST_SITE_CLASSIF_TBL TO EA_APEX;
/