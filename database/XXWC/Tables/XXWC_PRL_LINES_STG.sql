--
-- XXWC_PRL_LINES_STG  (Table) 
--
CREATE TABLE XXWC.XXWC_PRL_LINES_STG
(
  PRL_NAME           VARCHAR2(240 BYTE)             NULL,
  PRODUCT_ATTRIBUTE  VARCHAR2(240 BYTE)             NULL,
  PRODUCT_VALUE      VARCHAR2(240 BYTE)             NULL,
  VALUE              NUMBER                         NULL,
  FORMULA_NAME       VARCHAR2(240 BYTE)             NULL,
  CALL_FOR_PRICE     VARCHAR2(30 BYTE)              NULL,
  LIST_HEADER_ID     NUMBER                         NULL,
  PRODUCT_ID         NUMBER                         NULL,
  FORMULA_ID         NUMBER                         NULL,
  STATUS             VARCHAR2(30 BYTE)              NULL,
  ERR_MESSAGE        VARCHAR2(4000 BYTE)            NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


