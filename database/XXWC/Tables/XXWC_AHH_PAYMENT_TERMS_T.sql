  /********************************************************************************
  FILE NAME: XXWC_AHH_PAYMENT_TERMS_T.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Archive Table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)         DESCRIPTION
  ------- -----------   ----------------  ----------------------------------------------------
  1.0     07/09/2018    Vamshi Palavarapu Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- Drop Table
-- DROP TABLE XXWC.XXWC_AHH_PAYMENT_TERMS_T;

-- Create table
CREATE TABLE XXWC.XXWC_AHH_PAYMENT_TERMS_T
(
  AHH_TERM_NAME    VARCHAR2(50),
  TERM_NAME        VARCHAR2(50),
  TERM_DESCRIPTION VARCHAR2(150)
);

GRANT ALL ON XXWC.XXWC_AHH_PAYMENT_TERMS_T TO EA_APEX;
/