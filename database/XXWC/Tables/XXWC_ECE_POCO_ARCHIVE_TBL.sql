CREATE TABLE XXWC.XXWC_ECE_POCO_ARCHIVE_TBL
/*************************************************************************
  $Header XXWC_ECE_POCO_ARCHIVE_TBL.sql $
  Module Name : XXWC_ECE_POCO_ARCHIVE_TBL

  PURPOSE     : Table to store the purchase orders to be updated to avoid POCO EDI transmission
  TMS Task Id : 20141218-00011

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        04-Feb-2015  Manjula Chellappan    Initial Version
**************************************************************************/
(
   PO_HEADER_ID               NUMBER,
   PO_NUMBER                  VARCHAR2 (20),
   REVISION_NUM               NUMBER,
   CREATION_DATE              DATE,
   LAST_UPDATE_DATE           DATE,
   APPROVED_DATE              DATE,
   PRINTED_DATE               DATE,
   PRINT_COUNT                NUMBER,
   EDI_PROCESSED_FLAG         VARCHAR2 (1),
   VENDOR_ID                  NUMBER,
   VENDOR_NUM                 VARCHAR2 (30),
   VENDOR_NAME                VARCHAR2 (240),
   VENDOR_SITE_ID             NUMBER,
   EDI_LOCATION_CODE          VARCHAR2 (60),
   VENDOR_SITE_CODE           VARCHAR2 (30),
   STATUS                     VARCHAR2 (1),
   ERROR_MSG                  VARCHAR2 (2000),
   PRINTED_DATE_AFTER         DATE,
   PRINT_COUNT_AFTER          NUMBER,
   EDI_PROCESSED_FLAG_AFTER   VARCHAR2 (1),
   ORG_ID                     NUMBER
                                 DEFAULT NVL (
                                            TO_NUMBER (
                                               DECODE (
                                                  SUBSTRB (
                                                     USERENV ('CLIENT_INFO'),
                                                     1,
                                                     1),
                                                  ' ', NULL,
                                                  SUBSTRB (
                                                     USERENV ('CLIENT_INFO'),
                                                     1,
                                                     10))),
                                            -99),
   CONSTRAINT XXWC_ECE_POCO_ARCHIVE_TBL_U1 UNIQUE
      (po_header_id, revision_num, org_id)
)
/

CREATE SYNONYM APPS.XXWC_ECE_POCO_ARCHIVE_TBL FOR XXWC.XXWC_ECE_POCO_ARCHIVE_TBL
/

BEGIN
   DBMS_RLS.ADD_POLICY ('APPS',                                 -- SCHEMA NAME
                        'XXWC_ECE_POCO_ARCHIVE_TBL',           -- SYNONYM NAME
                        'ORG_SEC', -- USE POLICY_NAME 'ORG_SEC' –STANDARD POLICY
                        'APPS',                              -- FUNCTION_SCHEM
                        'MO_GLOBAL.ORG_SECURITY', -- STANDARD MO VPD POLICY FUN
                        'SELECT, INSERT, UPDATE, DELETE',    -- STATEMENT_TYPE
                        TRUE,                                  -- UPDATE_CHECK
                        TRUE                                  -- POLICY ENABLE
                            );
   COMMIT;
END;