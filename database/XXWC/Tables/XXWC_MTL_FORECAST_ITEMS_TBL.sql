/*************************************************************************
$Header XXWC_MTL_FORECAST_ITEMS_TBL.sql $
Module Name: XXWC_MTL_FORECAST_ITEMS_TBL
PURPOSE: Table to maintain Forecast Items
REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------
1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047
**************************************************************************/
CREATE TABLE XXWC.XXWC_MTL_FORECAST_ITEMS_TBL
  (
    INVENTORY_ITEM_ID       NUMBER NOT NULL,
    ITEM_NUMBER             VARCHAR2(40),
    FORECAST_DESIGNATOR     VARCHAR2(10),
    ORGANIZATION_ID         NUMBER,
    SOURCE_ORGANIZATION_ID  NUMBER,
    TOTAL_DEMAND            NUMBER,
    AVG_DEMAND              NUMBER,
    AMU                     NUMBER,
    FINAL_DEMAND            NUMBER,
    FINAL_AVG_DEMAND        NUMBER,
    NUM_OF_FORECAST_PERIODS NUMBER,
    NUM_OF_PREVIOUS_PERIODS NUMBER,
    SEASON_FACTOR_1         NUMBER,
    AVG_DEMAND_1            NUMBER,
    SEASON_FACTOR_2         NUMBER,
    AVG_DEMAND_2            NUMBER,
    SEASON_FACTOR_3         NUMBER,
    AVG_DEMAND_3            NUMBER,
    SEASON_FACTOR_4         NUMBER,
    AVG_DEMAND_4            NUMBER,
    SEASON_FACTOR_5         NUMBER,
    AVG_DEMAND_5            NUMBER,
    SEASON_FACTOR_6         NUMBER,
    AVG_DEMAND_6            NUMBER,
    SEASON_FACTOR_7         NUMBER,
    AVG_DEMAND_7            NUMBER,
    SEASON_FACTOR_8         NUMBER,
    AVG_DEMAND_8            NUMBER,
    SEASON_FACTOR_9         NUMBER,
    AVG_DEMAND_9            NUMBER,
    SEASON_FACTOR_10        NUMBER,
    AVG_DEMAND_10           NUMBER,
    SEASON_FACTOR_11        NUMBER,
    AVG_DEMAND_11           NUMBER,
    SEASON_FACTOR_12        NUMBER,
    AVG_DEMAND_12           NUMBER,
    BUCKET_TYPE             NUMBER
  );
CREATE INDEX xxwc.XXWC_MTL_FORECAST_ITEMS_TBL_N1 ON XXWC.XXWC_MTL_FORECAST_ITEMS_TBL
  (
    organization_id,
    inventory_item_id,
    forecast_designator
  ) ;
CREATE INDEX xxwc.XXWC_MTL_FORECAST_ITEMS_TBL_N2 ON XXWC.XXWC_MTL_FORECAST_ITEMS_TBL
  (
    organization_id,
    inventory_item_id
  ) ;
CREATE INDEX xxwc.XXWC_MTL_FORECAST_ITEMS_TBL_N3 ON XXWC.XXWC_MTL_FORECAST_ITEMS_TBL
  (
    source_organization_id,
    inventory_item_id,
    forecast_designator
  ) ; 