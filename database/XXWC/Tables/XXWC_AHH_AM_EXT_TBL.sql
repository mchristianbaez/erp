 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_AM_EXT_TBL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    P.Vamshidhar  TMS#20180709-00089 - AHH Customer Conversion External Tables.
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AHH_AM_EXT_TBL
(
  SLSREPOUT    VARCHAR2(20 BYTE),
  SALESREP_ID  VARCHAR2(20 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_AR_AHH_CASH_RCPT_CONV_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AHH_AM.bad'
    DISCARDFILE 'XXWC_AHH_AM.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                  )
     LOCATION (XXWC_AR_AHH_CASH_RCPT_CONV_DIR:'XXWC_AHH_AM.csv')
  )
REJECT LIMIT UNLIMITED
NOPARALLEL
NOMONITORING;
/
GRANT SELECT ON XXWC.XXWC_AHH_AM_EXT_TBL TO EA_APEX
/