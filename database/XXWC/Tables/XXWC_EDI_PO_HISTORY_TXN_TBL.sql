CREATE TABLE xxwc.xxwc_edi_po_history_txn_tbl
/*************************************************************************
  $Header xxwc_edi_po_history_txn_tbl.sql $
  Module Name : xxwc_edi_po_history_txn_tbl

  PURPOSE     : Table to store the disabled history PO transactions when
              the site is configured for EDI
  TMS Task Id :  20140421-00041

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        04/29/2014  Manjula Chellappan    Initial Version

**************************************************************************/
(
   po_header_id        NUMBER NOT NULL,
   po_number           VARCHAR2 (150),
   tp_header_id        NUMBER,
   edi_location_code   VARCHAR2 (150),
   edi_processed_flag_before  VARCHAR2(5),
   edi_processed_flag_after  VARCHAR2(5),
   print_count_before  NUMBER,
   print_count_after   NUMBER,
   printed_date_before DATE,
   printed_date_after DATE,
   disabled_status     VARCHAR2(1),
   disabled_date       DATE,
   error_msg           VARCHAR2(4000)
);
