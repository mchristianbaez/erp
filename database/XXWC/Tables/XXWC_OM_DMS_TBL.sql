  /*******************************************************************************
  Table:   XXWC_OM_DMS_TBL
  Description: This table is used to maintain timestamp when file was created for 
               MyLogistics
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     31-Oct-2014        Kathy Poling    Initial version TMS# 20140606-00082 
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_OM_DMS_TBL" 
   (  "FILE_ID" NUMBER, 
  "FILE_NAME" VARCHAR2(150), 
  "REQUEST_ID" NUMBER, 
  "STATUS" VARCHAR2(240), 
  "CREATED_BY" NUMBER, 
  "CREATION_DATE" DATE, 
  "LAST_UPDATED_BY" NUMBER, 
  "LAST_UPDATE_DATE" DATE, 
  "FILE_SIZE" NUMBER, 
  "ORG_ID" NUMBER DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),-99)
   );
