  /*******************************************************************************
  Table: "XXWC"."XXWC_ORACLE_ATTACH_ARCH_TBL"  
  Description: "XXWC"."XXWC_ORACLE_ATTACH_ARCH_TBL" 
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-JUL-2017        Pahwa Nancy   Task ID: 20170616-00237
  ********************************************************************************/
-- Create table
create table xxwc.XXWC_ORACLE_ATTACH_ARCH_TBL
(
  seq_num                  NUMBER,
  category_id              NUMBER,
  document_description     VARCHAR2(255),
  file_name                VARCHAR2(2048),
  url                      VARCHAR2(2048),
  function_name            VARCHAR2(30),
  entity_name              VARCHAR2(40),
  pk1_value                VARCHAR2(100),
  pk2_value                VARCHAR2(100),
  pk3_value                VARCHAR2(100),
  pk4_value                VARCHAR2(100),
  pk5_value                VARCHAR2(100),
  media_id                 NUMBER,
  user_id                  NUMBER,
  usage_type               VARCHAR2(1),
  title                    VARCHAR2(80),
  receipt_number           VARCHAR2(30),
  account_number           VARCHAR2(30),
  mssql_url                VARCHAR2(2048),
  old_attached_document_id NUMBER,
  old_document_id          NUMBER,
  old_creation_date        DATE,
  old_updated_date         DATE,
  flag                     VARCHAR2(1),
  text                     LONG,
  old_file_id              NUMBER,
  fnd_flag                 VARCHAR2(1)
);
/