/*************************************************************************
  $Header XXWC_PO_RTV_HEADERS_TBL $
  Module Name: XXWC_PO_RTV_HEADERS_TBL

  PURPOSE: Store Return to Vendor Transactions Header Details

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        04/10/2014  Manjula Chellappan    Initial Version
**************************************************************************/
CREATE TABLE XXWC.XXWC_PO_RTV_HEADERS_TBL
(
  RTV_HEADER_ID        NUMBER                   NOT NULL,
  SHIP_FROM_BRANCH_ID  NUMBER                   NOT NULL,
  BUYER_ID             NUMBER                   NOT NULL,
  SUPPLIER_ID          NUMBER                   NOT NULL,
  SUPPLIER_SITE_ID     NUMBER,
  PO_HEADER_ID         NUMBER,
  RETURN_NUMBER        VARCHAR2(100 BYTE)       NOT NULL,
  WC_RETURN_NUMBER     VARCHAR2(100 BYTE)       NOT NULL,
  SUPPLIER_ADDRESS     VARCHAR2(2000 BYTE),
  RESTOCKING_FEE       NUMBER,
-- TMS# 20140619-00123 notes field Added by Manjula on 20-Jun-14 
  NOTES                VARCHAR2(2000),
  RTV_STATUS_CODE      VARCHAR2(40 BYTE)        NOT NULL,
  SUPPLIER_CONTACT_ID  NUMBER,
  ERROR_MESSAGE        VARCHAR2(4000 BYTE),  
  CREATED_BY           NUMBER                   NOT NULL,
  CREATION_DATE        DATE                     NOT NULL,
  LAST_UPDATED_BY      NUMBER                   NOT NULL,
  LAST_UPDATE_DATE     DATE                     NOT NULL,
  LAST_UPDATE_LOGIN    NUMBER                   NOT NULL,
  ATTRIBUTE1           VARCHAR2(240 BYTE),
  ATTRIBUTE2           VARCHAR2(240 BYTE),
  ATTRIBUTE3           VARCHAR2(240 BYTE),
  ATTRIBUTE4           VARCHAR2(240 BYTE),
  ATTRIBUTE5           VARCHAR2(240 BYTE),
  ATTRIBUTE6           VARCHAR2(240 BYTE),
  ATTRIBUTE7           VARCHAR2(240 BYTE),
  ATTRIBUTE8           VARCHAR2(240 BYTE),
  ATTRIBUTE9           VARCHAR2(240 BYTE),
  ATTRIBUTE10          VARCHAR2(240 BYTE),
  ATTRIBUTE11          VARCHAR2(240 BYTE),
  ATTRIBUTE12          VARCHAR2(240 BYTE),
  ATTRIBUTE13          VARCHAR2(240 BYTE),
  ATTRIBUTE14          VARCHAR2(240 BYTE),
  ATTRIBUTE15          VARCHAR2(240 BYTE) 
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


--  There is no statement for index XXWC.SYS_C00940917.
--  The object is created when the parent object is created.

ALTER TABLE XXWC.XXWC_PO_RTV_HEADERS_TBL ADD (
  UNIQUE (RTV_HEADER_ID)
  USING INDEX
    TABLESPACE XXWC_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
  ENABLE VALIDATE);