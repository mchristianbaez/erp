CREATE TABLE xxwc.XXWC_MTL_DEMAND_HIST_LAST_RUN
/*************************************************************************
  $Header XXWC_MTL_DEMAND_HIST_LAST_RUN $
  Module Name: XXWC_MTL_DEMAND_HIST_LAST_RUN

  PURPOSE: Demand History Last Run dates

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        07/07/2014  Manjula Chellappan    Initial Version TMS # 20140203-00283
**************************************************************************/
    (
        organization_id   NUMBER
       ,last_run_date     DATE
       ,last_run_date_j   NUMBER
    );

    INSERT INTO xxwc.XXWC_MTL_DEMAND_HIST_LAST_RUN
        SELECT organization_id, SYSDATE - 600, TO_NUMBER (TO_CHAR (SYSDATE - 600, 'J')) FROM mtl_parameters
    COMMIT;
