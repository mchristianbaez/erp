CREATE TABLE XXWC.XXWC_ENABLE_INV_ORG_LIST
(
  ORGANIZATION_CODE  VARCHAR2(3 BYTE),
  ENABLE_FLAG        VARCHAR2(1 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


