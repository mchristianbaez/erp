create global temporary table xxwc.xxwc_bod_detail_temp
(organization_id    NUMBER
,inventory_item_id  NUMBER
,transaction_id     NUMBER
,transfer_transaction_id    NUMBER
,transfer_organization_id NUMBER
,transaction_date    DATE
,transaction_type_id NUMBER
,sum_pos_qty NUMBER
,sum_neg_qty NUMBER
,allocated_qty NUMBER)
ON COMMIT PRESERVE ROWS;