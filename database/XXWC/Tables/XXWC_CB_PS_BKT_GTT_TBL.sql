DROP TABLE xxwc.xxwc_cb_ps_bkt_gtt_tbl;

CREATE GLOBAL TEMPORARY TABLE xxwc.xxwc_cb_ps_bkt_gtt_tbl (customer_id   NUMBER
                                               , "current"       NUMBER
                                               , "1_30"          NUMBER
                                               , "31_60"         NUMBER
                                               , "61_90"         NUMBER
                                               , "91_plus"       NUMBER
                                               , "31_plus"       NUMBER
                                               , "all"           NUMBER
                                               )
ON COMMIT PRESERVE ROWS
NOCACHE;