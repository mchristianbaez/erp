/*************************************************************************
  $Header ALTER_XXWC_OM_CONTRACT_PRICING_LINES.sql $
  Module Name: ALTER_XXWC_OM_CONTRACT_PRICING_LINES.sql

  PURPOSE:   TMS#20180321-00207-Replacement Cost CSP Form Changes

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        03/21/2018  Rakesh Patel            TMS#20180321-00207-Replacement Cost CSP Form Changes
**************************************************************************/
ALTER TABLE XXWC.XXWC_OM_CONTRACT_PRICING_LINES ADD ( REPLACEMENT_COST   NUMBER                                              
								         );

ALTER TABLE XXWC.XXWC_OM_CSP_LINES_ARCHIVE ADD ( REPLACEMENT_COST   NUMBER
								         );								         
								         