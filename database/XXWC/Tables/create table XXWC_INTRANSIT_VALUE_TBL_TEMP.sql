--DROP TABLE XXWC.XXWC_INTRANSIT_VALUE_TBL;
CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_INTRANSIT_VALUE_TEMP
(creation_date      DATE,
created_by          NUMBER,
last_update_date    DATE,
last_updated_by     NUMBER,
request_id          NUMBER,
cutoff_date         DATE,
organization_code   VARCHAR2(3),
segment1            VARCHAR2(80),
organization_id     NUMBER,
inventory_item_id   NUMBER,
qty_source          NUMBER,
uom_code            VARCHAR2(3),
int_qty             NUMBER,
int_value           NUMBER
)
ON COMMIT PRESERVE ROWS;