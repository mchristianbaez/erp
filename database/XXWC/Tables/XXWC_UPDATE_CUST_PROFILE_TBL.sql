CREATE TABLE XXWC.XXWC_UPDATE_CUST_PROFILE_TBL
(
  CUST_ACCOUNT_PROFILE_ID  NUMBER,
  COLLECTOR_ID             NUMBER,
  CREDIT_ANALYST_ID        NUMBER,
  STATUS                   VARCHAR2(1 BYTE),
  ERROR_MESSAGE            VARCHAR2(2000 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


