/* Formatted on 07-Mar-2014 15:51:31 (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_PO_ACCEPTANCE_LINES
-- Generated 07-Mar-2014 15:51:21 from XXWC@EBIZFQA

CREATE TABLE xxwc.xxwc_po_acceptance_lines
(
    acc_detail_id           NUMBER NOT NULL
   ,acc_header_id           NUMBER
   ,assigned_id             VARCHAR2 (2000 BYTE)
   ,quantity_ordered        VARCHAR2 (2000 BYTE)
   ,po_unit_of_measure      VARCHAR2 (2000 BYTE)
   ,unit_price              VARCHAR2 (2000 BYTE)
   ,white_cap_item_number   VARCHAR2 (2000 BYTE)
   ,vendor_item_number      VARCHAR2 (2000 BYTE)
   ,upc_number              VARCHAR2 (2000 BYTE)
   ,item_description        VARCHAR2 (2000 BYTE)
   ,line_item_status_code   VARCHAR2 (2000 BYTE)
   ,quantity                VARCHAR2 (2000 BYTE)
   ,ack_unit_of_measure     VARCHAR2 (2000 BYTE)
   ,scheduled_ship_date     DATE
   ,CONSTRAINT xxwc_po_acceptance_lines_pk PRIMARY KEY
        (acc_detail_id)
        USING INDEX PCTFREE 10
                    INITRANS 2
                    MAXTRANS 255
                    TABLESPACE xxwc_data
                    STORAGE (INITIAL 65536
                             NEXT 1048576
                             MINEXTENTS 1
                             MAXEXTENTS 2147483645)
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
NOPARALLEL
LOGGING
/

-- End of DDL Script for Table XXWC.XXWC_PO_ACCEPTANCE_LINES
