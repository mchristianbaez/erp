CREATE TABLE XXWC.XXWC_CUST_XREF
(
  ACCOUNT_NUMBER  VARCHAR2(30 BYTE)             NOT NULL,
  ATTRIBUTE6      VARCHAR2(150 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


