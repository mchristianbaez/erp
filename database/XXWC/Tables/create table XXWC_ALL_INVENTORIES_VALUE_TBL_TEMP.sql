--DROP TABLE XXWC.XXWC_ALL_INVENTORIES_VALUE_TBL;
CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_ALL_INVENTORIES_VALUE_TMP
(creation_date      DATE,
created_by          NUMBER,
last_update_date    DATE,
last_updated_by     NUMBER,
request_id          NUMBER,
cutoff_date         DATE,
organization_code   VARCHAR2(3),
segment1            VARCHAR2(80),
organization_id     NUMBER,
inventory_item_id   NUMBER,
qty_source          NUMBER,
uom_code            VARCHAR2(3),
stk_qty             NUMBER,
int_qty             NUMBER,
rcv_qty             NUMBER,
stk_value           NUMBER,
int_value           NUMBER,
rcv_value           NUMBER,
ext_value           NUMBER
)
ON COMMIT PRESERVE ROWS;