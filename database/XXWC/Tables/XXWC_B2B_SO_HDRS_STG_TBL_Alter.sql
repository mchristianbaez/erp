/*************************************************************************************************
$Header XXWC_B2B_SO_HDRS_STG_TBL.sql $

Module Name: XXWC_B2B_SO_HDRS_STG_TBL

PURPOSE: Table to maintain Customer B2B PO Information.

REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------------------------------------
      1.0  06/11/2015   Gopi Damuluri         TMS# 20150615-00088 
                                              Additional columns for B2B Maintenance Form.
**************************************************************************************************/

ALTER TABLE XXWC.XXWC_B2B_SO_HDRS_STG_TBL ADD   (DELIVER_POA         VARCHAR2(1    BYTE)          DEFAULT 'N');

/