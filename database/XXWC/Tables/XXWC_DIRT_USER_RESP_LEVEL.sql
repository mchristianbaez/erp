/* Formatted on 3/3/2014 2:26:22 AM (QP5 v5.206) */
--
-- XXWC_DIRT_USER_RESP_LEVEL  (Table)
--
--drop table XXWC.XXWC_DIRT_USER_RESP_LEVEL

CREATE TABLE xxwc.xxwc_dirt_user_resp_level
(
    responsibility_key   VARCHAR2 (122 BYTE)
   ,resp_level           NUMBER
);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_FAB_USER', 11);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_ORDER_MGMT_PRICING_FULL', 8);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_ORDER_MGMT_PRICING_LTD', 10);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_ORDER_MGMT_PRICING_STD', 9);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_ORDER_MGMT_PRICING_SUPER', 7);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_PURCHASING_BUYER', 5);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_BUYER_BASE', 6);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_PURCHASING_MGR', 3);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_PURCHASING_RPM', 4);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_PURCHASING_SR_MRG_WC', 2);

INSERT INTO xxwc.xxwc_dirt_user_resp_level (responsibility_key, resp_level)
     VALUES ('XXWC_PUR_SUPER_USER', 1);

INSERT INTO xxwc.xxwc_dirt_user_resp_level
     VALUES ('XXWC_DEPOT_REPAIR_WC', 12);

COMMIT;

