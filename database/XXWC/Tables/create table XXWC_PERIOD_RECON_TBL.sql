--drop table XXWC.XXWC_PERIOD_RECON_TBL;
CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_PERIOD_RECON_TEMP
(creation_date      DATE,
created_by          NUMBER,
last_update_date    DATE,
last_updated_by     NUMBER,
request_id          NUMBER,
period              VARCHAR2(10),
organization_code   VARCHAR2(3),
segment1            VARCHAR2(80),
organization_id     NUMBER,
inventory_item_id   NUMBER,
accounted_value     NUMBER,
onhand_value        NUMBER,
variance_value      NUMBER)
ON COMMIT PRESERVE ROWS;