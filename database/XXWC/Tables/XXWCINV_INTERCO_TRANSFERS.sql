CREATE TABLE XXWC.XXWCINV_INTERCO_TRANSFERS
(
  TRANSACTION_ID      NUMBER,
  INVENTORY_ITEM_ID   NUMBER,
  ORGANIZATION_ID     NUMBER,
  SUBINVENTORY_CODE   VARCHAR2(10 BYTE),
  LOCATOR_ID          NUMBER,
  REVISION            VARCHAR2(3 BYTE),
  ORIG_DATE_RECEIVED  DATE,
  QUANTITY            NUMBER,
  CREATION_DATE       TIMESTAMP(6)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


