/*************************************************************************
  $Header XXWC_OM_PQC_PICKING_TBL.sql $
  Module Name: XXWC_OM_PQC_PICKING_TBL.sql

  PURPOSE:   Created New table

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        12/11/2017  Ram Talluri          TMS#20171011-00108 - Picking QC
                                              project - EBS - order information
   									          to APEX & Pick ticket changes -
                                              Initial Version
**************************************************************************/
--DROP TABLE XXWC.XXWC_OM_PQC_PICKING_TBL CASCADE CONSTRAINTS;
CREATE TABLE XXWC.XXWC_OM_PQC_PICKING_TBL
(
  HEADER_ID              NUMBER,
  ORDER_NUMBER           NUMBER,
  PICK_TICKET_VERSION    NUMBER,  
  CUSTOMER_NAME          VARCHAR2(2000 BYTE),
  CUSTOMER_NUMBER        NUMBER,
  CUST_ACCOUNT_ID        NUMBER,
  SHIP_FROM_ORG_ID       NUMBER,
  SHIP_FROM_ORG_CODE     VARCHAR2(3),
  REQUEST_DATE           DATE,
  FREIGHT_CARRIER_CODE  VARCHAR2(30 BYTE),
  CUSTOMER_CITY          VARCHAR2(2000),
  TOTAL_NUMBER_OF_LINES  NUMBER,
  NUMBER_OF_LINES_PULLED NUMBER,
  PICK_STATUS            VARCHAR2(30 BYTE),  
  PICKED_BY_USER_NTID    VARCHAR2(30 BYTE),  
  PICKED_BY_USER_NAME    VARCHAR2(200 BYTE),
  PICK_START_DATE        DATE,
  PICK_COMPLETION_DATE   DATE,
  CREATION_DATE          DATE,
  CREATED_BY             NUMBER,
  LAST_UPDATE_DATE       DATE,
  LAST_UPDATED_BY        NUMBER,
  REQUEST_ID             NUMBER
)
/
CREATE INDEX XXWC.XXWC_OM_PQC_PICKING_HDR_ID ON XXWC.XXWC_OM_PQC_PICKING_TBL(HEADER_ID)
/
DROP SYNONYM XXWC_OM_PQC_PICKING_TBL
/
CREATE OR REPLACE SYNONYM APPS.XXWC_OM_PQC_PICKING_TBL FOR XXWC.XXWC_OM_PQC_PICKING_TBL
/
GRANT SELECT, INSERT, UPDATE, DELETE ON APPS.XXWC_OM_PQC_PICKING_TBL TO EA_APEX
/