/*********************************************************************************
-- Table Name XXWC_AIS_LITE_GTT_TBL
-- *******************************************************************************
--
-- PURPOSE: Global temporary table used by AIS Lite form
-- HISTORY
-- ===========================================================================
-- ===========================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- -------------------------------------
-- 1.0     15-Dec-2015   Gopi Damuluri   TMS# 20151215-00085 - Global temporary 
--                                       table used by AIS Lite form
*******************************************************************************/

CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_AIS_LITE_GTT_TBL
(OPERATION                VARCHAR2(50 BYTE),
HEADER_ID                 NUMBER,
INVENTORY_ITEM_ID         NUMBER,
ORGANIZATION_ID           NUMBER,
ORDERED_QUANTITY          NUMBER)
ON COMMIT PRESERVE ROWS RESULT_CACHE (MODE DEFAULT) NOCACHE;
/