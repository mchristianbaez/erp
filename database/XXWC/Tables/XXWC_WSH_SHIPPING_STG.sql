--
-- XXWC_WSH_SHIPPING_STG  (Table) 
--
CREATE TABLE XXWC.XXWC_WSH_SHIPPING_STG
(
  HEADER_ID           NUMBER                        NULL,
  LINE_ID             NUMBER                        NULL,
  DELIVERY_ID         NUMBER                        NULL,
  DELIVERY_DETAIL_ID  NUMBER                        NULL,
  INVENTORY_ITEM_ID   NUMBER                        NULL,
  SHIP_FROM_ORG_ID    NUMBER                        NULL,
  ORDERED_QUANTITY    NUMBER                        NULL,
  FORCE_SHIP_QTY      NUMBER                        NULL,
  TRANSACTION_QTY     NUMBER                        NULL,
  LOT_NUMBER          VARCHAR2(80 BYTE)             NULL,
  RESERVATION_ID      NUMBER                        NULL,
  STATUS              VARCHAR2(240 BYTE)            NULL,
  CREATED_BY          NUMBER                        NULL,
  CREATION_DATE       DATE                          NULL,
  LAST_UPDATED_BY     NUMBER                        NULL,
  LAST_UPDATE_DATE    DATE                          NULL,
  LAST_UPDATE_LOGIN   NUMBER                        NULL,
  REQUEST_ID          NUMBER                        NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


--
-- XXWC_WSH_SHIPPING_STG_N1  (Index) 
--
CREATE INDEX XXWC.XXWC_WSH_SHIPPING_STG_N1 ON XXWC.XXWC_WSH_SHIPPING_STG
(HEADER_ID, LINE_ID)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


--
-- XXWC_WSH_SHIPPING_STG_N2  (Index) 
--
CREATE INDEX XXWC.XXWC_WSH_SHIPPING_STG_N2 ON XXWC.XXWC_WSH_SHIPPING_STG
(DELIVERY_ID)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


--
-- XXWC_WSH_SHIPPING_STG  (Synonym) 
--
CREATE SYNONYM APPS.XXWC_WSH_SHIPPING_STG FOR XXWC.XXWC_WSH_SHIPPING_STG;


