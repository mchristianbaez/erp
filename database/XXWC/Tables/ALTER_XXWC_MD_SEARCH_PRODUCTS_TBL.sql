/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header Alter XXWC_MD_SEARCH_PRODUCTS_TBL $
  Module Name: XXWC.XXWC_MD_SEARCH_PRODUCTS_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     10-Oct-2016   Pahwa, Nancy                Initially Created TMS# 20160801-00182  
**************************************************************************/
alter table xxwc.XXWC_MD_SEARCH_PRODUCTS_TBL add dummy2 CHAR(1);
alter table xxwc.XXWC_MD_SEARCH_PRODUCTS_TBL add partnumber2 VARCHAR2(40);