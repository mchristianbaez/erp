/* Formatted on 2/1/2013 10:49:55 AM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_EDI_INBOUND_FILES_HISTORY
-- Generated 2/1/2013 10:49:45 AM from XXWC@EBIZFQA

CREATE TABLE xxwc.xxwc_edi_inbound_files_history
(
    file_name          VARCHAR2 (128 BYTE)
   ,insert_date        DATE
   ,process_date       DATE
   ,process_flag       VARCHAR2 (64 BYTE)
   ,request_id         NUMBER
   ,return_status      VARCHAR2 (164 BYTE)
   ,file_permissions   VARCHAR2 (50 BYTE)
   ,file_type          VARCHAR2 (64 BYTE)
   ,file_owner         VARCHAR2 (64 BYTE)
   ,file_group         VARCHAR2 (64 BYTE)
   ,file_size          VARCHAR2 (64 BYTE)
   ,file_month         VARCHAR2 (64 BYTE)
   ,file_day           VARCHAR2 (64 BYTE)
   ,file_time          VARCHAR2 (64 BYTE)
   ,error_message      VARCHAR2 (2048 BYTE)
   ,log_file           CLOB
   ,child_request_id   NUMBER
   ,email_sent_date    DATE
   ,vendor_number      VARCHAR2 (64 BYTE)
   ,vendor_name        VARCHAR2 (128 BYTE)
   ,invoice_number     VARCHAR2 (64 BYTE)
   ,invoice_date       VARCHAR2 (64 BYTE)
   ,po_number          VARCHAR2 (64 BYTE)
   ,invoice_amount     VARCHAR2 (64 BYTE)
   ,cp_log_file        CLOB
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
LOB ("LOG_FILE") STORE AS sys_lob0000660814c00016$$
    (TABLESPACE xxwc_data
     STORAGE (INITIAL 65536
              NEXT 1048576
              MINEXTENTS 1
              MAXEXTENTS 2147483645)
     NOCACHE LOGGING
     CHUNK 8192)
LOB ("CP_LOG_FILE") STORE AS sys_lob0000660814c00025$$
    (TABLESPACE xxwc_data
     STORAGE (INITIAL 65536
              NEXT 1048576
              MINEXTENTS 1
              MAXEXTENTS 2147483645)
     NOCACHE LOGGING
     CHUNK 8192)
NOPARALLEL
NOLOGGING
/

-- Grants for Table
GRANT SELECT ON xxwc.xxwc_edi_inbound_files_history TO interface_apexwc
/
GRANT SELECT ON xxwc.xxwc_edi_inbound_files_history TO apps WITH GRANT OPTION
/

-- End of DDL Script for Table XXWC.XXWC_EDI_INBOUND_FILES_HISTORY
