 /********************************************************************************
  FILE NAME: "XXWC"."XXWC_AP_AHH_VENDOR_SITES_EXT_T" 

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE XXWC.XXWC_AP_AHH_VENDOR_SITES_EXT_T 
   (  ahh_vendor_num varchar2(100), 
ahh_vendor_site varchar2(30), 
oracle_vendor varchar2(10), 
oracle_vendor_site varchar2(30)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AP_AHH_VENDOR_SITES_REF.bad'
    DISCARDFILE 'XXWC_AP_AHH_VENDOR_SITES_REF.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AP_AHH_VENDOR_SITES_REF.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;