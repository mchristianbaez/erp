  /*******************************************************************************
  Table: XXWC_B2B_POD_SO_INFO_TBL
  Description: This table is used to maintain Trading Partner information for B2B Integration
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     21-Jan-2016        Gopi Damuluri   TMS# 20160207-00005 Performance Tuning
  ********************************************************************************/
CREATE TABLE XXWC.XXWC_PRICING_SEG_UPD_GTT_TBL
(
  SEG_LIST_HEADER_ID  NUMBER,
  CSP_LIST_HEADER_ID  NUMBER,
  NAME                VARCHAR2(240 BYTE),
  LIST_LINE_ID        NUMBER,
  AGREEMENT_ID        NUMBER,
  CUSTOMER_ID         NUMBER,
  CUSTOMER_SITE_ID    NUMBER,
  MODIFIER_TYPE       VARCHAR2(200 BYTE),
  UPDATE_LEVEL        VARCHAR2(1 BYTE),
  STATUS              VARCHAR2(20 BYTE),
  SCENARIO            VARCHAR2(20 BYTE)
);