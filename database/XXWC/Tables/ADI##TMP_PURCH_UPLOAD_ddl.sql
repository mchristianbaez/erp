/* Formatted on 4/3/2013 5:50:42 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.ADI##TMP_PURCH_UPLOAD
-- Generated 4/3/2013 5:50:32 PM from XXWC@EBIZFQA
DROP TABLE xxwc.adi##tmp_purch_upload;
CREATE GLOBAL TEMPORARY TABLE xxwc.adi##tmp_purch_upload
(
    org                 VARCHAR2 (3 BYTE)
   ,p_org_code          VARCHAR2 (124 BYTE)
   ,item_number         VARCHAR2 (40 BYTE)
   ,p_item_number       VARCHAR2 (124 BYTE)
   ,sourcing_rule       VARCHAR2 (4000 BYTE)
   ,p_sourcing_rule     VARCHAR2 (124 BYTE)
   ,source_org          VARCHAR2 (4000 BYTE)
   ,p_source_org        VARCHAR2 (124 BYTE)
   ,source_type         VARCHAR2 (80 BYTE) NOT NULL
   ,p_source_type       VARCHAR2 (124 BYTE)
   ,pplt                NUMBER
   ,p_pplt              VARCHAR2 (124 BYTE)
   ,plt                 NUMBER
   ,p_plt               VARCHAR2 (124 BYTE)
   ,flm                 NUMBER
   ,p_flm               VARCHAR2 (124 BYTE)
   ,classification      VARCHAR2 (4000 BYTE)
   ,p_classification    VARCHAR2 (124 BYTE)
   ,planning_method     VARCHAR2 (80 BYTE) NOT NULL
   ,p_planning_method   VARCHAR2 (124 BYTE)
   ,MIN                 NUMBER
   ,p_min               VARCHAR2 (124 BYTE)
   ,MAX                 NUMBER
   ,p_max               VARCHAR2 (124 BYTE)
   ,p_flag              VARCHAR2 (4000 BYTE)
   ,p_p_flag            VARCHAR2 (124 BYTE)
   ,reserve_stock       VARCHAR2 (240 BYTE)
   ,p_reserve_stock     VARCHAR2 (124 BYTE)
   ,buyer               VARCHAR2 (240 BYTE)
   ,p_buyer             VARCHAR2 (124 BYTE)
   ,amu                 VARCHAR2 (240 BYTE)
   ,p_amu               VARCHAR2 (124 BYTE)
   ,RUN_UNIQ             VARCHAR2 (124 BYTE)
)
ON COMMIT PRESERVE ROWS
/

-- End of DDL Script for Table XXWC.ADI##TMP_PURCH_UPLOAD
