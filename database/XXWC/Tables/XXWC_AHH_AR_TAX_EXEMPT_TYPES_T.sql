  /********************************************************************************
  FILE NAME: XXWC_AHH_AR_TAX_EXEMPT_TYPES_T.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Archive Table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)         DESCRIPTION
  ------- -----------   ----------------  ----------------------------------------------------
  1.0     07/09/2018    Vamshi Palavarapu Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- Drop Table
-- DROP TABLE XXWC.XXWC_AHH_AR_TAX_EXEMPT_TYPES_T;

-- Create table
CREATE TABLE XXWC.XXWC_AHH_AR_TAX_EXEMPT_TYPES_T
(
  CUST_NUM           VARCHAR2(20),
  SHIP_TO_SITE       VARCHAR2(50),
  ADDRESS1           VARCHAR2(150),
  ADDRESS2           VARCHAR2(150),
  ADDRESS3           VARCHAR2(150),
  CITY               VARCHAR2(20),
  STATE              VARCHAR2(2),
  ZIPCODE            VARCHAR2(20),
  TAXING_STATE       VARCHAR2(2),
  TAXABLE            VARCHAR2(1),
  TAX_CER            VARCHAR2(150),
  NON_TAX_REASON     VARCHAR2(150),
  ORA_TAX_EXEMP_TYPE VARCHAR2(150),
  SHIP_TO_STATE      VARCHAR2(240)
);

GRANT ALL ON XXWC.XXWC_AHH_AR_TAX_EXEMPT_TYPES_T TO EA_APEX;
/