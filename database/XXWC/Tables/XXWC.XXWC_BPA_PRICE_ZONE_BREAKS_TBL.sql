CREATE TABLE XXWC.XXWC_BPA_PRICE_ZONE_BREAKS_TBL
(
  CREATION_DATE              DATE               NOT NULL,
  CREATED_BY                 NUMBER             NOT NULL,
  LAST_UPDATE_DATE           DATE               NOT NULL,
  LAST_UPDATED_BY            NUMBER             NOT NULL,
  PO_HEADER_ID               NUMBER             NOT NULL,
  INVENTORY_ITEM_ID          NUMBER             NOT NULL,
  PRICE_ZONE                 NUMBER             NOT NULL,
  PRICE_ZONE_QUANTITY        NUMBER             NOT NULL,
  PRICE_ZONE_QUANTITY_PRICE  NUMBER             NOT NULL
);


