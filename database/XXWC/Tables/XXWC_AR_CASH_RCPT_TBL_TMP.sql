  /****************************************************************************************************
  Table: XXWC_AR_CASH_RCPT_TBL_TMP
  Description: whitecap Cash receipts data
  HISTORY
  File Name: XXWC_AR_CASH_RCPT_TBL_TMP.sql
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     28-NOV-2016        Niraj K Ranjan    
  *****************************************************************************************************/
CREATE TABLE XXWC.XXWC_AR_CASH_RCPT_TBL_TMP
AS
   SELECT ROWID CASH_ROWID, CASH_RECEIPT_ID, ATTRIBUTE1 OLD_URL
     FROM AR.AR_CASH_RECEIPTS_ALL
    WHERE 1=2
/
ALTER TABLE xxwc.XXWC_AR_CASH_RCPT_TBL_TMP ADD (NEW_URL VARCHAR2(500))
/
CREATE INDEX XXWC.XXWC_AR_CASH_RCPT_TBL_TMP_IDX ON XXWC.XXWC_AR_CASH_RCPT_TBL_TMP(OLD_URL)
/
CREATE INDEX XXWC.XXWC_AR_CASH_RCPT_TBL_TMP_U1 ON XXWC.XXWC_AR_CASH_RCPT_TBL_TMP(CASH_RECEIPT_ID)
/
EXECUTE DBMS_STATS.gather_table_stats('XXWC', 'XXWC_AR_CASH_RCPT_TBL_TMP');
