  /*******************************************************************************
  Table: XXWC.XXWC_DMS_DOCLINK_TBL
  Description: This table is used to maintain DMS DocLink information
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-Dec-2015        Gopi Damuluri   TMS# 20160120-00169 - B2B POD Enhancement
  ********************************************************************************/
DROP TABLE xxwc.xxwc_dms_doclink_tbl;
/

CREATE TABLE xxwc.xxwc_dms_doclink_tbl
( ID                  NUMBER,
  REQUEST_ID          NUMBER,
  DELIVERY_ID         NUMBER,
  HEADER_ID           NUMBER
);
