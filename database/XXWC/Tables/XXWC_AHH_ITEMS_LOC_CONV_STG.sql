  /********************************************************************************
  FILE NAME: XXWC_AHH_ITEMS_LOC_CONV_STG.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: AHH Items Locators Conversion Staging table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- DROP TABLE
DROP TABLE XXWC.XXWC_AHH_ITEMS_LOC_CONV_STG;

-- CREATE TABLE
CREATE TABLE XXWC.XXWC_AHH_ITEMS_LOC_CONV_STG
(
  BATCH_ID              NUMBER,
  ORGANIZATION_CODE     VARCHAR2(3),
  SEGMENT1              VARCHAR2(30),
  AHH_PARTNUMBER        VARCHAR2(100),
  AHH_ORGANIZATION_CODE VARCHAR2(10),
  ITEM_LOCATION         VARCHAR2(100),
  INVENTORY_ITEM_ID     NUMBER,
  ORGANIZATION_ID       NUMBER,
  PROCESSED_DATE        DATE,
  PROCESSED_BY          VARCHAR2(30),
  PROCESS_STATUS        VARCHAR2(3),
  STATUS                VARCHAR2(1),
  ERROR_MESSAGE         VARCHAR2(2000),
  BIN_NUMBER            NUMBER
);
-- CREATE/RECREATE INDEXES 
CREATE INDEX XXWC.XXWC_AHH_ITEMS_LOC_CONV_STG_N1 ON XXWC.XXWC_AHH_ITEMS_LOC_CONV_STG (BATCH_ID, STATUS);
CREATE INDEX XXWC.XXWC_AHH_ITEMS_LOC_CONV_STG_N2 ON XXWC.XXWC_AHH_ITEMS_LOC_CONV_STG (INVENTORY_ITEM_ID, ORGANIZATION_ID);
