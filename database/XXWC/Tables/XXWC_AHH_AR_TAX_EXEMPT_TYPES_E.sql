 /********************************************************************************
  FILE NAME: XXWC_AHH_AR_TAX_EXEMPT_TYPES_E.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    P.Vamshidhar  TMS#20180709-00089 - AHH Customer Conversion External Tables.
  ********************************************************************************/
CREATE TABLE XXWC.XXWC_AHH_AR_TAX_EXEMPT_TYPES_E
(
  CUST_NUM            VARCHAR2(20 BYTE),
  SHIP_TO_SITE        VARCHAR2(50 BYTE),
  ADDRESS1            VARCHAR2(150 BYTE),
  ADDRESS2            VARCHAR2(150 BYTE),
  ADDRESS3            VARCHAR2(150 BYTE),
  CITY                VARCHAR2(20 BYTE),
  STATE               VARCHAR2(2 BYTE),
  ZIPCODE             VARCHAR2(20 BYTE),
  TAXING_STATE        VARCHAR2(2 BYTE),
  TAXABLE             VARCHAR2(1 BYTE),
  TAX_CER             VARCHAR2(150 BYTE),
  NON_TAX_REASON      VARCHAR2(150 BYTE),
  ORA_TAX_EXEMP_TYPE  VARCHAR2(150 BYTE),
  SHIP_TO_STATE       VARCHAR2(240 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_AR_AHH_CASH_RCPT_CONV_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AHH_AR_TAX_EXEMPT_TYPES.bad'
    DISCARDFILE 'XXWC_AHH_AR_TAX_EXEMPT_TYPES.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                 )
     LOCATION (XXWC_AR_AHH_CASH_RCPT_CONV_DIR:'XXWC_AHH_AR_TAX_EXEMPT_TYPES.csv')
  )
REJECT LIMIT UNLIMITED
NOPARALLEL
NOMONITORING;
/
GRANT SELECT ON XXWC.XXWC_AHH_AR_TAX_EXEMPT_TYPES_E TO EA_APEX;
/