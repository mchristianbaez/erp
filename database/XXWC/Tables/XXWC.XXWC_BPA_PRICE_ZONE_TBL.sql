CREATE TABLE XXWC.XXWC_BPA_PRICE_ZONE_TBL
  (creation_date          DATE   NOT NULL
  ,created_by             NUMBER NOT NULL
  ,last_update_date       DATE   NOT NULL
  ,last_updated_by        NUMBER NOT NULL
  ,po_header_id           NUMBER NOT NULL
  ,inventory_item_id      NUMBER NOT NULL
  ,price_zone             NUMBER NOT NULL
  ,price_zone_price       NUMBER NOT NULL
  );