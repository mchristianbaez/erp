DROP TABLE xxwc.xxwc_cb_ps_bal_gtt_tbl;

CREATE GLOBAL TEMPORARY TABLE xxwc.xxwc_cb_ps_bal_gtt_tbl (customer_id   NUMBER
                                        , bucket        VARCHAR2(30)
                                        , balance       NUMBER)
ON COMMIT PRESERVE ROWS
NOCACHE;