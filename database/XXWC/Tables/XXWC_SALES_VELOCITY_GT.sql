CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_SALES_VELOCITY_GT
(
  INVENTORY_ITEM_ID               NUMBER,
  SEGMENT1                        VARCHAR2(40 BYTE),
  ORGANIZATION_ID                 NUMBER,
  SALES_VELOCITY_COUNT            NUMBER,
  ANNUAL_SALES_VELOCITY_COUNT     NUMBER,
  DC_SALES_VELOCITY_COUNT         NUMBER,
  DC_ANNUAL_SALES_VELOCITY_COUNT  NUMBER,
  ITEM_STATUS_CODE                VARCHAR2(30 BYTE),
  SALES_VELOCITY_CATEGORY         VARCHAR2(1 BYTE),
  OLD_CATEGORY_ID                 NUMBER,
  NEW_CATEGORY_ID                 NUMBER
)
ON COMMIT PRESERVE ROWS;


