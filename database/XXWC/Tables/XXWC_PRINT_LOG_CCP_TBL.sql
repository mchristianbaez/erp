drop table XXWC.XXWC_PRINT_LOG_CCP_TBL;


create table XXWC.XXWC_PRINT_LOG_CCP_TBL
   (application_id          NUMBER,
    concurrent_program_id   NUMBER,      
    header_id               NUMBER,
    organization_id         NUMBER,
    table_name              VARCHAR2(30));

CREATE UNIQUE INDEX XXWC.XXWC_PRINT_LOG_CCP_U1
    on XXWC.XXWC_PRINT_LOG_CCP_TBL (APPLICATION_ID, CONCURRENT_PROGRAM_ID);

grant all on XXWC.XXWC_PRINT_LOG_CCP_TBL to SYS;


delete XXWC.XXWC_PRINT_LOG_CCP_TBL;

select fcp.CONCURRENT_PROGRAM_NAME,
       x1.*
from   XXWC.XXWC_PRINT_LOG_CCP_TBL x1,
       fnd_concurrent_programs fcp
where  fcp.APPLICATION_ID = x1.APPLICATION_ID
and    fcp.CONCURRENT_PROGRAM_ID = x1.CONCURRENT_PROGRAM_ID

select *
FROM   fnd_concurrent_programs
where  concurrent_program_name = 'XXWC_OM_WSHBOL_NEW';

   
---Quote Detail
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,66413
   ,1
   ,NULL
   ,'OE_ORDER_HEADERS_ALL');
   

--Pick Ticket   
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,75400
   ,2
   ,1
   ,'OE_ORDER_HEADERS_ALL');

--Packing Slip   
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,70401
   ,2
   ,1
   ,'OE_ORDER_HEADERS_ALL');


--Customer Sales Receipt
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,66423
   ,1
   ,NULL
   ,'OE_ORDER_HEADERS_ALL');


--Sales Receipt
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,66424
   ,1
   ,NULL
   ,'OE_ORDER_HEADERS_ALL');



--Internal Pick Ticket   
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,74402
   ,2
   ,1
   ,'OE_ORDER_HEADERS_ALL');


--Internal Pack Ticket   
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,75404
   ,2
   ,1
   ,'OE_ORDER_HEADERS_ALL');


--Rental Pick Slip Report   
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,76400
   ,2
   ,1
   ,'OE_ORDER_HEADERS_ALL');


--Rental Return Worksheet
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,66417
   ,9
   ,NULL
   ,'OE_ORDER_HEADERS_ALL');
   
--Rental Return Worksheet By Item
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,70400
   ,9
   ,NULL
   ,'OE_ORDER_HEADERS_ALL');
   
   
--Hazmat
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,71402
   ,1
   ,NULL
   ,'OE_ORDER_HEADERS_ALL');
   


SELECT *
from   XXWC.XXWC_PRINT_LOG_CCP_TBL;
