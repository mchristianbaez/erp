/*************************************************************************
  $Header XXWC_OM_PQC_LOAD_CHK_LBL_TBL.sql $
  Module Name: XXWC_OM_PQC_LOAD_CHK_LBL_TBL.sql

  PURPOSE:   Created New Global Temp table to print multiple labels from
             XXWC_OM_PQC_LOAD_CHK_LBL_TBL.rdf

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        01/10/2018  Pattabhi Avula       TMS#20171011-00108 - Picking QC
                                              project - EBS - order information
   									          to APEX & Pick ticket changes -
                                              Initial Version
**************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_OM_PQC_LOAD_CHK_LBL_TBL
( REQUEST_DATE    VARCHAR2(8),
  PREPARED_BY     VARCHAR2(50),
  CHECKED_BY      VARCHAR2(50),
  CUSTOMER        VARCHAR2(100),
  CITY            VARCHAR2(50),
  ORDER_NUMBER    NUMBER,
  PALLETS         NUMBER,
  BUNDLES         NUMBER,
  TOOLS           NUMBER,
  UNITS           NUMBER,
  NUM_OF_LBLS     NUMBER,
  NUM_OF_LABLES   NUMBER,
  STATUS_FLAG     VARCHAR2(10),
  PRINT_DATE      VARCHAR2(8)
) ON COMMIT PRESERVE ROWS
/
