/*************************************************************************************************
$Header XXWC_INV_SPECIAL_ITEMS_TBL.sql $

Module Name: XXWC.XXWC_INV_SPECIAL_ITEMS_TBL

PURPOSE: Load the final CSP and Special Items for Inactivation through Item Import

REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------------------------------------
1.0        01/21/2016   Kishorebabu V         TMS# 20150928-00127 PLM - Retirement process
                                              Added additional columns
**************************************************************************************************/

ALTER TABLE XXWC.XXWC_INV_SPECIAL_ITEMS_TBL ADD (REQ_EXISTS      VARCHAR2(1 BYTE)
                                                ,WEBSITE_EXISTS  VARCHAR2(1 BYTE)
												,ONE_YEAR_EXISTS VARCHAR2(1 BYTE))
/