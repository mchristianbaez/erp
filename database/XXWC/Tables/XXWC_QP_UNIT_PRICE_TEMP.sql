-- used to hold updated list price information for price list extracts
-- used by XXWC_QP_EXTRACTS_PKG to hold update list price information for DELTA extracts
Create global temporary table XXWC.XXWC_QP_UNIT_PRICE_TEMP
(inventory_item_id         NUMBER
,organization_id           NUMBER
,item_number               VARCHAR2(40)
,description               VARCHAR2(2000)
,unit_price                NUMBER
)
ON COMMIT PRESERVE ROWS
/