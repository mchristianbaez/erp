   /*************************************************************************
   *   $Header XXWC_OE_AIS_LPP_TBL.sql $
   *   Module Name: XXWC_OE_AIS_LPP_TBL
   *
   *   PURPOSE:   To store the last price paid 
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        17-May-2015  Rakesh Patel          Initial Version
   *                                                 TMS#20160516-00102 -LPP Performance Improvement
   * ***************************************************************************/
  
--drop table xxwc.XXWC_OE_AIS_LPP_TBL;
   
CREATE TABLE "XXWC"."XXWC_OE_AIS_LPP_TBL"
(
   "INVENTORY_ITEM_ID"       	   NUMBER,      
   "SHIP_TO_ORG_ID"                NUMBER,
   "LAST_PRICE_PAID"               NUMBER,
   "LINE_CREATION_DATE"        	   DATE,
   "LAST_PRICE_PAID_EXCL_FLAG"     VARCHAR2(1),
   "CREATED_BY"              	   NUMBER,
   "LAST_UPDATED_BY"         	   NUMBER,
   "CREATION_DATE"           	   DATE,
   "LAST_UPDATE_DATE"        	   DATE,
   "LAST_UPDATE_LOGIN"       	   NUMBER
)
SEGMENT CREATION DEFERRED
PCTFREE 10
PCTUSED 40
INITRANS 1
MAXTRANS 255
NOCOMPRESS
LOGGING
TABLESPACE "XXWC_DATA";