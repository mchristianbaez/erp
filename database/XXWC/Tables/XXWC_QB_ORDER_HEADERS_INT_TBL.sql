CREATE TABLE XXWC.XXWC_QB_ORDER_HEADERS_INT_TBL (QUOTE_NUMBER		NUMBER
                                             , ECOMM_ORDER_ID           NUMBER
                                             , ORDERED_DATE		DATE
                                             , PO_NUMBER		VARCHAR2(50)
                                             , CUSTOMER_NUMBER		NUMBER
                                             , CUSTOMER_SITE_NUMBER	VARCHAR2 (30)
                                             , BRANCH_NUMBER		VARCHAR2(3)
                                             , SHIPPING_METHOD		VARCHAR2(150)
                                             , SHIPPING_INSTRUCTIONS	VARCHAR2(2000)
                                             , TAX_AMOUNT		NUMBER
                                             , CONTACT_FIRST_NAME	VARCHAR2(150)
                                             , CONTACT_LAST_NAME	VARCHAR2(150)
                                             , CONTACT_NUMBER		VARCHAR2(50)
                                             , SHIPPING_CHARGES		NUMBER
                                             , CREATION_DATE		DATE
                                             , CREATED_BY		NUMBER
                                             , LAST_UPDATE_DATE		DATE
                                             , LAST_UPDATED_BY		NUMBER
                                             , PROCESS_FLAG             VARCHAR2(1)
                                             , ERROR_MESSAGE            VARCHAR2(2000)
                                             , ATTRIBUTE1		VARCHAR2(250)
                                             , ATTRIBUTE2		VARCHAR2(250)
                                             , ATTRIBUTE3		VARCHAR2(250)
                                             , ATTRIBUTE4		VARCHAR2(250)
                                             , ATTRIBUTE5		VARCHAR2(250));

GRANT ALL ON xxwc.xxwc_qb_order_headers_int_tbl TO interface_wcs;