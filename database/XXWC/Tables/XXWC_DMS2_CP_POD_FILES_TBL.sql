  /********************************************************************************
  FILE NAME: XXWC_DMS2_CP_POD_FILES_TBL.sql
  
  PROGRAM TYPE: XXWC_DMS2_CP_POD_FILES_TBL table for DMS Orders for POD
  
  PURPOSE: XXWC_DMS2_CP_POD_FILES_TBL table for DMS Orders for POD
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     09/19/2017    Rakesh P.       TMS# 20180301-00314-DMS 2.0 Pilot enhancements send POD for DMS Orders
  *******************************************************************************************/
CREATE TABLE XXWC.XXWC_DMS2_CP_POD_FILES_TBL
 (
  REQUEST_ID        NUMBER,
  DELIVERY_ID       NUMBER,
  HEADER_ID         NUMBER,
  ORDER_NUMBER      NUMBER,
  CUST_ACCOUNT_ID   NUMBER,
  SITE_USE_ID       NUMBER,
  CREATION_DATE     DATE,
  CREATED_BY        NUMBER(15),
  LAST_UPDATE_DATE  DATE,
  LAST_UPDATED_BY   NUMBER(15)
);

CREATE PUBLIC SYNONYM XXWC_DMS2_CP_POD_FILES_TBL FOR xxwc.XXWC_DMS2_CP_POD_FILES_TBL;

CREATE INDEX XXWC.XXWC_DMS2_CP_POD_FILES_TBL_N1 ON XXWC.XXWC_DMS2_CP_POD_FILES_TBL
(HEADER_ID, DELIVERY_ID, ORDER_NUMBER);      

CREATE INDEX XXWC.XXWC_DMS2_CP_POD_FILES_TBL_N2 ON XXWC.XXWC_DMS2_CP_POD_FILES_TBL
(HEADER_ID, DELIVERY_ID, ORDER_NUMBER, CUST_ACCOUNT_ID, SITE_USE_ID);
             

