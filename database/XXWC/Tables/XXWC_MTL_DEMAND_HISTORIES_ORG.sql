CREATE TABLE XXWC.XXWC_MTL_DEMAND_HISTORIES_ORG
/*************************************************************************
  $Header XXWC_MTL_DEMAND_HISTORIES_ORG.sql $
  Module Name: XXWC_MTL_DEMAND_HISTORIES_ORG

  PURPOSE: To Maintain the list or Organizations with their last run date

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        30-Apr-2015  Manjula Chellappan    Initial Version TMS # 20150302-00057

**************************************************************************/
(
  ORGANIZATION_ID            NUMBER             NOT NULL PRIMARY KEY,
  ORGANIZATION_CODE          VARCHAR2(3 BYTE),
  CALENDAR_CODE              VARCHAR2(10 BYTE),
  CALENDAR_EXCEPTION_SET_ID  NUMBER,
  LAST_RUN_DATE              DATE,
  LAST_RUN_J_DATE            NUMBER
)
/
