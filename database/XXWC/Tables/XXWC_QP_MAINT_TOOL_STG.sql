CREATE TABLE XXWC.XXWC_QP_MAINT_TOOL_STG
(
  FILE_ID                NUMBER                     NULL,
  FILE_TYPE              VARCHAR2(240 BYTE)         NULL,
  ACCESS_ID              NUMBER                     NULL,
  FILE_NAME              VARCHAR2(256 BYTE)         NULL,
  CREATED_BY             NUMBER                     NULL,
  CREATION_DATE          DATE                       NULL,
  LAST_UPDATED_BY        NUMBER                     NULL,
  LAST_UPDATE_DATE       DATE                       NULL,
  LAST_UPDATE_LOGIN      NUMBER                     NULL,
  PROCESSING_REQUEST_ID  NUMBER                     NULL,
  STATUS                 VARCHAR2(240 BYTE)         NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


