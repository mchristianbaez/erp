CREATE TABLE XXWC.XXWC_AR_PRISM_RENTAL_ORD_STG
(
  PRISM_RENTAL_ORD_ID    NUMBER,
  COMPANY                VARCHAR2(3 BYTE),
  CUSTOMER_NUMBER        NUMBER,
  MASTER_NUMBER          NUMBER,
  INVOICE_NUMBER         NUMBER,
  ORDER_NUMBER           NUMBER,
  SUB_ORDER_NUMBER       NUMBER,
  FIRST_DATE_OUT         DATE,
  FIRST_DATE_IN          DATE,
  SECOND_DATE_OUT        DATE,
  SECOND_DATE_IN         DATE,
  TIME_OUT               VARCHAR2(8 BYTE),
  TIME_IN                VARCHAR2(8 BYTE),
  MONTHS                 NUMBER,
  WEEKS                  NUMBER,
  DAYS                   NUMBER,
  STOP_DATE              DATE,
  CONVERT_DATE           DATE,
  EXPIRE_DATE            DATE,
  ANNIVERSARY_DATE       DATE,
  TIMES_INVOICED         NUMBER,
  VENDOR_RENT_RATE       NUMBER,
  VENDOR_DISCOUNT        NUMBER,
  NEXT_INVOICE_DATE      DATE,
  LAST_INVOICE_DATE      DATE,
  ESTIMATED_RETURN_DATE  DATE,
  RENTAL_TYPE            VARCHAR2(1 BYTE),
  POSTBILL_SW            VARCHAR2(1 BYTE),
  DATE_REBILLED          DATE,
  FROM_DATE              DATE,
  TO_DATE                DATE,
  RENTAL_TEXT1           VARCHAR2(64 BYTE),
  RENTAL_TEXT2           VARCHAR2(64 BYTE),
  RENTAL_TEXT3           VARCHAR2(64 BYTE),
  TRX_NUMBER             VARCHAR2(240 BYTE),
  CREATION_DATE          DATE,
  CREATED_BY             NUMBER,
  LAST_UPDATE_DATE       DATE,
  LAST_UPDATED_BY        NUMBER,
  REQUEST_ID             NUMBER,
  PROCESSING_DATE        DATE,
  ARCHIVE_DATE           DATE
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


