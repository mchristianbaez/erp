/*************************************************************************
  $Header ALTER_xxwc.XXWC_QP_MAINT_TOOL_LINE_STG.sql $
  Module Name: xxwc.XXWC_QP_MAINT_TOOL_LINE_STG.sql

  PURPOSE:   To Add the column xxwc.XXWC_QP_MAINT_TOOL_LINE_STG

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        03/05/2018  Nancy Pahwa       Task ID: 20180313-00150 Replacement cost 
**************************************************************************/
alter table xxwc.XXWC_QP_MAINT_TOOL_LINE_STG add cost_flag varchar2(10);
/

