--//============================================================================
--//
--// Object Name         :: xxwc_pdh_image_filenames_tmp
--//
--// Object Type         :: Table
--//
--// Object Description  :: This is temp table to insert data using sql loader.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     15/03/2014    Initial Build - TMS#20140207-00083
--//============================================================================
CREATE TABLE xxwc.xxwc_pdh_image_filenames_tmp(xxwc_image_filename_attr VARCHAR2(1000)
                                              ,xxwc_image_filetype_attr VARCHAR2(150)
                                              ,last_update_date         DATE);
