CREATE TABLE XXWC.XXWC_OM_UPD_DEL_SUBINV
(
  HEADER_ID     NUMBER,
  LINE_ID       NUMBER,
  SUBINVENTORY  VARCHAR2(50 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


