/******************************************************************************
   NAME:       XXWC_CSP_DM_MAINTENANCE_TAB.sql
   PURPOSE:    Table to store DM info to be maintained from CSP DM maintenance form
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        09/06/2017  Niraj K Ranjan   Initial Version TMS#20150625-00057   
                                           CSP Enhance bundle - Item #4  CSP Approval Workflow
******************************************************************************/
CREATE TABLE XXWC.XXWC_CSP_DM_MAINTENANCE_TAB
(
   REGION             VARCHAR2 (150),
   DISTRICT           VARCHAR2 (150),
   DM_NAME            VARCHAR2 (240),
   DM_NT_ID           VARCHAR2 (100), 
   DM_EMAIL_ADDRESS   VARCHAR2 (240),
   CSP_ALT_NOTIF_ADDR VARCHAR2 (2000),
   SEND_BOTH          VARCHAR2 (1),
   CREATION_DATE      DATE,
   CREATED_BY         NUMBER,
   LAST_UPDATE_DATE   DATE,
   LAST_UPDATED_BY    NUMBER,
   LAST_UPDATE_LOGIN  NUMBER
);