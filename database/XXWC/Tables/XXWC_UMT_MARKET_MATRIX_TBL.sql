  /****************************************************************************************************
  Table: XXWC_UMT_MARKET_MATRIX_TBL	
  Description: To Maintain market matrix.
  HISTORY
  File Name: XXWC_UMT_MARKET_MATRIX_TBL.sql
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     11-Feb-2016        P.Vamshidhar    Initial version TMS#20160209-00194
                                             Reporting Automation � Disposition   
  *****************************************************************************************************/

DROP TABLE XXWC.XXWC_UMT_MARKET_MATRIX_TBL;

CREATE TABLE XXWC.XXWC_UMT_MARKET_MATRIX_TBL
(
  MARKET_ID      NUMBER,
  DISTRICT_NAME  VARCHAR2(100 BYTE),
  CREATED_BY     VARCHAR2(30 BYTE),
  CREATION_DATE  DATE                           DEFAULT SYSDATE
);

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_UMT_MARKET_MATRIX_TBL TO INTERFACE_APEXWC;
/
GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_UMT_MARKET_MATRIX_TBL TO INTERFACE_DSTAGE;
/