DROP TABLE XXWC.xxwc_cb_ps_age_gtt_tbl;

CREATE GLOBAL TEMPORARY TABLE xxwc.xxwc_cb_ps_age_gtt_tbl
( customer_id            NUMBER
, age                    NUMBER
, amount_due_remaining   NUMBER
, dup_flag               VARCHAR2(1))
ON COMMIT PRESERVE ROWS
NOCACHE;