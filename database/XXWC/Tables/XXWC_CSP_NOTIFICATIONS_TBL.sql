/******************************************************************************
   NAME:       XXWC_CSP_NOTIFICATIONS_TBL.sql
   PURPOSE:    Table to store csp info to be submitted from CSP maintenance form
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        16/05/2017  Niraj K Ranjan   Initial Version TMS#20150625-00057   
                                           CSP Enhance bundle - Item #4  CSP Approval Workflow
******************************************************************************/
CREATE TABLE XXWC.XXWC_OM_CSP_NOTIFICATIONS_TBL
(AGREEMENT_ID      NUMBER,
 REVISION_NUMBER   NUMBER,
 SUBMITTED_BY      VARCHAR2(500),
 SUBMITTED_DATE    DATE,
 CUSTOMER_NUMBER   NUMBER,
 CUSTOMER_NAME     VARCHAR2(1000),
 SITE_NUMBER       VARCHAR2(50),
 SITE_INFO         VARCHAR2(100),
 LOCATION_NAME     VARCHAR2(1000),
 SALES_REP         VARCHAR2(1000),
 DISTRICT_MANAGER  VARCHAR2(1000),
 DM_REGION         VARCHAR2(100),
 AGREEMENT_STATUS  VARCHAR2(500),
 SELECTED_CSP      VARCHAR2(1),
 CREATION_DATE     DATE,
 CREATED_BY        NUMBER,
 LAST_UPDATE_DATE  DATE,
 LAST_UPDATED_BY   NUMBER,
 LAST_UPDATE_LOGIN NUMBER,
 REQUEST_ID        NUMBER,
 DEBUG_MESSAGE     VARCHAR2(4000)
 );
