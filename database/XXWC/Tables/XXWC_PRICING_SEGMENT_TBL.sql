/*************************************************************************
$Header XXWC_PRICING_SEGMENT_TBL.sql $
Module Name: XXWC_PRICING_SEGMENT_TBL.sql

PURPOSE:   This Table is used to load Segment Modifiers to Oracle

REVISIONS:
Ver        Date        Author                  Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
                                               TMS# 20140609-00256
1.1        11/23/2015  Gopi Damuluri           TMS# 20151123-00124
                                               Pricing QP Maintenance Tool changes - Modifier Header upload
****************************************************************************/

CREATE TABLE XXWC.XXWC_PRICING_SEGMENT_TBL
(
  ACCOUNT_NUMBER     VARCHAR2(30 BYTE),
  NAME               VARCHAR2(240 BYTE),
  CONTEXT            VARCHAR2(10 BYTE),
  ATTRIBUTE10        VARCHAR2(20 BYTE),
  LIST_TYPE_CODE     VARCHAR2(10 BYTE),
  AUTOMATIC_FLAG     VARCHAR2(1 BYTE),
  OPERATION          VARCHAR2(20 BYTE),
  START_DATE_ACTIVE  DATE,
  END_DATE_ACTIVE    DATE,
  STATUS             VARCHAR2(10 BYTE)          DEFAULT 'NEW',
  FILE_ID            NUMBER,
  CREATION_DATE      DATE,
  CREATED_BY         NUMBER,
  LAST_UPDATE_DATE   DATE,
  LAST_UPDATED_BY    NUMBER,
  REQUEST_ID         NUMBER
)
/

GRANT ALL ON XXWC.XXWC_PRICING_SEGMENT_TBL TO TD002849;
/

GRANT ALL ON XXWC.XXWC_PRICING_SEGMENT_TBL TO RV003897;
/