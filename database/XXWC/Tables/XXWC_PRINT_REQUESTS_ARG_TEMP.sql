CREATE TABLE XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
(
  BATCH_ID      NUMBER,
  PROCESS_FLAG  NUMBER,
  GROUP_ID      NUMBER,
  ARGUMENT      NUMBER,
  VALUE         VARCHAR2(240 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


