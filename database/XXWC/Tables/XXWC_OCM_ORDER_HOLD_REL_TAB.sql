   /******************************************************************************
      NAME:       XXWC_OCM_ORDER_HOLD_REL_TAB.sql
      PURPOSE:    Table to store no change information for a case folder

      REVISIONS:
      Ver        Date        Author               Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        01/30/2017     Niraj K Ranjan   Initial Version TMS#20150811-00089   
	                                             Credit - Remove capability of Branch 
												 Users to release Hard Hold or No Change option
   ******************************************************************************/
DROP TABLE XXWC.XXWC_OCM_ORDER_HOLD_REL_TAB;
CREATE TABLE XXWC.XXWC_OCM_ORDER_HOLD_REL_TAB(CASE_FOLDER_ID NUMBER,ORD_HEADER_ID NUMBER, NO_CHANGE_FLAG VARCHAR2(1),NO_CHANGE_REASON VARCHAR2(60));