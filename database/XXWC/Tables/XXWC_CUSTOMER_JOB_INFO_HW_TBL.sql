  /********************************************************************************
  FILE NAME: XXWC_CUSTOMER_JOB_INFO_HW_TBL.sql
  
  PROGRAM TYPE: Customer Job Info table for online sales order form
  
  PURPOSE: Customer Job Info table for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  *******************************************************************************************/
CREATE TABLE XXWC.XXWC_CUSTOMER_JOB_INFO_HW_TBL
(
  CUST_ACCOUNT_ID         NUMBER(15)               NOT NULL,
  COUNTRY                 VARCHAR2(60 BYTE) NOT NULL,
  ADDRESS1                VARCHAR2(240 BYTE) NOT NULL,
  ADDRESS2                VARCHAR2(240 BYTE),
  ADDRESS3                VARCHAR2(240 BYTE),
  ADDRESS4                VARCHAR2(240 BYTE),
  CITY                    VARCHAR2(60 BYTE),
  POSTAL_CODE             VARCHAR2(60 BYTE),
  STATE_PROVINCE          VARCHAR2(60 BYTE),
  COUNTY                  VARCHAR2(60 BYTE),
  SITE_STATUS             VARCHAR2(100 BYTE)       NOT NULL,
  HCASA_CUST_ACCT_SITE_ID NUMBER(15), 
  SHIP_TO_SITE_USE_ID     NUMBER(15),
  BILL_TO_SITE_USE_ID     NUMBER(15),
  SITE_NAME               VARCHAR2(40 BYTE), 
  PRIMARY_SALESREP_ID     NUMBER(15),
  sales_rep_name          VARCHAR2(3000 BYTE),  
  CREATION_DATE           DATE                     NOT NULL,
  CREATED_BY              NUMBER(15)               NOT NULL,
  LAST_UPDATE_DATE        DATE                     NOT NULL,
  LAST_UPDATED_BY         NUMBER(15)               NOT NULL,
  REQUEST_ID              NUMBER(15),
  ATTRIBUTE1              VARCHAR2(150 BYTE),
  ATTRIBUTE2              VARCHAR2(150 BYTE),
  ATTRIBUTE3              VARCHAR2(150 BYTE),
  ATTRIBUTE4              VARCHAR2(150 BYTE),
  ATTRIBUTE5              VARCHAR2(150 BYTE),
  ATTRIBUTE6              VARCHAR2(150 BYTE),
  ATTRIBUTE7              VARCHAR2(150 BYTE),
  ATTRIBUTE8              VARCHAR2(150 BYTE),
  ATTRIBUTE9              VARCHAR2(150 BYTE),
  ATTRIBUTE10             VARCHAR2(150 BYTE)
);

GRANT SELECT ON XXWC.XXWC_CUSTOMER_JOB_INFO_HW_TBL TO INTERFACE_MSSQL;