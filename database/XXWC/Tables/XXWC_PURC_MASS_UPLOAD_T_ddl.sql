/* Formatted on 11/2/2012 8:28:06 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_PURC_MASS_UPLOAD_T
-- Generated 11/2/2012 8:27:51 PM from XXWC@EBIZCON

CREATE TABLE xxwc_purc_mass_upload_t
(
    p_org_code           VARCHAR2 (124 BYTE)
   ,p_item_number        VARCHAR2 (124 BYTE)
   ,p_sourcing_rule      VARCHAR2 (124 BYTE)
   ,p_source_org         VARCHAR2 (124 BYTE)
   ,p_source_type        VARCHAR2 (124 BYTE)
   ,p_pplt               VARCHAR2 (124 BYTE)
   ,p_plt                VARCHAR2 (124 BYTE)
   ,p_flm                VARCHAR2 (124 BYTE)
   ,p_classification     VARCHAR2 (124 BYTE)
   ,p_planning_method    VARCHAR2 (124 BYTE)
   ,p_min                VARCHAR2 (124 BYTE)
   ,p_max                VARCHAR2 (124 BYTE)
   ,p_p_flag             VARCHAR2 (124 BYTE)
   ,p_reserve_stock      VARCHAR2 (124 BYTE)
   ,p_buyer              VARCHAR2 (124 BYTE)
   ,p_amu                VARCHAR2 (124 BYTE)
   ,insert_date          DATE
   ,error_message        CLOB
   ,process_flag         VARCHAR2 (64 BYTE)
   ,organization_id      NUMBER
   ,rows_inserted        NUMBER
   ,wakeup_rows          NUMBER
   ,start_process_date   TIMESTAMP (6)
   ,end_process_date     TIMESTAMP (6)
   ,user_id              NUMBER
   ,session_id           NUMBER
   ,resp_id              NUMBER
   ,resp_appl_id         NUMBER
   ,run_uniq             VARCHAR2 (64 BYTE)
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
LOB ("ERROR_MESSAGE") STORE AS sys_lob0000711971c00018$$
    (TABLESPACE xxwc_data
     STORAGE (INITIAL 65536
              NEXT 1048576
              MINEXTENTS 1
              MAXEXTENTS 2147483645)
     NOCACHE LOGGING
     CHUNK 8192)
PARALLEL (DEGREE 6)
NOLOGGING
/

-- End of DDL Script for Table XXWC.XXWC_PURC_MASS_UPLOAD_T
