CREATE TABLE XXWC.XXWC_GP_DMP_VAL
(
  CUSTNO   VARCHAR2(20 BYTE)                    NOT NULL,
  INVNO    VARCHAR2(20 BYTE)                    NOT NULL,
  BALANCE  FLOAT(126),
  INVDTE   DATE
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


