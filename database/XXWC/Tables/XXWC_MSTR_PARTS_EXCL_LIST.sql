CREATE TABLE XXWC.XXWC_MSTR_PARTS_EXCL_LIST
(
  PARTS_EXCL_LIST_ID     NUMBER                 NOT NULL,
  ITEM_NUMBER            VARCHAR2(240 BYTE),
  ITEM_ID                NUMBER,
  ORGANIZATION_ID        NUMBER                 NOT NULL,
  LAST_UPDATE_DATE       DATE                   NOT NULL,
  LAST_UPDATED_BY        NUMBER                 NOT NULL,
  CREATION_DATE          DATE                   NOT NULL,
  CREATED_BY             NUMBER                 NOT NULL,
  LAST_UPDATE_LOGIN      NUMBER,
  OBJECT_VERSION_NUMBER  NUMBER                 NOT NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


