   /*************************************************************************
   *   $Header XXWC_CSP_EXTRACT_VW $
   *   Module Name: XXWC_CSP_EXTRACT_VW
   *
   *   PURPOSE:   This View is used in CSP extract
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/01/2014  Gopi Damuluri             Initial Version   
   *   2.0        06/25/2015  Manjula Chellappan        TMS# 20150624-00229 - CSP Enhancement Bundle - Item #1 - CSP Extract
   * ***************************************************************************/   

--Commenting DROP command @ rev 2.0 to avoind accidental drop of the object
--DROP TABLE XXWC.XXWC_QP_OOL_GTT_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_QP_OOL_GTT_TBL
(
  UNIT_COST                   NUMBER,
  UNIT_SELLING_PRICE          NUMBER,
  SHIPPED_QUANTITY            NUMBER,
  ORDERED_QUANTITY            NUMBER,
  LINE_TYPE_ID                NUMBER,
  TAX_VALUE                   NUMBER,
  ACTUAL_SHIPMENT_DATE        DATE,
  LINE_ID                     NUMBER,
  REQUEST_DATE                DATE,
  PRICE_ADJUSTMENT_ID         NUMBER,
  AGREEMENT_LINE_ID           NUMBER,
  PRICE_TYPE                  VARCHAR2(30 BYTE),
  ORDER_GROSS_MARGIN          NUMBER,
  INCOMPATABILITY             VARCHAR2(30 BYTE),
  ORGANIZATION_ID             NUMBER,
  CUSTOMER_ID                 NUMBER,
  CUSTOMER_SITE_ID            NUMBER,
  AGREEMENT_ID                NUMBER,
  AGREEMENT_TYPE              VARCHAR2(16 BYTE),
  VQ_NUMBER                   VARCHAR2(30 BYTE),
  ITEM_ATTRIBUTE              VARCHAR2(30 BYTE),
  ITEM_NUMBER                 VARCHAR2(50 BYTE),
  ITEM_DESCRIPTION            VARCHAR2(240 BYTE),
  START_DATE                  DATE,
  END_DATE                    DATE,
  LIST_PRICE                  NUMBER,
  MODIFIER_TYPE               VARCHAR2(30 BYTE),
  MODIFIER_VALUE              NUMBER,
  SELLING_PRICE               NUMBER,
  SPECIAL_COST                NUMBER,
  VENDOR_NUMBER               VARCHAR2(30 BYTE),
  AVERAGE_COST                NUMBER,
  GROSS_MARGIN                NUMBER,
  LAST_UPDATE_DATE            DATE,
  LATEST_REC_FLAG             VARCHAR2(1 BYTE),
  CREATION_DATE               DATE,
  REVISION_NUMBER             NUMBER,
  CUSTOMER_NAME               VARCHAR2(1000 BYTE),
  CUSTOMER_NUMBER             VARCHAR2(50 BYTE),
  LOCATION                    VARCHAR2(100 BYTE),
  PARTY_SITE_NUMBER           VARCHAR2(100 BYTE),
  MODIFIER_NAME               VARCHAR2(1000 BYTE),
  LIST_HEADER_ID              NUMBER,
  SALESREP_NAME               VARCHAR2(360 BYTE),
  SALESREP_ID                 NUMBER,
  ORGANIZATION_CODE           VARCHAR2(3 BYTE),
  COST_AMOUNT_LAST_6MONTHS    NUMBER,
  UNITS_SOLD_IN_LAST_6MONTHS  NUMBER,
  SALE_AMOUNT_LAST_6MONTHS    NUMBER,
  LAST_PURCHASE_DATE          DATE,
  ORG_ID                      NUMBER,
  LAST_UPDATED_BY             VARCHAR2(240 BYTE),
--Added for Rev 2.0 
  QP_MODIFIER_TYPE            VARCHAR2(30 BYTE),
  QP_MODIFIER_VALUE           VARCHAR2(1000 BYTE)    
);
