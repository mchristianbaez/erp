  CREATE TABLE XXWC.XXWC_PO_COMM_HIST 
   (PO_HEADER_ID NUMBER, 
	TRIGGER_TYPE VARCHAR2(2000 BYTE), 
	CREATION_DATE DATE, 
	XML_OR_EDI VARCHAR2(2000 BYTE), 
	PRINT_CHECK VARCHAR2(1 BYTE), 
	FAX_NUMBER VARCHAR2(2000 BYTE), 
	EMAIL_ADDRESS VARCHAR2(2000 BYTE));
  