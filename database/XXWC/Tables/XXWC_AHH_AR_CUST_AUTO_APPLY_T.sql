  /********************************************************************************
  FILE NAME: XXWC_AHH_AR_CUST_AUTO_APPLY_T.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Archive Table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)         DESCRIPTION
  ------- -----------   ----------------  ----------------------------------------------------
  1.0     07/09/2018    Vamshi Palavarapu Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- Drop Table
-- DROP TABLE XXWC.XXWC_AHH_AR_CUST_AUTO_APPLY_T;

-- Create table
CREATE TABLE XXWC.XXWC_AHH_AR_CUST_AUTO_APPLY_T
(
  AHH_CODE   VARCHAR2(20),
  CUST_NAME  VARCHAR2(240),
  CUST_NUM   VARCHAR2(20),
  CUST_CITY  VARCHAR2(50),
  CUST_STATE VARCHAR2(20),
  AUTO_APPLY VARCHAR2(20)
);

GRANT ALL ON XXWC.XXWC_AHH_AR_CUST_AUTO_APPLY_T TO EA_APEX;
/