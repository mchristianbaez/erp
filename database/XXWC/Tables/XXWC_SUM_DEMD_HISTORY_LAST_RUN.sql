CREATE TABLE xxwc.XXWC_SUM_DEMD_HISTORY_LAST_RUN
    (
        organization_id   NUMBER
       ,last_run_date     DATE
       ,last_run_date_j   NUMBER
    );

    INSERT INTO xxwc.xxwc_sum_demd_history_last_run
        SELECT organization_id, SYSDATE - 600, TO_NUMBER (TO_CHAR (SYSDATE - 600, 'J')) FROM mtl_parameters
    COMMIT;
