CREATE TABLE XXWC.XXWC_EGO_SKU_PUBLISH_HISTORY
   /**************************************************************************
    File Name:XXWC_EGO_SKU_PUBLISH_HISTORY
    TYPE: Table
    PURPOSE: Store the last successfully published category details for 
				         website Item SKU.
    HISTORY
    -- Description   : 
    --                 
    ================================================================
           Last Update Date : 08-Jan-2014
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     08-Jan-2014   Praveen Pawar   					   
   **************************************************************************/
			AS
			(
				SELECT MIC.INVENTORY_ITEM_ID  INVENTORY_ITEM_ID
									, MIC.ORGANIZATION_ID    ORGANIZATION_ID
									, MIC.CATEGORY_SET_ID    CATEGORY_SET_ID
									, MIC.CATEGORY_ID        CATEGORY_ID
									, MIC.LAST_UPDATE_DATE   PUBLISH_DATE
									, MIC.REQUEST_ID         REQUEST_ID
				FROM MTL_ITEM_CATEGORIES MIC
							, MTL_CATEGORY_SETS   MCS
				WHERE MIC.CATEGORY_SET_ID = MCS.CATEGORY_SET_ID
						AND MIC.ORGANIZATION_ID = TO_NUMBER (APPS.fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'))
						AND MCS.CATEGORY_SET_NAME = 'Website Item'
			)
			;
			
			ALTER TABLE XXWC.XXWC_EGO_SKU_PUBLISH_HISTORY
   ADD CONSTRAINT XXWC_EGO_SKU_PUB_HIST_U1 UNIQUE (INVENTORY_ITEM_ID, ORGANIZATION_ID, CATEGORY_SET_ID, CATEGORY_ID);
			
   CREATE OR REPLACE PUBLIC SYNONYM XXWC_EGO_SKU_PUBLISH_HISTORY FOR XXWC.XXWC_EGO_SKU_PUBLISH_HISTORY;