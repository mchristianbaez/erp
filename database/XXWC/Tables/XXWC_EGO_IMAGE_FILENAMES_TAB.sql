CREATE TABLE XXWC.XXWC_EGO_IMAGE_FILENAMES
   /**************************************************************************
    File Name:XXWC_EGO_IMAGE_FILENAMES
    TYPE: Table
    PURPOSE: Store the fullsize and thumbnail image names    
    -- Description:  This table will store the fullsize image name
    -- with type as 
    ================================================================
           Last Update Date : 26-Feb-2014
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     26-Feb-2014   Praveen Pawar   Initial version					   
   **************************************************************************/
			(
				 XXWC_IMAGE_FILENAME_ATTR    VARCHAR2(1000)
				,XXWC_IMAGE_FILETYPE_ATTR    VARCHAR2(150)
				,LAST_UPDATE_DATE            DATE
			)
			;
