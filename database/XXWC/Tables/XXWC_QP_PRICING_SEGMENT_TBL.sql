/*************************************************************************
    *   SCRIPT Name: XXWC_QP_PRICING_SEGMENT_TBL
    *
    *   PURPOSE:   Create Table XXWC_QP_PRICING_SEGMENT_TBL
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     01/16/2018     Nancy Pahwa   TMS#20170201-00276   CSP Form Should be modified to accommodate Matrix modifiers--Matrix Maintenance
*****************************************************************************/
CREATE TABLE "XXWC"."XXWC_QP_PRICING_SEGMENT_TBL" 
   (	"SEGMENT" VARCHAR2(50), 
	"PRODUCT_TYPE" VARCHAR2(50) NOT NULL ENABLE, 
	"PRODUCT" VARCHAR2(50) NOT NULL ENABLE, 
	"DESCRIPTION" VARCHAR2(240), 
	"DISCOUNT_TYPE" VARCHAR2(50) NOT NULL ENABLE, 
	"TIER1" NUMBER NOT NULL ENABLE, 
	"TIER2" NUMBER NOT NULL ENABLE, 
	"TIER3" NUMBER NOT NULL ENABLE, 
	"TIER4" NUMBER NOT NULL ENABLE, 
	"TIER5" NUMBER NOT NULL ENABLE, 
	"TIER6" NUMBER, 
	"TIER7" NUMBER, 
	"DELETE_FLAG" VARCHAR2(1), 
	"LAST_UPDATE_DATE" DATE, 
	"LAST_UPDATED_BY" NUMBER, 
	"CREATION_DATE" DATE, 
	"CREATED_BY" NUMBER, 
	"PRODUCT_ID" NUMBER, 
	"FILE_ID" NUMBER, 
	"REQUEST_ID" NUMBER, 
	"START_DATE" DATE, 
	"END_DATE" DATE
   )
/