CREATE TABLE XXWC.XXWC_STAGE_UDA_TAB
   /**************************************************************************
    File Name:XXWC_STAGE_UDA_TAB
    TYPE:     Table
    PURPOSE:   
    -- Description: This table will store the UDA data from .csv file 
    ================================================================
           Last Update Date : 21-Apr-2014
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     18-Apr-2014   Praveen Pawar   Initial version					   
   **************************************************************************/
			(
				 ITEM_NUMBER                    VARCHAR2(150)
				,INVENTORY_ITEM_ID              NUMBER
				,ORGANIZATION_CODE              VARCHAR2(3)
				,ORGANIZATION_ID                NUMBER
				,ITEM_CATALOG_CATEGORY          VARCHAR2(40)
				,ITEM_CATALOG_GROUP_ID          NUMBER
				,ATTRIBUTE_GROUP_INTERNAL_NAME  VARCHAR2(30)
				,ATTRIBUTE_GROUP_ID             NUMBER
				,ATTRIBUTE_INTERNAL_NAME        VARCHAR2(30)
				,ATTRIBUTE_VALUE                VARCHAR2(1000)
				,DATA_TYPE_CODE                 VARCHAR(10)
				,STATUS                         VARCHAR2(40)
				,ERROR_MSG                      VARCHAR2(4000)
				,LAST_UPDATED_BY                NUMBER
				,LAST_UPDATE_DATE               DATE
			)
			;
