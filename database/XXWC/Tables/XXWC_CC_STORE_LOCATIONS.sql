CREATE TABLE XXWC.XXWC_CC_STORE_LOCATIONS
(
  INV_ORG_ID    NUMBER(15)                      NOT NULL,
  INV_ORG_NAME  VARCHAR2(240 BYTE)              NOT NULL,
  SELECT_TRX    CHAR(1 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


