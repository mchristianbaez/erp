/* Formatted on 2/28/2014 8:16:11 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_AP_HOLD_NOTIFICATIONS
-- Generated 2/28/2014 8:16:01 PM from XXWC@ebizrnd

CREATE TABLE xxwc.xxwc_ap_hold_notifications
(
    hold_id                      NUMBER (15, 0) NOT NULL
   ,hold_lookup_code             VARCHAR2 (25 BYTE) NOT NULL
   ,hold_reason                  VARCHAR2 (240 BYTE)
   ,wf_status                    VARCHAR2 (30 BYTE)
   ,agent_id                     NUMBER (9, 0)
   ,vendor_id                    NUMBER
   ,org_id                       NUMBER (15, 0)
   ,invoice_num                  VARCHAR2 (50 BYTE) NOT NULL
   ,invoice_date                 DATE
   ,shipto_org                   VARCHAR2 (60 BYTE)
   ,invoice_id                   NUMBER (15, 0) NOT NULL
   ,inv_line_number              NUMBER NOT NULL
   ,inv_line_amount              NUMBER NOT NULL
   ,inv_line_qty_invoiced        NUMBER
   ,uom                          VARCHAR2 (25 BYTE)
   ,inv_unit_price               NUMBER
   ,line_location_id             NUMBER
   ,last_updated_by              NUMBER (15, 0) NOT NULL
   ,disputable_flag              VARCHAR2 (1 BYTE)
   ,line_type_lookup_code        VARCHAR2 (25 BYTE) NOT NULL
   ,inventory_item_id            NUMBER
   ,inv_item_desc                VARCHAR2 (240 BYTE)
   ,supplier_item_number         VARCHAR2 (25 BYTE)
   ,vendor_item_desc             VARCHAR2 (240 BYTE)
   ,matching_basis               VARCHAR2 (30 BYTE)
   ,po_number                    VARCHAR2 (20 BYTE)
   ,quantity_overbilled          NUMBER
   ,quantity_not_received        NUMBER
   ,unit_price_variance          NUMBER
   ,amount_overbilled            NUMBER
   ,po_matched                   VARCHAR2 (1 BYTE)
   ,shipment_number              NUMBER
   ,po_line_number               NUMBER
   ,po_line_shipment_qty         NUMBER
   ,po_line_shipment_qty_rcved   NUMBER
   ,po_unit_price                NUMBER
   ,po_line_amount               NUMBER
   ,po_line_item_description     VARCHAR2 (240 BYTE)
   ,po_line_need_by_date         DATE
   ,invoice_line_type            VARCHAR2 (80 BYTE) NOT NULL
   ,original_amount              NUMBER NOT NULL
   ,original_quantity_invoiced   NUMBER
   ,original_unit_price          NUMBER
   ,po_distribution_id           NUMBER
   ,branch_manager               NUMBER
   ,ship_org_id                  NUMBER
   ,notification_id              NUMBER
   ,notification_status          VARCHAR2 (64 BYTE)
   ,notif_mail_status            VARCHAR2 (64 BYTE)
   ,notif_begin_date             DATE
   ,notif_end_date               DATE
   ,notif_due_date               DATE
   ,notif_subject                VARCHAR2 (512 BYTE)
   ,hdr_invoice_total            NUMBER
   ,form_link                    VARCHAR2 (512 BYTE)
   ,recipient_role               VARCHAR2 (64 BYTE)
   ,recepient_name               VARCHAR2 (255 BYTE)
   ,buyer_role                   VARCHAR2 (64 BYTE)
   ,buyer_name                   VARCHAR2 (255 BYTE)
   ,branch_man_name              VARCHAR2 (255 BYTE)
   ,branch_man_role              VARCHAR2 (64 BYTE)
   ,item_number                  VARCHAR2 (64 BYTE)
   ,supplier_name                VARCHAR2 (255 BYTE)
   ,wf_item_start_date           DATE
   ,wait_before_notify           NUMBER
   ,reminder_days                NUMBER
   ,invoice_total                NUMBER
   ,item_lines_total             NUMBER
   ,resolution_code              VARCHAR2 (255 BYTE)
   ,amount_approved              NUMBER
   ,approver_note                VARCHAR2 (512 BYTE)
   ,second_page_link             VARCHAR2 (512 BYTE)
   ,action_date                  DATE
   ,original_recipient           VARCHAR2 (64 BYTE)
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
NOPARALLEL
NOLOGGING
/

-- End of DDL Script for Table XXWC.XXWC_AP_HOLD_NOTIFICATIONS
