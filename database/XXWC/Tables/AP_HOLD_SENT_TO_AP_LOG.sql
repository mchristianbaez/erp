/* Formatted on 2/28/2014 8:22:50 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.AP_HOLD_SENT_TO_AP_LOG
-- Generated 2/28/2014 8:22:48 PM from XXWC@ebizrnd

-- Drop the old instance of AP_HOLD_SENT_TO_AP_LOG
DROP TABLE xxwc.ap_hold_sent_to_ap_log
/

CREATE TABLE xxwc.ap_hold_sent_to_ap_log
(
    hold_id         NUMBER
   ,sent_date       DATE
   ,error_message   VARCHAR2 (512 BYTE)
)

/

-- End of DDL Script for Table XXWC.AP_HOLD_SENT_TO_AP_LOG
