/***********************************************************************************************************************
     PURPOSE:     Table creation script
     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    --------------------------------------------------------------------------
     1.0    21/03/2018   Niraj K Ranjan        TMS#20180309-00022   XXWC INV Items Inactivation Program Completed in Error
*********************************************************************************************************************/
  CREATE TABLE "XXWC"."XXWC_INV_SP_INACT_ITEMS##" 
   ("INVENTORY_ITEM_ID" NUMBER NOT NULL ENABLE, 
	"ORGANIZATION_ID" NUMBER NOT NULL ENABLE, 
	"ORGANIZATION_CODE" VARCHAR2(3 BYTE), 
	"PART_NUMBER" VARCHAR2(40 BYTE), 
	"CREATION_DATE" DATE NOT NULL ENABLE, 
	"LAST_UPDATE_DATE" DATE NOT NULL ENABLE, 
	"LAST_ACTIVITY_DATE" DATE, 
	"OH_EXISTS" VARCHAR2(1 BYTE), 
	"SO_EXISTS" VARCHAR2(1 BYTE), 
	"QUOTE_EXISTS" VARCHAR2(1 BYTE), 
	"PO_EXISTS" VARCHAR2(1 BYTE), 
	"REQ_EXISTS" VARCHAR2(1 BYTE), 
	"WEBSITE_EXISTS" VARCHAR2(1 BYTE), 
	"ONE_YEAR_EXISTS" VARCHAR2(1 BYTE), 
	"ACTIVE_CSP_EXISTS" VARCHAR2(1 BYTE), 
	"INACTIVE_CSP_EXISTS" VARCHAR2(1 BYTE), 
	"INVENTORY_ITEM_STATUS_CODE" VARCHAR2(10 BYTE) NOT NULL ENABLE, 
	"ITEM_TYPE" VARCHAR2(30 BYTE), 
	"PRIMARY_UOM_CODE" VARCHAR2(3 BYTE), 
	"BUYER_ID" NUMBER(9,0), 
	"PROCESS_FLAG" CHAR(1 BYTE)
   );
