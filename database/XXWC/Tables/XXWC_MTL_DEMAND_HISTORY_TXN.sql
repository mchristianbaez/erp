CREATE TABLE XXWC.XXWC_MTL_DEMAND_HISTORY_TXN
/*************************************************************************
  $Header XXWC_INV_DEMAND_HIST_TXN $
  Module Name: XXWC_INV_DEMAND_HIST_TXN

  PURPOSE: Demand History Transactions Table

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        07/07/2014  Manjula Chellappan    Initial Version TMS # 20140203-00283
**************************************************************************/
(
    transaction_id number
   ,CONSTRAINT XXWC_MTL_DEMAND_HISTORY_TXN_PK PRIMARY KEY (transaction_id)
)
ORGANIZATION INDEX
TABLESPACE xxwc_data
PARALLEL (DEGREE 16)
