  /*******************************************************************************
  Table:   XXWC_OM_DMS2_SHIP_CONF_GTT_TBL
  Description: This table is used to hold ship confirm data from Descartes with the delivered 
               Orders
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     29-Nov-2017        Ashwin Sridhar  Global Temporary table for Ship Confirm
  ********************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL
(
  ORDER_NUMBER                 NUMBER,
  LINE_NUMBER                  NUMBER,
  ITEM_NUMBER                  VARCHAR2(40 BYTE),
  DELIVERY_ID                  NUMBER,
  BRANCH                       VARCHAR2(50 BYTE),
  DELIVERED_QTY                NUMBER,
  ORDERED_QUANTITY             NUMBER,
  STOP_EXCEPTION               VARCHAR2(50 BYTE),
  LINE_EXCEPTION               VARCHAR2(50 BYTE),
  SIGNED_BY                    VARCHAR2(60 BYTE),
  NOTES                        VARCHAR2(500 BYTE),
  DRIVER_NAME                  VARCHAR2(60 BYTE),
  ROUTE_NUMBER                 VARCHAR2(60 BYTE),
  ROUTE_NAME                   VARCHAR2(60 BYTE),
  DISPATCH_DATE                DATE,
  CONCURRENT_REQUEST_ID        NUMBER,
  CREATION_DATE                DATE,
  CREATED_BY                   NUMBER,
  LAST_UPDATE_DATE             DATE,
  LAST_UPDATED_BY              NUMBER,
  SHIP_CONFIRM_STATUS          VARCHAR2(10 BYTE),
  SHIP_CONFIRM_EXCEPTION       VARCHAR2(240 BYTE),
  ORG_ID                       NUMBER,
  DMS2_ITEM_ID                 NUMBER,
  HEADER_ID                    NUMBER,
  INVENTORY_ITEM_ID            NUMBER,
  SIGNED_BY_FILE_UPDATE        VARCHAR2(10 BYTE),
  SHIP_CONFRIM_REQUEST_ID      NUMBER,
  SEQUENCE_ID                  NUMBER,
  SIGNED_BY_FILE_UPDATE_ERROR  VARCHAR2(240 BYTE)
)
ON COMMIT PRESERVE ROWS NOCACHE;
 
CREATE OR REPLACE SYNONYM APPS.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL FOR XXWC.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL;