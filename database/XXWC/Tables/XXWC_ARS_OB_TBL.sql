  /********************************************************************************
  FILE NAME: XXWC.XXWC_ARS_OB_TBL.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: ARS Outbound Interfaces staging table.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     02/16/2016    Andre Rivas     Initial creation 
  1.1     02/16/2016    neha Saini      TMS# 20160217-00098 Added new column Batch source
  1.2     12/19/2016    Neha Saini      TMS# 20160126-00238 Adding a new column account status
  ********************************************************************************/
  
DROP TABLE XXWC.XXWC_ARS_OB_TBL;-- ver1.2

CREATE TABLE XXWC.XXWC_ARS_OB_TBL
(
  ACCOUNT_NAME         VARCHAR2(150 BYTE),
  BILL_ADDR            VARCHAR2(150 BYTE),
  BILL_CITY            VARCHAR2(50 BYTE),
  BILL_STATE           VARCHAR2(15 BYTE),
  BILL_POSTAL_CODE     VARCHAR2(15 BYTE),
  SHIP_ADDR            VARCHAR2(150 BYTE),
  SHIP_CITY            VARCHAR2(50 BYTE),
  SHIP_STATE           VARCHAR2(15 BYTE),
  SHIP_POSTAL_CODE     VARCHAR2(15 BYTE),
  LOCATION             VARCHAR2(150 BYTE),
  PARTY_SITE_NUMBER    VARCHAR2(25 BYTE),
  BALANCE              NUMBER,
  AMOUNT_DUE_ORIGINAL  NUMBER,
  TRX_NUMBER           VARCHAR2(25 BYTE),
  TRX_DATE             VARCHAR2(25 BYTE),
  ACCOUNT_NUMBER       VARCHAR2(25 BYTE),
  DESCRIPTION          VARCHAR2(15 BYTE),
  ATTRIBUTE1           VARCHAR2(15 BYTE),
  CONTACT_ID           VARCHAR2(25 BYTE),
  FIRSTNAME            VARCHAR2(50 BYTE),
  LASTNAME             VARCHAR2(50 BYTE),
  DEAR                 VARCHAR2(50 BYTE),
  TITLE                VARCHAR2(75 BYTE),
  FAX                  VARCHAR2(50 BYTE),
  PHONE                VARCHAR2(50 BYTE),
  HPHONE               VARCHAR2(50 BYTE),
  ADDRESS1             VARCHAR2(50 BYTE),
  ADDRESS2             VARCHAR2(50 BYTE),
  ADDRESS3             VARCHAR2(50 BYTE),
  CITY                 VARCHAR2(50 BYTE),
  STATE                VARCHAR2(15 BYTE),
  ZIP                  VARCHAR2(15 BYTE),
  EMAIL                VARCHAR2(75 BYTE),
  GC_CON_FIRST_NAME    VARCHAR2(100 BYTE),
  GC_CON_LAST_NAME     VARCHAR2(50 BYTE),
  GC_EMAIL_ADDRESS     VARCHAR2(75 BYTE),
  GC_PHONE             VARCHAR2(50 BYTE),
  GC_FAX               VARCHAR2(50 BYTE),
  GC_ADDRESS1          VARCHAR2(50 BYTE),
  GC_ADDRESS2          VARCHAR2(50 BYTE),
  GC_CITY              VARCHAR2(50 BYTE),
  GC_COUNTY            VARCHAR2(50 BYTE),
  GC_STATE             VARCHAR2(15 BYTE),
  GC_POSTAL_CODE       VARCHAR2(15 BYTE),
  GC_RESP_TYPE         VARCHAR2(75 BYTE),
  OWNR_CON_FIRST_NAME  VARCHAR2(100 BYTE),
  OWNR_CON_LAST_NAME   VARCHAR2(50 BYTE),
  OWNR_EMAIL_ADDRESS   VARCHAR2(75 BYTE),
  OWNR_PHONE           VARCHAR2(50 BYTE),
  OWNR_FAX             VARCHAR2(50 BYTE),
  OWNR_ADDRESS1        VARCHAR2(50 BYTE),
  OWNR_ADDRESS2        VARCHAR2(50 BYTE),
  OWNR_CITY            VARCHAR2(50 BYTE),
  OWNR_COUNTY          VARCHAR2(50 BYTE),
  OWNR_STATE           VARCHAR2(15 BYTE),
  OWNR_POSTAL_CODE     VARCHAR2(15 BYTE),
  OWNR_RESP_TYPE       VARCHAR2(75 BYTE),
  BC_CON_FIRST_NAME    VARCHAR2(100 BYTE),
  BC_CON_LAST_NAME     VARCHAR2(50 BYTE),
  BC_EMAIL_ADDRESS     VARCHAR2(75 BYTE),
  BC_PHONE             VARCHAR2(50 BYTE),
  BC_FAX               VARCHAR2(50 BYTE),
  BC_ADDRESS1          VARCHAR2(50 BYTE),
  BC_ADDRESS2          VARCHAR2(50 BYTE),
  BC_CITY              VARCHAR2(50 BYTE),
  BC_COUNTY            VARCHAR2(50 BYTE),
  BC_STATE             VARCHAR2(15 BYTE),
  BC_POSTAL_CODE       VARCHAR2(15 BYTE),
  BC_RESP_TYPE         VARCHAR2(75 BYTE),
  FI_CON_FIRST_NAME    VARCHAR2(100 BYTE),
  FI_CON_LAST_NAME     VARCHAR2(50 BYTE),
  FI_EMAIL_ADDRESS     VARCHAR2(75 BYTE),
  FI_PHONE             VARCHAR2(50 BYTE),
  FI_FAX               VARCHAR2(50 BYTE),
  FI_ADDRESS1          VARCHAR2(50 BYTE),
  FI_ADDRESS2          VARCHAR2(50 BYTE),
  FI_CITY              VARCHAR2(50 BYTE),
  FI_COUNTY            VARCHAR2(50 BYTE),
  FI_STATE             VARCHAR2(15 BYTE),
  FI_POSTAL_CODE       VARCHAR2(15 BYTE),
  FI_RESP_TYPE         VARCHAR2(75 BYTE),
  ORG_ID               NUMBER,
  BATCH_SOURCE         NUMBER, -- ver 1.1 TMS# 20160217-00098 Added new column
  ACCOUNT_STATUS       VARCHAR2(30 BYTE)  --ver1.2 TMS# 20160126-00238 Adding a new column account status
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );