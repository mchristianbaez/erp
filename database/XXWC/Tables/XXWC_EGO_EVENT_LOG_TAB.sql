/***********************************************************************************
File Name: XXWC_EGO_EVENT_LOG_TAB
PROGRAM TYPE: SQL TABLE file
HISTORY
PURPOSE: Table used for storing Catalog data used in generating CatalogGroups 
         data file
====================================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ----------------------------------------------
1.0     09/23/2012    Rajiv Rathod    Initial creation of the file
************************************************************************************/

DROP TABLE XXWC.XXWC_EGO_EVENT_LOG_TAB CASCADE CONSTRAINTS;

DROP SYNONYM XXWC_EGO_EVENT_LOG_TAB;

CREATE TABLE XXWC.XXWC_EGO_EVENT_LOG_TAB
(
  SEQ            NUMBER,
  CREATION_DATE  DATE,
  EVENT_NAME     VARCHAR2(100 BYTE),
  EVENT_KEY      VARCHAR2(100 BYTE),
  PARAM_NAME1    VARCHAR2(100 BYTE),
  PARAM_VALUE1   VARCHAR2(100 BYTE),
  PARAM_NAME2    VARCHAR2(100 BYTE),
  PARAM_VALUE2   VARCHAR2(100 BYTE),
  PARAM_NAME3    VARCHAR2(100 BYTE),
  PARAM_VALUE3   VARCHAR2(100 BYTE),
  PARAM_NAME4    VARCHAR2(100 BYTE),
  PARAM_VALUE4   VARCHAR2(100 BYTE),
  PARAM_NAME5    VARCHAR2(100 BYTE),
  PARAM_VALUE5   VARCHAR2(100 BYTE),
  PARAM_NAME6    VARCHAR2(100 BYTE),
  PARAM_VALUE6   VARCHAR2(100 BYTE),
  PARAM_NAME7    VARCHAR2(100 BYTE),
  PARAM_VALUE7   VARCHAR2(100 BYTE),
  PARAM_NAME8    VARCHAR2(100 BYTE),
  PARAM_VALUE8   VARCHAR2(100 BYTE),
  PARAM_NAME9    VARCHAR2(100 BYTE),
  PARAM_VALUE9   VARCHAR2(100 BYTE),
  PARAM_NAME10   VARCHAR2(100 BYTE),
  PARAM_VALUE10  VARCHAR2(100 BYTE)
);
CREATE SYNONYM APPS.XXWC_EGO_EVENT_LOG_TAB FOR XXWC.XXWC_EGO_EVENT_LOG_TAB;

CREATE INDEX XXWC.XXWC_EGO_EVENT_LOG_TAB_N1  ON XXWC.XXWC_EGO_EVENT_LOG_TAB(param_name4)
/

