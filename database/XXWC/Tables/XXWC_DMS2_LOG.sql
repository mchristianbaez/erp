  /********************************************************************************
  FILE NAME: XXWC_DMS2_LOG.sql
  
  PROGRAM TYPE: Log table for debug messages
  
  PURPOSE: Log table for debug messages
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     12/26/2017    Rakesh P.       TMS#20170901-00010 DMS Phase-2.0 Inbound Extract
  *******************************************************************************************/
  CREATE TABLE XXWC.XXWC_DMS2_LOG
  (
    HEADER_ID      NUMBER,
    LINE_ID        NUMBER,
    LOG_MESSAGE    VARCHAR2(4000 BYTE),
    CREATION_DATE  DATE,
    LOG_SEQUENCE   NUMBER
);

CREATE PUBLIC SYNONYM XXWC_DMS2_LOG FOR xxwc.XXWC_DMS2_LOG;

CREATE OR REPLACE SYNONYM APPS.XXWC_DMS2_LOG FOR XXWC.XXWC_DMS2_LOG;

CREATE INDEX XXWC.XXWC_DMS2_LOG_N1 ON XXWC.XXWC_DMS2_LOG
(HEADER_ID, LINE_ID);
