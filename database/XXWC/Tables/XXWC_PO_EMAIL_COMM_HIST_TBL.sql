/*************************************************************************
  $Header XXWC_PO_EMAIL_COMM_HIST_TBL.sql $
  Module Name: XXWC_PO_EMAIL_COMM_HIST_TBL

  PURPOSE: Table to maintain custom PO Workflow email history.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        21-Jul-2017  P.Vamshidhar         TMS#20170323-00270 - Auto-fill E-Mail Address on PO Approval form
*******************************************************************************************************************************************/ 
CREATE TABLE XXWC.XXWC_PO_EMAIL_COMM_HIST_TBL
(
   PO_NUMBER          NUMBER (15),
   PO_REVISION_NUM    NUMBER (6),
   SENDER_EMAIL_ADD   VARCHAR2 (100 BYTE),
   RCV_EMAIL_ADD      VARCHAR2 (1000 BYTE),
   PO_EMAIL_SUBJECT   VARCHAR2 (4000 BYTE),
   PO_EMAIL_BODY      VARCHAR2 (4000 BYTE),
   PO_EMAIL_ATTACH    VARCHAR2 (4000 BYTE),
   CONC_REQUEST_ID    NUMBER,
   CONC_REQ_STATUS    VARCHAR2 (100 BYTE),
   CREATION_DATE      DATE,
   NOTES              VARCHAR2 (4000 BYTE)
);
/