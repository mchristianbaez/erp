/*******************************************************************************
  $Header XXWC_OE_ORDER_HDR_QUOTE_BKP.sql $
  Module Name: OM  
  
  * Procedure  : Backup tables creation
  * Description: Backup tables creation
  
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-MAR-2018  Pattabhi Avula         TMS#20180216-00273
  
********************************************************************************/

CREATE TABLE XXWC.XXWC_OE_ORDER_HDR_QUOTE_BKP AS SELECT * FROM APPS.OE_ORDER_HEADERS_ALL WHERE 1>2
/
CREATE TABLE XXWC.XXWC_OE_ORDER_LINES_QUOTE_BKP AS SELECT *  FROM APPS.OE_ORDER_LINES_ALL  WHERE 1>2
/
CREATE TABLE XXWC.XXWC_OM_QUOTE_HEADERS_BKP AS SELECT * FROM xxwc.xxwc_om_quote_headers WHERE 1>2                                                 
/
CREATE TABLE XXWC.XXWC_OM_QUOTE_LINES_BKP AS SELECT * FROM xxwc.xxwc_om_quote_lines  WHERE 1>2 
/