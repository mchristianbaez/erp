  ALTER TABLE xxwc.xxwc_po_freight_burden_tbl
    ADD freight_per_lb NUMBER
    ADD source_organization_id NUMBER
    ADD freight_basis VARCHAR2(1)
    ADD VENDOR_TYPE  VARCHAR2(10);

update xxwc.xxwc_po_freight_burden_tbl set freight_basis = 'C';

update xxwc.xxwc_po_freight_burden_tbl set vendor_type = 'EXTERNAL';

UPDATE xxwc.xxwc_po_freight_burden_tbl SET freight_per_lb = 0;

