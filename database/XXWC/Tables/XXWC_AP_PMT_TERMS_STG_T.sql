 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_PMT_TERMS_STG_T.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00301   -- Initial Version
  ********************************************************************************/  
  CREATE TABLE XXWC.XXWC_AP_PMT_TERMS_STG_T 
   (AHH_TERMS_ID 		VARCHAR2(50), 
	ORACLE_TERMS_ID 	NUMBER, 
	CREATION_DATE 		DATE, 
	CREATED_BY 			NUMBER, 
	LAST_UPDATED_BY 	NUMBER, 
	LAST_UPDATE_DATE 	DATE, 
	LAST_UPDATE_LOGIN 	NUMBER, 
	PROCESS_STATUS 		VARCHAR2(1), 
	ERROR_MESSAGE 		VARCHAR2(2000), 
	BATCH_ID 			NUMBER
   );
   /