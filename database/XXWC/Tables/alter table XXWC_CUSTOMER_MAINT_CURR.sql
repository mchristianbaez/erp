ALTER TABLE XXWC.XXWC_CUSTOMER_MAINT_CURR
ADD    (CUST_ACCT_SITE_ROWID          ROWID,
PRINT_PRICE                    VARCHAR2(10 BYTE),
  MAND_PO                        VARCHAR2(10 BYTE),
  JOINT_CHECK                    VARCHAR2(10 BYTE),
  CATEGORY                       VARCHAR2(20 BYTE)
);
