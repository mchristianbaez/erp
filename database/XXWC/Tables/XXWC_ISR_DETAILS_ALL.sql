/****************************************************************************************************************************
    $Header XXWC_ISR_DETAILS_ALL.sql $
    Module Name: XXWC_ISR_DETAILS_ALL
    PURPOSE: Table to store EISR Final Data

    REVISIONS:
    Ver    Date         Author                Description
    ------ -----------  ------------------    -------------------------------------------------------------------------------
    1.0    16-Oct-2014  Manjula Chellappan    TMS # 20141001-00063 - EISR Re-Architecture
                                              TMS # 20150317-00111 - Enhancements for Purchasing
                                              Added columns 
                                              CORE, TIER, CAT_SBA_OWNER,  MFG_PART_NUMBER,
                                              MST_VENDOR, MAKE_BUY, ORG_ITEM_STATUS, 
                                              ORG_USER_ITEM_TYPE, MST_ITEM_STATUS, 
                                              MST_USER_ITEM_TYPE, LAST_RECEIPT_DATE,ONHAND_GT_270, 
                                              SUPERSEDE_ITEM, FLIP_DATE, ORG_ID, OPERATING_UNIT
    2.0   16-Oct-2015  P.Vamshidhar           TMS#20151008-00138 - Renamed column name Item_cost --> List_price 
****************************************************************************************************************************/

CREATE TABLE XXWC.XXWC_ISR_DETAILS_ALL
(
  ORG                    VARCHAR2(240 BYTE),
  PRE                    VARCHAR2(240 BYTE),
  ITEM_NUMBER            VARCHAR2(240 BYTE),
  VENDOR_NUM             VARCHAR2(240 BYTE),
  VENDOR_NAME            VARCHAR2(240 BYTE),
  SOURCE                 VARCHAR2(240 BYTE),
  ST                     VARCHAR2(240 BYTE),
  DESCRIPTION            VARCHAR2(450 BYTE),
  CAT                    VARCHAR2(240 BYTE),
  PPLT                   NUMBER,
  PLT                    NUMBER,
  UOM                    VARCHAR2(240 BYTE),
  CL                     VARCHAR2(240 BYTE),
  STK_FLAG               VARCHAR2(10 BYTE),
  PM                     VARCHAR2(240 BYTE),
  MINN                   NUMBER,
  MAXN                   NUMBER,
  AMU                    VARCHAR2(240 BYTE),
  MF_FLAG                VARCHAR2(10 BYTE),
  HIT6_STORE_SALES       NUMBER,
  HIT6_OTHER_INV_SALES   NUMBER,
  AVER_COST              NUMBER,
  --ITEM_COST              NUMBER,   -- Commented by Vamshi in Rev 2.0
  LIST_PRICE             NUMBER,     -- Added by Vamshi in Rev 2.0
  BPA                    VARCHAR2(240 BYTE),
  QOH                    NUMBER,
  AVAILABLE              NUMBER,
  AVAILABLEDOLLAR        NUMBER,
  ONE_STORE_SALE         NUMBER,
  SIX_STORE_SALE         NUMBER,
  TWELVE_STORE_SALE      NUMBER,
  ONE_OTHER_INV_SALE     NUMBER,
  SIX_OTHER_INV_SALE     NUMBER,
  TWELVE_OTHER_INV_SALE  NUMBER,
  BIN_LOC                VARCHAR2(240 BYTE),
  MC                     VARCHAR2(240 BYTE),
  FI_FLAG                VARCHAR2(10 BYTE),
  FREEZE_DATE            DATE,
  RES                    VARCHAR2(240 BYTE),
  THIRTEEN_WK_AVG_INV    NUMBER,
  THIRTEEN_WK_AN_COGS    NUMBER,
  TURNS                  NUMBER,
  BUYER                  VARCHAR2(240 BYTE),
  TS                     NUMBER,
  JAN_STORE_SALE         NUMBER,
  FEB_STORE_SALE         NUMBER,
  MAR_STORE_SALE         NUMBER,
  APR_STORE_SALE         NUMBER,
  MAY_STORE_SALE         NUMBER,
  JUN_STORE_SALE         NUMBER,
  JUL_STORE_SALE         NUMBER,
  AUG_STORE_SALE         NUMBER,
  SEP_STORE_SALE         NUMBER,
  OCT_STORE_SALE         NUMBER,
  NOV_STORE_SALE         NUMBER,
  DEC_STORE_SALE         NUMBER,
  JAN_OTHER_INV_SALE     NUMBER,
  FEB_OTHER_INV_SALE     NUMBER,
  MAR_OTHER_INV_SALE     NUMBER,
  APR_OTHER_INV_SALE     NUMBER,
  MAY_OTHER_INV_SALE     NUMBER,
  JUN_OTHER_INV_SALE     NUMBER,
  JUL_OTHER_INV_SALE     NUMBER,
  AUG_OTHER_INV_SALE     NUMBER,
  SEP_OTHER_INV_SALE     NUMBER,
  OCT_OTHER_INV_SALE     NUMBER,
  NOV_OTHER_INV_SALE     NUMBER,
  DEC_OTHER_INV_SALE     NUMBER,
  HIT4_STORE_SALES       NUMBER,
  HIT4_OTHER_INV_SALES   NUMBER,
  INVENTORY_ITEM_ID      NUMBER,
  ORGANIZATION_ID        NUMBER,
  SET_OF_BOOKS_ID        NUMBER,
  ORG_NAME               VARCHAR2(150 BYTE),
  DISTRICT               VARCHAR2(100 BYTE),
  REGION                 VARCHAR2(100 BYTE),
  COMMON_OUTPUT_ID       NUMBER,
  PROCESS_ID             NUMBER,
  INV_CAT_SEG1           VARCHAR2(240 BYTE),
  ON_ORD                 NUMBER,
  BPA_COST               NUMBER,
  OPEN_REQ               NUMBER,
  WT                     NUMBER,
  FML                    NUMBER,
  SOURCING_RULE          VARCHAR2(450 BYTE),
  SO                     NUMBER,
  SS                     NUMBER,
  CLT                    NUMBER,
  AVAIL2                 NUMBER,
  INT_REQ                NUMBER,
  DIR_REQ                NUMBER,
  DEMAND                 NUMBER,
  ITEM_STATUS_CODE       VARCHAR2(50 BYTE),
  SITE_VENDOR_NUM        VARCHAR2(240 BYTE),
  VENDOR_SITE            VARCHAR2(240 BYTE),
  CORE                   VARCHAR2(40 BYTE),
  TIER                   VARCHAR2(150 BYTE),
  CAT_SBA_OWNER          VARCHAR2(150 BYTE),
  MFG_PART_NUMBER        VARCHAR2(2000 BYTE),
  MST_VENDOR             VARCHAR2(30 BYTE),
  MAKE_BUY               VARCHAR2(30 BYTE),
  ORG_ITEM_STATUS        VARCHAR2(240 BYTE),
  ORG_USER_ITEM_TYPE     VARCHAR2(80 BYTE),
  MST_ITEM_STATUS        VARCHAR2(240 BYTE),
  MST_USER_ITEM_TYPE     VARCHAR2(80 BYTE),
  LAST_RECEIPT_DATE      DATE,
  ONHAND_GT_270          NUMBER,
  SUPERSEDE_ITEM         VARCHAR2(2000 BYTE),
  LAST_SV_CHANGE_DATE	 DATE,
  FLIP_DATE 			 DATE,
  ORG_ID                 NUMBER,
  OPERATING_UNIT         VARCHAR2(240 BYTE)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE 4 INSTANCES 1 )
MONITORING;


CREATE INDEX XXWC.XXWC_ISR_DETAILS_ALL_N1 ON XXWC.XXWC_ISR_DETAILS_ALL
(INVENTORY_ITEM_ID, ORGANIZATION_ID)
LOGGING
TABLESPACE XXWC_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE SYNONYM INTERFACE_OBI.XXWC_ISR_DETAILS_ALL FOR XXWC.XXWC_ISR_DETAILS_ALL;




