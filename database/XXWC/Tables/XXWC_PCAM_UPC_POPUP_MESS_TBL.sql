/******************************************************************************************************************
   NAME:       XXWC_PCAM_UPC_POPUP_MESS_TBL.sql
   PURPOSE:    Table to store PCAM UPC error message to user.
   
   REVISIONS:
   Ver        Date          Author               Description
   ---------  -----------  ---------------  ---------------------------------------------------------------------
   1.0        15-May-2017  P.Vamshidhar     TMS#20170421-00137 - UPC Validation(Product Creation and Maintenance)
   1.1        04-Jul-2017  P.Vamshidhar     TMS#20170618-00001 - UPC validation error correction for Data Team in PCAM
******************************************************************************************************************/
CREATE TABLE XXWC.XXWC_PCAM_UPC_POPUP_MESS_TBL
(
  ITEM_NUMBER      VARCHAR2(30 BYTE),
  
  CROSS_REFERENCE  VARCHAR2(150 BYTE),
  CREATED_BY       NUMBER,
  CREATION_DATE    DATE,
  CREATED_LOGIN    NUMBER,
  MESSAGE          VARCHAR2(3000 BYTE)
  ITEM_LEVEL       VARCHAR2(10 BYTE), -- Added in 1.1
  REQUESTOR_NOTES  VARCHAR2(4000 BYTE)  -- Added in 1.1 
);