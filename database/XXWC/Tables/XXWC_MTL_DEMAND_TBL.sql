/*************************************************************************
$Header XXWC_MTL_DEMAND_TBL.sql $
Module Name: XXWC_MTL_DEMAND_TBL
PURPOSE: Table to maintain the demand Data
REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------
1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047
**************************************************************************/
CREATE TABLE XXWC.XXWC_MTL_DEMAND_TBL
  (
    PERIOD_START_DATE      DATE NOT NULL,
    SOURCE_ORGANIZATION_ID NUMBER NOT NULL,
    FORECAST_DESIGNATOR    VARCHAR2(10) NOT NULL,   
    INVENTORY_ITEM_ID      NUMBER NOT NULL,
    DEMAND                 NUMBER,
    AMU			   NUMBER,
    FINAL_DEMAND	   NUMBER
  );
CREATE INDEX XXWC.XXWC_MTL_DEMAND_TBL_N1 ON XXWC.XXWC_MTL_DEMAND_TBL
  (
    FORECAST_DESIGNATOR ,
    SOURCE_ORGANIZATION_ID ,
    INVENTORY_ITEM_ID
  );