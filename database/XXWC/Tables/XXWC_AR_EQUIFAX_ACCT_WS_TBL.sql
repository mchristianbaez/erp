/************************************************************************************************************
* Table: XXWC.XXWC_AR_EQUIFAX_ACCT_WS_TBL
=============================================================================================================
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- ------------------------------------------------------------------
  1.0    19-Dec-2017       P.Vamshidhar    TMS#20131016-00419 - Credit - Implement online credit application 
                                                              - Customer Accounts/Update File
*************************************************************************************************************/
CREATE TABLE XXWC.XXWC_AR_EQUIFAX_ACCT_WS_TBL
(
  RECORD_ID                 NUMBER,
  SALES_FORCE_ID            VARCHAR2(240 BYTE),
  CUSTOMER_NUMBER           VARCHAR2(240 BYTE),
  BUSINESS_NAME             VARCHAR2(240 BYTE),
  BUSINESS_PHONE            VARCHAR2(240 BYTE),
  BUSINESS_FAX              VARCHAR2(240 BYTE),
  EMAIL_ADDRESS             VARCHAR2(240 BYTE),
  SALESREP_ID               VARCHAR2(240 BYTE),
  TYPE_OF_BUSINESS          VARCHAR2(240 BYTE),
  PROFILE_CLASS             VARCHAR2(240 BYTE),
  CREDIT_MANAGER            VARCHAR2(240 BYTE),
  CREDIT_CLASSIFICATION     VARCHAR2(240 BYTE),
  ACCOUNT_STATUS            VARCHAR2(240 BYTE),
  PREDOMINANT_TRADE         VARCHAR2(240 BYTE),
  STATUS                    VARCHAR2(240 BYTE),
  BILLING_ADDRESS           VARCHAR2(240 BYTE),
  BILLING_CITY              VARCHAR2(240 BYTE),
  BILLING_COUNTY            VARCHAR2(240 BYTE),
  BILLING_STATE             VARCHAR2(240 BYTE),
  BILLING_COUNTRY           VARCHAR2(240 BYTE),
  BILLING_ZIP_CODE          VARCHAR2(240 BYTE),
  SHIPPING_ADDRESS          VARCHAR2(240 BYTE),
  SHIPPING_CITY             VARCHAR2(240 BYTE),
  SHIPPING_COUNTY           VARCHAR2(240 BYTE),
  SHIPPING_STATE            VARCHAR2(240 BYTE),
  SHIPPING_COUNTRY          VARCHAR2(240 BYTE),
  SHIPPING_ZIP_CODE         VARCHAR2(240 BYTE),
  YARD_JOB_ACCNT_PROJECT    VARCHAR2(240 BYTE),
  CREDIT_LIMIT              VARCHAR2(240 BYTE),
  DEFAULT_JOB_CREDIT_LIMIT  VARCHAR2(240 BYTE),
  CREDIT_DECISIONING        VARCHAR2(240 BYTE),
  AP_CONTACT_FIRST_NAME     VARCHAR2(240 BYTE),
  AP_CONTACT_LAST_NAME      VARCHAR2(240 BYTE),
  AP_PHONE                  VARCHAR2(240 BYTE),
  AP_EMAIL                  VARCHAR2(240 BYTE),
  INVOICE_EMAIL_ADDRESS     VARCHAR2(240 BYTE),
  POD_EMAIL_ADDRESS         VARCHAR2(240 BYTE),
  SOA_EMAIL_ADDRESS         VARCHAR2(240 BYTE),
  PURCHASE_ORDER_REQUIRED   VARCHAR2(240 BYTE),
  DUNSNUMBER                VARCHAR2(100 BYTE),
  PROCESS_FLAG              VARCHAR2(10 BYTE),
  PROCESS_MESSAGE           VARCHAR2(2000 BYTE),
  CREATION_DATE             DATE,
  CREATED_BY                NUMBER
)
/