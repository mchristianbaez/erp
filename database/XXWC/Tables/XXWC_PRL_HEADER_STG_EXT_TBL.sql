 /********************************************************************************
  FILE NAME: "XXWC"."XXWC_PRL_HEADER_STG_EXT_TBL" 

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_PRL_HEADER_STG_EXT_TBL" 
   (  "NAME" VARCHAR2(240), 
  "DESCRIPTION" VARCHAR2(240), 
  "WAREHOUSE" VARCHAR2(3), 
  "LIST_HEADER_ID" NUMBER, 
  "QUALIFIER_ID" NUMBER, 
  "STATUS" VARCHAR2(30), 
  "ERR_MESSAGE" VARCHAR2(4000), 
  "ORG_ID" NUMBER
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_PRL_HEADER_STG.bad'
    DISCARDFILE 'XXWC_PRL_HEADER_STG.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_PRL_HEADER_STG.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;