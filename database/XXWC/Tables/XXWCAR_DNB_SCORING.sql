CREATE TABLE XXWC.XXWCAR_DNB_SCORING
(
  AHC_MIN            NUMBER,
  AHC_MAX            NUMBER,
  FSS                NUMBER,
  AUTO_DECLINE       VARCHAR2(1 BYTE),
  GOV_IND            VARCHAR2(1 BYTE),
  REC_CODE           VARCHAR2(10 BYTE),
  REC_LIMIT          NUMBER,
  REC_CREDIT_CLASS   VARCHAR2(50 BYTE),
  REC_SKIP_APPROVAL  VARCHAR2(1 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


