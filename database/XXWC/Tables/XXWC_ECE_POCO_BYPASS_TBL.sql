/*************************************************************************
  $Header XXWC_ECE_POCO_BYPASS_TBL $
  Module Name: XXWC_ECE_POCO_BYPASS_TBL.sql

  PURPOSE: Table to store the list of cancelled POs from summary Screen to bypass EDI

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        12-Mar-15   Manjula Chellappan     Initial Version TMS # 20141113-00118
**************************************************************************/

CREATE TABLE XXWC.XXWC_ECE_POCO_BYPASS_TBL
(
   PO_HEADER_ID   NUMBER NOT NULL,
   PO_NUMBER      VARCHAR2 (30) NOT NULL,
   ORG_ID         NUMBER
                     DEFAULT NVL (
                                TO_NUMBER (
                                   DECODE (
                                      SUBSTRB (USERENV ('CLIENT_INFO'), 1, 1),
                                      ' ', NULL,
                                      SUBSTRB (USERENV ('CLIENT_INFO'),
                                               1,
                                               10))),
                                -99)
                     NOT NULL,
   INSERT_DATE    DATE
)
/

CREATE SYNONYM APPS.XXWC_ECE_POCO_BYPASS_TBL FOR XXWC.XXWC_ECE_POCO_BYPASS_TBL
/

BEGIN
   DBMS_RLS.ADD_POLICY ('APPS',                                 -- SCHEMA NAME
                        'XXWC_ECE_POCO_BYPASS_TBL',            -- SYNONYM NAME
                        'ORG_SEC', -- USE POLICY_NAME 'ORG_SEC' –STANDARD POLICY
                        'APPS',                              -- FUNCTION_SCHEM
                        'MO_GLOBAL.ORG_SECURITY', -- STANDARD MO VPD POLICY FUN
                        'SELECT, INSERT, UPDATE, DELETE',    -- STATEMENT_TYPE
                        TRUE,                                  -- UPDATE_CHECK
                        TRUE                                  -- POLICY ENABLE
                            );
   COMMIT;
END;
/