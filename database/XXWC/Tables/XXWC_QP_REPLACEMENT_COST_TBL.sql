  /*******************************************************************************
  Table:   XXWC_QP_REPLACEMENT_COST_TBL
  Description: This table is used to load data for replacement cost
               Orders
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     07-MAR-2017        Rakesh Patel    TMS#20180302-00245-Replacement Cost Enhancement
  ********************************************************************************/
CREATE TABLE XXWC.XXWC_QP_REPLACEMENT_COST_TBL
( 
  FILE_ID                      NUMBER,
  ITEM_NUMBER                  VARCHAR2(40 BYTE),
  INVENTORY_ITEM_ID            NUMBER,
  COST_FLAG                    VARCHAR2(10),   
  REPLACEMENT_COST             NUMBER,         
  DISTRICT                     VARCHAR2(150 BYTE),
  REGION                       VARCHAR2(150 BYTE),  
  STATUS                       VARCHAR2(1 BYTE),
  START_DATE_ACTIVE            DATE,
  END_DATE_ACTIVE              DATE,
  LAST_UPDATE_DATE             DATE          NOT NULL,
  LAST_UPDATED_BY              NUMBER        NOT NULL,
  CREATION_DATE                DATE          NOT NULL,
  CREATED_BY                   NUMBER        NOT NULL,
  LAST_UPDATE_LOGIN            NUMBER,
  CONCURRENT_REQUEST_ID        NUMBER
);

CREATE INDEX XXWC.XXWC_QP_REPLMNT_COST_TBL_N1 ON XXWC.XXWC_QP_REPLACEMENT_COST_TBL(ITEM_NUMBER);

CREATE INDEX XXWC.XXWC_QP_REPLMNT_COST_TBL_N2 ON XXWC.XXWC_QP_REPLACEMENT_COST_TBL(INVENTORY_ITEM_ID, DISTRICT);
  
CREATE INDEX XXWC.XXWC_QP_REPLMNT_COST_TBL_N3 ON XXWC.XXWC_QP_REPLACEMENT_COST_TBL(INVENTORY_ITEM_ID, DISTRICT, REGION);

CREATE INDEX XXWC.XXWC_QP_REPLMNT_COST_TBL_N4 ON XXWC.XXWC_QP_REPLACEMENT_COST_TBL(INVENTORY_ITEM_ID, DISTRICT, START_DATE_ACTIVE, END_DATE_ACTIVE);

CREATE INDEX XXWC.XXWC_QP_REPLMNT_COST_TBL_N5 ON XXWC.XXWC_QP_REPLACEMENT_COST_TBL(INVENTORY_ITEM_ID, DISTRICT, REGION, START_DATE_ACTIVE, END_DATE_ACTIVE);  

CREATE OR REPLACE SYNONYM APPS.XXWC_QP_REPLACEMENT_COST_TBL FOR XXWC.XXWC_QP_REPLACEMENT_COST_TBL;


