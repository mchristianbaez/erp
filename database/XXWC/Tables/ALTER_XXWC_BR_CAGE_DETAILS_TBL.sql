/*************************************************************************
  $Header ALTER_XXWC_BR_CAGE_DETAILS_TBL.sql $
  Module Name: ALTER_XXWC_BR_CAGE_DETAILS_TBL.sql

  PURPOSE:   To Add the constraints

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        07/5/2017  Nancy Pahwa            Task ID: 20170607-00052
**************************************************************************/
alter table XXWC.XXWC_BR_CAGE_DETAILS_TBL
  add constraint XXWC_BR_CAGE_DETAILS_PK primary key (ID);
alter table XXWC.XXWC_BR_CAGE_DETAILS_TBL
  add constraint XXWC_BR_CAGE_DETAILS_UK unique (SERIAL_NUMBER, PARTNUMBER);