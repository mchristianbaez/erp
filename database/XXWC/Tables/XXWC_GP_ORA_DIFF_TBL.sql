CREATE TABLE XXWC.XXWC_GP_ORA_DIFF_TBL
(
  ACCOUNT_NUMBER        VARCHAR2(30 BYTE)       NOT NULL,
  TRX_NUMBER            VARCHAR2(30 BYTE),
  AMOUNT_DUE_REMAINING  NUMBER                  NOT NULL,
  TRX_DATE              DATE,
  CUSTNO                VARCHAR2(20 BYTE)       NOT NULL,
  INVNO                 VARCHAR2(20 BYTE)       NOT NULL,
  BALANCE               FLOAT(126),
  INVDTE                DATE
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


