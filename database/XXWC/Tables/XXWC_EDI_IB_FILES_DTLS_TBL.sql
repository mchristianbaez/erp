/******************************************************************************
      $Header XXWC_EDI_IB_FILES_EXTRNL_TBL.sql $
      Module Name: XXWC_EDI_IB_FILES_EXTRNL_TBL.sql

      PURPOSE:   This XXWC_EDI_IB_FILES_EXTRNL_TBL table is used to load the
	             Customer Integration - Order Import Modifications for ecomm

      REVISIONS:
      Ver        Date         Author              Description 
      ---------  ----------   ---------------     --------------------------
      1.0        12-04-2018  Pattabhi Avula       TMS#20180308-00104
	                                              Initial version
*******************************************************************************/
CREATE TABLE XXWC.XXWC_EDI_IB_FILES_DTLS_TBL(FILE_NAME     VARCHAR2(250),
                                             PROCESS_FLAG  VARCHAR2(1)
											 )
/