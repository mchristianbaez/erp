DROP TABLE XXWC.XXWC_QP_MAINT_TOOL_LINE_STG;

CREATE TABLE XXWC.XXWC_QP_MAINT_TOOL_LINE_STG
(
  FILE_ID             NUMBER                        NULL,
  PRL_NAME            VARCHAR2(240 BYTE)            NULL,
  ITEM_NUMBER         VARCHAR2(40 BYTE)             NULL,
  LIST_PRICE          NUMBER                        NULL,
  OBSOLETE_ITEM       VARCHAR2(1 BYTE)              NULL,
  PRODUCT_ATTRIBUTE   VARCHAR2(30 BYTE)             NULL,
  PRODUCT_ATTR_VALUE  VARCHAR2(240 BYTE)            NULL,
  PRICE_TYPE          VARCHAR2(50 BYTE)             NULL,
  CALL_FOR_PRICE      VARCHAR2(1 BYTE)              NULL,
  LIST_HEADER_ID      NUMBER                        NULL,
  LIST_LINE_ID        NUMBER                        NULL,
  INVENTORY_ITEM_ID   NUMBER                        NULL,
  PRODUCT_ATTR_ID     NUMBER                        NULL,
  PRICING_FORMULA_ID  NUMBER                        NULL,
  STATUS              VARCHAR2(30 BYTE)             NULL,
  ERR_MESSAGE         VARCHAR2(4000 BYTE)           NULL,
  REQUEST_ID          NUMBER                        NULL,
  CREATED_BY          NUMBER                        NULL,
  CREATION_DATE       DATE                          NULL,
  LAST_UPDATED_BY     NUMBER                        NULL,
  LAST_UPDATE_DATE    DATE                          NULL,
  LAST_UPDATE_LOGIN   NUMBER                        NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


