DROP TABLE XXWC.XXWC_OM_DMS_CHANGE_GTT_TBL CASCADE CONSTRAINTS;
/
/**************************************************************************
File Name:XXWC_OM_DMS_CHANGE_GTT_TBL.sql
TYPE:     Table
Description: Global Temporary Table used by DMS File Generation Process

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ---------------------------------
1.0     11-FEB-2015   Gopi Damuluri   Initial version TMS# 20150128-00043
**************************************************************************/

CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_OM_DMS_CHANGE_GTT_TBL
(
  HEADER_ID         NUMBER,
  LINE_ID           NUMBER,
  DELIVERY_ID       NUMBER,
  CREATED_BY        NUMBER,
  CREATION_DATE     DATE,
  LAST_UPDATED_BY   NUMBER,
  LAST_UPDATE_DATE  DATE,
  CHANGE_TYPE       VARCHAR2(1 BYTE),
  ORG_ID            NUMBER,
  STATUS            VARCHAR2(240 BYTE)
)
ON COMMIT PRESERVE ROWS
RESULT_CACHE (MODE DEFAULT)
NOCACHE;