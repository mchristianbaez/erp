CREATE TABLE XXWC.XXWC_MTL_DEMAND_HISTORIES
/*************************************************************************
  $Header XXWC_MTL_DEMAND_HISTORIES $
  Module Name: XXWC_MTL_DEMAND_HISTORIES

  PURPOSE: Demand History Archive Table

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        07/07/2014  Manjula Chellappan    Initial Version TMS # 20140203-00283
**************************************************************************/
(
    transaction_id               NUMBER
   ,bucket_to_update             VARCHAR2 (124 BYTE)
   ,include_exclude_flag         VARCHAR2 (32 BYTE)
   ,transaction_date             NUMBER
   ,inventory_item_id            NUMBER
   ,transaction_source_type_id   NUMBER
   ,transaction_action_id        NUMBER
   ,transaction_type_id          NUMBER
   ,primary_quantity             NUMBER
   ,ol_source_type_code          VARCHAR2 (124 BYTE)
   ,source_line_id               NUMBER
   ,organization_id              NUMBER
   ,insert_date                  DATE
   ,vsql                         VARCHAR2 (512 BYTE)
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
PARALLEL (DEGREE 32)
NOLOGGING
/