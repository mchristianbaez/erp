CREATE TABLE XXWC.XXWC_MTL_DEMAND_HISTORY_TYPES
/*************************************************************************
  $Header XXWC_MTL_DEMAND_HISTORY_TYPES $
  Module Name: XXWC_MTL_DEMAND_HISTORY_TYPES

  PURPOSE: Demand History Transactions Types Table

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        07/07/2014  Manjula Chellappan    Initial Version TMS # 20140203-00283
**************************************************************************/
(
    transaction_source_type_name   VARCHAR2 (30 BYTE) NOT NULL
   ,transaction_type_name          VARCHAR2 (80 BYTE) NOT NULL
   ,description                    VARCHAR2 (240 BYTE)
   ,transaction_type_id            NUMBER NOT NULL
   ,transaction_source_type_id     NUMBER NOT NULL
   ,include_exclude_flag           CHAR (7 BYTE)
   ,transaction_sign               CHAR (8 BYTE)
   ,bucket_to_update               CHAR (67 BYTE)
   ,add_information                CHAR (84 BYTE)
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
NOPARALLEL
LOGGING;
--insert data into table
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Purchase order','PO Receipt','Receive Purchase Order',18,1,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Purchase order','Logical Return to Vendor','Logical Return to Vendor',39,1,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Purchase order','Logical PO Receipt Adjustment','Logical PO Receipt Adjustment',69,1,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Purchase order','Retroactive Price Update','Retroactive Price Update',20,1,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Purchase order','PO Rcpt Adjust','Delivery adjustments on a Purchase order receipt',71,1,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Purchase order','Transfer to Regular','Transfer to Regular',74,1,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Purchase order','Return to Vendor','Return to vendor from stores',36,1,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Purchase order','Logical PO Receipt','Logical PO Receipt',19,1,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Sales order','Sales order issue','Ship Confirm external Sales Order',33,2,'exclude','negative','sales_order_demand','Program should exclude all Drop Ship Orders  Order Line Source Type =  External     ');
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Sales order','Sales Order Pick','Staging transfer on a Sales order',52,2,'exclude','negative','sales_order_demand','Program should exclude all Drop Ship Orders  Order Line Source Type =  External     ');
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Sales order','Logical Sales Order Issue','Logical Sales Order Issue',30,2,'exclude','negative','sales_order_demand','Program should exclude all Drop Ship Orders  Order Line Source Type =  External     ');
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Sales order','COGS Recognition','COGS Recognition',10008,2,'exclude','negative','sales_order_demand','Program should exclude all Drop Ship Orders  Order Line Source Type =  External     ');
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Account','Account issue','Issue material against account',1,3,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Account','Account receipt','Receive material against account',40,3,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Move order','Move Order Transfer','Transact Subinventory Transfer Move Order',64,4,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Move order','Move Order Issue','Transact Account Issue Move Order',63,4,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Move order','Move Order Putaway','Move Order Putaway',81,4,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Completion','WIP Completion',44,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Return','WIP Return',43,5,'include','positive','std_wip_usage',NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Completion Return','WIP Completion Return',17,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Issue','WIP Issue',35,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP assembly scrap','Scrap assemblies from WIP',90,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Byproduct Return','WIP Byproduct Return',1003,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Byproduct Completion','WIP Byproduct Completion',1002,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP estimated scrap','WIP estimated scrap transaction',92,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Lot Quantity Update','WIP Lot Quantity Update',58,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Lot Bonus','WIP Lot Bonus',57,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Lot Merge','Lot Merge',56,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Lot Split','Lot Split',55,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Negative Return','WIP Negative Return',48,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP Negative Issue','WIP Negative Issue',38,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP return from scrap','Return assemblies scrapped to WIP',91,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Job or Schedule','WIP cost update','Update cost of a WIP item',25,5,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Account alias','Account alias issue (Inventory Type Adjustment Out)','Issue material against account alias',31,6,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Account alias','WC Return to Vendor',NULL,102,6,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Account alias','WC Inventory Asset Transfer','WC Inventory Asset Transfer',100,6,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Account alias','Account alias receipt (Inventory Type Adjustment In)','Receive material against account alias',41,6,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal requisition','Logical Intransit Receipt','Logical Intransit Receipt',76,7,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal requisition','Logical Expense Requisition Receipt','Logical Expense Requisition Receipt',27,7,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal requisition','Int Req Intr Rcpt','Delivery of intransit material sourced by Internal requisition',61,7,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal requisition','Int Req Rcpt Adjust','Delivery adjustments on intransit receipt sourced by Internal req.',72,7,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal requisition','Int Req Sub Xfer','Internal Requisition Subinventory Transfer',96,7,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal requisition','Int Req Direct Org Xfer','Internal Requisition Direct Org Transfer',95,7,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal order','Internal Order Xfer','Subinventory transfer sourced by Internal order',50,8,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal order','Internal Order Pick','Staging transfer on an Internal order',53,8,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal order','Int Order Direct Ship','Direct transfer between two organizations on a internal order',54,8,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal order','Logical Intransit Shipment','Logical Intransit Shipment',65,8,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal order','Int Order Intr Ship','Ship to intransit sourced by Internal order',62,8,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Internal order','Internal order issue','Ship Confirm Internal Order: Issue',34,8,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Cycle Count','Cycle Count Transfer','Cycle Count Sub Transfer',5,9,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Cycle Count','Cycle Count Adjust','Record cycle count adjustments',4,9,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Physical Inventory','Physical Inv Transfer','Physical Count Sub Transfer',9,10,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Physical Inventory','Physical Inv Adjust','Physical Inventory adjustment transactions',8,10,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Standard cost update','Standard cost update','Update standard cost information',24,11,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('RMA','RMA Return','Return return material authorization',37,12,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('RMA','RMA Receipt','Return material authorization',15,12,'include','positive','sales_order_demand',NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('RMA','Logical RMA Receipt','Logical RMA Receipt',16,12,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Miscellaneous issue','Perform miscellaneous issue of material',32,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Miscellaneous receipt','Perform miscellaneous receipt of material',42,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Subinventory Transfer','Transfer material between subinventories',2,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Backflush Transfer','Backflush subinventory transfer',51,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Direct Org Transfer','Direct transfer between two organizations',3,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Intransit Receipt','Receive from intransit',12,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Intransit Shipment','Ship to intransit sourced from Inventory',21,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Project Borrow','Borrow from project in project manufacturing',66,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Project Transfer','Transfer to project in project manufacturing',67,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Project Payback','Payback to project in project manufacturing',68,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Shipment Rcpt Adjust','Adjustment to receipt of in-transit delivery',70,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Average cost update','Update average cost information',80,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Container Unpack','Container Unpack',88,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Field Service Usage','Field Service Usage',93,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Cost Group Transfer','Cost Group Transfer',86,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Field Service Recovery','Field Service Recovery',94,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Planning Transfer','Planning Transfer',73,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Inventory Lot Split','Lot Split',82,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Inventory Lot Merge','Lot Merge',83,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Inventory Lot Translate','Lot Translate',84,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Container Split','Container Split',89,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Container Pack','Container Pack',87,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Transfer to Consigned','Transfer to Consigned',75,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Logical Intercompany Shipment Receipt','Logical Intercompany Shipment Receipt',10,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Logical Intercompany Sales Issue','Logical Intercompany Sales Issue',11,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Logical Intercompany Receipt Return','Logical Intercompany Receipt Return',13,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Logical Intercompany Sales Return','Logical Intercompany Sales Return',14,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Logical Intercompany Procurement Receipt','Logical Intercompany Procurement Receipt',22,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Logical Intercompany Procurement Return','Logical Intercompany Procurement Return',23,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Do not Use - WC Return to Vendor',NULL,101,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Logical Intransit Receipt','Logical Intransit Receipt',59,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Logical Intransit Shipment','Logical Intransit Shipment',60,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Lot Conversion Decrease','Lot UOM Conversion Quantity Change - Decrease',97,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Residual Qty Issue','Residual Quantity Issue',98,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Residual Qty Receipt','Residual Quantity Receipt',99,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Lot Conversion Increase','Lot UOM Conversion Quantity Change - Increase',1004,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Deduct Sample Qty','Deduct Sample Qty',1001,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Miscellaneous Issue(RG Update)','Miscellaneous Issue (RG Update) Transaction',100001,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Inventory','Miscellaneous Recpt(RG Update)','Miscellaneous Receipt (RG Update) Transaction',100002,13,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Periodic Cost Update','Periodic Cost Update','Update Periodic Cost',26,14,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Layer Cost Update','Layer Cost Update','Layer Cost Update',28,15,NULL,NULL,NULL,NULL);
INSERT INTO xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES 
VALUES('Project Contract','ProjectContract Issue','Project contract Issue',77,16,NULL,NULL,NULL,NULL);
COMMIT;
