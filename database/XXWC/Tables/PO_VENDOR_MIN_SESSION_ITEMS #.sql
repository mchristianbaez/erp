/* Formatted on 2/6/2014 5:23:10 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.PO_VENDOR_MIN_SESSION_ITEMS#
-- Generated 2/6/2014 5:23:08 PM from XXWC@EBIZDEV

CREATE TABLE xxwc.po_vendor_min_session_items#
(
    source_organization_id   NUMBER
   ,inventory_item_id        NUMBER
   ,insert_date              DATE
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
NOPARALLEL
LOGGING
/

-- End of DDL Script for Table XXWC.PO_VENDOR_MIN_SESSION_ITEMS#
