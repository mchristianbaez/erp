CREATE TABLE XXWC.XXWC_AP_PREF_SUP_ATTR_STG_TBL
/*************************************************************************
  $Header XXWC_AP_PREF_SUP_ATTR_STG_TBL.sql $
  Module Name: XXWC_AP_PREF_SUP_ATTR_STG_TBL

  PURPOSE: Staging table to load the Preferred Supplier attributes though WEB ADI

  TMS Task Id :  20141104-00115

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        29-Oct-2014  Manjula Chellappan    Initial Version
**************************************************************************/
(
   process_id        NUMBER PRIMARY KEY,
   process_date      DATE,
   supplier_id       NUMBER,
   supplier_name     VARCHAR2 (240),
   supplier_number   VARCHAR2 (60),
   supplier_tier     VARCHAR2 (150),
   supplier_sba      VARCHAR2 (150),
   error_msg         VARCHAR2 (2000),
   status            VARCHAR2 (1)
)