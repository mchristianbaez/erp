CREATE TABLE "XXWC"."XXWC_APPLY_EXT_TBL" 
/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_APPLY_EXT_TBL$
  Module Name: XXWC_APPLY_EXT_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-APR-2018  Nancy Pahwa  20160218-00198  Initially Created
**************************************************************************/
   (	"PROCESS_FLAG" VARCHAR2(100), 
	"CUSTOMER_NAME" VARCHAR2(360), 
	"CUSTOMER_NUMBER" VARCHAR2(30), 
	"RECEIPT_NUMBER" VARCHAR2(30), 
	"RECEIPT_DATE" DATE, 
	"RECEIPT_AMOUNT" NUMBER, 
	"TRANSACTION_NUMBER" VARCHAR2(4000), 
	"AMOUNT_APPLIED" NUMBER, 
	"DISCOUNT_AMOUNT" NUMBER, 
	"STATUS" VARCHAR2(100)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_PDH_ITEM_LOAD_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
    SKIP 1
    BADFILE 'unapplyLoadBad.bad'
    DISCARDFILE 'unapplyLoadDiscard.dsc'
    FIELDS TERMINATED BY ','
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
      )
      LOCATION
       ( "XXWC_PDH_ITEM_LOAD_DIR":'XXWC_Unapply_Apply_Load.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;
