CREATE TABLE XXWC.XXWCPO_BPA_INTERFACE
(
  ORG_ID                     NUMBER,
  DOCUMENT_NUMBER            VARCHAR2(40 BYTE),
  LINE_NUMBER                NUMBER,
  SHIPMENT_NUMBER            NUMBER,
  ORGANIZATION_CODE          VARCHAR2(3 BYTE),
  UNIT_PRICE                 NUMBER,
  PRICE_OVERRIDE             NUMBER,
  QUANTITY                   NUMBER,
  START_DATE                 DATE,
  END_DATE                   DATE,
  ALLOW_PRICE_OVERRIDE_FLAG  VARCHAR2(1 BYTE),
  NOT_TO_EXCEED_PRICE        NUMBER,
  ITEM_NUMBER                VARCHAR2(40 BYTE),
  DESCRIPTION                VARCHAR2(240 BYTE),
  SUPPLIER_ITEM              VARCHAR2(40 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


