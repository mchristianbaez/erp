/* Formatted on 11/10/2015 2:07:18 PM (QP5 v5.256.13226.35538) */
   /*************************************************************************
   *   $Header XXWC_OM_PGR_HOLD_ERR_TBL.sql $
   *   Module Name: XXWC_OM_PGR_HOLD_ERR_TBL
   *
   *   PURPOSE:   To store the Error Records from XXWC OM pricing Guard Rail Hold Program
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        4-Nov-2015   Manjula Chellappan    Initial Version
   *                                                  TMS# 20151006-00045 Pricing guard rail project (Removal of pricing guard rail holds)
   * ***************************************************************************/

CREATE TABLE "XXWC"."XXWC_OM_PGR_HOLD_ERR_TBL"
(
   "LINE_ID"                 NUMBER,
   "LINE_NUMBER"             NUMBER,
   "HEADER_ID"               NUMBER,
   "ORDER_NUMBER"            NUMBER,
   "ORG_ID"                  NUMBER,
   "INVENTORY_ITEM_ID"       NUMBER,
   "ORDERED_ITEM"            VARCHAR2 (30 BYTE),
   "LINE_FLOW_STATUS_CODE"   VARCHAR2 (30 BYTE),
   "ORDERED_QUANTITY"        NUMBER,
   "UNIT_SELLING_PRICE"      NUMBER,
   "EXTENDED_PRICE"          NUMBER,
   "MARGIN"                  NUMBER,
   "LINE_TYPE"               VARCHAR2 (30 BYTE),
   "USER_ITEM_DESCRIPTION"   VARCHAR2 (100 BYTE),
   "HOLD_STATUS"             VARCHAR2 (20 BYTE),
   "ERROR"                   VARCHAR2 (4000 BYTE),
   "CREATED_BY"              NUMBER,
   "LAST_UPDATED_BY"         NUMBER,
   "CREATION_DATE"           DATE,
   "LAST_UPDATE_DATE"        DATE,
   "LAST_UPDATE_LOGIN"       NUMBER
)
SEGMENT CREATION DEFERRED
PCTFREE 10
PCTUSED 40
INITRANS 1
MAXTRANS 255
NOCOMPRESS
LOGGING
TABLESPACE "XXWC_DATA";