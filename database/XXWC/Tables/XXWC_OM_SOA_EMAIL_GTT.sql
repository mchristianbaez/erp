/***********************************************************************************************************************
  PURPOSE:     Temp Table creation script
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     21-Mar-2018        P.Vamshidhar    Initial creation of the Table
                                             TMS# 20180320-00096
*********************************************************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_OM_SOA_EMAIL_GTT
(
  EMAIL_ADDRESS  VARCHAR2(100 BYTE),
  FLAG           VARCHAR2(10 BYTE)
)
ON COMMIT DELETE ROWS
RESULT_CACHE (MODE DEFAULT)
NOCACHE;