CREATE TABLE XXWC.XXWC_INTERCO_EXCEPTION
    (p_trx_id   NUMBER
    ,p_xfer_trx_id NUMBER
    ,primary_quantity NUMBER
    ,process_flag   NUMBER
    ,MESSAGE VARCHAR2(2000))
   NOLOGGING;
    
DROP TABLE XXWC.XXWC_INTERCO_EXCEPTION;