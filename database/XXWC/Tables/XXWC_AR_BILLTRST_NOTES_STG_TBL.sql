   /***************************************************************************************************************************************************
        $Header XXWC_AR_BILLTRST_NOTES_STG_TBL $
        Module Name: XXWC_AR_BILLTRST_NOTES_STG_TBL.sql

        PURPOSE:Bill Trust Notes Load

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        09/20/2018  P.Vamshidhar     20180910-00004_Billtrust Notes related to AHH Invoices 
   ******************************************************************************************************************************************************/
  
CREATE TABLE XXWC.XXWC_AR_BILLTRST_NOTES_STG_TBL ( 
ACCOUNT_NUMBER             VARCHAR2(30),
CUST_SITE_NUMBER           VARCHAR2(150), 
ORDERNO                    VARCHAR2(30),  
ORDERSUF                   VARCHAR2(20), 
ORD_LINENUM                VARCHAR2(30),
NOTES_TYPE                 VARCHAR2(30),
PRINTFL                    VARCHAR2(3),  
PRINTFL5                   VARCHAR2(3),  
NOTE_LEVEL                 VARCHAR2(10),
NOTE_SOURCE                VARCHAR2(10),
CREATED_BY                 VARCHAR2(100),
CREATION_DATE              DATE DEFAULT SYSDATE,
NOTELN                     CLOB)
/