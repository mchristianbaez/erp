  /********************************************************************************
  FILE NAME: XXWC_GOOGLE_LOC_INVENTORY_GTT.sql
  
  PROGRAM TYPE: Global Temporary Table Script
  
  PURPOSE: Global temp table to store google local ads data 
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     02/09/2017    P.Vamshidhar    TMS#20161104-00076  - SEO - Local Inventory Ads (LIA)
                                        Initial Version
  *******************************************************************************************/

DROP TABLE XXWC.XXWC_GOOGLE_LOC_INVENTORY_GTT
/