-- Start of DDL Script for Table XXWC.XXWC_SECURITY_ROLES_REPORT
-- Generated 13-Jun-2012 16:12:43 from XXWC@EBIZCON

CREATE TABLE xxwc.xxwc_security_roles_report
    (user_name_oracle               VARCHAR2(100 BYTE),
    first_name                     VARCHAR2(150 BYTE),
    last_name                      VARCHAR2(150 BYTE) NOT NULL,
    email_address                  VARCHAR2(240 BYTE),
    employee_number                VARCHAR2(30 BYTE),
    full_name                      VARCHAR2(240 BYTE),
    assignment_start_date          DATE,
    assignment_end_date            VARCHAR2(17 BYTE),
    manager_first_name             VARCHAR2(150 BYTE),
    manager_last_name              VARCHAR2(150 BYTE),
    manager_email                  VARCHAR2(240 BYTE),
    segment1                       VARCHAR2(25 BYTE),
    responsibility_id              NUMBER,
    assignment_type                VARCHAR2(1 BYTE),
    user_name                      VARCHAR2(320 BYTE) NOT NULL,
    role_name                      VARCHAR2(320 BYTE) NOT NULL,
    relationship_id                NUMBER NOT NULL,
    assigning_role                 VARCHAR2(320 BYTE) NOT NULL,
    start_date                     DATE,
    end_date                       DATE,
    created_by                     NUMBER(15,0),
    creation_date                  DATE,
    last_updated_by                NUMBER(15,0),
    last_update_date               DATE,
    last_update_login              NUMBER(15,0),
    user_start_date                DATE,
    role_start_date                DATE,
    assigning_role_start_date      DATE,
    user_end_date                  DATE,
    role_end_date                  DATE,
    assigning_role_end_date        DATE,
    partition_id                   NUMBER NOT NULL,
    effective_start_date           DATE,
    effective_end_date             DATE,
    user_orig_system               VARCHAR2(30 BYTE),
    user_orig_system_id            NUMBER,
    role_orig_system               VARCHAR2(30 BYTE),
    role_orig_system_id            NUMBER,
    parent_orig_system             VARCHAR2(30 BYTE),
    parent_orig_system_id          NUMBER,
    owner_tag                      VARCHAR2(50 BYTE),
    assignment_reason              VARCHAR2(4000 BYTE),
    alert1                         VARCHAR2(256 BYTE),
    alert2                         VARCHAR2(256 BYTE),
    responsibility_key             VARCHAR2(112 BYTE))
  SEGMENT CREATION IMMEDIATE
  PCTFREE     10
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  xxwc_data
  STORAGE   (
    INITIAL     65536
    NEXT        1048576
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
  NOCACHE
  MONITORING
  NOPARALLEL
  NOLOGGING
/





-- End of DDL Script for Table XXWC.XXWC_SECURITY_ROLES_REPORT

