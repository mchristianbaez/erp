  /*******************************************************************************
  Table:   XXWC_OM_DMS_CHANGE_TBL
  Description: This table inserted from two triggers (XXWC_OE_ORDER_LINES_BRU2_TRG,  
               and xxwc_wsh_shipping_trg the data is used to generate the file to 
               MyLogistics
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     31-Oct-2014        Kathy Poling    Initial version TMS# 20140606-00082 
  ********************************************************************************/
CREATE TABLE XXWC.XXWC_OM_DMS_CHANGE_TBL
(header_id        NUMBER,
 line_id          NUMBER,
 delivery_id      NUMBER,
 created_by       NUMBER,
 creation_date    DATE,
 last_updated_by  NUMBER,
 last_update_date DATE,
 change_type      VARCHAR2(1),
 org_id           NUMBER DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),-99)
 );
