CREATE TABLE xxwc.XXWC_SR_SALESREPS_SS
(
   SALESREP_ID          NUMBER NOT NULL,
   SS_DATE              DATE NOT NULL,
   RESOURCE_ID          NUMBER,
   EEID                 NUMBER,
   NTID                 VARCHAR2 (30),
   PRISM_SR_NUMBER      VARCHAR2 (30),
   SR_NAME              VARCHAR2 (360),
   SR_TYPE              VARCHAR2 (30),
   AUTO_UPDATE          VARCHAR2 (1),
   SR_TITLE             VARCHAR2 (80),
   HOME_BR              VARCHAR2 (10),
   HOME_MARKET_CODE     VARCHAR2 (10),
   HOME_MARKET          VARCHAR2 (80),
   HOME_DISTRICT_CODE   VARCHAR2 (10),
   HOME_DISTRICT        VARCHAR2 (80),
   HOME_REGION_CODE     VARCHAR2 (30),
   HOME_REGION          VARCHAR2 (80),
   START_DATE           DATE,
   END_DATE             DATE,
   LAST_UPDATE_DATE     DATE,
   LAST_UPDATE_USER     VARCHAR2 (30)
)
/

CREATE SYNONYM apps.xxwc_sr_salesreps_ss FOR xxwc.xxwc_sr_salesreps_ss
/

GRANT ALL ON xxwc.XXWC_SR_SALESREPS_SS TO interface_dstage;
/

GRANT ALL ON xxwc.XXWC_SR_SALESREPS_SS TO interface_apexwc;
/