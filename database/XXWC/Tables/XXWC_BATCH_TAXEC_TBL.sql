CREATE TABLE XXWC.XXWC_BATCH_TAXEC_TBL
(
  PARTY_SITE_NUMBER  VARCHAR2(100 BYTE)         NOT NULL,
  TAX_EXEMPTION      VARCHAR2(150 BYTE)         NOT NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


