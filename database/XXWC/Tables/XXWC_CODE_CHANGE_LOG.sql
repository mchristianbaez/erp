-- Start of DDL Script for Table XXWC.CODE_CHANGE_LOG
-- Generated 22-Jun-2012 13:05:24 from XXWC@EBIZCON

CREATE TABLE xxwc.xxwc_code_change_log
    (sys_event                      VARCHAR2(255 BYTE),
    object_owner                   VARCHAR2(255 BYTE),
    object_name                    VARCHAR2(255 BYTE),
    sql_text                       CLOB,
    user_name                      VARCHAR2(255 BYTE),
    action_date                    DATE)
 
/





-- End of DDL Script for Table XXWC.CODE_CHANGE_LOG

