/* Formatted on 1/31/2013 6:29:32 PM (QP5 v5.206) */
 ALTER TABLE xxwc.xxwc_edi_inbound_files_archive
 ADD (  vendor_number      VARCHAR2 (64 BYTE)
   ,vendor_name        VARCHAR2 (128 BYTE)
   ,invoice_number     VARCHAR2 (64 BYTE)
   ,invoice_date       VARCHAR2 (64 BYTE)
   ,po_number          VARCHAR2 (64 BYTE)
   ,invoice_amount     VARCHAR2 (64 BYTE)
   ,cp_log_file        CLOB);
/* Formatted on 1/31/2013 6:29:37 PM (QP5 v5.206) */
   ALTER TABLE xxwc.xxwc_edi_inbound_files_history
 ADD (  vendor_number      VARCHAR2 (64 BYTE)
   ,vendor_name        VARCHAR2 (128 BYTE)
   ,invoice_number     VARCHAR2 (64 BYTE)
   ,invoice_date       VARCHAR2 (64 BYTE)
   ,po_number          VARCHAR2 (64 BYTE)
   ,invoice_amount     VARCHAR2 (64 BYTE)
   ,cp_log_file        CLOB);
