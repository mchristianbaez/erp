/**************************************************************************************
TMS# 20130322-01259  : TO improve performance of Modifier List WEB ADI interface  added Index on XXWC_QP_MODIFIER_LIST
                       and created Global Temp table 
***************************************************************************************/
Connect APPS/&APPS_PWD ; 

CReate Index XXWC_QP_MODIFIER_INT_N1 on XXWC.XXWC_QP_MODIFIER_INT(LIST_HEADER_ID, LN_LIST_LINE_ID) ;


DROP TABLE XXWC.XXWC_QP_MODIFIER_INT_GT CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_QP_MODIFIER_INT_GT
(
  BATCH_ID                      NUMBER,
  LIST_HEADER_ID                NUMBER,
  MODIFIER_LIST_NAME            VARCHAR2(2000 BYTE),
  MODIFIER_LIST_NUMBER          VARCHAR2(240 BYTE),
  LN_ORIG_SYS_LINE_REF          VARCHAR2(50 BYTE),
  LN_LIST_LINE_ID               NUMBER,
  LN_LIST_LINE_NO               VARCHAR2(30 BYTE),
  LN_MODIFIER_LEVEL             VARCHAR2(4000 BYTE),
  LN_MODIFIER_LEVEL_CODE        VARCHAR2(30 BYTE),
  LN_LIST_LINE_TYPE             VARCHAR2(4000 BYTE),
  LN_LIST_LINE_TYPE_CODE        VARCHAR2(30 BYTE),
  LN_START_DATE_ACTIVE          DATE,
  LN_END_DATE_ACTIVE            DATE,
  LN_AUTOMATIC_FLAG             VARCHAR2(1 BYTE),
  LN_OVERRIDE_FLAG              VARCHAR2(1 BYTE),
  LN_PRICING_PHASE              VARCHAR2(4000 BYTE),
  LN_PRICING_PHASE_ID           NUMBER,
  LN_PRODUCT_PRECEDENCE         NUMBER,
  LN_PRICE_BREAK_TYPE_CODE      VARCHAR2(30 BYTE),
  LN_ARITHMETIC_OPERATOR_TYPE   VARCHAR2(4000 BYTE),
  LN_ARITHMETIC_OPERATOR        VARCHAR2(30 BYTE),
  LN_OPERAND                    NUMBER,
  LN_ACCRUAL_FLAG               VARCHAR2(1 BYTE),
  PA_PRICING_ATTRIBUTE_ID       NUMBER,
  PA_ORIG_SYS_PRICING_ATTR_REF  VARCHAR2(50 BYTE),
  PA_PRODUCT_ATTRIBUTE_CONTEXT  VARCHAR2(30 BYTE),
  PA_PRODUCT_ATTRIBUTE          VARCHAR2(30 BYTE),
  PA_PRODUCT_ATTR_VALUE         VARCHAR2(240 BYTE),
  PA_PRODUCT_UOM_CODE           VARCHAR2(3 BYTE),
  PA_PRICING_ATTRIBUTE          VARCHAR2(30 BYTE),
  PA_COMPARISON_OPERATOR_CODE   VARCHAR2(30 BYTE),
  PA_PRICING_ATTRIBUTE_CONTEXT  VARCHAR2(30 BYTE),
  PA_PRICING_ATTR_VALUE_FROM    VARCHAR2(240 BYTE),
  PA_PRICING_ATTR_VALUE_TO      VARCHAR2(240 BYTE),
  PA_EXCLUDER_FLAG              VARCHAR2(1 BYTE),
  PA_ATTRIBUTE_GROUPING_NO      NUMBER,
  QL_QUALIFIER_ID               NUMBER,
  QL_ORIG_SYS_QUALIFIER_REF     VARCHAR2(50 BYTE),
  QL_QUALIFIER_GROUPING_NO      NUMBER,
  QL_QUALIFIER_CONTEXT          VARCHAR2(30 BYTE),
  QL_QUALIFIER_ATTRIBUTE        VARCHAR2(30 BYTE),
  QL_QUALIFIER_ATTR_VALUE       VARCHAR2(240 BYTE),
  QL_QUALIFIER_ATTR_VALUE_TO    VARCHAR2(240 BYTE),
  QL_COMPARISON_OPERATOR_CODE   VARCHAR2(30 BYTE),
  QL_QUALIFIER_PRECEDENCE       NUMBER,
  STATUS                        VARCHAR2(1 BYTE),
  ERROR_MESSAGE                 VARCHAR2(2000 BYTE),
  CREATED_BY                    NUMBER,
  CREATION_DATE                 DATE,
  LAST_UPDATED_BY               NUMBER,
  LAST_UPDATE_DATE              DATE,
  LAST_UPDATE_LOGIN             NUMBER,
  HDR_OPERATION                 VARCHAR2(30 BYTE),
  LN_OPERATION                  VARCHAR2(30 BYTE),
  QL_OPERATION                  VARCHAR2(30 BYTE),
  PA_OPERATION                  VARCHAR2(30 BYTE),
  CURRENCY_CODE                 VARCHAR2(30 BYTE),
  LIST_TYPE_CODE                VARCHAR2(30 BYTE),
  SOURCE_SYSTEM_CODE            VARCHAR2(30 BYTE),
  PTE_CODE                      VARCHAR2(30 BYTE),
  START_DATE_ACTIVE             DATE,
  END_DATE_ACTIVE               DATE,
  GLOBAL_FLAG                   VARCHAR2(1 BYTE),
  AUTOMATIC_FLAG                VARCHAR2(1 BYTE),
  COMMENTS                      VARCHAR2(2000 BYTE),
  LN_INCOMPATIBILITY_GRP_CODE   VARCHAR2(30 BYTE),
  LN_PRICING_GROUP_SEQUENCE     NUMBER,
  LN_INCLUDE_ON_RETURNS_FLAG    VARCHAR2(1 BYTE),
  LN_GENERATE_USING_FORMULA_ID  NUMBER,
  LN_PRICE_BY_FORMULA_ID        NUMBER,
  LN_QUALIFICATION_IND          NUMBER,
  LN_MODIFIER_PARENT_INDEX      NUMBER,
  PA_PRICING_PHASE_ID           NUMBER,
  PA_QUALIFICATION_IND          NUMBER,
  QL_ACTIVE_FLAG                VARCHAR2(1 BYTE),
  QL_LIST_TYPE_CODE             VARCHAR2(30 BYTE),
  REQUEST_ID                    NUMBER,
  MODIFIER_TYPE                 VARCHAR2(150 BYTE)
)
ON COMMIT PRESERVE ROWS; 


DROP SYNONYM APPS.XXWC_QP_MODIFIER_INT_GT;

CREATE SYNONYM APPS.XXWC_QP_MODIFIER_INT_GT FOR XXWC.XXWC_QP_MODIFIER_INT_GT;


DROP SYNONYM XXWC_DEV_ADMIN.XXWC_QP_MODIFIER_INT_GT;

CREATE SYNONYM XXWC_DEV_ADMIN.XXWC_QP_MODIFIER_INT_GT FOR XXWC.XXWC_QP_MODIFIER_INT_GT;

CReate Index XXWC_QP_MODIFIER_INT_GT_N1 on XXWC.XXWC_QP_MODIFIER_INT_GT(LIST_HEADER_ID, LN_LIST_LINE_ID) ;

Connect XXWC/&XXWC_PWD ; 

GRANT ALTER, DELETE, INDEX, INSERT,  SELECT, UPDATE ON XXWC.XXWC_QP_MODIFIER_INT_GT TO APPS;

GRANT ALTER, DELETE, INDEX, INSERT,  SELECT, UPDATE ON XXWC.XXWC_QP_MODIFIER_INT_GT TO XXWC_DEV_ADMIN;

exit;



