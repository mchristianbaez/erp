DROP TABLE XXWC.XXWC_NTL_MARKET_PRL_UPD_STG;

CREATE TABLE XXWC.XXWC_NTL_MARKET_PRL_UPD_STG
(
  BASE_MARKET_PRL_ID     NUMBER                     NULL,
  MARKET_PRL_ID          NUMBER                     NULL,
  INVENTORY_ITEM_ID      NUMBER                     NULL,
  PRIMARY_UOM_CODE       VARCHAR2(3 BYTE)           NULL,
  PRICE_LIST_VALUE       NUMBER                     NULL,
  CALL_FOR_PRICE         VARCHAR2(1 BYTE)           NULL,
  SOURCE_PROD_ATTRIBUTE  VARCHAR2(240 BYTE)         NULL,
  SOURCE_PROD_VALUE      VARCHAR2(240 BYTE)         NULL,
  STATUS                 VARCHAR2(30 BYTE)          NULL,
  ERR_MESSAGE            VARCHAR2(4000 BYTE)        NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


