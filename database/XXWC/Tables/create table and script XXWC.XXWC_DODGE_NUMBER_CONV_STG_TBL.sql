DECLARE
  CURSOR c_main IS
    SELECT hcsu.attribute4 oracle_dodge_number
          ,tmp_ddg.dodge_project_name
          ,hcsu.status
          ,tmp_ddg.cust_acct_site_id
          ,hcsu.site_use_id
          ,tmp_ddg.dodge_number stg_dodge_number
      FROM ar.hz_cust_site_uses_all            hcsu
          --,ar.hz_cust_acct_sites_all           hcas
          ,xxwc.xxwc_dodge_number_conv_stg_tbl tmp_ddg
     WHERE 1=1
                   --AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
       AND hcsu.cust_acct_site_id = tmp_ddg.cust_acct_site_id
       AND hcsu.attribute4 IS NULL
       AND hcsu.status = 'A';
BEGIN
  FOR j IN c_main
  LOOP  
      UPDATE ar.hz_cust_site_uses_all
         SET attribute4 = j.stg_dodge_number
       WHERE site_use_id = j.site_use_id
         AND attribute4 IS NULL; --this is double check to verify that attribute4 is null.
  END LOOP;
  COMMIT;
END;
