CREATE TABLE xxwc.XXWC_BR_MGMT_LEVELS
(
   MGT_ID             NUMBER NOT NULL PRIMARY KEY,
   MGT_LVL            NUMBER,
   MGT_DESC           VARCHAR2 (80),
   START_DATE         DATE,
   END_DATE           DATE,
   LAST_UPDATE_DATE   DATE,
   LAST_UPDATE_USER   VARCHAR2 (10)
)
/

CREATE SYNONYM apps.XXWC_BR_MGMT_LEVELS FOR xxwc.XXWC_BR_MGMT_LEVELS
/

CREATE SEQUENCE xxwc.XXWC_BR_MGMT_LEVELS_s
/

CREATE SYNONYM apps.XXWC_BR_MGMT_LEVELS_s FOR xxwc.XXWC_BR_MGMT_LEVELS_s
/

GRANT ALL ON xxwc.XXWC_BR_MGMT_LEVELS TO interface_dstage;
/

GRANT ALL ON xxwc.XXWC_BR_MGMT_LEVELS TO interface_apexwc;
/