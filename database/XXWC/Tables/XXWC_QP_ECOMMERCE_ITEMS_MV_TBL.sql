DROP TABLE XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL CASCADE CONSTRAINTS;

/**************************************************************************
File Name:XXWC_QP_ECOMMERCE_ITEMS_MV_TBL.sql
TYPE:     Table
Description: Temporary Table used by EComm Customer/Organization Extract Process to store Pricing Information.

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ---------------------------------
1.0     11-FEB-2015   Gopi Damuluri   Initial version TMS# 20141212-00194
1.1     04-MAR-2015   Gopi Damuluri   TMS# 20150219-00234 Added CALL_FOR_PRICE_FLAG
1.2     10-MAR-2015   P.Vamshidhar    TMS#20150309-00325 Added additional columns
                                      WEB_FLAG and LAST_UPDATE_DATE_MSIB
**************************************************************************/

CREATE TABLE XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL
(
  INVENTORY_ITEM_ID    NUMBER                   NOT NULL,
  ORGANIZATION_ID      NUMBER                   NOT NULL,
  ITEM_NUMBER          VARCHAR2(40 BYTE),
  DESCRIPTION          VARCHAR2(240 BYTE),
  LAST_UPDATE_DATE     DATE                     NOT NULL,
  CALL_FOR_PRICE_FLAG  VARCHAR2(1 BYTE),                        -- TMS# 20150219-00234
  WEB_FLAG               VARCHAR2(40 BYTE),                     -- TMS#20150309-00325
  LAST_UPDATE_DATE_MSIB  DATE                                   -- TMS#20150309-00325
);