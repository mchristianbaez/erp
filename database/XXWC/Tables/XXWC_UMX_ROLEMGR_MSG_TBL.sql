DROP TABLE XXWC.XXWC_UMX_ROLEMGR_MSG_TBL CASCADE CONSTRAINTS;
/*******************************************************************************************************
  -- Table Name XXWC_UMX_ROLEMGR_MSG_TBL
  -- ***************************************************************************************************
  --
  -- PURPOSE: Temp table used to store return message which need to pass to role manager tool
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     15-Jul-2016   Niraj K Ranjan   Initial Version - TMS#20160629-00068
********************************************************************************************************/
CREATE TABLE XXWC.XXWC_UMX_ROLEMGR_MSG_TBL
   ( REQUEST_ID      NUMBER,
     USER_NAME       VARCHAR2(100),
     RETURN_MESSAGE  VARCHAR2(2000)
   );
