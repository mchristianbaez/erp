/**************************************************************************
File Name:XXWC_CUST_PREDOMINANT_TRD_TBL.sql
TYPE:     Table
Description: Temporary Table used to update Customer Classification and Predominant Trade

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ---------------------------------
1.0     13-MAR-2015   Gopi Damuluri   Initial version TMS# 20150313-00071
**************************************************************************/

CREATE TABLE XXWC.XXWC_CUST_PREDOMINANT_TRD_TBL
(
  CUSTOMER_NUMBER        VARCHAR2(30 BYTE),
  CUSTOMER_NAME          VARCHAR2(240 BYTE),
  PREDOMINANT_TRADE      VARCHAR2(150 BYTE),
  CUSTOMER_CLASS_CODE    VARCHAR2(30 BYTE),
  STATUS                 VARCHAR2(1 BYTE),
  ERROR_MESSAGE          VARCHAR2(2000 BYTE)
);