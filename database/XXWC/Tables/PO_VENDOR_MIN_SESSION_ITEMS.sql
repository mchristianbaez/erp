/* Formatted on 2/6/2014 5:16:15 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.PO_VENDOR_MIN_SESSION_ITEMS
-- Generated 2/6/2014 5:16:10 PM from XXWC@EBIZDEV

CREATE GLOBAL TEMPORARY TABLE xxwc.po_vendor_min_session_items
(
    source_organization_id   NUMBER
   ,inventory_item_id        NUMBER
)
ON COMMIT PRESERVE ROWS
  SEGMENT CREATION IMMEDIATE
  NOPARALLEL
/

-- End of DDL Script for Table XXWC.PO_VENDOR_MIN_SESSION_ITEMS
