DROP TABLE XXWC.DW_HOUSEACCTS;

CREATE TABLE XXWC.DW_HOUSEACCTS
(
  COMPANY                       CHAR(3 BYTE)    NOT NULL,
  DATECREATED                   DATE,
  DATEMODIFIED                  DATE,
  DISTRICTMANAGER_HREMPLOYEEID  VARCHAR2(12 BYTE),
  DISTRICTMANAGER_NTID          VARCHAR2(64 BYTE),
  HREMPLOYEEID                  VARCHAR2(12 BYTE) NOT NULL,
  NTID                          VARCHAR2(64 BYTE),
  FULLNAME                      VARCHAR2(67 BYTE) NOT NULL,
  NATIONALMANAGER_HREMPLOYEEID  VARCHAR2(12 BYTE),
  NATIONALMANAGER_NTID          VARCHAR2(64 BYTE),
  REGIONALMANAGER_HREMPLOYEEID  VARCHAR2(12 BYTE),
  REGIONALMANAGER_NTID          VARCHAR2(64 BYTE),
  SALESREPNUMBER                VARCHAR2(12 BYTE) NOT NULL,
  SALESREPNAME                  VARCHAR2(64 BYTE)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.DISTRICTMANAGER_HREMPLOYEEID IS 'HR ID for the DSM';

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.DISTRICTMANAGER_NTID IS 'NT ID for the DSM';

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.HREMPLOYEEID IS 'HR ID for the current sales rep';

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.NTID IS 'NT ID for the current sales rep';

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.FULLNAME IS 'full name for the sales rep';

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.NATIONALMANAGER_HREMPLOYEEID IS 'HR ID for the NSM';

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.NATIONALMANAGER_NTID IS 'NT ID for the NSM';

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.REGIONALMANAGER_HREMPLOYEEID IS 'HR ID for the RSM
';

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.REGIONALMANAGER_NTID IS 'NT ID for the NSM';

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.SALESREPNUMBER IS 'House account belongs to this sales rep ID';

COMMENT ON COLUMN XXWC.DW_HOUSEACCTS.SALESREPNAME IS 'House account belongs to this sales rep name';


ALTER TABLE XXWC.DW_HOUSEACCTS ADD (
  PRIMARY KEY
 (SALESREPNUMBER));

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.DW_HOUSEACCTS TO APPS WITH GRANT OPTION;

GRANT DELETE, INSERT ON XXWC.DW_HOUSEACCTS TO XXWC_PRISM_INSERT_ROLE;

GRANT SELECT ON XXWC.DW_HOUSEACCTS TO XXWC_PRISM_SELECT_ROLE;

