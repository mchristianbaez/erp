CREATE TABLE XXWC.XXWC_INV_CYCLECOUNT_LINE_DTLS
(
  CYCLECOUNT_LINE_DTL_ID  NUMBER                NOT NULL,
  CYCLECOUNT_LINE_ID      NUMBER                NOT NULL,
  CYCLECOUNT_HDR_ID       NUMBER                NOT NULL,
  INV_ORG_ID              NUMBER                NOT NULL,
  SUBINVENTORY_CODE       VARCHAR2(50 BYTE),
  INV_LOCATION_ID         NUMBER,
  INV_ITEM_ID             NUMBER,
  CATEGORY_ID             NUMBER,
  PROCESS_STATUS          VARCHAR2(10 BYTE)     NOT NULL,
  PROCESS_DATE            DATE,
  LAST_UPDATE_DATE        DATE                  NOT NULL,
  LAST_UPDATED_BY         NUMBER                NOT NULL,
  CREATION_DATE           DATE                  NOT NULL,
  CREATED_BY              NUMBER                NOT NULL,
  LAST_UPDATE_LOGIN       NUMBER,
  OBJECT_VERSION_NUMBER   NUMBER                NOT NULL,
  BIN                     VARCHAR2(100 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


