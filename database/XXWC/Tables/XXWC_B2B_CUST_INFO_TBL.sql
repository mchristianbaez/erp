  /*******************************************************************************
  Table:   	
  Description: This table is used to maintain Trading Partner information for B2B Integration
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-May-2014        Gopi Damuluri   Initial version TMS# 20140219-00148
  1.1     09-Jun-2014        Gopi Damuluri   TMS# 20140609-00256
                                             Added  a new column TP_NAME
  1.2     15-Dec-2015        Gopi Damuluri   TMS# 20160120-00169 - B2B POD Enhancement
  ********************************************************************************/
DROP TABLE XXWC.XXWC_B2B_CUST_INFO_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_B2B_CUST_INFO_TBL
(
  PARTY_ID            NUMBER,
  PARTY_NAME          VARCHAR2(360 BYTE),
  PARTY_NUMBER        VARCHAR2(30 BYTE),
  CUSTOMER_ID         NUMBER,
  DELIVER_SOA         VARCHAR2(1 BYTE),
  DELIVER_ASN         VARCHAR2(1 BYTE),
  DELIVER_INVOICE     VARCHAR2(1 BYTE),
  DELIVER_POD         VARCHAR2(1 BYTE),   -- Version# 1.2
  POD_EMAIL           VARCHAR2(240 BYTE), -- Version# 1.2
  POD_FREQUENCY       VARCHAR2(50 BYTE),  -- Version# 1.2
  START_DATE_ACTIVE   DATE,
  END_DATE_ACTIVE     DATE,
  CREATION_DATE       DATE,
  CREATED_BY          NUMBER,
  LAST_UPDATE_DATE    DATE,
  LAST_UPDATED_BY     NUMBER,
  TP_NAME             VARCHAR2(50 BYTE),
  DELIVER_POA         VARCHAR2(1 BYTE)          DEFAULT 'N',
  DEFAULT_EMAIL       VARCHAR2(240 BYTE),
  STATUS              VARCHAR2(40 BYTE),
  NOTIFICATION_EMAIL  VARCHAR2(250 BYTE),
  NOTIFY_ACCOUNT_MGR  VARCHAR2(1 BYTE),
  SOA_EMAIL           VARCHAR2(240 BYTE),
  ASN_EMAIL           VARCHAR2(240 BYTE),
  COMMENTS            VARCHAR2(2000 BYTE),
  APPROVED_DATE       DATE  
);