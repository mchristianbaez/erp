 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_PAID_INVOICE_EXT_TBL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AP_PAID_INVOICE_EXT_TBL 
   (INVOICE_NUM 		VARCHAR2(30), 
	VENDOR_NUM  		VARCHAR2(30), 
	VENDOR_SITE_CODE 	VARCHAR2(100), 
	INVOICE_AMOUNT   	VARCHAR2(30), 
	INVOICE_DATE 		VARCHAR2(30), 
	ORG_ID 				VARCHAR2(10)  
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AP_PAID_INVOICE_CONV.bad'
    DISCARDFILE 'XXWC_AP_PAID_INVOICE_CONV.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                  )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AP_PAID_INVOICE_CONV.csv'
       )
    )
   REJECT LIMIT UNLIMITED;
   /