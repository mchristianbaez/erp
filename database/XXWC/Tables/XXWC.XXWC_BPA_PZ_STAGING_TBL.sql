CREATE TABLE XXWC.XXWC_BPA_PZ_STAGING_TBL
  (creation_date          DATE   
  ,created_by             NUMBER 
  ,last_update_date       DATE   
  ,last_updated_by        NUMBER 
  ,vendor_name            VARCHAR2(240)
  ,vendor_num             VARCHAR2(30)
  ,vendor_site_code       VARCHAR2(15)
  ,po_number              VARCHAR2(240) NOT NULL
  ,item_number            VARCHAR2(40) NOT NULL
  ,item_description       VARCHAR2(240)
  ,vendor_item_number     VARCHAR2(240)
  ,expiration_date        DATE
  ,implement_date         DATE
  ,bpa_table_flag         NUMBER
  ,national_price         NUMBER
  ,national_quantity      NUMBER
  ,national_break         NUMBER
  ,promo_price            NUMBER
  ,promo_start_date       DATE
  ,promo_end_date         DATE
  ,promo_table_flag         NUMBER
  ,price_zone_price1        NUMBER 
  ,price_zone_quantity1     NUMBER
  ,price_zone_quantity_price1 NUMBER
  ,price_zone_price2        NUMBER 
  ,price_zone_quantity2     NUMBER
  ,price_zone_quantity_price2 NUMBER
  ,price_zone_price3        NUMBER 
  ,price_zone_quantity3     NUMBER
  ,price_zone_quantity_price3 NUMBER
  ,price_zone_price4        NUMBER 
  ,price_zone_quantity4     NUMBER
  ,price_zone_quantity_price4 NUMBER
  ,price_zone_price5        NUMBER 
  ,price_zone_quantity5     NUMBER
  ,price_zone_quantity_price5 NUMBER
  ,price_zone_price6        NUMBER 
  ,price_zone_quantity6     NUMBER
  ,price_zone_quantity_price6 NUMBER
  ,price_zone_price7        NUMBER 
  ,price_zone_quantity7     NUMBER
  ,price_zone_quantity_price7 NUMBER
  ,price_zone_price8        NUMBER 
  ,price_zone_quantity8     NUMBER
  ,price_zone_quantity_price8 NUMBER
  ,price_zone_price9        NUMBER 
  ,price_zone_quantity9     NUMBER
  ,price_zone_quantity_price9 NUMBER
  ,price_zone_price10        NUMBER 
  ,price_zone_quantity10     NUMBER
  ,price_zone_quantity_price10 NUMBER
  ,price_zone_price11        NUMBER 
  ,price_zone_quantity11     NUMBER
  ,price_zone_quantity_price11 NUMBER
  ,price_zone_price12        NUMBER 
  ,price_zone_quantity12     NUMBER
  ,price_zone_quantity_price12 NUMBER
  ,price_zone_price13        NUMBER 
  ,price_zone_quantity13     NUMBER
  ,price_zone_quantity_price13 NUMBER
  ,price_zone_price14        NUMBER 
  ,price_zone_quantity14     NUMBER
  ,price_zone_quantity_price14 NUMBER
  ,price_zone_price15        NUMBER 
  ,price_zone_quantity15     NUMBER
  ,price_zone_quantity_price15 NUMBER
  ,price_zone_price16        NUMBER 
  ,price_zone_quantity16     NUMBER
  ,price_zone_quantity_price16 NUMBER
  ,price_zone_price17        NUMBER 
  ,price_zone_quantity17     NUMBER
  ,price_zone_quantity_price17 NUMBER
  ,price_zone_price18        NUMBER 
  ,price_zone_quantity18     NUMBER
  ,price_zone_quantity_price18 NUMBER
  ,price_zone_price19        NUMBER 
  ,price_zone_quantity19     NUMBER
  ,price_zone_quantity_price19 NUMBER
  ,price_zone_price20        NUMBER 
  ,price_zone_quantity20     NUMBER
  ,price_zone_quantity_price20 NUMBER
  ,price_zone_price21        NUMBER 
  ,price_zone_quantity21     NUMBER
  ,price_zone_quantity_price21 NUMBER
  ,price_zone_price22        NUMBER 
  ,price_zone_quantity22     NUMBER
  ,price_zone_quantity_price22 NUMBER
  ,price_zone_price23        NUMBER 
  ,price_zone_quantity23     NUMBER
  ,price_zone_quantity_price23 NUMBER
  ,price_zone_price24        NUMBER 
  ,price_zone_quantity24     NUMBER
  ,price_zone_quantity_price24 NUMBER
  ,price_zone_price25        NUMBER 
  ,price_zone_quantity25     NUMBER
  ,price_zone_quantity_price25 NUMBER
  ,price_zone_price26        NUMBER 
  ,price_zone_quantity26     NUMBER
  ,price_zone_quantity_price26 NUMBER
  ,price_zone_price27        NUMBER 
  ,price_zone_quantity27     NUMBER
  ,price_zone_quantity_price27 NUMBER
  ,price_zone_price28        NUMBER 
  ,price_zone_quantity28     NUMBER
  ,price_zone_quantity_price28 NUMBER
  ,price_zone_price29        NUMBER 
  ,price_zone_quantity29     NUMBER
  ,price_zone_quantity_price29 NUMBER
  ,price_zone_price30        NUMBER 
  ,price_zone_quantity30     NUMBER
  ,price_zone_quantity_price30 NUMBER
  ,price_zone_table_flag   NUMBER
  ,ORG_PRICE_ZONE_1    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_2    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_3    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_4    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_5    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_6    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_7    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_8    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_9    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_10    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_11    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_12    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_13    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_14    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_15    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_16    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_17    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_18    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_19    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_20    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_21    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_22    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_23    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_24    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_25    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_26    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_27    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_28    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_29    VARCHAR2 (4000 Byte)
  ,ORG_PRICE_ZONE_30    VARCHAR2 (4000 Byte)
  ,APPROVE_FLAG         NUMBER);  
