 /*************************************************************************************
   $Header XXWC_PO_SUPPLIER_CONFIRMATIONS $
   Module Name: XXWC_PO_SUPPLIER_CONFIRMATIONS.sql

   PURPOSE:   This script creates a table for PO Supplier Confirmation Info.

   REVISIONS:
   Ver        Date        Author                             Description
   ---------  ----------  -------------------------------    -------------------------
   1.0        18/04/2018  Ashwin Sridhar                     Initial Version-TMS#20180305-00300--Automate PO Confirmations - Oracle EBS
  *************************************************************************************/
--DROP TABLE XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS
(
  PO_NUMBER            VARCHAR2(20 BYTE),
  VENDOR_NUM           VARCHAR2(30 BYTE),
  PO_TOTAL             NUMBER                   NOT NULL,
  CONFIRMATION_URL     VARCHAR2(500 BYTE),
  VENDOR_ID            NUMBER,
  VENDOR_SITE_ID       NUMBER,
  VENDOR_NAME          VARCHAR2(240 BYTE),
  ANYDOC_BATCH_ID      VARCHAR2(10 BYTE),
  ANYDOC_BCH_COMPL_DT  DATE,
  ORG_ID               NUMBER,
  PO_HEADER            NUMBER,
  PROCESSED_FLAG       VARCHAR2(10 BYTE),
  ERROR_MESSAGE        VARCHAR2(2000 BYTE),
  CREATION_DATE        DATE                     DEFAULT SYSDATE,
  CREATED_BY           VARCHAR2(30 BYTE),
  LAST_UPDATE_DATE     DATE,
  LAST_UPDATED_BY      VARCHAR2(30 BYTE)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

GRANT INSERT, SELECT ON XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS TO INTERFACE_APEXHDSORACLE;

GRANT INSERT, SELECT ON XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS TO INTERFACE_ECM;

GRANT SELECT ON XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS TO XXCUS_EIS_ROLE;