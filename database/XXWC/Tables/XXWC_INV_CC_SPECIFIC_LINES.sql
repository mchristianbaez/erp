CREATE TABLE XXWC.XXWC_INV_CC_SPECIFIC_LINES
(
  CC_SPECIFIC_LINE_ID    NUMBER                 NOT NULL,
  CYCLECOUNT_HDR_ID      NUMBER                 NOT NULL,
  TYPE                   VARCHAR2(1 BYTE),
  INV_ORG_ID             NUMBER,
  ITEM_NUMBER            VARCHAR2(100 BYTE),
  INV_ITEM_ID            NUMBER,
  CATEGORY               VARCHAR2(100 BYTE),
  CATEGORY_ID            NUMBER,
  PROCESS_DATE           DATE,
  PROCESS_STATUS         VARCHAR2(80 BYTE),
  REQUEST_ID             NUMBER,
  LAST_UPDATE_DATE       DATE                   NOT NULL,
  LAST_UPDATED_BY        NUMBER                 NOT NULL,
  CREATION_DATE          DATE                   NOT NULL,
  CREATED_BY             NUMBER                 NOT NULL,
  LAST_UPDATE_LOGIN      NUMBER,
  OBJECT_VERSION_NUMBER  NUMBER                 NOT NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


