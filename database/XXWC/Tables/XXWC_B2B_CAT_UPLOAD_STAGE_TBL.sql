/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_B2B_CAT_UPLOAD_STAGE_TBL $
  Module Name: XXWC_B2B_CAT_UPLOAD_STAGE_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029 
**************************************************************************/
-- Create table
create table XXWC.XXWC_B2B_CAT_UPLOAD_STAGE_TBL
(
  customer_id          NUMBER,
  customer_name        VARCHAR2(360),
  catalog_group_id     NUMBER,
  item_number          VARCHAR2(100),
  item_description     VARCHAR2(240),
  cust_catalog_name    VARCHAR2(20),
  transaction_flag     VARCHAR2(1),
  inventory_item_id    NUMBER,
  transaction_flag_num NUMBER
)
tablespace XXWC_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/