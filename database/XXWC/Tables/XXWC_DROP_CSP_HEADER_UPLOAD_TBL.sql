/*************************************************************************
      SCRIPT Name: XXWC_DROP_CSP_HEADER_UPLOAD_TBL.sql

      PURPOSE:   Drop table

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        22/06/2018    Niraj K Ranjan     TMS#20180622-00085   Roll back code in PROD. for TMS # 20180622-00062
 ***************************************************************************/
DROP TABLE XXWC.XXWC_CSP_HEADER_UPLOAD_TBL;