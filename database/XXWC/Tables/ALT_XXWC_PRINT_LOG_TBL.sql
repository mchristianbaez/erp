/*************************************************************************
  $Header ALT_XXWC_PRINT_LOG_TBL.sql $
  Module Name: ALT_XXWC_PRINT_LOG_TBL.sql

  PURPOSE:   DESCARTES Project	

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        01/08/2018  Rakesh Patel            TMS#20170901-00010 DMS Phase-2.0 Inbound Extract
**************************************************************************/
ALTER TABLE XXWC.XXWC_PRINT_LOG_TBL ADD ( SCHEDULE_DELIVERY_DATE   DATE                                              
								         );

ALTER TABLE XXWC.XXWC_PRINT_LOG_BK_TBL ADD ( SCHEDULE_DELIVERY_DATE   DATE                                              
								         );								         