/*************************************************************************
*   Table Name: xxwc_auth_buyer_deactivate
*
*   PURPOSE: XXWC Authorized Buyer De-activate Upload 

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     11/10/2017    Pattabhi Avula      TMS#20171003-00065Initial Release

*************************************************************************/
CREATE TABLE xxwc.XXWC_AUTH_BUYER_DEACTIVATE(role VARCHAR2(100),account_number VARCHAR2(100),account_id NUMBER(38));
/