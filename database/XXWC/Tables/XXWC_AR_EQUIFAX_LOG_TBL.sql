/************************************************************************************************************
* Table: XXWC.XXWC_AR_EQUIFAX_LOG_TBL
=============================================================================================================
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- ------------------------------------------------------------------
  1.0    19-Dec-2017       P.Vamshidhar    TMS#20131016-00419 - Credit - Implement online credit application 
                                                              - Customer Accounts/Update File
*************************************************************************************************************/
CREATE TABLE XXWC.XXWC_AR_EQUIFAX_LOG_TBL
(
  LOG_SEQ_NO     NUMBER,
  RECORD_ID      NUMBER,
  LOG_MSG        VARCHAR2(4000 BYTE),
  CREATION_DATE  DATE   DEFAULT SYSDATE
)
/