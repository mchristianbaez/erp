/*************************************************************************
    *   SCRIPT Name: XXWC_OM_CSP_MATRIX_CHANGE_REVERT.sql
    *
    *   PURPOSE:   Alter table XXWC_OM_CONTRACT_PRICING_HDR
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     03/26/2018     Niraj K ranjan   TMS#20180323-00157   Remove matrix price changes from EBSQA instance
*****************************************************************************/
drop index xxwc.XXWC_AGREEMENT_ID_N1;

drop package XXWC_OM_PRICING_CONV_PKG;

alter table xxwc.xxwc_om_contract_pricing_hdr DROP COLUMN agreement_type;

ALTER TABLE  XXWC.XXWC_OM_CSP_HEADER_ARCHIVE DROP COLUMN agreement_type;

alter table xxwc.XXWC_OM_CONTRACT_PRICING_HDR DROP COLUMN conversion_status; 

alter table xxwc.XXWC_OM_CSP_HEADER_ARCHIVE DROP COLUMN conversion_status; 

alter table xxwc.xxwc_om_contract_pricing_hdr DROP COLUMN request_id; 

alter table XXWC.XXWC_OM_CSP_HEADER_ARCHIVE DROP COLUMN  request_id;

alter table xxwc.XXWC_OM_CONTRACT_PRICING_LINES DROP COLUMN conversion_status;

alter table XXWC.XXWC_OM_CSP_LINES_ARCHIVE DROP COLUMN conversion_status;

alter table xxwc.XXWC_OM_CONTRACT_PRICING_LINES DROP COLUMN category_id;

alter table XXWC.XXWC_OM_CSP_LINES_ARCHIVE DROP COLUMN category_id;


