/* Formatted on 11/28/2012 6:07:19 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_ACCOUNT_MASS_REPORT
-- Generated 11/28/2012 6:07:12 PM from XXWC@EBIZDEV

CREATE GLOBAL TEMPORARY TABLE xxwc.xxwc_account_mass_report
(
    change_type           CHAR (41 BYTE)
   ,previous_name         CHAR (41 BYTE)
   ,new_name              CHAR (41 BYTE)
   ,site_uses_rowid       ROWID
   ,prof_rowid            ROWID
   ,account_number        VARCHAR2 (30 BYTE)
   ,account_name          VARCHAR2 (240 BYTE)
   ,account_address       VARCHAR2 (4000 BYTE)
   ,site_address          VARCHAR2 (4000 BYTE)
   ,location              VARCHAR2 (40 BYTE)
   ,collector_name        VARCHAR2 (30 BYTE)
   ,credit_analyst_name   VARCHAR2 (4000 BYTE)
   ,salesreps_name        VARCHAR2 (4000 BYTE)
   ,error_message         VARCHAR2 (1024 BYTE)
   ,run_id                NUMBER
)
ON COMMIT PRESERVE ROWS
 
/

-- End of DDL Script for Table XXWC.XXWC_ACCOUNT_MASS_REPORT
