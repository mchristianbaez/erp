CREATE TABLE XXWC.XXWC_INV_MASS_LOC_LOAD_IFACE
     /*************************************************************************
       $Header XXWC_INV_MASS_LOC_LOAD_IFACE $
       Module Name: XXWC_INV_MASS_LOC_LOAD_IFACE

       PURPOSE: Table to maintain the Stock Locator WEB ADI upload Data

       REVISIONS:
       Ver        Date         Author                Description
       ---------  -----------  ------------------    ----------------
       1.0        25-Oct-2013  Consuelo Gonzalez     Initial Version TMS # 20130228-01305
	   1.0        27-Nov-2013  Consuelo Gonzalez     TMS # 20131120-00024 - Added Delete_flag
	   3.0        18-May-2015  Manjula Chellappan    TMS # 20150430-00019 - Added Subinventory

     **************************************************************************/
(
  ORGANIZATION_CODE  VARCHAR2(3 BYTE)               NULL,
  ITEM_NUMBER        VARCHAR2(40 BYTE)              NULL,
  CUR_LOCATION       VARCHAR2(80 BYTE)              NULL,
  STATUS             VARCHAR2(100 BYTE)             NULL,
  DELETE_FLAG        VARCHAR2(1 BYTE)               NULL,
  CREATION_DATE      DATE                           NULL,
  CREATED_BY         NUMBER                         NULL,
  LAST_UPDATE_DATE   DATE                           NULL,
  LAST_UPDATED_BY    NUMBER                         NULL,
  LAST_UPDATE_LOGIN  NUMBER                         NULL,
  CUST_ASSIGN_SEQ    NUMBER                         NULL,
  --Added the org_id column for MultiOrg
  ORG_ID             NUMBER                     DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),-99),
  CUR_SUBINV         VARCHAR2(10 BYTE) -- Revision 3.0 
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

CREATE OR REPLACE SYNONYM APPS.XXWC_INV_MASS_LOC_LOAD_IFACE FOR XXWC.XXWC_INV_MASS_LOC_LOAD_IFACE;   --Added the synonym for MultiOrg

