/***************************************************************************************************************************************************
        $Header XXWCAP_INV_INT_STG_TBL $
        Module Name: XXWCAP_INV_INT_STG_TBL.sql

        PURPOSE:   TMS# 20180712-00088-AH Harries AP Invoices Interface

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        07/12/2018  Rakesh Patel     TMS# 20180712-00088-AH Harries AP Invoices Interface
******************************************************************************************************************************************************/
DROP TABLE XXWC.XXWCAP_INV_INT_STG_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWCAP_INV_INT_STG_TBL
(
  INVOICE_NUM         VARCHAR2(50 BYTE),
  VENDOR_NUM          VARCHAR2(40 BYTE),
  VENDOR_NAME         VARCHAR2(240 BYTE),
  INVOICE_AMOUNT      VARCHAR2(30 BYTE),
  GL_ACCOUNT          VARCHAR2(15 BYTE),
  INVOICE_DATE        DATE,
  ATTRIBUTE_CATEGORY  VARCHAR2(250 BYTE),
  ATTRIBUTE1          VARCHAR2(250 BYTE),
  ATTRIBUTE2          VARCHAR2(250 BYTE),
  ATTRIBUTE3          VARCHAR2(250 BYTE),
  ATTRIBUTE4          VARCHAR2(250 BYTE),
  ATTRIBUTE5          VARCHAR2(250 BYTE),
  ATTRIBUTE6          VARCHAR2(250 BYTE),
  ATTRIBUTE7          VARCHAR2(250 BYTE),
  ATTRIBUTE8          VARCHAR2(250 BYTE),
  ATTRIBUTE9          VARCHAR2(250 BYTE),
  ATTRIBUTE10         VARCHAR2(250 BYTE),
  ATTRIBUTE11         VARCHAR2(250 BYTE),
  ATTRIBUTE12         VARCHAR2(250 BYTE),
  ATTRIBUTE13         VARCHAR2(250 BYTE),
  ATTRIBUTE14         VARCHAR2(250 BYTE),
  ATTRIBUTE15         VARCHAR2(250 BYTE),
  STATUS              VARCHAR2(15 BYTE),
  ERROR_MESSAGE       VARCHAR2(2000 BYTE),
  STG_RECORD_ID       NUMBER,
  CREATED_BY          NUMBER,
  CREATION_DATE       DATE,
  LAST_UPDATED_BY     NUMBER,
  LAST_UPDATE_DATE    DATE,
  ORG_ID              NUMBER                    DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),162),
  TERM_NAME           VARCHAR2(100 BYTE),
  LINE_AMOUNT         NUMBER
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;

CREATE OR REPLACE SYNONYM APPS.XXWCAP_INV_INT_STG_TBL FOR XXWC.XXWCAP_INV_INT_STG_TBL;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWCAP_INV_INT_STG_TBL TO EA_APEX;