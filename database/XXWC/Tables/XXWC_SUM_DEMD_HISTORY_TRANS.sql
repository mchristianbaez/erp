CREATE TABLE xxwc.xxwc_sum_demd_history_trans
(
    transaction_id number
   ,CONSTRAINT xxwc_sum_demd_history_trans_pk PRIMARY KEY (transaction_id)
)
ORGANIZATION INDEX
TABLESPACE xxwc_data
PARALLEL (DEGREE 16)
