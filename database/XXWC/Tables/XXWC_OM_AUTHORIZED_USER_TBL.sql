/*******************************************************************************************************
  -- Table Name XXWC_OM_AUTHORIZED_USER_TBL
  -- ***************************************************************************************************
  --
  -- PURPOSE: External table used to store user detail which need to be inactivated
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     23-Jun-2016   Niraj K Ranjan   Initial Version
********************************************************************************************************/
CREATE TABLE XXWC.XXWC_OM_AUTHORIZED_USER_TBL
   ( NTID      VARCHAR2(100 BYTE),
     USER_NAME VARCHAR2(150 BYTE)
   )
   ORGANIZATION EXTERNAL
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AHH_USER_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE
	    SKIP 1
		BADFILE 'XXWC_OM_AUTHORIZED_USER.bad'
        DISCARDFILE 'XXWC_OM_AUTHORIZED_USER.dsc'
        FIELDS TERMINATED BY ','
		OPTIONALLY ENCLOSED BY '"' AND '"'
		REJECT ROWS WITH ALL NULL FIELDS
      )
      LOCATION
       (XXWC_AHH_USER_CONV_DIR:'OM_AUTHORIZED_USER.csv')
    )
REJECT LIMIT UNLIMITED ;
