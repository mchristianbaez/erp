CREATE TABLE XXWC.XXWC_RCV_INTORG_TBL
(
  CREATION_DATE            DATE,
  CREATED_BY               NUMBER,
  LAST_UPDATE_DATE         DATE,
  LAST_UPDATED_BY          NUMBER,
  EMPLOYEE_ID              NUMBER,
  ORG_ID                   NUMBER,
  TRANSACTION_TYPE         VARCHAR2(30 BYTE),
  RECEIPT_NUM              VARCHAR2(30 BYTE),
  SHIPMENT_NUM             VARCHAR2(30 BYTE),
  SHIPMENT_HEADER_ID       NUMBER,
  ORGANIZATION_ID          NUMBER,
  SHIPMENT_LINE_ID         NUMBER,
  TO_ORGANIZATION_ID       NUMBER,
  RECEIPT_SOURCE_CODE      VARCHAR2(25 BYTE),
  TRANSACTION_DATE         DATE,
  UNIT_OF_MEASURE          VARCHAR2(25 BYTE),
  PRIMARY_UNIT_OF_MEASURE  VARCHAR2(25 BYTE),
  UOM_CONVERSION_RATE      NUMBER,
  REQUISITION_NUMBER       VARCHAR2(20 BYTE),
  REQUISITION_LINE_NUM     NUMBER,
  CATEGORY_ID              NUMBER,
  INVENTORY_ITEM_ID        NUMBER,
  ITEM_NUMBER              VARCHAR2(40 BYTE),
  DESCRIPTION              VARCHAR2(240 BYTE),
  OE_HEADER_ID             NUMBER,
  ORDER_NUMBER             NUMBER,
  OE_LINE_ID               NUMBER,
  OE_LINE_NUMBER           NUMBER,
  QUANTITY_SHIPPED         NUMBER,
  QUANTITY_RECEIVED        NUMBER,
  QUANTITY_OPEN            NUMBER,
  EXPECTED_RECEIPT_DATE    DATE,
  SHIPPED_DATE             DATE,
  LOT_NUMBER               VARCHAR2(30),
  GROUP_ID                 NUMBER,
  BATCH_ID                 NUMBER,
  PROCESS_FLAG             NUMBER,
  MESSAGE                  VARCHAR2(2000 BYTE)
)



create synonym XXWC_RCV_INTORG_TBL for XXWC.XXWC_RCV_INTORG_TBL;