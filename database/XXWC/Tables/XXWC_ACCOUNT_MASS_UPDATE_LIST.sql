/* Formatted on 11/28/2012 6:04:32 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_ACCOUNT_MASS_UPDATE_LIST
-- Generated 11/28/2012 6:04:27 PM from XXWC@EBIZDEV

CREATE GLOBAL TEMPORARY TABLE xxwc.xxwc_account_mass_update_list
(
    table_rowid         VARCHAR2 (128 BYTE)
   ,table_name          VARCHAR2 (64 BYTE)
   ,run_id              NUMBER
   ,change_name         VARCHAR2 (64 BYTE)
   ,old_name            VARCHAR2 (128 BYTE)
   ,new_name            VARCHAR2 (128 BYTE)
   ,old_id              NUMBER
   ,new_id              NUMBER
   ,last_updated_by     NUMBER
   ,last_update_date    DATE
   ,responsibility_id   NUMBER
   ,org_id              NUMBER
   ,error_message       CLOB
   ,insert_date         DATE
)
ON COMMIT PRESERVE ROWS
 
/

-- End of DDL Script for Table XXWC.XXWC_ACCOUNT_MASS_UPDATE_LIST
