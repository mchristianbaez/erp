CREATE TABLE XXWC.XXWCAR_LOCKBOX_TBL
(
  CHECK_NBR                VARCHAR2(20 BYTE),
  LOCKBOX_NUMBER           VARCHAR2(30 BYTE)    NOT NULL,
  DEPOSIT_DATE             DATE,
  CHECK_AMT                NUMBER,
  CONTROL_NBR              VARCHAR2(15 BYTE),
  BANK_ORIGINATION_NUMBER  VARCHAR2(30 BYTE)    NOT NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


