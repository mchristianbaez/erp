drop table XXWC.XXWC_PO_VENDOR_MIN_PARAM_STG

CREATE TABLE XXWC.XXWC_PO_VENDOR_MIN_PARAM_STG
(
  ORGANIZATION_ID         NUMBER                NOT NULL,
  INVENTORY_ITEM_ID       NUMBER                NOT NULL,
  MP_DST_ORG_CODE         VARCHAR2(3 BYTE)          NULL,
  ITEM_NUMBER             VARCHAR2(40 BYTE)         NULL,
  SESSION_ID              NUMBER                    NULL,
  USER_ID                 NUMBER                    NULL
)
NOCACHE;
