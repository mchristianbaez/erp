/* Formatted on 21-Feb-2013 00:58:21 (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_SALES_ORDER_MMSI_LINE
-- Generated 21-Feb-2013 00:58:15 from XXWC@EBIZDEV

CREATE TABLE xxwc.xxwc_sales_order_mmsi_line
(
    line_to_file         VARCHAR2 (4000 BYTE)
   ,customer_name        VARCHAR2 (360 BYTE) NOT NULL
   ,customer_po_number   VARCHAR2 (50 BYTE)
   ,order_date           VARCHAR2 (10 BYTE)
   ,order_number         NUMBER NOT NULL
   ,line_number          NUMBER NOT NULL
   ,description          VARCHAR2 (240 BYTE)
   ,booked_date          DATE
   ,file_name            VARCHAR2 (128 BYTE)
   ,file_sent_date       DATE
   ,header_id            NUMBER
   ,line_id              NUMBER
   ,send_flag            VARCHAR2 (1 BYTE)
   ,account_number       VARCHAR2 (124 BYTE)
   ,run_id               NUMBER
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
NOPARALLEL
NOLOGGING
/

-- End of DDL Script for Table XXWC.XXWC_SALES_ORDER_MMSI_LINE
