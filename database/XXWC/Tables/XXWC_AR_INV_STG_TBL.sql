   /***************************************************************************************************************************************************
        $Header XXWC_AR_INV_STG_TBL $
        Module Name: XXWC_AR_INV_STG_TBL.sql

        PURPOSE:   AHH DM/Invoices Interface/Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018  P.Vamshidhar     TMS#20180708-00002 - AH HARRIS AR Debit Memo/Invoice Interface
		1.0        07/12/2018  P.Vamshidhar     TMS#20180708-00002 - Added new columns
   ******************************************************************************************************************************************************/

DROP TABLE XXWC.XXWC_AR_INV_STG_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_AR_INV_STG_TBL
(
  INTF_BATCH_ID                  NUMBER,
  INTF_STG_ID                    NUMBER,
  STATUS                         VARCHAR2(30 BYTE),
  ERROR_MESSAGE                  VARCHAR2(250 BYTE),
  TRX_DATE                       DATE,
  QUANTITY_ORDERED               NUMBER,
  UNIT_SELLING_PRICE             NUMBER,
  WAREHOUSE_ID                   VARCHAR2(10 BYTE),
  PURCHASE_ORDER                 VARCHAR2(50 BYTE),
  SALES_ORDER                    VARCHAR2(50 BYTE),
  PRIMARY_SALESREP_NUMBER        VARCHAR2(30 BYTE),
  PRIMARY_SALESREP_ID            VARCHAR2(10 BYTE),
  SALES_ORDER_DATE               DATE,
  TRX_NUMBER                     VARCHAR2(20 BYTE),
  TERM_NAME                      VARCHAR2(15 BYTE),
  TERM_ID                        VARCHAR2(10 BYTE),
  LEGAL_ENTITY_ID                NUMBER(15),
  ORG_ID                         NUMBER(15)     DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),162),
  PAYING_CUSTOMER_ID             NUMBER(15),
  PAYING_SITE_USE_ID             VARCHAR2(20 BYTE),
  GL_DATE                        DATE,
  REASON_CODE                    VARCHAR2(30 BYTE),
  SHIP_VIA                       VARCHAR2(25 BYTE),
  AMOUNT                         NUMBER,
  CODE_COMBINATION_ID            NUMBER,
  OVERRIDE_AUTO_ACCOUNTING_FLAG  VARCHAR2(1 BYTE),
  LINE_TYPE                      VARCHAR2(20 BYTE),
  ORIG_SYSTEM_BILL_CUSTOMER_REF  VARCHAR2(240 BYTE),
  ORIG_SYSTEM_SHIP_CUSTOMER_REF  VARCHAR2(240 BYTE),
  TAX_RATE                       NUMBER,
  TAX_CODE                       VARCHAR2(50 BYTE),
  TAX_PRECEDENCE                 NUMBER,
  INVENTORY_ITEM_NUM             VARCHAR2(50 BYTE),
  ITEM_DESCRIPTION               VARCHAR2(200 BYTE),
  REVENUE_AMOUNT                 NUMBER,
  FREIGHT_AMOUNT                 NUMBER,
  TAX_AMOUNT                     NUMBER,
  REVENUE_ACCOUNT                VARCHAR2(30 BYTE),
  FREIGHT_ACCOUNT                VARCHAR2(30 BYTE),
  TAX_ACCOUNT                    VARCHAR2(30 BYTE),
  UOM_CODE                       VARCHAR2(10 BYTE),
  UOM_NAME                       VARCHAR2(25 BYTE),
  LINE_NUMBER                    NUMBER(15),
  ORIG_SYSTEM_BILL_CUSTOMER_ID   NUMBER(15),
  CUST_TRX_TYPE_ID               NUMBER(15),
  TRANSACTION_CLASS              VARCHAR2(250 BYTE),
  TAKEN_BY                       VARCHAR2(250 BYTE),
  QUANTITY_BACK_ORDERED          NUMBER,
  QUANTITY_SHIPPED               NUMBER,
  HDS_SITE_FLAG                  VARCHAR2(250 BYTE),
  LEGACY_PARTY_SITE_NUMBER       VARCHAR2(250 BYTE),
  CATEGORY_CLASS                 VARCHAR2(10 BYTE),
  DIRECT_FLAG                    VARCHAR2(1 BYTE),
  PRISM_INV_TYPE                 VARCHAR2(50 BYTE),
  ORDERED_BY                     VARCHAR2(250 BYTE),
  RECEIVED_BY                    VARCHAR2(250 BYTE),
  CREATED_BY                     NUMBER,
  CREATION_DATE                  DATE,
  LAST_UPDATED_BY                NUMBER,
  LAST_UPDATE_DATE               DATE,
  STG_ATTRIBUTE1                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE2                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE3                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE4                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE5                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE6                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE7                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE8                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE9                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE10                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE11                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE12                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE13                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE14                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE15                VARCHAR2(250 BYTE),
  RENTAL_START_DATE              DATE,
  RENTAL_END_DATE                DATE,
  STG_ATTRIBUTE16                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE17                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE18                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE19                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE20                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE21                VARCHAR2(250 BYTE)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX XXWC.XXWC_AR_INV_STG_TBL_N1 ON XXWC.XXWC_AR_INV_STG_TBL
(STATUS)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


CREATE INDEX XXWC.XXWC_AR_INV_STG_TBL_N2 ON XXWC.XXWC_AR_INV_STG_TBL
(TRX_NUMBER)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER APPS.XXWC_AR_INV_STG_TBL_TRG
  BEFORE INSERT ON XXWC.XXWC_AR_INV_STG_TBL
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
DECLARE
  /**************************************************************************
  File Name: XXWC_AR_INV_STG_TBL_TRG

  PROGRAM TYPE: SQL Script

  PURPOSE:

  HISTORY
  =============================================================================
         Last Update Date : 05/15/2012
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     14-May-2012   Gopi Damuluri      Creation of trigger

  =============================================================================
  *****************************************************************************/
  l_err_msg       VARCHAR2(3000);
  l_sec           VARCHAR2(255);
  l_msg           VARCHAR2(150);

  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXWC_AR_INV_STG_TBL_TRG';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

BEGIN
  ---------------------------------------------------------------------------------------------
  -- Update XXWC_AR_INV_STG_TBL.INTF_STG_ID using the sequence XXWC_AR_INV_STG_TBL_SEQ
  ---------------------------------------------------------------------------------------------
--  IF (:new.intf_stg_id = '') AND (:NEW.status = 'NEW') THEN
    :NEW.intf_stg_id := XXWC_AR_INV_STG_TBL_SEQ.NEXTVAL;
--  END IF;

EXCEPTION
  WHEN OTHERS THEN

    l_msg := 'Failure in Trigger XXWC_AR_INV_STG_TBL_TRG: ' || SQLERRM;
    -- Calling ERROR API
    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom
                                        ,p_calling     => l_err_callpoint
                                         --,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => SQLERRM
                                        ,p_error_desc        => l_msg
                                        ,p_argument1         => :new.trx_number
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AR');
END XXWC_AR_INV_STG_TBL_TRG;
/


CREATE OR REPLACE SYNONYM APPS.XXWC_AR_INV_STG_TBL FOR XXWC.XXWC_AR_INV_STG_TBL;


GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_INV_STG_TBL TO EA_APEX;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_INV_STG_TBL TO INTERFACE_APEXWC;

GRANT ALTER, DELETE, INSERT, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_AR_INV_STG_TBL TO INTERFACE_PRISM;

GRANT ALTER, DELETE, INSERT, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_AR_INV_STG_TBL TO INTERFACE_XXCUS;

GRANT ALTER, DELETE, INSERT, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_AR_INV_STG_TBL TO XXWC_DEV_ADMIN_ROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_INV_STG_TBL TO XXWC_EDIT_IFACE_ROLE;

GRANT ALTER, DELETE, INSERT, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_AR_INV_STG_TBL TO XXWC_PRISM_EXECUTE_ROLE;

GRANT INSERT ON XXWC.XXWC_AR_INV_STG_TBL TO XXWC_PRISM_INSERT_ROLE;

GRANT SELECT ON XXWC.XXWC_AR_INV_STG_TBL TO XXWC_PRISM_SELECT_ROLE;
/