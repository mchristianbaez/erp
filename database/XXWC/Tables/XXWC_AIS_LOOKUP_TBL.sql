/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_AIS_LOOKUP_TBL $
  Module Name: XXWC.XXWC_AIS_LOOKUP_TBL

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-apr-2017   Pahwa, Nancy                Initially Created 
TMS# 20170302-00126
**************************************************************************/
CREATE TABLE  XXWC."XXWC_AIS_LOOKUP_TBL" 
   (     "ID" NUMBER NOT NULL ENABLE, 
         "CUTOFF_DATE" DATE, 
          CONSTRAINT "XXWC_AIS_LOOKUP_TBL_PK" PRIMARY KEY ("ID")
  USING INDEX  ENABLE
   );