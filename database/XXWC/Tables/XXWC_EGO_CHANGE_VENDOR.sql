/****************************************************************************************************************************
File Name: XXWC_EGO_CHANGE_VENDOR.sql
TYPE:     Table

Description: Table used to main vendor data change order or change request wise.

 Ver        Date          Author                     Description
---------  -----------  ---------------    ----------------------------------------------------------------------------------
 1.0       01-JUN-2015  P.Vamshidhar       TMS#20150519-00091  - Enhancements.
                                           Initial Version
*****************************************************************************************************************************/
CREATE TABLE XXWC.XXWC_EGO_CHANGE_VENDOR
(
  VENDOR_NUMBER     VARCHAR2(40 BYTE),
  LOGIN_ID          NUMBER,
  STATUS            VARCHAR2(1 BYTE),
  CREATION_DATE     DATE,
  LAST_UPDATE_DATE  DATE,
  CHANGE_NAME       VARCHAR2(40 BYTE)
)

GRANT ALL ON XXWC.XXWC_EGO_CHANGE_VENDOR TO APPS;