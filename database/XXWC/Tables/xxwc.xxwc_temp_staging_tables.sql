/* Formatted on 10-Sep-2013 14:25:22 (QP5 v5.206) */
CREATE TABLE xxwc.xxwc_temp_staging_tables
(
    package_name   VARCHAR2 (164)
   ,schema_name    VARCHAR2 (64)
   ,table_name     VARCHAR2 (64)
   ,creation       DATE
   ,description1   VARCHAR2 (512)
   ,description2   VARCHAR2 (512)
   ,description3   VARCHAR2 (512)
   ,description4   VARCHAR2 (512)
   ,description5   VARCHAR2 (512)
)
--drop table xxwc.xxwc_temp_staging_tables
