  /*******************************************************************************
  Table:   XXWC_OM_DMS_SHIP_CONFIRM_BKP
  Description: This table is used to backup file info from MyLogistics with the delivered 
               Orders
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     22-Feb-2016        Gopi Damuluri   Initial version TMS# 20160217-00165
  ********************************************************************************/
DROP TABLE XXWC.XXWC_OM_DMS_SHIP_CONFIRM_BKP CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_OM_DMS_SHIP_CONFIRM_BKP
(
  ORDER_NUMBER            NUMBER,
  LINE_NUMBER             NUMBER,
  DELIVERY_ID             NUMBER,
  BRANCH                  VARCHAR2(50 BYTE),
  DELIVERED_QTY           NUMBER,
  STOP_EXCEPTION          VARCHAR2(8 BYTE),
  LINE_EXCEPTION          VARCHAR2(8 BYTE),
  SIGNED_BY               VARCHAR2(60 BYTE),
  NOTES                   VARCHAR2(500 BYTE),
  DRIVER_NAME             VARCHAR2(60 BYTE),
  ROUTE_NUMBER            NUMBER,
  ROUTE_NAME              VARCHAR2(20 BYTE),
  DISPATCH_DATE           DATE,
  CREATION_DATE           DATE,
  CREATED_BY              NUMBER,
  LAST_UPDATE_DATE        DATE,
  LAST_UPDATED_BY         NUMBER,
  SHIP_CONFIRM_STATUS     VARCHAR2(10 BYTE),
  SHIP_CONFIRM_EXCEPTION  VARCHAR2(240 BYTE),
  ORG_ID                  NUMBER
  , FILE_NAME             VARCHAR2(200)
)