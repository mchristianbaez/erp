/* Formatted on 3/4/2014 1:32:13 AM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.DEFERRED_TO_DELETE
-- Generated 3/4/2014 1:32:05 AM from XXWC@EBIZFQA

CREATE TABLE xxwc.XXWC_AP_HOLD_DEFERRED 
(
    nid     NUMBER
   ,msgid   RAW (16)
)
SEGMENT CREATION DEFERRED
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
NOCACHE
MONITORING
NOPARALLEL
NOLOGGING
/

-- End of DDL Script for Table XXWC.DEFERRED_TO_DELETE
