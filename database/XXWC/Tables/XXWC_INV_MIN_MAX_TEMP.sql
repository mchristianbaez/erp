CREATE TABLE XXWC.XXWC_INV_MIN_MAX_TEMP
(
  ITEM_SEGMENTS            VARCHAR2(800 BYTE),
  DESCRIPTION              VARCHAR2(240 BYTE),
  ERROR                    VARCHAR2(80 BYTE),
  SORTEE                   VARCHAR2(300 BYTE),
  SUBINVENTORY_CODE        VARCHAR2(10 BYTE),
  MIN_QTY                  NUMBER,
  MAX_QTY                  NUMBER,
  ONHAND_QTY               NUMBER,
  SUPPLY_QTY               NUMBER,
  DEMAND_QTY               NUMBER,
  TOT_AVAIL_QTY            NUMBER,
  MIN_ORD_QTY              NUMBER,
  MAX_ORD_QTY              NUMBER,
  FIX_MULT                 NUMBER,
  REORD_QTY                NUMBER,
  CREATION_DATE            DATE,
  REQUEST_ID               NUMBER,
  ORGANIZATION_CODE        VARCHAR2(3 BYTE),
  INVENTORY_ITEM_ID        NUMBER,
  ORGANIZATION_ID          NUMBER,
  INVENTORY_PLANNING_CODE  NUMBER,
  SAFETY_STOCK             NUMBER,
  REORDER_POINT            NUMBER,
  LEAD_TIME_DEMAND         NUMBER,
  AMU                      NUMBER,
  RESERVE_STOCK            NUMBER,
  EOQ                      NUMBER,
  PROCESS_LEAD_TIME        NUMBER,
  LEAD_TIME                NUMBER,
  REVIEW_TIME              NUMBER
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


