DROP TABLE xxwc.xxwc_cb_ps_gtt_tbl;

CREATE GLOBAL TEMPORARY TABLE xxwc.xxwc_cb_ps_gtt_tbl
( customer_id            NUMBER
, dup_flag               VARCHAR2(1))
ON COMMIT PRESERVE ROWS
NOCACHE;