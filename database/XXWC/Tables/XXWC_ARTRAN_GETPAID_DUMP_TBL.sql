CREATE TABLE XXWC.XXWC_ARTRAN_GETPAID_DUMP_TBL
(
  CUSTNO           VARCHAR2(20 BYTE),
  CUSTNO_ORIGINAL  VARCHAR2(20 BYTE),
  INVNO            VARCHAR2(20 BYTE),
  INVNO_ORIGINAL   VARCHAR2(20 BYTE),
  ITEM             VARCHAR2(16 BYTE),
  DESCRIP          VARCHAR2(65 BYTE),
  DISC             FLOAT(126),
  PRICE            FLOAT(126),
  QTYSHP           FLOAT(126),
  EXTPRICE         FLOAT(126),
  SEQNUM           INTEGER,
  UNIQUEID         INTEGER,
  UOM              VARCHAR2(10 BYTE),
  QTYBKO           INTEGER,
  QTYORD           INTEGER,
  TAXABLEITEM      VARCHAR2(1 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


