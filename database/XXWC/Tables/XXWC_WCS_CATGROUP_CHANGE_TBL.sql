/********************************************************************************************************************
$Header XXWC_WCS_CATGROUP_CHANGE_TBL.sql $

Module Name: XXWC_WCS_CATGROUP_CHANGE_TBL

PURPOSE: Table to track catagory changes for ecomm extract purpose.

REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------------------------------------
1.0        15-Sep-2015  P.Vamshidhar          Initial Version TMS#20150817-00185 - PDH E-Comm catalog file changes

********************************************************************************************************************/
CREATE TABLE XXWC.XXWC_WCS_CATGROUP_CHANGE_TBL
(
  CATEGORY_ID            NUMBER,
  REQUEST_ID             NUMBER,
  EXECUTION_DATE         DATE,
  GROUPIDENTIFIER        VARCHAR2(163 BYTE),
  TOPGROUP               CHAR(5 BYTE),
  PARENTGROUPIDENTIFIER  VARCHAR2(163 BYTE),
  SEQUENCE               NUMBER,
  NAME                   VARCHAR2(163 BYTE),
  SHORTDESCRIPTION       VARCHAR2(240 BYTE),
  LONGDESCRIPTION        CHAR(1 BYTE),
  THUMBNAIL              VARCHAR2(4000 BYTE),
  FULLIMAGE              VARCHAR2(4000 BYTE),
  KEYWORD                CHAR(1 BYTE),
  DELETE_FLAG            NUMBER
);
/
GRANT ALL ON XXWC.XXWC_WCS_CATGROUP_CHANGE_TBL TO APPS;
/