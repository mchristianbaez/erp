/*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header XXWC_AR_BT_HEADER_TBL
     Module Name: XXWC_AR_BT_HEADER_TBL

     PURPOSE:   

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0         10/02/2015  Maharajan Shunmugam    TMS#20150805-00039 Billtrust - Improvements to BT extract logic
   **************************************************************************/

CREATE TABLE XXWC.XXWC_AR_BT_HEADER_TBL
(
  CUST_ACCOUNT_ID             NUMBER(15)        NOT NULL,
  CUSTOMER_ACCOUNT_NUMBER     VARCHAR2(30 BYTE) NOT NULL,
  CUSTOMER_NAME               VARCHAR2(360 BYTE) NOT NULL,
  CUST_PAYMENT_TERM           VARCHAR2(240 BYTE),
  PRIM_BILL_TO_ID             NUMBER(15)        NOT NULL,
  PRIM_BILL_TO_SITE           VARCHAR2(40 BYTE) NOT NULL,
  PRIM_BILL_TO_ADDRESS1       VARCHAR2(240 BYTE) NOT NULL,
  PRIM_BILL_TO_ADDRESS2       VARCHAR2(481 BYTE),
  PRIM_BILL_TO_CITY           VARCHAR2(60 BYTE),
  PRIM_BILL_TO_CITY_PROVINCE  VARCHAR2(60 BYTE),
  PRIM_BILL_TO_ZIP_CODE       VARCHAR2(60 BYTE),
  PRIM_BILL_TO_COUNTRY        VARCHAR2(60 BYTE) NOT NULL,
  ACCOUNT_MANAGER             VARCHAR2(240 BYTE),
  ACCT_MGR_OFFICE_NUMBER      VARCHAR2(4000 BYTE),
  REMIT_TO_ADDRESS_CODE       VARCHAR2(150 BYTE),
  REMIT_TO_LN1                VARCHAR2(720 BYTE),
  REMIT_TO_LN2                VARCHAR2(720 BYTE),
  REMIT_TO_LN3                VARCHAR2(720 BYTE),
  REMIT_TO_LN4                VARCHAR2(720 BYTE),
  REMIT_TO_LN5                VARCHAR2(720 BYTE),
  STMT_BY_JOB                 VARCHAR2(150 BYTE),
  SEND_STATEMENT_FLAG         VARCHAR2(1 BYTE),
  SEND_CREDIT_BAL_FLAG        VARCHAR2(1 BYTE),
  PAYMENT_SCHEDULE_ID         NUMBER(15)        NOT NULL,
  TRX_BILL_TO_SITE            VARCHAR2(40 BYTE) NOT NULL,
  TRX_CUSTOMER_ID             NUMBER(15),
  TRX_BILL_SITE_USE_ID        NUMBER(15),
  TRX_SITE_USE_CODE           VARCHAR2(30 BYTE) NOT NULL,
  TRX_ADDR_TYPE               VARCHAR2(150 BYTE),
  CUSTOMER_JOB_NUMBER         VARCHAR2(30 BYTE),
  TRX_NUMBER                  VARCHAR2(30 BYTE),
  TRX_DATE                    DATE,
  CUSTOMER_PO_NUMBER          VARCHAR2(50 BYTE),
  PMT_STATUS                  VARCHAR2(30 BYTE),
  TRX_TYPE                    VARCHAR2(20 BYTE),
  AMOUNT_DUE_ORIGINAL         NUMBER            NOT NULL,
  AMOUNT_DUE_REMAINING        NUMBER            NOT NULL,
  TRX_AGE                     NUMBER,
  CURRENT_BALANCE             NUMBER,
  THIRTY_DAYS_BAL             NUMBER,
  SIXTY_DAYS_BAL              NUMBER,
  NINETY_DAYS_BAL             NUMBER,
  OVER_NINETY_DAYS_BAL        NUMBER);