CREATE TABLE XXWC.XXWC_AR_INV_STG_TBL_20120531
(
  INTF_BATCH_ID                  NUMBER,
  INTF_STG_ID                    NUMBER,
  STATUS                         VARCHAR2(30 BYTE),
  ERROR_MESSAGE                  VARCHAR2(250 BYTE),
  TRX_DATE                       DATE,
  QUANTITY_ORDERED               NUMBER,
  UNIT_SELLING_PRICE             NUMBER,
  WAREHOUSE_ID                   NUMBER(15),
  PURCHASE_ORDER                 VARCHAR2(50 BYTE),
  SALES_ORDER                    VARCHAR2(50 BYTE),
  PRIMARY_SALESREP_NUMBER        VARCHAR2(30 BYTE),
  PRIMARY_SALESREP_ID            NUMBER(15),
  SALES_ORDER_DATE               DATE,
  TRX_NUMBER                     VARCHAR2(20 BYTE),
  TERM_NAME                      VARCHAR2(15 BYTE),
  TERM_ID                        NUMBER(15),
  LEGAL_ENTITY_ID                NUMBER(15),
  ORG_ID                         NUMBER(15),
  PAYING_CUSTOMER_ID             NUMBER(15),
  PAYING_SITE_USE_ID             NUMBER(15),
  GL_DATE                        DATE,
  REASON_CODE                    VARCHAR2(30 BYTE),
  SHIP_VIA                       VARCHAR2(25 BYTE),
  AMOUNT                         NUMBER,
  CODE_COMBINATION_ID            NUMBER,
  OVERRIDE_AUTO_ACCOUNTING_FLAG  VARCHAR2(1 BYTE),
  LINE_TYPE                      VARCHAR2(20 BYTE),
  ORIG_SYSTEM_BILL_CUSTOMER_REF  VARCHAR2(240 BYTE),
  ORIG_SYSTEM_SHIP_CUSTOMER_REF  VARCHAR2(240 BYTE),
  TAX_RATE                       NUMBER,
  TAX_CODE                       VARCHAR2(50 BYTE),
  TAX_PRECEDENCE                 NUMBER,
  INVENTORY_ITEM_NUM             VARCHAR2(50 BYTE),
  ITEM_DESCRIPTION               VARCHAR2(200 BYTE),
  REVENUE_AMOUNT                 NUMBER,
  FREIGHT_AMOUNT                 NUMBER,
  TAX_AMOUNT                     NUMBER,
  REVENUE_ACCOUNT                VARCHAR2(30 BYTE),
  FREIGHT_ACCOUNT                VARCHAR2(30 BYTE),
  TAX_ACCOUNT                    VARCHAR2(30 BYTE),
  UOM_CODE                       VARCHAR2(3 BYTE),
  UOM_NAME                       VARCHAR2(25 BYTE),
  LINE_NUMBER                    NUMBER(15),
  ORIG_SYSTEM_BILL_CUSTOMER_ID   NUMBER(15),
  CUST_TRX_TYPE_ID               NUMBER(15),
  TRANSACTION_CLASS              VARCHAR2(250 BYTE),
  TAKEN_BY                       VARCHAR2(250 BYTE),
  QUANTITY_BACK_ORDERED          NUMBER,
  QUANTITY_SHIPPED               NUMBER,
  HDS_SITE_FLAG                  VARCHAR2(250 BYTE),
  LEGACY_PARTY_SITE_NUMBER       VARCHAR2(250 BYTE),
  CATEGORY_CLASS                 VARCHAR2(10 BYTE),
  DIRECT_FLAG                    VARCHAR2(1 BYTE),
  PRISM_INV_TYPE                 VARCHAR2(50 BYTE),
  ORDERED_BY                     VARCHAR2(250 BYTE),
  RECEIVED_BY                    VARCHAR2(250 BYTE),
  CREATED_BY                     NUMBER,
  CREATION_DATE                  DATE,
  LAST_UPDATED_BY                NUMBER,
  LAST_UPDATE_DATE               DATE,
  STG_ATTRIBUTE1                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE2                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE3                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE4                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE5                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE6                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE7                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE8                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE9                 VARCHAR2(250 BYTE),
  STG_ATTRIBUTE10                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE11                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE12                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE13                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE14                VARCHAR2(250 BYTE),
  STG_ATTRIBUTE15                VARCHAR2(250 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


