CREATE TABLE XXWC.XXWC_OM_CASH_REFUND_TBL
(
  HEADER_ID            NUMBER,
  RETURN_HEADER_ID     NUMBER,
  CASH_RECEIPT_ID      NUMBER,
  PAYMENT_TYPE_CODE    VARCHAR2(30 BYTE),
  ADJ_CCID             NUMBER,
  REFUND_DATE          DATE,
  REFUND_AMOUNT        NUMBER,
  CHECK_REFUND_AMOUNT  NUMBER,
  PROCESS_FLAG         VARCHAR2(1 BYTE),
  PROCESS_DATE         DATE,
  ERROR_MESSAGE        VARCHAR2(2000 BYTE),
  CREATION_DATE        DATE,
  CREATED_BY           NUMBER,
  LAST_UPDATE_DATE     DATE,
  LAST_UPDATED_BY      NUMBER,
  LAST_UPDATE_LOGIN    NUMBER
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


