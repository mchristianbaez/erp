/* Formatted on 20-Mar-2013 22:10:28 (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.AR_INVOICE_LINE_TEMP
-- Generated 20-Mar-2013 22:10:19 from XXWC@EBIZFQA

CREATE TABLE xxwc.ar_invoice_line_temp
(
    customer_trx_id                 NUMBER (15, 0) NOT NULL
   ,creation_date                   DATE NOT NULL
   ,trx_date                        DATE NOT NULL
   ,purchase_order                  VARCHAR2 (50 BYTE)
   ,org_id                          NUMBER (15, 0)
   ,batch_source_id                 NUMBER (15, 0)
   ,line_number                     NUMBER NOT NULL
   ,line_type                       VARCHAR2 (20 BYTE) NOT NULL
   ,sales_order_date                DATE
   ,description                     VARCHAR2 (240 BYTE)
   ,uom_code                        VARCHAR2 (3 BYTE)
   ,quantity_invoiced               NUMBER
   ,quantity_credited               NUMBER
   ,quantity_ordered                NUMBER
   ,unit_selling_price              NUMBER
   ,extended_amount                 NUMBER NOT NULL
   ,customer_trx_line_id            NUMBER (15, 0) NOT NULL
   ,interface_line_attribute10      VARCHAR2 (150 BYTE)
   ,inventory_item_id               NUMBER (15, 0)
   ,interface_line_attribute6       VARCHAR2 (150 BYTE)
   ,interface_line_context          VARCHAR2 (30 BYTE)
   ,warehouse_id                    NUMBER (15, 0)
   ,insert_flag                     VARCHAR2 (12 BYTE)
   ,mst_organization_id             NUMBER
   ,rctla_gd_customer_trx_id        NUMBER
   ,rctla_gd_customer_trx_line_id   NUMBER
   ,invoice_post_date               DATE
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
NOPARALLEL
NOLOGGING
/

-- End of DDL Script for Table XXWC.AR_INVOICE_LINE_TEMP
