  /********************************************************************************
  FILE NAME: XXWC_ITEM_MASTER_REF.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Item Master Reference table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version. 20180716-00075
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- DROP TABLE
DROP TABLE XXWC.XXWC_ITEM_MASTER_REF;


-- CREATE TABLE
CREATE TABLE XXWC.XXWC_ITEM_MASTER_REF
(
  ICSP_PROD                      VARCHAR2(100),
  ITEM                           VARCHAR2(100),
  ICSP_DESCRIP1                  VARCHAR2(2000),
  DESCRIPTION                    VARCHAR2(2000),
  ICSP_DESCRIP2                  VARCHAR2(2000),
  LONG_DESCRIPTION               VARCHAR2(2000),
  ICSP_STATUSTY                  VARCHAR2(100),
  ITEM_STATUS                    VARCHAR2(100),
  USER_ITEM_TYPE                 VARCHAR2(100),
  ICSW_STATUSTY                  VARCHAR2(100),
  ICSP_LIFOCAT                   VARCHAR2(100),
  SHELF_LIFE_DAYS                VARCHAR2(100),
  ICSW_ARPVENDNO                 VARCHAR2(100),
  VENDOR_OWNER_NUMBER            VARCHAR2(100),
  APSV_NAME                      VARCHAR2(100),
  VENDOR_NAME                    VARCHAR2(100),
  ICSW_VENDPROD                  VARCHAR2(100),
  VENDOR_PART_NUMBER             VARCHAR2(100),
  ICSP_UNITSTOCK                 VARCHAR2(100),
  ORACLE_PRIMARY_UNIT_OF_MEASURE VARCHAR2(100),
  ICSP_LENGTH                    VARCHAR2(100),
  UNIT_LENGTH                    VARCHAR2(100),
  ICSP_HEIGHT                    VARCHAR2(100),
  UNIT_HEIGHT                    VARCHAR2(100),
  ICSP_WIDTH                     VARCHAR2(100),
  UNIT_WIDTH                     VARCHAR2(100),
  ICSP_WEIGHT                    VARCHAR2(100),
  UNIT_WEIGHT                    VARCHAR2(100),
  PART_TYPE                      VARCHAR2(100),
  ICSP_USER1_DOT_CODE            VARCHAR2(100),
  INV_CAT_CLASS                  VARCHAR2(100),
  FINAL_CATMGT_GRP               VARCHAR2(2000),
  FINAL_CATMGT_SUBCATEGORY_DESC  VARCHAR2(2000),
  FINAL_CATCLASS_DESCRIPTION     VARCHAR2(2000),
  ICSW_NONTAXTY                  VARCHAR2(100),
  ICSW_TAXABLETY                 VARCHAR2(100),
  ICSW_TAXGROUP                  VARCHAR2(100),
  ICSW_TAXTYPE                   VARCHAR2(100),
  HAZMAT_DOT_CODE                VARCHAR2(100),
  HAMAT_DESCRIPTION              VARCHAR2(2000),
  HAZMAT_FLAG                    VARCHAR2(100),
  HAZARD_CLASS                   VARCHAR2(100),
  ENTERDT                        VARCHAR2(100),
  CREATE_DATE                    VARCHAR2(100),
  WC_AVP_CODE                    VARCHAR2(100)
);
-- CREATE/RECREATE INDEXES 
CREATE INDEX XXWC.XXWC_ITEM_MASTER_REF_N1 ON XXWC.XXWC_ITEM_MASTER_REF (ICSP_PROD, ITEM);
CREATE INDEX XXWC.XXWC_ITEM_MASTER_REF_N2 ON XXWC.XXWC_ITEM_MASTER_REF (ICSP_PROD);
