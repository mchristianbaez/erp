CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_OEH_WCD_GTT_TBL
(
  ORDER_NUMBER      NUMBER                      NOT NULL,
  ATTRIBUTE2        VARCHAR2(240 BYTE),
  SHIP_FROM_ORG_ID  NUMBER,
  HEADER_ID         NUMBER                      NOT NULL
)
ON COMMIT PRESERVE ROWS;


GRANT ALL ON xxwc.xxwc_oeh_wcd_gtt_tbl TO XXEIS;

CREATE INDEX xxwc.xxwc_oeh_wcd_gtt_tbl_n1 ON xxwc.xxwc_oeh_wcd_gtt_tbl(header_id, ship_from_org_id);

-- GRANT ALL ON xxwc.xxwc_oeh_wcd_gtt_tbl_n1 TO XXEIS;