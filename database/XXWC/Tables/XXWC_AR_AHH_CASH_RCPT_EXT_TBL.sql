 /********************************************************************************
  FILE NAME: "XXWC"."XXWC_AR_AHH_CASH_RCPT_EXT_TBL" 

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_AR_AHH_CASH_RCPT_EXT_TBL" 
   (  "RECEIPT_NUMBER" VARCHAR2(50), 
  "RECEIPT_METHOD" VARCHAR2(50), 
  "RECEIPT_DATE" VARCHAR2(100), 
  "RECEIPT_AMOUNT" NUMBER, 
  "CURRENCY_CODE" VARCHAR2(10), 
  "LEGACY_CUSTOMER_NUM" VARCHAR2(20), 
  "LEGACY_CUSTOMER_SITE" VARCHAR2(30), 
  "BANK_BRANCH_NAME" VARCHAR2(50), 
  "BANK_ACCOUNT_NUMBER" VARCHAR2(50), 
  "COMMENTS" VARCHAR2(1000), 
  "GL_DATE" VARCHAR2(100), 
  "RECEIPT_REFERENCE" VARCHAR2(100), 
  "DISPUTE_CODE" VARCHAR2(100)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AHH_CASH_RCPTS_CONV_LOAD.bad'
    DISCARDFILE 'XXWC_AHH_CASH_RCPTS_CONV_LOAD.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
            )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AHH_CASH_RCPTS_CONV_LOAD.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;