DROP TABLE XXWC.XXWC_B2B_SO_HDRS_STG_TBL CASCADE CONSTRAINTS;
/

  /*********************************************************************************
  -- Table Name XXWC_B2B_SO_LINES_STG_TBL
  -- *******************************************************************************
  --
  -- PURPOSE: Staging table used to store B2B Sales Order Headers
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     04-Apr-2015   Gopi Damuluri   TMS# 20150317-00069, 20150302-00006
                                           Added PROJECT_ID, PROJECT_NAME, COMPANY_ID, ACCOUNT_NUMBER and NOTES column to XXWC.XXWC_B2B_SO_HDRS_STG_TBL table
  -- 1.1     21-Sep-2015   Gopi Damuluri   TMS# 20150921-00041
  --                                       Added DELIVER_POA
  *******************************************************************************/

CREATE TABLE XXWC.XXWC_B2B_SO_HDRS_STG_TBL
(
  CUSTOMER_PO_NUMBER          VARCHAR2(50 BYTE),
  ORDER_TOTAL                 NUMBER,
  CUSTOMER_NUMBER             VARCHAR2(30 BYTE),
  SHIP_TO_NUMBER              VARCHAR2(30 BYTE),
  SHIP_TO_ADDRESS1            VARCHAR2(240 BYTE),
  SHIP_TO_ADDRESS2            VARCHAR2(240 BYTE),
  SHIP_TO_ADDRESS3            VARCHAR2(240 BYTE),
  SHIP_TO_ADDRESS4            VARCHAR2(240 BYTE),
  SHIP_TO_CITY                VARCHAR2(60 BYTE),
  SHIP_TO_STATE               VARCHAR2(60 BYTE),
  SHIP_TO_COUNTRY             VARCHAR2(60 BYTE),
  SHIP_TO_POSTAL_CODE         VARCHAR2(60 BYTE),
  BILL_TO_ADDRESS1            VARCHAR2(240 BYTE),
  BILL_TO_ADDRESS2            VARCHAR2(240 BYTE),
  BILL_TO_ADDRESS3            VARCHAR2(240 BYTE),
  BILL_TO_ADDRESS4            VARCHAR2(240 BYTE),
  BILL_TO_CITY                VARCHAR2(60 BYTE),
  BILL_TO_STATE               VARCHAR2(60 BYTE),
  BILL_TO_COUNTRY             VARCHAR2(60 BYTE),
  BILL_TO_POSTAL_CODE         VARCHAR2(60 BYTE),
  DELIVER_TO_ADDRESS1         VARCHAR2(240 BYTE),
  DELIVER_TO_ADDRESS2         VARCHAR2(240 BYTE),
  DELIVER_TO_ADDRESS3         VARCHAR2(240 BYTE),
  DELIVER_TO_ADDRESS4         VARCHAR2(240 BYTE),
  DELIVER_TO_CITY             VARCHAR2(60 BYTE),
  DELIVER_TO_STATE            VARCHAR2(60 BYTE),
  DELIVER_TO_COUNTRY          VARCHAR2(60 BYTE),
  DELIVER_TO_POSTAL_CODE      VARCHAR2(60 BYTE),
  SHIPPING_INSTRUCTIONS       VARCHAR2(2000 BYTE),
  CONTACT_FIRST_NAME          VARCHAR2(240 BYTE),
  CONTACT_LAST_NAME           VARCHAR2(240 BYTE),
  EMAIL                       VARCHAR2(240 BYTE),
  CONTACT_NUM                 VARCHAR2(30 BYTE),
  FAX_NUM                     VARCHAR2(30 BYTE),
  PROCESS_FLAG                VARCHAR2(1 BYTE),
  ERROR_MESSAGE               VARCHAR2(2000 BYTE),
  CREATION_DATE               DATE,
  CREATED_BY                  NUMBER,
  LAST_UPDATE_DATE            DATE,
  LAST_UPDATED_BY             NUMBER,
  REC_TYPE                    VARCHAR2(2 BYTE),
  SHIP_TO_CONTACT_FIRST_NAME  VARCHAR2(240 BYTE),
  SHIP_TO_CONTACT_LAST_NAME   VARCHAR2(240 BYTE),
  PROJECT_ID                  VARCHAR2(30 BYTE),      -- Version# 1.0
  PROJECT_NAME                VARCHAR2(400 BYTE),     -- Version# 1.0
  COMPANY_ID                  VARCHAR2(100 BYTE),     -- Version# 1.0
  ACCOUNT_NUMBER              VARCHAR2(100 BYTE),     -- Version# 1.0
  NOTES                       CHAR(1000 BYTE),         -- Version# 1.0
  DELIVER_POA                 NUMBER(1)         DEFAULT 0 -- Version# 1.1
);
/