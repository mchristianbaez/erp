  /********************************************************************************
  FILE NAME: XXWC_ORDER_PAYMENTS_TBL.sql
  
  PROGRAM TYPE: Order payments table type online sales order form
  
  PURPOSE: Order payments table type online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     11/03/2017    Rakesh P.       TMS#20171102-00074-Mobile apps new plsql procedures for WOC
  *******************************************************************************************/
  
SET serveroutput ON;

WHENEVER SQLERROR CONTINUE

CREATE OR REPLACE TYPE XXWC.XXWC_ORDER_PAYMENTS_TBL AS TABLE OF XXWC.xxwc_order_payment_rec;
/

CREATE PUBLIC SYNONYM XXWC_ORDER_PAYMENTS_TBL FOR XXWC.XXWC_ORDER_PAYMENTS_TBL;
/
