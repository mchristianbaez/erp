/***********************************************************************************
File Name: XXWC_EGO_MOD_ITEMS_TAB
PROGRAM TYPE: SQL TABLE file
HISTORY
PURPOSE: Table used for storing Catalog data used in generating CatalogEntries 
         data file
====================================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ----------------------------------------------
1.0     09/23/2012    Rajiv Rathod    Initial creation of the file
************************************************************************************/

DROP TABLE XXWC.XXWC_EGO_MOD_ITEMS_TAB CASCADE CONSTRAINTS;

DROP SYNONYM XXWC_EGO_MOD_ITEMS_TAB;

CREATE TABLE XXWC.XXWC_EGO_MOD_ITEMS_TAB
(
   ITEM_NUMBER           VARCHAR2 (150),
   INVENTORY_ITEM_ID     NUMBER,
   WEB_HIERARCHY         VARCHAR2 (100 BYTE),
   LAST_UPDATE_DATE_WH   DATE,
   WEB_FLAG              VARCHAR2 (100 BYTE),
   LAST_UPDATE_DATE_WF   DATE,
   PRICE                 NUMBER,
   LAST_UPDATE_DATE_P    DATE,
   LAST_UPDATE_DATE_MSIB DATE,
   LAST_UPDATE_DATE_BASE DATE,
   LAST_UPDATE_DATE_LANG DATE,
   REQUEST_ID            NUMBER
);

CREATE SYNONYM APPS.XXWC_EGO_MOD_ITEMS_TAB FOR XXWC.XXWC_EGO_MOD_ITEMS_TAB;

CREATE INDEX XXWC.XXWC_EGO_MOD_ITEMS_TAB_N1
   ON XXWC.XXWC_EGO_MOD_ITEMS_TAB (INVENTORY_ITEM_ID)
/