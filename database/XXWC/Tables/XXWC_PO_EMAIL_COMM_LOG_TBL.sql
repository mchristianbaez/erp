/*************************************************************************
  $Header XXWC_PO_EMAIL_COMM_LOG_TBL.sql $
  Module Name: XXWC_PO_EMAIL_COMM_LOG_TBL

  PURPOSE: Table to maintain custom PO Workflow log messages.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        21-Jul-2017  P.Vamshidhar         TMS#20170323-00270 - Auto-fill E-Mail Address on PO Approval form
*******************************************************************************************************************************************/ 
CREATE TABLE XXWC.XXWC_PO_EMAIL_COMM_LOG_TBL
(
   WF_ITEM_KEY     VARCHAR2 (100),
   LOG_MESSAGE     VARCHAR2 (4000),
   CREATION_DATE   DATE,
   CREATED_BY      NUMBER
);
/