   /***************************************************************************************************************************************************
        $Header XXWCAR_CASH_RCPTS_TBL $
        Module Name: XXWCAR_CASH_RCPTS_TBL.sql

        PURPOSE:   AHH DM/Invoices Interface/Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018  P.Vamshidhar     TMS#20180708-00002 - AH HARRIS AR Debit Memo/Invoice Interface
   ******************************************************************************************************************************************************/
   
DROP TABLE XXWC.XXWCAR_CASH_RCPTS_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWCAR_CASH_RCPTS_TBL
(
  CONTROL_NBR            VARCHAR2(15 BYTE),
  DEPOSIT_DATE           DATE,
  OPERATOR_CODE          VARCHAR2(15 BYTE),
  CHECK_NBR              VARCHAR2(20 BYTE),
  CHECK_AMT              NUMBER,
  URL_LINK               VARCHAR2(200 BYTE),
  CUSTOMER_NBR           VARCHAR2(30 BYTE),
  INVOICE_NBR            VARCHAR2(30 BYTE),
  FILL                   VARCHAR2(15 BYTE),
  INVOICE_AMT            NUMBER,
  DISC_AMT               NUMBER,
  GL_ACCT_NBR            VARCHAR2(10 BYTE),
  SHORT_PAY_CODE         VARCHAR2(30 BYTE),
  CREATION_DATE          DATE,
  UPDATED_DATE           DATE,
  STATUS                 VARCHAR2(64 BYTE),
  BILL_TO_LOCATION       VARCHAR2(40 BYTE),
  COMMENTS               VARCHAR2(240 BYTE),
  SHORT_CODE_INTERFACED  VARCHAR2(1 BYTE),
  SHORT_CODE_INT_DATE    DATE,
  RECEIPT_TYPE           VARCHAR2(15 BYTE),
  ORACLE_ACCOUNT_NBR     VARCHAR2(164 BYTE),
  CUST_ACCOUNT_ID        NUMBER,
  ORACLE_FLAG            VARCHAR2(128 BYTE),
  RECEIPT_ID             NUMBER,
  ABA_NUMBER             VARCHAR2(30 BYTE),
  BANK_ACCOUNT_NUM       VARCHAR2(100 BYTE),
  ORG_ID                 NUMBER                 DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),162)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX XXWC.XXWCAR_CASH_RCPTS_TBL_N1 ON XXWC.XXWCAR_CASH_RCPTS_TBL
(CUSTOMER_NBR)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWCAR_CASH_RCPTS_TBL_N1
  MONITORING USAGE;


CREATE INDEX XXWC.XXWCAR_CASH_RCPTS_TBL_N2 ON XXWC.XXWCAR_CASH_RCPTS_TBL
(DEPOSIT_DATE)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWCAR_CASH_RCPTS_TBL_N2
  MONITORING USAGE;


CREATE INDEX XXWC.XXWCAR_CASH_RCPTS_TBL_N3 ON XXWC.XXWCAR_CASH_RCPTS_TBL
(CUSTOMER_NBR, CHECK_NBR)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWCAR_CASH_RCPTS_TBL_N3
  MONITORING USAGE;


CREATE INDEX XXWC.XXWCAR_CASH_RCPTS_TBL_N4 ON XXWC.XXWCAR_CASH_RCPTS_TBL
(RECEIPT_ID)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWCAR_CASH_RCPTS_TBL_N4
  MONITORING USAGE;


CREATE INDEX XXWC.XXWCAR_CASH_RCPTS_TBL_N5 ON XXWC.XXWCAR_CASH_RCPTS_TBL
(STATUS, CONTROL_NBR)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWCAR_CASH_RCPTS_TBL_N5
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER APPS.xxwcar_cash_rcpts_trg
  BEFORE INSERT ON xxwc.xxwcar_cash_rcpts_tbl
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
DECLARE
  l_recpt_id NUMBER := 0;

BEGIN
  IF :new.receipt_id IS NULL
  THEN
    SELECT apps.xxwcar_cash_rcpts_s.nextval INTO l_recpt_id FROM dual;
    :new.receipt_id := l_recpt_id;

  END IF;
END;
/


GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWCAR_CASH_RCPTS_TBL TO EA_APEX;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWCAR_CASH_RCPTS_TBL TO INTERFACE_APEXWC;

GRANT SELECT ON XXWC.XXWCAR_CASH_RCPTS_TBL TO INTERFACE_PRISM;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWCAR_CASH_RCPTS_TBL TO XXWC_DEV_ADMIN_ROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWCAR_CASH_RCPTS_TBL TO XXWC_EDIT_IFACE_ROLE;

GRANT INSERT ON XXWC.XXWCAR_CASH_RCPTS_TBL TO XXWC_PRISM_INSERT_ROLE;

GRANT SELECT ON XXWC.XXWCAR_CASH_RCPTS_TBL TO XXWC_PRISM_SELECT_ROLE;
/