  /********************************************************************************
  FILE NAME: XXWC_WAREHOUSE_NUM_TBL.sql
  
  PROGRAM TYPE: Create table record type to store warehouse number 
  
  PURPOSE: Create table record type to store warehouse number 
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/19/2017    Rakesh P.       TMS#20170313-00308 -Validate the user for resp access in order create api, enable debug message
  *******************************************************************************************/
SET serveroutput ON;

WHENEVER SQLERROR CONTINUE
    
CREATE OR REPLACE TYPE xxwc.xxwc_warhouse_num_tbl IS TABLE OF VARCHAR2(3);
/

CREATE OR REPLACE PUBLIC SYNONYM xxwc_warhouse_num_tbl FOR xxwc.xxwc_warhouse_num_tbl;
/

GRANT ALL ON xxwc.xxwc_warhouse_num_tbl TO INTERFACE_OSO;
/

