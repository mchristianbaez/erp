  /********************************************************************************
  FILE NAME: XXWC_GOOGLE_LOC_INVENTORY_TEMP.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: XXWC_GOOGLE_LOC_INVENTORY_TEMP 
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     02/09/2017    P.Vamshidhar    TMS#20161104-00076  - SEO - Local Inventory Ads (LIA)
                                        Initial Version
  *******************************************************************************************/

CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_GOOGLE_LOC_INVENTORY_TEMP
(
  INVENTORY_ITEM_ID  NUMBER,
  ORGANIZATION_ID    NUMBER,
  PRIMARY_UOM_CODE   VARCHAR2(30 BYTE),
  ORGANIZATION_CODE  VARCHAR2(30 BYTE),
  ITEM_NUMBER        VARCHAR2(30 BYTE),
  ON_HAND_QTY        NUMBER,
  AVAILABILITY       VARCHAR2(50 BYTE),
  ITEM_PRICE         NUMBER
)
ON COMMIT DELETE ROWS
RESULT_CACHE (MODE DEFAULT)
NOCACHE
/