--
-- XXWC_PARAM_LIST_VALUES  (Table) 
--
CREATE TABLE XXWC_PARAM_LIST_VALUES
(
  LIST_ID      NUMBER,
  VALUE_ID     NUMBER,
  LIST_NAME    VARCHAR2(124 BYTE),
  LIST_TYPE    VARCHAR2(124 BYTE),
  LIST_VALUE   VARCHAR2(124 BYTE),
  FULL_STRING  CLOB
)
LOB (FULL_STRING) STORE AS (
  TABLESPACE  XXWC_DATA
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                 ))
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
