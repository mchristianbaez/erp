--
-- XXWC_WSH_SHIPPING_LOG  (Table) 
--
CREATE TABLE XXWC.XXWC_WSH_SHIPPING_LOG
(
  HEADER_ID      NUMBER                             NULL,
  LINE_ID        NUMBER                             NULL,
  LOG_MESSAGE    VARCHAR2(4000 BYTE)                NULL,
  CREATION_DATE  DATE                               NULL,
  LOG_SEQUENCE   NUMBER                             NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


--
-- XXWC_WSH_SHIPPING_LOG  (Synonym) 
--
CREATE SYNONYM APPS.XXWC_WSH_SHIPPING_LOG FOR XXWC.XXWC_WSH_SHIPPING_LOG;


