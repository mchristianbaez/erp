 /*******************************************************************************
  Table: XXWC_MD_SEARCH_PROD_GTT_TBL_V
  Description: This table is used to load items searched on AIS form
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     01-Nov-2016        Pahwa Nancy   TMS# 20161102-00153   Performance Tuning
  ********************************************************************************/
CREATE GLOBAL TEMPORARY TABLE "XXWC"."XXWC_MD_SEARCH_PROD_GTT_TBL_V" 
   (	"SCORE" NUMBER, 
	"INVENTORY_ITEM_ID" NUMBER, 
	"ORGANIZATION_ID" NUMBER, 
	"PARTNUMBER" VARCHAR2(40), 
	"TYPE" CHAR(7), 
	"MANUFACTURERPARTNUMBER" VARCHAR2(150), 
	"MANUFACTURER" VARCHAR2(150), 
	"SEQUENCE" NUMBER, 
	"SHORTDESCRIPTION" VARCHAR2(240), 
	"QUANTITYONHAND" NUMBER, 
	"OPEN_SALES_ORDERS" NUMBER, 
	"LIST_PRICE" NUMBER, 
	"BUYABLE" CHAR(1), 
	"KEYWORD" VARCHAR2(1000), 
	"ITEM_TYPE" VARCHAR2(30), 
	"CROSS_REFERENCE" VARCHAR2(4000), 
	"PRIMARY_UOM_CODE" VARCHAR2(3), 
	"SELLING_PRICE" NUMBER, 
	"MODIFIER" VARCHAR2(240), 
	"MODIFIER_TYPE" VARCHAR2(200)
   ) ON COMMIT PRESERVE ROWS ;
/