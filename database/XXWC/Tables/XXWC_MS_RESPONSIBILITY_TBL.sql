create table XXWC.XXWC_MS_RESPONSIBILITY_TBL
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_RESPONSIBILITY_TBL $
  Module Name: XXWC_MS_RESPONSIBILITY_TBL

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
(
  responsibility_id   NUMBER not null,
  responsibility_name VARCHAR2(250),
  app_id              NUMBER,
  created_by          VARCHAR2(50),
  created_on          DATE,
  updated_by          VARCHAR2(50),
  updated_on          DATE
)
tablespace XXWC_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );