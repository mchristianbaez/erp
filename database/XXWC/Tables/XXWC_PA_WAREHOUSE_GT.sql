-- Satish U: 06-JUL-2012 
-- Script to create Global Temporary Table for capturing 
-- Warehouse information from Pricing and Availability Form 

Connect xxwc/&xxwc_pswd;

DROP TABLE XXWC.XXWC_PA_WAREHOUSE_GT CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_PA_WAREHOUSE_GT
(
  ORGANIZATION_ID  NUMBER
)
ON COMMIT DELETE ROWS
NOCACHE;

grant all on XXWC.XXWC_PA_WAREHOUSE_GT to apps; 

connect apps/&apps_pswd; 

drop synonym apps.XXWC_PA_WAREHOUSE_GT; 

create synonym apps.XXWC_PA_WAREHOUSE_GT for xxwc.XXWC_PA_WAREHOUSE_GT;

exit; 
