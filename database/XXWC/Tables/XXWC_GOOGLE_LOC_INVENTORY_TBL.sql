  /********************************************************************************
  FILE NAME: XXWC_GOOGLE_LOC_INVENTORY_TBL.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Global table to store google local ads data 
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     02/09/2017    P.Vamshidhar    TMS#20161104-00076  - SEO - Local Inventory Ads (LIA)
                                        Initial Version
  *******************************************************************************************/

CREATE TABLE XXWC.XXWC_GOOGLE_LOC_INVENTORY_TBL
(
  INVENTORY_ITEM_ID  NUMBER,
  ORGANIZATION_ID    NUMBER,
  PRIMARY_UOM_CODE   VARCHAR2(30 BYTE),
  ORGANIZATION_CODE  VARCHAR2(30 BYTE),
  ITEM_NUMBER        VARCHAR2(30 BYTE),
  ON_HAND_QTY        NUMBER,
  AVAILABILITY       VARCHAR2(50 BYTE),
  ITEM_PRICE         NUMBER
)
/
CREATE INDEX XXWC.XXWC_GOOGLE_LOC_INVENT_TBL_N1 ON XXWC.XXWC_GOOGLE_LOC_INVENTORY_TBL
(ORGANIZATION_ID)
/
CREATE UNIQUE INDEX XXWC.XXWC_GOOGLE_LOC_INVENT_TBL_U1 ON XXWC.XXWC_GOOGLE_LOC_INVENTORY_TBL
(ORGANIZATION_ID, INVENTORY_ITEM_ID)
/