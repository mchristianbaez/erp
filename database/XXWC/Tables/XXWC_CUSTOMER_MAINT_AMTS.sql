/* Formatted on 26-Nov-2012 23:28:52 (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_CUSTOMER_MAINT_AMTS
-- Generated 26-Nov-2012 23:28:40 from XXWC@EBIZCON

CREATE TABLE xxwc.xxwc_customer_maint_amts
(
    prof_rowid                      ROWID
   ,cust_rowid                      ROWID
   ,site_uses_rowid                 ROWID
   ,cust_account_id                 NUMBER (15, 0) NOT NULL
   ,cust_account_profile_id         NUMBER (15, 0) NOT NULL
   ,cust_acct_site_id               NUMBER (15, 0)
   ,site_use_id                     NUMBER (15, 0)
   ,party_site_party_id             NUMBER (15, 0)
   ,lv                              VARCHAR2 (7 BYTE)
   ,account_number                  VARCHAR2 (30 BYTE) NOT NULL
   ,profile_party_id                NUMBER (15, 0)
   ,account_name                    VARCHAR2 (240 BYTE)
   ,account_address                 VARCHAR2 (4000 BYTE)
   ,site_address                    VARCHAR2 (4000 BYTE)
   ,party_site_number               VARCHAR2 (30 BYTE)
   ,account_status                  VARCHAR2 (30 BYTE)
   ,profile_account_status          VARCHAR2 (30 BYTE)
   ,account_freight_term            VARCHAR2 (30 BYTE)
   ,collector_id                    NUMBER (15, 0) NOT NULL
   ,collector_name                  VARCHAR2 (30 BYTE) NOT NULL
   ,credit_analyst_id               NUMBER (15, 0)
   ,credit_analyst_name             VARCHAR2 (4000 BYTE)
   ,credit_checking                 VARCHAR2 (1 BYTE) NOT NULL
   ,credit_hold                     VARCHAR2 (1 BYTE) NOT NULL
   ,remmit_to_code_attribute2       VARCHAR2 (150 BYTE)
   ,account_payment_terms           NUMBER (15, 0)
   ,standard_terms_name             VARCHAR2 (15 BYTE)
   ,standard_terms_description      VARCHAR2 (240 BYTE)
   ,credit_classification           VARCHAR2 (30 BYTE)
   ,profile_class_id                NUMBER (15, 0) NOT NULL
   ,profile_class_name              VARCHAR2 (30 BYTE) NOT NULL
   ,credit_classification_meaning   VARCHAR2 (4000 BYTE)
   ,account_status_meaning          VARCHAR2 (4000 BYTE)
   ,purposes                        VARCHAR2 (4000 BYTE)
   ,site_use_code                   VARCHAR2 (30 BYTE)
   ,primary_flag                    VARCHAR2 (1 BYTE)
   ,acct_site_bill_to_flag          VARCHAR2 (1 BYTE)
   ,acct_site_ship_to_flag          VARCHAR2 (1 BYTE)
   ,site_status                     VARCHAR2 (1 BYTE)
   ,location                        VARCHAR2 (40 BYTE)
   ,org_id                          NUMBER (15, 0)
   ,primary_salesrep_id             NUMBER (15, 0)
   ,salesreps_name                  VARCHAR2 (4000 BYTE)
   ,site_freight_terms              VARCHAR2 (30 BYTE)
   ,acct_site_party_site_id         NUMBER (15, 0)
   ,acct_site_status                VARCHAR2 (1 BYTE)
   ,site_uses_creation_date         DATE
   ,level_flag                      CHAR (1 BYTE)
   ,amount_rowid                    ROWID
   ,cust_acct_profile_amt_id        NUMBER (15, 0) NOT NULL
   ,currency_code                   VARCHAR2 (15 BYTE) NOT NULL
   ,overall_credit_limit            NUMBER
   ,trx_credit_limit                NUMBER
   ,min_statement_amount            NUMBER
   ,auto_rec_min_receipt_amount     NUMBER
   ,interest_rate                   NUMBER
   ,object_version_number           NUMBER
)

NOCACHE
MONITORING
NOPARALLEL
NOLOGGING
/

-- End of DDL Script for Table XXWC.XXWC_CUSTOMER_MAINT_AMTS
