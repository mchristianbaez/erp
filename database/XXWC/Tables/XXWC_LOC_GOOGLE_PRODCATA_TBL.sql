  /********************************************************************************
  FILE NAME: XXWC_LOC_GOOGLE_PRODCATA_TBL.sql
  
  PROGRAM TYPE: Table Creation Script
  
  PURPOSE: Table to store Oracle and Google Category mapping.
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     02/09/2017    P.Vamshidhar    TMS#20161104-00076  - SEO - Local Inventory Ads (LIA)
                                        Initial Version
  *******************************************************************************************/

CREATE TABLE XXWC.XXWC_LOC_GOOGLE_PRODCATA_TBL
(
  ORA_CATEGORY_ID       NUMBER,
  ORA_PROD_CATEGORY     VARCHAR2(1000 BYTE),
  GOOGLE_PROD_CATEGORY  VARCHAR2(1000 BYTE),
  CREATION_DATE         DATE,
  CREATED_BY            VARCHAR2(40 BYTE),
  LAST_UPDATED_DATE     DATE,
  LAST_UPDATED_BY       VARCHAR2(40 BYTE)
)
/