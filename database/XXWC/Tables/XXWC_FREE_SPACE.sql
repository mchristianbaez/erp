/* Formatted on 2/24/2014 10:57:35 AM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_FREE_SPACE##
-- Generated 2/24/2014 10:28:20 AM from XXWC@EBIZFQA
drop table xxwc.XXWC_FREE_SPACE;

CREATE TABLE xxwc.xxwc_free_space
(
    tablespace       VARCHAR2 (30 BYTE)
   ,total_space      NUMBER
   ,free_space       NUMBER
   ,total_space_mb   NUMBER
   ,used_space_mb    NUMBER
   ,free_space_mb    NUMBER
   ,pct_free         NUMBER
   ,insert_date      DATE
)
SEGMENT CREATION DEFERRED
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
NOCACHE
MONITORING
NOPARALLEL
LOGGING
/

INSERT INTO xxwc.xxwc_free_space
    SELECT df.tablespace_name tablespace
          ,df.total_space total_space
          ,fs.free_space free_space
          ,df.total_space_mb total_space_mb
          , (df.total_space_mb - fs.free_space_mb) used_space_mb
          ,fs.free_space_mb free_space_mb
          ,ROUND (100 * (fs.free_space / df.total_space), 2) pct_free
          ,TRUNC (SYSDATE) insert_date
      FROM (  SELECT tablespace_name, SUM (bytes) total_space, ROUND (SUM (bytes) / 1048576) total_space_mb
                FROM dba_data_files
            GROUP BY tablespace_name) df
          ,(  SELECT tablespace_name, SUM (bytes) free_space, ROUND (SUM (bytes) / 1048576) free_space_mb
                FROM dba_free_space
            GROUP BY tablespace_name) fs
     WHERE df.tablespace_name = fs.tablespace_name(+) AND df.tablespace_name LIKE 'XX%';

COMMIT;
-- End of DDL Script for Table XXWC.XXWC_FREE_SPACE##
