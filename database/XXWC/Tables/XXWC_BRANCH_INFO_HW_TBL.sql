  /********************************************************************************
  FILE NAME: XXWC_BRANCH_INFO_HW_TBL.sql
  
  PROGRAM TYPE: Branch Info table for online sales order form
  
  PURPOSE: Branch Info table table for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  *******************************************************************************************/
CREATE TABLE XXWC.XXWC_BRANCH_INFO_HW_TBL
(
  ORGANIZATION_ID      NUMBER                   NOT NULL,
  ORGANIZATION_CODE    VARCHAR2(3 BYTE),
  LOCATION_CODE        VARCHAR2(60 BYTE),
  BRANCH_STREET        VARCHAR2(240 BYTE),
  BRANCH_CITY          VARCHAR2(3000 BYTE),
  BRANCH_TEL1          VARCHAR2(60 BYTE),
  BRANCH_TEL2          VARCHAR2(60 BYTE),
  CREATION_DATE        DATE                     NOT NULL,
  CREATED_BY           NUMBER(15)               NOT NULL,
  LAST_UPDATE_DATE     DATE                     NOT NULL,
  LAST_UPDATED_BY      NUMBER(15)               NOT NULL,
  REQUEST_ID           NUMBER(15),
  ATTRIBUTE1           VARCHAR2(150 BYTE),
  ATTRIBUTE2           VARCHAR2(150 BYTE),
  ATTRIBUTE3           VARCHAR2(150 BYTE),
  ATTRIBUTE4           VARCHAR2(150 BYTE),
  ATTRIBUTE5           VARCHAR2(150 BYTE),
  ATTRIBUTE6           VARCHAR2(150 BYTE),
  ATTRIBUTE7           VARCHAR2(150 BYTE),
  ATTRIBUTE8           VARCHAR2(150 BYTE),
  ATTRIBUTE9           VARCHAR2(150 BYTE),
  ATTRIBUTE10          VARCHAR2(150 BYTE),
  ATTRIBUTE11          VARCHAR2(150 BYTE),
  ATTRIBUTE12          VARCHAR2(150 BYTE),
  ATTRIBUTE13          VARCHAR2(150 BYTE),
  ATTRIBUTE14          VARCHAR2(150 BYTE),
  ATTRIBUTE15          VARCHAR2(150 BYTE),
  ATTRIBUTE16          VARCHAR2(150 BYTE),
  ATTRIBUTE17          VARCHAR2(150 BYTE),
  ATTRIBUTE18          VARCHAR2(150 BYTE),
  ATTRIBUTE19          VARCHAR2(150 BYTE),
  ATTRIBUTE20          VARCHAR2(150 BYTE)
);

GRANT SELECT ON XXWC.XXWC_BRANCH_INFO_HW_TBL TO INTERFACE_MSSQL;