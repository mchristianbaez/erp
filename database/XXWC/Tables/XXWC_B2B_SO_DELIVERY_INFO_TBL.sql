  /*******************************************************************************
  Table:   XXWC_B2B_SO_DELIVERY_INFO_TBL
  Description: This table is used to maintain delivery information of B2B Sales Orders
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-May-2014        Gopi Damuluri   Initial version
  ********************************************************************************/
DROP TABLE XXWC.XXWC_B2B_SO_DELIVERY_INFO_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_B2B_SO_DELIVERY_INFO_TBL
(
  HEADER_ID                NUMBER,
  DELIVERY_ID              NUMBER,
  REQUEST_ID               NUMBER,
  CONCURRENT_PROGRAM_NAME  VARCHAR2(30 BYTE),
  PARTY_ID                 NUMBER,
  CUSTOMER_ID              NUMBER,
  REQUEST_DATE             DATE,
  CC_EMAIL                 VARCHAR2(200 BYTE),
  TRACKING_NUM             VARCHAR2(50 BYTE),
  EXPECTED_RECEIVED_DATE   DATE,
  SOA_DELIVERED            VARCHAR2(1 BYTE),
  ASN_DELIVERED            VARCHAR2(1 BYTE),
  INVOICE_DELIVERED        VARCHAR2(1 BYTE),
  CREATION_DATE            DATE,
  CREATED_BY               NUMBER,
  LAST_UPDATE_DATE         DATE,
  LAST_UPDATED_BY          NUMBER,
  PROCESS_FLAG             VARCHAR2(1 BYTE),
  ERROR_MESSAGE            VARCHAR2(2000 BYTE),
  BATCH_ID                 NUMBER
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;