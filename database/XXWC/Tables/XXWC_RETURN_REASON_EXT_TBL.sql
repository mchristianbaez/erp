   /*********************************************************************************************************************
   -- Table Name: XXWC_RETURN_REASON_EXT_TBL
   -- *******************************************************************************************************************
   --  To store Return reason codes.
   -- HISTORY
   -- ===================================================================================================================
   -- ===================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ------------------------------------------------------
   -- 1.0     16-Jul-2018   P.Vamshidhar    TMS#20180716-00025 - AHH Customers Interface    
   ***********************************************************************************************************************/

   CREATE TABLE XXWC.XXWC_RETURN_REASON_EXT_TBL
(
  DESCRIPTION           VARCHAR2(30 BYTE),
  ORACLE_RETURN_REASON  VARCHAR2(30 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_AR_AHH_CASH_RCPT_CONV_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_RETURN_REASON_CONV.bad'
    DISCARDFILE 'XXWC_RETURN_REASON_CONV.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                  )
     LOCATION (XXWC_AR_AHH_CASH_RCPT_CONV_DIR:'XXWC_RETURN_REASON_CONV.csv')
  )
REJECT LIMIT UNLIMITED
NOPARALLEL
NOMONITORING;


GRANT ALL ON XXWC.XXWC_RETURN_REASON_EXT_TBL TO EA_APEX;
/