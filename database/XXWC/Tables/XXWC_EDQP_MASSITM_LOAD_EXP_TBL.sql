/*******************************************************************************************************************
File Name:XXWC_EDQP_MASSITM_LOAD_EXP_TBL.sql

TYPE:     Table

Description: Table used to maintain edqp items created by information.

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------------------------------------------
1.0     27-MAR-2015   P.vamshidhar    Initial version TMS#20150223-00026 - Improve exception error handling in EDQP
********************************************************************************************************************/


CREATE TABLE XXWC.XXWC_EDQP_MASSITM_LOAD_EXP_TBL
(
  JOB_ID             VARCHAR2(20 BYTE),
  VALIDATION_COLUMN  VARCHAR2(30 BYTE),
  COLUMN_VALUE       VARCHAR2(1000 BYTE),
  STATUS             VARCHAR2(40 BYTE),
  ERROR_MESSAGE      VARCHAR2(1000 BYTE),
  ID                 VARCHAR2(40 BYTE)
);


GRANT ALL ON XXWC.XXWC_EDQP_MASSITM_LOAD_EXP_TBL TO DLS, APPS;


CREATE SYNONYM dls.XXWC_EDQP_MASSITM_LOAD_EXP_TBL FOR xxwc.XXWC_EDQP_MASSITM_LOAD_EXP_TBL


