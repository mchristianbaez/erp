CREATE TABLE XXWC.XXWC_PGR_MESSAGE
(
  HEADER_ID       NUMBER,
  LINE_ID         NUMBER,
  RETURN_STATUS   VARCHAR2(1 BYTE),
  RETURN_MESSAGE  VARCHAR2(2000 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


