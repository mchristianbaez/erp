  /********************************************************************************
  FILE NAME: XXWC_GL_AHH_JOURNALS_STG_TBL.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: GL Journal Staging table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE

-- DROP TABLE
DROP TABLE XXWC.XXWC_GL_AHH_JOURNALS_STG_TBL;

-- Create table
create table XXWC.XXWC_GL_AHH_JOURNALS_STG_TBL
(
  status              VARCHAR2(100),
  currency_code       VARCHAR2(10),
  actual_flag         VARCHAR2(10),
  product             VARCHAR2(10),
  location            VARCHAR2(10),
  cost_center         VARCHAR2(10),
  account             VARCHAR2(10),
  project_code        VARCHAR2(10),
  future_use          VARCHAR2(10),
  future_use2         VARCHAR2(10),
  debit_amount        NUMBER,
  credit_amount       NUMBER,
  batch_number        VARCHAR2(100),
  batch_description   VARCHAR2(2000),
  journal_name        VARCHAR2(200),
  journal_description VARCHAR2(2000),
  journal_line_desc   VARCHAR2(2000),
  reference_field1    VARCHAR2(2000),
  reference_field2    VARCHAR2(2000),
  reference_field3    VARCHAR2(2000),
  reference_field4    VARCHAR2(2000),
  reference_field5    VARCHAR2(2000),
  reference_field6    VARCHAR2(2000),
  reference_field7    VARCHAR2(2000),
  reference_field8    VARCHAR2(2000),
  reference_field9    VARCHAR2(2000),
  reference_field10   VARCHAR2(2000),
  error_message       VARCHAR2(4000),
  accounting_date     DATE
);
