/* Formatted on 5/21/2014 5:43:16 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_PO_VENDOR_MIN_INT_STG
-- Generated 5/21/2014 5:43:12 PM from XXWC@EBIZFQA

CREATE GLOBAL TEMPORARY TABLE xxwc.xxwc_po_vendor_min_int_stg
(
    source_organization_id   NUMBER NOT NULL
   ,organization_id          NUMBER
   ,inventory_item_id        NUMBER NOT NULL
   ,mp_dst_org_code          VARCHAR2 (3 BYTE)
   ,item_number              VARCHAR2 (40 BYTE)
   ,description              VARCHAR2 (240 BYTE)
   ,amu                      VARCHAR2 (240 BYTE)
   ,unit_weight              NUMBER
   ,weight_uom_code          VARCHAR2 (3 BYTE)
   ,fixed_lot_multiplier     NUMBER
   ,min_minmax_quantity      NUMBER
   ,max_minmax_quantity      NUMBER
   ,list_price_per_unit      NUMBER
   ,on_hand_qty              NUMBER
   ,sales_velocity           VARCHAR2 (90 BYTE)
   ,open_demand              NUMBER
   ,open_po                  NUMBER
   ,open_req                 NUMBER
   ,incomplete_reqs          NUMBER
   ,potential_demand         NUMBER
   ,perc_spread              NUMBER
   ,status                   VARCHAR2 (240 BYTE)
   ,ord_quantity             NUMBER
   ,avail2                   NUMBER
   ,sort_sequence            NUMBER
   ,sort_sequence2           NUMBER
   ,status_short             VARCHAR2 (24 BYTE)
)
ON COMMIT PRESERVE ROWS
 
/

-- End of DDL Script for Table XXWC.XXWC_PO_VENDOR_MIN_INT_STG
