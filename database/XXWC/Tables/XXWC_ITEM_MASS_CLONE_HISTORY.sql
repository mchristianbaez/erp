--
-- XXWC_ITEM_MASS_CLONE_HISTORY  (Table) 
--
CREATE TABLE XXWC_ITEM_MASS_CLONE_HISTORY
(
  RUN_NUMBER                 VARCHAR2(164 BYTE),
  CLONE_ID                   NUMBER,
  INSERT_DATE                DATE,
  USER_ID                    NUMBER,
  ITEM_NUMBER                VARCHAR2(164 BYTE),
  ORG_NUMBER                 VARCHAR2(32 BYTE),
  ITEM_ID                    NUMBER,
  ORG_ID                     NUMBER,
  API_RETURN_CODE            VARCHAR2(32 BYTE),
  ERROR_MESSAGE              CLOB,
  VERIFY_DATABASE            VARCHAR2(32 BYTE),
  PRIMARY_UOM_CODE           VARCHAR2(24 BYTE),
  SOURCING_RULE              VARCHAR2(128 BYTE),
  ITEM_COST_PRICE            NUMBER,
  SOURCING_RULE_IN_DATABASE  VARCHAR2(128 BYTE),
  CLONE_PURPOSE              VARCHAR2(512 BYTE),
  SOURCING_RULE_ID           NUMBER
)
LOB (ERROR_MESSAGE) STORE AS (
  TABLESPACE  XXWC_DATA
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                 ))
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
