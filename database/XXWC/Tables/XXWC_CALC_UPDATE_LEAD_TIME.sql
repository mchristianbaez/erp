CREATE TABLE XXWC.XXWC_CALC_UPDATE_LEAD_TIME
(
  CP_LAST_RUN_DATE   DATE                       NOT NULL,
  LAST_UPDATE_DATE   DATE                       NOT NULL,
  LAST_UPDATED_BY    NUMBER                     NOT NULL,
  CREATION_DATE      DATE                       NOT NULL,
  CREATED_BY         NUMBER                     NOT NULL,
  LAST_UPDATE_LOGIN  NUMBER,
  REQUEST_ID         NUMBER
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


