CREATE TABLE XXWC.XXWC_PO_VENDOR_MINIMUM
(
  VENDOR_ID           NUMBER                        NULL,
  VENDOR_SITE_ID      NUMBER                        NULL,
  ORGANIZATION_ID     NUMBER                        NULL,
  VENDOR_MIN_DOLLAR   NUMBER                        NULL,
  FREIGHT_MIN_DOLLAR  NUMBER                        NULL,
  FREIGHT_MIN_UOM     NUMBER                        NULL,
  CREATION_DATE       DATE                          NULL,
  CREATED_BY          NUMBER                        NULL,
  LAST_UPDATE_DATE    DATE                          NULL,
  LAST_UPDATED_BY     NUMBER                        NULL,
  LAST_UPDATE_LOGIN   NUMBER                        NULL,
  PPD_FREIGHT_UNITS   NUMBER                        NULL,
  NOTES               VARCHAR2(4000 BYTE)           NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN XXWC.XXWC_PO_VENDOR_MINIMUM.NOTES IS '12/09/CG: TMS 20130904-00654: Added New Column';

COMMENT ON COLUMN XXWC.XXWC_PO_VENDOR_MINIMUM.PPD_FREIGHT_UNITS IS '12/09/CG: TMS 20130904-00654: Added New Column';


CREATE SYNONYM XXWC_DEV_ADMIN.XXWC_PO_VENDOR_MINIMUM FOR XXWC.XXWC_PO_VENDOR_MINIMUM;


CREATE SYNONYM APPS.XXWC_PO_VENDOR_MINIMUM FOR XXWC.XXWC_PO_VENDOR_MINIMUM;


