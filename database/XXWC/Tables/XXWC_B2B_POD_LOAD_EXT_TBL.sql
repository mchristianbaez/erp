  /*******************************************************************************
  Table: xxwc.XXWC_B2B_POD_LOAD_EXT_TBL
  Description: XXWC.XXWC_B2B_POD_LOAD_EXT_TBL
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     18-Apr-2017        Pahwa Nancy   TMS# 20170206-00257  XXWC_B2B_POD_LOAD_EXT_TBL 
  ********************************************************************************/
-- Create table
--drop table "XXWC"."XXWC_B2B_POD_LOAD_EXT_TBL"; 
CREATE TABLE "XXWC"."XXWC_B2B_POD_LOAD_EXT_TBL" 
   (  "ACCOUNT_NUMBER" VARCHAR2(30), 
    "JOB_SITE" VARCHAR2(100),
      "DELIVER_POD" VARCHAR2(1),
  "POD_FREQUENCY" VARCHAR2(50), 
    "POD_EMAIL" VARCHAR2(240)
    ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_PDH_ITEM_LOAD_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
        LOAD WHEN("ACCOUNT_NUMBER" != BLANKS)
        SKIP 1
        BADFILE 'itemPodLoadBad.bad'
        DISCARDFILE 'itemPodLoadDiscard.dsc'
        FIELDS TERMINATED BY ','
        OPTIONALLY ENCLOSED BY '"' AND '"'
        REJECT ROWS WITH ALL NULL FIELDS
                                    )
      LOCATION
       ( 'XXWC_POD_Load.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;
/