DROP TABLE xxwc.xxwc_cb_cust_gtt_tbl;

CREATE GLOBAL TEMPORARY TABLE xxwc.xxwc_cb_cust_gtt_tbl (customer_id     NUMBER
                                      , customer_number      VARCHAR2 (30)  
                                      , party_id            NUMBER
                                      , customer_name          VARCHAR2 (360)
                                      , cust_acct_site_id   NUMBER
                                      , party_site_id       NUMBER   
                                      , site_use_id         NUMBER
                                      , terms               VARCHAR2 (15)
                                      , org_id              NUMBER
                                      , credit_analyst      VARCHAR2 (360 Byte))
ON COMMIT PRESERVE ROWS
NOCACHE;