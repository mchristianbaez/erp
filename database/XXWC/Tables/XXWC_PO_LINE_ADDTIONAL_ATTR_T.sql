/*************************************************************************************************
$Header XXWC_PO_LINE_ADDTIONAL_ATTR_T.sql $

Module Name: XXWC_PO_LINE_ADDTIONAL_ATTR_T

PURPOSE: Table to maintain PO Line level additional attributes

REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------------------------------------
1.0        28-Aug-2015  P.Vamshidhar          Initial Version TMS#20150515-00063

**************************************************************************************************/
CREATE TABLE XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T
(
  PO_HEADER_ID        NUMBER,
  PO_LINE_ID          NUMBER,
  LINE_LOCATION_ID    NUMBER,
  ORGANIZATION_ID     NUMBER,
  ITEM_ID             NUMBER,
  STOCK_STATUS        VARCHAR2(10 BYTE),
  SYSTEM_DELIV_PRICE  NUMBER,
  BPA_NUMBER          VARCHAR2(20 BYTE),
  BPA_PRICE           NUMBER,
  LIST_PRICE          NUMBER,
  CREATED_BY          NUMBER,
  CREATION_DATE       DATE,
  LAST_UPDATED_BY     NUMBER,
  LAST_UPDATED_DATE   DATE,
  LAST_UPDATED_LOGIN  NUMBER,
  ORG_ID              NUMBER
);

GRANT ALL ON XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T TO APPS;

CREATE INDEX XXWC.XXWC_PO_LINE_ADDT_ATTR_IDX ON XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T (PO_LINE_ID);