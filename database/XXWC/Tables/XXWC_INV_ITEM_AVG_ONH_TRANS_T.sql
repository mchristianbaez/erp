/*******************************************************************************
  * Table:   XXWC_INV_ITEM_AVG_ONH_TRANS_T
  * Description: This table is using from TRANSFER_BUTTON and REPORT_BUTTON in
                 XXWC_INV_ITEM_AVG_ONH_TRANS.fmb, XXWC_INV_ITEM_AVG_ONH_TRANS.rdf
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     10-Mar-2018        Pattabhi Avula  Initial creation of the procedure
                                             TMS#20171024-00015
  ********************************************************************************/
 CREATE TABLE XXWC.XXWC_INV_ITEM_AVG_ONH_TRANS_T 
   (PROCESS_ID           NUMBER, 
	BRANCH 				 VARCHAR2(5), 
	FROM_ITEM            VARCHAR2(30), 
	FROM_ITEM_DESC       VARCHAR2(240), 
	FROM_PRIMARY_UOM     VARCHAR2(10), 
	FROM_AVG_COST        NUMBER(10,5), 
	FROM_ONHAND_QTY      NUMBER, 
	TO_ITEM              VARCHAR2(30), 
	TO_ITEM_DESC         VARCHAR2(240), 
	TO_PRIMARY_UOM 		 VARCHAR2(10), 
	TO_AVG_COST 		 NUMBER(10,5), 
	TO_ONHAND_QTY        NUMBER, 
	SOURCE 				 VARCHAR2(100), 
	LAST_UPDATE_DATE 	 DATE, 
	LAST_UPDATED_BY 	 NUMBER, 
	CREATION_DATE        DATE, 
	CREATED_BY 			 NUMBER, 
	FROM_FLAG 			 VARCHAR2(1)
   ) 
/
