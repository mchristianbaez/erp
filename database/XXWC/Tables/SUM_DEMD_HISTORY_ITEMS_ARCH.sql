/* Formatted on 5/21/2014 2:04:12 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.SUM_DEMD_HISTORY_ITEMS_ARCH
-- Generated 5/21/2014 2:04:06 PM from XXWC@EBIZFQA

CREATE TABLE xxwc.sum_demd_history_items_arch
(
    transaction_id               NUMBER
   ,backet_to_update             VARCHAR2 (124 BYTE)
   ,include_exclude_flag         VARCHAR2 (32 BYTE)
   ,transaction_date             NUMBER
   ,inventory_item_id            NUMBER
   ,transaction_source_type_id   NUMBER
   ,transaction_action_id        NUMBER
   ,transaction_type_id          NUMBER
   ,primary_quantity             NUMBER
   ,ol_source_type_code          VARCHAR2 (124 BYTE)
   ,source_line_id               NUMBER
   ,organization_id              NUMBER
   ,insert_date                  DATE
   ,vsql                         VARCHAR2 (512 BYTE)
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
PARALLEL (DEGREE 32)
NOLOGGING
/

-- End of DDL Script for Table XXWC.SUM_DEMD_HISTORY_ITEMS_ARCH
