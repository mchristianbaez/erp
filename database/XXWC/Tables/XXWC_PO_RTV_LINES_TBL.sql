/*************************************************************************
  $Header XXWC_PO_RTV_LINES_TBL $
  Module Name: XXWC_PO_RTV_LINES_TBL

  PURPOSE: Store Return to Vendor Transactions Line Details

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        04/10/2014  Manjula Chellappan    Initial Version
**************************************************************************/
CREATE TABLE XXWC.XXWC_PO_RTV_LINES_TBL
(
   RTV_LINE_ID         NUMBER UNIQUE NOT NULL,
   RTV_HEADER_ID       NUMBER NOT NULL,
   LINE_NUMBER         NUMBER NOT NULL,
   INVENTORY_ITEM_ID   NUMBER NOT NULL,
   SUBINVENTORY        VARCHAR2 (120) NOT NULL,
   RETURN_QUANTITY     NUMBER NOT NULL,
   UOM_CODE            VARCHAR2 (20) NOT NULL,
   RETURN_UNIT_PRICE   NUMBER NOT NULL,
   RETURN_REASON       VARCHAR2 (100) NOT NULL,
   NOTES               VARCHAR2 (1000),
   PROCESSED_FLAG      VARCHAR2 (40) NOT NULL,
   CREATED_BY          NUMBER NOT NULL,
   CREATION_DATE       DATE NOT NULL,
   LAST_UPDATED_BY     NUMBER NOT NULL,
   LAST_UPDATE_DATE    DATE NOT NULL,
   LAST_UPDATE_LOGIN   NUMBER NOT NULL,
   ATTRIBUTE1          VARCHAR2 (240),
   ATTRIBUTE2          VARCHAR2 (240),
   ATTRIBUTE3          VARCHAR2 (240),
   ATTRIBUTE4          VARCHAR2 (240),
   ATTRIBUTE5          VARCHAR2 (240),
   ATTRIBUTE6          VARCHAR2 (240),
   ATTRIBUTE7          VARCHAR2 (240),
   ATTRIBUTE8          VARCHAR2 (240),
   ATTRIBUTE9          VARCHAR2 (240),
   ATTRIBUTE10         VARCHAR2 (240),
   ATTRIBUTE11         VARCHAR2 (240),
   ATTRIBUTE12         VARCHAR2 (240),
   ATTRIBUTE13         VARCHAR2 (240),
   ATTRIBUTE14         VARCHAR2 (240),
   ATTRIBUTE15         VARCHAR2 (240)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


--  There is no statement for index XXWC.SYS_C00940933.
--  The object is created when the parent object is created.

ALTER TABLE XXWC.XXWC_PO_RTV_LINES_TBL ADD (
  UNIQUE (RTV_LINE_ID)
  USING INDEX
    TABLESPACE XXWC_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
  ENABLE VALIDATE);