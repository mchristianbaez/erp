--Run this file in XXWC user
/*************************************************************************
    $Header ALTER_XXWC_B2B_CUST_INFO_TBL.sql $
     Module Name: ALTER_XXWC_B2B_CUST_INFO_TBL
   
     PURPOSE:   This script to rename b2b tables.

     REVISIONS:
     Ver        Date        Author                             Description
     ---------  ----------  -------------------------------    -------------------------
     1.0        12/08/2016  Rakesh Patel                       TMS#20161207-00249- B2B. Task to rename XXWC_B2B_CUST_INFO_TBL.
**************************************************************************/
RENAME  XXWC_B2B_CUST_INFO_TBL TO XXWC_B2B_CUST_INFO_TBL_BK1210;