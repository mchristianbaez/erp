DROP TABLE XXWC.XXWC_OM_DMS_CHANGE_ARCHIVE_TBL CASCADE CONSTRAINTS;

   /**************************************************************************
    File Name:XXWC_OM_DMS_CHANGE_ARCHIVE_TBL.sql
    TYPE:     Table
    Description: Table used to Archive DMS Delivery Information

    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     11-FEB-2015   Gopi Damuluri   Initial version TMS# 20150128-00043
   **************************************************************************/

CREATE TABLE XXWC.XXWC_OM_DMS_CHANGE_ARCHIVE_TBL
(
  HEADER_ID         NUMBER,
  LINE_ID           NUMBER,
  DELIVERY_ID       NUMBER,
  CREATED_BY        NUMBER,
  CREATION_DATE     DATE,
  LAST_UPDATED_BY   NUMBER,
  LAST_UPDATE_DATE  DATE,
  CHANGE_TYPE       VARCHAR2(1 BYTE),
  ORG_ID            NUMBER,
  STATUS            VARCHAR2(240 BYTE)
)
/

CREATE SYNONYM APPS.XXWC_OM_DMS_CHANGE_ARCHIVE_TBL FOR XXWC.XXWC_OM_DMS_CHANGE_ARCHIVE_TBL
/

BEGIN
   DBMS_RLS.ADD_POLICY ('APPS',                                     -- SCHEMA NAME
                        'XXWC_OM_DMS_CHANGE_ARCHIVE_TBL',           -- SYNONYM NAME
                        'ORG_SEC',                                  -- USE POLICY_NAME 'ORG_SEC' –STANDARD POLICY
                        'APPS',                                     -- FUNCTION_SCHEM
                        'MO_GLOBAL.ORG_SECURITY',                   -- STANDARD MO VPD POLICY FUN
                        'SELECT, INSERT, UPDATE, DELETE',           -- STATEMENT_TYPE
                        TRUE,                                       -- UPDATE_CHECK
                        TRUE                                        -- POLICY ENABLE
                            );
   COMMIT;
END;