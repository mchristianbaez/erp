/*************************************************************************
  $Header XXWC_USER_BLOCKING_LOG_TBL $
  Module Name: XXWC_USER_BLOCKING_LOG_TBL

  PURPOSE: Table to store logging information about user blocking sessions

  TMS Task Id :  20150210-00079 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        13-Feb-2015  Andre Rivas		    Initial Version
  2.0        24-Feb-2015  Andre Rivas		    Added and renamed columns
**************************************************************************/
CREATE TABLE XXWC.XXWC_USER_BLOCKING_LOG_TBL(
 "SNAPSHOT_TIME"          DATE
,"WAIT_MINS"              NUMBER
,"BLOCKING_STATUS"        VARCHAR2(2000)
,"Blocking_SQL_ID"        VARCHAR2(20)
,"Blocking_Prev_SQL_ID"   varchar2(20)
,"Blocking_Module"        VARCHAR2(64)
,"Blocking_Action"        VARCHAR2(64)
,"Blocking_Event"         VARCHAR2(64)
,"Blocking_Instance"      NUMBER
,"Block_Type"             VARCHAR(64)
,"Blocked_SQL_ID"         VARCHAR2(13)
,"Blocked_Module"         VARCHAR2(64)
,"Blocked_Action"         VARCHAR2(64)
,"Blocked_Event"          VARCHAR2(64)
,"Blocked_Instance"       NUMBER
,"Blocked_Wait_Class"     VARCHAR2(240)
,"To_Kill_Blocker"        VARCHAR2(300)
,"Blocking_NT_ID"         VARCHAR2(64)
,"Blocking_Person"        VARCHAR2(240)
,"Blocked_NT_ID"          VARCHAR2(64)
,"Blocked_Person"         VARCHAR2(240));