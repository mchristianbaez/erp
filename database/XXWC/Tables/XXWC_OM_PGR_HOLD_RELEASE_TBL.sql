create table xxwc.xxwc_om_pgr_hold_release_tbl
(
header_id number,
hold_id number,
org_id number,
process_flag varchar2(1),
error_message varchar2(240),
creation_date date,
created_by number,
last_update_date date,
last_updated_by number,
last_update_login number
)
/

grant all on xxwc.xxwc_om_pgr_hold_release_tbl to apps
/

create synonym apps.xxwc_om_pgr_hold_release_tbl for xxwc.xxwc_om_pgr_hold_release_tbl
/
