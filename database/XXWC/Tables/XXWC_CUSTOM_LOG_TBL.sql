 /****************************************************************************************************
    File Name:XXWC_CUSTOM_LOG_TBL.sql
    VERSION DATE          AUTHOR(S)           DESCRIPTION
    ------- -----------   --------------- ---------------------------------
	1.0     02-Oct-2018   P.Vamshidhar        TMS#20180806-00122 - Purchasing Mass 
	                                          upload taking more time these days
											  Added debug messages.
   *************************************************************************************************/

CREATE TABLE XXWC.XXWC_CUSTOM_LOG_TBL
(SNO NUMBER,
 PROCESS_NAME VARCHAR2(1000),
 LOG_MSG varchar2(4000),
 REQUEST_ID NUMBER,
 CREATION_DATE DATE DEFAULT SYSDATE)
 /

