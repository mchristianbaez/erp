/*******************************************************************************************************************
File Name: XXWC_CHG_MTL_CROSS_REF.sql

TYPE:     Table

Description: Table used to main items referece data till creating in base tables.

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------------------------------------------
1.0     01-MAR-2015   Shankar Vanga   Table used to main items referece data till creating in base tables.
                                       TMS#20150126-00044  - Create Single Item Workflow Tool
1.1     01-Dec-2015   P.Vamshidhar    TMS#20151201-00053  - PCAM error page displayed editing xref owner field
********************************************************************************************************************/

CREATE TABLE XXWC.XXWC_CHG_MTL_CROSS_REF
(
  ROW_ID                     ROWID,
  CROSS_REFERENCE_ID         NUMBER,
  INVENTORY_ITEM_ID          NUMBER,
  ORGANIZATION_ID            NUMBER,
  CROSS_REFERENCE_TYPE       VARCHAR2(25 BYTE),
  CROSS_REFERENCE            VARCHAR2(255 BYTE),
  DESCRIPTION                VARCHAR2(240 BYTE),
  LAST_UPDATE_DATE           DATE,
  LAST_UPDATED_BY            NUMBER,
  CREATION_DATE              DATE,
  CREATED_BY                 NUMBER,
  LAST_UPDATE_LOGIN          NUMBER,
  REQUEST_ID                 NUMBER,
  PROGRAM_APPLICATION_ID     NUMBER,
  PROGRAM_ID                 NUMBER,
  PROGRAM_UPDATE_DATE        DATE,
  ATTRIBUTE_CATEGORY         VARCHAR2(150 BYTE),
  ATTRIBUTE1                 VARCHAR2(150 BYTE),
  ATTRIBUTE2                 VARCHAR2(150 BYTE),
  ATTRIBUTE3                 VARCHAR2(150 BYTE),
  ATTRIBUTE4                 VARCHAR2(150 BYTE),
  ATTRIBUTE5                 VARCHAR2(150 BYTE),
  ATTRIBUTE6                 VARCHAR2(150 BYTE),
  ATTRIBUTE7                 VARCHAR2(150 BYTE),
  ATTRIBUTE8                 VARCHAR2(150 BYTE),
  ATTRIBUTE9                 VARCHAR2(150 BYTE),
  ATTRIBUTE10                VARCHAR2(150 BYTE),
  ATTRIBUTE11                VARCHAR2(150 BYTE),
  ATTRIBUTE12                VARCHAR2(150 BYTE),
  ATTRIBUTE13                VARCHAR2(150 BYTE),
  ATTRIBUTE14                VARCHAR2(150 BYTE),
  ATTRIBUTE15                VARCHAR2(150 BYTE),
  ORG_INDEPENDENT_FLAG       VARCHAR2(1 BYTE),
  UOM_CODE                   VARCHAR2(3 BYTE),
  REVISION_ID                NUMBER,
  EPC_GTIN_SERIAL            NUMBER,
  SOURCE_SYSTEM_ID           NUMBER,
  START_DATE_ACTIVE          DATE,
  END_DATE_ACTIVE            DATE,
  OBJECT_VERSION_NUMBER      NUMBER,
  ORACLE_CROSS_REFERENCE_ID  NUMBER,
  CHANGE_ID                  NUMBER,
  ORACLE_CHANGE_ID           NUMBER,
  MFG_DELETE_FLAG            VARCHAR2(100 BYTE), -- Added by vamshi in 1.1 Rev
  BAR_DELETE_FLAG            VARCHAR2(100 BYTE)  -- Added by Vamshi in 1.1 Rev
);

GRANT ALL ON XXWC.XXWC_CHG_MTL_CROSS_REF TO APPS;

CREATE OR REPLACE PUBLIC SYNONYM APPS.XXWC_CHG_MTL_CROSS_REF FOR XXWC.XXWC_CHG_MTL_CROSS_REF;
