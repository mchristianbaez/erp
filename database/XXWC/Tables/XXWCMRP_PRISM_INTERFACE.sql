CREATE TABLE XXWC.XXWCMRP_PRISM_INTERFACE
(
  ORGANIZATION_CODE  VARCHAR2(3 BYTE),
  ITEM_NUMBER        VARCHAR2(80 BYTE),
  ITEM_DESCRIPTION   VARCHAR2(240 BYTE),
  MONTH1             NUMBER,
  MONTH2             NUMBER,
  MONTH3             NUMBER,
  MONTH4             NUMBER,
  MONTH5             NUMBER,
  MONTH6             NUMBER,
  MONTH7             NUMBER,
  MONTH8             NUMBER,
  MONTH9             NUMBER,
  MONTH10            NUMBER,
  MONTH11            NUMBER,
  MONTH12            NUMBER,
  PROCESS_FLAG       VARCHAR2(1 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


