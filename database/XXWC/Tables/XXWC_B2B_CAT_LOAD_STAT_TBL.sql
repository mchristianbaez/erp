/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_B2B_CAT_LOAD_STAT_TBL$
  Module Name: XXWC_B2B_CAT_LOAD_STAT_TBL

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-Mar-2016   Pahwa, Nancy                Initially Created 
TMS# 20160223-00029   
**************************************************************************/
CREATE TABLE  "XXWC"."XXWC_B2B_CAT_LOAD_STAT_TBL" 
   (  "LOAD_ID" NUMBER NOT NULL ENABLE, 
  "LOAD_DATE" TIMESTAMP (6) DEFAULT SYSDATE NOT NULL ENABLE, 
  "UPDATE_STATUS" VARCHAR2(50) NOT NULL ENABLE, 
   CONSTRAINT "XXWC_B2B_CAT_LOAD_STAT_PK" PRIMARY KEY ("LOAD_ID")
  USING INDEX  ENABLE
   );
/