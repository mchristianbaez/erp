/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_B2B_CAT_CUSTOMER_TBL$
  Module Name: XXWC.XXWC_B2B_CAT_CUSTOMER_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029 
**************************************************************************/
-- Create table
create table XXWC.XXWC_B2B_CAT_CUSTOMER_TBL
(
  id                NUMBER not null,
  customer_id       NUMBER,
  catalog_group_id  NUMBER,
  item_number       VARCHAR2(100),
  created_by        VARCHAR2(20),
  created_on        DATE,
  updated_by        VARCHAR2(20),
  updated_on        DATE,
  cust_catalog_name VARCHAR2(20),
  is_active         VARCHAR2(1),
  hash_key          VARCHAR2(100),
  inventory_item_id NUMBER
)
tablespace APPS_TS_TX_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table XXWC_B2B_CAT_CUSTOMER_TBL
  add constraint XXWC_CUSTOMER_CATALOG_PK primary key (ID)
  using index 
  tablespace APPS_TS_TX_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
/