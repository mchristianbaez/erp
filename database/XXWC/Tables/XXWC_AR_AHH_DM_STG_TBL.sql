   /***************************************************************************************************************************************************
        $Header XXWC_AR_AHH_DM_STG_TBL $
        Module Name: XXWC_AR_AHH_DM_STG_TBL.sql

        PURPOSE:AHH AR Debit Memos import. 

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        08/16/2018  P.Vamshidhar     20180816-00001_AHH Debit Memo and Cash Receipts Staging table Change
   ******************************************************************************************************************************************************/
   
CREATE TABLE XXWC.XXWC_AR_AHH_DM_STG_TBL ( 
CONTROL_NBR                VARCHAR2(15),  
DEPOSIT_DATE               DATE         , 
OPERATOR_CODE              VARCHAR2(15),  
CHECK_NBR                  VARCHAR2(20), 
CHECK_AMT                  NUMBER        ,
URL_LINK                   VARCHAR2(200) ,
CUSTOMER_NBR               VARCHAR2(30),  
INVOICE_NBR                VARCHAR2(30) , 
FILL                       VARCHAR2(15)  ,
INVOICE_AMT                NUMBER        ,
DISC_AMT                   NUMBER        ,
GL_ACCT_NBR                VARCHAR2(10)  ,
SHORT_PAY_CODE             VARCHAR2(30)  ,
CREATION_DATE              DATE          ,
UPDATED_DATE               DATE          ,
STATUS                     VARCHAR2(64)  ,
BILL_TO_LOCATION           VARCHAR2(40)  ,
COMMENTS                   VARCHAR2(240) ,
SHORT_CODE_INTERFACED      VARCHAR2(1)   ,
SHORT_CODE_INT_DATE        DATE          ,
RECEIPT_TYPE               VARCHAR2(15)  ,
ORACLE_ACCOUNT_NBR         VARCHAR2(164) ,
CUST_ACCOUNT_ID            NUMBER        ,
ORACLE_FLAG                VARCHAR2(128) ,
RECEIPT_ID                 NUMBER        ,
ABA_NUMBER                 VARCHAR2(30)  ,
BANK_ACCOUNT_NUM           VARCHAR2(100) ,
ORG_ID                     NUMBER        );

CREATE INDEX XXWC.XXWC_AR_AHH_DM_STG_TBL_N1 ON XXWC.XXWC_AR_AHH_DM_STG_TBL
(CUSTOMER_NBR)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWC_AR_AHH_DM_STG_TBL_N1
  MONITORING USAGE;


CREATE INDEX XXWC.XXWC_AR_AHH_DM_STG_TBL_N2 ON XXWC.XXWC_AR_AHH_DM_STG_TBL
(DEPOSIT_DATE)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWC_AR_AHH_DM_STG_TBL_N2
  MONITORING USAGE;


CREATE INDEX XXWC.XXWC_AR_AHH_DM_STG_TBL_N3 ON XXWC.XXWC_AR_AHH_DM_STG_TBL
(CUSTOMER_NBR, CHECK_NBR)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWC_AR_AHH_DM_STG_TBL_N3
  MONITORING USAGE;


CREATE INDEX XXWC.XXWC_AR_AHH_DM_STG_TBL_N4 ON XXWC.XXWC_AR_AHH_DM_STG_TBL
(RECEIPT_ID)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWC_AR_AHH_DM_STG_TBL_N4
  MONITORING USAGE;


CREATE INDEX XXWC.XXWC_AR_AHH_DM_STG_TBL_N5 ON XXWC.XXWC_AR_AHH_DM_STG_TBL
(STATUS, CONTROL_NBR)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWC_AR_AHH_DM_STG_TBL_N5
  MONITORING USAGE;



GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_AHH_DM_STG_TBL TO EA_APEX;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_AHH_DM_STG_TBL TO INTERFACE_APEXWC;

GRANT SELECT ON XXWC.XXWC_AR_AHH_DM_STG_TBL TO INTERFACE_PRISM;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_AHH_DM_STG_TBL TO XXWC_DEV_ADMIN_ROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_AHH_DM_STG_TBL TO XXWC_EDIT_IFACE_ROLE;

GRANT INSERT ON XXWC.XXWC_AR_AHH_DM_STG_TBL TO XXWC_PRISM_INSERT_ROLE;

GRANT SELECT ON XXWC.XXWC_AR_AHH_DM_STG_TBL TO XXWC_PRISM_SELECT_ROLE;
/