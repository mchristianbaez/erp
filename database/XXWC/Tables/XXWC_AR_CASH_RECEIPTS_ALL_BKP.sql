/*******************************************************************************************************
  -- Table Name XXWC_AR_CASH_RECEIPTS_ALL_BKP
  -- ***************************************************************************************************
  --
  -- PURPOSE: Backup table for ar_cash_receipts_all
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     09-Sep-2016   Niraj K Ranjan   Initial Version - TMS#20150903-00017   IT - data script for new URL on lockbox receipts
********************************************************************************************************/
DROP TABLE xxwc.xxwc_ar_cash_receipts_all_bkp CASCADE CONSTRAINTS;

CREATE TABLE xxwc.xxwc_ar_cash_receipts_all_bkp
AS (SELECT * FROM ar_cash_receipts_all WHERE TRUNC(creation_date) < TO_DATE('15-NOV-2015','DD-MON-YYYY'));