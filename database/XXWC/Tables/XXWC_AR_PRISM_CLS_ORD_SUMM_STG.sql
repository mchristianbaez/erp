CREATE TABLE XXWC.XXWC_AR_PRISM_CLS_ORD_SUMM_STG
(
  CLOSED_ORDER_STG_ID    NUMBER,
  COMPANY                VARCHAR2(4 BYTE),
  LOCATION               NUMBER,
  SALESREP_NUMBER        NUMBER,
  CUSTOMER_NUMBER        NUMBER,
  ORIGIN_DATE            DATE,
  MASTER_NUMBER          NUMBER,
  MASTER_ORIGIN_DATE     DATE,
  INVOICE_NUMBER         NUMBER,
  ORDER_NUMBER           NUMBER,
  SUBORDER_NUMBER        NUMBER,
  DATE_OF_SALE           DATE,
  BUSINESS_DATE          DATE,
  INVOICE_DATE           DATE,
  NET_SALES_DOLLARS      NUMBER,
  NET_SALES_COST         NUMBER,
  NET_STANDARD_COST      NUMBER,
  NET_AVERAGE_COST       NUMBER,
  NET_MARGIN_COST        NUMBER,
  NET_VENDOR_QUOTE_COST  NUMBER,
  NET_SALES_UNITS        NUMBER,
  GP                     NUMBER,
  GP_PERCENT             NUMBER,
  MARGIN_GP              NUMBER,
  MARGIN_GP_PERCENT      NUMBER,
  PURCHASE_ORDER_NUMBER  VARCHAR2(18 BYTE),
  TAKEN_BY               VARCHAR2(5 BYTE),
  CREDIT_MEMO_FLAG       VARCHAR2(1 BYTE),
  RENTAL_ORDER_FLAG      VARCHAR2(1 BYTE),
  DIRECT_SHIPMENT_FLAG   VARCHAR2(1 BYTE),
  TRX_NUMBER             VARCHAR2(30 BYTE),
  CREATED_BY             NUMBER,
  CREATION_DATE          DATE,
  LAST_UPDATED_BY        NUMBER,
  LAST_UPDATE_DATE       DATE,
  LAST_UPDATE_LOGIN      NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX XXWC.XXWC_AR_PRISM_CLS_ORD_STG_PK ON XXWC.XXWC_AR_PRISM_CLS_ORD_SUMM_STG
(CLOSED_ORDER_STG_ID)
LOGGING
NOPARALLEL;


CREATE OR REPLACE TRIGGER XXWC.xxwc_ar_prism_cls_ord_stg_T1
   BEFORE INSERT
   ON XXWC.XXWC_AR_PRISM_CLS_ORD_SUMM_STG    REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   L_closed_order_stg_id   NUMBER;
/******************************************************************************
   NAME:
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        06/05/2012             1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:
      Sysdate:         3/29/2012
      Date and Time:   3/29/2012, 3:54:27 PM, and 3/29/2012 3:54:27 PM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:      XXWC_AR_CUSTOMER_IFACE_STG_TRG1 (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/
BEGIN
   L_closed_order_stg_id := 0;

	SELECT xxwc_ar_prism_closed_ords_s.NEXTVAL
     INTO L_closed_order_stg_id
     FROM DUAL;

   :NEW.closed_order_stg_id  := L_closed_order_stg_id;

   :NEW.CREATION_DATE := SYSDATE;
   :NEW.CREATED_BY := 1;
EXCEPTION
   WHEN OTHERS
   THEN
      -- Consider logging the error and then re-raise
      RAISE;
END;
/


ALTER TABLE XXWC.XXWC_AR_PRISM_CLS_ORD_SUMM_STG ADD (
  CONSTRAINT XXWC_AR_PRISM_CLS_ORD_STG_PK
 PRIMARY KEY
 (CLOSED_ORDER_STG_ID));

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_AR_PRISM_CLS_ORD_SUMM_STG TO APPS WITH GRANT OPTION;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_PRISM_CLS_ORD_SUMM_STG TO XXWC_EDIT_IFACE_ROLE;

GRANT INSERT ON XXWC.XXWC_AR_PRISM_CLS_ORD_SUMM_STG TO XXWC_PRISM_INSERT_ROLE;

GRANT SELECT ON XXWC.XXWC_AR_PRISM_CLS_ORD_SUMM_STG TO XXWC_PRISM_SELECT_ROLE;

