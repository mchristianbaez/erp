/*************************************************************************
  $Header ALTER_XXWC_WSH_SHIPPING_STG.sql $
  Module Name: ALTER_XXWC_WSH_SHIPPING_STG.sql

  PURPOSE:   To Add the column ORDER_HEADER_ID

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        02-Jun-2016 Rakesh Patel            TMS#20160511-00208-IT - Improvements to AR Credit Management backend workflow
**************************************************************************/
ALTER TABLE XXWC.XXWC_OM_WF_LOG_TBL ADD (ORDER_HEADER_ID VARCHAR2(10));