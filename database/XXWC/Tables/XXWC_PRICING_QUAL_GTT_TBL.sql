/*************************************************************************
  $Header XXWC_PRICING_QUAL_GTT_TBL $
  Module Name: XXWC_PRICING_QUAL_GTT_TBL

  PURPOSE: Table to store Pricing Segmentation qualifier details

  TMS Task Id : 20160115-00002

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        15-Jan-2016  Gopi Damuluri         Initial Version
**************************************************************************/

CREATE GLOBAL TEMPORARY TABLE xxwc.xxwc_pricing_qual_gtt_tbl
(
  PRODUCT_ATTR         VARCHAR2(240),
  PRODUCT_ATTR_VAL     VARCHAR2(240),
  QUALIFIER_RULE_ID    NUMBER,
  LIST_HEADER_ID       NUMBER,
  LIST_LINE_ID         NUMBER,
  CUSTOMER_ID          NUMBER,
  CUSTOMER_SITE_ID     NUMBER
);