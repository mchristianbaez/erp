/***********************************************************************************
File Name: XXWC_CAT_CHANGE_TAB
PROGRAM TYPE: SQL TABLE file
HISTORY
PURPOSE: Table used for storing data used in generating parent catalog relationship 
         data file
====================================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ----------------------------------------------
1.0     09/23/2012    Rajiv Rathod    Initial creation of the file
************************************************************************************/

DROP TABLE XXWC.XXWC_CAT_CHANGE_TAB CASCADE CONSTRAINTS;

DROP SYNONYM XXWC_CAT_CHANGE_TAB;

CREATE TABLE XXWC.XXWC_CAT_CHANGE_TAB
(
  INVENTORY_ITEM_ID  NUMBER,
  ORGANIZATION_ID    NUMBER,
  CATEGORY_SET_ID    NUMBER,
  CATEGORY_ID_OLD    NUMBER,
  CATEGORY_ID_NEW    NUMBER,
  CREATED_BY         NUMBER,
  LAST_UPDATE_DATE   DATE,
  SEQUENCE           NUMBER,
  DMLTYPE            VARCHAR2(1)
);

CREATE SYNONYM APPS.XXWC_CAT_CHANGE_TAB FOR XXWC.XXWC_CAT_CHANGE_TAB;

CREATE INDEX XXWC.XXWC_CAT_CHANGE_TAB_N1
   ON XXWC.XXWC_CAT_CHANGE_TAB (SEQUENCE)
/