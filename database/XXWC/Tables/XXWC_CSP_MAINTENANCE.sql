CREATE TABLE XXWC.XXWC_CSP_MAINTENANCE
(
  ACTION             VARCHAR2(240 BYTE)             NULL,
  REFERENCE_ID       VARCHAR2(240 BYTE)             NULL,
  AGREEMENT_TYPE     VARCHAR2(30 BYTE)              NULL,
  PRODUCT_ATTRIBUTE  VARCHAR2(30 BYTE)              NULL,
  PRODUCT_VALUE      VARCHAR2(100 BYTE)             NULL,
  APP_METHOD         VARCHAR2(30 BYTE)              NULL,
  MODIFIER_VALUE     NUMBER                         NULL,
  VQ_NUMBER          VARCHAR2(100 BYTE)             NULL,
  SPECIAL_COST       NUMBER                         NULL,
  VENDOR_NUMBER      VARCHAR2(30 BYTE)              NULL,
  PROCESS_FLAG       VARCHAR2(1 BYTE)               NULL,
  ERROR_MESSAGE      VARCHAR2(240 BYTE)             NULL
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


