DROP TABLE XXWC.XXWC_B2B_SO_LINES_STG_TBL CASCADE CONSTRAINTS;

  /*********************************************************************************
  -- Table Name XXWC_B2B_SO_LINES_STG_TBL
  -- *******************************************************************************
  --
  -- PURPOSE: Staging table used to store B2B Sales Order Lines
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     04-Apr-2015   Gopi Damuluri   TMS# 20150317-00069
                                           Added Notes column to XXWC.XXWC_B2B_SO_LINES_STG_TBL table
  *******************************************************************************/

CREATE TABLE XXWC.XXWC_B2B_SO_LINES_STG_TBL
(
  CUSTOMER_PO_NUMBER  VARCHAR2(50 BYTE),
  LINE_NUMBER         NUMBER,
  INVENTORY_ITEM      VARCHAR2(2000 BYTE),
  ORDERED_QUANTITY    NUMBER,
  UNIT_OF_MEASURE     VARCHAR2(3 BYTE),
  UNIT_SELLING_PRICE  NUMBER,
  REQUEST_DATE        DATE,
  ITEM_DESCRIPTION    VARCHAR2(2000 BYTE),
  PROCESS_FLAG        VARCHAR2(1 BYTE),
  ERROR_MESSAGE       VARCHAR2(2000 BYTE),
  CREATION_DATE       DATE,
  CREATED_BY          NUMBER,
  LAST_UPDATE_DATE    DATE,
  LAST_UPDATED_BY     NUMBER,
  REC_TYPE            VARCHAR2(2 BYTE),
  NOTES               CHAR(1000 BYTE)     -- Version# 1.0
);