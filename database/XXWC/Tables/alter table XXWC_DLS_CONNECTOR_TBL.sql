  /********************************************************************************
  FILE NAME: xxwc.XXWC_DLS_CONNECTOR_TBL
  
  PROGRAM TYPE:table for edqp requests
  
  PURPOSE: table for edqp requests
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     01/19/2018  Nancy Pahwa     TMS#20171108-00066 - Issue with populating cross reference
  *******************************************************************************************/
alter table xxwc.XXWC_DLS_CONNECTOR_TBL add START_TIME date;
alter table xxwc.XXWC_DLS_CONNECTOR_TBL add END_TIME date;
/