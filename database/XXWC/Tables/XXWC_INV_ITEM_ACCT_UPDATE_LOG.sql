/******************************************************************************************************
-- File Name: XXWC_INV_ITEM_ACCT_UPDATE_LOG.sql
--
-- PROGRAM TYPE: Table script
--
-- PURPOSE: Table use to track account update details..
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
--                                       Initial version

************************************************************************************************************/
CREATE TABLE XXWC.XXWC_INV_ITEM_ACCT_UPDATE_LOG
(
  INVENTORY_ITEM_ID  NUMBER,
  ORGANIZATION_ID    NUMBER,
  ACCOUNT_TYPE       VARCHAR2(50 BYTE),
  OLD_CCID           NUMBER,
  NEW_CCID           NUMBER,
  STATUS             VARCHAR2(10 BYTE),
  CREATION_DATE      DATE                       DEFAULT SYSDATE,
  CREATED_BY         NUMBER,
  COMMENTS           VARCHAR2(512 BYTE)
)
/