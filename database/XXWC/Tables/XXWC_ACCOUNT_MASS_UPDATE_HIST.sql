/* Formatted on 11/28/2012 6:12:56 PM (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.XXWC_ACCOUNT_MASS_UPDATE_HIST
-- Generated 11/28/2012 6:12:51 PM from XXWC@EBIZDEV

CREATE TABLE xxwc.xxwc_account_mass_update_hist
(
    change                 VARCHAR2 (1024 BYTE)
   ,run_id                 NUMBER
   ,change_name            VARCHAR2 (64 BYTE)
   ,old_name               VARCHAR2 (128 BYTE)
   ,new_name               VARCHAR2 (128 BYTE)
   ,old_id                 NUMBER
   ,new_id                 NUMBER
   ,records_count_before   NUMBER
   ,records_count_after    NUMBER
   ,last_updated_by        NUMBER
   ,last_update_date       DATE
   ,responsibility_id      NUMBER
   ,org_id                 NUMBER
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxwc_data
STORAGE (INITIAL 65536
         NEXT 1048576
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
NOPARALLEL
NOLOGGING
/

-- End of DDL Script for Table XXWC.XXWC_ACCOUNT_MASS_UPDATE_HIST
