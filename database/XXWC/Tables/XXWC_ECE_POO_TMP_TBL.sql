   /*************************************************************************
     $Header XXWC_ECE_POO_TMP_TBL.sql $     

     PURPOSE: Wrapper for the PO Outbound EDI program
     TMS Task Id :  20141002-00016

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        6-Oct-2014  Manjula Chellappan    Initial Version

   **************************************************************************/
CREATE TABLE XXWC.XXWC_ECE_POO_TMP_TBL
(
   request_id         NUMBER,
   parent_request_id   NUMBER,
   request_date    DATE,
   po_number VARCHAR2(100),
   edi_file_name VARCHAR2(100)
)