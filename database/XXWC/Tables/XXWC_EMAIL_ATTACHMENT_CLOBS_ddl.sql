/* Formatted on 21-Feb-2013 18:58:47 (QP5 v5.206) */
-- Start of DDL Script for Table XXWC.EMAIL_ATTACHMENT_CLOBS
-- Generated 21-Feb-2013 18:58:38 from XXWC@EBIZDEV

CREATE GLOBAL TEMPORARY TABLE xxwc.email_attachment_clobs
(
    clob_less_32k   CLOB
   ,file_name       VARCHAR2 (254)
   ,insert_id       NUMBER
)
ON COMMIT PRESERVE ROWS
  SEGMENT CREATION IMMEDIATE
  NOPARALLEL
/

-- End of DDL Script for Table XXWC.EMAIL_ATTACHMENT_CLOBS
