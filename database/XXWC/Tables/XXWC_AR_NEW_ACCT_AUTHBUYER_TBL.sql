/*************************************************************************
  $Header XXWC_AR_NEW_ACCT_AUTHBUYER_TBLL $
  Module Name: XXWC.XXWC_AR_NEW_ACCT_AUTHBUYER_TBL

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0                                           Initial version
  1.1        28-Jan-2015  Maharajan Shunmugam   TMS 20140516-00042
**************************************************************************/
CREATE TABLE XXWC.XXWC_AR_NEW_ACCT_AUTHBUYER_TBL
(
  RECORD_ID          NUMBER,
  FIRST_NAME         VARCHAR2(100 BYTE),
  LAST_NAME          VARCHAR2(100 BYTE),
  PROCESS_FLAG       VARCHAR2(1 BYTE),
  PROCESS_MESSAGE    VARCHAR2(2000 BYTE),
  CREATION_DATE      DATE,
  CREATED_BY         NUMBER,
  LAST_UPDATE_DATE   DATE,
  LAST_UPDATED_BY    NUMBER,
  LAST_UPDATE_LOGIN  NUMBER,                    --Added below columns for ver#1.1
  ROLE               VARCHAR2(80 BYTE),
  EMAIL_ADDRESS      VARCHAR2(150 BYTE),
  PHONE_NUMBER       VARCHAR2(30 BYTE),
  FAX_NUMBER         VARCHAR2(30 BYTE)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

CREATE OR REPLACE SYNONYM APPS.XXWC_AR_NEW_ACCT_AUTHBUYER_TBL FOR XXWC.XXWC_AR_NEW_ACCT_AUTHBUYER_TBL;
