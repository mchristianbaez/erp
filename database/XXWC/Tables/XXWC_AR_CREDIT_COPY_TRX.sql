CREATE TABLE XXWC.XXWC_AR_CREDIT_COPY_TRX
(
  CREDIT_COPY_TRX_ID          NUMBER            NOT NULL,
  CUSTOMER_TRX_ID             NUMBER,
  REV_CUSTOMER_TRX_ID         NUMBER,
  NEW_CUSTOMER_TRX_ID         NUMBER,
  CUST_ACCOUNT_ID             NUMBER,
  REBILL_BILL_TO_SITE_USE_ID  NUMBER,
  REBILL_SHIP_TO_SITE_USE_ID  NUMBER,
  REASON_CODE                 VARCHAR2(30 BYTE),
  LAST_UPDATE_DATE            DATE              NOT NULL,
  LAST_UPDATED_BY             NUMBER            NOT NULL,
  CREATION_DATE               DATE              NOT NULL,
  CREATED_BY                  NUMBER            NOT NULL,
  LAST_UPDATE_LOGIN           NUMBER,
  OBJECT_VERSION_NUMBER       NUMBER,
  PROCESS_STATUS              VARCHAR2(1 BYTE),
  ERROR_MESSAGE               VARCHAR2(2000 BYTE),
  SELECT_TRX                  VARCHAR2(1 BYTE),
  REQUEST_ID                  NUMBER
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


