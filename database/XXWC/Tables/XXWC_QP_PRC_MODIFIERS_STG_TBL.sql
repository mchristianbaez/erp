 /****************************************************************************************************
  Table: XXWC_QP_PRC_MODIFIERS_STG_TBL.sql     
  Description: Activating/Inactivating Modifiers.
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     26-Apr-2017        P.Vamshidhar    Initial version TMS# 20160526-00017 
                                             Ability to inactive Matrix modifiers in Bulk
  *****************************************************************************************************/
CREATE TABLE XXWC.XXWC_QP_PRC_MODIFIERS_STG_TBL
(
  NAME               VARCHAR2(240 BYTE),
  ACTIVE_FLAG        VARCHAR2(1 BYTE),
  START_DATE_ACTIVE  DATE,
  END_DATE_ACTIVE    DATE,
  OPERATION          VARCHAR2(20 BYTE),
  FILE_ID            NUMBER,
  CREATION_DATE      DATE,
  CREATED_BY         NUMBER,
  LAST_UPDATE_DATE   DATE,
  LAST_UPDATED_BY    NUMBER,
  REQUEST_ID         NUMBER,
  STATUS             VARCHAR2(20 BYTE)
)
/

