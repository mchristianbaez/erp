CREATE TABLE XXWC.XXWC_ADI_ERROR
(
  INSERT_DATE  DATE,
  LINE_STRING  VARCHAR2(1024 BYTE),
  ERROR        VARCHAR2(1024 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


