DROP TABLE XXWC.XXWC_OM_WF_LOG_TBL CASCADE CONSTRAINTS;
/*******************************************************************************************************
  -- Table Name XXWC_OM_WF_LOG_TBL
  -- ***************************************************************************************************
  --
  -- PURPOSE: Custom table used to store updated script info by through workflow OEOL.wft
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     16-may-2016   Niraj K Ranjan   Initial Version
********************************************************************************************************/
CREATE TABLE XXWC.XXWC_OM_WF_LOG_TBL
(ORDER_LINE_ID     NUMBER,
 API_NAME          VARCHAR2(50),
 API_SECTION       VARCHAR2(200),
 CREATION_DATE     DATE,
 CREATED_BY        NUMBER,
 LAST_UPDATE_DATE  DATE,
 LAST_UPDATED_BY   NUMBER
);

