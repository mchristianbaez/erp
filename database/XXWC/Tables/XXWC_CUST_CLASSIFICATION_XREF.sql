CREATE TABLE XXWC.XXWC_CUST_CLASSIFICATION_XREF
(
  PRISM_CODE           VARCHAR2(20 BYTE),
  PRISM_DESCRIPTION    VARCHAR2(100 BYTE),
  CUST_CLASSIFICATION  VARCHAR2(100 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


