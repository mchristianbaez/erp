CREATE TABLE XXWC.XXWC_EGO_WEB_HIERARCHY
(
  CHILD   VARCHAR2(150 BYTE),
  PARENT  VARCHAR2(150 BYTE)
)
TABLESPACE XXWC_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


