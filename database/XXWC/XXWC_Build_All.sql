--
-- Create Schema Script 
--   Database Version          : 11.2.0.2.0 
--   Database Compatible Level : 11.1.0 
--   Toad Version              : 11.5.0.56 
--   DB Connect String         : EBIZPRD 
--   Schema                    : XXWC 
--   Script Created by         : XXWC_DEV_ADMIN 
--   Script Created at         : 6/12/2012 9:57:12 AM 
--   Physical Location         :  
--   Notes                     :  
--

-- Object Counts: 
--   Indexes: 42        Columns: 64         
--   Sequences: 21 
--   Synonyms: 20 
--   Tables: 116        Columns: 4117       Constraints: 8      
--   Triggers: 6 



-- "Set define off" turns off substitution variables. 
Set define off; 


@./Sequences/XXWC_MRPRPROP_S.sql;
@./Sequences/XXWC_PO_SEL_ITEMS_S.sql;
@./Sequences/XXWC_INV_PRODUCT_REQUEST_S.sql;
@./Sequences/XXWC_AR_CREDIT_COPY_TRX_S.sql;
@./Sequences/XXWC_ONHAND_LOT_S.sql;
@./Sequences/XXWC_AR_CREATE_PROFILE_S.sql;
@./Sequences/XXWC_INV_CYCLECOUNT_HDR_S.sql;
@./Sequences/XXWC_INV_CYCLECOUNT_LINES_S.sql;
@./Sequences/XXWC_INV_CYCLECOUNT_LINE_DTL_S.sql;
@./Sequences/XXWC_MSTR_PARTS_EXCL_LIST_S.sql;
@./Sequences/XXWC_INV_CC_SPECIFIC_LINES_S.sql;
@./Sequences/XXWC_INV_CC_BLOCKOUT_DATE_S.sql;
@./Sequences/XXWC_QP_BATCH_S.sql;
@./Sequences/XXWC_OM_PRICING_GUARDRAIL_S.sql;
@./Sequences/XXWC_AR_CUSTOMER_IFACE_S.sql;
@./Sequences/XXWC_AR_CUST_CONTACT_IFACE_S.sql;
@./Sequences/XXWC_AR_CUST_SITE_IFACE_S.sql;
@./Sequences/XXWC_PRISM_LOCKBOX_RUN_S.sql;
@./Sequences/XXWC_AR_PRISM_RENTAL_LINE_S.sql;
@./Sequences/XXWC_AR_PRISM_RENTAL_ORD_S.sql;
@./Sequences/XXWC_AR_PRISM_INV_LN_NOTES_S.sql;
@./Tables/DW_SALESREPS.sql;
@./Tables/MNBVCT_2BDLFMGYZPLESASZ.sql;
@./Tables/PRISM_CUST_MAP.sql;
@./Tables/XXWCAP_INV_INT_STG_TBL.sql;
@./Tables/XXWCAR_CASH_RCPTS_TBL.sql;
@./Tables/XXWCAR_CASH_RCPTS_TBL_20120531.sql;
@./Tables/XXWCAR_DCTM_IB_FILES_TBL.sql;
@./Tables/XXWCAR_DNB_SCORING.sql;
@./Tables/XXWCAR_LOCKBOX_TBL.sql;
@./Tables/XXWCINV_DEMAND_FORECAST_IFACE.sql;
@./Tables/XXWCINV_INTERCO_TRANSFERS.sql;
@./Tables/XXWCINV_INV_AGING_GROUPS.sql;
@./Tables/XXWCINV_INV_AGING_TEMP.sql;
@./Tables/XXWCINV_ITEM_XREF_IFACE.sql;
@./Tables/XXWCINV_STOCK_LOCATOR_IFACE.sql;
@./Tables/XXWCMRP_PRISM_INTERFACE.sql;
@./Tables/XXWCMRP_SRC_RULE_ASSGN_IFACE.sql;
@./Tables/XXWCPO_BPA_INTERFACE.sql;
@./Tables/XXWC_ACCOUNT_STATUS_XREF.sql;
@./Tables/XXWC_AP_INVOICE_INT.sql;
@./Tables/XXWC_AP_PAID_INVOICE_STG.sql;
@./Tables/XXWC_AP_SUPPLIER_INT.sql;
@./Tables/XXWC_AP_SUPPLIER_SITES_INT.sql;
@./Tables/XXWC_AP_SUP_SITE_CONTACT_INT.sql;
@./Tables/XXWC_ARCUST_GETPAID_DUMP_TBL.sql;
@./Tables/XXWC_AREXTI_GETPAID_DUMP_BKP.sql;
@./Tables/XXWC_AREXTI_GETPAID_DUMP_TBL.sql;
@./Tables/XXWC_ARMAST_GETPAID_DUMP_BKP.sql;
@./Tables/XXWC_ARMAST_GETPAID_DUMP_TBL.sql;
@./Tables/XXWC_ARTRAN_GETPAID_DUMP_TBL.sql;
@./Tables/XXWC_AR_CONTACT_POINTS_CNV.sql;
@./Tables/XXWC_AR_CONT_IFACE_STG_052612.sql;
@./Tables/XXWC_AR_CREDIT_COPY_TRX.sql;
@./Tables/XXWC_AR_CUSTOMERS_CNV.sql;
@./Tables/XXWC_AR_CUSTOMER_IFACE_STG.sql;
@./Tables/XXWC_AR_CUST_CONTACT_IFACE_STG.sql;
@./Tables/XXWC_AR_CUST_IFACE_STG_052612.sql;
@./Tables/XXWC_AR_CUST_SITE_IFACE_STG.sql;
@./Tables/XXWC_AR_HOUSE_ACCOUNTS_XREF.sql;
@./Tables/XXWC_AR_INVOICES_CONV.sql;
@./Tables/XXWC_AR_INV_STG_TBL.sql;
@./Tables/XXWC_AR_INV_STG_TBL_20120531.sql;
@./Tables/XXWC_AR_PRISM_INV_LN_NOTES_STG.sql;
@./Tables/XXWC_AR_PRISM_INV_NOTES_TBL.sql;
@./Tables/XXWC_AR_PRISM_RENTAL_LN_STG.sql;
@./Tables/XXWC_AR_PRISM_RENTAL_ORD_STG.sql;
@./Tables/XXWC_AR_RECEIPTS_CONV.sql;
@./Tables/XXWC_AR_SITES_CNV.sql;
@./Tables/XXWC_AR_SITE_IFACE_STG_052612.sql;
@./Tables/XXWC_BACKORDERED_TXNS_LNS_STG.sql;
@./Tables/XXWC_BACKORDERED_TXNS_SNS_STG.sql;
@./Tables/XXWC_BATCH_TAXEC_TBL.sql;
@./Tables/XXWC_CALC_UPDATE_LEAD_TIME.sql;
@./Tables/XXWC_CC_STORE_LOCATIONS.sql;
@./Tables/XXWC_COLLECTOR_CR_ANALYST_XREF.sql;
@./Tables/XXWC_CR_CLASSIFICATION_XREF.sql;
@./Tables/XXWC_CUSTOMER_INT_ERRORS.sql;
@./Tables/XXWC_CUST_CLASSIFICATION_XREF.sql;
@./Tables/XXWC_CUST_PROFILE_CLASS_XREF.sql;
@./Tables/XXWC_CUST_XREF.sql;
@./Tables/XXWC_CUST_XREF_BKP.sql;
@./Tables/XXWC_EDI_INBOUND_FILES_ARCHIVE.sql;
@./Tables/XXWC_EDI_INBOUND_FILES_HISTORY.sql;
@./Tables/XXWC_ET_MAPPING_GTT_TBL.sql;
@./Tables/XXWC_GP_DMP_VAL.sql;
@./Tables/XXWC_GP_DMP_VAL_BKP.sql;
@./Tables/XXWC_GP_ORA_DIFF_TBL.sql;
@./Tables/XXWC_GP_ORA_DIFF_TBL_BKP.sql;
@./Tables/XXWC_GP_ORA_VAL.sql;
@./Tables/XXWC_GP_ORA_VAL2.sql;
@./Tables/XXWC_GP_ORA_VAL_BKP.sql;
@./Tables/XXWC_HCSUA_LOCATION_FIX_STG.sql;
@./Tables/XXWC_HCSUA_LOC_FIX_061012_BKP.sql;
@./Tables/XXWC_IBY_BANK_ACCOUNTS.sql;
@./Tables/XXWC_INVITEM_STG.sql;
@./Tables/XXWC_INVITEM_XREF_STG.sql;
@./Tables/XXWC_INV_CC_BIN_EXCL.sql;
@./Tables/XXWC_INV_CC_BLOCKOUT_DATES.sql;
@./Tables/XXWC_INV_CC_PARTS_EXCL.sql;
@./Tables/XXWC_INV_CC_SPECIFIC_LINES.sql;
@./Tables/XXWC_INV_CYCLECOUNT_DAYS.sql;
@./Tables/XXWC_INV_CYCLECOUNT_HDR.sql;
@./Tables/XXWC_INV_CYCLECOUNT_LINES.sql;
@./Tables/XXWC_INV_CYCLECOUNT_LINE_DTLS.sql;
@./Tables/XXWC_INV_PRODUCT_ORG.sql;
@./Tables/XXWC_INV_PRODUCT_REQUEST.sql;
@./Tables/XXWC_LEGAL_COLLECTION_IND_XREF.sql;
@./Tables/XXWC_MRPRPROP.sql;
@./Tables/XXWC_MSTR_PARTS_EXCL_LIST.sql;
@./Tables/XXWC_OM_CASH_REFUND_TBL.sql;
@./Tables/XXWC_OM_PRICING_GUARDRAIL.sql;
@./Tables/XXWC_ONHAND_BAL_CNV.sql;
@./Tables/XXWC_ONHAND_LOT_CNV.sql;
@./Tables/XXWC_PARTS_ON_FLY_GT.sql;
@./Tables/XXWC_PAYMENT_TERMS_XREF.sql;
@./Tables/XXWC_PGR_MESSAGE.sql;
@./Tables/XXWC_PO_FREIGHT_BURDEN_TBL.sql;
@./Tables/XXWC_PO_SEL_ITEMS_TBL.sql;
@./Tables/XXWC_PO_VENDOR_MINIMUM.sql;
@./Tables/XXWC_PRICING_GUARD_RAILS_GT.sql;
@./Tables/XXWC_QP_MODIFIER_HDR_CNV.sql;
@./Tables/XXWC_QP_MODIFIER_INT.sql;
@./Tables/XXWC_QP_MODIFIER_LINE_CNV.sql;
@./Tables/XXWC_RA_INT_DIST_060912_1.sql;
@./Tables/XXWC_RA_INT_DIST_060912_2.sql;
@./Tables/XXWC_RA_INT_DIST_061112_1.sql;
@./Tables/XXWC_RA_INT_LINES_060912_1.sql;
@./Tables/XXWC_RA_INT_LINES_060912_2.sql;
@./Tables/XXWC_RA_INT_LINES_061112_1.sql;
@./Tables/XXWC_RA_INT_SACR_060912_1.sql;
@./Tables/XXWC_RA_INT_SACR_061112_1.sql;
@./Tables/XXWC_REMIT_TO_ADDR_XREF.sql;
@./Tables/XXWC_SALES_VELOCITY_GT.sql;
@./Tables/XXWC_SHARE_VARIABLES.sql;
@./Tables/XXWC_SOURCING_RULES_CONV.sql;
@./Tables/XXWC_SR_ASSINGMENTS_CONV.sql;
@./Indexes/PK_HREMPLOYEEID.sql;
@./Indexes/XXWCAR_CASH_RCPTS_TBL_N1.sql;
@./Indexes/XXWCAR_CASH_RCPTS_TBL_N2.sql;
@./Indexes/XXWCAR_CASH_RCPTS_TBL_N3.sql;
@./Indexes/XXWCAR_CASH_RCPTS_TBL_N4.sql;
@./Indexes/XXWCINV_INV_AGING_GROUPS_U1.sql;
@./Indexes/XXWC_AR_CONTACT_POINTS_CNV.sql;
@./Indexes/XXWC_AR_CONTACT_POINTS_CNV_N1.sql;
@./Indexes/XXWC_AR_CONTACT_POINTS_CNV_N2.sql;
@./Indexes/XXWC_AR_CREDIT_COPY_TRX_U1.sql;
@./Indexes/XXWC_AR_CUSTOMERS_CNV_N1.sql;
@./Indexes/XXWC_AR_CUSTOMER_IFACE_STG_PK.sql;
@./Indexes/XXWC_AR_CUST_CONTACT_IFACE__PK.sql;
@./Indexes/XXWC_AR_CUST_SITE_IFACE_STG_PK.sql;
@./Indexes/XXWC_AR_INVOICES_CONV_N1.sql;
@./Indexes/XXWC_AR_INVOICES_CONV_N2.sql;
@./Indexes/XXWC_AR_INVOICES_CONV_N3.sql;
@./Indexes/XXWC_AR_INVOICES_CONV_N4.sql;
@./Indexes/XXWC_AR_PRISM_INV_LN_NOTES__PK.sql;
@./Indexes/XXWC_AR_PRISM_INV_NOTES_TBL_N1.sql;
@./Indexes/XXWC_AR_PRISM_RENTAL_LN_STG_PK.sql;
@./Indexes/XXWC_AR_PRISM_RENTAL_ORD_STGPK.sql;
@./Indexes/XXWC_AR_SITES_CNV_N2.sql;
@./Indexes/XXWC_BATCH_TAXEC_PK.sql;
@./Indexes/XXWC_COLL_CR_ANALYST_XREF_IND1.sql;
@./Indexes/XXWC_CR_CLASSIFICATION_XREF.sql;
@./Indexes/XXWC_CUST_CLASS_XREF_IND.sql;
@./Indexes/XXWC_CUST_PROF_CLASS_XREF_IND1.sql;
@./Indexes/XXWC_INVITEM_STG_N1.sql;
@./Indexes/XXWC_INVITEM_STG_N2.sql;
@./Indexes/XXWC_INVITEM_XREF_STG_N1.sql;
@./Indexes/XXWC_INV_PRODUCT_ORG_N1.sql;
@./Indexes/XXWC_INV_PRODUCT_REQUEST_U1.sql;
@./Indexes/XXWC_MRPRPOP_U1.sql;
@./Indexes/XXWC_ONHAND_LOT_CNV_N1.sql;
@./Indexes/XXWC_PAYMENT_TERMS_XREF.sql;
@./Indexes/XXWC_PO_FREIGHT_BURDEN_TBL_N1.sql;
@./Indexes/XXWC_PO_VENDOR_MINIMUM_U1.sql;
@./Indexes/XXWC_QP_MODIFIER_HDR_CNV_N1.sql;
@./Indexes/XXWC_QP_MODIFIER_LINE_CNV_N1.sql;
@./Indexes/XXWC_REMIT_TO_ADDR_XREF.sql;
@./Indexes/XXWC_SALES_VELOCITY_GT_U.sql;
@./Triggers/XXWC_AR_CUSTOMER_IFACE_STG_T1.trg;
@./Triggers/XXWC_AR_CUST_CONT_IFACE_STG_T1.trg;
@./Triggers/XXWC_AR_CUST_SITE_IFACE_STG_T1.trg;
@./Triggers/XXWC_AR_PRISM_RNTAL_LN_STG_T1.trg;
@./Triggers/XXWC_AR_PRISM_RNTAL_ORD_STG_T1.trg;
@./Triggers/XXWC_AR_PRS_INV_LN_NOTE_STG_T1.trg;
@./Synonym/XXWC_AR_PRISM_INV_LN_NOTES_S.sql;
@./Synonym/XXWC_AR_PRISM_INV_LN_NOTES_STG.sql;
@./Synonym/XXWC_BACKORDERED_TXNS_LNS_STG.sql;
@./Synonym/XXWC_AR_PRISM_RENTAL_ORD_STG.sql;
@./Synonym/XXWC_PO_FREIGHT_BURDEN_TBL.sql;
@./Synonym/XXWC_AR_CUST_SITE_IFACE_STG.sql;
@./Synonym/XXWC_AR_CUST_SITE_IFACE_S.sql;
@./Synonym/XXWC_AR_CUST_CONTACT_IFACE_STG.sql;
@./Synonym/XXWC_AR_CUST_CONTACT_IFACE_S.sql;
@./Synonym/XXWC_AR_CUSTOMER_IFACE_STG.sql;
@./Synonym/XXWC_AR_CUSTOMER_IFACE_S.sql;
@./Synonym/XXWC_AR_INVOICES_CONV.sql;
@./Synonym/XXWC_SR_ASSINGMENTS_CONV.sql;
@./Synonym/XXWC_SOURCING_RULES_CONV.sql;
@./Synonym/XXWC_AP_PAID_INVOICE_STG.sql;
@./Synonym/XXWC_IBY_BANK_ACCOUNTS.sql;
@./Synonym/XXWC_AP_SUP_SITE_CONTACT_INT.sql;
@./Synonym/XXWC_AP_SUPPLIER_SITES_INT.sql;
@./Synonym/XXWC_AP_SUPPLIER_INT.sql;
@./Synonym/XXWC_OM_CASH_REFUND_TBL.sql;
@./Constraints/DW_SALESREPS_NonFK.sql;
@./Constraints/XXWC_AR_CUSTOMER_IFACE_STG_NonFK.sql;
@./Constraints/XXWC_AR_CUST_CONTACT_IFACE_STG_NonFK.sql;
@./Constraints/XXWC_AR_CUST_SITE_IFACE_STG_NonFK.sql;
@./Constraints/XXWC_AR_PRISM_INV_LN_NOTES_STG_NonFK.sql;
@./Constraints/XXWC_AR_PRISM_RENTAL_LN_STG_NonFK.sql;
@./Constraints/XXWC_AR_PRISM_RENTAL_ORD_STG_NonFK.sql;
@./Constraints/XXWC_BATCH_TAXEC_TBL_NonFK.sql;
