CREATE OR REPLACE TRIGGER XXWC.XXWC_AR_CUST_CONT_IFACE_STG_T1
   BEFORE INSERT
   ON XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   L_CUST_CONTACT_IFACE_STG_ID   NUMBER;
/******************************************************************************
   NAME:       XXWC_AR_CUST_CONT_IFACE_STG_T1
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/29/2012             1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     XXWC_AR_CUST_CONT_IFACE_STG_T1
      Sysdate:         3/29/2012
      Date and Time:   3/29/2012, 4:00:25 PM, and 3/29/2012 4:00:25 PM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:      XXWC_AR_CUST_CONTACT_IFACE_STG (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/
BEGIN
   L_CUST_CONTACT_IFACE_STG_ID := 0;

   SELECT XXWC_AR_CUST_CONTACT_IFACE_S.NEXTVAL
     INTO L_CUST_CONTACT_IFACE_STG_ID
     FROM DUAL;

   :NEW.CUST_CONTACT_IFACE_STG_ID := L_CUST_CONTACT_IFACE_STG_ID;

   :NEW.Creation_Date := SYSDATE;
   :NEW.Created_By := 1;
EXCEPTION
   WHEN OTHERS
   THEN
      -- Consider logging the error and then re-raise
      RAISE;
END XXWC_AR_CUST_CONT_IFACE_STG_T1;
/


