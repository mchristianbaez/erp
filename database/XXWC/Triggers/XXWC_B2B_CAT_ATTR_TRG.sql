create or replace trigger "XXWC"."XXWC_B2B_CAT_ATTR_TRG"
  before insert or update on "XXWC"."XXWC_B2B_CAT_ATTR_TBL"
  for each row
/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_CUSTOMER_ATTR_TRG $
  Module Name: XXWC_CUSTOMER_ATTR_TRG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS 20160223-00029
**************************************************************************/
begin
  if inserting then
    if :NEW."ID" is null then
      select "XXWC_B2B_CAT_ATTR_SEQ".nextval into :NEW."ID" from dual;
    end if;
    :NEW.CREATED_ON := SYSDATE;
    :NEW.CREATED_BY := nvl(v('APP_USER'), USER);

  end if;
  if updating then
    :NEW.UPDATED_ON := SYSDATE;
    :NEW.UPDATED_BY := nvl(v('APP_USER'), USER);

  end if;
end;
/