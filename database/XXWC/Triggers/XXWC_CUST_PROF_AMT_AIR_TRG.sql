/*************************************************************************
 *  Copyright (c) 2011 Lucidity Consulting Group
 *  All rights reserved.
 **************************************************************************
 *   $Header XXWC_HZ_CUST_PROF_AMT_AIR_TRG.sql$
 *   Module Name: XXWC_HZ_CUST_PROF_AMT_AIR_TRG.sql
 *
 *   PURPOSE:   This script to create the trigger to assign credit usage rules if created from Customer standard Creation process
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
 *   1.0        09/27/2011  Vivek Lakaman             Initial Version
 *   1.1        05/25/2012  Satish Upadhyayula        Remove Default User Name 
 *   1.2        07/06/2012  Satish Upadhyayula        Trigger Should be fired for transactions that have Cust_Account_ID
 * ************************************************************************/

CREATE OR REPLACE TRIGGER APPS.XXWC_HZ_CUST_PROF_AMT_AIR_TRG
   AFTER INSERT
   ON HZ_CUST_PROFILE_AMTS
   FOR EACH ROW  WHEN (NEW.CUST_ACCOUNT_ID > 0 )
DECLARE
   -- variable declarations
   x_return_status   VARCHAR2 (10);
   x_msg_count       NUMBER;
   x_msg_data        VARCHAR2 (2000);
   l_itemtype        VARCHAR2 (100) := 'XXWCACUR';
   l_itemkey         VARCHAR2 (100);
   l_system_date     VARCHAR2 (10);
   save_threshold    NUMBER;
   v_request_id      NUMBER DEFAULT 0 ;
   result            BOOLEAN;
   l_User_Name       FND_USER.USER_NAME%TYPE ; 
BEGIN
   xxwc_cust_credit_limit_pkg.log_msg (1,'XXWC_HZ_CUST_PROF_AMT_AIR_TRG',  'Begining of Trigger');

   -- 25-MAY-2012 : Satish U:  Added code to derive User Name 
   Begin
     Select User_Name 
     Into l_User_Name
     From Fnd_User 
     Where User_ID = NVL(:NEW.CREATED_BY, FND_GLOBAL.User_ID ) ;
   Exception 
      When Others Then 
          l_User_Name := 'SYSADMIN' ; 
   End;
   -- Commented this If Condition : Satish U: 
   xxwc_cust_credit_limit_pkg.log_msg (1,'XXWC_HZ_CUST_PROF_AMT_AIR_TRG',  'Created By Module =' || :NEW.CREATED_BY_MODULE);

   IF :NEW.CREATED_BY_MODULE <> 'ONT_UI_ADD_CUSTOMER' THEN
      SELECT   TO_CHAR (SYSDATE, 'MMDDYY') INTO l_system_date FROM DUAL;
      
      --- 25-MAY-2012 : Satish U : Made changes to  Item Key Using Primary Key of the table. 
      l_ItemKey := :NEW.CUST_ACCT_PROFILE_AMT_ID  ; 
      
      xxwc_cust_credit_limit_pkg.log_msg (
         1,
         'XXWC_HZ_CUST_PROF_AMT_AIR_TRG',
         '++++++++++++++++++++++++++++++++++'
      );
      xxwc_cust_credit_limit_pkg.log_msg (1,
                                          'XXWC_HZ_CUST_PROF_AMT_AIR_TRG',
                                          'l_itemkey =' || l_itemkey);
      xxwc_cust_credit_limit_pkg.log_msg (1,
                                          'XXWC_HZ_CUST_PROF_AMT_AIR_TRG',
                                          'call create_process');
      --save_threshold := wf_engine.threshold;
      --wf_engine.threshold := -1;

      wf_engine.createprocess (l_itemtype,
                               l_itemkey,
                               'XXWC_ASSIGN_CREDIT_USAGE_PROC');

      wf_engine.setitemattrtext (itemtype   => l_itemtype,
                                 itemkey    => l_itemkey,
                                 aname      => 'XXWC_CUST_ACCT_PROF_AMT_ID',
                                 avalue     => :NEW.CUST_ACCT_PROFILE_AMT_ID);

      wf_engine.setitemattrtext (itemtype   => l_itemtype,
                                 itemkey    => l_itemkey,
                                 aname      => 'XXWC_CUST_ACCOUNT_ID',
                                 avalue     => :NEW.CUST_ACCOUNT_ID);

      wf_engine.setitemattrtext (itemtype   => l_itemtype,
                                 itemkey    => l_itemkey,
                                 aname      => 'XXWC_SITE_USE_ID',
                                 avalue     => :NEW.SITE_USE_ID);

      -- Satish U: 25-MAY-2012 : Removed Hard Coded String 
      wf_engine.setitemowner (itemtype   => l_itemtype,
                              itemkey    => l_itemkey,
                              owner      => l_User_Name); --'VL002096' 

      xxwc_cust_credit_limit_pkg.log_msg (1,
                                          'XXWC_HZ_CUST_PROF_AMT_AIR_TRG',
                                          'call start_process');
      wf_engine.startprocess (l_itemtype, l_itemkey);
   
   END IF;
EXCEPTION
   WHEN OTHERS  THEN
      xxwc_cust_credit_limit_pkg.log_msg (1,
                                          'XXWC_HZ_CUST_PROF_AMT_AIR_TRG',
                                          'Others exception :'||sqlerrm);
      
      NULL;
END;
/
SHOW ERRORS;
COMMIT;
EXIT;
