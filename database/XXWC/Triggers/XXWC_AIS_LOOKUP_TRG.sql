/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC."XXWC_AIS_LOOKUP_TRG" $
  Module Name: XXWC."XXWC_AIS_LOOKUP_TRG"

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-apr-2017   Pahwa, Nancy                Initially Created 
TMS# 20170302-00126
**************************************************************************/
CREATE OR REPLACE TRIGGER  XXWC."XXWC_AIS_LOOKUP_TRG" 
  before insert on XXWC."XXWC_AIS_LOOKUP_TBL"               
  for each row  
begin   
  if :NEW."ID" is null then 
    select XXWC."XXWC_AIS_LOOKUP_TBL_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end; 
ALTER TRIGGER  XXWC."XXWC_AIS_LOOKUP_TRG" ENABLE;
