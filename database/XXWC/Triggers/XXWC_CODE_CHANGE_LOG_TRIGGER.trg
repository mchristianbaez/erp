CREATE OR REPLACE TRIGGER xxwc.xxwc_code_change_log_trigger
   BEFORE CREATE OR ALTER OR DROP
   ON SCHEMA
DECLARE
   sql_text    ora_name_list_t;
   i           PLS_INTEGER;
   sql_text_   CLOB;
BEGIN
   i := sql_txt (sql_text);

   FOR l IN 1 .. i
   LOOP
      sql_text_ := sql_text_ || sql_text (l);
   END LOOP;

   INSERT INTO xxwc.xxwc_code_change_log
      SELECT ora_sysevent
            ,ora_dict_obj_owner
            ,ora_dict_obj_name
            ,sql_text_
            ,SYS_CONTEXT ('USERENV', 'OS_USER')
            ,SYSDATE
        FROM DUAL;
END;
/


