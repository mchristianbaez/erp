CREATE OR REPLACE TRIGGER xxwc.xxwc_ar_prism_cls_ord_stg_t1
   BEFORE INSERT
   ON xxwc.xxwc_ar_prism_cls_ord_summ_stg
   REFERENCING NEW AS new OLD AS old
   FOR EACH ROW
DECLARE
   l_closed_order_stg_id   NUMBER;
/******************************************************************************
   NAME:
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        06/05/2012             1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:
      Sysdate:         3/29/2012
      Date and Time:   3/29/2012, 3:54:27 PM, and 3/29/2012 3:54:27 PM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:      XXWC_AR_CUSTOMER_IFACE_STG_TRG1 (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/
BEGIN
   l_closed_order_stg_id := 0;

   SELECT xxwc_ar_prism_closed_ords_s.NEXTVAL
     INTO l_closed_order_stg_id
     FROM DUAL;

   :new.closed_order_stg_id := l_closed_order_stg_id;

   :new.creation_date := SYSDATE;
   :new.created_by := 1;
EXCEPTION
   WHEN OTHERS
   THEN
      -- Consider logging the error and then re-raise
      RAISE;
END;
/


