/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_BR_CAGE_DETAILS_TRG$
  Module Name: XXWC.XXWC_BR_CAGE_DETAILS_TRG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
   1.0     30-Jun-2017        Pahwa Nancy   TMS# 20170607-00052 
**************************************************************************/
create or replace trigger XXWC."XXWC_BR_CAGE_DETAILS_TRG"
  before insert or update on "XXWC"."XXWC_BR_CAGE_DETAILS_TBL"
  for each row
begin
 if inserting then
  if :NEW."ID" is null then
    select XXWC."XXWC_BR_CAGE_DETAILS_SEQ".nextval into :NEW."ID" from sys.dual;
  end if;
  if :NEW.CHECK_IN_DATE is null then
    :NEW.CHECK_IN_DATE := SYSDATE;
    end if;
      :NEW.CHECKED_IN_BY := nvl(v('APP_USER'), USER);
 end if;

  if updating then
    if :NEW.CHECK_OUT_DATE is null then
    :NEW.CHECK_OUT_DATE := SYSDATE;
    end if;
    :NEW.CHECKED_OUT_BY := nvl(v('APP_USER'), USER);

  end if;
end;
/