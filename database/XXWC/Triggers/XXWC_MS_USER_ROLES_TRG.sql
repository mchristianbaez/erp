create or replace trigger "XXWC"."XXWC_MS_USER_ROLES_TRG"
  before insert or update on "XXWC"."XXWC_MS_USER_ROLES_TBL"
  for each row
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_USER_ROLES_TRG $
  Module Name: XXWC_MS_USER_ROLES_TRG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
declare
  l_journal_notes varchar2(32000);
  l_changed_check varchar2(1) default 'N';
begin
  if inserting then
  if :NEW."USER_ROLE_ID" is null then
    select "XXWC_MS_USER_ROLES_SEQ".nextval into :NEW."USER_ROLE_ID" from dual;
  end if;
   :NEW.CREATED_ON := SYSDATE;
   :NEW.CREATED_BY := nvl(v('APP_USER'),USER);
    l_journal_notes := 'New Role ' || apps.xxwc_ms_master_admin_pkg.get_role_name(:new.role_id)
    || ' assigned to the user ' || apps.xxwc_ms_master_admin_pkg.get_user_name(:new.user_id);
    insert into XXWC_MS_USER_ROLES_JOURNAL_TBL
      (user_roles_id, notes,user_id)
    values
      (:new.USER_ROLE_ID, l_journal_notes,:new.user_id);
  end if;
  if updating then
    :NEW.UPDATED_ON := SYSDATE;
    :NEW.UPDATED_BY := nvl(v('APP_USER'),USER);
     if :NEW.USER_ID != :old.user_id then
      l_journal_notes := l_journal_notes || ' User from: ' ||
                         apps.xxwc_ms_master_admin_pkg.get_user_name(:old.user_id) || ' => '
                         || apps.xxwc_ms_master_admin_pkg.get_user_name(:new.user_id);
      l_changed_check := 'Y';
    end if;
     if :NEW.ROLE_ID != :old.role_id then
      l_journal_notes := l_journal_notes || ' Role from: ' ||
                        apps.xxwc_ms_master_admin_pkg.get_role_name(:old.role_id) || ' => '
                        || apps.xxwc_ms_master_admin_pkg.get_role_name(:new.role_id);
      l_changed_check := 'Y';
    end if;
     if :NEW.OWNER_FLAG != :old.owner_flag then
      l_journal_notes := l_journal_notes || ' Owner Flag from: ' ||
                         :old.owner_flag || ' => ' || :new.owner_flag || ' for Role ' ||
                         apps.xxwc_ms_master_admin_pkg.get_role_name(:old.role_id);
      l_changed_check := 'Y';
    end if;
    if l_changed_check = 'Y' then
     insert into XXWC_MS_USER_ROLES_JOURNAL_TBL
      (user_roles_id, notes,user_id)
    values
      (:new.USER_ROLE_ID, l_journal_notes,:new.user_id);
    end if;
  end if;
 /* if :NEW."APP_ID" is null then
    :NEW.APP_ID := v('APP_ID');
  end if;*/
end;
/