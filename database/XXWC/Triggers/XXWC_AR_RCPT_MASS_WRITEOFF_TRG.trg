create or replace trigger xxwc."XXWC_AR_RCPT_MASS_WRITEOFF_TRG"
  before insert or update on "XXWC"."XXWC_AR_RCPT_MASS_WRITEOFF_TBL"
  for each row
   
declare
     l_sec             VARCHAR2(100);
     l_user_id         NUMBER;

/*************************************************************************
Copyright (c) 2012 HD Supply 
All rights reserved.
**************************************************************************
   $Header XXWC_AR_RCPT_MASS_WRITEOFF_TRG $
  Module Name: XXWC_AR_RCPT_MASS_WRITEOFF_TRG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-APR-2018  Nancy Pahwa  20160218-00198  Initially Created
**************************************************************************/
begin
  /* if :NEW."BATCH_ID" is null then
    select "XXWC_AR_RCPT_MASS_WRITEOFF_SEQ".nextval into :NEW."BATCH_ID" from sys.dual;
  end if;*/
    l_sec := 'Derive User-Id';
  BEGIN
    SELECT user_id
      INTO l_user_id
      FROM apps.fnd_user
     WHERE user_name = NVL(v('APP_USER'), USER);
  EXCEPTION
    WHEN OTHERS THEN
      l_sec := 'Error deriving User-Id';
  END;
  if inserting then
    if :new.batch_id is null then
      :new.batch_id := TO_CHAR(SYSDATE, 'MMDDYYYYHHMISS');
    end if;
    :NEW.CREATION_DATE := SYSDATE;
    :NEW.CREATED_BY    := l_user_id;
    :NEW.LAST_UPDATE_DATE := SYSDATE;
    :NEW.LAST_UPDATED_BY  := l_user_id;
    :NEW.LAST_UPDATE_LOGIN := l_user_id;
    :new.status :='NEW';
  end if;
  if updating then
    if :old.status = 'ERROR' then 
      if
       :old.customer_number != :new.customer_number or
       :old.receipt_number != :new.receipt_number or
       :old.receipt_date != :new.receipt_date or
       :old.receipt_amount != :new.receipt_amount or
       :old.activity_name != :new.activity_name or
       :old.writeoff_amount != :new.writeoff_amount or
       :old.comments != :new.comments then
      :new.status := 'NEW';
      else 
        null;
    end if;
    end if;
    :NEW.LAST_UPDATE_DATE := SYSDATE;
    :NEW.LAST_UPDATED_BY  := l_user_id;
    :NEW.LAST_UPDATE_LOGIN := l_user_id;
  end if;
end;
/
