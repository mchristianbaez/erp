CREATE OR REPLACE TRIGGER XXWC.XXWC_AR_CUSTOMER_IFACE_STG_T1
   BEFORE INSERT
   ON XXWC.XXWC_AR_CUSTOMER_IFACE_STG
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   Customer_Iface_Stg_ID   NUMBER;
/******************************************************************************
   NAME:
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/29/2012             1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:
      Sysdate:         3/29/2012
      Date and Time:   3/29/2012, 3:54:27 PM, and 3/29/2012 3:54:27 PM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:      XXWC_AR_CUSTOMER_IFACE_STG_TRG1 (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/
BEGIN
   Customer_Iface_Stg_ID := 0;

   SELECT XXWC_AR_CUSTOMER_IFACE_S.NEXTVAL
     INTO Customer_Iface_Stg_ID
     FROM DUAL;

   :NEW.CUSTOMER_IFACE_STG_ID := Customer_Iface_Stg_ID;

   :NEW.CREATION_DATE := SYSDATE;
   :NEW.CREATED_BY := 1;
EXCEPTION
   WHEN OTHERS
   THEN
      -- Consider logging the error and then re-raise
      RAISE;
END;
/


