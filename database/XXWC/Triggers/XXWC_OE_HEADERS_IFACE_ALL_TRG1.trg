CREATE OR REPLACE TRIGGER APPS.XXWC_OE_HEADERS_IFACE_ALL_TRG1
BEFORE INSERT
ON ONT.OE_HEADERS_IFACE_ALL
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
WHEN (
NEW.ORDER_SOURCE_ID = 10
      )
DECLARE

/******************************************************************************
   NAME:       XXWC_OE_HEADERS_IFACE_ALL_TRG1
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author            Description
   ---------  ----------  ---------------   ------------------------------------
   1.0        8/9/2012    Consuelo Gonzalez 1. Initial definition to default ship
                                            from org id on the order header
   1.1        8/15/2012   Lee Spitzer       2. Updated orig_sys_document_ref 

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     XXWC_OE_HEADERS_IFACE_ALL_TRG1
      Sysdate:         8/9/2012
      Date and Time:   8/9/2012, 3:29:53 PM, and 8/9/2012 3:29:53 PM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:      OE_HEADERS_IFACE_ALL (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/

    l_ship_from_org_id  NUMBER;

BEGIN
    l_ship_from_org_id := null;
                                              
    begin
        SELECT     distinct(req_ln.source_organization_id)  
         INTO   l_ship_from_org_id
         FROM po_requisition_headers_all req_hdr,
              po_requisition_lines_all req_ln,
              mtl_parameters mp_from,
              org_organization_definitions ood_from,
              mtl_parameters mp_to,
              org_organization_definitions ood_to
        WHERE req_hdr.type_lookup_code = 'INTERNAL'
          --AND req_hdr.segment1 = :new.orig_sys_document_ref removed 8/15/2012
          AND req_hdr.requisition_header_id = :new.orig_sys_document_ref --added -8/15/2012
          AND req_hdr.requisition_header_id = req_ln.requisition_header_id
          --AND req_ln.requisition_header_id = :source_document_id
          AND req_ln.source_organization_id = mp_from.organization_id
          AND mp_from.organization_id = ood_from.organization_id
          AND req_ln.destination_organization_id = mp_to.organization_id
          AND mp_to.organization_id = ood_to.organization_id     
          AND rownum = 1;
    exception
    when others then
        l_ship_from_org_id := null;
    end;    
   
   :New.ship_from_org_id := l_ship_from_org_id;

   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       null;
END XXWC_OE_HEADERS_IFACE_ALL_TRG1;
/

