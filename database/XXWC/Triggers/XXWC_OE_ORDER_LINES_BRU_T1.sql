CREATE OR REPLACE TRIGGER XXWC_OE_ORDER_LINES_BRU_T1
BEFORE UPDATE ON OE_ORDER_LINES_ALL
FOR EACH ROW
WHEN (
NEW.LINE_CATEGORY_CODE   ='ORDER'
      )
DECLARE
  -- Satish U : 15-DEC-2011 Pricing GuardRails Variables
  --
  l_Return_Status       Varchar2(1) ; 
  l_Return_Message      Varchar2(2000) ; 
  l_Header_ID           Number ; 
  l_Line_Id             Number; 
  l_Unit_Selling_Price  Number ; 
  l_Ship_From_Org_ID    Number ; 
  l_Pricing_Date        Date; 
  l_Pricing_Date_Char   Varchar2(30); 
  l_Flow_Status_COde    Varchar2(30) ; 
  l_Unit_Cost           Number; 
  l_Unit_List_Price     Number; 
  l_Order_Type_ID       Number ; 
  l_Inventory_Item_ID   Number; 
  l_Org_ID              Number; 
  l_Module_Name         Varchar2(80) := 'XXWC.DB_TRIGGER.OM.OE_ORDER_LINES_ALL'; 
  c_level_unexpected        CONSTANT NUMBER := 6;
  c_level_error             CONSTANT NUMBER := 5;
  c_level_exception         CONSTANT NUMBER := 4;
  c_level_event             CONSTANT NUMBER := 3;
  c_level_procedure         CONSTANT NUMBER := 2;
  c_level_statement         CONSTANT NUMBER := 1;
  l_Line_Number Varchar2(10); 
  l_Manual_Adj_Exists     Varchar2(1) := 'Y'; 
  l_Auto_Adj_Exists       Varchar2(1) := 'Y'; 
  
  Exp_PGR_Violation Exception   ; 
  Pragma Exception_init (Exp_PGR_Violation, -20001 );

  
BEGIN
    IF :NEW.LINE_CATEGORY_CODE   ='ORDER' 
      AND :NEW.UNIT_LIST_PRICE  IS NOT NULL 
      AND :NEW.UNIT_SELLING_PRICE IS NOT NULL
      AND :NEW.FLOW_STATUS_CODE  <> 'CLOSED' THEN 
      XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => 1,
                                  p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '10 Beging of API');
       l_Inventory_Item_ID  := :NEW.INVENTORY_ITEM_ID ; 
       l_Line_ID            := :NEW.LINE_ID  ;
       l_Unit_Selling_Price := :NEW.UNIT_SELLING_PRICE ; 
       l_Ship_From_Org_ID   := :NEW.SHIP_FROM_ORG_ID  ; 
       
       l_Pricing_Date_Char  := TO_CHAR(:NEW.PRICING_DATE, 'DD-MON-RRRR') ; 
       l_Pricing_Date       := TO_DATE( l_Pricing_Date_Char, 'DD-MON-RRRR') ; 
       l_Header_ID          := :NEW.HEADER_ID ; 
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                  p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '11 Flow Status Code from Public API' || OE_ORDER_PUB.G_LINE.flow_status_code);
       -- Satish U: 04-APR-2012 : Should be Old Flow Status Code and not new. 
       --l_Flow_Status_Code   := :NEW.FLOW_STATUS_CODE ; 
       
       l_Flow_Status_Code   := :OLD.FLOW_STATUS_CODE ; 
       l_Unit_Cost          := :NEW.UNIT_COST; 
       l_Unit_List_Price    := :NEW.UNIT_LIST_PRICE ; 
       l_Line_Number        := :NEW.LINE_NUMBER || '.' || :NEW.SHIPMENT_NUMBER; 
       l_Org_ID             := :NEW.ORG_ID ; 
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                  p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '20 Getting Order Type ID');
      
       Select Order_Type_ID
       INTO l_Order_Type_ID 
       From OE_ORDER_HEADERS_ALL
       WHERE Header_ID = :NEW.HEADER_ID ; 
       
       
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                   p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '25.1 Attribute20 is : '|| :NEW.ATTRIBUTE20); 
                                   
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                   p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '25.2 OLD Unit Selling Price is : '|| :OLD.UNIT_SELLING_PRICE); 
        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                   p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '25.3 NEW Unit Selling Price is : '|| :NEW.UNIT_SELLING_PRICE);
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                   p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '25.4 Price Date in Char : '|| l_Pricing_Date_Char);
       
       
       Begin 
          Select 'Y' 
          Into  l_Manual_Adj_Exists 
          From Oe_Price_Adjustments
          Where ( Line_ID = :NEW.Line_ID OR 
                (Line_ID IS NULL AND HEADER_ID = :NEW.HEADER_ID ) )
          And   NVL(Automatic_Flag,'N')  = 'N' 
          And  List_Line_Type_Code in ('DIS') 
          And Rownum = 1; 
       Exception 
          When Others Then 
             l_Manual_Adj_Exists := 'N'; 
       End ;
       
       Begin 
          Select 'Y' 
          Into  l_Auto_Adj_Exists 
          From Oe_Price_Adjustments
          Where ( Line_ID = :NEW.Line_ID OR 
                (Line_ID IS NULL AND HEADER_ID = :NEW.HEADER_ID ) )
          And   NVL(Automatic_Flag,'N')  = 'Y' 
          And  List_Line_Type_Code in ('DIS') 
          And Rownum = 1; 
       Exception 
          When Others Then 
             l_Auto_Adj_Exists := 'N'; 
       End ;
       
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                   p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '25.5 Manual Adjustment Flag Value : ' || l_Manual_Adj_Exists); 
       IF :NEW.ATTRIBUTE20 IS NULL 
           AND NVL(:OLD.UNIT_SELLING_PRICE,0) <> NVL(:NEW.UNIT_SELLING_PRICE,0) THEN 
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                   p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '26.0 Inside the ifstatement   Pricing Guard Rail API'); 
                                   
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                   p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '26.1 Line Id :' || l_line_Id); 
           
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                   p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '26.2 Line Id :' || l_Header_ID); 
           
                                                          
           XXWC_OM_PRICING_GUARDRAIL_PKG.DBTRigger_PGR_Check(l_line_Id,
                                                     l_Inventory_Item_ID,
                                                     l_Unit_Selling_Price,
                                                     l_Ship_From_Org_ID,
                                                     l_Pricing_Date,
                                                     l_Header_ID,
                                                     l_Flow_Status_Code,
                                                     l_Unit_Cost,
                                                     l_Unit_List_Price,
                                                     l_Order_Type_ID,
                                                     l_Line_Number,
                                                     l_Org_ID,
                                                     l_Manual_Adj_Exists,
                                                     l_Auto_Adj_Exists,
                                                     l_Return_Status,
                                                     l_Return_Message);
          
          -- Inserting Into Global Temp Table SatishU : 10/17/2011 
          xxwc_om_pricing_guardrail_pkg.Update_Status(p_Header_ID       => l_Header_ID , 
                     P_Line_ID         => l_Line_ID,  
                     p_Return_Status   => l_Return_Status, 
                     p_Return_Message  => l_Return_Message)  ;
          --******************/
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                       p_mod_name      => l_Module_Name,
                                       P_DEBUG_MSG     => '30 After the Call to Pricing Guard Rail API');  
            
            
             
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                       p_mod_name      => l_Module_Name,
                                       P_DEBUG_MSG     => '40 Return Status is : '|| l_Return_Status );  
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                       p_mod_name      => l_Module_Name,
                                       P_DEBUG_MSG     => '41 Return Message is : '|| l_Return_Message );  
            
            
            IF l_Return_Status IS NOT NULL AND l_Return_Status = 'S' AND l_Return_Message is Not NULL Then 
               NULL;
                
            ELSIF l_Return_Status IS NOT NULL AND l_Return_Status <> 'S' AND l_Return_Message is Not NULL Then
               XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                       p_mod_name      => l_Module_Name,
                                       P_DEBUG_MSG     => '42 Updating Error Message : '|| l_Return_Message );  
                                   
               xxwc_om_pricing_guardrail_pkg.Update_Message(
                     p_Header_ID       => l_Header_ID , 
                     P_Line_ID         => l_Line_ID,  
                     p_Return_Status   => l_Return_Status, 
                     p_Return_Message  => l_Return_Message)  ;
               XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                       p_mod_name      => l_Module_Name,
                                       P_DEBUG_MSG     => '50 Raising Application Error');  
               
               Raise Exp_PGR_Violation ; 
            END If;
       END IF; 
    END IF;  -- Line_Category_Code 
    :NEW.ATTRIBUTE20 := NULL;
     
EXCEPTION 
   When Exp_PGR_Violation  Then 
      xxwc_om_pricing_guardrail_pkg.Update_Status(p_Header_ID       => l_Header_ID , 
                     P_Line_ID         => l_Line_ID,  
                     p_Return_Status   => l_Return_Status, 
                     p_Return_Message  => l_Return_Message)  ;
      RAISE_APPLICATION_ERROR(-20001, 'Encountered error in trigger XXWC_OE_ORDER_LINES_ARU_T1' || substr(sqlerrm,1,400));
   WHEN OTHERS THEN 
      l_Return_Status := 'E' ; 
      l_Return_Message := l_Return_Message || 'When Others Exception' ; 
      XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => C_LEVEL_PROCEDURE,
                                   p_mod_name      => l_Module_Name,
                                   P_DEBUG_MSG     => '60 When Others Trigger Raising Application Error');  
                                   
      xxwc_om_pricing_guardrail_pkg.Update_Message(
                     p_Header_ID       => l_Header_ID , 
                     P_Line_ID         => l_Line_ID,  
                     p_Return_Status   => l_Return_Status, 
                     p_Return_Message  => l_Return_Message)  ;
      RAISE_APPLICATION_ERROR(-20002, 'Encountered error in trigger XXWC_OE_ORDER_LINES_ARU_T1' || substr(sqlerrm,1,400));
END;
/

