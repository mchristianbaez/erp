CREATE OR REPLACE TRIGGER XXWC.XXWC_MS_ROLE_RESP2_TRG
AFTER DELETE
   ON "XXWC"."XXWC_MS_ROLE_RESP_TBL"
   FOR EACH ROW
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MS_ROLE_RESP2_TRG $
  Module Name: XXXWC_MS_ROLE_RESP2_TRG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
declare
  l_journal_notes varchar2(32000);
  l_changed_check varchar2(1) default 'N';
begin
   if deleting then

     l_journal_notes := null;

      l_journal_notes := l_journal_notes || ' Deleted the mapping of Responsibility: ' ||
                         apps.xxwc_ms_master_admin_pkg.get_responsibility_name(:old.responsibility_id,:old.app_id)
                         || ' to Role: ' ||
                         apps.xxwc_ms_master_admin_pkg.get_role_name(:old.role_id) ;
      l_changed_check := 'Y';

     if l_changed_check = 'Y' then
      insert into XXWC_MS_ROLES_RESP_JOURNAL_TBL
      (ROLES_RESP_ID, notes,app_id)
    values
      (:old.ROLE_RESP_ID, l_journal_notes,:old.app_id);
    end if;
  end if;
end;
/