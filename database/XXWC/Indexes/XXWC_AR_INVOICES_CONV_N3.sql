--
-- XXWC_AR_INVOICES_CONV_N3  (Index) 
--
CREATE INDEX XXWC.XXWC_AR_INVOICES_CONV_N3 ON XXWC.XXWC_AR_INVOICES_CONV
(ORIG_SYSTEM_SOLD_CUSTOMER_REF)
TABLESPACE XXWC_DATA;


