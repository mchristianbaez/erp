 /********************************************************************************
  FILE NAME: XXWC.XXWC_INVITEM_STG_N3.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE:  XXWC.XXWC_INVITEM_STG_N3 Indexes script
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     08/23/2018    Nancy Pahwa TMS 20180817-00025  Initial Version.
  *******************************************************************************************/
drop index XXWC.XXWC_INVITEM_STG_N2;
create index XXWC.XXWC_INVITEM_STG_N3 on XXWC.XXWC_INVITEM_STG (UPPER(AHH_PARTNUMBER), STATUS);