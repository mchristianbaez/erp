/**************************************************************************
File Name:XXWC_OM_DMS_CHANGE_ARCHIVE_N1.sql
TYPE:     Index
Description: Index On columns HEADER_ID, DELIVERY_ID IN XXWC.XXWC_OM_DMS_CHANGE_ARCHIVE_TBL Table

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ---------------------------------
1.0     11-FEB-2015   Gopi Damuluri   Initial version TMS# 20150128-00043
**************************************************************************/
CREATE INDEX XXWC.XXWC_OM_DMS_CHANGE_ARCHIVE_N1 ON XXWC.XXWC_OM_DMS_CHANGE_ARCHIVE_TBL(HEADER_ID, DELIVERY_ID);