/********************************************************************************
   $Header XXWC_AR_CMGT_CREDIT_REQ_N1.sql $
   Module Name: XXWC_AR_CMGT_CREDIT_REQ_N1

   PURPOSE:   This index to improve performance when quering the data from AR_CMGT_CREDIT_REQUESTS table

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        05/17/2016  Rakesh Patel            TMS# TMS#20160511-00208
********************************************************************************/
CREATE INDEX XXWC.XXWC_AR_CMGT_CREDIT_REQ_N1 ON AR_CMGT_CREDIT_REQUESTS (SOURCE_COLUMN1, SOURCE_COLUMN3, REVIEW_TYPE);