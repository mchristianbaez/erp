/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_CUSTOMER_CATALOG_N2$
  Module Name: XXWC.XXWC_CUSTOMER_CATALOG_N2
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029 
**************************************************************************/
-- Create/Recreate indexes 
create index XXWC.XXWC_CUSTOMER_CATALOG_N2 on XXWC.XXWC_B2B_CAT_CUSTOMER_TBL (CATALOG_GROUP_ID)
  tablespace XXWC_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );