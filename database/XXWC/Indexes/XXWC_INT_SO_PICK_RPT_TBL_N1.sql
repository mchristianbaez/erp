--
-- XXWC_INT_SO_PICK_RPT_TBL_N1  (Index) 
--
CREATE INDEX XXWC.XXWC_INT_SO_PICK_RPT_TBL_N1 ON XXWC.XXWC_INTERNAL_SO_PICK_RPT_TBL
(REQUISITION_HEADER_ID, REQUISITION_LINE_ID, HEADER_ID, LINE_ID)
TABLESPACE XXWC_DATA;


