CREATE INDEX XXWC.XXWC_BPA_PRICE_ZONE_BREAKS_N1
 ON XXWC.XXWC_BPA_PRICE_ZONE_BREAKS_TBL(po_header_id, inventory_item_id, price_zone, price_zone_quantity);