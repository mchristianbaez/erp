/*************************************************************************
  $Header XXWC_QP_MODIFIERS_STG_N3$
  Module Name: XXWC_QP_MODIFIERS_STG_N3.sql

  PURPOSE: Index on XXWC.XXWC_QP_MODIFIERS_STG (REQUEST_ID)

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------        -------------------------
  1.0        10/25/2016  Gopi Damuluri          TMS# 20160922-00037 - Matrix Pricing revert to Standard functionality
**************************************************************************/

CREATE INDEX XXWC.XXWC_QP_MODIFIERS_STG_N3 ON XXWC.XXWC_QP_MODIFIERS_STG (REQUEST_ID);
/
