drop index XXWC.XXWC_PO_VENDOR_MINIMUM_U1;

create unique index XXWC.XXWC_PO_VENDOR_MINIMUM_U1 on XXWC.XXWC_PO_VENDOR_MINIMUM
    (vendor_id, vendor_site_id, organization_id, vendor_contact_id);

