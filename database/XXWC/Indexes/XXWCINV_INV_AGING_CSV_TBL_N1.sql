   /****************************************************************************************************************************************
     $Header XXWCINV_INV_AGING_CSV_TBL_N1
     File Name: XXWCINV_INV_AGING_CSV_TBL_N1.sql

     PURPOSE:   

     REVISIONS:
     Ver        Date         Author             Description
     ---------  ----------   ---------------    ------------------------------------------------------------------------------------
     1.0        04-JUL-2016  P.Vamshidhar      TMS #20170416-00006 - XXWC Inventory Aging Report - CSV Output
   ***************************************************************************************************************************************/

CREATE INDEX XXWC.XXWCINV_INV_AGING_CSV_TBL_N1
   ON XXWC.XXWCINV_INV_AGING_CSV_TBL (ITEM_ID,TO_ORGANIZATION_ID)
/