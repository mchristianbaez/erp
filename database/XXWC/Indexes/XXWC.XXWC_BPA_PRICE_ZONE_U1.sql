CREATE UNIQUE INDEX XXWC.XXWC_BPA_PRICE_ZONE_U1
  ON XXWC.XXWC_BPA_PRICE_ZONE_TBL (po_header_id, inventory_item_id, price_zone);