   /***************************************************************************************************************************
   -- File Name: XXWC_BPA_PROMO_TBL_N1.sql
   --
   -- PURPOSE: Index on XXWC_BPA_PROMO_TBL to improve performance.
   -- HISTORY
   -- =========================================================================================================================
   -- =========================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -----------------------------------------------------------------------------------
   -- 1.0     01-Apr-2015   P.Vamshidhar    Initial version.
   --                                       TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
   --                                       Index created to improve performance of the query from BPA Price zone form package.           
   ***************************************************************************************************************************/
CREATE INDEX XXWC.XXWC_BPA_PROMO_TBL_N1
   ON XXWC.XXWC_BPA_PROMO_TBL (PO_HEADER_ID, INVENTORY_ITEM_ID, ORG_ID);   
