DROP INDEX AR.XXWC_HZ_GEOGRAPHIES_N11;

CREATE INDEX AR.XXWC_HZ_GEOGRAPHIES_N11 ON AR.HZ_GEOGRAPHIES
(GEOGRAPHY_ELEMENT4, GEOGRAPHY_ELEMENT5)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
