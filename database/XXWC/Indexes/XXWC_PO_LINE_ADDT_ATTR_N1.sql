   /****************************************************************************************************************************************
     $Header XXWC_PO_LINE_ADDT_ATTR_N1
     File Name: XXWC_PO_LINE_ADDT_ATTR_N1.sql

     PURPOSE:   

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------------------------------------------------------------------
     1.0         17/12/2015 P.Vamshidhar            TMS#20151217-00113 Stock status showing null in OBIEE for some of the Purchase orders.
   ***************************************************************************************************************************************/

CREATE INDEX XXWC.XXWC_PO_LINE_ADDT_ATTR_N1
   ON XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T (LINE_LOCATION_ID);