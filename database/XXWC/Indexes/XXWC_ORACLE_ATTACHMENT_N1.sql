  /*******************************************************************************
  Name: "XXWC"."XXWC_ORACLE_ATTACHMENT_N1"  
  Description: "XXWC"."XXWC_ORACLE_ATTACHMENT_TBL indexes" 
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-JUL-2017        Pahwa Nancy   Task ID: 20170616-00237
  ********************************************************************************/
-- Create Indexes
  CREATE INDEX "XXWC"."XXWC_ORACLE_ATTACHMENT_N1" ON "XXWC"."XXWC_ORACLE_ATTACHMENT_TBL" ("SEQ_NUM");