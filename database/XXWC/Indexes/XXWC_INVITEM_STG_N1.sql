--
-- XXWC_INVITEM_STG_N1  (Index) 
--
CREATE INDEX XXWC.XXWC_INVITEM_STG_N1 ON XXWC.XXWC_INVITEM_STG
(PARTNUMBER, VENDORPARTNUMBER)
TABLESPACE XXWC_DATA;


