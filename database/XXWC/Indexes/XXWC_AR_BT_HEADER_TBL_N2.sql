/*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header XXWC_AR_BT_HEADER_TBL_N2
     Module Name: XXWC_AR_BT_HEADER_TBL_N2

     PURPOSE:   

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0         10/02/2015  Maharajan Shunmugam    TMS#20150805-00039 Billtrust - Improvements to BT extract logic
   **************************************************************************/
CREATE INDEX XXWC.XXWC_AR_BT_HEADER_TBL_N2 ON XXWC.XXWC_AR_BT_HEADER_TBL
(CUST_PAYMENT_TERM);