/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_WC_USER_ROLES_UK $
  Module Name: XXWC_WC_USER_ROLES_UK

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-AUG-2015   Pahwa, Nancy                Initially Created 
TMS# 20150901-00129   
**************************************************************************/
create unique index XXWC_WC_USER_ROLES_UK on XXWC.XXWC_MS_USER_ROLES_TBL (USER_ID, ROLE_ID)
  tablespace XXWC_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );