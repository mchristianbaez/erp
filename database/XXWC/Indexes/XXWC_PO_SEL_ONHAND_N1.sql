   /*************************************************************************
      $Header XXWC_PO_SEL_ONHAND_N1 $
      Module Name: XXWC_PO_SEL_ONHAND_N1.sql
   
      PURPOSE:   Table used for loading on-hand information
   
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        02/12/2015  Gopi Damuluri           TMS# 20150209-00077 Initial Version
   ****************************************************************************/
CREATE INDEX XXWC.XXWC_PO_SEL_ONHAND_N1 ON XXWC.XXWC_PO_SEL_ONHAND_TBL(INVENTORY_ITEM_ID, ORGANIZATION_ID);