/******************************************************************************
   NAME:       XXWC_LINE_PICK_IDX.sql
   PURPOSE:    sequence

   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        02/05/2017  Niraj K Ranjan   Initial Version TMS#20160815-00078   
                                           Branch Visit - Pick Ticket Reprint Improvements
******************************************************************************/
CREATE INDEX xxwc.xxwc_line_pick_n1 on xxwc.xxwc_om_so_line_pick_aud_tbl(header_id,line_id);
CREATE INDEX xxwc.xxwc_line_pick_n2 on xxwc.xxwc_om_so_line_pick_aud_tbl(hdr_sequence_id);