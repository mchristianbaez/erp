/********************************************************************************
   $Header XXWC_QP_LIST_HEADERS_B_N1.sql $
   Module Name: XXWC_QP_LIST_HEADERS_B_N1

   PURPOSE:   This index to improve performance when quering the data from OE_PRICE_ADJUSTMENTS table

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        06/14/2016  Rakesh Patel            TMS-20150122-00027/20160614-00317 Need to add a condition to the Price Type attribute
********************************************************************************/
CREATE INDEX XXWC.XXWC_QP_LIST_HEADERS_B_N1 ON QP.QP_LIST_HEADERS_B
(LIST_HEADER_ID, 
TO_NUMBER(attribute14),
active_flag);

