/*************************************************************************
  $Header XXWC_OM_CONT_PRILINE_END_IDX.sql $
  Module Name: TMS#20161103-00137   Creating Index on XXWC.XXWC_OM_CONTRACT_PRICING_LINES - START and END DATES

  PURPOSE: Data Fix to activate user

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-NOV-2016  Niraj                 TMS#20161103-00137

**************************************************************************/ 
CREATE INDEX XXWC.XXWC_OM_CONT_PRILINE_END_IDX ON XXWC.XXWC_OM_CONTRACT_PRICING_LINES (TRUNC(END_DATE),1);