   /*************************************************************************
      $Header XXWC_PO_SEL_ONHAND_N2 $
      Module Name: XXWC_PO_SEL_ONHAND_N2.sql
   
      PURPOSE:   Table used for loading on-hand information
   
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        02/12/2015  Gopi Damuluri           TMS# 20150209-00077 Initial Version
   ****************************************************************************/
CREATE INDEX XXWC.XXWC_PO_SEL_ONHAND_N2 ON XXWC.XXWC_PO_SEL_ONHAND_TBL(ORGANIZATION_ID);