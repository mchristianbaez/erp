/*******************************************************************************************************
  -- Script Name: XXWC_AHH_OE_AIS_LPP_TBL_IDX
  -- ***************************************************************************************************
  --
  -- PURPOSE: Indexes for table xxwc.XXWC_AHH_OE_AIS_LPP_TBL
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     21-Sep-2018   Niraj K Ranjan   TMS#20180926-00003   AHH LLP price upload
********************************************************************************************************/
CREATE INDEX XXWC.XXWC_AHH_AIS_LPP_TBL_N1 ON XXWC.XXWC_AHH_OE_AIS_LPP_TBL
(INVENTORY_ITEM_ID, SHIP_TO_ORG_ID);

CREATE INDEX XXWC.XXWC_AHH_AIS_LPP_TBL_N2 ON XXWC.XXWC_AHH_OE_AIS_LPP_TBL
(INVENTORY_ITEM_ID, CUST_ACCOUNT_ID);

CREATE INDEX XXWC.XXWC_AHH_AIS_LPP_TBL_N3 ON XXWC.XXWC_AHH_OE_AIS_LPP_TBL
(INVENTORY_ITEM_ID, SHIP_TO_ORG_ID, CUST_ACCOUNT_ID);

CREATE INDEX XXWC.XXWC_AHH_AIS_LPP_TBL_N4 ON XXWC.XXWC_AHH_OE_AIS_LPP_TBL
(AHH_PART_NBR);

CREATE INDEX XXWC.XXWC_AHH_AIS_LPP_TBL_N5 ON XXWC.XXWC_AHH_OE_AIS_LPP_TBL
(AHH_CUSTOMER_NBR);

CREATE INDEX XXWC.XXWC_AHH_AIS_LPP_TBL_N6 ON XXWC.XXWC_AHH_OE_AIS_LPP_TBL
(AHH_SITE_NBR);