  /********************************************************************************
  FILE NAME: XXWC_MTL_ITEM_CATEGORIES_INTERFACE_IDX.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE:  MTL_ITEM_CATEGORIES_INTERFACE Indexes script
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- CREATE INDEXES
CREATE INDEX XXWC.XXWC_MTL_ITEM_CATS_INTF_N1 ON INV.MTL_ITEM_CATEGORIES_INTERFACE (ITEM_NUMBER, PROCESS_FLAG, ORGANIZATION_CODE, CATEGORY_SET_NAME);