--
-- XXWC_INVITEM_STG_N2  (Index) 
--
CREATE INDEX XXWC.XXWC_INVITEM_STG_N2 ON XXWC.XXWC_INVITEM_STG
(PARTNUMBER, ORGANIZATION_CODE)
TABLESPACE XXWC_DATA;


