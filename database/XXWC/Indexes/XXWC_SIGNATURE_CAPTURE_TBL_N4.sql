  /******************************************************************************
   NAME:     XXWC_SIGNATURE_CAPTURE_TBL_N4
  
   PURPOSE:  Index on XXWC_SIGNATURE_CAPTURE_TBL.ID, GROUP_ID
  
  
   REVISIONS:
   Ver        Date        Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        20-Feb-2015  Gopi Damuluri    Initial Version.
                                            TMS 20150220-00107 - Move DMS Functionality to APEX on EBS
  ******************************************************************************/
CREATE INDEX XXWC.XXWC_SIGNATURE_CAPTURE_TBL_N4 ON XXWC.XXWC_SIGNATURE_CAPTURE_TBL (ID, GROUP_ID);