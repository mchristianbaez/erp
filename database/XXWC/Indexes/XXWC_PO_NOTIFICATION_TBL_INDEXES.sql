/*************************************************************************
      Script : XXWC_PO_NOTIFICATION_TBL_INDEXES.sql

      PURPOSE:   To create indexes for table XXWC_PO_NOTIFICATION_TBL

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        17/10/2017  Niraj K Ranjan          TMS#20170712-00193   Late PO notification
 ****************************************************************************/
 CREATE INDEX XXWC.XXWC_PO_NTF_N1 ON XXWC.XXWC_PO_NOTIFICATION_TBL(PO_HEADER_ID);
 CREATE INDEX XXWC.XXWC_PO_NTF_N2 ON XXWC.XXWC_PO_NOTIFICATION_TBL(EMAIL_SENT_DATE);
 CREATE INDEX XXWC.XXWC_PO_NTF_N3 ON XXWC.XXWC_PO_NOTIFICATION_TBL(TYPE);