/********************************************************************************
   $Header XXWC_QP_ECOMMERCE_ITEMS_MV_N1.sql $
   Module Name: XXWC_QP_ECOMMERCE_ITEMS_MV_N1

   PURPOSE:   This index to improve performance when quering the data from XXWC_QP_ECOMMERCE_ITEMS_MV_TBL table

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        12/07/2017  Rakesh Patel            TMS# 20171207-00166 
********************************************************************************/
DROP INDEX XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_N1;

CREATE INDEX XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_N1 ON XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL
(TO_CHAR(INVENTORY_ITEM_ID)
);

