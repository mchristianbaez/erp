 /**********************************************************************************
  -- Index Script: XXWC_OM_ORDERS_DATAFIX_LOG_N2.sql
  -- *******************************************************************************
  --
  -- PURPOSE: Index for table XXWC.XXWC_OM_ORDERS_DATAFIX_LOG_TBL
  -- HISTORY
  -- ===============================================================================
  -- ===============================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -----------------------------------------
  -- 1.0     22-Sep-2016   Niraj K Ranjan   Initial Version - TMS#20160911-00002   Data fix scripts into Concurrent program
************************************************************************************/
CREATE INDEX XXWC.XXWC_OM_ORDERS_DATAFIX_LOG_N2  ON XXWC.XXWC_OM_ORDERS_DATAFIX_LOG_TBL (order_line_id);
/
