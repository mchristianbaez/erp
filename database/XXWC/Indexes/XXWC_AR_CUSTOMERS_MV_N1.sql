/**************************************************************************
File Name:XXWC_AR_CUSTOMERS_MV_N1.sql
TYPE:     Index
Description: Index On columns CUST_ACCOUNT_CODE , SITE_USE_ID, SITE_USE_CODE IN XXWC.XXWC_AR_CUSTOMERS_MV_TBL Table

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ---------------------------------
1.0     11-FEB-2015   Gopi Damuluri   Initial version TMS# 20141212-00194
**************************************************************************/
CREATE INDEX XXWC.XXWC_AR_CUSTOMERS_MV_N1 ON XXWC.XXWC_AR_CUSTOMERS_MV_TBL (CUST_ACCOUNT_CODE, SITE_USE_ID, SITE_USE_CODE);