--
-- XXWC_AR_PRISM_LN_NOTES_STG_N1  (Index) 
--
CREATE INDEX XXWC.XXWC_AR_PRISM_LN_NOTES_STG_N1 ON XXWC.XXWC_AR_PRISM_INV_LN_NOTES_STG
(TRX_NUMBER, LINE_NO, LINE_ORDER, PRINT)
TABLESPACE XXWC_DATA;


