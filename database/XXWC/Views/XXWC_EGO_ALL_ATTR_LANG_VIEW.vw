CREATE OR REPLACE FORCE VIEW xxwc.xxwc_ego_all_attr_lang_view
(
   item_number
  ,attr_group_disp_name
  ,attr_display_name
  ,multi_row_code
  ,extension_id
  ,organization_id
  ,inventory_item_id
  ,revision_id
  ,last_update_date
  ,data_level_id
  ,pk1_value
  ,pk2_value
  ,pk3_value
  ,pk4_value
  ,pk5_value
  ,attributegroup_id
  ,application_id
  ,attribute_group_name
  ,attribute_id
  ,attribute_name
  ,data_type_code
  ,language
  ,attribute_translatable_value
  ,value_set_id
  ,validation_code
  ,description
)
AS
   SELECT /*+ PARALLEL(UDA,16) USE_NL(UDA) */
         msib.segment1 item_number
         ,ag.attr_group_disp_name
         ,agc.attr_display_name
         ,ag.multi_row_code
         ,extension_id AS extension_id
         ,uda.organization_id AS organization_id
         ,uda.inventory_item_id AS inventory_item_id
         ,uda.revision_id AS revision_id
         ,uda.last_update_date AS last_update_date
         ,data_level_id AS data_level_id
         ,pk1_value AS pk1_value
         ,pk2_value AS pk2_value
         ,pk3_value AS pk3_value
         ,pk4_value AS pk4_value
         ,pk5_value AS pk5_value
         ,ag.attr_group_id AS attributegroup_id
         ,agc.application_id AS application_id
         ,ag.attr_group_name AS attribute_group_name
         ,agc.attr_id AS attribute_id
         ,agc.attr_name AS attribute_name
         ,agc.data_type_code AS data_type_code
         ,language AS language
         ,DECODE (
             agc.data_type_code
            ,'A', DECODE (database_column
                         ,'TL_EXT_ATTR1', uda.tl_ext_attr1
                         ,'TL_EXT_ATTR2', uda.tl_ext_attr2
                         ,'TL_EXT_ATTR3', uda.tl_ext_attr3
                         ,'TL_EXT_ATTR4', uda.tl_ext_attr4
                         ,'TL_EXT_ATTR5', uda.tl_ext_attr5
                         ,'TL_EXT_ATTR6', uda.tl_ext_attr6
                         ,'TL_EXT_ATTR7', uda.tl_ext_attr7
                         ,'TL_EXT_ATTR8', uda.tl_ext_attr8
                         ,'TL_EXT_ATTR9', uda.tl_ext_attr9
                         ,'TL_EXT_ATTR10', uda.tl_ext_attr10
                         ,'TL_EXT_ATTR11', uda.tl_ext_attr11
                         ,'TL_EXT_ATTR12', uda.tl_ext_attr12
                         ,'TL_EXT_ATTR13', uda.tl_ext_attr13
                         ,'TL_EXT_ATTR14', uda.tl_ext_attr14
                         ,'TL_EXT_ATTR15', uda.tl_ext_attr15
                         ,'TL_EXT_ATTR16', uda.tl_ext_attr16
                         ,'TL_EXT_ATTR17', uda.tl_ext_attr17
                         ,'TL_EXT_ATTR18', uda.tl_ext_attr18
                         ,'TL_EXT_ATTR19', uda.tl_ext_attr19
                         ,'TL_EXT_ATTR20', uda.tl_ext_attr20))
             AS attribute_translatable_value
         ,agc.value_set_id
         ,agc.validation_code
         ,agc.description
     FROM apps.ego_attrs_v agc
         ,apps.ego_attr_groups_v ag
         ,apps.ego_mtl_sy_items_ext_tl uda
         ,apps.mtl_system_items_b msib
    WHERE uda.attr_group_id = ag.attr_group_id AND agc.application_id = ag.application_id AND agc.attr_group_type = ag.attr_group_type AND agc.attr_group_name = ag.attr_group_name AND agc.data_type_code = 'A' AND msib.inventory_item_id = uda.inventory_item_id AND msib.organization_id = uda.organization_id AND msib.organization_id = 222 AND ag.attr_group_name LIKE 'XXWC%';


