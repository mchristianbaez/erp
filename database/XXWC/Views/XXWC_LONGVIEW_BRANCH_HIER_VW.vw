CREATE OR REPLACE FORCE VIEW xxwc.xxwc_longview_branch_hier_vw
(
/*************************************************************************
     $Header    : XXWC_LONGVIEW_BRANCH_HIER_VW
     Module Name: XXWC_LONGVIEW_BRANCH_HIER_VW
      
     REVISIONS:

     Ver        Date        Author              Description
     ---------  ----------  ----------          ----------------
     1.0        03/30/2015  Raghavendra S       TMS#20150317-00105 Longview table exclusion in EBS database
     2.0        04/21/2015  Raghavendra S       20150421-00013 - Modify the view in ebiz to bring in WC Canada branch hierarchy
     3.0        05/10/2015  Steve Moffa	        TMS#20150122-00077 Changed view to incorporate Canada 
 **************************************************************************/
  organization
 ,region_code
 ,region_desc
 ,market_code
 ,market_desc
 ,dstrt_code
 ,dstrt_desc
 ,bw_num
 ,bw_desc
 ,fru_num
 ,branch_num
 ,closed_br
)
AS
  SELECT CASE WHEN lbh.sym_nm3 = 'INTL37' THEN 'WCCAN' ELSE 'WHITECAP' END AS organization -- Added for V3.0
        ,lbh.sym_nm4 region_code
        ,lbh.sym_desc_e4 region_desc
        ,NVL (TRIM (lbh.sym_nm5), 'Unspecified') AS market_code
        ,NVL (TRIM (lbh.sym_desc_e5), 'Unspecified') AS market_desc
        ,NVL (TRIM (lbh.sym_nm6), 'Unspecified') AS dstrt_code
        ,NVL (TRIM (lbh.sym_desc_e6), 'Unspecified') AS dstrt_desc
        ,NVL (TRIM (lbh.leaf_nm), 'Unspecified') AS bw_num
        ,NVL (TRIM (lbh.leaf_desc), 'Unspecified') AS bw_desc
        ,NVL (TRIM (leaf_fru), 'Unspecified') AS fru_num
        ,NVL (TRIM (lct.lob_branch), 'Unspecified') AS branch_num
        ,NVL (TRIM (lct.inactive), 'Y') AS closed_br
    FROM xxwc.xxwc_longview_branch_hierarchy lbh
         LEFT OUTER JOIN xxcus.xxcus_location_code_tbl lct ON lbh.leaf_nm = lct.entrp_loc
   WHERE    (    leaf_nm LIKE 'BW%'
             AND sym_nm4 LIKE 'RG%'     
             AND (   TO_CHAR (lct.effdt  ,'YYYY') > 2012  OR lct.inactive = 'N')) -- Added for V1.0
         OR lbh.sym_nm3 = 'INTL37' -- Added for V2.0
  UNION
  SELECT CASE WHEN lbh.sym_nm3 = 'INTL37' THEN 'WCCAN' ELSE 'WHITECAP' END AS organization -- Added for V2.0
        ,lbh.sym_nm3 region_code
        ,lbh.sym_desc_e3 region_desc
        ,CASE WHEN TRIM (lbh.sym_nm4) LIKE 'CB%' THEN TRIM (lbh.sym_nm4) ELSE 'CB999' END AS market_code
        ,CASE WHEN TRIM (lbh.sym_nm4) LIKE 'CB%' THEN TRIM (lbh.sym_desc_e3) ELSE 'CORPORATE MARKET' END
           AS market_desc
        ,CASE WHEN TRIM (lbh.sym_nm5) LIKE 'DT%' THEN TRIM (lbh.sym_nm5) ELSE 'DT999' END AS dstrt_code
        ,CASE WHEN TRIM (lbh.sym_nm5) LIKE 'DT%' THEN TRIM (lbh.sym_desc_e5) ELSE 'CORPORATE DISTRICT' END
           AS dstrt_desc
        ,NVL (TRIM (lbh.leaf_nm), 'Unspecified') AS bw_num
        ,NVL (TRIM (lbh.leaf_desc), 'Unspecified') AS bw_desc
        ,NVL (TRIM (leaf_fru), 'Unspecified') AS fru_num
        ,NVL (TRIM (lct.lob_branch), 'Unspecified') AS branch_num
        ,NVL (TRIM (lct.inactive), 'Y') AS closed_br
    FROM xxwc.xxwc_longview_branch_hierarchy lbh
         LEFT OUTER JOIN xxcus.xxcus_location_code_tbl lct ON lbh.leaf_nm = lct.entrp_loc
   WHERE    (    leaf_nm LIKE 'BW%'
             AND sym_nm3 LIKE 'RG%'
             AND sym_desc_e3 = 'WHITE CAP - CORPORATE REGION')
         OR (    lbh.sym_nm3 = 'INTL37'
             AND sym_desc_e3 = 'WHITE CAP - CORPORATE REGION') -- Added for V2.0
;