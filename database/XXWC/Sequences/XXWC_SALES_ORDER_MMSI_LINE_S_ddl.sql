/* Formatted on 21-Feb-2013 00:58:46 (QP5 v5.206) */
-- Start of DDL Script for Sequence XXWC.XXWC_SALES_ORDER_MMSI_LINE_S
-- Generated 21-Feb-2013 00:58:45 from XXWC@EBIZDEV

CREATE SEQUENCE xxwc.xxwc_sales_order_mmsi_line_s
    INCREMENT BY 1
    START WITH 81
    MINVALUE 1
    MAXVALUE 9999999999999999999999999999
    NOCYCLE
    NOORDER
    CACHE 20
/

-- End of DDL Script for Sequence XXWC.XXWC_SALES_ORDER_MMSI_LINE_S
