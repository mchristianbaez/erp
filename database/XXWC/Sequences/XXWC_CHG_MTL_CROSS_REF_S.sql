/*******************************************************************************************************************
File Name: XXWC_CHG_MTL_CROSS_REF_S.sql

TYPE:     Sequence

Description: 

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------------------------------------------
1.0     01-MAR-2015   Shankar Vanga   TMS#20150126-00044  - Create Single Item Workflow Tool
                                      
********************************************************************************************************************/

CREATE SEQUENCE XXWC.XXWC_CHG_MTL_CROSS_REF_S
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;


GRANT ALL ON XXWC.XXWC_CHG_MTL_CROSS_REF_S TO APPS


CREATE SYNONYM APPS.XXWC_CHG_MTL_CROSS_REF_S FOR XXWC.XXWC_CHG_MTL_CROSS_REF_S