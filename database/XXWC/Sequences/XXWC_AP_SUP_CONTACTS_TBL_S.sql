/*************************************************************************
  $Header XXWC_AP_SUP_CONTACT_TBL_S.sql $
  Module Name: XXWC_AP_SUP_CONTACT_TBL_S

  PURPOSE: Unique id used in Supplier contacts Custom table

  TMS Task Id :  20141211-00121

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        28-Jan-2015  Manjula Chellappan    Initial Version
**************************************************************************/
CREATE SEQUENCE XXWC.XXWC_AP_SUP_CONTACTS_TBL_S START WITH 1000
                                            INCREMENT BY 1
                                            NOCACHE