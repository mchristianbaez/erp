/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_AIS_SEARCH_LOG_SEQ $
  Module Name: XXWC.XXWC_AIS_SEARCH_LOG_SEQ

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-aPRIL-2017   Pahwa, Nancy                Initially Created 
TMS# 20170302-00126  
**************************************************************************/
-- Create sequence 
CREATE SEQUENCE  "XXWC"."XXWC_AIS_SEARCH_LOG_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;