/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_B2B_CAT_LOAD_STAT_SEQ$
  Module Name: XXWC.XXWC_B2B_CAT_LOAD_STAT_SEQ

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-Mar-2016   Pahwa, Nancy                Initially Created 
TMS# 20160223-00029   
**************************************************************************/
-- Create sequence 
create sequence XXWC.XXWC_B2B_CAT_LOAD_STAT_SEQ
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;