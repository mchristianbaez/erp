/************************************************************************************************************
* Table: XXWC.XXWC_AR_EQUIFAX_LOG_SEQ
=============================================================================================================
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- ------------------------------------------------------------------
  1.0    19-Dec-2017       P.Vamshidhar    TMS#20131016-00419 - Credit - Implement online credit application 
                                                              - Customer Accounts/Update File
*************************************************************************************************************/
CREATE SEQUENCE XXWC.XXWC_AR_EQUIFAX_LOG_SEQ START WITH 1
                                               MAXVALUE 9999999999999999999999999999
                                               MINVALUE 1
                                               NOCYCLE
                                               CACHE 20
/