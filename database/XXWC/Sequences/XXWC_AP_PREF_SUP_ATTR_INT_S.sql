/*************************************************************************
  $Header XXWC_AP_PREF_SUP_ATTR_INT_S.sql $
  Module Name: XXWC_AP_PREF_SUP_ATTR_INT_S

  PURPOSE: Unique id used in Preferred Supplier Attributes staging table

  TMS Task Id :  20141104-00115

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        29-Oct-2014  Manjula Chellappan    Initial Version
**************************************************************************/
CREATE SEQUENCE XXWC.XXWC_AP_PREF_SUP_ATTR_INT_S START WITH 1000
                                            INCREMENT BY 1
                                            NOCACHE