create sequence xxwc.xxwc_inv_mass_loc_upl_s
  START WITH 1
  MAXVALUE 999999999
  MINVALUE 1
  NOCYCLE
  CACHE 2
  NOORDER;


CREATE SYNONYM APPS.xxwc_inv_mass_loc_upl_s FOR XXWC.xxwc_inv_mass_loc_upl_s;


CREATE SYNONYM XXWC_DEV_ADMIN.xxwc_inv_mass_loc_upl_s FOR XXWC.xxwc_inv_mass_loc_upl_s;