/*************************************************************************
      Table Name: XXWC_WOC_APP_LOG_TBL_SEQ

      PURPOSE:   This is to create sequence

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        01/16/2018    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
 ****************************************************************************/
DROP SEQUENCE XXWC.XXWC_WOC_APP_LOG_TBL_SEQ;
CREATE SEQUENCE XXWC.XXWC_WOC_APP_LOG_TBL_SEQ
 MINVALUE 1
 START WITH 1
 INCREMENT BY 1
 CACHE 10;