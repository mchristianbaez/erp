/***********************************************************************************
File Name: XXWC_EGO_EVENT_LOG_TAB_S
PROGRAM TYPE: SQL SEQUENCE file
HISTORY
PURPOSE: Sequence used in main procedure for generating extracts
====================================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ----------------------------------------------
1.0     09/23/2012    Rajiv Rathod    Initial creation of the file
************************************************************************************/

DROP SEQUENCE XXWC.XXWC_EGO_EVENT_LOG_TAB_S;

CREATE SEQUENCE XXWC.XXWC_EGO_EVENT_LOG_TAB_S
   START WITH 1
   MAXVALUE 9999999999999999999999999999
   MINVALUE 1
   NOCYCLE
   CACHE 20
   NOORDER;

CREATE SYNONYM APPS.XXWC_EGO_EVENT_LOG_TAB_S FOR XXWC.XXWC_EGO_EVENT_LOG_TAB_S
/