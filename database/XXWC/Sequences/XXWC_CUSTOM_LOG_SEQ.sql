 /****************************************************************************************************
    File Name:XXWC_CUSTOM_LOG_SEQ.sql
    VERSION DATE          AUTHOR(S)           DESCRIPTION
    ------- -----------   --------------- ---------------------------------
	1.0     09-Oct-2018   P.Vamshidhar        TMS#20180806-00122 - Purchasing Mass 
	                                          upload taking more time these days
											  Added debug messages.
   *************************************************************************************************/

CREATE SEQUENCE  "XXWC"."XXWC_CUSTOM_LOG_SEQ"  
      MINVALUE 1 
	  MAXVALUE 9999999999999999999999999999 
	  INCREMENT BY 1 
	  START WITH 1 
	  CACHE 20 
	  NOORDER  
	  NOCYCLE  
	  NOPARTITION
/  
  


