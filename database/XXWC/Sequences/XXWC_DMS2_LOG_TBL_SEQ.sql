/*************************************************************************
     $Header XXWC_DMS2_LOG_TBL_SEQ $
     Module Name: XXWC_DMS2_LOG_TBL_SEQ.pkb

     PURPOSE:   DESCARTES Project

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/11/2017  Rakesh Patel            TMS#20170901-00010 DMS Phase-2.0 Inbound Extract
**************************************************************************/
-- Create sequence 
create sequence XXWC.XXWC_DMS2_LOG_TBL_SEQ
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;
