/*******************************************************************************************************************
File Name: XXWC_MTL_SYSTEM_CHANGE_ID_S.sql

TYPE:     Sequence

Description: sequence used to create change ID's

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------------------------------------------
1.0     01-MAR-2015   Shankar Vanga   sequence used to create change ID's
                                      TMS#20150126-00044  - Create Single Item Workflow Tool
********************************************************************************************************************/

CREATE SEQUENCE XXWC.XXWC_MTL_SYSTEM_CHANGE_ID_S
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;


GRANT ALL ON XXWC.XXWC_MTL_SYSTEM_CHANGE_ID_S TO APPS


CREATE OR REPLACE PUBLIC SYNONYM APPS.XXWC_MTL_SYSTEM_CHANGE_ID_S for XXWC.XXWC_MTL_SYSTEM_CHANGE_ID_S;
