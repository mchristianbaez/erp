/* Formatted on 26-Nov-2012 23:30:31 (QP5 v5.206) */
-- Start of DDL Script for Sequence XXWC.XXWC_AR_CUST_MAINT_ERR_S
-- Generated 26-Nov-2012 23:30:30 from XXWC@EBIZCON

CREATE SEQUENCE xxwc.xxwc_ar_cust_maint_err_s
    INCREMENT BY 1
    START WITH 1
    MINVALUE 1
    MAXVALUE 9999999999999999999999999999
    NOCYCLE
    NOORDER
    
/

-- End of DDL Script for Sequence XXWC.XXWC_AR_CUST_MAINT_ERR_S
