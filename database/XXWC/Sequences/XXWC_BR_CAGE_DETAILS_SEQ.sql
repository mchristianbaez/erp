/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_BR_CAGE_DETAILS_SEQ$
  Module Name: XXWC.XXWC_BR_CAGE_DETAILS_SEQ
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
   1.0     30-Jun-2017        Pahwa Nancy   TMS# 20170607-00052 
**************************************************************************/
-- Create sequence 
create sequence XXWC.XXWC_BR_CAGE_DETAILS_SEQ
minvalue 1
maxvalue 9999999999999999999999999999
start with 2001
increment by 1
cache 1000;