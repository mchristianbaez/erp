/* Formatted on 29-Apr-2013 16:57:50 (QP5 v5.206) */
-- Start of DDL Script for Sequence XXWC.XXWC_ITEM_MASS_CLONE_HISTORY_S
-- Generated 29-Apr-2013 16:57:50 from XXWC@EBIZDEV

CREATE SEQUENCE xxwc.xxwc_item_mass_clone_history_s
    INCREMENT BY 1
    START WITH 27381
    MINVALUE 1
    MAXVALUE 9999999999999999999999999999
    NOCYCLE
    NOORDER
    CACHE 20
/

-- End of DDL Script for Sequence XXWC.XXWC_ITEM_MASS_CLONE_HISTORY_S
