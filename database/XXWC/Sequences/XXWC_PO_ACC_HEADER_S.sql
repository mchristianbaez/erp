/* Formatted on 07-Mar-2014 15:53:22 (QP5 v5.206) */
-- Start of DDL Script for Sequence XXWC.XXWC_PO_ACC_HEADER_S
-- Generated 07-Mar-2014 15:53:21 from XXWC@EBIZFQA

CREATE SEQUENCE xxwc.xxwc_po_acc_header_s
    INCREMENT BY 1
    START WITH 1
    MINVALUE 1
    MAXVALUE 9999999999999999999999999999
    NOCYCLE
    NOORDER
    NOCACHE
/

-- End of DDL Script for Sequence XXWC.XXWC_PO_ACC_HEADER_S
