  /*******************************************************************************
  Table: "XXWC"."XXWC_ORACLE_ATTACHMENT_TBL"  
  Description: "XXWC"."XXWC_ORACLE_ATTACHMENT_TBL grants" 
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-JUL-2017        Pahwa Nancy   Task ID: 20170616-00237
  ********************************************************************************/
-- Grants
  GRANT INSERT ON "XXWC"."XXWC_ORACLE_ATTACHMENT_TBL" TO "APPS" WITH GRANT OPTION;
  GRANT SELECT ON "XXWC"."XXWC_ORACLE_ATTACHMENT_TBL" TO "APPS" WITH GRANT OPTION;
  GRANT UPDATE ON "XXWC"."XXWC_ORACLE_ATTACHMENT_TBL" TO "APPS" WITH GRANT OPTION;
  GRANT DELETE ON "XXWC"."XXWC_ORACLE_ATTACHMENT_TBL" TO "APPS" WITH GRANT OPTION;
/