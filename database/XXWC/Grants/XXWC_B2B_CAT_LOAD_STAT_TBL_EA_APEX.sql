/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_B2B_CAT_LOAD_STAT_TBL$
  Module Name: XXWC_B2B_CAT_LOAD_STAT_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029 
**************************************************************************/
grant select, insert, update, delete on XXWC.XXWC_B2B_CAT_LOAD_STAT_TBL to EA_APEX;