  /********************************************************************************
  FILE NAME: xxwc.XXWC_DLS_CONNECTOR_TBL
  
  PROGRAM TYPE:table for edqp requests
  
  PURPOSE: table for edqp requests
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     01/19/2018  Nancy Pahwa     TMS#20171108-00066 - Issue with populating cross reference
  *******************************************************************************************/
  GRANT FLASHBACK ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT DEBUG ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT QUERY REWRITE ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT ON COMMIT REFRESH ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT READ ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT REFERENCES ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT UPDATE ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT SELECT ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT INSERT ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT INDEX ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT DELETE ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";
  GRANT ALTER ON "XXWC"."XXWC_DLS_CONNECTOR_TBL" TO "DLS";