/*************************************************************************
      Script Name: XXWC_B2B_CONFIG_TBL_GRANT.sql

      PURPOSE:   Grant execute on XXWC_CHECKITAPP_PKG to INTERFACE_OSO package

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        10-Nov-2017   Niraj K Ranjan       TMS#20171102-00074 - Mobile apps new PL/SQL procedures for WOC
****************************************************************************/
GRANT SELECT ON XXWC.XXWC_B2B_CONFIG_TBL TO INTERFACE_OSO
/
GRANT SELECT ON APPS.HZ_PARTIES TO INTERFACE_OSO
/
GRANT SELECT ON APPS.HZ_CUST_ACCOUNTS TO INTERFACE_OSO
/