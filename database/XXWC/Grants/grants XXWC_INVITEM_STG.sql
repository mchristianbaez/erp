 /********************************************************************************
  FILE NAME: XXWC.XXWC_INVITEM_STG.sql

  PROGRAM TYPE: Grant scripts

  PURPOSE: Conversion purpose in APEX application

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/09/2018    Pattabhi Avula  TMS#20180308-00301 -- Initial Version
  ********************************************************************************/
  GRANT INSERT ON "XXWC"."XXWC_INVITEM_STG" TO "EA_APEX"
/
  GRANT DELETE ON "XXWC"."XXWC_INVITEM_STG" TO "EA_APEX"
/
  GRANT UPDATE ON "XXWC"."XXWC_INVITEM_STG" TO "EA_APEX"
/
  GRANT SELECT ON "XXWC"."XXWC_INVITEM_STG" TO "EA_APEX"
/
  GRANT SELECT ON "XXWC"."XXWC_INVITEM_STG" TO "INTERFACE_MSSQL"
/