 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_GL_CONV_REF_GRANTS_TO_APEX_USER.sql

  PROGRAM TYPE: Grant scripts

  PURPOSE: AP Conversion purpose in APEX application

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/09/2018    Naveen K  		TMS#20180713-00072  -- Initial Version
  ********************************************************************************/
  GRANT ALTER,SELECT,INSERT,UPDATE,DELETE ON "XXWC"."XXWC_AP_BRANCH_LIST_STG_TBL" TO EA_APEX
/
  GRANT SELECT ON "XXWC"."XXWC_AP_BRANCH_LIST_EXT_TBL" TO EA_APEX
/
  GRANT ALTER,SELECT,INSERT,UPDATE,DELETE ON "XXWC"."XXWC_AP_GL_MAP_STG_TBL" TO EA_APEX
/
  GRANT SELECT ON "XXWC"."XXWC_AP_GL_MAP_EXT_TBL" TO EA_APEX
/
