  /*******************************************************************************
  Table: "XXWC"."XXWC_MSSQL_ATTACH_TBL"  
  Description: "XXWC"."XXWC_MSSQL_ATTACH_TBL grants" 
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-JUL-2017        Pahwa Nancy   Task ID: 20170616-00237
  ********************************************************************************/
-- Grants
grant select, insert, update, delete, references, alter, index on XXWC.XXWC_MSSQL_ATTACH_TBL to INTERFACE_MSSQL;
/