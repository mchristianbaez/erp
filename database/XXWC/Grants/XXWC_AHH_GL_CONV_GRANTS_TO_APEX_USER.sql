 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_GL_CONV_GRANTS_TO_APEX_USER.sql

  PROGRAM TYPE: Grant scripts

  PURPOSE: AP Conversion purpose in APEX application

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/09/2018    Naveen K  		TMS#20180713-00073  -- Initial Version
  ********************************************************************************/
  GRANT ALTER,SELECT,INSERT,UPDATE,DELETE ON "XXWC"."XXWC_GL_AHH_JOURNALS_STG_TBL" TO EA_APEX
/
  GRANT SELECT ON "XXWC"."XXWC_GL_AHH_JOURNALS_STG_EXT_T" TO EA_APEX
/
