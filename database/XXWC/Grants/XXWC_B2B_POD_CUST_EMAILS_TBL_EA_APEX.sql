/*
-- ********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     7-Jun-2017   Nancy Pahwa   TMS#  20170601-00292 
*/
-- Grant/Revoke object privileges 
grant select, insert, update, delete, references, alter, index on XXWC.XXWC_B2B_POD_CUST_EMAILS_TBL to EA_APEX;