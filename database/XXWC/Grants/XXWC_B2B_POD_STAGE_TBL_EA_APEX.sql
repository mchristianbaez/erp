/*
-- ********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     18-APR-2017   Nancy Pahwa   TMS# 20170206-00257
*/
  GRANT DELETE ON "XXWC"."XXWC_B2B_POD_STAGE_TBL" TO "EA_APEX";
  GRANT INSERT ON "XXWC"."XXWC_B2B_POD_STAGE_TBL" TO "EA_APEX";
  GRANT SELECT ON "XXWC"."XXWC_B2B_POD_STAGE_TBL" TO "EA_APEX";
  GRANT UPDATE ON "XXWC"."XXWC_B2B_POD_STAGE_TBL" TO "EA_APEX";
/