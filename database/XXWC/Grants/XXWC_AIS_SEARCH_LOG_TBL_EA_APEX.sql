/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_AIS_SEARCH_LOG_TBL$
  Module Name: XXWC_AIS_SEARCH_LOG_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-apr-2017   Pahwa, Nancy                Initially Created TMS# TMS# 20170302-00126
**************************************************************************/
GRANT SELECT ON "XXWC"."XXWC_AIS_SEARCH_LOG_TBL" TO "EA_APEX";
