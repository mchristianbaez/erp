--//============================================================================
--//
--// Object Name         :: XXWC_MS_USER_ROLES_TBL
--//
--// Object Type         :: Grant script.
--//
--// Object Description  :: Script is used to Grant Permissons on
--//                        XXWC.XXWC_MS_USER_ROLES_TBL to EA_APEX
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pahwa Nancy     09/05/2015    Initial Build - TMS#20150901-00129 
--//============================================================================
grant select, insert, update, delete on XXWC.XXWC_MS_USER_ROLES_TBL to EA_APEX;