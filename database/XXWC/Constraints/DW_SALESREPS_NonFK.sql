ALTER TABLE XXWC.DW_SALESREPS ADD (
  CONSTRAINT PK_HREMPLOYEEID
  PRIMARY KEY
  (HREMPLOYEEID)
  USING INDEX XXWC.PK_HREMPLOYEEID
  ENABLE VALIDATE);

