DECLARE
   PROCEDURE xxwc_delete_constraint (
      p_table_name         VARCHAR2,
      p_constraint_name    VARCHAR2 DEFAULT NULL)
   IS
      CURSOR c_cons
      IS
         SELECT table_name, constraint_name
           FROM dba_constraints
          WHERE     table_name = UPPER (p_table_name)
                AND constraint_name =
                       NVL (UPPER (p_constraint_name), constraint_name);

      l_table_owner   VARCHAR2 (50);
   BEGIN
      DBMS_OUTPUT.put_line (
         '-----------------------------------------------------------------');
      DBMS_OUTPUT.put_line (
         'Deleting constraint of a table: ' || p_table_name);

      BEGIN
         SELECT owner
           INTO l_table_owner
           FROM dba_tables
          WHERE table_name = UPPER (p_table_name);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_table_owner := NULL;
            DBMS_OUTPUT.put_line (
               'Error in derving table owner: ' || SQLERRM);
      END;

      IF l_table_owner = 'XXWC'
      THEN
         FOR rec_cons IN c_cons
         LOOP
            DBMS_OUTPUT.put_line (
               'Deleting constraint:  ' || rec_cons.constraint_name);

            BEGIN
               EXECUTE IMMEDIATE
                     'alter table '
                  || l_table_owner
                  || '.'
                  || rec_cons.table_name
                  || ' drop constraint '
                  || rec_cons.constraint_name;
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.put_line (
                     'Error in dropping constraint: ' || SQLERRM);
            END;
         END LOOP;
      END IF;
   END;
----------------------------------------------------
BEGIN
   xxwc_delete_constraint ('xxwc_mtl_sy_items_chg_b');
   xxwc_delete_constraint ('XXWC_CHG_MTL_CROSS_REF');
   xxwc_delete_constraint ('XXWC_CHG_EGO_ITEM_ATTRS');
END;