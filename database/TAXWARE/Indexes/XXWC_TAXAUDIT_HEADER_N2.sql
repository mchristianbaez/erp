--
-- XXWC_TAXAUDIT_HEADER_N2  (Index) 
--
CREATE INDEX TAXWARE.XXWC_TAXAUDIT_HEADER_N2 ON TAXWARE.TAXAUDIT_HEADER
(TO_CHAR("TRANSDATE",'MM/DD/YYYY'))
TABLESPACE TAXWARE;


