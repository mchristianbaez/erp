--
-- XXWC_RA_CUSTOMER_TRX_N27  (Index) 
--
CREATE INDEX AR.XXWC_RA_CUSTOMER_TRX_N27 ON AR.RA_CUSTOMER_TRX_ALL
(BILL_TO_CUSTOMER_ID)
TABLESPACE APPS_TS_TX_DATA;


