---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_RA_CUST_TRX_LINES_N17
  File Name: XXWC_RA_CUST_TRX_LINES_N17.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        04-Apr-2016  Siva        --TMS#20150921-00332 Invoice Pre-Register report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX AR.XXWC_RA_CUST_TRX_LINES_N17 
ON AR.RA_CUSTOMER_TRX_LINES_ALL (INTERFACE_LINE_CONTEXT,INTERFACE_LINE_ATTRIBUTE1,DESCRIPTION) TABLESPACE APPS_TS_TX_DATA
/
