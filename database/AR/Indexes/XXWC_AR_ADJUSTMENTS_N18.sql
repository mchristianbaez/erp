--------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_AR_ADJUSTMENTS_N18
  File Name: XXWC_AR_ADJUSTMENTS_N18.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        27-Sep-2016  Siva        --TMS#20160826-00345   Invoice Register - Write Offs - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX AR.XXWC_AR_ADJUSTMENTS_N18 ON AR.AR_ADJUSTMENTS_ALL
  (
    STATUS,
    POSTABLE,
    ADJUSTMENT_ID,
    RECEIVABLES_TRX_ID
  )
  TABLESPACE APPS_TS_TX_DATA
/
