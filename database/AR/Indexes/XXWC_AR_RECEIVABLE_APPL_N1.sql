/********************************************************************************
   $Header XXWC_AR_RECEIVABLE_APPL_N1.SQL $
   Module Name: XXWC_AR_RECEIVABLE_APPL_N1

   PURPOSE:   Index on table AR_RECEIVABLE_APPLICATIONS_ALL

   REVISIONS:
   Ver        Date        Author                  Description
   ---------  ----------  ---------------         -------------------------
   1.0        02/26/2016  Manjula Chellappan      TMS# 20160226-00136 - Create Index on AR_RECEIVABLE_APPLICATIONS_ALL
********************************************************************************/ 
 CREATE INDEX AR.XXWC_AR_RECEIVABLE_APPL_N1
 ON AR.AR_RECEIVABLE_APPLICATIONS_ALL 
(APPLIED_PAYMENT_SCHEDULE_ID, CASH_RECEIPT_ID, STATUS, DISPLAY); 