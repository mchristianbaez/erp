---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_RA_CUSTOMER_TRX_N33 $
  REVISIONS   :
  VERSION 		  DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     		24-Oct-2016       		Siva   		 TMS#20160804-00030-- performance issue for EIS Vendor Qoute Batch Summary Report
**************************************************************************************************************/
CREATE INDEX AR.XXWC_RA_CUSTOMER_TRX_N33 ON AR.RA_CUSTOMER_TRX_ALL
  (
    TRUNC(TRX_DATE)
  )
  TABLESPACE APPS_TS_TX_DATA
/
