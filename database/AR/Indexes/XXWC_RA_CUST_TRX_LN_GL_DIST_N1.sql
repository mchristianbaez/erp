-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_RA_CUST_TRX_LN_GL_DIST_N1
  File Name: XXWC_RA_CUST_TRX_LN_GL_DIST_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        12-Apr-2016  Siva        TMS#20160412-00032 --White Cap End of Month Commissions Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX AR.XXWC_RA_CUST_TRX_LN_GL_DIST_N1 
 ON AR.RA_CUST_TRX_LINE_GL_DIST_ALL (TRUNC(GL_DATE)) TABLESPACE APPS_TS_TX_DATA
/
