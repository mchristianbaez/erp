-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_RA_CUST_TRX_LINES_N20
  File Name: XXWC_RA_CUST_TRX_LINES_N20.sql 
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        12-Apr-2016  Siva        TMS#20150928-00191 --Low Qty Manual Price Change Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX AR.XXWC_RA_CUST_TRX_LINES_N20 
  ON AR.RA_CUSTOMER_TRX_LINES_ALL (TRUNC(CREATION_DATE),INTERFACE_LINE_CONTEXT,INTERFACE_LINE_ATTRIBUTE1,INTERFACE_LINE_ATTRIBUTE6) TABLESPACE APPS_TS_TX_DATA
/
