   /******************************************************************************************************************************************************
        $Header XXWC_HZ_CUST_ACCOUNTS_N8 $
        Module Name: XXWC_HZ_CUST_ACCOUNTS_N8.sql

        PURPOSE: Invoice Interface

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018  P.Vamshidhar     TMS#20180708-00002 - AH HARRIS AR Debit Memo/Invoice Interface
   ******************************************************************************************************************************************************/
CREATE INDEX AR.XXWC_HZ_CUST_ACCOUNTS_N8 ON AR.HZ_CUST_ACCOUNTS
(STATUS, ATTRIBUTE4, CUST_ACCOUNT_ID)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
/
