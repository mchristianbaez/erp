--------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_RA_CUST_TRX_LINES_N16
  File Name: XXWC_RA_CUST_TRX_LINES_N16.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        04-Apr-2016  Siva        --TMS#20150921-00332 Invoice Pre-Register report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX AR.XXWC_RA_CUST_TRX_LINES_N16 ON 
 AR.RA_CUSTOMER_TRX_LINES_ALL (CREATION_DATE,INTERFACE_LINE_ATTRIBUTE1,INTERFACE_LINE_ATTRIBUTE6) TABLESPACE APPS_TS_TX_DATA
/
