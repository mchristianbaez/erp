-- ******************************************************************************
-- *   $Header XXWC_HZ_CUST_ACCT_SITES_N7.ctl $
-- *   Module Name: XXWC_HZ_CUST_ACCT_SITES_N7.ctl
-- *
-- *   PURPOSE:   Index on AR.HZ_CUST_ACCT_SITES_ALL
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------
-- *   1.0        07/03/2018  P.Vamshidhar            TMS#20180708-00002 - AH HARRIS AR Debit Memo/Invoice Interface
--                                                    
-- * ****************************************************************************
CREATE INDEX AR.XXWC_HZ_CUST_ACCT_SITES_N7 ON AR.HZ_CUST_ACCT_SITES_ALL
(ORG_ID, BILL_TO_FLAG)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
/