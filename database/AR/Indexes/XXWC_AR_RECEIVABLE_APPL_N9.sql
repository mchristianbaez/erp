--------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_AR_RECEIVABLE_APPL_N9
  File Name: XXWC_AR_RECEIVABLE_APPL_N9.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        07-Jul-2016  Siva        --TMS#20160606-00037  Customer Short Pay Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX AR.XXWC_AR_RECEIVABLE_APPL_N9 ON 
	AR.AR_RECEIVABLE_APPLICATIONS_ALL (DISPLAY,STATUS,APPLICATION_TYPE) TABLESPACE APPS_TS_TX_DATA
/
