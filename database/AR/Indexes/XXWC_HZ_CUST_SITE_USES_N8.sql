---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_HZ_CUST_SITE_USES_N8 $
  REVISIONS   :
  VERSION 		  DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     		24-Oct-2016       		Siva   		 TMS#20160826-00137-- performance issue for WC AR Customer Site Inactivation Report 
**************************************************************************************************************/
CREATE INDEX AR.XXWC_HZ_CUST_SITE_USES_N8 ON AR.HZ_CUST_SITE_USES_ALL
  (
    STATUS,
    ATTRIBUTE1,
    PRIMARY_FLAG,
    ORG_ID
  )
  TABLESPACE APPS_TS_TX_DATA
/
