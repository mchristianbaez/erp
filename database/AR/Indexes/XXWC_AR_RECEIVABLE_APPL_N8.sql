--------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_AR_RECEIVABLE_APPL_N8
  File Name: XXWC_AR_RECEIVABLE_APPL_N8.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        22-Apr-2016  Siva        --TMS#20160426-00100 Customer Short Pay Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX AR.XXWC_AR_RECEIVABLE_APPL_N8 ON 
  AR.AR_RECEIVABLE_APPLICATIONS_ALL (TRUNC(GL_DATE)) TABLESPACE APPS_TS_TX_DATA
/
