-- ******************************************************************************
-- *   $Header XXWC_HZ_CUST_SITE_USES_A_N1.sql $
-- *   Module Name: Receivables
-- *
-- *   PURPOSE:   Index on AR.HZ_CUST_SITE_USES_ALL_A
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------
-- *   1.0        08/17/2018   Siva            			TMS#20180802-00013  - Customer Audit History Report - WC report performance
--                                                    
-- * ****************************************************************************
CREATE INDEX AR.XXWC_HZ_CUST_SITE_USES_A_N1 ON AR.HZ_CUST_SITE_USES_ALL_A
  (
    TRUNC(AUDIT_TIMESTAMP)
  )
  TABLESPACE APPS_TS_TX_DATA
/