-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_RA_INTERFACE_LINES_ALL_N3
  File Name: XXWC_RA_INTERFACE_LINES_ALL_N3.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        04-Apr-2016  Siva        --TMS#20150921-00332 Invoice Pre-Register report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX AR.XXWC_RA_INTERFACE_LINES_ALL_N3
 ON AR.RA_INTERFACE_LINES_ALL (INTERFACE_LINE_CONTEXT,INTERFACE_LINE_ATTRIBUTE6,INTERFACE_LINE_ATTRIBUTE1) TABLESPACE APPS_TS_TX_DATA
/
