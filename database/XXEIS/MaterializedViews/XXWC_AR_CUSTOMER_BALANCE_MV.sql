/* Formatted on 8/14/2013 9:40:50 AM (QP5 v5.206) */
--as apps
DROP MATERIALIZED VIEW xxeis.xxwc_ar_customer_balance_mv;

CREATE MATERIALIZED VIEW xxeis.xxwc_ar_customer_balance_mv
PCTFREE 10
MAXTRANS 255 TABLESPACE xxeis_data
STORAGE (INITIAL 131072
         NEXT 131072
         PCTINCREASE 0
         MINEXTENTS 1
         MAXEXTENTS 2147483645) NOLOGGING
PARALLEL
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
AS
    SELECT * FROM xxeis.xxwc_ar_customer_balance_mv##g;
--select * from xxeis.xxwc_ar_customer_balance_mv
