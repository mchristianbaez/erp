CREATE OR REPLACE PACKAGE BODY XXEIS.EIS_WC_ORDER_HOLD_DET_PKG
AS        
/*************************************************************************
     Package : EIS_WC_ORDER_HOLD_DET_PKG

     PURPOSE:   This will populate the staging table for the Eis report
                Bill Trust
     Parameter:
	 REVISIONS:
     Ver          Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        10/16/2015  Maharajan Shunmugam    TMS#20140516-00041 Credit - EIS Create Credit Hold Release Report
   ************************************************************************/
   PROCEDURE populate_stage 
   IS
  /*************************************************************************
     Procedure : populate_stage

     PURPOSE:   This will populate the staging table for the Eis report
                Bill Trust
     Parameter:
	 REVISIONS:
     Ver          Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        10/16/2015  Maharajan Shunmugam    TMS#20140516-00041 Credit - EIS Create Credit Hold Release Report
   ************************************************************************/
 l_sec                              VARCHAR2(200);
 l_distro_list                   VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN 
 
   l_sec :=  'Truncating the staging table';
   --EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_CR_HOLD_CASE_FOLDER_STG';  
   DELETE FROM XXWC.XXWC_CR_HOLD_CASE_FOLDER_STG;
   COMMIT; 
 
   l_sec := 'Inserting into staging table';

   INSERT INTO XXWC.XXWC_CR_HOLD_CASE_FOLDER_STG
  (SELECT accr.source_column1,
          accr.created_by ,
          accf.case_folder_number,
          DECODE(substr(accf.review_type,1,1),'O','O','C','C','S','B') REVIEW_TYPE,
          to_char(accf.creation_date,'DD-MON-YYYY HH24:MI:SS') FOLDER_CREATION_DATE ,
          to_char(accf.last_update_date,'DD-MON-YYYY HH24:MI:SS') CASE_FOLDER_RELEASED_DATE
   FROM   apps.ar_cmgt_case_folders accf, 
          apps.ar_cmgt_credit_requests accr
   WHERE accf.credit_request_id = accr.credit_request_id
   AND   accf.case_folder_number IS NOT NULL ) ;     
                      
COMMIT;

EXCEPTION
WHEN OTHERS THEN
         apps.xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXEIS.EIS_WC_ORDER_HOLD_DET_PKG.populate_stage',
          p_calling                => l_sec,
          p_request_id             => NULL,
          p_ora_error_msg          =>  SUBSTR(DBMS_UTILITY.format_error_stack ()|| DBMS_UTILITY.format_error_backtrace (), 1,2000),
          p_error_desc             => 'WHEN OTHERS EXCEPTION in XXEIS.EIS_WC_ORDER_HOLD_DET_PKG',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );

END populate_stage;
END EIS_WC_ORDER_HOLD_DET_PKG;
/