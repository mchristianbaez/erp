CREATE OR REPLACE package body XXEIS.EIS_XXWC_CASH_DRAW_PKG is
  
 PROCEDURE POPULATE_ON_ACCOUNT_DATA (P_PROCESS_ID IN NUMBER) IS

 BEGIN   
   
   insert into xxeis.EIS_XXWC_CASH_EXCE_TBL(PROCESS_ID,CASH_RECEIPT_ID,PAYMENT_SET_ID)
   SELECT  /*+ index (RAP1 AR_RECEIVABLE_APPLICATIONS_N14) */ P_PROCESS_ID,cash_receipt_id, 
                  oe.payment_set_id payment_set_id
             FROM ar_receivable_applications_all rap1,
                  oe_payments oe,
                  oe_order_headers_all oh
            WHERE rap1.payment_set_id = oe.payment_set_id   
              AND oh.header_id        = oe.header_id
              AND oh.order_type_id    = 1004;  

   
    insert into XXEIS.EIS_XXWC_CASH_EXCE_TMP_TBL
                    ( PROCESS_ID,
                      PAYMENT_SET_ID,
                      ON_ACCOUNT
                     )
    SELECT P_PROCESS_ID,
           rap2.payment_set_id, 
           SUM(NVL(amount_applied,0)) on_account
      FROM ar_receivable_applications rap,
          (SELECT   distinct cash_receipt_id, 
                 payment_set_id
             FROM EIS_XXWC_CASH_EXCE_TBL
             WHERE process_id = P_PROCESS_ID
              ) rap2
     WHERE rap.applied_payment_schedule_id = -1
       AND rap.display                       = 'Y'
       AND rap2.cash_receipt_id = rap.cash_receipt_id
       AND NOT EXISTS
            (SELECT 1
               FROM XXWC_OM_CASH_REFUND_TBL XOC
              WHERE XOC.cash_receipt_id=rap.cash_receipt_id
            )
     group by rap2.payment_set_id;    
     --COMMIT;
 EXCEPTION 
  WHEN OTHERS THEN
    fnd_file.put_line(FND_FILE.LOG,SQLERRM);
       
 end;

 PROCEDURE DELETE_ON_ACCOUNT_DATA (P_PROCESS_ID IN NUMBER) IS

 BEGIN   
   delete from XXEIS.EIS_XXWC_CASH_EXCE_TMP_TBL where process_id = P_PROCESS_ID;
   --commit;
  EXCEPTION 
  WHEN OTHERS THEN
    fnd_file.put_line(FND_FILE.LOG,SQLERRM);       
 end;
END;
/