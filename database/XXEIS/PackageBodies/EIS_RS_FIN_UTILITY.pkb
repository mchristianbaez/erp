SET SCAN OFF DEFINE OFF;
CREATE OR REPLACE  
PACKAGE BODY XXEIS.eis_rs_fin_utility wrapped 
0
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
3
b
9200000
1
4
0 
ec
2 :e:
1PACKAGE:
1BODY:
1EIS_RS_FIN_UTILITY:
1FUNCTION:
1GET_SEGMENT_DESCRIPTION:
1P_VALUE_SET_ID:
1NUMBER:
1P_SEGMENT:
1VARCHAR2:
1P_CODE_COMBINATION_ID:
1RETURN:
1P_DESCRIPTION:
1100:
1FFV:
1DESCRIPTION:
1FND_FLEX_VALUES_VL:
1GL_CODE_COMBINATIONS_KFV:
1GCC:
1FLEX_VALUE_SET_ID:
1FLEX_VALUE:
1UPPER:
1CODE_COMBINATION_ID:
1SELECT ffv.description :n        INTO p_description :n        FROM fnd_flex_v+
1alues_vl ffv, gl_code_combinations_kfv gcc :n       WHERE flex_value_set_id =+
1 p_value_set_id :n         AND ffv.flex_value = UPPER (p_segment) :n         +
1AND gcc.code_combination_id = p_code_combination_id:
1NO_DATA_FOUND:
1OTHERS:
1DECODE_VSET:
1P_VSET_VALUE:
1P_VSET:
1P_DSET_VALUE:
1CURSOR:
1CSR_VSET_VALUE:
1FND_FLEX_VALUE_SETS:
1FFVS:
1FLEX_VALUE_SET_NAME:
1TRIM:
1NVL:
1PARENT_FLEX_VALUE_LOW:
1SELECT ffv.description :n    from fnd_flex_value_sets ffvs, :n         fnd_fl+
1ex_values_vl ffv :n   where ffvs.flex_value_set_name = trim(p_vset) :n     an+
1d ffvs.flex_value_set_id = ffv.flex_value_set_id :n    --and ffv.enabled_flag+
1 = 'Y' :n     and ffv.flex_value = p_vset_value :n     and nvl(ffv.parent_fle+
1x_value_low,1)=nvl(p_dset_value,nvl(ffv.parent_flex_value_low,1)):
1L_DESC:
1240:
1L_HASH_INDEX:
1200:
1IS NOT NULL:
1||:
1-:
1G_VALUE_SET_TBL:
1OPEN:
1CLOSE:
1CREATE_GL_SEGMENT_LOVS:
1P_SOB_ID:
1P_LOV_VS_NAME:
1P_GL_ACCOUNT_LOV_CREATION:
1N:
1P_ID_FLEX_NUM:
1C_GET_VS:
1FFS:
1APPLICATION_COLUMN_NAME:
1SEGMENT_NAME:
1ID_FLEX_NUM:
1SEGMENT_NUM:
1FND_ID_FLEX_SEGMENTS:
1ID_FLEX_CODE:
1APPLICATION_ID:
1GSOB:
1CHART_OF_ACCOUNTS_ID:
1GL_SETS_OF_BOOKS:
1GL_SECURITY_PKG:
1VALIDATE_ACCESS:
1SET_OF_BOOKS_ID:
1SELECT :n	--gsob.name, :n	--gsob.short_name, :n	ffs.APPLICATION_COLUMN_NAME, +
1:n	ffs.SEGMENT_NAME, :n	ffs.ID_FLEX_NUM, :n	ffs.segment_num, :n	ffs.flex_valu+
1e_set_id, :n	ffvs.flex_value_set_name :n  FROM  --gl_sets_of_books gsob, :n  +
1		FND_ID_FLEX_SEGMENTS ffs , :n		FND_FLEX_VALUE_SETS ffvs :n WHERE --gsob.set+
1_of_books_id = nvl(p_sob_id,gsob.set_of_books_id) :n --and gsob.chart_of_acco+
1unts_id = ffs.id_flex_num :n --and  :n     ffs.id_flex_code = 'GL#' and ffs.A+
1PPLICATION_ID = 101 :n and ffs.flex_value_set_id = ffvs.flex_value_set_id  :n+
1 AND ffs.id_flex_num = ( select gsob.chart_of_accounts_id  :n                +
1           from gl_sets_of_books gsob  :n                          where gl_s+
1ecurity_pkg.validate_access(gsob.set_of_books_id) = 'TRUE') :
1C_GET_ID_FLEX_NUM:
1FND_ID_FLEX_STRUCTURES:
1SELECT id_flex_num :nfrom fnd_id_flex_structures :nwhere application_id = 101+
1 :nand id_flex_num = nvl(p_id_flex_num,id_flex_num) :nand id_flex_code = 'GL#+
1':
1C_GET_SEGMENTS:
1TYPE:
1SELECT id_flex_num,application_column_name, segment_name :nfrom fnd_id_flex_s+
1egments :nwhere 1=1 :nand application_id = 101 :nand id_flex_code = 'GL#' :na+
1nd id_flex_num = p_id_flex_num :norder by segment_num:
1L_LOV_NAME:
120000:
1L_LOV_QUERY:
1XXEIS:
1EIS_RS_LOVS:
1LOV_QUERY:
1L_LOV_DESCR:
1L_CREATED_BY:
1CREATED_BY:
1L_COA_NAME:
1FND_ID_FLEX_STRUCTURES_TL:
1ID_FLEX_STRUCTURE_NAME:
1L_LOV_SEGMENTS:
1L_ALIAS:
1L_APPLICATION_COLUMN_NAME:
1L_COA_ID:
1L_PRODUCT_RELEASE:
110:
1SUBSTR:
1RELEASE_NAME:
1FND_PRODUCT_GROUPS:
1SELECT SUBSTR(release_name,1,2) :n  into L_PRODUCT_RELEASE :n  FROM fnd_produ+
1ct_groups:
1=:
1IS NULL:
1I:
1LOOP:
1SELECT ffv.flex_value, ffvtl.description,decode(ffv.summary_flag,'Y', 'Parent+
1','N', 'Child') Type :n			FROM :n				fnd_flex_value_sets ffvs , :n				fnd_fle+
1x_values ffv, :n				fnd_flex_values_tl ffvtl :n			WHERE   upper(ffvs.flex_val+
1ue_set_name) = upper(':
1') :n			 and ffv.flex_value_set_id = ffvs.flex_value_set_id :n			 and ffv.FLE+
1X_VALUE_ID = ffvtl.FLEX_VALUE_ID :n			AND ffv.enabled_flag = upper('Y') :n			+
1AND ffv.summary_flag in ('Y','N') :n			AND ffvtl.LANGUAGE = USERENV('LANG') +
1:n			AND  xxeis.eis_gl_security_pkg.validate_segment_value( ':
1' , ffv.flex_value)='TRUE' :n			order by ffv.flex_value	:
1GL_:
1_:
1EIS_RS_INS:
1LOV:
1:
1GET_USER_NAME:
1FND_GLOBAL:
1USER_ID:
1SYSDATE:
112:
1MAX:
1EIS_GL_SECURITY_PKG:
1SELECT max(chart_of_accounts_id) :n		  into L_COA_ID :n		  from GL_SETS_OF_BO+
1OKS GSOB :n		  where XXEIS.EIS_GL_SECURITY_PKG.VALIDATE_ACCESS(GSOB.SET_OF_BO+
1OKS_ID) = 'TRUE':
1SELECT max(chart_of_accounts_id) :n		  INTO L_COA_ID :n		  from GL_SETS_OF_BO+
1OKS GSOB :n		  where set_of_books_id=p_sob_id:
1ROWNUM:
1SELECT ffs.application_column_name  :n		INTO l_application_column_name		   :n+
1		FROM fnd_id_flex_segments ffs , :n		     fnd_flex_value_sets ffvs :n		 WHER+
1E  ffs.id_flex_code = 'GL#'  :n		 AND 	ffs.application_id = 101 :n		 AND 	id_+
1flex_num	=	nvl(p_id_flex_num,l_coa_id)--hari :n		 AND    UPPER(ffvs.flex_valu+
1e_set_name) = UPPER(p_lov_vs_name) :n		 AND 	ffs.flex_value_set_id = ffvs.fle+
1x_value_set_id :n		 and  rownum<=1:
1SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,'Y', 'Paren+
1t','N', 'Child') Type :n				FROM :n					fnd_flex_value_sets ffvs , :n					fnd+
1_flex_values ffv, :n					fnd_flex_values_tl ffvtl :n				WHERE   upper(ffvs.fl+
1ex_value_set_name) = upper(':
1') :n				 and ffv.flex_value_set_id = ffvs.flex_value_set_id :n				 and ffv.F+
1LEX_VALUE_ID = ffvtl.FLEX_VALUE_ID :n				AND ffv.enabled_flag = upper('Y') :n+
1				AND ffv.summary_flag in ('Y','N') :n				AND ffvtl.LANGUAGE = USERENV('LAN+
1G') :n				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ':
1' , ffv.flex_value)='TRUE' :n				order by ffv.flex_value	:
1Y:
1FND_USER:
1USER_NAME:
1EIS_RS_UTILITY:
1GET_DEFAULT_ADMIN_USER:
1SELECT user_id :n	into l_created_by :n	from fnd_user :n	where user_name = nvl+
1(xxeis.eis_rs_ins.get_user_name(fnd_global.user_id),xxeis.eis_rs_utility.get_+
1default_admin_user):
1FLEX:
1LANGUAGE:
1USERENV:
1SELECT ID_FLEX_STRUCTURE_NAME :n		into l_coa_name :n		from fnd_id_flex_struct+
1ures_tl :n		where id_flex_code = 'GL#' :n		and application_id = 101 :n		and l+
1anguage = userenv('LANG') :n		and id_flex_num = flex.id_flex_num:
1_ACCOUNT_LOV:
1GL Account LOV for the Chart Of Accounts :
1SEG:
1EIS_RSC_UTILITY:
1REPLACE_CHARS:
1 :
1":
1,:
1CHR:
11:
1LENGTH:
12:
1SELECT CONCATENATED_SEGMENTS Account_String,:
1  FROM gl_code_combinations_kfv :n			 WHERE chart_of_accounts_id IN ( :n					+
1SELECT chart_of_accounts_id :n					  FROM gl_sets_of_books :n					 WHERE gl_s+
1ecurity_pkg.validate_access(set_of_books_id) = 'TRUE') :n			   and gl_securit+
1y_pkg.validate_access(null,code_combination_id) = 'TRUE'   :n			   and nvl(su+
1mmary_flag,'N') = upper('N') :n			order by CONCATENATED_SEGMENTS:
1P_APPLICATION_ID:
1P_LOV_QUERY:
1P_LOV_VALUES:
1P_LOV_NAME:
1P_CREATED_BY:
1P_CREATION_DATE:
1ROLLBACK:
1ROLLBACK_NR:
1GET_VENDOR_OR_SITE:
1G_VENDOR_OR_SITE:
1GET_DATE:
1DATE:
1G_START_DATE:
1GET_ACT_INACT:
1L_ACT_INACT:
1LOOKUP_CODE:
1FND_LOOKUPS:
1LOOKUP_TYPE:
1MEANING:
1G_ACT_INACT:
1SELECT lookup_code INTO l_Act_InAct :nFROM FND_LOOKUPS :nwhere lookup_type = +
1'YES_NO' AND  MEANING=g_Act_InAct:
1GET_BATCH_ID:
1L_BATCH_ID:
1L_RELEASE:
1L_QUERY:
11000:
1SELECT SUBSTR (release_name, 1, 2) :n        INTO l_release :n        FROM fn+
1d_product_groups:
1SELECT JE_BATCH_ID  :nFROM GL_JE_BATCHES :nWHERE JE_BATCH_ID IN :n(SELECT JE_+
1BATCH_ID :nFROM GL_JE_HEADERS JEH :nWHERE JE_SOURCE = 'Consolidation' :nAND E+
1XISTS (SELECT 'Has References' :nFROM GL_JE_LINES JEL :nWHERE JEL.JE_HEADER_I+
1D = JEH.JE_HEADER_ID :nAND   REFERENCE_1 IS NOT NULL :nAND   REFERENCE_2 IS N+
1OT NULL :nAND   REFERENCE_3 IS NOT NULL :nAND   REFERENCE_4 IS NOT NULL :nAND+
1   REFERENCE_5 IS NOT NULL):
1 and ledger_id = fnd_profile.VALUE('GL_SET_OF_BKS_ID')):
1 and set_of_books_id = fnd_profile.VALUE('GL_SET_OF_BKS_ID')):
1 AND NAME = ':
1G_BATCH_NAME:
1' ORDER BY NAME:
1EXECUTE:
1IMMEDIATE:
1GET_TXN_TYPE_ID:
1L_TRX_TYPE:
1SELECT CUST_TRX_TYPE_ID :nFROM RA_CUST_TRX_TYPES :nWHERE TYPE IN ( 'INV', 'DM+
1', 'CM') AND NAME = ::1:
1USING:
1G_TXN_TYPE:
1GET_BATCH_SRC_ID:
1L_BATCH_SOURCE_ID:
1 SELECT batch_source_id :nFROM ra_batch_sources :nWHERE NAME=::1:
1G_BATCH_SRC_NAME:
1GET_QUALIFIER_SEGNUM:
1APPL_ID:
1KEY_FLEX_CODE:
1STRUCTURE_NUMBER:
1FLEX_QUAL_NAME:
1THIS_SEGMENT_NUM:
1SEGMENT_NUMBER:
1S:
1FND_SEGMENT_ATTRIBUTE_VALUES:
1SAV:
1FND_SEGMENT_ATTRIBUTE_TYPES:
1SAT:
1ENABLED_FLAG:
1ATTRIBUTE_VALUE:
1SEGMENT_ATTRIBUTE_TYPE:
1UNIQUE_FLAG:
1SELECT s.segment_num INTO this_segment_num :n     FROM fnd_id_flex_segments s+
1, fnd_segment_attribute_values sav, :n     fnd_segment_attribute_types sat :n+
1     WHERE s.application_id = appl_id :n     AND s.id_flex_code = key_flex_co+
1de :n     AND s.id_flex_num = structure_number :n     AND s.enabled_flag = 'Y+
1' :n     AND s.application_column_name = sav.application_column_name :n     A+
1ND sav.application_id = appl_id :n     AND sav.id_flex_code = key_flex_code +
1:n     AND sav.id_flex_num = structure_number :n     AND sav.attribute_value +
1= 'Y' :n     AND sav.segment_attribute_type = sat.segment_attribute_type :n  +
1   AND sat.application_id = appl_id :n     AND sat.id_flex_code = key_flex_co+
1de :n     AND sat.unique_flag = 'Y' :n     AND sat.segment_attribute_type = f+
1lex_qual_name :n     AND '$Header:: AFFFAPIB.pls 115.9 2004/12/22 09::46::49 +
1vsoolapa ship $' IS NOT NULL:
1COUNT:
1SELECT count(segment_num) INTO segment_number :n     FROM fnd_id_flex_segment+
1s :n     WHERE application_id = appl_id :n     AND id_flex_code = key_flex_co+
1de :n     AND id_flex_num = structure_number :n     AND enabled_flag = 'Y' :n+
1     AND segment_num <= this_segment_num :n     AND '$Header:: AFFFAPIB.pls 1+
115.9 2004/12/22 09::46::49 vsoolapa ship $' IS NOT NULL:
1GET_ENCUMBRANCE_TYPE:
1L_ENCUMBRANCE_TYPE_ID:
1 SELECT ENCUMBRANCE_TYPE_ID :n    FROM GL_ALL_ENC_TYPES_VIEW :n    WHERE ENCU+
1MBRANCE_TYPE=nvl(::1,'ALL'):
1G_ENCUMBRANCE_TYPE:
1GET_TAX_RATE:
1L_TAX_RATE_SEGS:
1500:
1L_CUR_SEG:
150:
1L_SEG_DELIMITER:
14:
1L_STMT:
14000:
1CURSOR_TYPE:
1REF:
1C1:
1CONCATENATED_SEGMENT_DELIMITER:
1FND_DESCRIPTIVE_FLEXS:
1DESCRIPTIVE_FLEXFIELD_NAME:
1SELECT ''''||concatenated_segment_delimiter||'''' :n  into   l_seg_delimiter +
1:n  from   fnd_descriptive_flexs :n  where  descriptive_flexfield_name = 'AR_+
1SALES_TAX_RATES':
1SELECT 'to_char(ar.'||dfcu.application_column_name||')' :n     FROM   fnd_des+
1cr_flex_column_usages dfcu, :n            ar_system_parameters sp :n     WHER+
1E  dfcu.descriptive_flexfield_name = 'AR_SALES_TAX_RATES' :n     AND    dfcu.+
1application_id             = 222 :n     AND    dfcu.enabled_flag             +
1  = 'Y' :n     AND    dfcu.descriptive_flex_context_code = :n                +
1                        to_char(sp.location_structure_id) :n     ORDER  BY co+
1lumn_seq_num:
1FOUND:
1EXIT:
1GET_ACCOUNT_SEGMENT_NUMBER:
1SELECT max( segment.segment_num) :n    :n FROM fnd_flex_value_sets vs, :n    +
1 fnd_id_flex_segments segment, :n     fnd_flex_validation_tables tab, :n     +
1fnd_segment_attribute_values qual :n WHERE segment.application_id = 222 :n AN+
1D   segment.id_flex_code = 'RLOC' :n AND   segment.id_flex_num = (SELECT ar.l+
1ocation_structure_id :n                               FROM    gl_sets_of_book+
1s sob, :n                        	    ar_system_parameters ar :n             +
1                 WHERE   sob.set_of_books_id  = ar.set_of_books_id) :n :n :n +
1AND   tab.flex_value_set_id = vs.flex_value_set_id :n AND   tab.application_t+
1able_name = 'AR_LOCATION_VALUES' :n AND   segment.application_id = qual.appli+
1cation_id :n AND   segment.id_flex_code = qual.id_flex_code :n AND   segment.+
1id_flex_num  = qual.id_flex_num :n AND   segment.application_column_name = qu+
1al.application_column_name :n AND   segment.enabled_flag = 'Y' :n AND   qual.+
1attribute_value = 'Y' :n AND   qual.segment_attribute_type = 'TAX_ACCOUNT':
0

0
0
5a0
2
0 :2 a0 97 a0 8d 8f a0 b0
3d 8f a0 b0 3d 8f a0 b0
3d b4 :2 a0 2c 6a a3 a0 51
a5 1c 81 b0 :10 a0 12a :2 a0 5a
65 b7 :2 a0 4d 65 b7 a6 9
a0 53 a0 4d 65 b7 a6 9
a4 b1 11 68 4f a0 8d 8f
a0 b0 3d 8f a0 b0 3d 8f
a0 4d b0 3d b4 :2 a0 2c 6a
a0 f4 b4 bf c8 :19 a0 12a bd
b7 11 a4 b1 a3 a0 51 a5
1c 81 b0 a3 a0 51 a5 1c
81 b0 a0 4d d a0 7e b4
2e :2 a0 7e 6e b4 2e 7e a0
b4 2e 7e 6e b4 2e 7e a0
b4 2e d :3 a0 a5 b d b7
:2 a0 7e 6e b4 2e 7e a0 b4
2e d :3 a0 a5 b d b7 :2 19
3c b7 :3 a0 e9 dd b3 :2 a0 e9
d3 :2 a0 e9 c1 a0 7e b4 2e
:2 a0 7e 6e b4 2e 7e a0 b4
2e 7e 6e b4 2e 7e a0 b4
2e d :2 a0 a5 b a0 d b7
:2 a0 7e 6e b4 2e 7e a0 b4
2e d :2 a0 a5 b a0 d b7
:2 19 3c b7 a6 9 a4 b1 11
4f :2 a0 65 b7 a4 a0 b1 11
68 4f 9a 8f a0 4d b0 3d
8f a0 4d b0 3d 8f a0 6e
b0 3d 8f a0 4d b0 3d b4
55 6a a0 f4 b4 bf c8 :22 a0
12a bd b7 11 a4 b1 a0 f4
b4 bf c8 :8 a0 12a bd b7 11
a4 b1 a0 f4 8f :2 a0 6b :2 a0
f b0 3d b4 bf c8 :9 a0 12a
bd b7 11 a4 b1 a3 a0 51
a5 1c 81 b0 a3 :2 a0 6b a0
6b :2 a0 f 1c 81 b0 a3 :2 a0
6b a0 6b :2 a0 f 1c 81 b0
a3 :2 a0 6b a0 6b :2 a0 f 1c
81 b0 a3 :2 a0 6b :2 a0 f 1c
81 b0 a3 a0 51 a5 1c 81
b0 a3 a0 51 a5 1c 81 b0
a3 :2 a0 6b :2 a0 f 1c 81 b0
a3 a0 1c 81 b0 a3 a0 51
a5 1c 81 b0 :4 a0 12a a0 7e
6e b4 2e 5a a0 7e b4 2e
5a 91 :2 a0 37 a0 6e 7e :2 a0
6b b4 2e 7e 6e b4 2e 7e
:2 a0 6b b4 2e 7e 6e b4 2e
d a0 6e 7e :2 a0 6b b4 2e
7e 6e b4 2e 7e :2 a0 6b b4
2e d :2 a0 6b a0 6b 4d a0
6e :4 a0 6b a0 6b :2 a0 6b a5
b a0 a5 57 b7 a0 47 b7
a0 7e 6e b4 2e :a a0 12a b7
:7 a0 12a b7 :2 19 3c :19 a0 12a a0
6e 7e a0 b4 2e 7e 6e b4
2e 7e a0 b4 2e 7e 6e b4
2e d :2 a0 d :2 a0 6b a0 6b
4d a0 6e :4 a0 6b a0 6b :2 a0
6b a5 b a0 a5 57 b7 :2 19
3c b7 19 3c a0 7e 6e b4
2e 5a :d a0 12a 91 :2 a0 37 :a a0
12a a0 6e 7e :2 a0 6b b4 2e
7e 6e b4 2e d a0 6e 7e
a0 b4 2e d a0 4d d a0
4d d 91 :3 a0 6b a5 b a0
37 :3 a0 6b a0 6b :2 a0 6b a5
b d :2 a0 7e :2 a0 6b b4 2e
7e 6e b4 2e 7e 6e b4 2e
7e a0 b4 2e 7e 6e b4 2e
7e 6e b4 2e 7e a0 51 a5
b b4 2e d b7 a0 47 :3 a0
51 :2 a0 a5 b 7e 51 b4 2e
a5 b 7e a0 51 a5 b b4
2e d a0 6e 7e a0 51 a5
b b4 2e 7e a0 b4 2e 7e
6e b4 2e d :2 a0 6b a0 6b
a0 4d e :2 a0 e a0 6e e
:2 a0 e :2 a0 e :2 a0 e :2 a0 e
a5 57 b7 a0 47 b7 19 3c
b7 a0 53 a0 57 a0 b4 e9
b7 a6 9 a4 a0 b1 11 68
4f a0 8d a0 b4 a0 2c 6a
:2 a0 65 b7 a4 a0 b1 11 68
4f a0 8d a0 b4 a0 2c 6a
:2 a0 65 b7 a4 a0 b1 11 68
4f a0 8d a0 b4 a0 2c 6a
a3 a0 51 a5 1c 81 b0 :6 a0
12a :2 a0 65 b7 a4 a0 b1 11
68 4f a0 8d a0 b4 a0 2c
6a a3 a0 1c 81 b0 a3 a0
51 a5 1c 81 b0 a3 a0 51
a5 1c 81 b0 :4 a0 12a a0 6e
d a0 7e 6e b4 2e :2 a0 7e
6e b4 2e d b7 :2 a0 7e 6e
b4 2e d b7 :2 19 3c :2 a0 7e
6e b4 2e 7e a0 b4 2e 7e
6e b4 2e d :4 a0 11e 11d :2 a0
65 b7 a4 a0 b1 11 68 4f
a0 8d a0 b4 a0 2c 6a a3
a0 1c 81 b0 :2 a0 6e :3 a0 112
11e 11a 11d :2 a0 65 b7 a4 b1
11 68 4f a0 8d a0 b4 a0
2c 6a a3 a0 1c 81 b0 :2 a0
6e :3 a0 112 11e 11a 11d :2 a0 65
b7 a4 a0 b1 11 68 4f a0
8d 8f a0 b0 3d 8f a0 b0
3d 8f a0 b0 3d 8f a0 b0
3d b4 :2 a0 2c 6a a3 a0 1c
81 b0 a3 a0 1c 81 b0 :32 a0
12a :d a0 12a :2 a0 5a 65 b7 a0
53 a0 4d 5a 65 b7 a6 9
a4 a0 b1 11 68 4f a0 8d
a0 b4 a0 2c 6a a3 a0 51
a5 1c 81 b0 :2 a0 6e :3 a0 112
11e 11a 11d :2 a0 65 b7 a4 a0
b1 11 68 4f a0 8d a0 b4
a0 2c 6a a3 a0 51 a5 1c
6e 81 b0 a3 a0 51 a5 1c
6e 81 b0 a3 a0 51 a5 1c
6e 81 b0 a3 a0 51 a5 1c
81 b0 a0 9d :2 a0 c8 77 a3
a0 1c 81 b0 :4 a0 12a a0 6e
d :3 a0 11c 11d :3 a0 e9 d3 :2 a0
f a0 7e b4 2e :2 a0 7e 6e
b4 2e 7e a0 b4 2e 7e 6e
b4 2e 7e a0 b4 2e d b7
:2 a0 d b7 :2 19 3c b7 a0 2b
b7 :2 19 3c b7 a0 47 :2 a0 e9
c1 :2 a0 65 b7 a0 53 a0 4d
65 b7 a6 9 a4 b1 11 68
4f a0 8d a0 b4 a0 2c 6a
a3 a0 1c 81 b0 :2 a0 6e a0
11e 11d :2 a0 65 b7 a0 53 a0
4d 65 b7 a6 9 a4 a0 b1
11 68 4f b1 b7 a4 11 b1
56 4f 1d 17 b5 
5a0
2
0 3 7 b 15 19 35 31
30 3d 4a 46 2d 52 5b 57
45 63 42 68 6c 70 74 91
7c 80 83 84 8c 7b 98 9c
a0 a4 a8 ac b0 b4 b8 bc
c0 c4 c8 cc d0 d4 d8 e4
e8 78 ec f0 f2 f6 fa fb
ff 101 102 107 1 10b 10f 110
114 116 117 11c 120 122 12e 132
134 138 154 150 14f 15c 169 165
14c 171 17b 176 17a 164 183 161
188 18c 190 194 198 19c 1ad 1ae
1b1 1b5 1b9 1bd 1c1 1c5 1c9 1cd
1d1 1d5 1d9 1dd 1e1 1e5 1e9 1ed
1f1 1f5 1f9 1fd 201 205 209 20d
211 215 219 225 22a 22c 238 23c
257 242 246 249 24a 252 241 274
262 23e 266 267 26f 261 27b 25e
27f 283 287 28a 28b 290 294 298
29b 2a0 2a1 2a6 2a9 2ad 2ae 2b3
2b6 2bb 2bc 2c1 2c4 2c8 2c9 2ce
2d2 2d6 2da 2de 2df 2e1 2e5 2e7
2eb 2ef 2f2 2f7 2f8 2fd 300 304
305 30a 30e 312 316 31a 31b 31d
321 323 327 32b 32e 330 334 338
33c 341 345 346 34a 34e 353 358
35c 360 365 367 36b 36e 36f 374
378 37c 37f 384 385 38a 38d 391
392 397 39a 39f 3a0 3a5 3a8 3ac
3ad 3b2 3b6 3ba 3be 3bf 3c1 3c5
3c9 3cb 3cf 3d3 3d6 3db 3dc 3e1
3e4 3e8 3e9 3ee 3f2 3f6 3fa 3fb
3fd 401 405 407 40b 40f 412 414
415 41a 41e 420 42c 42e 432 436
43a 43c 440 444 446 452 456 458
475 470 474 46f 47d 48a 486 46c
485 492 4a4 49b 49f 482 4ac 4b6
4b1 4b5 49a 4be 497 4c3 4c7 4cb
4cf 4e0 4e1 4e4 4e8 4ec 4f0 4f4
4f8 4fc 500 504 508 50c 510 514
518 51c 520 524 528 52c 530 534
538 53c 540 544 548 54c 550 554
558 55c 560 564 568 56c 570 57c
581 583 58f 593 595 599 5aa 5ab
5ae 5b2 5b6 5ba 5be 5c2 5c6 5ca
5ce 5d2 5de 5e3 5e5 5f1 5f5 5f7
5fb 628 610 614 618 61b 61f 623
60f 630 60c 635 638 63c 640 644
648 64c 650 654 658 65c 660 66c
671 673 67f 683 69e 689 68d 690
691 699 688 6d2 6a9 6ad 685 6b1
6b5 6b8 6bc 6c0 6c5 6cd 6a8 706
6dd 6e1 6a5 6e5 6e9 6ec 6f0 6f4
6f9 701 6dc 73a 711 715 6d9 719
71d 720 724 728 72d 735 710 767
745 749 70d 74d 751 755 75a 762
744 784 772 741 776 777 77f 771
7a1 78f 76e 793 794 79c 78e 7ce
7ac 7b0 78b 7b4 7b8 7bc 7c1 7c9
7ab 7ea 7d9 7dd 7e5 7a8 806 7f1
7f5 7f8 7f9 801 7d8 80d 811 815
819 81d 829 7d5 82d 832 833 838
83b 83f 842 843 848 84b 84f 853
857 859 85d 862 865 869 86d 870
871 876 879 87e 87f 884 887 88b
88f 892 893 898 89b 8a0 8a1 8a6
8aa 8ae 8b3 8b6 8ba 8be 8c1 8c2
8c7 8ca 8cf 8d0 8d5 8d8 8dc 8e0
8e3 8e4 8e9 8ed 8f1 8f5 8f8 8fc
8ff 900 904 909 90d 911 915 919
91c 920 923 927 92b 92e 92f 931
935 936 93b 93d 941 948 94a 94e
951 956 957 95c 960 964 968 96c
970 974 978 97c 980 984 990 992
996 99a 99e 9a2 9a6 9aa 9ae 9ba
9bc 9c0 9c4 9c7 9cb 9cf 9d3 9d7
9db 9df 9e3 9e7 9eb 9ef 9f3 9f7
9fb 9ff a03 a07 a0b a0f a13 a17
a1b a1f a23 a27 a2b a37 a3b a40
a43 a47 a48 a4d a50 a55 a56 a5b
a5e a62 a63 a68 a6b a70 a71 a76
a7a a7e a82 a86 a8a a8e a91 a95
a98 a99 a9d aa2 aa6 aaa aae ab2
ab5 ab9 abc ac0 ac4 ac7 ac8 aca
ace acf ad4 ad6 ada ade ae1 ae3
ae7 aea aee af1 af6 af7 afc aff
b03 b07 b0b b0f b13 b17 b1b b1f
b23 b27 b2b b2f b33 b3f b43 b47
b4b b4d b51 b55 b59 b5d b61 b65
b69 b6d b71 b75 b81 b85 b8a b8d
b91 b95 b98 b99 b9e ba1 ba6 ba7
bac bb0 bb4 bb9 bbc bc0 bc1 bc6
bca bce bcf bd3 bd7 bd8 bdc be0
be4 be8 bec bef bf0 bf2 bf6 bf8
bfc c00 c04 c07 c0b c0e c12 c16
c19 c1a c1c c20 c24 c28 c2b c2f
c33 c36 c37 c3c c3f c44 c45 c4a
c4d c52 c53 c58 c5b c5f c60 c65
c68 c6d c6e c73 c76 c7b c7c c81
c84 c88 c8b c8c c8e c8f c94 c98
c9a c9e ca5 ca9 cad cb1 cb4 cb8
cbc cbd cbf cc2 cc5 cc6 ccb ccc
cce cd1 cd5 cd8 cd9 cdb cdc ce1
ce5 ce9 cee cf1 cf5 cf8 cf9 cfb
cfc d01 d04 d08 d09 d0e d11 d16
d17 d1c d20 d24 d28 d2b d2f d32
d36 d37 d39 d3d d41 d43 d47 d4c
d4e d52 d56 d58 d5c d60 d62 d66
d6a d6c d70 d74 d76 d77 d7c d7e
d82 d89 d8b d8f d92 d94 1 d98
d9c da1 da5 da6 dab dad dae db3
db7 dbb dbd dc9 dcd dcf dd3 de7
deb dec df0 df4 df8 dfc e00 e04
e06 e0a e0e e10 e1c e20 e22 e26
e3a e3e e3f e43 e47 e4b e4f e53
e57 e59 e5d e61 e63 e6f e73 e75
e79 e8d e91 e92 e96 e9a eb7 ea2
ea6 ea9 eaa eb2 ea1 ebe ec2 ec6
eca ece ed2 ed6 ee2 ee6 eea e9e
eee ef2 ef6 ef8 f04 f08 f0a f0e
f22 f26 f27 f2b f2f f48 f37 f3b
f43 f36 f65 f53 f33 f57 f58 f60
f52 f82 f70 f4f f74 f75 f7d f6f
f89 f8d f91 f95 f99 fa5 fa9 fae
fb2 f6c fb6 fbb fbc fc1 fc5 fc9
fcc fd1 fd2 fd7 fdb fdd fe1 fe5
fe8 fed fee ff3 ff7 ff9 ffd 1001
1004 1008 100c 100f 1014 1015 101a 101d
1021 1022 1027 102a 102f 1030 1035 1039
103d 1041 1045 1049 104e 1052 1056 105a
105e 1060 1064 1068 106a 1076 107a 107c
1080 1094 1098 1099 109d 10a1 10ba 10a9
10ad 10b5 10a8 10c1 10c5 10c9 10ce 10d2
10d6 10a5 10da 10df 10e0 10e4 10e8 10ec
10f0 10f2 10f6 10f8 1104 1108 110a 110e
1122 1126 1127 112b 112f 1148 1137 113b
1143 1136 114f 1153 1157 115c 1160 1164
1133 1168 116d 116e 1172 1176 117a 117e
1180 1184 1188 118a 1196 119a 119c 11a0
11bc 11b8 11b7 11c4 11d1 11cd 11b4 11d9
11e2 11de 11cc 11ea 11f7 11f3 11c9 11ff
11f2 1204 1208 120c 1210 1229 1218 121c
1224 11ef 1241 1230 1234 123c 1217 1248
124c 1250 1254 1258 125c 1260 1264 1268
126c 1270 1274 1278 127c 1280 1284 1288
128c 1290 1294 1298 129c 12a0 12a4 12a8
12ac 12b0 12b4 12b8 12bc 12c0 12c4 12c8
12cc 12d0 12d4 12d8 12dc 12e0 12e4 12e8
12ec 12f0 12f4 12f8 12fc 1300 1304 1308
130c 1310 131c 1320 1324 1328 132c 1330
1334 1338 133c 1340 1344 1348 134c 1350
135c 1360 1214 1364 1368 136a 1 136e
1372 1373 1376 137a 137c 137d 1382 1386
138a 138c 1398 139c 139e 13a2 13b6 13ba
13bb 13bf 13c3 13e0 13cb 13cf 13d2 13d3
13db 13ca 13e7 13eb 13ef 13f4 13f8 13fc
13c7 1400 1405 1406 140a 140e 1412 1416
1418 141c 1420 1422 142e 1432 1434 1438
144c 1450 1451 1455 1459 147b 1461 1465
1468 1469 1471 1476 1460 149d 1486 145d
148a 148b 1493 1498 1485 14bf 14a8 1482
14ac 14ad 14b5 14ba 14a7 14dc 14ca 14a4
14ce 14cf 14d7 14c9 14e3 14f7 14eb 14ef
14f3 14e7 1513 1502 1506 150e 14c6 14fe
151a 151e 1522 1526 1532 1536 153b 153f
1543 1547 154b 154e 1552 1556 155a 155e
1563 1568 156c 1570 1575 1579 157c 157d
1582 1586 158a 158d 1592 1593 1598 159b
159f 15a0 15a5 15a8 15ad 15ae 15b3 15b6
15ba 15bb 15c0 15c4 15c6 15ca 15ce 15d2
15d4 15d8 15dc 15df 15e1 15e5 15eb 15ed
15f1 15f5 15f8 15fa 15fe 1605 1609 160d
1612 1614 1618 161c 1620 1622 1 1626
162a 162b 162f 1631 1632 1637 163b 163d
1649 164d 164f 1653 1667 166b 166c 1670
1674 168d 167c 1680 1688 167b 1694 1698
169c 16a1 16a5 16aa 16ae 16b2 16b6 1678
16ba 1 16be 16c2 16c3 16c7 16c9 16ca
16cf 16d3 16d7 16d9 16e5 16e9 16eb 16ed
16ef 16f3 16ff 1701 1704 1706 1707 1710 
5a0
2
0 1 9 e 4 d 7 24
:3 7 24 :3 7 24 :2 7 25 7 e
:2 4 7 17 21 20 :2 17 7 e
12 :2 e 21 26 3f e 22 e
12 1f 26 e 12 28 :2 7 f
e 7 4 c a 11 a :3 7
:2 c a 11 a :3 7 :5 4 3 c
13 26 :2 13 4 17 :3 4 17 28
:2 4 11 3 a :3 3 a 0 :2 3
a e a 1e a 1d a f
25 2a a f 23 27 a e
1b a e 12 2b 2f 3c 40
44 :7 3 b 14 13 :2 b :2 3 10
19 18 :2 10 3 5 f 5 :4 c
9 18 1e 20 :2 18 23 25 :2 18
31 33 :2 18 36 38 :2 18 :2 9 13
23 :2 13 9 26 b 1a 20 22
:2 1a 25 27 :2 1a :2 b 15 25 :2 15
b :4 9 5 a 9 f :3 9 f
23 :3 9 f :2 9 :5 d 1c 22 24
:2 1c 27 29 :2 1c 35 37 :2 1c 3a
3c :2 1c :2 d 1d :2 d 2e d 27
c 1b 21 23 :2 1b 26 28 :2 1b
:2 c 1c :2 c 2d c 9 :3 a :4 5
:3 3 5 c 5 :2 3 7 :4 3 b
1 d 1c :3 1 11 22 :2 1 2
1c 2d :3 2 10 1f :2 2 :4 1 8
0 :2 1 2 6 2 6 2 6
2 6 2 6 2 7 5 1a
3 17 6 a 23 27 6 a
1e 23 6 a 21 26 21 32
21 31 41 46 :7 1 8 0 :2 1
8 6 7 5 13 17 25 5
:7 1 8 17 25 3a 25 :2 46 25
:2 17 16 :2 1 8 14 2d 6 :3 5
13 a :7 1 c 15 14 :2 c :2 1
d 13 d 1f d :2 29 :3 d :2 1
d 13 d 1f d :2 2b :3 d :2 1
11 17 11 23 11 :2 2e :3 11 :2 1
d 27 d :2 3e :3 d :2 1 11 1a
19 :2 11 :2 1 11 1a 19 :2 11 :2 1
1b 30 1b :2 48 :3 1b :2 1 :3 f :2 1
15 1e 1d :2 15 1 8 f :2 8
1 5 1f 21 :2 1f 4 :4 6 4
7 c :3 3 4 37 39 :2 3b :2 4
4e 50 :2 4 3e 40 :2 42 :2 4 59
5b :2 4 3 4 12 17 19 :2 1b
:2 12 26 28 :2 12 2b 2d :2 2f :2 12
:2 4 :2 a :2 15 1a 1f 2b 2e 39
44 :2 4a :2 55 63 :2 6e :2 44 77 :2 4
3 7 3 1d 4 16 18 :2 16
c 10 :2 a 1b b 11 25 35
3a 5 1d d 11 :2 a 1b b
1b 6 2 :3 1 a e :2 8 1d
8 1c b f 9 d 9 17
1b 29 b 11 16 2d 33 9
d 21 26 9 :3 3 38 3a :2 3
47 49 :2 3 3f 41 :2 3 5a 5c
:4 3 10 :2 3 :2 9 :2 14 19 1f 2b
2e 39 44 :2 4a :2 55 63 :2 6e :2 44
77 :2 3 :4 1 26 :2 1 6 20 22
:2 20 4 9 :2 7 8 14 18 1e
29 37 42 4b 51 60 2 6
e :2 2 a :2 8 9 :2 7 12 7
15 1a :2 3 11 16 18 :2 1d :2 11
28 2a :2 11 :2 3 12 3d 3f :2 12
:2 3 13 :2 3 15 3 7 e 1d
:2 22 :2 e :2 3 4 f :2 15 :2 25 33
:2 37 :2 f :2 4 16 24 26 :2 2a :2 16
41 43 :2 16 46 48 :2 16 4b 4d
:2 16 54 56 :2 16 59 5b :2 16 5e
60 64 :2 60 :2 16 4 3 7 :2 3
15 1c 2b 2d 34 :2 2d 43 44
:2 2d :2 15 46 48 4c :2 48 :2 15 :2 3
4 32 34 38 :2 34 :2 4 3b :3 4
12 6 :2 4 :2 3 :2 9 :2 14 7 20
:2 7 20 :2 7 20 :2 7 20 :2 7 20
:2 7 1f :2 7 1f 7 :2 3 2 6
2 27 :3 1 :2 6 :5 5 d :3 1 5
:5 1 a 1d 0 24 :3 1 8 :3 1
5 :5 1 a 13 0 1a :3 1 8
:3 1 5 :5 1 a 18 0 1f :3 1
d 16 15 :2 d 1 8 19 6
7 23 2b :2 1 8 :3 1 6 :5 1
a 17 0 1e :3 1 :3 c :2 1 b
14 13 :2 b :2 1 9 12 11 :2 9
1 e 16 :2 e :2 7 a 7 a
14 16 :2 14 a 10 d :3 10 a
7 a 10 d :3 10 a :5 7 e
16 19 :2 e 2a 2d :2 e 3a 3d
:2 e :2 7 f :2 19 :3 7 e 7 :2 1
5 :5 1 a 1a 0 21 :3 1 :3 c
:2 1 9 13 3f 4a 50 4a :4 1
8 :8 1 a 1b 0 22 :3 1 :3 14
:2 1 9 13 16 28 2e 28 :4 1
8 :3 1 5 :5 1 a 1f 34 :2 1f
a 1f :3 a 1f :3 a 1f :2 a 1e
3 a :2 1 6 :3 17 :2 6 :3 19 6
b d 1e b 20 23 40 6
22 c e 1f a c 1b a
c 1a a c a c 26 2a
a e 1f a e 1d a e
1c a e a e 27 2b a
e 1f a e 1d a e a
e 27 4 b 11 23 b c
1d a 19 a 18 :2 a 19 :2 4
b a 4 1 :2 9 7 e d
7 10 :2 4 1 5 :5 1 a 1f
0 26 :3 1 17 20 1f :2 17 1
2 a 14 33 49 4f 49 :3 2
5 c 5 :2 1 5 :5 1 a 17
0 1e :2 1 3 13 1c 1b 13
24 13 :2 3 13 1c 1b 13 24
13 :2 3 13 1c 1b 13 24 13
:2 3 a 13 12 :2 a :2 3 8 17
1b 17 :2 3 :3 6 3 10 :3 a :2 3
d :2 3 8 :2 f :2 3 b 13 :2 5
8 b 8 :5 a 1d 2c 2e :2 1d
32 34 :2 1d 43 45 :2 1d 49 4b
:2 1d a 26 a 1d a :4 7 11
:2 7 :4 5 3 7 1 3 9 :3 3
a 3 1 :2 8 14 1b 14 f
:2 3 2 :4 1 2 b 27 0 2e
:2 2 4 :3 10 4 2 a 14 3e
:3 2 9 :2 2 :2 11 4 b 4 18
:2 c 2 6 :4 2 :4 4 :6 1 
5a0
4
0 :3 1 :2 21 :4 22
:4 23 :4 24 21 :2 26
:2 21 :7 28 :2 2b 2c
:4 2d :2 2e :4 2f :3 30
2b :4 32 2a 34
:3 36 35 :2 34 :2 37
:3 39 38 :2 37 33
:4 21 :2 48 :4 49 :4 4a
:5 4b 49 :2 4d :2 48
:2 4f 0 :2 4f :2 50
:2 51 :2 52 :4 53 :4 54
:3 56 :8 57 50 :5 4f
:7 5a :7 5b :3 60 :4 64
:13 66 :6 67 64 :b 6a
:6 6b 69 :3 64 63
70 :5 72 :4 73 :4 74
:4 75 :13 76 :6 77 75
:b 7a :6 7b 79 :3 75
71 :2 70 6f :3 5d
:3 80 :2 5d 82 :4 48
90 :5 92 :5 93 :5 94
:5 95 91 :2 90 :2 9f
0 :2 9f :2 a4 :2 a5
:2 a6 :2 a7 :2 a8 :2 a9
:2 ab :2 ac :4 b0 :4 b1
:4 b2 :2 b3 :4 b4 a1
:5 9f :2 b7 0 :2 b7
b8 b9 ba :4 bb
bc b8 :5 b7 :e be
:3 bf c0 c2 c3
:2 c4 c5 bf :5 be
:7 c7 :c c8 :c c9 :c ca
:a cb :7 cc :7 cd :a ce
:5 cf :7 d0 :2 d3 d4
d5 d3 :6 d7 :5 d9
:2 db dc db dd
de :4 e3 :2 de :2 e3
:2 de :4 e9 :2 de :2 e9
:2 de dd :13 ec :17 ee
dc ef db d9
:5 f3 :2 f5 f6 :2 f7
:5 f8 f5 f3 :2 fc
fd :2 fe :2 ff fc
fb :3 f3 :2 103 104
:2 105 :2 106 :2 107 :2 108
:4 109 :5 10a :4 10b 10c
103 10e 10f :2 114
:2 10f :2 114 :2 10f :2 11a
:2 10f :2 11a :2 10f 10e
:3 11e :17 11f f1 :3 d9
:3 d7 :6 126 128 129
12a :a 12b 128 :2 12e
12f 12e 131 132
133 134 135 :2 136
:3 137 131 :d 13a :7 13b
:3 13f :3 140 :7 144 145
144 :c 146 :24 147 145
148 144 :16 14c 14f
:9 150 151 :2 150 151
152 :2 150 14f :5 15d
:3 15f :3 160 :3 161 :3 162
:3 163 :3 164 :3 165 :2 15d
12f 168 12e :3 126
d1 :2 16e :5 16f :3 16e
16d 170 :4 90 :3 179
0 :3 179 :3 17c :2 17b
17d :4 179 :3 17e 0
:3 17e :3 181 :2 180 182
:4 17e :3 183 0 :3 183
:7 185 :2 187 188 :3 189
187 :3 18a :2 186 18c
:4 183 :3 18d 0 :3 18d
:5 18f :7 190 :7 191 :2 193
194 195 193 197
198 197 :5 1a7 1a9
1aa :2 1ab :2 1aa 1a9
1a8 1ad 1ae :2 1af
:2 1ae 1ad 1ac :3 1a7
1b2 :d 1b3 1b2 :3 1b5
1b6 :2 1b5 :3 1b8 :2 192
1ba :4 18d :3 1bb 0
:3 1bb :5 1bd :3 1bf :4 1c1
:3 1bf :3 1c2 :2 1be :4 1bb
:3 1c5 0 :3 1c5 :5 1c7
:3 1c9 :4 1cb :3 1c9 :3 1cd
:2 1c8 1ce :4 1c5 :6 1cf
:4 1d0 :4 1d1 :4 1d2 1cf
:2 1d4 :2 1cf :5 1d6 :5 1d7
:3 1d9 :4 1da :2 1db :3 1dc
:3 1dd :3 1de :2 1df :4 1e0
:3 1e1 :3 1e2 :3 1e3 :2 1e4
:4 1e5 :3 1e6 :3 1e7 :2 1e8
:3 1e9 1d9 :3 1ec 1ed
:2 1ee :2 1ef :2 1f0 1f1
:2 1f2 1ec :4 1f5 1d8
:2 1f7 :4 1f8 :3 1f7 1f6
1f9 :4 1cf :3 1fa 0
:3 1fa :7 1fc :3 1fe :4 200
:3 1fe :3 201 :2 1fd 202
:4 1fa :3 204 0 :3 204
:8 207 :8 208 :8 209 :7 20a
:6 20b :5 20c 21a 21b
21c 21d 21a :3 21f
:5 228 22b :4 22c :3 22e
:4 22f :13 230 22f :3 232
231 :3 22f 22e :2 235
234 :3 22e 22b 237
219 :4 238 :3 23a 219
:8 23d 23c :4 204 :3 241
0 :3 241 :5 243 :3 246
25c :2 246 :3 25e 245
:2 25f :3 260 :4 25f 261
:4 241 :4 21 :6 1 
1712
4
:3 0 1 :3 0 2
:3 0 3 :6 0 1
:2 0 4 :3 0 5
:a 0 46 2 :7 0
5 42 0 3
7 :3 0 6 :7 0
8 7 :3 0 9
:2 0 7 9 :3 0
8 :7 0 c b
:3 0 7 :3 0 a
:7 0 10 f :3 0
b :3 0 9 :3 0
12 14 0 46
5 15 :2 0 30
:2 0 f 9 :3 0
d :2 0 d 18
1a :6 0 1d 1b
0 44 0 c
:6 0 e :3 0 f
:3 0 c :3 0 10
:3 0 e :3 0 11
:3 0 12 :3 0 13
:3 0 6 :3 0 e
:3 0 14 :3 0 15
:3 0 8 :3 0 12
:3 0 16 :3 0 a
:4 0 17 1 :8 0
33 b :3 0 c
:3 0 31 :2 0 33
11 45 18 :3 0
b :4 0 36 :2 0
38 14 3a 16
39 38 :2 0 43
19 :3 0 b :4 0
3e :2 0 40 18
42 1a 41 40
:2 0 43 1c :2 0
45 1f 45 44
33 43 :6 0 46
1 0 5 15
45 59a :2 0 4
:3 0 1a :a 0 115
3 :7 0 23 161
0 21 9 :3 0
1b :7 0 4c 4b
:3 0 27 :2 0 25
9 :3 0 1c :7 0
50 4f :3 0 9
:4 0 1d :7 0 55
53 54 :2 0 b
:3 0 9 :3 0 57
59 0 115 49
5a :2 0 1e :3 0
1f :a 0 4 7b
:5 0 5d 60 0
5e :3 0 e :3 0
f :3 0 20 :3 0
21 :3 0 10 :3 0
e :3 0 21 :3 0
22 :3 0 23 :3 0
1c :3 0 21 :3 0
13 :3 0 e :3 0
13 :3 0 e :3 0
14 :3 0 1b :3 0
24 :3 0 e :3 0
25 :3 0 24 :3 0
1d :3 0 24 :3 0
e :3 0 25 :4 0
26 1 :8 0 7c
5d 60 7d 0
113 2b 7d 7f
7c 7e :6 0 7b
1 :6 0 7d 2a
:2 0 2f 9 :3 0
28 :2 0 2d 81
83 :6 0 86 84
0 113 0 27
:9 0 33 9 :3 0
31 88 8a :6 0
8d 8b 0 113
0 29 :6 0 27
:3 0 8e 8f 0
110 1d :3 0 2b
:2 0 35 92 93
:3 0 29 :3 0 1c
:3 0 2c :2 0 2d
:4 0 37 97 99
:3 0 2c :2 0 1d
:3 0 3a 9b 9d
:3 0 2c :2 0 2d
:4 0 3d 9f a1
:3 0 2c :2 0 1b
:3 0 40 a3 a5
:3 0 95 a6 0
ae 27 :3 0 2e
:3 0 29 :3 0 43
a9 ab a8 ac
0 ae 45 c1
29 :3 0 1c :3 0
2c :2 0 2d :4 0
48 b1 b3 :3 0
2c :2 0 1b :3 0
4b b5 b7 :3 0
af b8 0 c0
27 :3 0 2e :3 0
29 :3 0 4e bb
bd ba be 0
c0 50 c2 94
ae 0 c3 0
c0 0 c3 53
0 c4 56 10b
18 :3 0 2f :3 0
1f :4 0 c9 :2 0
106 c7 ca :3 0
1f :3 0 27 :4 0
ce :2 0 106 cb
cc :3 0 30 :3 0
1f :4 0 d2 :2 0
106 d0 0 1d
:3 0 2b :2 0 58
d4 d5 :3 0 29
:3 0 1c :3 0 2c
:2 0 2d :4 0 5a
d9 db :3 0 2c
:2 0 1d :3 0 5d
dd df :3 0 2c
:2 0 2d :4 0 60
e1 e3 :3 0 2c
:2 0 1b :3 0 63
e5 e7 :3 0 d7
e8 0 f0 2e
:3 0 29 :3 0 66
ea ec 27 :3 0
ed ee 0 f0
68 103 29 :3 0
1c :3 0 2c :2 0
2d :4 0 6b f3
f5 :3 0 2c :2 0
1b :3 0 6e f7
f9 :3 0 f1 fa
0 102 2e :3 0
29 :3 0 71 fc
fe 27 :3 0 ff
100 0 102 73
104 d6 f0 0
105 0 102 0
105 76 0 106
79 108 7e 107
106 :2 0 109 80
:2 0 10b 0 10b
10a c4 109 :6 0
110 3 :3 0 b
:3 0 27 :3 0 10e
:2 0 110 82 114
:3 0 114 1a :3 0
86 114 113 110
111 :6 0 115 1
0 49 5a 114
59a :2 0 31 :a 0
37b 6 :a 0 8a
7 :4 0 32 :7 0
11b 119 11a :2 0
8e 497 0 8c
9 :3 0 33 :7 0
120 11e 11f :2 0
92 :2 0 90 9
:3 0 35 :4 0 34
:7 0 125 123 124
:2 0 7 :4 0 36
:7 0 12a 128 129
:2 0 12c :2 0 37b
117 12d :2 0 1e
:3 0 37 :a 0 7
157 :5 0 130 133
0 131 :3 0 38
:3 0 39 :3 0 38
:3 0 3a :3 0 38
:3 0 3b :3 0 38
:3 0 3c :3 0 38
:3 0 13 :3 0 21
:3 0 22 :3 0 3d
:3 0 38 :3 0 20
:3 0 21 :3 0 38
:3 0 3e :3 0 38
:3 0 3f :3 0 38
:3 0 13 :3 0 21
:3 0 13 :3 0 38
:3 0 3b :3 0 40
:3 0 41 :3 0 42
:3 0 40 :3 0 43
:3 0 44 :3 0 40
:3 0 45 :4 0 46
1 :8 0 158 130
133 159 0 379
97 159 15b 158
15a :6 0 157 1
:6 0 159 1e :3 0
47 :a 0 8 16a
:5 0 15d 160 0
15e :3 0 3b :3 0
48 :3 0 3f :3 0
3b :3 0 24 :3 0
36 :3 0 3b :3 0
3e :4 0 49 1
:8 0 16b 15d 160
16c 0 379 99
16c 16e 16b 16d
:6 0 16a 1 :6 0
16c 1e :3 0 4a
:a 0 9 187 :4 0
9d :2 0 9b 3d
:3 0 3b :2 0 4
172 173 0 4b
:3 0 4b :2 0 1
174 176 :3 0 36
:7 0 178 177 :3 0
170 17c 0 17a
:3 0 3b :3 0 39
:3 0 3a :3 0 3d
:3 0 3f :3 0 3e
:3 0 3b :3 0 36
:3 0 3c :4 0 4c
1 :8 0 188 170
17c 189 0 379
9f 189 18b 188
18a :6 0 187 1
:6 0 189 194 195
0 a3 9 :3 0
4e :2 0 a1 18d
18f :6 0 192 190
0 379 0 4d
:6 0 1a0 1a1 0
a5 50 :3 0 51
:2 0 4 52 :2 0
4 196 197 0
4b :3 0 4b :2 0
1 198 19a :3 0
19b :7 0 19e 19c
0 379 0 4f
:6 0 1ac 1ad 0
a7 50 :3 0 51
:2 0 4 f :2 0
4 1a2 1a3 0
4b :3 0 4b :2 0
1 1a4 1a6 :3 0
1a7 :7 0 1aa 1a8
0 379 0 53
:6 0 1b8 1b9 0
a9 50 :3 0 51
:2 0 4 55 :2 0
4 1ae 1af 0
4b :3 0 4b :2 0
1 1b0 1b2 :3 0
1b3 :7 0 1b6 1b4
0 379 0 54
:6 0 4e :2 0 ab
57 :3 0 58 :2 0
4 4b :3 0 4b
:2 0 1 1ba 1bc
:3 0 1bd :7 0 1c0
1be 0 379 0
56 :6 0 4e :2 0
af 9 :3 0 ad
1c2 1c4 :6 0 1c7
1c5 0 379 0
59 :6 0 1d0 1d1
0 b3 9 :3 0
b1 1c9 1cb :6 0
1ce 1cc 0 379
0 5a :6 0 b7
7d5 0 b5 3d
:3 0 39 :2 0 4
4b :3 0 4b :2 0
1 1d2 1d4 :3 0
1d5 :7 0 1d8 1d6
0 379 0 5b
:6 0 63 :2 0 bb
7 :3 0 1da :7 0
1dd 1db 0 379
0 5c :6 0 9
:3 0 5e :2 0 b9
1df 1e1 :6 0 1e4
1e2 0 379 0
5d :6 0 5f :3 0
60 :3 0 5d :3 0
61 :4 0 62 1
:8 0 36c 34 :3 0
35 :4 0 bf 1eb
1ed :3 0 1ee :2 0
33 :3 0 64 :2 0
c2 1f1 1f2 :3 0
1f3 :2 0 65 :3 0
37 :3 0 66 :3 0
1f5 1f6 4f :3 0
67 :4 0 2c :2 0
65 :3 0 22 :3 0
1fc 1fd 0 c4
1fb 1ff :3 0 2c
:2 0 68 :4 0 c7
201 203 :3 0 2c
:2 0 65 :3 0 39
:3 0 206 207 0
ca 205 209 :3 0
2c :2 0 69 :4 0
cd 20b 20d :3 0
1f9 20e 0 23a
4d :3 0 6a :4 0
2c :2 0 65 :3 0
3b :3 0 213 214
0 d0 212 216
:3 0 2c :2 0 6b
:4 0 d3 218 21a
:3 0 2c :2 0 65
:3 0 39 :3 0 21d
21e 0 d6 21c
220 :3 0 210 221
0 23a 50 :3 0
6c :3 0 223 224
0 6d :3 0 225
226 :2 0 4f :3 0
6e :4 0 4d :3 0
4d :3 0 50 :3 0
6c :3 0 22d 22e
0 6f :3 0 22f
230 0 70 :3 0
71 :3 0 232 233
0 d9 231 235
72 :3 0 db 227
238 :2 0 23a e3
23c 66 :3 0 1f8
23a :4 0 23d e7
2a3 5d :3 0 63
:2 0 73 :4 0 eb
23f 241 :3 0 74
:3 0 41 :3 0 5c
:3 0 42 :3 0 40
:3 0 50 :3 0 75
:3 0 44 :3 0 40
:3 0 45 :4 0 76
1 :8 0 24e ee
258 74 :3 0 41
:3 0 5c :3 0 42
:3 0 40 :3 0 45
:3 0 32 :4 0 77
1 :8 0 257 f0
259 242 24e 0
25a 0 257 0
25a f2 0 2a2
38 :3 0 39 :3 0
5b :3 0 3d :3 0
38 :3 0 20 :3 0
21 :3 0 38 :3 0
3e :3 0 38 :3 0
3f :3 0 3b :3 0
24 :3 0 36 :3 0
5c :3 0 15 :3 0
21 :3 0 22 :3 0
15 :3 0 33 :3 0
38 :3 0 13 :3 0
21 :3 0 13 :3 0
78 :4 0 79 1
:8 0 2a2 4f :3 0
7a :4 0 2c :2 0
33 :3 0 f5 277
279 :3 0 2c :2 0
7b :4 0 f8 27b
27d :3 0 2c :2 0
5b :3 0 fb 27f
281 :3 0 2c :2 0
7c :4 0 fe 283
285 :3 0 275 286
0 2a2 4d :3 0
33 :3 0 288 289
0 2a2 50 :3 0
6c :3 0 28b 28c
0 6d :3 0 28d
28e :2 0 4f :3 0
6e :4 0 4d :3 0
4d :3 0 50 :3 0
6c :3 0 295 296
0 6f :3 0 297
298 0 70 :3 0
71 :3 0 29a 29b
0 101 299 29d
72 :3 0 103 28f
2a0 :2 0 2a2 10b
2a4 1f4 23d 0
2a5 0 2a2 0
2a5 111 0 2a6
114 2a7 1ef 2a6
0 2a8 116 0
36c 34 :3 0 63
:2 0 7d :4 0 11a
2aa 2ac :3 0 2ad
:2 0 71 :3 0 54
:3 0 7e :3 0 7f
:3 0 24 :3 0 50
:3 0 6c :3 0 6f
:3 0 70 :3 0 71
:3 0 50 :3 0 80
:3 0 81 :4 0 82
1 :8 0 369 83
:3 0 47 :3 0 66
:3 0 2bd 2be 58
:3 0 56 :3 0 57
:3 0 3e :3 0 3f
:3 0 84 :3 0 85
:3 0 3b :3 0 83
:3 0 3b :4 0 86
1 :8 0 366 4d
:3 0 6a :4 0 2c
:2 0 83 :3 0 3b
:3 0 2cf 2d0 0
11d 2ce 2d2 :3 0
2c :2 0 87 :4 0
120 2d4 2d6 :3 0
2cc 2d7 0 366
53 :3 0 88 :4 0
2c :2 0 56 :3 0
123 2db 2dd :3 0
2d9 2de 0 366
4f :4 0 2e0 2e1
0 366 59 :4 0
2e3 2e4 0 366
89 :3 0 4a :3 0
83 :3 0 3b :3 0
2e8 2e9 0 126
2e7 2eb 66 :3 0
2e6 2ec 5a :3 0
50 :3 0 8a :3 0
2f0 2f1 0 8b
:3 0 2f2 2f3 0
89 :3 0 3a :3 0
2f5 2f6 0 128
2f4 2f8 2ef 2f9
0 31f 59 :3 0
59 :3 0 2c :2 0
89 :3 0 39 :3 0
2fe 2ff 0 12a
2fd 301 :3 0 2c
:2 0 8c :4 0 12d
303 305 :3 0 2c
:2 0 8d :4 0 130
307 309 :3 0 2c
:2 0 5a :3 0 133
30b 30d :3 0 2c
:2 0 8d :4 0 136
30f 311 :3 0 2c
:2 0 8e :4 0 139
313 315 :3 0 2c
:2 0 8f :3 0 5e
:2 0 13c 318 31a
13e 317 31c :3 0
2fb 31d 0 31f
141 321 66 :3 0
2ee 31f :4 0 366
59 :3 0 5f :3 0
59 :3 0 90 :2 0
91 :3 0 59 :3 0
144 326 328 2d
:2 0 92 :2 0 146
32a 32c :3 0 149
323 32e 2c :2 0
8f :3 0 5e :2 0
14d 331 333 14f
330 335 :3 0 322
336 0 366 4f
:3 0 93 :4 0 2c
:2 0 8f :3 0 5e
:2 0 152 33b 33d
154 33a 33f :3 0
2c :2 0 59 :3 0
157 341 343 :3 0
2c :2 0 94 :4 0
15a 345 347 :3 0
338 348 0 366
50 :3 0 6c :3 0
34a 34b 0 6d
:3 0 34c 34d 0
95 :4 0 34f 350
96 :3 0 4f :3 0
352 353 97 :3 0
6e :4 0 355 356
98 :3 0 4d :3 0
358 359 c :3 0
53 :3 0 35b 35c
99 :3 0 54 :3 0
35e 35f 9a :3 0
72 :3 0 361 362
15d 34e 364 :2 0
366 165 368 66
:3 0 2c0 366 :4 0
369 16f 36a 2ae
369 0 36b 172
0 36c 174 37a
19 :3 0 9b :3 0
371 372 :2 0 373
9c :2 0 80 :2 0
370 :2 0 374 178
376 17a 375 374
:2 0 377 17c :2 0
37a 31 :3 0 17e
37a 379 36c 377
:6 0 37b 1 0
117 12d 37a 59a
:2 0 4 :3 0 9d
:a 0 38c d :7 0
b :4 0 9 :3 0
380 381 0 38c
37e 382 :2 0 b
:3 0 9e :3 0 385
:2 0 387 18c 38b
:3 0 38b 9d :4 0
38b 38a 387 388
:6 0 38c 1 0
37e 382 38b 59a
:2 0 4 :3 0 9f
:a 0 39d e :7 0
b :4 0 a0 :3 0
391 392 0 39d
38f 393 :2 0 b
:3 0 a1 :3 0 396
:2 0 398 18e 39c
:3 0 39c 9f :4 0
39c 39b 398 399
:6 0 39d 1 0
38f 393 39c 59a
:2 0 4 :3 0 a2
:a 0 3bc f :7 0
b :4 0 9 :3 0
3a2 3a3 0 3bc
3a0 3a4 :2 0 194
3bb 0 192 9
:3 0 5e :2 0 190
3a7 3a9 :6 0 3ac
3aa 0 3ba 0
a3 :6 0 a4 :3 0
a3 :3 0 a5 :3 0
a6 :3 0 a7 :3 0
a8 :4 0 a9 1
:8 0 3b7 b :3 0
a3 :3 0 3b5 :2 0
3b7 :3 0 3bb a2
:3 0 197 3bb 3ba
3b7 3b8 :6 0 3bc
1 0 3a0 3a4
3bb 59a :2 0 4
:3 0 aa :a 0 415
10 :7 0 b :4 0
7 :3 0 3c1 3c2
0 415 3bf 3c3
:2 0 5e :2 0 199
7 :3 0 3c6 :7 0
3c9 3c7 0 413
0 ab :6 0 ae
:2 0 19d 9 :3 0
19b 3cb 3cd :6 0
3d0 3ce 0 413
0 ac :6 0 63
:2 0 1a1 9 :3 0
19f 3d2 3d4 :6 0
3d7 3d5 0 413
0 ad :6 0 5f
:3 0 60 :3 0 ac
:3 0 61 :4 0 af
1 :8 0 410 ad
:3 0 b0 :4 0 3dd
3de 0 410 ac
:3 0 73 :4 0 1a5
3e1 3e3 :3 0 ad
:3 0 ad :3 0 2c
:2 0 b1 :4 0 1a8
3e7 3e9 :3 0 3e5
3ea 0 3ec 1ab
3f5 ad :3 0 ad
:3 0 2c :2 0 b2
:4 0 1ad 3ef 3f1
:3 0 3ed 3f2 0
3f4 1b0 3f6 3e4
3ec 0 3f7 0
3f4 0 3f7 1b2
0 410 ad :3 0
ad :3 0 2c :2 0
b3 :4 0 1b5 3fa
3fc :3 0 2c :2 0
b4 :3 0 1b8 3fe
400 :3 0 2c :2 0
b5 :4 0 1bb 402
404 :3 0 3f8 405
0 410 b6 :3 0
b7 :3 0 ad :3 0
ab :3 0 409 40a
:3 0 40b :2 0 410
b :3 0 ab :3 0
40e :2 0 410 1be
414 :3 0 414 aa
:3 0 1c5 414 413
410 411 :6 0 415
1 0 3bf 3c3
414 59a :2 0 4
:3 0 b8 :a 0 434
11 :7 0 b :4 0
7 :3 0 41a 41b
0 434 418 41c
:2 0 428 :2 0 1c9
7 :3 0 41f :7 0
422 420 0 432
0 b9 :6 0 b6
:3 0 b7 :3 0 ba
:4 0 b9 :3 0 bb
:3 0 bc :3 0 425
426 42b :2 0 1cb
42a :2 0 430 b
:3 0 b9 :3 0 42e
:2 0 430 1cd 433
:3 0 433 1d0 433
432 430 431 :6 0
434 1 0 418
41c 433 59a :2 0
4 :3 0 bd :a 0
454 12 :7 0 b
:4 0 7 :3 0 439
43a 0 454 437
43b :2 0 447 :2 0
1d2 7 :3 0 43e
:7 0 441 43f 0
452 0 be :6 0
b6 :3 0 b7 :3 0
bf :4 0 be :3 0
bb :3 0 c0 :3 0
444 445 44a :2 0
1d4 449 :2 0 44f
b :3 0 be :3 0
44d :2 0 44f 1d6
453 :3 0 453 bd
:3 0 1d9 453 452
44f 450 :6 0 454
1 0 437 43b
453 59a :2 0 4
:3 0 c1 :a 0 4ca
13 :7 0 1dd 11c9
0 1db 7 :3 0
c2 :7 0 45a 459
:3 0 1e1 11ef 0
1df 9 :3 0 c3
:7 0 45e 45d :3 0
7 :3 0 c4 :7 0
462 461 :3 0 1e8
1214 0 1e3 9
:3 0 c5 :7 0 466
465 :3 0 b :3 0
7 :3 0 468 46a
0 4ca 457 46b
:2 0 4b9 :2 0 1ea
7 :3 0 46e :7 0
471 46f 0 4c8
0 c6 :6 0 7
:3 0 473 :7 0 476
474 0 4c8 0
c7 :6 0 c8 :3 0
3c :3 0 c6 :3 0
3d :3 0 c8 :3 0
c9 :3 0 ca :3 0
cb :3 0 cc :3 0
c8 :3 0 3f :3 0
c2 :3 0 c8 :3 0
3e :3 0 c3 :3 0
c8 :3 0 3b :3 0
c4 :3 0 c8 :3 0
cd :3 0 c8 :3 0
39 :3 0 ca :3 0
39 :3 0 ca :3 0
3f :3 0 c2 :3 0
ca :3 0 3e :3 0
c3 :3 0 ca :3 0
3b :3 0 c4 :3 0
ca :3 0 ce :3 0
ca :3 0 cf :3 0
cc :3 0 cf :3 0
cc :3 0 3f :3 0
c2 :3 0 cc :3 0
3e :3 0 c3 :3 0
cc :3 0 d0 :3 0
cc :3 0 cf :3 0
c5 :4 0 d1 1
:8 0 4bc d2 :3 0
3c :3 0 c7 :3 0
3d :3 0 3f :3 0
c2 :3 0 3e :3 0
c3 :3 0 3b :3 0
c4 :3 0 cd :3 0
3c :3 0 c6 :4 0
d3 1 :8 0 4bc
b :3 0 c6 :3 0
4ba :2 0 4bc 1ec
4c9 19 :3 0 b
:4 0 4c0 :2 0 4c1
:2 0 4c3 1f0 4c5
1f2 4c4 4c3 :2 0
4c6 1f4 :2 0 4c9
c1 :3 0 1f6 4c9
4c8 4bc 4c6 :6 0
4ca 1 0 457
46b 4c9 59a :2 0
4 :3 0 d4 :a 0
4ec 14 :7 0 b
:4 0 9 :3 0 4cf
4d0 0 4ec 4cd
4d1 :2 0 4df :2 0
1fb 9 :3 0 d
:2 0 1f9 4d4 4d6
:6 0 4d9 4d7 0
4ea 0 d5 :6 0
b6 :3 0 b7 :3 0
d6 :4 0 d5 :3 0
bb :3 0 d7 :3 0
4dc 4dd 4e2 :2 0
1fd 4e1 :2 0 4e7
b :3 0 d5 :3 0
4e5 :2 0 4e7 1ff
4eb :3 0 4eb d4
:3 0 202 4eb 4ea
4e7 4e8 :6 0 4ec
1 0 4cd 4d1
4eb 59a :2 0 4
:3 0 d8 :a 0 570
15 :7 0 b :4 0
9 :3 0 4f1 4f2
0 570 4ef 4f3
:2 0 dc :2 0 206
9 :3 0 da :2 0
204 4f6 4f8 :6 0
6e :4 0 4fc 4f9
4fa 56e 0 d9
:6 0 de :2 0 20a
9 :3 0 208 4fe
500 :6 0 6e :4 0
504 501 502 56e
0 db :6 0 e0
:2 0 20e 9 :3 0
20c 506 508 :6 0
6e :4 0 50c 509
50a 56e 0 dd
:6 0 214 14fe 0
212 9 :3 0 210
50e 510 :6 0 513
511 0 56e 0
df :6 0 4b :3 0
515 0 518 56e
e2 :3 0 1e :7 0
e1 518 515 :4 0
e4 :3 0 e1 :3 0
51b :7 0 51e 51c
0 56e 0 e3
:6 0 dd :3 0 e5
:3 0 e6 :4 0 e7
1 :8 0 564 df
:3 0 e8 :4 0 524
525 0 564 2f
:3 0 e3 :3 0 df
:3 0 528 529 0
52a :2 0 564 66
:3 0 e3 :3 0 db
:4 0 530 :2 0 55a
52d 52e :3 0 e3
:3 0 e9 :3 0 531
532 :3 0 d9 :3 0
2b :2 0 216 535
536 :3 0 d9 :3 0
d9 :3 0 2c :2 0
2c :4 0 218 53a
53c :3 0 2c :2 0
dd :3 0 21b 53e
540 :3 0 2c :2 0
2c :4 0 21e 542
544 :3 0 2c :2 0
db :3 0 221 546
548 :3 0 538 549
0 54b 224 550
d9 :3 0 db :3 0
54c 54d 0 54f
226 551 537 54b
0 552 0 54f
0 552 228 0
553 22b 557 ea
:8 0 556 22d 558
533 553 0 559
0 556 0 559
22f 0 55a 232
55c 66 :4 0 55a
:4 0 564 30 :3 0
e3 :4 0 560 :2 0
564 55e 0 b
:3 0 d9 :3 0 562
:2 0 564 235 56f
19 :3 0 b :4 0
568 :2 0 56a 23c
56c 23e 56b 56a
:2 0 56d 240 :2 0
56f 242 56f 56e
564 56d :6 0 570
1 0 4ef 4f3
56f 59a :2 0 4
:3 0 eb :a 0 594
17 :7 0 b :4 0
7 :3 0 575 576
0 594 573 577
:2 0 24b 593 0
249 7 :3 0 57a
:7 0 57d 57b 0
592 0 3c :6 0
b6 :3 0 b7 :3 0
ec :4 0 3c :3 0
580 581 :3 0 582
:2 0 587 b :3 0
3c :3 0 585 :2 0
587 19 :3 0 b
:4 0 58b :2 0 58d
24e 58f 250 58e
58d :2 0 590 252
:2 0 593 eb :3 0
254 593 592 587
590 :6 0 594 1
0 573 577 593
59a :3 0 599 0
599 :3 0 599 59a
597 598 :6 0 59b
:2 0 256 0 3
599 59e :3 0 59d
59b 59f :8 0 
264
4
:3 0 1 6 1
a 1 e 3
9 d 11 1
19 1 17 2
2e 32 1 37
1 34 1 3f
1 3c 2 3a
42 1 1c 1
4a 1 4e 1
52 3 4d 51
56 1 7a 1
82 1 80 1
89 1 87 1
91 2 96 98
2 9a 9c 2
9e a0 2 a2
a4 1 aa 2
a7 ad 2 b0
b2 2 b4 b6
1 bc 2 b9
bf 2 c1 c2
1 c3 1 d3
2 d8 da 2
dc de 2 e0
e2 2 e4 e6
1 eb 2 e9
ef 2 f2 f4
2 f6 f8 1
fd 2 fb 101
2 103 104 4
c8 cd d1 105
1 c5 1 108
3 90 10b 10f
3 7b 85 8c
1 118 1 11d
1 122 1 127
4 11c 121 126
12b 1 156 1
169 1 171 1
179 1 186 1
18e 1 18c 1
193 1 19f 1
1ab 1 1b7 1
1c3 1 1c1 1
1ca 1 1c8 1
1cf 1 1d9 1
1e0 1 1de 1
1ec 2 1ea 1ec
1 1f0 2 1fa
1fe 2 200 202
2 204 208 2
20a 20c 2 211
215 2 217 219
2 21b 21f 1
234 7 228 229
22a 22b 22c 236
237 3 20f 222
239 1 23c 1
240 2 23e 240
1 24d 1 256
2 258 259 2
276 278 2 27a
27c 2 27e 280
2 282 284 1
29c 7 290 291
292 293 294 29e
29f 5 25a 274
287 28a 2a1 2
2a3 2a4 1 2a5
1 2a7 1 2ab
2 2a9 2ab 2
2cd 2d1 2 2d3
2d5 2 2da 2dc
1 2ea 1 2f7
2 2fc 300 2
302 304 2 306
308 2 30a 30c
2 30e 310 2
312 314 1 319
2 316 31b 2
2fa 31e 1 327
2 329 32b 3
324 325 32d 1
332 2 32f 334
1 33c 2 339
33e 2 340 342
2 344 346 7
351 354 357 35a
35d 360 363 9
2cb 2d8 2df 2e2
2e5 321 337 349
365 2 2bc 368
1 36a 3 1e9
2a8 36b 1 373
1 36e 1 376
d 157 16a 187
191 19d 1a9 1b5
1bf 1c6 1cd 1d7
1dc 1e3 1 386
1 397 1 3a8
1 3a6 2 3b3
3b6 1 3ab 1
3c5 1 3cc 1
3ca 1 3d3 1
3d1 1 3e2 2
3e0 3e2 2 3e6
3e8 1 3eb 2
3ee 3f0 1 3f3
2 3f5 3f6 2
3f9 3fb 2 3fd
3ff 2 401 403
6 3dc 3df 3f7
406 40c 40f 3
3c8 3cf 3d6 1
41e 1 429 2
42c 42f 1 421
1 43d 1 448
2 44b 44e 1
440 1 458 1
45c 1 460 1
464 4 45b 45f
463 467 1 46d
1 472 3 4a9
4b7 4bb 1 4c2
1 4be 1 4c5
2 470 475 1
4d5 1 4d3 1
4e0 2 4e3 4e6
1 4d8 1 4f7
1 4f5 1 4ff
1 4fd 1 507
1 505 1 50f
1 50d 1 51a
1 534 2 539
53b 2 53d 53f
2 541 543 2
545 547 1 54a
1 54e 2 550
551 1 552 1
555 2 557 558
2 52f 559 6
523 526 52b 55c
55f 563 1 569
1 566 1 56c
6 4fb 503 50b
512 519 51d 1
579 2 583 586
1 58c 1 589
1 58f 1 57c
d 46 115 37b
38c 39d 3bc 415
434 454 4ca 4ec
570 594 
1
4
0 
59e
0
1
28
17
41
0 1 1 3 3 1 6 6
6 6 6 b 1 1 1 1
1 1 1 1 1 15 1 0
0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0

472 13 0
515 15 0
458 13 0
80 3 0
3d1 10 0
43d 12 0
5d 3 4
460 13 0
4e 3 0
41e 11 0
579 17 0
a 2 0
3 0 1
1d9 6 0
3bf 1 10
46d 13 0
117 1 6
464 13 0
3ca 10 0
1de 6 0
170 6 9
4fd 15 0
130 6 7
4f5 15 0
2e6 c 0
1cf 6 0
45c 13 0
49 1 3
1ab 6 0
38f 1 e
437 1 12
15d 6 8
1c8 6 0
505 15 0
4d3 14 0
118 6 0
457 1 13
19f 6 0
3a6 f 0
1c1 6 0
3c5 10 0
1b7 6 0
52 3 0
5 1 2
1f5 a 0
11d 6 0
6 2 0
122 6 0
193 6 0
4cd 1 14
e 2 0
51a 15 0
418 1 11
3a0 1 f
171 9 0
127 6 0
4a 3 0
17 2 0
50d 15 0
2bd b 0
573 1 17
4ef 1 15
37e 1 d
87 3 0
18c 6 0
0

/
