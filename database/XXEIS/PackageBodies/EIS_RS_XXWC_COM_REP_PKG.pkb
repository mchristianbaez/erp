CREATE OR REPLACE PACKAGE BODY XXEIS.eis_rs_xxwc_com_rep_pkg
--//=======================================================================================================================
--//
--// Object Name         :: eis_rs_xxwc_com_rep_pkg
--//
--// Object Type         :: Package Body
--//
--// Object Description  :: This package will trigger at before report level and insert the data into a temp table.
--//
--// Version Control
--//=======================================================================================================================
--// Vers    Author           Date          Description
--//-----------------------------------------------------------------------------------------------------------------------
--// 1.0     Mahender Reddy   07/07/2014    Initial Build --TMS#20140602-00298 by Mahender on 07-07-2014�.
--// 1.1     Mahender Reddy   09/12/2014    TMS#20140903-00140 added new variable's l_condition_str and l_inr_object
--//                                        to pass period name,operating unit and invoice source to the package section.
--//                                        Used EXECUTE IMMEDIATE to insert the data into temp table.
--// 1.2     Sowmya KM        31/03/2015    TMS#20150220-00115 - Modified the logic for ExtSpecialCost Column
--// 1.3     Mahender Reddy   07/02/2014    TMS#20150630-00275 Added more rental items categories to 
--//                                        category_concat_segs      for column report_type
--// 1.4     P.Vamshidhar     05/09/2016    TMS# 20160509-00068  - Reduce parallelism on EIS Reports to 4
--//=======================================================================================================================
AS
   g_dflt_email   VARCHAR2 (50) := 'HDSOracleDevelopers@hdsupply.com';                              --Mahender

   --//====================================================================================================================
   --//
   --// Object Name         :: comm_rep_par
   --//
   --// Object Type         :: Procedure
   --//
   --// Object Description  :: This procedure will insert the values into a temp table
   --//
   --// Version Control
   --//====================================================================================================================
   --// Vers    Author           Date          Description
   --//----------------------------------------------------------------------------
--//     1.0     Mahender Reddy   07/07/2014    Initial Build --TMS#20140602-00298 by Mahender on 07-07-2014�.
--//     1.1     Mahender Reddy   09/12/2014    TMS#20140903-00140 added new variable's l_condition_str and l_inr_object
--//                                            to pass period name,operating unit and invoice source to the package section.
--//                                            Used EXECUTE IMMEDIATE to insert the data into temp table.
    --// 1.4     P.Vamshidhar     05/09/2016    TMS# 20160509-00068  - Reduce parallelism on EIS Reports to 4
   --//======================================================================================================================
   PROCEDURE comm_rep_par (p_period_name      IN VARCHAR2
                          ,p_operating_unit   IN VARCHAR2
                          ,p_invoice_source   IN VARCHAR2)
   IS
      l_err_msg         VARCHAR2 (3000);
      l_err_callfrom    VARCHAR2 (50);
      l_err_callpoint   VARCHAR2 (50);
      l_condition_str   VARCHAR2(32000);
      l_inr_object      VARCHAR2(32000);

   BEGIN
      l_err_callfrom := 'eis_rs_xxwc_com_rep_pkg.comm_rep_par';
      l_err_callpoint := 'comm rep par';

  l_condition_str  :=' and 1=1 ';

  IF p_operating_unit      IS NOT NULL THEN
    l_condition_str:= l_condition_str||' and hou.name in ('||xxeis.eis_rs_utility.get_param_values(p_operating_unit)||' )';
  END IF;

  IF p_period_name      IS NOT NULL THEN
    l_condition_str:= l_condition_str||' and gp.period_name in ('||xxeis.eis_rs_utility.get_param_values(p_period_name)||' )';
  END IF;

  IF p_invoice_source      IS NOT NULL THEN
    l_condition_str:= l_condition_str||' and bs.name in ('||xxeis.eis_rs_utility.get_param_values(p_invoice_source)||' )';
  END IF;



  If p_invoice_source IS NOT NULL THEN
  FOR rec
    IN (SELECT distinct b.name
        FROM  ra_batch_sources_all b)

  LOOP

        l_inr_object :=
     --'INSERT /*+ append parallel(6) */   commented and added below in 1.4 Rev  
     'INSERT /*+ append parallel(4) */
            INTO  xxeis.xxwc_eom_comm_rep_tbl
         SELECT gd.gl_date businessdate
               ,CASE
                   WHEN msi.segment1 = ''CONTBILL''
                   THEN
                      ''FabRebar''
                   WHEN (mic.category_concat_segs IN (''99.99E1''
                                                     ,''99.99E2''
                                                     ,''99.99F1''
                                                     ,''99.99F2''
                                                     ,''99.99RR''
                                                     ,''99.99RT''
                                                     ,''99.99S1''
                                                     ,''99.99S2''
                                                     ,''99.99SP''
                                                     ,''99.99T1''
                                                     ,''99.99T2''))
                  THEN
                      ''Rental''
                   WHEN mic.segment1 IN (''BB''
                                        ,''BS''
                                        ,''DC''
                                        ,''EC''
                                        ,''EP''
                                        ,''FA''
                                        ,''FC''
                                        ,''JS''
                                        ,''ME''
                                        ,''ML''
                                        ,''MR''
                                        ,''NS''
                                        ,''PS''
                                        ,''RE''
                                        ,''RL''
                                        ,''RP''
                                        ,''SA''
                                        ,''SC''
                                        ,''SH''
                                        ,''TH'')
                   THEN
                      ''Intangibles''
                   WHEN mc.segment1 IN (''GC''
                                       ,''MD''
                                       ,''PC''
                                       ,''PD''
                                       ,''PR''
                                       ,''RR''
                                       ,''RV''
                                       ,''ST''
                                       ,''XX'')
                   THEN
                      ''Non Comm.''
                   WHEN    (mic.category_concat_segs IN (''FB.OSPI''
                                                        ,''FB.MODL''
                                                        ,''FB.SUBA''
                                                        ,''FB.OPTC''
                                                        ,''RF.OSPI''
                                                        ,''RF.SERV''))
                        OR mic.category_concat_segs IN (''60.6021'', ''60.6022'', ''60.6035'')
                   THEN
                      ''FabRebar''
                   WHEN ( (ol.attribute3 = ''Y'') OR (ol.attribute15 = ''Y''))
                   THEN
                      ''Blow and Expired''
                   ELSE
                      ''Product''
                END
                   report_type
               ,oh.order_number order_number
               ,ct.trx_number invoicenumber
               ,ctl.line_number lineno
               ,DECODE (
                   msi.segment1
                  ,''Rental Charge'', DECODE (
                                       ol.line_category_code
                                      ,''RETURN'', xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item_desc (
                                                    ol.reference_line_id)
                                      ,xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item_desc (
                                          ol.link_to_line_id))
                  ,ctl.description)
                   description
               ,NVL (ctl.quantity_invoiced, ctl.quantity_credited) qty
               ,NVL (ctl.unit_selling_price, 0) unitprice
               ,NVL (
                   ctl.extended_amount
                  ,DECODE (
                      NVL (ctl.unit_selling_price, 0)
                     ,0, 0
                     , (  NVL (ctl.quantity_invoiced, ctl.quantity_credited)
                        * (  NVL (ctl.unit_selling_price, 0)
                           + xxeis.eis_rs_xxwc_com_util_pkg.get_mfgadjust (ol.header_id, ol.line_id)))))
                   extsale
               ,hca.account_number masternumber
               ,hca.account_name mastername
               ,hps.party_site_number jobnumber
               ,hcsu.location customername
               ,jrs.salesrep_number salesrepnumber
               ,NVL (jrse.source_name, jrse.resource_name) salesrepname
               ,mp.organization_code loc
               ,mp.organization_id mp_organization_id
               ,mp.attribute9 regionname
               ,DECODE (
                   msi.segment1
                  ,''Rental Charge'', DECODE (
                                       ol.line_category_code
                                      ,''RETURN'', xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item (
                                                    ol.reference_line_id)
                                      ,xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item (ol.link_to_line_id))
                  ,msi.segment1)
                   partno
               ,mc.segment2 catclass
               ,bs.name invoice_source
               ,NVL (ol.shipped_quantity, 0) shipped_quantity
               ,DECODE (ol.source_type_code,  ''INTERNAL'', 0,  ''EXTERNAL'', 1,  ol.source_type_code) directflag
               ,CASE
                   WHEN ol.line_type_id = 1008
                   THEN
                      NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.reference_line_id), 0)
                   WHEN ol.line_type_id = 1003
                   THEN
                      NVL (
                         xxeis.eis_rs_xxwc_com_util_pkg.get_bill_line_cost (oh.header_id
                                                                           ,ol.inventory_item_id)
                        ,0)
                   ELSE
                      NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id), 0)
                END
                   averagecost
               ,DECODE (xxeis.eis_rs_xxwc_com_util_pkg.get_trader (ol.header_id, ol.line_id)
                       ,0, ''-''
                       ,''Trader'')
                   trader
               ,NVL (
                   ROUND (
                      xxwc_mv_routines_pkg.get_vendor_quote_cost(ol.line_id)-- modified by sowmya on 31march2015 for tms task 20150220-00115
                     ,9)-- modified by sowmya on 31march2015 for tms task 20150220-00115
                  ,0)
                   special_cost
               ,hou.name operating_unit
               ,msi.list_price_per_unit list_price
               ,hou.organization_id org_id
               ,msi.inventory_item_id
               ,msi.organization_id
               ,ct.customer_trx_id
               ,hp.party_id
               ,gp.period_name period_name
               ,xxeis.eis_rs_xxwc_com_util_pkg.get_mfgadjust (ol.header_id, ol.line_id) mfgadjust
               ,DECODE (jrse.category,  ''EMPLOYEE'', ''Employee'',  ''OTHER'', ''House Acct'',  ''House Acct'')
                   salesrep_type
               ,jrse.category sales_type
               ,ctl.interface_line_attribute2
               ,fu.description insidesalesrep
               ,fu.employee_id insidesrnum
               ,srep.prism_num prismsrnum
               ,ott.name linetype
               ,NVL ( (SELECT emp.full_name
                         FROM apps.per_all_people_f emp, apps.fnd_user fu
                        WHERE ol.created_by = fu.user_id AND ROWNUM = 1 AND fu.employee_id = emp.person_id)
                    ,'''')
                   hrinsidesrname
               ,NVL ( (SELECT emp.employee_number
                         FROM apps.per_all_people_f emp, apps.fnd_user fu
                        WHERE ol.created_by = fu.user_id AND ROWNUM = 1 AND fu.employee_id = emp.person_id)
                    ,'''')
                   hrinsidesrnum
           FROM ra_customer_trx ct
               ,ra_customer_trx_lines ctl
               ,ra_cust_trx_line_gl_dist gd
               ,hz_parties hp
               ,hz_cust_accounts hca
               ,hz_party_sites hps
               ,hz_cust_acct_sites hcas
               ,hz_cust_site_uses hcsu
               ,jtf_rs_salesreps jrs
               ,jtf_rs_resource_extns_vl jrse
               ,mtl_parameters mp
               ,mtl_system_items_kfv msi
               ,mtl_item_categories_v mic
               ,mtl_categories_b_kfv mc
               ,oe_order_lines ol
               ,oe_order_headers oh
               ,hr_operating_units hou
               ,mtl_category_sets mdcs
               ,ra_batch_sources bs
               ,oe_transaction_types_vl oel
               ,oe_transaction_types_vl oeh
               ,fnd_user fu
               ,gl_periods gp
               ,gl_ledgers gl
               ,xxeis.eis_xxwc_ar_prism_salesrep srep
               ,ont.oe_transaction_types_tl ott
          WHERE     1 = 1
                AND srep.employee_num(+) = jrs.salesrep_number
                AND ol.line_type_id = ott.transaction_type_id
                AND gd.customer_trx_id = ct.customer_trx_id
                AND gd.account_class = ''REC''
                AND ctl.customer_trx_id = ct.customer_trx_id
                AND ct.org_id = hou.organization_id
                AND gd.org_id = hou.organization_id
                AND ct.bill_to_customer_id = hca.cust_account_id
                AND hca.party_id = hp.party_id(+)
                AND hp.party_id = hps.party_id
                AND hcas.party_site_id = hps.party_site_id
                AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcsu.site_use_id = ct.bill_to_site_use_id
                AND hca.cust_account_id = hcas.cust_account_id
                AND ol.salesrep_id = jrs.salesrep_id
                AND hca.account_number <> ''70200000''
                AND oh.org_id = jrs.org_id
                AND jrs.resource_id = jrse.resource_id
                AND TO_CHAR (oh.order_number) = ct.interface_header_attribute1
                AND ctl.interface_line_context = ''ORDER ENTRY''
                AND TO_CHAR (ol.line_id) = ctl.interface_line_attribute6
                AND oh.header_id = ol.header_id
                AND msi.inventory_item_id = ol.inventory_item_id
                AND msi.organization_id = ol.ship_from_org_id
                AND mic.category_id = mc.category_id
                AND msi.organization_id = mic.organization_id
                AND msi.inventory_item_id = mic.inventory_item_id
                AND msi.organization_id = mp.organization_id
                AND mic.category_set_id = mdcs.category_set_id
                AND ctl.extended_amount <> 0
                AND mdcs.category_set_name = ''Inventory Category''
                AND ct.batch_source_id = bs.batch_source_id
                AND ct.org_id = bs.org_id
                AND mc.structure_id = 101
                AND oel.transaction_type_id = ol.line_type_id
                AND oeh.transaction_type_id = oh.order_type_id
                AND oeh.org_id = oh.org_id
                AND oel.org_id = ol.org_id
                AND oh.header_id = ol.header_id
                AND ol.created_by = fu.user_id
                AND gp.period_set_name = gl.period_set_name
                AND gl.ledger_id = gd.set_of_books_id
                AND gl.period_set_name = ''4-4-QTR''
                AND TRUNC (gd.gl_date) BETWEEN TRUNC (gp.start_date) AND TRUNC (gp.end_date)'||l_condition_str||'
                AND bs.name ='''||rec.name||'''';

FND_FILE.PUT_LINE(FND_FILE.LOG,'Executing the main Query....');

     EXECUTE IMMEDIATE l_inr_object;

     COMMIT;

     END LOOP;

     FND_FILE.PUT_LINE(FND_FILE.LOG,'For records end loop....');

  END IF;
   FND_FILE.PUT_LINE(FND_FILE.LOG,'Data Insert Completed....');
  EXCEPTION
      WHEN OTHERS
      THEN
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Exception....');

         apps.xxcus_error_pkg.xxcus_error_main_api (p_called_from         => l_err_callfrom
                                                   ,p_calling             => l_err_callpoint
                                                   ,p_request_id          => -1
                                                   ,p_ora_error_msg       => SQLERRM
                                                   ,p_error_desc          => l_err_msg
                                                   ,p_distribution_list   => g_dflt_email
                                                   ,p_module              => 'APPS');
   END comm_rep_par;
END eis_rs_xxwc_com_rep_pkg;
/