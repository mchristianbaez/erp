/* Formatted on 10-Jul-2013 01:43:14 (QP5 v5.206) */
-- Start of DDL Script for Package Body XXEIS.EIS_PO_XXWC_ISR_UTIL_PKG
-- Generated 10-Jul-2013 01:43:05 from XXEIS@EBIZFQA

CREATE OR REPLACE PACKAGE BODY xxeis.eis_po_xxwc_isr_util_pkg
AS
    FUNCTION get_vendor_name (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2
    IS
        l_vendor_name   VARCHAR2 (240) := NULL;
    BEGIN
        /* select max(pov.vendor_name )
         into l_vendor_name
         from mrp_sr_assignments  ass,
              mrp_sr_receipt_org rco,
              mrp_sr_source_org sso,
              po_vendors pov
         where 1=1
         and ass.inventory_item_id(+)=p_inventory_item_id
         and ass.organization_id(+)=p_organization_id
         and rco.sourcing_rule_id(+)=ass.sourcing_rule_id
         and sso.sr_receipt_id(+)=rco.sr_receipt_id
         and pov.vendor_id(+)=sso.vendor_id;*/

        BEGIN
            SELECT pov.vendor_name
              INTO l_vendor_name
              FROM mrp_sr_assignments ass
                  ,mrp_sr_receipt_org rco
                  ,mrp_sr_source_org sso
                  ,po_vendors pov
                  ,mtl_system_items_b msi
             WHERE     1 = 1
                   AND msi.inventory_item_id = ass.inventory_item_id
                   AND msi.organization_id = ass.organization_id
                   AND ass.inventory_item_id = p_inventory_item_id
                   AND ass.organization_id = p_organization_id
                   AND msi.source_type = 2
                   AND rco.sourcing_rule_id = ass.sourcing_rule_id
                   AND sso.sr_receipt_id = rco.sr_receipt_id
                   AND sso.source_type = 3
                   AND pov.vendor_id = sso.vendor_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                l_vendor_name := NULL;
        END;

        IF l_vendor_name IS NULL
        THEN
            SELECT MAX (pov.vendor_name)
              INTO l_vendor_name
              FROM mrp_sr_assignments ass
                  ,mrp_sr_receipt_org rco
                  ,mrp_sr_source_org sso
                  ,po_vendors pov
                  ,mtl_system_items_b msi
             WHERE     1 = 1
                   AND msi.inventory_item_id = ass.inventory_item_id
                   AND msi.organization_id = ass.organization_id
                   AND ass.inventory_item_id = p_inventory_item_id
                   --and ass.organization_id   = p_organization_id
                   AND msi.source_type = 2
                   AND rco.sourcing_rule_id = ass.sourcing_rule_id
                   AND sso.sr_receipt_id = rco.sr_receipt_id
                   AND sso.source_type = 3
                   AND pov.vendor_id = sso.vendor_id;
        END IF;

        RETURN l_vendor_name;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_vendor_number (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2
    IS
        l_vendor_number   VARCHAR2 (240);
    BEGIN
        BEGIN
            SELECT pov.segment1
              INTO l_vendor_number
              FROM mrp_sr_assignments ass
                  ,mrp_sr_receipt_org rco
                  ,mrp_sr_source_org sso
                  ,po_vendors pov
                  ,mtl_system_items_b msi
             WHERE     1 = 1
                   AND msi.inventory_item_id = ass.inventory_item_id
                   AND msi.organization_id = ass.organization_id
                   AND ass.inventory_item_id = p_inventory_item_id
                   AND ass.organization_id = p_organization_id
                   AND msi.source_type = 2
                   AND rco.sourcing_rule_id = ass.sourcing_rule_id
                   AND sso.sr_receipt_id = rco.sr_receipt_id
                   AND sso.source_type = 3
                   AND pov.vendor_id = sso.vendor_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                l_vendor_number := NULL;
        END;

        IF l_vendor_number IS NULL
        THEN
            SELECT MAX (pov.segment1)
              INTO l_vendor_number
              FROM mrp_sr_assignments ass
                  ,mrp_sr_receipt_org rco
                  ,mrp_sr_source_org sso
                  ,po_vendors pov
                  ,mtl_system_items_b msi
             WHERE     1 = 1
                   AND msi.inventory_item_id = ass.inventory_item_id
                   AND msi.organization_id = ass.organization_id
                   AND ass.inventory_item_id = p_inventory_item_id
                   --and ass.organization_id   = p_organization_id
                   AND msi.source_type = 2
                   AND rco.sourcing_rule_id = ass.sourcing_rule_id
                   AND sso.sr_receipt_id = rco.sr_receipt_id
                   AND sso.source_type = 3
                   AND pov.vendor_id = sso.vendor_id;
        END IF;

        RETURN l_vendor_number;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_inv_cat_class (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2
    IS
        l_cat_class   VARCHAR2 (400);
    BEGIN
        SELECT mcv.segment2
          INTO l_cat_class
          FROM mtl_categories_kfv mcv, mtl_category_sets mcs, mtl_item_categories mic
         WHERE     mcs.category_set_name = 'Inventory Category'
               AND mcs.structure_id = mcv.structure_id
               AND mic.inventory_item_id = p_inventory_item_id
               AND mic.organization_id = p_organization_id
               AND mic.category_set_id = mcs.category_set_id
               AND mic.category_id = mcv.category_id;

        RETURN l_cat_class;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_inv_cat_seg1 (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2
    IS
        l_cat_class   VARCHAR2 (400);
    BEGIN
        SELECT mcv.segment1
          INTO l_cat_class
          FROM mtl_categories_kfv mcv, mtl_category_sets mcs, mtl_item_categories mic
         WHERE     mcs.category_set_name = 'Inventory Category'
               AND mcs.structure_id = mcv.structure_id
               AND mic.inventory_item_id = p_inventory_item_id
               AND mic.organization_id = p_organization_id
               AND mic.category_set_id = mcs.category_set_id
               AND mic.category_id = mcv.category_id;

        RETURN l_cat_class;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_inv_vel_cat_class (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2
    IS
        l_cat_class   VARCHAR2 (400);
    BEGIN
        SELECT mcv.segment1
          INTO l_cat_class
          FROM mtl_categories_kfv mcv, mtl_category_sets mcs, mtl_item_categories mic
         WHERE     mcs.category_set_name = 'Sales Velocity'
               AND mcs.structure_id = mcv.structure_id
               AND mic.inventory_item_id = p_inventory_item_id
               AND mic.organization_id = p_organization_id
               AND mic.category_set_id = mcs.category_set_id
               AND mic.category_id = mcv.category_id;

        RETURN l_cat_class;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_isr_item_cost (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER
    IS
        l_item_cost   NUMBER;
    BEGIN
        SELECT unit_price
          INTO l_item_cost
          FROM po_lines_all
         WHERE po_line_id IN
                   (SELECT MAX (pol.po_line_id)
                      FROM po_headers_all poh
                          ,po_line_locations_all poll
                          ,po_lines_all pol
                          ,po_vendors pov
                     WHERE     poh.type_lookup_code = 'BLANKET'
                           AND poh.po_header_id = pol.po_header_id
                           AND pol.po_line_id = poll.po_line_id(+)
                           AND NVL (poh.cancel_flag, 'N') = 'N'
                           --and  poll.quantity_received > 0
                           AND pol.item_id = p_inventory_item_id
                           AND (poh.global_agreement_flag = 'Y' OR poll.ship_to_organization_id = p_organization_id)
                           AND poh.vendor_id = pov.vendor_id
                           AND pov.segment1 = get_vendor_number (p_inventory_item_id, p_organization_id));

        RETURN l_item_cost;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_isr_bpa_doc (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER
    IS
        l_bpa_doc   NUMBER;
    BEGIN
        SELECT segment1
          INTO l_bpa_doc
          FROM po_headers_all
         WHERE po_header_id IN
                   (SELECT MAX (poh.po_header_id)
                      FROM po_headers_all poh
                          ,po_line_locations poll
                          ,po_lines_all pol
                          ,po_vendors pov
                     WHERE     poh.type_lookup_code = 'BLANKET'
                           AND poh.po_header_id = pol.po_header_id
                           AND NVL (poh.cancel_flag, 'N') = 'N'
                           AND pol.po_line_id = poll.po_line_id(+)
                           --and  poll.quantity_received > 0
                           AND pol.item_id = p_inventory_item_id
                           AND (poh.global_agreement_flag = 'Y' OR poll.ship_to_organization_id = p_organization_id)
                           AND poh.vendor_id = pov.vendor_id
                           AND pov.segment1 = get_vendor_number (p_inventory_item_id, p_organization_id));

        RETURN l_bpa_doc;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_isr_open_po_qty (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER
    IS
        l_open_qty               NUMBER;
        l_internal_receipt_qty   NUMBER;
        l_onorder_qty            NUMBER;
        l_component_qty          NUMBER;
        l_assembly_qy            NUMBER;
    BEGIN
        SELECT SUM (qty)
          INTO l_open_qty
          FROM (SELECT SUM (poll.quantity - NVL (poll.quantity_received, 0) - NVL (poll.quantity_cancelled, 0)) qty
                  FROM po_headers_all poh
                      ,po_line_locations_all poll
                      ,po_lines_all pol
                      ,po_vendors pov
                 WHERE     1 = 1
                       AND NVL (poh.cancel_flag, 'N') = 'N'
                       AND NVL (pol.cancel_flag, 'N') = 'N'
                       AND NVL (poll.cancel_flag, 'N') = 'N'
                       AND NVL (poh.closed_code, 'OPEN') NOT IN ('FINALLY CLOSED', 'CLOSED')
                       AND NVL (poll.closed_code, 'OPEN') NOT IN ('CLOSED', 'FINALLY CLOSED')
                       AND NVL (pol.closed_code, 'OPEN') NOT IN ('CLOSED', 'FINALLY CLOSED')
                       AND poh.type_lookup_code IN ('STANDARD', 'BLANKET', 'PLANNED')
                       AND poh.po_header_id = pol.po_header_id
                       AND pol.po_line_id = poll.po_line_id
                       AND poll.quantity - NVL (poll.quantity_received, 0) > 0
                       AND pol.item_id = p_inventory_item_id
                       AND poll.ship_to_organization_id = p_organization_id
                       AND poh.vendor_id = pov.vendor_id);

        --query to get internal receipt qty---

        SELECT SUM (oel.ordered_quantity - NVL (oel.fulfilled_quantity, 0))
          INTO l_internal_receipt_qty
          FROM apps.oe_order_lines_all oel, apps.oe_order_headers_all oeh, apps.po_requisition_headers_all prh
         WHERE     oeh.header_id = oel.header_id
               AND oeh.source_document_id = prh.requisition_header_id
               AND oel.source_type_code = 'INTERNAL'
               AND oel.flow_status_code = 'CLOSED'
               AND oel.inventory_item_id = p_inventory_item_id
               AND oel.ship_from_org_id = p_organization_id
               AND NOT EXISTS
                           (SELECT 1
                              FROM apps.rcv_transactions rt, apps.po_requisition_lines_all prl
                             WHERE     rt.source_document_code = 'REQ'
                                   AND rt.requisition_line_id = prl.requisition_line_id
                                   AND prl.requisition_header_id = prh.requisition_header_id);

        --how ROP and Min-Max get the WIP Demand (If the item is a component on a work order).

        SELECT SUM (o.required_quantity - o.quantity_issued)
          INTO l_component_qty
          FROM wip_discrete_jobs d, wip_requirement_operations o
         WHERE     o.wip_entity_id = d.wip_entity_id
               AND o.organization_id = d.organization_id
               AND d.organization_id = p_organization_id
               AND o.inventory_item_id = p_inventory_item_id
               AND o.date_required <= SYSDATE
               AND o.required_quantity >= o.quantity_issued
               AND o.operation_seq_num > 0
               AND d.status_type IN (1, 3, 4, 6)
               AND o.wip_supply_type NOT IN (5, 6)
               --  AND nvl(o.supply_subinventory,1) = decode(:subinv,NULL,nvl(o.supply_subinventory,1),:subinv)
               AND NOT EXISTS
                           (SELECT /*+ index(mtl MTL_DEMAND_N12)*/
                                  wip.wip_entity_id
                              FROM wip_so_allocations wip, mtl_demand mtl
                             WHERE     wip_entity_id = o.wip_entity_id
                                   AND wip.organization_id = p_organization_id
                                   AND wip.organization_id = mtl.organization_id
                                   AND wip.demand_source_header_id = mtl.demand_source_header_id
                                   AND wip.demand_source_line = mtl.demand_source_line
                                   AND wip.demand_source_delivery = mtl.demand_source_delivery
                                   AND mtl.inventory_item_id = p_inventory_item_id);

        --how ROP and Min-Max get WIP Supply (If it's an assembly item on a work order).

        SELECT SUM (NVL (start_quantity, 0) - NVL (quantity_completed, 0) - NVL (quantity_scrapped, 0))
          INTO l_assembly_qy
          FROM wip_discrete_jobs
         WHERE     organization_id = p_organization_id
               AND primary_item_id = p_inventory_item_id
               AND status_type IN (1, 3, 4, 6)
               AND job_type IN (1, 3)
               AND scheduled_completion_date <= TO_DATE (TO_CHAR (SYSDATE), 'DD-MON-RR');

        l_onorder_qty :=
            NVL (l_open_qty, 0) + NVL (l_internal_receipt_qty, 0) + NVL (l_component_qty, 0) + NVL (l_assembly_qy, 0);

        RETURN l_onorder_qty;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_isr_open_req_qty (p_inventory_item_id NUMBER, p_organization_id NUMBER, p_req_type VARCHAR2)
        RETURN NUMBER
    IS
        l_open_qty   NUMBER;
    BEGIN
        /* SELECT SUM(prdl.req_line_quantity) QTY
         into l_open_qty
         from  po_requisition_headers prh,
               po_req_distributions prdl,
               po_requisition_lines prl
         where prh.requisition_header_id = prl.requisition_header_id
         AND prdl.requisition_line_id=prl.requisition_line_id
         and item_id=p_inventory_item_id
         and nvl(prh.cancel_flag,'N')='N'
         and nvl(prl.cancel_flag,'N')='N'
         and nvl(prl.closed_code,'OPEN')='OPEN'
         AND prh.authorization_status = 'APPROVED'
         and destination_organization_id=p_organization_id
         AND not exists (select 1
                         from po_action_history pah
                         WHERE  pah.object_id = prh.requisition_header_id
                           AND pah.object_type_code  = 'REQUISITION'
                           AND pah.action_code       = 'CANCEL'
                        )
         and not exists (select 1
                         from po_distributions pod
                         WHERE POD.REQ_DISTRIBUTION_ID=PRDL.DISTRIBUTION_ID
                        )
        and not exists(select 1 from oe_order_headers oh
                        where order_type_id = 1011
                        and oh.source_document_id = prl.REQUISITION_HEADER_ID);*/

        SELECT SUM (qty)
          INTO l_open_qty
          FROM (SELECT NVL (SUM (to_org_primary_quantity), 0) qty
                  FROM apps.mtl_supply sup, apps.po_requisition_headers_all prh
                 WHERE     sup.supply_type_code IN ('REQ')
                       AND sup.destination_type_code = 'INVENTORY'
                       AND sup.to_organization_id = p_organization_id
                       AND sup.item_id = p_inventory_item_id
                       AND prh.requisition_header_id = sup.req_header_id
                       AND p_req_type <> 'INVENTORY'
                       --AND (p_req_type ='BOTH' OR  prh.type_lookup_code = p_req_type)
                       AND (   NVL (sup.from_organization_id, -1) <> p_organization_id
                            OR (    sup.from_organization_id = p_organization_id
                                AND EXISTS
                                        (SELECT 'x'
                                           FROM apps.mtl_secondary_inventories sub1
                                          WHERE     sub1.organization_id = sup.from_organization_id
                                                AND sub1.secondary_inventory_name = sup.from_subinventory
                                                AND sub1.availability_type <> 1)))
                       AND (   sup.to_subinventory IS NULL
                            OR (EXISTS
                                    (SELECT 'x'
                                       FROM apps.mtl_secondary_inventories sub2
                                      WHERE     sub2.secondary_inventory_name = sup.to_subinventory
                                            AND sub2.organization_id = sup.to_organization_id
                                            AND sub2.availability_type = DECODE (2, 1, sub2.availability_type, 1))))
                       AND (   (    p_req_type IN ('INVENTORY', 'VENDOR', 'BOTH')
                                AND NOT EXISTS
                                            (SELECT 'X'
                                               FROM apps.oe_drop_ship_sources odss
                                              WHERE DECODE (sup.po_header_id
                                                           ,NULL, sup.req_line_id
                                                           ,sup.po_line_location_id) =
                                                        DECODE (sup.po_header_id
                                                               ,NULL, odss.requisition_line_id
                                                               ,odss.line_location_id)))
                            OR (    p_req_type = 'DIRECT'
                                AND EXISTS
                                        (SELECT 'X'
                                           FROM apps.oe_drop_ship_sources odss
                                          WHERE DECODE (sup.po_header_id
                                                       ,NULL, sup.req_line_id
                                                       ,sup.po_line_location_id) =
                                                    DECODE (sup.po_header_id
                                                           ,NULL, odss.requisition_line_id
                                                           ,odss.line_location_id))))
                       AND (   sup.po_line_location_id IS NULL
                            OR EXISTS
                                   (SELECT 'x'
                                      FROM apps.po_line_locations_all lilo
                                     WHERE     lilo.line_location_id = sup.po_line_location_id
                                           AND NVL (lilo.vmi_flag, 'N') = 'N'))
                       AND (   sup.req_line_id IS NULL
                            OR EXISTS
                                   (SELECT 'x'
                                      FROM apps.po_requisition_lines_all prl
                                     WHERE     prl.requisition_line_id = sup.req_line_id
                                           AND prl.line_location_id IS NULL
                                           AND (   p_req_type = 'BOTH'
                                                OR p_req_type = 'DIRECT'
                                                OR prl.source_type_code = p_req_type)
                                           AND NVL (prl.vmi_flag, 'N') = 'N'))
                UNION
                SELECT NVL (SUM (prl.quantity), 0) qty
                  --INTO l_open_qty
                  FROM po_requisition_headers_all prh, po_requisition_lines_all prl
                 WHERE     prh.requisition_header_id = prl.requisition_header_id
                       AND item_id = p_inventory_item_id
                       AND NVL (prh.cancel_flag, 'N') = 'N'
                       AND NVL (prl.cancel_flag, 'N') = 'N'
                       AND NVL (prl.closed_code, 'OPEN') = 'OPEN'
                       AND prh.authorization_status = 'INCOMPLETE'
                       AND destination_organization_id = p_organization_id
                       AND p_req_type = 'INVENTORY'
                       AND source_type_code = 'INVENTORY'
                       AND NOT EXISTS
                                   (SELECT 1
                                      FROM po_action_history pah
                                     WHERE     pah.object_id = prh.requisition_header_id
                                           AND pah.object_type_code = 'REQUISITION'
                                           AND pah.action_code = 'CANCEL'));

        --   and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)

        RETURN l_open_qty;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 0;
    END;

    FUNCTION has_drop_ships (p_po_header_id NUMBER, p_req_line_id NUMBER, p_po_line_location_id NUMBER)
        RETURN VARCHAR2
    IS
        l_retval   VARCHAR2 (1);
    BEGIN
        IF p_po_header_id IS NULL
        THEN
            SELECT 'Y'
              INTO l_retval
              FROM apps.oe_drop_ship_sources odss
             WHERE odss.requisition_line_id = p_req_line_id AND ROWNUM < 2;
        ELSE
            SELECT 'Y'
              INTO l_retval
              FROM apps.oe_drop_ship_sources odss
             WHERE odss.line_location_id = p_po_line_location_id AND ROWNUM < 2;
        END IF;

        RETURN (l_retval);
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            RETURN ('N');
    END has_drop_ships;

    FUNCTION is_not_vmi (p_req_type VARCHAR2, p_req_line_id NUMBER, p_po_line_location_id NUMBER)
        RETURN VARCHAR2
    IS
        l_retval   VARCHAR2 (1) := 'N';
    BEGIN
        IF p_req_line_id IS NULL AND p_po_line_location_id IS NULL
        THEN
            RETURN ('Y');
        END IF;

        IF p_po_line_location_id IS NOT NULL
        THEN
            BEGIN
                SELECT 'Y'
                  INTO l_retval
                  FROM apps.po_line_locations_all lilo
                 WHERE lilo.line_location_id = p_po_line_location_id AND NVL (lilo.vmi_flag, 'N') = 'N' AND ROWNUM < 2;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    l_retval := 'N';
                    RETURN (l_retval);
            END;
        END IF;

        IF p_req_line_id IS NOT NULL
        THEN
            BEGIN
                SELECT 'Y'
                  INTO l_retval
                  FROM apps.po_requisition_lines_all prl
                 WHERE     prl.requisition_line_id = p_req_line_id
                       AND prl.line_location_id IS NULL
                       AND (p_req_type = 'BOTH' OR p_req_type = 'DIRECT' OR prl.source_type_code = 'VENDOR')
                       AND NVL (prl.vmi_flag, 'N') = 'N'
                       AND ROWNUM < 2;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    l_retval := 'N';
                    RETURN (l_retval);
            END;
        END IF;

        RETURN ('Y');
    END is_not_vmi;

    FUNCTION is_nettable_subinv (p_organization_id NUMBER, p_subinv_name VARCHAR2)
        RETURN VARCHAR2
    IS
        l_retval   VARCHAR2 (1);
    BEGIN
        IF p_subinv_name IS NULL
        THEN
            RETURN ('Y');
        ELSE
            SELECT 'Y'
              INTO l_retval
              FROM apps.mtl_secondary_inventories sub2
             WHERE     sub2.secondary_inventory_name = p_subinv_name
                   AND sub2.organization_id = p_organization_id
                   AND sub2.availability_type = 1
                   AND ROWNUM < 2;
        END IF;

        RETURN ('Y');
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            RETURN ('N');
    END is_nettable_subinv;

    PROCEDURE get_isr_open_req_qty (p_organization_id     IN     NUMBER
                                   ,p_inventory_item_id   IN     NUMBER
                                   ,p_both_qty               OUT NUMBER
                                   ,p_vendor_qty             OUT NUMBER
                                   ,p_inventory_qty          OUT NUMBER
                                   ,p_direct_qty             OUT NUMBER)
    IS
        l_vendor_qty      NUMBER;
        l_direct_qty      NUMBER;
        l_inventory_qty   NUMBER;
        l_both_qty        NUMBER;
    BEGIN
        FOR l_rec
            IN (  SELECT sup.to_organization_id
                        ,sup.item_id
                        ,                        --        is_not_vmi('DIRECT',sup.req_line_id,sup.po_line_location_id),
                         --         has_drop_ships (sup.po_header_id,sup.req_line_id,sup.po_line_location_id)
                         SUM (
                             CASE
                                 WHEN     xxeis.eis_po_xxwc_isr_util_pkg.is_not_vmi ('BOTH'
                                                                                    ,sup.req_line_id
                                                                                    ,sup.po_line_location_id) = 'Y'
                                      AND xxeis.eis_po_xxwc_isr_util_pkg.has_drop_ships (sup.po_header_id
                                                                                        ,sup.req_line_id
                                                                                        ,sup.po_line_location_id) = 'N'
                                 THEN
                                     NVL (to_org_primary_quantity, 0)
                                 ELSE
                                     0
                             END)
                             both_qty
                        ,SUM (
                             CASE
                                 WHEN     xxeis.eis_po_xxwc_isr_util_pkg.is_not_vmi ('VENDOR'
                                                                                    ,sup.req_line_id
                                                                                    ,sup.po_line_location_id) = 'Y'
                                      AND xxeis.eis_po_xxwc_isr_util_pkg.has_drop_ships (sup.po_header_id
                                                                                        ,sup.req_line_id
                                                                                        ,sup.po_line_location_id) = 'N'
                                 THEN
                                     NVL (to_org_primary_quantity, 0)
                                 ELSE
                                     0
                             END)
                             vendor_qty
                        ,SUM (
                             CASE
                                 WHEN     xxeis.eis_po_xxwc_isr_util_pkg.is_not_vmi ('DIRECT'
                                                                                    ,sup.req_line_id
                                                                                    ,sup.po_line_location_id) = 'Y'
                                      AND xxeis.eis_po_xxwc_isr_util_pkg.has_drop_ships (sup.po_header_id
                                                                                        ,sup.req_line_id
                                                                                        ,sup.po_line_location_id) = 'Y'
                                 THEN
                                     NVL (to_org_primary_quantity, 0)
                                 ELSE
                                     0
                             END)
                             direct_qty
                    FROM apps.mtl_supply sup, apps.po_requisition_headers_all prh
                   WHERE     sup.supply_type_code IN ('REQ')
                         AND sup.destination_type_code = 'INVENTORY'
                         AND sup.to_organization_id = p_organization_id
                         AND sup.item_id = p_inventory_item_id
                         AND prh.requisition_header_id = sup.req_header_id
                         --AND (p_req_type ='BOTH' OR  prh.type_lookup_code = p_req_type)
                         AND (   NVL (sup.from_organization_id, -1) <> p_organization_id
                              OR (    sup.from_organization_id = p_organization_id
                                  AND EXISTS
                                          (SELECT 'x'
                                             FROM apps.mtl_secondary_inventories sub1
                                            WHERE     sub1.organization_id = sup.from_organization_id
                                                  AND sub1.secondary_inventory_name = sup.from_subinventory
                                                  AND sub1.availability_type <> 1)))
                         AND (   sup.to_subinventory IS NULL
                              OR (EXISTS
                                      (SELECT 'x'
                                         FROM apps.mtl_secondary_inventories sub2
                                        WHERE     sub2.secondary_inventory_name = sup.to_subinventory
                                              AND sub2.organization_id = sup.to_organization_id
                                              AND sub2.availability_type = DECODE (2, 1, sub2.availability_type, 1))))
                         AND (   sup.po_line_location_id IS NULL
                              OR EXISTS
                                     (SELECT 'x'
                                        FROM apps.po_line_locations_all lilo
                                       WHERE     lilo.line_location_id = sup.po_line_location_id
                                             AND NVL (lilo.vmi_flag, 'N') = 'N'))
                GROUP BY sup.to_organization_id, sup.item_id)
        LOOP
            l_vendor_qty := l_rec.vendor_qty;
            l_direct_qty := l_rec.direct_qty;
            l_both_qty := l_rec.both_qty;
        END LOOP;

        FOR l_rec IN (SELECT SUM (qty) inventory_qty
                        FROM (SELECT NVL (SUM (prl.quantity), 0) qty
                                --INTO l_open_qty
                                FROM po_requisition_headers_all prh, po_requisition_lines_all prl
                               WHERE     prh.requisition_header_id = prl.requisition_header_id
                                     AND item_id = p_inventory_item_id
                                     AND NVL (prh.cancel_flag, 'N') = 'N'
                                     AND NVL (prl.cancel_flag, 'N') = 'N'
                                     AND NVL (prl.closed_code, 'OPEN') = 'OPEN'
                                     AND prh.authorization_status = 'INCOMPLETE'
                                     AND destination_organization_id = p_organization_id
                                     --        AND p_req_type                   = 'INVENTORY'
                                     AND source_type_code = 'INVENTORY'
                                     AND NOT EXISTS
                                                 (SELECT 1
                                                    FROM po_action_history pah
                                                   WHERE     pah.object_id = prh.requisition_header_id
                                                         AND pah.object_type_code = 'REQUISITION'
                                                         AND pah.action_code = 'CANCEL')))
        LOOP
            l_inventory_qty := l_rec.inventory_qty;
        END LOOP;

        p_direct_qty := NVL (l_direct_qty, 0);
        p_inventory_qty := NVL (l_inventory_qty, 0);
        p_vendor_qty := NVL (l_vendor_qty, 0);
        p_both_qty := NVL (l_both_qty, 0);
        RETURN;
    EXCEPTION
        WHEN OTHERS
        THEN
            p_direct_qty := 0;
            p_inventory_qty := 0;
            p_vendor_qty := 0;
            p_both_qty := 0;
    END get_isr_open_req_qty;

    FUNCTION get_isr_avail_qty (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER
    IS
        l_demand_qty   NUMBER := 0;
        l_avail_qty    NUMBER;
    BEGIN
        --This is code is commneted by srinivas--
        /* select sum(ol.ordered_quantity-nvl(ol.fulfilled_quantity,0))
         into  l_demand_qty
         from  oe_order_lines_all ol
         where OL.INVENTORY_ITEM_ID     = P_INVENTORY_ITEM_ID
         and   OL.SHIP_FROM_ORG_ID      = P_ORGANIZATION_ID
         and   (ol.ordered_quantity-nvl(ol.fulfilled_quantity,0))>0
         and   ol.flow_status_code not  in ('CANCELLED','CLOSED');*/

        --The following code added by srinivas---

        SELECT NVL (SUM (mr.reservation_quantity), 0)
          INTO l_demand_qty
          FROM                                                                                      --OE_ORDER_LINES OL,
               -- MTL_SALES_ORDERS MSO,
               mtl_reservations mr
         WHERE mr.inventory_item_id = p_inventory_item_id AND mr.organization_id = p_organization_id;

        -- AND   MR.DEMAND_SOURCE_LINE_ID  = OL.LINE_ID
        --        AND  ol.flow_status_CODE not in ('CLOSED')
        -- AND   MR.DEMAND_SOURCE_HEADER_ID=MSO.SALES_ORDER_ID;

        l_avail_qty :=
            (  xxeis.eis_po_xxwc_isr_util_qa_pkg.get_planning_quantity (2
                                                                       ,1
                                                                       ,p_organization_id
                                                                       ,NULL
                                                                       ,p_inventory_item_id)
             - NVL (l_demand_qty, 0));
        --l_avail_qty:=(get_onhand_inv(p_inventory_item_id,p_organization_id)-nvl(l_demand_qty,0));
        RETURN l_avail_qty;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            RETURN l_avail_qty;
    END;

    FUNCTION get_isr_ss_cnt (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER
    IS
        l_ss_item_cnt   NUMBER;
    BEGIN
        SELECT COUNT (*)
          INTO l_ss_item_cnt
          FROM mtl_system_items_kfv msi
         WHERE msi.inventory_item_id = p_inventory_item_id AND msi.source_organization_id = p_organization_id;

        RETURN l_ss_item_cnt;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_isr_sourcing_rule (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2
    IS
        l_sr_name   VARCHAR2 (500);
    BEGIN
        SELECT MAX (msr.sourcing_rule_name)
          INTO l_sr_name
          FROM mrp_sr_assignments ass
              ,mrp_sr_receipt_org rco
              ,mrp_sr_source_org sso
              ,mrp_sourcing_rules msr
              ,po_vendors pov
         WHERE     1 = 1
               AND ass.inventory_item_id(+) = p_inventory_item_id
               AND ass.organization_id(+) = p_organization_id
               AND rco.sourcing_rule_id(+) = ass.sourcing_rule_id
               AND sso.sr_receipt_id(+) = rco.sr_receipt_id
               AND msr.sourcing_rule_id = ass.sourcing_rule_id
               AND pov.vendor_id(+) = sso.vendor_id;

        RETURN l_sr_name;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_isr_ss (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER
    IS
        l_ss   NUMBER;
    BEGIN
        SELECT mst.safety_stock_quantity
          INTO l_ss
          FROM mtl_safety_stocks_view mst, org_acct_periods oap, org_organization_definitions ood
         WHERE     ood.organization_id = oap.organization_id
               AND inventory_item_id = p_inventory_item_id
               AND mst.organization_id = p_organization_id
               AND mst.organization_id = oap.organization_id
               AND TRUNC (mst.effectivity_date) BETWEEN period_start_date AND NVL (period_close_date, SYSDATE)
               AND TRUNC (SYSDATE) BETWEEN period_start_date AND NVL (period_close_date, SYSDATE);

        RETURN l_ss;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    FUNCTION get_int_req_so_qty (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER
    IS
        l_inter_sales_order_qty   NUMBER;
        l_internal_req_qty        NUMBER;
        l_booked_qty              NUMBER;
        l_avialble_qty            NUMBER;
        l_avialble_qty2           NUMBER;
    BEGIN
        --query to get internal sales order qty--

        /*   SELECT   SUM(OEL.ORDERED_QUANTITY-NVL(OL.FULFILLED_QUANTITY,0))
           into L_INTER_SALES_ORDER_QTY
     FROM  OE_ORDER_LINES         OEL,
           OE_ORDER_HEADERS        OEH
          -- PO_REQUISITION_HEADERS  PORH,
          -- PO_REQUISITION_LINES    PORL
       where OEH.HEADER_ID               = OEL.HEADER_ID
      -- AND OEL.SOURCE_DOCUMENT_ID        = PORH.REQUISITION_HEADER_ID
     --  AND OEL.SOURCE_DOCUMENT_LINE_ID   = PORL.REQUISITION_LINE_ID
      -- AND PORH.REQUISITION_HEADER_ID    = PORL.REQUISITION_HEADER_ID
       and OEL.SOURCE_TYPE_CODE          ='INTERNAL'
       and oeh. order_type_id = 1011
      -- and PORL.SOURCE_TYPE_CODE         ='INVENTORY'
     --  AND OEL.ORDER_SOURCE_ID           = 10             --order_source_id for 'Internal'
     --AND oel.orig_sys_document_ref = Int_Req_num'
      -- and OEL.ORG_ID                    = PORH.ORG_ID
       and OEL.INVENTORY_ITEM_ID          = P_INVENTORY_ITEM_ID
       and OEL.SHIP_FROM_ORG_ID           = P_ORGANIZATION_ID
     --  and PORH.TRANSFERRED_TO_OE_FLAG        = 'Y'
       and OL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED')
      and  exists(select 1
                           from PO_REQUISITION_LINES prl
                            where  oh.source_document_id = prl.REQUISITION_HEADER_ID);*/
        SELECT SUM (oel.ordered_quantity - NVL (oel.fulfilled_quantity, 0) - NVL (mr.reservation_quantity, 0))
          INTO l_inter_sales_order_qty
          FROM oe_order_lines_all oel
              ,oe_order_headers_all oeh
              ,mtl_sales_orders mso
              ,mtl_reservations mr
         WHERE     oeh.header_id = oel.header_id
               AND oel.source_type_code = 'INTERNAL'
               AND oeh.order_type_id = 1011
               AND mr.inventory_item_id(+) = oel.inventory_item_id
               AND mr.organization_id(+) = oel.ship_from_org_id
               AND mr.demand_source_line_id(+) = oel.line_id
               AND mso.sales_order_id(+) = mr.demand_source_header_id
               -- and OEL.ORDER_SOURCE_ID           = 10
               AND oel.inventory_item_id = p_inventory_item_id
               AND oel.ship_from_org_id = p_organization_id
               AND oel.flow_status_code NOT IN ('CANCELLED', 'CLOSED')
               AND EXISTS
                       (SELECT 1
                          FROM po_requisition_lines_all prl
                         WHERE oeh.source_document_id = prl.requisition_header_id);

        --query to get internal req which are not interfaced for sales orders ---

        SELECT SUM (prl.quantity)
          INTO l_internal_req_qty
          FROM po_requisition_lines prl, po_requisition_headers prh
         WHERE     prh.requisition_header_id = prl.requisition_header_id
               AND prh.type_lookup_code = 'INTERNAL'
               AND prl.source_type_code = 'INVENTORY'
               AND prl.item_id = p_inventory_item_id
               AND prl.source_organization_id = p_organization_id
               AND prh.transferred_to_oe_flag = 'N';

        --Query to get all booked sales order qty's exculding the return orders and open quotes and internale sales orders

        SELECT SUM (ol.ordered_quantity - NVL (ol.fulfilled_quantity, 0) - NVL (mr.reservation_quantity, 0))
          INTO l_booked_qty
          FROM oe_order_lines ol
              ,oe_order_headers oh
              ,mtl_sales_orders mso
              ,mtl_reservations mr
         WHERE     oh.header_id = ol.header_id
               AND ol.inventory_item_id = p_inventory_item_id
               AND ol.ship_from_org_id = p_organization_id
               AND mr.inventory_item_id(+) = ol.inventory_item_id
               AND mr.organization_id(+) = ol.ship_from_org_id
               AND mr.demand_source_line_id(+) = ol.line_id
               AND mso.sales_order_id(+) = mr.demand_source_header_id
               AND (ol.ordered_quantity - NVL (ol.fulfilled_quantity, 0)) > 0
               AND oh.flow_status_code IN ('BOOKED')
               AND oh.transaction_phase_code <> 'N'                                         --NOT ALLOWED TO OPEN QUOTES
               AND ol.line_category_code <> 'RETURN'                                      --NOT ALLOWED TO RETURN oRDERS
               AND ol.source_type_code <> 'INTERNAL'                             --Not allowed to Internale Sales Orders
               --and OH. ORDER_TYPE_ID            <> 1012   --Not allowed to Internale Sales Orders
               --and OL.ORDER_SOURCE_ID           <> 10 --Not allowed to Internale Sales Orders
               AND ol.flow_status_code NOT IN ('CANCELLED', 'CLOSED');

        l_avialble_qty := get_isr_avail_qty (p_inventory_item_id, p_organization_id);

        l_avialble_qty2 :=
            l_avialble_qty - (NVL (l_booked_qty, 0) + NVL (l_inter_sales_order_qty, 0) + NVL (l_internal_req_qty, 0));

        RETURN l_avialble_qty2;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_avialble_qty2 := 0;
    END;

    FUNCTION get_isr_rpt_dc_mod_sub
        RETURN VARCHAR2
    IS
    BEGIN
        RETURN g_isr_rpt_dc_mod_sub;
    END;

    FUNCTION get_onhand_inv (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER
    IS
        l_onhand_inv   NUMBER;
    BEGIN
        SELECT NVL (SUM (moq.transaction_quantity), 0)
          INTO l_onhand_inv
          FROM mtl_onhand_quantities_detail moq
         WHERE inventory_item_id = p_inventory_item_id AND organization_id = p_organization_id;

        RETURN l_onhand_inv;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 0;
    END;

    FUNCTION get_primary_bin_loc (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2
    IS
        l_seg   VARCHAR2 (5000);
    BEGIN
        SELECT mil.segment1
          INTO l_seg
          FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl
         WHERE     msl.inventory_item_id = p_inventory_item_id
               AND msl.organization_id = p_organization_id
               AND msl.secondary_locator = mil.inventory_location_id
               AND mil.segment1 LIKE '1%';

        RETURN l_seg;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    ---Below code for Demand Qty---

    FUNCTION get_staged_qty (p_org_id            NUMBER
                            ,p_subinv            VARCHAR2
                            ,p_item_id           NUMBER
                            ,p_order_line_id     NUMBER
                            ,p_include_nonnet    NUMBER)
        RETURN NUMBER
    IS
        l_staged_qty   NUMBER := 0;
    BEGIN
        BEGIN
            --
            -- Bugfix 2333526: Need to calculate staged quantity
            -- for sub level planning.  If passed-in (planning)
            -- sub is the also the staging sub, then ignore
            -- p_include_nonnet
            --
            SELECT NVL (SUM (primary_reservation_quantity), 0)
              INTO l_staged_qty
              FROM mtl_reservations
             WHERE     organization_id = p_org_id
                   AND inventory_item_id = p_item_id
                   AND demand_source_line_id = p_order_line_id
                   AND demand_source_type_id IN (2, 8, 12)
                   AND NVL (staged_flag, 'X') = 'Y'
                   AND subinventory_code IS NOT NULL
                   AND subinventory_code <> p_subinv;                                                     -- Bug 4313204
        EXCEPTION
            WHEN OTHERS
            THEN
                l_staged_qty := 0;
        END;

        RETURN l_staged_qty;
    END get_staged_qty;

    FUNCTION get_pick_released_qty (p_org_id           NUMBER
                                   ,p_subinv           VARCHAR2
                                   ,p_item_id          NUMBER
                                   ,p_order_line_id    NUMBER)
        RETURN NUMBER
    IS
        l_pick_released_qty   NUMBER := 0;
    BEGIN
        BEGIN
            --
            -- Move order type 3 is pick wave, source type 2 is sales order
            -- Bug 3181367 added transaction_source_type_id 8 too.
            SELECT NVL (SUM (mtrl.quantity - NVL (mtrl.quantity_delivered, 0)), 0)
              INTO l_pick_released_qty
              FROM mtl_txn_request_headers mtrh, mtl_txn_request_lines mtrl
             WHERE     mtrh.move_order_type = 3
                   AND mtrh.header_id = mtrl.header_id
                   AND mtrl.organization_id = p_org_id
                   AND mtrl.inventory_item_id = p_item_id
                   AND mtrl.from_subinventory_code = p_subinv
                   AND mtrl.txn_source_line_id = p_order_line_id
                   AND mtrl.transaction_source_type_id IN (2, 8)
                   AND mtrl.line_status NOT IN (5, 6);
        EXCEPTION
            WHEN OTHERS
            THEN
                l_pick_released_qty := 0;
        END;

        RETURN l_pick_released_qty;
    END get_pick_released_qty;

    FUNCTION get_loaded_qty (p_org_id       NUMBER
                            ,p_subinv       VARCHAR2
                            ,p_level        NUMBER
                            ,p_item_id      NUMBER
                            ,p_net_rsv      NUMBER
                            ,p_net_unrsv    NUMBER)
        RETURN NUMBER
    IS
        CURSOR c_loaded_quantities_v
        IS
            SELECT SUM (quantity)
              FROM wms_loaded_quantities_v
             WHERE     inventory_item_id = p_item_id
                   AND subinventory_code = NVL (p_subinv, subinventory_code)
                   AND organization_id = p_org_id;

        l_loaded_qty   NUMBER := 0;
    BEGIN
        --The loaded quantity will be calculated only if the report is ran with
        --parameters "reserved demand=>No , unreserved demand=>No".
        --If the parameters are "yes", the MTL_RESERVATIONS or MMTT will be accounted for this qty.

        IF (p_net_rsv = 2 AND p_net_unrsv = 2)
        THEN
            OPEN c_loaded_quantities_v;

            FETCH c_loaded_quantities_v INTO l_loaded_qty;

            CLOSE c_loaded_quantities_v;
        END IF;

        RETURN (l_loaded_qty);
    EXCEPTION
        WHEN OTHERS
        THEN
            fnd_file.put_line (fnd_file.LOG, 'The error is ' || SQLCODE || SQLERRM);

            RAISE;
    END get_loaded_qty;

    FUNCTION get_shipped_qty (p_organization_id IN NUMBER, p_inventory_item_id IN NUMBER, p_order_line_id IN NUMBER)
        RETURN NUMBER
    IS
        l_shipped_qty   NUMBER := 0;
    BEGIN
        --
        -- Only look at source types 2 and 8 (sales orders, internal orders)
        --
        SELECT NVL (SUM (primary_quantity), 0)
          INTO l_shipped_qty
          FROM mtl_material_transactions
         WHERE     transaction_action_id = 1
               AND source_line_id = p_order_line_id
               AND organization_id = p_organization_id
               AND inventory_item_id = p_inventory_item_id
               AND transaction_source_type_id IN (2, 8);

        IF l_shipped_qty IS NULL
        THEN
            l_shipped_qty := 0;
        ELSE
            l_shipped_qty := -1 * l_shipped_qty;
        END IF;

        RETURN l_shipped_qty;
    END get_shipped_qty;

    FUNCTION get_supply_qty (p_org_id                NUMBER
                            ,p_subinv                VARCHAR2
                            ,p_item_id               NUMBER
                            ,p_postproc_lead_time    NUMBER
                            ,p_cal_code              VARCHAR2
                            ,p_except_id             NUMBER
                            ,p_level                 NUMBER
                            ,p_s_cutoff              DATE
                            ,p_include_po            NUMBER
                            ,p_include_mo            NUMBER
                            ,p_vmi_enabled           VARCHAR2
                            ,p_include_nonnet        NUMBER
                            ,p_include_wip           NUMBER
                            ,p_include_if            NUMBER
                            /* nsinghi MIN-MAX INVCONV start */
                            ,p_process_org           VARCHAR2                          /* nsinghi MIN-MAX INVCONV end */
                                                             )
        RETURN NUMBER
    IS
        l_qty        NUMBER := 0;
        l_total      NUMBER := 0;
        l_puom       VARCHAR2 (3);
        --   G_TRACE_ON     number:=1;

        -- supply records. Modified the query to consider receipt_date for
        -- records with supply type code 'SHIPMENT' and 'RECEIVING'

        l_stmt       VARCHAR2 (4000)
            := ' SELECT NVL(sum(to_org_primary_quantity), 0)
                FROM mtl_supply         sup
               WHERE sup.supply_type_code IN (''PO'',''REQ'',''SHIPMENT'',''RECEIVING'')
                 AND sup.destination_type_code  = ''INVENTORY''
                 AND sup.to_organization_id     = :l_org_id
                 AND sup.item_id                = :l_item_id
                 AND (NVL(sup.from_organization_id,-1) <> :l_org_id
                      OR (sup.from_organization_id      = :l_org_id
                          AND ((:l_include_nonnet       = 2
                                AND
                                EXISTS (SELECT ''x''
                                          FROM mtl_secondary_inventories sub1
                                         WHERE sub1.organization_id          = sup.from_organization_id
                                           AND sub1.secondary_inventory_name = sup.from_subinventory
                                           AND sub1.availability_type       <> 1
                                       )
                               )
                               OR :l_level = 2
                              )
                         )
                     )
                 AND (sup.to_subinventory IS NULL
                      OR
                      (EXISTS (SELECT ''x''
                                 FROM mtl_secondary_inventories sub2
                                WHERE sub2.secondary_inventory_name = sup.to_subinventory
                                  AND sub2.organization_id          = sup.to_organization_id
                                  AND sub2.availability_type        = decode(:l_include_nonnet,
                                                                             1,sub2.availability_type,
                                                                             1)
                              )
                      )
                      OR :l_level = 2
                     )
                 AND (:l_level = 1 OR to_subinventory = :l_subinv)
-- Bug 5041763 Not considering supply from drop ship orders
                    AND NOT EXISTS (SELECT ''X'' FROM apps.OE_DROP_SHIP_SOURCES ODSS
                            WHERE   DECODE(sup.PO_HEADER_ID, NULL, sup.REQ_LINE_ID, sup.PO_LINE_LOCATION_ID) =
                                    DECODE(sup.PO_HEADER_ID,NULL, ODSS.REQUISITION_LINE_ID, ODSS.LINE_LOCATION_ID)) ';

        /*' SELECT NVL(sum(to_org_primary_quantity), 0)
            FROM mtl_supply         sup
              , apps.bom_calendar_dates c
              , apps.bom_calendar_dates c1
           WHERE sup.supply_type_code IN (''PO'',''REQ'',''SHIPMENT'',''RECEIVING'')
             AND sup.destination_type_code  = ''INVENTORY''
             AND sup.to_organization_id     = :l_org_id
             AND sup.item_id                = :l_item_id
             AND c.calendar_code            = :l_cal_code
             AND c.exception_set_id         = :l_except_id
             AND c.calendar_date            = trunc(decode(sup.supply_type_code, ''SHIPMENT'', sup.receipt_date, ''RECEIVING'', sup.receipt_date,nvl(sup.need_by_date, sup.receipt_date)))
             AND c1.calendar_code           = c.calendar_code
             AND c1.exception_set_id        = c.exception_set_id
             AND c1.seq_num                 = (c.next_seq_num + trunc(:l_postproc_lead_time))
             AND c1.calendar_date   <= :l_s_cutoff + 0.99999  /* bug no 6009682 */
                /* AND (NVL(sup.from_organization_id,-1) <> :l_org_id
                      OR (sup.from_organization_id      = :l_org_id
                          AND ((:l_include_nonnet       = 2
                                AND
                                EXISTS (SELECT ''x''
                                          FROM mtl_secondary_inventories sub1
                                         WHERE sub1.organization_id          = sup.from_organization_id
                                           AND sub1.secondary_inventory_name = sup.from_subinventory
                                           AND sub1.availability_type       <> 1
                                       )
                               )
                               OR :l_level = 2
                              )
                         )
                     )
                 AND (sup.to_subinventory IS NULL
                      OR
                      (EXISTS (SELECT ''x''
                                 FROM mtl_secondary_inventories sub2
                                WHERE sub2.secondary_inventory_name = sup.to_subinventory
                                  AND sub2.organization_id          = sup.to_organization_id
                                  AND sub2.availability_type        = decode(:l_include_nonnet,
                                                                             1,sub2.availability_type,
                                                                             1)
                              )
                      )
                      OR :l_level = 2
                     )
                 AND (:l_level = 1 OR to_subinventory = :l_subinv)
-- Bug 5041763 Not considering supply from drop ship orders
                    AND NOT EXISTS (SELECT ''X'' FROM apps.OE_DROP_SHIP_SOURCES ODSS
                            WHERE   DECODE(sup.PO_HEADER_ID, NULL, sup.REQ_LINE_ID, sup.PO_LINE_LOCATION_ID) =
                                    DECODE(sup.PO_HEADER_ID,NULL, ODSS.REQUISITION_LINE_ID, ODSS.LINE_LOCATION_ID)) '; */

        l_vmi_stmt   VARCHAR2 (2000)
                         := ' AND (sup.po_line_location_id IS NULL
                   OR EXISTS (SELECT ''x''
                                FROM po_line_locations_all lilo
                               WHERE lilo.line_location_id    = sup.po_line_location_id
                                 AND NVL(lilo.vmi_flag,''N'') = ''N''
                             )
                  )
              AND (sup.req_line_id IS NULL
                   OR EXISTS (SELECT ''x''
                                FROM po_requisition_lines_all prl
                               WHERE prl.requisition_line_id = sup.req_line_id
                                 AND NVL(prl.vmi_flag,''N'') = ''N''
                             )
                  )';

        TYPE c_po_sup_curtype IS REF CURSOR;

        c_po_qty     c_po_sup_curtype;
    BEGIN
        /*dbms_output.put_line('sbitrap_org_id: '               || to_char(p_org_id)         ||
                    ', p_subinv: '             || p_subinv                  ||
                    ', p_item_id: '            || to_char(p_item_id)        ||
                    ', p_postproc_lead_time: ' || to_char(p_postproc_lead_time) ||
                    ', p_cal_code: '           || p_cal_code                ||
                    ', p_except_id: '          || to_char(p_except_id)      ||
                    ', p_level: '              || to_char(p_level)          ||
                    ', p_s_cutoff: '           || to_char(p_s_cutoff, 'DD-MON-YYYY HH24:MI:SS') ||
                    ', p_include_po: '         || to_char(p_include_po)     ||
                    ', p_include_mo: '         || to_char(p_include_mo)     ||
                    ', p_include_nonnet: '     || to_char(p_include_nonnet) ||
                    ', p_include_wip: '        || to_char(p_include_wip)    ||
                    ', p_include_if: '         || to_char(p_include_if)
                    , 'get_supply_qty'
                    , 9);
    */

        l_total := 0;

        --
        -- MTL_SUPPLY
        --

        IF p_include_po = 1
        THEN
            IF (p_vmi_enabled = 'Y') AND (p_level = 1)
            THEN
                OPEN c_po_qty FOR l_stmt || l_vmi_stmt
                    USING p_org_id
                         ,p_item_id
                         ,p_org_id
                         ,p_org_id
                         ,p_include_nonnet
                         ,p_level
                         ,p_include_nonnet
                         ,p_level
                         ,p_level
                         ,p_subinv;
            ELSE
                OPEN c_po_qty FOR l_stmt
                    USING p_org_id
                         ,p_item_id
                         ,p_org_id
                         ,p_org_id
                         ,p_include_nonnet
                         ,p_level
                         ,p_include_nonnet
                         ,p_level
                         ,p_level
                         ,p_subinv;
            END IF;

            FETCH c_po_qty INTO l_qty;

            --DBMS_OUTPUT.put_line ('the main query is ' || l_stmt || l_vmi_stmt);

            CLOSE c_po_qty;

            --DBMS_OUTPUT.put_line ('Supply from mtl_supply: ' || TO_CHAR (l_qty));

            l_total := l_total + l_qty;
        END IF;

        --
        -- Take into account the quantity for which a move order
        -- has already been created assuming that the move order
        -- can be created only within the same org

        IF (p_level = 2 AND p_include_mo = 1)
        THEN
            SELECT NVL (SUM (mtrl.quantity - NVL (mtrl.quantity_delivered, 0)), 0)
              INTO l_qty
              FROM mtl_transaction_types mtt, mtl_txn_request_lines mtrl
             WHERE     mtt.transaction_action_id IN (2, 28)
                   AND mtt.transaction_type_id = mtrl.transaction_type_id
                   AND mtrl.organization_id = p_org_id
                   AND mtrl.inventory_item_id = p_item_id
                   AND mtrl.to_subinventory_code = p_subinv
                   AND mtrl.line_status IN (3, 7);

            --AND mtrl.date_required      <= p_s_cutoff + 0.99999;

            --DBMS_OUTPUT.put_line ('Supply from move orders: ' || TO_CHAR (l_qty));

            l_total := l_total + l_qty;
        END IF;

        --
        -- Supply FROM WIP discrete job is to be included at Org Level Planning Only
        --
        IF p_level = 1 AND p_include_wip = 1
        THEN
            /* Here check will need to be made if the org is Process org or Discrete org. */

            IF p_process_org = 'Y'
            THEN
                SELECT SUM (
                             NVL ( (NVL (d.wip_plan_qty, d.plan_qty) - d.actual_qty), 0)
                           * (original_primary_qty / original_qty))
                  INTO l_qty
                  FROM gme_material_details d, gme_batch_header h
                 WHERE     h.batch_type IN (0, 10)
                       AND h.batch_status IN (1, 2)
                       AND h.batch_id = d.batch_id
                       AND d.inventory_item_id = p_item_id
                       AND d.organization_id = p_org_id
                       AND NVL (d.original_qty, 0) <> 0
                       --AND    d.material_requirement_date <= p_s_cutoff
                       AND d.line_type > 0;

                --DBMS_OUTPUT.put_line ('Supply from OPM Batches : ' || TO_CHAR (l_qty));

                l_total := l_total + NVL (l_qty, 0);

                SELECT SUM (NVL (start_quantity, 0) - NVL (quantity_completed, 0) - NVL (quantity_scrapped, 0))
                  INTO l_qty
                  FROM wip_discrete_jobs
                 WHERE     organization_id = p_org_id
                       AND primary_item_id = p_item_id
                       AND job_type IN (1, 3)
                       AND status_type IN (1, 3, 4, 6)
                       --Bug 2647862
                       -- AND scheduled_completion_date <= p_s_cutoff + 0.99999 /* bug no 6009682 */
                       AND (NVL (start_quantity, 0) - NVL (quantity_completed, 0) - NVL (quantity_scrapped, 0)) > 0;

                l_total := l_total + NVL (l_qty, 0);

                --
                -- WIP REPETITIVE JOBS to be included at Org Level Planning Only
                --
                SELECT SUM (
                               daily_production_rate
                             * GREATEST (0, LEAST (processing_work_days, p_s_cutoff + 90 - first_unit_completion_date))
                           - quantity_completed)
                  INTO l_qty
                  FROM wip_repetitive_schedules wrs, wip_repetitive_items wri
                 WHERE     wrs.organization_id = p_org_id
                       AND wrs.status_type IN (1, 3, 4, 6)
                       AND wri.organization_id = p_org_id
                       AND wri.primary_item_id = p_item_id
                       AND wri.wip_entity_id = wrs.wip_entity_id
                       AND wri.line_id = wrs.line_id
                       AND (    daily_production_rate
                              * GREATEST (0
                                         ,LEAST (processing_work_days, p_s_cutoff + 90 - first_unit_completion_date))
                            - quantity_completed) > 0;

                --DBMS_OUTPUT.put_line ('Supply from WIP repetitive schedules: ' || TO_CHAR (l_qty));

                l_total := l_total + NVL (l_qty, 0);
            ELSE
                /* nsinghi MIN-MAX INVCONV end */

                SELECT SUM (NVL (start_quantity, 0) - NVL (quantity_completed, 0) - NVL (quantity_scrapped, 0))
                  INTO l_qty
                  FROM wip_discrete_jobs
                 WHERE     organization_id = p_org_id
                       AND primary_item_id = p_item_id
                       AND job_type IN (1, 3)
                       AND status_type IN (1, 3, 4, 6)
                       --Bug 2647862
                       -- AND scheduled_completion_date <= p_s_cutoff + 0.99999 /* bug no 6009682 */
                       AND (NVL (start_quantity, 0) - NVL (quantity_completed, 0) - NVL (quantity_scrapped, 0)) > 0;

                --DBMS_OUTPUT.put_line ('Supply from WIP discrete jobs: ' || TO_CHAR (l_qty));

                l_total := l_total + NVL (l_qty, 0);

                --
                -- WIP REPETITIVE JOBS to be included at Org Level Planning Only
                --
                SELECT SUM (
                               daily_production_rate
                             * GREATEST (0, LEAST (processing_work_days, p_s_cutoff + 90 - first_unit_completion_date))
                           - quantity_completed)
                  INTO l_qty
                  FROM wip_repetitive_schedules wrs, wip_repetitive_items wri
                 WHERE     wrs.organization_id = p_org_id
                       AND wrs.status_type IN (1, 3, 4, 6)
                       AND wri.organization_id = p_org_id
                       AND wri.primary_item_id = p_item_id
                       AND wri.wip_entity_id = wrs.wip_entity_id
                       AND wri.line_id = wrs.line_id
                       AND (    daily_production_rate
                              * GREATEST (0
                                         ,LEAST (processing_work_days, p_s_cutoff + 90 - first_unit_completion_date))
                            - quantity_completed) > 0;

                --DBMS_OUTPUT.put_line ('Supply from WIP repetitive schedules: ' || TO_CHAR (l_qty));

                l_total := l_total + NVL (l_qty, 0);
            END IF;                                                                            /* p_process_org = 'Y' */
        END IF;

        IF (p_include_if = 2)
        THEN
            RETURN (l_total);
        END IF;

        --
        -- po_requisitions_interface_all
        --
        -- Bug: 2320752
        -- Do not include records in error status
        -- kkoothan Bug Fix 2891818
        -- Used NVL function for the condition involving process_flag
        -- so that the interface records with NULL value for process_flag
        -- are considered as Interface Supply.
        --
        /* Bug 3894347 -- Added the following section of code to consider conversion
           of quantities in po_requisitions_interface_all if uom_code is different than
           primary uom code */

        SELECT uom_code
          INTO l_puom
          FROM mtl_system_items_vl msiv, mtl_units_of_measure_vl muom
         WHERE     msiv.inventory_item_id = p_item_id
               AND msiv.organization_id = p_org_id
               AND muom.unit_of_measure = msiv.primary_unit_of_measure;

        --Bug9122329, calling the function get_item_uom_code in case when uom_code is null
        --in the po_requisitions_interface_all table.

        /*  SELECT NVL(SUM(DECODE(NVL(uom_code,get_item_uom_code(unit_of_measure)),
                                  L_PUOM,QUANTITY,
                                  APPS.INV_CONVERT.INV_UM_CONVERT(P_ITEM_ID,null,QUANTITY,NVL(UOM_CODE,GET_ITEM_UOM_CODE(UNIT_OF_MEASURE)),L_PUOM,null,null)
                                )),0)

            INTO l_qty
            FROM po_requisitions_interface_all
           WHERE destination_organization_id = p_org_id
             AND item_id                     = p_item_id
             AND p_include_po                = 1
             AND (p_level = 1 or destination_subinventory = p_subinv)
             AND need_by_date               <= (trunc(p_s_cutoff) + 1 - (1/(24*60*60)))
             AND NVL(process_flag,'@@@') <> 'ERROR'
             AND (NVL(source_organization_id,-1) <> p_org_id OR
                  (source_organization_id         = p_org_id AND
                   (( p_include_nonnet            = 2 AND
                     EXISTS (SELECT 'x'
                               FROM mtl_secondary_inventories sub1
                              WHERE sub1.organization_id          = source_organization_id
                                AND sub1.secondary_inventory_name = source_subinventory
                                AND sub1.availability_type       <> 1)) OR
                   p_level = 2)
                 ))
             AND (destination_subinventory IS NULL OR
                  EXISTS (SELECT 1
                            FROM mtl_secondary_inventories sub2
                           WHERE secondary_inventory_name = destination_subinventory
                             AND destination_subinventory = NVL(p_subinv,
                                                            destination_subinventory)
                             AND sub2.organization_id     = p_org_id
                             AND sub2.availability_type   = decode(p_include_nonnet,
                                                                   1,sub2.availability_type,1)) OR
                  p_level = 2);


          --DBMS_OUTPUT.put_line('Supply from po_requisitions_interface_all: ' || to_char(l_qty)
                      );*/

        l_total := l_total + NVL (l_qty, 0);

        --
        -- WIP_JOB_SCHEDULE_INTERFACE, processed immediately, hence not included
        --
        -- Supply FROM Flow to be included in org level only
        --
        IF p_level = 1
        THEN
            SELECT SUM (NVL (planned_quantity, 0) - NVL (quantity_completed, 0))
              INTO l_qty
              FROM wip_flow_schedules
             WHERE organization_id = p_org_id AND primary_item_id = p_item_id AND status = 1 AND scheduled_flag = 1 -- Bug 3151797
                                                                                                                    --Bug 2647862
                                                                                                                    --AND scheduled_completion_date <= p_s_cutoff + 0.99999 /* bug no 6009682 */
                   AND (NVL (planned_quantity, 0) - NVL (quantity_completed, 0)) > 0;

            --DBMS_OUTPUT.put_line ('Supply from WIP flow schedules: ' || TO_CHAR (l_qty));

            l_total := l_total + NVL (l_qty, 0);
        END IF;

        RETURN (l_total);
    EXCEPTION
        WHEN OTHERS
        THEN
            IF c_po_qty%ISOPEN
            THEN
                CLOSE c_po_qty;
            END IF;

            RETURN (l_total);
    --DBMS_OUTPUT.put_line ('The error is ' || SQLCODE || SQLERRM);
    END get_supply_qty;

    FUNCTION get_item_uom_code (p_uom_name VARCHAR2)
        RETURN VARCHAR2
    IS
        l_uom_code   mtl_units_of_measure.uom_code%TYPE := NULL;
    BEGIN
        SELECT uom_code
          INTO l_uom_code
          FROM mtl_units_of_measure_vl
         WHERE unit_of_measure = p_uom_name;

        RETURN (l_uom_code);
    EXCEPTION
        WHEN OTHERS
        THEN
            --DBMS_OUTPUT.put_line ('Error in  get_item_uom_code function');

            RAISE;
    END get_item_uom_code;

    FUNCTION get_demand_qty (p_org_id            NUMBER
                            ,p_subinv            VARCHAR2
                            ,p_level             NUMBER
                            ,p_item_id           NUMBER
                            ,p_d_cutoff          DATE
                            ,p_include_nonnet    NUMBER
                            ,p_net_rsv           NUMBER
                            ,p_net_unrsv         NUMBER
                            ,p_net_wip           NUMBER
                            /* nsinghi MIN-MAX INVCONV start */
                            ,p_process_org       VARCHAR2                              /* nsinghi MIN-MAX INVCONV end */
                                                         )
        RETURN NUMBER
    IS
        qty                   NUMBER := 0;
        total                 NUMBER := 0;
        l_total_demand_qty    NUMBER := 0;
        l_demand_qty          NUMBER := 0;
        l_total_reserve_qty   NUMBER := 0;
        l_pick_released_qty   NUMBER := 0;
        l_staged_qty          NUMBER := 0;
        l_sub_reserve_qty     NUMBER := 0;
        l_allocated_qty       NUMBER := 0;
        l_loaded_qty          NUMBER := 0;                                                              /*Bug 6240025 */
    BEGIN
        --DBMS_OUTPUT.put_line ('The Process start here');

        --
        -- select unreserved qty from mtl_demand for non oe rows.
        --
        IF p_net_unrsv = 1
        THEN
            --DBMS_OUTPUT.put_line (' unreserved qty from mtl_demand for non oe rows.');

            /*4518296*/
            SELECT SUM (primary_uom_quantity - GREATEST (NVL (reservation_quantity, 0), NVL (completed_quantity, 0)))
              INTO qty
              FROM mtl_demand
             WHERE     reservation_type = 1
                   AND parent_demand_id IS NULL
                   AND organization_id = p_org_id
                   AND primary_uom_quantity > GREATEST (NVL (reservation_quantity, 0), NVL (completed_quantity, 0))
                   AND inventory_item_id = p_item_id
                   --and REQUIREMENT_DATE      <= sysdate + 0.99999 /* bug no 6009682 */
                   AND demand_source_type NOT IN (2, 8, 12)
                   AND (p_level = 1 OR subinventory = p_subinv)                          -- Included later for ORG Level
                   AND (   subinventory IS NULL
                        OR p_level = 2
                        OR EXISTS
                               (SELECT 1
                                  FROM mtl_secondary_inventories s
                                 WHERE     s.organization_id = p_org_id
                                       AND s.secondary_inventory_name = subinventory
                                       AND s.availability_type = DECODE (p_include_nonnet, 1, s.availability_type, 1)))
                   /* nsinghi MIN-MAX INVCONV start */
                   AND (   locator_id IS NULL
                        OR p_level = 2
                        OR EXISTS
                               (SELECT 1
                                  FROM mtl_item_locations mil
                                 WHERE     mil.organization_id = p_org_id
                                       AND mil.inventory_location_id = locator_id
                                       AND mil.subinventory_code = NVL (subinventory, mil.subinventory_code)
                                       AND mil.availability_type =
                                               DECODE (p_include_nonnet, 1, mil.availability_type, 1)))
                   AND (   lot_number IS NULL
                        OR p_level = 2
                        OR EXISTS
                               (SELECT 1
                                  FROM mtl_lot_numbers mln
                                 WHERE     mln.organization_id = p_org_id
                                       AND mln.lot_number = lot_number
                                       AND mln.inventory_item_id = p_item_id
                                       AND mln.availability_type =
                                               DECODE (p_include_nonnet, 1, mln.availability_type, 1)));

            --DBMS_OUTPUT.put_line (' unreserved qty from mtl_demand for non oe rows.' || qty);

            total := total + NVL (qty, 0);

            /* nsinghi MIN-MAX INVCONV end */

            /*   IF G_TRACE_ON = 1 THEN
               print_debug('Demand from mtl_demand: ' || to_char(qty), 'get_demand_qty', 9);
               END IF;

           END IF;*/

            --
            -- select the reserved quantity from mtl_reservations for non OE rows
            --
            IF p_net_rsv = 1
            THEN
                --DBMS_OUTPUT.put_line ('select the reserved quantity from mtl_reservations for non OE rows ');

                SELECT SUM (primary_reservation_quantity)
                  INTO qty
                  FROM mtl_reservations
                 WHERE     organization_id = p_org_id
                       AND inventory_item_id = p_item_id
                       --and REQUIREMENT_DATE <= sysdate + 0.99999 /* bug no 6009682 */
                       AND demand_source_type_id NOT IN (2, 8, 12)
                       AND (p_level = 1 OR subinventory_code = p_subinv)                 -- Included later for ORG Level
                       AND (   subinventory_code IS NULL
                            OR p_level = 2
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = p_org_id
                                           AND s.secondary_inventory_name = subinventory_code
                                           AND s.availability_type =
                                                   DECODE (p_include_nonnet, 1, s.availability_type, 1)))
                       /* nsinghi MIN-MAX INVCONV start */
                       AND (   locator_id IS NULL
                            OR p_level = 2
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_item_locations mil
                                     WHERE     mil.organization_id = p_org_id
                                           AND mil.inventory_location_id = locator_id
                                           AND mil.subinventory_code = NVL (subinventory_code, mil.subinventory_code)
                                           AND mil.availability_type =
                                                   DECODE (p_include_nonnet, 1, mil.availability_type, 1)))
                       AND (   lot_number IS NULL
                            OR p_level = 2
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_lot_numbers mln
                                     WHERE     mln.organization_id = p_org_id
                                           AND mln.lot_number = lot_number
                                           AND mln.inventory_item_id = p_item_id
                                           AND mln.availability_type =
                                                   DECODE (p_include_nonnet, 1, mln.availability_type, 1)));

                --DBMS_OUTPUT.put_line ('select the reserved quantity from mtl_reservations for non OE rows ' || qty);
                /* nsinghi MIN-MAX INVCONV end */

                /*  IF G_TRACE_ON = 1 THEN
                  print_debug('Demand (reserved qty) for non OE rows in mtl_reservations: ' || to_char(qty)
                              , 'get_demand_qty'
                              , 9);
                  END IF;*/
                total := total + NVL (qty, 0);
            --DBMS_OUTPUT.put_line ('select the reserved quantity from mtl_reservations for non OE rows ' || total);
            END IF;

            --
            -- get the total demand which is the difference between the
            -- ordered qty. and the shipped qty.
            -- This gives the total demand including the reserved
            -- and the unreserved material.
            --
            -- Bug 2333526: For sub level planning we need to compute
            -- the staged qty.  The existing WHERE clause makes sure
            -- we only do this when the order is sourced from the
            -- planning sub: level = 1... or SUBINVENTORY = p_subinv
            --
            -- Bug 2350243: For sub level, calculate pick released
            -- (move order) qty
            --

            -- Bug 3480523, from patch I onwards schedule_ship_date is being populated
            -- with time component, hence truncating it to get the same day demand. These
            -- changes are also in mtl_reservation queries.
            IF p_net_unrsv = 1
            THEN
                --DBMS_OUTPUT.put_line ('get the total demand');

                SELECT SUM (
                             NVL (ordered_quantity, 0)
                           - NVL (xxeis.eis_po_xxwc_isr_util_qa_pkg.get_shipped_qty (p_org_id, p_item_id, ool.line_id)
                                 ,0))
                      ,SUM (DECODE (2
                                   ,2, xxeis.eis_po_xxwc_isr_util_qa_pkg.get_staged_qty (p_org_id
                                                                                        --, p_subinv
                                                                                        ,ool.subinventory
                                                                                        ,p_item_id
                                                                                        ,ool.line_id
                                                                                        ,p_include_nonnet)
                                   ,0))
                      ,SUM (DECODE (2
                                   ,2, xxeis.eis_po_xxwc_isr_util_qa_pkg.get_pick_released_qty (p_org_id
                                                                                               -- , p_subinv
                                                                                               ,ool.subinventory
                                                                                               ,p_item_id
                                                                                               ,ool.line_id)
                                   ,0))
                  INTO l_total_demand_qty, l_staged_qty, l_pick_released_qty
                  FROM oe_order_lines_all ool
                 WHERE     ship_from_org_id = p_org_id
                       AND open_flag = 'Y'
                       AND visible_demand_flag = 'Y'
                       AND shipped_quantity IS NULL
                       AND inventory_item_id = p_item_id
                       --and schedule_ship_date <= sysdate + 0.99999 /* bug no 6009682 */
                       AND DECODE (ool.source_document_type_id, 10, 8, DECODE (ool.line_category_code, 'ORDER', 2, 12)) IN
                               (2, 8, 12)
                       AND (   (    p_level = 1
                                AND DECODE (ool.source_document_type_id
                                           ,10, 8
                                           ,DECODE (ool.line_category_code, 'ORDER', 2, 12)) <> 8)
                            OR subinventory = p_subinv)                                  -- Included later for ORG Level
                       AND (   subinventory IS NULL
                            OR 2 = 2
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = p_org_id
                                           AND s.secondary_inventory_name = subinventory
                                           AND s.availability_type =
                                                   DECODE (p_include_nonnet, 1, s.availability_type, 1)));
            --DBMS_OUTPUT.put_line ('get the total demand' || TO_CHAR (l_total_demand_qty));
            /*IF G_TRACE_ON = 1 THEN
            print_debug('Demand from sales orders: ' ||
                        ' Ordered: '        || to_char(l_total_demand_qty)  ||
                        ', Pick released: ' || to_char(l_pick_released_qty) ||
                        ', Staged: '        || to_char(l_staged_qty)
                        , 'get_demand_qty'
                        , 9);
            END IF;*/
            END IF;

            --
            -- Find out the reserved qty for the material from mtl_reservations
            --
            -- Since total demand = reserved + unreserved, and we know total
            -- demand from oe_order_lines_all (above) we only need to query
            -- mtl_reservations if the user wants one of the following:
            --
            --  1) Only reserved: (p_net_rsv = 1 and p_net_unrsv = 2)
            --
            --  OR
            --
            --  2) Only unreserved: (p_net_rsv = 2 and p_net_unrsv = 1)
            --

            IF ( (p_net_rsv = 1 AND p_net_unrsv = 2) OR (p_net_rsv = 2 AND p_net_unrsv = 1))
            THEN
                --DBMS_OUTPUT.put_line ('Only reserved');

                SELECT SUM (primary_reservation_quantity)
                  INTO l_total_reserve_qty
                  FROM mtl_reservations
                 WHERE     organization_id = p_org_id
                       AND inventory_item_id = p_item_id
                       --and REQUIREMENT_DATE <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                       AND demand_source_type_id IN (2, 8, 12)
                       AND ( (p_level = 1 AND demand_source_type_id <> 8) OR subinventory_code = p_subinv) -- Included later for ORG Level
                       AND (   subinventory_code IS NULL
                            OR p_level = 2
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = p_org_id
                                           AND s.secondary_inventory_name = subinventory_code
                                           AND s.availability_type =
                                                   DECODE (p_include_nonnet, 1, s.availability_type, 1)))
                       /* nsinghi MIN-MAX INVCONV start */
                       AND (   locator_id IS NULL
                            OR p_level = 2
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_item_locations mil
                                     WHERE     mil.organization_id = p_org_id
                                           AND mil.inventory_location_id = locator_id
                                           AND mil.subinventory_code = NVL (subinventory_code, mil.subinventory_code)
                                           AND mil.availability_type =
                                                   DECODE (p_include_nonnet, 1, mil.availability_type, 1)))
                       AND (   lot_number IS NULL
                            OR p_level = 2
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_lot_numbers mln
                                     WHERE     mln.organization_id = p_org_id
                                           AND mln.lot_number = lot_number
                                           AND mln.inventory_item_id = p_item_id
                                           AND mln.availability_type =
                                                   DECODE (p_include_nonnet, 1, mln.availability_type, 1)))
                       -- Bug 5041763 excluding drop ship demand
                       AND NOT EXISTS
                               (SELECT 1
                                  FROM oe_drop_ship_sources odss
                                 WHERE odss.line_id = demand_source_line_id);
            --DBMS_OUTPUT.put_line ('Only reserved' || l_total_reserve_qty);
            /* nsinghi MIN-MAX INVCONV end */

            /* IF G_TRACE_ON = 1 THEN
             print_debug('Reserved demand (sales orders): ' || to_char(l_total_reserve_qty)
                         , 'get_demand_qty'
                         , 9);
             END IF;*/
            END IF;

            --
            -- Bug 3238390, we need to take care of reservations with sub but sales order
            -- with no sub for sub level planning. Adding the below query
            --

            /*  print_debug('Allocated demand for subinventory: ' || to_char(l_allocated_qty)
                            , 'get_demand_qty'
                            , 9);*/

            --
            -- total demand is calculated as follows:
            -- if we have to consider both unreserved matl and reserved matl. then the
            --    demand is simply the total demand = ordered qty - shipped qty.
            --    Bug 2333526: Deduct staged qty for sub level.  (l_staged_qty
            --    is always set to 0 for org level planning).
            --    Bug 3238390, add reserved qty for sales orders with no sub
            --    and reservation with sub for sub level planning.
            -- elsif we have to take into account only reserved matl. then the
            --    demand is simply the reservations from mtl_reservations for the matl.
            -- elsif we have to take into account just the unreserved matl. then the
            --    demand is total demand - the reservations for the material.
            --    Bug 3238390, add reserved qty for sales orders with no sub
            --    and reservation with sub for sub level planning, so that demand doesn't go -ve.
            IF p_net_unrsv = 1 AND p_net_rsv = 2
            THEN
                --DBMS_OUTPUT.put_line (' P_NET_UNRSV = 1 and P_NET_RSV = 1');
                l_demand_qty :=
                      NVL (l_total_demand_qty, 0)
                    - NVL (l_staged_qty, 0)
                    - NVL (l_pick_released_qty, 0)
                    + NVL (l_sub_reserve_qty, 0)
                    + NVL (l_allocated_qty, 0);
            --DBMS_OUTPUT.put_line (' P_NET_UNRSV = 1 and P_NET_RSV = 1 is ' || l_demand_qty);
            --DBMS_OUTPUT.put_line (' L_TOTAL_DEMAND_QTY' || l_total_demand_qty);
            --DBMS_OUTPUT.put_line (' l_pick_released_qty' || l_pick_released_qty);
            --DBMS_OUTPUT.put_line (' l_sub_reserve_qty' || l_sub_reserve_qty);
            --DBMS_OUTPUT.put_line (' L_ALLOCATED_QTY' || l_allocated_qty);
            --DBMS_OUTPUT.put_line (' l_staged_qty' || l_staged_qty);
            ELSIF p_net_rsv = 2
            THEN
                l_demand_qty := NVL (l_total_reserve_qty, 0) + NVL (l_allocated_qty, 0);
            --DBMS_OUTPUT.put_line (' p_net_rsv' || l_demand_qty);
            ELSIF p_net_unrsv = 1
            THEN
                l_demand_qty := NVL (l_total_demand_qty, 0) - NVL (l_total_reserve_qty, 0) + NVL (l_sub_reserve_qty, 0);
            --DBMS_OUTPUT.put_line (' p_net_unrsv' || l_demand_qty);
            END IF;

            /*  IF G_TRACE_ON = 1 THEN
              print_debug('Demand from shippable orders: ' || to_char(l_demand_qty)
                          , 'get_demand_qty'
                          , 9);
              END IF;*/
            total := total + NVL (l_demand_qty, 0);

            --DBMS_OUTPUT.put_line (' total := total + NVL(l_demand_qty,0);' || total);

            --
            -- Take care of internal orders for org level planning
            --
            IF p_level = 1
            THEN
                --DBMS_OUTPUT.put_line ('Take care of internal orders for org level planning');
                l_total_demand_qty := 0;
                l_demand_qty := 0;
                l_total_reserve_qty := 0;

                --
                -- get the total demand which is the difference between the
                -- ordered qty. and the shipped qty.
                -- This gives the total demand including the reserved
                -- and the unreserved material.
                --
                -- Bug 2820011. Modified the where clause to make use of source_document_id
                -- and source_document_line_id of oe_order_lines_all instead of
                -- orig_sys_document_ref and orig_sys_line_ref to identify requisitions
                -- and requisition lines uniquely.

                IF p_net_unrsv = 1
                THEN
                    SELECT SUM (
                                 NVL (ordered_quantity, 0)
                               - xxeis.eis_po_xxwc_isr_util_qa_pkg.get_shipped_qty (p_org_id, p_item_id, so.line_id))
                      INTO l_total_demand_qty
                      FROM oe_order_lines_all so,                 --                     po_requisition_headers_all poh,
                                                 po_requisition_lines_all pol
                     WHERE     so.source_document_id = pol.requisition_header_id
                           --                 and poh.requisition_header_id = pol.requisition_header_id
                           AND so.source_document_line_id = pol.requisition_line_id
                           AND (   pol.destination_organization_id <> p_org_id
                                OR (    pol.destination_organization_id = p_org_id
                                    AND                                                        -- Added code Bug#1012179
                                        (   pol.destination_type_code = 'EXPENSE'
                                         OR                                                        --Bug#3619239 started
                                            -- Bug 3619239 The functionality is added so that demand from Internal Sales Requisitions are taken
                                            -- into consideration if Destination Type is Inventory and Destination Subinventory is Non Quantity Tracked
                                            (    pol.destination_type_code = 'INVENTORY'
                                             AND pol.destination_subinventory IS NOT NULL
                                             AND EXISTS
                                                     (SELECT 1
                                                        FROM mtl_secondary_inventories
                                                       WHERE     secondary_inventory_name =
                                                                     pol.destination_subinventory
                                                             AND organization_id = pol.destination_organization_id
                                                             AND quantity_tracked = 2)))            -- Bug#3619239 ended
                                                                                        ))
                           AND so.ship_from_org_id = p_org_id
                           AND so.open_flag = 'Y'
                           AND so.visible_demand_flag = 'Y'
                           AND shipped_quantity IS NULL
                           AND so.inventory_item_id = p_item_id
                           --  and schedule_ship_date <= sysdate + 0.99999 /* bug no 6009682 */
                           AND DECODE (so.source_document_type_id
                                      ,10, 8
                                      ,DECODE (so.line_category_code, 'ORDER', 2, 12)) = 8
                           AND (   subinventory IS NULL
                                OR EXISTS
                                       (SELECT 1
                                          FROM mtl_secondary_inventories s
                                         WHERE     s.organization_id = p_org_id
                                               AND s.secondary_inventory_name = subinventory
                                               AND s.availability_type =
                                                       DECODE (p_include_nonnet, 1, s.availability_type, 1)));
                /*   IF G_TRACE_ON = 1 THEN
                   print_debug('Total demand (internal orders): ' || to_char(l_total_demand_qty)
                               , 'get_demand_qty'
                               , 9);
                   END IF;*/

                --DBMS_OUTPUT.put_line ('Take care of internal orders for org level planning' || l_total_demand_qty);
                END IF;

                --
                -- Find out the reserved qty for the material from mtl_reservations
                --
                IF ( (p_net_rsv = 1 AND p_net_unrsv = 2) OR (p_net_rsv = 2 AND p_net_unrsv = 1))
                THEN
                    --
                    -- Include the reserved demand from mtl_reservations
                    --
                    SELECT SUM (primary_reservation_quantity)
                      INTO l_total_reserve_qty
                      FROM mtl_reservations md, oe_order_lines_all so, --                      po_req_distributions_all pod,    Bug 5934651
                                                                      po_requisition_lines_all pol
                     WHERE     md.demand_source_line_id = so.line_id
                           --                  and to_number(so.ORIG_SYS_LINE_REF)     = pod.DISTRIBUTION_ID --Bug#2883172
                           AND so.source_document_id = pol.requisition_header_id                          -- Bug 5934651
                           AND so.source_document_line_id = pol.requisition_line_id
                           --                  and pod.REQUISITION_LINE_ID  = pol.REQUISITION_LINE_ID
                           AND (   pol.destination_organization_id <> p_org_id
                                OR (    pol.destination_organization_id = p_org_id
                                    AND                                                        -- Added code Bug#1012179
                                        (   pol.destination_type_code = 'EXPENSE'
                                         OR                                                       -- Bug#3619239 started
                                            -- Bug 3619239 The functionality is added so that demand from Internal Sales Requisitions are taken
                                            -- into consideration if Destination Type is Inventory and Destination Subinventory is Non Quantity Tracked
                                            (    pol.destination_type_code = 'INVENTORY'
                                             AND pol.destination_subinventory IS NOT NULL
                                             AND EXISTS
                                                     (SELECT 1
                                                        FROM mtl_secondary_inventories
                                                       WHERE     secondary_inventory_name =
                                                                     pol.destination_subinventory
                                                             AND organization_id = pol.destination_organization_id
                                                             AND quantity_tracked = 2)))            -- Bug#3619239 ended
                                                                                        ))
                           AND organization_id = p_org_id
                           AND md.inventory_item_id = p_item_id
                           --and REQUIREMENT_DATE <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                           AND demand_source_type_id = 8
                           AND (   subinventory_code IS NULL
                                OR EXISTS
                                       (SELECT 1
                                          FROM mtl_secondary_inventories s
                                         WHERE     s.organization_id = p_org_id
                                               AND s.secondary_inventory_name = subinventory_code
                                               AND s.availability_type =
                                                       DECODE (p_include_nonnet, 1, s.availability_type, 1)))
                           /* nsinghi MIN-MAX INVCONV start */
                           AND (   md.locator_id IS NULL
                                OR p_level = 2
                                OR EXISTS
                                       (SELECT 1
                                          FROM mtl_item_locations mil
                                         WHERE     mil.organization_id = p_org_id
                                               AND mil.inventory_location_id = md.locator_id
                                               AND mil.subinventory_code =
                                                       NVL (md.subinventory_code, mil.subinventory_code)
                                               AND mil.availability_type =
                                                       DECODE (p_include_nonnet, 1, mil.availability_type, 1)))
                           AND (   md.lot_number IS NULL
                                OR p_level = 2
                                OR EXISTS
                                       (SELECT 1
                                          FROM mtl_lot_numbers mln
                                         WHERE     mln.organization_id = p_org_id
                                               AND mln.lot_number = md.lot_number
                                               AND mln.inventory_item_id = p_item_id
                                               AND mln.availability_type =
                                                       DECODE (p_include_nonnet, 1, mln.availability_type, 1)));
                /* nsinghi MIN-MAX INVCONV end */

                /*  IF G_TRACE_ON = 1 THEN
                  print_debug('Reserved demand (internal orders): ' || to_char(l_total_reserve_qty)
                              , 'get_demand_qty'
                              , 9);
                  END IF;*/

                --DBMS_OUTPUT.put_line ('Include the reserved demand from mtl_reservations' || l_total_reserve_qty);
                END IF;

                --
                -- total demand is calculated as follows:
                -- if we have to consider both unreserved matl and reserved matl. then the
                --    demand is simply the total demand = ordered qty - shipped qty.
                -- elsif we have to take into account only reserved matl. then the
                --    demand is simply the reservations from mtl_reservations for the matl.
                -- elsif we have to take into account just the unreserved matl. then the
                --    demand is total demand - the reservations for the material.
                --
                IF p_net_unrsv = 1 AND p_net_rsv = 1
                THEN
                    l_demand_qty := NVL (l_total_demand_qty, 0);
                ELSIF p_net_rsv = 1
                THEN
                    l_demand_qty := NVL (l_total_reserve_qty, 0);
                ELSIF p_net_unrsv = 1
                THEN
                    l_demand_qty := NVL (l_total_demand_qty, 0) - NVL (l_total_reserve_qty, 0);
                END IF;

                /*  IF G_TRACE_ON = 1 THEN
                  print_debug('Demand from internal orders: ' || to_char(l_demand_qty)
                              , 'get_demand_qty'
                              , 9);
                  END IF;*/
                total := total + NVL (l_demand_qty, 0);
            --DBMS_OUTPUT.put_line ('  total := total + NVL(l_demand_qty,0)  is ' || total);
            END IF;                                                                                    -- end if level=1

            --
            /* Bug 3364512. Demand is double for back-to-back sales orders after
           auto-create requisition and for sales orders with ATO items after
           auto-create WIP job. Commenting the below code which fetches duplicate
           demand */

            -- WIP Reservations from mtl_demand
            --
            /*   IF p_level = 1 THEN
                   --
                   -- SUBINVENTORY IS Always expected to be Null when Reservation_type is 3.
                   --
                   select sum(PRIMARY_UOM_QUANTITY - GREATEST(NVL(RESERVATION_QUANTITY,0),
                          NVL(COMPLETED_QUANTITY,0)))
                     into qty
                     from mtl_demand
                    where RESERVATION_TYPE = 3
                      and ORGANIZATION_ID = p_org_id
                      and PRIMARY_UOM_QUANTITY >
                           GREATEST(NVL(RESERVATION_QUANTITY,0), NVL(COMPLETED_QUANTITY,0))
                      and INVENTORY_ITEM_ID = p_item_id
                      and REQUIREMENT_DATE <= p_d_cutoff
                      and p_net_rsv = 1;

                   IF G_TRACE_ON = 1 THEN
                   print_debug('WIP Reservations from mtl_demand: ' || to_char(qty)
                               , 'get_demand_qty'
                               , 9);
                   END IF;
                   total := total + NVL(qty,0);
               END IF; */

            --
            -- Wip Components are to be included at the Org Level Planning only.
            -- Qty Issued Substracted from the Qty Required
            --
            IF (p_net_wip = 1 AND p_level = 1)
            THEN
                /* nsinghi MIN-MAX INVCONV start */

                IF p_process_org = 'Y'
                THEN
                    /*      Here we need include the query to include OPM as source of demand.
                    Since GME will always give the complete demand (including reserved demand)
                    so subtracting the reserved demand as reserved demand will be considered
                    above from mtl_reservations query. */

                    SELECT SUM (
                                 NVL (NVL (d.wip_plan_qty, d.plan_qty) - d.actual_qty, 0)
                               - NVL (mtr.primary_reservation_quantity, 0))
                      INTO qty
                      FROM gme_material_details d, gme_batch_header h, mtl_reservations mtr
                     WHERE     h.batch_type IN (0, 10)
                           AND h.batch_status IN (1, 2)
                           AND h.batch_id = d.batch_id
                           AND d.line_type = -1
                           --              AND    NVL(d.original_qty, 0) <> 0       --commented as part of bug 8434499
                           AND d.organization_id = p_org_id
                           AND d.inventory_item_id = p_item_id
                           AND d.batch_id = mtr.demand_source_header_id(+)
                           AND d.material_detail_id = mtr.demand_source_line_id(+)
                           AND d.inventory_item_id = mtr.inventory_item_id(+)
                           AND d.organization_id = mtr.organization_id(+)
                           AND   NVL (NVL (d.wip_plan_qty, d.plan_qty) - d.actual_qty, 0)
                               - NVL (mtr.primary_reservation_quantity, 0) > 0
                           AND NVL (mtr.demand_source_type_id, 5) = 5
                           --AND    d.material_requirement_date <= p_d_cutoff
                           AND (   mtr.subinventory_code IS NULL
                                OR EXISTS
                                       (SELECT 1
                                          FROM mtl_secondary_inventories s
                                         WHERE     s.organization_id = p_org_id
                                               AND s.secondary_inventory_name = mtr.subinventory_code
                                               AND s.availability_type =
                                                       DECODE (p_include_nonnet, 1, s.availability_type, 1)))
                           AND (   mtr.locator_id IS NULL
                                OR EXISTS
                                       (SELECT 1
                                          FROM mtl_item_locations mil
                                         WHERE     mil.organization_id = p_org_id
                                               AND mil.inventory_location_id = mtr.locator_id
                                               AND mil.subinventory_code =
                                                       NVL (mtr.subinventory_code, mil.subinventory_code)
                                               AND mil.availability_type =
                                                       DECODE (p_include_nonnet, 1, mil.availability_type, 1)))
                           AND (   mtr.lot_number IS NULL
                                OR EXISTS
                                       (SELECT 1
                                          FROM mtl_lot_numbers mln
                                         WHERE     mln.organization_id = p_org_id
                                               AND mln.lot_number = mtr.lot_number
                                               AND mln.inventory_item_id = p_item_id
                                               AND mln.availability_type =
                                                       DECODE (p_include_nonnet, 1, mln.availability_type, 1)));

                    /* IF G_TRACE_ON = 1 THEN
                     print_debug('Batch Material requirements for OPM Batches : ' || to_char(qty)
                                 , 'get_demand_qty'
                                 , 9);
                     END IF;*/
                    total := total + NVL (qty, 0);

                    --DBMS_OUTPUT.put_line (' p_process_org' || total);

                    SELECT SUM (o.required_quantity - o.quantity_issued)
                      INTO qty
                      FROM wip_discrete_jobs d, wip_requirement_operations o
                     WHERE     o.wip_entity_id = d.wip_entity_id
                           AND o.organization_id = d.organization_id
                           AND d.organization_id = p_org_id
                           AND o.inventory_item_id = p_item_id
                           --and o.date_required    <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                           AND o.required_quantity > 0
                           AND o.required_quantity > o.quantity_issued
                           AND o.operation_seq_num > 0
                           AND d.status_type IN (1, 3, 4, 6)                    -- Excluded 5 from selection Bug#1016495
                           AND o.wip_supply_type NOT IN (5, 6);             -- Included 5 from the selection Bug#4488415

                    /*IF G_TRACE_ON = 1 THEN
                    print_debug('WIP component requirements for discrete jobs: ' || to_char(qty)
                                , 'get_demand_qty'
                                , 9);
                    END IF;*/
                    total := total + NVL (qty, 0);

                    --DBMS_OUTPUT.put_line (' p_process_org' || total);

                    --
                    -- Demand Qty to be added for a released repetitive schedule
                    -- Bug#691471
                    --
                    /*4518296*/
                    SELECT SUM (o.required_quantity - o.quantity_issued)
                      INTO qty
                      FROM wip_repetitive_schedules r, wip_requirement_operations o
                     WHERE     o.wip_entity_id = r.wip_entity_id
                           AND o.repetitive_schedule_id = r.repetitive_schedule_id
                           AND o.organization_id = r.organization_id
                           AND r.organization_id = p_org_id
                           AND o.inventory_item_id = p_item_id
                           --and o.date_required          <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                           AND o.required_quantity > 0
                           AND o.required_quantity > o.quantity_issued
                           AND o.operation_seq_num > 0
                           AND r.status_type IN (1, 3, 4, 6)                    -- Excluded 5 from selection Bug#1016495
                           AND o.wip_supply_type NOT IN (5, 6);             -- Included 5 from the selection Bug#4488415

                    /*   IF G_TRACE_ON = 1 THEN
                       print_debug('WIP component requirements for repetitive schedules: ' || to_char(qty)
                                   , 'get_demand_qty'
                                   , 9);
                       END IF;*/
                    total := total + NVL (qty, 0);
                --DBMS_OUTPUT.put_line (' p_process_org 3 ' || total);
                ELSE
                    /* nsinghi MIN-MAX INVCONV end */
                    /*4518296*/

                    SELECT SUM (o.required_quantity - o.quantity_issued)
                      INTO qty
                      FROM wip_discrete_jobs d, wip_requirement_operations o
                     WHERE     o.wip_entity_id = d.wip_entity_id
                           AND o.organization_id = d.organization_id
                           AND d.organization_id = p_org_id
                           AND o.inventory_item_id = p_item_id
                           --and o.date_required    <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                           AND o.required_quantity > 0
                           AND o.required_quantity > o.quantity_issued
                           AND o.operation_seq_num > 0
                           AND d.status_type IN (1, 3, 4, 6)                    -- Excluded 5 from selection Bug#1016495
                           AND o.wip_supply_type NOT IN (5, 6);             -- Included 5 from the selection Bug#4488415

                    /*IF G_TRACE_ON = 1 THEN
                    print_debug('WIP component requirements for discrete jobs: ' || to_char(qty)
                                , 'get_demand_qty'
                                , 9);
                    END IF;*/
                    total := total + NVL (qty, 0);

                    --DBMS_OUTPUT.put_line (' p_process_org' || total);

                    --
                    -- Demand Qty to be added for a released repetitive schedule
                    -- Bug#691471
                    --
                    /*4518296*/
                    SELECT SUM (o.required_quantity - o.quantity_issued)
                      INTO qty
                      FROM wip_repetitive_schedules r, wip_requirement_operations o
                     WHERE     o.wip_entity_id = r.wip_entity_id
                           AND o.repetitive_schedule_id = r.repetitive_schedule_id
                           AND o.organization_id = r.organization_id
                           AND r.organization_id = p_org_id
                           AND o.inventory_item_id = p_item_id
                           --and o.date_required          <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                           AND o.required_quantity > 0
                           AND o.required_quantity > o.quantity_issued
                           AND o.operation_seq_num > 0
                           AND r.status_type IN (1, 3, 4, 6)                    -- Excluded 5 from selection Bug#1016495
                           AND o.wip_supply_type NOT IN (5, 6);             -- Included 5 from the selection Bug#4488415

                    /*   IF G_TRACE_ON = 1 THEN
                       print_debug('WIP component requirements for repetitive schedules: ' || to_char(qty)
                                   , 'get_demand_qty'
                                   , 9);
                       END IF;*/
                    total := total + NVL (qty, 0);
                --DBMS_OUTPUT.put_line (' p_process_org 3 ' || total);
                END IF;                                                                        /* p_process_org = 'Y' */
            END IF;

            --
            -- Include move orders:
            -- Leave out the closed or cancelled lines
            -- Select only Issue from Stores for org level planning
            -- Also select those lines for sub level planning.
            --
            -- Exclude move orders created for WIP Issue transaction
            -- (txn type = 35, INV_Globals.G_TYPE_XFER_ORDER_WIP_ISSUE)
            -- since these are already taken into account (above) by
            -- directly querying the WIP tables for open component requirements
            --

            -- kkoothan Part of Bug Fix: 2875583
            -- Converting the quantities to the primary uom as the quantity
            -- and quantity delivered in mtl_txn_request_lines
            -- are in transaction uom.

            --Bug 3057273, Move order demand should be excluded if net unreserved demand is No.
            IF p_net_unrsv = 1
            THEN
                /*SELECT SUM(MTRL.QUANTITY - NVL(MTRL.QUANTITY_DELIVERED,0))
                  INTO qty
                  FROM MTL_TXN_REQUEST_LINES MTRL,
                       MTL_TRANSACTION_TYPES MTT
                 WHERE MTT.TRANSACTION_TYPE_ID = MTRL.TRANSACTION_TYPE_ID
                   AND MTRL.TRANSACTION_TYPE_ID <> INV_Globals.G_TYPE_XFER_ORDER_WIP_ISSUE
                   AND MTRL.ORGANIZATION_ID = p_org_id
                   AND MTRL.INVENTORY_ITEM_ID = p_item_id
                   AND MTRL.LINE_STATUS NOT IN (5,6)
                   AND MTT.TRANSACTION_ACTION_ID = 1
                   AND (p_level = 1  OR
                        MTRL.FROM_SUBINVENTORY_CODE = p_subinv)
                   AND (MTRL.FROM_SUBINVENTORY_CODE IS NULL OR
                        p_level = 2  OR
                        EXISTS (SELECT 1
                                  FROM MTL_SECONDARY_INVENTORIES S
                                 WHERE S.ORGANIZATION_ID = p_org_id
                                   AND S.SECONDARY_INVENTORY_NAME = MTRL.FROM_SUBINVENTORY_CODE
                                   AND S.AVAILABILITY_TYPE = DECODE(p_include_nonnet,
                                                                    1,S.AVAILABILITY_TYPE,1)))
                   AND MTRL.DATE_REQUIRED <= p_d_cutoff;*/

                SELECT NVL (SUM (apps.inv_decimals_pub.get_primary_quantity (
                                     p_org_id
                                    ,p_item_id
                                    ,mtrl.uom_code
                                    ,mtrl.quantity - NVL (mtrl.quantity_delivered, 0)))
                           ,0)
                  INTO qty
                  FROM mtl_txn_request_lines mtrl, mtl_transaction_types mtt
                 WHERE     mtt.transaction_type_id = mtrl.transaction_type_id
                       AND mtrl.transaction_type_id <> apps.inv_globals.g_type_xfer_order_wip_issue
                       AND mtrl.organization_id = p_org_id
                       AND mtrl.inventory_item_id = p_item_id
                       AND mtrl.line_status IN (3, 7)           --Changed for Bug 5330189: 3 = Approved 7 = Pre-Approved
                       AND mtt.transaction_action_id = 1
                       AND (p_level = 1 OR mtrl.from_subinventory_code = p_subinv)
                       AND (   mtrl.from_subinventory_code IS NULL
                            OR p_level = 2
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = p_org_id
                                           AND s.secondary_inventory_name = mtrl.from_subinventory_code
                                           AND s.availability_type =
                                                   DECODE (p_include_nonnet, 1, s.availability_type, 1)))
                       --AND mtrl.date_required <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                       /* nsinghi MIN-MAX INVCONV start */
                       AND (   mtrl.from_locator_id IS NULL
                            OR p_level = 2
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_item_locations mil
                                     WHERE     mil.organization_id = p_org_id
                                           AND mil.inventory_location_id = mtrl.from_locator_id
                                           AND mil.subinventory_code =
                                                   NVL (mtrl.from_subinventory_code, mil.subinventory_code)
                                           AND mil.availability_type =
                                                   DECODE (p_include_nonnet, 1, mil.availability_type, 1)))
                       AND (   mtrl.lot_number IS NULL
                            OR p_level = 2
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_lot_numbers mln
                                     WHERE     mln.organization_id = p_org_id
                                           AND mln.lot_number = mtrl.lot_number
                                           AND mln.inventory_item_id = p_item_id
                                           AND mln.availability_type =
                                                   DECODE (p_include_nonnet, 1, mln.availability_type, 1)));

                /* nsinghi MIN-MAX INVCONV end */

                /*  IF G_TRACE_ON = 1 THEN
                  PRINT_DEBUG('Demand from open move orders: ' || TO_CHAR(QTY), 'get_demand_qty', 9);
                  END IF;*/

                total := total + NVL (qty, 0);
            --DBMS_OUTPUT.put_line (' p_process_org4' || total);
            END IF;

            --
            -- Include the sub transfer and the staging transfer move orders
            -- for sub level planning
            -- Bug 3057273, Move order demand should be excluded if net unreserved demand is No.

            IF (p_level = 2 AND p_net_unrsv = 1)
            THEN
                -- kkoothan Part of Bug Fix: 2875583
                -- Converting the quantities to the primary uom as the quantity
                -- and quantity delivered in mtl_txn_request_lines
                -- are in transaction uom.

                /*SELECT NVL(sum(mtrl.quantity - NVL(mtrl.quantity_delivered,0)),0)
                  INTO qty
                  FROM mtl_transaction_types  mtt,
                       mtl_txn_request_lines  mtrl
                 WHERE mtt.transaction_action_id IN (2,28)
                   AND mtt.transaction_type_id     = mtrl.transaction_type_id
                   AND mtrl.organization_id        = p_org_id
                   AND mtrl.inventory_item_id      = p_item_id
                   AND mtrl.from_subinventory_code = p_subinv
                   AND mtrl.line_status NOT IN (5,6)
                   AND mtrl.date_required         <= p_d_cutoff;*/

                SELECT NVL (SUM (mtrl.quantity - NVL (mtrl.quantity_delivered, 0)), 0)
                  INTO qty
                  FROM mtl_transaction_types mtt, mtl_txn_request_lines mtrl
                 WHERE     mtt.transaction_action_id IN (2, 28)
                       AND mtt.transaction_type_id = mtrl.transaction_type_id
                       AND mtrl.organization_id = p_org_id
                       AND mtrl.inventory_item_id = p_item_id
                       AND mtrl.from_subinventory_code = p_subinv
                       AND mtrl.line_status IN (3, 7);          --Changed for Bug 5330189: 3 = Approved 7 = Pre-Approved

                --AND mtrl.date_required <= p_d_cutoff + 0.99999; /* bug no 6009682 */

                /* IF G_TRACE_ON = 1 THEN
                 print_debug('Qty pending out due to sub transfers and the staging transfer move orders: '
                              || to_char(qty)
                             , 'get_demand_qty'
                             , 9);
                 END IF;*/
                total := total + NVL (qty, 0);
            --DBMS_OUTPUT.put_line (' p_process_org 6' || total);
            END IF;

            -- Bug 5041763 need to exclude drop ship reservation from on-hand qty to get correct availability
            SELECT SUM (primary_reservation_quantity)
              INTO qty
              FROM mtl_reservations
             WHERE     organization_id = p_org_id
                   AND inventory_item_id = p_item_id
                   AND demand_source_type_id = 2
                   AND supply_source_type_id = 13
                   --and    REQUIREMENT_DATE <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                   AND ( (p_level = 1) OR subinventory_code = p_subinv)
                   AND (   subinventory_code IS NULL
                        OR p_level = 2
                        OR EXISTS
                               (SELECT 1
                                  FROM mtl_secondary_inventories s
                                 WHERE     s.organization_id = p_org_id
                                       AND s.secondary_inventory_name = subinventory_code
                                       AND s.availability_type = DECODE (p_include_nonnet, 1, s.availability_type, 1)))
                   AND EXISTS
                           (SELECT 1
                              FROM oe_drop_ship_sources odss
                             WHERE odss.line_id = demand_source_line_id);

            total := total + NVL (qty, 0);
            --DBMS_OUTPUT.put_line (' p_process_org 7' || total);
            --Bug 6240025 BEGIN
            l_loaded_qty :=
                xxeis.eis_po_xxwc_isr_util_qa_pkg.get_loaded_qty (p_org_id
                                                                 ,p_subinv
                                                                 ,p_level
                                                                 ,p_item_id
                                                                 ,p_net_rsv
                                                                 ,p_net_unrsv);
            total := total + NVL (l_loaded_qty, 0);
            --Bug 6240025 END
            RETURN (total);
        --DBMS_OUTPUT.put_line (' return toatl is ' || total);
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN (NVL (total, 0));
    --DBMS_OUTPUT.put_line ('The error is' || SQLCODE || SQLERRM);
    END;

    PROCEDURE get_planning_quantity (p_include_nonnet   IN            NUMBER
                                    ,p_level            IN            NUMBER
                                    ,p_org_id           IN            NUMBER
                                    ,p_subinv           IN            VARCHAR2
                                    ,p_item_id          IN            NUMBER
                                    ,p_grade_code       IN            VARCHAR2                         -- invConv change
                                    ,x_qoh                 OUT NOCOPY NUMBER                           -- invConv change
                                    ,x_sqoh                OUT NOCOPY NUMBER                           -- invConv change
                                                                            )
    IS
        x_return_status       VARCHAR2 (30);
        l_qoh                 NUMBER := 0;
        l_moq_qty             NUMBER := 0;
        l_mmtt_qty_src        NUMBER := 0;
        l_mmtt_qty_dest       NUMBER := 0;
        l_sqoh                NUMBER := 0;                                                             -- invConv change
        l_moq_sqty            NUMBER := 0;                                                             -- invConv change
        l_mmtt_sqty_src       NUMBER := 0;                                                             -- invConv change
        l_mmtt_sqty_dest      NUMBER := 0;                                                             -- invConv change
        l_lot_control         NUMBER := 1;
        l_debug               NUMBER := NVL (fnd_profile.VALUE ('INV_DEBUG_TRACE'), 0);
        l_lpn_qty             NUMBER := 0;                                                                -- Bug 4209192
        l_default_status_id   NUMBER := -1;                                                         -- Added for 6633612

        -- invConv changes begin
        l_uom_ind             VARCHAR2 (4);

        CURSOR get_item_info (l_org_id IN NUMBER, l_item_id IN NUMBER)
        IS
            SELECT tracking_quantity_ind, lot_control_code
              FROM mtl_system_items_b
             WHERE inventory_item_id = l_item_id AND organization_id = l_org_id;
    -- invConv changes end

    BEGIN
        -- invConv changes begin
        -- Only run this function when DUOM item.
        OPEN get_item_info (p_org_id, p_item_id);

        FETCH get_item_info
        INTO l_uom_ind, l_lot_control;

        CLOSE get_item_info;

        -- invConv change : this is included in the above cursor.
        -- SELECT lot_control_code
        -- into l_lot_control
        -- from  mtl_system_items_b
        -- where inventory_item_id = p_item_id
        -- and   organization_id = p_org_id;

        l_default_status_id := -1;

        IF (p_level = 1)
        THEN
            -- Organization Level

            /* nsinghi MIN-MAX INVCONV start */

            IF p_include_nonnet = 1
            THEN
                -- invConv change : replaced primary by secondary qty field.

                SELECT SUM (moq.primary_transaction_quantity), SUM (NVL (moq.secondary_transaction_quantity, 0))
                  INTO l_moq_qty, l_moq_sqty
                  FROM mtl_onhand_quantities_detail moq, mtl_lot_numbers mln, mtl_secondary_inventories msi
                 WHERE     moq.organization_id = p_org_id
                       AND moq.inventory_item_id = p_item_id
                       AND msi.organization_id = moq.organization_id
                       AND msi.secondary_inventory_name = moq.subinventory_code
                       AND moq.organization_id = NVL (moq.planning_organization_id, moq.organization_id)
                       AND moq.lot_number = mln.lot_number(+)
                       AND moq.organization_id = mln.organization_id(+)
                       AND moq.inventory_item_id = mln.inventory_item_id(+)
                       AND TRUNC (NVL (mln.expiration_date, SYSDATE + 1)) > TRUNC (SYSDATE)
                       AND NVL (moq.planning_tp_type, 2) = 2;
            ELSE                                                                                  /* include nettable */
                SELECT SUM (mon.primary_transaction_quantity), SUM (NVL (mon.secondary_transaction_quantity, 0))
                  INTO l_moq_qty, l_moq_sqty
                  FROM apps.mtl_onhand_net mon, mtl_lot_numbers mln
                 WHERE     mon.organization_id = p_org_id
                       AND mon.inventory_item_id = p_item_id
                       AND mon.organization_id = NVL (mon.planning_organization_id, mon.organization_id)
                       AND mon.lot_number = mln.lot_number(+)
                       AND mon.organization_id = mln.organization_id(+)
                       AND mon.inventory_item_id = mln.inventory_item_id(+)
                       AND TRUNC (NVL (mln.expiration_date, SYSDATE + 1)) > TRUNC (SYSDATE)
                       AND NVL (mon.planning_tp_type, 2) = 2;
            END IF;

            /* nsinghi MIN-MAX INVCONV end */

            IF (l_lot_control = 2)
            THEN                                                                                 /* Lot - Full Control*/
                -- Added the below if for 6633612
                IF l_default_status_id = -1
                THEN
                    SELECT SUM (
                                 DECODE (mmtt.transaction_action_id
                                        ,1, -1
                                        ,2, -1
                                        ,28, -1
                                        ,3, -1
                                        ,SIGN (mmtt.primary_quantity))
                               * ABS (mmtt.primary_quantity))
                          ,SUM (
                                 DECODE (mmtt.transaction_action_id
                                        ,1, -1
                                        ,2, -1
                                        ,28, -1
                                        ,3, -1
                                        ,SIGN (mmtt.secondary_transaction_quantity))
                               * ABS (NVL (mmtt.secondary_transaction_quantity, 0)))
                      INTO l_mmtt_qty_src, l_mmtt_sqty_src
                      FROM apps.mtl_material_transactions_temp mmtt
                     WHERE     mmtt.organization_id = p_org_id
                           AND mmtt.inventory_item_id = p_item_id
                           AND mmtt.posting_flag = 'Y'
                           AND mmtt.subinventory_code IS NOT NULL
                           AND NVL (mmtt.transaction_status, 0) <> 2
                           AND mmtt.transaction_action_id NOT IN (24, 30)
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM mtl_secondary_inventories msi
                                     WHERE     msi.organization_id = mmtt.organization_id
                                           AND msi.secondary_inventory_name = mmtt.subinventory_code
                                           AND msi.availability_type =
                                                   DECODE (p_include_nonnet, 1, msi.availability_type, 1))
                           AND mmtt.planning_organization_id IS NULL
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM apps.mtl_transaction_lots_temp mtlt, mtl_lot_numbers mln
                                     WHERE     mtlt.transaction_temp_id = mmtt.transaction_temp_id
                                           AND mtlt.lot_number = mln.lot_number(+)
                                           AND p_org_id = mln.organization_id(+)
                                           AND p_item_id = mln.inventory_item_id(+)
                                           /* nsinghi MIN-MAX INVCONV start */
                                           AND NVL (mln.availability_type, 2) =
                                                   DECODE (p_include_nonnet, 1, NVL (mln.availability_type, 2), 1)
                                           AND TRUNC (
                                                   NVL (NVL (mtlt.lot_expiration_date, mln.expiration_date)
                                                       ,SYSDATE + 1)) > TRUNC (SYSDATE))
                           AND (   mmtt.locator_id IS NULL
                                OR (    mmtt.locator_id IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_item_locations mil
                                              WHERE     mmtt.organization_id = mil.organization_id
                                                    AND mmtt.locator_id = mil.inventory_location_id
                                                    AND mmtt.subinventory_code = mil.subinventory_code
                                                    AND mil.availability_type =
                                                            DECODE (p_include_nonnet, 1, mil.availability_type, 1))))
                           /* nsinghi MIN-MAX INVCONV end */

                           AND NVL (mmtt.planning_tp_type, 2) = 2;

                    SELECT SUM (ABS (mmtt.primary_quantity)), SUM (ABS (NVL (mmtt.secondary_transaction_quantity, 0)))
                      INTO l_mmtt_qty_dest, l_mmtt_sqty_dest
                      FROM apps.mtl_material_transactions_temp mmtt
                     WHERE     DECODE (mmtt.transaction_action_id, 3, mmtt.transfer_organization, mmtt.organization_id) =
                                   p_org_id
                           AND mmtt.inventory_item_id = p_item_id
                           AND mmtt.posting_flag = 'Y'
                           AND NVL (mmtt.transaction_status, 0) <> 2
                           AND mmtt.transaction_action_id IN (2, 28, 3)
                           AND (   (mmtt.transfer_subinventory IS NULL)
                                OR (    mmtt.transfer_subinventory IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_secondary_inventories msi
                                              WHERE     msi.organization_id =
                                                            DECODE (mmtt.transaction_action_id
                                                                   ,3, mmtt.transfer_organization
                                                                   ,mmtt.organization_id)
                                                    AND msi.secondary_inventory_name = mmtt.transfer_subinventory
                                                    AND msi.availability_type =
                                                            DECODE (p_include_nonnet, 1, msi.availability_type, 1))))
                           AND mmtt.planning_organization_id IS NULL
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM apps.mtl_transaction_lots_temp mtlt, mtl_lot_numbers mln
                                     WHERE     mtlt.transaction_temp_id = mmtt.transaction_temp_id
                                           AND mtlt.lot_number = mln.lot_number(+)
                                           AND DECODE (mmtt.transaction_action_id
                                                      ,3, mmtt.transfer_organization
                                                      ,mmtt.organization_id) = mln.organization_id(+)
                                           AND p_item_id = mln.inventory_item_id(+)
                                           /* nsinghi MIN-MAX INVCONV start */
                                           AND NVL (mln.availability_type, 2) =
                                                   DECODE (p_include_nonnet, 1, NVL (mln.availability_type, 2), 1)
                                           AND TRUNC (
                                                   NVL (NVL (mtlt.lot_expiration_date, mln.expiration_date)
                                                       ,SYSDATE + 1)) > TRUNC (SYSDATE))
                           AND (   mmtt.transfer_to_location IS NULL
                                OR (    mmtt.transfer_to_location IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_item_locations mil
                                              WHERE     DECODE (mmtt.transaction_action_id
                                                               ,3, mmtt.transfer_organization
                                                               ,mmtt.organization_id) = mil.organization_id
                                                    AND mmtt.transfer_to_location = mil.inventory_location_id
                                                    AND mmtt.transfer_subinventory = mil.subinventory_code
                                                    AND mil.availability_type =
                                                            DECODE (p_include_nonnet, 1, mil.availability_type, 1))))
                           /* nsinghi MIN-MAX INVCONV end */
                           AND NVL (mmtt.planning_tp_type, 2) = 2;
                ELSE                                                    /* default material status enabled of the org */
                    SELECT SUM (
                                 DECODE (mmtt.transaction_action_id
                                        ,1, -1
                                        ,2, -1
                                        ,28, -1
                                        ,3, -1
                                        ,SIGN (mtlt.primary_quantity))
                               * ABS (mtlt.primary_quantity))
                          ,SUM (
                                 DECODE (mmtt.transaction_action_id
                                        ,1, -1
                                        ,2, -1
                                        ,28, -1
                                        ,3, -1
                                        ,SIGN (mtlt.secondary_quantity))
                               * ABS (NVL (mtlt.secondary_quantity, 0)))
                      INTO l_mmtt_qty_src, l_mmtt_sqty_src
                      FROM apps.mtl_material_transactions_temp mmtt, apps.mtl_transaction_lots_temp mtlt
                     WHERE     mmtt.organization_id = p_org_id
                           AND mmtt.inventory_item_id = p_item_id
                           AND mmtt.posting_flag = 'Y'
                           AND mtlt.transaction_temp_id = mmtt.transaction_temp_id
                           AND mmtt.subinventory_code IS NOT NULL
                           AND mmtt.subinventory_code IS NOT NULL
                           AND NVL (mmtt.transaction_status, 0) <> 2
                           AND mmtt.transaction_action_id NOT IN (24, 30)
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM mtl_secondary_inventories msi
                                     WHERE     msi.organization_id = mmtt.organization_id
                                           AND msi.secondary_inventory_name = mmtt.subinventory_code)
                           AND mmtt.planning_organization_id IS NULL
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM apps.mtl_transaction_lots_temp mtlt, mtl_lot_numbers mln
                                     WHERE     mtlt.transaction_temp_id = mmtt.transaction_temp_id
                                           AND mtlt.lot_number = mln.lot_number(+)
                                           AND p_org_id = mln.organization_id(+)
                                           AND p_item_id = mln.inventory_item_id(+)
                                           AND TRUNC (
                                                   NVL (NVL (mtlt.lot_expiration_date, mln.expiration_date)
                                                       ,SYSDATE + 1)) > TRUNC (SYSDATE))
                           AND (   mmtt.locator_id IS NULL
                                OR (    mmtt.locator_id IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_item_locations mil
                                              WHERE     mmtt.organization_id = mil.organization_id
                                                    AND mmtt.locator_id = mil.inventory_location_id
                                                    AND mmtt.subinventory_code = mil.subinventory_code)))
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM apps.mtl_material_statuses mms
                                     WHERE     mms.status_id = NVL (apps.inv_material_status_grp.get_default_status (
                                                                        mmtt.organization_id
                                                                       ,mmtt.inventory_item_id
                                                                       ,mmtt.subinventory_code
                                                                       ,mmtt.locator_id
                                                                       ,mtlt.lot_number
                                                                       ,mmtt.lpn_id
                                                                       ,mmtt.transaction_action_id)
                                                                   ,mms.status_id)
                                           AND mms.availability_type = 1)
                           AND NVL (mmtt.planning_tp_type, 2) = 2;

                    SELECT SUM (ABS (mtlt.primary_quantity)), SUM (ABS (NVL (mtlt.secondary_quantity, 0)))
                      INTO l_mmtt_qty_dest, l_mmtt_sqty_dest
                      FROM apps.mtl_material_transactions_temp mmtt, apps.mtl_transaction_lots_temp mtlt
                     WHERE     DECODE (mmtt.transaction_action_id, 3, mmtt.transfer_organization, mmtt.organization_id) =
                                   p_org_id
                           AND mmtt.inventory_item_id = p_item_id
                           AND mmtt.posting_flag = 'Y'
                           AND NVL (mmtt.transaction_status, 0) <> 2
                           AND mmtt.transaction_action_id IN (2, 28, 3)
                           AND mtlt.transaction_temp_id = mmtt.transaction_temp_id
                           AND (   (mmtt.transfer_subinventory IS NULL)
                                OR (    mmtt.transfer_subinventory IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_secondary_inventories msi
                                              WHERE     msi.organization_id =
                                                            DECODE (mmtt.transaction_action_id
                                                                   ,3, mmtt.transfer_organization
                                                                   ,mmtt.organization_id)
                                                    AND msi.secondary_inventory_name = mmtt.transfer_subinventory)))
                           AND mmtt.planning_organization_id IS NULL
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM apps.mtl_transaction_lots_temp mtlt, mtl_lot_numbers mln
                                     WHERE     mtlt.transaction_temp_id = mmtt.transaction_temp_id
                                           AND mtlt.lot_number = mln.lot_number(+)
                                           AND DECODE (mmtt.transaction_action_id
                                                      ,3, mmtt.transfer_organization
                                                      ,mmtt.organization_id) = mln.organization_id(+)
                                           AND p_item_id = mln.inventory_item_id(+)
                                           /* nsinghi MIN-MAX INVCONV start */
                                           AND TRUNC (
                                                   NVL (NVL (mtlt.lot_expiration_date, mln.expiration_date)
                                                       ,SYSDATE + 1)) > TRUNC (SYSDATE))
                           AND (   mmtt.transfer_to_location IS NULL
                                OR (    mmtt.transfer_to_location IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_item_locations mil
                                              WHERE     DECODE (mmtt.transaction_action_id
                                                               ,3, mmtt.transfer_organization
                                                               ,mmtt.organization_id) = mil.organization_id
                                                    AND mmtt.transfer_to_location = mil.inventory_location_id
                                                    AND mmtt.transfer_subinventory = mil.subinventory_code)))
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM apps.mtl_material_statuses mms
                                     WHERE     mms.status_id = NVL (apps.inv_material_status_grp.get_default_status (
                                                                        DECODE (mmtt.transaction_action_id
                                                                               ,3, mmtt.transfer_organization
                                                                               ,mmtt.organization_id)
                                                                       ,mmtt.inventory_item_id
                                                                       ,mmtt.transfer_subinventory
                                                                       ,mmtt.transfer_to_location
                                                                       ,mtlt.lot_number
                                                                       ,mmtt.lpn_id
                                                                       ,mmtt.transaction_action_id
                                                                       ,apps.inv_material_status_grp.get_default_status (
                                                                            mmtt.organization_id
                                                                           ,mmtt.inventory_item_id
                                                                           ,mmtt.subinventory_code
                                                                           ,mmtt.locator_id
                                                                           ,mtlt.lot_number
                                                                           ,mmtt.lpn_id
                                                                           ,mmtt.transaction_action_id))
                                                                   ,mms.status_id)
                                           AND mms.availability_type = 1)
                           /* nsinghi MIN-MAX INVCONV end */
                           AND NVL (mmtt.planning_tp_type, 2) = 2;
                -- Rkatoori, For sub inventory transfer type, transfer_organization is null, so we have to do testing in that aspect..
                -- If there are any issues, need to add decode for that..
                END IF;                                                         /* End of IF l_default_status_id = -1 */
            ELSE                                                                                /* non lot controlled */
                -- Added the below if for 6633612
                IF l_default_status_id = -1
                THEN
                    SELECT SUM (
                                 DECODE (mmtt.transaction_action_id
                                        ,1, -1
                                        ,2, -1
                                        ,28, -1
                                        ,3, -1
                                        ,SIGN (mmtt.primary_quantity))
                               * ABS (mmtt.primary_quantity))
                          ,SUM (
                                 DECODE (mmtt.transaction_action_id
                                        ,1, -1
                                        ,2, -1
                                        ,28, -1
                                        ,3, -1
                                        ,SIGN (mmtt.secondary_transaction_quantity))
                               * ABS (NVL (mmtt.secondary_transaction_quantity, 0)))
                      INTO l_mmtt_qty_src, l_mmtt_sqty_src
                      FROM apps.mtl_material_transactions_temp mmtt
                     WHERE     mmtt.organization_id = p_org_id
                           AND mmtt.inventory_item_id = p_item_id
                           AND mmtt.posting_flag = 'Y'
                           AND mmtt.subinventory_code IS NOT NULL
                           AND NVL (mmtt.transaction_status, 0) <> 2
                           AND mmtt.transaction_action_id NOT IN (24, 30)
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM mtl_secondary_inventories msi
                                     WHERE     msi.organization_id = mmtt.organization_id
                                           AND msi.secondary_inventory_name = mmtt.subinventory_code
                                           AND msi.availability_type =
                                                   DECODE (p_include_nonnet, 1, msi.availability_type, 1))
                           AND mmtt.planning_organization_id IS NULL
                           /* nsinghi MIN-MAX INVCONV start */
                           AND (   mmtt.locator_id IS NULL
                                OR (    mmtt.locator_id IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_item_locations mil
                                              WHERE     mmtt.organization_id = mil.organization_id
                                                    AND mmtt.locator_id = mil.inventory_location_id
                                                    AND mmtt.subinventory_code = mil.subinventory_code
                                                    AND mil.availability_type =
                                                            DECODE (p_include_nonnet, 1, mil.availability_type, 1))))
                           /* nsinghi MIN-MAX INVCONV end */

                           AND NVL (mmtt.planning_tp_type, 2) = 2;

                    SELECT SUM (ABS (mmtt.primary_quantity)), SUM (ABS (NVL (mmtt.secondary_transaction_quantity, 0)))
                      INTO l_mmtt_qty_dest, l_mmtt_sqty_dest
                      FROM apps.mtl_material_transactions_temp mmtt
                     WHERE     DECODE (mmtt.transaction_action_id, 3, mmtt.transfer_organization, mmtt.organization_id) =
                                   p_org_id
                           AND mmtt.inventory_item_id = p_item_id
                           AND mmtt.posting_flag = 'Y'
                           AND NVL (mmtt.transaction_status, 0) <> 2
                           AND mmtt.transaction_action_id IN (2, 28, 3)
                           AND (   (mmtt.transfer_subinventory IS NULL)
                                OR (    mmtt.transfer_subinventory IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_secondary_inventories msi
                                              WHERE     msi.organization_id =
                                                            DECODE (mmtt.transaction_action_id
                                                                   ,3, mmtt.transfer_organization
                                                                   ,mmtt.organization_id)
                                                    AND msi.secondary_inventory_name = mmtt.transfer_subinventory
                                                    AND msi.availability_type =
                                                            DECODE (p_include_nonnet, 1, msi.availability_type, 1))))
                           AND mmtt.planning_organization_id IS NULL
                           /* nsinghi MIN-MAX INVCONV start */
                           AND (   mmtt.transfer_to_location IS NULL
                                OR (    mmtt.transfer_to_location IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_item_locations mil
                                              WHERE     DECODE (mmtt.transaction_action_id
                                                               ,3, mmtt.transfer_organization
                                                               ,mmtt.organization_id) = mil.organization_id
                                                    AND mmtt.transfer_to_location = mil.inventory_location_id
                                                    AND mmtt.transfer_subinventory = mil.subinventory_code
                                                    AND mil.availability_type =
                                                            DECODE (p_include_nonnet, 1, mil.availability_type, 1))))
                           /* nsinghi MIN-MAX INVCONV end */

                           AND NVL (mmtt.planning_tp_type, 2) = 2;
                ELSE                                                    /* default material status enabled of the org */
                    SELECT SUM (
                                 DECODE (mmtt.transaction_action_id
                                        ,1, -1
                                        ,2, -1
                                        ,28, -1
                                        ,3, -1
                                        ,SIGN (mmtt.primary_quantity))
                               * ABS (mmtt.primary_quantity))
                          ,SUM (
                                 DECODE (mmtt.transaction_action_id
                                        ,1, -1
                                        ,2, -1
                                        ,28, -1
                                        ,3, -1
                                        ,SIGN (mmtt.secondary_transaction_quantity))
                               * ABS (NVL (mmtt.secondary_transaction_quantity, 0)))
                      INTO l_mmtt_qty_src, l_mmtt_sqty_src
                      FROM apps.mtl_material_transactions_temp mmtt
                     WHERE     mmtt.organization_id = p_org_id
                           AND mmtt.inventory_item_id = p_item_id
                           AND mmtt.posting_flag = 'Y'
                           AND mmtt.subinventory_code IS NOT NULL
                           AND NVL (mmtt.transaction_status, 0) <> 2
                           AND mmtt.transaction_action_id NOT IN (24, 30)
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM mtl_secondary_inventories msi
                                     WHERE     msi.organization_id = mmtt.organization_id
                                           AND msi.secondary_inventory_name = mmtt.subinventory_code)
                           AND mmtt.planning_organization_id IS NULL
                           /* nsinghi MIN-MAX INVCONV start */
                           AND (   mmtt.locator_id IS NULL
                                OR (    mmtt.locator_id IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_item_locations mil
                                              WHERE     mmtt.organization_id = mil.organization_id
                                                    AND mmtt.locator_id = mil.inventory_location_id
                                                    AND mmtt.subinventory_code = mil.subinventory_code)))
                           /* nsinghi MIN-MAX INVCONV end */
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM apps.mtl_material_statuses mms
                                     WHERE     mms.status_id = NVL (apps.inv_material_status_grp.get_default_status (
                                                                        mmtt.organization_id
                                                                       ,mmtt.inventory_item_id
                                                                       ,mmtt.subinventory_code
                                                                       ,mmtt.locator_id
                                                                       ,mmtt.lot_number
                                                                       ,mmtt.lpn_id
                                                                       ,mmtt.transaction_action_id)
                                                                   ,mms.status_id)
                                           AND mms.availability_type = 1)
                           AND NVL (mmtt.planning_tp_type, 2) = 2;

                    SELECT SUM (ABS (mmtt.primary_quantity)), SUM (ABS (NVL (mmtt.secondary_transaction_quantity, 0)))
                      INTO l_mmtt_qty_dest, l_mmtt_sqty_dest
                      FROM apps.mtl_material_transactions_temp mmtt
                     WHERE     DECODE (mmtt.transaction_action_id, 3, mmtt.transfer_organization, mmtt.organization_id) =
                                   p_org_id
                           AND mmtt.inventory_item_id = p_item_id
                           AND mmtt.posting_flag = 'Y'
                           AND NVL (mmtt.transaction_status, 0) <> 2
                           AND mmtt.transaction_action_id IN (2, 28, 3)
                           AND (   (mmtt.transfer_subinventory IS NULL)
                                OR (    mmtt.transfer_subinventory IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_secondary_inventories msi
                                              WHERE     msi.organization_id =
                                                            DECODE (mmtt.transaction_action_id
                                                                   ,3, mmtt.transfer_organization
                                                                   ,mmtt.organization_id)
                                                    AND msi.secondary_inventory_name = mmtt.transfer_subinventory)))
                           AND mmtt.planning_organization_id IS NULL
                           /* nsinghi MIN-MAX INVCONV start */
                           AND (   mmtt.transfer_to_location IS NULL
                                OR (    mmtt.transfer_to_location IS NOT NULL
                                    AND EXISTS
                                            (SELECT 'x'
                                               FROM mtl_item_locations mil
                                              WHERE     DECODE (mmtt.transaction_action_id
                                                               ,3, mmtt.transfer_organization
                                                               ,mmtt.organization_id) = mil.organization_id
                                                    AND mmtt.transfer_to_location = mil.inventory_location_id
                                                    AND mmtt.transfer_subinventory = mil.subinventory_code)))
                           /* nsinghi MIN-MAX INVCONV end */
                           AND EXISTS
                                   (SELECT 'x'
                                      FROM apps.mtl_material_statuses mms
                                     WHERE     mms.status_id = NVL (apps.inv_material_status_grp.get_default_status (
                                                                        DECODE (mmtt.transaction_action_id
                                                                               ,3, mmtt.transfer_organization
                                                                               ,mmtt.organization_id)
                                                                       ,mmtt.inventory_item_id
                                                                       ,mmtt.transfer_subinventory
                                                                       ,mmtt.transfer_to_location
                                                                       ,mmtt.lot_number
                                                                       ,mmtt.lpn_id
                                                                       ,mmtt.transaction_action_id
                                                                       ,apps.inv_material_status_grp.get_default_status (
                                                                            mmtt.organization_id
                                                                           ,mmtt.inventory_item_id
                                                                           ,mmtt.subinventory_code
                                                                           ,mmtt.locator_id
                                                                           ,mmtt.lot_number
                                                                           ,mmtt.lpn_id
                                                                           ,mmtt.transaction_action_id))
                                                                   ,mms.status_id)
                                           AND mms.availability_type = 1)
                           AND NVL (mmtt.planning_tp_type, 2) = 2;
                END IF;                                                         /* End of IF l_default_status_id = -1 */
            END IF;

            -- Bug 4209192, adding below query to account for undelivered LPNs for WIP assembly completions.
            SELECT SUM (apps.inv_decimals_pub.get_primary_quantity (p_org_id
                                                                   ,p_item_id
                                                                   ,mtrl.uom_code
                                                                   ,mtrl.quantity - NVL (mtrl.quantity_delivered, 0)))
              INTO l_lpn_qty
              FROM mtl_txn_request_lines mtrl, mtl_txn_request_headers mtrh, mtl_transaction_types mtt
             WHERE     mtrl.organization_id = p_org_id
                   AND mtrl.inventory_item_id = p_item_id
                   AND mtrl.header_id = mtrh.header_id
                   AND mtrh.move_order_type = 6                                                    -- Putaway Move Order
                   AND mtrl.transaction_source_type_id = 5                                                        -- Wip
                   AND mtt.transaction_action_id = 31                                         -- WIP Assembly Completion
                   AND mtt.transaction_type_id = mtrl.transaction_type_id
                   AND mtrl.line_status = 7                                                              -- Pre Approved
                   AND mtrl.lpn_id IS NOT NULL;
        ELSIF (p_level = 2)
        THEN
            /* nsinghi MIN-MAX INVCONV start */

            /* If Min-Max Planning is run at sub-inventory level, value for include-nonnettable is always
            assumned to be 1. Thus no need to check for nettablity when run at sub-inv level. */

            /* nsinghi MIN-MAX INVCONV end */

            -- Subinventory level
            SELECT SUM (moq.primary_transaction_quantity), SUM (NVL (moq.secondary_transaction_quantity, 0))
              INTO l_moq_qty, l_moq_sqty
              FROM mtl_onhand_quantities_detail moq, mtl_lot_numbers mln
             WHERE     moq.organization_id = p_org_id
                   AND moq.inventory_item_id = p_item_id
                   AND moq.subinventory_code = p_subinv
                   AND moq.lot_number = mln.lot_number(+)
                   AND moq.organization_id = mln.organization_id(+)
                   AND moq.inventory_item_id = mln.inventory_item_id(+)
                   AND TRUNC (NVL (mln.expiration_date, SYSDATE + 1)) > TRUNC (SYSDATE);

            IF (l_lot_control = 2)
            THEN
                SELECT SUM (
                             DECODE (mmtt.transaction_action_id
                                    ,1, -1
                                    ,2, -1
                                    ,28, -1
                                    ,3, -1
                                    ,SIGN (mmtt.primary_quantity))
                           * ABS (mmtt.primary_quantity))
                      ,SUM (
                             DECODE (mmtt.transaction_action_id
                                    ,1, -1
                                    ,2, -1
                                    ,28, -1
                                    ,3, -1
                                    ,SIGN (mmtt.secondary_transaction_quantity))
                           * ABS (NVL (mmtt.secondary_transaction_quantity, 0)))
                  INTO l_mmtt_qty_src, l_mmtt_sqty_src
                  FROM apps.mtl_material_transactions_temp mmtt
                 WHERE     mmtt.organization_id = p_org_id
                       AND mmtt.inventory_item_id = p_item_id
                       AND mmtt.subinventory_code = p_subinv
                       AND mmtt.posting_flag = 'Y'
                       AND mmtt.subinventory_code IS NOT NULL
                       AND NVL (mmtt.transaction_status, 0) <> 2
                       AND EXISTS
                               (SELECT 'x'
                                  FROM apps.mtl_transaction_lots_temp mtlt, apps.mtl_lot_numbers mln
                                 WHERE     mtlt.transaction_temp_id = mmtt.transaction_temp_id
                                       AND mtlt.lot_number = mln.lot_number(+)
                                       AND p_org_id = mln.organization_id(+)
                                       AND p_item_id = mln.inventory_item_id(+)
                                       AND TRUNC (
                                               NVL (NVL (mtlt.lot_expiration_date, mln.expiration_date), SYSDATE + 1)) >
                                               TRUNC (SYSDATE))
                       AND mmtt.transaction_action_id NOT IN (24, 30);

                SELECT SUM (ABS (mmtt.primary_quantity)), SUM (ABS (NVL (mmtt.secondary_transaction_quantity, 0)))
                  INTO l_mmtt_qty_dest, l_mmtt_sqty_dest
                  FROM apps.mtl_material_transactions_temp mmtt
                 WHERE     DECODE (mmtt.transaction_action_id, 3, mmtt.transfer_organization, mmtt.organization_id) =
                               p_org_id
                       AND mmtt.inventory_item_id = p_item_id
                       AND mmtt.transfer_subinventory = p_subinv
                       AND mmtt.posting_flag = 'Y'
                       AND NVL (mmtt.transaction_status, 0) <> 2
                       AND EXISTS
                               (SELECT 'x'
                                  FROM apps.mtl_transaction_lots_temp mtlt, mtl_lot_numbers mln
                                 WHERE     mtlt.transaction_temp_id = mmtt.transaction_temp_id
                                       AND mtlt.lot_number = mln.lot_number(+)
                                       AND DECODE (mmtt.transaction_action_id
                                                  ,3, mmtt.transfer_organization
                                                  ,mmtt.organization_id) = mln.organization_id(+)
                                       AND p_item_id = mln.inventory_item_id(+)
                                       AND TRUNC (
                                               NVL (NVL (mtlt.lot_expiration_date, mln.expiration_date), SYSDATE + 1)) >
                                               TRUNC (SYSDATE))
                       AND mmtt.transaction_action_id IN (2, 28, 3);
            ELSE
                SELECT SUM (
                             DECODE (mmtt.transaction_action_id
                                    ,1, -1
                                    ,2, -1
                                    ,28, -1
                                    ,3, -1
                                    ,SIGN (mmtt.primary_quantity))
                           * ABS (mmtt.primary_quantity))
                      ,SUM (
                             DECODE (mmtt.transaction_action_id
                                    ,1, -1
                                    ,2, -1
                                    ,28, -1
                                    ,3, -1
                                    ,SIGN (mmtt.secondary_transaction_quantity))
                           * ABS (NVL (mmtt.secondary_transaction_quantity, 0)))
                  INTO l_mmtt_qty_src, l_mmtt_sqty_src
                  FROM apps.mtl_material_transactions_temp mmtt
                 WHERE     mmtt.organization_id = p_org_id
                       AND mmtt.inventory_item_id = p_item_id
                       AND mmtt.subinventory_code = p_subinv
                       AND mmtt.posting_flag = 'Y'
                       AND mmtt.subinventory_code IS NOT NULL
                       AND NVL (mmtt.transaction_status, 0) <> 2
                       AND mmtt.transaction_action_id NOT IN (24, 30);

                SELECT SUM (ABS (mmtt.primary_quantity)), SUM (ABS (NVL (mmtt.secondary_transaction_quantity, 0)))
                  INTO l_mmtt_qty_dest, l_mmtt_sqty_dest
                  FROM apps.mtl_material_transactions_temp mmtt
                 WHERE     DECODE (mmtt.transaction_action_id, 3, mmtt.transfer_organization, mmtt.organization_id) =
                               p_org_id
                       AND mmtt.inventory_item_id = p_item_id
                       AND mmtt.transfer_subinventory = p_subinv
                       AND mmtt.posting_flag = 'Y'
                       AND NVL (mmtt.transaction_status, 0) <> 2
                       AND mmtt.transaction_action_id IN (2, 28, 3);
            END IF;
        END IF;

        -- Bug 4209192, adding undelivered LPN l_lpn_qty for WIP assembly completions in total onhand.
        l_qoh := NVL (l_moq_qty, 0) + NVL (l_mmtt_qty_src, 0) + NVL (l_mmtt_qty_dest, 0) + NVL (l_lpn_qty, 0);

        -- invConv change
        l_sqoh := NVL (l_moq_sqty, 0) + NVL (l_mmtt_sqty_src, 0) + NVL (l_mmtt_sqty_dest, 0);

        x_qoh := l_qoh;

        -- invConv changes begin
        IF (l_uom_ind = 'P')
        THEN
            -- This is not a DUOM item.

            x_sqoh := NULL;
        ELSE
            x_sqoh := l_sqoh;
        END IF;
    -- invConv changes end

    EXCEPTION
        WHEN OTHERS
        THEN
            x_qoh := NULL;
            x_sqoh := NULL;
    END get_planning_quantity;

    FUNCTION get_planning_quantity (p_include_nonnet    NUMBER
                                   ,p_level             NUMBER
                                   ,p_org_id            NUMBER
                                   ,p_subinv            VARCHAR2
                                   ,p_item_id           NUMBER)
        RETURN NUMBER
    IS
        l_qoh     NUMBER := 0;
        l_sqoh    NUMBER := NULL;
        l_debug   NUMBER := NVL (fnd_profile.VALUE ('INV_DEBUG_TRACE'), 0);
    BEGIN
        -- invConv changes begin :
        -- Calling the new GET_PLANNING_QUANTITY procedure
        get_planning_quantity (p_include_nonnet   => p_include_nonnet
                              ,p_level            => p_level
                              ,p_org_id           => p_org_id
                              ,p_subinv           => p_subinv
                              ,p_item_id          => p_item_id
                              ,p_grade_code       => NULL                                              -- invConv change
                              ,x_qoh              => l_qoh                                             -- invConv change
                              ,x_sqoh             => l_sqoh);                                          -- invConv change
        -- invConv changes end.

        RETURN (l_qoh);
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN (0);
    END get_planning_quantity;

    FUNCTION get_org_rec_qty (p_inventory_item_id IN NUMBER, p_organization_id IN NUMBER)
        RETURN NUMBER
    IS
        l_open_qty   NUMBER;
    BEGIN
        SELECT SUM (qty)
          INTO l_open_qty
          FROM (SELECT NVL (SUM (to_org_primary_quantity), 0) qty
                  FROM apps.mtl_supply sup
                 WHERE     sup.supply_type_code IN ('SHIPMENT')                   -- Need to check the supply_type_codde
                       AND sup.destination_type_code = 'INVENTORY'
                       AND sup.from_organization_id = p_organization_id
                       AND sup.intransit_owning_org_id IS NOT NULL
                       AND sup.item_id = p_inventory_item_id);

        RETURN (l_open_qty);
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN (0);
    END get_org_rec_qty;
END;
/

-- Grants for Package Body
GRANT EXECUTE ON xxeis.eis_po_xxwc_isr_util_pkg TO apps
/
GRANT DEBUG ON xxeis.eis_po_xxwc_isr_util_pkg TO apps
/

-- End of DDL Script for Package Body XXEIS.EIS_PO_XXWC_ISR_UTIL_PKG
