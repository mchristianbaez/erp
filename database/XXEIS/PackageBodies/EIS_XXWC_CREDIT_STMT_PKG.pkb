CREATE OR REPLACE package body XXEIS.EIS_XXWC_CREDIT_STMT_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  --TMS#20160418-00101  by Siva on 04-16-2016
--//
--// Object Usage 				:: This Object Referred by "White Cap EOM Credit Memo Statement Report"
--//
--// Object Name         		:: xxeis.EIS_XXWC_CREDIT_STMT_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report level and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     siva  	04/16/2016   Initial Build --TMS#20160418-00101  by Siva   	on 04-16-2016
--// 1.1     Siva 			15-Mar-2017     TMS#20170224-00065 
--//============================================================================

PROCEDURE POPULATE_CREDIT_INVOICES(P_PERIOD_YEAR IN NUMBER,
													P_period_name in varchar2,
													P_PROCESS_ID IN NUMBER,
                          p_location in varchar2,
                          p_Origin_Date_From in date,
                          p_Origin_Date_To in date
                          ) is
--//============================================================================
--//
--// Object Name         :: POPULATE_CREDIT_INVOICES  
--//
--// Object Usage 		 :: This Object Referred by "White Cap EOM Credit Memo Statement Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_AR_CREDIT_MEMO_DTLS_C View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     siva  	04/16/2016   Initial Build --TMS#20160418-00101  by Siva   	on 04-16-2016
--// 1.1     Siva 			15-Mar-2017     TMS#20170224-00065 
--//============================================================================
						  
l_stmt 				varchar2(32000);
L_PERIOD_COND 		VARCHAR2(32000);
l_stmt_hdr_qry		VARCHAR2(32000);
LC_WHERE_COND       VARCHAR2(32000);
LC_PERIOD_NAME   VARCHAR2(32000);
L_REF_CURSOR1       CURSOR_TYPE4;
L_REF_CURSOR2       CURSOR_TYPE4;

TYPE TRX_REC IS RECORD
(CUSTOMER_TRX_ID NUMBER);

type trx_rec_tab is table of trx_rec Index By Binary_Integer;
	trx_tab trx_rec_tab;

type l_credit_memo_state_type
is
  table of XXEIS.EIS_XXWC_CREDIT_INV_TAB%rowtype index by BINARY_INTEGER;
  l_credit_memo_state_REC_TBL l_credit_memo_state_type;


 Begin
 TRX_TAB.DELETE;
 l_credit_memo_state_REC_TBL.DELETE;
 
   IF P_PERIOD_NAME      IS NOT NULL THEN
    LC_PERIOD_NAME:= LC_WHERE_COND||'and p.period_name in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_period_name)||' )';
  END IF;
 
   IF p_location      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||'and loc.organization_code in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_location)||' )';
  END IF;
  
     IF p_Origin_Date_From      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||'and (SELECT MAX (apply_date)
             FROM ar.ar_receivable_applications_all
            WHERE     application_type = ''CM''
                  AND customer_trx_id = rctr.customer_trx_id) >= ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_Origin_Date_From)||' )';
  END IF;
  
       IF p_Origin_Date_To      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||'and (SELECT MAX (apply_date)
             FROM ar.ar_receivable_applications_all
            WHERE     application_type = ''CM''
                  AND customer_trx_id = rctr.customer_trx_id) <= ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_Origin_Date_To)||' )';
  END IF;
  
  -- Identify the driving table and Filtering the data based on the parameters  
		l_stmt :='	select  d.customer_trx_id 
					from 	ra_cust_trx_line_gl_dist_all d,
							gl_periods p,
							ra_customer_trx_all t
		where d.customer_trx_id =t.customer_trx_id 
		and t.cust_trx_type_id = 2
		and  t.interface_header_context=''ORDER ENTRY''
		 and ''REC'' = d.account_class
		 AND ''Y'' = d.latest_rec_flag
		 and p.period_set_name = ''4-4-QTR''
		 and trunc(d.gl_date) between trunc(p.start_date) and trunc(p.end_date)
		 and p.period_year='||P_Period_Year||'';
		 
      l_stmt:= l_stmt||' '||LC_PERIOD_NAME; 


	  -- Process the driving data to below query
	l_stmt_hdr_qry :=' SELECT DISTINCT '||P_Process_id||' process_id,
          gp.period_year FISCAL_YEAR,
          gp.period_name FISCAL_MONTH,
          PARTY_SITES.PARTY_SITE_NUMBER CUSTOMER_NUMBER,
          (SELECT MAX (apply_date)
             FROM ar.ar_receivable_applications_all
            WHERE     application_type = ''CM''
                  AND customer_trx_id = rctr.customer_trx_id)
             origin_date,
          sites.location ship_name,
          NVL (xocr.PAYMENT_TYPE_CODE, ''ON ACCOUNT'') invoice_type,
          TRUNC (rctr.trx_date) invoice_date,
          TRUNC (RCTGR.GL_DATE) BUSINESS_DATE,
          XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (
             ''TAXRATE'',
             rctr.customer_trx_id,
             rctlr.customer_trx_line_id)
             tax_rate,
          ps.freight_original freight_original,
          NVL (ps.amount_applied, 0) invoice_total,
          NULL CREDIT,
          NULL GLNO,
          CASE
             WHEN PSO.AMOUNT_DUE_REMAINING > 0
             THEN
                PARTY_SITEB.PARTY_SITE_NUMBER
             ELSE
                TO_CHAR (0)
          END
             AR_CUSTOMER_NUMBER,
          NULL restock_fee_percent,
          loc.organization_code location,
          CASE
             WHEN PS.AMOUNT_APPLIED IS NOT NULL THEN PS.AMOUNT_APPLIED * -1
             ELSE PSO.AMOUNT_DUE_ORIGINAL - PSO.AMOUNT_DUE_REMAINING
          END
             AMOUNT_PAID,
          XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (
             ''TAXAMT'',
             rctr.customer_trx_id,
             rctlr.customer_trx_line_id)
             tax_amt,
          rctr.trx_number invoice_number,
          rctr.interface_header_attribute1 order_number,
          rcto.trx_number original_invoice_number,
          --msi.segment1 part_number,                    --commented and added below by Maha on 4/16/2014 for TMS#20140203-00270
          (SELECT /*+ RESULT_CACHE */
                  segment1
             FROM mtl_system_items_b
            WHERE     inventory_item_id = rctlr.inventory_item_id
                  AND organization_id = loc.organization_id)
             part_number,
          rctr.reason_code credit_reason_sale,
          rctlr.extended_amount invoice_line_amount,
          NVL (rctlr.quantity_invoiced, rctlr.quantity_credited) qty_shipped,
          jsr.salesrep_number sales_rep_no,
          ac.name customer_territory,
          CASE
             WHEN INSTR (sites.location, ''-'') > 0
             THEN
                SUBSTR (sites.location, 1, (INSTR (sites.location, ''-'') - 1))
             ELSE
                sites.location
          END
             customer_name,
          custb.account_name master_name,
          rctlr.sales_order orginal_order_number,
          FU.USER_NAME TAKEN_BY
     FROM ONT.oe_order_headers_all oho,
          ONT.oe_order_lines_all olo,
          ONT.oe_order_headers_all ohr,
          ONT.oe_order_lines_all olr,
          AR.ra_customer_trx_all rctr,
          AR.ra_customer_trx_lines_all rctlr,
          AR.ra_cust_trx_line_gl_dist_all rctgr,
          AR.ra_customer_trx_all rcto,
          AR.ra_customer_trx_lines_all rctlo,
          AR.ra_cust_trx_line_gl_dist_all rctgo,
          AR.ar_payment_schedules_all ps,
          AR.ar_payment_schedules_all pso,
          AR.hz_cust_accounts custs,
          AR.hz_parties partys,
          AR.hz_cust_site_uses_all sites,
          AR.hz_cust_acct_sites_all cust_sites,
          AR.hz_party_sites party_sites,
          AR.hz_locations locs,
          AR.hz_cust_accounts custb,
          AR.hz_parties partyb,
          AR.hz_cust_site_uses_all siteb,
          AR.hz_cust_acct_sites_all cust_siteb,
          AR.hz_party_sites party_siteb,
          AR.hz_locations locb,
          AR.hz_customer_profiles hzp,
          AR.AR_COLLECTORS AC,
          JTF.jtf_rs_salesreps jsr,
          APPS.org_organization_definitions loc,
          APPS.fnd_user fu,
          --AR.RA_CUST_TRX_TYPES_all ctt,                    --commented by Maha on 4/16/2014 for TMS#20140203-00270
          GL.gl_periods gp,
          ont.oe_transaction_types_tl ott,
          xxwc.xxwc_om_cash_refund_tbl xocr
    --inv.MTL_SYSTEM_ITEMS_B msi                        --commented by Maha on 4/16/2014 for TMS#20140203-00270
    WHERE     ohr.header_id = olr.header_id
          AND olr.reference_header_id = olo.header_id
          AND olr.line_type_id = ott.transaction_type_id
          AND olr.reference_line_id = olo.line_id
          AND oho.header_id = olo.header_id
          AND ohr.header_id = xocr.return_header_id(+)
          AND rctr.cust_trx_type_id = 2 --commented below and added by Maha on 4/16/2014 for TMS#20140203-00270
          --AND rctr.CUST_TRX_TYPE_ID = CTT.CUST_TRX_TYPE_ID
          --AND ctt.name = ''Credit Memo''
          AND rctr.interface_header_context = ''ORDER ENTRY''
          AND TO_CHAR (ohr.order_number) = rctr.interface_header_attribute1
          AND TO_CHAR (olr.line_id) = rctlr.interface_line_attribute6
          AND rctr.customer_trx_id = rctlr.customer_trx_id
          --AND rctlr.inventory_item_id = msi.inventory_item_id            --commented by Maha on 4/16/2014 for TMS#20140203-00270
          AND rcto.INTERFACE_header_CONTEXT = ''ORDER ENTRY''
          AND TO_CHAR (oho.order_number) = rcto.interface_header_attribute1
          AND TO_CHAR (olo.line_id) = rctlo.interface_line_attribute6
          AND rcto.customer_trx_id = rctlo.customer_trx_id
          --AND rctlo.inventory_item_id = msi.inventory_item_id                --commented by Maha on 4/16/2014 for TMS#20140203-00270
          AND rctgo.customer_trx_id = rcto.customer_trx_id
          AND ''REC'' = rctgo.account_class
          AND ''Y'' = rctgo.latest_rec_flag
          AND rctgr.customer_trx_id = rctr.customer_trx_id
          AND ''REC'' = rctgr.account_class
          AND ''Y'' = rctgr.latest_rec_flag
          AND ps.customer_trx_id = rctr.customer_trx_id
          AND pso.customer_trx_id = rcto.customer_trx_id
          AND rctr.ship_to_customer_id = custs.cust_account_id
          AND custs.party_id = partys.party_id
          AND partys.party_id = party_sites.party_id --Added by Maha on 4/16/2014 for TMS#20140203-00270
          AND RCTR.SHIP_TO_SITE_USE_ID = SITES.SITE_USE_ID(+)
          AND CUST_SITES.CUST_ACCT_SITE_ID(+) = SITES.CUST_ACCT_SITE_ID
          AND cust_sites.party_site_id = party_sites.party_site_id(+)
          AND party_sites.location_id = locs.location_id(+)
          AND rctr.bill_to_customer_id = custb.cust_account_id
          AND custb.party_id = partyb.party_id
          AND partyb.party_id = party_siteb.party_id --Added by Maha on 4/16/2014 for TMS#20140203-00270
          AND rctr.bill_to_site_use_id = siteb.site_use_id
          AND CUST_SITEB.CUST_ACCT_SITE_ID = SITEB.CUST_ACCT_SITE_ID
          AND cust_siteb.party_site_id = party_siteb.party_site_id
          AND party_siteb.location_id = locb.location_id
          AND hzp.cust_account_id = custb.cust_account_id
          AND hzp.site_use_id = siteb.site_use_id
          AND AC.COLLECTOR_ID = HZP.COLLECTOR_ID(+)
          AND jsr.salesrep_id = rctr.primary_salesrep_id
          AND jsr.org_id = rctr.org_id
          AND loc.organization_id = olr.ship_from_org_id
          AND fu.user_id = ohr.created_by
          and gp.period_set_name = ''4-4-QTR''
          and trunc(rctgr.gl_date) between trunc(gp.start_date) and trunc(gp.end_date)
          and rctr.customer_trx_id = :1
		  ';
      
      
      l_stmt_hdr_qry:= l_stmt_hdr_qry||' '||LC_WHERE_COND; 
      
      
--Dbms_Output.Put_Line('l_stmt_hdr_qry'||l_stmt_hdr_qry);
  OPEN L_REF_CURSOR1 FOR l_stmt;
    FETCH L_REF_CURSOR1 BULK COLLECT INTO TRX_TAB;
  CLOSE L_REF_CURSOR1;

 IF TRX_TAB.COUNT>0
  THEN
  FOR I IN 1..TRX_TAB.COUNT
  LOOP
  
     OPEN L_REF_CURSOR2 FOR l_stmt_hdr_qry USING 
  TRX_TAB(i).CUSTOMER_TRX_ID;
  loop
    FETCH L_REF_CURSOR2 BULK COLLECT INTO l_credit_memo_state_REC_TBL limit 10000;
   
  if l_credit_memo_state_REC_TBL.COUNT>0
  Then  
    
      FORALL J IN 1..l_credit_memo_state_REC_TBL.COUNT 
  Insert Into xxeis.EIS_XXWC_CREDIT_INV_TAB Values l_credit_memo_state_REC_TBL(J);
  

    END IF;
    exit when L_REF_CURSOR2%notfound;
    END LOOP;
       --commit; --Commented for version 1.1
    
    close L_REF_CURSOR2;
 END LOOP; 
END IF;

     exception when others then
     Fnd_File.Put_Line(FND_FILE.log,'The ERROR '||sqlcode||sqlerrm);
     END;
         
/* --Commented code for version 1.1     
PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     siva  	04/16/2016   Initial Build --TMS#20160418-00101  by Siva   	on 04-16-2016
--//============================================================================
  BEGIN  
  DELETE FROM XXEIS.EIS_XXWC_CREDIT_INV_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;
*/ --Commented code for version 1.1

end  eis_xxwc_credit_stmt_pkg;
/
