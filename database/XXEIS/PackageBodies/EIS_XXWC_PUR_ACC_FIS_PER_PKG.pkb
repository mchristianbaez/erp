create or replace 
package body       XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "PURCHASES AND ACCRUALS - By VENDOR, LOB, BU,AGREEMENT YEAR, FISCAL PERIOD"
--//
--// Object Name         		:: EIS_XXWC_PUR_ACC_FIS_PER_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/29/2016      Initial Build  --TMS#20160429-00036 --Performance Tuning
--//============================================================================
procedure GET_PUR_ACC_FIS_PER_DTLS (	p_process_id 	in number,
								P_Vendor 	in varchar2,
								p_Lob			in varchar2,
								P_MVID			in varchar2,
								P_Fiscal_Period in varchar2,
								P_Agreement_Year	in number
							) as
l_cust_sql               varchar2(32000);
l_accural_sql            varchar2(32000);
l_purchase_sql           varchar2(32000);
l_fiscal_period_params   varchar2(32000);
l_lob_param              varchar2(32000);
l_vendor_param           varchar2(32000);
l_mvid_parm              varchar2(32000);
l_agreement_year_param   varchar2(32000);
l_fiscal_period_param    varchar2(32000);
l_mvid_parms             varchar2(32000);
l_lob_params             varchar2(32000);
l_agreement_year_params  varchar2(32000);
  
--  l_party_name					varchar2(150);
    type CUSTOMER_REC is RECORD
  (   customer_id 	number,
      party_name 	varchar2(240)
  );
type ACCURAL_REC
IS
--//============================================================================
--//
--// Object Name         :: get_pur_acc_fis_per_dtls  
--//
--// Object Usage 		 :: This Object Referred by "PURCHASES AND ACCRUALS - By VENDOR, LOB, BU,AGREEMENT YEAR, FISCAL PERIOD"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_PUR_PETSQO_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  04/29/2016       Initial Build  --TMS#20160429-00036 --Performance Tuning
--//============================================================================
  RECORD
  (
    process_id         number,
    mvid               varchar2(150),
    vendor_name        varchar2(150),
    lob                varchar2(150),
    bu                 varchar2(150),
    agreement_year     number,
    fiscal_period      varchar2(150),
    purchases          number,
    coop               number,
    rebate             number,    
    total_accruals     number    
);
    
type purchase_rec is RECORD
    (
    process_id         number,
    mvid               varchar2(150),
    vendor_name        varchar2(150),
    lob                varchar2(150),
    bu                 varchar2(150),
    agreement_year     number,
    fiscal_period      varchar2(150),
    purchases          number,
    coop               number,
    rebate             number,    
    total_accruals     number  
  );
    
    
	  l_PROCESS_ID 		    	NUMBER;
    l_AGREEMENT_YEAR     	NUMBER;
    l_MVID               	VARCHAR2(150);
    l_VENDOR_NAME        	VARCHAR2(150);
    l_LOB                	VARCHAR2(150);
    l_bu                 	varchar2(150);
    l_ACCRUAL_PURCHASES   NUMBER;
    l_REBATE             	NUMBER;
    l_COOP               	NUMBER;
    L_TOTAL_ACCRUALS     	NUMBER;
	  L_PARTY_NAME 			    VARCHAR2(150);
	  l_party_id 			     	number;
    l_FISCAL_PERIOD       varchar2(150);

  
  L_REF_CURSOR1         CURSOR_TYPE4;
  l_ref_cursor2 				CURSOR_TYPE4;
  L_REF_CURSOR3 				CURSOR_TYPE4;
  
	type customer_rec_tab is table of customer_rec Index By Binary_Integer;
	customer_tab customer_rec_tab;
	type accural_rec_tab is table of accural_REC;  
	accural_tab accural_REC_tab  :=  accural_REC_tab();
	type purchase_rec_tab is table of purchase_rec;  
    PURCHASE_TAB PURCHASE_REC_TAB  :=  PURCHASE_REC_TAB();
    
    L_COUNTER NUMBER;
    A_COUNTER NUmber;
  BEGIN
  
  L_COUNTER:=1;
  A_COUNTER:=1;
  
  fnd_file.put_line(fnd_file.log,'Started');--TMS#20160429-00036    by PRAMOD  on 04-29-2016
        if p_fiscal_period is not null then  
      L_Fiscal_Period_PARAM:= L_Fiscal_Period_PARAM||'  and Fiscal_Period  in  ('||xxeis.eis_rs_utility.get_param_values(P_Fiscal_Period)||' )';
        else
          L_Fiscal_Period_PARAM:=' and 1=1';
    end if;
  
         
   if p_mvid is not null then  
        L_Mvid_parm:= L_Mvid_parm||' and MVID   in   ('||xxeis.eis_rs_utility.get_param_values(p_Mvid)||' )';
          else
         L_Mvid_parm:=' and 1=1';
      end if;   
    
          IF P_LOB IS NOT NULL THEN   
        L_LOB_PARAM:= L_LOB_PARAM||'  and lob  in  ('||xxeis.eis_rs_utility.get_param_values(P_LOB)||' )';
        else
          L_LOB_PARAM:=' and 1=1';
        end if;
        
    
    
        IF P_AGREEMENT_YEAR IS NOT NULL THEN  --TMS#20160429-00036    by PRAMOD  on 04-29-2016
        L_AGREEMENT_YEAR_PARAM:= L_AGREEMENT_YEAR_PARAM||' and AGREEMENT_YEAR   in   ('||xxeis.eis_rs_utility.get_param_values(P_AGREEMENT_YEAR)||' )';
          else
         L_Agreement_Year_PARAM:=' and 1=1';
      end if;
 
      IF P_VENDOR IS NOT NULL THEN  
      L_VENDOR_PARAM:= L_VENDOR_PARAM||'  and party_name  in  ('||xxeis.eis_rs_utility.get_param_values(P_VENDOR)||' )';
        else
          L_VENDOR_PARAM:=' and 1=1';
    end if;
 
  
  L_CUST_SQL :='select C.CUSTOMER_ID,
                    c.party_name
              from XXCUS.XXCUS_REBATE_CUSTOMERS C
              where C.PARTY_ATTRIBUTE1   =''HDS_MVID''
           '||L_VENDOR_PARAM||'
        ';
			  
--    OPEN l_ref_cursor1  FOR l_cust_sql;--TMS#20160429-00036    by PRAMOD  on 04-29-2016
EXECUTE IMMEDIATE l_cust_sql  bulk collect into CUSTOMER_TAB;
--       FETCH L_REF_CURSOR1 bulk collect into CUSTOMER_TAB;-- limit 10000;
       

--	CLOSE L_REF_CURSOR1;
fnd_file.put_line(fnd_file.log,'upper CUSTOMER_TAB'||CUSTOMER_TAB.count);

--  DBMS_OUTPUT.PUT_LINE('CUSTOMER_TAB.count'||CUSTOMER_TAB.count);

FOR J IN 1..CUSTOMER_TAB.count--TMS#20160429-00036    by PRAMOD  on 04-29-2016
LOOP
-- L_PARTY_NAME:= CUSTOMER_TAB(J).PARTY_NAME;
-- L_PARTY_ID:= CUSTOMER_TAB(J).CUSTOMER_ID;
-- DBMS_OUTPUT.PUT_LINE('Start L_ACCURAL_SQL');
 	L_ACCURAL_SQL:=
  '  SELECT 
    MVID ,
    :party_name VENDOR_NAME,
    LOB ,
    BU ,
    AGREEMENT_YEAR,
    fiscal_period,
    0 PURCHASES ,
    SUM(
    CASE
      WHEN REBATE_TYPE=''COOP''
      THEN accrual_amount
      ELSE 0
    END) COOP ,
    SUM(
    CASE
      WHEN REBATE_TYPE=''REBATE''
      THEN accrual_amount
      ELSE 0
    END) REBATE ,
    SUM(accrual_amount) TOTAL_ACCRUALS
   FROM XXCUS.XXCUS_YTD_INCOME_B B
  WHERE B.OFU_CUST_ACCOUNT_ID =:CUST
      and calendar_year>=2013
 '||L_Fiscal_Period_PARAM||'
 '||L_Mvid_parm||'
 '||L_LOB_PARAM||'
 '||L_AGREEMENT_YEAR_PARAM||'
    GROUP BY
      MVID
      ,:party_name
      ,LOB
      ,BU
      ,AGREEMENT_YEAR 
      ,fiscal_period
    ';
    
--fnd_file.put_line(fnd_file.log,'L_ACCURAL_SQL   '||L_ACCURAL_SQL);
 
	OPEN l_ref_cursor2  FOR l_accural_sql using CUSTOMER_TAB(J).party_name,CUSTOMER_TAB(J).CUSTOMER_ID,CUSTOMER_TAB(J).party_name;
    LOOP--TMS#20160429-00036    by PRAMOD  on 04-29-2016
    fetch l_ref_cursor2 into
    l_MVID               ,
    l_VENDOR_NAME        ,
    l_LOB                ,
    l_bu                 ,
    l_agreement_year     ,
    l_fiscal_period      ,
    l_accrual_purchases  ,
    l_COOP               ,
    l_REBATE             ,
    l_TOTAL_ACCRUALS   ;  
    


	    accural_tab.extend;
        
        accural_tab(L_COUNTER).PROCESS_ID          	:=  P_PROCESS_ID;
        ACCURAL_TAB(L_COUNTER).MVID           	   	:=  L_MVID;
        ACCURAL_TAB(L_COUNTER).VENDOR_NAME        	:=  L_VENDOR_NAME;
        ACCURAL_TAB(L_COUNTER).LOB           	    	:=  L_LOB;
        accural_tab(L_COUNTER).BU      			      	:=  l_BU;
        ACCURAL_TAB(L_COUNTER).AGREEMENT_YEAR      	:=  L_AGREEMENT_YEAR;
        accural_tab(L_COUNTER).FISCAL_PERIOD        :=  l_FISCAL_PERIOD;
        ACCURAL_TAB(L_COUNTER).PURCHASES            :=  L_ACCRUAL_PURCHASES;
        ACCURAL_TAB(L_COUNTER).COOP              		:=  L_COOP;
        ACCURAL_TAB(L_COUNTER).REBATE              	:=  L_REBATE;
		    accural_tab(L_COUNTER).TOTAL_ACCRUALS       :=  l_TOTAL_ACCRUALS;
		 l_counter    := l_counter+1;
     exit when L_REF_CURSOR2%NOTFOUND;
            end LOOP;
            close L_REF_CURSOR2;
 END LOOP;
-- fnd_file.put_line(fnd_file.log,'upper ACCRUALS_TAB'||accural_tab.count);
 
-- begin
  DBMS_OUTPUT.PUT_LINE('ACCRUALS_TAB.COUNT'||ACCURAL_TAB.COUNT);
  
--   fnd_file.put_line(fnd_file.log,'ACCRUALS_TAB.COUNT'||ACCURAL_TAB.COUNT);
--   DBMS_OUTPUT.PUT_LINE('PURCHASE_TAB.COUNT'||PURCHASE_TAB.COUNT);

        if p_fiscal_period is not null then  
      L_Fiscal_Period_PARAMS:= L_Fiscal_Period_PARAMS||'  and T.NAME  in  ('||xxeis.eis_rs_utility.get_param_values(P_Fiscal_Period)||' )';
        else
          L_Fiscal_Period_PARAMS:=' and 1=1';
    end if;--TMS#20160429-00036    by PRAMOD  on 04-29-2016
    
   if P_MVID is not null then  
        L_Mvid_parmS:= L_Mvid_parmS||' and C.CUSTOMER_ATTRIBUTE2   in   ('||xxeis.eis_rs_utility.get_param_values(p_Mvid)||' )';
          else
         L_Mvid_parmS:=' and 1=1';
      end if;   
    
          if P_LOB is not null then   
        L_LOB_PARAMS:= L_LOB_PARAMS||'  and Z.PARTY_NAME  in  ('||xxeis.eis_rs_utility.get_param_values(P_LOB)||' )';
        else
          L_LOB_PARAMS:=' and 1=1';
        end if;
        
    
    
        if P_AGREEMENT_YEAR is not null then  
        L_AGREEMENT_YEAR_PARAMs:= L_AGREEMENT_YEAR_PARAMs||' and to_number(M.CALENDAR_YEAR)   in   ('||xxeis.eis_rs_utility.get_param_values(P_AGREEMENT_YEAR)||' )';
          else
         L_AGREEMENT_YEAR_PARAMs:=' and 1=1';
      end if;
 
--   fnd_file.put_line(fnd_file.log,'PURCHASE_TAB');
	
	FOR J IN 1..CUSTOMER_TAB.COUNT  LOOP
-- L_PARTY_NAME:= CUSTOMER_TAB(J).PARTY_NAME;
-- L_PARTY_ID:= CUSTOMER_TAB(J).CUSTOMER_ID;
 	
	l_purchase_sql:='--TMS#20160429-00036    by PRAMOD  on 04-29-2016
  SELECT  C.CUSTOMER_ATTRIBUTE2 MVID ,
    :party_name VENDOR_name,
    Z.PARTY_NAME LOB ,
    Z1.PARTY_NAME BU ,
    to_number(M.CALENDAR_YEAR) AGREEMENT_YEAR ,
    T.NAME FISCAL_PERIOD,
    M.TOTAL_PURCHASES PURCHASES ,
    0 COOP ,
    0 REBATE ,
    0 TOTAL_ACCRUALS
  FROM APPS.XXCUSOZF_PURCHASES_MV M ,
    XXCUS.XXCUS_REBATE_CUSTOMERS C ,
    APPS.HZ_PARTIES Z ,
    APPS.HZ_PARTIES Z1,
    apps.ozf_time_ent_period t
  WHERE 1               =1
  AND M.LOB_ID          =Z.PARTY_ID
  AND M.BU_ID           =Z1.PARTY_ID
  AND M.MVID            =C.CUSTOMER_ID
  and C.CUSTOMER_ID     =:CUSTOMER_ID
  and t.ent_period_id   =m.period_id
  AND GRP_MVID          = 0
  AND grp_branch        = 1
  AND grp_qtr           = 1
  AND grp_year          = 1
  AND GRP_LOB           = 0
  AND GRP_BU_ID         = 0
  AND grp_period        = 0
  AND grp_cal_year      = 0
  AND GRP_CAL_PERIOD    = 1
  AND Z.ATTRIBUTE1      =''HDS_LOB''
  AND Z1.ATTRIBUTE1     =''HDS_BU''
 '||L_Fiscal_Period_PARAMS||'
 '||L_MVID_PARMS||'
 '||L_LOB_PARAMS||'
 '||L_AGREEMENT_YEAR_PARAMS||'
		';
    
--    fnd_file.put_line(fnd_file.log,'L_PURCHASE_SQL   '||L_PURCHASE_SQL);
OPEN l_ref_cursor3  FOR l_purchase_sql using CUSTOMER_TAB(J).party_name,CUSTOMER_TAB(J).CUSTOMER_ID;
    LOOP
    fetch l_ref_cursor3 into
    l_MVID          ,
    l_VENDOR_NAME  ,
    l_LOB   ,
    l_bu   ,
    l_AGREEMENT_YEAR ,
    l_FISCAL_PERIOD  ,
    l_accrual_purchases ,
    l_COOP ,
    l_REBATE ,
    l_TOTAL_ACCRUALS;  

	    PURCHASE_TAB.extend;
        
      PURCHASE_TAB(A_COUNTER).PROCESS_ID         := P_PROCESS_ID;
      PURCHASE_TAB(A_COUNTER).AGREEMENT_YEAR     := L_AGREEMENT_YEAR;
      PURCHASE_TAB(A_COUNTER).MVID               := l_MVID;
      PURCHASE_TAB(A_COUNTER).VENDOR_NAME        := l_VENDOR_NAME;
      PURCHASE_TAB(A_COUNTER).LOB                := l_LOB;
      PURCHASE_TAB(A_COUNTER).BU                 := l_BU;
      PURCHASE_TAB(A_COUNTER).FISCAL_PERIOD      := l_FISCAL_PERIOD;
      PURCHASE_TAB(A_COUNTER).PURCHASES          := l_ACCRUAL_PURCHASES;
      PURCHASE_TAB(A_COUNTER).REBATE             := l_REBATE;
      PURCHASE_TAB(A_COUNTER).COOP               := l_COOP;
      PURCHASE_TAB(A_COUNTER).TOTAL_ACCRUALS     := L_TOTAL_ACCRUALS;
      
  		 A_COUNTER    := A_COUNTER+1;
         exit when L_REF_CURSOR3%NOTFOUND;
            end LOOP;
            close L_REF_CURSOR3;
 END LOOP;
-- end;
--  fnd_file.put_line(fnd_file.log,'upper PURCHASE_TAB'||PURCHASE_TAB.count);
--	 dbms_output.put_line('Total records'||PURCHASE_TAB.count);
--     DBMS_OUTPUT.PUT_LINE('Income Sql Time End time'||TO_CHAR(SYSDATE,'dd-mon-yyyy hh24:mi:ss'));
     
      if L_COUNTER  >= 1  then--TMS#20160429-00036    by PRAMOD  on 04-29-2016
                  FORALL J IN 1..ACCURAL_TAB.COUNT 
                     INSERT INTO XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_TAB
                          VALUES accural_tab (J);                    
               END IF; 
               commit;
			   
	      if A_COUNTER >= 1  then--TMS#20160429-00036    by PRAMOD  on 04-29-2016
                  FORALL J IN 1..PURCHASE_TAB.COUNT 
                     INSERT INTO XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_TAB
                          VALUES PURCHASE_TAB (J);    
                
               end if; 
			   
    COMMIT;      
EXCEPTION WHEN OTHERS THEN
 fnd_file.put_line(fnd_file.log,'THE ERROR IS'||SQLCODE||SQLERRM);
	  END GET_PUR_ACC_FIS_PER_DTLS;
    
PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
	 --//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/29/2016      Initial Build --TMS#20160429-00036  --Performance Tuning
--//============================================================================
  BEGIN  
  DELETE FROM XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
end ;  
end EIS_XXWC_PUR_ACC_FIS_PER_PKG;
/
