CREATE OR REPLACE package body   XXEIS.EIS_XXWC_PURCHASE_VEN_RPT_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_PURCHASE_VEN_RPT_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160412-00033  --Performance Tuning
--//============================================================================


  procedure GET_ACC_PUR_DTLS (p_process_id            in number,
                            P_MVID                    IN varchar2,
                            p_Bu                      in varchar2,
                            p_Agreement_Year 	        in number,
                            P_LOB 				            in varchar2,
                            P_VENDOR 			            IN VARCHAR2
                            ) as
--//============================================================================
--//
--// Object Name         :: GET_ACC_PUR_DTLS  
--//
--// Object Usage 		 :: This Object Referred by "PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, AGREEMENT_YEAR, REBATE TYPE(NEW)"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_PUR_LOB_BU_TYPE_REB_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160412-00033 --Performance Tuning
--//============================================================================							
  L_CUST_SQL 		            varchar2(32000);
  L_ACCRUALS_SQL  	        varchar2(32000); 
  L_PURCHASE_SQL            varchar2(32000);
  L_CONDITION_STR           varchar2(32000);
  L_CONDITION_STR1          VARCHAR2(32000);
  L_AGREEMENT_YEAR  	      number;
	L_PERIOD_ID 			        NUMBER;
	L_MVID 				            varchar2(150);
	L_CUST_ID 			          number;
	L_VENDOR 				          varchar2(150); 
	L_LOB 				            varchar2(150) ;
	L_BU 					            varchar2(150);
	L_ACCRUAL_PURCHASES  	    number;
	L_REBATE 				          NUMBER;
  L_OFFER_NAME              VARCHAR2(240) ;
	L_COOP 				            number;
	L_TOTAL 				          NUMBER;
  
  L_COUNT                   number;
  L_AGREEMENT_YEAR_PARAM    varchar2(32000);
  L_LOB_PARAM               varchar2(32000);
  L_VENDOR_PARAM            VARCHAR2(32000);
  
  l_bu_parmp varchar2(32000);
        l_mvid_parmp varchar2(32000);
        l_lob_paramp varchar2(32000);
l_agreement_year_paramp varchar2(32000);
  
  
  L_PARTY_NAME VARCHAR2(240);
  L_PARTY_ID NUMBER;
  l_process_id number;
  l_mvid_parm VARCHAR2(32000);
  L_Bu_parm VARCHAR2(32000);


  type CUSTOMER_REC is RECORD
  (   CUSTOMER_ID NUMBER,
      party_name varchar2(240),
      CUSTOMER_ATTRIBUTE2 VARCHAR2(240)
  );
  
    type ACCRUALS_REC is RECORD
  ( 
  process_id            number,
  mvid                  varchar2(150),
  vendor                varchar2(150) ,
  lob                   varchar2(150) ,
  bu                    varchar2(150) ,
  agreement_year        number,
  purchases             number,
  coop                  number,
  rebate                number,
  total_accruals        number
  );

  type PURCHASE_REC is RECORD
  ( 
  PROCESS_ID            number,
  mvid                  varchar2(150) ,
  vendor                varchar2(150) ,
  lob                   varchar2(150) ,
  bu                    varchar2(150) ,
  agreement_year        number,
  purchases             number,
  coop                  number,
  rebate                number,
  total_accruals        number
  );
    L_REF_CURSOR1                 CURSOR_TYPE4;
    L_REF_CURSOR2                 CURSOR_TYPE4;
    l_ref_cursor3                 CURSOR_TYPE4;
  type customer_rec_tab is table of customer_rec Index By Binary_Integer;
      customer_tab customer_rec_tab;
  type purchase_rec_tab is table of purchase_rec;  
      purchase_tab purchase_rec_tab  :=  purchase_rec_tab();
  type ACCRUALS_REC_tab is table of ACCRUALS_REC;  
      ACCRUALS_TAB ACCRUALS_REC_tab  :=  ACCRUALS_REC_tab();

  L_COUNTER NUMBER;
  A_COUNTER number;
  L_TEST   varchar2(240) ;
  L_TESTING varchar2(240) ;
    pr_vendor varchar2(240) ;
  begin
  CUSTOMER_TAB.delete;
  PURCHASE_TAB.DELETE;
  L_COUNTER :=1;
  A_COUNTER:=1;
  L_TEST:='''';
  L_testing:='''';
  
--FND_FILE.PUT_LINE(FND_FILE.log,'L_test '||L_TEST);
--FND_FILE.PUT_LINE(FND_FILE.log,'Started the package');
--FND_FILE.PUT_LINE(FND_FILE.log,'L_testing '||L_testing);




     if p_mvid is not null then  
        L_Mvid_parm:= L_Mvid_parm||' and MVID   in   ('||xxeis.eis_rs_utility.get_param_values(p_Mvid)||' )';
          else
         L_Mvid_parm:=' and 1=1';
      end if;   
    
    
    if p_bu is not null then  
        L_Bu_parm:= L_Bu_parm||' and Bu   in   ('||xxeis.eis_rs_utility.get_param_values(p_Bu)||' )';
          else
         L_Bu_parm:=' and 1=1';
      end if; 
    
    
        IF P_AGREEMENT_YEAR IS NOT NULL THEN  
        L_AGREEMENT_YEAR_PARAM:= L_AGREEMENT_YEAR_PARAM||' and AGREEMENT_YEAR   in   ('||xxeis.eis_rs_utility.get_param_values(P_AGREEMENT_YEAR)||' )';
          else
         L_Agreement_Year_PARAM:=' and 1=1';
      end if;


        IF P_LOB IS NOT NULL THEN   
        L_LOB_PARAM:= L_LOB_PARAM||'  and lob  in  ('||xxeis.eis_rs_utility.get_param_values(P_LOB)||' )';
        else
          L_LOB_PARAM:=' and 1=1';
        end if;
    
      IF P_VENDOR IS NOT NULL THEN  
      L_VENDOR_PARAM:= L_VENDOR_PARAM||'  and party_name  in  ('||xxeis.eis_rs_utility.get_param_values(P_VENDOR)||' )';
        else
          L_VENDOR_PARAM:=' and 1=1';
    end if;
 
  
  L_CUST_SQL :='select C.CUSTOMER_ID,
                       c.party_name,
                       c.CUSTOMER_ATTRIBUTE2
              from XXCUS.XXCUS_REBATE_CUSTOMERS C
              where C.PARTY_ATTRIBUTE1   =''HDS_MVID''
           '||L_VENDOR_PARAM||'
        ';
                 
    fnd_file.put_line(fnd_file.log,'l_cust_sql IS '||l_cust_sql);
              
    --prase sql 
    
    OPEN l_ref_cursor1  FOR l_cust_sql;
       FETCH L_REF_CURSOR1 bulk collect into CUSTOMER_TAB;  
    close L_REF_CURSOR1;
    
        
   FOR J IN 1..CUSTOMER_TAB.COUNT    LOOP
       
    L_ACCRUALS_SQL := 
    'SELECT 
    MVID,
    :PARTY_NAME VENDOR,
    LOB,
    BU,
    AGREEMENT_YEAR,
    0 PURCHASES,
    SUM(
    CASE
      WHEN REBATE_TYPE=''COOP''
      THEN accrual_amount
      ELSE 0
    END) COOP ,
    SUM(
    CASE
      WHEN REBATE_TYPE=''REBATE''
      THEN accrual_amount
      ELSE 0
    END) REBATE ,
    SUM(accrual_amount) TOTAL_ACCRUALS
FROM 
    XXCUS.XXCUS_YTD_INCOME_B B 
  WHERE 1                  =1
  AND B.OFU_CUST_ACCOUNT_ID=:CUSTOMER_ID
'||L_Mvid_parm||'
 '||L_Bu_parm||'
 '||L_Agreement_Year_PARAM||'
 '||L_LOB_PARAM||'
  GROUP BY 
    MVID ,
    :PARTY_NAME  ,
    LOB ,
    BU ,
    AGREEMENT_YEAR 
';
    
    
     OPEN l_ref_cursor3  FOR l_ACCRUALS_sql using CUSTOMER_TAB(J).PARTY_NAME,CUSTOMER_TAB(J).CUSTOMER_ID,CUSTOMER_TAB(J).PARTY_NAME;
    LOOP
     
   FETCH  l_ref_cursor3  INTO 
--          l_process_id ,
          l_mvid 				            ,
          l_vendor                  ,
          l_lob                     ,
          l_bu                      ,
          L_AGREEMENT_YEAR  	      ,
          L_ACCRUAL_PURCHASES  	    ,
          L_COOP 				            ,
          L_REBATE 				          ,
          l_total
          ;
      exit when L_REF_CURSOR3%NOTFOUND;

        ACCRUALS_TAB.extend;
        
        ACCRUALS_TAB(A_COUNTER).process_id          :=  p_process_id;
        ACCRUALS_TAB(A_COUNTER).MVID                :=  L_MVID;
        DBMS_OUTPUT.PUT_LINE('L_MVID'||L_MVID);
        ACCRUALS_TAB(A_COUNTER).vendor              :=  l_vendor;
        ACCRUALS_TAB(A_COUNTER).lob                 :=  l_lob;
        ACCRUALS_TAB(A_COUNTER).BU                  :=  L_BU;
        ACCRUALS_TAB(A_COUNTER).agreement_year      :=  l_agreement_year;
        ACCRUALS_TAB(A_COUNTER).purchases           :=  L_ACCRUAL_PURCHASES;
        ACCRUALS_TAB(A_COUNTER).coop                :=  L_COOP;
        ACCRUALS_TAB(A_COUNTER).REBATE              :=  L_REBATE;
        ACCRUALS_TAB(A_COUNTER).total_accruals      :=  L_TOTAL;

                        A_COUNTER    := A_COUNTER+1;
            END LOOP;
            CLOSE L_REF_CURSOR3; 
       end LOOP;
    
  
    IF P_BU IS NOT NULL THEN  
        l_bu_parmp:= l_bu_parmp||' and Z1.PARTY_NAME  in   ('||xxeis.eis_rs_utility.get_param_values(p_Bu)||' )';
          ELSE
         l_bu_parmp:=' and 1=1';
      end if; 
    
    IF P_MVID IS NOT NULL THEN  
        l_mvid_parmp:= l_mvid_parmp||' and C.CUSTOMER_ATTRIBUTE2 in ('||xxeis.eis_rs_utility.get_param_values(p_Mvid)||' )';
          ELSE
         l_mvid_parmp:=' and 1=1';
      end if; 
      
      
        IF P_AGREEMENT_YEAR IS NOT NULL THEN  
        l_agreement_year_paramp:= L_AGREEMENT_YEAR_PARAMP||' and to_number(M.CALENDAR_YEAR)   in   ('||xxeis.eis_rs_utility.get_param_values(P_AGREEMENT_YEAR)||' )';
          ELSE
         L_Agreement_Year_PARAMp:=' and 1=1';
      end if;


        IF P_LOB IS NOT NULL THEN   
        l_lob_paramp:= l_lob_paramp||'  and  Z.PARTY_NAME  in  ('||xxeis.eis_rs_utility.get_param_values(P_LOB)||' )';
        ELSE
          L_LOB_PARAMP:=' and 1=1';
        end if;
        
      
        
  FOR S IN 1..CUSTOMER_TAB.COUNT  LOOP
  DBMS_OUTPUT.PUT_LINE('Entered into L_PURCHASE_SQL');
     L_PURCHASE_SQL:= 
      ' SELECT 
    C.CUSTOMER_ATTRIBUTE2 MVID ,
    :PARTY_NAME VENDOR ,
    Z.PARTY_NAME LOB ,
    Z1.PARTY_NAME BU ,
    to_number(M.CALENDAR_YEAR) AGREEMENT_YEAR ,
    M.TOTAL_PURCHASES PURCHASES ,
    0 COOP ,
    0 REBATE ,
    0 TOTAL_ACCRUALS
  FROM APPS.XXCUSOZF_PURCHASES_MV M ,
    APPS.HZ_PARTIES Z ,
    XXCUS.XXCUS_REBATE_CUSTOMERS C,
    APPS.HZ_PARTIES Z1
  WHERE 1               =1
  AND M.LOB_ID          =Z.PARTY_ID
  AND M.BU_ID           =Z1.PARTY_ID
  AND M.MVID            =:CUSTOMER_ID
  and M.MVID            = C.CUSTOMER_ID
  AND GRP_MVID          = 0
  AND grp_branch        = 1
  AND grp_qtr           = 1
  AND grp_year          = 1
  AND GRP_LOB           = 0
  AND grp_bu_id         = 0
  AND grp_period        = 1
  AND grp_cal_year      = 0
  AND GRP_CAL_PERIOD    = 1
  AND Z.ATTRIBUTE1      =''HDS_LOB''
  AND Z1.ATTRIBUTE1     =''HDS_BU''
 '||L_Mvid_parmp||'
 '||L_Bu_parmp||'
 '||L_Agreement_Year_PARAMP||'
 '||L_LOB_PARAMP||'
            '
	  ;
    
    DBMS_OUTPUT.PUT_LINE('L_PURCHASE_SQL  '||L_PURCHASE_SQL);

  OPEN l_ref_cursor2  FOR L_PURCHASE_SQL using CUSTOMER_TAB(S).PARTY_NAME,CUSTOMER_TAB(S).CUSTOMER_ID;
    LOOP
   FETCH  L_REF_CURSOR2  INTO 
--          l_process_id ,
          l_mvid 				            ,
          l_vendor                  ,
          l_lob                     ,
          l_bu                      ,
          L_AGREEMENT_YEAR  	      ,
          L_ACCRUAL_PURCHASES  	    ,
          L_COOP 				            ,
          L_REBATE 				          ,
          l_total
          ;
	exit when l_ref_cursor2%notfound;
          
        PURCHASE_TAB.extend;
        
        purchase_tab(l_counter).process_id          :=  p_process_id;
        purchase_tab(l_counter).mvid                :=  l_mvid;
        purchase_tab(l_counter).vendor              :=  l_vendor;
        purchase_tab(l_counter).lob                 :=  l_lob;
        PURCHASE_TAB(L_COUNTER).BU                  :=  L_BU;
        purchase_tab(l_counter).agreement_year      :=  l_agreement_year;
        purchase_tab(l_counter).purchases           :=  L_ACCRUAL_PURCHASES;
        purchase_tab(l_counter).coop                :=  L_COOP;
        purchase_tab(l_counter).REBATE              :=  L_REBATE;
        purchase_tab(l_counter).total_accruals      :=  L_TOTAL;            
       
           L_COUNTER    := L_COUNTER+1;
            end LOOP;
           
            close L_REF_CURSOR2;
           
 end LOOP;

      if A_COUNTER >= 1  then
      FORALL J IN 1 .. ACCRUALS_TAB.COUNT    
                     insert into xxeis.EIS_PUR_ACCU_RPT_DATA_TAB
                          values ACCRUALS_TAB(J); 
       END IF; 
       
      COMMIT;
      if L_COUNTER >= 1  then
                  FORALL S IN 1 .. PURCHASE_TAB.COUNT    
                     INSERT INTO xxeis.EIS_PUR_ACCU_RPT_DATA_TAB
                          VALUES PURCHASE_TAB (S);    
                
       end if; 
    COMMIT;      
EXCEPTION WHEN OTHERS THEN
fnd_file.put_line(fnd_file.log,'THE ERROR IS'||SQLCODE||SQLERRM);
COMMIT; 
end get_acc_pur_dtls;

PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160412-00033  --Performance Tuning
--//============================================================================  
  BEGIN  
  DELETE FROM XXEIS.EIS_PUR_ACCU_RPT_DATA_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ; 

end EIS_XXWC_PURCHASE_VEN_RPT_PKG;
/
