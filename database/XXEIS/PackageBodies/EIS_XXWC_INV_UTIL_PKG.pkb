CREATE OR REPLACE 
PACKAGE BODY  XXEIS.EIS_XXWC_INV_UTIL_PKG
AS
  --//============================================================================
  --// Object Name          :: EIS_XXWC_INV_UTIL_PKG
  --//
  --// Object Type          :: Package Body
  --//
  --// Object Description   :: This Package will trigger in view and populate the values into View.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016         Initial Build  -- Added for TMS#20160601-00025.
  --// 1.1        Siva        10/17/2016         TMS#20160930-00078 
  --// 1.2        Siva        10-Jan-2017        TMS#20161025-00027 
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --// 1.4		  Siva		  18-May-2017		 TMS#20170306-00240
  --// 1.5		  Siva		  29-May-2017		 TMS#20170317-00082
  --// 1.6		  Siva		  13-Jun-2017		 TMS#20170220-00206
  --// 1.7	  	  Siva		  14-May-2018		 TMS#20180511-00218   
  --//============================================================================
FUNCTION GET_ONHAND_QTY(
    P_INVENTORY_ITEM_ID NUMBER,
    P_ORGANIZATION_ID   NUMBER,
    P_SUBINVENTORY_CODE VARCHAR2)
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Inventory - Onhand Quantity - WC"
  --//
  --// Object Name          :: GET_ONHAND_QTY
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016      Initial Build  -- Added for TMS#20160601-00025.
  --//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
g_onhand_qty := NULL;

l_sql :='SELECT SUM (NVL(moq1.Primary_Transaction_Quantity,0))
    FROM Mtl_Onhand_Quantities_Detail Moq1
    WHERE Moq1.Inventory_Item_Id = :1
    AND Moq1.Organization_Id     = :2
    AND Moq1.subinventory_code   = :3';
    BEGIN
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_SUBINVENTORY_CODE;
        g_onhand_qty := G_ONHAND_QTY_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO g_onhand_qty USING P_INVENTORY_ITEM_ID,P_ORGANIZATION_ID,P_SUBINVENTORY_CODE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_onhand_qty :=0;
    WHEN OTHERS THEN
    g_onhand_qty :=0;
    END;      
                      l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_SUBINVENTORY_CODE;
                       G_ONHAND_QTY_VLDN_TBL(L_HASH_INDEX) := g_onhand_qty;
    END;
     return  g_onhand_qty;
     EXCEPTION when OTHERS then
      g_onhand_qty:=0;
      RETURN  g_onhand_qty;

end GET_ONHAND_QTY;

FUNCTION get_invoice_url(
    p_po_distribution_id NUMBER,
    p_period_name        VARCHAR2,
    p_request_type       VARCHAR2)
  RETURN VARCHAR2
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "HDS Account Analysis Subledger Detail Report"
  --//
  --// Object Name          :: get_invoice_url
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.1        Siva        10/07/2016        TMS#20160930-00078
  --//============================================================================  
  L_HASH_INDEX VARCHAR2(100);
  l_sql        VARCHAR2(32000);
  CURSOR v_inv_url_cur
  IS
    SELECT DISTINCT a.url,
      ai.invoice_num
    FROM fnd_attached_docs_form_vl a,
      ap_invoice_distributions b,
      ap_invoices_all ai
    WHERE b.po_distribution_id = p_po_distribution_id
    AND b.period_name          = p_period_name
    AND b.invoice_id           = ai.invoice_id
    AND a.entity_name          = 'AP_INVOICES'
    AND a.pk1_value            = TO_CHAR (b.invoice_id)
    AND a.datatype_name        = 'Web Page'
    AND A.category_description = 'Invoice Internal'
    AND a.document_description = 'Documentum Image URL'
    AND A.function_name        = 'APXINWKB' ;
BEGIN
  g_url         := NULL;
  g_invoice_num := NULL;
  BEGIN
    l_hash_index       :=p_po_distribution_id||'-'||p_period_name;
    IF P_request_type   ='URL' THEN
      g_url            := g_url_vldn_tbl(L_HASH_INDEX);
    ELSif P_request_type='INVOICENUM' THEN
      g_invoice_num    := G_invoice_num_VLDN_TBL(L_HASH_INDEX);
    END IF;
  EXCEPTION
  WHEN no_data_found THEN
    BEGIN
      FOR rec IN v_inv_url_cur
      LOOP
        g_url         := g_url || ' , ' || rec.url;
        g_invoice_num := g_invoice_num || ' , ' || rec.invoice_num;
      END LOOP;
      g_url         := SUBSTR (g_url, 4);
      g_invoice_num := SUBSTR (g_invoice_num, 4);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      IF P_request_type   ='URL' THEN
        g_url            := NULL;
      elsif P_request_type='INVOICENUM' THEN
        g_invoice_num    := NULL;
      END IF;
    END;
    l_hash_index                         :=p_po_distribution_id||'-'||p_period_name;
    g_url_vldn_tbl(l_hash_index)         := g_url;
    G_invoice_num_VLDN_TBL(L_HASH_INDEX) := g_invoice_num;
  END;
  IF P_request_type='URL' THEN
    RETURN g_url ;
  elsif P_request_type='INVOICENUM' THEN
    RETURN g_invoice_num ;
  END IF;
EXCEPTION
WHEN OTHERS THEN
  IF P_request_type='URL' THEN
    g_url         := NULL;
    RETURN g_url ;
  elsif P_request_type='INVOICENUM' THEN
    g_invoice_num    := NULL;
    RETURN g_invoice_num ;
  END IF;
END get_invoice_url;


FUNCTION GET_VENDOR_OWNER_NUM(
    p_inventory_item_id NUMBER,
    p_request_type       VARCHAR2)
  RETURN VARCHAR2
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "PO Cost Change Analysis Report - WC"
  --//
  --// Object Name          :: GET_VENDOR_OWNER_NUM
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.2        Siva          10-Jan-2017        TMS#20161025-00027 
  --//============================================================================  

L_HASH_INDEX number;
l_sql varchar2(32000);
begin
g_vendor_number := null;
g_vendor_name  := null;
g_vendor_id    := null;

l_sql :='SELECT segment1,
                vendor_name,
                vendor_id
        FROM apps.po_vendors
        WHERE segment1 = (SELECT MAX(mcr.attribute1)
                            FROM apps.mtl_cross_references_b mcr
                            WHERE mcr.inventory_item_id= :1
                              AND cross_reference_type   =''VENDOR''
                           )';
    BEGIN
        L_HASH_INDEX:=p_inventory_item_id;
        if P_request_type='NUM' then
           g_vendor_number := g_vendor_num_vldn_tbl(L_HASH_INDEX);
        ELSif P_request_type='NAME' then
            g_vendor_name := g_vendor_name_vldn_tbl(l_hash_index);
        elsif p_request_type='VEND_ID' then
            g_vendor_id := g_vendor_id_vldn_tbl(L_HASH_INDEX);
        END IF;
 
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO g_vendor_number,g_vendor_name,g_vendor_id USING P_INVENTORY_ITEM_ID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
	if P_request_type='NUM' then
    g_vendor_number := null;
	elsif P_request_type='NAME' then
    g_vendor_name 	:= null;
  elsif p_request_type='VEND_ID' then
    g_vendor_id 	:= null;
	end if;
    end;
    
		l_hash_index:=p_inventory_item_id;
			    g_vendor_num_vldn_tbl(l_hash_index) 	  := g_vendor_number;
      		g_vendor_name_vldn_tbl(l_hash_index)  	:= g_vendor_name; 
          g_vendor_id_vldn_tbl(l_hash_index)  	  := g_vendor_id; 
          
    END;
     	if P_request_type='NUM' then
    return  g_vendor_number ;
	elsif P_request_type='NAME' then
    RETURN  g_vendor_name ;
   elsif p_request_type='VEND_ID' THEN
   RETURN  g_vendor_id ;
   end if;
    EXCEPTION when OTHERS then
  	if P_request_type='NUM' then
    g_vendor_number := null;
    return  g_vendor_number ;
	elsif P_request_type='NAME' then
    g_vendor_name 	:= null;
    RETURN  g_vendor_name ;
  elsif p_request_type='VEND_ID' then
    g_vendor_id 	:= NULL;
    RETURN  g_vendor_id ;
	end if;

END GET_VENDOR_OWNER_NUM;


FUNCTION GET_CUST_SO_ADOPTION(
    P_DATE_FROM DATE,
    P_DATE_TO DATE,
    P_BRANCH VARCHAR2,
    P_REGION   VARCHAR2,
    P_DISTRICT VARCHAR2
    )
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: GET_CUST_SO_ADOPTION
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  
L_HASH_INDEX varchar2(1000);
L_SQL varchar2(32000);
begin
G_SO_ADOPTION := null;


l_sql :='SELECT round((ROUND(SUM(DECODE(OH.ATTRIBUTE6,''CUSTOM_SALES_ORDER'',1,0))/COUNT(ORDER_NUMBER),4 )*100),0) so_adoption
FROM apps.oe_order_headers_all oh,
  apps.mtl_parameters mp,
  apps.oe_transaction_types_tl ot
WHERE oh.ship_from_org_id = mp.organization_id
AND oh.flow_status_code ! =''CANCELLED''
AND oh.order_type_id      = ot.transaction_type_id
AND ot.language(+)        = userenv(''LANG'')
AND ot.name !             = ''INTERNAL ORDER''
AND TRUNC(OH.CREATION_DATE) >= :1
AND TRUNC(OH.CREATION_DATE) <= :2
and mp.organization_code     = :3
and mp.attribute9 			     = :4
and MP.ATTRIBUTE8 			     = :5
 ';
    BEGIN
        l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_BRANCH||'-'||P_REGION||'-'||P_DISTRICT;
        g_so_adoption := G_SO_ADOPTION_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin
    EXECUTE IMMEDIATE L_SQL INTO g_so_adoption USING P_DATE_FROM,P_DATE_TO,P_BRANCH,P_REGION,P_DISTRICT;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_so_adoption :=0;
    WHEN OTHERS THEN
    g_so_adoption :=0;
    END;
                      l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_BRANCH||'-'||P_REGION||'-'||P_DISTRICT;
                       G_SO_ADOPTION_VLDN_TBL(L_HASH_INDEX) := g_so_adoption;
    END;
     return  g_so_adoption;
     EXCEPTION when OTHERS then
      g_so_adoption:=0;
      RETURN  g_so_adoption;

end GET_CUST_SO_ADOPTION;

function eis_get_date_from
RETURN date
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: EIS_GET_DATE_FROM
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================
begin
      RETURN G_DATE_FROM;
END eis_get_date_from;

FUNCTION eis_get_date_to
RETURN date
is
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: EIS_GET_DATE_TO
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  
begin
      return G_DATE_TO;
END EIS_GET_DATE_TO;


FUNCTION GET_DISTRICT_ADOPTION(
    P_DATE_FROM DATE,
    P_DATE_TO DATE,
    P_REGION   VARCHAR2,
    P_DISTRICT VARCHAR2
    )
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: GET_DISTRICT_ADOPTION
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================ 
L_HASH_INDEX varchar2(1000);
L_SQL varchar2(32000);
begin
G_dist_ADOPTION := null;


l_sql :='SELECT round((ROUND(SUM(DECODE(OH.ATTRIBUTE6,''CUSTOM_SALES_ORDER'',1,0))/COUNT(ORDER_NUMBER),4 )*100),0) so_adoption
FROM apps.oe_order_headers_all oh,
  apps.mtl_parameters mp,
  apps.oe_transaction_types_tl ot
WHERE oh.ship_from_org_id = mp.organization_id
AND oh.flow_status_code ! =''CANCELLED''
AND oh.order_type_id      = ot.transaction_type_id
AND ot.language(+)        = userenv(''LANG'')
AND ot.name !             = ''INTERNAL ORDER''
AND TRUNC(OH.CREATION_DATE) >= :1
AND TRUNC(OH.CREATION_DATE) <= :2
and mp.attribute9 			     = :3
and MP.ATTRIBUTE8 			     = :4
 ';
    BEGIN
        l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_REGION||'-'||P_DISTRICT;
        G_dist_ADOPTION := G_dist_ADOPTION_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin
    EXECUTE IMMEDIATE L_SQL INTO G_dist_ADOPTION USING P_DATE_FROM,P_DATE_TO,P_REGION,P_DISTRICT;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_dist_ADOPTION :=0;
    WHEN OTHERS THEN
    G_dist_ADOPTION :=0;
    END;
                      l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_REGION||'-'||P_DISTRICT;
                       G_dist_ADOPTION_VLDN_TBL(L_HASH_INDEX) := G_dist_ADOPTION;
    END;
     return  G_dist_ADOPTION;
     EXCEPTION when OTHERS then
      G_dist_ADOPTION:=0;
      RETURN  G_dist_ADOPTION;

end GET_DISTRICT_ADOPTION;

FUNCTION GET_REGION_ADOPTION(
    P_DATE_FROM DATE,
    P_DATE_TO DATE,
    P_REGION   VARCHAR2
    )
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: GET_REGION_ADOPTION
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  
L_HASH_INDEX varchar2(1000);
L_SQL varchar2(32000);
begin
G_region_ADOPTION := null;


l_sql :='SELECT round((ROUND(SUM(DECODE(OH.ATTRIBUTE6,''CUSTOM_SALES_ORDER'',1,0))/COUNT(ORDER_NUMBER),4 )*100),0) so_adoption
FROM apps.oe_order_headers_all oh,
  apps.mtl_parameters mp,
  apps.oe_transaction_types_tl ot
WHERE oh.ship_from_org_id = mp.organization_id
AND oh.flow_status_code ! =''CANCELLED''
AND oh.order_type_id      = ot.transaction_type_id
AND ot.language(+)        = userenv(''LANG'')
AND ot.name !             = ''INTERNAL ORDER''
AND TRUNC(OH.CREATION_DATE) >= :1
AND TRUNC(OH.CREATION_DATE) <= :2
and mp.attribute9 			     = :3
 ';
    BEGIN
        l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_REGION;
        G_region_ADOPTION := G_region_ADOPTION_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin
    EXECUTE IMMEDIATE L_SQL INTO G_region_ADOPTION USING P_DATE_FROM,P_DATE_TO,P_REGION;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_region_ADOPTION :=0;
    WHEN OTHERS THEN
    G_region_ADOPTION :=0;
    END;
                      l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_REGION;
                       G_region_ADOPTION_VLDN_TBL(L_HASH_INDEX) := G_region_ADOPTION;
    END;
     return  G_region_ADOPTION;
     EXCEPTION when OTHERS then
      G_region_ADOPTION:=0;
      RETURN  G_region_ADOPTION;

end GET_REGION_ADOPTION;

FUNCTION GET_ITEM_CATEGORIES(
    p_item_catalog_group_id NUMBER,
    p_request_type       VARCHAR2)
  RETURN VARCHAR2
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Extended Attributes Extract (ICC) - WC"
  --//
  --// Object Name          :: GET_ITEM_CATEGORIES
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.4		  Siva		  18-May-2017		 TMS#20170306-00240
  --//============================================================================  

L_HASH_INDEX number;
l_sql varchar2(32000);
begin
g_catmgt_category_desc := null;
g_catclass  		   := null;
g_category_class_desc  := null;

l_sql :='SELECT
  (SELECT t_cat.description 
  FROM apps.fnd_flex_values b_cat,
    APPS.FND_FLEX_VALUES_TL T_CAT,
    APPS.FND_FLEX_VALUE_SETS V_CAt
  WHERE b_cat.flex_value_id      = t_cat.flex_value_id
  AND b_cat.flex_value_set_id    = v_cat.flex_value_set_id
  AND v_cat.flex_value_set_name IN (''XXWC_CATMGT_CATEGORIES'')
  and B_CAT.FLEX_VALUE           = MC.ATTRIBUTE5
  and rownum=1
  ) catmgt_category_desc,
  MC.CONCATENATED_SEGMENTS CATCLASS,
  MCT.DESCRIPTION CATEGORY_CLASS_DESC
FROM
  mtl_system_items_b msi,
  mtl_item_categories mic,
  apps.mtl_categories_tl mct ,
  APPS.MTL_CATEGORIES_B_KFV MC
WHERE msi.inventory_item_id = mic.inventory_item_id
AND msi.organization_id     =mic.organization_id
AND mic.category_id         =mc.category_id
AND mc.category_id          = mct.category_id
AND mct.language            = userenv(''LANG'')
and MC.STRUCTURE_ID         = 101
AND msi.ITEM_CATALOG_GROUP_ID =:1
AND rownum                 = 1
';
    BEGIN
        L_HASH_INDEX:=p_item_catalog_group_id;
        if P_request_type='CATMANGDESC' then
           g_catmgt_category_desc := g_catmgt_vldn_tbl(L_HASH_INDEX);
        ELSif P_request_type='CATCLASS' then
            g_catclass := g_catclass_vldn_tbl(l_hash_index);
        elsif p_request_type='CATCLASSDESC' then
            g_category_class_desc := g_cat_class_desc_vldn_tbl(L_HASH_INDEX);
        END IF;
 
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO g_catmgt_category_desc,g_catclass,g_category_class_desc USING p_item_catalog_group_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
	if P_request_type='CATMANGDESC' then
    g_catmgt_category_desc := null;
	elsif P_request_type='CATCLASS' then
    g_catclass 	:= null;
  elsif p_request_type='CATCLASSDESC' then
    g_category_class_desc 	:= null;
	end if;
    end;
    
		l_hash_index:=p_item_catalog_group_id;
			    g_catmgt_vldn_tbl(l_hash_index) 	  := g_catmgt_category_desc;
      		g_catclass_vldn_tbl(l_hash_index)  	:= g_catclass; 
          g_cat_class_desc_vldn_tbl(l_hash_index)  	  := g_category_class_desc; 
          
    END;
     	if P_request_type='CATMANGDESC' then
    return  g_catmgt_category_desc ;
	elsif P_request_type='CATCLASS' then
    RETURN  g_catclass ;
   elsif p_request_type='CATCLASSDESC' THEN
   RETURN  g_category_class_desc ;
   end if;
    EXCEPTION when OTHERS then
  	if P_request_type='CATMANGDESC' then
    g_catmgt_category_desc := null;
    return  g_catmgt_category_desc ;
	elsif P_request_type='CATCLASS' then
    g_catclass 	:= null;
    RETURN  g_catclass ;
  elsif p_request_type='CATCLASSDESC' then
    g_category_class_desc 	:= NULL;
    RETURN  g_category_class_desc ;
	end if;

END GET_ITEM_CATEGORIES;


FUNCTION GET_ATTR_SPECIFICATIONS(
      P_Desc_Flex_Context_Code IN VARCHAR2,
      p_num NUMBER)
    RETURN VARCHAR2
  IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Extended Attributes Extract (ICC) - WC"
  --//
  --// Object Name          :: Get_Attr_Specifications
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.4		  Siva		  18-May-2017		 TMS#20170306-00240
  --//============================================================================ 
    l_sql               VARCHAR2 (4000);
    L_Attr_Display_Name VARCHAR2(240);
  BEGIN
    l_sql:='Select Form_Left_Prompt
          from
        (SELECT FORM_LEFT_PROMPT,          
                Row_Number() Over (Order By Form_Left_Prompt) As Row_Num      
          From apps.fnd_descr_flex_col_usage_vl Fdfct      
          Where Fdfct.Descriptive_Flex_Context_Code = :1        
                And Fdfct.enabled_flag=''Y'')    
        where Row_Num=:2';
    EXECUTE IMMEDIATE L_SQL INTO l_ATTR_DISPLAY_NAME USING P_Desc_Flex_Context_Code,p_num;
    RETURN l_ATTR_DISPLAY_NAME;
  EXCEPTION
  WHEN OTHERS THEN
    Return Null;
End;

FUNCTION get_qp_customer_info(
    p_list_header_id number,
    p_request_type varchar2)
  RETURN VARCHAR2
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Matrix Modifier Line Extract - WC"
  --//
  --// Object Name          :: get_working_days
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.5		  Siva		  	29-May-2017		 TMS#20170317-00082 
  --// 1.7	  	  Siva		    14-May-2018		 TMS#20180511-00218   
  --//============================================================================  
  l_hash_index          NUMBER;
  l_sql1                VARCHAR2(32000);
  l_sql2                VARCHAR2(32000);
  l_qualifier_attribute VARCHAR2(30);
BEGIN
  g_customer_num      := NULL;
  g_customer_name     := NULL;
  g_party_site_name   := NULL;
  g_party_site_number := NULL;
  g_salesrep_name     := NULL;
  g_salesrep_dm       := NULL;
 
 
  l_sql1 := 'SELECT HCS.ACCOUNT_NUMBER,  
NVL(HP.PARTY_NAME,HCS.ACCOUNT_NAME) CUSTOMER_NAME,  
NULL PARTY_SITE_NAME,  
NULL PARTY_SITE_NUMBER ,  
RASA.name salesrep_name,  
(SELECT ppf.full_name  
FROM per_all_assignments_f paaf,    
per_all_people_f ppf  
WHERE paaf.person_id       =RASA.person_id  
AND paaf.primary_flag (+)  =''Y''  
AND paaf.assignment_type(+)=''E''  
AND paaf.supervisor_id     =ppf.person_id  
AND TRUNC(sysdate) BETWEEN paaf.effective_start_date AND paaf.effective_end_date  
AND TRUNC(sysdate) BETWEEN ppf.effective_start_date AND ppf.effective_end_date  
and rownum=1  
)SALESREP_DM  
FROM QP_QUALIFIERS qq ,  
APPS.HZ_CUST_ACCOUNTS HCS,  
APPS.HZ_PARTIES HP,  
hz_cust_acct_sites hcuas,  
HZ_CUST_SITE_USES_ALL HCSU,  
APPS.RA_SALESREPS RASA
where qq.LIST_HEADER_ID     = :1 
AND QUALIFIER_CONTEXT        = ''CUSTOMER''
AND qualifier_attribute      =''QUALIFIER_ATTRIBUTE32''
AND qq.QUALIFIER_ATTR_VALUE = TO_CHAR (HCS.CUST_ACCOUNT_ID)
AND HCS.PARTY_ID             = HP.PARTY_ID(+)
AND hcs.cust_account_id      = hcuas.cust_account_id(+)
AND HCUAS.CUST_ACCT_SITE_ID  = HCSU.CUST_ACCT_SITE_ID(+)
AND HCSU.PRIMARY_SALESREP_ID = RASA.SALESREP_ID(+)
AND hcsu.site_use_code       = ''SHIP_TO''  --added for version 1.7
AND HCUAS.STATUS             = ''A''  --added for version 1.7
AND HCSU.STATUS              = ''A''  --added for version 1.7
and rownum                   =1
and TRUNC(sysdate) between TRUNC(NVL(QQ.START_DATE_ACTIVE, sysdate -1))  and TRUNC(NVL(QQ.END_DATE_ACTIVE, sysdate+ 1))
'
;

l_sql2 := 'SELECT HCS.ACCOUNT_NUMBER,  
NVL(HP.PARTY_NAME,HCS.ACCOUNT_NAME) CUSTOMER_NAME,  
HCSU.location party_site_name,  
hzps_ship_to.party_site_number ,  
RASA.name salesrep_name,  
(SELECT ppf.full_name  
FROM per_all_assignments_f paaf,    
per_all_people_f ppf  
WHERE paaf.person_id       =RASA.person_id  
AND paaf.primary_flag (+)  =''Y''  
AND paaf.assignment_type(+)=''E''  
AND paaf.supervisor_id     =ppf.person_id  
AND TRUNC(sysdate) BETWEEN paaf.effective_start_date AND paaf.effective_end_date  
AND TRUNC(sysdate) BETWEEN ppf.effective_start_date AND ppf.effective_end_date  
AND rownum=1  
)SALESREP_DM
FROM QP_QUALIFIERS qq ,  
HZ_CUST_SITE_USES_ALL HCSU,  
HZ_CUST_ACCT_SITES HCUAS,  
APPS.HZ_CUST_ACCOUNTS HCS,  
APPS.HZ_PARTIES HP,  
apps.hz_party_sites hzps_ship_to,  
APPS.RA_SALESREPS RASA
WHERE qq.LIST_HEADER_ID     = :1
AND QUALIFIER_CONTEXT        =''CUSTOMER''
AND qualifier_attribute      =''QUALIFIER_ATTRIBUTE11''
AND qq.QUALIFIER_ATTR_VALUE = TO_CHAR (hcsu.site_use_id)
AND HCSU.CUST_ACCT_SITE_ID   = HCUAS.CUST_ACCT_SITE_ID
AND hcuas.cust_account_id    = hcs.cust_account_id(+)
AND HCS.PARTY_ID             = HP.PARTY_ID(+)
AND HCUAS.party_site_id      = hzps_ship_to.party_site_id(+)
AND HCSU.PRIMARY_SALESREP_ID = RASA.SALESREP_ID(+)
AND hcsu.site_use_code       = ''SHIP_TO''
AND HCUAS.STATUS             = ''A''
AND HCSU.STATUS              = ''A''
and rownum                   =1
AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qq.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qq.end_date_active, SYSDATE+ 1))
'
;

  begin
    l_hash_index          := p_list_header_id;
    IF p_request_type      = 'CUST_NUM' THEN
      g_customer_num      := g_customer_num_vldn_tbl(l_hash_index);
    elsif p_request_type   = 'CUST_NAME' THEN
      g_customer_name     := g_customer_name_vldn_tbl(l_hash_index);
    elsif p_request_type   = 'SITE_NAME' THEN
      g_party_site_name   := g_party_site_name_vldn_tbl(l_hash_index);
    elsif p_request_type   = 'SITE_NUM' THEN
      g_party_site_number := g_party_site_num_vldn_tbl(l_hash_index);
    elsif p_request_type   = 'SALESREP' THEN
      g_salesrep_name     := g_salesrep_name_vldn_tbl(l_hash_index);
    elsif p_request_type   = 'SALESREP_DM' THEN
      g_salesrep_dm       := g_salesrep_dm_vldn_tbl(l_hash_index);
    END IF;
  EXCEPTION
  WHEN no_data_found THEN
    begin
    
      SELECT qpq.qualifier_attribute
      INTO l_qualifier_attribute
      FROM qp_qualifiers qpq
      WHERE qpq.qualifier_context ='CUSTOMER'
      AND qpq.list_header_id      =p_list_header_id
      and rownum                  =1;
      
      if l_qualifier_attribute    ='QUALIFIER_ATTRIBUTE32' then
        EXECUTE immediate l_sql1 INTO g_customer_num,g_customer_name,g_party_site_name,g_party_site_number,g_salesrep_name,g_salesrep_dm USING p_list_header_id;
      else
        EXECUTE immediate l_sql2 INTO g_customer_num,g_customer_name,g_party_site_name,g_party_site_number,g_salesrep_name,g_salesrep_dm USING p_list_header_id;
      end if;
      
    EXCEPTION
    WHEN no_data_found THEN
      IF p_request_type      ='CUST_NUM' THEN
        g_customer_num      := NULL;
      elsif p_request_type   ='CUST_NAME' THEN
        g_customer_name     := NULL;
      elsif p_request_type   ='SITE_NAME' THEN
        g_party_site_name   := NULL;
      elsif p_request_type   ='SITE_NUM' THEN
        g_party_site_number := NULL;
      elsif p_request_type   ='SALESREP' THEN
        g_salesrep_name     := NULL;
      elsif p_request_type   ='SALESREP_DM' THEN
        g_salesrep_dm       := NULL;
      end if;
  end;    
      l_hash_index                            :=p_list_header_id;
      g_customer_num_vldn_tbl(l_hash_index)   := g_customer_num ;
      g_customer_name_vldn_tbl(l_hash_index)  := g_customer_name ;
      g_party_site_name_vldn_tbl(l_hash_index):= g_party_site_name ;
      g_party_site_num_vldn_tbl(l_hash_index) := g_party_site_number ;
      g_salesrep_name_vldn_tbl(l_hash_index)  := g_salesrep_name;
      g_salesrep_dm_vldn_tbl(l_hash_index)    := g_salesrep_dm;
    end;
 IF p_request_type='CUST_NUM' THEN
      RETURN g_customer_num ;
    elsif p_request_type='CUST_NAME' THEN
      RETURN g_customer_name ;
    elsif p_request_type='SITE_NAME' THEN
      RETURN g_party_site_name ;
    elsif p_request_type='SITE_NUM' THEN
      RETURN g_party_site_number ;
    elsif p_request_type='SALESREP' THEN
      RETURN g_salesrep_name ;
    elsif p_request_type='SALESREP_DM' THEN
      RETURN g_salesrep_dm ;
    end if;
    
  EXCEPTION
  WHEN OTHERS THEN
    IF p_request_type ='CUST_NUM' THEN
      g_customer_num := NULL;
      RETURN g_customer_num ;
    elsif p_request_type='CUST_NAME' THEN
      g_customer_name  := NULL;
      RETURN g_customer_name ;
    elsif p_request_type ='SITE_NAME' THEN
      g_party_site_name := NULL;
      RETURN g_party_site_name ;
    elsif p_request_type   ='SITE_NUM' THEN
      g_party_site_number := NULL;
      RETURN g_party_site_number ;
    elsif p_request_type='SALESREP' THEN
      g_salesrep_name  := NULL;
      RETURN g_salesrep_name ;
    elsif p_request_type='SALESREP_DM' then
      g_salesrep_dm    := null;
      return g_salesrep_dm ;
    end if;    
   
end get_qp_customer_info;

FUNCTION get_working_days(
    p_start_date IN DATE,
    p_end_date IN DATE)
  RETURN NUMBER
as  
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Days to Fulfill Report - WC"
  --//
  --// Object Name          :: get_working_days
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.5		  Siva		  	28-Mar-2017		 TMS#20170220-00206
  --//============================================================================   
L_HASH_INDEX varchar2(1000);
l_start_date date;
begin
g_working_days := 0;

    BEGIN
        l_hash_index:=p_start_date||'-'||p_end_date;
        g_working_days := G_DIST_WORK_DAYS_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin
	   l_start_date:=to_date(p_start_date,'DD-MON-YY');
	  while l_start_date <>TO_DATE(p_end_date,'DD-MON-YY')
		LOOP
         IF trim(TO_CHAR(l_start_date,'DAY'))='SATURDAY' OR trim(TO_CHAR(l_start_date,'DAY'))='SUNDAY' THEN
            null;
           ELSE
              g_working_days:=g_working_days+1;
           END IF;
           l_start_date:=l_start_date+1;
      END LOOP;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_working_days :=0;
    WHEN OTHERS THEN
    g_working_days :=0;
    END;
                      l_hash_index:=p_start_date||'-'||p_end_date;
                       G_DIST_WORK_DAYS_VLDN_TBL(L_HASH_INDEX) := g_working_days;
    END;
     return  g_working_days;
     EXCEPTION when OTHERS then
      g_working_days:=0;
      RETURN  g_working_days;
END get_working_days;

END EIS_XXWC_INV_UTIL_PKG;
/
