CREATE OR REPLACE PACKAGE BODY XXEIS.eis_rs_process_reports
IS
--
-- Purpose: This package contains procedures for processing reports for EIS e-Business Reporting.
--
-- $Header: eis_rs_process_reports.pkb 6.07 2009/06/2 16:57:29 $

-- MODIFICATION HISTORY
-- Person      Date          Version        Comments
-- ---------   ------        --------       ------------------------------------------
-- ASR         20-Feb-2004   1.0            Created the pkg
-- KP          01-May-2004   1.0        Backported to 8.1.7
--                                          Modified logic to write to CLOB. Now it writes
--                                          to PL/SQL table g_rep_output_tbl and then
--                                          to CLOB
-- ASR         03-May-2004   1.0            Modified the logic for writing clob file.  Now all clob_files will go into clobs table.
-- ASR         03-May-2004   1.0            Added the procedure get_clobs for getting clob files associated with a specific process id.
-- ASR         06-May-2004   1.0            Modified procedure submit_value_added_reports to fix a bug in pivot GTN
-- ASR         08-May-2004   1.0            Modified procedure submit_report and changed the logic of writing data into pl/sql table before inserting into clob file
-- ASR         13-May-2004   1.0            Removed submit values added report procedure...it has been moved to a new pkg called element_classifications
--                                          Fixed bug 85 - Duplicate Rows in the report output. . It is always having one chunk of data extra.
--                                          Fixed issue 86 - Changed user_id to user_name in header portion
--                                          Modified the procedures submit_report, submit_process, submit_conc_report to change the
--                                          logic of creating processes to resolve issues (5, 9) and to make more effective
--                                          Changed code for considering number of line per clob and no. of lines for data file
-- ASR         18-May-2004   1.1            Modified code to update process status as error in case of request creation fails
-- ASR         18-May-2004   1.1            Modified code to update status of errored processes only to 'E'...by ignoring teriminated processes.
-- KP          17-Jun-2004   1.4            Modified code to replace " in the columns which are used in drill_down_url ( Bug 130 )
-- KP          17-Jun-2004   1.4            Modified code to consideter global parameter drill_down_allowed Bug 136
-- KP          20-Jun-2004   1.4.3          Modifed Get_url function to add information to drill_down_url
-- RP          14-AUG-2004                  Added new functionality for Schedule request
-- BNR         19-OCT-2004   1.4.5          Added encrypt to the Drill_down_url
-- BNR         01-Nov-2004   1.4.6          Added report summary, created a procedure ins_or_inc_summary_count
-- BNR         04-Nov-2004   1.4.6          Added a p_report_type parameter in submit_process and submit_report procedures
-- KP          27-Nov-2004   1.4.6          added function rep_special_chars, use 'NULL' if summary field is null
-- BNR         05-Dec-2004   1.4.6.1        Added replace function, to replace (COUNT) to (SUM).
-- KP          17-Dec-2004   1.4.6          Commented IF INSTR(conditions_rec.value1, ':') in submit_report as IN clause with quotes is dropping quotes
-- KP          17-Dec-2004   1.4.7          Modified code for Trigger Execution
-- KP          19-Dec-2004   1.4.7          commented rep_special_chars procedure for 8.1.7
-- KP          20-Dec-2004   1.4.7          Added code to maintain sigle '(' if there are two '(' if conditions has a '('
-- Ankur       01-Jan-2005   1.4.7          Added Code for report Scheduling, Notification , opening URL from reports.
--                                          for this purpose added get_display_order ,increment_date ,user_authorization procedures.
-- Ankur       03-Jan-2005   1.4.7          Changed submit_conc_report procedure as well concurrent program , 2 new parameters p_notify and p_server_url is added.
--                                          Changed submit_process procedure to enable scheduling.
-- jyothi      03-Jan-2005   1.4.7          Modified code to intialize varible l_param_value to null in loop in submit_report procedure.
-- KP          05-Jan-2004   1.4.7          added code to use view_owner column of eis_rs_views
-- Ankur       08-Jan-2005   1.4.7          Added code for showing Scheduled Requests Page in frontend.
--                                          Added show_schedule_requests,terminate_schedule_requests,get_report_name , get_paramameters_Details for this purpose.
-- KP          09-Jan-2005   1.4.7          Initialied Parameters table and made changes to call same code both for scheduling and direct submit reports
-- KP          09-Jan-2005   1.4.7          Added code for :SYSTEM.PROCESS_ID
-- BNR         11-Apr-2005   1.4.7          Added get_filenames to fix max cursors problem
-- jyothi      28-Apr-2005   2.0.4          added function as_of_date
-- jyothi      09-May-2005   2.0.4          Modified get_clobs function,by adding request_id in order by clause
-- jyothi      02-Jun-2005   2.0.4          Modified to increase length of variable l_result_str
-- Ankur       07-Jun-2005   2.0.4          added Function get_column_data_type
-- Ankur       07-Jun-2005   2.0.4          Modified submit_report to Format Date columns with 'DD-Mon-YYYY'
-- Ankur       10-Jun-2005   2.0.4          Modified get_url to take protocol from Table.
-- Ankur       16-jun-2005   2.0.4          Modified submit_report to add Distinct Clause for report
-- Ankur       26-Jul-2005   3.0.0          Modified get_schedule_request_count and show_schedule_requests procedure to get only EiS pending reports.
-- BNR         10-Aug-2005   3.0.0          Modified get_schedule_request_count procedure
-- BNR         11-Aug-2005   3.0.0          Modified submit_report
-- Raju        25-Jul-2005                  Added procedure get_out_put_columns
-- Raju        25-Jul-2005                  Added html details sorting functionality
-- Raju        02-Aug-2005                  Added html summary(Pivoting) functionality
-- Rakesh      06-Oct-2005  after 3.0.0     added procedure insert_html_summary_data to insert data for HTML Summary output
-- KP          28-Nov-2005  4.0.0           Modified parse_delimited_Data to take care of null values and single quotes
-- KP          28-Nov-2005  4.0.0           Modified insert_html_summary_data to comment the g_log append in the exception as it is causing problem with the variable size.
-- Pramod      28-Nov-2005  4.0.0           Modified the  g_log datatype to increase 32000 charaters
-- Nagraj      29-Nov-2005  4.0.0           Added function get_view_datatype to get view column data type
-- RL           01-Dec-2005  4.0.0        modified procedure get_segment_prompt
-- Nagraj      07-Dec-2005  4.0.0           Modified submit_process procedure remove summary report code
-- Nagraj      07-Dec-2005  4.0.0        Modified submit_process procedure to fix 14th parameter issue, view sql builds where condition without value.
-- KP          19-Dec-2005  4.0.0           Modified submit_report to fix the order of row fields in the html summary. It is coming in reverse order
-- KP          19-Dec-2005  4.0.0           Modified parse_output_data to fix the ','
-- KP          22-Dec-2005  4.0.0           Modified submit_report, get_segment_prompt to add GL Segments to Pivot, HTML Summary
-- KP          23-Dec-2005  4.0.0           Modified Submit Report to be able to send multiple parameters for trigger based report Bug 140
-- Saritha     31-Dec-2005  4.0.0           Modified submit report procedure to allow dynamic account segment generation for all the Financial reports that has segment prompt flag enabled
-- Nagraj      16-Jan-2006  4.0.0           Modified get_url procedure to fix Employee Drill Down
-- pramod      16-feb-2006  4.0.2           modified to fix the problem PLS-00103: Encountered the symbol ","  16-feb-2006
-- KP          18-Feb-2006  4.0.3           Modified parse_output_data to add hyperlink for new html format.
-- Jyothi      13-Jun-2006  5.0.0           Modified Report Parameters, while concatenatiing by replacing ',' with '-'. This change is made to display mulitple parameter names in Excel sheet.
-- Gattu       17-Jul-2006  5.0.0           Added xml publisher changes
-- Gattu       20-JUL-2006  5.0.0           Fixed l_xml_procedure_name issue
-- Gattu       24-JUL-2006  5.0.0           Fixed Value added reports issue
-- Gattu       09-AUG-2006  5.0.0           Fixed trigger based report parameter issue
-- Gattu       16-AUG-2006  5.0.0           Modified User Group logic in submit_conc_report procedure
-- Gattu       24-AUG-2006  5.0.0           Added new table design changes
-- Gattu       28-AUG-2006  5.0.0           Fixed Mastec issue
-- gattu       01-Sep-2006  5.0.0           Added eis_rs_common_outputs delete
-- Gattu       21-Sep-2006  5.0.0           Removed eis_rs_processes from delete_report
-- Gattu       06-Oct-2006  5.0.0           Added new line characters
-- Gattu       Oct-19-2006  5.0.0/5.0.1     Added HR Cross Business Group
-- Gattu       Oct-30-2006  5.0.0/5.0.1     Increased the data type size for  l_row_fields,l_col_fields,l_page_fields and l_data_fields
-- Gattu       Nov-09-2006  5.0.0/5.0.1     Modified HR Cross Business Group condition
-- Gattu       Nov-21-2006  5.0.0/5.0.1     Fixed UTPA financial reports special characters issue
-- Gattu       Mar-27-2006  6.0.0           Fixed delete_report issue
-- Gattu       APR-24-2007  6.0.0           Fixed User group notification Issue
-- Gattu       MAY-15-2007  6.0.0           Modified data type logic in get_view_datatype
-- Gattu       JUL-09-2007  6.0.0           Added output type conditions to decide the data insert into html tables
-- Nelli       SEP-06-2007  6.0.1           Added condition to restrict the data insert into html/clob tables
-- KP          SEP-16-2007  6.0.2           Added code for field mask

-- MODIFICATION HISTORY
-- Person   Version  Header#  Bug#    Issue#  Date       Comments
-- ------   ------   -------  ------  ------  ---------  ---------
-- Sunil    6.0.2    6.0.2                    18-Dec-07  Modified for Version Controlling.
--                                                       The new modification history is to keep track of
--                                                       all the fixes that got fixed by mentioning
--                                                       respective bug No's of Bugzilla and Issue No's of
--                                                       Salesforce and the version number in which the
--                                                       fix as went.
--
-- Rajani  6.3.0     6.3.0    578             31-Jan-08  Modified submit_conc_report, user_authorization
--                                                       procedures to fix distribution issues.
-- Rajani  6.3.0     6.3.0                    06-Feb-08  Modified submit_conc_report procedure
--                                                       to fix the LFU issue (concurrent request failing
--                                                       when request is submitted with the User Group)
-- Ashwin  6.3.0     6.04                     17-Feb-08  Added procedure copy_report_parameters ( GL Segment Expansion Enhancements )
-- Ashwin  6.3.0     6.04                     17-Feb-08  1. Modified submit_process to update tables EIS_RS_PROCESS_RPT_PARAMETERS,EIS_RS_PROCESS_RPT_CONDITIONS,EIS_RS_PROCESS_RPT_SORTS
--                                                            with process_id.
-- Rajani  6.3.0                              26-Feb-08  Modified get_url function to fix drill down issues
-- Ashwin  6.3.0     6.05                     03-Apr-08  Modified submit_conc_report to handle population of copy tables while report is scehduled...
-- KP      6.3.0     6.05                     04-Apr-08  commented the fnd_file.log in check_if_masked
-- KP      6.3.0     6.05                     04-apr-08  used per_all_people_f in get_user_emaill_id_c
-- Rajani  6.3.0     6.05                     05-apr-08  Modified submit_conc_report() to use per_all_people_f in sql_stmt
-- Rajani  6.3.0     6.05                     09-Apr-08  Modified submit_report() to fix trigger text issue
-- Rajani  6.3.0     6.05                     09-Apr-08  Modified delete_report() to fix delete report issue
-- Rajani  6.3.0     6.05                     09-Apr-08  Added commit in copy_report_parameters()to fix portal feature issue
-- Rajani  6.3.0     6.05                     18-Apr-08  Modified copy_report_parameter() to fix the bug#754
-- Ashwin  6.3.0     6.06                  21-Apr-08  Modified function get_view_datatype to fix the bug#750
-- Rajani  6.3.0     6.06              24-Apr-08  Modified submit_report() to fix Field Masking issue
-- Rajani  6.3.0     6.06                     07-May-08  Modified user_authorization(), submit_conc_report() to fix the bug# 576
-- Rajani  6.3.0     6.06                     07-May-08  Modified delete_report() to fix the bug# 755
-- Sunil   6.3.0                              08-May-08  Added gants to package with grant option
-- Sunil   6.3.0                              14-May-08  Added Schema.(xxeis.) in call to eis_rs_column_masks table
-- Kiran   6.3.0     6.07      785      2491  20-May-08  Added code to show the data for global security profiles
-- Rajani  6.3.0               537            13-Jun-08  Modified submit_report() local variable lengths to fix bug# 537
-- Rajani  6.3.0               812            18-Jun-08  Modified submit_report() to fix parameter value has a colon(:) issue
-- KP      6.3.0               567            25-Jul-08  Modified submit_process() to fix interactive report submission issue
-- KP      6.3.0               854            26-Jul-08  Modified submit_process() to copy parameters if p_col_process_id is null ( called from processor )
-- KP      6.3.0                              11-aug-08  added insert_html_table for bulk insert
-- Rajani  6.3.0                              11-Aug-08  Modified submit_process() to fix scheduling issue with user date format.
-- Rajani  6.3.0                              11-Aug-08  Modified parse_delimited_data() to fix apostrophe issue.
-- Rajani  6.3.1                        2765  14-Aug-08  Modified delete_report() procedure
-- Rajani  6.3.1            2741  20-Aug-08  Modified parse_delimited_data() to fix apostrophe in employee names.
-- Rajani  6.3.1              1003      2874  24-Sep-08  Increased submit_report() local variables size to support calcuation column upto 4000 characters
-- Rajani  6.3.1              1022      3033  04-Nov-08  Modified submit_report() to fix sorting issue when
--                                                         Distinct Clause under Columns tab is checked.
--Saritha  6.3.1                                20-Nov-08 Modified the submit Report procedure to add GL Security Rules implementation Logic
--Saritha  6.3.1                                20-Nov-08 Modified the submit Report procedure to add the GL Segment parameters conditions logic as parent/child segment parameter values
--                                                        submitted in the report.
-- KP       6.3.2                               28-Nov-08 Modified submit_report to use components
-- KP       6.3.2                               28-Dec-08 added call to eis_rs_pre_process_reports.create_process_cond_comp_joins before conditions_c
-- KP       6.3.2.                              10-Jan-08 modified submit_process to update eis_rs_processes with parameter1-30 with exact parameter values
-- Rajani   6.3.2                               19-Jan-09 Modified get_process_column_data_type() and get_view_datatype() to fix report errors when CHAR columns exists in report.
-- Sjampala 6.3.2                               27-Jan-09 made changes to the login where conditions are created for the GL Segment parameters.
-- Rajani   6.3.2                          3479 10-Mar-09 Modified submit_report() to fix DFF issue
-- Rajani   6.3.2                               20-May-09 Backported changes for 632.6 release
-- KP       6.3.2                                2-Jun-09 Added orig_column_name and passed to check_if_masked and get_process_column_data_type to fix the masking issue
-- Rajani   6.3.2                               11-Jun-09 Commented set_location call in insert_html_table to avoid showing whole output data in concurrent log window
-- Nelli    6.3.2             1111        31-Jul-09 Weekly scheduling issue, added code to convert weekly schedule to 7 Day schedule - same as Oracle concurrent request scheduling
-- Rajani   6.3.2.8                             03-Sep-09 Modified get_parameters_details() to fix parameters missing issue in tools -> scheduling screen
-- Rajani   6.3.2.8                             04-Sep-09 Modified submit_process to fix duplicate column names
-- Ramana   6.3.2.8                             30-Sep-09 Modified submit_report to centralize apps initialization code for 11i and R12
-- Rajani   6.3.2.8           2436              11-Nov-09 Modified submit_report to fix sort clause ordering issue
-- Rajani    6.3.2.8                            30-Oct-09 Modified submit_report for range/include/exclude conditions
-- Sravani   6.3.2.8          2448              13-Nov-09 Modified submit_report for ^^^^ issue
-- Soujanya  6.3.2.8                            08-Dec-09 Modified to add Report plugin modifications and dynamic view generation on html tables
-- Rajani    6.3.2.8                            10-Jan-10 Modified user_authorization() to fix apostrophe issue in employee name
-- Rajani    6.3.2.8                            13-Jan-10 Modified submit_conc_report() to fix request ID missing issue in Requests screen
-- Praveen.P 6.3.2.9        3116        27-Jan-10 Modified submit_report(), incresed size of l_tmp_cond_column variable to fix buffer too samll error while submitting a report
-- Soujanya  6.3.2.9        3024        28-Jan-10 Modified submit_report() added null in exception block while executing dynamic view()l_dyn_stmt generation.
-- Praveen.P 6.3.2.9        3187        18-Feb-10 Modified submit_report(), to set information in Log file regarding Publish
-- Sravani.M  6.3.2.8       3314                22-Feb-10 Modified insert_html_table to fix bug# 3314
-- Madhavi.S  6.3.2.8       3311                23-Feb-10 Modified submit_process procedure to fix the bug 3311
-- Praveen.P 6.3.2.9        3187        12-Mar-10 Modified submit_report(), to set information in Log file regarding Publish
-- Praveen.P 6.3.2.9        3191        12-Mar-10 Modified submit_process(),as Scheduled reports are unable to publish.
-- Rajani    6.3.2.9        3489             15-Mar-10 Modified user_authorization() to fix distribution issue
-- Praveen.P 6.3.2.9        3187        17-Mar-10 Modified submit_report(), to set information in Log file regarding Publish
-- Praveen.P 6.3.2.9        3533        18-Mar-10 to fix the issue when more report parameters are selected
-- Praveen.P 6.3.2.9        2304        25-Mar-10 updated submit_report() procedure to fix the issue when columns are added from components and values are assigned to them
-- Praveen.P 6.3.2.9        2304        26-Mar-10 updated submit_report() procedure to fix the issue when columns are added from components and values are assigned to them whie submitting the report
-- Madhavi.S 6.3.2.9        3571        31-Mar-10 Modified submit_report() procedure to fix the issue while submitting the procedure based template
-- Saritha   6.3.2.9        3652        08-Apr-10 Modified submit_report() procedure to fix character string too small error in payables module
-- Praveen.P 6.3.2.9        3474        09-Apr-10 updated submit_report() procedure to fix html output issue in portals
-- Ramana    7.0.0          3667        15-Apr-10 Added logic to replace ~ with space by default if column is varchar
-- Praveen.P 6.3.2.9        3533        18-Mar-10 To fix the issue when more report parameters are selected(Long value error)
-- Praveen.P 6.3.2.9        3533        22-Apr-10 To fix the issue when more report parameters are selected(can bind a LONG value only)
-- Sravani.M 6.3.2.9        3971                12-May-10 To fix the issue when we publish any report that report is unable to delete
-- Rajani    7.0.0          4072                01-Jun-10 Modified to fix the Sort with Add Distinct Clause issue
-- Rajani    7.0.0          4128                08-Jun-10 Modified to fix upper(user_name) reference in user_authorization() procedure.
-- Soujanya  7.0.0          4209        24-Jun-10 Below dynamic view generation is commented
-- Sravani.M 7.0.0          4259                20-Jul-10 Modified submit_report procedure to fix calculation column sort issue
-- Sravani.M 7.0.0          4477                26-Aug-10 Modified insert_html_table procedure to fix bug# 4477
-- Praveen.P 7.0.0          4532        27-Sep-10 Updated submit_report() to fix the issue HTML output is blank when there are more than 50 varchar columns in a report
-- Praveen.P 7.0.0          4532        27-Sep-10 Updated get_output_columns() to fix the issue HTML output is blank when there are more than 50 varchar columns in a report
-- Praveen.P 7.0.0          4532        27-Sep-10 Updated get_process_output_columns() to fix the issue HTML output is blank when there are more than 50 varchar columns in a report
-- Praveen.P 7.0.0          3428        01-Oct-10 Updated submit_report() to fix Responsibility name issue in Excel output type
-- Praveen.P 7.0.0          4672                14-Oct-10 Updated submit_report() cursor parameters_c() to fetch the parameters in display_order
-- Praveen.P 7.0.0          4758                14-Oct-10 Updated insert_html_table() to fix character string buffer too small  while submitting the reports
-- Rajani    7.0.0          4639                21-Oct-10 Modified submit_report() to update status after "after report trigger" is executed
-- Soujanya  7.0.0        4774                21-Oct-10 addd group by flag
-- Sravani.M 7.0.0          4984 ,4966          14-Dec-10 Modifed to fix buG# 4984
-- Rajani    7.0.0          2734                20-Dec-10 Modifed submit_report() to use eis_rs_oaf_api.get_responsibility_context()
-- Praveen.P 7.0.0          1338,               27-Sep-10 Created new procedures convert_char_to_date, set_date_params for bug#1338 for supporting
--                          1339                          canonical format and ability to submit in any date format
-- Praveen.P 7.0.0          1270                27-Sep-10 Added orig_column_name and passed to check_if_masked and get_process_column_data_type to fix the masking issue
-- Ramana    7.0.0                              24-Dec-10 Modified the submit_report procedure for multi-sob and multi-org changes.
-- Sravani   7.0.0          4994                27-Dec-10 Modified submit_report to fix group by enhancement
-- Ramana    7.0.0          4805                28-Dec-10 Added the changes required for financial publishing enhancement in html table.
-- Praveen.P 7.0.0          1338                21-Dec-10 Modified convert_char_to_date, set_date_params, process_reports
-- Soujanya  7.0.0          1338                03-Jan-11 Modified set_date_params and removed admin parameter to use ICX date format as it is not needed and modified submit_process to remove
--                              alter session dd-mon-rrrr if submission type is interactive
-- KP        7.0.0          1338                4-Jan-2011 View SQL is coming blank as process_id is passed blank
-- Sravani   7.0.0          5098                5-Jan-2011 commented convert_char_to_date() and changed references
-- Madhavi.S 7.0.0          5185                05-Jan-2011 Modified to insert submission_source column in submit_process procedure
-- Rajani    7.0.0                              05-Jan-2011 Replaced xxeis.eis_rs_pre_process_reports.get_parameter_list with xxeis.eis_rs_utility.get_process_param_values in submit_conc_report()
-- KP        7.0.0                              06-Jan-2011 modified submit_conc_process to take care of parameters larger than 240 characters. search for 240 to find the code changes
-- KP        7.0.0                              06-Jan-2011 modified submit_conc_process, increment_dates to take care of increment_date parameters for cononical date format and should work for multi language.
-- Praveen.P 7.0.0                        10-Jan-2011 Modified Submit_Report ,to fix extra condition bindings even when report parameters were disabled
-- Praveen.P 7.0.0                        10-Jan-2011 Added l_data_formats for header ,to support multi language date formats in plugin code
-- Praveen.P 7.0.0                        13-Jan-2011 Updated HTML_PIVOT_STRING to store advanced format details as well
-- Rajani    7.0.0                            24-Jan-2011 Removed the commented convert_char_to_date()
-- Rajani    7.0.0          5231               27-Jan-2011 Modified to use g_date_fomrat instead of ICX_DATE_FORMAT profile value
-- Sravani.M 7.0.0          5098                28-Jan-2011 Modified to use g_date_format in user_authorisation
-- Praveen.P 7.0.0          4994                17-Feb-2011 Updated submit_report(), to fix the issue in Group_by
-- Sravani.M 7.0.0          5098                04-Mar-2011 Updated submit_process(),to fix the date format issue in csv header
-- Rajani    7.0.0          5334                07-Mar-2011 Modified submit_report() to fix numeric or value error when report has more columns
-- Praveen.P 7.0.0          5334                14-Feb-2011 Modified Submit_Report() to fix the issue of mismatch in column_names and respective data_types in CSV output
-- Rajani    7.0.0                07-Mar-2011 Modified to fix payroll processor report submission with date format enhancement
-- Rajani    7.0.0                28-Mar-2011 Modified to save dates in report data in canonical format in eis_rs_clobs and to display date parameters in CSV header in canonical format
-- Rajani    7.0.0        5441        30-Mar-2011 Modified to fix scheduled requests are failing issue when date format or admin preference is changed
-- Sravani.M 7.0.0          5438                01-Apr-2011 Modified to fix new line character issue in html tables
-- Rajani    7.0.0          5455                01-Apr-2011 Modified to fix parameterized column issue
-- Rajani    7.0.0                              08-Apr-2011 Modified to hardcode CSV header date parameter values to DD-MON-YYYY format for XL Connect refresh issue
-- Rajani    7.0.0                              14-Apr-2011 Modified the query to get the information needed in CSV header
-- Madhavi   7.0.0          2003                20-Apr-2011 Modified to fix the bug 2003
-- Rajani    7.0.0                              21-Apr-2011 Commented hr_utility.set_location() in set_location() procedure
-- Rajani    7.0.0                              02-May-2011 Modified to add distribution enhancement logic implemented in 70050_BE patch
-- Soujanya  7.0.0                              09-May-2011 Modified submit_conc_report to add temporary virtual table as using g_param_tbl always is causing some problem in referencing correct data
-- Rajani    7.0.0          5622                17-May-2011 Modified submit_report() to support number format in Excel output
-- Praveen.P 7.0.0          5621                18-May-2011 Modified to fix scheduling issue when it is scheduled once
-- Soujanya  7.0.0                              19-May-2011 Modified submit_conc_report to modify l_parse_tbl_def value from 100 to 10000 to fix issue when parameter values of length more than 100 is given then report fails
-- Rajani    7.0.0          5628        19-May-2011 Modified submit_report() for sort order
-- Madhavi.S 7.0.1          5784                28-Jun-2011 Modified delete_report() procedure to delete from eis_rs_report_distribution
-- Satya     7.0.1          5857                11-Jul-2011 Modified submit_report() procedure to fix bug #5857
-- Sravani.M  7.0.1         5864                12-Jul-2011 Modified set_date_params() procedure to fix bug# 5864
-- Rajani    7.0.1          5142                    12-Jul-2011 Modified submit_conc_report() to avoid End At as mandatory field in scheduling screen.
-- Rajani    7.0.1                                   12-Jul-2011 Modified submit_report() to fetch output types from eis_rs_process_rpts table rather than eis_rs_reports.
-- Rajani    7.0.1          6184                     18-Aug-2011 Modified submit_report() to fix column data types when selected from components
-- Rajani    7.0.1          6184                     01-Nov-2011 Modified submit_report() to fix NUMBER data type calculation column when Group By is checked.
-- Soujanya  7.0.1          6184                     02-Nov-2011 Modified submit_report() to fix group by statement where all columns should come in order
-- Rajani    7.0.1                                   28-Nov-2011 Modified submit_report() to include security group information in CSV header
-- Rajani    7.0.1          6884                01-Feb-2012 Modified submit_report() to get distinct flag from process rpt table
-- Rajani    7.0.1          6940                16-Feb-2012 Modified submit_report() to fix applying group functions on calculation columns when group by is checked issue
-- Harsha    7.0.2          20130920-00347      20-12-2013 Added table to for Performance Tuning for EiS OpenSalesOrderReport
-- Rakesh    7.1.1          20160509-00068      09-May-2016 Reduce parallelism on EIS Reports to 4

TYPE v4000_type IS TABLE OF VARCHAR2(10000)
      INDEX BY BINARY_INTEGER;

TYPE genTabType IS TABLE OF VARCHAR2(256) INDEX BY BINARY_INTEGER;
dtab            genTabType;
ctab            genTabType;

g_rep_output_tbl v4000_type ;
--added by rakesh for getting column list
G_DYNAMIC_COL_LIST VARCHAR2(10000) := NULL;
--g_gl_segment_check VARCHAR2(1) := 'N';
g_gl_segment_count NUMBER ;
g_date_format VARCHAR2(100) ; --Bug#1338 , 1339


 PROCEDURE set_location ( p_message IN VARCHAR2 ) IS
 BEGIN

      IF g_debug THEN -- g_declared in spec
         dbms_output.enable ( 10000000 );
         dbms_output.put_line(TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS ') || SUBSTR(p_message, 1, 250));
      END IF;

        fnd_file.put_line ( fnd_file.log, p_message ) ;
        --hr_utility.set_location ( p_message, -99 );
 END;

PROCEDURE set_date_params ( p_process_id in NUMBER,
                            p_type IN VARCHAR2 ) IS

    l_use_icx_date_format_mask VARCHAR2(10);

    CURSOR parameters_c IS
        SELECT errp.parameter_name, errp.display_order, errp.data_type
          FROM xxeis.EIS_RS_PROCESS_RPT_PARAMETERS errp
         WHERE nvl(errp.enabled_flag, 'Y') = 'Y'
           AND errp.process_id = p_process_id
       AND (errp.data_type = 'DATE' OR errp.parameter_name LIKE '%Period%Date' OR errp.parameter_name LIKE ('Quarter%Date'));

    l_orig_param VARCHAR2(4000);
    l_date_format VARCHAR2(100);
    l_product_flag VARCHAR2(10);

BEGIN
    /*--Modified to fix payroll processor or xlconnect report submission with date format enhancement
    BEGIN
        SELECT ers.product_flag
        INTO l_product_flag
        FROM xxeis.eis_rs_processes erp,
             xxeis.eis_rs_sessions ers
        WHERE erp.session_id = ers.session_id
        AND erp.process_id   = p_process_id;
    EXCEPTION WHEN OTHERS THEN
        null;
    END;

        IF l_product_flag IN ('PP','XLC') THEN
          g_date_format  := 'DD-MON-YYYY';
        ELSE
          g_date_format :=xxeis.eis_rs_utility.get_date_format;
        END IF;*/

    --Bug# 6557 fix
    g_date_format :=xxeis.eis_rs_utility.get_date_format;

        IF p_type = 'date_to_canonical' THEN

            FOR parameters_rec IN parameters_c LOOP
                l_orig_param := g_param_tbl(parameters_rec.display_order) ;
                     --added by Sravani.M to fix bug# 5864
                   IF INSTR(l_orig_param,'''')>0 THEN

                     l_orig_param:=substr(l_orig_param,2,length(l_orig_param)-2);
                   END IF;
                  --by Sravani
                IF trim(g_param_tbl (parameters_rec.display_order)) IS NOT NULL THEN
                    g_param_tbl(parameters_rec.display_order) := fnd_date.date_to_canonical(xxeis.eis_rs_utility.convert_char_to_date (l_orig_param)) ;
                END IF;

            END LOOP ;

        ELSIF p_type = 'canonical_to_date' THEN

            FOR parameters_rec IN parameters_c LOOP
                l_orig_param := g_param_tbl(parameters_rec.display_order) ;
                g_param_tbl(parameters_rec.display_order) := TO_CHAR(fnd_date.canonical_to_date(g_param_tbl (parameters_rec.display_order)), g_date_format) ;

            END LOOP ;

        END IF;


END;
--Updated until here by Praveen.P to fix Bug#1338 , 1339
--Schedule request
PROCEDURE schedule_request(p_end_time  IN   VARCHAR2) IS
    l_parameter_value  INTEGER;
    l_schedule         BOOLEAN;
    l_request_id       NUMBER;
BEGIN

 l_schedule:= fnd_request.set_repeat_options(NULL,
     15 ,'MINUTES','START', p_end_time  );

     IF l_schedule THEN
        FND_FILE.PUT_LINE ( FND_FILE.LOG, 'Schedule - TRUE') ;
     ELSE
        FND_FILE.PUT_LINE ( FND_FILE.LOG, 'Schedule - FALSE') ;
     END IF;
END;

--Commented by Rajani as part of column data type fix, bug# 6184
/*  FUNCTION get_view_datatype (p_report_id  NUMBER ,
                                p_column_name VARCHAR2) RETURN VARCHAR2
    IS

        l_data_type VARCHAR2(30);
        l_pivoting VARCHAR2(30);
    l_cnt    number ;--Bug 750 -- 6.06
    BEGIN

    BEGIN
        SELECT data_type
        INTO l_data_type
        FROM xxeis.eis_rs_view_columns ervc
        , xxeis.eis_rs_reports err
        WHERE ervc.view_id = err.view_id
        AND err.report_id =p_report_id
        AND ervc.column_name = p_column_name;

    EXCEPTION WHEN OTHERS THEN
        l_data_type := NULL ;
    END ;


    IF l_data_type IS  NULL THEN

      SELECT  errc.data_type, errc.pivoting
            INTO l_data_type, l_pivoting
          FROM  xxeis.EIS_RS_PROCESS_RPT_COLUMNS       errc,--6.04
                xxeis.eis_rs_reports    err
         WHERE  err.report_id = p_report_id
         AND    errc.column_name =p_column_name
         AND    err.report_id = errc.report_id
         and errc.process_id = g_report_process_id --6.04
         AND  nvl(errc.enabled_flag, 'Y') = 'Y';

        IF l_data_type IS NULL THEN
            IF l_pivoting = 'DATA_FIELD' THEN
                l_data_type := 'NUMBER' ;
            END IF;
        END IF;

    END IF;

    --Bug750 : 6.06
    SELECT count(1) into l_cnt
    FROM xxeis.eis_rs_column_masks
    WHERE (report_id IS NULL OR report_id = p_report_id)
    AND column_name = p_column_name;

    IF(l_cnt > 0 OR l_data_type = 'CHAR' ) THEN
        l_data_type := 'VARCHAR2';
    END IF;

    RETURN  NVL(l_data_type,'VARCHAR2') ;

   EXCEPTION WHEN OTHERS THEN
     RETURN 'VARCHAR2';

    END get_view_datatype;


PROCEDURE get_process_column_data_type(p_process_id in number,
                                p_report_id IN NUMBER,
                                p_column_name IN VARCHAR2,
                                p_data_type  OUT VARCHAR2
                            )IS
l_data_type VARCHAR2(300);
BEGIN
       SELECT  NVL ( errc.data_type , ervc.data_type)
       INTO l_data_type
          FROM  xxeis.eis_rs_process_rpt_columns       errc,
                xxeis.eis_rs_reports    err,
                xxeis.eis_rs_view_columns ervc
         WHERE  err.report_id = p_report_id
         and   errc.process_id = p_process_id
         AND    errc.column_name =p_column_name
         AND    errc.column_name=ervc.column_name
         AND    err.report_id = errc.report_id
         AND    err.view_id = ervc.view_id
         AND  nvl(errc.enabled_flag, 'Y') = 'Y';

         if l_data_type = 'CHAR' then
            l_data_type := 'VARCHAR2';
         END IF;

        p_data_type := l_data_type;
--Updated by Praveen.P to fix Bug#1338 , 1339
EXCEPTION WHEN OTHERS THEN
    p_data_type := NULL ;
    xxeis.eis_rs_utility.log (' get_process_column_data_type', 'error p_process_id => ' || p_process_id ||
' p_report_id => ' || p_report_id || ' p_column_name => ' || p_column_name ) ;
END get_process_column_data_type;


PROCEDURE get_column_data_type(   p_report_id IN NUMBER,
                                p_column_name IN VARCHAR2,
                                p_data_type  OUT VARCHAR2
                            )IS
l_data_type VARCHAR2(300);
BEGIN
       SELECT  NVL ( errc.data_type , ervc.data_type)
       INTO l_data_type
          FROM  xxeis.EIS_RS_PROCESS_RPT_COLUMNS       errc,--6.04
                xxeis.eis_rs_reports    err,
                xxeis.eis_rs_view_columns ervc
         WHERE  err.report_id = p_report_id
         AND    errc.column_name =p_column_name
         AND    errc.column_name=ervc.column_name
         AND    err.report_id = errc.report_id
         AND    err.view_id = ervc.view_id
         AND  nvl(errc.enabled_flag, 'Y') = 'Y'
         and errc.process_id = g_report_process_id ;  --6.04

        p_data_type := l_data_type;

EXCEPTION WHEN OTHERS THEN
    p_data_type := NULL ;
END get_column_data_type;  */

--Bug# 6184
FUNCTION get_process_column_data_type(p_process_id  IN NUMBER,
                                      p_view_id     IN NUMBER,
                                      p_column_name IN VARCHAR2
                                      ) RETURN VARCHAR2 IS
      l_data_type VARCHAR2(300);
      --l_pivoting  VARCHAR2(30);
      l_report_id NUMBER;
      l_cnt      NUMBER ;--Bug 750
BEGIN

      SELECT nvl ( errc.data_type , ervc.data_type), errc.report_id
        INTO l_data_type, l_report_id
      FROM xxeis.eis_rs_process_rpt_columns errc,
        xxeis.eis_rs_reports err,
        xxeis.eis_rs_view_columns ervc
      WHERE errc.process_id           = p_process_id
      AND errc.column_name            = p_column_name
      AND errc.column_name            = ervc.column_name
      AND err.report_id               = errc.report_id
      AND errc.view_id                = ervc.view_id
      AND errc.view_id                = p_view_id
      AND NVL(errc.enabled_flag, 'Y') = 'Y';

      /*IF l_data_type IS NULL AND l_pivoting = 'DATA_FIELD' THEN
    l_data_type := 'NUMBER' ;
      END IF;*/

      BEGIN
        SELECT COUNT(1)
          INTO l_cnt
        FROM xxeis.eis_rs_column_masks
        WHERE (report_id IS NULL
          OR report_id      = l_report_id)
          AND column_name   = p_column_name;
      EXCEPTION WHEN OTHERS THEN
      NULL;
      END;

      IF (l_cnt > 0 or l_data_type = 'CHAR' ) THEN
        l_data_type := 'VARCHAR2';
      END IF;

    RETURN  NVL(l_data_type,'VARCHAR2') ;

EXCEPTION WHEN OTHERS THEN
   RETURN 'VARCHAR2';
END get_process_column_data_type;

FUNCTION rep_special_chars ( p_in_string IN VARCHAR2 ) RETURN VARCHAR2 IS
    l_out_string VARCHAR2(4000);
BEGIN

    l_out_string := REPLACE ( REPLACE ( REPLACE ( REPLACE ( REPLACE ( REPLACE ( REPLACE ( REPLACE ( REPLACE ( REPLACE ( REPLACE ( REPLACE (
                        p_in_string,
                            'u', 'u' ),
                            'o', 'o' ),
                            '?', 'b'),
                            'e', 'e'),
                            'e', 'e'),
                            'e', 'e'),
                            'a', 'a'),
                            'e ', 'e'),
                            '?', 'a'),
                            'a', 'a'),
                            'o', 'o'),
                            'c', 'c');
    return l_out_string ;
END;


 FUNCTION is_param_there( p_param_name  IN VARCHAR2
                         ,p_report_id   IN NUMBER  ) RETURN NUMBER IS
    l_counter NUMBER := 0 ;
 BEGIN

    SELECT COUNT(*)
      INTO l_counter
      FROM xxeis.EIS_RS_PROCESS_RPT_PARAMETERS errp
     WHERE errp.report_id = p_report_id
       AND upper(p_param_name) LIKE '%'||':'||UPPER(parameter_name)
       and process_id = g_report_process_id; -- 6.04

     RETURN l_counter;
 END ;

 PROCEDURE parse_delimited_data ( p_input_data IN VARCHAR2,
                                  p_demiliter in varchar2 default ',' ) is
    --bug #2003
    l_input_data  LONG;
    l_delimit_pos    NUMBER := -1;
    l_end_ch_pos        number := -1;
    l_field            LONG;
    dl                  VARCHAR2(1) := p_demiliter ;  -- dl - delimiter
    ec                  VARCHAR2(4) := '''' ;         -- ec - enclosed character
    --

BEGIN
    --
    -- Empty the PL/SQL Table
    g_data_tbl.delete ;
    --
    l_input_data := RTRIM(p_input_data, dl ); -- Remove all delimiters at the end
    l_input_data := lTRIM(l_input_data, chr(10) ); -- Remove all New line characters
    --
    LOOP
        --

    IF ec IS NOT NULL AND SUBSTR(l_input_data,1,1) = ec THEN
       l_end_ch_pos  := INSTR(l_input_data, ec, 1, 2); -- Get the 2nd enclosed character position
                                                           -- for eg : In "a","b",,,, 2nd enc ch position would be 2
       --case# 2741 apostrophe in employee names fix
       IF LENGTH(TRIM(SUBSTR(l_input_data,l_end_ch_pos+1)))>0 THEN
            IF INSTR(l_input_data, ec||dl)>0 THEN
                l_end_ch_pos := INSTR(l_input_data, ec||dl);
            ELSE
                l_end_ch_pos := INSTR(l_input_data, ec, -1);
            END IF;
        END IF;
    ELSE
       l_end_ch_pos := 0;
    END IF;
        IF l_end_ch_pos > 0 THEN
            l_delimit_pos := INSTR(l_input_data, dl, l_end_ch_pos, 1); -- Get the delimiter after 2nd enclosed character position
        ELSE
            l_delimit_pos := INSTR(l_input_data, dl); -- Get the delimiter position
        END IF;
        --
        IF l_delimit_pos > 0 THEN
            l_field := SUBSTR( l_input_data, 1, l_delimit_pos - 1);  -- Get the field
        ELSE
            l_field := l_input_data ;
        END IF;
        --Added by Rajani to fix apostrophe issue
        if substr(l_field,1,1) = ec and substr(l_field,length(l_field),1) = ec then
          l_field := substr(l_field,2,length(l_field)-2);
          l_field := REPLACE(l_field, ec,'''''');
          g_data_tbl (nvl(g_data_tbl.count,-1) + 1) := l_field;
        else
            g_data_tbl (nvl(g_data_tbl.count,-1) + 1) := REPLACE(l_field, ec) ;
        END IF;

        --g_data_tbl (nvl(g_data_tbl.count,-1) + 1) := REPLACE(l_field, ec) ;  -- Insert into the PL/SQL Table ( after removing the enclosed character )
        l_input_data := SUBSTR( l_input_data, l_delimit_pos + 1 ); -- Trim off the Parsed data

        EXIT WHEN l_delimit_pos = 0 OR g_data_tbl.count = 1000; -- Exit when there are no more delimiters
        --
    END LOOP;
 EXCEPTION WHEN OTHERS THEN
    NULL ;
 END ;

 FUNCTION return_col_data_type (  p_report_id   IN  NUMBER
                                , p_col_name IN VARCHAR2) RETURN VARCHAR2 IS
 l_data_type        VARCHAR2(30) ;
 BEGIN
     IF INSTR(p_col_name, ':') > 0 THEN

        SELECT upper(trim(data_type))
          INTO l_data_type
          FROM xxeis.EIS_RS_PROCESS_RPT_PARAMETERS
         WHERE report_id = p_report_id
           AND upper(trim(parameter_name)) = upper(trim(p_col_name))
           and process_id = g_report_process_id ; --6.04

     ELSE

        SELECT upper(trim(data_type))
          INTO l_data_type
          FROM xxeis.EIS_RS_PROCESS_RPT_COLUMNS -- 6.04
         WHERE report_id = p_report_id
           AND upper(trim(column_name)) = upper(trim(p_col_name))
           and process_id = g_report_process_id ;--6.04

     END IF ;
 RETURN l_data_type ;
 EXCEPTION WHEN OTHERS THEN
 RETURN NULL ;
 END return_col_data_type;

   PROCEDURE build_dyn_sql (string_in IN VARCHAR2)
   IS
      v_length BINARY_INTEGER;
      v_start BINARY_INTEGER := 1;

   BEGIN
      IF string_in IS NOT NULL THEN
         v_length := LENGTH (string_in);
         LOOP
            sql_table (NVL (sql_table.LAST, 0)+1) :=
               next_row (string_in, v_start, v_length);
            EXIT WHEN v_start > v_length;
         END LOOP;
      END IF;
   END build_dyn_sql;

--to get the parameter count in report parameter list
PROCEDURE get_display_order(p_report_id IN NUMBER ,
                            p_parameter_name IN  VARCHAR2 ,
                            p_display_index OUT NUMBER,
                            p_date OUT VARCHAR2 )IS
l_date_type  VARCHAR2(30);
l_display_order NUMBER ;
BEGIN

        SELECT DATA_TYPE,DISPLAY_ORDER
        INTO l_date_type,l_display_order
        FROM xxeis.EIS_RS_PROCESS_RPT_PARAMETERS
        WHERE report_id =p_report_id
        AND PARAMETER_NAME =p_parameter_name
        and process_id = g_report_process_id;--6.04

        IF l_date_type ='DATE' THEN
        p_display_index :=l_display_order;
        p_date := 'Y';
        ELSE
        p_display_index := -1;
        p_date := 'N';
        END IF ;

END get_display_order ;

FUNCTION get_report_name(p_report_id IN  VARCHAR2 )
 RETURN VARCHAR2
 IS
 l_report_name VARCHAR2(100);
 BEGIN
       SELECT report_name
       INTO l_report_name
       FROM xxeis.eis_rs_reports
       WHERE report_id = TO_NUMBER( p_report_id);
       RETURN  l_report_name;
 EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL ;
END get_report_name;

FUNCTION get_parameters_details( p_request_id IN  NUMBER  ,
                                  p_report_id IN  NUMBER
                                ) RETURN VARCHAR2 IS
        CURSOR param_name IS
            SELECT PARAMETER_NAME NAME
            FROM xxeis.EIS_RS_PROCESS_RPT_PARAMETERS param,
                 xxeis.eis_rs_processes erp
            WHERE erp.report_id = p_report_id
             AND  erp.request_id = p_request_id
             AND  erp.process_id = param.process_id
             ORDER BY param.DISPLAY_ORDER ASC ;

l_temp VARCHAR2(4000);
l_arg_tab parse_tbl_def;
counter NUMBER :=1;
BEGIN

        SELECT ARGUMENT4,ARGUMENT5,ARGUMENT6,
        ARGUMENT7,ARGUMENT8,ARGUMENT9,
        ARGUMENT10,ARGUMENT11,ARGUMENT12,
        ARGUMENT13,ARGUMENT14,ARGUMENT15,
        ARGUMENT16,ARGUMENT17,ARGUMENT18,
        ARGUMENT19,ARGUMENT20,ARGUMENT21,
        ARGUMENT22,ARGUMENT23,ARGUMENT24,
        ARGUMENT25
        INTO l_arg_tab(1),l_arg_tab(2),l_arg_tab(3),l_arg_tab(4),
        l_arg_tab(5),l_arg_tab(6),l_arg_tab(7),l_arg_tab(8),l_arg_tab(9),
        l_arg_tab(10),l_arg_tab(11),l_arg_tab(12),l_arg_tab(13),l_arg_tab(14),
        l_arg_tab(15),l_arg_tab(16),l_arg_tab(17),l_arg_tab(18),l_arg_tab(19),
        l_arg_tab(20),l_arg_tab(21),l_arg_tab(22)
        FROM fnd_concurrent_requests
        WHERE REQUEST_ID = p_request_id  ;

FOR p_rec IN param_name
LOOP

IF l_arg_tab(counter) IS  NOT NULL THEN
l_temp := l_temp || p_rec.NAME ||' = '||l_arg_tab(counter)||' : ';
END IF ;
counter := counter + 1 ;

END LOOP;

RETURN l_temp ;
END get_parameters_details;

PROCEDURE get_schedule_request_count(p_user_name IN  VARCHAR2 ,
                                    p_schedule_count OUT  NUMBER )IS
l_user_id NUMBER ;
BEGIN

        SELECT user_id
        INTO l_user_id
        FROM fnd_user
        WHERE UPPER (user_name) = UPPER (TRIM ( p_user_name) ) ;

        SELECT COUNT(*)
        INTO p_schedule_count
        FROM fnd_concurrent_requests fcr,
             fnd_concurrent_programs fcp
        WHERE fcr.STATUS_CODE ='I'
        AND fcr.PHASE_CODE='P'
        AND fcr.REQUESTED_BY =l_user_id
        AND fcp.application_id = fcr.program_application_id
        AND fcp.concurrent_program_id = fcr.concurrent_program_id
        AND fcr.resubmit_interval_unit_code IS  NOT NULL
        AND fcp.concurrent_program_name = 'XXEIS_RS_SUBMIT_REPORT' ;


END get_schedule_request_count;

--this procedure is to termintae scheduled reports
PROCEDURE terminate_schedule_requests( p_request_id IN NUMBER )IS
BEGIN
    UPDATE fnd_concurrent_requests
    SET phase_code = 'C'
     , status_code = 'D'
     , actual_completion_date = SYSDATE
     , completion_text = 'Terminated request.'
    WHERE request_id = p_request_id;

    UPDATE xxeis.eis_rs_processes
    SET status = 'T'
     , end_time = SYSDATE
    WHERE request_id = p_request_id;

    COMMIT ;

END terminate_schedule_requests;

---to show the schedule requests page for the current user.
PROCEDURE show_schedule_requests( p_user_name IN  VARCHAR2 ,
                                  p_main OUT cursor_type
                                )IS
        l_user_id NUMBER ;
        l_report_id NUMBER ;
        l_request_id NUMBER ;
        l_stmt    VARCHAR2(3000) ;

BEGIN

        SELECT user_id
        INTO l_user_id
        FROM fnd_user
        WHERE UPPER (user_name) = UPPER (TRIM ( p_user_name) ) ;

        l_stmt :=' SELECT REQUEST_ID ,xxeis.EIS_RS_PROCESS_REPORTS.get_report_name( ARGUMENT1 ) report_name  ,
        RESUBMIT_INTERVAL ,RESUBMIT_INTERVAL_UNIT_CODE ,ARGUMENT1 report_id,xxeis.EIS_RS_PROCESS_REPORTS.get_parameters_details(REQUEST_ID,to_number (ARGUMENT1)) parameters,
        LTRIM (RTRIM (TRIM (SUBSTR (argument_text,
                                           (INSTR (argument_text, '','', 1, 35) + 1
                                           ),
                                           (  INSTR (argument_text, ''http'', 1, 1) - (INSTR (argument_text, '','', 1, 35) + 1)
                                           )
                                          )
                                  ),
                             '',''
                            ),
                      '',''
                     ) notify
        FROM fnd_concurrent_requests fcr,
             fnd_concurrent_programs fcp
        WHERE fcr.STATUS_CODE =''I''
        AND fcr.PHASE_CODE=''P''
        AND fcr.REQUESTED_BY ='|| l_user_id ||'
        AND fcp.application_id = fcr.program_application_id
        AND fcp.concurrent_program_id = fcr.concurrent_program_id
        AND fcp.concurrent_program_name = ''XXEIS_RS_SUBMIT_REPORT''';


        OPEN p_main FOR l_stmt ;

END show_schedule_requests ;


--to check from URL Link in order to open Report output from Workflow
PROCEDURE user_authorization ( p_user_id      IN VARCHAR2
                               ,p_process_id   IN NUMBER
                               ,p_auth_user  OUT VARCHAR2
                               ,p_eis_home OUT VARCHAR2
                               ,p_start_time OUT VARCHAR2
                               ,p_end_time OUT VARCHAR2
                               ,p_request_id OUT VARCHAR2
                               ,p_rows OUT VARCHAR2
                               ,p_report_name OUT VARCHAR2
                               ,p_enc_session_id OUT VARCHAR2
                               ,p_user_name OUT VARCHAR2
                               ,p_submitted_by OUT VARCHAR2
                               ,p_summary_flag OUT VARCHAR2
                   ,p_report_id  OUT   VARCHAR2
                   ,p_portal_login IN VARCHAR2 DEFAULT NULL) IS

    l_count NUMBER ;
    l_auth_user BOOLEAN := FALSE ;

BEGIN

      SELECT COUNT(*)
      INTO l_count
      FROM xxeis.eis_rs_process_dist_lines
        WHERE process_id = p_process_id
        AND user_id = p_user_id ;

      IF l_count > 0 THEN
        l_auth_user := TRUE ;
      END IF;

      -- if person matches, send person is authorised to see report
      IF l_auth_user = TRUE OR p_portal_login = 'Y' THEN
        p_auth_user := 'Y';
      ELSE
        p_auth_user := 'N';
      END IF ;

    --send eis home directory of csv
        SELECT PARAMETER_VALUE
        INTO p_eis_home
        FROM xxeis.eis_rs_admin_parameters
        WHERE upper(PARAMETER_NAME) ='EIS_DIRECTORY';

        SELECT USER_NAME
        INTO  p_user_name
        FROM fnd_user
        WHERE user_id = p_user_id;

    --get header section of screen.
    SELECT erp.report_id,
      erp.request_id ,
      TO_CHAR(erp.start_time,xxeis.eis_rs_utility.get_date_format
      || ' HH:MI:SS AM'),
      TO_CHAR(erp.end_time,xxeis.eis_rs_utility.get_date_format
      || ' HH:MI:SS AM'),
      erp.rows_retrieved,
      xxeis.eis_rs_utility.x(erp.session_id),
      er.report_name,
      fu.user_name
    INTO p_report_id,
      p_request_id ,
      p_start_time,
      p_end_time,
      p_rows,
      p_enc_session_id,
      p_report_name,
      p_submitted_by
    FROM xxeis.eis_rs_processes erp,
      xxeis.eis_rs_reports er,
      fnd_user fu
    WHERE erp.report_id = er.report_id
    AND erp.created_by  = fu.user_id
    AND erp.process_id  = p_process_id;


END user_authorization;

FUNCTION   increment_date(  p_initial_date IN VARCHAR2,
                           p_interval IN VARCHAR2 ,
                           p_duration IN VARCHAR2 )RETURN VARCHAR2
 IS
    l_return_date DATE;
    l_initial_date DATE ;

BEGIN

    IF p_initial_date LIKE '%:%' THEN --canonical format
        l_initial_date := fnd_date.canonical_to_date(p_initial_date);
    ELSE
        l_initial_date := xxeis.eis_rs_utility.convert_char_to_date(p_initial_date);
    END IF;

    IF p_duration = 'D' THEN --days
        l_return_date := l_initial_date + p_interval ;
    ELSIF p_duration = 'M' THEN --Minutes
        l_return_date := l_initial_date + p_interval*(1/(24*60)) ;
    ELSIF p_duration = 'H' THEN --hours
        l_return_date := l_initial_date + p_interval*(1/24) ;
    ELSIF p_duration = 'W' THEN --Weeks
        l_return_date := l_initial_date + p_interval*7 ;
    ELSIF p_duration = 'Z' THEN --Months
        l_return_date := add_months(l_initial_date , p_interval);
    END IF ;

    RETURN fnd_date.date_to_canonical(l_return_date);

EXCEPTION WHEN OTHERS THEN
        RETURN p_initial_date;
END increment_date;

   FUNCTION next_row
     ( string_in    IN VARCHAR2,
       start_inout  IN OUT BINARY_INTEGER,
       len_in       IN BINARY_INTEGER)
   RETURN VARCHAR2
   IS
      v_start BINARY_INTEGER := start_inout;
   BEGIN
      start_inout := LEAST (len_in + 1, start_inout + 256);
      RETURN SUBSTR (string_in, v_start, 256);
   END next_row;

 PROCEDURE check_if_masked( p_column_name IN VARCHAR2,
                            p_report_id IN NUMBER,
                            p_replace_character OUT VARCHAR2,
                            p_do_not_rep_column_type OUT VARCHAR2,
                            p_do_not_rep_column_width OUT NUMBER ) IS

 BEGIN

    BEGIN

        SELECT replace_character, do_not_rep_column_type, do_not_rep_column_width
        INTO p_replace_character, p_do_not_rep_column_type, p_do_not_rep_column_width
        FROM xxeis.eis_rs_column_masks
        WHERE UPPER(column_name) = UPPER(p_column_name)
        AND report_id = p_report_id ;

    EXCEPTION WHEN NO_DATA_FOUND THEN
        NULL;
    END;

    IF p_replace_character IS NULL THEN

        SELECT replace_character, do_not_rep_column_type, do_not_rep_column_width
        INTO p_replace_character, p_do_not_rep_column_type, p_do_not_rep_column_width
        FROM xxeis.eis_rs_column_masks
        WHERE UPPER(column_name) = UPPER(p_column_name)
        AND report_id IS NULL
        AND ROWNUM < 2 ;

    END IF;
 EXCEPTION WHEN OTHERS THEN
    --fnd_file.put_line ( fnd_file.LOG, '!!! Error in check_if_masked ' || SQLERRM );
    NULL ;
 END;

 FUNCTION get_html_table_name (p_process_id    IN VARCHAR2) RETURN VARCHAR2
 IS
    l_decider_len NUMBER(20);
    l_decide_table NUMBER(6);
    l_table_name VARCHAR2(100);


BEGIN

   l_decider_len := LENGTH(p_process_id);
    SELECT SUBSTR(p_process_id,l_decider_len,l_decider_len) INTO l_decide_table FROM dual;
    IF l_decide_table = 1 THEN
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl1';
    ELSIF l_decide_table = 2 THEN
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl2';
    ELSIF l_decide_table = 3 THEN
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl3';
    ELSIF l_decide_table = 4 THEN
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl4';
    ELSIF l_decide_table = 5 THEN
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl5';
    ELSIF l_decide_table = 6 THEN
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl6';
    ELSIF l_decide_table = 7 THEN
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl7';
    ELSIF l_decide_table = 8 THEN
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl8';
    ELSIF l_decide_table = 9 THEN
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl9';
    ELSIF l_decide_table = 0 THEN
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl10';
    ELSE
       l_table_name := 'xxeis.eis_rs_html_rslt_dtl1';
    END IF;

    UPDATE xxeis.eis_rs_processes set OUTPUT_TABLE = l_table_name
    WHERE process_id = p_process_id;
   -- COMMIT;

    RETURN l_table_name;

END get_html_table_name;

PROCEDURE bulk_insert_html_table ( p_table_name IN VARCHAR2 )  IS
    --bug #2003
      l_sql_string long;
BEGIN

    l_sql_string :=  '
    BEGIN
    FORALL i IN nvl(xxeis.eis_rs_process_reports.g_html_table.FIRST,0)..NVL(xxeis.eis_rs_process_reports.g_html_table.LAST,-1)
    INSERT INTO '||p_table_name||' VALUES xxeis.eis_rs_process_reports.g_html_table(i);
    END;';

    EXECUTE IMMEDIATE l_sql_string ;

    g_html_table.DELETE ;

END;

PROCEDURE insert_html_table (p_process_id    IN VARCHAR2,
                             p_output_col    IN VARCHAR2,
                             p_report_id     IN VARCHAR2,
                             p_table_name    IN VARCHAR2,
                             p_security_col_list IN VARCHAR2)
IS

    l_seq NUMBER ;

    l_output_data     VARCHAR2(4000) := p_output_col||'~';
    l_output_col      VARCHAR2(4000);
    l_output_date      VARCHAR2(4000) := NULL;
    l_output_var      VARCHAR2(4000) := NULL;
    l_output_num      VARCHAR2(4000) := NULL;
    l_output_temp     VARCHAR2(10000) := NULL;--Length increased by Praveen.p to fix string buffer too small issue in demo instance Bug#4758
    l_hyperlink     VARCHAR2(4000);
    l_datatype_temp VARCHAR2(100);
    l_datatype         LONG := g_dynamic_col_list||',';
    num_list        NUMBER := 0;
    var_list        NUMBER := 0;
    date_list        NUMBER := 0;
    l_publish_data  VARCHAR2(4000) := p_security_col_list||'~';
    l_publish_temp  VARCHAR2(1000);
    l_publish_col   VARCHAR2(1000);

BEGIN

--set_location ( p_output_col ) ;

    SELECT xxeis.eis_rs_html_rslt_dtl5_s.NEXTVAL INTO l_seq FROM dual;

    g_html_table(g_record_count).process_id := p_process_id ;
    g_html_table(g_record_count).EIS_RSLT_DTL_ID := l_seq ;
    --rakesh publish changes
    WHILE l_publish_data IS NOT NULL OR l_publish_data <> '~' LOOP
        l_publish_temp := SUBSTR(l_publish_data,0,INSTR(l_publish_data,'~',1,1)-1);
        l_publish_col := SUBSTR(l_publish_temp,0,INSTR(l_publish_temp,'=',1,1)-1);
        IF l_publish_col = 'person_id' THEN
            g_html_table(g_record_count).person_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'assignment_id' THEN
            g_html_table(g_record_count).assignment_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'org_id' THEN
            g_html_table(g_record_count).org_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'task_id' THEN
            g_html_table(g_record_count).task_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'ledger_id' THEN
            g_html_table(g_record_count).ledger_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'project_id' THEN
            g_html_table(g_record_count).project_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'set_of_books_id' THEN
            g_html_table(g_record_count).set_of_books_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'organization_id' THEN
            g_html_table(g_record_count).organization_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'book_type_code' THEN
            g_html_table(g_record_count).book_type_code := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'code_combination_id' THEN
            g_html_table(g_record_count).code_combination_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'security_col1' THEN
            g_html_table(g_record_count).security_col1 := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'security_col2' THEN
            g_html_table(g_record_count).security_col2 := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'security_col3' THEN
            g_html_table(g_record_count).security_col3 := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'security_col4' THEN
            g_html_table(g_record_count).security_col4 := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'security_col5' THEN
            g_html_table(g_record_count).security_col5 := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        /*ELSIF l_publish_col = 'payroll_id' THEN
            g_html_table(g_record_count).payroll_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'organization_id' THEN
            g_html_table(g_record_count).organization_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        ELSIF l_publish_col = 'assignment_id' THEN
            g_html_table(g_record_count).assignment_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);*/
        ELSE
            g_html_table(g_record_count).assignment_id := SUBSTR(l_publish_temp,INSTR(l_publish_temp,'=',1,1)+1);
        END IF;

        l_publish_data := SUBSTR(l_publish_data,INSTR(l_publish_data,'~',1,1)+1);
    END LOOP;

    WHILE l_output_data IS NOT NULL OR l_output_data <> '~' LOOP
        l_output_temp := SUBSTR(l_output_data,0,INSTR(l_output_data,'~',1,1)-1);
        l_datatype_temp := SUBSTR(l_datatype,0,INSTR(l_datatype,',',1,1)-1);


        IF INSTR(l_output_temp,'=HYPERLINK') > 0 THEN
            l_hyperlink := SUBSTR(l_output_temp, 1, INSTR(l_output_temp,'"^"'));
            l_hyperlink := RTRIM(REPLACE(l_hyperlink, '=HYPERLINK("'),'"');
            l_output_temp := SUBSTR(l_output_temp,INSTR(l_output_temp,'"^"')+3);
            --added by sravani to fix bug# 4477
            l_output_temp := RTRIM(RTRIM(l_output_temp,')'),'"');
            l_output_temp := l_output_temp || '^#'||l_hyperlink ;

        END IF;

        IF l_datatype_temp LIKE 'VARCHAR2_COL%' THEN
            --l_output_temp := ''''||replace(l_output_temp, '''', '''''')||''''; -- Kp Change to care of single quotes - 11/28/2005
            l_output_var := trim(l_output_temp);-- KP Change to takecare of null - 11/28/2005
            var_list := var_list + 1;
            IF var_list BETWEEN 1 AND 10 THEN
                CASE
                    WHEN var_list = 1 THEN g_html_table(g_record_count).varchar2_col1 := l_output_var ;
                    WHEN var_list = 2 THEN g_html_table(g_record_count).varchar2_col2 := l_output_var ;
                    WHEN var_list = 3 THEN g_html_table(g_record_count).varchar2_col3 := l_output_var ;
                    WHEN var_list = 4 THEN g_html_table(g_record_count).varchar2_col4 := l_output_var ;
                    WHEN var_list = 5 THEN g_html_table(g_record_count).varchar2_col5 := l_output_var ;
                    WHEN var_list = 6 THEN g_html_table(g_record_count).varchar2_col6 := l_output_var ;
                    WHEN var_list = 7 THEN g_html_table(g_record_count).varchar2_col7 := l_output_var ;
                    WHEN var_list = 8 THEN g_html_table(g_record_count).varchar2_col8 := l_output_var ;
                    WHEN var_list = 9 THEN g_html_table(g_record_count).varchar2_col9 := l_output_var ;
                    WHEN var_list = 10 THEN g_html_table(g_record_count).varchar2_col10 := l_output_var ;
                    ELSE NULL ;
                END CASE ;
            END IF;

            IF var_list BETWEEN 11 AND 20 THEN
                CASE
                WHEN var_list = 11 THEN g_html_table(g_record_count).varchar2_col11 := l_output_var ;
                WHEN var_list = 12 THEN g_html_table(g_record_count).varchar2_col12 := l_output_var ;
                WHEN var_list = 13 THEN g_html_table(g_record_count).varchar2_col13 := l_output_var ;
                WHEN var_list = 14 THEN g_html_table(g_record_count).varchar2_col14 := l_output_var ;
                WHEN var_list = 15 THEN g_html_table(g_record_count).varchar2_col15 := l_output_var ;
                WHEN var_list = 16 THEN g_html_table(g_record_count).varchar2_col16 := l_output_var ;
                WHEN var_list = 17 THEN g_html_table(g_record_count).varchar2_col17 := l_output_var ;
                WHEN var_list = 18 THEN g_html_table(g_record_count).varchar2_col18 := l_output_var ;
                WHEN var_list = 19 THEN g_html_table(g_record_count).varchar2_col19 := l_output_var ;
                WHEN var_list = 20 THEN g_html_table(g_record_count).varchar2_col20 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;

            IF var_list BETWEEN 21 AND 30 THEN
                CASE
                WHEN var_list = 21 THEN g_html_table(g_record_count).varchar2_col21 := l_output_var ;
                WHEN var_list = 22 THEN g_html_table(g_record_count).varchar2_col22 := l_output_var ;
                WHEN var_list = 23 THEN g_html_table(g_record_count).varchar2_col23 := l_output_var ;
                WHEN var_list = 24 THEN g_html_table(g_record_count).varchar2_col24 := l_output_var ;
                WHEN var_list = 25 THEN g_html_table(g_record_count).varchar2_col25 := l_output_var ;
                WHEN var_list = 26 THEN g_html_table(g_record_count).varchar2_col26 := l_output_var ;
                WHEN var_list = 27 THEN g_html_table(g_record_count).varchar2_col27 := l_output_var ;
                WHEN var_list = 28 THEN g_html_table(g_record_count).varchar2_col28 := l_output_var ;
                WHEN var_list = 29 THEN g_html_table(g_record_count).varchar2_col29 := l_output_var ;
                WHEN var_list = 30 THEN g_html_table(g_record_count).varchar2_col30 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;

            IF var_list BETWEEN 31 AND 40 THEN
                CASE
                WHEN var_list = 31 THEN g_html_table(g_record_count).varchar2_col31 := l_output_var ;
                WHEN var_list = 32 THEN g_html_table(g_record_count).varchar2_col32 := l_output_var ;
                WHEN var_list = 33 THEN g_html_table(g_record_count).varchar2_col33 := l_output_var ;
                WHEN var_list = 34 THEN g_html_table(g_record_count).varchar2_col34 := l_output_var ;
                WHEN var_list = 35 THEN g_html_table(g_record_count).varchar2_col35 := l_output_var ;
                WHEN var_list = 36 THEN g_html_table(g_record_count).varchar2_col36 := l_output_var ;
                WHEN var_list = 37 THEN g_html_table(g_record_count).varchar2_col37 := l_output_var ;
                WHEN var_list = 38 THEN g_html_table(g_record_count).varchar2_col38 := l_output_var ;
                WHEN var_list = 39 THEN g_html_table(g_record_count).varchar2_col39 := l_output_var ;
                WHEN var_list = 40 THEN g_html_table(g_record_count).varchar2_col40 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;

            IF var_list BETWEEN 41 AND 50 THEN
                CASE
                WHEN var_list = 41 THEN g_html_table(g_record_count).varchar2_col41 := l_output_var ;
                WHEN var_list = 42 THEN g_html_table(g_record_count).varchar2_col42 := l_output_var ;
                WHEN var_list = 43 THEN g_html_table(g_record_count).varchar2_col43 := l_output_var ;
                WHEN var_list = 44 THEN g_html_table(g_record_count).varchar2_col44 := l_output_var ;
                WHEN var_list = 45 THEN g_html_table(g_record_count).varchar2_col45 := l_output_var ;
                WHEN var_list = 46 THEN g_html_table(g_record_count).varchar2_col46 := l_output_var ;
                WHEN var_list = 47 THEN g_html_table(g_record_count).varchar2_col47 := l_output_var ;
                WHEN var_list = 48 THEN g_html_table(g_record_count).varchar2_col48 := l_output_var ;
                WHEN var_list = 49 THEN g_html_table(g_record_count).varchar2_col49 := l_output_var ;
                WHEN var_list = 50 THEN g_html_table(g_record_count).varchar2_col50 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
            --Bug #2003
           IF var_list BETWEEN 51 AND 60 THEN
                CASE
                WHEN var_list = 51 THEN g_html_table(g_record_count).varchar2_col51 := l_output_var ;
                WHEN var_list = 52 THEN g_html_table(g_record_count).varchar2_col52 := l_output_var ;
                WHEN var_list = 53 THEN g_html_table(g_record_count).varchar2_col53 := l_output_var ;
                WHEN var_list = 54 THEN g_html_table(g_record_count).varchar2_col54 := l_output_var ;
                WHEN var_list = 55 THEN g_html_table(g_record_count).varchar2_col55 := l_output_var ;
                WHEN var_list = 56 THEN g_html_table(g_record_count).varchar2_col56 := l_output_var ;
                WHEN var_list = 57 THEN g_html_table(g_record_count).varchar2_col57 := l_output_var ;
                WHEN var_list = 58 THEN g_html_table(g_record_count).varchar2_col58 := l_output_var ;
                WHEN var_list = 59 THEN g_html_table(g_record_count).varchar2_col59 := l_output_var ;
                WHEN var_list = 60 THEN g_html_table(g_record_count).varchar2_col60 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
             IF var_list BETWEEN 61 AND 70 THEN
                CASE
                WHEN var_list = 61 THEN g_html_table(g_record_count).varchar2_col61 := l_output_var ;
                WHEN var_list = 62 THEN g_html_table(g_record_count).varchar2_col62 := l_output_var ;
                WHEN var_list = 63 THEN g_html_table(g_record_count).varchar2_col63 := l_output_var ;
                WHEN var_list = 64 THEN g_html_table(g_record_count).varchar2_col64 := l_output_var ;
                WHEN var_list = 65 THEN g_html_table(g_record_count).varchar2_col65 := l_output_var ;
                WHEN var_list = 66 THEN g_html_table(g_record_count).varchar2_col66 := l_output_var ;
                WHEN var_list = 67 THEN g_html_table(g_record_count).varchar2_col67 := l_output_var ;
                WHEN var_list = 68 THEN g_html_table(g_record_count).varchar2_col68 := l_output_var ;
                WHEN var_list = 69 THEN g_html_table(g_record_count).varchar2_col69 := l_output_var ;
                WHEN var_list = 70 THEN g_html_table(g_record_count).varchar2_col70 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
             IF var_list BETWEEN 71 AND 80 THEN
                CASE
                WHEN var_list = 71 THEN g_html_table(g_record_count).varchar2_col71 := l_output_var ;
                WHEN var_list = 72 THEN g_html_table(g_record_count).varchar2_col72 := l_output_var ;
                WHEN var_list = 73 THEN g_html_table(g_record_count).varchar2_col73 := l_output_var ;
                WHEN var_list = 74 THEN g_html_table(g_record_count).varchar2_col74 := l_output_var ;
                WHEN var_list = 75 THEN g_html_table(g_record_count).varchar2_col75 := l_output_var ;
                WHEN var_list = 76 THEN g_html_table(g_record_count).varchar2_col76 := l_output_var ;
                WHEN var_list = 77 THEN g_html_table(g_record_count).varchar2_col77 := l_output_var ;
                WHEN var_list = 78 THEN g_html_table(g_record_count).varchar2_col78 := l_output_var ;
                WHEN var_list = 79 THEN g_html_table(g_record_count).varchar2_col79 := l_output_var ;
                WHEN var_list = 80 THEN g_html_table(g_record_count).varchar2_col80 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
             IF var_list BETWEEN 81 AND 90 THEN
                CASE
                WHEN var_list = 81 THEN g_html_table(g_record_count).varchar2_col81 := l_output_var ;
                WHEN var_list = 82 THEN g_html_table(g_record_count).varchar2_col82 := l_output_var ;
                WHEN var_list = 83 THEN g_html_table(g_record_count).varchar2_col83 := l_output_var ;
                WHEN var_list = 84 THEN g_html_table(g_record_count).varchar2_col84 := l_output_var ;
                WHEN var_list = 85 THEN g_html_table(g_record_count).varchar2_col85 := l_output_var ;
                WHEN var_list = 86 THEN g_html_table(g_record_count).varchar2_col86 := l_output_var ;
                WHEN var_list = 87 THEN g_html_table(g_record_count).varchar2_col87 := l_output_var ;
                WHEN var_list = 88 THEN g_html_table(g_record_count).varchar2_col88 := l_output_var ;
                WHEN var_list = 89 THEN g_html_table(g_record_count).varchar2_col89 := l_output_var ;
                WHEN var_list = 90 THEN g_html_table(g_record_count).varchar2_col90 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
             IF var_list BETWEEN 91 AND 100 THEN
                CASE
                WHEN var_list = 91 THEN g_html_table(g_record_count).varchar2_col91 := l_output_var ;
                WHEN var_list = 92 THEN g_html_table(g_record_count).varchar2_col92 := l_output_var ;
                WHEN var_list = 93 THEN g_html_table(g_record_count).varchar2_col93 := l_output_var ;
                WHEN var_list = 94 THEN g_html_table(g_record_count).varchar2_col94 := l_output_var ;
                WHEN var_list = 95 THEN g_html_table(g_record_count).varchar2_col95 := l_output_var ;
                WHEN var_list = 96 THEN g_html_table(g_record_count).varchar2_col96 := l_output_var ;
                WHEN var_list = 97 THEN g_html_table(g_record_count).varchar2_col97 := l_output_var ;
                WHEN var_list = 98 THEN g_html_table(g_record_count).varchar2_col98 := l_output_var ;
                WHEN var_list = 99 THEN g_html_table(g_record_count).varchar2_col99 := l_output_var ;
                WHEN var_list = 100 THEN g_html_table(g_record_count).varchar2_col100 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
              IF var_list BETWEEN 101 AND 110 THEN
                CASE
                WHEN var_list = 101 THEN g_html_table(g_record_count).varchar2_col101 := l_output_var ;
                WHEN var_list = 102 THEN g_html_table(g_record_count).varchar2_col102 := l_output_var ;
                WHEN var_list = 103 THEN g_html_table(g_record_count).varchar2_col103 := l_output_var ;
                WHEN var_list = 104 THEN g_html_table(g_record_count).varchar2_col104 := l_output_var ;
                WHEN var_list = 105 THEN g_html_table(g_record_count).varchar2_col105 := l_output_var ;
                WHEN var_list = 106 THEN g_html_table(g_record_count).varchar2_col106 := l_output_var ;
                WHEN var_list = 107 THEN g_html_table(g_record_count).varchar2_col107 := l_output_var ;
                WHEN var_list = 108 THEN g_html_table(g_record_count).varchar2_col108 := l_output_var ;
                WHEN var_list = 109 THEN g_html_table(g_record_count).varchar2_col109 := l_output_var ;
                WHEN var_list = 110 THEN g_html_table(g_record_count).varchar2_col110 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
              IF var_list BETWEEN 111 AND 120 THEN
                CASE
                WHEN var_list = 111 THEN g_html_table(g_record_count).varchar2_col111 := l_output_var ;
                WHEN var_list = 112 THEN g_html_table(g_record_count).varchar2_col112 := l_output_var ;
                WHEN var_list = 113 THEN g_html_table(g_record_count).varchar2_col113 := l_output_var ;
                WHEN var_list = 114 THEN g_html_table(g_record_count).varchar2_col114 := l_output_var ;
                WHEN var_list = 115 THEN g_html_table(g_record_count).varchar2_col115 := l_output_var ;
                WHEN var_list = 116 THEN g_html_table(g_record_count).varchar2_col116 := l_output_var ;
                WHEN var_list = 117 THEN g_html_table(g_record_count).varchar2_col117 := l_output_var ;
                WHEN var_list = 118 THEN g_html_table(g_record_count).varchar2_col118 := l_output_var ;
                WHEN var_list = 119 THEN g_html_table(g_record_count).varchar2_col119 := l_output_var ;
                WHEN var_list = 120 THEN g_html_table(g_record_count).varchar2_col120 := l_output_var ;
                ELSE NULL;
                end case ;
              END IF;
               IF var_list BETWEEN 121 AND 130 THEN
                CASE
                WHEN var_list = 121 THEN g_html_table(g_record_count).varchar2_col121 := l_output_var ;
                WHEN var_list = 122 THEN g_html_table(g_record_count).varchar2_col122 := l_output_var ;
                WHEN var_list = 123 THEN g_html_table(g_record_count).varchar2_col123 := l_output_var ;
                WHEN var_list = 124 THEN g_html_table(g_record_count).varchar2_col124 := l_output_var ;
                WHEN var_list = 125 THEN g_html_table(g_record_count).varchar2_col125 := l_output_var ;
                WHEN var_list = 126 THEN g_html_table(g_record_count).varchar2_col126 := l_output_var ;
                WHEN var_list = 127 THEN g_html_table(g_record_count).varchar2_col127 := l_output_var ;
                WHEN var_list = 128 THEN g_html_table(g_record_count).varchar2_col128 := l_output_var ;
                WHEN var_list = 129 THEN g_html_table(g_record_count).varchar2_col129 := l_output_var ;
                WHEN var_list = 130 THEN g_html_table(g_record_count).varchar2_col130 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
              IF var_list BETWEEN 131 AND 140 THEN
                CASE
                WHEN var_list = 131 THEN g_html_table(g_record_count).varchar2_col131 := l_output_var ;
                WHEN var_list = 132 THEN g_html_table(g_record_count).varchar2_col132 := l_output_var ;
                WHEN var_list = 133 THEN g_html_table(g_record_count).varchar2_col133 := l_output_var ;
                WHEN var_list = 134 THEN g_html_table(g_record_count).varchar2_col134 := l_output_var ;
                WHEN var_list = 135 THEN g_html_table(g_record_count).varchar2_col135 := l_output_var ;
                WHEN var_list = 136 THEN g_html_table(g_record_count).varchar2_col136 := l_output_var ;
                WHEN var_list = 137 THEN g_html_table(g_record_count).varchar2_col137 := l_output_var ;
                WHEN var_list = 138 THEN g_html_table(g_record_count).varchar2_col138 := l_output_var ;
                WHEN var_list = 139 THEN g_html_table(g_record_count).varchar2_col139 := l_output_var ;
                WHEN var_list = 140 THEN g_html_table(g_record_count).varchar2_col140 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
              IF var_list BETWEEN 141 AND 150 THEN
                CASE
                WHEN var_list = 141 THEN g_html_table(g_record_count).varchar2_col141 := l_output_var ;
                WHEN var_list = 142 THEN g_html_table(g_record_count).varchar2_col142 := l_output_var ;
                WHEN var_list = 143 THEN g_html_table(g_record_count).varchar2_col143 := l_output_var ;
                WHEN var_list = 144 THEN g_html_table(g_record_count).varchar2_col144 := l_output_var ;
                WHEN var_list = 145 THEN g_html_table(g_record_count).varchar2_col145 := l_output_var ;
                WHEN var_list = 146 THEN g_html_table(g_record_count).varchar2_col146 := l_output_var ;
                WHEN var_list = 147 THEN g_html_table(g_record_count).varchar2_col147 := l_output_var ;
                WHEN var_list = 148 THEN g_html_table(g_record_count).varchar2_col148 := l_output_var ;
                WHEN var_list = 149 THEN g_html_table(g_record_count).varchar2_col149 := l_output_var ;
                WHEN var_list = 150 THEN g_html_table(g_record_count).varchar2_col150 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
              IF var_list BETWEEN 151 AND 160 THEN
                CASE
                WHEN var_list = 151 THEN g_html_table(g_record_count).varchar2_col151 := l_output_var ;
                WHEN var_list = 152 THEN g_html_table(g_record_count).varchar2_col152 := l_output_var ;
                WHEN var_list = 153 THEN g_html_table(g_record_count).varchar2_col153 := l_output_var ;
                WHEN var_list = 154 THEN g_html_table(g_record_count).varchar2_col154 := l_output_var ;
                WHEN var_list = 155 THEN g_html_table(g_record_count).varchar2_col155 := l_output_var ;
                WHEN var_list = 156 THEN g_html_table(g_record_count).varchar2_col156 := l_output_var ;
                WHEN var_list = 157 THEN g_html_table(g_record_count).varchar2_col157 := l_output_var ;
                WHEN var_list = 158 THEN g_html_table(g_record_count).varchar2_col158 := l_output_var ;
                WHEN var_list = 159 THEN g_html_table(g_record_count).varchar2_col159 := l_output_var ;
                WHEN var_list = 160 THEN g_html_table(g_record_count).varchar2_col160 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
             IF var_list BETWEEN 161 AND 170 THEN
                CASE
                WHEN var_list = 161 THEN g_html_table(g_record_count).varchar2_col161 := l_output_var ;
                WHEN var_list = 162 THEN g_html_table(g_record_count).varchar2_col162 := l_output_var ;
                WHEN var_list = 163 THEN g_html_table(g_record_count).varchar2_col163 := l_output_var ;
                WHEN var_list = 164 THEN g_html_table(g_record_count).varchar2_col164 := l_output_var ;
                WHEN var_list = 165 THEN g_html_table(g_record_count).varchar2_col165 := l_output_var ;
                WHEN var_list = 166 THEN g_html_table(g_record_count).varchar2_col166 := l_output_var ;
                WHEN var_list = 167 THEN g_html_table(g_record_count).varchar2_col167 := l_output_var ;
                WHEN var_list = 168 THEN g_html_table(g_record_count).varchar2_col168 := l_output_var ;
                WHEN var_list = 169 THEN g_html_table(g_record_count).varchar2_col169 := l_output_var ;
                WHEN var_list = 170 THEN g_html_table(g_record_count).varchar2_col170 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
              IF var_list BETWEEN 171 AND 180 THEN
                CASE
                WHEN var_list = 171 THEN g_html_table(g_record_count).varchar2_col171 := l_output_var ;
                WHEN var_list = 172 THEN g_html_table(g_record_count).varchar2_col172 := l_output_var ;
                WHEN var_list = 173 THEN g_html_table(g_record_count).varchar2_col173 := l_output_var ;
                WHEN var_list = 174 THEN g_html_table(g_record_count).varchar2_col174 := l_output_var ;
                WHEN var_list = 175 THEN g_html_table(g_record_count).varchar2_col175 := l_output_var ;
                WHEN var_list = 176 THEN g_html_table(g_record_count).varchar2_col176 := l_output_var ;
                WHEN var_list = 177 THEN g_html_table(g_record_count).varchar2_col177 := l_output_var ;
                WHEN var_list = 178 THEN g_html_table(g_record_count).varchar2_col178 := l_output_var ;
                WHEN var_list = 179 THEN g_html_table(g_record_count).varchar2_col179 := l_output_var ;
                WHEN var_list = 180 THEN g_html_table(g_record_count).varchar2_col180 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
              IF var_list BETWEEN 181 AND 190 THEN
                CASE
                WHEN var_list = 181 THEN g_html_table(g_record_count).varchar2_col181 := l_output_var ;
                WHEN var_list = 182 THEN g_html_table(g_record_count).varchar2_col182 := l_output_var ;
                WHEN var_list = 183 THEN g_html_table(g_record_count).varchar2_col183 := l_output_var ;
                WHEN var_list = 184 THEN g_html_table(g_record_count).varchar2_col184 := l_output_var ;
                WHEN var_list = 185 THEN g_html_table(g_record_count).varchar2_col185 := l_output_var ;
                WHEN var_list = 186 THEN g_html_table(g_record_count).varchar2_col186 := l_output_var ;
                WHEN var_list = 187 THEN g_html_table(g_record_count).varchar2_col187 := l_output_var ;
                WHEN var_list = 188 THEN g_html_table(g_record_count).varchar2_col188 := l_output_var ;
                WHEN var_list = 189 THEN g_html_table(g_record_count).varchar2_col189 := l_output_var ;
                WHEN var_list = 190 THEN g_html_table(g_record_count).varchar2_col190 := l_output_var ;
                ELSE NULL;
                END CASE ;
            END IF;
              IF var_list BETWEEN 191 AND 200 THEN
                CASE
                WHEN var_list = 191 THEN g_html_table(g_record_count).varchar2_col191 := l_output_var ;
                WHEN var_list = 192 THEN g_html_table(g_record_count).varchar2_col192 := l_output_var ;
                WHEN var_list = 193 THEN g_html_table(g_record_count).varchar2_col193 := l_output_var ;
                WHEN var_list = 194 THEN g_html_table(g_record_count).varchar2_col194 := l_output_var ;
                WHEN var_list = 195 THEN g_html_table(g_record_count).varchar2_col195 := l_output_var ;
                WHEN var_list = 196 THEN g_html_table(g_record_count).varchar2_col196 := l_output_var ;
                WHEN var_list = 197 THEN g_html_table(g_record_count).varchar2_col197 := l_output_var ;
                WHEN var_list = 198 THEN g_html_table(g_record_count).varchar2_col198 := l_output_var ;
                WHEN var_list = 199 THEN g_html_table(g_record_count).varchar2_col199 := l_output_var ;
                WHEN var_list = 200 THEN g_html_table(g_record_count).varchar2_col200 := l_output_var ;
                ELSE NULL;
                END CASE ;
                END IF;
        ELSIF l_datatype_temp LIKE 'DATE_COL%' THEN
            l_output_date := trim(l_output_temp);
            date_list := date_list + 1;
            CASE
            WHEN date_list = 1 THEN g_html_table(g_record_count).date_col1 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 2 THEN g_html_table(g_record_count).date_col2 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 3 THEN g_html_table(g_record_count).date_col3 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 4 THEN g_html_table(g_record_count).date_col4 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 5 THEN g_html_table(g_record_count).date_col5 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 6 THEN g_html_table(g_record_count).date_col6 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 7 THEN g_html_table(g_record_count).date_col7 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 8 THEN g_html_table(g_record_count).date_col8 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 9 THEN g_html_table(g_record_count).date_col9 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 10 THEN g_html_table(g_record_count).date_col10 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 11 THEN g_html_table(g_record_count).date_col11 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 12 THEN g_html_table(g_record_count).date_col12 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 13 THEN g_html_table(g_record_count).date_col13 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 14 THEN g_html_table(g_record_count).date_col14 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 15 THEN g_html_table(g_record_count).date_col15 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 16 THEN g_html_table(g_record_count).date_col16 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 17 THEN g_html_table(g_record_count).date_col17 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 18 THEN g_html_table(g_record_count).date_col18 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 19 THEN g_html_table(g_record_count).date_col19 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 20 THEN g_html_table(g_record_count).date_col20 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 21 THEN g_html_table(g_record_count).date_col21 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 22 THEN g_html_table(g_record_count).date_col22 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 23 THEN g_html_table(g_record_count).date_col23 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 24 THEN g_html_table(g_record_count).date_col24 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 25 THEN g_html_table(g_record_count).date_col25 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 26 THEN g_html_table(g_record_count).date_col26 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 27 THEN g_html_table(g_record_count).date_col27 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 28 THEN g_html_table(g_record_count).date_col28 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 29 THEN g_html_table(g_record_count).date_col29 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 30 then g_html_table(g_record_count).date_col30 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 31 THEN g_html_table(g_record_count).date_col31 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 32 THEN g_html_table(g_record_count).date_col32 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 33 THEN g_html_table(g_record_count).date_col33 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 34 THEN g_html_table(g_record_count).date_col34 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 35 THEN g_html_table(g_record_count).date_col35 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 36 THEN g_html_table(g_record_count).date_col36 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 37 THEN g_html_table(g_record_count).date_col37 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 38 THEN g_html_table(g_record_count).date_col38 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 39 THEN g_html_table(g_record_count).date_col39 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 40 THEN g_html_table(g_record_count).date_col40 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 41 THEN g_html_table(g_record_count).date_col41 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 42 THEN g_html_table(g_record_count).date_col42 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 43 THEN g_html_table(g_record_count).date_col43 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 44 THEN g_html_table(g_record_count).date_col44 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 45 THEN g_html_table(g_record_count).date_col45 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 46 THEN g_html_table(g_record_count).date_col46 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 47 THEN g_html_table(g_record_count).date_col47 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 48 THEN g_html_table(g_record_count).date_col48 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 49 THEN g_html_table(g_record_count).date_col49 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 50 THEN g_html_table(g_record_count).date_col50 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 51 THEN g_html_table(g_record_count).date_col51 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 52 THEN g_html_table(g_record_count).date_col52 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 53 THEN g_html_table(g_record_count).date_col53 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 54 THEN g_html_table(g_record_count).date_col54 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 55 THEN g_html_table(g_record_count).date_col55 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 56 THEN g_html_table(g_record_count).date_col56 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 57 THEN g_html_table(g_record_count).date_col57 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 58 THEN g_html_table(g_record_count).date_col58 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 59 THEN g_html_table(g_record_count).date_col59 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 60 then g_html_table(g_record_count).date_col60 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
        WHEN date_list = 61 THEN g_html_table(g_record_count).date_col61 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 62 THEN g_html_table(g_record_count).date_col62 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 63 THEN g_html_table(g_record_count).date_col63 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 64 THEN g_html_table(g_record_count).date_col64 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 65 THEN g_html_table(g_record_count).date_col65 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 66 THEN g_html_table(g_record_count).date_col66 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 67 THEN g_html_table(g_record_count).date_col67 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 68 THEN g_html_table(g_record_count).date_col68 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 69 THEN g_html_table(g_record_count).date_col69 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 70 THEN g_html_table(g_record_count).date_col70 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 71 THEN g_html_table(g_record_count).date_col71 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 72 THEN g_html_table(g_record_count).date_col72 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 73 THEN g_html_table(g_record_count).date_col73 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 74 THEN g_html_table(g_record_count).date_col74 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 75 THEN g_html_table(g_record_count).date_col75 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 76 THEN g_html_table(g_record_count).date_col76 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 77 THEN g_html_table(g_record_count).date_col77 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 78 THEN g_html_table(g_record_count).date_col78 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 79 THEN g_html_table(g_record_count).date_col79 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 80 THEN g_html_table(g_record_count).date_col80 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 81 THEN g_html_table(g_record_count).date_col81 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 82 THEN g_html_table(g_record_count).date_col82 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 83 THEN g_html_table(g_record_count).date_col83 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 84 THEN g_html_table(g_record_count).date_col84 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 85 THEN g_html_table(g_record_count).date_col85 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 86 THEN g_html_table(g_record_count).date_col86 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 87 THEN g_html_table(g_record_count).date_col87 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 88 THEN g_html_table(g_record_count).date_col88 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 89 THEN g_html_table(g_record_count).date_col89 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 90 then g_html_table(g_record_count).date_col90 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
        WHEN date_list = 91 THEN g_html_table(g_record_count).date_col91 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 92 THEN g_html_table(g_record_count).date_col92 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 93 THEN g_html_table(g_record_count).date_col93 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 94 THEN g_html_table(g_record_count).date_col94 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 95 THEN g_html_table(g_record_count).date_col95 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 96 THEN g_html_table(g_record_count).date_col96 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 97 THEN g_html_table(g_record_count).date_col97 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 98 THEN g_html_table(g_record_count).date_col98 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 99 then g_html_table(g_record_count).date_col99 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            WHEN date_list = 100 then g_html_table(g_record_count).date_col100 := to_date(l_output_date, 'YYYY/MM/DD HH:MI:SS AM');
            ELSE NULL;
            END CASE ;

        ELSIF l_datatype_temp LIKE 'NUMBER_COL%' THEN
            l_output_num := trim(l_output_temp);
            num_list :=num_list + 1;
            CASE
               WHEN num_list = 1 THEN g_html_table(g_record_count).number_col1 := l_output_num ;
            WHEN num_list = 2 THEN g_html_table(g_record_count).number_col2 := l_output_num ;
            WHEN num_list = 3 THEN g_html_table(g_record_count).number_col3 := l_output_num ;
            WHEN num_list = 4 THEN g_html_table(g_record_count).number_col4 := l_output_num ;
            WHEN num_list = 5 THEN g_html_table(g_record_count).number_col5 := l_output_num ;
            WHEN num_list = 6 THEN g_html_table(g_record_count).number_col6 := l_output_num ;
            WHEN num_list = 7 THEN g_html_table(g_record_count).number_col7 := l_output_num ;
            WHEN num_list = 8 THEN g_html_table(g_record_count).number_col8 := l_output_num ;
            WHEN num_list = 9 THEN g_html_table(g_record_count).number_col9 := l_output_num ;
            WHEN num_list = 10 THEN g_html_table(g_record_count).number_col10 := l_output_num ;
            WHEN num_list = 11 THEN g_html_table(g_record_count).number_col11 := l_output_num ;
            WHEN num_list = 12 THEN g_html_table(g_record_count).number_col12 := l_output_num ;
            WHEN num_list = 13 THEN g_html_table(g_record_count).number_col13 := l_output_num ;
            WHEN num_list = 14 THEN g_html_table(g_record_count).number_col14 := l_output_num ;
            WHEN num_list = 15 THEN g_html_table(g_record_count).number_col15 := l_output_num ;
            WHEN num_list = 16 THEN g_html_table(g_record_count).number_col16 := l_output_num ;
            WHEN num_list = 17 THEN g_html_table(g_record_count).number_col17 := l_output_num ;
            WHEN num_list = 18 THEN g_html_table(g_record_count).number_col18 := l_output_num ;
            WHEN num_list = 19 THEN g_html_table(g_record_count).number_col19 := l_output_num ;
            WHEN num_list = 20 THEN g_html_table(g_record_count).number_col20 := l_output_num ;
            WHEN num_list = 21 THEN g_html_table(g_record_count).number_col21 := l_output_num ;
            WHEN num_list = 22 THEN g_html_table(g_record_count).number_col22 := l_output_num ;
            WHEN num_list = 23 THEN g_html_table(g_record_count).number_col23 := l_output_num ;
            WHEN num_list = 24 THEN g_html_table(g_record_count).number_col24 := l_output_num ;
            WHEN num_list = 25 THEN g_html_table(g_record_count).number_col25 := l_output_num ;
            WHEN num_list = 26 THEN g_html_table(g_record_count).number_col26 := l_output_num ;
            WHEN num_list = 27 THEN g_html_table(g_record_count).number_col27 := l_output_num ;
            WHEN num_list = 28 THEN g_html_table(g_record_count).number_col28 := l_output_num ;
            WHEN num_list = 29 THEN g_html_table(g_record_count).number_col29 := l_output_num ;
            WHEN num_list = 30 THEN g_html_table(g_record_count).number_col30 := l_output_num ;
        --added by sravani to fix bug# 3314
        WHEN num_list = 31 THEN g_html_table(g_record_count).number_col31 := l_output_num ;
            WHEN num_list = 32 THEN g_html_table(g_record_count).number_col32 := l_output_num ;
            WHEN num_list = 33 THEN g_html_table(g_record_count).number_col33 := l_output_num ;
            WHEN num_list = 34 THEN g_html_table(g_record_count).number_col34 := l_output_num ;
            WHEN num_list = 35 THEN g_html_table(g_record_count).number_col35 := l_output_num ;
            WHEN num_list = 36 THEN g_html_table(g_record_count).number_col36 := l_output_num ;
            WHEN num_list = 37 THEN g_html_table(g_record_count).number_col37 := l_output_num ;
            WHEN num_list = 38 THEN g_html_table(g_record_count).number_col38 := l_output_num ;
            WHEN num_list = 39 THEN g_html_table(g_record_count).number_col39 := l_output_num ;
            WHEN num_list = 40 THEN g_html_table(g_record_count).number_col40 := l_output_num ;
            WHEN num_list = 41 THEN g_html_table(g_record_count).number_col41 := l_output_num ;
            WHEN num_list = 42 THEN g_html_table(g_record_count).number_col42 := l_output_num ;
            WHEN num_list = 43 THEN g_html_table(g_record_count).number_col43 := l_output_num ;
            WHEN num_list = 44 THEN g_html_table(g_record_count).number_col44 := l_output_num ;
            WHEN num_list = 45 THEN g_html_table(g_record_count).number_col45 := l_output_num ;
            WHEN num_list = 46 THEN g_html_table(g_record_count).number_col46 := l_output_num ;
            WHEN num_list = 47 THEN g_html_table(g_record_count).number_col47 := l_output_num ;
            WHEN num_list = 48 THEN g_html_table(g_record_count).number_col48 := l_output_num ;
            WHEN num_list = 49 THEN g_html_table(g_record_count).number_col49 := l_output_num ;
            when num_list = 50 then g_html_table(g_record_count).number_col50 := l_output_num ;
            --Bug #2003
            WHEN num_list = 51 THEN g_html_table(g_record_count).number_col51 := l_output_num ;
            WHEN num_list = 52 THEN g_html_table(g_record_count).number_col52 := l_output_num ;
            WHEN num_list = 53 THEN g_html_table(g_record_count).number_col53 := l_output_num ;
            WHEN num_list = 54 THEN g_html_table(g_record_count).number_col54 := l_output_num ;
            WHEN num_list = 55 THEN g_html_table(g_record_count).number_col55 := l_output_num ;
            WHEN num_list = 56 THEN g_html_table(g_record_count).number_col56 := l_output_num ;
            WHEN num_list = 57 THEN g_html_table(g_record_count).number_col57 := l_output_num ;
            WHEN num_list = 58 THEN g_html_table(g_record_count).number_col58 := l_output_num ;
            WHEN num_list = 59 THEN g_html_table(g_record_count).number_col59 := l_output_num ;
            WHEN num_list = 60 THEN g_html_table(g_record_count).number_col60 := l_output_num ;
            WHEN num_list = 61 THEN g_html_table(g_record_count).number_col61 := l_output_num ;
            WHEN num_list = 62 THEN g_html_table(g_record_count).number_col62 := l_output_num ;
            WHEN num_list = 63 THEN g_html_table(g_record_count).number_col63 := l_output_num ;
            WHEN num_list = 64 THEN g_html_table(g_record_count).number_col64 := l_output_num ;
            WHEN num_list = 65 THEN g_html_table(g_record_count).number_col65 := l_output_num ;
            WHEN num_list = 66 THEN g_html_table(g_record_count).number_col66 := l_output_num ;
            WHEN num_list = 67 THEN g_html_table(g_record_count).number_col67 := l_output_num ;
            WHEN num_list = 68 THEN g_html_table(g_record_count).number_col68 := l_output_num ;
            WHEN num_list = 69 THEN g_html_table(g_record_count).number_col69 := l_output_num ;
            when num_list = 70 then g_html_table(g_record_count).number_col70 := l_output_num ;
              WHEN num_list = 71 THEN g_html_table(g_record_count).number_col71 := l_output_num ;
            WHEN num_list = 72 THEN g_html_table(g_record_count).number_col72 := l_output_num ;
            WHEN num_list = 73 THEN g_html_table(g_record_count).number_col73 := l_output_num ;
            WHEN num_list = 74 THEN g_html_table(g_record_count).number_col74 := l_output_num ;
            WHEN num_list = 75 THEN g_html_table(g_record_count).number_col75 := l_output_num ;
            WHEN num_list = 76 THEN g_html_table(g_record_count).number_col76 := l_output_num ;
            WHEN num_list = 77 THEN g_html_table(g_record_count).number_col77 := l_output_num ;
            WHEN num_list = 78 THEN g_html_table(g_record_count).number_col78 := l_output_num ;
            WHEN num_list = 79 THEN g_html_table(g_record_count).number_col79 := l_output_num ;
            WHEN num_list = 80 THEN g_html_table(g_record_count).number_col80 := l_output_num ;
            WHEN num_list = 81 THEN g_html_table(g_record_count).number_col81 := l_output_num ;
            WHEN num_list = 82 THEN g_html_table(g_record_count).number_col82 := l_output_num ;
            WHEN num_list = 83 THEN g_html_table(g_record_count).number_col83 := l_output_num ;
            WHEN num_list = 84 THEN g_html_table(g_record_count).number_col84 := l_output_num ;
            WHEN num_list = 85 THEN g_html_table(g_record_count).number_col85 := l_output_num ;
            WHEN num_list = 86 THEN g_html_table(g_record_count).number_col86 := l_output_num ;
            WHEN num_list = 87 THEN g_html_table(g_record_count).number_col87 := l_output_num ;
            WHEN num_list = 88 THEN g_html_table(g_record_count).number_col88 := l_output_num ;
            WHEN num_list = 89 THEN g_html_table(g_record_count).number_col89 := l_output_num ;
            when num_list = 90 then g_html_table(g_record_count).number_col90 := l_output_num ;
              WHEN num_list = 91 THEN g_html_table(g_record_count).number_col91 := l_output_num ;
            WHEN num_list = 92 THEN g_html_table(g_record_count).number_col92 := l_output_num ;
            WHEN num_list = 93 THEN g_html_table(g_record_count).number_col93 := l_output_num ;
            WHEN num_list = 94 THEN g_html_table(g_record_count).number_col94 := l_output_num ;
            WHEN num_list = 95 THEN g_html_table(g_record_count).number_col95 := l_output_num ;
            WHEN num_list = 96 THEN g_html_table(g_record_count).number_col96 := l_output_num ;
            WHEN num_list = 97 THEN g_html_table(g_record_count).number_col97 := l_output_num ;
            WHEN num_list = 98 THEN g_html_table(g_record_count).number_col98 := l_output_num ;
            when num_list = 99 then g_html_table(g_record_count).number_col99 := l_output_num ;
            WHEN num_list = 100 THEN g_html_table(g_record_count).number_col100 := l_output_num ;
            ELSE NULL;
            END CASE ;
        END IF;

        l_output_data := SUBSTR(l_output_data,INSTR(l_output_data,'~',1,1)+1);
        l_datatype := SUBSTR(l_datatype,INSTR(l_datatype,',',1,1)+1);
    END LOOP;
    -- Modified to fix commit performance issues on RAC dbs especially
    IF  MOD(g_record_count,g_bulk_html_row_count) = 0 THEN
        bulk_insert_html_table ( p_table_name ) ;
    commit;
    END IF;

    g_record_count := g_record_count + 1 ;

END insert_html_table;

PROCEDURE submit_report(p_report_id      IN NUMBER ,
                        p_request_id     IN NUMBER ,
                        p_user_id        IN NUMBER ,
                        p_process_id     IN NUMBER ,
                        p_report_name    IN VARCHAR2 DEFAULT NULL ,
                        p_result         OUT CLOB ,
                        p_view_sql       IN OUT VARCHAR2 ,
                        p_error_msg      OUT VARCHAR2 ,
                        p_param1         IN VARCHAR2 DEFAULT NULL ,
                        p_param2         IN VARCHAR2 DEFAULT NULL ,
                        p_param3         IN VARCHAR2 DEFAULT NULL ,
                        p_param4         IN VARCHAR2 DEFAULT NULL ,
                        p_param5         IN VARCHAR2 DEFAULT NULL ,
                        p_param6         IN VARCHAR2 DEFAULT NULL ,
                        p_param7         IN VARCHAR2 DEFAULT NULL ,
                        p_param8         IN VARCHAR2 DEFAULT NULL ,
                        p_param9         IN VARCHAR2 DEFAULT NULL ,
                        p_param10        IN VARCHAR2 DEFAULT NULL ,
                        p_param11        IN VARCHAR2 DEFAULT NULL ,
                        p_param12        IN VARCHAR2 DEFAULT NULL ,
                        p_param13        IN VARCHAR2 DEFAULT NULL ,
                        p_param14        IN VARCHAR2 DEFAULT NULL ,
                        p_param15        IN VARCHAR2 DEFAULT NULL ,
                        p_param16        IN VARCHAR2 DEFAULT NULL ,
                        p_param17        IN VARCHAR2 DEFAULT NULL ,
                        p_param18        IN VARCHAR2 DEFAULT NULL ,
                        p_param19        IN VARCHAR2 DEFAULT NULL ,
                        p_param20        IN VARCHAR2 DEFAULT NULL ,
                        p_param21        IN VARCHAR2 DEFAULT NULL ,
                        p_param22        IN VARCHAR2 DEFAULT NULL ,
                        p_param23        IN VARCHAR2 DEFAULT NULL ,
                        p_param24        IN VARCHAR2 DEFAULT NULL ,
                        p_param25        IN VARCHAR2 DEFAULT NULL ,
                        p_param26        IN VARCHAR2 DEFAULT NULL ,
                        p_param27        IN VARCHAR2 DEFAULT NULL ,
                        p_param28        IN VARCHAR2 DEFAULT NULL ,
                        p_param29        IN VARCHAR2 DEFAULT NULL ,
                        p_param30        IN VARCHAR2 DEFAULT NULL ,
                        p_col_process_id IN NUMBER DEFAULT NULL )
IS
  l_stmt LONG ;
  l_conditions LONG ;
  l_tmp_cond   VARCHAR2(8000) := NULL ;
  l_delimiter  VARCHAR2(2)    := NULL ;
  l_view_names VARCHAR2(1000) ;
  l_value1 xxeis.eis_rs_report_conditions.value1%TYPE;
  l_value2 xxeis.eis_rs_report_conditions.value2%TYPE;
  l_param_counter   NUMBER ;
  l_conc_param      VARCHAR2(10000) ; -- Updated by Praveen.P for case# 3533
  l_trig_conc_param VARCHAR2(10000) ; -- KP 23-Dec-2005   -- Updated by Praveen.P for case# 3533
  l_temp_delimiter  VARCHAR2(2);
  l_temp_delimiter1 VARCHAR2(2);
  l_param_value     VARCHAR2(10000) ; -- Updated by Praveen.P for case# 3533
  l_sort            VARCHAR2(4000) ;
  l_quotes           VARCHAR2(4) ;
  l_ind              NUMBER ;
  l_result_str       VARCHAR2(20000) := NULL ;
  l_insertion_status VARCHAR2(1000) ;
  l_process_id       NUMBER         := p_process_id;
  l_result_rec       VARCHAR2(4000) := NULL ;
  l_param_str        VARCHAR2(10000) ; -- Updated by Praveen.P for case# 3533
  l_csv_param_str    VARCHAR2(10000) ;
  l_csv_date_param   VARCHAR2(10000) ;
  --    l_all_params        VARCHAR2(4000) ;
  l_col_names         VARCHAR2(20000) ;
  l_col_delim         VARCHAR2(2) ;
  l_header            VARCHAR2(32000) ; -- Updated by Praveen.P for case# 3533
  l_request_value     VARCHAR2(30) ;
  l_col_name_tmp      VARCHAR2(4000) ;
  l_hint              VARCHAR2(100) ;
  l_is_url_there      BOOLEAN := FALSE ;
  l_no_of_rows        NUMBER  := 0 ;
  l_master_clob_id    NUMBER ;
  l_program_location  VARCHAR2(500) ;
  l_clob_file_counter NUMBER ;
  l_clob_file_str     VARCHAR2(1000);
  l_file_name         VARCHAR2(100) ;
  l_user_name         VARCHAR2(100) ;
  l_seg_column        NUMBER; -- sjampala 11/04/08
  --    l_security_init               NUMBER;
  --    l_security_exist              NUMBER;
  l_seg_col_prefix VARCHAR2 (20);
  l_segment_num    NUMBER;
  --variables for header part
  l_pivot_total_rows VARCHAR2(30) ;
  l_row_fields       VARCHAR2(4000) ;
  l_col_fields       VARCHAR2(4000) ;
  l_page_fields      VARCHAR2(10000) ;
  l_data_fields      VARCHAR2(4000) ;
  l_title            VARCHAR2(100) := p_report_name;
  l_requested_by     VARCHAR2(100) ;
  l_request_date DATE ;
  l_actual_start_date DATE ;
  l_actual_comp_date DATE ;
  l_row_cnt                 NUMBER ;
  l_col_cnt                 NUMBER ;
  l_data_cnt                NUMBER ;
  l_page_cnt                NUMBER ;
  l_col_delimiter           VARCHAR2(1) := NULL ;
  l_row_delimiter           VARCHAR2(1) := NULL ;
  l_page_delimiter          VARCHAR2(1) := NULL ;
  l_data_delimiter          VARCHAR2(1) := NULL ;
  l_param_delimiter         VARCHAR2(1) := NULL ;
  l_free_text               VARCHAR2(4000) ;
  l_view_owner              VARCHAR2(30) ;
  l_source                  VARCHAR2(50) ;
  l_max_rows                NUMBER ;
  l_before_trigger_text     VARCHAR2(4000) ;
  l_after_trigger_text      VARCHAR2(4000) ;
  l_xml_procedure_name      VARCHAR2(4000) ;
  l_calculation_flag        BOOLEAN := FALSE ;
  l_records_counter         NUMBER ;
  l_extended_flag           BOOLEAN := FALSE ;
  l_no_lines_clob           NUMBER  := 0;
  l_no_lines_file           NUMBER  := 0;
  l_no_lines_file_counter   NUMBER  := 0 ;
  l_drill_down_allowed      VARCHAR2(20) ;
  l_varchar_col_cnt         NUMBER ;
  l_hr_cross_business_group VARCHAR2(20) ;
  --  l_rpt_cross_business_group    varchar2(20) ;
  --Added by Nagraj
  l_distinct_flag VARCHAR2(1) := 'N';
  l_distinct_text VARCHAR2(10) ;
  ---Added by ankur
  l_column_data_type VARCHAR2(300);
  --Thease variables added by raju in order to provide pivot html functionality.
  l_pivot_flag BOOLEAN := TRUE;
  l_d_types LONG;
  l_data_formats VARCHAR2(4000) := '';--added by Pravee.P to support multi language date formats in plugin code
  l_utf8_date_language fnd_languages.utf8_date_language%type;
  l_csv_d_types VARCHAR2(4000) := '';--added by Pravee.P to fix Bug#5334 the issue of mismatch in column_names and respective data_types in CSV output
  l_columns LONG;
  l_d_columns LONG;
  l_pivot_page_fields         VARCHAR2(4000) := '';
  l_pivot_row_fields          VARCHAR2(4000) := '';
  l_pivot_data_fields         VARCHAR2(4000) := '';
  l_pivot_column_fields       VARCHAR2(4000) := '';
  l_pivot_data_field_captions VARCHAR2(4000) := '';
  l_pivot_process_stmt CLOB;
  l_pivot_data_subtotals VARCHAR2(4000) := '';
  l_sql_string LONG                     := 'SELECT ';
  l_conditions1 LONG;

  --variable to get segment name and form prompt
  l_apps_col_name        VARCHAR2(2000);
  l_frm_prompt_name      VARCHAR2(2000);
  l_gl_segment_data_type VARCHAR2(2000);
  l_process_count        NUMBER :=0;
  l_table_name           VARCHAR2(300);
  l_outputs xxeis.eis_rs_reports.general_outputs%TYPE;
  l_html_output_flag        BOOLEAN := FALSE ;
  l_portal_html_output_flag BOOLEAN := FALSE ;                           --added by Praveen.P to fix Bug#3474
  l_portal_outputs xxeis.eis_rs_reports.general_outputs%TYPE;  --added by Praveen.P to fix Bug#3474
  l_portal_xml_outputs xxeis.eis_rs_reports.xml_outputs%TYPE ; --added by Praveen.P to fix Bug#3474
  --Added for masking 6.0.2
  l_replace_character       VARCHAR2(10);
  l_do_not_rep_column_type  VARCHAR2(10);
  l_do_not_rep_column_width NUMBER;
  l_clob_count              NUMBER;
  l_view_col_count  NUMBER := 0;
  l_view_val1_count NUMBER := 0;
  l_view_val2_count NUMBER := 0;
  l_param_col_id    NUMBER := 0;
  l_tmp_idx         NUMBER := 0;
  l_range_cond      NUMBER := 0;
  l_tmp_range_cond  VARCHAR2(4000);
  l_tmp_cond_column VARCHAR2(4000);--incresed size to fix buffer too samll error while submitting a report by Praveen.P
  -- Plugin modification variables
  l_session_id  NUMBER;
  l_appl_id     NUMBER;
  l_resp_name   VARCHAR2(100);
  l_module_name VARCHAR2(100);

  -- publish report (rakesh)
  l_publish_flag       NUMBER;
  l_column_list        VARCHAR2(4000);
  l_security_col_list  VARCHAR2(1000);
  l_cntr               NUMBER         := 0;
  l_sort_calc_columns  VARCHAR2(4000) :=NULL;
  l_temp_cal_delimeter VARCHAR2(4000) :=NULL;

  --Start Group By : Declared local variables to use for group by enhancement
  l_group_by             VARCHAR2(5)   :=NULL;
  l_group_by_stmt        VARCHAR2(4000):=NULL;
  l_group_by_dt_st       VARCHAR2(4000):=NULL;
  l_group_by_var_st      VARCHAR2(4000):=NULL;
  l_group_by_pub_st      VARCHAR2(4000):=NULL;
  l_group_by_stmt_delim  VARCHAR2(5)   :=NULL;
  l_group_by_stmt_delim1 VARCHAR2(5)   :=NULL;
  l_sort_col_datatype    VARCHAR2(40)  :=NULL;
  --End Group By :
  --Bug# 5455
  l_skip_condition_flag   VARCHAR2(1) ;
  l_start_time            DATE; --Bug#5857
  l_date_format         xxeis.eis_rs_process_rpt_columns.data_format%TYPE;
  l_data_alignment      VARCHAR2(10000);
  l_pivot_count         NUMBER;
  l_pivot_header        VARCHAR2(32000);
  l_pivot_format        VARCHAR2(100);
  l_pivot_layout        VARCHAR2(100);
  l_pivot_style_options varchar2(100);
  l_summary_page_fields varchar2(10000);
  l_summary_row_fields  varchar2(10000);
  l_summary_data_fields varchar2(10000);
  l_summary_data_field_captions VARCHAR2(10000);
  l_summary_data_subtotals VARCHAR2(10000);
  l_summary_calculations   VARCHAR2(10000);

  l_html_pivot_string      CLOB;
  l_tmp_col_name           VARCHAR2(4000);
  l_group_func_count       NUMBER;
  l_number_format_string   VARCHAR2(100);
  l_date_format_string     VARCHAR2(100);

  l_pivoting               xxeis.eis_rs_process_rpt_pivot_dtls.pivoting%TYPE;
  l_pivot_data_type        xxeis.eis_rs_process_rpt_pivot_dtls.pivot_data_type%TYPE;
  l_rn                     NUMBER;
  l_security_group_id      NUMBER;

  l_update_row_retrieved NUMBER;



  --Cursor is used to check the process column id existence
  CURSOR chk_process_cols_c(c_process_id IN NUMBER)
  IS
    SELECT COUNT(process_id)
    FROM xxeis.eis_rs_process_rpt_columns
    WHERE report_id = p_report_id
    AND process_id  =c_process_id;

  --This cursor gets all process columns for this report
  CURSOR select_process_cols_c(c_process_id IN NUMBER)
  IS
    SELECT errc.column_name,
      errc.column_name orig_column_name,
      NVL(errc.display_name, errc.column_name) display_name,
      errc.calculation_column ,
      errc.derived_flag,
      errc.drill_down_url,
      NVL(errc.data_type,xxeis.eis_rs_process_reports.get_process_column_data_type(c_process_id, errc.view_id, errc.column_name)) data_type, --bug# 6184
      errc.view_id,
      errc.column_id ,
      errc.data_format,
      errc.alignment
    FROM xxeis.eis_rs_process_rpt_columns errc
    WHERE NVL(errc.enabled_flag, 'Y') = 'Y'
    AND process_id = c_process_id
    ORDER BY display_order;

  --This cursor gets all the process pivot columns for this report
  CURSOR pivot_process_cols_c (c_process_id IN NUMBER)
  IS
    SELECT errc.column_name,
      NVL (errc.display_name, errc.column_name) display_name,
      errpd.pivoting,
      errpd.pivot_data_type,
      errpd.pivot_display_name,
      errpd.pivot_column_order
    FROM xxeis.eis_rs_process_rpt_pivots errp,
      xxeis.eis_rs_process_rpt_pivot_dtls errpd,
      xxeis.eis_rs_process_rpt_columns errc
    WHERE errc.process_id            = errpd.process_id
    AND errc.process_id              = errp.process_id
    AND errp.pivot_id                = errpd.pivot_id
    AND errc.column_id               = errpd.column_id
    AND errp.process_id              = c_process_id
    AND NVL (errc.enabled_flag, 'Y') = 'Y'
    ORDER BY errpd.pivoting,
      NVL (errpd.pivot_column_order, 0) DESC,
      errc.display_order;

  --This cursor gets all the process html columns for this report
  CURSOR pivot_process_cols_html_c (c_process_id IN NUMBER)
  IS
    SELECT errc.column_name,
      NVL (errc.display_name, errc.column_name) display_name,
      errpd.pivoting,
      errpd.pivot_data_type,
      errpd.pivot_display_name,
      errpd.pivot_column_order
    FROM xxeis.eis_rs_process_rpt_pivots errp,
      xxeis.eis_rs_process_rpt_pivot_dtls errpd,
      xxeis.eis_rs_process_rpt_columns errc
    WHERE errc.process_id            = errpd.process_id
    AND errc.process_id              = errp.process_id
    AND errp.pivot_id                = errpd.pivot_id
    AND errc.column_id               = errpd.column_id
    AND errp.process_id              = c_process_id
    AND NVL (errc.enabled_flag, 'Y') = 'Y'
    ORDER BY errpd.pivoting,
      NVL (errpd.pivot_column_order, 0) DESC,
      errc.display_order;

  CURSOR get_process_pivots_c
  IS
    SELECT process_id,
      pivot_id,
      report_id,
      pivot_name,
      display_order,
      pivot_layout,
      pivot_style_options
    FROM xxeis.eis_rs_process_rpt_pivots
    WHERE process_id = g_report_process_id
    ORDER BY display_order;

  CURSOR get_process_pivot_dtls_c(c_pivot_id IN NUMBER)
  IS
    SELECT errc.display_name,
      errpd.pivoting,
      errpd.pivot_data_type,
      errpd.pivot_display_name,
      errpd.row_field_settings,
      errpd.data_field_settings,
      errpd.pivot_column_order
    FROM xxeis.eis_rs_process_rpt_pivot_dtls errpd,
      xxeis.eis_rs_process_rpt_columns errc
    WHERE errc.report_id = errpd.report_id
    AND errc.report_id   = errpd.report_id
    AND errc.column_id   = errpd.column_id
    AND errc.process_id  = errpd.process_id
    AND errpd.pivot_id   = c_pivot_id
    AND errpd.process_id = g_report_process_id
    ORDER BY errpd.pivoting,
      errpd.pivot_column_order;

  -- This cursor gets all conditions for this report
  CURSOR conditions_c
  IS
    SELECT errc.column_name,
      errc.operator_name,
      errc.value1,
      errc.value2,
      errc.param_based_condition
    FROM xxeis.eis_rs_process_rpt_conditions errc, --6.04
        xxeis.eis_rs_reports err
    WHERE errc.report_id           = err.report_id
    AND NVL(errc.enable_flag, 'Y') = 'Y'
    AND err.report_id              = p_report_id
    AND errc.free_text            IS NULL
    AND errc.process_id            = g_report_process_id --6.04
    ORDER BY display_order ;

  --This cursor gets all parameters for the respective report
  --Updated by Praveen.P to fix Bug# 4672
  CURSOR report_params_c
  IS
    SELECT errp.parameter_name,
      errp.display_order,
      errp.data_type
    FROM xxeis.eis_rs_process_rpt_parameters errp
    WHERE errp.report_id            = p_report_id
    AND NVL(errp.enabled_flag, 'Y') = 'Y'
    AND errp.process_id             = g_report_process_id -- 6.04
      --ORDER BY display_order ;
    ORDER BY LENGTH(parameter_name) DESC;

  CURSOR parameters_c
  IS
    SELECT errp.parameter_name,
      errp.display_order,
      errp.data_type
    FROM xxeis.eis_rs_process_rpt_parameters errp
    WHERE errp.report_id            = p_report_id
    AND NVL(errp.enabled_flag, 'Y') = 'Y'
    AND errp.process_id             = g_report_process_id -- 6.04
    ORDER BY display_order ;

  -- This cursor gets sort columns for this report
  CURSOR sort_c
  IS
  SELECT errc.column_name,
    ers.free_text,
    ers.asc_or_dsc,
    errc.view_id,
    ers.column_id ,
    errc.calculation_column,
    ers.sort_order
  FROM xxeis.eis_rs_process_rpt_columns errc,
    xxeis.eis_rs_process_rpt_sorts ers
  WHERE errc.report_id = ers.report_id
  AND errc.column_id   = ers.column_id
  AND errc.process_id  = ers.process_id
  AND ers.process_id   = g_report_process_id
  AND ers.report_id    = p_report_id
  UNION
  SELECT NULL column_name,
    ers.free_text,
    ers.asc_or_dsc,
    NULL view_id,
    NULL column_id ,
    NULL calculation_column,
    ers.sort_order
  FROM xxeis.eis_rs_process_rpt_sorts ers
  WHERE ers.free_text IS NOT NULL
  AND ers.process_id = g_report_process_id
  ORDER BY sort_order;

  --Get all views other than main view
  CURSOR views_c ( p_process_id IN NUMBER )
  IS
    SELECT DISTINCT ev.view_owner
      ||'.'
      || ev.view_name
      || ' '
      || evc.alias_name view_name
    FROM xxeis.eis_rs_reports er,
      xxeis.eis_rs_process_rpt_columns erc,
      xxeis.eis_rs_views ev,
      xxeis.eis_rs_view_components evc
    WHERE er.report_id        = erc.report_id
    AND erc.view_id           = ev.view_id
    AND er.view_id            = evc.view_id
    AND evc.view_component_id = erc.view_component_id --Fixed as part of dff issue
    AND ev.view_name          = evc.component_name
    AND er.report_id          = p_report_id
    AND erc.process_id        = p_process_id
    AND er.view_id           <> erc.view_id;
TYPE plsql_tbl_def
IS
  TABLE OF parameters_c%ROWTYPE INDEX BY BINARY_INTEGER;
  --pl/sql table for holding parameter cursor values
  param_tbl plsql_tbl_def ;
  result_c cursor_type;
  l_query_cursor VARCHAR2(100);
  -- 6.0.1 added variables
  l_xml_outputs xxeis.eis_rs_reports.xml_outputs%TYPE ;
  l_global_security_count NUMBER := 0; --Issue 785
  l_report_view_id        NUMBER;
  l_report_view_alias xxeis.eis_rs_views.view_alias%TYPE ;
  l_component_alias xxeis.eis_rs_view_components.alias_name%TYPE;
BEGIN
  g_log := g_log || chr(10) || 'Started the execution of submit report ' ;

  g_param_tbl.DELETE ;
  g_param_tbl(1)  :=p_param1;
  g_param_tbl(2)  :=p_param2;
  g_param_tbl(3)  :=p_param3;
  g_param_tbl(4)  :=p_param4;
  g_param_tbl(5)  :=p_param5;
  g_param_tbl(6)  :=p_param6;
  g_param_tbl(7)  :=p_param7;
  g_param_tbl(8)  :=p_param8;
  g_param_tbl(9)  :=p_param9;
  g_param_tbl(10) :=p_param10;
  g_param_tbl(11) :=p_param11;
  g_param_tbl(12) :=p_param12;
  g_param_tbl(13) :=p_param13;
  g_param_tbl(14) :=p_param14;
  g_param_tbl(15) :=p_param15;
  g_param_tbl(16) :=p_param16;
  g_param_tbl(17) :=p_param17;
  g_param_tbl(18) :=p_param18;
  g_param_tbl(19) :=p_param19;
  g_param_tbl(20) :=p_param20;
  g_param_tbl(21) :=p_param21;
  g_param_tbl(22) :=p_param22;
  g_param_tbl(23) :=p_param23;
  g_param_tbl(24) :=p_param24;
  g_param_tbl(25) :=p_param25;
  g_param_tbl(26) :=p_param26;
  g_param_tbl(27) :=p_param27;
  g_param_tbl(28) :=p_param28;
  g_param_tbl(29) :=p_param29;
  g_param_tbl(30) :=p_param30;
  -- Till here bug#1338 , 1339 fix

  BEGIN
    SELECT actual_start_date
    INTO l_start_time
    FROM fnd_concurrent_requests
    WHERE request_id = p_request_id;
  EXCEPTION WHEN OTHERS THEN
    NULL;
  END;

  xxeis.eis_rs_insert.eis_rs_processes( p_process_id => l_process_id ,
                                        p_request_id => p_request_id ,
                                        p_start_time => nvl(l_start_time , sysdate), -- bug#5857
                                        p_report_id => p_report_id ,
                                        p_status => 'P' ,
                                        p_auto_commit => 'Y' ,
                                        p_operation_type => 'U' ,
                                        p_insertion_status => l_insertion_status ) ;

  IF INSTR(l_insertion_status, 'Operation failed.') > 0 THEN
    g_log := g_log || chr(10) || 'Updation errors after processes update failure.. ';

    INSERT INTO xxeis.eis_rs_errors
      ( error_id,
        process_id,
        report_id,
        error_message,
        status,
        program_location
      )
      VALUES
      ( xxeis.eis_rs_errors_s.NEXTVAL,
        l_process_id,
        p_report_id,
        l_insertion_status,
        'U',
        l_program_location
      );
   -- COMMIT ;
  END IF ;

  IF l_process_id IS NOT NULL THEN -- l_process_id is null for View SQL
    -- Added for Muti-org/cross sob changes
    SELECT session_id
    INTO l_session_id
    FROM xxeis.eis_rs_processes
    WHERE process_id = l_process_id;
    xxeis.eis_rs_utility.lov_initialize(l_session_id,l_process_id);
    set_date_params ( l_process_id, 'canonical_to_date') ;
  END IF;

  --call procedure to check security where clause for published report only (Rakesh 10/28/09)
  BEGIN
    SELECT COUNT(*)
    INTO l_publish_flag
    FROM xxeis.eis_rs_process_rpt_pgs
    where process_id = l_process_id
    AND enable_flag  = 'Y'; -- bug# 3187 fix
  EXCEPTION WHEN OTHERS THEN
    l_publish_flag := 0;
  END;

  l_column_list    := NULL;
  IF l_publish_flag > 0 THEN
    l_column_list  := xxeis.eis_rs_publish_report_pkg.get_column_list(l_process_id);
    l_group_by_pub_st := l_column_list; --Added for bug# 4994 fix
  END IF;

  IF l_column_list IS NOT NULL THEN
    l_column_list  := ', '||l_column_list ||' publish_cols ';
  ELSE
    l_column_list := ', null' ||' publish_cols ';
  END IF;

  g_process_id        := l_process_id ;
  g_report_process_id := l_process_id ;
  xxeis.eis_rs_utility.process_log ( l_process_id, g_log ) ;
  g_log              := NULL ;
  l_program_location := 'start of submit report';
  g_log              := g_log||chr(10)||' g_report_process_id : '||g_report_process_id;
  --get all columns for generating report query
  l_temp_delimiter   := NULL ;
  l_temp_delimiter1  := NULL ;
  l_col_cnt          := 0 ;
  l_varchar_col_cnt  :=0;
  l_delimiter        := NULL ;
  l_col_delimiter    := NULL ;
  g_log              := g_log || chr(10) || 'Building column names...' ;
  l_program_location := 'Constructing column names';

  BEGIN --Added for HR Cross Business
    SELECT DECODE ( NVL(upper(PARAMETER_VALUE), 'NO'), 'NO', 'N', 'YES', 'Y', upper(PARAMETER_VALUE) )
    INTO l_hr_cross_business_group
    FROM xxeis.eis_rs_admin_parameters
    WHERE upper(parameter_name) LIKE UPPER('hr_cross_business_group');
    --Check for Global Security profile Issue 785
    SELECT COUNT(*)
    INTO l_global_security_count
    FROM per_security_profiles
    WHERE security_profile_id    = fnd_profile.value('PER_SECURITY_PROFILE_ID')
    AND business_group_id       IS NULL
    AND view_All_employees_flag  = 'Y'
    AND view_all_flag            = 'Y' ;
    IF l_global_security_count   > 0 THEN
      l_hr_cross_business_group := 'Y';
    END IF;
  EXCEPTION WHEN OTHERS THEN
    l_hr_cross_business_group := 'N' ;
  END;
--Updated by Praveen.P to fix Bug# 4532
   /*  BEGIN
     SELECT cross_business_group
        INTO l_rpt_cross_business_group
        FROM xxeis.eis_rs_reports where report_id = p_report_id;
    END;
    */

  BEGIN
    IF l_hr_cross_business_group IS NOT NULL THEN
      fnd_profile.put ( 'HR_CROSS_BUSINESS_GROUP', l_hr_cross_business_group);
    END IF;
  END;

  BEGIN --Bug 136 Added KP 7/17/2004
    SELECT DECODE ( NVL(upper(parameter_value), 'YES'), 'ON', 'YES', 'OFF', 'NO', upper(parameter_value) )
    INTO l_drill_down_allowed
    FROM xxeis.eis_rs_admin_parameters
    WHERE upper(parameter_name) LIKE UPPER('drilldown_allowed');
  EXCEPTION WHEN OTHERS THEN
    l_drill_down_allowed := 'YES' ;
  END;

  -- added by rakesh lathiya
  /*BEGIN
    SELECT 'Y'
    INTO g_gl_segment_check
    FROM xxeis.eis_rs_reports
    where report_id         = p_report_id;
    --AND segment_prompt_flag = 'Y';
    --   AND application_id = 101;  commented by Saritha Jampala 31-Dec-2005
  EXCEPTION WHEN NO_DATA_FOUND THEN
    g_gl_segment_check := 'N';
  END;

  IF g_gl_segment_check = 'Y' THEN
    get_segment_prompt (p_app_col_name => l_apps_col_name ,
                        p_form_left_prompt => l_frm_prompt_name ,
                        p_report_id => p_report_id ,
                        p_process_id => p_process_id ,
                        p_col_count => g_gl_segment_count ,
                        p_segment_data_type => l_gl_segment_data_type
    ); -- Added KP 22-Dec-2005
  END IF;*/

  -- all the segment population logic is moved to eis_gl_security_pkg in order to centralize the code.
  l_d_types      := '';
  l_data_formats := '';
  l_csv_d_types  := ''; --added by Pravee.P to fix Bug# 5334 the issue of mismatch in column_names and respective data_types in CSV output

  IF p_view_sql = 'SQL' AND p_col_process_id <> 0 THEN
    l_process_id        := p_col_process_id;
    g_report_process_id := p_col_process_id ; -- This is setting g_report_process_id for view_sql scenario
  END IF;

  OPEN chk_process_cols_c(c_process_id => l_process_id);
  FETCH chk_process_cols_c INTO l_process_count;
  CLOSE chk_process_cols_c;

  -- Modified to fix bug# 6884
  SELECT err.report_name,
    erv.view_name,
    hint_name,
    view_owner,
    NVL ( erpr.distinct_flag ,'N'),
    err.view_id,
    erv.view_alias,
    NVL(erpr.group_by_flag,'N')
  INTO l_title,
    l_view_names,
    l_hint,
    l_view_owner,
    l_distinct_flag,
    l_report_view_id,
    l_report_view_alias,
    l_group_by
  FROM xxeis.eis_rs_process_rpts erpr,
    xxeis.eis_rs_reports err,
    xxeis.eis_rs_views erv
  WHERE erpr.view_id  = erv.view_id
  AND err.view_id     = erv.view_id
  AND erpr.report_id = err.report_id
  AND erpr.process_id = l_process_id;

  BEGIN
    SELECT utf8_date_language
    INTO l_utf8_date_language
    FROM fnd_languages fl,
      xxeis.eis_rs_sessions ers
    WHERE ers.language_code = fl.language_code
    AND ers.session_id      = l_session_id;
  EXCEPTION WHEN OTHERS THEN
    NULL;
  END ;

  IF l_process_count <> 0 THEN
    FOR select_cols_rec IN select_process_cols_c(c_process_id => l_process_id)
    LOOP
      l_col_cnt := l_col_cnt + 1 ;

      --Append view/component alias
      IF select_cols_rec.view_id IS NOT NULL THEN
        IF select_cols_rec.view_id = l_report_view_id THEN
          IF l_report_view_alias IS NOT NULL THEN
            select_cols_rec.column_name := l_report_view_alias||'.'|| select_cols_rec.column_name;
          END IF;
        ELSE -- View Component
          BEGIN
            SELECT evc.alias_name
            INTO l_component_alias
            FROM xxeis.eis_rs_reports er,
              xxeis.eis_rs_process_rpt_columns erc,
              xxeis.eis_rs_views ev,
              xxeis.eis_rs_view_components evc
            WHERE er.report_id = erc.report_id
            AND erc.view_id    = ev.view_id
            AND er.view_id     = evc.view_id
            AND erc.view_component_id      = evc.view_component_id
            AND er.report_id               = p_report_id
            AND erc.column_id              = select_cols_rec.column_id
            AND erc.process_id             = l_process_id ;
            IF l_component_alias IS NOT NULL THEN
              select_cols_rec.column_name := l_component_alias||'.'|| select_cols_rec.column_name;
            END IF;
          EXCEPTION WHEN OTHERS THEN
            NULL;
          END ;
        END IF;
      END IF; -- select_cols_rec.view_id IS NOT NULL

      -- To get ICX Numeric Character and Date Formats
      BEGIN
        xxeis.eis_rs_utility.get_icx_formats(l_number_format_string,
                                             l_date_format_string);
      EXCEPTION WHEN OTHERS THEN
        NULL;
      END;

      l_pivoting := NULL;
      l_pivot_data_type := NULL;
      BEGIN
        SELECT errpd.pivot_data_type
        INTO l_pivot_data_type
        FROM xxeis.eis_rs_process_rpt_columns errc,
          xxeis.eis_rs_process_rpt_pivots errp,
          xxeis.eis_rs_process_rpt_pivot_dtls errpd
        WHERE errc.process_id         = errp.process_id
        AND errp.process_id           = errpd.process_id
        AND errp.pivot_id             = errpd.pivot_id
        AND errc.column_id            = errpd.column_id
        AND errc.column_id            = select_cols_rec.column_id
        AND NVL(errp.display_order,1) = 1
        AND errc.process_id           = l_process_id;
      EXCEPTION WHEN OTHERS THEN
        NULL;
      END;


      /*IF l_pivoting = 'DATA_FIELD' AND l_pivot_data_type = 'SUM' THEN
        l_d_types := l_d_types || ',NUMBER';
        l_data_formats := l_data_formats||NVL(select_cols_rec.data_format,'~~~0')||'^^';
        --added by Pravee.P to support multi language date formats in plugin code
      ELS*/IF select_cols_rec.data_type = 'VARCHAR2' THEN
        l_data_formats := l_data_formats||'^^';
        IF l_varchar_col_cnt < 200 THEN
          l_d_types := l_d_types || ',' || select_cols_rec.data_type;
          l_varchar_col_cnt := l_varchar_col_cnt+1;
        END IF;
      ELSIF select_cols_rec.data_type   = 'DATE' THEN
        l_d_types := l_d_types || ',' || select_cols_rec.data_type;
        IF select_cols_rec.data_format IS NULL THEN
          l_date_format := g_date_format;  -- Reporting Date Format admin parameter value
        ELSIF select_cols_rec.data_format = 'USER_PREFERENCE_DATE_FORMAT' THEN
          l_date_format := l_date_format_string;  --ICX Date Format
        ELSE
          l_date_format := select_cols_rec.data_format;
        END IF;

        IF l_utf8_date_language = 'NUMERIC DATE LANGUAGE' THEN
          l_data_formats := l_data_formats||REPLACE(REPLACE(l_date_format, 'R', 'Y'),'MON','MM')||'^^';
        ELSE
          l_data_formats := l_data_formats||REPLACE(REPLACE(REPLACE(REPLACE(l_date_format, 'R', 'Y'),'Month','MMMM'),'Mon','Mmm'),'MON','MMM')||'^^';
        END IF;
      ELSIF select_cols_rec.data_type != 'VARCHAR2' THEN
        l_d_types := l_d_types || ',' || select_cols_rec.data_type;
        --Bug# 5622 fix
        IF select_cols_rec.data_type = 'NUMBER' THEN
          IF instr(select_cols_rec.data_format, '~T~D~') > 0 THEN
              select_cols_rec.data_format := replace(select_cols_rec.data_format,'T~D', l_number_format_string);
          END IF;
          l_data_formats := l_data_formats||NVL(select_cols_rec.data_format,'~~~0')||'^^';
        ELSE
          l_data_formats := l_data_formats||'^^';
        END IF;
      END IF;

      --added by Pravee.P to fix Bug# 5334 the issue of mismatch in column_names and respective data_types in CSV output
      /*IF l_pivoting = 'DATA_FIELD' AND l_pivot_data_type = 'SUM' THEN
        l_csv_d_types := l_csv_d_types || ',NUMBER';
        --added by Pravee.P to support multi language date formats in plugin code
      ELS*/IF select_cols_rec.data_type  = 'VARCHAR2' THEN
        l_csv_d_types := l_csv_d_types || ',' || select_cols_rec.data_type;
      ELSIF select_cols_rec.data_type  = 'DATE' THEN
        l_csv_d_types := l_csv_d_types || ',' || select_cols_rec.data_type;
      ELSIF select_cols_rec.data_type != 'VARCHAR2' THEN
        l_csv_d_types := l_csv_d_types || ',' || select_cols_rec.data_type;
      END IF;

      --IF l_pivot_flag THEN
        --Updated by Praveen.P to fix the issue HTML output is blank when there are more than 50 varchar columns in a report
        IF((select_cols_rec.data_type = 'VARCHAR2' AND l_varchar_col_cnt < 201) OR (select_cols_rec.data_type != 'VARCHAR2')) THEN
          l_columns := l_columns || ',' || select_cols_rec.display_name;
          IF(l_varchar_col_cnt = 200) THEN
            l_varchar_col_cnt := l_varchar_col_cnt+1;
            --bug#2003
          END IF;
        END IF;
        --  END IF;
        IF select_cols_rec.calculation_column IS NULL THEN
          l_d_columns := l_d_columns || ',' || select_cols_rec.column_name;
        ELSE
          l_d_columns := l_d_columns || ',' || select_cols_rec.calculation_column;
        END IF;
     -- END IF;

      l_column_data_type := select_cols_rec.data_type;

      IF trim (l_column_data_type) ='DATE' THEN
        l_col_name_tmp := 'TO_CHAR ('|| select_cols_rec.column_name||' ,''YYYY/MM/DD HH:MI:SS AM'')';
      ELSIF trim (l_column_data_type) = 'NUMBER' THEN
        --Start Group By : Applying sum if pivot dataype is null
        IF l_group_by = 'Y' THEN

          -- To handle calculation column when group by is checked
          IF select_cols_rec.calculation_column IS NOT NULL THEN
            l_tmp_col_name := select_cols_rec.calculation_column;
          ELSE
            l_tmp_col_name := select_cols_rec.column_name;
          END IF;

          -- Bug# 6940 fix
          SELECT INSTR(LOWER(REPLACE(select_cols_rec.calculation_column,' ','')),'sum(')
          +
          INSTR(LOWER(REPLACE(select_cols_rec.calculation_column,' ','')),'avg(')
          +
          INSTR(LOWER(REPLACE(select_cols_rec.calculation_column,' ','')),'count(')
          +
          INSTR(LOWER(REPLACE(select_cols_rec.calculation_column,' ','')),'min(')
          +
          INSTR(LOWER(REPLACE(select_cols_rec.calculation_column,' ','')),'max(')
          INTO l_group_func_count
          FROM DUAL;

          --  Applying group function on calculation columns only when group function is NOT already included in calculation column
          IF l_group_func_count IS NULL OR l_group_func_count = 0 THEN
            IF l_pivot_data_type IS NOT NULL THEN
              l_col_name_tmp := l_pivot_data_type||'('||l_tmp_col_name||')';
            ELSE
              l_col_name_tmp := 'SUM('||l_tmp_col_name||')';
            END IF;
          ELSE
             l_col_name_tmp := l_tmp_col_name;
          END IF;
        ELSE
          l_col_name_tmp := select_cols_rec.column_name;
        END IF;
      ELSE
        l_col_name_tmp := select_cols_rec.column_name;
      END IF ;

      -- Start of Group By clause logic
      IF trim (l_column_data_type) <> 'NUMBER' THEN

       --Start Group By :  To use calculation text for VARCHAR2 columns in the group by clause  else just use column name
        IF l_group_by = 'Y' THEN

          IF select_cols_rec.calculation_column IS NOT NULL THEN
            l_group_by_var_st := l_group_by_var_st||l_group_by_stmt_delim1||select_cols_rec.calculation_column;
          ELSE
            l_group_by_var_st := l_group_by_var_st||l_group_by_stmt_delim1||select_cols_rec.column_name;

          END IF;
          l_group_by_stmt_delim1:=',';
        END IF;
      END IF;
      -- End of Group By logic


      IF select_cols_rec.derived_flag = 'Y' THEN

    IF l_group_by <> 'Y' OR (l_group_by = 'Y' AND  l_column_data_type<> 'NUMBER') THEN
      l_col_name_tmp := select_cols_rec.calculation_column ;
    END IF;
        l_calculation_flag := TRUE ;
      END IF ; --end of derived flag if condition

      --Masking
      l_replace_character := NULL ; --mask character
      check_if_masked( select_cols_rec.orig_column_name, p_report_id, l_replace_character, l_do_not_rep_column_type, l_do_not_rep_column_width ) ;
      IF l_replace_character IS NOT NULL THEN -- Mask the field
        l_col_name_tmp := 'xxeis.eis_rs_utility.mask('||l_col_name_tmp||', '''||l_replace_character||''',''' || l_do_not_rep_column_type || ''','''|| l_do_not_rep_column_width || ''')';
      END IF ;

      l_col_names := l_col_names||l_col_delim||rtrim(ltrim(select_cols_rec.display_name)) ;
      IF p_view_sql != 'SQL' THEN
        -- Include replace statments for varcha2 datatype in order to replace ~ with space bug 3667
        IF trim (l_column_data_type) = 'VARCHAR2' THEN
          l_col_name_tmp := '^&*'||l_col_name_tmp||'*&^';
        END IF; -- replace ~ with space logic
        IF select_cols_rec.drill_down_url IS NOT NULL AND l_drill_down_allowed = 'YES' THEN
          ---Added to Support encrypt
          select_cols_rec.drill_down_url := REPLACE ( select_cols_rec.drill_down_url, 'encrypt', 'xxeis.eis_rs_utility.x') ;
          l_stmt := l_stmt||l_temp_delimiter
                          ||l_temp_delimiter1
                          ||l_delimiter
                          ||l_temp_delimiter1
                          ||l_temp_delimiter
                          ||''''||'=HYPERLINK("'
                          ||get_url( select_cols_rec.drill_down_url, p_request_id )
                          ||'||'
                          ||''''
                          ||'"^"'
                          ||''''
                          ||'||REPLACE('                        -- Bug 130 - KP - 7/17/2004
                          ||l_col_name_tmp
                          ||', ''"'' )||'||''''||'")'||'''' ;
          l_is_url_there := TRUE ;
        ELSE
          l_stmt := l_stmt||l_temp_delimiter
                          ||l_temp_delimiter1
                          ||l_delimiter
                          ||l_temp_delimiter1
                          ||l_temp_delimiter
                          ||l_col_name_tmp ;
        END IF ;
        l_delimiter       := '~' ;
        l_temp_delimiter  := '||' ;
        l_temp_delimiter1 := '''' ;
        l_col_delim       := '~' ;


    fnd_file.put_line(fnd_file.log,'l_stmt..after downscroll url..'||l_stmt);
      ELSE
        l_stmt := l_stmt||l_delimiter||l_col_name_tmp ;
        l_delimiter := ', ' ;
        fnd_file.put_line(fnd_file.log,'l_stmt..after else url..'||l_stmt);
      END IF ; --condition checking view sql ends here

      IF select_cols_rec.alignment IS NOT NULL AND LENGTH (select_cols_rec.alignment) > 0 THEN
        l_data_alignment := l_data_alignment||','||select_cols_rec.alignment;
      END IF;

    END LOOP ; --end of select_col_c cursor

    IF l_data_alignment IS NOT NULL AND LENGTH (l_data_alignment) >0 THEN
      l_data_alignment  := SUBSTR(l_data_alignment,2);
    END IF;

    --Start Group By :Concatenate varchar2 and date group by statements
    IF l_group_by = 'Y' THEN
      /*IF l_group_by_dt_st IS NOT NULL AND l_group_by_var_st IS NOT NULL THEN
        l_group_by_stmt := l_group_by_var_st||','||l_group_by_dt_st;
      ELSIF l_group_by_dt_st  IS NOT NULL THEN
        l_group_by_stmt := l_group_by_dt_st;
      ELSIF l_group_by_var_st IS NOT NULL THEN
        l_group_by_stmt := l_group_by_var_st;
      END IF;*/

      l_group_by_stmt := l_group_by_var_st;

      --added by Sravani.M to fix bug$ 4994
      IF l_group_by_pub_st IS NOT NULL THEN
        l_group_by_stmt := l_group_by_stmt||','||l_group_by_pub_st;
      END IF;
    END IF;
    --End Group By :
  END IF;

  --Added for bug# 4259 fix
  FOR sort_cal_column IN sort_c
  LOOP
    IF sort_cal_column.calculation_column IS NOT NULL THEN
      IF NVL(l_group_by,'N') <> 'Y' THEN
        l_sort_calc_columns :=l_sort_calc_columns || l_temp_cal_delimeter ||sort_cal_column.calculation_column ||' '||sort_cal_column.asc_or_dsc;
        --Start group by
      ELSIF l_group_by = 'Y' THEN
        l_sort_col_datatype := xxeis.eis_rs_process_reports.get_process_column_data_type(p_process_id,SORT_CAL_COLUMN.view_id,SORT_CAL_COLUMN.column_name); --bug# 6184
        IF l_sort_col_datatype <> 'NUMBER' THEN
          l_sort_calc_columns := l_sort_calc_columns||l_temp_cal_delimeter||sort_cal_column.calculation_column||' '||sort_cal_column.asc_or_dsc ;
        END IF;
        --End Group By :
      END IF;
      l_temp_cal_delimeter :=',';
    END IF;

  END LOOP;

  --Updated by Praveen.P to fix the issue HTML output is blank when there are more than 50 varchar columns in a report  Bug# 4532
  -- Added KP 22-Dec-2005
  IF l_frm_prompt_name IS NOT NULL THEN
    l_columns := l_columns || ',' || REPLACE(l_frm_prompt_name, '~', ',') ;
  ELSIF l_gl_segment_data_type = 'VARCHAR2' AND l_varchar_col_cnt < 201 AND l_frm_prompt_name IS NOT NULL THEN
    l_varchar_col_cnt := l_varchar_col_cnt+1;
    l_d_types := l_d_types || ',' || l_gl_segment_data_type ;
  END IF ;

  --added by Pravee.P to fix the issue of mismatch in column_names and respective data_types in CSV output
  IF l_gl_segment_data_type = 'VARCHAR2' AND l_frm_prompt_name IS NOT NULL THEN
    l_csv_d_types := l_csv_d_types || ',' || l_gl_segment_data_type ;
  END IF ;

  /*IF g_gl_segment_check = 'Y' THEN
    l_stmt := l_stmt||l_temp_delimiter
                    ||l_temp_delimiter1
                    ||l_delimiter
                    ||l_temp_delimiter1
                    ||l_temp_delimiter
                    ||l_apps_col_name;
    l_col_names := l_col_names||l_col_delim ||l_frm_prompt_name;
  END IF;*/

  IF l_d_types IS NOT NULL THEN
    l_d_types  := SUBSTR(l_d_types,2);
  END IF;

  --added by Pravee.P to fix Bug# 5334 the issue of mismatch in column_names and respective data_types in CSV output
  IF l_csv_d_types IS NOT NULL THEN
    l_csv_d_types := SUBSTR(l_csv_d_types,2);
  END IF;
  g_log := g_log || chr(10) || 'Getting pivot details...' ;
  l_program_location := 'Getting pivot data';

  IF l_process_count   <> 0 THEN
    FOR pivot_cols_rec IN pivot_process_cols_html_c(c_process_id => l_process_id)
    LOOP
     -- IF l_pivot_flag THEN
        IF upper(TRIM(pivot_cols_rec.pivoting)) = 'DATA_FIELD' THEN
          l_pivot_data_fields := l_pivot_data_fields || ',' || rtrim(ltrim(pivot_cols_rec.display_name));
          l_pivot_data_field_captions := l_pivot_data_field_captions || ',' || NVL(rtrim(ltrim(pivot_cols_rec.pivot_display_name)),rtrim(ltrim(pivot_cols_rec.display_name)));
          l_pivot_data_subtotals := l_pivot_data_subtotals || ',' || rtrim(ltrim(NVL(pivot_cols_rec.pivot_data_type, 'SUM')));
          l_data_delimiter := ',' ;
        ELSIF upper(TRIM(pivot_cols_rec.pivoting)) = 'ROW_FIELD' THEN
          l_pivot_row_fields := l_pivot_row_fields||l_row_delimiter||rtrim(ltrim(pivot_cols_rec.display_name)) ;
          l_row_delimiter := ',' ;
        END IF ;
     -- END IF;
    END LOOP ;
  END IF;

 /* IF g_gl_segment_check = 'Y' THEN
    l_page_fields := l_page_fields||l_page_delimiter||REPLACE(l_frm_prompt_name, '~', ',');
  END IF;*/
  l_ind := 0;
  l_program_location := 'storing report parameters in pl/sql table';
  FOR parameters_rec IN report_params_c
  LOOP
    param_tbl(l_ind).parameter_name := parameters_rec.parameter_name ;
    param_tbl(l_ind).display_order  := parameters_rec.display_order ;
    param_tbl(l_ind).data_type      := parameters_rec.data_type ;
    l_ind                           := l_ind + 1;
  END LOOP ;

  --get the free text for conditions if at all user enters it
  BEGIN
    SELECT errc.free_text
    INTO l_free_text
    FROM xxeis.eis_rs_process_rpt_conditions errc --6.04
    WHERE errc.report_id = p_report_id
    AND errc.free_text  IS NOT NULL
    AND errc.process_id  = g_report_process_id --6.04
    AND ROWNUM < 2;
    IF l_free_text IS NOT NULL THEN
      l_free_text := ' '||l_free_text||' ' ;
    END IF ;
  EXCEPTION WHEN OTHERS THEN
    l_conditions := NULL ;
  END ;

  -- IF l_conditions IS NULL THEN
  l_conditions       := 'WHERE 1=1';
  l_param_counter    := 1 ;
  g_log              := g_log || chr(10) || 'Building conditions...' ;
  l_program_location := 'Construncting conditions';
  xxeis.eis_rs_pre_process_reports.create_process_cond_comp_joins ( l_process_id, p_report_id );
  FOR conditions_rec IN conditions_c
  LOOP
    --Fix for the addition of DFF columns from the components by adding proper alias
    l_view_col_count  := 0;
    l_view_val1_count := 0;
    l_view_val2_count := 0;
    l_component_alias := NULL;
    l_range_cond      :=0;

    IF INSTR(conditions_rec.column_name,'.') = 0 THEN
      SELECT COUNT(*)
      INTO l_view_col_count
      FROM xxeis.eis_rs_view_columns
      WHERE column_name = conditions_rec.column_name
      AND view_id       = l_report_view_id;

      SELECT COUNT(*)
      INTO l_view_val1_count
      FROM xxeis.eis_rs_view_columns
      WHERE column_name = conditions_rec.value1
      AND view_id       = l_report_view_id;

      SELECT COUNT(*)
      INTO l_view_val2_count
      FROM xxeis.eis_rs_view_columns
      WHERE column_name             = conditions_rec.value2
      AND view_id                   = l_report_view_id;

      IF l_view_col_count           > 0 AND l_report_view_alias IS NOT NULL THEN
        conditions_rec.column_name := l_report_view_alias||'.'|| conditions_rec.column_name;
      ELSE -- View Component
        IF instr(conditions_rec.value1,':')>0 AND conditions_rec.value1 NOT LIKE ':SYSTEM%'
           AND conditions_rec.column_name NOT LIKE '%.%' THEN
          l_param_col_id := NULL ;
          BEGIN
            SELECT column_id
            INTO l_param_col_id
            FROM xxeis.eis_rs_process_rpt_parameters
            WHERE parameter_name = SUBSTR(trim(conditions_rec.value1),2)
            AND report_id        = p_report_id
            AND process_id       = l_process_id ;
          EXCEPTION WHEN OTHERS THEN
            l_param_col_id := NULL;
          END;
          -- Updated by Praveen.P to fix Bug# 2304
          IF l_param_col_id IS NOT NULL THEN
            BEGIN
              SELECT evc.alias_name
              INTO l_component_alias
              FROM xxeis.eis_rs_reports er,
                xxeis.eis_rs_process_rpt_columns erc,
                xxeis.eis_rs_views ev,
                xxeis.eis_rs_view_components evc,
                xxeis.eis_rs_view_columns ervc
              WHERE er.report_id = erc.report_id
              AND erc.view_id    = ev.view_id
              AND er.view_id     = evc.view_id
                --AND ev.view_name = evc.component_name
              AND erc.view_component_id = evc.view_component_id
              AND er.report_id          = p_report_id
              AND erc.view_id           = ervc.view_id
              AND erc.column_name       = ervc.column_name
              AND ervc.view_column_id   = l_param_col_id
              AND erc.process_id        = l_process_id;
            EXCEPTION WHEN OTHERS THEN
              l_component_alias := NULL;
            END;
          END IF;
        ELSE
          BEGIN
            SELECT evc.alias_name
            INTO l_component_alias
            FROM xxeis.eis_rs_reports er,
              xxeis.eis_rs_process_rpt_columns erc,
              xxeis.eis_rs_views ev,
              xxeis.eis_rs_view_components evc
            WHERE er.report_id = erc.report_id
            AND erc.view_id    = ev.view_id
            AND er.view_id     = evc.view_id
              --AND ev.view_name = evc.component_name
            AND erc.view_component_id = evc.view_component_id
            AND er.report_id          = p_report_id
            AND erc.column_name       = conditions_rec.column_name
            AND erc.process_id        = l_process_id ;
          EXCEPTION WHEN OTHERS THEN
            NULL;
          END ;
        END IF;
        IF l_component_alias         IS NOT NULL THEN
          conditions_rec.column_name := l_component_alias||'.'|| conditions_rec.column_name;
        END IF;
      END IF;

      IF l_view_val1_count > 0 AND l_report_view_alias IS NOT NULL THEN
        conditions_rec.value1 := l_report_view_alias||'.'|| conditions_rec.value1;
      END IF;

      IF l_view_val2_count     > 0 AND l_report_view_alias IS NOT NULL THEN
        conditions_rec.value2 := l_report_view_alias||'.'|| conditions_rec.value2;
      END IF;
    END IF;
    l_tmp_cond            := conditions_rec.column_name ;
    l_tmp_cond_column     := conditions_rec.column_name ;
    l_value1              := conditions_rec.value1 ;
    l_value2              := conditions_rec.value2 ;
    l_skip_condition_flag := 'N' ;
    l_param_value         := NULL;

    IF ( upper(conditions_rec.param_based_condition) = 'Y'
         AND( (instr(conditions_rec.column_name, ':') > 0 AND upper(conditions_rec.column_name) NOT LIKE '%SYSTEM.PROCESS_ID%')
         OR (instr(conditions_rec.value1, ':') > 0 AND upper(conditions_rec.value1) NOT LIKE '%SYSTEM.PROCESS_ID%')
         OR (INSTR(CONDITIONS_REC.VALUE2, ':') > 0 AND UPPER(CONDITIONS_REC.VALUE2) NOT LIKE '%SYSTEM.PROCESS_ID%') ) ) THEN
      FOR i IN param_tbl.first .. param_tbl.last
      LOOP
        IF upper(conditions_rec.column_name) LIKE '%:'||upper(trim(param_tbl(i).parameter_name))||'%'
        OR upper(conditions_rec.value1) LIKE '%:'||upper(trim(param_tbl(i).parameter_name))||'%'
        OR upper(conditions_rec.value2) LIKE '%:'||upper(trim(param_tbl(i).parameter_name))|| '%' THEN
          --Updated by Praveen.P to fix Bug#1338 , 1339
          IF param_tbl(i).display_order    = 1 THEN
            l_param_value                 := g_param_tbl(1) ;
          ELSIF param_tbl(i).display_order = 2 THEN
            l_param_value                 := g_param_tbl(2) ;
          ELSIF param_tbl(i).display_order = 3 THEN
            l_param_value                 := g_param_tbl(3) ;
          ELSIF param_tbl(i).display_order = 4 THEN
            l_param_value                 := g_param_tbl(4) ;
          ELSIF param_tbl(i).display_order = 5 THEN
            l_param_value                 := g_param_tbl(5) ;
          ELSIF param_tbl(i).display_order = 6 THEN
            l_param_value                 := g_param_tbl(6) ;
          ELSIF param_tbl(i).display_order = 7 THEN
            l_param_value                 := g_param_tbl(7) ;
          ELSIF param_tbl(i).display_order = 8 THEN
            l_param_value                 := g_param_tbl(8) ;
          ELSIF param_tbl(i).display_order = 9 THEN
            l_param_value                 := g_param_tbl(9) ;
          ELSIF param_tbl(i).display_order = 10 THEN
            l_param_value                 := g_param_tbl(10) ;
          ELSIF param_tbl(i).display_order = 11 THEN
            l_param_value                 := g_param_tbl(11) ;
          ELSIF param_tbl(i).display_order = 12 THEN
            l_param_value                 := g_param_tbl(12) ;
          ELSIF param_tbl(i).display_order = 13 THEN
            l_param_value                 := g_param_tbl(13) ;
          ELSIF param_tbl(i).display_order = 14 THEN
            l_param_value                 := g_param_tbl(14) ;
          ELSIF param_tbl(i).display_order = 15 THEN
            l_param_value                 := g_param_tbl(15) ;
          ELSIF param_tbl(i).display_order = 16 THEN
            l_param_value                 := g_param_tbl(16) ;
          ELSIF param_tbl(i).display_order = 17 THEN
            l_param_value                 := g_param_tbl(17) ;
          ELSIF param_tbl(i).display_order = 18 THEN
            l_param_value                 := g_param_tbl(18) ;
          ELSIF param_tbl(i).display_order = 19 THEN
            l_param_value                 := g_param_tbl(19) ;
          ELSIF param_tbl(i).display_order = 20 THEN
            l_param_value                 := g_param_tbl(20) ;
          ELSIF param_tbl(i).display_order = 21 THEN
            l_param_value                 := g_param_tbl(21) ;
          ELSIF param_tbl(i).display_order = 22 THEN
            l_param_value                 := g_param_tbl(22) ;
          ELSIF param_tbl(i).display_order = 23 THEN
            l_param_value                 := g_param_tbl(23) ;
          ELSIF param_tbl(i).display_order = 24 THEN
            l_param_value                 := g_param_tbl(24) ;
          ELSIF param_tbl(i).display_order = 25 THEN
            l_param_value                 := g_param_tbl(25) ;
          ELSIF param_tbl(i).display_order = 26 THEN
            l_param_value                 := g_param_tbl(26) ;
          ELSIF param_tbl(i).display_order = 27 THEN
            l_param_value                 := g_param_tbl(27) ;
          ELSIF param_tbl(i).display_order = 28 THEN
            l_param_value                 := g_param_tbl(28) ;
          ELSIF param_tbl(i).display_order = 29 THEN
            l_param_value                 := g_param_tbl(29) ;
          ELSIF param_tbl(i).display_order = 30 THEN
            l_param_value                 := g_param_tbl(30) ;
          END IF ;
          EXIT ;
        END IF ;
      END LOOP ;
      IF l_param_value IS NULL THEN
        l_skip_condition_flag := 'Y';
      END IF;
    END IF;

    IF l_skip_condition_flag = 'Y' THEN
      NULL;
    ELSE
      IF upper(conditions_rec.operator_name) LIKE '%BETWEEN' THEN
        l_tmp_cond := l_tmp_cond ||' '||upper(conditions_rec.operator_name)||' '||conditions_rec.value1||' AND '||conditions_rec.value2 ;
      ELSIF upper(conditions_rec.operator_name) LIKE '%IN' THEN
        l_conc_param := trim(conditions_rec.value1);
        --code added by Rajani for range/include/exclude
        BEGIN
          IF( instr(l_param_value,''':''')>0 OR instr(l_param_value,'''+''')>0 OR instr(l_param_value,'''-''')>0 ) THEN
            l_tmp_range_cond             := xxeis.eis_fin_util_pkg.get_range_incl_excl_conditions (l_tmp_cond_column ,l_param_value );
            l_range_cond                 :=1;
            --end of code by Rajani
          END IF;
        EXCEPTION WHEN OTHERS THEN
          l_range_cond := 0;
        END;
        /*following code added by sjampala 11/03/08*/
        BEGIN
          /* select 1 INTO l_seg_column from xxeis.eis_rs_process_rpt_conditions where process_id = g_report_process_id
          and column_name = upper(l_tmp_cond);*/
          SELECT 1
          INTO l_seg_column
          FROM gl_sets_of_books_v sob,
            fnd_id_flex_segments_tl fifv
          WHERE sob.chart_of_accounts_id = fifv.id_flex_num
          AND sob.set_of_books_id        = fnd_profile.VALUE ('GL_SET_OF_BKS_ID')
          AND fifv.application_id        = 101
          AND fifv.id_flex_code          = 'GL#'
          AND fifv.LANGUAGE              = USERENV ('LANG')
          AND fifv.form_left_prompt      = initcap(REPLACE(SUBSTR (l_tmp_cond, INSTR (l_tmp_cond, '#', 1, 2) + 1),'_',' ')); -- commented the below one and added this in order to resolve segment range option issue by sjampala
          --initcap(SUBSTR (l_tmp_cond, INSTR (l_tmp_cond, '#', 1, 2) + 1));
          --                     AND 'GL#' || fifv.application_column_name = l_tmp_cond;
        EXCEPTION WHEN OTHERS THEN
          l_seg_column := 0;
        END;
        IF (l_seg_column = 1) THEN
          l_tmp_cond    := NULL;
        ELSE
          /*end of code added by sjampala*/
          IF SUBSTR(l_conc_param, 1,1 ) = '(' AND SUBSTR(l_conc_param,-1) = ')' THEN
            l_conc_param := SUBSTR (l_conc_param, 2, LENGTH(l_conc_param) -2 ) ;
          END IF;
          l_tmp_cond := l_tmp_cond||' '||upper(conditions_rec.operator_name)||' ('||l_conc_param ||')' ;
        END IF ; -- added by sjampala
      ELSE
        l_tmp_cond := l_tmp_cond||' '||upper(conditions_rec.operator_name)||' '||conditions_rec.value1 ;
      END IF ;
      IF (l_tmp_cond    IS NULL) THEN
        l_conditions    := l_conditions;
      ELSIF l_range_cond = 1 THEN
        l_conditions    := l_conditions || l_tmp_range_cond;
      ELSIF l_tmp_cond  <> l_tmp_cond_column THEN
        l_conditions    := l_conditions || ' AND ' || l_tmp_cond;
      END IF;
    END IF ;
  END LOOP ;
  l_conditions := l_conditions||l_free_text ;
  BEGIN
    l_before_trigger_text := NULL;
    SELECT trigger_text
    INTO l_before_trigger_text
    FROM xxeis.eis_rs_report_triggers
    WHERE report_id      = p_report_id
    AND pre_or_post_flag = 'B'
    AND trigger_text IS NOT NULL
    AND ROWNUM < 2;
  EXCEPTION WHEN OTHERS THEN
    l_before_trigger_text := NULL;
  END;

  BEGIN
    l_after_trigger_text := NULL;
    SELECT trigger_text
    INTO l_after_trigger_text
    FROM xxeis.eis_rs_report_triggers
    WHERE report_id = p_report_id
    AND PRE_OR_POST_FLAG = 'A'
    AND trigger_text IS NOT NULL
    AND ROWNUM < 2;
  EXCEPTION WHEN OTHERS THEN
    l_after_trigger_text := NULL;
  END ;

  --Added for procedure calls
  BEGIN
    l_xml_procedure_name := NULL;
    SELECT procedure_name
    INTO l_xml_procedure_name
    FROM xxeis.eis_rs_report_templates
    WHERE report_id = p_report_id
    AND procedure_name IS NOT NULL
    AND ROWNUM < 2;
  EXCEPTION WHEN OTHERS THEN
    l_xml_procedure_name := NULL;
  END;

  IF l_xml_procedure_name IS NOT NULL THEN
    l_xml_procedure_name  := trim(l_xml_procedure_name);
  END IF;

  -- Start of report sort logic
  l_sort           := NULL ;
  l_temp_delimiter := null ;

  FOR sort_rec IN sort_c
  LOOP
    IF sort_rec.view_id        IS NOT NULL THEN
      IF sort_rec.view_id       = l_report_view_id THEN
        IF l_report_view_alias IS NOT NULL THEN
          sort_rec.column_name := l_report_view_alias||'.'|| sort_rec.column_name;
        END IF;
      ELSE -- View Component
        BEGIN
          SELECT evc.alias_name
          INTO l_component_alias
          FROM xxeis.eis_rs_reports er,
            xxeis.eis_rs_process_rpt_columns erc,
            xxeis.eis_rs_process_rpt_sorts ers,
            xxeis.eis_rs_views ev,
            xxeis.eis_rs_view_components evc
          WHERE er.report_id = ers.report_id
          AND erc.view_id    = ev.view_id
          AND er.view_id     = evc.view_id
            --AND ev.view_name = evc.component_name
          AND erc.view_component_id = evc.view_component_id
          AND er.report_id          = p_report_id
          AND ers.column_id         = erc.column_id
          AND ers.column_id         = sort_rec.column_id
          AND ers.process_id        = erc.process_id
          AND ers.process_id        = l_process_id ;
          IF l_component_alias     IS NOT NULL THEN
            sort_rec.column_name   := l_component_alias||'.'|| sort_rec.column_name;
          END IF;
        EXCEPTION WHEN OTHERS THEN
          NULL;
        END ;
      END IF;
    END IF;

    IF sort_rec.free_text IS NOT NULL THEN
      sort_rec.column_name := sort_rec.free_text;
    END IF;

    -- Added for bug# 4259 fix
    IF sort_rec.calculation_column IS NULL THEN
      /*--Start Group By : Added below group by condition so that if it is not null then number columns should not be sorted
      -- as according to the new change they are aggregated sum(numbercols)
      */
      IF NVL(l_group_by,'N') <> 'Y' THEN
        l_sort := l_sort||l_temp_delimiter||sort_rec.column_name||' '||sort_rec.asc_or_dsc ;
      ELSIF l_group_by = 'Y' THEN
        l_sort_col_datatype := xxeis.eis_rs_process_reports.get_process_column_data_type(p_process_id,sort_rec.view_id,sort_rec.column_name); --bug# 6184
        IF l_sort_col_datatype <> 'NUMBER' THEN
          l_sort := l_sort||l_temp_delimiter||sort_rec.column_name||' '||sort_rec.asc_or_dsc ;
        END IF;
        --End Group By :
      END IF;
      l_temp_delimiter :=',';
    END IF;
  END LOOP ;

  -- Added for case# 4961 fix
  IF l_sort_calc_columns IS NOT NULL AND l_sort IS NOT NULL THEN
    l_sort := l_sort_calc_columns||','||l_sort;
  ELSE
    IF l_sort_calc_columns IS NOT NULL AND l_sort IS NULL THEN
      l_sort := l_sort_calc_columns;
    END IF;
  END IF;

  IF l_sort IS NOT NULL THEN
    l_sort  := ' ORDER BY '||l_sort ;
  END IF;
  -- End of report sort logic

  -- replacing parameters with their values
  g_log              := g_log || chr(10) || 'Replacing parameters with values' ;
  l_program_location := 'Replacing parameter values';
  l_param_counter    := 1 ;
  l_param_delimiter  := NULL ;

  IF l_ind > 0 THEN
    FOR tbl_ind IN param_tbl.first .. param_tbl.last
    LOOP
      l_param_value := NULL;
      --Updated by Praveen.P to fix Bug#1338 , 1339
      IF param_tbl(tbl_ind).display_order = 1 AND g_param_tbl(1) IS NOT NULL THEN
        l_param_value                    := g_param_tbl(1) ;
        l_param_str                      := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(1), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type   ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param               := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 2 AND g_param_tbl(2) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(2) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(2), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 3 AND g_param_tbl(3) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(3) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(3), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 4 AND g_param_tbl(4) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(4) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(4), ':', '^^'), 'Null');
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 5 AND g_param_tbl(5) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(5) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(5), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 6 AND g_param_tbl(6) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(6) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(6), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 7 AND g_param_tbl(7) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(7) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(7), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 8 AND g_param_tbl(8) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(8) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(8), ':', '^^'), 'Null');
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 9 AND g_param_tbl(9) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(9) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(9), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 10 AND g_param_tbl(10) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(10) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(10), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 11 AND g_param_tbl(11) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(11) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(11), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 12 AND g_param_tbl(12) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(12) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(12), ':', '^^'), 'Null');
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 13 AND g_param_tbl(13) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(13) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(13), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 14 AND g_param_tbl(14) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(14) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(14), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 15 AND g_param_tbl(15) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(15) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(15), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 16 AND g_param_tbl(16) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(16) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(16), ':', '^^'), 'Null');
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 17 AND g_param_tbl(17) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(17) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(17), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 18 AND g_param_tbl(18) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(18) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(18), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 19 AND g_param_tbl(19) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(19) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(19), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 20 AND g_param_tbl(20) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(20) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(20), ':', '^^'), 'Null');
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 21 AND g_param_tbl(21) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(21) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(21), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 22 AND g_param_tbl(22) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(22) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(22), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 23 AND g_param_tbl(23) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(23) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(23), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 24 AND g_param_tbl(24) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(24) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(24), ':', '^^'), 'Null');
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 25 AND g_param_tbl(25) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(25) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(25), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 26 AND g_param_tbl(26) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(26) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(26), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 27 AND g_param_tbl(27) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(27) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(27), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 28 AND g_param_tbl(28) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(28) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(28), ':', '^^'), 'Null');
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 29 AND g_param_tbl(29) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(29) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(29), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter                   := ':' ;
      ELSIF param_tbl(tbl_ind).display_order = 30 AND g_param_tbl(30) IS NOT NULL THEN
        l_param_value                       := g_param_tbl(30) ;
        l_param_str                         := l_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(g_param_tbl(30), ':', '^^'), 'Null') ;
        IF param_tbl(tbl_ind).data_type      ='DATE' OR param_tbl(tbl_ind).parameter_name LIKE '%Period%Date' OR param_tbl(tbl_ind).parameter_name LIKE ('Quarter%Date') THEN
          l_csv_date_param                  := TO_CHAR(fnd_date.string_to_date(l_param_value,g_date_format),'DD-MON-YYYY','NLS_DATE_LANGUAGE = AMERICAN');
          l_csv_param_str                   := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_csv_date_param, ':', '^^'), 'Null') ;
        ELSE
          l_csv_param_str := l_csv_param_str||l_param_delimiter||param_tbl(tbl_ind).parameter_name||'='||NVL(REPLACE(l_param_value, ':', '^^'), 'Null') ;
        END IF;
        l_param_delimiter := ':' ;
      END IF ;

      --Updated till here to fix Bug#1338 , 1339
      IF upper(trim(param_tbl(tbl_ind).data_type)) = 'NUMBER' THEN
        l_quotes := '';
      ELSE
        l_quotes := '''';
      END IF ;
      l_delimiter      := ',' ;
      l_temp_delimiter := NULL ;
      l_conc_param     := NULL ;

      IF l_param_value IS NOT NULL THEN
        PARSE_DELIMITED_DATA ( l_param_value, l_delimiter );
        FOR g_data_ind IN g_data_tbl.first..g_data_tbl.last
        LOOP
          l_conc_param     :=l_conc_param||l_temp_delimiter||l_quotes|| trim(g_data_tbl(g_data_ind))||l_quotes ;
          l_temp_delimiter := ',' ;
        END LOOP;
      ELSE
        -- Modified to fix trigger report parameter default SQL value issue
        /*SELECT nvl(default_value, null)
        INTO l_conc_param
        FROM xxeis.EIS_RS_PROCESS_RPT_PARAMETERS
        WHERE upper(trim(parameter_name)) = upper(trim(param_tbl(tbl_ind).parameter_name))
        and report_id = p_report_id -- 6.04
        and process_id = g_report_process_id -- 6.04
        AND ROWNUM < 2 ;*/
        l_conc_param:=l_quotes||l_quotes ;
      END IF ;
      l_conditions := REPLACE (l_conditions, ':'|| param_tbl(tbl_ind).parameter_name, l_conc_param) ;
      /* This line added by raju */
      l_d_columns := REPLACE (l_d_columns, ':'||param_tbl(tbl_ind).parameter_name, l_conc_param) ;
      --added by sravani to fix case 4961
      l_sort_calc_columns := REPLACE (l_sort_calc_columns, ':'||param_tbl(tbl_ind).parameter_name, l_conc_param) ;
      --Start Group By : To replace parmeter names with values in the caculation text used in group by
      IF l_group_by = 'Y' THEN
        l_group_by_stmt := REPLACE (l_group_by_stmt, ':'||param_tbl(tbl_ind).parameter_name, l_conc_param) ;
      END IF;
      -- End Group By :
      -- added by sjampala
      BEGIN
        SELECT flex_value_Set_id,
          segment_num
        INTO l_seg_col_prefix,
          l_segment_num
        FROM xxeis.eis_rs_process_rpt_parameters
        WHERE UPPER (TRIM (parameter_name)) = UPPER (TRIM (param_tbl (tbl_ind).parameter_name))
        AND report_id = p_report_id -- 6.04
        AND process_id = g_report_process_id
        AND flex_value_set_id IS NOT NULL;
        IF ( (l_seg_col_prefix IS NOT NULL) AND (l_segment_num IS NOT NULL) AND (l_param_value IS NOT NULL) ) THEN
          l_conc_param := REPLACE (l_conc_param, '''', '');
          l_tmp_cond := xxeis.eis_fin_util_pkg.get_conditions (l_conc_param,
                                                               UPPER(trim(param_tbl (tbl_ind).parameter_name)) ,
                                                               p_report_id
          -- l_seg_col_prefix|| l_segment_num
                                                              );
          l_conditions := l_conditions || l_tmp_cond;
        END IF;
      EXCEPTION WHEN OTHERS THEN
        l_tmp_cond := NULL;
      END;
      -- end of code added by sjampala
      --This line added by raju.
      l_conditions1 := l_conditions;
      --1.4.7 09-Jan-2005 KP
      IF INSTR(UPPER(l_conditions), ':SYSTEM.PROCESS_ID' ) > 0 THEN
        l_conditions := REPLACE (l_conditions, ':system.process_id', TO_CHAR(p_process_id)) ;
        l_conditions := REPLACE (l_conditions, ':SYSTEM.PROCESS_ID', TO_CHAR(p_process_id)) ;
      END IF;
      l_param_counter := l_param_counter + 1 ;
      IF ( l_is_url_there = TRUE ) THEN
        l_stmt := REPLACE (l_stmt, ''':'||param_tbl(tbl_ind).parameter_name||'''', l_conc_param) ;
      END IF;
      IF (l_calculation_flag = TRUE) THEN
        l_stmt := REPLACE (l_stmt, ':'|| param_tbl(tbl_ind).parameter_name, l_conc_param) ;
      END IF;
      --1.4.7 09-Jan-2005 KP
      IF INSTR(UPPER(l_stmt), ':SYSTEM.PROCESS_ID' ) > 0 THEN
        l_stmt := REPLACE (l_conditions, ':system.process_id', TO_CHAR(p_process_id)) ;
        l_stmt := REPLACE (l_conditions, ':SYSTEM.PROCESS_ID', TO_CHAR(p_process_id)) ;
      END IF;

    fnd_file.put_line(fnd_file.log,'l_stmt..after after system processid..'||l_stmt);
      l_trig_conc_param := l_conc_param ;
      -- KP 23-Dec-2005 - Bug 140
      --Added this code to rectify the single quote problem if report LOV parameter has only one value
      IF INSTR(REPLACE(L_CONC_PARAM, ' '), ''',') > 0 THEN
        L_TRIG_CONC_PARAM := '''' || REPLACE(L_CONC_PARAM, '''', '''''') || '''' ;
      ELSIF SUBSTR(l_conc_param, 1,1) = '''' AND SUBSTR(l_conc_param, -1) = '''' AND INSTR( l_conc_param, ',') > 0 AND INSTR( l_conc_param, ',', 1, 2 ) = 0 THEN
        L_TRIG_CONC_PARAM := '''' || REPLACE(L_CONC_PARAM, '''', '''''') || '''' ;
      END IF;
      /*
      IF INSTR(REPLACE(l_conc_param, ' '), ''',') > 0 THEN
      l_trig_conc_param := '''' || REPLACE(l_conc_param, '''', '''''') || '''' ;
      END IF;
      */
      fnd_file.put_line ( fnd_file.log, param_tbl(tbl_ind).parameter_name || ' value ' || l_trig_conc_param ) ;
      l_before_trigger_text := REPLACE (l_before_trigger_text, ':'||param_tbl(tbl_ind).parameter_name, l_trig_conc_param) ;
      l_after_trigger_text  := REPLACE (l_after_trigger_text, ':'||param_tbl(tbl_ind).parameter_name, l_trig_conc_param) ;
      l_xml_procedure_name  := replace (l_xml_procedure_name, ':'||param_tbl(tbl_ind).parameter_name, l_trig_conc_param) ;
      l_sort  := REPLACE (l_sort, ':'||param_tbl(tbl_ind).parameter_name, l_trig_conc_param) ;

      --1.4.7 09-Jan-2005 KP
      IF INSTR(UPPER(l_before_trigger_text), ':SYSTEM.PROCESS_ID' ) > 0 THEN
        l_before_trigger_text := REPLACE (l_before_trigger_text, ':system.process_id', TO_CHAR(p_process_id)) ;
        l_before_trigger_text := REPLACE (l_before_trigger_text, ':SYSTEM.PROCESS_ID', TO_CHAR(p_process_id)) ;
      END IF;
      --1.4.7 09-Jan-2005 KP
      IF INSTR(UPPER(l_after_trigger_text), ':SYSTEM.PROCESS_ID' ) > 0 THEN
        l_after_trigger_text := REPLACE (l_after_trigger_text, ':system.process_id', TO_CHAR(p_process_id)) ;
        l_after_trigger_text := REPLACE (l_after_trigger_text, ':SYSTEM.PROCESS_ID', TO_CHAR(p_process_id)) ;
      END IF;
      --this part executes Procedure based templates
      IF INSTR(UPPER(l_xml_procedure_name), ':SYSTEM.PROCESS_ID' ) > 0 THEN
        l_xml_procedure_name := REPLACE (l_xml_procedure_name, ':system.process_id', TO_CHAR(p_process_id)) ;
        l_xml_procedure_name := REPLACE (l_xml_procedure_name, ':SYSTEM.PROCESS_ID', TO_CHAR(p_process_id)) ;
      END IF;
    END LOOP ;
  END IF ;

  /*-- Get report name, base view and distinct flag information from eis_rs_reports table
  SELECT err.report_name ,
    erv.view_name,
    hint_name,
    view_owner,
    view_alias,
    NVL ( err.distinct_flag ,'N')
  INTO l_title,
    l_view_names,
    l_hint,
    l_view_owner,
    l_report_view_alias,
    l_distinct_flag
  FROM xxeis.eis_rs_reports err ,
    xxeis.eis_rs_views erv
  WHERE err.view_id = erv.view_id
  AND err.report_id = p_report_id;*/

  BEGIN
    IF l_view_owner IS NULL THEN -- Added for 1.4.7 KP
      SELECT owner
      INTO l_view_owner
      FROM all_views
      WHERE view_name = upper(l_view_names );
    END IF;
  EXCEPTION WHEN OTHERS THEN
    l_view_owner := 'XXEIS' ;
  END ;
  l_view_names := l_view_owner || '.' || l_view_names || ' ' || l_report_view_alias;

 fnd_file.put_line(fnd_file.log,'l_view_names...'||l_view_names);

--------------added by harsha for performance issue TMS#20130920-00347-----------

     if l_view_names='XXEIS.XXWC_OPEN_ORDER_LINES_V XOOLV' then
       

    --insert /*+ PARALLEL (AUTO) */ into  XXEIS.XXWC_OPEN_ORDER_LINES_GTT_TBL value (select * from  xxeis.xxeis_om_detail_so_vw);commented for TMS 20160509-00068(ver 7.1.1) by rakesh patel
    
    insert /*+ PARALLEL(4) */ into  XXEIS.XXWC_OPEN_ORDER_LINES_GTT_TBL value (select * from  xxeis.xxeis_om_detail_so_vw);--Changed for TMS 20160509-00068(ver 7.1.1) by rakesh patel

	end if;
--------------added by harsha for performance issue TMS#20130920-00347-----------



  --Append other views
  FOR get_view_name_rec IN views_c ( l_process_id )
  LOOP
    l_view_names := l_view_names || ', ' || get_view_name_rec.view_name ;
  END LOOP ;
  l_view_names := RTRIM(TRIM(l_view_names), ',');

  --Get query hint
  IF l_hint IS NOT NULL THEN
    l_hint  := 'SELECT /*+ '||l_hint||' */ ' ;
  ELSE
    l_hint := 'SELECT ' ;
  END IF ;

  -- Get distinct status
  IF l_distinct_flag ='Y' THEN
    l_distinct_text := 'DISTINCT ';
  ELSE
    l_distinct_text := ' ';
  END IF;

--  -- Start of report sort logic
--  l_sort           := NULL ;
--  l_temp_delimiter := null ;
--
--  FOR sort_rec IN sort_c
--  LOOP
--    IF sort_rec.view_id        IS NOT NULL THEN
--      IF sort_rec.view_id       = l_report_view_id THEN
--        IF l_report_view_alias IS NOT NULL THEN
--          sort_rec.column_name := l_report_view_alias||'.'|| sort_rec.column_name;
--        END IF;
--      ELSE -- View Component
--        BEGIN
--          SELECT evc.alias_name
--          INTO l_component_alias
--          FROM xxeis.eis_rs_reports er,
--            xxeis.eis_rs_process_rpt_columns erc,
--            xxeis.eis_rs_process_rpt_sorts ers,
--            xxeis.eis_rs_views ev,
--            xxeis.eis_rs_view_components evc
--          WHERE er.report_id = ers.report_id
--          AND erc.view_id    = ev.view_id
--          AND er.view_id     = evc.view_id
--            --AND ev.view_name = evc.component_name
--          AND erc.view_component_id = evc.view_component_id
--          AND er.report_id          = p_report_id
--          AND ers.column_id         = erc.column_id
--          AND ers.column_id         = sort_rec.column_id
--          AND ers.process_id        = erc.process_id
--          AND ers.process_id        = l_process_id ;
--          IF l_component_alias     IS NOT NULL THEN
--            sort_rec.column_name   := l_component_alias||'.'|| sort_rec.column_name;
--          END IF;
--        EXCEPTION WHEN OTHERS THEN
--          NULL;
--        END ;
--      END IF;
--    END IF;
--
--    IF sort_rec.free_text IS NOT NULL THEN
--      sort_rec.column_name := sort_rec.free_text;
--    END IF;
--    xxeis.eis_rs_utility.log('Sort',sort_rec.column_name||';');commit;
--    -- Added for bug# 4259 fix
--    IF sort_rec.calculation_column IS NULL THEN
--      /*--Start Group By : Added below group by condition so that if it is not null then number columns should not be sorted
--      -- as according to the new change they are aggregated sum(numbercols)
--      */
--      IF NVL(l_group_by,'N') <> 'Y' THEN
--        l_sort := l_sort||l_temp_delimiter||sort_rec.column_name||' '||sort_rec.asc_or_dsc ;
--      ELSIF l_group_by = 'Y' THEN
--        l_sort_col_datatype := xxeis.eis_rs_process_reports.get_process_column_data_type(p_process_id,sort_rec.view_id,sort_rec.column_name); --bug# 6184
--        IF l_sort_col_datatype <> 'NUMBER' THEN
--          l_sort := l_sort||l_temp_delimiter||sort_rec.column_name||' '||sort_rec.asc_or_dsc ;
--        END IF;
--        --End Group By :
--      END IF;
--      l_temp_delimiter :=',';
--    END IF;
--  end loop ;
--  xxeis.eis_rs_utility.log('Sort','Sort after loop'||l_sort);commit;
--  -- Added for case# 4961 fix
--  IF l_sort_calc_columns IS NOT NULL AND l_sort IS NOT NULL THEN
--    l_sort := l_sort_calc_columns||','||l_sort;
--  ELSE
--    IF l_sort_calc_columns IS NOT NULL AND l_sort IS NULL THEN
--      l_sort := l_sort_calc_columns;
--    END IF;
--  END IF;
--  xxeis.eis_rs_utility.log('Sort','Final Sort'||l_sort);commit;
--  IF l_sort IS NOT NULL THEN
--    l_sort  := ' ORDER BY '||l_sort ;
--  END IF;
--  -- End of report sort logic

  --condition checking whether the request is for view sql
  IF p_view_sql != 'SQL' THEN
    --code to fix sort issue when distinct flag is checked, case#3033
    IF l_distinct_flag = 'Y' AND l_sort IS NOT NULL THEN
    -- l_stmt := 'SELECT details '|| l_column_list||' FROM (' || l_hint || l_stmt || ' details ,ROW_NUMBER() OVER ( PARTITION BY ' || l_stmt || ' ' || l_sort || ') rn' || l_column_list || ' FROM ' || l_view_names || ' ' || l_conditions || l_sort || ') WHERE rn = 1';
    l_stmt:=l_hint || l_stmt || ' details ,ROW_NUMBER() OVER ( PARTITION BY ' || l_stmt || ' ' || l_sort || ') rn' || l_column_list || ' FROM ' || l_view_names || ' ' || l_conditions || l_sort  ;

 fnd_file.put_line(fnd_file.log,'l_stmt..after l_distinct_flag..'||l_stmt);

    --Start Group By : Preparing another lstmt
    ELSIF NVL(l_group_by,'N') = 'Y' THEN
      l_stmt := l_hint||l_stmt ||' details ' ||' , NULL ' ||' FROM ' ||l_view_names ||' ' ||l_conditions;
      IF l_group_by_stmt     IS NOT NULL THEN
        l_stmt := l_stmt||' group by '|| l_group_by_stmt ;
      END IF;
      l_stmt := l_stmt ||' '||l_sort ;
       fnd_file.put_line(fnd_file.log,'l_stmt..before group by..'||l_stmt);
      --End Group By :
    ELSE
      l_stmt := l_hint||l_distinct_text||l_stmt
                      ||' details '
              ||l_column_list --rakesh publish changes
                      ||' FROM '
              ||l_view_names
              ||' '
              ||l_conditions
              ||l_sort; 

 fnd_file.put_line(fnd_file.log,'l_stmt..in if..'||l_stmt);
    END IF;

    g_log := g_log || chr(10) || 'Before updating sql_string and parameters in processes...' ;
    l_program_location := 'updating processes with sql string and parameters';
    -- Initialize result variable
    dbms_lob.CreateTemporary( p_result, TRUE );
    BEGIN
      UPDATE xxeis.eis_rs_processes
      SET sql_string = REPLACE(REPLACE(l_stmt,'^&*',''),'*&^',''), --bug 3667
          parameters = SUBSTR(l_param_str, 1, 3900) --Updated by Praveen.P to fix Bug # 3533
      WHERE process_id = p_process_id
      AND report_id    = p_report_id ;
 fnd_file.put_line(fnd_file.log,'l_stmt..before update..'||l_stmt);
     -- COMMIT;
    EXCEPTION WHEN OTHERS THEN
      NULL;
    END;

    g_log := g_log || chr(10) || 'Updated sql_string, parameters in processes ' || l_process_id ;
    xxeis.eis_rs_utility.process_log ( l_process_id, g_log ) ;
    g_log := NULL ;
    l_program_location := 'After updating of processes data sql_string and parameters';

    --This part executes for procedure based templates
    BEGIN
      IF l_xml_procedure_name IS NOT NULL THEN
        l_xml_procedure_name := REPLACE(REPLACE(l_xml_procedure_name,chr(10)),chr(13),' ');
        l_program_location := 'Procedure base templates';
        g_log := g_log || chr(10) || 'Firing "Procedure based template call" ...' ;
        set_location( ' l_xml_procedure_name => ' || l_xml_procedure_name );
        EXECUTE IMMEDIATE l_xml_procedure_name ;
      END IF;
    EXCEPTION WHEN OTHERS THEN
      l_xml_procedure_name := NULL;
      g_log := g_log || chr(10) || 'Exception in procedure based template call ...'|| sqlerrm ;
      set_location( ' Exception in procedure based template call => ' || sqlerrm );
    END;

    --This part executes pre report trigger
    BEGIN
      IF l_before_trigger_text IS NOT NULL THEN
        l_before_trigger_text :=REPLACE(REPLACE(l_before_trigger_text,chr(10),' '),chr(13),' ');
        l_program_location := 'Before trigger part';
        g_log := g_log || chr(10) || 'Firing "before" trigger...' ;
        set_location( ' l_before_trigger_text => ' || l_before_trigger_text );
        set_location( ' date_format => ' || g_date_format );
        EXECUTE IMMEDIATE l_before_trigger_text ;
      END IF;
      l_before_trigger_text := NULL;
    END;

    BEGIN
      SELECT user_name INTO l_user_name
      FROM fnd_user WHERE user_id = p_user_id;
    EXCEPTION WHEN OTHERS THEN
      l_user_name := NULL;
    END;

    --Added by Rajani to get the information needed in CSV header
    BEGIN
      SELECT err.application_id eis_application_id,
        frv.responsibility_name||DECODE(fnd_profile.value_specific ('ENABLE_SECURITY_GROUPS',
                                                NULL,
                                                frv.responsibility_id,
                                                frv.application_id
                                               ),'Y',','||sec.security_group_name,null)||xxeis.eis_rs_oaf_api.get_responsibility_context(err.application_id,frv.responsibility_id),

        era.application_name module_name,
        erp.request_id,
        erp.created_by,
        erp.start_time,
        erp.end_time,
        erp.creation_date,
        ers.security_group_id
      INTO l_appl_id,
        l_resp_name,
        l_module_name,
        l_request_value,
        l_requested_by,
        l_actual_start_date,
        l_actual_comp_date,
        l_request_date,
        l_security_group_id
      FROM fnd_responsibility_vl frv,
        fnd_security_groups_vl sec,
        fnd_user_resp_groups furg,
        xxeis.eis_rs_sessions ers,
        xxeis.eis_rs_processes erp,
        xxeis.eis_rs_reports err,
        xxeis.eis_rs_applications era
      WHERE ers.responsibility_id = frv.responsibility_id
      AND frv.responsibility_id = furg.responsibility_id
      AND furg.responsibility_id = ers.responsibility_id
      AND furg.security_group_id  = sec.security_group_id
      AND sec.security_group_id = ers.security_group_id
      AND err.report_id           = erp.report_id
      AND err.application_id      = era.application_id
      and ers.session_id          = erp.session_id
      AND TRUNC(SYSDATE) BETWEEN furg.start_date AND NVL (furg.end_date, hr_general.end_of_time)
      AND furg.user_id = ers.user_id
      AND erp.process_id          = l_process_id;
    EXCEPTION WHEN OTHERS THEN
      NULL;
    END;

    l_col_cnt := l_col_cnt + NVL(g_gl_segment_count, 0) ; --Added KP 22-Dec-2005

    -- added RL 24-Oct-2007 Payroll Register
    BEGIN
      l_clob_count := xxeis.eis_rs_custom_pkg.get_custom_flag(p_report_id);
    EXCEPTION WHEN OTHERS THEN
      l_clob_count := 0;
    END;

    -- Start of multiple pivots logic
    l_pivot_total_rows := 'R'||'*l_no_of_rows*'||'C'||l_col_cnt;

    SELECT COUNT(*)
    INTO l_pivot_count
    FROM xxeis.eis_rs_process_rpt_pivots
    WHERE report_id = p_report_id
    and process_id = l_process_id;

    BEGIN
    IF l_pivot_count >0 THEN
      l_pivot_header := l_pivot_header ||'Number Of Pivots=' || l_pivot_count ||';' ;
      l_pivot_header := l_pivot_header ||'Pivot Total Rows and Columns='||l_pivot_total_rows||';' ;

      FOR get_process_pivots_rec IN get_process_pivots_c
      LOOP

        l_pivot_page_fields        := NULL;
        l_pivot_row_fields         := NULL;
        l_pivot_data_fields        := NULL;
        l_pivot_column_fields      := NULL;
        l_summary_row_fields  := NULL;
        l_summary_data_fields := NULL;
        l_summary_data_field_captions := NULL;
        l_summary_data_subtotals := NULL;

        FOR get_process_pivot_dtls_rec IN get_process_pivot_dtls_c(get_process_pivots_rec.pivot_id)
        LOOP

          IF get_process_pivot_dtls_rec.pivoting = 'PAGE_FIELD' THEN
            l_pivot_page_fields := l_pivot_page_fields||trim(get_process_pivot_dtls_rec.display_name)||',';
          END IF;

          IF get_process_pivot_dtls_rec.pivoting = 'ROW_FIELD' THEN
            l_pivot_row_fields := l_pivot_row_fields||trim(get_process_pivot_dtls_rec.display_name)||'('||get_process_pivot_dtls_rec.row_field_settings||'),';
            l_summary_row_fields := l_summary_row_fields||trim(get_process_pivot_dtls_rec.display_name)||',';
          END IF;

          IF get_process_pivot_dtls_rec.pivoting = 'DATA_FIELD' THEN
            IF get_process_pivot_dtls_rec.pivot_display_name IS NOT NULL THEN
              l_pivot_data_fields := l_pivot_data_fields||trim(get_process_pivot_dtls_rec.display_name)||'|'||trim(get_process_pivot_dtls_rec.pivot_display_name);
            ELSE
              l_pivot_data_fields := l_pivot_data_fields||trim(get_process_pivot_dtls_rec.display_name);
            END IF;
            l_pivot_data_fields := l_pivot_data_fields||'('||NVL(trim(get_process_pivot_dtls_rec.pivot_data_type),'SUM')||'|'||trim(get_process_pivot_dtls_rec.data_field_settings)||'),' ;
            l_summary_data_fields := l_summary_data_fields||trim(get_process_pivot_dtls_rec.display_name)||',';
            l_summary_data_field_captions := l_summary_data_field_captions||NVL(trim(get_process_pivot_dtls_rec.pivot_display_name),trim(get_process_pivot_dtls_rec.display_name))||',';
            l_summary_data_subtotals := l_summary_data_subtotals||NVL(trim(get_process_pivot_dtls_rec.pivot_data_type),'SUM')||',';
          END IF;

          IF get_process_pivot_dtls_rec.pivoting = 'COLUMN_FIELD' THEN
            l_pivot_column_fields := l_pivot_column_fields||trim(get_process_pivot_dtls_rec.display_name)||',';
          END IF;
        END LOOP;

        l_summary_calculations := xxeis.eis_rs_process_reports.get_summary_calc_display_cols(get_process_pivots_rec.process_id, get_process_pivots_rec.pivot_id);

        l_pivot_page_fields   := rtrim(l_pivot_page_fields,',');
        l_pivot_row_fields    := rtrim(l_pivot_row_fields,',');
        l_pivot_data_fields   := rtrim(l_pivot_data_fields,',');
        l_pivot_column_fields := rtrim(l_pivot_column_fields,',');

        l_pivot_format := '1|'||get_process_pivots_rec.pivot_name||'|'||get_process_pivots_rec.display_order;
        l_pivot_header := l_pivot_header ||'Pivot Table Page Fields=' || l_pivot_page_fields ||';' ;
        l_pivot_header := l_pivot_header ||'Pivot Table Row Fields=' || l_pivot_row_fields ||';' ;
        l_pivot_header := l_pivot_header ||'Pivot Table Data Fields=' || l_pivot_data_fields ||';' ;
        l_pivot_header := l_pivot_header ||'Pivot Table Column Fields='|| l_pivot_column_fields ||';' ;
        l_pivot_header := l_pivot_header ||'Pivot Table Format=' || l_pivot_format ||';' ;
        l_pivot_header := l_pivot_header ||'Pivot Layout=' || get_process_pivots_rec.pivot_layout ||';' ;
        l_pivot_header := l_pivot_header ||'Pivot Table Style Options=' || get_process_pivots_rec.pivot_style_options ||';' ;
        l_pivot_header := l_pivot_header ||'Summary Calculation Text='||l_summary_calculations||';' ;

        l_summary_page_fields         := l_pivot_page_fields;
        l_summary_row_fields          := rtrim(l_summary_row_fields,',');
        l_summary_data_fields         := rtrim(l_summary_data_fields,',');
        l_summary_data_field_captions := rtrim(l_summary_data_field_captions,',');
        l_summary_data_subtotals      := rtrim(l_summary_data_subtotals,',');

        l_html_pivot_string :=  l_html_pivot_string || '$page_fields=' || l_summary_page_fields || ';';
        l_html_pivot_string :=  l_html_pivot_string || '$row_fields=' || l_summary_row_fields || ';';
        l_html_pivot_string :=  l_html_pivot_string || '$data_fields=' || l_summary_data_fields || ';';
        l_html_pivot_string :=  l_html_pivot_string || '$data_field_captions=' || l_summary_data_field_captions || ';';
        l_html_pivot_string :=  l_html_pivot_string || '$data_fields_subtotals=' || l_summary_data_subtotals ||';';
        l_html_pivot_string :=  l_html_pivot_string || '$summary_calculations=' || l_summary_calculations;

        UPDATE xxeis.eis_rs_process_rpt_pivots
        SET html_summary_string = l_html_pivot_string
        WHERE process_id = get_process_pivots_rec.process_id
        AND pivot_id = get_process_pivots_rec.pivot_id;

      --  COMMIT;
        l_html_pivot_string := NULL;
      END LOOP;
      l_pivot_header := l_pivot_header ||'End of Pivot Definition;' ;
    ELSE
      l_pivot_header := 'Pivot Total Rows and Columns='||l_pivot_total_rows||';' ;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      NULL;
    END;
    -- End of multiple pivots logic

    IF l_clob_count <= 0 THEN
      l_header      := 'Header;'||'Replace=^$,;';
      l_header      := l_header ||'Data Files=*l_clob_file_str*;' ;
      l_header      := l_header ||'Source Format^' || l_csv_d_types || '=*l_source*'||';' ;
      l_header      := l_header ||'Advanced Format=' || l_data_formats ||';' ;
      l_header      := l_header ||'Alignment=' || l_data_alignment ||';' ;
      l_header      := l_header ||l_pivot_header; --Pivots Header
      l_header      := l_header ||'Title='||l_title||';';
      l_header      := l_header ||'Application Id='||l_appl_id||';';
      l_header      := l_header ||'Security Group Id='||l_security_group_id||';';
      l_header      := l_header ||'Responsibility Name='||l_resp_name||';';
      l_header      := l_header ||'Module Name='||l_module_name||';';
      l_header      := l_header ||'Report Parameters:'||trim(l_csv_param_str)||';';
      l_header      := l_header ||'Requested By='||l_user_name||';';
      l_header      := l_header ||'Request Id='||l_request_value||';';
      l_header      := l_header ||'Process Id='||l_process_id||';';
      l_header      := l_header ||'Request Date='||TO_CHAR(l_request_date,'YYYY/MM/DD')||';';
      l_header      := l_header ||'Actual Starting Date='||TO_CHAR(l_actual_start_date, 'YYYY/MM/DD HH:MI:SS AM')||';';
      l_header      := l_header ||'Actual Completion Date=l_actual_comp_date;';
      l_header      := l_header ||'Header Finished;';
      g_rep_output_tbl.delete ;
      g_rep_output_tbl(0) := l_header ;

      --IF l_pivot_flag THEN
        BEGIN
          IF l_pivot_data_field_captions IS NOT NULL THEN
            l_pivot_data_field_captions  := SUBSTR(l_pivot_data_field_captions,2);
          END IF;
          IF l_pivot_data_fields IS NOT NULL THEN
            l_pivot_data_fields  := SUBSTR(l_pivot_data_fields,2);
          END IF;
          IF l_pivot_row_fields IS NOT NULL THEN
            l_pivot_row_fields  := SUBSTR(l_pivot_row_fields,2);
          END IF;
          IF l_pivot_data_subtotals IS NOT NULL THEN
            l_pivot_data_subtotals  := SUBSTR(l_pivot_data_subtotals,2);
          END IF;
          IF l_columns IS NOT NULL THEN
            l_columns  := SUBSTR(l_columns,2);
          END IF;
          IF l_d_columns IS NOT NULL THEN
            l_d_columns  := SUBSTR(l_d_columns,2);
          END IF;
          IF l_process_count   <> 0 THEN
            g_dynamic_col_list := get_process_output_columns(p_report_id,p_process_id);
          ELSE
            g_dynamic_col_list := get_output_columns(p_report_id);
          END IF;

          l_pivot_process_stmt := '$report_columns=' || l_columns || ';';
          l_pivot_process_stmt := l_pivot_process_stmt || '$data_columns=' || REPLACE(g_dynamic_col_list,',EXTRA_COL') || ';';
          l_pivot_process_stmt := l_pivot_process_stmt || '$data_types=' || l_d_types || ';';
          l_pivot_process_stmt := l_pivot_process_stmt || '$data_advanced_format=' || l_data_formats||';';
          l_pivot_process_stmt := l_pivot_process_stmt || '$data_alignment=' || l_data_alignment;

          UPDATE xxeis.eis_rs_processes
          SET html_pivot_string = l_pivot_process_stmt
          WHERE process_id      = p_process_id;
        EXCEPTION WHEN OTHERS THEN
          NULL;
        END;
      --END IF;

      g_log := g_log || chr(10) || 'Opening cursor...' ;
      l_program_location := 'Getting Master clob_id';
      SELECT xxeis.eis_rs_clobs_s.NEXTVAL
      INTO l_master_clob_id FROM dual;

      l_result_str            := NULL ;
      l_records_counter       := 0 ;
      l_clob_file_counter     := 1 ;
      l_clob_file_str         := NULL ;
      l_no_lines_file_counter := 0 ;
      l_file_name             := l_process_id||'_'||'1.csv' ;

      BEGIN
        SELECT NVL(parameter_value, -1)
        INTO l_no_lines_clob
        FROM xxeis.eis_rs_admin_parameters
        WHERE upper(parameter_name) LIKE UPPER('Max_no_of_lines_per_clob');
        IF l_no_lines_clob = -1 THEN
          l_no_lines_clob := 30;
        END IF ;
      EXCEPTION WHEN OTHERS THEN
        l_no_lines_clob := 30;
      END ;

      BEGIN
        SELECT NVL(parameter_value, -1)
        INTO l_no_lines_file
        FROM xxeis.eis_rs_admin_parameters
        WHERE upper(parameter_name) LIKE UPPER('Max_no_of_lines_per_data_file');
        IF l_no_lines_file = -1 THEN
          l_no_lines_file := 30;
        END IF ;
      EXCEPTION WHEN OTHERS THEN
        l_no_lines_file := 60;
      END ;

      --for commiting the rows retrieved and butlk html insert basedo n admin parameters
        BEGIN
          SELECT nvl(parameter_value,10000)
          into l_update_row_retrieved
          FROM xxeis.eis_rs_admin_parameters
          WHERE upper(parameter_name) LIKE upper('NO_OF_ROWS_RETRIEVED_COMMIT_SIZE');
        EXCEPTION WHEN OTHERS THEN
          l_update_row_retrieved := 10000;
        END;

      BEGIN
        SELECT nvl(parameter_value,20000)
        into g_bulk_html_row_count
        FROM xxeis.eis_rs_admin_parameters
        WHERE upper(parameter_name) LIKE upper('Bulk_Insert_Commit_Size');
      EXCEPTION WHEN OTHERS THEN
        g_bulk_html_row_count := 20000;
      END;
      --End

      l_program_location := 'Opening dynamic cursor';
      l_table_name       := get_html_table_name(l_process_id);
      g_log              := g_log || chr(10) || 'Html table Name ' || l_table_name|| l_process_id ;
      xxeis.eis_rs_utility.process_log ( l_process_id, g_log ) ;
      g_log := NULL ;

      BEGIN
        SELECT trim(xml_outputs),
          trim(general_outputs)
        INTO l_xml_outputs,
          l_outputs
        FROM xxeis.eis_rs_process_rpts
        WHERE report_id = p_report_id
        AND process_id  = l_process_id;

        SELECT trim(xml_outputs),
          trim(general_outputs)
        INTO l_portal_xml_outputs,
          l_portal_outputs
        FROM xxeis.eis_rs_report_portals
        WHERE report_id = p_report_id;
      EXCEPTION WHEN OTHERS THEN
        NULL;
      END;

      IF INSTR(l_portal_outputs,'HTML') > 0 OR INSTR(l_portal_outputs,'Html Summary') > 0 OR INSTR(l_portal_outputs,'XML') > 0 OR INSTR(l_portal_xml_outputs,'PDF') > 0 OR INSTR(l_portal_xml_outputs,'RTF') > 0 THEN
        l_portal_html_output_flag := TRUE;
      END IF;

      IF INSTR(l_outputs,'HTML') > 0 OR INSTR(l_outputs,'Html Summary') > 0 OR INSTR(l_outputs,'XML') > 0 OR INSTR(l_xml_outputs,'PDF') > 0 OR INSTR(l_xml_outputs,'RTF') > 0 THEN
        l_html_output_flag := TRUE;
      END IF;

      -- Start of bug# 3667 fix
      l_stmt := REPLACE(l_stmt,'^&*',' Replace( ');
      l_stmt := REPLACE(l_stmt,'*&^',', ''~'','' '') ');
      -- End of bug# 3667 fix
 fnd_file.put_line(fnd_file.log,'l_stmt..after replace..'||l_stmt);

      set_location( 'Query => ' || l_stmt );

      OPEN result_c FOR l_stmt;
      LOOP
        IF l_distinct_flag = 'Y' AND l_sort IS NOT NULL THEN
          FETCH result_c INTO l_result_rec, l_rn, l_security_col_list;
        ELSE
          FETCH result_c INTO l_result_rec, l_security_col_list;
        END IF;
        EXIT WHEN result_c%NOTFOUND;

        l_result_rec := REPLACE(REPLACE(l_result_rec,chr(13),' '),chr(10),' ');

        --If no HTML ,HTML Summary ,XML and XML publisher outputs then we are not inserting the data.
        IF l_html_output_flag = TRUE OR l_portal_html_output_flag = TRUE THEN

          IF l_cntr = 0 THEN
            --if l_security_col_list IS NOT NULL OR l_security_col_list <> '~' THEN
            IF l_publish_flag > 0 THEN
              l_cntr := 1;
              g_log := g_log || chr(10) || 'Publishing the report...' ;
            END IF;
          END IF;
          insert_html_table(p_process_id, l_result_rec, p_report_id,l_table_name, l_security_col_list);
        END IF;

        l_no_of_rows := l_no_of_rows + 1;
        IF l_extended_flag = FALSE AND l_no_of_rows <= l_no_lines_clob THEN
          g_rep_output_tbl(l_no_of_rows) := l_result_rec ;
        END IF;


        IF MOD(l_no_of_rows,l_update_row_retrieved)=0 THEN
          UPDATE xxeis.eis_rs_processes
          SET rows_retrieved = l_no_of_rows
          WHERE process_id   = p_process_id ;
          COMMIT ;
        END IF;


        IF l_extended_flag = TRUE OR l_no_of_rows > l_no_lines_clob THEN
          l_extended_flag          := TRUE ;
          l_records_counter        := l_records_counter       + 1 ;
          l_no_lines_file_counter  := l_no_lines_file_counter + 1;
          l_result_str             := l_result_str||trim(REPLACE(l_result_rec, ';', '`'))||';';

          IF (LENGTH(l_result_str) >= 3500) THEN
            dbms_lob.writeappend(p_result, LENGTH(l_result_str), REPLACE(l_result_str,',','^'));
            l_result_str := NULL;
          END IF ;

          IF l_records_counter > l_no_lines_clob THEN
            IF l_no_lines_file_counter > l_no_lines_file THEN
              l_clob_file_counter     := l_clob_file_counter + 1;
              l_file_name             := l_process_id||'_'||l_clob_file_counter||'.csv' ;
              l_clob_file_str         := l_clob_file_str||':'||l_file_name ;
              l_no_lines_file_counter := 0 ;
            END IF ;
            p_result := REPLACE(REPLACE(p_result,chr(13),' '),chr(10),' ');

            INSERT INTO xxeis.eis_rs_clobs
             (clob_id,
              process_id,
              clob_file,
              file_name
              )
              VALUES
             (xxeis.eis_rs_clobs_s.NEXTVAL,
              l_process_id,
              p_result,
              l_file_name
              );
            COMMIT ;
            -- DBMS_LOB.close(p_result) ;
            dbms_lob.createtemporary(p_result, TRUE);
            l_records_counter := 0 ;
          END IF ;
        END IF;
      END LOOP;
      --Rasmi on 03 Aug 2012

      bulk_insert_html_table(l_table_name);


      /*l_dynamic_query := 'Select ' || g_dynamic_col_list || ' From '|| l_table_name ||' WHERE PROCESS_ID ='||l_process_id;
      UPDATE xxeis.eis_rs_processes set DATA_QUERY = l_dynamic_query
      WHERE process_id = l_process_id;
      COMMIT; */
      IF l_result_str IS NOT NULL THEN
        dbms_lob.writeappend(p_result, LENGTH(l_result_str), REPLACE(l_result_str,',','^'));
        l_result_str := NULL ;
      END IF ;
      IF l_records_counter         > 0 THEN
        IF l_no_lines_file_counter > l_no_lines_file THEN
          l_clob_file_counter     := l_clob_file_counter + 1;
          l_file_name             := l_process_id||'_'||l_clob_file_counter||'.csv' ;
          l_clob_file_str         := l_clob_file_str||':'||l_file_name ;
          l_no_lines_file_counter := 0 ;
        END IF ;
        p_result := REPLACE(REPLACE(p_result,chr(13),' '),chr(10),' ');
        INSERT INTO xxeis.eis_rs_clobs
          ( clob_id,
            process_id,
            clob_file,
            file_name
          )
          VALUES
          ( xxeis.eis_rs_clobs_s.NEXTVAL,
            l_process_id,
            p_result,
            l_file_name
          );
       -- COMMIT ;
        dbms_lob.createtemporary(p_result, TRUE);
      END IF ;
      BEGIN
        SELECT parameter_value
        INTO l_max_rows
        FROM xxeis.eis_rs_admin_parameters
        WHERE upper(parameter_name) LIKE upper('Maximum_no_of_rows_allowed_in_excel');
      EXCEPTION WHEN NO_DATA_FOUND THEN
        l_max_rows := 64000;
      END;
      IF l_no_of_rows < NVL(l_max_rows, 64000) THEN
        l_source     := 'Excel';
      ELSE
        l_source := 'Access';
      END IF;

      l_file_name := l_process_id||'_'||'1.csv' ;
      l_clob_file_str := l_file_name||l_clob_file_str ;
      --Replace the header
      g_rep_output_tbl(0) := REPLACE(g_rep_output_tbl(0), '=l_actual_comp_date;', '='||TO_CHAR(SYSDATE, 'YYYY/MM/DD HH:MI:SS AM')||';');
      g_rep_output_tbl(0) := REPLACE(g_rep_output_tbl(0), '~;', ';');
      g_rep_output_tbl(0) := REPLACE(g_rep_output_tbl(0), '*l_no_of_rows*', TO_CHAR(l_no_of_rows+1)) ;
      g_rep_output_tbl(0) := REPLACE(g_rep_output_tbl(0), '*l_source*', l_source) ;
      g_rep_output_tbl(0) := REPLACE(g_rep_output_tbl(0), '*l_clob_file_str*', l_clob_file_str) ;
      dbms_lob.writeappend(p_result, LENGTH(g_rep_output_tbl(0)), g_rep_output_tbl(0));
      l_result_rec := NULL ;
      l_result_str := l_col_names||';';
      g_log := g_log || chr(10) || 'Writing to clob file...' ;

      FOR l_index  IN 1..g_rep_output_tbl.count-1
      LOOP
        l_result_rec := g_rep_output_tbl(l_index) ;
        l_result_str := l_result_str||trim(REPLACE(l_result_rec, ';', '`'))||';';
        IF (LENGTH(l_result_str) >= 3500) THEN
          dbms_lob.writeappend(p_result, LENGTH(l_result_str), REPLACE(l_result_str,',','^'));
          l_result_str := NULL;
        END IF;
      END LOOP;

      IF l_result_str IS NOT NULL THEN
        dbms_lob.writeappend(p_result, LENGTH(l_result_str), REPLACE(l_result_str,',','^'));
      END IF ;
      g_log    := g_log || chr(10) || 'Inserting clob file...' ;
      p_result := REPLACE(REPLACE(p_result,chr(13),' '),chr(10),' ');

      INSERT INTO xxeis.eis_rs_clobs
        ( clob_id,
          process_id,
          clob_file,
          file_name
        )
        VALUES
        ( l_master_clob_id,
          l_process_id,
          p_result,
          l_file_name
        );
      g_log := g_log || chr(10) || 'Commiting master clob file...' ;
     -- COMMIT ;
      g_rep_output_tbl.delete ;
      l_program_location := 'Closing cursor';
      g_log := g_log || chr(10) || 'Closing cursor...' ;
      CLOSE result_c ;
    END IF; -- clob count (payroll register)
    --COMMIT ;

    -- This part executes post report trigger
    BEGIN
      IF l_after_trigger_text IS NOT NULL THEN
        l_after_trigger_text :=REPLACE(REPLACE(l_after_trigger_text,chr(10),' '),chr(13),' ');
        g_log := g_log || chr(10)|| 'Firing "After" trigger...' ;
        set_location('After Report trigger ' || l_after_trigger_text);
        EXECUTE IMMEDIATE l_after_trigger_text ;
      END IF;
    END ;

    -- Bug# 4639 fix
    IF l_clob_count <= 0 THEN
      UPDATE xxeis.eis_rs_processes
      SET end_time     = SYSDATE,
        rows_retrieved =l_no_of_rows, -- modified to insert number of rows retreived by SRK.
        status         = 'C'
        --  ,CLOB_file = p_result
      WHERE process_id = l_process_id ;
    ELSE
      UPDATE xxeis.eis_rs_processes
      SET end_time     = SYSDATE,
        status         = 'C'
      WHERE process_id = l_process_id ;
    END IF;
    g_log := g_log || chr(10) || 'Writing Log';
    xxeis.eis_rs_utility.process_log ( l_process_id, g_log ) ;
    g_log := NULL ;
  ELSE
    l_stmt := l_hint||l_stmt
                    ||' FROM '
            ||l_view_names
            ||' '
            ||l_conditions;

    --Start group by
    -- ||l_sort;
    IF l_group_by_stmt IS NOT NULL THEN
      l_stmt := l_stmt||' group by '|| l_group_by_stmt;
    END IF;
    l_stmt := l_stmt||l_sort;
    --End group by
    p_view_sql := l_stmt;
  END IF ; --view sql condition check ends here

  DELETE FROM xxeis.eis_rs_common_outputs WHERE process_id = l_process_id;
  COMMIT;
EXCEPTION WHEN OTHERS THEN
  UPDATE xxeis.eis_rs_processes
  SET end_time       = sysdate,
      rows_retrieved = 0, -- modified to insert number of rows retreived by SRK.
      status         = 'E'
  WHERE process_id = l_process_id
  AND status      != 'T';
  COMMIT ;

  p_error_msg := SUBSTR (sqlerrm, 1, 400);
  INSERT INTO xxeis.eis_rs_errors
    ( error_id,
      process_id,
      report_id,
      error_message,
      status,
      program_location
    )
    VALUES
    ( xxeis.eis_rs_errors_s.NEXTVAL,
      l_process_id,
      p_report_id,
      p_error_msg,
      'U',
      l_program_location
    );
  xxeis.eis_rs_utility.process_log(l_process_id, p_error_msg || ' at ' || l_program_location);
  fnd_file.put_line(fnd_file.log, p_error_msg || ' at ' || l_program_location);
  COMMIT ;
end submit_report;


/*
*Submit Process :This will call from JSP
*/
PROCEDURE submit_process( p_report_id              IN NUMBER
                            ,p_submission_type        IN VARCHAR2
                            ,p_result                 OUT CLOB
                            ,p_process_id             OUT NUMBER
                            ,p_request_id             OUT NUMBER
                            ,p_error_msg              OUT VARCHAR2
                            ,p_session_id             IN NUMBER
                            ,p_summary_flag           IN VARCHAR2
                            ,p_param1                 IN VARCHAR2 DEFAULT NULL
                            ,p_param2                 IN VARCHAR2 DEFAULT NULL
                            ,p_param3                 IN VARCHAR2 DEFAULT NULL
                            ,p_param4                 IN VARCHAR2 DEFAULT NULL
                            ,p_param5                 IN VARCHAR2 DEFAULT NULL
                            ,p_param6                 IN VARCHAR2 DEFAULT NULL
                            ,p_param7                 IN VARCHAR2 DEFAULT NULL
                            ,p_param8                 IN VARCHAR2 DEFAULT NULL
                            ,p_param9                 IN VARCHAR2 DEFAULT NULL
                            ,p_param10                IN VARCHAR2 DEFAULT NULL
                            ,p_param11                IN VARCHAR2 DEFAULT NULL
                            ,p_param12                IN VARCHAR2 DEFAULT NULL
                            ,p_param13                IN VARCHAR2 DEFAULT NULL
                            ,p_param14                IN VARCHAR2 DEFAULT NULL
                            ,p_param15                IN VARCHAR2 DEFAULT NULL
                            ,p_param16                IN VARCHAR2 DEFAULT NULL
                            ,p_param17                IN VARCHAR2 DEFAULT NULL
                            ,p_param18                IN VARCHAR2 DEFAULT NULL
                            ,p_param19                IN VARCHAR2 DEFAULT NULL
                            ,p_param20                IN VARCHAR2 DEFAULT NULL
                            ,p_param21                IN VARCHAR2 DEFAULT NULL
                            ,p_param22                IN VARCHAR2 DEFAULT NULL
                            ,p_param23                IN VARCHAR2 DEFAULT NULL
                            ,p_param24                IN VARCHAR2 DEFAULT NULL
                            ,p_param25                IN VARCHAR2 DEFAULT NULL
                            ,p_param26                IN VARCHAR2 DEFAULT NULL
                            ,p_param27                IN VARCHAR2 DEFAULT NULL
                            ,p_param28                IN VARCHAR2 DEFAULT NULL
                            ,p_param29                IN VARCHAR2 DEFAULT NULL
                            ,p_param30                IN VARCHAR2 DEFAULT NULL
                            ,p_schedule_param         IN VARCHAR2 DEFAULT NULL
                            ,p_server_url             IN VARCHAR2 DEFAULT NULL
                      ,p_col_process_id         IN NUMBER   DEFAULT NULL
                            ,p_submission_source      IN VARCHAR2 DEFAULT NULL
                            )
   IS

    l_application_name  VARCHAR2(30) ;
    l_submit_type       VARCHAR2(30) := p_submission_type ;
    l_user_id           NUMBER := NULL ;
    l_report_name       VARCHAR2(100) ;
    l_program_location  VARCHAR2(500) := NULL ;
    l_process_id        NUMBER ;
    l_error_msg         VARCHAR2(1000) := NULL ;
    l_schedule         BOOLEAN;
    --Variables used for schedule process
    l_run_job           varchar2(15);
    l_start_at          varchar2(30) ;
    l_end_at            VARCHAR2(30);
    l_re_submit_interval integer;
    l_re_submit_interval_code varchar2(10);
    l_increment_dates   VARCHAR2(5);
    l_notify            VARCHAR2(4000);
    l_col_process_id    NUMBER ;
     --- END of variables.
     --Variable used for options
     l_opt              BOOLEAN;
     l_add_notify       BOOLEAN;
     t_temp              VARCHAR2(100);
--     l_submission_source VARCHAR2(30) :=p_submission_source;

   BEGIN
   --Updated by Praveen.P to fix Bug#1338 , 1339
    xxeis.eis_rs_utility.lov_initialize(p_session_id);
    g_param_tbl.DELETE ;

    g_param_tbl(1) :=p_param1;
    g_param_tbl(2) :=p_param2;
    g_param_tbl(3) :=p_param3;
    g_param_tbl(4) :=p_param4;
    g_param_tbl(5) :=p_param5;
    g_param_tbl(6) :=p_param6;
    g_param_tbl(7) :=p_param7;
    g_param_tbl(8) :=p_param8;
    g_param_tbl(9) :=p_param9;
    g_param_tbl(10) :=p_param10;
    g_param_tbl(11) :=p_param11;
    g_param_tbl(12) :=p_param12;
    g_param_tbl(13) :=p_param13;
    g_param_tbl(14) :=p_param14;
    g_param_tbl(15) :=p_param15;
    g_param_tbl(16) :=p_param16;
    g_param_tbl(17) :=p_param17;
    g_param_tbl(18) :=p_param18;
    g_param_tbl(19) :=p_param19;
    g_param_tbl(20) :=p_param20;
    g_param_tbl(21) :=p_param21;
    g_param_tbl(22) :=p_param22;
    g_param_tbl(23) :=p_param23;
    g_param_tbl(24) :=p_param24;
    g_param_tbl(25) :=p_param25;
    g_param_tbl(26) :=p_param26;
    g_param_tbl(27) :=p_param27;
    g_param_tbl(28) :=p_param28;
    g_param_tbl(29) :=p_param29;
    g_param_tbl(30) :=p_param30;

    IF p_col_process_id IS NOT NULL THEN
        set_date_params ( p_col_process_id, 'date_to_canonical' ) ; -- sets all dates g_param_tbl
    End If;

--till here to fix Bug#1338 , 1339
     BEGIN

        SELECT user_id INTO l_user_id FROM xxeis.eis_rs_sessions WHERE session_id = p_session_id ;
        EXCEPTION WHEN OTHERS THEN
            l_user_id := NULL ;
        END ;
        SELECT report_name  INTO l_report_name   FROM xxeis.eis_rs_reports WHERE report_id = p_report_id ;

        IF p_submission_type = 'INT' THEN

            xxeis.eis_rs_insert.eis_rs_processes( p_process_id => l_process_id
                                                , p_request_id  => NULL
                                                , p_start_time   => SYSDATE
                                                , p_end_time   => NULL
                                                , p_report_id   => p_report_id
                                                , p_parameters  => NULL
                                                , p_output_file_name => NULL
                                                , p_status  => 'U'
                                                , p_clob_file  => NULL
                                                , p_session_id  => p_session_id
                                                , p_sql_string   => NULL
                                                , p_created_by  => l_user_id
                                                , p_creation  => SYSDATE
                                                , p_auto_commit => 'Y'
                                                , p_operation_type  => 'I'
                                                , p_insertion_status => l_error_msg
                                                , p_submission_source => p_submission_source) ;

            IF (l_error_msg LIKE '%Operation failed.') THEN
                p_process_id := 0 ;
                p_error_msg := 'Sorry! System encountered a serious problem while creating a process.'||p_error_msg ;
                RETURN ;
            END IF ;
            p_process_id := l_process_id ;


        IF p_col_process_id IS NOT NULL AND p_col_process_id <> 0 THEN

      --added by Sravani.M to fix bug# 4994
      UPDATE XXEIS.EIS_RS_PROCESS_RPTS SET PROCESS_ID = P_PROCESS_ID
               WHERE PROCESS_ID = P_COL_PROCESS_ID AND
               REPORT_ID=P_REPORT_ID;
              --by Sravani.M

               UPDATE xxeis.eis_rs_process_rpt_columns set process_id = p_process_id
               WHERE process_id = p_col_process_id
           AND REPORT_ID = p_report_id;--Have TO CHANGE
           commit;
            -- Added following code to Bug 567
           UPDATE xxeis.eis_rs_process_rpt_parameters set process_id = p_process_id
               WHERE process_id = p_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.eis_rs_process_rpt_conditions set process_id = p_process_id
               WHERE process_id = p_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.eis_rs_process_rpt_sorts set process_id = p_process_id
               WHERE process_id = p_col_process_id
               AND REPORT_ID = p_report_id;

        UPDATE xxeis.eis_rs_process_rpt_pgs set process_id = p_process_id
               WHERE process_id = p_col_process_id
               AND REPORT_ID = p_report_id;

        UPDATE XXEIS.eis_rs_process_rpt_distribute
          SET PROCESS_ID   = p_process_id
          WHERE process_id = p_col_process_id
          AND REPORT_ID    = p_report_id;

        UPDATE xxeis.eis_rs_process_rpt_pivots
          SET process_id = p_process_id
                WHERE process_id = p_col_process_id
               AND report_id = p_report_id;

            UPDATE xxeis.eis_rs_process_rpt_pivot_dtls
          SET process_id = p_process_id
                WHERE process_id = p_col_process_id
              AND report_id = p_report_id;

                UPDATE xxeis.eis_rs_process_rpt_summ_calcs
          SET process_id = p_process_id
                WHERE process_id = p_col_process_id;

            END IF;
--Updated by Praveen.P to fix Bug#1338 , 1339
          UPDATE xxeis.eis_rs_processes
             SET parameter1 = trim(g_param_tbl(1))
                ,parameter2 = trim(g_param_tbl(2))
                ,parameter3 = trim(g_param_tbl(3))
                ,parameter4 = trim(g_param_tbl(4))
                ,parameter5 = trim(g_param_tbl(5))
                ,parameter6 = trim(g_param_tbl(6))
                ,parameter7 = trim(g_param_tbl(7))
                ,parameter8 = trim(g_param_tbl(8))
                ,parameter9 = trim(g_param_tbl(9))
                ,parameter10 = trim(g_param_tbl(10))
                ,parameter11 = trim(g_param_tbl(11))
                ,parameter12 = trim(g_param_tbl(12))
                ,parameter13 = trim(g_param_tbl(13))
                ,parameter14 = trim(g_param_tbl(14))
                ,parameter15 = trim(g_param_tbl(15))
                ,parameter16 = trim(g_param_tbl(16))
                ,parameter17 = trim(g_param_tbl(17))
                ,parameter18 = trim(g_param_tbl(18))
                ,parameter19 = trim(g_param_tbl(19))
                ,parameter20 = trim(g_param_tbl(20))
                ,parameter21 = trim(g_param_tbl(21))
                ,parameter22 = trim(g_param_tbl(22))
                ,parameter23 = trim(g_param_tbl(23))
                ,parameter24 = trim(g_param_tbl(24))
                ,parameter25 = trim(g_param_tbl(25))
                ,parameter26 = trim(g_param_tbl(26))
                ,parameter27 = trim(g_param_tbl(27))
                ,parameter28 = trim(g_param_tbl(28))
                ,parameter29 = trim(g_param_tbl(29))
                ,parameter30 = trim(g_param_tbl(30))
                WHERE PROCESS_ID = p_process_id;
--utill here to fix Bug#1338 , 1339
            submit_report( p_report_id          => p_report_id
                          ,p_result             => p_result
                          ,p_user_id            => l_user_id
                          ,p_process_id         => p_process_id
                          ,p_report_name        => l_report_name
                          ,p_request_id         => p_request_id
                          ,p_view_sql           => l_submit_type
                          ,p_error_msg          => p_error_msg
                          ,p_param1              => trim(g_param_tbl(1))
                          ,p_param2              => trim(g_param_tbl(2))
                          ,p_param3              => trim(g_param_tbl(3))
                          ,p_param4              => trim(g_param_tbl(4))
                          ,p_param5              => trim(g_param_tbl(5))
                          ,p_param6              => trim(g_param_tbl(6))
                          ,p_param7              => trim(g_param_tbl(7))
                          ,p_param8              => trim(g_param_tbl(8))
                          ,p_param9              => trim(g_param_tbl(9))
                          ,p_param10             => trim(g_param_tbl(10))
                          ,p_param11             => trim(g_param_tbl(11))
                          ,p_param12             => trim(g_param_tbl(12))
                          ,p_param13             => trim(g_param_tbl(13))
                          ,p_param14             => trim(g_param_tbl(14))
                          ,p_param15             => trim(g_param_tbl(15))
                          ,p_param16             => trim(g_param_tbl(16))
                          ,p_param17             => trim(g_param_tbl(17))
                          ,p_param18             => trim(g_param_tbl(18))
                          ,p_param19             => trim(g_param_tbl(19))
                          ,p_param20             => trim(g_param_tbl(20))
                          ,p_param21             => trim(g_param_tbl(21))
                          ,p_param22             => trim(g_param_tbl(22))
                          ,p_param23             => trim(g_param_tbl(23))
                          ,p_param24             => trim(g_param_tbl(24))
                          ,p_param25             => trim(g_param_tbl(25))
                          ,p_param26             => trim(g_param_tbl(26))
                          ,p_param27             => trim(g_param_tbl(27))
                          ,p_param28             => trim(g_param_tbl(28))
                          ,p_param29             => trim(g_param_tbl(29))
                          ,p_param30             => trim(g_param_tbl(30))
                           ) ;  --Until here to fix Bug#1338 , 1339
        ELSE
            p_request_id := NULL ;
            BEGIN
                SELECT nvl(APPLICATION_SHORT_NAME, 'XXEIS')INTO l_application_name
                  FROM fnd_application
                 WHERE application_id = ( SELECT application_id
                                            FROM fnd_concurrent_programs
                                           WHERE concurrent_program_name LIKE 'XXEIS_RS_SUBMIT_REPORT'
                                             AND ROWNUM < 2 )
                   AND ROWNUM < 2 ;
            EXCEPTION WHEN OTHERS THEN
                l_application_name := 'XXEIS' ;
            END ;

        p_request_id := NULL ;
            xxeis.eis_rs_insert.eis_rs_processes( p_process_id => l_process_id
                                                , p_request_id  => p_request_id
                                                , p_start_time   => SYSDATE
                                                , p_end_time   => NULL
                                                , p_report_id   => p_report_id
                                                , p_parameters  => NULL
                                                , p_output_file_name => NULL
                                                , p_status  => 'U'
                                                , p_clob_file  => NULL
                                                , p_session_id  => p_session_id
                                                , p_sql_string   => NULL
                                                , p_created_by  => l_user_id
                                                , p_creation  => SYSDATE
                                                , p_auto_commit => 'Y'
                                                , p_operation_type  => 'I'
                                                , p_insertion_status => l_error_msg
                                                , p_submission_source =>p_submission_source) ;

            IF (p_error_msg LIKE '%Operation failed.') THEN
                l_process_id := 0 ;
                p_error_msg := 'Sorry! System encountered a serious problem while creating a process.'||p_error_msg ;
                RETURN ;
            END IF ;

            p_process_id := l_process_id ;

        IF p_col_process_id IS NOT NULL AND p_col_process_id <> 0 THEN

      --added by Sravani.M to fix bug# 4994
              UPDATE xxeis.eis_rs_process_rpts SET process_id = p_process_id
               WHERE PROCESS_ID = P_COL_PROCESS_ID AND
               REPORT_ID=P_REPORT_ID;
              --by Sravani.M
               UPDATE xxeis.eis_rs_process_rpt_columns set process_id = p_process_id
               WHERE process_id = p_col_process_id
           AND REPORT_ID = p_report_id;--Have TO CHANGE

        -- Added : ashwin : glsegment expansion enhancement : 6.04
               UPDATE xxeis.EIS_RS_PROCESS_RPT_PARAMETERS set process_id = p_process_id
               WHERE process_id = p_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.EIS_RS_PROCESS_RPT_CONDITIONS set process_id = p_process_id
               WHERE process_id = p_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.EIS_RS_PROCESS_RPT_SORTS set process_id = p_process_id
               WHERE process_id = p_col_process_id
               AND REPORT_ID = p_report_id;

        UPDATE xxeis.EIS_RS_PROCESS_RPT_PGS set process_id = p_process_id
                WHERE process_id = p_col_process_id
               AND REPORT_ID = p_report_id;
        -- End Added : ashwin : glsegment expansion enhancement

        UPDATE xxeis.eis_rs_process_rpt_distribute
            SET process_id   = p_process_id
            WHERE process_id = p_col_process_id
            AND report_id    = p_report_id;

        UPDATE xxeis.eis_rs_process_rpt_pivots
          SET process_id = p_process_id
        WHERE process_id = p_col_process_id
          AND REPORT_ID = p_report_id;

        UPDATE xxeis.eis_rs_process_rpt_pivot_dtls
          SET process_id = p_process_id
        WHERE process_id = p_col_process_id
          AND REPORT_ID = p_report_id;

        UPDATE xxeis.eis_rs_process_rpt_summ_calcs
          SET process_id = p_process_id
        where process_id = p_col_process_id;

           COMMIT;
        END IF;



        --Bug #854 --KP 7/26/2008
        IF p_col_process_id IS NULL THEN

            --i.e. the copy tables need to be re-populated not based on the existing process_id but afresh
            copy_report_parameters
            (
                    p_report_id  => p_report_id    ,
                    p_process_id => l_col_process_id   ,
                    p_existing_process_id => null -- pass existing process id as null
            );
--Updated by Praveen.P to fix Bug#1338 , 1339
               SET_DATE_PARAMS ( L_COL_PROCESS_ID, 'date_to_canonical' ) ; -- sets all dates g_param_tbl
--added by Sravani.M to fix bug# 4994
               UPDATE XXEIS.EIS_RS_PROCESS_RPTS SET process_id = l_process_id
               WHERE PROCESS_ID = l_col_process_id AND
               REPORT_ID = p_report_id;

             --by Sravani.M
               UPDATE xxeis.eis_rs_process_rpt_columns SET process_id = l_process_id
               WHERE process_id = l_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.EIS_RS_PROCESS_RPT_PARAMETERS set process_id = l_process_id
               WHERE process_id = l_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.EIS_RS_PROCESS_RPT_CONDITIONS set process_id = l_process_id
               WHERE process_id = l_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.EIS_RS_PROCESS_RPT_SORTS set process_id = l_process_id
               WHERE process_id = l_col_process_id
               AND REPORT_ID = p_report_id;

           UPDATE xxeis.EIS_RS_PROCESS_RPT_PGS set process_id = l_process_id
               WHERE process_id = l_col_process_id
               AND REPORT_ID = p_report_id;

           UPDATE xxeis.EIS_RS_PROCESS_RPT_DISTRIBUTE set process_id = l_process_id
               WHERE process_id = l_col_process_id
               AND report_id = p_report_id;

UPDATE xxeis.EIS_RS_PROCESS_RPT_PIVOTS set process_id = l_process_id
               WHERE process_id = l_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.EIS_RS_PROCESS_RPT_PIVOT_DTLS set process_id = l_process_id
               WHERE process_id = l_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.eis_rs_process_rpt_summ_calcs set process_id = l_process_id
               where process_id = l_col_process_id;

             --Updated by Praveen.P to fix Bug#1338 , 1339
               --Set the date
               set_date_params ( l_col_process_id, 'date_to_canonical' ) ; -- sets all dates g_param_tbl
        END IF;
                /* code added by ankur to enable scheduling in reports */
                --getting data into temp plsql table .
                  PARSE_DELIMITED_DATA ( p_schedule_param ,'#');
                    FOR g_data_ind IN g_data_tbl.first..g_data_tbl.last LOOP
                         IF substr(g_data_tbl(g_data_ind),1,7) = 'RUN_JOB' THEN
                            l_run_job := substr(g_data_tbl(g_data_ind),
                                                instr(g_data_tbl(g_data_ind),'=')+1,
                                                length(g_data_tbl(g_data_ind)) );

                         ELSIF rtrim(ltrim(substr(g_data_tbl(g_data_ind),1,8))) = 'START_AT' THEN
                            l_start_at := substr(g_data_tbl(g_data_ind),
                                                instr(g_data_tbl(g_data_ind),'=')+1,
                                                length(g_data_tbl(g_data_ind)) );

                         ELSIF rtrim(ltrim(substr(g_data_tbl(g_data_ind),1,6))) = 'END_AT' THEN
                            l_end_at := substr(g_data_tbl(g_data_ind),
                                                instr(g_data_tbl(g_data_ind),'=')+1,
                                                length(g_data_tbl(g_data_ind)) );

                         ELSIF rtrim(ltrim(substr(g_data_tbl(g_data_ind),1,23))) = 'RE_SUBMIT_INTERVAL_CODE' THEN
                            l_re_submit_interval_code := substr(g_data_tbl(g_data_ind),
                                                instr(g_data_tbl(g_data_ind),'=')+1,
                                                length(g_data_tbl(g_data_ind)) );

                         ELSIF rtrim(ltrim(substr(g_data_tbl(g_data_ind),1,15))) = 'INCREMENT_DATES' THEN
                            l_increment_dates := substr(g_data_tbl(g_data_ind),
                                                instr(g_data_tbl(g_data_ind),'=')+1,
                                                length(g_data_tbl(g_data_ind)) );

                         ELSIF rtrim(ltrim(substr(g_data_tbl(g_data_ind),1,18))) = 'RE_SUBMIT_INTERVAL' THEN
                            l_re_submit_interval := to_number(rtrim(ltrim(substr(g_data_tbl(g_data_ind),
                                                instr(g_data_tbl(g_data_ind),'=')+1,
                                                length(g_data_tbl(g_data_ind))))));

                         ELSIF rtrim(ltrim(substr(g_data_tbl(g_data_ind),1,10))) = 'USER_GROUP' THEN
                                /*write code here to notify users*/

                               l_notify :=substr(g_data_tbl(g_data_ind),
                                                instr(g_data_tbl(g_data_ind),'=')+1,
                                                length(g_data_tbl(g_data_ind)) );
                        END IF;
                       END LOOP;

                       -- Nelli, added condition for Weekly schedule
                       IF  ltrim(rtrim(l_re_submit_interval_code)) = 'WEEKS' THEN
                               l_re_submit_interval_code := 'DAYS';
                               l_re_submit_interval      := 7 * l_re_submit_interval;
                       END IF;
--Updated by Praveen.P to fix Bug#5621
         if trim(l_run_job) ='PERIODICALLY' THEN
         l_end_at := trim(l_end_at);
             l_end_at:= fnd_date.date_to_canonical(nvl(to_date(l_end_at, fnd_profile.VALUE ( 'ICX_DATE_FORMAT_MASK') ||' HH24:MI:SS'), hr_general.end_of_time)); --Bug# 5142
             l_schedule:= fnd_request.set_repeat_options(NULL,
             l_re_submit_interval,RTRIM(LTRIM(l_re_submit_interval_code)),'START', l_end_at ,RTRIM(LTRIM(l_increment_dates)));
       --  ELSIF trim(l_run_job) = 'ONCE' THEN
         --    l_start_at :=RTRIM(LTRIM(fnd_date.date_to_canonical(to_date(l_start_at,fnd_profile.VALUE ('ICX_DATE_FORMAT_MASK')||' HH24:MI:SS'))));
         END IF;


      -- Update Fnd table , just to keep data synchronized with our tables .
    --Updated by Praveen.P to fix Bug#1338 , 1339
          UPDATE xxeis.eis_rs_processes
             SET parameter1 = trim(g_param_tbl(1))
                ,parameter2 = trim(g_param_tbl(2))
                ,parameter3 = trim(g_param_tbl(3))
                ,parameter4 = trim(g_param_tbl(4))
                ,parameter5 = trim(g_param_tbl(5))
                ,parameter6 = trim(g_param_tbl(6))
                ,parameter7 = trim(g_param_tbl(7))
                ,parameter8 = trim(g_param_tbl(8))
                ,parameter9 = trim(g_param_tbl(9))
                ,parameter10 = trim(g_param_tbl(10))
                ,parameter11 = trim(g_param_tbl(11))
                ,parameter12 = trim(g_param_tbl(12))
                ,parameter13 = trim(g_param_tbl(13))
                ,parameter14 = trim(g_param_tbl(14))
                ,parameter15 = trim(g_param_tbl(15))
                ,parameter16 = trim(g_param_tbl(16))
                ,parameter17 = trim(g_param_tbl(17))
                ,parameter18 = trim(g_param_tbl(18))
                ,parameter19 = trim(g_param_tbl(19))
                ,parameter20 = trim(g_param_tbl(20))
                ,parameter21 = trim(g_param_tbl(21))
                ,parameter22 = trim(g_param_tbl(22))
                ,parameter23 = trim(g_param_tbl(23))
                ,parameter24 = trim(g_param_tbl(24))
                ,parameter25 = trim(g_param_tbl(25))
                ,parameter26 = trim(g_param_tbl(26))
                ,parameter27 = trim(g_param_tbl(27))
                ,parameter28 = trim(g_param_tbl(28))
                ,parameter29 = trim(g_param_tbl(29))
                ,parameter30 = trim(g_param_tbl(30))
                WHERE PROCESS_ID = p_process_id;
--Until here to fix Bug#1338 , 1339
                COMMIT ;

            l_start_at := fnd_date.date_to_canonical(nvl(to_date(l_start_at, fnd_profile.VALUE ( 'ICX_DATE_FORMAT_MASK') ||' HH24:MI:SS'), SYSDATE));

          -- Submission of the Request
                p_request_id := fnd_request.submit_request(
                                l_application_name,
                                'XXEIS_RS_SUBMIT_REPORT',
                                 NULL,
                                 l_start_at,
                                 FALSE,
                                 TO_CHAR(p_report_id),
                                 TRIM(l_report_name),
                                 TO_CHAR(p_process_id),
                                 --KP Bug#1338 Start
                                 TRIM(SUBSTR(g_param_tbl(1), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(2), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(3), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(4), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(5), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(6), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(7), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(8), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(9), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(10), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(11), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(12), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(13), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(14), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(15), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(16), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(17), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(18), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(19), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(20), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(21), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(22), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(23), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(24), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(25), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(26), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(27), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(28), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(29), 0, 240)),
                                 TRIM(SUBSTR(g_param_tbl(30), 0, 240)),
                                 --KP Bug#1338 End
                                 TO_CHAR(l_user_id),
                                 to_char(p_session_id), -- Added for the schedule request.
                                 TRIM(l_notify),
                                 TRIM(p_server_url), -- Added to provide link in notification
                                 CHR(0));

                COMMIT;
            -- commented for the schedule request testing..
            IF p_request_id > 0 THEN
                UPDATE xxeis.eis_rs_processes
                   set request_id = p_request_id
                WHERE process_id = p_process_id ;
                COMMIT ;
            END IF ;

            IF p_request_id = 0  OR p_request_id IS NULL THEN
                p_error_msg := 'There is a problem with Concurrent manager.   Request Id has not been created.' ;

        INSERT INTO xxeis.eis_rs_errors
          ( error_id,
            process_id,
            report_id,
            error_message,
            status,
            program_location
          )
          VALUES
          ( xxeis.eis_rs_errors_s.NEXTVAL,
            p_process_id,
            p_report_id,
            p_error_msg,
            'U',
            l_program_location
          );

        COMMIT ;

                -- commented for the schedule request testing..
                UPDATE xxeis.eis_rs_processes
                SET status = 'E'
                WHERE process_id = l_process_id ;
                COMMIT ;
            END IF ;
   END IF ;
 EXCEPTION WHEN OTHERS THEN
    p_error_msg := SQLERRM ;
    INSERT INTO xxeis.eis_rs_errors
      ( error_id,
        process_id,
        report_id,
        error_message,
        status,
        program_location
      )
      VALUES
      ( xxeis.eis_rs_errors_s.nextval,
        p_process_id,
        p_report_id,
        p_error_msg,
        'U',
        l_program_location
      );
    COMMIT ;

 END submit_process;

 PROCEDURE submit_conc_report ( p_errbuf                 OUT VARCHAR2
                               ,p_retcode                OUT NUMBER
                               ,p_report_id              IN NUMBER DEFAULT NULL
                               ,p_report_name            IN VARCHAR2 DEFAULT NULL
                               ,p_process_id             IN NUMBER DEFAULT null
                               ,p_param1                 IN VARCHAR2 DEFAULT NULL
                               ,p_param2                 IN VARCHAR2 DEFAULT NULL
                               ,p_param3                 IN VARCHAR2 DEFAULT NULL
                               ,p_param4                 IN VARCHAR2 DEFAULT NULL
                               ,p_param5                 IN VARCHAR2 DEFAULT NULL
                               ,p_param6                 IN VARCHAR2 DEFAULT NULL
                               ,p_param7                 IN VARCHAR2 DEFAULT NULL
                               ,p_param8                 IN VARCHAR2 DEFAULT NULL
                               ,p_param9                 IN VARCHAR2 DEFAULT NULL
                               ,p_param10                IN VARCHAR2 DEFAULT NULL
                               ,p_param11                IN VARCHAR2 DEFAULT NULL
                               ,p_param12                IN VARCHAR2 DEFAULT NULL
                               ,p_param13                IN VARCHAR2 DEFAULT NULL
                               ,p_param14                IN VARCHAR2 DEFAULT NULL
                               ,p_param15                IN VARCHAR2 DEFAULT NULL
                               ,p_param16                IN VARCHAR2 DEFAULT NULL
                               ,p_param17                IN VARCHAR2 DEFAULT NULL
                               ,p_param18                IN VARCHAR2 DEFAULT NULL
                               ,p_param19                IN VARCHAR2 DEFAULT NULL
                               ,p_param20                IN VARCHAR2 DEFAULT NULL
                               ,p_param21                IN VARCHAR2 DEFAULT NULL
                               ,p_param22                IN VARCHAR2 DEFAULT NULL
                               ,p_param23                IN VARCHAR2 DEFAULT NULL
                               ,p_param24                IN VARCHAR2 DEFAULT NULL
                               ,p_param25                IN VARCHAR2 DEFAULT NULL
                               ,p_param26                IN VARCHAR2 DEFAULT NULL
                               ,p_param27                IN VARCHAR2 DEFAULT NULL
                               ,p_param28                IN VARCHAR2 DEFAULT NULL
                               ,p_param29                IN VARCHAR2 DEFAULT NULL
                               ,p_param30                IN VARCHAR2 DEFAULT NULL
                               ,p_user_id                IN NUMBER DEFAULT -1
                               ,p_session_id             IN NUMBER
                               ,p_notify                 IN VARCHAR2 DEFAULT NULL
                               ,p_server_url             IN VARCHAR2 DEFAULT NULL
                               ) IS

    --Cursor is used to get the Email ids
CURSOR get_userid_c(c_emp_name in VARCHAR2) IS
       SELECT fu.user_id, ppf.person_id
         FROM fnd_user fu,per_all_people_f ppf
    WHERE fu.EMPLOYEE_ID = ppf.person_id
      AND TRUNC(SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
        AND replace(ppf.full_name,'''') = c_emp_name
        AND ROWNUM < 2;

    --Cursor is used to get the parameter values
    CURSOR get_param_values_c(c_process_id IN VARCHAR2) IS
     SELECT * FROM xxeis.eis_rs_processes
      WHERE PROCESS_ID =c_process_id;

    l_result        CLOB ;
    l_process_id    NUMBER := p_process_id;
    l_error_msg     VARCHAR2(500) ;
    l_request_id    NUMBER ;
    l_request_value VARCHAR2(20) ;
    l_submit_type   VARCHAR2(10) := 'CONC' ;
    l_application_id    NUMBER ;
    l_report_name   VARCHAR2(100);
    l_session_id    NUMBER := p_session_id ;
    l_process_end_time date;

    /*parameter added for incrementing dates*/
    l_increment_dates_flag fnd_concurrent_requests.increment_dates%TYPE ;
    l_interval NUMBER ;
    l_duration VARCHAR2 (100);

    l_status           xxeis.eis_rs_processes.status%TYPE ;
    l_user_id          NUMBER;
    l_person_id        NUMBER;
    l_user_group_id    xxeis.eis_rs_user_groups.user_group_id%TYPE ;
    l_notify_emails    VARCHAR2(4000);

    l_col_process_id    number;--6.05
    l_cnt               number;--6.05

    --Bug# 5441
    l_session_date_format_mask  xxeis.eis_rs_sessions.date_format_mask%type;

    CURSOR get_pre_process_parameters (p_proc_id in number) IS
            SELECT display_order
            FROM xxeis.eis_rs_process_rpt_parameters
            WHERE process_id = p_proc_id
        AND (data_type = 'DATE' OR parameter_name LIKE '%Period%Date' OR parameter_name LIKE ('Quarter%Date'))
            ORDER BY display_order ;
/*Below 2 lines are added by soujanya to fix g_param_tbl as some times the value is going  null to submit report procedure*/
TYPE l_parse_tbl_def IS TABLE OF VARCHAR2(10000) INDEX BY BINARY_INTEGER;
  l_param_tbl          l_parse_tbl_def ;

BEGIN

    fnd_profile.get('CONC_REQUEST_ID', l_request_value) ;

    g_log := 'Started Conc Report - ' || l_request_value ;

    --Added by Praveen.P to get the request id
    IF l_request_value IS NULL THEN
      l_request_value := fnd_global.conc_request_id;
    END IF;

    g_log := g_log||' Global Conc Report g- ' || l_request_value ;

    -- To fix Request ID missing issue in Requests tab
    IF l_request_value IS NOT NULL THEN
        l_request_id := to_number( trim(l_request_value) ) ;
    ELSE
        SELECT  request_id
        INTO  l_request_id
        FROM  xxeis.eis_rs_processes
        WHERE  process_id = p_process_id ;
    END IF;

    SELECT  application_id, report_name
      INTO  l_application_id, l_report_name
      FROM xxeis.eis_rs_reports
     WHERE report_id = p_report_id ;

       --start schedule
    BEGIN
        SELECT  end_time
        INTO  l_process_end_time
        FROM  xxeis.eis_rs_processes
        WHERE  process_id = p_process_id ;
    EXCEPTION WHEN OTHERS THEN
        --l_process_end_time := NULL;
       --updated by Sravani.M to fix multi language changes bug
        l_process_end_time :=  hr_general.end_of_time;
    END ;

    g_param_tbl.DELETE ;

    g_param_tbl(1) :=p_param1;
    g_param_tbl(2) :=p_param2;
    g_param_tbl(3) :=p_param3;
    g_param_tbl(4) :=p_param4;
    g_param_tbl(5) :=p_param5;
    g_param_tbl(6) :=p_param6;
    g_param_tbl(7) :=p_param7;
    g_param_tbl(8) :=p_param8;
    g_param_tbl(9) :=p_param9;
    g_param_tbl(10) :=p_param10;
    g_param_tbl(11) :=p_param11;
    g_param_tbl(12) :=p_param12;
    g_param_tbl(13) :=p_param13;
    g_param_tbl(14) :=p_param14;
    g_param_tbl(15) :=p_param15;
    g_param_tbl(16) :=p_param16;
    g_param_tbl(17) :=p_param17;
    g_param_tbl(18) :=p_param18;
    g_param_tbl(19) :=p_param19;
    g_param_tbl(20) :=p_param20;
    g_param_tbl(21) :=p_param21;
    g_param_tbl(22) :=p_param22;
    g_param_tbl(23) :=p_param23;
    g_param_tbl(24) :=p_param24;
    g_param_tbl(25) :=p_param25;
    g_param_tbl(26) :=p_param26;
    g_param_tbl(27) :=p_param27;
    g_param_tbl(28) :=p_param28;
    g_param_tbl(29) :=p_param29;
    g_param_tbl(30) :=p_param30;

    --If Scheduled program
    IF  l_process_end_time IS NOT NULL THEN
        --report is running under schedule mode.for first run it will skip this code.
    --added by Praveen.P to get request id
    IF l_request_value IS  NULL THEN
      SELECT MAX(request_id)
        INTO l_request_value
      FROM fnd_concurrent_requests fcr,
           fnd_concurrent_programs fcp
      WHERE fcp.concurrent_program_id = fcr.concurrent_program_id
        AND fcp.concurrent_program_name = 'XXEIS_RS_SUBMIT_REPORT'
        AND requested_by=p_user_id
        AND  status_code='R';
    END IF;

    -- To update date format for scheduled processes Bug# 5441
        SELECT nvl(date_format_mask,'DD-MON-YYYY')
    INTO l_session_date_format_mask
        FROM xxeis.eis_rs_sessions ers
        WHERE session_id = l_session_id;

        IF l_session_date_format_mask <> xxeis.eis_rs_utility.get_date_format THEN
          UPDATE xxeis.eis_rs_sessions
            SET date_format_mask = xxeis.eis_rs_utility.get_date_format
            WHERE session_id = l_session_id;
          COMMIT;
        END IF;

        --get duration and amount of date increment. it will depend upon how frequent report is run.
        SELECT INCREMENT_DATES,RESUBMIT_INTERVAL ,
        DECODE(RESUBMIT_INTERVAL_UNIT_CODE,'MINUTES','M','HOURS','H','DAYS','D','WEEKS','W','MONTHS','Z','D')
        INTO l_increment_dates_flag ,l_interval,l_duration
        FROM fnd_concurrent_requests
        WHERE request_id =l_request_id;

/** --6.05 : Call the copy process to populate the tables **/
        SELECT count(1)
        INTO l_cnt
        FROM xxeis.eis_rs_process_rpt_columns
        WHERE process_id = p_process_id;

        IF (l_cnt > 0) then
        --i.e. the old process id's rows exist in the copy tables
    copy_report_parameters( p_report_id  => p_report_id,
                p_process_id => l_col_process_id,
                p_existing_process_id => p_process_id
                   );

        ELSE
        --i.e. the copy tables need to be re-populated not based on the existing process_id but afresh, pass existing process id as null
    copy_report_parameters( p_report_id  => p_report_id,
                p_process_id => l_col_process_id,
                p_existing_process_id => null
                   );
        END IF;

/** --6.05 : End- Call the copy process to populate the tables **/
        xxeis.eis_rs_insert.eis_rs_processes( p_process_id => l_process_id
                                                , p_request_id  => l_request_id
                                                , p_start_time   => SYSDATE
                                                , p_end_time   => NULL
                                                , p_report_id   => p_report_id
                                                , p_parameters  => NULL
                                                , p_output_file_name => NULL
                                                , p_status  => 'U'
                                                , p_clob_file  => NULL
                                                , p_session_id  => l_session_id
                                                , p_sql_string   => NULL
                                                , p_created_by  => p_user_id
                                                , p_creation  => SYSDATE
                                                , p_auto_commit => 'Y'
                                                , p_operation_type  => 'I'
                                                , p_insertion_status => l_error_msg ) ;


        /** --6.05 : Update currently populated l_col_process_id with newly generated l_process_id **/
        UPDATE xxeis.eis_rs_process_rpts set process_id = l_process_id
        WHERE process_id = l_col_process_id
        AND REPORT_ID = p_report_id;

        UPDATE xxeis.eis_rs_process_rpt_columns set process_id = l_process_id
        WHERE process_id = l_col_process_id
        AND REPORT_ID = p_report_id;

        UPDATE xxeis.EIS_RS_PROCESS_RPT_PARAMETERS set process_id = l_process_id
        WHERE process_id = l_col_process_id
        AND REPORT_ID = p_report_id;

        UPDATE xxeis.EIS_RS_PROCESS_RPT_CONDITIONS set process_id = l_process_id
        WHERE process_id = l_col_process_id
        AND REPORT_ID = p_report_id;

        UPDATE xxeis.EIS_RS_PROCESS_RPT_SORTS set process_id = l_process_id
        WHERE process_id = l_col_process_id
        AND REPORT_ID = p_report_id;

        UPDATE xxeis.EIS_RS_PROCESS_RPT_PGS set process_id = l_process_id
        WHERE process_id = l_col_process_id
        AND REPORT_ID = p_report_id;

    UPDATE xxeis.EIS_RS_PROCESS_RPT_DISTRIBUTE set process_id = l_process_id
        WHERE process_id = l_col_process_id
        AND REPORT_ID = p_report_id;

        UPDATE xxeis.EIS_RS_PROCESS_RPT_PIVOTS set process_id = l_process_id
               WHERE process_id = l_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.EIS_RS_PROCESS_RPT_PIVOT_DTLS set process_id = l_process_id
               WHERE process_id = l_col_process_id
               AND REPORT_ID = p_report_id;

               UPDATE xxeis.eis_rs_process_rpt_summ_calcs set process_id = l_process_id
               where process_id = l_col_process_id;
        g_report_process_id := l_process_id ; -- 6.04

        --Copy the parameter value from original process_id if larger parameter values
        --There is a limitation of 240 characters for concurrent program.
        FOR i IN 1..30 LOOP
            IF length(g_param_tbl(i)) > 230 THEN
                execute IMMEDIATE 'select parameter'||i|| ' from xxeis.eis_rs_processes ' ||
                                  ' where process_id = :1' INTO g_param_tbl(i) USING p_process_id ;

            END IF;
        END LOOP;

        -- if increment date  is checked , to update dates value each run time .
        IF l_increment_dates_flag = 'Y' THEN

            FOR get_pre_process_parameters_rec IN get_pre_process_parameters ( l_process_id ) LOOP
                g_param_tbl(get_pre_process_parameters_rec.display_order) :=
                    increment_date(g_param_tbl(get_pre_process_parameters_rec.display_order),l_interval,l_duration);
            END LOOP ;

            -- Update Fnd table , just to keep data synchronized with our tables .
            UPDATE fnd_concurrent_requests
                set ARGUMENT4 =  SUBSTR(g_param_tbl(1),1,240)
                ,   ARGUMENT5 =  SUBSTR(g_param_tbl(2),1,240)
                ,   ARGUMENT6 =  SUBSTR(g_param_tbl(3),1,240)
                ,   ARGUMENT7 =  SUBSTR(g_param_tbl(4),1,240)
                ,   ARGUMENT8 =  SUBSTR(g_param_tbl(5),1,240)
                ,   ARGUMENT9 =  SUBSTR(g_param_tbl(6),1,240)
                ,   ARGUMENT10 =  SUBSTR(g_param_tbl(7),1,240)
                ,   ARGUMENT11 =  SUBSTR(g_param_tbl(8),1,240)
                ,   ARGUMENT12 =  SUBSTR(g_param_tbl(9),1,240)
                ,   ARGUMENT13 =  SUBSTR(g_param_tbl(10),1,240)
                ,   ARGUMENT14 =  SUBSTR(g_param_tbl(11),1,240)
                ,   ARGUMENT15 =  SUBSTR(g_param_tbl(12),1,240)
                ,   ARGUMENT16 =  SUBSTR(g_param_tbl(13),1,240)
                ,   ARGUMENT17 =  SUBSTR(g_param_tbl(14),1,240)
                ,   ARGUMENT18 =  SUBSTR(g_param_tbl(15),1,240)
                ,   ARGUMENT19 =  SUBSTR(g_param_tbl(16),1,240)
                ,   ARGUMENT20 =  SUBSTR(g_param_tbl(17),1,240)
                ,   ARGUMENT21 =  SUBSTR(g_param_tbl(18),1,240)
                ,   ARGUMENT22 =  SUBSTR(g_param_tbl(19),1,240)
                ,   ARGUMENT23 =  SUBSTR(g_param_tbl(20),1,240)
                ,   ARGUMENT24 =  SUBSTR(g_param_tbl(21),1,240)
                ,   ARGUMENT25 =  SUBSTR(g_param_tbl(22),1,240)
            WHERE request_id =l_request_id;

            COMMIT ;

        END IF ; -- Inc_flag = 'Y'

        UPDATE xxeis.eis_rs_processes
        SET parameter1 = trim(g_param_tbl(1))
                ,parameter2 = trim(g_param_tbl(2))
                ,parameter3 = trim(g_param_tbl(3))
                ,parameter4 = trim(g_param_tbl(4))
                ,parameter5 = trim(g_param_tbl(5))
                ,parameter6 = trim(g_param_tbl(6))
                ,parameter7 = trim(g_param_tbl(7))
                ,parameter8 = trim(g_param_tbl(8))
                ,parameter9 = trim(g_param_tbl(9))
                ,parameter10 = trim(g_param_tbl(10))
                ,parameter11 = trim(g_param_tbl(11))
                ,parameter12 = trim(g_param_tbl(12))
                ,parameter13 = trim(g_param_tbl(13))
                ,parameter14 = trim(g_param_tbl(14))
                ,parameter15 = trim(g_param_tbl(15))
                ,parameter16 = trim(g_param_tbl(16))
                ,parameter17 = trim(g_param_tbl(17))
                ,parameter18 = trim(g_param_tbl(18))
                ,parameter19 = trim(g_param_tbl(19))
                ,parameter20 = trim(g_param_tbl(20))
                ,parameter21 = trim(g_param_tbl(21))
                ,parameter22 = trim(g_param_tbl(22))
                ,parameter23 = trim(g_param_tbl(23))
                ,parameter24 = trim(g_param_tbl(24))
                ,parameter25 = trim(g_param_tbl(25))
                ,parameter26 = trim(g_param_tbl(26))
                ,parameter27 = trim(g_param_tbl(27))
                ,parameter28 = trim(g_param_tbl(28))
                ,parameter29 = trim(g_param_tbl(29))
                ,parameter30 = trim(g_param_tbl(30))
        WHERE PROCESS_ID = l_process_id;

    ELSE --Not a scheduled program

        g_param_tbl.DELETE ;

        FOR csr_param_values_rec IN get_param_values_c
                                 (c_process_id   => l_process_id)
        LOOP
            IF csr_param_values_rec.PARAMETER1 IS NOT NULL THEN
               g_param_tbl(1) :=csr_param_values_rec.PARAMETER1;
            ELSE
               g_param_tbl(1) :=p_param1;
            END IF;

            IF csr_param_values_rec.PARAMETER2 IS NOT NULL THEN
                g_param_tbl(2) :=csr_param_values_rec.PARAMETER2;
            ELSE
                g_param_tbl(2) :=p_param2;
            END IF;

            IF csr_param_values_rec.PARAMETER3 IS NOT NULL THEN
                g_param_tbl(3) :=csr_param_values_rec.PARAMETER3;
            ELSE
                g_param_tbl(3) :=p_param3;
            END IF;

            IF csr_param_values_rec.PARAMETER4 IS NOT NULL THEN
                g_param_tbl(4) :=csr_param_values_rec.PARAMETER4;
            ELSE
                g_param_tbl(4) :=p_param4;
            END IF;

            IF csr_param_values_rec.PARAMETER5 IS NOT NULL THEN
                g_param_tbl(5) :=csr_param_values_rec.PARAMETER5;
            ELSE
                g_param_tbl(5) :=p_param5;
            END IF;

            IF csr_param_values_rec.PARAMETER6 IS NOT NULL THEN
                g_param_tbl(6) :=csr_param_values_rec.PARAMETER6;
            ELSE
                g_param_tbl(6) :=p_param6;
            END IF;

            IF csr_param_values_rec.PARAMETER7 IS NOT NULL THEN
                g_param_tbl(7) :=csr_param_values_rec.PARAMETER7;
            ELSE
                g_param_tbl(7) :=p_param7;
            END IF;

            IF csr_param_values_rec.PARAMETER8 IS NOT NULL THEN
                g_param_tbl(8) :=csr_param_values_rec.PARAMETER8;
            ELSE
                g_param_tbl(8) :=p_param8;
            END IF;

            IF csr_param_values_rec.PARAMETER9 IS NOT NULL THEN
                g_param_tbl(9) :=csr_param_values_rec.PARAMETER9;
            ELSE
                g_param_tbl(9) :=p_param9;
            END IF;

            IF csr_param_values_rec.PARAMETER10 IS NOT NULL THEN
                g_param_tbl(10) :=csr_param_values_rec.PARAMETER10;
            ELSE
                g_param_tbl(10) :=p_param10;
            END IF;

            IF csr_param_values_rec.PARAMETER11 IS NOT NULL THEN
                g_param_tbl(11) :=csr_param_values_rec.PARAMETER11;
            ELSE
                g_param_tbl(11) :=p_param11;
            END IF;

            IF csr_param_values_rec.PARAMETER12 IS NOT NULL THEN
                g_param_tbl(12) :=csr_param_values_rec.PARAMETER12;
            ELSE
                g_param_tbl(12) :=p_param12;
            END IF;

            IF csr_param_values_rec.PARAMETER13 IS NOT NULL THEN
                g_param_tbl(13) :=csr_param_values_rec.PARAMETER13;
            ELSE
                g_param_tbl(13) :=p_param13;
            END IF;

            IF csr_param_values_rec.PARAMETER14 IS NOT NULL THEN
                g_param_tbl(14) :=csr_param_values_rec.PARAMETER14;
            ELSE
                g_param_tbl(14) :=p_param14;
            END IF;

            IF csr_param_values_rec.PARAMETER15 IS NOT NULL THEN
                g_param_tbl(15) :=csr_param_values_rec.PARAMETER15;
            ELSE
                g_param_tbl(15) :=p_param15;
            END IF;

            IF csr_param_values_rec.PARAMETER16 IS NOT NULL THEN
                g_param_tbl(16) :=csr_param_values_rec.PARAMETER16;
            ELSE
                g_param_tbl(16) :=p_param16;
            END IF;

            IF csr_param_values_rec.PARAMETER17 IS NOT NULL THEN
                g_param_tbl(17) :=csr_param_values_rec.PARAMETER17;
            ELSE
                g_param_tbl(17) :=p_param17;
            END IF;

            IF csr_param_values_rec.PARAMETER18 IS NOT NULL THEN
                g_param_tbl(18) :=csr_param_values_rec.PARAMETER18;
            ELSE
                g_param_tbl(18) :=p_param18;
            END IF;

            IF csr_param_values_rec.PARAMETER19 IS NOT NULL THEN
                g_param_tbl(19) :=csr_param_values_rec.PARAMETER19;
            ELSE
                g_param_tbl(19) :=p_param19;
            END IF;

            IF csr_param_values_rec.PARAMETER20 IS NOT NULL THEN
                g_param_tbl(20) :=csr_param_values_rec.PARAMETER20;
            ELSE
                g_param_tbl(20) :=p_param20;
            END IF;

            IF csr_param_values_rec.PARAMETER21 IS NOT NULL THEN
                g_param_tbl(21) :=csr_param_values_rec.PARAMETER21;
            ELSE
                g_param_tbl(21) :=p_param21;
            END IF;

            IF csr_param_values_rec.PARAMETER22 IS NOT NULL THEN
                g_param_tbl(22) :=csr_param_values_rec.PARAMETER22;
            ELSE
                g_param_tbl(22) :=p_param22;
            END IF;

            IF csr_param_values_rec.PARAMETER23 IS NOT NULL THEN
                g_param_tbl(23) :=csr_param_values_rec.PARAMETER23;
            ELSE
                g_param_tbl(23) :=p_param23;
            END IF;

            IF csr_param_values_rec.PARAMETER24 IS NOT NULL THEN
                g_param_tbl(24) :=csr_param_values_rec.PARAMETER24;
            ELSE
                g_param_tbl(24) :=p_param24;
            END IF;

            IF csr_param_values_rec.PARAMETER25 IS NOT NULL THEN
                g_param_tbl(25) :=csr_param_values_rec.PARAMETER25;
            ELSE
                g_param_tbl(25) :=p_param25;
            END IF;

            IF csr_param_values_rec.PARAMETER26 IS NOT NULL THEN
                g_param_tbl(26) :=csr_param_values_rec.PARAMETER26;
            ELSE
                g_param_tbl(26) :=p_param26;
            END IF;

            IF csr_param_values_rec.PARAMETER27 IS NOT NULL THEN
                g_param_tbl(27) :=csr_param_values_rec.PARAMETER27;
            ELSE
                g_param_tbl(27) :=p_param27;
            END IF;

            IF csr_param_values_rec.PARAMETER28 IS NOT NULL THEN
                g_param_tbl(28) :=csr_param_values_rec.PARAMETER28;
            ELSE
                g_param_tbl(28) :=p_param28;
            END IF;

            IF csr_param_values_rec.PARAMETER29 IS NOT NULL THEN
                g_param_tbl(29) :=csr_param_values_rec.PARAMETER29;
            ELSE
                g_param_tbl(29) :=p_param29;
            END IF;

            IF csr_param_values_rec.PARAMETER30 IS NOT NULL THEN
                g_param_tbl(30) :=csr_param_values_rec.PARAMETER30;
            ELSE
                g_param_tbl(30) :=p_param30;
            END IF;

        END LOOP;

    END IF; --process_end_time is null


        g_log := g_log || chr(10) || 'Calling EIS report engine for creating clob... ' ;

     for i in 1..30 loop
          l_param_tbl(i) := g_param_tbl(i);
        end loop ;

        submit_report( p_report_id          => p_report_id
                      ,p_result             => l_result
                      ,p_user_id            => p_user_id
                      ,p_process_id         => l_process_id
                      ,p_request_id         => l_request_id
                      ,p_view_sql           => l_submit_type
                      ,p_error_msg          => l_error_msg
                      ,p_param1             => l_param_tbl(1)
                      ,p_param2             => l_param_tbl(2)
                      ,p_param3             => l_param_tbl(3)
                      ,p_param4             => l_param_tbl(4)
                      ,p_param5             => l_param_tbl(5)
                      ,p_param6             => l_param_tbl(6)
                      ,p_param7             => l_param_tbl(7)
                      ,p_param8             => l_param_tbl(8)
                      ,p_param9             => l_param_tbl(9)
                      ,p_param10            => l_param_tbl(10)
                      ,p_param11            => l_param_tbl(11)
                      ,p_param12            => l_param_tbl(12)
                      ,p_param13            => l_param_tbl(13)
                      ,p_param14            => l_param_tbl(14)
                      ,p_param15            => l_param_tbl(15)
                      ,p_param16            => l_param_tbl(16)
                      ,p_param17            => l_param_tbl(17)
                      ,p_param18            => l_param_tbl(18)
                      ,p_param19            => l_param_tbl(19)
                      ,p_param20            => l_param_tbl(20)
                      ,p_param21            => l_param_tbl(21)
                      ,p_param22            => l_param_tbl(22)
                      ,p_param23            => l_param_tbl(23)
                      ,p_param24            => l_param_tbl(24)
                      ,p_param25            => l_param_tbl(25)
                      ,p_param26            => l_param_tbl(26)
                      ,p_param27            => l_param_tbl(27)
                      ,p_param28            => l_param_tbl(28)
                      ,p_param29            => l_param_tbl(29)
                      ,p_param30            => l_param_tbl(30)
                       ) ;
    -- Bug# 4225 fix
    SELECT status  INTO l_status
     FROM xxeis.eis_rs_processes
     WHERE process_id =l_process_id;

     IF l_status = 'E' THEN
      p_retcode := 2;
     END IF;

-- Distribution starts from here
    -- For Old scheduled jobs
    IF p_notify IS NOT NULL AND l_process_end_time IS NOT NULL
    THEN

        SELECT COUNT(*)
        INTO l_cnt
        FROM XXEIS.EIS_RS_PROCESS_RPT_DISTRIBUTE
        WHERE process_id = l_process_id ;

        IF l_cnt = 0 THEN

            l_notify_emails   := substr(p_notify,instr(p_notify,'$')+9,length(p_notify));
            l_user_group_id := trim(substr(p_notify,1,instr(p_notify,'$')-1));

            IF l_notify_emails IS NOT NULL THEN

                OPEN  get_userid_c(c_emp_name => trim(l_notify_emails));
                FETCH get_userid_c into l_user_id, l_person_id;
                CLOSE get_userid_c;

                xxeis.eis_rs_distribution_pkg.eis_rs_process_rpt_distribute
                    (p_process_distribution_id => -1,
                    p_process_id              => p_process_id ,
                    p_report_id               => p_report_id ,
                    p_PERSON_ID               => l_person_id ,
                    p_user_id                 => l_user_id ,
                    p_EMAIL_ADDRESS           => NULL ,
                    p_user_group_id           => NULL ,
                    p_select_flag             => 'Y') ;

            END IF;
            IF l_user_group_id IS NOT NULL THEN
                xxeis.eis_rs_distribution_pkg.eis_rs_process_rpt_distribute
                    (p_process_distribution_id => -1,
                    p_process_id              => p_process_id ,
                    p_report_id               => p_report_id ,
                    p_PERSON_ID               => NULL,
                    p_user_id                 => NULL,
                    p_EMAIL_ADDRESS           => NULL ,
                    p_user_group_id           => l_user_group_id ,
                    p_select_flag             => 'Y');
            END IF;

        END IF;

    END IF;

    xxeis.eis_rs_distribution_pkg.distribute(l_process_id);

    xxeis.eis_rs_utility.process_log ( l_process_id, g_log ) ;
    g_log := null ;
 END submit_conc_report ;

 --The following function returns the URL for hyperlinks by substituting required param values

 FUNCTION get_url( p_url_str IN VARCHAR2, p_request_id IN number DEFAULT NULL ) RETURN VARCHAR2
 IS
    l_in_url                    VARCHAR2(4000) := p_url_str ;
    l_delim                     VARCHAR2(4) := NULL;
    l_check_params_counter        NUMBER ;
    l_index                     NUMBER:=0 ;
    l_out_url                   VARCHAR2(4000)  ;
    l_url_tbl                    url_def ;
    l_server_name               varchar2(200) ;
    l_protocol                  VARCHAR2(10) := 'http';
    l_enc_session               VARCHAR2(200);

 BEGIN
-- Ankur:: Added Code to get Protocol from Admin Parameters.
    BEGIN
        SELECT PARAMETER_VALUE
        INTO l_protocol
        FROM xxeis.eis_rs_admin_parameters
        WHERE upper(PARAMETER_NAME) = UPPER('browser_protocol');
    EXCEPTION WHEN OTHERS THEN
        l_protocol := 'http';
    END ;


    IF upper(l_in_url) not like '%HTTP%' THEN

        BEGIN
            SELECT apps_server_name,xxeis.eis_rs_utility.x(erp.session_id)enc_session
            INTO l_server_name,l_enc_session
            FROM xxeis.eis_rs_processes erp, xxeis.eis_rs_sessions ers
            WHERE erp.session_id = ers.session_id
            and request_id = p_request_id ;
        EXCEPTION WHEN OTHERS THEN
            l_server_name := NULL;
            l_enc_session := NULL;
        END;

        IF l_server_name IS NULL THEN

            SELECT apps_server_name,xxeis.eis_rs_utility.x(ers.session_id)enc_session
            INTO l_server_name,l_enc_session
            FROM xxeis.eis_rs_sessions ers
            WHERE session_id = ( SELECT max(session_id)
                                   FROM  xxeis.eis_rs_sessions ) ;

        END IF ;

        IF l_enc_session IS NOT NULL THEN
            l_in_url := REPLACE(l_in_url,'@osid@',l_enc_session);
        END IF;

        IF l_server_name IS NOT NULL THEN

            IF l_protocol ='HTTPS' THEN
                l_in_url := 'https://' || l_server_name || '/OA_HTML/eisrs/jsp/reporting/' || l_in_url ;
            ELSE
                l_in_url := 'http://' || l_server_name || '/OA_HTML/eisrs/jsp/reporting/' || l_in_url ;
            END IF ;
        END IF;
    END IF;


    l_check_params_counter := INSTR(l_in_url,'"');

    LOOP

        EXIT WHEN ( l_check_params_counter = 0 );

        l_url_tbl(l_index) := SUBSTR(l_in_url, 1, l_check_params_counter-1);
        l_in_url:= SUBSTR(l_in_url, l_check_params_counter+1, LENGTH(l_in_url));
        l_check_params_counter := INSTR(nvl(l_in_url,0),'"');
        l_index := l_index + 1 ;

    END LOOP ;
    l_url_tbl(l_index) := l_in_url ;

    --The following logic distinguishes view colums from parameters

    FOR l_index IN l_url_tbl.FIRST..l_url_tbl.LAST LOOP
        IF (l_index != 0) AND ( INSTR(l_url_tbl(l_index), ':') > 0 ) THEN
            l_url_tbl(l_index) := ''''||l_url_tbl(l_index)||'''' ;
        END IF ;
    END LOOP ;

    --The following logic constructs return URL

    FOR l_index IN l_url_tbl.FIRST..l_url_tbl.LAST LOOP

        l_out_url := l_out_url||l_delim||l_url_tbl(l_index) ;

        IF l_delim = '''||' THEN
            l_delim := '||''' ;
        ELSE
            l_delim := '''||' ;
        END IF ;
    END LOOP ;

    RETURN l_out_url||'''' ;

 END get_url;

 FUNCTION get_view_sql ( p_report_id              IN NUMBER
                        ,p_param1                 IN VARCHAR2 DEFAULT NULL
                        ,p_param2                 IN VARCHAR2 DEFAULT NULL
                        ,p_param3                 IN VARCHAR2 DEFAULT NULL
                        ,p_param4                 IN VARCHAR2 DEFAULT NULL
                        ,p_param5                 IN VARCHAR2 DEFAULT NULL
                        ,p_param6                 IN VARCHAR2 DEFAULT NULL
                        ,p_param7                 IN VARCHAR2 DEFAULT NULL
                        ,p_param8                 IN VARCHAR2 DEFAULT NULL
                        ,p_param9                 IN VARCHAR2 DEFAULT NULL
                        ,p_param10                IN VARCHAR2 DEFAULT NULL
                        ,p_param11                IN VARCHAR2 DEFAULT NULL
                        ,p_param12                IN VARCHAR2 DEFAULT NULL
                        ,p_param13                IN VARCHAR2 DEFAULT NULL
                        ,p_param14                IN VARCHAR2 DEFAULT NULL
                        ,p_param15                IN VARCHAR2 DEFAULT NULL
                        ,p_param16                IN VARCHAR2 DEFAULT NULL
                        ,p_param17                IN VARCHAR2 DEFAULT NULL
                        ,p_param18                IN VARCHAR2 DEFAULT NULL
                        ,p_param19                IN VARCHAR2 DEFAULT NULL
                        ,p_param20                IN VARCHAR2 DEFAULT NULL
                        ,p_param21                IN VARCHAR2 DEFAULT NULL
                        ,p_param22                IN VARCHAR2 DEFAULT NULL
                        ,p_param23                IN VARCHAR2 DEFAULT NULL
                        ,p_param24                IN VARCHAR2 DEFAULT NULL
                        ,p_param25                IN VARCHAR2 DEFAULT NULL
                        ,p_param26                IN VARCHAR2 DEFAULT NULL
                        ,p_param27                IN VARCHAR2 DEFAULT NULL
                        ,p_param28                IN VARCHAR2 DEFAULT NULL
                        ,p_param29                IN VARCHAR2 DEFAULT NULL
                        ,p_param30                IN VARCHAR2 DEFAULT NULL
                        ,p_col_process_id         IN NUMBER   DEFAULT NULL
                         ) RETURN VARCHAR2 IS

    l_str           VARCHAR2(32000) := 'SQL';
    l_result        CLOB ;
    l_request_id    NUMBER ;
    l_error_msg     VARCHAR2(1000) ;
    l_process_id    NUMBER ;
    l_user_id       NUMBER := -1 ;
    l_report_name   VARCHAR2(100) := NULL ;

 BEGIN



   submit_report(  p_report_id          => p_report_id
                  ,p_result             => l_result
                  ,p_user_id            => l_user_id
                  ,p_process_id         => l_process_id
                  ,p_report_name        => l_report_name
                  ,p_request_id         => l_request_id
                  ,p_view_sql           => l_str
                  ,p_error_msg          => l_error_msg
                  ,p_param1             => trim(p_param1)
                  ,p_param2             => trim(p_param2)
                  ,p_param3             => trim(p_param3)
                  ,p_param4             => trim(p_param4)
                  ,p_param5             => trim(p_param5)
                  ,p_param6             => trim(p_param6)
                  ,p_param7             => trim(p_param7)
                  ,p_param8             => trim(p_param8)
                  ,p_param9             => trim(p_param9)
                  ,p_param10            => trim(p_param10)
                  ,p_param11            => trim(p_param11)
                  ,p_param12            => trim(p_param12)
                  ,p_param13            => trim(p_param13)
                  ,p_param14            => trim(p_param14)
                  ,p_param15            => trim(p_param15)
                  ,p_param16            => trim(p_param16)
                  ,p_param17            => trim(p_param17)
                  ,p_param18            => trim(p_param18)
                  ,p_param19            => trim(p_param19)
                  ,p_param20            => trim(p_param20)
                  ,p_param21            => trim(p_param21)
                  ,p_param22            => trim(p_param22)
                  ,p_param23            => trim(p_param23)
                  ,p_param24            => trim(p_param24)
                  ,p_param25            => trim(p_param25)
                  ,p_param26            => trim(p_param26)
                  ,p_param27            => trim(p_param27)
                  ,p_param28            => trim(p_param28)
                  ,p_param29            => trim(p_param29)
                  ,p_param30            => trim(p_param30)
          ,p_col_process_id     => p_col_process_id) ;

    RETURN l_str ;

 END get_view_sql;

PROCEDURE delete_report(p_report_id IN NUMBER, p_status OUT VARCHAR2)
IS
    l_view_id NUMBER;
    l_repcount NUMBER;
    l_status   VARCHAR2(100);
    l_function_id NUMBER;
    l_menu_id   NUMBER;
    l_ENTRY_SEQUENCE     NUMBER :=0;

    CURSOR get_processes (p_report_id IN NUMBER)
    IS
    SELECT process_id FROM xxeis.eis_rs_processes
    WHERE report_id = p_report_id;
BEGIN

    DELETE FROM xxeis.eis_rs_errors
     WHERE report_id = p_report_id ;

    DELETE FROM xxeis.eis_rs_formal_exceptions
     WHERE report_id = p_report_id ;

    DELETE FROM xxeis.eis_rs_report_conditions
     WHERE report_id = p_report_id ;

    DELETE FROM xxeis.eis_rs_report_parameters
     WHERE report_id = p_report_id ;

    DELETE FROM xxeis.eis_rs_report_publish_groups
     WHERE report_id = p_report_id;

    DELETE FROM xxeis.eis_rs_sorts
     WHERE report_id = p_report_id;

    DELETE FROM xxeis.eis_rs_totals
     WHERE report_id = p_report_id;

    DELETE FROM xxeis.eis_rs_report_triggers
     WHERE report_id = p_report_id;

    DELETE FROM xxeis.eis_rs_report_security
     WHERE report_id = p_report_id;

    DELETE FROM xxeis.eis_rs_report_columns
     WHERE report_id = p_report_id;

   DELETE FROM  xxeis.eis_rs_report_templates
     WHERE report_id = p_report_id;

   DELETE FROM  xxeis.eis_rs_report_favorites
     WHERE report_id = p_report_id;

   --bug# 755
   BEGIN

     SELECT function_id,menu_id  INTO l_function_id,l_menu_id FROM xxeis.eis_rs_report_portals
     WHERE  report_id = p_report_id;

     IF l_function_id IS NOT NULL THEN
        fnd_form_functions_pkg.delete_row
                          (x_function_id      => l_function_id);

        SELECT entry_sequence
            INTO   l_entry_sequence
            FROM   fnd_menu_entries
            WHERE  menu_id= l_menu_id
            AND  function_id = l_function_id;

        fnd_menu_entries_pkg.delete_row(x_menu_id => l_menu_id,
                                   x_entry_sequence =>l_entry_sequence );
        COMMIT;
     END IF;
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

   DELETE FROM  xxeis.eis_rs_report_portals
     WHERE report_id = p_report_id;

    DELETE FROM xxeis.eis_rs_report_distribution
     WHERE report_id = p_report_id ;

    DELETE FROM xxeis.eis_rs_report_pivot_details
     WHERE report_id = p_report_id ;

    DELETE FROM xxeis.eis_rs_summary_calculations
     WHERE report_id = p_report_id ;

    DELETE FROM xxeis.eis_rs_report_pivots
     WHERE report_id = p_report_id ;

    DELETE FROM xxeis.eis_rs_reports
     WHERE report_id = p_report_id ;

    FOR get_processes_rec IN get_processes(p_report_id)
    LOOP
    xxeis.eis_rs_utility.delete_a_process(get_processes_rec.process_id);
    END LOOP;

    COMMIT ;

    p_status := 'Successfully deleted the report, '||p_report_id||'.  Also Committed the deletion' ;

EXCEPTION WHEN OTHERS THEN
    p_status := SUBSTR (SQLERRM, 1, 75) ;
END delete_report;

 PROCEDURE get_clobs( p_process_id IN NUMBER
                     ,p_result OUT cursor_type )
  IS

  l_stmt    VARCHAR2(1000) ;

  BEGIN

    ---Changed by Nagraj 1.4.6
    l_stmt :='SELECT clob_file, file_name
                FROM xxeis.eis_rs_clobs
               WHERE file_name NOT LIKE ''s%''
               AND process_id = '||p_process_id||' ORDER BY request_id,clob_id' ;

    OPEN p_result FOR l_stmt ;


  END ;

  ---Added by Nagraj 11-Apr-2005
  ---Get the pivot file name
    PROCEDURE get_filenames(p_process_id       IN VARCHAR2,
                            p_summary_flag     IN VARCHAR2,
                            p_filenames        OUT VARCHAR2,
                            p_lastfile         OUT VARCHAR2)
    IS
       l_stmt VARCHAR2(2000);
       l_filenames VARCHAR2(4000);
       l_lastfile VARCHAR2(1000);

     CURSOR cur1 IS
      SELECT distinct file_name file_name
      FROM xxeis.eis_rs_clobs
      WHERE process_id=p_process_id
      AND file_name like 's%'
      ORDER BY to_number(replace(substr(file_name, instr(file_name, '_')+1), '.csv'));

     CURSOR cur2 IS
      SELECT distinct file_name file_name
      FROM xxeis.eis_rs_clobs
      WHERE process_id=p_process_id
      AND file_name not like 's%'
      ORDER BY to_number(replace(substr(file_name, instr(file_name, '_')+1), '.csv'));

    BEGIN

         l_filenames:=NULL;

         IF UPPER(p_summary_flag)='Y' THEN ---Summary Report

               FOR rec1 IN cur1 LOOP
                    l_lastfile := rec1.file_name ;
                    IF l_filenames IS NULL THEN
                        l_filenames := l_lastfile;
                    ELSE
                        l_filenames := l_filenames || ':'|| l_lastfile;
                    END IF;

               END LOOP ;
         ELSE---Details Report
               FOR rec2 IN cur2 LOOP
                    l_lastfile := rec2.file_name ;

                    IF l_filenames IS NULL THEN
                        l_filenames := l_lastfile ;
                    ELSE
                        l_filenames := l_filenames || ':'|| l_lastfile;
                    END IF;

               END LOOP ;
         END IF;

        IF l_filenames IS NULL THEN
            p_filenames := '';
            p_lastfile :='';
        ELSE
            p_filenames := l_filenames;
            p_lastfile := l_lastfile ;
        END IF;

    EXCEPTION WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.log, SUBSTR(SQLERRM,1,75)||' Operation failed.')  ;
    END;

    FUNCTION as_of_date
    RETURN DATE
    IS
    BEGIN
    RETURN trunc(g_as_of_date);
    END;

     /* Formatted on 2005/08/16 15:58 (Formatter Plus v4.8.0) */
     --modified to fix the problem PLS-00103: Encountered the symbol ","  16-feb-2006
    FUNCTION get_output_columns (
       p_report_id   NUMBER
    )
       RETURN VARCHAR2
    AS
       l_map_columns                 VARCHAR2 (4000) := '';
       --l_columns                     VARCHAR2 (4000) := '';
       l_number                      NUMBER := 1;
       l_varchar                     NUMBER := 1;
       l_date                        NUMBER := 1;
       CURSOR get_c_inv(p_report_id IN VARCHAR2)  IS
        SELECT   errc.column_name
            , NVL (errc.display_name, column_name) display_name
            , errc.calculation_column
            , ERRC.DERIVED_FLAG
            , DRILL_DOWN_URL
            , NVL(errc.data_type,xxeis.eis_rs_process_reports.get_process_column_data_type(g_report_process_id, errc.view_id, errc.column_name)) data_type --bug# 6184
         FROM xxeis.eis_rs_process_rpt_columns errc--6.04
        WHERE errc.report_id = p_report_id
        and errc.process_id = g_report_process_id --6.04
          AND NVL (errc.enabled_flag, 'Y') = 'Y'
         ORDER BY display_order;
    BEGIN
       FOR c_inv IN get_c_inv(p_report_id) LOOP

--Updated by Praveen to fix Bug# 4532
          IF c_inv.data_type = 'VARCHAR2' THEN
             IF l_varchar< 201 THEN
            l_map_columns              :=    l_map_columns
                                          || ','
                                          || 'VARCHAR2_COL'
                                         || l_varchar;
            ELSE
              l_map_columns              :=    l_map_columns|| ',EXTRA_COL';
            END IF;
             L_Varchar                  := L_Varchar + 1;
          ELSIF c_inv.data_type = 'NUMBER' THEN
         l_map_columns              :=    l_map_columns
                                           || ','
                                           || 'NUMBER_COL'
                                           || l_number;
             l_number                   := l_number + 1;
          ELSIF c_inv.data_type = 'DATE' THEN
             l_map_columns              :=    l_map_columns
                                           || ','
                                           || 'DATE_COL'
                                           || l_date;
             l_date                     := l_date + 1;
          END IF;
       END LOOP;

       IF l_map_columns IS NOT NULL THEN
            l_map_columns := substr(l_map_columns,2);
       END IF;

       --Added KP -22-Dec-2005
      /* IF g_gl_segment_check = 'Y' THEN
--Updated by Praveen.P to fix Bug# 4532
            FOR segment_ctr IN 1..g_gl_segment_count LOOP
         IF l_varchar< 201 THEN
               l_map_columns              :=    l_map_columns
                                              || ','
                                             || 'VARCHAR2_COL'
                                             || l_varchar;
           ELSE
                l_map_columns              :=    l_map_columns|| ',EXTRA_COL';
         END IF;
                 l_varchar                  := l_varchar + 1;
            END LOOP ;


       END IF;*/

       RETURN l_map_columns;
    END get_output_columns;

--Added by Gattu
    FUNCTION get_process_output_columns (
       p_report_id   NUMBER
      ,p_process_id  NUMBER
    )
       RETURN VARCHAR2
    as
      -- l_map_columns                 VARCHAR2 (4000) := '';
       l_map_columns                 LONG;
       --l_columns                     VARCHAR2 (4000) := '';
       l_number                      NUMBER := 1;
       l_varchar                     NUMBER := 1;
       l_date                        NUMBER := 1;
       CURSOR get_c_inv(p_report_id IN VARCHAR2)  IS
       SELECT   errc.column_name
                            , NVL (errc.display_name, column_name) display_name
                            , errc.calculation_column
                            , ERRC.DERIVED_FLAG
                            , DRILL_DOWN_URL
                            , xxeis.eis_rs_process_reports.get_process_column_data_type(p_process_id,errc.view_id, errc.column_name) data_type --bug# 6184
                         FROM xxeis.eis_rs_process_rpt_columns errc
                        WHERE errc.report_id = p_report_id
              AND errc.process_id =p_process_id
                          AND NVL (errc.enabled_flag, 'Y') = 'Y'
                     ORDER BY display_order;
    BEGIN
       FOR c_inv IN get_c_inv(p_report_id) LOOP

--Updated by Praveen.P to fix Bug# 4532
          if c_inv.data_type = 'VARCHAR2'  then
           IF l_varchar< 201 THEN
             l_map_columns              :=    l_map_columns
                                           || ','
                                           || 'VARCHAR2_COL'
                                           || l_varchar;
            ELSE
              l_map_columns              :=    l_map_columns|| ',EXTRA_COL';
            END IF;
             l_varchar                  := l_varchar + 1;
          ELSIF c_inv.data_type = 'NUMBER' THEN
             l_map_columns              :=    l_map_columns
                                           || ','
                                           || 'NUMBER_COL'
                                           || l_number;
             l_number                   := l_number + 1;
          ELSIF c_inv.data_type = 'DATE' THEN
             l_map_columns              :=    l_map_columns
                                           || ','
                                           || 'DATE_COL'
                                           || l_date;
             l_date                     := l_date + 1;
          END IF;
       END LOOP;

       IF l_map_columns IS NOT NULL THEN
            l_map_columns := substr(l_map_columns,2);
       END IF;

       --Added KP -22-Dec-2005
     /*  IF g_gl_segment_check = 'Y' THEN
--Updated by Praveen.P to fix Bug# 4532
            for segment_ctr in 1..g_gl_segment_count loop
        IF l_varchar< 201 THEN
                l_map_columns              :=    l_map_columns
                                               || ','
                                               || 'VARCHAR2_COL'
                                               || l_varchar;
              ELSE
                  l_map_columns              :=    l_map_columns|| ',EXTRA_COL';
         END IF;
                 l_varchar                  := l_varchar + 1;

            END LOOP ;


       END IF;*/

       RETURN l_map_columns;
    END get_process_output_columns;

--Added by Rakesh
-- to add GL segment in reprot output dynamically
PROCEDURE get_segment_prompt ( p_app_col_name     OUT VARCHAR2
                              ,p_form_left_prompt OUT VARCHAR2
                              ,p_report_id        IN NUMBER
                              ,p_process_id       IN NUMBER
                              ,p_col_count        OUT NUMBER
                              ,p_segment_data_type OUT VARCHAR2 ) -- Added KP 22-Dec-2005
IS
    l_app_col_name      VARCHAR2(250) := NULL;
    l_form_left_prompt  VARCHAR2(250) := NULL;
    l_col_delim         VARCHAR2(2):= '~' ;
    l_seg_count            NUMBER;
    l_temp_delimiter    VARCHAR2(2);
    l_temp_delimiter1   VARCHAR2(2);
    l_delimiter         VARCHAR2(2) := NULL ;
    l_session_id        NUMBER;

    CURSOR get_seg_prompt IS
        SELECT fifv.application_column_name, fifv.FORM_LEFT_PROMPT
          FROM gl_sets_of_books_v sob,
               fnd_id_flex_segments_tl fifv
         WHERE sob.chart_of_accounts_id = fifv.id_flex_num
           AND sob.set_of_books_id = fnd_profile.VALUE ('GL_SET_OF_BKS_ID')
           AND fifv.application_id = 101
           AND fifv.id_flex_code = 'GL#'
           AND fifv.language = userenv('LANG');

BEGIN
    BEGIN
        SELECT session_id
          INTO l_session_id
          FROM xxeis.eis_rs_processes
          WHERE process_id = p_process_id;
     EXCEPTION WHEN NO_DATA_FOUND THEN
         l_session_id := 0;
    END;
    IF l_session_id IS NOT NULL AND l_session_id <> 0 THEN
        xxeis.eis_rs_utility.eis_session_initialize(l_session_id);
    END IF;
    FOR get_seg_prompt_rec IN get_seg_prompt
    LOOP
        BEGIN
            SELECT COUNT(*)
              INTO l_seg_count
              FROM xxeis.eis_rs_views erv, xxeis.eis_rs_view_columns ervc, xxeis.eis_rs_reports err
             WHERE erv.view_id = ervc.view_id
               AND erv.view_id = err.view_id
               AND err.report_id = p_report_id
               AND UPPER(ervc.column_name) = UPPER('GL#'||get_seg_prompt_rec.application_column_name);
           EXCEPTION WHEN NO_DATA_FOUND THEN
               l_seg_count := 0;
        END;
        -- added GL# to show in report output column name as GL#col_name
        IF l_seg_count = 1 THEN
            l_app_col_name := l_app_col_name||l_temp_delimiter
                                            ||l_temp_delimiter1
                                            ||l_delimiter
                                            ||l_temp_delimiter1
                                            ||l_temp_delimiter
                                            ||'GL#' --RL added GL# to show in report output dated 01/12/2005
                                            || get_seg_prompt_rec.application_column_name;

            l_form_left_prompt := l_form_left_prompt||l_delimiter
                                ||'GL#'
                                                    ||get_seg_prompt_rec.FORM_LEFT_PROMPT;
               l_delimiter := '~' ;
            l_temp_delimiter := '||' ;
            l_temp_delimiter1 := '''' ;
            p_col_count := NVL(p_col_count, 0) + 1 ; -- Added KP 22-Dec-2005
            p_segment_data_type := p_segment_data_type || 'VARCHAR2,' ;
        END IF;
    END LOOP;
    p_app_col_name := l_app_col_name;
    p_segment_data_type := RTRIM(p_segment_data_type, ',');
    p_form_left_prompt := l_form_left_prompt;

END get_segment_prompt;

-- =======================================================================
-- ~ parse_output_data
-- =======================================================================
FUNCTION parse_output_data(p_output_data IN VARCHAR2)
    RETURN VARCHAR2
IS
    l_output_data     VARCHAR2(4000) := p_output_data||'~';
    l_output_col      VARCHAR2(4000);
    l_output_date      VARCHAR2(4000) := NULL;
    l_output_var      VARCHAR2(4000) := NULL;
    l_output_num      VARCHAR2(4000) := NULL;
    l_output_temp     VARCHAR2(1000) := NULL;
    l_hyperlink     VARCHAR2(4000);
    l_datatype_temp varchar2(100);
    l_datatype         VARCHAR2(10000) := g_dynamic_col_list||',';
    num_list        NUMBER := 0;
    var_list        NUMBER := 0;
    date_list        NUMBER := 0;
BEGIN

    WHILE l_output_data IS NOT NULL OR l_output_data <> '~' LOOP
        l_output_temp := SUBSTR(l_output_data,0,INSTR(l_output_data,'~',1,1)-1);
        l_datatype_temp := SUBSTR(l_datatype,0,INSTR(l_datatype,',',1,1)-1);
        IF INSTR(l_output_temp,'=HYPERLINK') > 0 THEN
            l_hyperlink := SUBSTR(l_output_temp, 1, INSTR(l_output_temp,'"^"'));
            l_hyperlink := RTRIM(REPLACE(l_hyperlink, '=HYPERLINK("'),'"');
            l_output_temp := SUBSTR(l_output_temp,INSTR(l_output_temp,'"^"')+3);
            l_output_temp := REPLACE(REPLACE(l_output_temp,')'),'"');
            l_output_temp := l_output_temp || '^#'||l_hyperlink ;

        END IF;

        IF l_datatype_temp LIKE 'VARCHAR2_COL%' THEN
            l_output_temp := ''''||replace(l_output_temp, '''', '''''')||''''; -- Kp Change to care of single quotes - 11/28/2005
            l_output_var := l_output_var ||','||nvl(trim(l_output_temp),'null');-- KP Change to takecare of null - 11/28/2005
            var_list := var_list + 1;
        ELSIF l_datatype_temp LIKE 'DATE_COL%' THEN
            l_output_temp := ''''||l_output_temp||'''';
            l_output_date := l_output_date ||','||nvl(trim(l_output_temp),'null');
            date_list := date_list + 1;
        ELSIF l_datatype_temp LIKE 'NUMBER_COL%' THEN
            l_output_num := l_output_num ||','||nvl(trim(l_output_temp),'null');
            num_list :=num_list + 1;
        END IF;

        l_output_data := SUBSTR(l_output_data,INSTR(l_output_data,'~',1,1)+1);
        l_datatype := SUBSTR(l_datatype,INSTR(l_datatype,',',1,1)+1);
    END LOOP;

    l_output_var := LTRIM(l_output_var,',');
    l_output_num := LTRIM(l_output_num,',');
    l_output_date := LTRIM(l_output_date,',');

--bug #2003
    WHILE num_list < 100 LOOP
        l_output_num := l_output_num||',null';
        num_list := num_list + 1;
    END LOOP;
    WHILE date_list < 100 LOOP
        l_output_date := l_output_date||',null';
        date_list := date_list + 1;
    end loop;
    WHILE var_list < 200 LOOP
        l_output_var := l_output_var||',null';
        var_list := var_list + 1;
    END LOOP;


    l_output_col := l_output_num||','||l_output_date||','||l_output_var;
    l_output_col := replace(ltrim(l_output_col, ','), ',,',',') ;-- Added KP 12/19/2005
RETURN l_output_col;
END parse_output_data;

-- =======================================================================
-- ~ Send_Mail_To_Users
-- =======================================================================
PROCEDURE Send_Mail_To_Users(
    p_to            in varchar2,
    p_from          in varchar2,
    p_subject       in varchar2,
    p_text          in varchar2 default null,
    p_html          in varchar2 default null,
    p_smtp_hostname in varchar2,
    p_smtp_portnum  in varchar2,
    p_mail_status   out varchar2)
is
    l_boundary      varchar2(255) default 'a1b2c3d4e3f2g1';
    l_connection    utl_smtp.connection;
    l_body_html     clob := empty_clob;  --This LOB will be the email message
    l_offset        number;
    l_ammount       number;
    l_temp          varchar2(32767) default null;

begin
    l_connection := utl_smtp.open_connection( p_smtp_hostname, p_smtp_portnum );
    utl_smtp.helo( l_connection, p_smtp_hostname );
    utl_smtp.mail( l_connection, p_from );
    utl_smtp.rcpt( l_connection, p_to );

    l_temp := l_temp || 'MIME-Version: 1.0' ||  chr(13) || chr(10);
    l_temp := l_temp || 'To: ' || p_to || chr(13) || chr(10);
    l_temp := l_temp || 'From: ' || p_from || chr(13) || chr(10);
    l_temp := l_temp || 'Subject: ' || p_subject || chr(13) || chr(10);
    l_temp := l_temp || 'Reply-To: ' || p_from ||  chr(13) || chr(10);
    l_temp := l_temp || 'Content-Type: multipart/alternative; boundary=' ||
                         chr(34) || l_boundary ||  chr(34) || chr(13) ||
                         chr(10)||utl_tcp.CRLF;

    ----------------------------------------------------
    -- Write the headers
    dbms_lob.createtemporary( l_body_html, false, 10 );
    dbms_lob.write(l_body_html,length(l_temp),1,l_temp);


    ----------------------------------------------------
    -- Write the text boundary
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    l_temp   := '--' || l_boundary || chr(13)||chr(10);
    l_temp   := l_temp || 'content-type: text/plain; charset=us-ascii' ||
                  chr(13) || chr(10) || chr(13) || chr(10);
    dbms_lob.write(l_body_html,length(l_temp),l_offset,l_temp);

    ----------------------------------------------------
    -- Write the plain text portion of the email
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    dbms_lob.write(l_body_html,length(p_text),l_offset,p_text);

    ----------------------------------------------------
    -- Write the HTML boundary
    l_temp   := chr(13)||chr(10)||chr(13)||chr(10)||'--' || l_boundary ||
                    chr(13) || chr(10);
    l_temp   := l_temp || 'content-type: text/html;' ||
                   chr(13) || chr(10) || chr(13) || chr(10);
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    dbms_lob.write(l_body_html,length(l_temp),l_offset,l_temp);

    ----------------------------------------------------
    -- Write the HTML portion of the message
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    dbms_lob.write(l_body_html,length(p_html),l_offset,p_html);

    ----------------------------------------------------
    -- Write the final html boundary
    l_temp   := chr(13) || chr(10) || '--' ||  l_boundary || '--' || chr(13);
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    dbms_lob.write(l_body_html,length(l_temp),l_offset,l_temp);


    ----------------------------------------------------
    -- Send the email in 1900 byte chunks to UTL_SMTP
    l_offset  := 1;
    l_ammount := 1900;
    utl_smtp.open_data(l_connection);
    while l_offset < dbms_lob.getlength(l_body_html) loop
        utl_smtp.write_data(l_connection,
                            dbms_lob.substr(l_body_html,l_ammount,l_offset));
        l_offset  := l_offset + l_ammount ;
        l_ammount := least(1900,dbms_lob.getlength(l_body_html) - l_ammount);
    end loop;
    utl_smtp.close_data(l_connection);
    utl_smtp.quit( l_connection );
    dbms_lob.freetemporary(l_body_html);
    p_mail_status   := null;
exception when others then
    p_mail_status := SUBSTR(SQLERRM, 1, 200);
end Send_Mail_To_Users;

/*========================================================================================================*
**
**        Function       : copy_report_parameters
**        Description   :
                        This function creates the parameters,conditions,columns,sorts for each report run in the
                        following tables. It also expands the GL segments based on the session's set of books.
                        EIS_RS_PROCESS_RPT_PARAMETERS
                        EIS_RS_PROCESS_RPT_CONDITIONS
                        EIS_RS_PROCESS_RPT_COLUMNS
                        EIS_RS_PROCESS_RPT_SORTS
                        This procedure invokes EIS_FIN_GL_SEG_EXPAND_PKG.copy_process_parameters to perform
                        the above tasks of Copying.
**        Scope          : Public
**        Date Created  : 17-Feb-08
**        Created By      : Ashwin ( Added for GL Segment Expansion Enhancements )
**
**========================================================================================================*/

PROCEDURE copy_report_parameters(
    p_report_id           IN NUMBER ,
    p_process_id          OUT NUMBER ,
    p_user_name           IN VARCHAR2 DEFAULT NULL,
    p_resp_name           IN VARCHAR2 DEFAULT NULL,
    p_existing_process_id IN NUMBER DEFAULT NULL )
AS
  l_err_msg        VARCHAR(20000);
  l_copy_proces_id NUMBER;
BEGIN
  IF ( p_existing_process_id IS NULL ) THEN
    -- i.e. this is during a report initial submit
    xxeis.eis_rs_pre_process_reports.copy_report_parameters( p_report_id,
                                                             l_copy_proces_id,
                                                             l_err_msg );
    IF( l_err_msg   = 'Success' ) THEN
      p_process_id := l_copy_proces_id;
    ELSE
      RAISE_APPLICATION_ERROR(-20002,'Copy Process Errored ! Please contact administrator ');
    END IF ;
  ELSE
    -- when report is submitted from the REquests Page , then the p_existing_process_id would be passed in
    xxeis.eis_rs_pre_process_reports.copy_existing_process_params( p_report_id,
                                                                   l_copy_proces_id,
                                                                   p_existing_process_id,
                                                                   l_err_msg );
    IF ( l_copy_proces_id IS NOT NULL ) THEN
      p_process_id        := l_copy_proces_id;
    END IF;
  END IF;
  COMMIT;
END copy_report_parameters ;


FUNCTION get_summary_calc_display_cols( p_process_id IN VARCHAR2,
                                        p_pivot_id   IN NUMBER)
  RETURN VARCHAR2
IS
  CURSOR get_summary_calc_c
  IS
    SELECT pivot_display_name,
      summary_calculation
    FROM xxeis.eis_rs_process_rpt_summ_calcs
    WHERE process_id = p_process_id
    AND pivot_id     = p_pivot_id
ORDER BY display_order;

  CURSOR get_display_name(c_process_id IN NUMBER, c_pivot_id IN NUMBER, c_column_name IN VARCHAR2)
  IS
    SELECT errc.display_name
    FROM xxeis.eis_rs_views erv,
      xxeis.eis_rs_view_columns ervc,
      xxeis.eis_rs_process_rpt_columns errc,
      xxeis.eis_rs_process_rpt_pivots errp,
      xxeis.eis_rs_process_rpt_pivot_dtls errpd
    WHERE erv.view_id           = ervc.view_id (+)
    AND errc.view_id            = erv.view_id(+)
    AND errc.process_id         = errp.process_id
    AND errp.process_id         = errpd.process_id
    AND errc.column_id          = errpd.column_id
    AND errp.pivot_id           = errpd.pivot_id
    AND errc.column_name        = ervc.column_name
    AND errc.process_id         = c_process_id
    AND errp.pivot_id           = c_pivot_id
    AND (errc.column_name       = c_column_name
    OR errc.display_name        = c_column_name
    OR errpd.pivot_display_name = c_column_name
    OR upper(erv.view_alias ||'.'||errc.column_name) = c_column_name );

  CURSOR get_calc_col_disp_name (c_report_id IN NUMBER, c_column_name IN VARCHAR2)
  IS
    SELECT errc.display_name
    FROM xxeis.eis_rs_report_columns errc
    WHERE errc.report_id  = c_report_id
    AND (errc.column_name = c_column_name
    OR errc.display_name  = c_column_name);
  l_tot_fields      NUMBER    := 0;
  l_start_pos       NUMBER    := 0;
  l_end_pos         NUMBER    := 0;
  l_act_st_pos      NUMBER    := 0;
  l_act_ed_pos      NUMBER    := 0;
  l_old_column_name VARCHAR2(80);
  l_column_name     VARCHAR2(80);
  l_input_field     VARCHAR2(1000);
  l_output_field    VARCHAR2(1000);
  l_final_return    VARCHAR2(3000);
BEGIN

  FOR get_summary_calc_rec IN get_summary_calc_c
  LOOP
    IF get_summary_calc_rec.pivot_display_name IS NOT NULL AND get_summary_calc_rec.summary_calculation IS NOT NULL THEN
      l_input_field := get_summary_calc_rec.summary_calculation;
      dbms_output.put_line('l_input_field is '||l_input_field);
      l_tot_fields := LENGTH(l_input_field) - LENGTH(REPLACE(l_input_field,'<'));

      FOR i IN 1..l_tot_fields
      LOOP
        l_start_pos       := instr(l_input_field,'<',1,i);
        l_end_pos         := instr(l_input_field,'>',1,i);
        l_act_st_pos      := l_start_pos + 1;
        l_act_ed_pos      := l_end_pos   - l_act_st_pos;
        l_old_column_name := SUBSTR(l_input_field,l_act_st_pos,l_act_ed_pos);

    OPEN get_display_name(p_process_id, p_pivot_id, l_old_column_name);
        FETCH get_display_name INTO l_column_name;
        CLOSE get_display_name;

    /*IF l_column_name IS NULL THEN
          OPEN get_calc_col_disp_name (get_summary_calc_rec.report_id,l_old_column_name);
          FETCH get_calc_col_disp_name INTO l_column_name;
          CLOSE get_calc_col_disp_name;
        END IF;*/

    dbms_output.put_line('l_old_column_name is '||i||':'||l_old_column_name);
        dbms_output.put_line('l_column_name is '||i||':'||l_column_name);

    IF i              = 1 THEN
          l_output_field := REPLACE(l_input_field,'<'||l_old_column_name||'>','<'||l_column_name||'>');
        ELSE
          l_output_field := REPLACE(l_output_field,'<'||l_old_column_name||'>','<'||l_column_name||'>');
        END IF;
        dbms_output.put_line('l_output_field is '||i||':'||l_output_field);

      END LOOP;
      l_final_return := l_final_return||','||get_summary_calc_rec.pivot_display_name||'('||l_output_field||')';
    END IF;
  END LOOP;

  IF l_final_return IS NOT NULL THEN
    l_final_return  := SUBSTR(l_final_return,2);
  END IF;

  dbms_output.put_line('l_final_return is :'||l_final_return);
  RETURN l_final_return;
EXCEPTION WHEN OTHERS THEN
  RETURN NULL;
END get_summary_calc_display_cols;

/*FUNCTION get_report_column_data_type(p_report_id   IN NUMBER,
                                      p_view_id     IN NUMBER,
                                      p_column_name IN VARCHAR2
                                    ) RETURN VARCHAR2 IS
l_data_type VARCHAR2(50);
BEGIN
 SELECT nvl ( errc.data_type , ervc.data_type)
        INTO l_data_type
      FROM xxeis.eis_rs_report_columns errc,
        xxeis.eis_rs_reports err,
        xxeis.eis_rs_view_columns ervc
      WHERE  errc.column_name           = p_column_name
        AND errc.column_name            = ervc.column_name
        AND err.report_id               = errc.report_id
        AND errc.view_id                = ervc.view_id
        AND errc.view_id                = p_view_id
        AND errc.report_id              = p_report_id
        AND NVL(errc.enabled_flag, 'Y') = 'Y';

    RETURN  NVL(l_data_type,'VARCHAR2') ;

EXCEPTION WHEN OTHERS THEN
   RETURN 'VARCHAR2';
END get_report_column_data_type;

FUNCTION get_html_query( p_sql_stmt LONG )
  RETURN LONG
IS
  l_temp_stmt LONG;
  l_part1 LONG;
  l_part2 LONG;
  l_part3 LONG;
  l_position NUMBER;
BEGIN
  l_temp_stmt := p_sql_stmt;

  WHILE instr(l_temp_stmt,'xxeis.eis_rs_utility.get_number_formatted_string(')>0
  LOOP
    l_position  := INSTR(l_temp_stmt,'xxeis.eis_rs_utility.get_number_formatted_string(');
    l_part1     := SUBSTR(l_temp_stmt,1,l_position      -1);
    l_part2     := SUBSTR(SUBSTR(l_temp_stmt, l_position+49),1,INSTR(SUBSTR(l_temp_stmt, l_position+49),',')-1);
    l_part3     := SUBSTR(SUBSTR(l_temp_stmt, l_position+49),INSTR(SUBSTR(l_temp_stmt, l_position+49),')')+1);
    l_temp_stmt := l_part1||l_part2||l_part3;
  END LOOP;
  RETURN l_temp_stmt;
END get_html_query;*/

END eis_rs_process_reports;
/
