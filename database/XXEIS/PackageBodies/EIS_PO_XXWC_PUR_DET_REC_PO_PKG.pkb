CREATE OR REPLACE PACKAGE BODY    XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG
IS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Name         		:: XXEIS.EIS_PO_XXWC_PUR_DET_REC_PO_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package programs will call in XXEIS_WC_PO_DETAILS_V view to populate data
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      18-may-2016       Initial Build TMS#20160414-00110
--//============================================================================
 FUNCTION GET_PO_TYPE (P_TYPE_LOOKUP_CODE in VARCHAR2,P_org_id in number) RETURN VARCHAR2
	IS
--//============================================================================
--//
--// Object Name         :: get_po_type  
--//
--// Object Usage 		 :: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================ 
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	G_PO_TYPE := NULL;
	l_sql :=' SELECT PDT.TYPE_NAME
	  FROM po_document_types pdt
	  WHERE pdt.document_type_code  = ''PO''
	  AND PDT.DOCUMENT_SUBTYPE      = :1
	  AND PDT.ORG_ID                = :2';
		begin
			l_hash_index:=P_TYPE_LOOKUP_CODE||'-'||P_org_id;
			G_PO_TYPE := G_PO_TYPE_VLDN_TBL(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO G_PO_TYPE USING P_TYPE_LOOKUP_CODE,P_org_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		G_PO_TYPE :=null;
		WHEN OTHERS THEN
		G_PO_TYPE :=null;
		END;      
						   l_hash_index:=P_TYPE_LOOKUP_CODE||'-'||P_org_id;
						   G_PO_TYPE_VLDN_TBL(L_HASH_INDEX) := G_PO_TYPE;
		end;
		RETURN  G_PO_TYPE;
		EXCEPTION WHEN OTHERS THEN
		  G_PO_TYPE:=null;
		  RETURN  G_PO_TYPE;

	END GET_PO_TYPE;

FUNCTION GET_authorization_status (P_LOOKUP_CODE IN VARCHAR2) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: get_authorization_status  
--//
--// Object Usage 		 :: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	G_authorization_status := NULL;
	L_SQL :='SELECT plc.DISPLAYED_FIELD
		FROM PO_LOOKUP_CODES PLC
		WHERE PLC.LOOKUP_CODE = :1
		AND plc.lookup_type in (''PO APPROVAL'', ''DOCUMENT STATE'')'; 
		begin
			L_HASH_INDEX:=P_LOOKUP_CODE;
			G_authorization_status := G_author_status_VLDN_TBL(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO G_authorization_status USING P_LOOKUP_CODE;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		G_authorization_status :=null;
		WHEN OTHERS THEN
		G_authorization_status :=null;
		END;      
						   l_hash_index:=P_LOOKUP_CODE;
						   G_author_status_VLDN_TBL(L_HASH_INDEX) := G_authorization_status;
		end;
		RETURN  G_authorization_status;
		EXCEPTION WHEN OTHERS THEN
		  G_authorization_status:=null;
		  RETURN  G_authorization_status;

	END GET_authorization_status;
	
FUNCTION GET_LOOKUP_MEANING (P_LOOKUP_CODE in VARCHAR2,p_lookup_type in VARCHAR2) RETURN VARCHAR2
	IS
--//============================================================================
--//
--// Object Name         :: get_lookup_meaning  
--//
--// Object Usage 		 :: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	G_lookup_meaning := NULL;
	L_SQL :='SELECT plc.DISPLAYED_FIELD
		FROM PO_LOOKUP_CODES PLC
		WHERE PLC.LOOKUP_CODE = :1
		AND plc.lookup_type = :2'; 
		begin
			L_HASH_INDEX:=P_LOOKUP_CODE||'-'||P_LOOKUP_TYPE;
			G_lookup_meaning := G_lookup_meaning_VLDN_TBL(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO G_lookup_meaning USING P_LOOKUP_CODE,p_lookup_type;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		G_lookup_meaning :=null;
		WHEN OTHERS THEN
		G_lookup_meaning :=null;
		END;      
						   l_hash_index:=P_LOOKUP_CODE||'-'||p_lookup_type;
						   G_PO_TYPE_VLDN_TBL(L_HASH_INDEX) := G_lookup_meaning;
		end;
		RETURN  G_lookup_meaning;
		EXCEPTION WHEN OTHERS THEN
		  G_lookup_meaning:=null;
		  RETURN  G_lookup_meaning;

	end GET_LOOKUP_MEANING;


FUNCTION GET_ACCEPTANCE_DETAILS (P_po_header_id  IN NUMBER,P_REQUEST_TYPE IN VARCHAR2) RETURN VARCHAR2 IS
--//============================================================================
--//
--// Object Name         :: get_acceptance_details  
--//
--// Object Usage 		 :: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================ 
L_HASH_INDEX NUMBER;
l_sql varchar2(32000);
	BEGIN
	G_ACTION_DATE	  := NULL; 
	G_ACCEPTED_FLAG	  := NULL;
	G_ACCEPTANCE_TYPE := NULL;
	G_ACCEPTED_BY     := NULL;

	L_SQL :='select  
			PA1.ACTION_DATE ACTION_DATE,
			NVL(PA1.ACCEPTED_FLAG,''N'') ACCEPTED_FLAG,
			POC.DISPLAYED_FIELD ACCEPTANCE_TYPE,
			HR.FULL_NAME ACCEPTED_BY      
		from 
		  PO_ACCEPTANCES pa1,
		  (SELECT po_header_id,
			MAX(acceptance_id) max_acceptance_id
		  FROM po_acceptances
		  GROUP BY po_header_id
		  ) PA2,
		  PO_LOOKUP_CODES POC,
		  PO_AGENTS PA,
		  PER_all_PEOPLE_F HR 
		WHERE pa1.po_header_id = pa2.po_header_id
		and PA1.ACCEPTANCE_ID  = PA2.MAX_ACCEPTANCE_ID
		and pa1.po_header_id   = :1
		AND POC.LOOKUP_CODE(+) = PA1.ACCEPTANCE_LOOKUP_CODE
		AND POC.LOOKUP_TYPE(+) = ''ACCEPTANCE TYPE''
		and PA1.EMPLOYEE_ID  = PA.AGENT_ID(+)
		and PA.AGENT_ID      = HR.PERSON_ID(+)
		and TRUNC(sysdate) between NVL(HR.EFFECTIVE_START_DATE, TRUNC(sysdate)) and NVL(HR.EFFECTIVE_END_DATE, TRUNC(sysdate))
		AND pa1.PO_LINE_LOCATION_ID IS NULL'; 
		
		
		   BEGIN
			 L_HASH_INDEX:=P_po_header_id;
			if P_request_type='ACTION_DATE' then
				G_ACTION_DATE := G_ACTN_DATE_VLDN_TBL(L_HASH_INDEX);
			ELSif P_request_type='ACCEPTED_FLAG' then
				G_ACCEPTED_FLAG := G_ACCEP_FLAG_VLDN_TBL(L_HASH_INDEX);
			ELSif P_request_type='ACCEPTANCE_TYPE' then
				G_ACCEPTANCE_TYPE := G_ACCEP_TYPE_VLDN_TBL(L_HASH_INDEX);
			ELSif P_request_type='ACCEPTANCE_BY' then
				G_ACCEPTED_BY := G_ACCEP_BY_VLDN_TBL(L_HASH_INDEX);
			END IF;

		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO G_ACTION_DATE,G_ACCEPTED_FLAG,G_ACCEPTANCE_TYPE,G_ACCEPTED_BY USING P_po_header_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		if P_request_type='ACTION_DATE' then
		G_ACTION_DATE := null;
		elsif P_request_type='ACCEPTED_FLAG' then
		G_ACCEPTED_FLAG := null;
		elsif P_request_type='ACCEPTANCE_TYPE' then
		G_ACCEPTANCE_TYPE := null;
		elsif P_request_type='ACCEPTANCE_BY' then
		G_ACCEPTED_BY := null;
		end if;
		 end;
						   l_hash_index:=P_po_header_id;
						   G_ACTN_DATE_VLDN_TBL(L_HASH_INDEX) := G_ACTION_DATE;
						   G_ACCEP_FLAG_VLDN_TBL(L_HASH_INDEX):= G_ACCEPTED_FLAG;
						   G_ACCEP_TYPE_VLDN_TBL(L_HASH_INDEX):= G_ACCEPTANCE_TYPE;
						   G_ACCEP_BY_VLDN_TBL(L_HASH_INDEX)  := G_ACCEPTED_BY;
						   
						   
		end;
		   IF P_REQUEST_TYPE='ACTION_DATE' THEN
			return  G_ACTION_DATE ;
			ELSIF P_REQUEST_TYPE='ACCEPTED_FLAG' THEN
			return   G_ACCEPTED_FLAG ;
			ELSIF P_REQUEST_TYPE='ACCEPTANCE_TYPE' THEN
			return   G_ACCEPTANCE_TYPE ;
			ELSIF P_REQUEST_TYPE='ACCEPTANCE_BY' THEN
			return   G_ACCEPTED_BY ;
		   END IF;
		EXCEPTION WHEN OTHERS THEN
		 IF P_REQUEST_TYPE='ACTION_DATE' THEN
			G_ACTION_DATE := null;
			return  G_ACTION_DATE ;
			ELSIF P_REQUEST_TYPE='ACCEPTED_FLAG' THEN
			G_ACCEPTED_FLAG := null;
			return   G_ACCEPTED_FLAG ;
			ELSIF P_REQUEST_TYPE='ACCEPTANCE_TYPE' THEN
			G_ACCEPTANCE_TYPE := null;
			return   G_ACCEPTANCE_TYPE ;
			ELSIF P_REQUEST_TYPE='ACCEPTANCE_BY' THEN
			G_ACCEPTED_BY := null;
			return   G_ACCEPTED_BY ;
		   END IF;

	END GET_ACCEPTANCE_DETAILS;
  
FUNCTION GET_EMPLOYEE_NAME(P_PERSON_ID IN NUMBER) RETURN VARCHAR2
  	IS
--//============================================================================
--//
--// Object Name         :: get_employee_name  
--//
--// Object Usage 		 :: This Object Referred by "Purchase Order Detail By Received PO - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================ 
	L_HASH_INDEX number;
	l_sql varchar2(32000);
	BEGIN
	G_FULL_NAME := NULL;
	l_sql :='SELECT PAPF.full_name
          FROM PER_ALL_PEOPLE_F PAPF
        WHERE PAPF.PERSON_ID = :1
          AND TRUNC(sysdate) BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE';
		BEGIN
			l_hash_index:= P_PERSON_ID;
			G_FULL_NAME := g_full_name_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO G_FULL_NAME USING P_PERSON_ID;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		G_FULL_NAME :=null;
		WHEN OTHERS THEN
		G_FULL_NAME :=null;
		END;      
						   l_hash_index:= P_PERSON_ID;
						   g_full_name_vldn_tbl(L_HASH_INDEX) := G_FULL_NAME;
		end;
		RETURN  G_FULL_NAME;
		EXCEPTION WHEN OTHERS THEN
		  G_FULL_NAME:=null;
		  RETURN  G_FULL_NAME;

	END GET_EMPLOYEE_NAME;

  END EIS_PO_XXWC_PUR_DET_REC_PO_PKG;
/
