CREATE OR REPLACE package body    XXEIS.EIS_XXWC_PURCHASE_ACC_RPT_PKG AS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  --TMS#20160412-00031	by Pramod 	on 04-12-2016
--//
--// Object Usage 				:: This Object Referred by "PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_PURCHASE_ACC_RPT_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160412-00031	by Pramod 	on 04-12-2016
--//============================================================================

  procedure GET_ACC_PUR_DTLS (p_process_id        in number,
                              p_Lob_Branch 		    in varchar2,
                              P_AGREEMENT_YEAR 	  in number,
                              P_FISCAL_PERIOD 	  in varchar2,
                              P_LOB 				      in varchar2,
                              P_VENDOR 			      IN VARCHAR2
                            ) as
--//============================================================================
--//
--// Object Name         :: GET_ACC_PUR_DTLS  
--//
--// Object Usage 		 :: This Object Referred by "PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_PUR_ACC_OLD_TABLE_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160412-00031	by Pramod 	on 04-12-2016
--//============================================================================							
  L_CUST_SQL 		            varchar2(32000);
  L_INCOME_SQL  	          varchar2(32000); 
  l_selling_sql             varchar2(32000);
  L_CONDITION_STR           varchar2(32000);
  L_CONDITION_STR1          VARCHAR2(32000);
  
  L_AGREEMENT_YEAR  	      number;
	L_PERIOD_ID 			        NUMBER;
	L_FISCAL_PERIOD 		      VARCHAR2(15);
	L_MVID 				            varchar2(30);
	L_CUST_ID 			          number;
	L_VENDOR 				          varchar2(150); 
	L_LOB 				            varchar2(150) ;
	L_BU 					            varchar2(150);
	L_LOB_BRANCH 			        varchar2(240);
	L_POSTED_FIN_LOCATION     varchar2(40);
	l_FISCAL_PURCHASES  	    number;
	L_ACCRUAL_PURCHASES  	    number;
	L_REBATE 				          NUMBER;
  L_OFFER_NAME               VARCHAR2(240) ;
	L_COOP 				            number;
	L_TOTAL 				          NUMBER;
  
  L_COUNT                   number;
  L_LOB_BRANCH_PARAM        varchar2(32000);
  L_AGREEMENT_YEAR_PARAM    varchar2(32000);
  L_FISCAL_PERIOD_PARAM     varchar2(32000);
  L_LOB_PARAM               varchar2(32000);
  L_VENDOR_PARAM            VARCHAR2(32000);
  L_PARTY_NAME VARCHAR2(240);
  L_PARTY_ID NUMBER;
  l_process_id number;



  type CUSTOMER_REC is RECORD
  (   customer_id number,
      party_name varchar2(240)
  );

  type PURCHASE_REC is RECORD
  ( process_id            number,
    AGREEMENT_YEAR  	  number,
    PERIOD_ID 			  NUMBER,
    FISCAL_PERIOD 		  VARCHAR2(15),
    MVID 				  varchar2(30),
    CUST_ID 			  number,
    VENDOR 				  varchar2(150) ,
    lob 				  varchar2(150) ,
    BU 					  varchar2(150),
    LOB_BRANCH 			  varchar2(240),
    POSTED_FIN_LOCATION   VARCHAR2(40),
    OFFER_NAME            varchar2(240),
    FISCAL_PURCHASES  	  number,
    ACCRUAL_PURCHASES  	  number,
    REBATE 				  number,
    COOP 				  number,
    TOTAL 				  number
  );
    L_REF_CURSOR1                 CURSOR_TYPE4;
    l_ref_cursor2                CURSOR_TYPE4;
  type customer_rec_tab is table of customer_rec Index By Binary_Integer;
      customer_tab customer_rec_tab;
  type purchase_rec_tab is table of purchase_rec;  
      PURCHASE_TAB PURCHASE_REC_TAB  :=  PURCHASE_REC_TAB();

l_counter NUMBER;

  begin
  CUSTOMER_TAB.delete;
  PURCHASE_TAB.DELETE;
  l_counter := 1;
  
      IF P_LOB_BRANCH IS NOT NULL THEN    
       L_LOB_BRANCH_PARAM:= L_LOB_BRANCH_PARAM||' and BRANCH in  ('||xxeis.eis_rs_utility.get_param_values(p_Lob_Branch)||' )';
        else
            L_LOB_BRANCH_PARAM:=' and 1=1';
        end if;
    
        IF P_AGREEMENT_YEAR IS NOT NULL THEN  
        L_AGREEMENT_YEAR_PARAM:= L_AGREEMENT_YEAR_PARAM||' and AGREEMENT_YEAR   in   ('||xxeis.eis_rs_utility.get_param_values(P_AGREEMENT_YEAR)||' )';
        else
         L_Agreement_Year_PARAM:=' and 1=1';
      end if;

        IF P_FISCAL_PERIOD IS NOT NULL THEN 
        L_FISCAL_PERIOD_PARAM:= L_FISCAL_PERIOD_PARAM||' and FISCAL_PERIOD  in  ('||xxeis.eis_rs_utility.get_param_values(P_FISCAL_PERIOD)||' )';
        else
          L_FISCAL_PERIOD_PARAM:=' and 1=1';
        end if;

        IF P_LOB IS NOT NULL THEN   
        L_LOB_PARAM:= L_LOB_PARAM||'  and lob  in  ('||xxeis.eis_rs_utility.get_param_values(P_LOB)||' )';
        else
          L_LOB_PARAM:=' and 1=1';
        end if;
    
      IF P_VENDOR IS NOT NULL THEN  
      L_VENDOR_PARAM:= L_VENDOR_PARAM||'  and party_name  in  ('||xxeis.eis_rs_utility.get_param_values(P_VENDOR)||' )';
        else
          L_VENDOR_PARAM:=' and 1=1';
    end if;
 
  
  L_CUST_SQL :='select C.CUSTOMER_ID,
                    c.party_name
              from XXCUS.XXCUS_REBATE_CUSTOMERS C
              where C.PARTY_ATTRIBUTE1   =''HDS_MVID''
           '||L_VENDOR_PARAM||'
        ';
        
   
  
  l_selling_sql := '  SELECT /*+INDEX(td XXWC_OZF_TIME_DAY_N2)*/ SUM(SELLING_PRICE * QUANTITY)
                              FROM APPS.OZF_RESALE_LINES_ALL,
                                    ozf.ozf_time_day td
                            WHERE 1=1
                            and SOLD_FROM_CUST_ACCOUNT_ID =:1
                            AND DATE_ORDERED = TD.REPORT_DATE
                            and substr(td.month_id,0,4) =:2
                            AND :3 =LINE_ATTRIBUTE6
                            AND :4 =BILL_TO_PARTY_NAME
                            and :5 =LINE_ATTRIBUTE2
                            and td.ent_period_id =:6
                            ';

    
    OPEN l_ref_cursor1  FOR l_cust_sql;
       FETCH L_REF_CURSOR1 bulk collect into CUSTOMER_TAB;
	close L_REF_CURSOR1;

  FOR J IN 1..CUSTOMER_TAB.COUNT  LOOP
     L_INCOME_SQL:= 
      'SELECT 
        AGREEMENT_YEAR AGREEMENT_YEAR ,
				PERIOD_ID ,
				FISCAL_PERIOD ,
				MVID ,
				ofu_cust_account_id cust_id,
        :party_name,
        LOB ,
				BU ,
				BRANCH LOB_BRANCH ,
				POSTED_FIN_LOCATION,
				OFFER_NAME ,
				SUM(
				CASE
				  WHEN UTILIZATION_TYPE=''ACCRUAL''
				  THEN Purchases
				  ELSE 0
				END) PURCHASES ,
				SUM(
				CASE
				  WHEN REBATE_TYPE IN (''REBATE'')
				  THEN accrual_AMOUNT
				  ELSE 0
				END) REBATE ,
				SUM(
				CASE
				  WHEN REBATE_TYPE IN (''COOP'')
				  THEN accrual_AMOUNT
				  ELSE 0
				END) COOP ,
				SUM(accrual_AMOUNT) TOTAL_ACCRUALS
				
				FROM 
				 XXCUS.XXCUS_YTD_INCOME_B 
				  where OFU_CUST_ACCOUNT_ID=:1
		   '||L_FISCAL_PERIOD_PARAM||'
		   '||L_LOB_BRANCH_PARAM||'
           '||L_Agreement_Year_PARAM||'
           '||L_LOB_PARAM||'
    			  GROUP BY 
            AGREEMENT_YEAR  ,
            PERIOD_ID ,
            FISCAL_PERIOD , 
            MVID ,
            ofu_cust_account_id ,
            :party_name,
            LOB ,
            BU ,
            BRANCH  ,
            posted_fin_location,
            OFFER_NAME'
            
	  ;

  OPEN l_ref_cursor2  FOR l_income_sql using CUSTOMER_TAB(J).PARTY_NAME,CUSTOMER_TAB(J).CUSTOMER_ID,CUSTOMER_TAB(J).PARTY_NAME;
    LOOP

   FETCH  L_REF_CURSOR2  INTO 
          L_AGREEMENT_YEAR,
          L_PERIOD_ID,
          L_FISCAL_PERIOD,
          L_MVID,
          L_CUST_ID,
          L_VENDOR,
          L_LOB,
          L_BU,
          L_LOB_BRANCH,
          L_POSTED_FIN_LOCATION,
          L_OFFER_NAME,
          L_ACCRUAL_PURCHASES,
          L_REBATE,
          L_COOP,
          L_TOTAL 				          
          ;

          
        PURCHASE_TAB.extend;
        
        PURCHASE_TAB(L_COUNTER).PROCESS_ID          :=  P_PROCESS_ID;
        PURCHASE_TAB(L_COUNTER).AGREEMENT_YEAR      :=  L_AGREEMENT_YEAR;
        PURCHASE_TAB(L_COUNTER).PERIOD_ID           :=  L_PERIOD_ID;
        PURCHASE_TAB(L_COUNTER).FISCAL_PERIOD       :=  L_FISCAL_PERIOD;
        PURCHASE_TAB(L_COUNTER).MVID                :=  L_MVID;
        PURCHASE_TAB(L_COUNTER).CUST_ID             :=  L_CUST_ID;
        PURCHASE_TAB(L_COUNTER).VENDOR              :=  L_VENDOR;
        PURCHASE_TAB(L_COUNTER).LOB                 :=  L_LOB;
        PURCHASE_TAB(L_COUNTER).BU                  :=  L_BU;
        PURCHASE_TAB(L_COUNTER).LOB_BRANCH          :=  L_LOB_BRANCH;
        PURCHASE_TAB(L_COUNTER).POSTED_FIN_LOCATION :=  L_POSTED_FIN_LOCATION;
        PURCHASE_TAB(L_COUNTER).OFFER_NAME          :=  L_OFFER_NAME;

    begin 
    execute immediate L_SELLING_SQL
    into L_FISCAL_PURCHASES  
    using 
          L_CUST_ID,
          L_AGREEMENT_YEAR,
          L_LOB_BRANCH,
          L_LOB,
          L_BU,
          L_PERIOD_ID;
                
    EXCEPTION 
    when OTHERS then
    L_FISCAL_PURCHASES :=0;
    end;
        PURCHASE_TAB(L_COUNTER).FISCAL_PURCHASES      :=  L_FISCAL_PURCHASES;
        PURCHASE_TAB(L_COUNTER).ACCRUAL_PURCHASES     :=  L_ACCRUAL_PURCHASES;
        PURCHASE_TAB(L_COUNTER).REBATE                :=  L_REBATE;
        PURCHASE_TAB(L_COUNTER).COOP                  :=  L_COOP;
        PURCHASE_TAB(L_COUNTER).TOTAL                 :=  L_TOTAL;
       
           L_COUNTER    := L_COUNTER+1;
		   
		             EXIT when L_REF_CURSOR2%NOTFOUND;
                     

            end LOOP;
			close L_REF_CURSOR2;
 end LOOP;
 
      if L_COUNTER >= 1  then
                  FORALL J IN 1 .. PURCHASE_TAB.COUNT 
                     
                     INSERT INTO XXEIS.EIS_PUR_ACC_RPT_DATA_TAB
                          VALUES PURCHASE_TAB (J);    
                
               end if; 
COMMIT;      
EXCEPTION WHEN OTHERS THEN
Fnd_File.Put_Line(FND_FILE.log,'THE ERROR IS'||SQLCODE||SQLERRM);
END GET_ACC_PUR_DTLS;

PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160412-00031	by Pramod 	on 04-12-2016
--//============================================================================ 
  BEGIN  
  DELETE FROM XXEIS.EIS_PUR_ACC_RPT_DATA_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;    

end eis_xxwc_purchase_acc_rpt_pkg;
/
