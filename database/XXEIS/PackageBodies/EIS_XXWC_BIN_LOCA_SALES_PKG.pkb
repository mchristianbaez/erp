create or replace 
PACKAGE BODY       XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG AS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Bin Location With Sales History"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Venu  	      18-may-2016       Initial Build TMS#20160301-00067
--// 1.1        Siva  	      26-Oct-2016        TMS#20160824-00006 --Performance issue
--// 1.2        Siva  	      26-Dec-2016        TMS#20161014-00031 --Performance issue
--// 1.3     	Siva 		  15-Mar-2017        TMS#20170224-00065 
--//============================================================================

PROCEDURE SPLIT_SEGMENTS (P_STRING      IN  VARCHAR2,
                          P_SEGMENT1   OUT  VARCHAR2,
                          P_SEGMENT2   OUT  VARCHAR2,
                          P_SEGMENT3   OUT  VARCHAR2,
                          P_SEGMENT4   OUT  VARCHAR2
                          )
 is
--//============================================================================
--//
--// Object Name         :: SPLIT_SEGMENTS  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will split segments of concatenated segments
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Venu  				05-May-2016 	Initial Build   
--//============================================================================


	l_string_text varchar2(32000);	
  l_string_length number;
  I             number;
  L_STR         varchar2(10000);
  L_POS_START   number;
  l_pos_end     number;
 
   begin
    l_string_text:=P_STRING||'.';
    l_string_length := dbms_lob.getlength(l_string_text);
    I := 1;
   L_POS_START := 1;
    l_pos_end     := dbms_lob.instr(l_string_text,'.',1,i);
   LOOP
   l_str       :=dbms_lob.substr(l_string_text,(l_pos_end-l_pos_start),l_pos_start ); --TMS#20160429-00037    by siva  on 04-24-2016
   L_POS_START := L_POS_END  + 1;
   I           := I  + 1;
   L_POS_END   := DBMS_LOB.INSTR(l_string_text,'.',1,I);
                                               
    IF I=2
   THEN
   p_SEGMENT1 :=L_STR;
   END IF;
   IF I=3
   THEN
   p_SEGMENT2 :=L_STR;
   END IF;
   IF I=4
   THEN
   p_SEGMENT3 :=L_STR;
   END IF;
   IF I=5
   THEN
   p_SEGMENT4 :=L_STR;
   end if;
   
   IF l_pos_end =0 THEN
     EXIT;
   END IF;
 END LOOP;
   END SPLIT_SEGMENTS; 

/* --Commented start for version 1.1
function get_last_received_date (p_inventory_item_id in number,p_organization_id in number) return date
is
--//============================================================================
--//
--// Object Name         :: get_last_received_date  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Venu        	18-May-2016       Initial Build 
--//============================================================================
l_hash_index varchar2(100);
l_sql varchar2(32000);
begin
g_last_received_date := null;


l_sql :='select max (prt.transaction_date)
               from     po.rcv_transactions prt,
                          po.po_lines_all ppla
               where      prt.organization_id = :1
                      and prt.po_line_id      = ppla.po_line_id
                      and ppla.item_id        =  :2
                      and prt.destination_type_code = ''INVENTORY''';
    begin
        l_hash_index:=p_inventory_item_id||'-'||p_organization_id;
        g_last_received_date := g_last_received_date_vldn_tbl(l_hash_index);

    exception
    when no_data_found
    then
    begin      
    execute immediate l_sql into g_last_received_date using p_organization_id,p_inventory_item_id;
    exception when no_data_found then
    g_last_received_date :=null;
    when others then
    g_last_received_date :=null;
    end;      
    
                      l_hash_index:=p_inventory_item_id||'-'||p_organization_id;
                       g_last_received_date_vldn_tbl(l_hash_index) := g_last_received_date;
    end;

     return  g_last_received_date;
   
      exception when others then
      g_last_received_date:=null;
      return  g_last_received_date;

end get_last_received_date;

function get_item_cost(p_num in number,p_inventory_item_id in number,p_organization_id in number) return number
is
--//============================================================================
--//
--// Object Name         :: GET_ITEM_COST  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Venu        	18-May-2016       Initial Build 
--//============================================================================
l_hash_index varchar2(100);
l_sql varchar2(32000);
begin
g_item_cost := null;

    begin

        l_hash_index:=p_inventory_item_id||'-'||p_organization_id;
        g_item_cost :=g_item_cost_vldn_tbl(l_hash_index);

 exception
    when no_data_found
    then
    begin      

g_item_cost:=apps.cst_cost_api.get_item_cost(p_num,p_inventory_item_id,p_organization_id);

    exception when no_data_found then
    g_item_cost :=0;
    when others then
    g_item_cost :=0;
    end;      
                      l_hash_index:=p_inventory_item_id||'-'||p_organization_id;
                       g_item_cost_vldn_tbl(l_hash_index) := g_item_cost;
    end;
     return  g_item_cost;
     exception when others then
      g_item_cost:=0;
      return  g_item_cost;

end get_item_cost;

function get_primary_bin_loc (p_inventory_item_id in number,p_organization_id in number) return varchar2
is
--//============================================================================
--//
--// Object Name         :: GET_PRIMARY_BIN_LOC  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Venu        	18-May-2016       Initial Build 
--//============================================================================
l_hash_index varchar2(100);
l_sql varchar2(32000);
begin
g_primary_bin_loc := null;
l_sql :='select mil.segment1
        from    mtl_item_locations_kfv mil, 
               mtl_secondary_locators msl
        where  msl.inventory_item_id = :1
             and msl.organization_id   = :2
             and msl.secondary_locator = mil.inventory_location_id
             and mil.segment1 like ''1-%''';
    begin
        l_hash_index:=p_inventory_item_id||'-'||p_organization_id;
        g_primary_bin_loc := g_primary_bin_loc_vldn_tbl(l_hash_index);
 exception
    when no_data_found
    then
    begin      
    execute immediate l_sql into g_primary_bin_loc using p_inventory_item_id,p_organization_id;
    exception when no_data_found then
    g_primary_bin_loc :=null;
    when others then
    g_primary_bin_loc :=null;
    end;      
                      l_hash_index:=p_inventory_item_id||'-'||p_organization_id;
                       g_primary_bin_loc_vldn_tbl(l_hash_index) := g_primary_bin_loc;
    end;
     return  g_primary_bin_loc;
    exception when others then
      g_primary_bin_loc:=null;
      return  g_primary_bin_loc;

end GET_PRIMARY_BIN_LOC;

FUNCTION GET_ALTERNATE_BIN_LOC (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN varchar2
is
--//============================================================================
--//
--// Object Name         :: GET_ALTERNATE_BIN_LOC  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Venu        	18-May-2016       Initial Build 
--//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
C1 SYS_REFCURSOR;
L_STR  varchar2(1000);
begin
G_ALTERNATE_BIN_LOC := NULL;
L_SQL :='SELECT mil.concatenated_segments
              FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl
              WHERE     msl.inventory_item_id = :1
                    AND msl.organization_id   = :2
                    AND msl.secondary_locator = mil.inventory_location_id
                    AND mil.segment1 NOT LIKE ''1-%''';
    BEGIN

        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
        G_ALTERNATE_BIN_LOC := G_ALTERNATE_BIN_LOC_VLDN_TBL(L_HASH_INDEX);

    exception
    when NO_DATA_FOUND
    THEN
    begin   

     OPEN c1 FOR L_SQL USING P_INVENTORY_ITEM_ID,P_organization_id;


  LOOP
    FETCH C1 into L_STR;
    EXIT when c1%NOTFOUND;
    G_ALTERNATE_BIN_LOC:=G_ALTERNATE_BIN_LOC||','||L_STR;
    G_ALTERNATE_BIN_LOC:=ltrim(G_ALTERNATE_BIN_LOC,',');
  end LOOP;

    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_ALTERNATE_BIN_LOC :=null;
    WHEN OTHERS THEN
    G_ALTERNATE_BIN_LOC :=null;
    END;      
                      l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
                       G_ALTERNATE_BIN_LOC_VLDN_TBL(L_HASH_INDEX) := G_ALTERNATE_BIN_LOC;
    END;
    
     RETURN  G_ALTERNATE_BIN_LOC;
    EXCEPTION WHEN OTHERS THEN
      G_ALTERNATE_BIN_LOC:=null;
      RETURN  G_ALTERNATE_BIN_LOC;

end get_alternate_bin_loc;
*/  --Commented start for version 1.1

function GET_START_BIN return varchar2
as
--//============================================================================
--//
--// Object Name         :: GET_START_BIN  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Venu        	18-May-2016       Initial Build 
--//============================================================================
begin  
    RETURN G_START_BIN;
      EXCEPTION
      WHEN OTHERS THEN
      return null;
end;


function GET_END_BIN return varchar2
as
--//============================================================================
--//
--// Object Name         :: GET_END_BIN  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Venu        	18-May-2016       Initial Build 
--//============================================================================
begin  
    RETURN G_END_BIN;
      EXCEPTION
      when OTHERS then
      return null;
end;

/* -- comment start for version 1.2
FUNCTION get_mtd_sales (p_inventory_item_id    NUMBER,
                           p_organization_id      NUMBER,
                           p_set_of_books_id number)
      RETURN NUMBER
   IS
--//============================================================================
--//
--// Object Name         :: get_mtd_sales  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Venu        	18-May-2016       Initial Build 
--//============================================================================   
      l_mtd_sales   number;
      p_start_date date;
      p_end_date date;
      l_sql_1 varchar2(1000);
   BEGIN
   
   l_sql_1 :='Select start_date,nvl(end_date,sysdate) from 
              gl_period_statuses
              where application_id =101
              AND set_of_books_id =:1
              AND TRUNC(sysdate) between start_date and end_date';
   
   execute immediate l_sql_1 into p_start_date,p_end_date using p_set_of_books_id;
   
   
   
                SELECT NVL (SUM (oel.ordered_quantity), 0)
          INTO l_mtd_sales
          FROM OE_ORDER_HEADERS OEH,
            OE_ORDER_LINES OEL
          WHERE TRUNC (ordered_date) BETWEEN p_start_date AND p_end_date
          AND oeh.header_id         = oel.header_id
          AND oel.inventory_item_id = p_inventory_item_id
          AND OEL.ship_from_org_id  = P_ORGANIZATION_ID
          AND OEH.flow_status_code !='CANCELLED'
          AND oel.flow_status_code !='CANCELLED';

      RETURN l_mtd_sales;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   end;
*/  -- comment end for version 1.2

function get_start_date  ( p_set_of_books_id  in number)
  return date is
l_start_date      date;
l_hash_index      number;
l_period_year     NUMBER;
l_period_num      NUMBER;
l_pre_period_num  NUMBER;
l_pre_period_year NUMBER;
l_sql             VARCHAR2(32000);
l_sql1            varchar2(32000);  

BEGIN

l_start_date := null;

begin                 
        l_hash_index:= p_set_of_books_id;
        l_start_date := g_period_name_vldn_tbl(l_hash_index);
   
    exception
    when no_data_found 
    then
    l_sql:='Select period_year,period_num from 
              gl_period_statuses
              where application_id =101
              AND SET_OF_BOOKS_ID =:1
              AND TRUNC(sysdate) between start_date and end_date';

BEGIN  
EXECUTE IMMEDIATE L_SQL INTO L_PERIOD_YEAR,L_PERIOD_NUM USING P_SET_OF_BOOKS_ID;
exception when others then
null;
END;

if l_period_num<>12 then
l_pre_period_num := l_period_num+1;
l_pre_period_year:= l_period_year-1;
else
l_pre_period_num :=1;
l_pre_period_year:=l_period_year;
end if;

	l_sql1:='Select start_date from 
              gl_period_statuses
              WHERE APPLICATION_ID =101
              AND SET_OF_BOOKS_ID =:1
              AND PERIOD_NUM=:2
              and period_year=:3';

EXECUTE IMMEDIATE l_sql1 INTO L_START_DATE USING P_SET_OF_BOOKS_ID,L_PRE_PERIOD_NUM,L_PRE_PERIOD_YEAR;              

           l_hash_index:= p_set_of_books_id;
           g_period_name_vldn_tbl(l_hash_index) := l_start_date;
    end;
    return l_start_date;
    exception when others then
      l_start_date:=null;
      RETURN  L_START_DATE;
END get_start_date;


FUNCTION get_period_start_end_date(p_set_of_books_id in number,P_request_type in varchar2)
 RETURN date
IS
--//============================================================================
--//
--// Object Name         :: get_period_start_end_date  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter .
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.2        Siva  	      26-Dec-2016        TMS#20161014-00031 --Performance issue
--//============================================================================  
L_HASH_INDEX number;
l_sql varchar2(32000);
begin
g_period_start_date := NULL;
g_period_end_date   := NULL;

l_sql :='Select start_date,nvl(end_date,sysdate) from 
              gl_period_statuses
              where application_id =101
              AND set_of_books_id =:1
              AND TRUNC(sysdate) between start_date and end_date';
    BEGIN
           L_HASH_INDEX:= p_set_of_books_id; 
            if P_request_type  ='START_DATE' then
           g_period_start_date := G_START_DATE_VLDN_TBL(L_HASH_INDEX);
			ELSif P_request_type  ='END_DATE' then
           g_period_end_date   := G_END_DATE_VLDN_TBL(L_HASH_INDEX);
			END IF;
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO g_period_start_date,g_period_end_date USING p_set_of_books_id; 
    EXCEPTION WHEN NO_DATA_FOUND THEN
                if P_request_type='START_DATE' then
				g_period_start_date := null;
                elsif P_request_type='END_DATE' then
				g_period_end_date := null;
                end if;
    end;
    
        l_hash_index:= p_set_of_books_id; 
         G_START_DATE_VLDN_TBL(L_HASH_INDEX) := g_period_start_date;
         G_END_DATE_VLDN_TBL(L_HASH_INDEX)  := g_period_end_date;
                                                
    END;
      if P_request_type='START_DATE' then
		return  g_period_start_date ;
      elsif P_request_type='END_DATE' then
		return  g_period_end_date ;
	  end if;
    EXCEPTION when OTHERS then
        if P_request_type='START_DATE' then
			g_period_start_date := null;
            return  g_period_start_date ;
        elsif P_request_type='END_DATE' then
			g_period_end_date := null;
            return  g_period_end_date ;
        end if;

END get_period_start_end_date;

  
procedure get_bin_location_info (p_process_id         in number,
                                 p_category_from      in varchar2 default null,
                                 p_category_to        in varchar2 default null,
                                 p_organization       IN VARCHAR2  DEFAULT NULL,
                                 p_category_set       IN VARCHAR2  DEFAULT NULL--,
                                 --p_quantity_on_hand   in varchar2
								) as
--//============================================================================
--//
--// Object Name         :: get_bin_location_info  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     	Venu  			05-May-2016 		Initial Build   
--// 1.1        Siva  	      	26-Oct-2016       TMS#20160824-00006 --Performance issue
--// 1.3     	Siva 		    15-Mar-2017        TMS#20170224-00065 
--//============================================================================								
  type org_list_rec is record
  (
  process_id number,
  organization_id number
  );
  type org_list_rec_tab is table of org_list_rec index by binary_integer;
  ORG_LIST_TAB ORG_LIST_REC_TAB;
  TYPE GET_BIN_LOC_FLAG_REC IS RECORD 
  ( process_id 		      number,
    INVENTORY_ITEM_ID 	NUMBER,
    organization_id 	  number
  );
TYPE GET_BIN_LOC_FLAG_REC_TAB IS TABLE OF GET_BIN_LOC_FLAG_REC INDEX BY BINARY_INTEGER;
get_bin_loc_flag_tab get_bin_loc_flag_rec_tab;

type inv_onhand_dtls_rec is record
(
  PROCESS_ID          NUMBER,
  onhand 			        number,
  INVENTORY_ITEM_ID 	NUMBER,
  organization_id 	  number
);
TYPE INV_ONHAND_DTLS_REC_TAB IS TABLE OF INV_ONHAND_DTLS_REC INDEX BY BINARY_INTEGER;
inv_onhand_dtls_tab inv_onhand_dtls_rec_tab;
  
  L_LOCATION_ID 	VARCHAR2(32000);
  l_category_set  VARCHAR2(32000);
  L_REF_CURSOR1   CURSOR_TYPE4;
  L_REF_CURSOR2   CURSOR_TYPE4;
  L_REF_CURSOR3   CURSOR_TYPE4;
  L_REF_CURSOR4   CURSOR_TYPE4;
  L_STMT1         VARCHAR2(32000);
  L_STMT2         VARCHAR2(32000);
  L_STMT3         VARCHAR2(32000);
  L_STMT4         VARCHAR2(32000);
  L_SEGMENT1      VARCHAR2(200);
  L_SEGMENT2      VARCHAR2(200);
  L_SEGMENT3      VARCHAR2(200);
  L_SEGMENT4      varchar2(200);
  L_SEGMENT1_FROM VARCHAR2(10000);
  L_SEGMENT2_FROM VARCHAR2(10000);
  L_SEGMENT3_FROM VARCHAR2(10000);
  L_SEGMENT4_FROM VARCHAR2(10000);
  L_SEGMENT1_TO   VARCHAR2(10000);
  L_SEGMENT2_TO   VARCHAR2(10000);
  L_SEGMENT3_TO   VARCHAR2(10000);
  l_segment4_to   varchar2(10000);
    l_delete_stmt  varchar2(32000);
     TYPE BIN_DATA_INFO IS TABLE OF XXEIS.EIS_XXWC_INV_BIN_LOC_SALES_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
   bin_data_info_tab                    bin_data_info;

  BEGIN
    inv_onhand_dtls_tab.delete;
  bin_data_info_tab.delete;
  GET_BIN_LOC_FLAG_TAB.DELETE;
  ORG_LIST_TAB.DELETE;
  
  if p_organization='All' then  
  l_location_id :='and 1=1';
  elsif p_organization is null then 
  l_location_id :='and 1=1';
  ELSE
  l_location_id :=   'and moq.organization_id in (select organization_id from mtl_parameters where organization_code in('||xxeis.eis_rs_utility.get_param_values(p_organization)||' ))'	; --need to correct one this
  END IF;

   IF P_ORGANIZATION IS NULL THEN
     L_STMT1:=	'select '||P_PROCESS_ID||',organization_id from xxeis.eis_org_access_v';
   ELSIF P_ORGANIZATION!='All' THEN
    L_STMT1:=	'select '||P_PROCESS_ID||',organization_id from xxeis.eis_org_access_v where  organization_code in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ORGANIZATION)||' )';
   ELSE
    L_STMT1:=	'select '||P_PROCESS_ID||',organization_id from xxeis.eis_org_access_v';
  end if;
  
  IF P_CATEGORY_SET IS NOT NULL  THEN
    L_CATEGORY_SET:='and mcats.category_set_name in  ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_CATEGORY_SET)||')';
  end if;
  
  if p_category_from is not null then

    SPLIT_SEGMENTS ( P_CATEGORY_FROM,L_SEGMENT1,L_SEGMENT2 ,L_SEGMENT3,l_segment4);
 
     if L_SEGMENT1 is not null then
     
--     and nvl(mcvs.segment1,'''||l_segment1||''')
       L_SEGMENT1_FROM:='and nvl(mcvs.segment1,'''||L_SEGMENT1||''') >='''||L_SEGMENT1||'''';
    end if;
    
     if l_segment2 is not null then
      L_SEGMENT2_FROM:='and nvl(mcvs.segment2,'''||L_SEGMENT2||''') >='''||l_segment2||'''';
    end if;
    
     if l_segment3 is not null then
      L_SEGMENT3_FROM:='and nvl(mcvs.segment3,'''||L_SEGMENT3||''') >='''||l_segment3||'''';
    end if;
    
     if l_segment4 is not null then
      L_SEGMENT4_FROM:='and nvl(mcvs.segment4,'''||L_SEGMENT4||''') >='''||l_segment4||'''';
    end if;
 END IF;


  if p_category_to is not null then

    split_segments ( p_category_to,l_segment1,l_segment2 ,l_segment3,l_segment4);
 
     if l_segment1 is not null then
        L_SEGMENT1_to:='and nvl(mcvs.segment1,'''||L_SEGMENT1||''') <='''||L_SEGMENT1||'''';
    end if;
    
     if l_segment2 is not null then
      L_SEGMENT2_to:='and nvl(mcvs.segment2,'''||L_SEGMENT2||''')  <='''||l_segment2||'''';
    end if;
    
     if l_segment3 is not null then
      L_SEGMENT3_to:='and nvl(mcvs.segment3,'''||L_SEGMENT3||''') <='''||l_segment3||'''';
    end if;
    
     if l_segment4 is not null then
      L_SEGMENT4_to:='and nvl(mcvs.segment4,'''||L_SEGMENT4||''') <='''||l_segment4||'''';
    END IF;
 END IF;

--dbms_output.put_line('l_stmt1'||l_stmt1);
    open l_ref_cursor1  for l_stmt1  ;
		loop
			fetch l_ref_cursor1 bulk collect into org_list_tab limit 10000;
			if org_list_tab.count  >= 1  then
                  forall j in 1 .. org_list_tab.count 
                      insert into xxeis.EIS_XXWC_VALID_BIN_SALES_ORGS
                          values org_list_tab(j);    
                 --commit; --Commented for version 1.3
       end if;

       			if l_ref_cursor1%notfound then
              close l_ref_cursor1;
              exit;
            END IF;
		END LOOP;
    
    ---load bin orgs info
--  dbms_output.put_line('bin start');
  
    if  xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_START_BIN is null and xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_END_BIN is null then
      NULL;
	  elsif  xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_START_BIN is not null and  xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_END_BIN is not null then
    L_STMT2:='select '||p_process_id||' process_id ,
              msl.inventory_item_id,
              msl.organization_id
            from mtl_item_locations_kfv mil, mtl_secondary_locators msl
            where  msl.secondary_locator = mil.inventory_location_id
            and substr (mil.segment1, 3) between xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_START_BIN and xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_END_BIN';
												 --create index on substr (mil.segment1, 3)
	  elsif  xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_START_BIN is not null and  xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_END_BIN is null then
      L_STMT2:='select '||p_process_id||' process_id ,
                msl.inventory_item_id,
                msl.organization_id
            from mtl_item_locations_kfv mil, mtl_secondary_locators msl
              where  msl.secondary_locator = mil.inventory_location_id
              and substr (mil.segment1, 3) >= xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_START_BIN';
	  elsif  xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_START_BIN is null and  xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_END_BIN is not null then
	  L_STMT2:='select '||p_process_id||' process_id ,
              msl.inventory_item_id,
              msl.organization_id
           from mtl_item_locations_kfv mil, mtl_secondary_locators msl
           where  msl.secondary_locator = mil.inventory_location_id
          and substr (mil.segment1, 3) <= xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_END_BIN';
	 end if;	
   
 IF  XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_START_BIN IS NULL AND XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_END_BIN IS NULL THEN
 NULL;
 ELSE 
   open l_ref_cursor2  for L_STMT2  ;
		loop
			fetch l_ref_cursor2 bulk collect into get_bin_loc_flag_tab limit 10000;
			if get_bin_loc_flag_tab.count  >= 1  then
                  forall j in 1 .. get_bin_loc_flag_tab.count 
                     insert into xxeis.EIS_XXWC_BIN_SALES_ORGS_TAB
                          values get_bin_loc_flag_tab(j);    
                 --commit; --Commented for version 1.3
       end if;

       			if l_ref_cursor2%notfound then
				close l_ref_cursor2;
				exit;
			end if;
		END LOOP;     
 end if;			 
	
 --Removing duplicates from  table EIS_XXWC_BIN_SALES_ORGS_TAB
BEGIN
  l_delete_stmt:= 'DELETE FROM xxeis.EIS_XXWC_BIN_SALES_ORGS_TAB
   WHERE ROWID IN (
              SELECT rid
                FROM (SELECT ROWID RID,
                             ROW_NUMBER () OVER (PARTITION BY inventory_item_id,organization_id ORDER BY ROWID) RN
                        FROM xxeis.EIS_XXWC_BIN_SALES_ORGS_TAB where process_id='||P_PROCESS_ID ||')
               WHERE RN <> 1)
   AND PROCESS_ID ='||P_PROCESS_ID ||'';
   
   EXECUTE IMMEDIATE L_DELETE_STMT;
   
--   dbms_output.put_line('l_delete_statement is' ||l_delete_stmt);
   --commit;  --Commented for version 1.3
        EXCEPTION WHEN OTHERS THEN
     FND_FILE.PUT_LINE(FND_FILE.LOG,'Removing Duplicates '||SQLCODE||SQLERRM);
--dbms_output.PUT_LINE('Removing Duplicates '||SQLCODE||SQLERRM);

END; 
  
  
   
   IF  XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_START_BIN IS NULL AND XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_END_BIN IS NULL THEN 
  l_stmt3 := 'select '||p_process_id||' process_id,onhand,inventory_item_id,organization_id 
            from(
                select /*+ index(moq mtl_onhand_quantities_n4)*/ 
                      nvl (sum (moq.transaction_quantity), 0) onhand ,
                      moq.inventory_item_id,
                      moq.organization_id
                      from mtl_onhand_quantities_detail moq
					  where 1=1
                    '||l_location_id||'
                  group by moq.inventory_item_id,moq.organization_id)x
                  ';
                  dbms_output.put_line('l_stmt3'||l_stmt3);
  ELSE
  l_stmt3 := 'select '||p_process_id||' process_id,onhand,inventory_item_id,organization_id 
              from(
                  select /*+ index(moq mtl_onhand_quantities_n4)*/ 
                  nvl (sum (moq.transaction_quantity), 0) onhand ,
                  moq.inventory_item_id,
                  moq.organization_id
            from mtl_onhand_quantities_detail moq,
                xxeis.EIS_XXWC_BIN_SALES_ORGS_TAB tab --- create index on item and org
            where tab.inventory_item_id=moq.inventory_item_id
              and tab.organization_id=moq.organization_id
              and tab.process_id ='||P_process_id||'
                '||l_location_id||'
              group by moq.inventory_item_id,moq.organization_id) x
            ';
  end if;
  
  
  
    /*IF P_QUANTITY_ON_HAND ='On Hand' THEN
        l_stmt3 := l_stmt3||chr(10)||'where x.onhand>0' ;
      ELSIF P_QUANTITY_ON_HAND ='No on Hand' THEN
        l_stmt3 := l_stmt3||chr(10)|| 'where x.onhand=0' ;
      ELSIF P_QUANTITY_ON_HAND ='Negative on hand' THEN
        l_stmt3 := l_stmt3||chr(10)|| 'where x.onhand<0' ;
      end if;*/
	  
	        open l_ref_cursor3  for l_stmt3  ;
		loop
			fetch l_ref_cursor3 bulk collect into inv_onhand_dtls_tab limit 10000;
			if inv_onhand_dtls_tab.count  >= 1  then
                  forall j in 1 .. inv_onhand_dtls_tab.count 
                     
                     insert into xxeis.EIS_XXWC_BIN_SALES_ONHAND_TAB
                          values inv_onhand_dtls_tab(j);    
                 --commit; --Commented for version 1.3
       end if;

       			if l_ref_cursor3%notfound then
				close l_ref_cursor3;
				exit;
			END IF;
		end loop; 

	IF  XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_START_BIN IS NULL AND XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG.GET_END_BIN IS NULL THEN 		
  l_stmt4:= 'select /*+ use_nl(mics,mcvs)*/  --added for version 1.1
    '||p_process_id||' process_id,
    ood.organization_code organization,
    msi.segment1 part_number,
    msi.description,
    msi.primary_uom_code uom,
    msi.list_price_per_unit selling_price,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,
    isr.vendor_name vendor_name,
    isr.vendor_num vendor_number,
    min_minmax_quantity,
    max_minmax_quantity,
    NVL (apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost , --added for version 1.1
    --nvl ( xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost, --Commented for version 1.1
    msi.unit_weight weight, --xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat,
    isr.inv_cat_seg1
    || ''.''
    || isr.cat cat,
    --    xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_onhand_inv ( msi.inventory_item_id, msi.organization_id) onhand,  --tms#20160429-00037    by siva  on 04-24-2016
    nvl(boh.onhand,0) onhand, 
	  (SELECT nvl(SUM (oel.ordered_quantity),0)
          FROM OE_ORDER_HEADERS OEH,
            OE_ORDER_LINES OEL
          WHERE TRUNC (ordered_date) BETWEEN xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_period_start_end_date(ood.set_of_books_id,''START_DATE'')
          AND xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_period_start_end_date(ood.set_of_books_id,''END_DATE'')
          AND oeh.header_id         = oel.header_id
          AND oel.inventory_item_id = msi.inventory_item_id
          AND OEL.ship_from_org_id  = msi.organization_id
          AND OEH.flow_status_code !=''CANCELLED''
          AND oel.flow_status_code !=''CANCELLED'') mtd_sales,  --added for version 1.2
   -- xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_mtd_sales(msi.inventory_item_id,msi.organization_id,ood.set_of_books_id) mtd_sales,
	--    xxeis.eis_rs_xxwc_com_util_pkg.get_mtd_sales(msi.inventory_item_id,msi.organization_id,gps.start_date,gps.end_date) mtd_sales,
    -- xxeis.eis_rs_xxwc_com_util_pkg.get_ytd_sales(msi.inventory_item_id,msi.organization_id,gps.year_start_date) ytd_sales, Commeneted for Ver 1.1
     /*( SELECT NVL (SUM (oel.ordered_quantity), 0)
       FROM apps.oe_order_headers_all oeh, apps.oe_order_lines_all oel
       WHERE     TRUNC (ordered_date) BETWEEN  add_months(trunc(ordered_date, ''MM''), -12)
                                          AND TRUNC (SYSDATE)
             AND oeh.header_id = oel.header_id
             AND oel.inventory_item_id = msi.inventory_item_id
             AND oel.ship_from_org_id = msi.organization_id
             AND oel.ORDERED_item_id = msi.inventory_item_id
			 ) ytd_sales, --Added for Ver 1.1
       */
       (SELECT nvl((SUM(mmt.TRANSACTION_QUANTITY)*-1),0) 
		FROM MTL_MATERIAL_TRANSACTIONS MMT
		WHERE MMT.INVENTORY_ITEM_ID = msi.inventory_item_id
			AND MMT.ORGANIZATION_ID     =MSI.ORGANIZATION_ID
			AND MMT.TRANSACTION_TYPE_ID =33
			AND TRUNC(mmt.CREATION_DATE) BETWEEN TRUNC(XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG.get_start_date(ood.set_of_books_id)) AND TRUNC(sysdate)) ytd_sales,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_mtd_sales(msi.inventory_item_id,msi.organization_id,gps.start_date,gps.end_date) mtd_sales,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_ytd_sales(msi.inventory_item_id,msi.organization_id,gps.year_start_date) ytd_sales,
    --          xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (
    --             msi.inventory_item_id,
    --             msi.organization_id)
    --             primary_bin_loc1,  --commented for tms#20160429-00037    by siva  on 04-24-2016
    --xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) primary_bin_loc,  --Commented for version 1.1
	   (select mil.segment1        
			from mtl_item_locations_kfv mil,                
				 mtl_secondary_locators msl        
		where  msl.inventory_item_id = msi.inventory_item_id            
			and msl.organization_id   = msi.organization_id            
			and msl.secondary_locator = mil.inventory_location_id             
			and mil.segment1 like ''1-%''
			and rownum=1) primary_bin_loc, --Added for version 1.1
    --          xxeis.eis_rs_xxwc_com_util_pkg.get_alternate_bin_loc (
    --             msi.inventory_item_id,
    --             msi.organization_id)
    --             alternate_bin_loc,--commented --tms#20160429-00037    by siva  on 04-24-2016
    -- xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_alternate_bin_loc ( msi.inventory_item_id, msi.organization_id) alternate_bin_loc,   --Commented for version 1.1
	(SELECT mil.concatenated_segments              
          FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl              
          WHERE     msl.inventory_item_id = msi.inventory_item_id                    
          	AND msl.organization_id   = msi.organization_id                    
          	AND msl.secondary_locator = mil.inventory_location_id                    
          	AND mil.segment1 NOT LIKE ''1-%''
          	and rownum=1) alternate_bin_loc, --Added for version 1.1
    substr (msi.segment1, 3) bin_loc,
    ood.organization_name location,
    case
      when (mcvs.segment1 in (''1'', ''2'', ''3'', ''4'', ''5'', ''6'', ''7'', ''8'', ''9'', ''C'', ''B''))
      then ''Y''
      when ( mcvs.segment1    in (''E'')
      and (min_minmax_quantity = 0
      and max_minmax_quantity  = 0))
      then ''N''
      when ( mcvs.segment1    in (''E'')
      and (min_minmax_quantity > 0
      and max_minmax_quantity  > 0))
      then ''Y''
      when (mcvs.segment1 in (''N'', ''Z''))
        --and item_type                                                                                           =''non-stock'')
      then ''N''
      else ''N''
    end stk, --xxeis.eis_rs_xxwc_com_util_pkg.get_open_order_status(msi.inventory_item_id, msi.organization_id) open_order,
    case
      when nvl (isr.demand, 0) != 0
      then ''Y''
      else ''N''
    end open_order,
    case
      when nvl (isr.demand, 0) != 0
      then ''Y''
      else ''N''
    end open_demand, --added
    mcvs.concatenated_segments category,
    mcats.category_set_name category_set_name, --primary keys
    msi.inventory_item_id,
    msi.organization_id inv_organization_id,
    ood.organization_id org_organization_id,
  --  gps.application_id,
      101 application_id,
      ood.set_of_books_id,
  --  gps.set_of_books_id ----------------added by diane 1/23/2014
  -- xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_last_received_date(msi.inventory_item_id,msi.organization_id) last_received_date --Commented for version 1.1
     (SELECT MAX(transaction_date)
      FROM apps.rcv_transactions rt
      WHERE rt.destination_type_code = ''INVENTORY''
        AND rt.organization_id         = msi.organization_id
        AND EXISTS
        (SELECT 1
            FROM apps.rcv_shipment_lines rsl
          WHERE item_id              = msi.inventory_item_id
        AND to_organization_id       = msi.organization_id
        AND rsl.shipment_line_id     = rt.shipment_line_id
        AND rsl.shipment_header_id   = rt.shipment_header_id
        AND rsl.po_line_location_id IS NOT NULL ) )last_received_date  --Added for version 1.1
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  from  mtl_system_items_kfv msi,
        org_organization_definitions ood,
		-- gl_period_statuses gps,
		mtl_categories_kfv mcvs,
		mtl_item_categories mics,
		xxeis.EIS_XXWC_BIN_SALES_ONHAND_TAB boh,
		xxeis.eis_xxwc_po_isr_tab isr, --added
        xxeis.EIS_XXWC_VALID_BIN_SALES_ORGS xvbo,
		mtl_category_sets mcats
  where 1                   = 1
  and msi.organization_id   = ood.organization_id
  and msi.organization_id   = isr.organization_id(+)
  and msi.inventory_item_id = isr.inventory_item_id(+)
  --and application_id        = 101
  --and gps.set_of_books_id   = ood.set_of_books_id
  --and trunc(sysdate) between gps.start_date and gps.end_date
  -- and xxeis.eis_rs_xxwc_com_util_pkg.get_bin_loc_flag ( msi.inventory_item_id, msi.organization_id) = ''Y''
  and msi.organization_id = mics.organization_id(+)
  and msi.inventory_item_id = mics.inventory_item_id(+)
  and mics.category_id = mcvs.category_id(+)
  and mics.category_set_id = mcats.category_set_id(+)          --added
  and msi.inventory_item_id = boh.inventory_item_id(+)
  and msi.organization_id  = boh.organization_id(+)  
  and boh.process_id(+)      ='||p_process_id||'
  and msi.organization_id  = xvbo.organization_id  
  and xvbo.process_id       ='||p_process_id||'
  '||l_category_set||'
  '||l_segment1_from||'
  '||l_segment2_from||'
  '||l_segment3_from||'
  '||l_segment4_from||'
  '||l_segment1_to||'
  '||l_segment2_to||'
  '||l_segment3_to||'
  '||l_segment4_to||'';
  else
  L_STMT4:=  'select '||P_PROCESS_ID||' process_id,--tms#20160429-00037    by siva  on 04-24-2016
    ood.organization_code organization,
    msi.segment1 part_number,
    msi.description,
    msi.primary_uom_code uom,
    msi.list_price_per_unit selling_price, --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,
    isr.vendor_name vendor_name,
    isr.vendor_num vendor_number,
    min_minmax_quantity,
    max_minmax_quantity,
    NVL (apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost , --added for version 1.1
    --nvl ( xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost, --Commented for version 1.1
    msi.unit_weight weight, --xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat,
    isr.inv_cat_seg1
    || ''.''
    || isr.cat cat,
   --    xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_onhand_inv ( msi.inventory_item_id, msi.organization_id) onhand,  --tms#20160429-00037    by siva  on 04-24-2016
   nvl(boh.onhand,0) onhand, 
		  (SELECT nvl(SUM (oel.ordered_quantity),0)
          FROM OE_ORDER_HEADERS OEH,
            OE_ORDER_LINES OEL
          WHERE TRUNC (ordered_date) BETWEEN xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_period_start_end_date(ood.set_of_books_id,''START_DATE'')
          AND xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_period_start_end_date(ood.set_of_books_id,''END_DATE'')
          AND oeh.header_id         = oel.header_id
          AND oel.inventory_item_id = msi.inventory_item_id
          AND OEL.ship_from_org_id  = msi.organization_id
          AND OEH.flow_status_code !=''CANCELLED''
          AND oel.flow_status_code !=''CANCELLED'') mtd_sales, --added for version 1.2
   -- xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_mtd_sales(msi.inventory_item_id,msi.organization_id,ood.set_of_books_id) mtd_sales,
   --   xxeis.eis_rs_xxwc_com_util_pkg.get_mtd_sales(msi.inventory_item_id,msi.organization_id,gps.start_date,gps.end_date) mtd_sales,
   -- xxeis.eis_rs_xxwc_com_util_pkg.get_ytd_sales(msi.inventory_item_id,msi.organization_id,gps.year_start_date) ytd_sales, Commeneted for Ver 1.1
  /*( SELECT NVL (SUM (oel.ordered_quantity), 0)
       FROM apps.oe_order_headers_all oeh, apps.oe_order_lines_all oel
       WHERE     TRUNC (ordered_date) BETWEEN  add_months(trunc(ordered_date, ''MM''), -12)
                                          AND TRUNC (SYSDATE)
             AND oeh.header_id = oel.header_id
             AND oel.inventory_item_id = msi.inventory_item_id
             AND oel.ship_from_org_id = msi.organization_id
             AND oel.ORDERED_item_id = msi.inventory_item_id
			 ) ytd_sales, --Added for Ver 1.1
       */
       (SELECT nvl((SUM(mmt.TRANSACTION_QUANTITY)*-1),0) 
			FROM MTL_MATERIAL_TRANSACTIONS MMT
			WHERE MMT.INVENTORY_ITEM_ID = msi.inventory_item_id
				AND MMT.ORGANIZATION_ID     =MSI.ORGANIZATION_ID
				AND MMT.TRANSACTION_TYPE_ID =33
				AND TRUNC(mmt.CREATION_DATE) BETWEEN TRUNC(XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG.get_start_date(ood.set_of_books_id)) AND TRUNC(sysdate)) ytd_sales,
    --          xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (
    --             msi.inventory_item_id,
    --             msi.organization_id)
    --             primary_bin_loc1,  --commented for tms#20160429-00037    by siva  on 04-24-2016
    --xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) primary_bin_loc,  --Commented for version 1.1
	 (select mil.segment1        
			from mtl_item_locations_kfv mil,                
				 mtl_secondary_locators msl        
		where  msl.inventory_item_id = msi.inventory_item_id            
			and msl.organization_id   = msi.organization_id            
			and msl.secondary_locator = mil.inventory_location_id             
			and mil.segment1 like ''1-%''
			and rownum=1) primary_bin_loc,  --Added for version 1.1
    --          xxeis.eis_rs_xxwc_com_util_pkg.get_alternate_bin_loc (
    --             msi.inventory_item_id,
    --             msi.organization_id)
    --             alternate_bin_loc,
    --xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_alternate_bin_loc ( msi.inventory_item_id, msi.organization_id) alternate_bin_loc,   --Commented for version 1.1
	(SELECT mil.concatenated_segments              
          FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl              
          WHERE     msl.inventory_item_id = msi.inventory_item_id                    
          	AND msl.organization_id   = msi.organization_id                    
          	AND msl.secondary_locator = mil.inventory_location_id                    
          	AND mil.segment1 NOT LIKE ''1-%''
          	and rownum=1) alternate_bin_loc, --Commented for version 1.1
    substr (msi.segment1, 3) bin_loc,
    ood.organization_name location,
    case
      when (mcvs.segment1 in (''1'', ''2'', ''3'', ''4'', ''5'', ''6'', ''7'', ''8'', ''9'', ''c'', ''b''))
      then ''Y''
      when ( mcvs.segment1    in (''E'')
      and (min_minmax_quantity = 0
      and max_minmax_quantity  = 0))
      then ''N''
      when ( mcvs.segment1    in (''E'')
      and (min_minmax_quantity > 0
      and max_minmax_quantity  > 0))
      then ''Y''
      when (mcvs.segment1 in (''N'', ''Z''))
        --and item_type                                                                                           =''non-stock'')
      then ''N''
      else ''N''
    end stk, --xxeis.eis_rs_xxwc_com_util_pkg.get_open_order_status(msi.inventory_item_id, msi.organization_id) open_order,
    case
      when nvl (isr.demand, 0) != 0
      then ''Y''
      else ''N''
    end open_order,
    case
      when nvl (isr.demand, 0) != 0
      then ''Y''
      else ''N''
    end open_demand, --added
    mcvs.concatenated_segments category,
    mcats.category_set_name category_set_name, --primary keys
    msi.inventory_item_id,
    msi.organization_id inv_organization_id,
    ood.organization_id org_organization_id,
  --  gps.application_id,
    101 application_id,
    ood.set_of_books_id,
  --  gps.set_of_books_id ----------------added by diane 1/23/2014
  -- xxeis.EIS_XXWC_BIN_LOCA_SALES_PKG.get_last_received_date(msi.inventory_item_id,msi.organization_id) last_received_date --Commented for version 1.1
     (SELECT MAX(transaction_date)
      FROM apps.rcv_transactions rt
      WHERE rt.destination_type_code = ''INVENTORY''
        AND rt.organization_id         = msi.organization_id
        AND EXISTS
        (SELECT 1
            FROM apps.rcv_shipment_lines rsl
          WHERE item_id              = msi.inventory_item_id
        AND to_organization_id       = msi.organization_id
        AND rsl.shipment_line_id     = rt.shipment_line_id
        AND rsl.shipment_header_id   = rt.shipment_header_id
        AND rsl.po_line_location_id IS NOT NULL ) )last_received_date  --Commented for version 1.1
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  from mtl_system_items_kfv msi,
    org_organization_definitions ood,
   -- gl_period_statuses gps,
    mtl_categories_kfv mcvs,
    mtl_item_categories mics,
    xxeis.EIS_XXWC_BIN_SALES_ONHAND_TAB boh,
    xxeis.eis_xxwc_po_isr_tab isr, --added
    xxeis.EIS_XXWC_BIN_SALES_ORGS_TAB ftab, --- added
    xxeis.EIS_XXWC_VALID_BIN_SALES_ORGS xvbo,
    mtl_category_sets mcats
  where 1                   = 1
  and msi.organization_id   = ood.organization_id
  and msi.organization_id   = isr.organization_id(+)
  and msi.inventory_item_id = isr.inventory_item_id(+)
 -- and application_id        = 101
 -- and gps.set_of_books_id   = ood.set_of_books_id
 -- and trunc(sysdate) between gps.start_date and gps.end_date
 -- and xxeis.eis_rs_xxwc_com_util_pkg.get_bin_loc_flag ( msi.inventory_item_id, msi.organization_id) = ''Y''
  and msi.organization_id = mics.organization_id(+)
  and msi.inventory_item_id = mics.inventory_item_id(+)
  and mics.category_id = mcvs.category_id(+)
  and mics.category_set_id = mcats.category_set_id(+)          --added
  and msi.inventory_item_id = boh.inventory_item_id(+)
  and msi.organization_id  = boh.organization_id(+)
  and boh.process_id   (+)    ='||p_process_id||'
  and msi.inventory_item_id  = ftab.inventory_item_id
  and msi.organization_id    = ftab.organization_id
  and ftab.process_id       ='||p_process_id||'
  and msi.organization_id  = xvbo.organization_id  
  and xvbo.process_id       ='||p_process_id||'
   '||l_category_set||'
    '||l_segment1_from||'
    '||l_segment2_from||'
    '||l_segment3_from||'
    '||l_segment4_from||'
    '||l_segment1_to||'
    '||l_segment2_to||'
    '||l_segment3_to||'
    '||l_segment4_to||'
    ';
  
  
  End if;
  
  FND_FILE.PUT_LINE(FND_FILE.LOG,'l_stmt4'||l_stmt4);
    open l_ref_cursor4  for l_stmt4  ;
		LOOP
			fetch l_ref_cursor4 bulk collect into bin_data_info_tab limit 10000;
			if bin_data_info_tab.count  >= 1  then
                  forall j in 1 .. bin_data_info_tab.count 
                     
                     insert into xxeis.EIS_XXWC_INV_BIN_LOC_SALES_TBL
                          values bin_data_info_tab(j);    
                -- commit; --Commented for version 1.3
       end if;

       			IF L_REF_CURSOR4%NOTFOUND THEN
				close l_ref_cursor4;
				exit;
			END IF;
		end loop; 

--COMMIT; --Commented for version 1.3

END GET_BIN_LOCATION_INFO;

/* --Commented code for version 1.3
PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: CLEAR_TEMP_TABLES  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in Afteer report and delete data from Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Venu  				05-May-2016 	Initial Build   
--//============================================================================
  begin  
  delete from XXEIS.EIS_XXWC_VALID_BIN_SALES_ORGS where PROCESS_ID=P_PROCESS_ID;
  delete from XXEIS.EIS_XXWC_BIN_SALES_ORGS_TAB   where PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_BIN_SALES_ONHAND_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_INV_BIN_LOC_SALES_TBL WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
  
END ;                                        
*/ --Commented code for version 1.3

END EIS_XXWC_BIN_LOCA_SALES_PKG;
/
