CREATE OR REPLACE PACKAGE BODY XXEIS.eis_inv_xxwc_prodfill_rate_pkg
AS

/* GET_BO_AGING_BUCKET: This procedure is used to get the backordered lines aging buket information.
*/
FUNCTION GET_BO_AGING_BUCKET(
    P_BUCKET_SIZE NUMBER ,
    P_RELEASED_STATUS VARCHAR2,
    p_item_id     NUMBER,
    p_org_id      NUMBER,
    p_date DATE )
  RETURN NUMBER
IS
  l_30_bo_bucket NUMBER;
  l_60_bo_bucket NUMBER;
  l_90_bo_bucket NUMBER;
BEGIN
  IF p_bucket_size=30 THEN
    BEGIN
      SELECT COUNT(split_from_line_id)
      INTO l_30_bo_bucket
      FROM oe_order_lines
      WHERE creation_date between sysdate - 30 and sysdate
        AND inventory_item_id = p_item_id
        AND ship_from_org_id  = p_org_id;
      RETURN l_30_bo_bucket;
    EXCEPTION
    WHEN no_data_found THEN
      RETURN 0;
    WHEN OTHERS THEN
      RETURN 0;
    END ;
  END IF;
  IF p_bucket_size=60 THEN
    BEGIN
      SELECT COUNT(split_from_line_id)
      INTO l_60_bo_bucket
      FROM oe_order_lines 
      WHERE creation_date between sysdate - 60 and sysdate
        AND inventory_item_id = p_item_id
        AND ship_from_org_id  = p_org_id;
      RETURN l_60_bo_bucket;
    EXCEPTION
    WHEN no_data_found THEN
      RETURN 0;
    WHEN OTHERS THEN
      RETURN 0;
    END ;
  END IF;
  IF p_bucket_size=90 THEN
    BEGIN
      SELECT COUNT(split_from_line_id)
      INTO L_90_BO_BUCKET
      FROM oe_order_lines
      WHERE creation_date between sysdate - 90 and sysdate
        AND inventory_item_id = p_item_id
        AND ship_from_org_id  = p_org_id;
      RETURN L_90_BO_BUCKET;
    EXCEPTION
    WHEN no_data_found THEN
      RETURN 0;
    WHEN OTHERS THEN
      RETURN 0;
    END ;
  END IF;
END get_bo_aging_bucket;
PROCEDURE PROD_FILL_RATE(
    p_process_id     IN NUMBER,
    p_region         IN VARCHAR2,
    p_district       IN VARCHAR2,
    p_location       IN VARCHAR2,
    p_location_list  IN VARCHAR2,
    p_vendor_list    IN VARCHAR2,
    p_source_list    IN VARCHAR2,
    p_cat_class_list IN VARCHAR2,
    p_item_list      IN VARCHAR2 )
IS
  l_main_sql LONG;
  l_order_sql LONG;
  L_BO_SQL LONG;
  L_REGDIST_SQL LONG;
  L_BUYER_SQL LONG;
  L_SVELOCITY_SQL LONG;
  L_SUMM_ORDER_SQL LONG;
  L_YESTERDAY_SQL LONG;
  L_STK_REGDIST_SQL LONG;
  L_LOC_SQL LONG;
  l_condition_str        VARCHAR2(32000);
  L_Location_List_Vlaues VARCHAR2(32000);
  l_supplier_exists      VARCHAR2(32767);
  l_org_exists           VARCHAR2(32000);
  l_item_exists          VARCHAR2(32000);
  l_catclass_exists      VARCHAR2(32000);
  l_source_exists        VARCHAR2(32000);
  l_ref_cursor CURSOR_TYPE;
  l_insert_recd_cnt  NUMBER;
  l_order_Status     NUMBER;
  l_Order_count      NUMBER;
  l_closed_status    NUMBER;
  l_bo_status        NUMBER;
  l_error_count      NUMBER;
  l_30_BO_count      NUMBER;
  l_60_bo_count      NUMBER;
  l_90_bo_count      NUMBER;
  l_item_id          NUMBER;
  l_organization_id  NUMBER;
  l_request_date     NUMBER;
  l_ordered_quantity NUMBER;
  l_shipped_quantity NUMBER;
  l_item             VARCHAR2(240);
  l_organization_id1 NUMBER;
  l_shipment_date DATE;
  l_bo_qty            NUMBER;
  L_INVENTORY_ITEM_ID NUMBER;
  L_RELEASED_STATUS   Varchar2(10);
  L_ORDERED_QUANTITY_Y NUMBER;
  L_SHIPPED_QUANTITY_Y NUMBER;
  L_ITEM_Y             VARCHAR2(240);
  L_ORGANIZATION_ID1_Y NUMBER;
  L_SHIPMENT_DATE_Y DATE;
  l_bo_qty_y            NUMBER;
  L_INVENTORY_ITEM_ID_Y NUMBER;
  L_RELEASED_STATUS_Y   Varchar2(10);
  l_common_output_id  NUMBER;
  l_region_sum        VARCHAR2(240);
  l_dist_sum          VARCHAR2(240);
  l_buyer_sum         VARCHAR2(240);
  l_cat_sum           VARCHAR2(240);
  L_DPM_SUM           VARCHAR2(240);
  l_org_sum           VARCHAR2(240);
  l_stk_sum           VARCHAR2(240);
  l_comp_orders_sum   NUMBER;
  l_bo_orders_sum     NUMBER;
  l_total_orders_sum  NUMBER;
  l_comp_lines_sum    NUMBER;
  L_BO_LINES_SUM      NUMBER;
  L_TOTAL_LINES_SUM   NUMBER;
  l_bk_comp_lines_sum NUMBER;
  l_count             NUMBER;
BEGIN
  l_insert_recd_cnt:=1;
  l_condition_str  :=' and 1=1 ';
  IF p_region      IS NOT NULL THEN
    l_condition_str:= l_condition_str||' and isr.region in ('||xxeis.eis_rs_utility.get_param_values(p_region)||' )';
  END IF;
  IF p_district    IS NOT NULL THEN
    l_condition_str:= l_condition_str||' and isr.district in ('||xxeis.eis_rs_utility.get_param_values(p_district)||' )';
  END IF;
  IF p_location    IS NOT NULL THEN
    l_condition_str:= l_condition_str||' and isr.org in ('||xxeis.eis_rs_utility.get_param_values(p_location)||' )';
  END IF;
  IF p_location_list IS NOT NULL THEN
    xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list(p_process_id,p_location_list,'Org');
      l_location_list_vlaues:= ' AND EXISTS (SELECT 1  
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME  = '''||replace(p_location_list,'''','')||'''
                                         AND x.LIST_TYPE     =''Org''
                                         AND x.process_id    = '||P_PROCESS_ID||'
                                         AND isr.ORG = X.list_value) ';       
    fnd_file.put_line(fnd_file.log,'The location list is '||l_location_list_vlaues);
  ELSE
    l_location_list_vlaues :=' and 1=1 ';
  END IF;
  IF p_vendor_list IS NOT NULL THEN
  
    xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list(p_process_id,p_vendor_list,'Supplier');
    l_supplier_exists := ' AND EXISTS (SELECT 1                                                
                                         FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                              
                                        WHERE x.LIST_NAME     ='''||replace(p_vendor_list,'''','')||'''
                                          AND x.LIST_TYPE       =''Supplier''                                              
                                          AND x.process_id      = '||p_process_id||'                                              
                                          AND isr.vendor_num    = X.list_value)';
  ELSE
    l_supplier_exists :=' and 1=1 ';
  END IF;
  IF p_cat_class_list IS NOT NULL THEN
    xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list(p_process_id,p_cat_class_list,'Cat Class');
    l_catclass_exists:= ' AND EXISTS ( SELECT 1                                              
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                            
                                       WHERE x.LIST_NAME     ='''||replace(P_CAT_CLASS_LIST,'''','')||'''
                                         AND x.LIST_TYPE       =''Cat Class''                                            
                                         AND x.process_id      = '||p_process_id||'                                            
                                         AND isr.cat           = X.list_value)';
  ELSE
    l_catclass_exists :=' and 1=1 ';
  END IF;
  IF p_source_list IS NOT NULL THEN
    xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list(p_process_id,p_source_list,'Source');
    l_source_exists:= ' AND EXISTS (SELECT 1                                              
                                      FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                            
                                     WHERE x.LIST_NAME     ='''||replace(p_source_list,'''','')||'''                                            
                                       AND x.LIST_NAME       =''Source''                                            
                                       AND x.process_id      = '||p_process_id||'                                            
                                       AND isr.source        = X.list_value)';
  ELSE
    l_source_exists :=' and 1=1 ';
  END IF;
  IF p_item_list IS NOT NULL THEN
    xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list(p_process_id,p_item_list,'Item');
    l_item_exists:=' AND EXISTS (SELECT 1                                          
                                   FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x                                          
                                  WHERE x.LIST_NAME     ='''||replace(p_item_list,'''','')||'''
                                    AND x.LIST_TYPE      =''Item''                                         
                                    AND x.process_id     = '||p_process_id||'                                         
                                    AND isr.item_number  = X.list_value)';
  ELSE
    l_item_exists :=' and 1=1 ';
  END IF;
  
  BEGIN
   INSERT INTO XXEIS.EIS_XXWC_FILL_RATE_TMP_TBL(PROCESS_ID, HEADER_ID)     
   Select p_process_id, oh.header_id
     from OE_ORDER_HEADERS_all OH
    Where oh.order_type_id =1001
      AND exists(select 1 
                  from oe_order_lines_all OL,
                       mtl_system_items_b msi
                 where oh.header_id = ol.header_id
                   AND msi.organization_id   = ol.ship_from_org_id
                   AND msi.inventory_item_id = ol.inventory_item_id
                   AND msi.Item_type != 'SPECIAL'
                   AND trunc(actual_shipment_date) = trunc(decode(to_char(sysdate,'dy'),'mon',sysdate-3,'sun',sysdate-2,sysdate-1))
                   AND source_type_code != 'EXTERNAL'
                );    
  END;
  
  l_order_sql :=' INSERT INTO xxeis.EIS_XXWC_FILL_RATE_BK_TMP_TBL
           SELECT OL.INVENTORY_ITEM_ID, 
                  OL.SHIP_FROM_ORG_ID ORGANIZATION_ID,
                  oh.order_number,          
                  NVL(OL2.ORDERED_QUANTITY,0) + NVL(OL.ORDERED_QUANTITY,0)  ORDERED_QUANTITY,                      
                  nvl(ol2.shipped_quantity,0) shipped_quantity,
                  nvl(ol.ORDERED_QUANTITY,0)   BackOrdered_quantity,                  
                  ol.ordered_item item,                      
                  Hca.Account_Number Customer_number,
                  NVL(jrse.source_name,jrse.resource_name) salesrepname,
                  ol.salesrep_id , '||p_process_id ||'                  
             FROM OE_ORDER_LINES_all OL,
                  OE_ORDER_LINES_all OL2,
                  OE_ORDER_HEADERS_all OH,                 
                  HZ_CUST_ACCOUNTS HCA,
                  JTF_RS_SALESREPS JRS,
                  jtf_rs_resource_extns_vl jrse,
                  mtl_system_items_b msi,
                  XXEIS.EIS_XXWC_FILL_RATE_TMP_TBL tmp
            WHERE 1=1    
              AND OH.header_ID = ol.header_id
              AND oh.header_id              = tmp.header_id
              AND tmp.process_id            = '||p_process_id ||'
              AND ol.split_from_line_id     = ol2.line_id(+)
              AND oh.sold_to_org_id         = hca.cust_account_id
              AND ol.salesrep_id            = jrs.salesrep_id
              AND (ol.user_item_description  = ''BACKORDERED'' OR OL.user_item_description =''PARTIAL BACKORDERED'')
              AND OH.ORG_ID                 = JRS.ORG_ID
              AND JRS.RESOURCE_ID           = JRSE.RESOURCE_ID
              AND msi.organization_id       = ol.ship_from_org_id
              AND msi.inventory_item_id     = ol.inventory_item_id
              AND msi.Item_type            != ''SPECIAL''              
              AND ol.source_type_code      != ''EXTERNAL''
              AND msi.inventory_item_status_code != ''Intangible''
              AND oh.order_type_id          = 1001
              --AND trunc(ol.creation_date)   = ''25-FEB-2014''
              AND not Exists(Select 1 
                      from OE_ORDER_LINES_all ol1
                     where ol1.header_id= oh.header_id 
                       and trunc(actual_shipment_date) < trunc(decode(to_char(sysdate,''dy''),''mon'',sysdate-3,''sun'',sysdate-2,sysdate-1))) ';

    FND_FILE.PUT_LINE(FND_FILE.LOG,'l_Back_order_sql'||L_ORDER_SQL);
    FND_FILE.PUT_LINE(FND_FILE.log,'start time'||to_char(sysdate,'DD-MM-YY HH24:MI:SS'));
    
    EXECUTE IMMEDIATE L_ORDER_SQL;
/*  L_ORDER_SQL :=
  ' SELECT                  
          NVL(OL.ORDERED_QUANTITY,0) ORDERED_QUANTITY,                      
          nvl(ol.shipped_quantity,0) shipped_quantity,
          nvl(S.CYCLE_COUNT_QUANTITY,0)   BackOrdered_quantity,                  
          ol.ordered_item item,                      
          OL.INVENTORY_ITEM_ID,                      
          OL.SHIP_FROM_ORG_ID,    
          S.RELEASED_STATUS,
          trunc(ol.actual_shipment_date) shipment_date                      
    FROM OE_ORDER_LINES OL,
         OE_ORDER_HEADERS OH
         WSH_DELIVERY_DETAILS S                     
   WHERE 1=1    
     AND OH.header_ID = 
     and s.INVENTORY_ITEM_ID       = ol.INVENTORY_ITEM_ID
     and s.source_header_id        = ol.header_id                      
     AND S.SOURCE_LINE_ID          = OL.LINE_ID                     
     AND TRUNC(SCHEDULE_SHIP_DATE) = '25-FEB-2014'
     AND S.RELEASED_STATUS         IN (''B'',''R'')
     AND 
     --AND  EXISTS (SELECT 1 FROM wsh_delivery_details Y WHERE RELEASED_STATUS =''B'' AND Y.SOURCE_HEADER_ID=OL.HEADER_ID)
'
  ; */
  --dbms_output.put_line('Start Process');
    BEGIN
     l_order_sql :='
      INSERT
      INTO XXEIS.EIS_RS_COMMON_OUTPUTS
        (
          common_output_id ,
          process_id ,     --PROCESS_ID,
          varchar2_col1 ,  --bin_loc
          varchar2_col2 ,  -- bpm_rpm,
          varchar2_col3 ,  -- part_number,
          varchar2_col4 ,  -- source,
          varchar2_col5 ,  -- part_desc,
          number_col1,     --rvw
          number_col2,     --lt
          varchar2_col6 ,  -- cl,
          varchar2_col7 ,  -- stk,
          number_col3,     --min
          number_col4,     --max
          varchar2_col8 ,  -- amu,
          varchar2_col9 ,  -- lg,
          number_col5,     -- hit4_sales,
          number_col6,     -- hit6_sales,
          number_col7,     -- l_30_bo
          number_col8,     -- l_60_bo,
          number_col9,     -- l_90_bo,
          number_col10,    -- qty_ord,
          number_col11,    -- shipped,
          number_col12,    -- qty_bo,
          varchar2_col10 , -- oh,
          number_col13,    -- qoh,
          number_col14,    -- ON_ORD,
          number_col15,    -- avail,
          varchar2_col11 , -- buyer,
          number_col16,    -- JAN_SALES,
          number_col17,    --  FEB_SALES,
          number_col18,    --  MAR_SALES,
          number_col19,    --  APR_SALES,
          number_col20,    -- MAY_SALES,
          number_col21,    --   JUNE_SALES,
          number_col22,    --  JUL_SALES,
          number_col23,    --   AUG_SALES,
          number_col24,    --   SEP_SALES,
          number_col25,    --  OCT_SALES,
          number_col26,    --   nov_sales,
          NUMBER_COL27,    --   DEC_SALES
          VARCHAR2_COL14, --- report_type
          VARCHAR2_COL15, --- velocity class
          VARCHAR2_COL16,  --- location
          VARCHAR2_COL20, --- ORDER_NUMBER
          VARCHAR2_COL21, --- Customer_NUMBER
          VARCHAR2_COL22,  --- SALES REP NAME
          VARCHAR2_COL23,  --- BO_FLAG
          VARCHAR2_COL24, --- Region
          VARCHAR2_COL25,  --- District
          VARCHAR2_COL26,--- ORG_NAME
          NUMBER_COL40, --AVAIL2,
          VARCHAR2_COL30,  --- Vendor#
          VARCHAR2_COL31, -- Vendor_name
          NUMBER_COL41, --PPLT  --- Vendor#
          NUMBER_COL42, --PLT--- Vendor_name    
          NUMBER_COL43, -- CLT,  --- Vendor#
          NUMBER_COL44, --Safety_Stock,--- Vendor_name    
          NUMBER_COL45,-- Source_ORGS,
          VARCHAR2_COL32,-- UOM,
          VARCHAR2_COL33 ,--RES_QTY,
          VARCHAR2_COL34 ,--Cat,
          VARCHAR2_COL35 --ST          
        )
      SELECT xxeis.eis_rs_common_outputs_s.nextval, '||
        p_process_id ||',
        isr.bin_loc bin_loc,
        ffv.attribute1 bpm_rpm,
        isr.item_number part_number,
        isr.source source,
        isr.description part_desc,
        isr.pplt rvw,
        isr.plt lt,
        isr.cl cl,
        isr.stk_flag stk,
        isr.minn MIN,
        isr.maxn MAX,
        isr.amu amu,
        NULL lg,
        NVL(six_store_sale,0) +NVL(six_other_inv_sale,0) hit4_sales,
        NVL( ISR.HIT6_STORE_SALES,0)+NVL(ISR.HIT6_OTHER_INV_SALES,0) HIT6_SALES,
        xxeis.EIS_INV_XXWC_PRODFILL_RATE_PKG.GET_BO_AGING_BUCKET(30,NULL,isr.inventory_item_id,isr.organization_id,null) L_30_BO,
        xxeis.EIS_INV_XXWC_PRODFILL_RATE_PKG.GET_BO_AGING_BUCKET(60,NULL,isr.inventory_item_id,isr.organization_id,null)L_60_BO,
        xxeis.EIS_INV_XXWC_PRODFILL_RATE_PKG.get_bo_aging_bucket(90,NULL,isr.inventory_item_id,isr.organization_id,null) l_90_bo,
        oi.ORDERED_QUANTITY qty_ord,
        oi.shipped_quantity shipped,
        oi.BackOrdered_quantity qty_bo,
        NULL oh,
        isr.qoh qoh,
        isr.on_ord ON_ORD,
        isr.available avail,
        isr.buyer buyer,
        NVL(JAN_STORE_SALE,0)+NVL(JAN_OTHER_INV_SALE,0) JAN_SALES,
        NVL(FEB_STORE_SALE,0)+NVL(FEB_OTHER_INV_SALE,0) FEB_SALES,
        NVL(MAR_STORE_SALE,0)+NVL(MAR_OTHER_INV_SALE,0) MAR_SALES,
        NVL(APR_STORE_SALE,0)+NVL(APR_OTHER_INV_SALE,0) APR_SALES,
        NVL(MAY_STORE_SALE,0)+NVL(MAY_OTHER_INV_SALE,0) MAY_SALES,
        NVL(JUN_STORE_SALE,0)+NVL(JUN_OTHER_INV_SALE,0) JUNE_SALES,
        NVL(JUL_STORE_SALE,0)+NVL(JUL_OTHER_INV_SALE,0) JUL_SALES,
        NVL(AUG_STORE_SALE,0)+NVL(AUG_OTHER_INV_SALE,0) AUG_SALES,
        NVL(SEP_STORE_SALE,0)+NVL(SEP_OTHER_INV_SALE,0) SEP_SALES,
        NVL(OCT_STORE_SALE,0)+NVL(OCT_OTHER_INV_SALE,0) OCT_SALES,
        NVL(nov_store_sale,0)+NVL(nov_other_inv_sale,0) nov_sales,
        NVL(DEC_STORE_SALE,0)+NVL(DEC_OTHER_INV_SALE,0) DEC_SALES,
        ''Detail'' REPORT_TYPE,
        TO_CHAR(NULL) VELOCITY_CLASS,
        isr.org location,
        OI.order_number,
        OI.customer_number,
        OI.SALESREPNAME,
        ''Y'' BO_FLAG,
        isr.region,
        isr.district,
        isr.org_name,
        isr.avail2,
        isr.vendor_num,
        isr.vendor_name,
        isr.pplt,
        isr.plt,
        isr.clt,
        ISR.SS,
        ISR.SO,
        ISR.UOM,
        ISR.RES,
        ISR.CAT,
        ISR.ST        
    FROM xxeis.EIS_XXWC_FILL_RATE_BK_TMP_TBL oi,                                 
          XXEIS.EIS_XXWC_PO_ISR_TAB isr,
          fnd_flex_value_sets ffvs, 
          fnd_flex_values ffv  
    WHERE flex_value_set_name   = ''XXWC_DISTRICT''
      AND ffvs.flex_value_set_id = ffv.flex_value_set_id
      AND isr.district           = ffv.flex_value
      AND oi.process_id          = '|| p_process_id ||'
      '||l_condition_str || l_location_list_vlaues || l_supplier_exists ||l_catclass_exists ||l_source_exists ||l_item_exists ||' 
      AND isr.inventory_item_id = oi.INVENTORY_ITEM_ID
      AND ISR.ORGANIZATION_ID   = oi.ORGANIZATION_ID 
      ';
     
    FND_FILE.PUT_LINE(FND_FILE.LOG,'l_order_sql'||L_ORDER_SQL);
    FND_FILE.PUT_LINE(FND_FILE.log,'start time'||to_char(sysdate,'DD-MM-YY HH24:MI:SS'));
    
    EXECUTE IMMEDIATE L_ORDER_SQL;
    
  END;
  COMMIT;
  FND_FILE.PUT_LINE(FND_FILE.LOG,'After insert into EIS_RS_COMMON_OUTPUTS time'||TO_CHAR(SYSDATE,'DD-MM-YY HH24:MI:SS'));


  
BEGIN
    DBMS_OUTPUT.PUT_LINE('Start Sql Process');
    L_REGDIST_SQL :=
    'SELECT                
           NVL(TOTAL_ORDERS,0) - NVL(BACK_ORDERS,0) COMPELET_ORDERS,                
           NVL(BACK_ORDERS,0) BACK_ORDERS,                
           TOTAL_ORDERS total_orders,                
           NVL(total_lines,0) - NVL(BO_LINES,0)  COMPELET_LINES,                
           NVL(BO_LINES,0)  BACK_LINES,                
           NVL(total_lines,0) total_lines,                
           REGION,                
           district,
           NVL(total_lines,0) - NVL(BK_BO_LINES,0) BK_COMPELET_LINES
      from
           (select /*+ no_parallel(isr) no_parallel(tmp) no_parallel(msi) no_parallel(ol) no_parallel(oh) */
               COUNT(distinct (CASE WHEN (msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                          OL.HEADER_ID
                        ELSE
                         NULL
                       END
                       )) TOTAL_ORDERS,
                   COUNT(DISTINCT (CASE WHEN (( user_item_description =''BACKORDERED'' OR user_item_description =''PARTIAL BACKORDERED'' ) AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                          OL.HEADER_ID
                        ELSE
                         NULL
                       END
                       )) BACK_ORDERS,
                   COUNT(CASE WHEN (OL.SPLIT_FROM_LINE_ID IS NOT NULL  OR msi.Item_type = ''SPECIAL'' OR ol.source_type_code = ''EXTERNAL'' )THEN
                           NULL                           
                          ELSE 
                            ol.line_id
                         END 
                        )total_lines, 
                   COUNT(CASE WHEN ((user_item_description =''BACKORDERED''  OR user_item_description =''PARTIAL BACKORDERED'')AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                           ol.line_id
                          ELSE 
                            NULL
                         END 
                        )BO_LINES,   
                  COUNT(CASE WHEN ((OL.SPLIT_FROM_LINE_ID IS NOT NULL OR user_item_description =''BACKORDERED'') AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'' AND ((24*60*(oh.booked_date - ol.creation_date)) <3)) THEN
                           ol.line_id
                          ELSE 
                            NULL
                         END 
                        )BK_BO_LINES,                           
                   isr.region,
                   isr.district
              From XXEIS.EIS_XXWC_PO_ISR_TAB        isr,
                   XXEIS.EIS_XXWC_FILL_RATE_TMP_TBL tmp,
                   mtl_system_items_b_kfv           msi,   
                       oe_order_lines_all               ol,
                   oe_order_headers_all oh
             where 1=1
               AND isr.organization_id   = ol.ship_from_org_id
               and isr.inventory_item_id = ol.inventory_item_id
               AND msi.organization_id   = isr.organization_id
               AND msi.inventory_item_id = isr.inventory_item_id
               AND ol.flow_status_code   != ''CANCELLED''
               AND msi.Item_type            != ''SPECIAL''              
               AND ol.source_type_code      != ''EXTERNAL''
               AND msi.inventory_item_status_code != ''Intangible''
               AND tmp.header_id         = oh.header_id
               '||l_condition_str || l_location_list_vlaues || l_supplier_exists ||l_catclass_exists ||l_source_exists ||l_item_exists ||' 
               AND ol.header_id          = tmp.header_id
                 AND tmp.process_id        ='||p_process_id ||'
                 AND not Exists(Select 1 
                                from OE_ORDER_LINES_all ol1
                               where ol1.header_id= ol.header_id 
                                 and trunc(actual_shipment_date) < trunc(decode(to_char(sysdate,''dy''),''mon'',sysdate-3,''sun'',sysdate-2,sysdate-1))
                             )  
             group BY isr.region, isr.district
           ) 
             ' ;
             
    l_buyer_sql:=
    'SELECT             
           NVL(total_lines,0) - NVL(BO_LINES,0)  COMPELET_LINES,                
           NVL(BO_LINES,0)  BACK_LINES,                
           NVL(total_lines,0) total_lines,                
           bpm_rpm,             
           NVL(total_lines,0) - NVL(BK_BO_LINES,0) BK_COMPELET_LINES
      from
           (select /*+ no_parallel(isr) no_parallel(tmp) no_parallel(msi) no_parallel(ol) no_parallel(oh) */
            COUNT(CASE WHEN (OL.SPLIT_FROM_LINE_ID IS NOT NULL OR msi.Item_type = ''SPECIAL'' OR ol.source_type_code = ''EXTERNAL'' )THEN
                           NULL                           
                          ELSE 
                            ol.line_id
                         END 
                        )total_lines, 
                   COUNT(CASE WHEN ((user_item_description =''BACKORDERED'' OR user_item_description =''PARTIAL BACKORDERED'') AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                           ol.line_id
                          ELSE 
                            NULL
                         END 
                        )BO_LINES,         
                   COUNT(CASE WHEN ((OL.SPLIT_FROM_LINE_ID IS NOT NULL OR user_item_description =''BACKORDERED'') AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'' AND ((24*60*(oh.booked_date - ol.creation_date)) <3)) THEN
                           ol.line_id
                          ELSE 
                            NULL
                         END 
                        )BK_BO_LINES,                                                
                   ffv.attribute1 bpm_rpm
              From XXEIS.EIS_XXWC_PO_ISR_TAB        isr,
                   XXEIS.EIS_XXWC_FILL_RATE_TMP_TBL tmp,
                   mtl_system_items_b_kfv           msi,   
                   oe_order_lines_all               ol,                   
                   oe_order_headers_all oh,
                   fnd_flex_value_sets ffvs, 
                   fnd_flex_values ffv  
             where 1=1
               AND isr.organization_id   = ol.ship_from_org_id
               and isr.inventory_item_id = ol.inventory_item_id
               AND ol.flow_status_code   != ''CANCELLED''
               AND msi.organization_id   = isr.organization_id
               AND msi.inventory_item_id = isr.inventory_item_id               
               '||l_condition_str || l_location_list_vlaues || l_supplier_exists ||l_catclass_exists ||l_source_exists ||l_item_exists ||' 
               AND flex_value_set_name   = ''XXWC_DISTRICT''
               AND ffvs.flex_value_set_id = ffv.flex_value_set_id
               AND isr.district           = ffv.flex_value               
                 AND ol.header_id          = tmp.header_id
               AND msi.Item_type            != ''SPECIAL''              
               AND ol.source_type_code      != ''EXTERNAL''
               AND msi.inventory_item_status_code != ''Intangible''
               AND tmp.header_id         = oh.header_id
                 AND tmp.process_id        = '||p_process_id ||'
                 AND not Exists(Select 1 
                                from OE_ORDER_LINES_all ol1
                               where ol1.header_id= ol.header_id 
                                 and trunc(actual_shipment_date) < trunc(decode(to_char(sysdate,''dy''),''mon'',sysdate-3,''sun'',sysdate-2,sysdate-1))
                             )  
             group BY ffv.attribute1
           )' ; 
           
    L_SVELOCITY_SQL:=
    'SELECT             
           NVL(total_lines,0) - NVL(BO_LINES,0)  COMPELET_LINES,                
           NVL(BO_LINES,0)  BACK_LINES,                
           NVL(total_lines,0) total_lines,                
           bpm_rpm,
             CL,
           NVL(total_lines,0) - NVL(BK_BO_LINES,0) BK_COMPELET_LINES
      from
           (select /*+ no_parallel(isr) no_parallel(tmp) no_parallel(msi) no_parallel(ol) no_parallel(oh) */
                  COUNT(CASE WHEN (OL.SPLIT_FROM_LINE_ID IS NOT NULL OR msi.Item_type = ''SPECIAL'' OR ol.source_type_code = ''EXTERNAL'' )THEN
                           NULL                           
                          ELSE 
                            ol.line_id
                         END 
                        )total_lines, 
                   COUNT(CASE WHEN ((user_item_description =''BACKORDERED'' OR user_item_description =''PARTIAL BACKORDERED'') AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                           ol.line_id
                          ELSE 
                            NULL
                         END 
                        )BO_LINES,    
                   COUNT(CASE WHEN ((OL.SPLIT_FROM_LINE_ID IS NOT NULL OR user_item_description =''BACKORDERED'') AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'' AND ((24*60*(oh.booked_date - ol.creation_date)) <3)) THEN
                           ol.line_id
                          ELSE 
                            NULL
                         END 
                        )BK_BO_LINES,                            
                   isr.cl,
                   ffv.attribute1 bpm_rpm
              From XXEIS.EIS_XXWC_PO_ISR_TAB        isr,
                   XXEIS.EIS_XXWC_FILL_RATE_TMP_TBL tmp,
                    mtl_system_items_b_kfv           msi,   
                   oe_order_lines_all               ol,
                   oe_order_headers_all oh,
                   fnd_flex_value_sets ffvs, 
                   fnd_flex_values ffv  
             where 1=1
               AND isr.organization_id   = ol.ship_from_org_id
               and isr.inventory_item_id = ol.inventory_item_id
               AND msi.organization_id   = isr.organization_id
               AND msi.inventory_item_id = isr.inventory_item_id
               AND ol.flow_status_code   != ''CANCELLED''
               AND msi.Item_type            != ''SPECIAL''              
               AND ol.source_type_code      != ''EXTERNAL''
               AND msi.inventory_item_status_code != ''Intangible''
               AND flex_value_set_name   = ''XXWC_DISTRICT''
               AND ffvs.flex_value_set_id = ffv.flex_value_set_id
               AND isr.district           = ffv.flex_value
               '||l_condition_str || l_location_list_vlaues || l_supplier_exists ||l_catclass_exists ||l_source_exists ||l_item_exists ||'                  
                 AND ol.header_id          = tmp.header_id
               AND oh.header_id          = tmp.header_id
                 AND tmp.process_id        = '||p_process_id ||'
                 AND not Exists(Select 1 
                                from OE_ORDER_LINES_all ol1
                               where ol1.header_id= ol.header_id 
                                 and trunc(actual_shipment_date) < trunc(decode(to_char(sysdate,''dy''),''mon'',sysdate-3,''sun'',sysdate-2,sysdate-1))
                             )  
             group BY ffv.attribute1 ,isr.CL
           )'    ;
           
           
 L_STK_REGDIST_SQL :=
    'SELECT                
           NVL(TOTAL_ORDERS,0) - NVL(BACK_ORDERS,0) COMPELET_ORDERS,                
           NVL(BACK_ORDERS,0) BACK_ORDERS,                
           TOTAL_ORDERS total_orders,                
           NVL(total_lines,0) - NVL(BO_LINES,0)  COMPELET_LINES,                
           NVL(BO_LINES,0)  BACK_LINES,                
           NVL(total_lines,0) total_lines,                
           REGION,                
           district,           
           org,
           DPM_RPM,
           NVL(total_lines,0) - NVL(BK_BO_LINES,0) BK_COMPELET_LINES
      from
           (select /*+ no_parallel(isr) no_parallel(tmp) no_parallel(msi) no_parallel(ol) no_parallel(oh) */
               COUNT(distinct (CASE WHEN (msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                          OL.HEADER_ID
                        ELSE
                         NULL
                       END
                       )) TOTAL_ORDERS,
                   COUNT(DISTINCT (CASE WHEN ((user_item_description =''BACKORDERED'' OR user_item_description =''PARTIAL BACKORDERED'' ) AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                          OL.HEADER_ID
                        ELSE
                         NULL
                       END
                       )) BACK_ORDERS,
                   COUNT(CASE WHEN (OL.SPLIT_FROM_LINE_ID IS NOT NULL  OR msi.Item_type = ''SPECIAL'' OR ol.source_type_code = ''EXTERNAL'' )THEN
                           NULL                           
                          ELSE 
                            ol.line_id
                         END 
                        )total_lines, 
                   COUNT(CASE WHEN ((user_item_description =''BACKORDERED'' OR user_item_description =''PARTIAL BACKORDERED'')AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                           ol.line_id
                          ELSE 
                            NULL
                         END 
                        )BO_LINES,   
                  COUNT(CASE WHEN ((OL.SPLIT_FROM_LINE_ID IS NOT NULL OR user_item_description =''BACKORDERED'' OR user_item_description =''PARTIAL BACKORDERED'') AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'' AND ((24*60*(oh.booked_date - ol.creation_date)) <3)) THEN
                           ol.line_id
                          ELSE 
                            NULL
                         END 
                        )BK_BO_LINES,                           
                   isr.region,
                   isr.district,                   
                   isr.org,
                   ffv.attribute1 DPM_RPM
              From XXEIS.EIS_XXWC_PO_ISR_TAB        isr,
                   XXEIS.EIS_XXWC_FILL_RATE_TMP_TBL tmp,
                   mtl_system_items_b_kfv           msi,   
                       oe_order_lines_all               ol,
                   oe_order_headers_all oh,
                   fnd_flex_value_sets ffvs, 
                   fnd_flex_values ffv
             where 1=1
               AND isr.organization_id   = ol.ship_from_org_id
               and isr.inventory_item_id = ol.inventory_item_id
               AND msi.organization_id   = isr.organization_id
               AND msi.inventory_item_id = isr.inventory_item_id
               AND ol.flow_status_code   != ''CANCELLED''
               AND flex_value_set_name   = ''XXWC_DISTRICT''
               AND msi.Item_type            != ''SPECIAL''              
               AND ol.source_type_code      != ''EXTERNAL''
               AND msi.inventory_item_status_code != ''Intangible''
               AND ffvs.flex_value_set_id = ffv.flex_value_set_id
               AND isr.district           = ffv.flex_value
               AND tmp.header_id         = oh.header_id
               '||l_condition_str || l_location_list_vlaues || l_supplier_exists ||l_catclass_exists ||l_source_exists ||l_item_exists ||' 
               AND ol.header_id          = tmp.header_id
                 AND tmp.process_id        ='||p_process_id ||'
                 AND not Exists(Select 1 
                                from OE_ORDER_LINES_all ol1
                               where ol1.header_id= ol.header_id 
                                 and trunc(actual_shipment_date) < trunc(decode(to_char(sysdate,''dy''),''mon'',sysdate-3,''sun'',sysdate-2,sysdate-1))
                             )  
             group BY isr.org,isr.region, isr.district,ffv.attribute1
           ) 
             ' ;
             
L_LOC_SQL :=
    'SELECT                
           NVL(total_lines,0) - NVL(BO_LINES,0)  COMPELET_LINES,                
           NVL(BO_LINES,0)  BACK_LINES,                
           NVL(total_lines,0) total_lines,                
           REGION,                
           district,
           decode(stk_flag,''N'',''Non Stock'',''Stock'') stk_flag,
           org,
           DPM_RPM,
           NVL(total_lines,0) - NVL(BK_BO_LINES,0) BK_COMPELET_LINES
      from
           (select /*+ no_parallel(isr) no_parallel(tmp) no_parallel(msi) no_parallel(ol) no_parallel(oh) */
                  COUNT(distinct (CASE WHEN (msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                          OL.HEADER_ID
                        ELSE
                         NULL
                       END
                       )) TOTAL_ORDERS,
                   COUNT(DISTINCT (CASE WHEN ((user_item_description =''BACKORDERED'' OR user_item_description =''PARTIAL BACKORDERED'' ) AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                          OL.HEADER_ID
                        ELSE
                         NULL
                       END
                       )) BACK_ORDERS,
                   COUNT(CASE WHEN (OL.SPLIT_FROM_LINE_ID IS NOT NULL  OR msi.Item_type = ''SPECIAL'' OR ol.source_type_code = ''EXTERNAL'' )THEN
                           NULL                           
                          ELSE 
                            ol.line_id
                         END 
                        )total_lines, 
                   COUNT(CASE WHEN ((user_item_description =''BACKORDERED'' OR user_item_description =''PARTIAL BACKORDERED'')AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'') THEN
                           ol.line_id
                          ELSE 
                            NULL
                         END 
                        )BO_LINES,   
                  COUNT(CASE WHEN ((OL.SPLIT_FROM_LINE_ID IS NOT NULL OR user_item_description =''BACKORDERED'') AND msi.Item_type != ''SPECIAL'' AND ol.source_type_code != ''EXTERNAL'' AND ((24*60*(oh.booked_date - ol.creation_date)) <3)) THEN
                           ol.line_id
                          ELSE 
                            NULL
                         END 
                        )BK_BO_LINES,                           
                   isr.region,
                   isr.district,
                   isr.stk_flag,
                   isr.org,
                   ffv.attribute1 DPM_RPM
              From XXEIS.EIS_XXWC_PO_ISR_TAB        isr,
                   XXEIS.EIS_XXWC_FILL_RATE_TMP_TBL tmp,
                   mtl_system_items_b_kfv           msi,   
                       oe_order_lines_all               ol,
                   oe_order_headers_all oh,
                   fnd_flex_value_sets ffvs, 
                   fnd_flex_values ffv
             where 1=1
               AND isr.organization_id   = ol.ship_from_org_id
               and isr.inventory_item_id = ol.inventory_item_id
               AND msi.organization_id   = isr.organization_id
               AND msi.inventory_item_id = isr.inventory_item_id
               AND ol.flow_status_code   != ''CANCELLED''
               AND msi.Item_type            != ''SPECIAL''              
               AND ol.source_type_code      != ''EXTERNAL''
               AND msi.inventory_item_status_code != ''Intangible''
               AND flex_value_set_name   = ''XXWC_DISTRICT''
               AND ffvs.flex_value_set_id = ffv.flex_value_set_id
               AND isr.district           = ffv.flex_value
               AND tmp.header_id         = oh.header_id
               '||l_condition_str || l_location_list_vlaues || l_supplier_exists ||l_catclass_exists ||l_source_exists ||l_item_exists ||' 
               AND ol.header_id          = tmp.header_id
                 AND tmp.process_id        ='||p_process_id ||'
                 AND not Exists(Select 1 
                                from OE_ORDER_LINES_all ol1
                               where ol1.header_id= ol.header_id 
                                 and trunc(actual_shipment_date) < trunc(decode(to_char(sysdate,''dy''),''mon'',sysdate-3,''sun'',sysdate-2,sysdate-1))
                             )  
             group BY isr.org,isr.stk_flag,isr.region, isr.district,ffv.attribute1
           ) 
             ' ;
              
FND_FILE.PUT_LINE(FND_FILE.log,'L_REGDIST_SQL'||L_REGDIST_SQL);
    ---Start Summary Region and Dist ---
    OPEN l_ref_cursor FOR L_REGDIST_SQL;
    LOOP
      l_count:=0;
      --  DBMS_OUTPUT.PUT_LINE('After Inside Loop');
      fnd_file.put_line(fnd_file.log,'After Inside Loop');
      FETCH L_REF_CURSOR
      INTO L_COMP_ORDERS_SUM,
        L_BO_ORDERS_SUM ,
        l_total_orders_sum ,
        L_COMP_LINES_SUM,
        L_BO_LINES_SUM ,
        l_total_lines_sum,
        L_REGION_SUM,
        L_DIST_SUM,
        l_bk_comp_lines_sum;
      IF l_ref_cursor%rowcount=0 THEN
        fnd_file.put_line(fnd_file.log,'The error is '||SQLCODE||sqlerrm);
        --dbms_output.put_line('The error is '||sqlcode||sqlerrm);
        FND_FILE.PUT_LINE(FND_FILE.LOG,'the length is '||LENGTH(L_MAIN_SQL));
        --dbms_output.put_line('the length is '||length(l_main_sql));
      END IF;
      EXIT
    WHEN l_ref_cursor%notfound;
      DBMS_OUTPUT.PUT_LINE('l_region_sum is '||L_REGION_SUM);
      
      FND_FILE.PUT_LINE(FND_FILE.log,'Summary Data Insert start time'||to_char(sysdate,'DD-MM-YY HH24:MI:SS'));
      
      BEGIN
        INSERT
        INTO XXEIS.EIS_RS_COMMON_OUTPUTS
          (
            common_output_id ,
            process_id ,      --  PROCESS_ID,
            varchar2_col1 ,   --  bin_loc
            varchar2_col2 ,   --  bpm_rpm,
            varchar2_col3 ,   --  part_number,
            varchar2_col4 ,   --  source,
            varchar2_col5 ,   --  part_desc,
            number_col1,      --  rvw
            number_col2,      --  lt
            varchar2_col6 ,   --  cl,
            varchar2_col7 ,   --  stk,
            number_col3,      --  min
            number_col4,      --  max
            varchar2_col8 ,   --  amu,
            varchar2_col9 ,   --  lg,
            number_col5,      --  hit4_sales,
            number_col6,      --  hit6_sales,
            number_col7,      --  l_30_bo
            number_col8,      --  l_60_bo,
            number_col9,      --  l_90_bo,
            number_col10,     --  qty_ord,
            number_col11,     --  shipped,
            number_col12,     --  qty_bo,
            varchar2_col10 ,  --  oh,
            number_col13,     --  qoh,
            number_col14,     --  ON_ORD,
            number_col15,     --  avail,
            varchar2_col11 ,  --  buyer,
            varchar2_col12 ,  --  region,
            varchar2_col13 ,  --  dist,
            number_col16,     --  JAN_SALES,
            number_col17,     --  FEB_SALES,
            number_col18,     --  MAR_SALES,
            number_col19,     --  APR_SALES,
            number_col20,     --  MAY_SALES,
            number_col21,     --  JUNE_SALES,
            number_col22,     --  JUL_SALES,
            number_col23,     --  AUG_SALES,
            number_col24,     --  SEP_SALES,
            number_col25,     --  OCT_SALES,
            number_col26,     --  nov_sales,
            number_col27 ,    --  DEC_SALES
            number_col28 ,    --  comp_orders,
            number_col29 ,    --  bo_orders,
            number_col30 ,    --  total_orders,
            number_col31 ,    --  comp_lines,
            number_col32 ,    --  bo_lines,
            NUMBER_COL33 ,    --  total_lines
            VARCHAR2_COL14,  ---  report_type
            VARCHAR2_COL15 , --- velocity_class
            VARCHAR2_COL16 ,  --Location
            VARCHAR2_COL17,   -- SBUYER,
            NUMBER_COL34 ,    --  BL_COMP_LINES,
            NUMBER_COL35 ,    --  bl_bo_lines,
            NUMBER_COL36 ,    -- bl_TOTAL_LINES
            VARCHAR2_COL18 ,  --  SVELOCITY,
            NUMBER_COL37 ,    -- SVL_COMP_LINES,
            NUMBER_COL38 ,    --  SVL_BO_LINES,
            NUMBER_COL39 ,     --  SVl_TOTAL_LINES
            VARCHAR2_COL19,       --  branch  
            VARCHAR2_COL27,--- REG_SUM_DPM_RPM,
            NUMBER_COL46 -- REG_BK_COMP_LINES
          )
          VALUES
          (
            xxeis.eis_rs_common_outputs_s.nextval,
            P_PROCESS_ID,       --  PROCESS_ID,
            NULL ,              --  bin_loc
            NULL ,              --  bpm_rpm,
            NULL ,              --  part_number,
            NULL ,              --  source,
            NULL ,              --  part_desc,
            NULL ,              --  rvw
            NULL ,              --  lt
            NULL ,              --  cl,
            NULL ,              --  stk,
            NULL ,              --  min
            NULL ,              --  max
            NULL ,              --  amu,
            NULL ,              --  lg,
            NULL ,              --  hit4_sales,
            NULL ,              --  hit6_sales,
            NULL ,              --  l_30_bo
            NULL ,              --  l_60_bo,
            NULL ,              --  l_90_bo,
            NULL ,              --  qty_ord,
            NULL ,              --  shipped,
            NULL ,              --  qty_bo,
            NULL ,              --  oh,
            NULL ,              --  qoh,
            NULL ,              --  ON_ORD,
            NULL ,              --  avail,
            NULL ,              --  buyer,
            l_region_sum ,      --  region,
            l_dist_sum ,        --  dist,
            NULL ,              --  JAN_SALES,
            NULL ,              --  FEB_SALES,
            NULL ,              --  MAR_SALES,
            NULL ,              --  APR_SALES,
            NULL ,              --  MAY_SALES,
            NULL ,              --  JUNE_SALES,
            NULL ,              --  JUL_SALES,
            NULL ,              --  AUG_SALES,
            NULL ,              --  SEP_SALES,
            NULL ,              --  OCT_SALES,
            NULL ,              --  nov_sales,
            NULL ,              --  DEC_SALES
            l_comp_orders_sum,  --  comp_orders;
            l_bo_orders_sum ,   --  bo_orders;
            L_TOTAL_ORDERS_SUM, --  total_orders;
            L_COMP_LINES_SUM ,  --  comp_lines;
            L_BO_LINES_SUM ,    --  bo_lines;
            l_total_lines_sum , --  total_lines;
            'Summary Region',                -- report_type
            NULL,               -- velocity class
            null ,         --  Location
            NULL,               --Sbuyer
            NULL,               --  BL_COMP_LINES,
            NULL,               --  bl_bo_lines,
            NULL,               -- bl_TOTAL_LINES
            NULL ,              --  SVELOCITY,
            NULL ,              -- SVL_COMP_LINES,
            null ,              --  SVL_BO_LINES,
            null  ,              --  SVl_TOTAL_LINES
            NULL,       --  branch
            NULL,
            l_bk_comp_lines_sum
          ) ;
   END;
      L_COUNT:=L_COUNT+1;
    END LOOP;
    fnd_file.put_line
    (
      fnd_file.log,'L_COUNT'||L_COUNT
    )
    ;
    ---End Summary Order--------------
    ------Start Summary Buyer Pivot ---
    FND_FILE.PUT_LINE
    (
      FND_FILE.log,'L_BUYER_SQL'||L_BUYER_SQL
    )
    ;
    OPEN l_ref_cursor FOR L_BUYER_SQL;
    LOOP
      l_count:=0;
      --  DBMS_OUTPUT.PUT_LINE('After Inside Loop');
      fnd_file.put_line
      (
        fnd_file.log,'After Inside Loop'
      )
      ;
      FETCH L_REF_CURSOR
      INTO L_COMP_LINES_SUM,
        L_BO_LINES_SUM ,
        L_TOTAL_LINES_SUM,
        L_DPM_SUM,        
        l_bk_comp_lines_sum;
      IF l_ref_cursor%rowcount=0 THEN
        fnd_file.put_line(fnd_file.log,'The error is '||SQLCODE||sqlerrm);
        --dbms_output.put_line('The error is '||sqlcode||sqlerrm);
        FND_FILE.PUT_LINE(FND_FILE.LOG,'the length is '||LENGTH(L_MAIN_SQL));
        --dbms_output.put_line('the length is '||length(l_main_sql));
      END IF;
      EXIT
    WHEN l_ref_cursor%notfound;
      DBMS_OUTPUT.PUT_LINE('l_region_sum is '||L_REGION_SUM);
      BEGIN
        INSERT
        INTO XXEIS.EIS_RS_COMMON_OUTPUTS
          (
            common_output_id ,
            process_id ,      --  PROCESS_ID,
            varchar2_col1 ,   --  bin_loc
            varchar2_col2 ,   --  bpm_rpm,
            varchar2_col3 ,   --  part_number,
            varchar2_col4 ,   --  source,
            varchar2_col5 ,   --  part_desc,
            number_col1,      --  rvw
            number_col2,      --  lt
            varchar2_col6 ,   --  cl,
            varchar2_col7 ,   --  stk,
            number_col3,      --  min
            number_col4,      --  max
            varchar2_col8 ,   --  amu,
            varchar2_col9 ,   --  lg,
            number_col5,      --  hit4_sales,
            number_col6,      --  hit6_sales,
            number_col7,      --  l_30_bo
            number_col8,      --  l_60_bo,
            number_col9,      --  l_90_bo,
            number_col10,     --  qty_ord,
            number_col11,     --  shipped,
            number_col12,     --  qty_bo,
            varchar2_col10 ,  --  oh,
            number_col13,     --  qoh,
            number_col14,     --  ON_ORD,
            number_col15,     --  avail,
            varchar2_col11 ,  --  buyer,
            varchar2_col12 ,  --  region,
            varchar2_col13 ,  --  dist,
            number_col16,     --  JAN_SALES,
            number_col17,     --  FEB_SALES,
            number_col18,     --  MAR_SALES,
            number_col19,     --  APR_SALES,
            number_col20,     --  MAY_SALES,
            number_col21,     --  JUNE_SALES,
            number_col22,     --  JUL_SALES,
            number_col23,     --  AUG_SALES,
            number_col24,     --  SEP_SALES,
            number_col25,     --  OCT_SALES,
            number_col26,     --  nov_sales,
            number_col27 ,    --  DEC_SALES
            number_col28 ,    --  comp_orders,
            number_col29 ,    --  bo_orders,
            number_col30 ,    --  total_orders,
            number_col31 ,    --  comp_lines,
            number_col32 ,    --  bo_lines,
            NUMBER_COL33 ,    --  total_lines
            VARCHAR2_COL14,  ---  report_type
            VARCHAR2_COL15 , --- velocity_class
            VARCHAR2_COL16,   --         location,
            VARCHAR2_COL17,   -- SBUYER,
            NUMBER_COL34 ,    --  BL_COMP_LINES,
            NUMBER_COL35 ,    --  bl_bo_lines,
            NUMBER_COL36 ,    -- bl_TOTAL_LINES
            VARCHAR2_COL18 ,  --  SVELOCITY,
            NUMBER_COL37 ,    -- SVL_COMP_LINES,
            NUMBER_COL38 ,    --  SVL_BO_LINES,
            NUMBER_COL39 ,     --  SVl_TOTAL_LINES
            VARCHAR2_COL19,       --  branch
            VARCHAR2_COL28,       --  Buyer_DPM_SUM
            NUMBER_COL47 --    BY_BK_COMP_LINES
          )
          VALUES
          (
            xxeis.eis_rs_common_outputs_s.nextval,
            P_PROCESS_ID,      --  PROCESS_ID,
            NULL ,             --  bin_loc
            NULL ,             --  bpm_rpm,
            NULL ,             --  part_number,
            NULL ,             --  source,
            NULL ,             --  part_desc,
            NULL ,             --  rvw
            NULL ,             --  lt
            NULL ,             --  cl,
            NULL ,             --  stk,
            NULL ,             --  min
            NULL ,             --  max
            NULL ,             --  amu,
            NULL ,             --  lg,
            NULL ,             --  hit4_sales,
            NULL ,             --  hit6_sales,
            NULL ,             --  l_30_bo
            NULL ,             --  l_60_bo,
            NULL ,             --  l_90_bo,
            NULL ,             --  qty_ord,
            NULL ,             --  shipped,
            NULL ,             --  qty_bo,
            NULL ,             --  oh,
            NULL ,             --  qoh,
            NULL ,             --  ON_ORD,
            NULL ,             --  avail,
            NULL ,             --  buyer,
            NULL ,             --  region,
            NULL ,             --  dist,
            NULL ,             --  JAN_SALES,
            NULL ,             --  FEB_SALES,
            NULL ,             --  MAR_SALES,
            NULL ,             --  APR_SALES,
            NULL ,             --  MAY_SALES,
            NULL ,             --  JUNE_SALES,
            NULL ,             --  JUL_SALES,
            NULL ,             --  AUG_SALES,
            NULL ,             --  SEP_SALES,
            NULL ,             --  OCT_SALES,
            NULL ,             --  nov_sales,
            NULL ,             --  DEC_SALES
            NULL,              --  comp_orders;
            NULL ,             --  bo_orders;
            NULL,              --  total_orders;
            NULL ,             -- comp_lines;
            NULL ,             --  bo_lines;
            NULL ,             --  total_lines;
            'Summary Buyer',               --  report_type
            NULL,              --  velocity class
            NULL ,             --  Location
            NULL ,      --  sbuyer
            L_COMP_LINES_SUM , --  comp_lines;
            L_BO_LINES_SUM ,   --  bo_lines;
            L_TOTAL_LINES_SUM, --  total_lines;
            NULL ,             --  SVELOCITY,
            NULL ,             -- SVL_COMP_LINES,
            null ,             --  SVL_BO_LINES,
            null,               --  SVl_TOTAL_LINES
            NULL,       --  branch
            L_DPM_SUM,
            l_bk_comp_lines_sum
          );  
      END;
      L_COUNT:=L_COUNT+1;
    END LOOP;
    fnd_file.put_line
    (
      fnd_file.log,'L_COUNT'||L_COUNT
    )
    ;
    ------End Summary Buyer Pivot----------------- */
    -------Start Sales Velocity Pivot----
    FND_FILE.PUT_LINE
    (
      FND_FILE.log,'L_SVELOCITY_SQL'||L_SVELOCITY_SQL
    )
    ;
    OPEN l_ref_cursor FOR L_SVELOCITY_SQL;
    LOOP
      l_count:=0;
      --  DBMS_OUTPUT.PUT_LINE('After Inside Loop');
      fnd_file.put_line
      (
        fnd_file.log,'After Inside Loop'
      )
      ;
      FETCH L_REF_CURSOR
      INTO L_COMP_LINES_SUM,
        L_BO_LINES_SUM ,
        L_TOTAL_LINES_SUM,
        L_DPM_SUM,
        l_cat_sum,
        l_bk_comp_lines_sum;
      IF l_ref_cursor%rowcount=0 THEN
        fnd_file.put_line(fnd_file.log,'The error is '||SQLCODE||sqlerrm);
        --dbms_output.put_line('The error is '||sqlcode||sqlerrm);
        FND_FILE.PUT_LINE(FND_FILE.LOG,'the length is '||LENGTH(L_MAIN_SQL));
        --dbms_output.put_line('the length is '||length(l_main_sql));
      END IF;
      EXIT
    WHEN l_ref_cursor%notfound;
      DBMS_OUTPUT.PUT_LINE('l_region_sum is '||L_REGION_SUM);
      BEGIN
        INSERT
        INTO XXEIS.EIS_RS_COMMON_OUTPUTS
          (
            common_output_id ,
            process_id ,      --  PROCESS_ID,
            varchar2_col1 ,   --  bin_loc
            varchar2_col2 ,   --  bpm_rpm,
            varchar2_col3 ,   --  part_number,
            varchar2_col4 ,   --  source,
            varchar2_col5 ,   --  part_desc,
            number_col1,      --  rvw
            number_col2,      --  lt
            varchar2_col6 ,   --  cl,
            varchar2_col7 ,   --  stk,
            number_col3,      --  min
            number_col4,      --  max
            varchar2_col8 ,   --  amu,
            varchar2_col9 ,   --  lg,
            number_col5,      --  hit4_sales,
            number_col6,      --  hit6_sales,
            number_col7,      --  l_30_bo
            number_col8,      --  l_60_bo,
            number_col9,      --  l_90_bo,
            number_col10,     --  qty_ord,
            number_col11,     --  shipped,
            number_col12,     --  qty_bo,
            varchar2_col10 ,  --  oh,
            number_col13,     --  qoh,
            number_col14,     --  ON_ORD,
            number_col15,     --  avail,
            varchar2_col11 ,  --  buyer,
            varchar2_col12 ,  --  region,
            varchar2_col13 ,  --  dist,
            number_col16,     --  JAN_SALES,
            number_col17,     --  FEB_SALES,
            number_col18,     --  MAR_SALES,
            number_col19,     --  APR_SALES,
            number_col20,     --  MAY_SALES,
            number_col21,     --  JUNE_SALES,
            number_col22,     --  JUL_SALES,
            number_col23,     --  AUG_SALES,
            number_col24,     --  SEP_SALES,
            number_col25,     --  OCT_SALES,
            number_col26,     --  nov_sales,
            number_col27 ,    --  DEC_SALES
            number_col28 ,    --  comp_orders,
            number_col29 ,    --  bo_orders,
            number_col30 ,    --  total_orders,
            number_col31 ,    --  comp_lines,
            number_col32 ,    --  bo_lines,
            NUMBER_COL33 ,    --  total_lines
            VARCHAR2_COL14,  ---  report_type
            VARCHAR2_COL15 , --- velocity_class
            VARCHAR2_COL16 ,  --Location
            VARCHAR2_COL17,   -- SBUYER,
            NUMBER_COL34 ,    --  BL_COMP_LINES,
            NUMBER_COL35 ,    --  bl_bo_lines,
            NUMBER_COL36 ,    -- bl_TOTAL_LINES
            VARCHAR2_COL18 ,  --  SVELOCITY,
            NUMBER_COL37 ,    -- SVL_COMP_LINES,
            NUMBER_COL38 ,    --  SVL_BO_LINES,
            NUMBER_COL39  ,    --  SVl_TOTAL_LINES
            VARCHAR2_COL19,       --  branch
            VARCHAR2_COL29,       --  SV_DPM_SUM
            NUMBER_COL48 --svl_bK_comp_lines
          )
          VALUES
          (
            xxeis.eis_rs_common_outputs_s.nextval,
            P_PROCESS_ID,      --  PROCESS_ID,
            NULL ,             --  bin_loc
            NULL ,             --  bpm_rpm,
            NULL ,             --  part_number,
            NULL ,             --  source,
            NULL ,             --  part_desc,
            NULL ,             --  rvw
            NULL ,             --  lt
            NULL ,             --  cl,
            NULL ,             --  stk,
            NULL ,             --  min
            NULL ,             --  max
            NULL ,             --  amu,
            NULL ,             --  lg,
            NULL ,             --  hit4_sales,
            NULL ,             --  hit6_sales,
            NULL ,             --  l_30_bo
            NULL ,             --  l_60_bo,
            NULL ,             --  l_90_bo,
            NULL ,             --  qty_ord,
            NULL ,             --  shipped,
            NULL ,             --  qty_bo,
            NULL ,             --  oh,
            NULL ,             --  qoh,
            NULL ,             --  ON_ORD,
            NULL ,             --  avail,
            NULL ,             --  buyer,
            NULL ,             --  region,
            NULL ,             --  dist,
            NULL ,             --  JAN_SALES,
            NULL ,             --  FEB_SALES,
            NULL ,             --  MAR_SALES,
            NULL ,             --  APR_SALES,
            NULL ,             --  MAY_SALES,
            NULL ,             --  JUNE_SALES,
            NULL ,             --  JUL_SALES,
            NULL ,             --  AUG_SALES,
            NULL ,             --  SEP_SALES,
            NULL ,             --  OCT_SALES,
            NULL ,             --  nov_sales,
            NULL ,             --  DEC_SALES
            NULL,              --  comp_orders;
            NULL ,             --  bo_orders;
            NULL,              --  total_orders;
            NULL ,             --  comp_lines;
            NULL ,             --  bo_lines;
            NULL ,             --  total_lines;
            'Summary SV',               -- report_type
            NULL,              -- velocity class
            NULL ,             --  Location
            NULL,              --  Sbuyer
            NULL,              --  BL_COMP_LINES,
            NULL,              --  bl_bo_lines,
            NULL,              -- bl_TOTAL_LINES
            l_cat_sum ,        --  SVELOCITY,
            L_COMP_LINES_SUM , -- SVL_COMP_LINES,
            L_BO_LINES_SUM ,   --  SVL_BO_LINES,
            L_TOTAL_LINES_SUM,  --  SVl_TOTAL_LINES
            null,                  --  branch
            l_DPM_SUM,
            l_bk_comp_lines_sum  --SVL_BK_COMP_LINES
          );
      END;
      L_COUNT:=L_COUNT+1;
    END LOOP;
    fnd_file.put_line
    (
      fnd_file.log,'L_COUNT'||L_COUNT
    )
    ;
FND_FILE.PUT_LINE(FND_FILE.log,'L_STK_REGDIST_SQL'||L_STK_REGDIST_SQL);
    ---Start Summary By Loc ---
    OPEN l_ref_cursor FOR L_STK_REGDIST_SQL;
    LOOP
      l_count:=0;
      --  DBMS_OUTPUT.PUT_LINE('After Inside Loop');
      fnd_file.put_line(fnd_file.log,'After Inside Loop');
      FETCH L_REF_CURSOR
      INTO L_COMP_ORDERS_SUM,
        L_BO_ORDERS_SUM ,
        l_total_orders_sum ,
        L_COMP_LINES_SUM,
        L_BO_LINES_SUM ,
        l_total_lines_sum,
        L_REGION_SUM,
        L_DIST_SUM,               
        l_org_Sum,
        l_DPM_SUM,
        l_bk_comp_lines_sum;
      IF l_ref_cursor%rowcount=0 THEN
        fnd_file.put_line(fnd_file.log,'The error is '||SQLCODE||sqlerrm);
        --dbms_output.put_line('The error is '||sqlcode||sqlerrm);
        FND_FILE.PUT_LINE(FND_FILE.LOG,'the length is '||LENGTH(L_MAIN_SQL));
        --dbms_output.put_line('the length is '||length(l_main_sql));
      END IF;
      EXIT
    WHEN l_ref_cursor%notfound;
      DBMS_OUTPUT.PUT_LINE('l_region_sum is '||L_REGION_SUM);
      
      FND_FILE.PUT_LINE(FND_FILE.log,'Summary Data Insert start time'||to_char(sysdate,'DD-MM-YY HH24:MI:SS'));
      
      BEGIN
        INSERT
        INTO XXEIS.EIS_RS_COMMON_OUTPUTS
          (
            common_output_id ,
            process_id ,      --  PROCESS_ID,
            varchar2_col1 ,   --  bin_loc
            varchar2_col2 ,   --  bpm_rpm,
            varchar2_col3 ,   --  part_number,
            varchar2_col4 ,   --  source,
            varchar2_col5 ,   --  part_desc,
            number_col1,      --  rvw
            number_col2,      --  lt
            varchar2_col6 ,   --  cl,
            varchar2_col7 ,   --  stk,
            number_col3,      --  min
            number_col4,      --  max
            varchar2_col8 ,   --  amu,
            varchar2_col9 ,   --  lg,
            number_col5,      --  hit4_sales,
            number_col6,      --  hit6_sales,
            number_col7,      --  l_30_bo
            number_col8,      --  l_60_bo,
            number_col9,      --  l_90_bo,
            number_col10,     --  qty_ord,
            number_col11,     --  shipped,
            number_col12,     --  qty_bo,
            varchar2_col10 ,  --  oh,
            number_col13,     --  qoh,
            number_col14,     --  ON_ORD,
            number_col15,     --  avail,
            varchar2_col11 ,  --  buyer,
            varchar2_col12 ,  --  region,
            varchar2_col13 ,  --  dist,
            number_col16,     --  JAN_SALES,
            number_col17,     --  FEB_SALES,
            number_col18,     --  MAR_SALES,
            number_col19,     --  APR_SALES,
            number_col20,     --  MAY_SALES,
            number_col21,     --  JUNE_SALES,
            number_col22,     --  JUL_SALES,
            number_col23,     --  AUG_SALES,
            number_col24,     --  SEP_SALES,
            number_col25,     --  OCT_SALES,
            number_col26,     --  nov_sales,
            number_col27 ,    --  DEC_SALES
            number_col28 ,    --  comp_orders,
            number_col29 ,    --  bo_orders,
            number_col30 ,    --  total_orders,
            number_col31 ,    --  comp_lines,
            number_col32 ,    --  bo_lines,
            NUMBER_COL33 ,    --  total_lines
            VARCHAR2_COL14,  ---  report_type
            VARCHAR2_COL15 , --- velocity_class
            VARCHAR2_COL16 ,  --Location
            VARCHAR2_COL17,   -- SBUYER,
            NUMBER_COL34 ,    --  BL_COMP_LINES,
            NUMBER_COL35 ,    --  bl_bo_lines,
            NUMBER_COL36 ,    -- bl_TOTAL_LINES
            VARCHAR2_COL18 ,  --  SVELOCITY,
            NUMBER_COL37 ,    -- SVL_COMP_LINES,
            NUMBER_COL38 ,    --  SVL_BO_LINES,
            NUMBER_COL39 ,     --  SVl_TOTAL_LINES
            VARCHAR2_COL19,       --  branch  
            VARCHAR2_COL27,--- REG_SUM_DPM_RPM,
            NUMBER_COL46, -- REG_BK_COMP_LINES
            NUMBER_COL49,-- LOC_COMP_ORDERS,
            NUMBER_COL50, -- LOC_BO_ORDERS,
            NUMBER_COL51, -- LOC_TOT_ORDERS,
            NUMBER_COL52, -- LOC_COMP_LINES,
            NUMBER_COL53, -- LOC_BO_LINES,
            NUMBER_COL54,-- LOC_TOT_LINES,
            NUMBER_COL55,-- LOC_BK_COMP_LINES,
            VARCHAR2_COL36,-- LOC_REG_SUM,
            VARCHAR2_COL37,-- LOC_DIST_SUM,
            VARCHAR2_COL38,-- LOC_ORG_SUM,
            VARCHAR2_COL39,-- LOC_STK_SUM,
            VARCHAR2_COL40-- LOC_DPM_RPM_SUM
          )
          VALUES
          (
            xxeis.eis_rs_common_outputs_s.nextval,
            P_PROCESS_ID,       --  PROCESS_ID,
            NULL ,              --  bin_loc
            NULL ,              --  bpm_rpm,
            NULL ,              --  part_number,
            NULL ,              --  source,
            NULL ,              --  part_desc,
            NULL ,              --  rvw
            NULL ,              --  lt
            NULL ,              --  cl,
            NULL ,              --  stk,
            NULL ,              --  min
            NULL ,              --  max
            NULL ,              --  amu,
            NULL ,              --  lg,
            NULL ,              --  hit4_sales,
            NULL ,              --  hit6_sales,
            NULL ,              --  l_30_bo
            NULL ,              --  l_60_bo,
            NULL ,              --  l_90_bo,
            NULL ,              --  qty_ord,
            NULL ,              --  shipped,
            NULL ,              --  qty_bo,
            NULL ,              --  oh,
            NULL ,              --  qoh,
            NULL ,              --  ON_ORD,
            NULL ,              --  avail,
            NULL ,              --  buyer,
            NULL ,      --  region,
            NULL ,        --  dist,
            NULL ,              --  JAN_SALES,
            NULL ,              --  FEB_SALES,
            NULL ,              --  MAR_SALES,
            NULL ,              --  APR_SALES,
            NULL ,              --  MAY_SALES,
            NULL ,              --  JUNE_SALES,
            NULL ,              --  JUL_SALES,
            NULL ,              --  AUG_SALES,
            NULL ,              --  SEP_SALES,
            NULL ,              --  OCT_SALES,
            NULL ,              --  nov_sales,
            NULL ,              --  DEC_SALES
            NULL,  --  comp_orders;
            NULL ,   --  bo_orders;
            NULL, --  total_orders;
            NULL ,  --  comp_lines;
            NULL ,    --  bo_lines;
            NULL , --  total_lines;
            'Summary Loc',                -- report_type
            NULL,               -- velocity class
            null ,         --  Location
            NULL,               --Sbuyer
            NULL,               --  BL_COMP_LINES,
            NULL,               --  bl_bo_lines,
            NULL,               -- bl_TOTAL_LINES
            NULL ,              --  SVELOCITY,
            NULL ,              -- SVL_COMP_LINES,
            null ,              --  SVL_BO_LINES,
            null  ,              --  SVl_TOTAL_LINES
            NULL,       --  branch
            NULL,
            NULL,        
            L_COMP_ORDERS_SUM,-- LOC_COMP_ORDERS,
            L_BO_ORDERS_SUM, -- LOC_BO_ORDERS,
            l_total_orders_sum, -- LOC_TOT_ORDERS,
            L_COMP_LINES_SUM, -- LOC_COMP_LINES,
            L_BO_LINES_SUM, -- LOC_BO_LINES,
            l_total_lines_sum,-- LOC_TOT_LINES,
            l_bk_comp_lines_sum,-- LOC_BK_COMP_LINES,
            L_REGION_SUM,-- LOC_REG_SUM,
            L_DIST_SUM,-- LOC_DIST_SUM,
            l_org_Sum,-- LOC_ORG_SUM,
            NULL,-- LOC_STK_SUM,
            l_DPM_SUM-- LOC_DPM_RPM_SUM
            
          ) ;          
        
   END;
      L_COUNT:=L_COUNT+1;
    END LOOP;
    fnd_file.put_line
    (
      fnd_file.log,'L_COUNT'||L_COUNT
    )
    ;
    ---End Summary BY Loc--------------   
FND_FILE.PUT_LINE(FND_FILE.log,'L_LOC_SQL'||L_LOC_SQL);
    ---Start Summary By Loc ---
    OPEN l_ref_cursor FOR L_LOC_SQL;
    LOOP
      l_count:=0;
      --  DBMS_OUTPUT.PUT_LINE('After Inside Loop');
      fnd_file.put_line(fnd_file.log,'After Inside Loop');
      FETCH L_REF_CURSOR
      INTO  L_COMP_LINES_SUM,
        L_BO_LINES_SUM ,
        l_total_lines_sum,
        L_REGION_SUM,
        L_DIST_SUM,  
        l_stk_sum,
        l_org_Sum,
        l_DPM_SUM,
        l_bk_comp_lines_sum;
      IF l_ref_cursor%rowcount=0 THEN
        fnd_file.put_line(fnd_file.log,'The error is '||SQLCODE||sqlerrm);
        --dbms_output.put_line('The error is '||sqlcode||sqlerrm);
        FND_FILE.PUT_LINE(FND_FILE.LOG,'the length is '||LENGTH(L_LOC_SQL));
        --dbms_output.put_line('the length is '||length(l_main_sql));
      END IF;
      EXIT
    WHEN l_ref_cursor%notfound;
      DBMS_OUTPUT.PUT_LINE('l_region_sum is '||L_REGION_SUM);
      
      FND_FILE.PUT_LINE(FND_FILE.log,'Summary Data Insert start time'||to_char(sysdate,'DD-MM-YY HH24:MI:SS'));
      
      BEGIN
        INSERT
        INTO XXEIS.EIS_RS_COMMON_OUTPUTS
          (
            common_output_id ,
            process_id ,      --  PROCESS_ID,
            varchar2_col1 ,   --  bin_loc
            varchar2_col2 ,   --  bpm_rpm,
            varchar2_col3 ,   --  part_number,
            varchar2_col4 ,   --  source,
            varchar2_col5 ,   --  part_desc,
            number_col1,      --  rvw
            number_col2,      --  lt
            varchar2_col6 ,   --  cl,
            varchar2_col7 ,   --  stk,
            number_col3,      --  min
            number_col4,      --  max
            varchar2_col8 ,   --  amu,
            varchar2_col9 ,   --  lg,
            number_col5,      --  hit4_sales,
            number_col6,      --  hit6_sales,
            number_col7,      --  l_30_bo
            number_col8,      --  l_60_bo,
            number_col9,      --  l_90_bo,
            number_col10,     --  qty_ord,
            number_col11,     --  shipped,
            number_col12,     --  qty_bo,
            varchar2_col10 ,  --  oh,
            number_col13,     --  qoh,
            number_col14,     --  ON_ORD,
            number_col15,     --  avail,
            varchar2_col11 ,  --  buyer,
            varchar2_col12 ,  --  region,
            varchar2_col13 ,  --  dist,
            number_col16,     --  JAN_SALES,
            number_col17,     --  FEB_SALES,
            number_col18,     --  MAR_SALES,
            number_col19,     --  APR_SALES,
            number_col20,     --  MAY_SALES,
            number_col21,     --  JUNE_SALES,
            number_col22,     --  JUL_SALES,
            number_col23,     --  AUG_SALES,
            number_col24,     --  SEP_SALES,
            number_col25,     --  OCT_SALES,
            number_col26,     --  nov_sales,
            number_col27 ,    --  DEC_SALES
            number_col28 ,    --  comp_orders,
            number_col29 ,    --  bo_orders,
            number_col30 ,    --  total_orders,
            number_col31 ,    --  comp_lines,
            number_col32 ,    --  bo_lines,
            NUMBER_COL33 ,    --  total_lines
            VARCHAR2_COL14,  ---  report_type
            VARCHAR2_COL15 , --- velocity_class
            VARCHAR2_COL16 ,  --Location
            VARCHAR2_COL17,   -- SBUYER,
            NUMBER_COL34 ,    --  BL_COMP_LINES,
            NUMBER_COL35 ,    --  bl_bo_lines,
            NUMBER_COL36 ,    -- bl_TOTAL_LINES
            VARCHAR2_COL18 ,  --  SVELOCITY,
            NUMBER_COL37 ,    -- SVL_COMP_LINES,
            NUMBER_COL38 ,    --  SVL_BO_LINES,
            NUMBER_COL39 ,     --  SVl_TOTAL_LINES
            VARCHAR2_COL19,       --  branch  
            VARCHAR2_COL27,--- REG_SUM_DPM_RPM,
            NUMBER_COL46, -- REG_BK_COMP_LINES
            NUMBER_COL49,-- LOC_COMP_ORDERS,
            NUMBER_COL50, -- LOC_BO_ORDERS,
            NUMBER_COL51, -- LOC_TOT_ORDERS,
            NUMBER_COL56, -- LOC_COMP_LINES,
            NUMBER_COL57, -- LOC_BO_LINES,
            NUMBER_COL58,-- LOC_TOT_LINES,
            NUMBER_COL59,-- LOC_BK_COMP_LINES,
            VARCHAR2_COL41,-- LOC_REG_SUM,
            VARCHAR2_COL42,-- LOC_DIST_SUM,
            VARCHAR2_COL43,-- LOC_ORG_SUM,
            VARCHAR2_COL44,-- LOC_STK_SUM,
            VARCHAR2_COL45-- LOC_DPM_RPM_SUM
          )
          VALUES
          (
            xxeis.eis_rs_common_outputs_s.nextval,
            P_PROCESS_ID,       --  PROCESS_ID,
            NULL ,              --  bin_loc
            NULL ,              --  bpm_rpm,
            NULL ,              --  part_number,
            NULL ,              --  source,
            NULL ,              --  part_desc,
            NULL ,              --  rvw
            NULL ,              --  lt
            NULL ,              --  cl,
            NULL ,              --  stk,
            NULL ,              --  min
            NULL ,              --  max
            NULL ,              --  amu,
            NULL ,              --  lg,
            NULL ,              --  hit4_sales,
            NULL ,              --  hit6_sales,
            NULL ,              --  l_30_bo
            NULL ,              --  l_60_bo,
            NULL ,              --  l_90_bo,
            NULL ,              --  qty_ord,
            NULL ,              --  shipped,
            NULL ,              --  qty_bo,
            NULL ,              --  oh,
            NULL ,              --  qoh,
            NULL ,              --  ON_ORD,
            NULL ,              --  avail,
            NULL ,              --  buyer,
            NULL ,      --  region,
            NULL ,        --  dist,
            NULL ,              --  JAN_SALES,
            NULL ,              --  FEB_SALES,
            NULL ,              --  MAR_SALES,
            NULL ,              --  APR_SALES,
            NULL ,              --  MAY_SALES,
            NULL ,              --  JUNE_SALES,
            NULL ,              --  JUL_SALES,
            NULL ,              --  AUG_SALES,
            NULL ,              --  SEP_SALES,
            NULL ,              --  OCT_SALES,
            NULL ,              --  nov_sales,
            NULL ,              --  DEC_SALES
            NULL,  --  comp_orders;
            NULL ,   --  bo_orders;
            NULL, --  total_orders;
            NULL ,  --  comp_lines;
            NULL ,    --  bo_lines;
            NULL , --  total_lines;
            'Summary Stock',                -- report_type
            NULL,               -- velocity class
            null ,         --  Location
            NULL,               --Sbuyer
            NULL,               --  BL_COMP_LINES,
            NULL,               --  bl_bo_lines,
            NULL,               -- bl_TOTAL_LINES
            NULL ,              --  SVELOCITY,
            NULL ,              -- SVL_COMP_LINES,
            null ,              --  SVL_BO_LINES,
            null  ,              --  SVl_TOTAL_LINES
            NULL,       --  branch
            NULL,
            NULL,        
            NULL,-- LOC_COMP_ORDERS,
            NULL, -- LOC_BO_ORDERS,
            NULL, -- LOC_TOT_ORDERS,
            L_COMP_LINES_SUM, -- LOC_COMP_LINES,
            L_BO_LINES_SUM, -- LOC_BO_LINES,
            l_total_lines_sum,-- LOC_TOT_LINES,
            l_bk_comp_lines_sum,-- LOC_BK_COMP_LINES,
            L_REGION_SUM,-- LOC_REG_SUM,
            L_DIST_SUM,-- LOC_DIST_SUM,
            l_org_Sum,-- LOC_ORG_SUM,
            l_STK_SUM,-- LOC_STK_SUM,
            l_DPM_SUM-- LOC_DPM_RPM_SUM            
          ) ;          
        
   END;
      L_COUNT:=L_COUNT+1;
    END LOOP;
    fnd_file.put_line
    (
      fnd_file.log,'L_COUNT'||L_COUNT
    )
    ;
    ---End Summary BY Loc--------------   
        
    COMMIT;
  END;
  
  FND_FILE.PUT_LINE(FND_FILE.LOG,'After Summary insert end time'||TO_CHAR(SYSDATE,'DD-MM-YY HH24:MI:SS'));
  
EXCEPTION
WHEN no_data_found THEN
  fnd_file.put_line
  (
    fnd_file.log,'The Error is '||SQLCODE||sqlerrm
  )
  ;
  -- dbms_output.put_line('The Error is outer expection no '||sqlcode||sqlerrm);
  --    raise;
WHEN OTHERS THEN
  fnd_file.put_line
  (
    fnd_file.log, 'The Error is '||SQLCODE||sqlerrm
  )
  ;
  --     dbms_output.put_line('The Error is outer exp others '||sqlcode||sqlerrm);
END;

PROCEDURE LOAD_ORDER_INFO
    (
    p_process_id            IN NUMBER   
    )
IS
BEGIN
   g_process_id := p_process_id;
   INSERT INTO XXEIS.EIS_XXWC_FILL_RATE_TMP_TBL(PROCESS_ID, HEADER_ID)     
   Select p_process_id, oh.header_id
     from OE_ORDER_HEADERS_all OH
    Where oh.order_type_id =1001
      AND exists(select 1 
                  from oe_order_lines_all OL,
                       mtl_system_items_b msi
                 where oh.header_id = ol.header_id
                   AND msi.organization_id   = ol.ship_from_org_id
                   AND msi.inventory_item_id = ol.inventory_item_id
                   AND msi.Item_type != 'SPECIAL'
                   AND trunc(actual_shipment_date) = trunc(decode(to_char(sysdate,'dy'),'mon',sysdate-3,'sun',sysdate-2,sysdate-1))
                   AND source_type_code != 'EXTERNAL'
                );   
 EXCEPTION 
 WHEN OTHERS THEN
     fnd_file.put_line
  (
    fnd_file.log,'The Error is '||SQLCODE||sqlerrm
  );
 END;
  
   FUNCTION get_process_id
      RETURN number
   IS
   BEGIN
      RETURN g_process_id;
   --FND_FILE.PUT_LINE(FND_FILE.LOG,'Get date'||G_DATE_FROM);
   EXCEPTION
      WHEN OTHERS
      THEN
         --FND_FILE.PUT_LINE(FND_FILE.LOG,'Error Message'||SQLCODE||SQLERRM);
         RETURN NULL;
   END;

END EIS_INV_XXWC_PRODFILL_RATE_PKG;
/