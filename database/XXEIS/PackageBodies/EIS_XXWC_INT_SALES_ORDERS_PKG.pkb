CREATE OR REPLACE PACKAGE BODY    XXEIS.EIS_XXWC_INT_SALES_ORDERS_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  --TMS#20160418-00106   by Pramod on 04-16-2016
--//
--// Object Usage 				:: This Object Referred by "Internal Sales Orders Report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_INT_SALES_ORDERS_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod 	04/16/2016   		Initial Build --TMS#20160418-00106   by Pramod on 04-16-2016
--// 1.1     Siva	 	05/07/2016  		TMS#20160621-00138
--// 1.2	 Siva		14-Mar-2017		    TMS#20170209-00052 
--//============================================================================

  procedure process_int_sales_orders(
        p_process_id              in number,
        p_req_appr_date_from      in date,
        p_req_appr_date_to        in date,
        p_order_type              in varchar2,
        p_schedule_ship_date_from in date,
        p_shipping_method         in varchar2,
        p_ship_from_org           in varchar2,
        p_schedule_ship_date_to   in date,
        p_order_line_status       in varchar2,
        p_order_header_status     in varchar2,
        p_payment_terms           in varchar2,
        p_district                in varchar2,
        p_region                  in varchar2,
        p_location_list           in varchar2,
        p_ship_to_org             in varchar2,
        p_req_created_by          in number      )  as
--//============================================================================
--//
--// Object Name         :: process_int_sales_orders  
--//
--// Object Usage 		 :: This Object Referred by "Internal Sales Orders Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_OM_INT_OPEN_ORDERS_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/16/2016   Initial Build --TMS#20160418-00106   by Pramod on 04-16-2016
--// 1.1     Siva	 	05/07/2016  		TMS#20160621-00138
--// 1.2	 Siva		14-Mar-2017		    TMS#20170209-00052 
--//============================================================================
 
type INT_ORDER_REC is RECORD
  ( EMPLOYEE_ID 		 number,
    header_id        number,
    last_update_date 	date,
    description     varchar2(240),
    segment1        varchar2(20)
  );
 type int_order_rec_tab is table of int_order_rec index by binary_integer;  
  req_appr_date_tab 	  int_order_rec_tab;
  
type int_sales_order_rec
is
  record
  (
    PROCESS_ID           number,
    WAREHOUSE            varchar2(30) ,
    sales_person_name    varchar2(3600) ,
    created_by           varchar2(2400) ,
    ORDER_NUMBER         number ,
    line_number          varchar2(810) ,
    ordered_date         date ,
    line_creation_date   date ,
    order_type           varchar2(300) ,
    QUOTE_NUMBER         number ,
    ORDER_LINE_STATUS    varchar2(24000) ,
    BACKORDER_QTY        NUMBER ,
    DISTRICT             varchar2(2150) ,
    region               varchar2(2150) ,
    ship_qty             number ,
    ORDER_HEADER_STATUS  varchar2(280) ,
    customer_number      varchar2(230) ,
    CUSTOMER_NAME        varchar2(2360) ,
    customer_job_name    varchar2(240) ,
    order_amount         number ,
    SHIPPING_METHOD      varchar2(280) ,
    ship_to_city         varchar2(260) ,
    zip_code             varchar2(260) ,
    schedule_ship_date   date ,
    HEADER_STATUS        varchar2(230) ,
    item_number          varchar2(240) ,
    item_description     varchar2(2240) ,
    QTY                  number ,
    payment_terms        varchar2(215) ,
    ship_to_org          varchar2(23) ,
    order_header_id      number ,
    ORDER_LINE_ID        number ,
    CUST_ACCOUNT_ID      number,
    cust_acct_site_id    number ,
    SALESREP_ID          number ,
    party_id             number,
    transaction_type_id  number ,
    MTP_ORGANIZATION_ID  number ,
    HZCS_SHIP_TO_STE_ID  number,
    HZPS_SHP_TO_PRT_ID   number ,
    REQUISITION_NUMBER   varchar2(220) ,
    po_req_created_by    varchar2(2240) ,
    po_req_created_by_id number ,
	req_appr_date        date,		--added for version 1.2
    bin1				         varchar2(100), --added for version 1.2
    bin2				         varchar2(100),  --added for version 1.2
    bin3				         varchar2(100));  --added for version 1.2
  
  type int_sales_order_rec_tab is table of int_sales_order_rec index by binary_integer;  
  int_sales_order_dtls 	  int_sales_order_rec_tab;

  
   l_ref_cursor1          cursor_type4;
   l_ref_cursor2          cursor_type4;
   

main_sql varchar2(32000) :='SELECT 
max(fu1.employee_id) employee_id,
max(oha.header_id) header_id,
max(prha.last_update_date) last_update_date,
max(fu1.description) description,
max(prha.segment1) segment1
              FROM apps.po_requisition_headers prha
                  ,apps.po_requisition_lines prla
                  ,apps.oe_order_headers oha
                  ,apps.fnd_user fu1
             WHERE     prha.requisition_header_id = prla.requisition_header_id
                   AND oha.orig_sys_document_ref = prha.segment1
                   AND oha.order_type_id = 1011                                            
                   AND prha.authorization_status = ''APPROVED''
                   AND prha.created_by = fu1.user_id
                   AND prha.transferred_to_oe_flag = ''Y''
                   AND prla.transferred_to_oe_flag = ''Y''
                   and prha.type_lookup_code = ''INTERNAL''
                   and prha.last_update_date >= '''||p_req_appr_date_from||''' 
                   and prha.last_update_date<= '''||p_req_appr_date_to||'''
                   group by fu1.employee_id,oha.header_id,prha.last_update_date,fu1.description,prha.segment1 ';
                   
                   
                   
                   
l_sql                   varchar2(32000);
l_where_order_type      varchar2(32000);
l_where_ship_date_from  varchar2(32000);
l_where_shipping_method varchar2(32000);
l_where_ship_from_org   varchar2(32000);
l_where_ship_date_to    varchar2(32000);
l_where_line_status     varchar2(32000);
l_where_header_status   varchar2(32000);
l_where_payment_terms   varchar2(32000);
l_where_district        varchar2(32000);
l_where_region          varchar2(32000);
l_where_location_list   varchar2(32000);
l_where_created_by      varchar2(32000);
l_where_ship_to_org     varchar2(32000);
    
    
  begin
  req_appr_date_tab.delete;
  int_sales_order_dtls.delete;  --added for version 1.2
  
  --execute immediate 'truncate table xxeis.eis_xxwc_internal_sales_tab';
  
	--Fnd_File.Put_Line(FND_FILE.log,'start');
 
      open l_ref_cursor1 for main_sql;
      fetch l_ref_cursor1  bulk collect into req_appr_date_tab;-- LIMIT 10000;
      close l_ref_cursor1;

	--fnd_file.put_line(fnd_file.log,'req_appr_date_tab'||req_appr_date_tab.count);

          if p_order_type  is not null then
      l_where_order_type:= l_where_order_type||' and ott.name in ('||xxeis.eis_rs_utility.get_param_values(p_order_type)||' ) ';
    else
      l_where_order_type:=' and 1=1';
      end if;
	  
	            if p_schedule_ship_date_from  is not null then
      l_where_ship_date_from:= l_where_ship_date_from||' and trunc(ol.schedule_ship_date) in ('||xxeis.eis_rs_utility.get_param_values(p_schedule_ship_date_from)||' )  ';
    else
      l_where_ship_date_from:=' and 1=1';
	  end if;
	  
	            if p_shipping_method  is not null then
      l_where_shipping_method:= l_where_shipping_method||' and flv_ship_mtd.meaning in ('||xxeis.eis_rs_utility.get_param_values(p_shipping_method)||' ) ';
    else
      l_where_shipping_method:=' and 1=1';
	  end if;
	            if p_ship_from_org  is not null then
      l_where_ship_from_org:= l_where_ship_from_org||' and mtp.organization_code in  ('||xxeis.eis_rs_utility.get_param_values(p_ship_from_org)||' ) ';
    else
      l_where_ship_from_org:=' and 1=1';
	  end if;
      if p_ship_to_org  is not null then
      l_where_ship_to_org:= l_where_ship_to_org||' and mp1.organization_code in ('||xxeis.eis_rs_utility.get_param_values(p_ship_to_org)||' )  ';
    else
      l_where_ship_to_org:=' and 1=1';
    end if;
	            if p_schedule_ship_date_to  is not null then
      l_where_ship_date_to:= l_where_ship_date_to||' and trunc(ol.schedule_ship_date) in ('||xxeis.eis_rs_utility.get_param_values(p_schedule_ship_date_to)||' )  ';
    else
      l_where_ship_date_to:=' and 1=1';
	  end if;
	            if p_order_line_status  is not null then
      l_where_line_status:= l_where_line_status||' and xxeis.eis_rs_xxwc_com_util_pkg.get_order_line_status(ol.line_id) in ('||xxeis.eis_rs_utility.get_param_values(p_order_line_status)||' ) ';  --Commented is null code for version 1.1
    else
      l_where_line_status:=' and 1=1';
	  end if;
	            if p_order_header_status  is not null then
      l_where_header_status:= l_where_header_status||' and flv_order_status.meaning in ('||xxeis.eis_rs_utility.get_param_values(p_order_header_status)||' ) ';
    else
      l_where_header_status:=' and 1=1';
	  end if;
	            if p_payment_terms  is not null then
      l_where_payment_terms:= l_where_payment_terms||' and rt.name in ('||xxeis.eis_rs_utility.get_param_values(p_payment_terms)||' ) ';
    else
      l_where_payment_terms:=' and 1=1';
	  end if;
	            if p_district  is not null then
      l_where_district:= l_where_district||' and mtp.attribute8 in  ('||xxeis.eis_rs_utility.get_param_values(p_district)||' )';
    else
      l_where_district:=' and 1=1';
	  end if;
	            if p_region  is not null then
      l_where_region:= l_where_region||' and mtp.attribute9 in  ('||xxeis.eis_rs_utility.get_param_values(p_region)||' )  ';
    else
      l_where_region:=' and 1=1';
	  end if;


        l_where_location_list:='AND ( ('||xxeis.eis_rs_utility.get_param_values(p_location_list)||' ) is null 
        or EXISTS (SELECT 1  FROM
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME=  ('||xxeis.eis_rs_utility.get_param_values(p_location_list)||' )
                            AND LIST_TYPE =''Org''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                         mtp.organization_code, 1, 1)<>0))
        ';

    
	            if p_req_created_by  is not null then
      l_where_created_by:= l_where_created_by||' and fu.description in ('||xxeis.eis_rs_utility.get_param_values(p_req_created_by)||' )  ';
    else
      l_where_created_by:=' and 1=1';
    end if;
      
      
      l_sql:='select 
		'||p_process_id||' process_id ,
         mtp.organization_code warehouse
         ,rep.name sales_person_name
         --,ppf.full_name created_by  --Commented by Mahender for TMS#20140804-00201 on 01/26/15
         ,fu.description created_by                   --  Added by Mahender for TMS#20140804-00201 on 01/26/15
         ,oh.order_number order_number
         , (ol.line_number || ''.'' || ol.shipment_number) line_number
         ,TRUNC (oh.ordered_date) ordered_date
         ,TRUNC (ol.creation_date) line_creation_date
         ,ott.name order_type
         ,oh.quote_number
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_order_line_status (ol.line_id) order_line_status
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_backorder_qty (ol.header_id, ol.line_id) backorder_qty
         ,mtp.attribute8 district
         ,mtp.attribute9 region
         ,NVL (ol.shipped_quantity, 0) ship_qty
         ,flv_order_status.meaning order_header_status
         ,                                                            --OL.FLOW_STATUS_CODE order_line_status,
          hca.account_number customer_number
         ,NVL (hca.account_name, hzp.party_name) customer_name
         ,hzcs_ship_to.location customer_job_name
         ,ROUND (
             (  ol.unit_selling_price
              * DECODE (ott.order_category_code, ''RETURN'', ol.ordered_quantity * -1, ol.ordered_quantity))
            ,2)
             order_amount
         ,flv_ship_mtd.meaning shipping_method
         ,hzl_ship_to.city ship_to_city
         ,hzl_ship_to.postal_code zip_code
         ,TRUNC (ol.schedule_ship_date) schedule_ship_date
         ,oh.flow_status_code header_status
         ,msi.segment1 item_number
         ,msi.description item_description
         ,DECODE (ott.order_category_code
                 ,''RETURN'', (NVL (ol.ordered_quantity, 0) * -1)
                 ,NVL (ol.ordered_quantity, 0))
             qty
         ,rt.name payment_terms
         ,mp1.organization_code ship_to_org
         ,                                                                                --Primary Keys Added
          oh.header_id order_header_id
         ,ol.line_id order_line_id
         ,hca.cust_account_id cust_account_id
         ,hcas_ship_to.cust_acct_site_id cust_acct_site_id
         ,rep.salesrep_id salesrep_id
         ,hzp.party_id party_id
         ,ott.transaction_type_id transaction_type_id
         ,mtp.organization_id mtp_organization_id
         ,hzcs_ship_to.site_use_id hzcs_ship_to_ste_id
         ,hzps_ship_to.party_site_id hzps_shp_to_prt_id,
         :1   requisition_number ,      
         :2   po_req_created_by    ,        
         :3   po_req_created_by_id   ,       
         :4   req_appr_date  
		 ,(SELECT mil.segment1
          FROM mtl_item_locations_kfv mil,
               mtl_secondary_locators msl
          WHERE msl.inventory_item_id = msi.inventory_item_id
          AND msl.organization_id     = msi.organization_id
          AND msl.secondary_locator   = mil.inventory_location_id
          AND mil.segment1 LIKE ''1%''
          AND rownum = 1
          ) bin1, --added for version 1.2
          (SELECT mil.segment1
          FROM mtl_item_locations_kfv mil,
               mtl_secondary_locators msl
          WHERE msl.inventory_item_id = msi.inventory_item_id
          AND msl.organization_id     = msi.organization_id
          AND msl.secondary_locator   = mil.inventory_location_id
          AND mil.segment1 LIKE ''2%''
          AND rownum = 1
          ) bin2, --added for version 1.2
          (SELECT mil.segment1
          FROM mtl_item_locations_kfv mil,
               mtl_secondary_locators msl
          WHERE msl.inventory_item_id = msi.inventory_item_id
          AND msl.organization_id     = msi.organization_id
          AND msl.secondary_locator   = mil.inventory_location_id
          AND mil.segment1 LIKE ''3%''
          AND rownum = 1
          ) bin3 --added for version 1.2
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,hz_cust_accounts hca
         ,hz_parties hzp
         ,oe_transaction_types_vl ott
         ,oe_transaction_types_vl otl
         ,ra_salesreps rep
         ,mtl_parameters mtp
         ,hz_cust_site_uses hzcs_ship_to
         ,hz_cust_acct_sites hcas_ship_to
         ,hz_party_sites hzps_ship_to
         ,hz_locations hzl_ship_to
         ,hr_all_organization_units haou
         ,per_people_f ppf
         ,fnd_user fu
         ,fnd_lookup_values_vl flv_ship_mtd
         ,oe_lookups flv_order_status
         ,ra_terms_vl rt
         ,mtl_system_items_kfv msi
         ,fnd_lookup_values_vl flv_line_status
         ,po_location_associations pla
         ,mtl_parameters mp1
    WHERE     1 = 1
          AND ol.header_id = oh.header_id
          AND oh.sold_to_org_id = hca.cust_account_id(+)
          AND hca.party_id = hzp.party_id(+)
          AND oh.order_type_id = ott.transaction_type_id
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ship_from_org_id = mtp.organization_id(+)
          AND oh.ship_to_org_id = hzcs_ship_to.site_use_id(+)
          AND hzcs_ship_to.cust_acct_site_id = hcas_ship_to.cust_acct_site_id(+)
          AND hcas_ship_to.party_site_id = hzps_ship_to.party_site_id(+)
          AND hzl_ship_to.location_id(+) = hzps_ship_to.location_id
          AND hzcs_ship_to.site_use_id = pla.site_use_id(+)
          AND pla.organization_id = mp1.organization_id(+)
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          --AND ol.flow_status_code NOT IN (''CLOSED'', ''CANCELLED'')  --Commented by Mahender for TMS#20140804-00201 on 01/26/15
          AND mtp.organization_id = haou.organization_id
          -- AND msi.organization_id            = mp.organization_id
          --AND OH.CREATED_BY                  = PPF.PERSON_ID
          AND fu.user_id = oh.created_by
          AND fu.employee_id = ppf.person_id(+)
          AND flv_ship_mtd.lookup_type(+) = ''SHIP_METHOD''
          AND flv_ship_mtd.lookup_code(+) = ol.shipping_method_code
          AND flv_order_status.lookup_type = ''FLOW_STATUS''
          AND flv_order_status.lookup_code = oh.flow_status_code
          AND oh.payment_term_id(+) = rt.term_id
          AND (ott.name LIKE ''INTERNAL ORDER'' OR otl.name LIKE ''INTERNAL LINE'')
          AND TRUNC (oh.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (oh.creation_date))
                                           AND NVL (ppf.effective_end_date, TRUNC (oh.creation_date))
          --  AND OH.TRANSACTION_PHASE_CODE <>''N''
          AND msi.organization_id = ol.ship_from_org_id
          AND msi.inventory_item_id = ol.inventory_item_id
          and flv_line_status.lookup_type(+) = ''LINE_FLOW_STATUS''
          AND flv_line_status.lookup_code(+) = ol.flow_status_code
          and oh.header_id = :5
          '||l_where_order_type||'    
          '||l_where_ship_date_from||'
          '||l_where_shipping_method||'
          '||l_where_ship_from_org||'
          '||l_where_ship_date_to||'
          '||l_where_line_status||'
          '||l_where_header_status||'
          '||l_where_payment_terms||'
          '||l_where_district||'
          '||l_where_region||'  
          '||l_where_location_list||'
          '||l_where_created_by||'
          '||l_where_ship_to_org|| '
';

      
      if  req_appr_date_tab.count>0
      then
  for i in 1..req_appr_date_tab.count
  loop

      open l_ref_cursor2 for l_sql using req_appr_date_tab(i).segment1,
                                         req_appr_date_tab(i).description,
                                         req_appr_date_tab(i).employee_id,
                                         req_appr_date_tab(i).last_update_date,
                                         req_appr_date_tab(i).header_id;
                           
             LOOP
      fetch l_ref_cursor2  bulk collect into int_sales_order_dtls limit 10000;

      if int_sales_order_dtls.count>0
  then  
    
      forall j in 1..int_sales_order_dtls.count 
  insert into xxeis.eis_xxwc_internal_sales_tab values int_sales_order_dtls(j);
  

    end if;
    exit when l_ref_cursor2%notfound;
    end loop;
    close l_ref_cursor2;
    end loop;
    end if;
 commit;
    
exception when others then
fnd_file.put_line(fnd_file.log,'THE ERROR IS'||sqlcode||sqlerrm);
commit; 
      
      
end;

/* --Commented code for version 1.2
procedure clear_temp_tables ( p_process_id in number)
as
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0    Pramod  	04/16/2016   Initial Build --TMS#20160418-00106   by Pramod on 04-16-2016
--//============================================================================  
  begin  
  delete from xxeis.eis_xxwc_internal_sales_tab where process_id=p_process_id;
  commit;
end ;
*/ --Commented code for version 1.2


end eis_xxwc_int_sales_orders_pkg;
/
