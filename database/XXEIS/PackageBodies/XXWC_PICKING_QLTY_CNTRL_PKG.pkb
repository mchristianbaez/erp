CREATE OR REPLACE 
PACKAGE BODY    XXEIS.XXWC_PICKING_QLTY_CNTRL_PKG as
--//============================================================================
--//
--// Object Usage 				:: This Object Referred by "Picking Quality Control Leadership report - WC"
--//
--// Object Name         		:: XXWC_PICKING_QLTY_CNTRL_PKG
--//
--// Object Type         		:: Package Spec
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20180116-00278 
--//============================================================================


FUNCTION GET_AVG_TIME_PER_ORDER_FUNC(
    P_ORGANIZATION VARCHAR2,
    P_FROM_DATE   DATE,
    P_TO_DATE DATE)
  RETURN number
IS
  --//============================================================================
--//
--// Object Name         :: GET_AVG_TIME_PER_ORDER_FUNC
--//
--// Object Usage 		 :: This Object Referred by "Picking Quality Control Leadership report - WC"
--//
--// Object Type         :: Function
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_PICKING_QLTY_CNTRL_V View.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20180116-00278 
--//============================================================================

L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
l_cond varchar2(10000);  
begin
g_avg_time_per_order := NULL;

l_sql :='SELECT ROUND(SUM((xopt.PICK_COMPLETION_DATE - xopt.PICK_START_DATE)*24*60)/ COUNT(DISTINCT xopt.ORDER_NUMBER))
  FROM XXWC.XXWC_OM_PQC_PICKING_TBL xopt
  WHERE xopt.ship_from_org_id          = :1
  AND TRUNC(xopt.pick_completion_date)>= :2
  AND TRUNC(xopt.pick_completion_date)<= :3
';
    BEGIN
        l_hash_index:=P_ORGANIZATION||'-'||P_FROM_DATE||'-'||P_TO_DATE;
        g_avg_time_per_order := g_avg_time_per_order_vldn_tbl(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO g_avg_time_per_order USING P_ORGANIZATION,P_FROM_DATE,P_TO_DATE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_avg_time_per_order :=0;
    WHEN OTHERS THEN
    g_avg_time_per_order :=0;
    END;      
                      l_hash_index:=P_ORGANIZATION||'-'||P_FROM_DATE||'-'||P_TO_DATE;
                       g_avg_time_per_order_vldn_tbl(L_HASH_INDEX) := g_avg_time_per_order;
    END;
     return  g_avg_time_per_order;
     EXCEPTION when OTHERS then
      g_avg_time_per_order:=0;
      RETURN  g_avg_time_per_order;

END GET_AVG_TIME_PER_ORDER_FUNC;


procedure remove_rpt_columns(p_report_type varchar2,p_process_id number)
as
--//============================================================================
--//
--// Object Name         :: remove_rpt_columns
--//
--// Object Usage 		   :: This Object Referred by "Picking Quality Control Leadership report - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_PICKING_QLTY_CNTRL_V View.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20180116-00278 
--//============================================================================

l_cnt number:=0;
l_report_id number;
begin

 select report_id
   into l_report_id
from xxeis.eis_rs_processes
where process_id        = p_process_id
;

 if p_report_type = 'Summary'  
 then
 select count(1)
   into l_cnt
from xxeis.eis_rs_process_rpt_columns
WHERE process_id        = p_process_id
and upper(column_name) in ('AVG_TIME_PER_ORDER','BR_TIME_PER_ORDER','AVG_TIME_PER_LINE','BR_TIME_PER_LINE','PICKING_ACCURACY_PERCENT','BR_PICKING_ACCURACY_PERCENT','TOTAL_NO_OF_ORDERS','TOTAL_NO_OF_LINES','OVERALL_BRANCH_RANKING','OVERALL_DISTRICT_RANKING','OVERALL_REGIONAL_RANKING')
;

fnd_file.put_line(fnd_file.log,'l_count at Summary '||l_cnt);

  if l_cnt=0 then
  insert into  xxeis.eis_rs_process_rpt_columns
    ( process_column_id,
      process_id,
      column_id,
      report_id,
      application_id,
      column_name,
      display_name,
      description,
      data_type,
      data_format,
      alignment,
      column_width,
      display_order,
      derived_flag,
      enabled_flag,
      calculation_column,
      drill_down_url,
      created_by,
      creation_date,
      view_id,
      view_component_id,
      show_gl_segments,
      show_gl_segment_descr,
      gl_seg_col_prefix,
      last_update_date,
      last_updated_by,
      last_update_login 
    )
   select
     xxeis.eis_rs_process_rpt_columns_s.nextval,--process_column_id,
      p_process_id,
      column_id,
      report_id,
      application_id,
      column_name,
      display_name,
      description,
      data_type,
      data_format,
      alignment,
      column_width,
      display_order,
      derived_flag,
      enabled_flag,
      calculation_column,
      drill_down_url,
      created_by,
      creation_date,
      view_id,
      view_component_id,
      show_gl_segments,
      show_gl_segment_descr,
      gl_seg_col_prefix,
      last_update_date,
      last_updated_by,
      last_update_login 
      from xxeis.eis_rs_report_columns
      where report_id = l_report_id
  and upper(column_name) in ('AVG_TIME_PER_ORDER','BR_TIME_PER_ORDER','AVG_TIME_PER_LINE','BR_TIME_PER_LINE','PICKING_ACCURACY_PERCENT','BR_PICKING_ACCURACY_PERCENT','TOTAL_NO_OF_ORDERS','TOTAL_NO_OF_LINES','OVERALL_BRANCH_RANKING','OVERALL_DISTRICT_RANKING','OVERALL_REGIONAL_RANKING')
;
end if;

begin
 delete
  from xxeis.eis_rs_process_rpt_columns
  where process_id        = p_process_id
  and upper(column_name) in ('ASSOCIATE_NAME','TOTAL_NO_OF_ORDER_BY_ASSOCIATE','TOTAL_NO_OF_LINES_BY_ASSOCIATE','AVGTIME_PER_ORDER_BY_ASSOCIATE','AVGTIME_PER_LINE_BY_ASSOCIATE','PICK_ACCURACY_ASSOCIATE_PERCNT','SPEED_RANKING_PERCENT','OVERALL_RANKING_FOR_ASSOCIATE');
exception
when others then
null;
end;

elsif p_report_type = 'Detail'  
then

 select count(1)
   into l_cnt
from xxeis.eis_rs_process_rpt_columns
WHERE process_id        = p_process_id
and upper(column_name) in ('ASSOCIATE_NAME','TOTAL_NO_OF_ORDER_BY_ASSOCIATE','TOTAL_NO_OF_LINES_BY_ASSOCIATE','AVGTIME_PER_ORDER_BY_ASSOCIATE','AVGTIME_PER_LINE_BY_ASSOCIATE','PICK_ACCURACY_ASSOCIATE_PERCNT','SPEED_RANKING_PERCENT','OVERALL_RANKING_FOR_ASSOCIATE')
;

fnd_file.put_line(fnd_file.log,'l_count at Detail '||l_cnt);

  if l_cnt=0 then
  insert into  xxeis.eis_rs_process_rpt_columns
    ( process_column_id,
      process_id,
      column_id,
      report_id,
      application_id,
      column_name,
      display_name,
      description,
      data_type,
      data_format,
      alignment,
      column_width,
      display_order,
      derived_flag,
      enabled_flag,
      calculation_column,
      drill_down_url,
      created_by,
      creation_date,
      view_id,
      view_component_id,
      show_gl_segments,
      show_gl_segment_descr,
      gl_seg_col_prefix,
      last_update_date,
      last_updated_by,
      last_update_login 
    )
   select
     xxeis.eis_rs_process_rpt_columns_s.nextval,--process_column_id,
      p_process_id,
      column_id,
      report_id,
      application_id,
      column_name,
      display_name,
      description,
      data_type,
      data_format,
      alignment,
      column_width,
      display_order,
      derived_flag,
      enabled_flag,
      calculation_column,
      drill_down_url,
      created_by,
      creation_date,
      view_id,
      view_component_id,
      show_gl_segments,
      show_gl_segment_descr,
      gl_seg_col_prefix,
      last_update_date,
      last_updated_by,
      last_update_login 
      from xxeis.eis_rs_report_columns
      where report_id = l_report_id
      and upper(column_name) in ('ASSOCIATE_NAME','TOTAL_NO_OF_ORDER_BY_ASSOCIATE','TOTAL_NO_OF_LINES_BY_ASSOCIATE','AVGTIME_PER_ORDER_BY_ASSOCIATE','AVGTIME_PER_LINE_BY_ASSOCIATE','PICK_ACCURACY_ASSOCIATE_PERCNT','SPEED_RANKING_PERCENT','OVERALL_RANKING_FOR_ASSOCIATE');
end if;

begin
 delete
  from xxeis.eis_rs_process_rpt_columns
  where process_id        = p_process_id
  and upper(column_name) in ('AVG_TIME_PER_ORDER','BR_TIME_PER_ORDER','AVG_TIME_PER_LINE','BR_TIME_PER_LINE','PICKING_ACCURACY_PERCENT','BR_PICKING_ACCURACY_PERCENT','TOTAL_NO_OF_ORDERS','TOTAL_NO_OF_LINES','OVERALL_BRANCH_RANKING','OVERALL_DISTRICT_RANKING','OVERALL_REGIONAL_RANKING');
exception
when others then
null;
end;

 end if;
 
exception
when others then
fnd_file.put_line(fnd_file.log,'error in remove_rpt_columns '||sqlerrm);
end;




Procedure Get_Picking_Qlty_Cntrl_Proc(
    p_REPORT_TYPE IN VARCHAR2 ,
    P_BRANCH      IN VARCHAR2,
    P_DISTRICT    IN VARCHAR2,
    P_REGION      IN VARCHAR2,
    P_FROM_DATE   IN DATE ,
    p_To_Date     IN DATE)
as                
--//============================================================================
--//
--// Object Name         :: get_picking_qlty_cntrl_proc
--//
--// Object Usage 		   :: This Object Referred by "Picking Quality Control Leadership report - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_PICKING_QLTY_CNTRL_V View.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20180116-00278 
--//============================================================================

l_summary_sql 		varchar2(32000);
L_SUMMARY_MAIN_SQL 		VARCHAR2(32000);
l_detail_sql 		  varchar2(32000);
L_DETAIL_MAIN_SQL 		VARCHAR2(32000);
Lc_Where_Cond Varchar2(32000);
L_REF_CURSOR1  CURSOR_TYPE4;
L_Ref_Cursor2  Cursor_Type4;
L_Ref_Cursor3  Cursor_Type4;
L_REF_CURSOR4  CURSOR_TYPE4;


type report_summ_rec_tab is table of xxeis.XXWC_PICKING_QLTY_SUMMARY_TBL%rowtype index by binary_integer ;
REPORT_SUMMARY_TAB report_summ_rec_tab;

TYPE report_detail_rec_tab IS TABLE OF XXEIS.XXWC_PICKING_QLTY_DETAIL_TBL%ROWTYPE INDEX BY BINARY_INTEGER ;
Report_Detail_Tab Report_Detail_Rec_Tab;

Type Picking_Cntrl_Rec_Tab Is Table Of Xxeis.XXWC_PICKING_QLTY_CNTRL_TBL%Rowtype Index By Binary_Integer ;
REPORT_MAIN_TAB picking_cntrl_Rec_Tab;
 
BEGIN
REPORT_DETAIL_TAB.delete;
Report_Summary_Tab.Delete;
REPORT_MAIN_TAB.delete;

IF p_branch IS NOT NULL THEN
lc_where_cond := lc_where_cond||' and mp.organization_code in (' || REPLACE (XXEIS.EIS_RSC_UTILITY.GET_PARAM_VALUES (P_PARAM_VALUE => TRIM (p_Branch ) ), '%%', ', ' ) || ')';
end if;

IF p_district IS NOT NULL THEN
lc_where_cond := lc_where_cond||' and mp.attribute8 in (' || REPLACE (XXEIS.EIS_RSC_UTILITY.GET_PARAM_VALUES (P_PARAM_VALUE => TRIM (p_district ) ), '%%', ', ' ) || ')';
end if;

IF p_region IS NOT NULL THEN
lc_where_cond := lc_where_cond||' and mp.attribute9 in (' || REPLACE (XXEIS.EIS_RSC_UTILITY.GET_PARAM_VALUES (P_PARAM_VALUE => TRIM (p_region ) ), '%%', ', ' ) || ')';
END IF;




l_summary_sql:='SELECT ''Summary'' REPORT_TYPE,
  mp.organization_code branch,
  mp.attribute8 district,
  mp.attribute9 region,
  XXEIS.XXWC_PICKING_QLTY_CNTRL_PKG.GET_AVG_TIME_PER_ORDER_FUNC(mp.organization_id,'''||p_From_Date||''','''||p_To_Date||''') AVG_TIME_PER_ORDER,
  (SELECT ROUND(SUM((xopt.PICK_COMPLETION_DATE - xopt.PICK_START_DATE)*24*60)/SUM(xopt.NUMBER_OF_LINES_PULLED))
  FROM XXWC.XXWC_OM_PQC_PICKING_TBL xopt
  WHERE xopt.ship_from_org_id          = mp.organization_id
  AND TRUNC(xopt.pick_completion_date)>= '''||p_From_Date||'''
  AND TRUNC(xopt.pick_completion_date)<= '''||p_To_Date||'''
  ) AVG_TIME_PER_LINE,
  (SELECT ROUND((1-SUM(NVL(xopt.LINE_ERRORS,0))/SUM(xopt.NUMBER_OF_LINES_PULLED))*100,2)
  FROM XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL xopt
  WHERE xopt.ship_from_org_id                = mp.organization_id
  AND TRUNC(xopt.load_check_completion_date)>='''||p_From_Date||'''
  AND TRUNC(xopt.load_check_completion_date)<= '''||p_To_Date||'''
  ) picking_accuracy_percent,
  (SELECT DISTINCT COUNT(xopt.ORDER_NUMBER)
  FROM XXWC.XXWC_OM_PQC_PICKING_TBL xopt
  WHERE xopt.ship_from_org_id          = mp.organization_id
  AND TRUNC(xopt.pick_completion_date)>='''||p_From_Date||'''
  AND TRUNC(xopt.pick_completion_date)<= '''||p_To_Date||'''
  ) total_no_of_orders ,
  (SELECT SUM(xopt.total_number_of_lines)
  FROM xxwc.xxwc_om_pqc_picking_tbl xopt
  WHERE xopt.ship_from_org_id          = mp.organization_id
  AND TRUNC(xopt.pick_completion_date)>='''||p_From_Date||'''
  AND TRUNC(xopt.pick_completion_date)<= '''||p_To_Date||'''
  ) TOTAL_NO_OF_LINES ,
  mp.ORGANIZATION_ID,
  ood.operating_unit 
FROM org_organization_definitions ood,
  mtl_parameters mp
WHERE ood.organization_id       =mp.organization_id
AND operating_unit              =162
AND NVL(DISABLE_DATE,SYSDATE                                         +1)>SYSDATE
and XXEIS.XXWC_PICKING_QLTY_CNTRL_PKG.GET_AVG_TIME_PER_ORDER_FUNC(mp.organization_id,'''||p_From_Date||''','''||p_To_Date||''') IS NOT NULL'
;

L_SUMMARY_MAIN_SQL:='SELECT xpt1.report_type,
  xpt1.branch ,
  xpt1.district ,
  xpt1.region ,
  xpt1.avg_time_per_order ,
  xpt1.br_time_per_order ,
  xpt1.avg_time_per_line ,
  xpt1.br_time_per_line ,
  xpt1.picking_accuracy_percent ,
  xpt1.br_picking_accuracy_percent ,
  xpt1.total_no_of_orders ,
  xpt1.total_no_of_lines ,
  xpt1.overall_branch_ranking ,
  rank() over (partition BY xpt1.operating_unit order by xpt1.district_ranking) overall_district_ranking,
  rank() over (partition BY xpt1.operating_unit order by xpt1.region_ranking) overall_regional_ranking ,
  NULL associate_name ,
  to_number(NULL) total_no_of_order_by_associate ,
  to_number(NULL) total_no_of_lines_by_associate ,
  to_number(NULL) avgtime_per_order_by_associate ,
  to_number(NULL) avgtime_per_line_by_associate ,
  to_number(NULL) pick_accuracy_associate_percnt ,
  to_number(NULL) speed_ranking_percent ,
  to_number(NULL) overall_ranking_for_associate ,
  xpt1.organization_id,
  xpt1.operating_unit
FROM
  (SELECT xpt.report_type,
    xpt.branch ,
    xpt.district ,
    xpt.region ,
    xpt.avg_time_per_order ,
    xpt.br_time_per_order ,
    xpt.avg_time_per_line ,
    xpt.br_time_per_line ,
    xpt.picking_accuracy_percent ,
    xpt.br_picking_accuracy_percent ,
    xpt.total_no_of_orders ,
    xpt.total_no_of_lines ,
    xpt.overall_branch_ranking ,
    ((SUM(NVL(xpt.overall_branch_ranking,0)) over (partition BY xpt.district))/xpt.count_district) district_ranking,
    ((SUM(NVL(xpt.overall_branch_ranking,0)) over (partition BY xpt.region))  /xpt.count_region) region_ranking,
    xpt.organization_id,
    xpt.operating_unit
  FROM
    (SELECT Xpqst.Report_Type,
      Xpqst.Branch ,
      Xpqst.District ,
      Xpqst.Region ,
      xpqst.AVG_TIME_PER_ORDER ,
      Rank() Over (Partition BY operating_unit Order By AVG_TIME_PER_ORDER) Br_Time_Per_Order ,
      Xpqst.Avg_Time_Per_Line ,
      Rank() Over (Partition BY operating_unit Order By Avg_Time_Per_Line) Br_Time_Per_Line ,
      xpqst.picking_accuracy_percent ,
      Rank() Over (Partition BY operating_unit Order By PICKING_ACCURACY_PERCENT DESC ) Br_Picking_Accuracy_Percent ,
      Xpqst.Total_No_Of_Orders ,
      xpqst.total_no_of_lines ,
      (((rank() over (partition BY operating_unit order by picking_accuracy_percent DESC))+(rank() over (partition BY operating_unit order by avg_time_per_line)))/2) overall_branch_ranking,
      COUNT(xpqst.branch) over (partition BY district) count_district,
      COUNT(Xpqst.Branch) Over (Partition BY Region) count_region,
      xpqst.organization_id,
      Xpqst.operating_unit
    from xxeis.xxwc_picking_qlty_summary_tbl xpqst
    where PICKING_ACCURACY_PERCENT is not null
    ) xpt
  )xpt1';



l_detail_sql:='SELECT DISTINCT ''Detail'' REPORT_TYPE,
  mp.organization_code branch,
  MP.ATTRIBUTE8 DISTRICT,
  mp.attribute9 region,
  XOPPT.PICKED_BY_USER_NAME ASSOCIATE_NAME ,
  (SELECT DISTINCT COUNT(ORDER_NUMBER)
  FROM XXWC.XXWC_OM_PQC_PICKING_TBL s
  WHERE S.PICKED_BY_USER_NAME       = XOPPT.PICKED_BY_USER_NAME
  AND S.SHIP_FROM_ORG_ID            = MP.ORGANIZATION_ID
  AND TRUNC(S.PICK_COMPLETION_DATE)>= '''||p_From_Date||'''
  AND TRUNC(S.pick_completion_date)<= '''||p_To_Date||'''
  ) total_no_of_order_by_associate,
  (SELECT SUM(TOTAL_NUMBER_OF_LINES)
  FROM XXWC.XXWC_OM_PQC_PICKING_TBL S
  WHERE S.PICKED_BY_USER_NAME       = xoppt.PICKED_BY_USER_NAME
  AND S.SHIP_FROM_ORG_ID            = MP.ORGANIZATION_ID
  AND TRUNC(S.PICK_COMPLETION_DATE)>='''||p_From_Date||'''
  AND TRUNC(S.pick_completion_date)<= '''||p_To_Date||'''
  ) TOTAL_NO_OF_LINES_BY_ASSOCIATE,
  (SELECT ROUND((SUM(PICK_COMPLETION_DATE - PICK_START_DATE)*24*60)/ COUNT(DISTINCT ORDER_NUMBER))
  FROM XXWC.XXWC_OM_PQC_PICKING_TBL s
  WHERE S.PICKED_BY_USER_NAME       = xoppt.PICKED_BY_USER_NAME
  AND S.SHIP_FROM_ORG_ID            = MP.ORGANIZATION_ID
  AND TRUNC(S.PICK_COMPLETION_DATE)>='''||p_From_Date||'''
  AND TRUNC(S.pick_completion_date)<= '''||p_To_Date||'''
  ) avgtime_per_order_by_associate,
  (SELECT ROUND((SUM(PICK_COMPLETION_DATE - PICK_START_DATE)*24*60)/SUM(NUMBER_OF_LINES_PULLED))
  FROM XXWC.XXWC_OM_PQC_PICKING_TBL S
  WHERE S.PICKED_BY_USER_NAME       = xoppt.PICKED_BY_USER_NAME
  AND S.SHIP_FROM_ORG_ID            = MP.ORGANIZATION_ID
  AND TRUNC(S.PICK_COMPLETION_DATE)>='''||p_From_Date||'''
  AND TRUNC(S.pick_completion_date)<= '''||p_To_Date||'''
  ) AVGTIME_PER_LINE_BY_ASSOCIATE,
  (SELECT ROUND((1-SUM(NVL(LINE_ERRORS,0))/SUM(NUMBER_OF_LINES_PULLED))*100,2)
  FROM XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL s
  WHERE s.PICKED_BY_USER_NAME             = xoppt.PICKED_BY_USER_NAME
  AND S.SHIP_FROM_ORG_ID                  = MP.ORGANIZATION_ID
  AND TRUNC(S.LOAD_CHECK_COMPLETION_DATE)>='''||p_From_Date||'''
  AND TRUNC(S.LOAD_CHECK_COMPLETION_DATE)<= '''||p_To_Date||'''
  )pick_accuracy_associate_percnt,
  (SELECT ROUND((SUM(PICK_COMPLETION_DATE - PICK_START_DATE)*24*60)/SUM(NUMBER_OF_LINES_PULLED))
  FROM XXWC.XXWC_OM_PQC_PICKING_TBL S
  WHERE TRUNC(S.PICK_COMPLETION_DATE)>='''||p_From_Date||'''
  AND TRUNC(S.pick_completion_date)<= '''||p_To_Date||'''
  and s.PICKED_BY_USER_NAME        IS NOT NULL
  ) NATION_WIDE_AVGTIME_PER_LINE,
  mp.ORGANIZATION_ID,
  ood.operating_unit
FROM XXWC.XXWC_OM_PQC_PICKING_TBL xoppt,
  org_organization_definitions ood,
  mtl_parameters mp
WHERE XOPPT.SHIP_FROM_ORG_ID          =MP.ORGANIZATION_ID
AND OOD.ORGANIZATION_ID               =MP.ORGANIZATION_ID
AND ood.OPERATING_UNIT                =162
AND NVL(ood.DISABLE_DATE,SYSDATE +1)      >SYSDATE
AND xoppt.PICKED_BY_USER_NAME        IS NOT NULL
AND TRUNC(xoppt.PICK_COMPLETION_DATE)>='''||p_From_Date||'''
AND TRUNC(XOPPT.PICK_COMPLETION_DATE)<= '''||p_To_Date||'''
';


L_DETAIL_MAIN_SQL:='SELECT XPQDT1.Report_Type,
  XPQDT1.Branch ,
  XPQDT1.District ,
  XPQDT1.Region ,
  to_number(NULL) avg_time_per_order ,
  to_number(NULL) br_time_per_order ,
  to_number(NULL) avg_time_per_line ,
  to_number(NULL) br_time_per_line ,
  to_number(NULL) picking_accuracy_percent ,
  to_number(NULL) br_picking_accuracy_percent ,
  to_number(NULL) total_no_of_orders ,
  to_number(NULL) total_no_of_lines ,
  to_number(NULL) overall_branch_ranking ,
  to_number(NULL) overall_district_ranking,
  to_number(NULL) overall_regional_ranking ,
  XPQDT1.Associate_Name ,
  XPQDT1.Total_No_Of_Order_By_Associate ,
  XPQDT1.Total_No_Of_Lines_By_Associate ,
  XPQDT1.Avgtime_Per_Order_By_Associate ,
  XPQDT1.AVGTIME_PER_LINE_BY_ASSOCIATE ,
  xpqdt1.pick_accuracy_associate_percnt ,
  xpqdt1.SPEED_RANKING_PERCENT,
--  Rank() Over (Partition BY operating_unit Order By XPQDT1.SPEED_RANKING_PERCENT desc) SPEED_RANKING_PERCENT,
  Rank() Over (Partition BY operating_unit Order By XPQDT1.Overall_percent_For_Associate desc) Overall_Ranking_For_Associate,
  XPQDT1.organization_id,
  XPQDT1.operating_unit
  from
(SELECT xpqdt.Report_Type,
  xpqdt.Branch ,
  xpqdt.District ,
  Xpqdt.Region ,
  Xpqdt.Associate_Name ,
  Xpqdt.Total_No_Of_Order_By_Associate ,
  Xpqdt.Total_No_Of_Lines_By_Associate ,
  Xpqdt.Avgtime_Per_Order_By_Associate ,
  XPQDT.AVGTIME_PER_LINE_BY_ASSOCIATE ,
  XPQDT.PICK_ACCURACY_ASSOCIATE_PERCNT ,
  (((DECODE(XPQDT.NATION_WIDE_AVGTIME_PER_LINE,0,0,(XPQDT.NATION_WIDE_AVGTIME_PER_LINE-XPQDT.AVGTIME_PER_LINE_BY_ASSOCIATE)/XPQDT.NATION_WIDE_AVGTIME_PER_LINE)))*100) SPEED_RANKING_PERCENT,
  ROUND((XPQDT.PICK_ACCURACY_ASSOCIATE_PERCNT+ (((decode(XPQDT.NATION_WIDE_AVGTIME_PER_LINE,0,0,(XPQDT.NATION_WIDE_AVGTIME_PER_LINE-XPQDT.AVGTIME_PER_LINE_BY_ASSOCIATE)/XPQDT.NATION_WIDE_AVGTIME_PER_LINE)))*100))/2,2) Overall_percent_For_Associate,
  XPQDT.organization_id,
  XPQDT.operating_unit
from xxeis.xxwc_picking_qlty_detail_tbl xpqdt
WHERE xpqdt.PICK_ACCURACY_ASSOCIATE_PERCNT IS NOT NULL ) XPQDT1';

l_summary_sql:= l_summary_sql||' '||lc_where_cond; 

l_detail_sql:= l_detail_sql||' '||lc_where_cond; 

--DBMS_OUTPUT.Put_Line('l_summary_sql  '||l_summary_sql);
--DBMS_OUTPUT.Put_Line('l_detail_sql  '||l_detail_sql);

if P_REPORT_TYPE='Summary' then

FND_FILE.PUT_LINE(FND_FILE.LOG,'l_summary_sql  '||L_SUMMARY_SQL);
Fnd_File.Put_Line(FND_FILE.log,'L_SUMMARY_MAIN_SQL  '||L_SUMMARY_MAIN_SQL);

 OPEN L_REF_CURSOR1 FOR l_summary_sql; 
  LOOP
    fetch L_REF_CURSOR1 bulk collect into REPORT_SUMMARY_TAB limit 10000;
  If REPORT_SUMMARY_TAB.COUNT>0
  then
      FORALL J in 1..REPORT_SUMMARY_TAB.COUNT
    	Insert Into XXEIS.XXWC_PICKING_QLTY_SUMMARY_TBL Values REPORT_SUMMARY_TAB(J);
    END IF;
    exit when L_REF_CURSOR1%notfound;
    END LOOP;
CLOSE L_REF_CURSOR1;

Fnd_File.Put_Line(FND_FILE.log,'REPORT_SUMMARY_TAB count  '||REPORT_SUMMARY_TAB.count);

 OPEN L_REF_CURSOR3 FOR L_SUMMARY_MAIN_SQL; 
  LOOP
    fetch L_REF_CURSOR3 bulk collect into REPORT_MAIN_TAB limit 10000;
  If REPORT_MAIN_TAB.COUNT>0
  then
      FORALL J in 1..REPORT_MAIN_TAB.COUNT
    	Insert Into XXEIS.XXWC_PICKING_QLTY_CNTRL_TBL Values REPORT_MAIN_TAB(J);
    end if;
    exit when L_REF_CURSOR3%notfound;
    end LOOP;
CLOSE L_REF_CURSOR3;
end if;


if P_REPORT_TYPE='Detail' then

FND_FILE.PUT_LINE(FND_FILE.LOG,'l_detail_sql  '||L_DETAIL_SQL);
FND_FILE.PUT_LINE(FND_FILE.LOG,'L_DETAIL_MAIN_SQL  '||L_DETAIL_MAIN_SQL);

 OPEN L_REF_CURSOR2 FOR l_detail_sql; 
  LOOP
    fetch L_REF_CURSOR2 bulk collect into REPORT_DETAIL_TAB limit 10000;
  If REPORT_DETAIL_TAB.COUNT>0
  then
      FORALL J in 1..REPORT_DETAIL_TAB.COUNT
    	Insert Into XXEIS.XXWC_PICKING_QLTY_DETAIL_TBL Values REPORT_DETAIL_TAB(J);
    END IF;
    EXIT WHEN L_REF_CURSOR2%NOTFOUND;
    END LOOP;
CLOSE L_REF_CURSOR2;

Fnd_File.Put_Line(FND_FILE.log,'REPORT_DETAIL_TAB count  '||REPORT_DETAIL_TAB.count);

 OPEN L_REF_CURSOR4 FOR L_DETAIL_MAIN_SQL; 
  LOOP
    fetch L_REF_CURSOR4 bulk collect into REPORT_MAIN_TAB limit 10000;
  If REPORT_MAIN_TAB.COUNT>0
  then
      FORALL J in 1..REPORT_MAIN_TAB.COUNT
    	Insert Into XXEIS.XXWC_PICKING_QLTY_CNTRL_TBL Values REPORT_MAIN_TAB(J);
    end if;
    exit when L_REF_CURSOR4%notfound;
    end LOOP;
CLOSE L_REF_CURSOR4;
end if;


  exception when others then
    Fnd_File.Put_Line(FND_FILE.log,'The Error is '||sqlcode||sqlerrm);
end;


END XXWC_PICKING_QLTY_CNTRL_PKG;
/
