CREATE OR REPLACE PACKAGE BODY XXEIS.eis_po_xxwc_isr_pkg_v2
/************************************************************************************************************
  $Header EIS_PO_XXWC_ISR_PKG_V2.pkb $
  Module Name: EIS_PO_XXWC_ISR_PKG_V2

  PURPOSE: ISR Datamart Package

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.1        07/31/2014  Manjula Chellappan   Modified for TMS # 20140728-00130
                                              Added procedure populate_bpa_cost
                                              Modified for TMS # 20140623-00097
                                              For performance tuning
                                              Modified for TMS # 20140609-00203
                                              For Index Reorg
  1.2        11/11/2014  Manjula Chellappan   TMS # 20140908-00200 Updated BPA Logic      
  1.3       05/09/2016   P.Vamshidhar         TMS#20160509-00068 - Reduce parallelism on EIS Reports to 4
                                          
*************************************************************************************************************/
AS
   /*
       This is the 2nd major version of the ISR Datamart package. It was
       re-written by Rasikha Galimova June-Aug 2013 based on the original
       package xxeis.eis_po_xxwc_isr_pkg, supplied by EIS.

       This version focused on being faster and more reliable over long
       execution times, while producing nearly identical output.
   */
   --procedure populate_on_ord added by Rasikha 02/12/2014 to calculate correct value on_ord quantity TMS 20140204-00148
   g_owner            VARCHAR2 (10) := 'XXEIS';
   g_errbuf           CLOB;
   l_errbuf           CLOB;
   l_error_message    CLOB;
   --Added the variables by Manjula to send email when Error occurs TMS # 20140728-00130
   g_sec              VARCHAR2 (500);
   pl_dflt_email      VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_procedure_name   VARCHAR2 (100);

   /*
       This is the main procedure that is executed to begin the datamart refresh. It
       is scheduled to run from EIS but it can also be run from anon PL/SQL or a
       concurrent program.
   */
   PROCEDURE alter_table_temp (p_owner VARCHAR2, p_table_name VARCHAR2)
   IS
      v_execute_string   CLOB;
      v_tablespace       VARCHAR2 (124);
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.alter_table_temp';

      FOR r
         IN (SELECT object_name
               FROM all_objects
              WHERE     object_name = UPPER (TRIM (p_table_name))
                    AND object_type = 'TABLE'
                    AND owner = UPPER (TRIM (p_owner)))
      LOOP
         v_execute_string :=
            'ALTER TABLE ' || p_owner || '.' || p_table_name || ' NOLOGGING';

         EXECUTE IMMEDIATE v_execute_string;

         v_execute_string :=
               'ALTER TABLE '
            || p_owner
            || '.'
            || p_table_name
            || ' MOVE TABLESPACE XXEIS_DATA';

         EXECUTE IMMEDIATE v_execute_string;

         v_execute_string :=
            'ALTER TABLE ' || p_owner || '.' || p_table_name || ' parallel 4';

         EXECUTE IMMEDIATE v_execute_string;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_errbuf :=
               ' Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();
         RAISE;
   END;

   --*************************************************************
   --*************************************************************

   --*************************************************************
   --*************************************************************
   -- helper function
   PROCEDURE create_table_from_other_table (p_owner             VARCHAR2,
                                            p_old_table_name    VARCHAR2,
                                            p_new_table_name    VARCHAR2)
   IS
   BEGIN
      g_procedure_name :=
         'eis_po_xxwc_isr_pkg_v2.create_table_from_other_table';

      DECLARE
         vr   NUMBER := 0;
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (p_owner,
                                                           p_new_table_name);
         vr :=
            apps.xxwc_common_tunning_helpers.create_copy_table (
               p_owner,
               p_old_table_name,
               p_new_table_name);
         -- FUNCTION create_copy_table (p_owner VARCHAR2, p_table_name VARCHAR2, p_new_table_name VARCHAR2)
         alter_table_temp (p_owner, p_new_table_name);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_errbuf :=
               ' Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

         RAISE;
   END;

   --********************************************************
   PROCEDURE main
   IS
      v_clob   CLOB;
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.main';

      apps.xxwc_common_tunning_helpers.elapsed_time ('ISR',
                                                     'start ,0000',
                                                     g_start);
      apps.xxwc_common_tunning_helpers.elapsed_time ('ISR',
                                                     'start ,0000',
                                                     g_start);

      g_sec := 'Start Main Procecdure';

      g_sec := 'Populate_driving_tables';

      populate_driving_tables;
      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'populate_driving_tables',
         g_start);

      g_sec := 'Drop Custom Tables';

      -- drop all_tables used in the package
      FOR r IN (SELECT 'drop table ' || owner || '.' || object_name v
                  FROM all_objects
                 WHERE     object_name IN ('EIS_XXWC_PO_ISR_POSALES_TAB##',
                                           'EIS_XXWC_PO_ISR_PSALES_TAB##',
                                           'EIS_XXWC_PO_ISR_OSALES_TAB##',
                                           'EIS_XXWC_PERIOD_NAME_TAB##',
                                           'EIS_XXWC_PO_ISR_TAB_TMP_##X',
                                           'EIS_XXWC_PO_ISR_TAB_V##',
                                           'EIS_PO_XXWC_ISR_SVENDOR##Z',
                                           'EIS_PO_XXWC_ISR_ITEMS##Z',
                                           'EIS_PO_XXWC_ISR_DEMAND##Z',
                                           'EIS_PO_XXWC_ISR_BPA##Z',
                                           'EIS_PO_XXWC_ISR_AVER_COST##Z',
                                           'EIS_PO_XXWC_ISR_INT_REQ##Z',
                                           'EIS_PO_XXWC_ISR_SUPPLY##Z',
                                           'EIS_PO_XXWC_ISR_SO##Z',
                                           'EIS_PO_XXWC_ISR_QOH_AVAL_T##Z',
                                           'XXWC_TEMP_ORG_GROUPS7##' -- ,'EIS_PO_XXWC_ISR_QOH##Z'
                                                                    ,
                                           'EIS_PO_XXWC_ISR_BPA_COST##Z',
                                           'EIS_PO_XXWC_ISR_SS##Z',
                                           'EIS_XXWC_PO_ISR_SALES_TAB##',
                                           'EIS_PO_XXWC_ISR_VENDOR##Z',
                                           'EIS_XXWC_PO_ISR_OHITS_TAB##',
                                           'EIS_XXWC_PO_ISR_HITS_TAB##',
                                           'EIS_PO_XXWC_ISR_BIN_LOC##Z',
                                           'XXWC_GET_ISR_OPEN_REQ_QTY##X')
                       AND object_type = 'TABLE'
                       AND owner = 'XXEIS'
                UNION
                SELECT 'drop table ' || owner || '.' || object_name v
                  FROM all_objects
                 WHERE object_name LIKE '%EIS_PO_XXWC_ISR_DEMAND##Z%')
      LOOP
         BEGIN
            EXECUTE IMMEDIATE r.v;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END LOOP;

      ---------------------------------------------------------------
      ---------------------------------------------------------------
      --populate on_ord table
      DECLARE
         v_job   NUMBER;
      BEGIN
         DBMS_JOB.submit (
            v_job,
            'begin xxeis.eis_po_xxwc_isr_pkg_v2.populate_on_ord; end;',
            SYSDATE,
            NULL);
         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.drop_temp_table (
         'xxeis',
         'xxwc_temp_org_demand##');

      EXECUTE IMMEDIATE
         'CREATE TABLE xxeis.xxwc_temp_org_demand##
    AS
    SELECT organization_id, ''eis_po_xxwc_isr_demand##z''||organization_id table_name
      FROM org_organization_definitions
     WHERE organization_id <> 222 AND operating_unit = 162 AND NVL (disable_date, SYSDATE + 1) > SYSDATE ';

      BEGIN
         FOR r IN (SELECT job
                     FROM dba_jobs
                    WHERE what LIKE '%populate_demand_org%')
         LOOP
            DBMS_JOB.remove (r.job);
            COMMIT;
         END LOOP;
      END;

      EXECUTE IMMEDIATE
         'DECLARE
             v_ex   VARCHAR2 (250);
         BEGIN
             FOR r IN (SELECT * FROM xxeis.xxwc_temp_org_demand##)
             LOOP
                 v_ex :=
                        ''BEGIN xxeis.eis_po_xxwc_isr_pkg_v2.populate_demand_org (''''''
                     || r.table_name
                     || '''''',''
                     || r.organization_id
                     || ''); END;'';

                 DECLARE
                     v_job   NUMBER;
                 BEGIN               
                     DBMS_JOB.submit (v_job
                                     ,v_ex
                                     ,SYSDATE
                                     ,NULL);                                                                        
                     COMMIT;                  
                 END;
             END LOOP;
         END;';

      v_clob := 'truncate TABLE xxeis.eis_xxwc_po_isr_tab_total';

      EXECUTE IMMEDIATE v_clob;

      alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_tab_total');

      ----------------------------------------------------------------

      ----------------------------------------------------------------

      new_main2;
      apps.xxwc_common_tunning_helpers.elapsed_time ('ISR',
                                                     'after new_main2 ,0001',
                                                     g_start);
      --xxeis.eis_po_xxwc_isr_pkg_v2.populate_master;
      --apps.xxwc_common_tunning_helpers.elapsed_time ('ISR', 'after populate_master ,0002', g_start);
      xxeis.eis_po_xxwc_isr_pkg_v2.build_index;
      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'after build_indexr ,0003',
         g_start);                  -- MOVE DATA TO -xxeis.eis_xxwc_po_isr_tab
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_message :=
               'xxeis.eis_po_xxwc_isr_pkg_v2'
            || 'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (l_error_message);
         --apps.xxwc_common_tunning_helpers.write_log (l_error_message);
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'ISR',
            SUBSTR (l_error_message, 1, 511),
            g_start);
   END;

   /*
       This appears to be simply an organizational sub-procedure. It is called
       from main.
   */
   PROCEDURE new_main2
   /*************************************************************************
     $Header EIS_PO_XXWC_ISR_PKG_V2.NEW_MAIN2 $
     Module Name: EIS_PO_XXWC_ISR_PKG_V2

     PURPOSE: Main Pocess

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        07/31/2014  Manjula Chellappan    TMS # 20140623-00097
                                                  Modified to improve performance

     1.3       05/09/2016   P.Vamshidhar         TMS#20160509-00068 - Reduce parallelism on EIS Reports to 4
   **************************************************************************/
   IS
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.main';

      DECLARE
         s_ex   CLOB;
      BEGIN
         COMMIT;

         --EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DDL'; -- Commented in Rev 1.3
         EXECUTE IMMEDIATE 'ALTER SESSION FORCE PARALLEL DDL PARALLEL 4'; -- Added in Rev 1.3

         --EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';    -- Commented in Rev 1.3
         EXECUTE IMMEDIATE 'ALTER SESSION FORCE PARALLEL DML PARALLEL 4'; -- Added in Rev 1.3 

         EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL QUERY';

         --here are staging table sets for delta processing: #s# for staging
         --xxwc_mtl_system_items_b#s#a --driver table keeps  all items and item information needed for processing, including master items
         --xxwc_mtl_system_items_b#s#b -- table keep inventory_item_id, organization_id,source_organization_id,postprocessing_lead_time- no master org
         --xxwc_mtl_system_items_b#s#c -- table keep inventory_item_id, organization_id,source_organization_id- no master org

         g_sec := 'Drop table eis_po_xxwc_isr_ITEMS##z';

         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         g_sec := 'Create table eis_po_xxwc_isr_ITEMS##z';

         s_ex :=
            '
    CREATE TABLE XXEIS.eis_po_xxwc_isr_ITEMS##z AS SELECT
    organization_id
   ,inventory_item_id  FROM XXEIS.xxwc_mtl_system_items_b#s#b WHERE 1=2';

         EXECUTE IMMEDIATE s_ex;

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##z');
         ---populate driver table: all items and orgs

         g_sec := 'Insert into eis_po_xxwc_isr_ITEMS##z';


         s_ex :=
            '
    insert /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##z SELECT
    organization_id
   ,inventory_item_id  FROM XXEIS.xxwc_mtl_system_items_b#s#c';

         EXECUTE IMMEDIATE s_ex;

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'XXEIS.eis_po_xxwc_isr_ITEMS##z,1',
         g_start);

      g_sec := 'populate_vendor_tables';

      populate_vendor_tables;
      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'populate_vendor_tables',
         g_start);

      --populate open_req table
      DECLARE
         s_ex   CLOB;
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'xxwc_get_isr_open_req_qty##X');

         g_sec := 'Create Table xxwc_get_isr_open_req_qty##X    ';

         EXECUTE IMMEDIATE 'CREATE TABLE xxeis.xxwc_get_isr_open_req_qty##X
(    organization_id     NUMBER
   ,inventory_item_id   NUMBER
   ,open_req          NUMBER
   ,dir_req          NUMBER
)';

         alter_table_temp ('XXEIS', 'xxwc_get_isr_open_req_qty##X');

         g_sec := 'Insert into Table xxwc_get_isr_open_req_qty##X';

         s_ex :=
            'INSERT /*+ append */
              INTO  xxeis.xxwc_get_isr_open_req_qty##x (organization_id
                                                       ,inventory_item_id
                                                       ,open_req
                                                       ,dir_req)
              SELECT sup.to_organization_id
                    ,sup.item_id
                    ,SUM (
                         CASE
                             WHEN     xxeis.eis_po_xxwc_isr_util_pkg.is_not_vmi (''VENDOR''
                                                                                ,sup.req_line_id
                                                                                ,sup.po_line_location_id) = ''Y''
                                  AND xxeis.eis_po_xxwc_isr_util_pkg.has_drop_ships (sup.po_header_id
                                                                                    ,sup.req_line_id
                                                                                    ,sup.po_line_location_id) = ''N''
                             THEN
                                 NVL (to_org_primary_quantity, 0)
                             ELSE
                                 0
                         END)
                         vendor_qty

                    ,SUM (
                         CASE
                             WHEN     xxeis.eis_po_xxwc_isr_util_pkg.is_not_vmi (''DIRECT''
                                                                                ,sup.req_line_id
                                                                                ,sup.po_line_location_id) = ''Y''
                                  AND xxeis.eis_po_xxwc_isr_util_pkg.has_drop_ships (sup.po_header_id
                                                                                    ,sup.req_line_id
                                                                                    ,sup.po_line_location_id) = ''Y''
                             THEN
                                 NVL (to_org_primary_quantity, 0)
                             ELSE
                                 0
                         END)
                         direct_qty
                FROM apps.mtl_supply sup, apps.po_requisition_headers_all prh,xxeis.eis_po_xxwc_isr_ITEMS##z  oru
               WHERE sup.supply_type_code IN (''REQ'')
                     AND sup.destination_type_code = ''INVENTORY''
                     AND sup.to_organization_id = oru.organization_id
                     AND sup.item_id = oru.inventory_item_id
                     AND prh.requisition_header_id = sup.req_header_id
                     AND (   NVL (sup.from_organization_id, -1) <> oru.organization_id
                          OR (    sup.from_organization_id = oru.organization_id
                              AND EXISTS
                                      (SELECT ''x''
                                         FROM apps.mtl_secondary_inventories sub1
                                        WHERE     sub1.organization_id = sup.from_organization_id
                                              AND sub1.secondary_inventory_name = sup.from_subinventory
                                              AND sub1.availability_type <> 1)))
                     AND (   sup.to_subinventory IS NULL
                          OR (EXISTS
                                  (SELECT ''x''
                                     FROM apps.mtl_secondary_inventories sub2
                                    WHERE     sub2.secondary_inventory_name = sup.to_subinventory
                                          AND sub2.organization_id = sup.to_organization_id
                                          AND sub2.availability_type = DECODE (2, 1, sub2.availability_type, 1))))
                     AND (   sup.po_line_location_id IS NULL
                          OR EXISTS
                                 (SELECT ''x''
                                    FROM apps.po_line_locations_all lilo
                                   WHERE     lilo.line_location_id = sup.po_line_location_id
                                         AND NVL (lilo.vmi_flag, ''N'') = ''N''))
            GROUP BY sup.to_organization_id, sup.item_id';

         --    DBMS_OUTPUT.put_line (s_ex);

         EXECUTE IMMEDIATE s_ex;

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.xxwc_get_isr_open_req_qty##x,2',
         g_start);

      g_sec := 'Join Data together for Items';

      ---*********************gather data together
      ---******************************************join data
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add( open_req NUMBER ,dir_req NUMBER)';

         g_sec := 'Insert into eis_po_xxwc_isr_ITEMS##zT';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.*,nvl(b.open_req,0),nvl(b.dir_req,0)
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.xxwc_get_isr_open_req_qty##X B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         g_sec :=
            ' Rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';

         alter_table_temp ('XXEIS', 'xxwc_get_isr_open_req_qty##X');
      END;

      ---******************************************
      --populate bin_loc xxeis.eis_po_xxwc_isr_bin_loc##z
      --*******************************************
      g_sec := ' Populate xxeis.eis_po_xxwc_isr_bin_loc##z';

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_bin_loc##z');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_bin_loc##z (bin_loc varchar2(280),inventory_item_id number,organization_id number)';

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_bin_loc##z');

         g_sec := ' Insert into xxeis.eis_po_xxwc_isr_bin_loc##z';


         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_bin_loc##z  SELECT mil.segment1, msl.inventory_item_id, msl.organization_id
  FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl
 WHERE msl.secondary_locator = mil.inventory_location_id AND mil.segment1 LIKE ''1%''';

         COMMIT;

         g_sec :=
            ' Delete duplicate records from xxeis.eis_po_xxwc_isr_bin_loc##z';

         EXECUTE IMMEDIATE
            'DELETE FROM xxeis.eis_po_xxwc_isr_bin_loc##z
      WHERE (inventory_item_id, organization_id) IN (  SELECT inventory_item_id, organization_id
                                                         FROM xxeis.eis_po_xxwc_isr_bin_loc##z
                                                     GROUP BY inventory_item_id, organization_id
                                                       HAVING COUNT (inventory_item_id) > 1)';

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_bin_loc##z,3',
         g_start);

      ---******************************************
      --combine
      --*******************************************
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add(bin_loc varchar2(280))';

         g_sec := ' Insert into eis_po_xxwc_isr_ITEMS##zT';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.*,b.bin_loc
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_bin_loc##z B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         g_sec :=
            ' Rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- alter_table_temp ('XXEIS', 'xxeis.eis_po_xxwc_isr_bin_loc##z');
      END;

      --*************************************************************
      --populate delta changes

      g_sec := ' Populate_delta_hits';

      populate_delta_hits;

      g_sec := ' populate_hits_table';

      Populate_hits_table;

      DECLARE
         s_ex   CLOB;
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_xxwc_po_isr_ohits_tab##');

         EXECUTE IMMEDIATE 'CREATE TABLE xxeis.eis_xxwc_po_isr_ohits_tab##
(
    organization_id        NUMBER
   ,inventory_item_id      NUMBER
   ,hit4_other_inv_sales   NUMBER
   ,hit6_other_inv_sales   NUMBER
)';

         alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_ohits_tab##');

         g_sec := ' Insert into Table eis_xxwc_po_isr_ohits_tab##';

         s_ex :=
            'INSERT /*+ append */ into xxeis.eis_xxwc_po_isr_ohits_tab## select    msi.source_organization_id  organization_id
                           ,MSI.inventory_item_id  inventory_item_id
                           ,sum(tab.HIT4_STORE_SALES)        HIT4_OTHER_INV_SALES
                           ,sum(tab.HIT6_STORE_SALES)        HIT6_OTHER_INV_SALES
                      from  xxeis.xxwc_mtl_system_items_b#s#c msi,
                            xxeis.eis_xxwc_po_isr_hits_tab## tab
                       where 1=1
                         and msi.inventory_item_id         = tab.inventory_item_id
                         and msi.organization_id           = tab.organization_id
                         AND msi.source_organization_id <> msi.organization_id
                    group by msi.inventory_item_id
                            ,msi.source_organization_id ';

         EXECUTE IMMEDIATE s_ex;

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_xxwc_po_isr_ohits_tab## delta ,4',
         g_start);

      ---******************************************
      --populate vendor
      --*******************************************

      --vendor************************xxeis.eis_po_xxwc_isr_vendor##z
      -- populate_vendor_tables;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_vendor##z ,67',
         g_start);

      ---******************************************join data
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add(vendor_number varchar2(156))';

         g_sec := ' Insert into Table eis_po_xxwc_isr_ITEMS##zT ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.*,b.vendor_number
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_vendor##z B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         --
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_vendor##z ,5',
         g_start);

      ---******************************************
      --ss************************xxeis.eis_po_xxwc_isr_ss##z

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ss##z');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_ss##z (safety_stock_quantity number,inventory_item_id number,organization_id number)';

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ss##z');

         g_sec := ' Insert into Table eis_po_xxwc_isr_ss##z ';
/* commented By Manjula on 15-Dec-15 to remove duplicates TMS # 20140908-00200 and added new query 
         EXECUTE IMMEDIATE */
       --     'insert  /*+ append */ into  xxeis.eis_po_xxwc_isr_ss##z
/*SELECT DISTINCT mst.safety_stock_quantity, inventory_item_id, mst.organization_id
  FROM mtl_safety_stocks_view mst, org_acct_periods oap, org_organization_definitions ood
 WHERE     ood.organization_id = oap.organization_id
       AND mst.organization_id = oap.organization_id
       AND TRUNC (mst.effectivity_date) BETWEEN period_start_date AND NVL (period_close_date, SYSDATE)
       AND TRUNC (SYSDATE) BETWEEN period_start_date AND NVL (period_close_date, SYSDATE)';
*/ 

EXECUTE IMMEDIATE
      ' INSERT  /*+ append */ INTO  xxeis.eis_po_xxwc_isr_ss##z
        SELECT mst.safety_stock_quantity, inventory_item_id, mst.organization_id
          FROM mtl_safety_stocks_view mst,
               org_acct_periods oap,
               org_organization_definitions ood
         WHERE     ood.organization_id = oap.organization_id
               AND mst.organization_id = oap.organization_id
               AND TRUNC (mst.effectivity_date) BETWEEN period_start_date
                                                    AND NVL (
                                                           NVL (period_close_date,
                                                                schedule_close_date),
                                                           TRUNC(SYSDATE))
               AND TRUNC (SYSDATE) BETWEEN period_start_date
                                       AND NVL (
                                              NVL (period_close_date,
                                                   schedule_close_date),
                                              TRUNC(SYSDATE)) ' ;
                                      
         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_ss##z ,6',
         g_start);

      ---******************************************join data
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add(ss number)';

         g_sec := ' Insert into Table eis_po_xxwc_isr_ITEMS##zT ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.*,b.safety_stock_quantity
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_ss##z  B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_ss##z');
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'XXEIS.eis_po_xxwc_isr_ITEMS##zT ,7',
         g_start);

      ---******************************************
      --bpa_cost************************xxeis.eis_po_xxwc_isr_bpa_cost##z
      ---******************************************
      --bpa_cost************************xxeis.eis_po_xxwc_isr_bpa_cost##z

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_bpa_cost##z');

         -- Modified By Manjula on 31-Jul-14 for TMS  20140728-00130  begin

         /*
                     EXECUTE IMMEDIATE
                         'create table xxeis.eis_po_xxwc_isr_bpa_cost##z ( bpa_cost number,inventory_item_id number,organization_id number)';

                     xxeis.eis_po_xxwc_isr_pkg_v2.alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_bpa_cost##z');

                     EXECUTE IMMEDIATE
                         'insert  /*+ append */
                /*into  xxeis.eis_po_xxwc_isr_bpa_cost##z
SELECT ll.unit_price, l.inventory_item_id, l.organization_id
  FROM (  SELECT MAX (pol.po_line_id) line_id, z.inventory_item_id, z.organization_id
            FROM po_headers_all poh
                ,po_line_locations_all poll
                ,po_lines_all pol
                ,po_vendors pov
                ,xxeis.eis_po_xxwc_isr_vendor##z z
           WHERE     poh.type_lookup_code = ''BLANKET''
                 AND poh.po_header_id = pol.po_header_id
                 AND pol.po_line_id = poll.po_line_id(+)
                 AND NVL (poh.cancel_flag, ''N'') = ''N''
                 AND pol.item_id = z.inventory_item_id
                 AND (poh.global_agreement_flag = ''Y'' OR poll.ship_to_organization_id = z.organization_id)
                 AND poh.vendor_id = pov.vendor_id
                 AND pov.segment1 = z.vendor_number
        GROUP BY z.inventory_item_id, z.organization_id) l
      ,po_lines_all ll
 WHERE l.line_id = ll.po_line_id';

            COMMIT;

*/

         g_sec := ' Load BPA Cost ';

         xxeis.eis_po_xxwc_isr_pkg_v2.populate_bpa_cost;

         /*--Commented by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic

                  EXECUTE IMMEDIATE
                     ' CREATE TABLE xxeis.eis_po_xxwc_isr_bpa_cost##z AS
                  SELECT NVL (
                        promo_price,
                        NVL (
                           price_zone_break_price,
                           NVL (
                              price_zone_price,
                              NVL (national_break_price, NVL (national_price, unit_price)))))
                        bpa_cost,
                     inventory_item_id,
                     organization_id
               FROM xxeis.eis_po_xxwc_isr_bpa_price## ';
               -- Modified By Manjula on 31-Jul-14 for TMS  20140728-00130  End
         */

         --Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic
         EXECUTE IMMEDIATE
            ' CREATE TABLE xxeis.eis_po_xxwc_isr_bpa_cost##z AS
                    SELECT NVL (
                              a.promo_price,
                              DECODE (
                                 a.vendor_site,
                                 ''0PURCHASING'', NVL (a.price_zone_price,
                                                     NVL (a.national_price, a.unit_price)),
                                 a.unit_price)) bpa_cost  ,             
            inventory_item_id,
            organization_id
      FROM xxeis.eis_po_xxwc_isr_bpa_price## a ';
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_bpa_cost##z ,8',
         g_start);

      ---******************************************join data
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add( bpa_cost number)';

         g_sec :=
            ' Insert into Table XXEIS.eis_po_xxwc_isr_ITEMS##zT with BPA Cost';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.*,b.bpa_cost
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_bpa_cost##z  B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_bpa_cost##z');
      END;

      ---******************************************
      --reserved********************xxeis.eis_po_xxwc_isr_reserv##z

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_reserv##z');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_reserv##z (reserved number,inventory_item_id number,organization_id number)';

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_reserv##z');

         g_sec :=
            ' Insert into Table XXEIS.eis_po_xxwc_isr_ITEMS##zT with Reservation Quantity';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into  xxeis.eis_po_xxwc_isr_reserv##z
   SELECT NVL (SUM (mr.reservation_quantity), 0), mr.inventory_item_id, mr.organization_id
    FROM
        mtl_reservations mr
GROUP BY mr.inventory_item_id, mr.organization_id ';

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_reserv##z ,9',
         g_start);

      --POPULATE eis_po_xxwc_isr_aver_cost##z
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_aver_cost##z');

         /* Ver 1.1 Commented the below by Manjula on 31-Jul-14 For TMS # 20140623-00097 Performance Improvement

                     EXECUTE IMMEDIATE
                         'create table xxeis.eis_po_xxwc_isr_aver_cost##z (aver_cost number,inventory_item_id number,organization_id number)';

                     alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_aver_cost##z');
         */
         g_sec :=
            ' Insert into Table XXEIS.eis_po_xxwc_isr_ITEMS##zT with average cost';

         --           EXECUTE IMMEDIATE
         --               'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_aver_cost##z
         /*                 SELECT NVL (apps.cst_cost_api.get_item_cost (1, r.inventory_item_id, r.organization_id), 0) aver_cost
               ,r.inventory_item_id
               ,r.organization_id
           FROM xxeis.xxwc_mtl_system_items_b#s#c r';

                     COMMIT;

         */
         -- Ver 1.1  Added the below by Manjula on 31-Jul-14 for TMS # 20140623-00097 Performance Improvement

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_aver_cost##z AS
            SELECT a.item_cost aver_cost,
              r.inventory_item_id,
              r.organization_id
            FROM cst_item_costs a,
              cst_cost_types b,
              xxeis.xxwc_mtl_system_items_b#s#c r
            WHERE 1                                  =1
            AND a.cost_type_id                       = b.cost_type_id
            AND b.COST_TYPE                          =''Average''
            AND a.organization_id                    = NVL(b.organization_id, a.organization_id)
            AND TRUNC(NVL(b.DISABLE_DATE,sysdate+1)) > TRUNC(sysdate)
            AND r.inventory_item_id                  = a.inventory_item_id(+)
            AND r.organization_id                    = a.organization_id(+)';


         EXECUTE IMMEDIATE
            'CREATE INDEX xxeis.eis_po_xxwc_isr_aver_cost##N1 on xxeis.eis_po_xxwc_isr_aver_cost##z(inventory_item_id,organization_id)';
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_aver_cost##z ,15',
         g_start);

      --******************************************
      -------------bpa****************************xxeis.eis_po_xxwc_isr_bpa##z

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_bpa##z');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_bpa##z (  bpa number,inventory_item_id number,organization_id number)';

         xxeis.eis_po_xxwc_isr_pkg_v2.alter_table_temp (
            'XXEIS',
            'eis_po_xxwc_isr_bpa##z');

         g_sec := ' Insert into Table eis_po_xxwc_isr_bpa##z';

--Commented by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic End         
 --        EXECUTE IMMEDIATE
 --           'insert  /*+ append */ into  xxeis.eis_po_xxwc_isr_bpa##z
/*SELECT h.segment1 bpa, l.inventory_item_id, l.organization_id
  FROM (  SELECT MAX (poh.po_header_id) po_header_id, z.inventory_item_id, z.organization_id
            FROM po_headers_all poh
                ,po_line_locations_all poll
                ,po_lines_all pol
                ,po_vendors pov
                ,xxeis.eis_po_xxwc_isr_vendor##z z
           WHERE     poh.type_lookup_code = ''BLANKET''
                 AND poh.po_header_id = pol.po_header_id
                 AND NVL (poh.cancel_flag, ''N'') = ''N''
                 AND pol.po_line_id = poll.po_line_id(+)
                 AND pol.item_id = z.inventory_item_id
                 AND (poh.global_agreement_flag = ''Y'' OR poll.ship_to_organization_id = z.organization_id)
                 AND poh.vendor_id = pov.vendor_id
                 AND pov.segment1 = z.vendor_number
        GROUP BY z.inventory_item_id, z.organization_id) l
      ,po_headers_all h
 WHERE h.po_header_id = l.po_header_id';
*/

EXECUTE IMMEDIATE
           'INSERT  /*+ append */ INTO  xxeis.eis_po_xxwc_isr_bpa##z
            SELECT bpa,  inventory_item_id,  organization_id
            FROM xxeis.eis_po_xxwc_isr_bpa_price##';


         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_bpa##z ,16',
         g_start);

      --*******************************************

      g_sec := ' eis_po_xxwc_isr_pkg_v2.populate_master';

      xxeis.eis_po_xxwc_isr_pkg_v2.populate_master;
      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'after populate_master ,0002',
         g_start);
      --
      ---
      --
      --demand populated from dbms_jobs
      --here wait to finish
      wait_for_jobs ('populate_demand');
      apps.xxwc_common_tunning_helpers.elapsed_time ('ISR',
                                                     ' wait_for_jobs ,18',
                                                     g_start);

      --combine data from demand tables
      BEGIN
         EXECUTE IMMEDIATE
            'BEGIN
    APPS.xxwc_common_tunning_helpers.drop_temp_table (''xxeis'', ''eis_po_xxwc_isr_demand##z'');

    EXECUTE IMMEDIATE
        ''create table xxeis.eis_po_xxwc_isr_demand##z (demand number,inventory_item_id number,organization_id number,qoh number,supply number)'';

    xxeis.eis_po_xxwc_isr_pkg_v2.alter_table_temp (''XXEIS'', ''eis_po_xxwc_isr_demand##z'');

    
    FOR r IN (SELECT * FROM xxeis.xxwc_temp_org_demand##)
    LOOP
        EXECUTE IMMEDIATE '' INSERT /*+ append */
                  INTO  xxeis.eis_po_xxwc_isr_demand##z
                  select * from xxeis.'' || r.table_name;

        COMMIT;
     END LOOP;
     END;';
      END;

      ----clean after
      BEGIN
         EXECUTE IMMEDIATE
            'BEGIN
    FOR r IN (SELECT * FROM xxeis.xxwc_temp_org_demand##)
    LOOP
        APPS.xxwc_common_tunning_helpers.drop_temp_table (''xxeis'', r.table_name);
    END LOOP;
END;';

         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'xxwc_temp_org_demand##');
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         ' combine data from demand tables ,18',
         g_start);

      -------qoh***********************xxeis.eis_po_xxwc_isr_qoh##z

      ---******************************************
      --populate qoh,aval
      --*******************************************
      ---- *****************************xxeis.eis_po_xxwc_isr_qoh_ava
      --xxeis.eis_po_xxwc_isr_qoh##z
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_qoh_aval##z');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_qoh_aval##z (qoh number,aval number,inventory_item_id number,organization_id number)';

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_qoh_aval##z');



         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_qoh_aval##z SELECT a.qoh
      ,a.qoh - NVL (b.reserved, 0) aval
      ,a.inventory_item_id
      ,a.organization_id
  FROM  xxeis.eis_po_xxwc_isr_demand##z a, xxeis.eis_po_xxwc_isr_reserv##z b
 WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_qoh_aval##z ,11',
         g_start);
      --

      apps.xxwc_common_tunning_helpers.drop_temp_table (
         'xxeis',
         'eis_po_xxwc_isr_reserv##z');

      --   apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'xxeis.eis_po_xxwc_isr_qoh##z');

      ---******************************************
      --populate qoh,aval,turns
      --*******************************************
      ---qoh,aval,turns*************************************xxeis.eis_po_xxwc_isr_qoh_aval_t##z

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_qoh_aval_t##z');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_qoh_aval_t##z (qoh number,aval number,turns number,inventory_item_id number,organization_id number)';

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_qoh_aval_t##z');

         g_sec := 'Insert into eis_po_xxwc_isr_qoh_aval_t##z';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_qoh_aval_t##z SELECT a.qoh
      ,a.aval
      ,CASE WHEN b.avg_inv != 0 THEN TO_NUMBER (b.cogs_amt / b.avg_inv) ELSE 0 END turns
      ,a.inventory_item_id
      ,a.organization_id
  FROM eis_po_xxwc_isr_qoh_aval##z a, xxeis.eis_xxwc_items_turns_data b
 WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_qoh_aval_t##z ,12',
         g_start);

      ---******************************************join data
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add( qoh number,aval number,turns number)';

         g_sec := 'Insert into  XXEIS.eis_po_xxwc_isr_ITEMS##zT with onhand';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.* ,b.qoh ,b.aval,b.turns
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_qoh_aval_t##z  B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_qoh_aval_t##z');
      END;

      apps.xxwc_common_tunning_helpers.drop_temp_table (
         'xxeis',
         'eis_po_xxwc_isr_qoh_aval##z');

      ---******************************************
      --populate so
      --*******************************************
      ---so************************************* xxeis.eis_po_xxwc_isr_so##z

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_so##z');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_so##z (so number,inventory_item_id number,organization_id number)';

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_so##z');

         g_sec := 'Insert into  xxeis.eis_po_xxwc_isr_so##z';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into  xxeis.eis_po_xxwc_isr_so##z
    SELECT COUNT (r.inventory_item_id) so, r.inventory_item_id, r.organization_id
    FROM xxeis.xxwc_mtl_system_items_b#s#b msi, xxeis.xxwc_mtl_system_items_b#s#c r
   WHERE     msi.inventory_item_id = r.inventory_item_id
         AND msi.source_organization_id = r.organization_id
GROUP BY r.inventory_item_id, r.organization_id ';

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_so##z ,12',
         g_start);

      ---******************************************join data
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add( SO number )';

         g_sec := 'Insert into  eis_po_xxwc_isr_ITEMS##zT with SO ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.* ,nvl(b.SO,0)
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_so##z  B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_so##z');
      END;

      ---******************************************
      --populate supply
      --*******************************************
      ---------------supply*************************************

      --******************************************join data
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add( supply number )';

         g_sec := 'Insert into eis_po_xxwc_isr_ITEMS##zT wiht Supply ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.* ,b.supply
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_demand##z B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_supply##z');
      END;

      ---******************************************
      --populate on_ord
      --*******************************************
      ---------------on_ord*************************************

      --******************************************join data

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         eis_po_xxwc_isr_pkg_v2.create_table_from_other_table (
            'XXEIS',
            'eis_po_xxwc_isr_ITEMS##z',
            'eis_po_xxwc_isr_ITEMS##zT');

         eis_po_xxwc_isr_pkg_v2.alter_table_temp (
            'XXEIS',
            'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add(  on_ord number )';

         g_sec := 'Insert into eis_po_xxwc_isr_ITEMS##zT with On Ord details ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.* , b.on_ord
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_on_ord## B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';

         -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_supply##z');
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'ISR',
            'populate on_ord ,15',
            g_start);
      END;

      ---******************************************
      --populate int_req
      --*******************************************
      -----int_req*********************************xxeis.eis_po_xxwc_isr_int_req##z

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_int_req##z');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_int_req##z (int_req number,inventory_item_id number,organization_id number)';

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_int_req##z');

         g_sec := 'Insert into eis_po_xxwc_isr_ITEMS##zT with Int Req qty ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_int_req##z    SELECT/*+ rule*/ NVL (SUM (prl.quantity), 0) qty, item_id, destination_organization_id
    FROM po_requisition_headers_all prh, po_requisition_lines_all prl
   WHERE     prh.requisition_header_id = prl.requisition_header_id
         AND NVL (prh.cancel_flag, ''N'') = ''N''
         AND NVL (prl.cancel_flag, ''N'') = ''N''
         AND NVL (prl.closed_code, ''OPEN'') = ''OPEN''
         AND prh.authorization_status = ''INCOMPLETE''
         AND source_type_code = ''INVENTORY''
         AND NOT EXISTS
                     (SELECT 1
                        FROM po_action_history pah
                       WHERE     pah.object_id = prh.requisition_header_id
                             AND pah.object_type_code = ''REQUISITION''
                             AND pah.action_code = ''CANCEL'')
GROUP BY item_id, destination_organization_id';

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_int_req##z ,14',
         g_start);

      --******************************************join data
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add( int_req number )';

         g_sec := 'Insert into eis_po_xxwc_isr_ITEMS##zT with Int Req Number ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.* ,nvl(b.int_req,0)
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_int_req##z B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_int_req##z');
      END;

      ---******************************************
      --populate aver_cost
      --*******************************************
      -----aver_cost**********************************xxeis.eis_po_xxwc_isr_aver_cost##z

      --MOVED UP
      --JOIN DATA
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add(aver_cost number )';

         g_sec := 'Insert into eis_po_xxwc_isr_ITEMS##zT with average cost ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.* ,b.aver_cost
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_aver_cost##z B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            ' rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_aver_cost##z');
      END;

      --populate bpa
      --*******************************************
      -------------bpa****************************xxeis.eis_po_xxwc_isr_bpa##z

      --MOVED UP

      --*************************************************************************

      --JOIN DATA
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add(BPA number )';

         g_sec := 'Insert into eis_po_xxwc_isr_ITEMS##zT with BPA ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.* ,b.BPA
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_bpa##z B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            'rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_bpa##z');
      END;

      --here was demand combine- was moved up

      ---******************************************join data
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add(demand number)';

         g_sec := 'Insert into eis_po_xxwc_isr_ITEMS##zT with demand Number ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.*,b.demand
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.eis_po_xxwc_isr_demand##z B
  WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.organization_id = b.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            'rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- alter_table_temp ('XXEIS', 'xxeis.eis_po_xxwc_isr_bin_loc##z');
      END;

      --******************************************
      --*******************************************

      --JOIN DATA
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##zT');
         create_table_from_other_table ('XXEIS',
                                        'eis_po_xxwc_isr_ITEMS##z',
                                        'eis_po_xxwc_isr_ITEMS##zT');

         alter_table_temp ('XXEIS', 'eis_po_xxwc_isr_ITEMS##zT');

         EXECUTE IMMEDIATE
            'alter table XXEIS.eis_po_xxwc_isr_ITEMS##zT add( segment1                     VARCHAR2 (40)
   ,source_organization_id       NUMBER
   ,description                  VARCHAR2 (240)
   ,full_lead_time               NUMBER
   ,primary_uom_code             VARCHAR2 (3)
   ,attribute21                  VARCHAR2 (240)
   ,attribute20                  VARCHAR2 (240)
   ,unit_weight                  NUMBER
   ,fixed_lot_multiplier         NUMBER
   ,buyer_id                     NUMBER (9, 0)
   ,attribute30                  VARCHAR2 (240)
   ,inventory_item_status_code   VARCHAR2 (10) NOT NULL
   ,inventory_planning_code      NUMBER
   ,source_type                  NUMBER
   ,mrp_safety_stock_code        NUMBER
   ,shelf_life_days  NUMBER
   ,LIST_PRICE_PER_UNIT  NUMBER
   ,MAX_MINMAX_QUANTITY NUMBER
   ,min_minmax_quantity NUMBER
   ,preprocessing_lead_time number)';

         g_sec := 'Insert into eis_po_xxwc_isr_ITEMS##zT with combined data ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_po_xxwc_isr_ITEMS##zT  SELECT A.* ,Z.segment1
      ,Z.source_organization_id
      ,Z.description
      ,Z.full_lead_time
      ,Z.primary_uom_code
      ,Z.attribute21
      ,Z.attribute20
      ,Z.unit_weight
      ,Z.fixed_lot_multiplier
      ,Z.buyer_id
      ,Z.attribute30
      ,Z.inventory_item_status_code
      ,Z.inventory_planning_code
      ,Z.source_type
      ,Z.mrp_safety_stock_code
      ,Z.shelf_life_days
      ,Z.LIST_PRICE_PER_UNIT
      ,Z.MAX_MINMAX_QUANTITY
      ,Z.min_minmax_quantity
      ,z.preprocessing_lead_time
  FROM XXEIS.eis_po_xxwc_isr_ITEMS##z A, xxeis.xxwc_mtl_system_items_b#s#a Z
  WHERE a.inventory_item_id = Z.inventory_item_id(+) AND a.organization_id = Z.organization_id(+)';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_ITEMS##z');

         EXECUTE IMMEDIATE
            'rename eis_po_xxwc_isr_ITEMS##zT to eis_po_xxwc_isr_ITEMS##z';
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_aver_cost##z');
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'XXEIS.eis_po_xxwc_isr_ITEMS##zT ,20',
         g_start);

      -----------------------------------------------------------------------------------------------
      DECLARE
         s_ex   CLOB;
      BEGIN
         s_ex := 'CREATE TABLE xxeis.eis_xxwc_po_isr_tab_v##
(
    org                      VARCHAR2 (3 BYTE)
   ,organization_name        VARCHAR2 (240 BYTE) NOT NULL
   ,district                 VARCHAR2 (150 BYTE)
   ,region                   VARCHAR2 (150 BYTE)
   ,pre                      VARCHAR2 (9 BYTE)
   ,item_number              VARCHAR2 (40 BYTE)
   ,vendor_name              VARCHAR2 (246 BYTE)
   ,vendor_number            VARCHAR2 (246 BYTE)
   ,source                   VARCHAR2 (30 BYTE)
   ,st                       CHAR (1 BYTE)
   ,description              VARCHAR2 (240 BYTE)
   ,cat_class                VARCHAR2 (40 BYTE)
   ,inv_cat_seg1             VARCHAR2 (40 BYTE)
   ,pplt                     NUMBER
   ,plt                      NUMBER
   ,uom                      VARCHAR2 (3 BYTE)
   ,cl                       VARCHAR2 (40 BYTE)
   ,stk                      CHAR (1 BYTE)
   ,pm                       VARCHAR2 (80 BYTE)
   ,MIN                      NUMBER
   ,MAX                      NUMBER
   ,hit_6                    NUMBER
   ,aver_cost                NUMBER
   ,item_cost                NUMBER
   ,bpa_cost                 NUMBER
   ,bpa                      NUMBER
   ,qoh                      NUMBER
   ,on_ord                   NUMBER
   ,avail                    NUMBER
   ,avail_d                  NUMBER
   ,bin_loc                  VARCHAR2 (1024 BYTE)
   ,mc                       CHAR (64 BYTE)
   ,fi                       VARCHAR2 (40 BYTE)
   ,freeze_date              DATE
   ,res                      VARCHAR2 (240 BYTE)
   ,thirteen_wk_avg_inv      NUMBER
   ,thirteen_wk_an_cogs      NUMBER
   ,turns                    NUMBER
   ,buyer                    VARCHAR2 (240 BYTE)
   ,ts                       NUMBER
   ,amu                      VARCHAR2 (240 BYTE)
   ,so                       NUMBER
   ,mf_flag                  CHAR (64 BYTE)
   ,wt                       NUMBER
   ,ss                       NUMBER
   ,sourcing_rule            VARCHAR2 (50 BYTE)
   ,fml                      NUMBER
   ,open_req                 NUMBER
   ,inventory_item_id        NUMBER NOT NULL
   ,organization_id          NUMBER NOT NULL
   ,set_of_books_id          NUMBER (15, 0) NOT NULL
   ,clt                      VARCHAR2 (240 BYTE)
   ,avail2                   NUMBER
   ,supply                   NUMBER
   ,demand                   NUMBER
   ,int_req                  NUMBER
   ,dir_req                  NUMBER
   ,item_status_code         VARCHAR2 (10 BYTE) NOT NULL
   ,vendor_site              VARCHAR2 (246 BYTE)
   ,source_organization_id   NUMBER


   ,SOURCE_TYPE NUMBER
   ,site_vendor_num  VARCHAR2 (246 BYTE)
)';
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_xxwc_po_isr_tab_v##');

         EXECUTE IMMEDIATE s_ex;

         alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_tab_v##');

         g_sec := 'Insert into xxeis.eis_xxwc_po_isr_tab_v## ';


         s_ex :=
            'insert /*+ append */
      INTO xxeis.eis_xxwc_po_isr_tab_v## SELECT ood.organization_code org
          ,ood.organization_name organization_name
          ,mtp.attribute8 district
          ,mtp.attribute9 region
          ,SUBSTR (Z.segment1, 1, 3) pre
          ,Z.segment1 item_number
          , pov.vendor_name
          , pov.segment1
          ,CASE
               WHEN item_source_type.meaning = ''Supplier''
               THEN
                  pov.segment1
               WHEN item_source_type.meaning = ''Inventory''
               THEN
                   (SELECT organization_code
                      FROM org_organization_definitions source_org
                     WHERE source_org.organization_id = Z.source_organization_id)
               ELSE
                   NULL
           END
               source
          ,CASE
               WHEN item_source_type.meaning = ''Supplier'' THEN ''S''
               WHEN item_source_type.meaning = ''Inventory'' THEN ''I''
               ELSE NULL
           END
               st
          ,Z.description description
          ,mcvc.segment2 cat_class
          ,mcvc.segment1 inv_cat_seg1
          ,preprocessing_lead_time pplt
          ,Z.full_lead_time plt
          ,Z.primary_uom_code uom
          ,mcvs.segment1 cl
          ,CASE
               WHEN (mcvs.segment1 IN (''1'', ''2'', ''3'', ''4'', ''5'', ''6'', ''7'', ''8'', ''9'', ''C'', ''B'')) THEN ''Y''
               WHEN (mcvs.segment1 IN (''E'') AND (min_minmax_quantity = 0 AND max_minmax_quantity = 0)) THEN ''N''
               WHEN (mcvs.segment1 IN (''E'') AND (min_minmax_quantity > 0 AND max_minmax_quantity > 0)) THEN ''Y''
               WHEN (mcvs.segment1 IN (''N'', ''Z'')) --AND ITEM_TYPE                                                                                           =''NON-STOCK'')
                                                 THEN ''N''
               ELSE ''N''
           END
               stk
          ,mrp_planning_code.meaning pm
          ,min_minmax_quantity MIN
          ,max_minmax_quantity MAX
          ,null hit_6
          ,z.aver_cost
          ,NVL (z.bpa_cost, list_price_per_unit) item_cost
          ,z.bpa_cost
          ,z.bpa
          ,z.qoh
         ,nvl(z.on_ord,0)
         -- ,z.supply -nvl(z.open_req,0)

          ,z.aval AVAIL
          ,z.aval * z.aver_cost avail_d
          ,z.bin_loc
          ,null mc
          ,mcvp.segment1 fi
          ,null freeze_date
          ,Z.attribute21 res
           ,NULL thirteen_wk_avg_inv
          ,NULL thirteen_wk_an_cogs
          ,z.turns
          ,ppf.full_name buyer
          ,shelf_life_days ts
          ,Z.attribute20 amu
          ,z.so
          ,null mf_flag
          ,Z.unit_weight wt
          ,z.ss
          ,msr.sourcing_rule_name sourcing_rule
          ,Z.fixed_lot_multiplier fml
          ,NVL (z.open_req, 0) open_req
          ,Z.inventory_item_id
          ,Z.organization_id
          ,ood.set_of_books_id
          ,Z.attribute30 clt
          ,z.qoh - z.demand avail2
          ,z.supply
          ,z.demand
          ,z.int_req
          ,z.dir_req
          ,Z.inventory_item_status_code item_status_code
          , pvs.vendor_site_code
          ,Z.source_organization_id

           ,Z.SOURCE_TYPE
           , pov.segment1 site_vendor_num
      FROM  org_organization_definitions ood
          ,mtl_categories_kfv mcvs
          ,mtl_item_categories mics
          ,mtl_categories_kfv mcvp
          ,mtl_item_categories micp
          ,mtl_categories_kfv mcvc
          ,mtl_item_categories micc
-- Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic Begin
--        ,mrp_sr_assignments msa
          ,(SELECT * FROM mrp_sr_assignments WHERE assignment_type = 6 AND assignment_set_id = 1 )    msa
--Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic End                        
          ,mrp_sr_receipt_org msro
          ,mrp_sr_source_org msso
          ,mrp_sourcing_rules msr
          ,po_vendors pov
          ,po_vendor_sites_all pvs
          ,mfg_lookups mrp_planning_code
          ,mfg_lookups item_source_type
          ,per_people_x ppf
          ,mtl_parameters mtp
          ,mfg_lookups sfty_stk
          ,xxeis.eis_po_xxwc_isr_ITEMS##z Z
     WHERE     Z.organization_id = ood.organization_id
           AND Z.buyer_id = ppf.person_id(+)
           --AND Z.vendor_number = pov.segment1(+)
           AND Z.inventory_item_id = mics.inventory_item_id(+)
           AND Z.organization_id = mics.organization_id(+)
           AND mics.category_id = mcvs.category_id(+)
           AND mcvs.structure_id(+) = 50410
           AND mics.category_set_id(+) = 1100000044
           AND Z.inventory_item_id = micp.inventory_item_id(+)
           AND Z.organization_id = micp.organization_id(+)
           AND micp.category_id = mcvp.category_id(+)
           AND mcvp.structure_id(+) = 50408
           AND micp.category_set_id(+) = 1100000043
           AND Z.inventory_item_id = micc.inventory_item_id
           AND Z.organization_id = micc.organization_id
           AND micc.category_id = mcvc.category_id
           AND mcvc.structure_id = 101
           AND micc.category_set_id = 1100000062
           AND Z.inventory_item_id = msa.inventory_item_id(+)
           AND Z.organization_id = msa.organization_id(+)
           AND msa.sourcing_rule_id = msro.sourcing_rule_id(+)
           AND msa.sourcing_rule_id = msr.sourcing_rule_id(+)
           AND msro.sr_receipt_id = msso.sr_receipt_id(+)
           AND msso.vendor_id = pov.vendor_id(+)
           AND msso.vendor_site_id = pvs.vendor_site_id(+)
           AND mrp_planning_code.lookup_type(+) = ''MTL_MATERIAL_PLANNING''
           AND mrp_planning_code.lookup_code(+) = Z.inventory_planning_code
           AND item_source_type.lookup_type(+) = ''MTL_SOURCE_TYPES''
           AND item_source_type.lookup_code(+) = Z.source_type
           AND Z.organization_id = mtp.organization_id
           AND sfty_stk.lookup_type(+) = ''MTL_SAFETY_STOCK_TYPE''
           AND sfty_stk.lookup_code(+) = Z.mrp_safety_stock_code
            AND NOT EXISTS
                       (SELECT 1
                          FROM mtl_parameters mp1
                         WHERE     mp1.organization_id = Z.organization_id
                               AND organization_code IN (''CAN'', ''HDS'', ''US1'', ''CN1'', ''MST'', ''WCC''))';

         EXECUTE IMMEDIATE s_ex;

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_xxwc_po_isr_tab_v## ,21',
         g_start);

      -----
      --POPULATE VENDOR
      --**********************************************************************
      --**********************************************************************
      --**********************************************************************
      --******************VENDOR**********************************************
      --**********************************************************************
      --**********************************************************************
      ------------------------------------------------------------------------
      -- start vendor      ---------------------------------------------------
      --**********************************************************************
      --**********************************************************************
      -- start vendor      ---------------------------------------------------
      --**********************************************************************
      --**********************************************************************

      BEGIN
         --************************************************************

         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_SVENDOR##x');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_SVENDOR##x ( inventory_item_id number,organization_id number,  vendor_org number  )';

         xxeis.eis_po_xxwc_isr_pkg_v2.alter_table_temp (
            'XXEIS',
            'eis_po_xxwc_isr_SVENDOR##x');

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_SVENDOR##x select inventory_item_id  ,
                organization_id  ,DECODE(NVL(ST,''X''),''I'', source_organization_id,organization_id)
              from xxeis.eis_xxwc_po_isr_tab_v##   WHERE (NVL(ST,''X'')=''I'' OR vendor_number IS NULL)';

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'eis_po_xxwc_isr_SVENDOR##x ,01',
         g_start);

      ----------------------------------------------------------------------------------------------

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_SVENDOR##z');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_SVENDOR##z (vendor_name VARCHAR2(245),VENDOR_NUMBER VARCHAR2(245),vendor_site VARCHAR2(245)
                 ,inventory_item_id number,organization_id number,vendor_id number)';

         xxeis.eis_po_xxwc_isr_pkg_v2.alter_table_temp (
            'XXEIS',
            'eis_po_xxwc_isr_SVENDOR##z');

         g_sec := 'Insert into xxeis.eis_po_xxwc_isr_SVENDOR##z ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_SVENDOR##z

SELECT       pov.vendor_name
               ,pov.segment1
               ,pvs.vendor_site_code
               ,z.inventory_item_id
               ,z.organization_id
               ,pov.vendor_id
-- Commented By Manjula on 17-Dec-14 TMS # 20140908-00200 Updated BPA Logic 
-- to select data only for the assignment set WC Default                 
  --FROM mrp_sr_assignments ass
    FROM (SELECT * FROM mrp_sr_assignments WHERE assignment_type = 6 AND assignment_set_id = 1 ) ass   
      ,mrp_sr_receipt_org rco
      ,mrp_sr_source_org sso
      ,po_vendors pov
      ,po_vendor_sites_all pvs
       ,xxeis.xxwc_mtl_system_items_b#s#a msi
      ,xxeis.eis_po_xxwc_isr_svendor##x z
 WHERE     1 = 1
       AND msi.inventory_item_id = ass.inventory_item_id
       AND msi.organization_id = ass.organization_id
       AND ass.inventory_item_id = z.inventory_item_id
       AND ass.organization_id = z.vendor_org
       AND msi.source_type = 2
       AND rco.sourcing_rule_id = ass.sourcing_rule_id
       AND sso.sr_receipt_id = rco.sr_receipt_id
       AND sso.source_type = 3
       AND pov.vendor_id = sso.vendor_id
       AND pvs.vendor_site_id = sso.vendor_site_id
 ';

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'eis_po_xxwc_isr_SVENDOR##z ,02',
         g_start);

      --------------------------------------------------------------------------------

      BEGIN
         xxeis.eis_po_xxwc_isr_pkg_v2.create_table_from_other_table (
            'XXEIS',
            'eis_po_xxwc_isr_SVENDOR##z',
            'eis_po_xxwc_isr_SVENDOR##zx');
         g_sec := 'Insert into eis_po_xxwc_isr_SVENDOR##zx ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_SVENDOR##zx
SELECT a.vendor_name
      ,a.vendor_number
      ,a.vendor_site
      ,b.inventory_item_id
      ,b.organization_id
      ,a.vendor_id
  FROM xxeis.eis_po_xxwc_isr_svendor##z a, xxeis.eis_po_xxwc_isr_svendor##x b
 WHERE a.inventory_item_id(+) = b.inventory_item_id AND a.organization_id(+) = b.organization_id';

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'eis_po_xxwc_isr_SVENDOR##zx ,03',
         g_start);

      ----------------------------------------------------------------------------------------------------

      BEGIN
         xxeis.eis_po_xxwc_isr_pkg_v2.create_table_from_other_table (
            'XXEIS',
            'eis_po_xxwc_isr_SVENDOR##zx',
            'eis_po_xxwc_isr_SVENDOR##z');

         g_sec :=
            'Insert into eis_po_xxwc_isr_SVENDOR##z where vendor number is not null ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_SVENDOR##z
SELECT vendor_name
      ,vendor_number
      ,vendor_site
      ,inventory_item_id
      ,organization_id
      ,null
  FROM xxeis.eis_po_xxwc_isr_svendor##zx a where a.vendor_number is not null';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_SVENDOR##b');

         EXECUTE IMMEDIATE
            'create table xxeis.eis_po_xxwc_isr_SVENDOR##b ( inventory_item_id number,vendor_id number  )';

         xxeis.eis_po_xxwc_isr_pkg_v2.alter_table_temp (
            'XXEIS',
            'eis_po_xxwc_isr_SVENDOR##b');

         g_sec :=
            'Insert into eis_po_xxwc_isr_SVENDOR##b where vendor number is not null ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_SVENDOR##b ( inventory_item_id)
 select distinct inventory_item_id   FROM xxeis.eis_po_xxwc_isr_svendor##zx  where vendor_number is null';

         COMMIT;
         xxeis.eis_po_xxwc_isr_pkg_v2.create_table_from_other_table (
            'XXEIS',
            'eis_po_xxwc_isr_SVENDOR##b',
            'eis_po_xxwc_isr_SVENDOR##c');

         g_sec := 'Insert into eis_po_xxwc_isr_SVENDOR##c ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_SVENDOR##c
 select  inventory_item_id,  xxeis.eis_po_xxwc_isr_pkg_v2.find_max_vendor_id ( inventory_item_id)
 from   xxeis.eis_po_xxwc_isr_SVENDOR##b';

         COMMIT;
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_SVENDOR##b');

         g_sec := 'Insert into xxeis.eis_po_xxwc_isr_SVENDOR##z ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_SVENDOR##z
SELECT a.vendor_name
      , a.vendor_number
      , a.vendor_site
      , a.inventory_item_id
      , a.organization_id
      , b.vendor_id
  FROM xxeis.eis_po_xxwc_isr_svendor##zx a, xxeis.eis_po_xxwc_isr_SVENDOR##c b  where a.vendor_number is null and a.inventory_item_id =b.inventory_item_id
  ';

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.drop_temp_table (
         'xxeis',
         'eis_po_xxwc_isr_SVENDOR##C');
      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'eis_po_xxwc_isr_SVENDOR##z ,04',
         g_start);

      ----------------------------------------------------------------

      BEGIN
         xxeis.eis_po_xxwc_isr_pkg_v2.create_table_from_other_table (
            'XXEIS',
            'eis_po_xxwc_isr_SVENDOR##z',
            'eis_po_xxwc_isr_SVENDOR##xz');


         g_sec := 'Insert into xxeis.eis_po_xxwc_isr_SVENDOR##xz ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into xxeis.eis_po_xxwc_isr_SVENDOR##xz
SELECT NVL (b.vendor_name
           , (SELECT vendor_name
                FROM po_vendors
               WHERE vendor_id = b.vendor_id))
      ,NVL (b.vendor_number
           , (SELECT segment1
                FROM po_vendors
               WHERE vendor_id = b.vendor_id))
      ,b.vendor_site
      ,b.inventory_item_id
      ,b.organization_id
      ,b.vendor_id
  FROM   xxeis.eis_po_xxwc_isr_svendor##z  b
 ';

         COMMIT;

         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_SVENDOR##z');
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_SVENDOR##x');
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_SVENDOR##zx');
      END;

      --end vendor
      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_po_xxwc_isr_SVENDOR##xz ,26',
         g_start);

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_xxwc_po_isr_tab_v##T');
         create_table_from_other_table ('XXEIS',
                                        'eis_xxwc_po_isr_tab_v##',
                                        'eis_xxwc_po_isr_tab_v##T');

         alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_tab_v##T');

         g_sec := 'Insert into eis_xxwc_po_isr_tab_v##T ';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_xxwc_po_isr_tab_v##T
        SELECT a.org
      ,a.organization_name
      ,a.district
      ,a.region
      ,a.pre
      ,a.item_number
      ,B.vendor_name
      , B.vendor_number
      ,a.source
      ,a.st
      ,a.description
      ,a.cat_class
      ,a.inv_cat_seg1
      ,a.pplt
      ,a.plt
      ,a.uom
      ,a.cl
      ,a.stk
      ,a.pm
      ,a.MIN
      ,a.MAX
      ,a.hit_6
      ,a.aver_cost
      ,a.item_cost
      ,a.bpa_cost
      ,a.bpa
      ,a.qoh
      ,a.on_ord
      ,a.avail
      ,a.avail_d
      ,a.bin_loc
      ,a.mc
      ,a.fi
      ,a.freeze_date
      ,a.res
      ,a.thirteen_wk_avg_inv
      ,a.thirteen_wk_an_cogs
      ,a.turns
      ,a.buyer
      ,a.ts
      ,a.amu
      ,a.so
      ,a.mf_flag
      ,a.wt
      ,a.ss
      ,a.sourcing_rule
      ,a.fml
      ,a.open_req
      ,a.inventory_item_id
      ,a.organization_id
      ,a.set_of_books_id
      ,a.clt
      ,a.avail2
      ,a.supply
      ,a.demand
      ,a.int_req
      ,a.dir_req
      ,a.item_status_code
      ,a.vendor_site
      ,a.source_organization_id
      ,a.source_type
      ,a.site_vendor_num

  FROM xxeis.eis_xxwc_po_isr_tab_v## a, xxeis.eis_po_xxwc_isr_SVENDOR##xz b
 WHERE a.inventory_item_id = b.inventory_item_id
 and a.organization_id =b.organization_id  ';

         COMMIT;
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'ISR',
            'XXEIS.eis_xxwc_po_isr_tab_v##T ,27',
            g_start);


         g_sec :=
            'Insert into eis_xxwc_po_isr_tab_v##T where vendor_number is not null';

         EXECUTE IMMEDIATE
            'insert  /*+ append */ into XXEIS.eis_xxwc_po_isr_tab_v##T
      SELECT *

  FROM xxeis.eis_xxwc_po_isr_tab_v##    WHERE  NVL(ST,''X'')<>''I'' and vendor_number IS not NULL   ';

         COMMIT;
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'ISR',
            'XXEIS.eis_xxwc_po_isr_tab_v##T,28',
            g_start);
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_xxwc_po_isr_tab_v##');

         EXECUTE IMMEDIATE
            'rename eis_xxwc_po_isr_tab_v##T to eis_xxwc_po_isr_tab_v##';
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_po_xxwc_isr_bpa##z');
      END;

      --****************************************************

      DECLARE
         s_ex   CLOB;
      BEGIN
         s_ex := 'CREATE TABLE xxeis.eis_xxwc_po_isr_tab_tmp_##X
(
    org                     VARCHAR2 (240 BYTE)
   ,pre                     VARCHAR2 (240 BYTE)
   ,item_number             VARCHAR2 (240 BYTE)
   ,vendor_num              VARCHAR2 (240 BYTE)
   ,vendor_name             VARCHAR2 (240 BYTE)
   ,source                  VARCHAR2 (240 BYTE)
   ,st                      VARCHAR2 (240 BYTE)
   ,description             VARCHAR2 (450 BYTE)
   ,cat                     VARCHAR2 (240 BYTE)
   ,pplt                    NUMBER
   ,plt                     NUMBER
   ,uom                     VARCHAR2 (240 BYTE)
   ,cl                      VARCHAR2 (240 BYTE)
   ,stk_flag                VARCHAR2 (10 BYTE)
   ,pm                      VARCHAR2 (240 BYTE)
   ,minn                    NUMBER
   ,maxn                    NUMBER
   ,amu                     VARCHAR2 (240 BYTE)
   ,mf_flag                 VARCHAR2 (10 BYTE)
   ,hit6_store_sales        NUMBER
   ,hit6_other_inv_sales    NUMBER
   ,aver_cost               NUMBER
   ,item_cost               NUMBER
   ,bpa                     VARCHAR2 (240 BYTE)
   ,qoh                     NUMBER
   ,available               NUMBER
   ,availabledollar         NUMBER
   ,one_store_sale          NUMBER
   ,six_store_sale          NUMBER
   ,twelve_store_sale       NUMBER
   ,one_other_inv_sale      NUMBER
   ,six_other_inv_sale      NUMBER
   ,twelve_other_inv_sale   NUMBER
   ,bin_loc                 VARCHAR2 (240 BYTE)
   ,mc                      VARCHAR2 (240 BYTE)
   ,fi_flag                 VARCHAR2 (10 BYTE)
   ,freeze_date             DATE
   ,res                     VARCHAR2 (240 BYTE)
   ,thirteen_wk_avg_inv     NUMBER
   ,thirteen_wk_an_cogs     NUMBER
   ,turns                   NUMBER
   ,buyer                   VARCHAR2 (240 BYTE)
   ,ts                      NUMBER
   ,jan_store_sale          NUMBER
   ,feb_store_sale          NUMBER
   ,mar_store_sale          NUMBER
   ,apr_store_sale          NUMBER
   ,may_store_sale          NUMBER
   ,jun_store_sale          NUMBER
   ,jul_store_sale          NUMBER
   ,aug_store_sale          NUMBER
   ,sep_store_sale          NUMBER
   ,oct_store_sale          NUMBER
   ,nov_store_sale          NUMBER
   ,dec_store_sale          NUMBER
   ,jan_other_inv_sale      NUMBER
   ,feb_other_inv_sale      NUMBER
   ,mar_other_inv_sale      NUMBER
   ,apr_other_inv_sale      NUMBER
   ,may_other_inv_sale      NUMBER
   ,jun_other_inv_sale      NUMBER
   ,jul_other_inv_sale      NUMBER
   ,aug_other_inv_sale      NUMBER
   ,sep_other_inv_sale      NUMBER
   ,oct_other_inv_sale      NUMBER
   ,nov_other_inv_sale      NUMBER
   ,dec_other_inv_sale      NUMBER
   ,hit4_store_sales        NUMBER
   ,hit4_other_inv_sales    NUMBER
   ,inventory_item_id       NUMBER
   ,organization_id         NUMBER
   ,set_of_books_id         NUMBER
   ,org_name                VARCHAR2 (150 BYTE)
   ,district                VARCHAR2 (100 BYTE)
   ,region                  VARCHAR2 (100 BYTE)
   ,common_output_id        NUMBER
   ,process_id              NUMBER
   ,inv_cat_seg1            VARCHAR2 (240 BYTE)
   ,on_ord                  NUMBER
   ,bpa_cost                NUMBER
   ,open_req                NUMBER
   ,wt                      NUMBER
   ,fml                     NUMBER
   ,sourcing_rule           VARCHAR2 (450 BYTE)
   ,so                      NUMBER
   ,ss                      NUMBER
   ,clt                     NUMBER
   ,avail2                  NUMBER
   ,int_req                 NUMBER
   ,dir_req                 NUMBER
   ,demand                  NUMBER
   ,item_status_code        VARCHAR2 (50 BYTE)
   ,SITE_VENDOR_NUM         VARCHAR2 (240 BYTE)
   ,vendor_site             VARCHAR2 (240 BYTE)
)';
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_xxwc_po_isr_tab_tmp_##x');

         EXECUTE IMMEDIATE s_ex;

         alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_tab_tmp_##x');

         g_sec := 'Insert into eis_xxwc_po_isr_tab_tmp_##x ';

         s_ex :=
            'INSERT /*+ append */
      INTO  xxeis.eis_xxwc_po_isr_tab_tmp_##x (org
                                              ,pre
                                              ,item_number
                                              ,vendor_num
                                              ,vendor_name
                                              ,source
                                              ,st
                                              ,description
                                              ,cat
                                              ,pplt
                                              ,plt
                                              ,uom
                                              ,cl
                                              ,stk_flag
                                              ,pm
                                              ,minn
                                              ,maxn
                                              ,amu
                                              ,mf_flag
                                              ,hit6_store_sales
                                              ,hit6_other_inv_sales
                                              ,aver_cost
                                              ,item_cost
                                              ,bpa_cost
                                              ,bpa
                                              ,qoh
                                              ,on_ord
                                              ,available
                                              ,availabledollar
                                              ,one_store_sale
                                              ,six_store_sale
                                              ,twelve_store_sale
                                              ,one_other_inv_sale
                                              ,six_other_inv_sale
                                              ,twelve_other_inv_sale
                                              ,bin_loc
                                              ,mc
                                              ,fi_flag
                                              ,freeze_date
                                              ,res
                                              ,thirteen_wk_avg_inv
                                              ,thirteen_wk_an_cogs
                                              ,turns
                                              ,buyer
                                              ,ts
                                              ,jan_store_sale
                                              ,feb_store_sale
                                              ,mar_store_sale
                                              ,apr_store_sale
                                              ,may_store_sale
                                              ,jun_store_sale
                                              ,jul_store_sale
                                              ,aug_store_sale
                                              ,sep_store_sale
                                              ,oct_store_sale
                                              ,nov_store_sale
                                              ,dec_store_sale
                                              ,jan_other_inv_sale
                                              ,feb_other_inv_sale
                                              ,mar_other_inv_sale
                                              ,apr_other_inv_sale
                                              ,may_other_inv_sale
                                              ,jun_other_inv_sale
                                              ,jul_other_inv_sale
                                              ,aug_other_inv_sale
                                              ,sep_other_inv_sale
                                              ,oct_other_inv_sale
                                              ,nov_other_inv_sale
                                              ,dec_other_inv_sale
                                              ,hit4_store_sales
                                              ,hit4_other_inv_sales
                                              ,so
                                              ,inventory_item_id
                                              ,organization_id
                                              ,set_of_books_id
                                              ,org_name
                                              ,district
                                              ,region
                                              ,inv_cat_seg1
                                              ,wt
                                              ,ss
                                              ,fml
                                              ,open_req
                                              ,sourcing_rule
                                              ,clt
                                              ,avail2
                                              ,int_req
                                              ,dir_req
                                              ,demand
                                              ,item_status_code
                                              ,SITE_VENDOR_NUM
                                              ,vendor_site)
    SELECT a.org
          ,a.pre
          ,a.item_number
          , A.vendor_number
          , A.vendor_name
          ,a.source
          ,a.st
          ,a.description
          ,a.cat_class
          ,a.pplt
          ,a.plt
          ,a.uom
          ,a.cl
          ,a.stk
          ,a.pm
          ,a.MIN
          ,a.MAX
          ,a.amu
          ,a.mf_flag
          ,NULL
          ,NULL
          ,a.aver_cost
          ,a.item_cost
          ,a.bpa_cost
          ,a.bpa
          ,a.qoh
          ,a.on_ord
          ,a.avail
          ,a.avail_d
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,a.bin_loc
          ,a.mc
          ,a.fi
          ,a.freeze_date
          ,a.res
          ,a.thirteen_wk_avg_inv
          ,a.thirteen_wk_an_cogs
          ,a.turns
          ,a.buyer
          ,a.ts
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,a.so
          ,a.inventory_item_id
          ,a.organization_id
          ,a.set_of_books_id
          ,a.organization_name
          ,a.district
          ,a.region
          ,a.inv_cat_seg1
          ,a.wt
          ,a.ss
          ,a.fml
          ,a.open_req
          ,a.sourcing_rule
          ,a.clt
          ,a.avail2
          ,a.int_req
          ,a.dir_req
          ,a.demand
          ,a.item_status_code
          ,a.SITE_VENDOR_NUM
          ,a.vendor_site
      FROM XXEIS.eis_xxwc_po_isr_tab_v## A';

         EXECUTE IMMEDIATE s_ex;

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_xxwc_po_isr_tab_tmp_##x ,29',
         g_start);

      --**********************************************************************************************
      DECLARE
         v_month    NUMBER := 0;
         v_insert   CLOB;
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'EIS_XXWC_PERIOD_NAME_TAB##');

         g_sec := 'Insert into EIS_XXWC_PERIOD_NAME_TAB## ';

         EXECUTE IMMEDIATE
            'CREATE TABLE xxeis.EIS_XXWC_PERIOD_NAME_TAB##
AS
    SELECT period_name
          ,period_year
          ,effective_period_num
          , TO_DATE (period_name, ''Mon-YYYY'') period_date ,
          TO_NUMBER(TO_CHAR( TO_DATE (period_name, ''Mon-YYYY'') , ''MM'')) MONTH_NUM, start_date , end_date
      FROM gl_period_statuses
     WHERE     application_id = 101
           AND adjustment_period_flag <> ''Y''
             and trunc(start_date,''MON'')< trunc(sysdate,''MON'')
            AND period_year BETWEEN TO_NUMBER (TO_CHAR (SYSDATE - 731,''YYYY'')) AND TO_NUMBER (TO_CHAR (SYSDATE + 731,''YYYY''))
           AND set_of_books_id = 2061 and 1=2';

         FOR r
            IN (  SELECT period_name,
                         period_year,
                         effective_period_num,
                         TO_DATE (period_name, 'Mon-YYYY') period_date,
                         TO_NUMBER (
                            TO_CHAR (TO_DATE (period_name, 'Mon-YYYY'), 'MM'))
                            month_num,
                         start_date,
                         end_date,
                         period_name hd_prname,
                         closing_status
                    FROM gl_period_statuses
                   WHERE     application_id = 101
                         AND adjustment_period_flag <> 'Y'
                         -- and trunc(start_date,'MON')< trunc(sysdate,'MON')
                         AND period_year BETWEEN TO_NUMBER (
                                                    TO_CHAR (SYSDATE - 731,
                                                             'YYYY'))
                                             AND TO_NUMBER (
                                                    TO_CHAR (SYSDATE + 731,
                                                             'YYYY'))
                         AND set_of_books_id = 2061
                         --AND closing_status IN ('O', 'C')
                         AND end_date < TRUNC (SYSDATE)
                ORDER BY start_date DESC)
         LOOP
            v_month := v_month + 1;

            IF v_month < 13
            THEN
               v_insert := 'INSERT INTO xxeis.eis_xxwc_period_name_tab##
                 VALUES(:period_name
                        ,:period_year
                       ,:effective_period_num
                        ,:period_date
                        ,:month_num
                        ,:start_date
                        ,:end_date)';

               EXECUTE IMMEDIATE v_insert
                  USING r.period_name,
                        r.period_year,
                        r.effective_period_num,
                        r.period_date,
                        r.month_num,
                        r.start_date,
                        r.end_date;
            END IF;
         END LOOP;

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.EIS_XXWC_PERIOD_NAME_TAB## ,30',
         g_start);

      --------------------------------------------------------------------
      --*********************************************************************************************
      DECLARE
         s_ex   CLOB;
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_xxwc_po_isr_sales_tab##');

         EXECUTE IMMEDIATE 'CREATE TABLE xxeis.eis_xxwc_po_isr_sales_tab##
(
    organization_id     NUMBER
   ,inventory_item_id   NUMBER
   ,twelve_store_sale   NUMBER
   ,six_store_sale      NUMBER
   ,one_store_sale      NUMBER
)';

         alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_sales_tab##');

         g_sec := 'Insert into eis_xxwc_po_isr_sales_tab## ';


         s_ex :=
            'INSERT /*+ append */
      INTO  xxeis.eis_xxwc_po_isr_sales_tab##
      SELECT organization_id
            ,inventory_item_id
            ,SUM (twelve_store_sale) twelve_store_sale
            ,SUM (six_store_sale) six_store_sale
            ,SUM (one_store_sale) one_store_sale
        FROM (SELECT organization_id
                    ,ol.inventory_item_id inventory_item_id
                    ,CASE
                         WHEN ol.action_date > TRUNC (SYSDATE - 365)
                         THEN
                             DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)
                     END
                         twelve_store_sale
                    ,CASE
                         WHEN ol.action_date > TRUNC (SYSDATE - 182)
                         THEN
                             DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)
                     END
                         six_store_sale
                    ,CASE
                         WHEN ol.action_date > TRUNC (SYSDATE - 30)
                         THEN
                             DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)
                     END
                         one_store_sale
                FROM eis_xxwc_po_isr_hits_tab3## ol)
    GROUP BY organization_id, inventory_item_id';

         EXECUTE IMMEDIATE s_ex;

         COMMIT;
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'ISR',
            'xxeis.eis_xxwc_po_isr_sales_tab## ,31',
            g_start);
      -- APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', 'eis_xxwc_po_isr_hits_tab3##');
      END;

      --------------------------------------------------------------------------

      DECLARE
         s_ex   CLOB;
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_xxwc_po_isr_osales_tab##');

         EXECUTE IMMEDIATE '
CREATE TABLE xxeis.eis_xxwc_po_isr_osales_tab##
(
    organization_id         NUMBER
   ,inventory_item_id       NUMBER
   ,twelve_other_inv_sale   NUMBER
   ,six_other_inv_sale      NUMBER
   ,one_other_inv_sale      NUMBER
)';

         alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_osales_tab##');

         g_sec := 'Insert into eis_xxwc_po_isr_osales_tab##  ';

         s_ex :=
            'INSERT /*+ append */ into XXEIS.eis_xxwc_po_isr_osales_tab## select    msi.source_organization_id  organization_id
                           ,TAB1.inventory_item_id  inventory_item_id
               ,sum(tab.twelve_store_sale)     twelve_other_inv_sale
                           ,sum(tab.six_store_sale)        six_other_inv_sale
                           ,sum(tab.one_STORE_SALE)        one_other_inv_sale
                      from  xxeis.xxwc_mtl_system_items_b#s#b msi,
                            xxeis.xxwc_mtl_system_items_b#s#c tab1,
                            xxeis.eis_xxwc_po_isr_sales_tab## tab
                       where 1=1
                         and msi.inventory_item_id         = tab1.inventory_item_id
                         and msi.source_organization_id    = tab1.organization_id
                         and msi.inventory_item_id         = tab.inventory_item_id
                         and msi.organization_id           = tab.organization_id
                         AND msi.source_organization_id <> msi.organization_id
                    group by tab1.inventory_item_id
                            ,msi.source_organization_id  ';

         EXECUTE IMMEDIATE s_ex;

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'XXEIS.eis_xxwc_po_isr_osales_tab## ,32',
         g_start);

      DECLARE
         s_ex   CLOB;
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_xxwc_po_isr_psales_tab##');

         EXECUTE IMMEDIATE 'CREATE TABLE xxeis.eis_xxwc_po_isr_psales_tab##
(
    inventory_item_id   NUMBER
   ,organization_id     NUMBER
   ,jan_store_sale      NUMBER
   ,feb_store_sale      NUMBER
   ,mar_store_sale      NUMBER
   ,apr_store_sale      NUMBER
   ,may_store_sale      NUMBER
   ,jun_store_sale      NUMBER
   ,jul_store_sale      NUMBER
   ,aug_store_sale      NUMBER
   ,sep_store_sale      NUMBER
   ,oct_store_sale      NUMBER
   ,nov_store_sale      NUMBER
   ,dec_store_sale      NUMBER
)';

         alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_psales_tab##');

         g_sec := 'Insert into eis_xxwc_po_isr_psales_tab##';

         s_ex :=
            'INSERT /*+ append */ into  xxeis.eis_xxwc_po_isr_psales_tab##
  SELECT tab.inventory_item_id inventory_item_id
        ,tab.organization_id organization_id
        ,SUM (tab.jan1) jan_store_sales
        ,SUM (tab.feb1) feb_store_sales
        ,SUM (tab.mar1) mar_store_sales
        ,SUM (tab.apr1) apr_store_sales
        ,SUM (tab.may1) may_store_sales
        ,SUM (tab.june1) jun_store_sales
        ,SUM (tab.july1) jul_store_sales
        ,SUM (tab.aug1) aug_store_sales
        ,SUM (tab.sept1) sep_store_sales
        ,SUM (tab.oct1) oct_store_sales
        ,SUM (tab.nov1) nov_store_sales
        ,SUM (tab.dec1) dec_store_sales
    FROM (SELECT gps.*
                ,mdh.inventory_item_id
                ,mdh.organization_id
                ,mdh.sales_order_demand + NVL (std_wip_usage, 0)
                ,CASE WHEN gps.month_num = 1 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END jan1
                ,CASE WHEN gps.month_num = 2 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END feb1
                ,CASE WHEN gps.month_num = 3 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END mar1
                ,CASE WHEN gps.month_num = 4 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END apr1
                ,CASE WHEN gps.month_num = 5 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END may1
                ,CASE WHEN gps.month_num = 6 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END june1
                ,CASE WHEN gps.month_num = 7 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END july1
                ,CASE WHEN gps.month_num = 8 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END aug1
                ,CASE WHEN gps.month_num = 9 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END sept1
                ,CASE WHEN gps.month_num = 10 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END oct1
                ,CASE WHEN gps.month_num = 11 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END nov1
                ,CASE WHEN gps.month_num = 12 THEN mdh.sales_order_demand + NVL (std_wip_usage, 0) END dec1
            FROM mtl_demand_histories mdh, xxeis.EIS_XXWC_PERIOD_NAME_TAB## gps
           WHERE 1 = 1 AND mdh.period_type = 3 AND mdh.period_start_date BETWEEN gps.start_date AND gps.end_date) tab
GROUP BY tab.inventory_item_id, tab.organization_id';

         EXECUTE IMMEDIATE s_ex;

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_xxwc_po_isr_psales_tab## ,33',
         g_start);

      DECLARE
         s_ex   CLOB;
      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_xxwc_po_isr_posales_tab##');

         EXECUTE IMMEDIATE 'CREATE TABLE xxeis.eis_xxwc_po_isr_posales_tab##
(
    inventory_item_id    NUMBER
   ,organization_id      NUMBER
   ,jan_other_inv_sale   NUMBER
   ,feb_other_inv_sale   NUMBER
   ,mar_other_inv_sale   NUMBER
   ,apr_other_inv_sale   NUMBER
   ,may_other_inv_sale   NUMBER
   ,jun_other_inv_sale   NUMBER
   ,jul_other_inv_sale   NUMBER
   ,aug_other_inv_sale   NUMBER
   ,sep_other_inv_sale   NUMBER
   ,oct_other_inv_sale   NUMBER
   ,nov_other_inv_sale   NUMBER
   ,dec_other_inv_sale   NUMBER
)';

         alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_posales_tab##');

         g_sec := 'Insert into eis_xxwc_po_isr_posales_tab## ';

         s_ex :=
            'INSERT /*+ append */ into  xxeis.eis_xxwc_po_isr_posales_tab## SELECT tab1.inventory_item_id inventory_item_id
        ,tab1.organization_id organization_id
        ,SUM (tab.jan_store_sale) jan_store_other_sales
        ,SUM (tab.feb_store_sale) feb_store_other_sales
        ,SUM (tab.mar_store_sale) mar_store_other_sales
        ,SUM (tab.apr_store_sale) apr_store_other_sales
        ,SUM (tab.may_store_sale) may_store_other_sales
        ,SUM (tab.jun_store_sale) jun_store_other_sales
        ,SUM (tab.jul_store_sale) jul_store_other_sales
        ,SUM (tab.aug_store_sale) aug_store_other_sales
        ,SUM (tab.sep_store_sale) sep_store_other_sales
        ,SUM (tab.oct_store_sale) oct_store_other_sales
        ,SUM (tab.nov_store_sale) nov_store_other_sales
        ,SUM (tab.dec_store_sale) dec_store_other_sales
    FROM xxeis.xxwc_mtl_system_items_b#s#b msi, xxeis.xxwc_mtl_system_items_b#s#c tab1, xxeis.eis_xxwc_po_isr_psales_tab## tab
   WHERE     1 = 1
         AND msi.inventory_item_id = tab1.inventory_item_id
         AND msi.source_organization_id = tab1.organization_id
         AND msi.inventory_item_id = tab.inventory_item_id
         AND msi.organization_id = tab.organization_id
         AND msi.source_organization_id <> msi.organization_id
GROUP BY tab1.inventory_item_id, tab1.organization_id';

         EXECUTE IMMEDIATE s_ex;

         COMMIT;
      END;

      apps.xxwc_common_tunning_helpers.elapsed_time (
         'ISR',
         'xxeis.eis_xxwc_po_isr_posales_tab## ,34',
         g_start);

      DECLARE
         v_clob   CLOB;
      BEGIN
         /*
          -- This was commented out and the table was switched to a permanent
          -- table name, eis_xxwc_po_isr_tab_total

                  APPS.xxwc_common_tunning_helpers.drop_temp_table ('xxeis', '');
                    v_clob := 'CREATE TABLE xxeis.eis_xxwc_po_isr_tab##total
        (
            org                     VARCHAR2 (240 BYTE)
           ,pre                     VARCHAR2 (240 BYTE)
           ,item_number             VARCHAR2 (240 BYTE)
           ,vendor_num              VARCHAR2 (240 BYTE)
           ,vendor_name             VARCHAR2 (240 BYTE)
           ,source                  VARCHAR2 (240 BYTE)
           ,st                      VARCHAR2 (240 BYTE)
           ,description             VARCHAR2 (450 BYTE)
           ,cat                     VARCHAR2 (240 BYTE)
           ,pplt                    NUMBER
           ,plt                     NUMBER
           ,uom                     VARCHAR2 (240 BYTE)
           ,cl                      VARCHAR2 (240 BYTE)
           ,stk_flag                VARCHAR2 (10 BYTE)
           ,pm                      VARCHAR2 (240 BYTE)
           ,minn                    NUMBER
           ,maxn                    NUMBER
           ,amu                     VARCHAR2 (240 BYTE)
           ,mf_flag                 VARCHAR2 (10 BYTE)
           ,hit6_store_sales        NUMBER
           ,hit6_other_inv_sales    NUMBER
           ,aver_cost               NUMBER
           ,item_cost               NUMBER
           ,bpa                     VARCHAR2 (240 BYTE)
           ,qoh                     NUMBER
           ,available               NUMBER
           ,availabledollar         NUMBER
           ,one_store_sale          NUMBER
           ,six_store_sale          NUMBER
           ,twelve_store_sale       NUMBER
           ,one_other_inv_sale      NUMBER
           ,six_other_inv_sale      NUMBER
           ,twelve_other_inv_sale   NUMBER
           ,bin_loc                 VARCHAR2 (240 BYTE)
           ,mc                      VARCHAR2 (240 BYTE)
           ,fi_flag                 VARCHAR2 (10 BYTE)
           ,freeze_date             DATE
           ,res                     VARCHAR2 (240 BYTE)
           ,thirteen_wk_avg_inv     NUMBER
           ,thirteen_wk_an_cogs     NUMBER
           ,turns                   NUMBER
           ,buyer                   VARCHAR2 (240 BYTE)
           ,ts                      NUMBER
           ,jan_store_sale          NUMBER
           ,feb_store_sale          NUMBER
           ,mar_store_sale          NUMBER
           ,apr_store_sale          NUMBER
           ,may_store_sale          NUMBER
           ,jun_store_sale          NUMBER
           ,jul_store_sale          NUMBER
           ,aug_store_sale          NUMBER
           ,sep_store_sale          NUMBER
           ,oct_store_sale          NUMBER
           ,nov_store_sale          NUMBER
           ,dec_store_sale          NUMBER
           ,jan_other_inv_sale      NUMBER
           ,feb_other_inv_sale      NUMBER
           ,mar_other_inv_sale      NUMBER
           ,apr_other_inv_sale      NUMBER
           ,may_other_inv_sale      NUMBER
           ,jun_other_inv_sale      NUMBER
           ,jul_other_inv_sale      NUMBER
           ,aug_other_inv_sale      NUMBER
           ,sep_other_inv_sale      NUMBER
           ,oct_other_inv_sale      NUMBER
           ,nov_other_inv_sale      NUMBER
           ,dec_other_inv_sale      NUMBER
           ,hit4_store_sales        NUMBER
           ,hit4_other_inv_sales    NUMBER
           ,inventory_item_id       NUMBER
           ,organization_id         NUMBER
           ,set_of_books_id         NUMBER
           ,org_name                VARCHAR2 (150 BYTE)
           ,district                VARCHAR2 (100 BYTE)
           ,region                  VARCHAR2 (100 BYTE)
           ,common_output_id        NUMBER
           ,process_id              NUMBER
           ,inv_cat_seg1            VARCHAR2 (240 BYTE)
           ,on_ord                  NUMBER
           ,bpa_cost                NUMBER
           ,open_req                NUMBER
           ,wt                      NUMBER
           ,fml                     NUMBER
           ,sourcing_rule           VARCHAR2 (450 BYTE)
           ,so                      NUMBER
           ,ss                      NUMBER
           ,clt                     NUMBER
           ,avail2                  NUMBER
           ,int_req                 NUMBER
           ,dir_req                 NUMBER
           ,demand                  NUMBER
           ,item_status_code        VARCHAR2 (50 BYTE)
           ,site_vendor_num         VARCHAR2 (240 BYTE)
           ,vendor_site             VARCHAR2 (240 BYTE)
        )';*/
         -- v_clob := 'truncate TABLE xxeis.eis_xxwc_po_isr_tab_total';---MOVED  INTO MAIN

         --EXECUTE IMMEDIATE v_clob;
         --alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_tab_total');

         g_sec := 'Insert into eis_xxwc_po_isr_tab_total ';

         COMMIT;

         v_clob := 'INSERT /*+append*/
      INTO XXEIS.eis_xxwc_po_isr_tab_total SELECT ORG ,
    PRE ,
    ITEM_NUMBER ,
    VENDOR_NUM ,
    VENDOR_NAME ,
    SOURCE ,
    ST ,
    DESCRIPTION ,
    CAT ,
    PPLT ,
    PLT ,
    UOM ,
    CL ,
    STK_FLAG ,
    PM ,
    MINN ,
    MAXN ,
    AMU ,
    MF_FLAG ,
    ht.HIT6_STORE_SALES ,
    oht.HIT6_OTHER_INV_SALES ,
    AVER_COST ,
    ITEM_COST ,
    BPA ,
    QOH ,
    AVAILABLE ,
    AVAILABLEDOLLAR ,
    st.ONE_STORE_SALE ,
    st.SIX_STORE_SALE ,
    st.TWELVE_STORE_SALE ,
    ost.ONE_OTHER_INV_SALE ,
    ost.SIX_OTHER_INV_SALE ,
    ost.TWELVE_OTHER_INV_SALE ,
    BIN_LOC ,
    MC ,
    FI_FLAG ,
    FREEZE_DATE ,
    RES ,
    THIRTEEN_WK_AVG_INV ,
    THIRTEEN_WK_AN_COGS ,
    TURNS ,
    BUYER ,
    TS ,
    PT.JAN_STORE_SALE ,
    PT.FEB_STORE_SALE ,
    PT.MAR_STORE_SALE ,
    PT.APR_STORE_SALE ,
    PT.MAY_STORE_SALE ,
    PT.JUN_STORE_SALE ,
    PT.JUL_STORE_SALE ,
    PT.AUG_STORE_SALE ,
    PT.SEP_STORE_SALE ,
    PT.OCT_STORE_SALE ,
    PT.NOV_STORE_SALE ,
    PT.DEC_STORE_SALE ,
    opt.JAN_OTHER_INV_SALE ,
    opt.FEB_OTHER_INV_SALE ,
    opt.MAR_OTHER_INV_SALE ,
    opt.APR_OTHER_INV_SALE ,
    opt.MAY_OTHER_INV_SALE ,
    opt.JUN_OTHER_INV_SALE ,
    opt.JUL_OTHER_INV_SALE ,
    opt.AUG_OTHER_INV_SALE ,
    opt.SEP_OTHER_INV_SALE ,
    opt.OCT_OTHER_INV_SALE ,
    opt.NOV_OTHER_INV_SALE ,
    opt.DEC_OTHER_INV_SALE ,
    ht.HIT4_STORE_SALES ,
    oht.HIT4_OTHER_INV_SALES ,
    it.INVENTORY_ITEM_ID ,
    it.ORGANIZATION_ID ,
    SET_OF_BOOKS_ID ,
    ORG_NAME ,
    DISTRICT ,
    REGION ,
    COMMON_OUTPUT_ID ,
    PROCESS_ID ,
    INV_CAT_SEG1 ,
    ON_ORD ,
    BPA_COST ,
    OPEN_REQ ,
    WT ,
    FML ,
    SOURCING_RULE ,
    SO ,
    SS ,
    CLT ,
    AVAIL2 ,
    INT_REQ ,
    DIR_REQ ,
    DEMAND ,
    ITEM_STATUS_CODE ,
    SITE_VENDOR_NUM ,
    VENDOR_SITE
  FROM xxeis.eis_xxwc_po_isr_tab_tmp_##x it,
    xxeis.eis_xxwc_po_isr_psales_tab## pt,
    xxeis.eis_xxwc_po_isr_posales_tab## opt,
    xxeis.eis_xxwc_po_isr_hits_tab## ht,
    xxeis.eis_xxwc_po_isr_ohits_tab## oht,
    xxeis.eis_xxwc_po_isr_sales_tab## st,
    xxeis.eis_xxwc_po_isr_osales_tab## ost
  WHERE 1                  =1
  AND it.organization_id   = pt.organization_id(+)
  AND it.inventory_item_id = pt.inventory_item_id(+)
  AND it.organization_id   = opt.organization_id(+)
  AND it.inventory_item_id = opt.inventory_item_id(+)
  AND it.organization_id   = ht.organization_id(+)
  AND it.inventory_item_id = ht.inventory_item_id(+)
  AND it.organization_id   = oht.organization_id(+)
  AND it.inventory_item_id = oht.inventory_item_id(+)
  AND it.organization_id   = st.organization_id(+)
  AND it.inventory_item_id = st.inventory_item_id(+)
  AND it.organization_id   = ost.organization_id(+)
  AND it.inventory_item_id = ost.inventory_item_id(+)';

         EXECUTE IMMEDIATE v_clob;

         COMMIT;
         apps.xxwc_common_tunning_helpers.elapsed_time (
            'ISR',
            'XXEIS.eis_xxwc_po_isr_tab_total ,35',
            g_start);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_message :=
               'xxeis.eis_po_xxwc_isr_pkg_v2'
            || 'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || 'Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (l_error_message);
         apps.xxwc_common_tunning_helpers.write_log (l_error_message);
         --   RAISE;

         -- Below Line uncommented by Manjula on 1-Aug-14 for TMS 20140728-00130

         apps.xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_procedure_name,
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          'Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || 'Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_errbuf, 1, 240),
            p_distribution_list   => pl_dflt_email,
            p_module              => 'INV');
   -- p_retcode := 2;
   -- p_errbuf := l_error_message;
   END;

   --*********************************************************************************
   --*********************************************************************************
   --*********************************************************************************
   --*********************************************************************************
   --*********************************************************************************

   /*   BEGIN
          EXECUTE IMMEDIATE 'CREATE INDEX XXEIS.XXWC_PO_ISR_TAB_N1 ON XXEIS.EIS_XXWC_PO_ISR_TAB
            (
        INVENTORY_ITEM_ID,
              ORGANIZATION_ID
            ) TABLESPACE XXEIS_IDX';
      EXCEPTION
          WHEN OTHERS
          THEN
              NULL;
      END;*/
   --MAIN

   ------

   --***********************************************************

   PROCEDURE build_index
   IS
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.build_index';

      EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_tab';

      alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_tab');

      EXECUTE IMMEDIATE
         'INSERT /*+ append */ into   xxeis.eis_xxwc_po_isr_tab select *FROM xxeis.eis_xxwc_po_isr_tab_total';

      COMMIT;

      BEGIN
         EXECUTE IMMEDIATE 'drop index XXEIS.XXWC_PO_ISR_TAB_N1';
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      BEGIN
         EXECUTE IMMEDIATE
            'CREATE INDEX XXEIS.XXWC_PO_ISR_TAB_N1 ON XXEIS.eis_xxwc_po_isr_tab
              (
          INVENTORY_ITEM_ID,
                ORGANIZATION_ID
              ) TABLESPACE XXEIS_IDX';

         DBMS_STATS.gather_index_stats (ownname   => 'XXEIS',
                                        indname   => 'XXWC_PO_ISR_TAB_N1');
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;
   END;

   ---********************************************************
   FUNCTION find_max_vendor_id (p_inventory_item_id NUMBER)
      RETURN NUMBER
   IS
      v_vendor_id   NUMBER := NULL;
      s_string      CLOB;
   BEGIN
      s_string :=
         'SELECT vendor_id
              FROM (  SELECT COUNT (sso.vendor_id) cnt, sso.vendor_id
-- Commented By Manjula on 17-Dec-14 TMS # 20140908-00200 Updated BPA Logic 
-- to select data only for the assignment set WC Default                
                        --FROM mrp_sr_assignments ass
                        FROM (SELECT * FROM mrp_sr_assignments WHERE assignment_type = 6 AND assignment_set_id = 1 ) ass  
                            ,mrp_sr_receipt_org rco
                            ,mrp_sr_source_org sso
                            ,xxeis.xxwc_mtl_system_items_b#s#a msi
                       WHERE     1 = 1
                             AND msi.inventory_item_id = ass.inventory_item_id
                             AND msi.organization_id = ass.organization_id
                             AND ass.inventory_item_id = :p_inventory_item_id
                             AND msi.source_type = 2
                             AND rco.sourcing_rule_id = ass.sourcing_rule_id
                             AND sso.sr_receipt_id = rco.sr_receipt_id
                             AND sso.source_type = 3
                    GROUP BY sso.vendor_id
                    ORDER BY 1 DESC) a
             WHERE ROWNUM <= 1';

      EXECUTE IMMEDIATE s_string INTO v_vendor_id USING p_inventory_item_id;

      IF v_vendor_id IS NOT NULL
      THEN
         RETURN v_vendor_id;
      END IF;

      RETURN v_vendor_id;
   END;

   --***********************************************
   /*
       This procedure is what populates data for a specific report instance. It is called
       in the EIS pre-report trigger and receives the process ID as a parameter. It is
       called each time a user runs an ISR report and it puts data into the eis_rs_common_outputs
       table for report generation. The EIS framework appears to manage purging that table.
   */
   PROCEDURE isr_rpt_proc (p_process_id            IN NUMBER,
                           p_region                IN VARCHAR2,
                           p_district              IN VARCHAR2,
                           p_location              IN VARCHAR2,
                           p_dc_mode               IN VARCHAR2,
                           p_tool_repair           IN VARCHAR2,
                           p_time_sensitive        IN VARCHAR2,
                           p_stk_items_with_hit4   IN VARCHAR2,
                           p_report_condition      IN VARCHAR2,
                           p_report_criteria       IN VARCHAR2,
                           p_report_criteria_val   IN VARCHAR2,
                           p_start_bin_loc         IN VARCHAR2,
                           p_end_bin_loc           IN VARCHAR2,
                           p_vendor                IN VARCHAR2,
                           p_item                  IN VARCHAR2,
                           p_cat_class             IN VARCHAR2,
                           p_org_list              IN VARCHAR2,
                           p_item_list             IN VARCHAR2,
                           p_supplier_list         IN VARCHAR2,
                           p_cat_class_list        IN VARCHAR2,
                           p_source_list           IN VARCHAR2,
                           p_intangibles           IN VARCHAR2)
   IS
      l_query               VARCHAR2 (32000);
      l_condition_str       VARCHAR2 (32000);
      lv_program_location   VARCHAR2 (2000);
      l_ref_cursor          cursor_type;
      l_insert_recd_cnt     NUMBER;
      l_supplier_exists     VARCHAR2 (32767);
      l_org_exists          VARCHAR2 (32000);
      l_item_exists         VARCHAR2 (32000);
      l_catclass_exists     VARCHAR2 (32000);
      l_source_exists       VARCHAR2 (32000);
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.isr_rpt_proc';

      IF p_region IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and REGION in ('
            || xxeis.eis_rs_utility.get_param_values (p_region)
            || ')';
      END IF;

      IF p_district IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and DISTRICT in ('
            || xxeis.eis_rs_utility.get_param_values (p_district)
            || ')';
      END IF;

      IF p_location IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and ORG in ('
            || xxeis.eis_rs_utility.get_param_values (p_location)
            || ')';
      /*  l_location_exists:='And exists (select 1 from apps.XXWC_PARAM_LIST
                                  where list_name=''ORG''
                                   and list_values like '''||'%'||p_location||'%'||'''
                                   )';
    ELSE
     l_location_exists:='and 1=1';*/
      END IF;

      IF p_vendor IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and VENDOR_NUM in ('
            || xxeis.eis_rs_utility.get_param_values (p_vendor)
            || ')';
      /* l_vendor_exists :='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''VENDOR''
                                     and list_values like '''||'%'||p_vendor||'%'||'''
                                     )';
        FND_FILE.PUT_LINE(FND_FILE.LOG,'l_vendor_exists is '||L_VENDOR_EXISTS);
        ELSE
        l_vendor_exists:='and 1=1';*/
      END IF;

      IF p_item IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and msi.concatenated_segments in ('
            || xxeis.eis_rs_utility.get_param_values (p_item)
            || ')';
      /* L_ITEM_EXISTS:='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''ITEM''
                                     and list_values like '''||'%'||p_item||'%'||'''
                                     )';
       ELSE
        L_ITEM_EXISTS:='and 1=1';*/

      END IF;

      IF p_cat_class IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and CAT in ('
            || xxeis.eis_rs_utility.get_param_values (p_cat_class)
            || ')';
      END IF;

      /*  If P_Dc_Mode Is Not Null
        l_condition_str:= l_condition_str:=||'and ORG_NAME ='P_Location;
        Then
        End If;*/
      IF (p_intangibles = 'Exclude')
      THEN
         l_condition_str :=
               l_condition_str
            || 'and nvl(ITEM_STATUS_CODE,''Active'') <> ''Intangible''';
      END IF;

      IF (p_tool_repair = 'Tool Repair Only' OR p_tool_repair LIKE 'Tool%')
      THEN
         l_condition_str := l_condition_str || 'and CAT = ''TH01''';
      END IF;

      IF p_tool_repair = 'Exclude'
      THEN
         l_condition_str := l_condition_str || 'and CAT <> ''TH01''';
      END IF;

      IF p_report_condition = 'All items'
      THEN
         l_condition_str := l_condition_str || 'and 1=1 ';
      END IF;

      IF p_report_condition = 'All items on hand'
      THEN
         l_condition_str := l_condition_str || 'and QOH > 0 ';
      END IF;

      IF (   p_report_condition = 'Non-stock on hand'
          OR p_report_condition LIKE 'Non-%')
      THEN
         l_condition_str :=
            l_condition_str || 'and STK_FLAG  =''N''' || ' and QOH > 0 ';
      END IF;

      IF (   p_report_condition = 'Non stock only'
          OR (    p_report_condition LIKE 'Non%'
              AND p_report_condition NOT LIKE 'Non-%'))
      THEN
         l_condition_str := l_condition_str || 'and STK_FLAG =''N'' ';
      END IF;

      IF p_report_condition = 'Active large'
      THEN
         l_condition_str :=
               l_condition_str
            || 'and STK_FLAG in (''N'',''Y'') '
            || 'and (QOH > 0 or twelve_sales > 0 or on_ord > 0 or Available > 0 or Available < 0)';
      END IF;

      IF p_report_condition = 'Active small'
      THEN
         l_condition_str :=
               l_condition_str
            || 'AND (( STK_FLAG =''Y'' OR (STK_FLAG =''N'' and (QOH > 0 or on_ord > 0 or open_req > 0 or  int_req > 0 or demand > 0 or dir_req > 0 or Available > 0 )))'
            || 'AND ((NOT EXISTS (select 1 from dual where REGEXP_LIKE(substr(item_number,1,1),''[a-zA-Z]''))) or item_number like ''SP%''))';
      ---l_condition_str:= l_condition_str||'and STK_FLAG in (''N'',''Y'') '||' and (QOH > 0 or on_ord > 0 or Available > 0 or Available < 0)';
      END IF;

      IF p_report_condition = 'Stock items only'
      THEN
         l_condition_str := l_condition_str || 'and STK_FLAG =''Y''';
      END IF;

      IF p_report_condition = 'Stock items with 0/0 min/max'
      THEN
         l_condition_str :=
               l_condition_str
            || 'and STK_FLAG =''Y'''
            || 'and MINN=0 and MAXN=0';
      END IF;

      IF (   p_time_sensitive = 'Time Sensitive Only'
          OR p_time_sensitive LIKE 'Time%')
      THEN
         l_condition_str := l_condition_str || 'and TS > 0 and TS < 4000';
      END IF;

      /* Removing and STK_FLAG =''Y'''|| ' because this no longer filters for stock parts- 20130904-01108 */
      IF p_stk_items_with_hit4 IS NOT NULL
      THEN
         l_condition_str :=
            l_condition_str || 'and hit4_sales >= ' || p_stk_items_with_hit4;
      END IF;

      IF p_org_list IS NOT NULL
      THEN
         --insert into xxeis.log values ('Start P_ORG_LIST'); commit;
         xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id,
                                                          p_org_list,
                                                          'Org');
         l_org_exists :=
               'AND EXISTS (SELECT 1
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME  = '''
            || REPLACE (p_org_list, '''', '')
            || '''
                                         AND x.LIST_TYPE     =''Org''
                                         AND x.process_id    = '
            || p_process_id
            || '
                                         AND TAB.ORG = X.list_value) ';
      ELSE
         l_org_exists := 'and 1=1 ';
      END IF;

      IF p_supplier_list IS NOT NULL
      THEN
         xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id,
                                                          p_supplier_list,
                                                          'Supplier');
         l_supplier_exists :=
               'AND EXISTS (SELECT 1
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''
            || REPLACE (p_supplier_list, '''', '')
            || '''
                                         AND x.LIST_TYPE    =''Supplier''
                                         AND x.process_id   = '
            || p_process_id
            || '
                                         AND TAB.vendor_num = X.list_value) ';
      ELSE
         l_supplier_exists := 'and 1=1 ';
      END IF;

      IF p_item_list IS NOT NULL
      THEN
         xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id,
                                                          p_item_list,
                                                          'Item');
         l_item_exists :=
               'AND EXISTS (SELECT 1
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''
            || REPLACE (p_item_list, '''', '')
            || '''
                                         AND x.LIST_TYPE    =''Item''
                                         AND x.process_id   = '
            || p_process_id
            || '
                                         AND TAB.item_number = X.list_value ) ';
      ELSE
         l_item_exists := 'and 1=1 ';
      END IF;

      IF p_cat_class_list IS NOT NULL
      THEN
         xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id,
                                                          p_cat_class_list,
                                                          'Cat Class');
         l_item_exists :=
               'AND EXISTS (SELECT 1
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''
            || REPLACE (p_cat_class_list, '''', '')
            || '''
                                         AND x.LIST_TYPE    =''Cat Class''
                                         AND x.process_id   = '
            || p_process_id
            || '
                                         AND TAB.cat        = X.list_value) ';
      ELSE
         l_catclass_exists := 'and 1=1 ';
      END IF;

      IF p_source_list IS NOT NULL
      THEN
         xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id,
                                                          p_source_list,
                                                          'Source');
         l_item_exists :=
               'AND EXISTS (SELECT 1
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''
            || REPLACE (p_source_list, '''', '')
            || '''
                                         AND x.LIST_TYPE    =''Source''
                                         AND x.process_id   = '
            || p_process_id
            || '
                                         AND TAB.source     = X.list_value) ';
      ELSE
         l_source_exists := 'and 1=1 ';
      END IF;

      IF p_report_criteria IS NOT NULL AND p_report_criteria_val IS NOT NULL
      THEN
         IF (   p_report_criteria = 'Vendor Number(%)'
             OR p_report_criteria LIKE 'Vendor%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(VENDOR_NUM) like '''
               || UPPER (p_report_criteria_val)
               || '''';
            fnd_file.put_line (fnd_file.LOG, l_condition_str);
         END IF;

         IF (   p_report_criteria = '3 Digit Prefix'
             OR p_report_criteria LIKE '3%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(PRE) = '''
               || UPPER (p_report_criteria_val)
               || '''';
         END IF;

         IF (   p_report_criteria = 'Item number'
             OR p_report_criteria LIKE 'Item%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(ITEM_NUMBER) = '''
               || UPPER (p_report_criteria_val)
               || '''';
         END IF;

         IF     p_report_criteria = 'Source'
            AND p_report_criteria_val IS NOT NULL
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(SOURCE) = '''
               || UPPER (p_report_criteria_val)
               || '''';
            NULL;
         END IF;

         IF p_report_criteria = 'External source vendor'
         THEN
            -- L_Condition_Str:= L_Condition_Str||'and ITEM_NUMBER = '''||P_Report_Criteria_Val||'';
            NULL;
         END IF;

         IF (p_report_criteria = '2 Digit Cat' OR p_report_criteria LIKE '2%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(inv_cat_seg1) = '''
               || UPPER (p_report_criteria_val)
               || '''';
            NULL;
         END IF;

         IF (   p_report_criteria = '4 Digit Cat Class'
             OR p_report_criteria LIKE '4%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(CAT) = '''
               || UPPER (p_report_criteria_val)
               || '''';
         END IF;

         IF (   p_report_criteria = 'Default Buyer(%)'
             OR p_report_criteria LIKE 'Default%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(Buyer) like '''
               || UPPER (p_report_criteria_val)
               || '''';                                 --commented by santosh
         -- l_condition_str:= l_condition_str||'and upper(Buyer) like '''||upper(replace(p_report_criteria_val,''''))||''''; --commented by santosh
         --             L_CONDITION_STR:= L_CONDITION_STR||'and upper(Buyer) in ('''||UPPER(P_REPORT_CRITERIA_VAL)||''')'; -- added by santosh
         --       L_CONDITION_STR:= L_CONDITION_STR||'and upper(Buyer) in ('''||UPPER(replace(replace(P_REPORT_CRITERIA_VAL,'''',''),',',', '))||''')'; -- added by santosh

         --l_condition_str:= l_condition_str||'and upper(Buyer) like '''||upper(XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_report_criteria_val))||'''';
         END IF;
      END IF;

      IF p_start_bin_loc IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and upper(Bin_Loc) >= '''
            || UPPER (p_start_bin_loc)
            || '''';
      END IF;

      IF p_end_bin_loc IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and upper(Bin_Loc) <= '''
            || UPPER (p_end_bin_loc)
            || '''';
      END IF;

      /*   IF p_all_items ='Yes'   THEN
         l_condition_str:= 'and 1=1';
         END IF;
  */

      l_query :=
            'select tab.*
             from
             xxeis.EIS_XXWC_PO_ISR_V tab,
             mtl_system_items_kfv msi
             where 1=1
             and msi.inventory_item_id = tab.inventory_item_id
             and msi.organization_id   = tab.organization_id  '
         || l_condition_str
         || l_org_exists
         || l_supplier_exists
         || l_item_exists
         || l_catclass_exists
         || l_source_exists;
      lv_program_location := 'The main query is ' || l_query;
      apps.xxwc_common_tunning_helpers.write_log (lv_program_location);
      l_insert_recd_cnt := 1;

      OPEN l_ref_cursor FOR l_query;

      LOOP
         FETCH l_ref_cursor INTO g_view_record;

         EXIT WHEN l_ref_cursor%NOTFOUND;
         g_view_tab (l_insert_recd_cnt).org := g_view_record.org;
         g_view_tab (l_insert_recd_cnt).pre := g_view_record.pre;
         g_view_tab (l_insert_recd_cnt).item_number :=
            g_view_record.item_number;
         g_view_tab (l_insert_recd_cnt).vendor_num := g_view_record.vendor_num;
         g_view_tab (l_insert_recd_cnt).vendor_name :=
            g_view_record.vendor_name;
         g_view_tab (l_insert_recd_cnt).source := g_view_record.source;
         g_view_tab (l_insert_recd_cnt).st := g_view_record.st;
         g_view_tab (l_insert_recd_cnt).description :=
            g_view_record.description;
         g_view_tab (l_insert_recd_cnt).cat := g_view_record.cat;
         g_view_tab (l_insert_recd_cnt).pplt := g_view_record.pplt;
         g_view_tab (l_insert_recd_cnt).plt := g_view_record.plt;
         g_view_tab (l_insert_recd_cnt).uom := g_view_record.uom;
         g_view_tab (l_insert_recd_cnt).cl := g_view_record.cl;
         g_view_tab (l_insert_recd_cnt).stk_flag := g_view_record.stk_flag;
         g_view_tab (l_insert_recd_cnt).pm := g_view_record.pm;
         g_view_tab (l_insert_recd_cnt).minn := g_view_record.minn;
         g_view_tab (l_insert_recd_cnt).maxn := g_view_record.maxn;
         g_view_tab (l_insert_recd_cnt).amu := g_view_record.amu;
         g_view_tab (l_insert_recd_cnt).mf_flag := g_view_record.mf_flag;
         g_view_tab (l_insert_recd_cnt).hit6_sales := g_view_record.hit6_sales;
         g_view_tab (l_insert_recd_cnt).aver_cost := g_view_record.aver_cost;
         g_view_tab (l_insert_recd_cnt).item_cost := g_view_record.item_cost;
         g_view_tab (l_insert_recd_cnt).bpa := g_view_record.bpa;
         g_view_tab (l_insert_recd_cnt).bpa_cost := g_view_record.bpa_cost;
         g_view_tab (l_insert_recd_cnt).qoh := g_view_record.qoh;
         g_view_tab (l_insert_recd_cnt).available := g_view_record.available;
         g_view_tab (l_insert_recd_cnt).availabledollar :=
            g_view_record.availabledollar;
         g_view_tab (l_insert_recd_cnt).jan_sales := g_view_record.jan_sales;
         g_view_tab (l_insert_recd_cnt).feb_sales := g_view_record.feb_sales;
         g_view_tab (l_insert_recd_cnt).mar_sales := g_view_record.mar_sales;
         g_view_tab (l_insert_recd_cnt).apr_sales := g_view_record.apr_sales;
         g_view_tab (l_insert_recd_cnt).may_sales := g_view_record.may_sales;
         g_view_tab (l_insert_recd_cnt).june_sales := g_view_record.june_sales;
         g_view_tab (l_insert_recd_cnt).jul_sales := g_view_record.jul_sales;
         g_view_tab (l_insert_recd_cnt).aug_sales := g_view_record.aug_sales;
         g_view_tab (l_insert_recd_cnt).sep_sales := g_view_record.sep_sales;
         g_view_tab (l_insert_recd_cnt).oct_sales := g_view_record.oct_sales;
         g_view_tab (l_insert_recd_cnt).nov_sales := g_view_record.nov_sales;
         g_view_tab (l_insert_recd_cnt).dec_sales := g_view_record.dec_sales;
         g_view_tab (l_insert_recd_cnt).hit4_sales := g_view_record.hit4_sales;
         g_view_tab (l_insert_recd_cnt).one_sales := g_view_record.one_sales;
         g_view_tab (l_insert_recd_cnt).six_sales := g_view_record.six_sales;
         g_view_tab (l_insert_recd_cnt).twelve_sales :=
            g_view_record.twelve_sales;
         g_view_tab (l_insert_recd_cnt).bin_loc := g_view_record.bin_loc;
         g_view_tab (l_insert_recd_cnt).mc := g_view_record.mc;
         g_view_tab (l_insert_recd_cnt).fi_flag := g_view_record.fi_flag;
         g_view_tab (l_insert_recd_cnt).freeze_date :=
            g_view_record.freeze_date;
         g_view_tab (l_insert_recd_cnt).res := g_view_record.res;
         g_view_tab (l_insert_recd_cnt).thirteen_wk_avg_inv :=
            g_view_record.thirteen_wk_avg_inv;
         g_view_tab (l_insert_recd_cnt).thirteen_wk_an_cogs :=
            g_view_record.thirteen_wk_an_cogs;
         g_view_tab (l_insert_recd_cnt).turns := g_view_record.turns;
         g_view_tab (l_insert_recd_cnt).buyer := g_view_record.buyer;
         g_view_tab (l_insert_recd_cnt).ts := g_view_record.ts;
         g_view_tab (l_insert_recd_cnt).so := g_view_record.so;
         g_view_tab (l_insert_recd_cnt).sourcing_rule :=
            g_view_record.sourcing_rule;
         g_view_tab (l_insert_recd_cnt).clt := g_view_record.clt;
         g_view_tab (l_insert_recd_cnt).inventory_item_id :=
            g_view_record.inventory_item_id;
         g_view_tab (l_insert_recd_cnt).organization_id :=
            g_view_record.organization_id;
         g_view_tab (l_insert_recd_cnt).set_of_books_id :=
            g_view_record.set_of_books_id;
         g_view_tab (l_insert_recd_cnt).on_ord := g_view_record.on_ord;
         g_view_tab (l_insert_recd_cnt).org_name := g_view_record.org_name;
         g_view_tab (l_insert_recd_cnt).district := g_view_record.district;
         g_view_tab (l_insert_recd_cnt).region := g_view_record.region;
         g_view_tab (l_insert_recd_cnt).wt := g_view_record.wt;
         g_view_tab (l_insert_recd_cnt).ss := g_view_record.ss;
         g_view_tab (l_insert_recd_cnt).fml := g_view_record.fml;
         g_view_tab (l_insert_recd_cnt).open_req := g_view_record.open_req;

         g_view_tab (l_insert_recd_cnt).common_output_id :=
            xxeis.eis_rs_common_outputs_s.NEXTVAL;
         g_view_tab (l_insert_recd_cnt).process_id := p_process_id;
         g_view_tab (l_insert_recd_cnt).avail2 := g_view_record.avail2;
         g_view_tab (l_insert_recd_cnt).int_req := g_view_record.int_req;
         g_view_tab (l_insert_recd_cnt).dir_req := g_view_record.dir_req;
         g_view_tab (l_insert_recd_cnt).demand := g_view_record.demand;

         g_view_tab (l_insert_recd_cnt).site_vendor_num :=
            g_view_record.site_vendor_num;
         g_view_tab (l_insert_recd_cnt).vendor_site :=
            g_view_record.vendor_site;
         l_insert_recd_cnt := l_insert_recd_cnt + 1;
      END LOOP;

      CLOSE l_ref_cursor;

      BEGIN
         fnd_file.put_line (fnd_file.LOG,
                            'l_insert_recd_cnt' || l_insert_recd_cnt);
         fnd_file.put_line (
            fnd_file.LOG,
            'first common_output_id' || g_view_tab (1).common_output_id);

         --    Fnd_File.Put_Line(Fnd_File.Log,'last common_output_id' ||G_View_Tab(L_Insert_Recd_Cnt).Common_Output_Id);
         IF l_insert_recd_cnt >= 1
         THEN
            FORALL i IN g_view_tab.FIRST .. g_view_tab.LAST
               INSERT INTO xxeis.eis_xxwc_po_isr_rpt_v
                    VALUES g_view_tab (i);
         END IF;

         COMMIT;

         g_view_tab.delete;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
               'Inside Exception==>' || SUBSTR (SQLERRM, 1, 240));
      END;
   END;

   -----_**************************************************
   PROCEDURE get_vendor_info (p_inventory_item_id   IN     NUMBER,
                              p_organization_id     IN     NUMBER,
                              p_vendor_name            OUT VARCHAR2,
                              p_vendor_number          OUT VARCHAR2,
                              p_vendor_site            OUT VARCHAR2)
   IS
      l_vendor_name     VARCHAR2 (240) := NULL;
      l_vendor_number   VARCHAR2 (240) := NULL;
      l_vendor_site     VARCHAR2 (240) := NULL;
      l_vendor_id       NUMBER;
      s_string          CLOB;
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.get_vendor_info';

      BEGIN
         s_string :=
            '
            SELECT pov.vendor_name, pov.segment1, pvs.vendor_site_code
              FROM 
-- Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic Begin
--     mrp_sr_assignments ass
      (SELECT * FROM mrp_sr_assignments WHERE assignment_type = 6 AND assignment_set_id = 1 )    ass
--Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic End    
                  ,mrp_sr_receipt_org rco
                  ,mrp_sr_source_org sso
                  ,po_vendors pov
                  ,po_vendor_sites_all pvs
                  , xxeis.xxwc_mtl_system_items_b#s#a msi
             WHERE     1 = 1
                   AND msi.inventory_item_id = ass.inventory_item_id
                   AND msi.organization_id = ass.organization_id
                   AND ass.inventory_item_id = :p_inventory_item_id
                   AND ass.organization_id = :p_organization_id
                   AND msi.source_type = 2
                   AND rco.sourcing_rule_id = ass.sourcing_rule_id
                   AND sso.sr_receipt_id = rco.sr_receipt_id
                   AND sso.source_type = 3
                   AND pov.vendor_id = sso.vendor_id                   
                   AND pvs.vendor_site_id = sso.vendor_site_id';

         EXECUTE IMMEDIATE s_string
            INTO l_vendor_name, l_vendor_number, l_vendor_site
            USING p_inventory_item_id, p_organization_id;

         p_vendor_name := l_vendor_name;
         p_vendor_number := l_vendor_number;
         p_vendor_site := l_vendor_site;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_vendor_name := NULL;
            p_vendor_name := NULL;
            p_vendor_number := NULL;
            p_vendor_site := NULL;
      END;

      IF l_vendor_name IS NULL
      THEN
         s_string :=
            'SELECT vendor_id
              FROM (  SELECT COUNT (sso.vendor_id) cnt, sso.vendor_id
-- Commented By Manjula on 17-Dec-14 TMS # 20140908-00200 Updated BPA Logic 
-- to select data only for the assignment set WC Default                
                        --FROM mrp_sr_assignments ass
                        FROM (SELECT * FROM mrp_sr_assignments WHERE assignment_type = 6 AND assignment_set_id = 1 ) ass 
                            ,mrp_sr_receipt_org rco
                            ,mrp_sr_source_org sso
                            ,xxeis.xxwc_mtl_system_items_b#s#a msi
                       WHERE     1 = 1
                             AND msi.inventory_item_id = ass.inventory_item_id
                             AND msi.organization_id = ass.organization_id
                             AND ass.inventory_item_id = :p_inventory_item_id
                             AND msi.source_type = 2
                             AND rco.sourcing_rule_id = ass.sourcing_rule_id
                             AND sso.sr_receipt_id = rco.sr_receipt_id
                             AND sso.source_type = 3
                    GROUP BY sso.vendor_id
                    ORDER BY 1 DESC) a
             WHERE ROWNUM <= 1';

         EXECUTE IMMEDIATE s_string
            INTO l_vendor_id
            USING p_inventory_item_id;

         SELECT segment1, vendor_name
           INTO l_vendor_number, l_vendor_name
           FROM po_vendors
          WHERE vendor_id = l_vendor_id;
      END IF;

      p_vendor_name := l_vendor_name;
      p_vendor_number := l_vendor_number;
      p_vendor_site := l_vendor_site;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_vendor_name := NULL;
         p_vendor_number := NULL;
         p_vendor_site := NULL;
   END;

   /*
       This is called from main().
   */
   PROCEDURE populate_master
   IS
      l_main_sql            CLOB;

      TYPE cursor_typer IS REF CURSOR;

      l_ref_cursor2         cursor_typer;

      TYPE view_tab3 IS TABLE OF xxeis.eis_xxwc_po_isr_tab_v%ROWTYPE
         INDEX BY BINARY_INTEGER;

      g_view_record2        view_tab3;

      TYPE view_tab2 IS TABLE OF xxeis.eis_xxwc_po_isr_tab%ROWTYPE
         INDEX BY BINARY_INTEGER;

      g_view_tab2           view_tab2;
      lv_program_location   VARCHAR2 (4000);

      l_item_cost           NUMBER;
      l_avail_d             NUMBER;
      l_on_ord              NUMBER;
      l_avail2              NUMBER;
      l_vendor_name         VARCHAR2 (240);
      l_vendor_number       VARCHAR2 (50);
      l_vendor_site         VARCHAR2 (100);
      l_organization_id     NUMBER;
      l_count               NUMBER;
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.populate_master';

      l_main_sql := 'SELECT *
                    from xxeis.EIS_XXWC_PO_ISR_MST_TAB_V ';

      OPEN l_ref_cursor2 FOR l_main_sql;

      LOOP
         FETCH l_ref_cursor2 BULK COLLECT INTO g_view_record2 LIMIT 10000;

         l_count := 0;

         FOR j IN 1 .. g_view_record2.COUNT
         LOOP
            --lv_program_location := '27, Populate Main query into table record ';
            l_item_cost :=
               NVL (g_view_record2 (j).bpa_cost,
                    g_view_record2 (j).item_cost);
            l_avail_d :=
               g_view_record2 (j).avail * g_view_record2 (j).aver_cost;
            l_on_ord :=
                 g_view_record2 (j).supply
               - NVL (g_view_record2 (j).open_req, 0);
            l_avail2 := g_view_record2 (j).qoh - g_view_record2 (j).demand;
            l_vendor_name := g_view_record2 (j).vendor_name;
            l_vendor_number := g_view_record2 (j).vendor_number;
            l_vendor_site := g_view_record2 (j).vendor_site;

            IF    g_view_record2 (j).st = 'I'
               OR g_view_record2 (j).vendor_number IS NULL
            THEN
               l_organization_id :=
                  CASE
                     WHEN g_view_record2 (j).st = 'I'
                     THEN
                        g_view_record2 (j).source_organization_id
                     ELSE
                        g_view_record2 (j).organization_id
                  END;
               get_vendor_info (g_view_record2 (j).inventory_item_id,
                                l_organization_id,
                                l_vendor_name,
                                l_vendor_number,
                                l_vendor_site);
            END IF;

            g_view_tab2 (j).org := g_view_record2 (j).org;
            g_view_tab2 (j).pre := g_view_record2 (j).pre;
            g_view_tab2 (j).item_number := g_view_record2 (j).item_number;
            g_view_tab2 (j).vendor_num := l_vendor_number;
            g_view_tab2 (j).vendor_name := l_vendor_name;
            g_view_tab2 (j).source := g_view_record2 (j).source;
            g_view_tab2 (j).st := g_view_record2 (j).st;
            g_view_tab2 (j).description := g_view_record2 (j).description;
            g_view_tab2 (j).cat := g_view_record2 (j).cat_class;
            g_view_tab2 (j).pplt := g_view_record2 (j).pplt;
            g_view_tab2 (j).plt := g_view_record2 (j).plt;
            g_view_tab2 (j).uom := g_view_record2 (j).uom;
            g_view_tab2 (j).cl := g_view_record2 (j).cl;
            g_view_tab2 (j).stk_flag := g_view_record2 (j).stk;
            g_view_tab2 (j).pm := g_view_record2 (j).pm;
            g_view_tab2 (j).minn := g_view_record2 (j).MIN;
            g_view_tab2 (j).maxn := g_view_record2 (j).MAX;
            g_view_tab2 (j).amu := g_view_record2 (j).amu;
            g_view_tab2 (j).mf_flag := g_view_record2 (j).mf_flag;
            g_view_tab2 (j).hit6_store_sales := NULL;
            g_view_tab2 (j).hit6_other_inv_sales := NULL;
            g_view_tab2 (j).aver_cost := g_view_record2 (j).aver_cost;
            g_view_tab2 (j).item_cost := l_item_cost;
            g_view_tab2 (j).bpa_cost := g_view_record2 (j).bpa_cost;
            g_view_tab2 (j).bpa := g_view_record2 (j).bpa;
            g_view_tab2 (j).qoh := g_view_record2 (j).qoh;
            g_view_tab2 (j).on_ord := l_on_ord;
            g_view_tab2 (j).available := g_view_record2 (j).avail;
            g_view_tab2 (j).availabledollar := l_avail_d;
            g_view_tab2 (j).one_store_sale := NULL;
            g_view_tab2 (j).six_store_sale := NULL;
            g_view_tab2 (j).twelve_store_sale := NULL;
            g_view_tab2 (j).one_other_inv_sale := NULL;
            g_view_tab2 (j).six_other_inv_sale := NULL;
            g_view_tab2 (j).twelve_other_inv_sale := NULL;
            g_view_tab2 (j).bin_loc := g_view_record2 (j).bin_loc;
            g_view_tab2 (j).mc := g_view_record2 (j).mc;
            g_view_tab2 (j).fi_flag := g_view_record2 (j).fi;
            g_view_tab2 (j).freeze_date := g_view_record2 (j).freeze_date;
            g_view_tab2 (j).res := g_view_record2 (j).res;
            g_view_tab2 (j).thirteen_wk_avg_inv :=
               g_view_record2 (j).thirteen_wk_avg_inv;
            g_view_tab2 (j).thirteen_wk_an_cogs :=
               g_view_record2 (j).thirteen_wk_an_cogs;
            g_view_tab2 (j).turns := g_view_record2 (j).turns;
            g_view_tab2 (j).buyer := g_view_record2 (j).buyer;
            g_view_tab2 (j).ts := g_view_record2 (j).ts;
            g_view_tab2 (j).jan_store_sale := NULL;
            g_view_tab2 (j).feb_store_sale := NULL;
            g_view_tab2 (j).mar_store_sale := NULL;
            g_view_tab2 (j).apr_store_sale := NULL;
            g_view_tab2 (j).may_store_sale := NULL;
            g_view_tab2 (j).jun_store_sale := NULL;
            g_view_tab2 (j).jul_store_sale := NULL;
            g_view_tab2 (j).aug_store_sale := NULL;
            g_view_tab2 (j).sep_store_sale := NULL;
            g_view_tab2 (j).oct_store_sale := NULL;
            g_view_tab2 (j).nov_store_sale := NULL;
            g_view_tab2 (j).dec_store_sale := NULL;
            g_view_tab2 (j).jan_other_inv_sale := NULL;
            g_view_tab2 (j).feb_other_inv_sale := NULL;
            g_view_tab2 (j).mar_other_inv_sale := NULL;
            g_view_tab2 (j).apr_other_inv_sale := NULL;
            g_view_tab2 (j).may_other_inv_sale := NULL;
            g_view_tab2 (j).jun_other_inv_sale := NULL;
            g_view_tab2 (j).jul_other_inv_sale := NULL;
            g_view_tab2 (j).aug_other_inv_sale := NULL;
            g_view_tab2 (j).sep_other_inv_sale := NULL;
            g_view_tab2 (j).oct_other_inv_sale := NULL;
            g_view_tab2 (j).nov_other_inv_sale := NULL;
            g_view_tab2 (j).dec_other_inv_sale := NULL;
            g_view_tab2 (j).hit4_store_sales := NULL;
            g_view_tab2 (j).hit4_other_inv_sales := NULL;
            g_view_tab2 (j).so := g_view_record2 (j).so;
            g_view_tab2 (j).inventory_item_id :=
               g_view_record2 (j).inventory_item_id;
            g_view_tab2 (j).organization_id :=
               g_view_record2 (j).organization_id;
            g_view_tab2 (j).set_of_books_id :=
               g_view_record2 (j).set_of_books_id;
            g_view_tab2 (j).org_name := g_view_record2 (j).organization_name;
            g_view_tab2 (j).district := g_view_record2 (j).district;
            g_view_tab2 (j).region := g_view_record2 (j).region;
            g_view_tab2 (j).inv_cat_seg1 := g_view_record2 (j).inv_cat_seg1;
            g_view_tab2 (j).wt := g_view_record2 (j).wt;
            g_view_tab2 (j).ss := g_view_record2 (j).ss;
            g_view_tab2 (j).fml := g_view_record2 (j).fml;
            g_view_tab2 (j).open_req := g_view_record2 (j).open_req;
            g_view_tab2 (j).sourcing_rule := g_view_record2 (j).sourcing_rule;
            g_view_tab2 (j).clt := g_view_record2 (j).clt;
            g_view_tab2 (j).avail2 := l_avail2;
            g_view_tab2 (j).int_req := g_view_record2 (j).int_req;
            g_view_tab2 (j).dir_req := g_view_record2 (j).dir_req;
            g_view_tab2 (j).demand := g_view_record2 (j).demand;
            g_view_tab2 (j).item_status_code :=
               g_view_record2 (j).item_status_code;
            g_view_tab2 (j).site_vendor_num :=
               g_view_record2 (j).vendor_number;
            g_view_tab2 (j).vendor_site := g_view_record2 (j).vendor_site;

            l_count := l_count + 1;
         END LOOP;

         IF l_count >= 1
         THEN
            lv_program_location := '5, Insert into eis_xxwc_po_isr_tab ';

            FORALL j IN 1 .. g_view_tab2.COUNT
               INSERT INTO xxeis.eis_xxwc_po_isr_tab_total
                    VALUES g_view_tab2 (j);

            COMMIT;
         END IF;

         lv_program_location := '6, Delete tab ';
         g_view_tab2.delete;
         g_view_record2.delete;

         IF l_ref_cursor2%NOTFOUND
         THEN
            CLOSE l_ref_cursor2;

            EXIT;
         END IF;
      END LOOP;
   END;

   PROCEDURE wait_for_jobs (p_job_what VARCHAR2)
   IS
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.wait_for_jobs';

      FOR r IN (SELECT '1'
                  FROM dba_jobs
                 WHERE what LIKE '%' || p_job_what || '%' AND ROWNUM = 1)
      LOOP
         apps.xxwc_sleep (10);
         wait_for_jobs (p_job_what);
      END LOOP;
   END;

   /* This procedure is called in a loop from main and looks like it loads demand
           on a per-org basis.
       */
   PROCEDURE populate_demand_org (p_table_name         VARCHAR2,
                                  p_organization_id    NUMBER)
   IS
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.populate_demand_org';

      apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                        p_table_name);

      EXECUTE IMMEDIATE
            'create table xxeis.'
         || p_table_name
         || '(demand number,inventory_item_id number,organization_id number,qoh number,supply number)';

      alter_table_temp ('XXEIS', p_table_name);

      EXECUTE IMMEDIATE
            '
            INSERT /*+ append */
                  INTO xxeis.'
         || p_table_name
         || ' SELECT xxeis.eis_po_xxwc_isr_util_pkg.get_demand_qty (r.organization_id
                                                                 ,NULL
                                                                 ,1
                                                                 ,r.inventory_item_id
                                                                 ,SYSDATE
                                                                 ,2
                                                                 ,1
                                                                 ,1
                                                                 ,1
                                                                 ,''Y'')
                           demand
                      ,r.inventory_item_id
                      ,r.organization_id,xxeis.eis_po_xxwc_isr_util_pkg.get_planning_quantity (2
                                                                            ,1
                                                                            ,r.organization_id
                                                                            ,NULL
                                                                            ,r.inventory_item_id)
                                                                            ,xxeis.eis_po_xxwc_isr_util_pkg.get_supply_qty (r.organization_id
                                                     ,NULL
                                                     ,r.inventory_item_id
                                                     ,r.postprocessing_lead_time
                                                     ,''WC STD''
                                                     ,-1
                                                     ,1
                                                     ,SYSDATE
                                                     ,1
                                                     ,1
                                                     ,''Y''
                                                     ,2
                                                     ,1
                                                     ,1
                                                     ,''Y'')
                  FROM xxeis.xxwc_mtl_system_items_b#s#c r
                 WHERE
                        r.organization_id ='
         || p_organization_id;

      COMMIT;
   END;

   --******************************************************
   PROCEDURE populate_delta_hits
   /*************************************************************************
     $Header EIS_PO_XXWC_ISR_PKG_V2.POPULATE_DELTA_HITS $
     Module Name: EIS_PO_XXWC_ISR_PKG_V2

     PURPOSE: POPULATE_DELTA_HITS

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        07/31/2014  Manjula Chellappan    Modified for TMS # 20140609-00203
                                                  For Index ReOrg

   **************************************************************************/
   IS
      v_last_update_date_exists   VARCHAR2 (1) := 'N';
      v_trunc_last_update_date    DATE;
      v_execute_string            CLOB;
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.populate_delta_hits';

      -- Below commented line updated by Manjula on 31-Jul-14 for TMS 20140609-00203 to update the Index name
      -- from XXWC_OE_ORDER_LINES_LUD1 to XXWC_OE_ORDER_LN_LUD1 and schema name from XXWC to ONT
      --BEGIN
      --    DBMS_STATS.gather_index_stats (ownname => 'ONT', indname => 'XXWC_OE_ORDER_LN_LUD1');
      -- END;
      -- Below line updated by Manjula on 31-Jul-14 for TMS 20140609-00203 to update the Index name
      -- from XXWC_OE_ORDER_LINES_LUD1 to XXWC_OE_ORDER_LN_LUD1 and schema name from XXWC to ONT

      --  apps.xxwc_oe_order_lines_index ('XXWC', 'XXWC_OE_ORDER_LINES_LUD1');
      apps.xxwc_oe_order_lines_index ('ONT', 'XXWC_OE_ORDER_LN_LUD1');

      --*************************************************
      --REPOPULATE THE TABLE AGAIN TO HAVE last_update_date FROM ORDER_LINES_TABLE
      --*************************************************
      FOR r
         IN (SELECT column_name
               FROM dba_tab_columns
              WHERE     table_name = UPPER ('eis_xxwc_po_isr_hits_tab3##')
                    AND column_name = 'TRUNC_LAST_UPDATE_DATE')
      LOOP
         v_last_update_date_exists := 'Y';
      END LOOP;

      IF v_last_update_date_exists = 'N'
      THEN
         BEGIN
            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab1##');

            EXECUTE IMMEDIATE
               '
CREATE TABLE xxeis.eis_xxwc_po_isr_hits_tab1##
(
    organization_id
   ,inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,ordered_quantity
   ,line_type_id
   ,fulfillment_date
   ,actual_shipment_date
   , TRUNC_LAST_UPDATE_DATE
   ,Flow_Status_Code
   ,Invoice_interface_status_code,Ship_to_Org_ID
    ,CONSTRAINT xxwc_po_isr_hits_tab1## PRIMARY KEY (
    inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,trunc_last_update_date )

)ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS SELECT oel.ship_from_org_id organization_id
      ,oel.inventory_item_id inventory_item_id
      ,oel.header_id
      ,oel.line_id
      ,oel.line_category_code
      ,oel.ordered_quantity
      ,oel.line_type_id
      ,oel.fulfillment_date
      ,oel.actual_shipment_date
      ,TRUNC(oel.LAST_UPDATE_DATE)
      ,oel.Flow_Status_Code
      ,oel.invoice_interface_status_code
      ,oel.Ship_to_Org_ID
  FROM oe_order_lines_all oel where 1=2
   ';
         END;

         DECLARE
            v_string   CLOB;
         BEGIN
            FOR r
               IN (SELECT DISTINCT TRUNC (oel.last_update_date) update_date
                     FROM oe_order_lines_all oel)
            LOOP
               -- Below line updated by Manjula on 31-Jul-14 for TMS 20140609-00203 to update the Index name
               -- from XXWC_OE_ORDER_LINES_LUD1 to XXWC_OE_ORDER_LN_LUD1

               v_string :=
                  'INSERT /*+ append */
      INTO  xxeis.eis_xxwc_po_isr_hits_tab1##
      SELECT /*+ INDEX(OEL XXWC_OE_ORDER_LN_LUD1)*/oel.ship_from_org_id organization_id
      ,oel.inventory_item_id inventory_item_id
      ,oel.header_id
      ,oel.line_id
      ,oel.line_category_code
      ,oel.ordered_quantity
      ,oel.line_type_id
      ,oel.fulfillment_date
      ,oel.actual_shipment_date
      ,TRUNC(oel.LAST_UPDATE_DATE)
      ,oel.Flow_Status_Code
      ,oel.invoice_interface_status_code
      ,oel.Ship_to_Org_ID
  FROM oe_order_lines_all oel
  where TRUNC(oel.LAST_UPDATE_DATE)=:update_date ';

               EXECUTE IMMEDIATE v_string USING r.update_date;

               COMMIT;
            END LOOP;
         END;

         BEGIN
            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab2##');

            EXECUTE IMMEDIATE
               '
CREATE TABLE xxeis.eis_xxwc_po_isr_hits_tab2##
(
    organization_id
   ,inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,ordered_quantity
   ,line_type_id
   ,fulfillment_date
   ,actual_shipment_date
   , TRUNC_LAST_UPDATE_DATE
   ,Flow_Status_Code
   ,Invoice_interface_status_code,Ship_to_Org_ID
    ,CONSTRAINT xxwc_po_isr_hits_tab2## PRIMARY KEY (
    inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,trunc_last_update_date )

)ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT *
  FROM xxeis.eis_xxwc_po_isr_hits_tab1##
 WHERE     flow_status_codE = ''CLOSED''
       AND invoice_interface_status_code = ''YES''
       AND NOT EXISTS
               (SELECT 1
                  FROM mtl_parameters mp
                 WHERE mp.organization_id = Ship_to_Org_ID) ';

            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab1##');
         END;

         --****************************************
         BEGIN
            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab21##');

            EXECUTE IMMEDIATE
               '
CREATE TABLE xxeis.eis_xxwc_po_isr_hits_tab21##
(
    organization_id
   ,inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,ordered_quantity
   ,line_type_id
   ,fulfillment_date
   ,actual_shipment_date
   , TRUNC_LAST_UPDATE_DATE
   ,Flow_Status_Code
   ,Invoice_interface_status_code,Ship_to_Org_ID,order_number,t_type
    ,CONSTRAINT xxwc_po_isr_hits_tab21## PRIMARY KEY (
    inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,trunc_last_update_date
   )

)ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT t.*
      , (SELECT oeh.order_number
           FROM oe_order_headers_all oeh
          WHERE t.header_id = oeh.header_id)
           order_number
      , (SELECT name
           FROM oe_transaction_types_tl
          WHERE transaction_type_id = t.line_type_id)
           t_type
  FROM xxeis.eis_xxwc_po_isr_hits_tab2## t ';
         END;

         --****************************************
         BEGIN
            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab2##');
            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab3##');

            EXECUTE IMMEDIATE
               '
CREATE TABLE xxeis.eis_xxwc_po_isr_hits_tab3##
(
    organization_id
   ,inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,ordered_quantity
   ,line_type_id
   ,fulfillment_date
   ,actual_shipment_date
   , TRUNC_LAST_UPDATE_DATE
   ,Flow_Status_Code
   ,Invoice_interface_status_code,Ship_to_Org_ID,order_number,t_type,action_date
    ,CONSTRAINT xxwc_po_isr_hits_tab3## PRIMARY KEY (
    inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,trunc_last_update_date )


)ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT t.*
     ,decode( t_type,''COUNTER LINE'' ,Trunc(t.fulfillment_Date),Trunc(t.actual_shipment_date) ) action_date
  FROM xxeis.eis_xxwc_po_isr_hits_tab21## t';

            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab21##');
         END;
      --*************************************************
      --END REPOPULATE THE TABLE AGAIN TO HAVE last_update_date FROM ORDER_LINES_TABLE
      --*************************************************
      ELSE
         -- Below line updated by Manjula on 31-Jul-14 for TMS 20140609-00203 to update the Index name
         -- from XXWC_OE_ORDER_LINES_LUD1 to XXWC_OE_ORDER_LN_LUD1 and schema name from XXWC to ONT

         --  apps.xxwc_oe_order_lines_index ('XXWC', 'XXWC_OE_ORDER_LINES_LUD1');
         apps.xxwc_oe_order_lines_index ('ONT', 'XXWC_OE_ORDER_LN_LUD1');

         BEGIN
            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab1##');

            EXECUTE IMMEDIATE
               '
CREATE TABLE xxeis.eis_xxwc_po_isr_hits_tab1##
(
    organization_id
   ,inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,ordered_quantity
   ,line_type_id
   ,fulfillment_date
   ,actual_shipment_date
   , TRUNC_LAST_UPDATE_DATE
   ,Flow_Status_Code
   ,Invoice_interface_status_code,Ship_to_Org_ID
    ,CONSTRAINT xxwc_po_isr_hits_tab1## PRIMARY KEY (
    inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,trunc_last_update_date )

)ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS SELECT oel.ship_from_org_id organization_id
      ,oel.inventory_item_id inventory_item_id
      ,oel.header_id
      ,oel.line_id
      ,oel.line_category_code
      ,oel.ordered_quantity
      ,oel.line_type_id
      ,oel.fulfillment_date
      ,oel.actual_shipment_date
      ,TRUNC(oel.LAST_UPDATE_DATE)
      ,oel.Flow_Status_Code
      ,oel.invoice_interface_status_code
      ,oel.Ship_to_Org_ID
  FROM oe_order_lines_all oel where 1=2
   ';
         END;

         DECLARE
            v_string                   CLOB;
            v_trunc_last_update_date   DATE;
         BEGIN
            EXECUTE IMMEDIATE
               'SELECT MAX(TRUNC_LAST_UPDATE_DATE)  FROM XXEIS.EIS_XXWC_PO_ISR_HITS_TAB3##'
               INTO v_trunc_last_update_date;

            v_string :=
               'DELETE FROM XXEIS.eis_xxwc_po_isr_hits_tab3## WHERE trunc_last_update_date> :v_trunc_last_update_date';

            EXECUTE IMMEDIATE v_string USING v_trunc_last_update_date;

            COMMIT;

            FOR r
               IN (SELECT DISTINCT TRUNC (oel.last_update_date) update_date
                     FROM oe_order_lines_all oel
                    WHERE TRUNC (oel.last_update_date) >=
                             v_trunc_last_update_date)
            LOOP
               -- Below line updated by Manjula on 31-Jul-14 for TMS 20140609-00203 to update the Index name
               -- from XXWC_OE_ORDER_LINES_LUD1 to XXWC_OE_ORDER_LN_LUD1

               v_string :=
                  'INSERT /*+ append */
      INTO  xxeis.eis_xxwc_po_isr_hits_tab1##
      SELECT /*+ INDEX(OEL XXWC_OE_ORDER_LN_LUD1)*/oel.ship_from_org_id organization_id
      ,oel.inventory_item_id inventory_item_id
      ,oel.header_id
      ,oel.line_id
      ,oel.line_category_code
      ,oel.ordered_quantity
      ,oel.line_type_id
      ,oel.fulfillment_date
      ,oel.actual_shipment_date
      ,TRUNC(oel.LAST_UPDATE_DATE)
      ,oel.Flow_Status_Code
      ,oel.invoice_interface_status_code
      ,oel.Ship_to_Org_ID
  FROM oe_order_lines_all oel
  where TRUNC(oel.LAST_UPDATE_DATE)=:update_date ';

               EXECUTE IMMEDIATE v_string USING r.update_date;

               COMMIT;
            END LOOP;
         END;

         --****************************************
         BEGIN
            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab2##');

            EXECUTE IMMEDIATE
               '
CREATE TABLE xxeis.eis_xxwc_po_isr_hits_tab2##
(
    organization_id
   ,inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,ordered_quantity
   ,line_type_id
   ,fulfillment_date
   ,actual_shipment_date
   , TRUNC_LAST_UPDATE_DATE
   ,Flow_Status_Code
   ,Invoice_interface_status_code,Ship_to_Org_ID
    ,CONSTRAINT xxwc_po_isr_hits_tab2## PRIMARY KEY (
    inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,trunc_last_update_date )

)ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT *
  FROM xxeis.eis_xxwc_po_isr_hits_tab1##
 WHERE     flow_status_codE = ''CLOSED''
       AND invoice_interface_status_code = ''YES''
       AND NOT EXISTS
               (SELECT 1
                  FROM mtl_parameters mp
                 WHERE mp.organization_id = Ship_to_Org_ID) ';

            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab1##');
         END;

         --**************************************
         BEGIN
            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab21##');

            EXECUTE IMMEDIATE
               '
CREATE TABLE xxeis.eis_xxwc_po_isr_hits_tab21##
(
    organization_id
   ,inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,ordered_quantity
   ,line_type_id
   ,fulfillment_date
   ,actual_shipment_date
   , TRUNC_LAST_UPDATE_DATE
   ,Flow_Status_Code
   ,Invoice_interface_status_code,Ship_to_Org_ID,order_number,t_type
    ,CONSTRAINT xxwc_po_isr_hits_tab21## PRIMARY KEY (
    inventory_item_id
   ,header_id
   ,line_id
   ,line_category_code
   ,trunc_last_update_date
   )

)ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT t.*
      , (SELECT oeh.order_number
           FROM oe_order_headers_all oeh
          WHERE t.header_id = oeh.header_id)
           order_number
      , (SELECT name
           FROM oe_transaction_types_tl
          WHERE transaction_type_id = t.line_type_id)
           t_type
  FROM xxeis.eis_xxwc_po_isr_hits_tab2## t ';
         END;

         --**********************************
         --DELETE BEFORE INSERT
         EXECUTE IMMEDIATE
            'DELETE FROM
      xxeis.eis_xxwc_po_isr_hits_tab3##  WHERE LINE_ID IN (SELECT LINE_ID FROM xxeis.eis_xxwc_po_isr_hits_tab21##)';

         COMMIT;

         BEGIN
            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab2##');

            EXECUTE IMMEDIATE
               'INSERT /*+ append*/
      INTO  xxeis.eis_xxwc_po_isr_hits_tab3##
SELECT t.*
     ,decode( t_type,''COUNTER LINE'' ,Trunc(t.fulfillment_Date),Trunc(t.actual_shipment_date) ) action_date
  FROM xxeis.eis_xxwc_po_isr_hits_tab21## t';

            COMMIT;
            apps.xxwc_common_tunning_helpers.drop_temp_table (
               'xxeis',
               'eis_xxwc_po_isr_hits_tab21##');
         END;

         apps.xxwc_common_tunning_helpers.elapsed_time (
            'ISR',
            'populate_delta_hits,6',
            g_start);
      END IF;
   END;

   --**************************************************
   PROCEDURE populate_hits_table
   IS
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.populate_hits_table';

      apps.xxwc_common_tunning_helpers.drop_temp_table (
         'xxeis',
         'eis_xxwc_po_isr_hits_tab0##');

      EXECUTE IMMEDIATE '
CREATE TABLE xxeis.eis_xxwc_po_isr_hits_tab0##
(
    organization_id     NUMBER
   ,inventory_item_id   NUMBER
   ,hit4_store_sales    NUMBER
   ,hit6_store_sales    NUMBER
)';

      alter_table_temp ('XXEIS', 'eis_xxwc_po_isr_hits_tab0##');

      EXECUTE IMMEDIATE
         'insert/*+append*/ into  XXEIS.eis_xxwc_po_isr_hits_tab0##
    SELECT oel.organization_id
                    ,oel.inventory_item_id inventory_item_id
                     ,CASE WHEN oel.action_date > TRUNC (SYSDATE - 112) THEN oel.header_id END hit4_store_sales
                     ,CASE WHEN oel.action_date > TRUNC (SYSDATE - 182) THEN oel.header_id END hit6_store_sales
                FROM xxeis.eis_xxwc_po_isr_hits_tab3## oel
               WHERE oel.line_category_code <> ''RETURN''
               AND NOT EXISTS
                             (SELECT 1
                                FROM DUAL
                               WHERE (DECODE (oel.line_category_code
                                             ,''RETURN'', (oel.ordered_quantity * -1)
                                             ,oel.ordered_quantity)) =
                                         (SELECT   -1
                                                 * SUM (
                                                       DECODE (olr.line_category_code
                                                              ,''RETURN'', (olr.ordered_quantity * -1)
                                                              ,olr.ordered_quantity))
                                            FROM oe_order_lines_all olr
                                           WHERE     olr.reference_header_id = oel.header_id
                                                 AND olr.reference_line_id = oel.line_id
                                                 AND olr.line_category_code = ''RETURN''
                                                 AND olr.return_context = ''ORDER''))';

      COMMIT;
      apps.xxwc_common_tunning_helpers.elapsed_time ('ISR',
                                                     'populate_hits_table,1',
                                                     g_start);
      create_table_from_other_table ('XXEIS',
                                     'eis_xxwc_po_isr_hits_tab0##',
                                     'eis_xxwc_po_isr_hits_tab##');

      EXECUTE IMMEDIATE
         '
INSERT /*+ append */
      INTO  xxeis.eis_xxwc_po_isr_hits_tab##
      SELECT
            organization_id
            ,inventory_item_id
            ,COUNT (DISTINCT hit4_store_sales) hit4_store_sales
            ,COUNT (DISTINCT hit6_store_sales) hit6_store_sales
        FROM (SELECT organization_id
                    ,inventory_item_id
                    ,hit4_store_sales
                    ,hit6_store_sales
                FROM xxeis.eis_xxwc_po_isr_hits_tab0##)
    GROUP BY organization_id, inventory_item_id';

      COMMIT;
      apps.xxwc_common_tunning_helpers.drop_temp_table (
         'xxeis',
         'eis_xxwc_po_isr_hits_tab0##');
      apps.xxwc_common_tunning_helpers.elapsed_time ('ISR',
                                                     'populate_hits_table,2',
                                                     g_start);
   END;

   --*******************************************************
   --*******************************************************
   --*******************************************************
   --*******************************************************
   PROCEDURE populate_driving_tables
   /*************************************************************************
     $Header EIS_PO_XXWC_ISR_PKG_V2.POPULATE_DRIVING_TABLES $
     Module Name: EIS_PO_XXWC_ISR_PKG_V2

     PURPOSE: Populate driving Tables

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        07/31/2014  Manjula Chellappan    TMS # 20140623-00097
                                                  For performance tuning


   **************************************************************************/

   IS
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.populate_driving_tables';

      --here are staging table sets for delta processing: #s# for staging
      --xxwc_mtl_system_items_b#s#a --driver table keeps  all items and item information needed for processing
      --xxwc_mtl_system_items_b#s#b -- table keep inventory_item_id, organization_id, last_update_date,source_organization_id
      --xxwc_mtl_system_items_b#s#c -- table keep inventory_item_id, organization_id, last_update_date,source_organization_id
      apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                        'wc_organizations##');

      g_sec := 'Create Table wc_organizations##';

      EXECUTE IMMEDIATE
         '
    create table xxeis.wc_organizations## as
    SELECT organization_id
      FROM org_organization_definitions
     WHERE  operating_unit = 162 AND NVL (disable_date, SYSDATE + 1) > SYSDATE';

      -- Ver 1.1  Added by below statement by Manjula on 18-Jun-14
      EXECUTE IMMEDIATE
         'ALTER TABLE xxeis.wc_organizations## ADD
CONSTRAINT PPk_wc_organizations## PRIMARY KEY ( organization_id  )';


      g_sec := 'Drop Table xxwc_mtl_system_items_b#s#a';

      apps.xxwc_common_tunning_helpers.drop_temp_table (
         'xxeis',
         'xxwc_mtl_system_items_b#s#a');


      --Ver 1.1 commented the below statement by Manjula on 18-Jun-14 TMS # 20140623-00097 performance tuning

      /*
              EXECUTE IMMEDIATE '
      CREATE TABLE xxeis.xxwc_mtl_system_items_b#s#a
      (
          inventory_item_id            NUMBER NOT NULL
         ,organization_id              NUMBER NOT NULL
         ,source_organization_id       NUMBER
         ,postprocessing_lead_time     NUMBER
         ,segment1                     VARCHAR2 (40 BYTE)
         ,description                  VARCHAR2 (240 BYTE)
         ,full_lead_time               NUMBER
         ,primary_uom_code             VARCHAR2 (3 BYTE)
         ,attribute21                  VARCHAR2 (240 BYTE)
         ,attribute20                  VARCHAR2 (240 BYTE)
         ,unit_weight                  NUMBER
         ,fixed_lot_multiplier         NUMBER
         ,buyer_id                     NUMBER (9, 0)
         ,attribute30                  VARCHAR2 (240 BYTE)
         ,inventory_item_status_code   VARCHAR2 (10 BYTE) NOT NULL
         ,inventory_planning_code      NUMBER
         ,source_type                  NUMBER
         ,mrp_safety_stock_code        NUMBER
         ,shelf_life_days              NUMBER
         ,list_price_per_unit          NUMBER
         ,max_minmax_quantity          NUMBER
         ,min_minmax_quantity          NUMBER
         ,preprocessing_lead_time      NUMBER
         ,CONSTRAINT Pk_mtl_system_items_b#s#a PRIMARY KEY ( inventory_item_id ,organization_id  )
      )
      ORGANIZATION INDEX
      TABLESPACE xxeis_data
      PARALLEL (DEGREE 4)
      NOLOGGING';

      */

      --       EXECUTE IMMEDIATE '
      --insert/*+append*/ into  xxeis.xxwc_mtl_system_items_b#s#a
      /* SELECT a.inventory_item_id
            ,a.organization_id
            ,a.source_organization_id
            ,a.postprocessing_lead_time
            ,a.segment1
            ,a.description
            ,a.full_lead_time
            ,a.primary_uom_code
            ,a.attribute21
            ,a.attribute20
            ,a.unit_weight
            ,a.fixed_lot_multiplier
            ,a.buyer_id
            ,a.attribute30
            ,a.inventory_item_status_code
            ,a.inventory_planning_code
            ,a.source_type
            ,a.mrp_safety_stock_code
            ,a.shelf_life_days
            ,a.list_price_per_unit
            ,a.max_minmax_quantity
            ,a.min_minmax_quantity
            ,a.preprocessing_lead_time
         FROM mtl_system_items_b a ,xxeis.wc_organizations## c
         where a.organization_id=c.organization_id ';

              COMMIT;

      */

      g_sec := 'Create Table xxwc_mtl_system_items_b#s#a';


      -- Ver 1.1 Added by below statement by Manjula on 18-Jun-14 TMS # 20140623-00097 performance tuning
      EXECUTE IMMEDIATE
         '
CREATE TABLE xxeis.xxwc_mtl_system_items_b#s#a AS
SELECT a.inventory_item_id
      ,a.organization_id
      ,a.source_organization_id
      ,a.postprocessing_lead_time
      ,a.segment1
      ,a.description
      ,a.full_lead_time
      ,a.primary_uom_code
      ,a.attribute21
      ,a.attribute20
      ,a.unit_weight
      ,a.fixed_lot_multiplier
      ,a.buyer_id
      ,a.attribute30
      ,a.inventory_item_status_code
      ,a.inventory_planning_code
      ,a.source_type
      ,a.mrp_safety_stock_code
      ,a.shelf_life_days
      ,a.list_price_per_unit
      ,a.max_minmax_quantity
      ,a.min_minmax_quantity
      ,a.preprocessing_lead_time
   FROM mtl_system_items_b a ,xxeis.wc_organizations## c
   where a.organization_id=c.organization_id ';

      EXECUTE IMMEDIATE
         'ALTER TABLE xxeis.xxwc_mtl_system_items_b#s#a ADD 
CONSTRAINT PPk_mtl_system_items_b#s#a PRIMARY KEY ( inventory_item_id ,organization_id  )';


      --*******************B table
      apps.xxwc_common_tunning_helpers.drop_temp_table (
         'xxeis',
         'xxwc_mtl_system_items_b#s#b');

      --Ver 1.1 commented the below statement by Manjula on 18-Jun-14 TMS # 20140623-00097 performance tuning
      /*
              EXECUTE IMMEDIATE '
      CREATE TABLE xxeis.xxwc_mtl_system_items_b#s#b
      (
          inventory_item_id            NUMBER NOT NULL
         ,organization_id              NUMBER NOT NULL
         ,source_organization_id       NUMBER
         ,postprocessing_lead_time     NUMBER
         ,CONSTRAINT Pk_mtl_system_items_b#s#b PRIMARY KEY ( inventory_item_id ,organization_id  )
      )
      ORGANIZATION INDEX
      TABLESPACE xxeis_data
      PARALLEL (DEGREE 4)
      NOLOGGING';

      */


      --        EXECUTE IMMEDIATE '
      --insert/*+append*/ into xxeis.xxwc_mtl_system_items_b#s#b SELECT inventory_item_id
      /*          ,organization_id
                ,source_organization_id,postprocessing_lead_time
            FROM xxeis.xxwc_mtl_system_items_b#s#a where organization_id <> 222';

              COMMIT;

      */

      --Ver 1.1 Added the below statements by Manjula on 18-Jun-14 TMS # 20140623-00097 performance tuning
      EXECUTE IMMEDIATE
         'CREATE TABLE xxeis.xxwc_mtl_system_items_b#s#b AS
SELECT inventory_item_id
          ,organization_id
          ,source_organization_id,postprocessing_lead_time
      FROM xxeis.xxwc_mtl_system_items_b#s#a where organization_id <> 222';

      EXECUTE IMMEDIATE
         'ALTER TABLE xxeis.xxwc_mtl_system_items_b#s#b ADD 
CONSTRAINT PPk_mtl_system_items_b#s#b PRIMARY KEY ( inventory_item_id ,organization_id  )';


      --*******************C table
      apps.xxwc_common_tunning_helpers.drop_temp_table (
         'xxeis',
         'xxwc_mtl_system_items_b#s#c');

      g_sec := 'Create Table xxwc_mtl_system_items_b#s#c';

      --Ver 1.1 Commented the below statements by Manjula on 18-Jun-14 TMS # 20140623-00097 performance tuning

      /*
              EXECUTE IMMEDIATE '
      CREATE TABLE xxeis.xxwc_mtl_system_items_b#s#c
      (
          inventory_item_id            NUMBER NOT NULL
         ,organization_id              NUMBER NOT NULL
         ,source_organization_id       NUMBER
         ,postprocessing_lead_time     NUMBER
         ,CONSTRAINT Pk_mtl_system_items_b#s#c PRIMARY KEY ( inventory_item_id ,organization_id  )
      )
      ORGANIZATION INDEX
      TABLESPACE xxeis_data
      PARALLEL (DEGREE 4)
      NOLOGGING';
      */


      --        EXECUTE IMMEDIATE '
      --insert/*+append*/ into xxeis.xxwc_mtl_system_items_b#s#c SELECT inventory_item_id
      /*          ,organization_id
                ,source_organization_id,postprocessing_lead_time
            FROM xxeis.xxwc_mtl_system_items_b#s#b where organization_id <> 222';

              COMMIT;
      */

      --Ver 1.1 Added the below statements by Manjula on 18-Jun-14 for TMS # 20140623-00097 performance tuning

      EXECUTE IMMEDIATE
         'CREATE TABLE xxeis.xxwc_mtl_system_items_b#s#c AS 
SELECT inventory_item_id
         ,organization_id
          ,source_organization_id,postprocessing_lead_time
      FROM xxeis.xxwc_mtl_system_items_b#s#b where organization_id <> 222';


      EXECUTE IMMEDIATE
         'ALTER TABLE xxeis.xxwc_mtl_system_items_b#s#c ADD 
CONSTRAINT Pk_mtl_system_items_b#s#c PRIMARY KEY ( inventory_item_id ,organization_id  )';
   END;

   --
   FUNCTION get_vendor_number (p_inventory_item_id    NUMBER,
                               p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_vendor_number   VARCHAR2 (240);
      s_string          CLOB;
   BEGIN
      BEGIN
         s_string := 'select pov.segment1
-- Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic Begin
--  from  mrp_sr_assignments ass
    from (SELECT * FROM mrp_sr_assignments WHERE assignment_type = 6 AND assignment_set_id = 1 )    ass
--Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic End        
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         po_vendors pov,
          xxeis.xxwc_mtl_system_items_b#s#a msi
    where 1=1
    AND msi.inventory_item_id = ass.inventory_item_id
    AND msi.organization_id   = ass.organization_id
    and ass.inventory_item_id = :p_inventory_item_id
    and ass.organization_id   = :p_organization_id
    and msi.source_type       = 2
    and rco.sourcing_rule_id  = ass.sourcing_rule_id
    and sso.sr_receipt_id     = rco.sr_receipt_id
    AND SSO.SOURCE_TYPE       = 3    
    and pov.vendor_id         = sso.vendor_id';

         EXECUTE IMMEDIATE s_string
            INTO l_vendor_number
            USING p_inventory_item_id, p_organization_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_vendor_number := NULL;
      END;

      IF l_vendor_number IS NULL
      THEN
         s_string := ' select MAX(pov.segment1)
-- Commented By Manjula on 17-Dec-14 TMS # 20140908-00200 Updated BPA Logic 
-- to select data only for the assignment set WC Default  
--      from mrp_sr_assignments  ass,
        FROM (SELECT * FROM mrp_sr_assignments WHERE assignment_type = 6 AND assignment_set_id = 1 ) ass, 
           mrp_sr_receipt_org rco,
           mrp_sr_source_org sso,
           po_vendors pov,
           xxeis.xxwc_mtl_system_items_b#s#a msi
      where 1=1
      AND msi.inventory_item_id = ass.inventory_item_id
      AND msi.organization_id   = ass.organization_id
      and ass.inventory_item_id = :p_inventory_item_id
      and msi.source_type       = 2
      and rco.sourcing_rule_id  = ass.sourcing_rule_id
      and sso.sr_receipt_id     = rco.sr_receipt_id
      AND SSO.SOURCE_TYPE       = 3
      and pov.vendor_id         = sso.vendor_id';

         EXECUTE IMMEDIATE s_string
            INTO l_vendor_number
            USING p_inventory_item_id;
      END IF;

      RETURN l_vendor_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   --************************************************************************
   --************************************************************************
   --************************************************************************
   --************************************************************************
   --************************************************************************
   --************************************************************************
   PROCEDURE populate_vendor_tables
   IS
   BEGIN
      g_procedure_name := 'eis_po_xxwc_isr_pkg_v2.populate_vendor_tables';

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##');

         EXECUTE IMMEDIATE
            'create table xxeis.vendor## as select pov.segment1 , msi.inventory_item_id,msi.organization_id
-- Commented By Manjula on 17-Dec-14 TMS # 20140908-00200 Updated BPA Logic 
-- to select data only for the assignment set WC Default  
--    from mrp_sr_assignments  ass,
FROM (SELECT * FROM mrp_sr_assignments WHERE assignment_type = 6 AND assignment_set_id = 1 ) ass, 
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         po_vendors pov,
          xxeis.xxwc_mtl_system_items_b#s#a msi
    where 1=1
    AND msi.inventory_item_id   = ass.inventory_item_id
    AND msi.organization_id    = ass.organization_id
    and msi.source_type         = 2
    and rco.sourcing_rule_id  = ass.sourcing_rule_id
    and sso.sr_receipt_id     = rco.sr_receipt_id
    AND SSO.SOURCE_TYPE       = 3
    and pov.vendor_id         = sso.vendor_id';

         EXECUTE IMMEDIATE 'alter table xxeis.vendor## nologging';

         EXECUTE IMMEDIATE
            'alter table xxeis.vendor## add(item_org_count number)';

         COMMIT;
      END;

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##M');

         EXECUTE IMMEDIATE
            '
CREATE TABLE xxeis.vendor##m
AS
      SELECT COUNT (inventory_item_id) item_org_count, inventory_item_id, organization_id
        FROM xxeis.vendor##
    GROUP BY inventory_item_id, organization_id
      HAVING COUNT (inventory_item_id) > 1';

         EXECUTE IMMEDIATE
            '
 begin
 for r in (select * from xxeis.vendor##m)
 loop
 DELETE FROM xxeis.vendor##  where inventory_item_id =r.inventory_item_id and organization_id=r.organization_id;
 end loop;
 commit;
 end;';
      END;

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##iot');

         EXECUTE IMMEDIATE
            '
CREATE TABLE xxeis.vendor##iot
(VENDOR_NUMBER,
    item_org_count
   ,inventory_item_id
   ,organization_id
   ,CONSTRAINT xxeis_vendor##iot PRIMARY KEY (VENDOR_NUMBER,inventory_item_id, organization_id, item_org_count)
)
ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
 SELECT  a.segment1
      ,NVL (a.item_org_count, 1)
      ,a.inventory_item_id
      ,a.organization_id
  FROM xxeis.vendor## a';
      END;

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##');

         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##m');
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##iotf');

         --****************************************************************************
         --missing item vendors

         EXECUTE IMMEDIATE
            '
CREATE TABLE xxeis.vendor##iotf
(   inventory_item_id
   ,organization_id
   ,source_type
   ,CONSTRAINT xxeis_vendor##iotf PRIMARY KEY (inventory_item_id, organization_id)
)
ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT b.inventory_item_id, b.organization_id ,b.source_type
  FROM xxeis.xxwc_mtl_system_items_b#s#a b
 WHERE  NOT exists (SELECT 1 FROM xxeis.vendor##iot a where a.inventory_item_id=b.inventory_item_id and
  a.organization_id=b.organization_id )
 and  b.organization_id<>222 and 1=2';

         EXECUTE IMMEDIATE
            'insert /*+append*/ into xxeis.vendor##iotf SELECT b.inventory_item_id, b.organization_id ,b.source_type
  FROM xxeis.xxwc_mtl_system_items_b#s#a b
 WHERE  NOT exists (SELECT 1 FROM xxeis.vendor##iot a where a.inventory_item_id=b.inventory_item_id and
  a.organization_id=b.organization_id )
 and  b.organization_id<>222';

         COMMIT;

         --****************************************************
         --get mtl data for missing
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##iotfh');
      END;

      BEGIN
         EXECUTE IMMEDIATE
            '
CREATE TABLE xxeis.vendor##iotfh
(   inventory_item_id
   ,organization_id
   ,source_type
   ,CONSTRAINT xxeis_vendor##iotfh PRIMARY KEY (inventory_item_id, organization_id)
)
ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT inventory_item_id, organization_id ,source_type
  FROM xxeis.xxwc_mtl_system_items_b#s#a b
 WHERE  B.inventory_item_id in  (SELECT inventory_item_id from xxeis.vendor##iotf )
 and 1=2 ';

         EXECUTE IMMEDIATE
            'insert /*+append*/ into xxeis.vendor##iotfh
SELECT b.inventory_item_id, b.organization_id ,b.source_type
  FROM xxeis.xxwc_mtl_system_items_b#s#a b
 WHERE  B.inventory_item_id in  (SELECT inventory_item_id from xxeis.vendor##iotf )';

         COMMIT;
      END;

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##iotl');

         EXECUTE IMMEDIATE
            'CREATE TABLE xxeis.vendor##iotl
(VENDOR_NUMBER
   ,inventory_item_id
   ,CONSTRAINT xxeis_vendor##iotl PRIMARY KEY (VENDOR_NUMBER,inventory_item_id )
)
ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT MAX (pov.segment1) max_vendor_number,ass.inventory_item_id
-- Commented By Manjula on 17-Dec-14 TMS # 20140908-00200 Updated BPA Logic 
-- to select data only for the assignment set WC Default  
--    FROM mrp_sr_assignments ass
    FROM (SELECT * FROM mrp_sr_assignments WHERE assignment_type = 6 AND assignment_set_id = 1 ) ass 
        ,mrp_sr_receipt_org rco
        ,mrp_sr_source_org sso
        ,po_vendors pov
        ,xxeis.vendor##iotfh msi
   WHERE     1 = 1
         AND msi.inventory_item_id = ass.inventory_item_id
         AND msi.organization_id = ass.organization_id
         AND msi.source_type = 2
         AND rco.sourcing_rule_id = ass.sourcing_rule_id
         AND sso.sr_receipt_id = rco.sr_receipt_id
         AND sso.source_type = 3
         AND pov.vendor_id = sso.vendor_id
GROUP BY ass.inventory_item_id';
      END;

      BEGIN
         --*****************************************

         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##jk');

         EXECUTE IMMEDIATE
            'CREATE TABLE xxeis.vendor##jk
(VENDOR_NUMBER
   ,inventory_item_id
   ,organization_id
   ,CONSTRAINT xxeis_vendor##jk PRIMARY KEY (VENDOR_NUMBER,inventory_item_id, organization_id)
)
ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)AS

SELECT a.vendor_number, b.inventory_item_id, b.organization_id
  FROM xxeis.vendor##iotl a, xxeis.vendor##iotf b
 WHERE a.inventory_item_id = b.inventory_item_id';

         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'VENDOR##IOTF');
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##FINAL');
      END;

      BEGIN
         EXECUTE IMMEDIATE
            'CREATE TABLE xxeis.vendor##final
(VENDOR_NUMBER
   ,inventory_item_id
   ,organization_id
   ,CONSTRAINT xxeis_vendor##iotlf PRIMARY KEY (VENDOR_NUMBER,inventory_item_id, organization_id)
)
ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT a.vendor_number, a.inventory_item_id, a.organization_id
  FROM xxeis.vendor##jk a
  union all
  SELECT a.vendor_number, a.inventory_item_id, a.organization_id
  FROM xxeis.vendor##iot a';
      END;

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'VENDOR##JK');
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'VENDOR##IOT');
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'vendor##FIN');

         EXECUTE IMMEDIATE
            'CREATE TABLE xxeis.vendor##fin
(VENDOR_NUMBER
   ,inventory_item_id
   ,organization_id
   ,CONSTRAINT xxeis_vendor##fin PRIMARY KEY (inventory_item_id, organization_id)
)
ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT a.vendor_number, b.inventory_item_id, b.organization_id
  FROM xxeis.vendor##final a, xxeis.xxwc_mtl_system_items_b#s#a b
 WHERE a.inventory_item_id(+) = b.inventory_item_id AND a.organization_id(+) = b.organization_id
 and 1=2';

         EXECUTE IMMEDIATE
            'insert /*+append*/ into  xxeis.vendor##fin
 SELECT a.vendor_number, b.inventory_item_id, b.organization_id
  FROM xxeis.vendor##final a, xxeis.xxwc_mtl_system_items_b#s#a b
 WHERE a.inventory_item_id(+) = b.inventory_item_id AND a.organization_id(+) = b.organization_id';

         COMMIT;
      END;

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'VENDOR##FINAL');
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'VENDOR##IOTFH');
         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'VENDOR##IOTL');

         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_vendor##z ');

         EXECUTE IMMEDIATE
            'CREATE TABLE xxeis.eis_po_xxwc_isr_vendor##z
(VENDOR_NUMBER
   ,inventory_item_id
   ,organization_id
   ,CONSTRAINT xxeis_vendor##zs PRIMARY KEY (inventory_item_id, organization_id)
)
ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT *
  FROM xxeis.vendor##fin
 WHERE  organization_id <>222';

         COMMIT;
      END;

      BEGIN
         apps.xxwc_common_tunning_helpers.drop_temp_table (
            'xxeis',
            'eis_po_xxwc_isr_vendor##mast ');

         EXECUTE IMMEDIATE
            'CREATE TABLE xxeis.eis_po_xxwc_isr_vendor##mast
(VENDOR_NUMBER
   ,inventory_item_id
   ,organization_id
   ,CONSTRAINT xxeis_vendor##mast PRIMARY KEY (inventory_item_id, organization_id)
)
ORGANIZATION INDEX
TABLESPACE xxeis_data
PARALLEL (DEGREE 4)
AS
SELECT *
  FROM xxeis.vendor##fin
 WHERE  organization_id =222';

         apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                           'VENDOR##fin');
         COMMIT;
      END;
   END;

   --*******************************************
   --added by Rasikha 02/12/2014 to calculate correct value on_ord quantity TMS 20140204-00148
   PROCEDURE populate_on_ord
   IS
      v_clob   CLOB;
   BEGIN
      apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                        'eis_po_on_ord##');

      EXECUTE IMMEDIATE '
CREATE TABLE xxeis.eis_po_on_ord##
(
    inventory_item_id    NUMBER
   ,organization_id      NUMBER
   ,ordered_uom          VARCHAR2 (64)
   ,primary_uom          VARCHAR2 (64)
   ,quantity             NUMBER
   ,quantity_received    NUMBER
   ,quantity_cancelled   NUMBER
)';

      xxeis.eis_po_xxwc_isr_pkg_v2.alter_table_temp ('XXEIS',
                                                     'eis_po_on_ord##');

      FOR r
         IN (SELECT organization_id
               FROM org_organization_definitions
              WHERE     organization_id <> 222
                    AND operating_unit = 162
                    AND NVL (disable_date, SYSDATE + 1) > SYSDATE)
      LOOP
         v_clob :=
               'insert/*+ append */ into xxeis.eis_po_on_ord##

SELECT pol.item_id
      ,poll.ship_to_organization_id
      ,pol.unit_meas_lookup_code ordered_uom
      ,msi.primary_unit_of_measure primary_uom
      ,poll.quantity
      ,poll.quantity_received
      ,poll.quantity_cancelled
  FROM apps.po_headers_all poh
      ,apps.po_line_locations_all poll
      ,apps.po_lines_all pol
      ,apps.po_releases_all por
      ,apps.hr_locations_all_tl hl
      ,apps.mtl_system_items msi
      ,apps.mtl_units_of_measure mum
      ,apps.po_line_types_b polt
      ,apps.gl_daily_conversion_types dct
      ,apps.rcv_parameters rp
 WHERE NVL (poll.approved_flag, ''N'') = ''Y''
       AND NVL (poll.cancel_flag, ''N'') = ''N''
       AND NVL (pol.clm_info_flag, ''N'') = ''N''
       AND (NVL (pol.clm_option_indicator, ''B'') <> ''O'' OR NVL (pol.clm_exercised_flag, ''N'') = ''Y'')
       AND poll.closed_code IN (''OPEN'', ''CLOSED FOR INVOICE'')
       AND poll.shipment_type IN (''STANDARD'', ''BLANKET'', ''SCHEDULED'')
       AND poh.po_header_id = poll.po_header_id
       AND pol.po_line_id = poll.po_line_id
       AND poll.po_release_id = por.po_release_id(+)
       AND poll.ship_to_location_id = hl.location_id(+)
       AND pol.line_type_id = polt.line_type_id(+)
       AND mum.unit_of_measure(+) = pol.unit_meas_lookup_code
       AND NVL (msi.organization_id, poll.ship_to_organization_id) = poll.ship_to_organization_id
       AND msi.inventory_item_id(+) = pol.item_id
       AND dct.conversion_type(+) = poh.rate_type
       AND NVL (poh.consigned_consumption_flag, ''N'') = ''N''
       AND NVL (por.consigned_consumption_flag, ''N'') = ''N''
       AND NVL (poll.matching_basis, ''QUANTITY'') != ''AMOUNT''
       AND poll.payment_type IS NULL
       AND NVL (poll.drop_ship_flag, ''N'') = ''N''
       AND rp.organization_id = poll.ship_to_organization_id
       AND (NVL (rp.pre_receive, ''N'') = ''N'' OR (NVL (rp.pre_receive, ''N'') = ''Y'' AND NVL (poll.lcm_flag, ''N'') = ''N''))
       and ( poll.quantity -nvl(poll.quantity_received,0) -nvl(poll.quantity_cancelled,0))<>0
       AND (   EXISTS
                   (SELECT ''Not associated to WIP Job''
                      FROM apps.po_distributions_all pod1, apps.po_lines_all pltv
                     WHERE     pod1.po_line_id = pltv.po_line_id
                           AND pod1.line_location_id = poll.line_location_id
                           AND pod1.wip_entity_id IS NULL)
            OR EXISTS
                   (SELECT ''Jobs not related to EAM WO or Closed WIP Jobs''
                      FROM apps.po_distributions_all pod1, apps.po_lines_all pltv, apps.wip_entities we
                     WHERE     pod1.po_line_id = pltv.po_line_id
                           AND pod1.line_location_id = poll.line_location_id
                           AND pod1.wip_entity_id = we.wip_entity_id
                           AND we.entity_type NOT IN (6, 7, 3))
            OR EXISTS
                   (SELECT ''Open EAM WO Receipts''
                      FROM apps.po_distributions_all pod1
                          ,apps.po_lines_all pltv
                          ,apps.wip_entities we
                          ,apps.wip_discrete_jobs wdj
                     WHERE     pod1.line_location_id = poll.line_location_id
                           AND pod1.wip_entity_id = we.wip_entity_id
                           AND we.wip_entity_id = wdj.wip_entity_id
                           AND we.entity_type = 6
                           AND wdj.status_type IN (3, 4, 6)))
       AND poll.ship_to_organization_id ='
            || r.organization_id;

         EXECUTE IMMEDIATE v_clob;

         COMMIT;
      END LOOP;

      FOR r
         IN (SELECT organization_id
               FROM org_organization_definitions
              WHERE     organization_id <> 222
                    AND operating_unit = 162
                    AND NVL (disable_date, SYSDATE + 1) > SYSDATE)
      LOOP
         v_clob :=
               'insert/*+ append */ into xxeis.eis_po_on_ord##
SELECT b.item_id
      ,b.destination_organization_id
      ,d.unit_of_measure
      ,c.primary_unit_of_measure
      ,b.quantity
      ,b.quantity_received
      ,b.quantity_cancelled
  FROM apps.po_requisition_lines_all b
      ,apps.po_requisition_headers_all a
      ,apps.mtl_system_items c
      ,apps.mtl_units_of_measure d
 WHERE     b.source_type_code = ''INVENTORY''
       AND NVL (b.cancel_flag, ''N'') = ''N''
       AND a.type_lookup_code = ''INTERNAL''
       AND a.authorization_status = ''APPROVED''
       AND a.requisition_header_id = b.requisition_header_id
       AND b.item_id = c.inventory_item_id
       AND b.unit_meas_lookup_code = d.unit_of_measure
       and ( b.quantity -nvl(b.quantity_received,0) -nvl(b.quantity_cancelled,0))<>0
       AND b.destination_organization_id = c.organization_id
       AND NVL (b.drop_ship_flag, ''N'') = ''N'' and b.destination_organization_id ='
            || r.organization_id;

         EXECUTE IMMEDIATE v_clob;

         COMMIT;
      END LOOP;

      apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                        'eis_on_ord##');

      EXECUTE IMMEDIATE '
CREATE TABLE xxeis.eis_on_ord##
(
    inventory_item_id    NUMBER
   ,organization_id      NUMBER
   ,on_ord number
)';

      xxeis.eis_po_xxwc_isr_pkg_v2.alter_table_temp ('XXEIS', 'eis_on_ord##');

      v_clob :=
         'insert/*+ append */ into xxeis.eis_on_ord## SELECT inventory_item_id
        ,organization_id
        ,SUM (
               (quantity - NVL (quantity_received, 0) - NVL (quantity_cancelled, 0))
             * po_uom_s.po_uom_convert (ordered_uom, primary_uom, inventory_item_id))
             on_ord
    FROM xxeis.eis_po_on_ord##
GROUP BY inventory_item_id, organization_id';

      EXECUTE IMMEDIATE v_clob;

      COMMIT;
      apps.xxwc_common_tunning_helpers.drop_temp_table ('xxeis',
                                                        'eis_po_on_ord##');
   END;


   /* ---------------------------------------------
       Procedure to populate the BPA cost
   -----------------------------------------------*/

   PROCEDURE populate_bpa_cost
   /*************************************************************************
     $Header EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST $
     Module Name: EIS_PO_XXWC_ISR_PKG_V2

     PURPOSE: populate BPA cost

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        07/31/2014  Manjula Chellappan    TMS # 20140728-00130
     1.1        11/10/2014  Manjula Chellappan    TMS # 20140908-00200 - Logic change for BPA


   **************************************************************************/
   IS
      --Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic Begin
      l_drop_table       VARCHAR2 (400)
                            := 'DROP TABLE XXEIS.EIS_PO_XXWC_ISR_ITEM_SOURCE##';

      l_create_table     VARCHAR2 (4000)
         := ' CREATE TABLE XXEIS.EIS_PO_XXWC_ISR_ITEM_SOURCE## AS
               SELECT msa.organization_id,                                   
                      msa.inventory_item_id,                    
                      msa.sourcing_rule_id,                    
                      msr.sourcing_rule_name sourcing_rule,                    
                      msro.sr_receipt_id,                    
                      msso.vendor_id,                    
                      aps.vendor_name vendor_name,                    
                      aps.segment1 vendor_num,                    
                      msso.vendor_site_id,                    
                      ass.vendor_site_code  vendor_site                  
-- Commented By Manjula on 17-Dec-14 TMS # 20140908-00200 Updated BPA Logic 
-- to select data only for the assignment set WC Default  
--                FROM mrp_sr_assignments msa, 
                FROM (SELECT * FROM mrp_sr_assignments WHERE assignment_type = 6 AND assignment_set_id = 1 )  msa ,               
                      mrp_sr_receipt_org msro,                    
                      mrp_sr_source_org msso,                    
                      mrp_sourcing_rules msr,                    
                      ap_suppliers aps,                    
                      ap_supplier_sites_all ass                    
                WHERE     msa.sourcing_rule_id = msro.sourcing_rule_id(+)                    
                      AND msa.sourcing_rule_id = msr.sourcing_rule_id(+)                    
                      AND msro.sr_receipt_id = msso.sr_receipt_id(+)                    
                      AND msso.vendor_id = aps.vendor_id(+)                    
                      AND msso.vendor_site_id = ass.vendor_site_id(+)
                      AND msa.assignment_type=6
                      AND msa.assignment_set_id = 1
                      AND msso.source_type = 3  ';

      l_error_msg        VARCHAR2 (4000)
         := 'Error Loading XXEIS.EIS_PO_XXWC_ISR_ITEM_SOURCE## : ';

      l_success_msg      VARCHAR2 (4000)
         := ' Loading XXEIS.EIS_PO_XXWC_ISR_ITEM_SOURCE## Completed ';

      l_drop_msg         VARCHAR2 (400)
                            := ' XXEIS.EIS_PO_XXWC_ISR_ITEM_SOURCE## dropped ';

      l_table_name       VARCHAR2 (100) := 'EIS_PO_XXWC_ISR_ITEM_SOURCE##';

      l_table_count      NUMBER := 0;

      l_create_index     VARCHAR2 (4000)
         := 'CREATE INDEX XXEIS.EIS_PO_XXWC_ISR_ITEM_SOUR##_N1
                       on XXEIS.EIS_PO_XXWC_ISR_ITEM_SOURCE## (organization_id, inventory_item_id, vendor_id, vendor_site_id)';

      --Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic End


      l_drop_table1      VARCHAR2 (400)
                            := 'DROP TABLE XXEIS.EIS_PO_XXWC_ISR_BPA_STG##';

      /*--Commented by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic 
            l_create_table1    VARCHAR2 (4000)
               := 'CREATE TABLE XXEIS.eis_po_xxwc_isr_bpa_line## AS
                   SELECT *
               FROM (SELECT pol.po_line_id po_line_id,
                    poh.po_header_id,
                    TRUNC (poh.creation_date) po_creation_date,
                    poh.vendor_id,
                    poh.vendor_site_id,
                    poh.global_agreement_flag,
                    pol.item_id inventory_item_id,
                    NVL(poll.ship_to_organization_id,z.organization_id) organization_id,
                    NVL (pol.cancel_flag, ''N'') l_cancel_flag,
                    NVL (poh.cancel_flag, ''N'') h_cancel_flag,
                    pol.unit_price,
                    NVL (pol.quantity, 0) quantity,
                    ROW_NUMBER ()
                    OVER (
                       PARTITION BY pol.item_id,
                            NVL(poll.ship_to_organization_id,z.organization_id),
                            NVL (pol.cancel_flag, ''N'')
                       ORDER BY pol.po_line_id DESC)
                       AS rn
                   FROM po_headers_all poh,
                    po_line_locations_all poll,
                    po_lines_all pol,
                    xxeis.eis_po_xxwc_isr_vendor##z z
                  WHERE     poh.type_lookup_code = ''BLANKET''
                    AND poh.po_header_id = pol.po_header_id
                    AND pol.po_line_id = poll.po_line_id(+)
                    AND NVL (pol.cancel_flag, ''N'') = ''N''
                    AND NVL (poh.cancel_flag, ''N'') = ''N''
                    AND pol.item_id = z.inventory_item_id
                    AND z.organization_id =
                       DECODE (poh.global_agreement_flag,
                           ''Y'', z.organization_id,
                           NVL(poll.ship_to_organization_id, z.organization_id))
                    ) l
              WHERE rn = 1';
      */

      l_create_table1    VARCHAR2 (4000)
         := ' CREATE TABLE XXEIS.eis_po_xxwc_isr_bpa_stg## AS
        SELECT *
          FROM (SELECT pol.po_line_id po_line_id,
                   poh.po_header_id,
                   poh.segment1 bpa,
                   TRUNC (poh.creation_date) po_creation_date,
                   poh.vendor_id,
                   poh.vendor_site_id,
                   poh.global_agreement_flag,
                   z.vendor_num,
                   z.vendor_name,
                   z.vendor_site,
                   NVL (pol.closed_code, ''X'') l_closed_code,
                   NVL (poh.closed_code, ''X'') h_closed_code,
                   pol.item_id inventory_item_id,
                   z.organization_id,
                   NVL (pol.cancel_flag, ''N'') l_cancel_flag,
                   NVL (poh.cancel_flag, ''N'') h_cancel_flag,
                   pol.unit_price,
                   NVL (pol.quantity, 0) quantity,
                   ROW_NUMBER ()
                   OVER (PARTITION BY pol.item_id, z.organization_id, NVL (pol.cancel_flag, ''N'')
                     ORDER BY pol.po_line_id DESC)
                  AS rn
              FROM po_headers_all poh,
                   po_lines_all pol,
                   xxeis.eis_po_xxwc_isr_item_source## z
             WHERE     poh.type_lookup_code = ''BLANKET''
                   AND poh.po_header_id = pol.po_header_id
                   AND NVL (pol.cancel_flag, ''N'') = ''N''
                   AND NVL (poh.cancel_flag, ''N'') = ''N''
                   AND pol.item_id = z.inventory_item_id
                   AND NVL (pol.closed_code, ''X'') <> ''CLOSED''
                   AND NVL (poh.closed_code, ''X'') <> ''CLOSED''
                   AND NVL (poh.authorization_status, ''X'') = ''APPROVED'' 
                   AND NVL (TRUNC(poh.end_date) , NVL (TRUNC (pol.expiration_date), TRUNC (SYSDATE) + 1)) >
                      TRUNC (SYSDATE)
                   AND poh.vendor_id = z.vendor_id
                   AND poh.vendor_site_id = z.vendor_site_id) l
         WHERE     l.rn = 1
               AND (   l.global_agreement_flag = ''Y''
                OR EXISTS
                  (SELECT ''x''
                     FROM po_line_locations_all
                    WHERE     po_line_id = l.po_line_id
                      AND ship_to_organization_id = l.organization_id)) ';

      l_error_msg1       VARCHAR2 (4000)
         := 'Error Loading XXEIS.EIS_PO_XXWC_ISR_BPA_STG## : ';

      l_success_msg1     VARCHAR2 (4000)
         := ' Loading XXEIS.EIS_PO_XXWC_ISR_BPA_STG## Completed ';

      l_drop_msg1        VARCHAR2 (400)
                            := ' XXEIS.EIS_PO_XXWC_ISR_BPA_STG## dropped ';

      l_table_name1      VARCHAR2 (100) := 'EIS_PO_XXWC_ISR_BPA_STG##';

      l_table_count1     NUMBER := 0;

      l_create_index1    VARCHAR2 (4000)
         := 'CREATE INDEX XXEIS.EIS_PO_XXWC_ISR_BPA_STG##_N1
                                on XXEIS.EIS_PO_XXWC_ISR_BPA_STG## (po_header_id)';

      --Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic Begin
      l_drop_table1a     VARCHAR2 (400)
         := 'DROP TABLE XXEIS.EIS_PO_XXWC_ISR_BPA_LINE##';

      l_create_table1a   VARCHAR2 (4000)
         := ' CREATE TABLE xxeis.eis_po_xxwc_isr_bpa_line## AS
                SELECT bpa_stg.*
                  FROM xxeis.eis_po_xxwc_isr_bpa_stg## bpa_stg
                 WHERE 1 =
                          (SELECT COUNT (*)
                             FROM po_lines_all
                            WHERE     po_header_id = bpa_stg.po_header_id
                                  AND item_id = bpa_stg.inventory_item_id
                                  AND (    NVL (closed_code, ''X'') = bpa_stg.l_closed_code
                                       AND NVL (cancel_flag, ''N'') = bpa_stg.l_cancel_flag)) ';
                                       
      l_error_msg1a      VARCHAR2 (4000)
         := 'Error Loading XXEIS.EIS_PO_XXWC_ISR_BPA_LINE## : ';

      l_success_msg1a    VARCHAR2 (4000)
         := ' Loading XXEIS.EIS_PO_XXWC_ISR_BPA_LINE## Completed ';

      l_drop_msg1a       VARCHAR2 (400)
                            := ' XXEIS.EIS_PO_XXWC_ISR_BPA_LINE## dropped ';

      l_table_name1a     VARCHAR2 (100) := 'EIS_PO_XXWC_ISR_BPA_LINE##';

      l_table_count1a    NUMBER := 0;

      l_create_index1a   VARCHAR2 (4000)
         := 'CREATE INDEX XXEIS.EIS_PO_XXWC_ISR_BPA_LINE##_N1
                       on XXEIS.EIS_PO_XXWC_ISR_BPA_LINE## (organization_id, inventory_item_id)';

      --Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic End



      l_drop_table2      VARCHAR2 (6000)
                            := 'DROP TABLE xxeis.eis_po_xxwc_isr_bpa_price##';

      /*--Commented by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic
            l_create_table2    VARCHAR2 (6000)
               := 'CREATE TABLE xxeis.eis_po_xxwc_isr_bpa_price## as
                    SELECT a.*,
                         NVL (b.price_zone, 0) price_zone,
                          (SELECT promo_price
                                FROM xxwc.xxwc_bpa_promo_tbl
                               WHERE     po_header_id = a.po_header_id
                                     AND inventory_item_id = a.inventory_item_id
                                     AND a.po_creation_date BETWEEN NVL (promo_start_date,
                                                                         po_creation_date)
                                                                AND NVL (promo_end_date,
                                                                         po_creation_date))
                         promo_price,
                         c.price_zone_price national_price,
                         d.price_zone_price,
                         e.price_zone_quantity_price national_break_price,
                         f.price_zone_quantity_price price_zone_break_price
                    FROM xxeis.eis_po_xxwc_isr_bpa_line## a,
                         xxwc.xxwc_vendor_price_zone_tbl b,
                         (SELECT *
                        FROM xxwc.xxwc_bpa_price_zone_tbl
                       WHERE price_zone = 0) c,
                      (SELECT a.*, c.organization_id
                        FROM xxwc.xxwc_bpa_price_zone_tbl a,
                             xxwc.xxwc_vendor_price_zone_tbl b,
                             xxeis.eis_po_xxwc_isr_bpa## c
                       WHERE     a.price_zone <> 0
                             AND a.inventory_item_id = c.inventory_item_id
                             AND a.po_header_id = c.po_header_id
                             AND b.vendor_id = c.vendor_id
                             AND b.vendor_site_id = c.vendor_site_id
                             AND b.organization_id = c.organization_id
                             AND a.price_zone = b.price_zone
                                       ) d ,
                         (SELECT *
                        FROM xxwc.xxwc_bpa_price_zone_breaks_tbl
                       WHERE price_zone = 0) e,
                      (SELECT a.*, c.organization_id
                        FROM xxwc.xxwc_bpa_price_zone_breaks_tbl a,
                             xxwc.xxwc_vendor_price_zone_tbl b,
                             xxeis.eis_po_xxwc_isr_bpa_line## c
                       WHERE     a.price_zone <> 0
                             AND a.inventory_item_id = c.inventory_item_id
                             AND a.po_header_id = c.po_header_id
                             AND b.vendor_id = c.vendor_id
                             AND b.vendor_site_id = c.vendor_site_id
                             AND b.organization_id = c.organization_id
                             AND a.price_zone = b.price_zone
                                       ) f
                   WHERE     a.vendor_id = b.vendor_id(+)
                         AND a.vendor_site_id = b.vendor_site_id(+)
                         AND a.organization_id = b.organization_id(+)
                         AND a.po_header_id = c.po_header_id(+)
                         AND a.inventory_item_id = c.inventory_item_id(+)
                         AND a.po_header_id = d.po_header_id(+)
                         AND a.inventory_item_id = d.inventory_item_id(+)
                         AND a.organization_id = d.organization_id(+)
                         AND a.po_header_id = e.po_header_id(+)
                         AND a.inventory_item_id = e.inventory_item_id(+)
                         AND NVL (e.price_zone_quantity, 0) =
                            (SELECT NVL (MAX (price_zone_quantity), NVL (e.price_zone_quantity, 0))
                           FROM xxwc.xxwc_bpa_price_zone_breaks_tbl
                          WHERE     po_header_id = a.po_header_id
                                AND inventory_item_id = a.inventory_item_id
                                AND price_zone_quantity <= a.quantity)
                         AND a.po_header_id = f.po_header_id(+)
                         AND a.inventory_item_id = f.inventory_item_id(+)
                         AND a.organization_id = f.organization_id(+)
                         AND NVL (f.price_zone_quantity, 0) =
                            (SELECT NVL (MAX (price_zone_quantity), NVL (f.price_zone_quantity, 0))
                           FROM xxwc.xxwc_bpa_price_zone_breaks_tbl
                          WHERE     po_header_id = a.po_header_id
                                AND inventory_item_id = a.inventory_item_id
                                AND NVL (b.price_zone, 0) = NVL (f.price_zone, 0)
                                AND price_zone_quantity <= a.quantity)';
      */


      l_create_table2    VARCHAR2 (6000)
         := ' CREATE TABLE xxeis.eis_po_xxwc_isr_bpa_price## as
              SELECT a.*,
                   NVL (b.price_zone, 0) price_zone,
                    (SELECT promo_price
                          FROM xxwc.xxwc_bpa_promo_tbl
                         WHERE     po_header_id = a.po_header_id
                               AND inventory_item_id = a.inventory_item_id
                               AND TRUNC(SYSDATE) BETWEEN NVL (promo_start_date,
                                                                   TRUNC(SYSDATE))
                                                          AND NVL (promo_end_date,
                                                                   TRUNC(SYSDATE)))
                   promo_price,
                   c.price_zone_price national_price,
                   d.price_zone_price
              FROM xxeis.eis_po_xxwc_isr_bpa_line## a,
                   xxwc.xxwc_vendor_price_zone_tbl b,               
                   (SELECT *
                  FROM xxwc.xxwc_bpa_price_zone_tbl
                 WHERE price_zone = 0) c,
                (SELECT a.*, c.organization_id
                  FROM xxwc.xxwc_bpa_price_zone_tbl a,
                       xxwc.xxwc_vendor_price_zone_tbl b,
                       xxeis.eis_po_xxwc_isr_bpa_line## c
                 WHERE     a.price_zone <> 0
                       AND a.inventory_item_id = c.inventory_item_id
                       AND a.po_header_id = c.po_header_id
                       AND b.vendor_id = c.vendor_id
                       AND b.vendor_site_id = c.vendor_site_id
                       AND b.organization_id = c.organization_id
                       AND a.price_zone = b.price_zone
                                 ) d                          
             WHERE     a.vendor_id = b.vendor_id(+)
                   AND a.vendor_site_id = b.vendor_site_id(+) 
                   AND a.organization_id = b.organization_id(+)               
                   AND a.po_header_id = c.po_header_id(+)
                   AND a.inventory_item_id = c.inventory_item_id(+)
                   AND a.po_header_id = d.po_header_id(+)
                   AND a.inventory_item_id = d.inventory_item_id(+)
                   AND a.organization_id = d.organization_id(+)                   
                   ';

      l_error_msg2       VARCHAR2 (4000)
         := 'Error Loading XXEIS.EIS_PO_XXWC_ISR_BPA_PRICE## : ';

      l_success_msg2     VARCHAR2 (4000)
         := ' Loading XXEIS.EIS_PO_XXWC_ISR_BPA_PRICE## Completed ';

      l_drop_msg2        VARCHAR2 (400)
                            := ' XXEIS.EIS_PO_XXWC_ISR_BPA_PRICE## dropped ';

      l_table_name2      VARCHAR2 (100) := 'EIS_PO_XXWC_ISR_BPA_PRICE##';

      l_table_count2     NUMBER := 0;

      l_create_index2    VARCHAR2 (4000)
         := 'CREATE INDEX XXEIS.EIS_PO_XXWC_ISR_BPA_PRICE##_N1
                                on XXEIS.EIS_PO_XXWC_ISR_BPA_PRICE## (po_header_id)';

      l_Procedure_name   VARCHAR2 (100)
                            := 'EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST';

      l_sec              VARCHAR2 (200);
      l_error_msg_final        CLOB;
   BEGIN
      --Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic     Begin
      l_sec :=
         'Checking table exists at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';

      SELECT COUNT (*)
        INTO l_table_count
        FROM all_tables
       WHERE table_name = l_table_name AND owner = g_owner;

      IF l_table_count = 1
      THEN
         EXECUTE IMMEDIATE l_drop_table;

         DBMS_OUTPUT.put_line (
            l_drop_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
            l_drop_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      END IF;


      l_sec := 'creating table at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';


      EXECUTE IMMEDIATE l_create_table;

      l_sec := 'creating index at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';

      EXECUTE IMMEDIATE l_create_index;

      DBMS_OUTPUT.put_line (
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
         l_success_msg || ' at ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


      --Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic End

      l_sec :=
         'Checking table1 exists at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';

      SELECT COUNT (*)
        INTO l_table_count1
        FROM all_tables
       WHERE table_name = l_table_name1 AND owner = g_owner;

      IF l_table_count1 = 1
      THEN
         EXECUTE IMMEDIATE l_drop_table1;

         DBMS_OUTPUT.put_line (
               l_drop_msg1
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_drop_msg1
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      END IF;


      l_sec := 'creating table1 at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';


      EXECUTE IMMEDIATE l_create_table1;

      l_sec := 'creating index1 at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';

      EXECUTE IMMEDIATE l_create_index1;

      DBMS_OUTPUT.put_line (
            l_success_msg1
         || ' at '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
            l_success_msg1
         || ' at '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

      --Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic     Begin
      l_sec :=
         'Checking table1a exists at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';

      SELECT COUNT (*)
        INTO l_table_count1a
        FROM all_tables
       WHERE table_name = l_table_name1a AND owner = g_owner;

      IF l_table_count1a = 1
      THEN
         EXECUTE IMMEDIATE l_drop_table1a;

         DBMS_OUTPUT.put_line (
               l_drop_msg1a
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_drop_msg1a
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      END IF;


      l_sec := 'creating table1a at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';


      EXECUTE IMMEDIATE l_create_table1a;

      l_sec := 'creating index1a at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';

      EXECUTE IMMEDIATE l_create_index1a;

      DBMS_OUTPUT.put_line (
            l_success_msg1a
         || ' at '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
            l_success_msg1a
         || ' at '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));


      --Added by Manjula on 10-Nov-14 for TMS # 20140908-00200 Updated BPA Logic End


      l_sec :=
         'Checking table2 exists at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';

      SELECT COUNT (*)
        INTO l_table_count2
        FROM all_tables
       WHERE table_name = l_table_name2 AND owner = g_owner;

      IF l_table_count2 = 1
      THEN
         EXECUTE IMMEDIATE l_drop_table2;

         DBMS_OUTPUT.put_line (
               l_drop_msg2
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_drop_msg2
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      END IF;


      l_sec := 'creating table2 at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';


      EXECUTE IMMEDIATE l_create_table2;

      l_sec := 'creating index2 at EIS_PO_XXWC_ISR_PKG_V2.POPULATE_BPA_COST ';

      EXECUTE IMMEDIATE l_create_index2;



      DBMS_OUTPUT.put_line (
            l_success_msg2
         || ' at '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (
         fnd_file.LOG,
            l_success_msg2
         || ' at '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg_final :=
            l_error_msg_final || CHR (10) || l_sec || CHR (10) || SQLERRM;

         l_errbuf := g_errbuf || CHR (10) || l_error_msg_final;

         DBMS_OUTPUT.put_line (
               l_error_msg_final
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
         fnd_file.put_line (
            fnd_file.LOG,
               l_error_msg_final
            || ' at '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   END populate_bpa_cost;
END;
/

GRANT EXECUTE ON xxeis.eis_po_xxwc_isr_pkg_v2 TO interface_prism
/
GRANT EXECUTE ON xxeis.eis_po_xxwc_isr_pkg_v2 TO apps
/
