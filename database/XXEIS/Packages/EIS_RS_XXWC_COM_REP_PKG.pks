CREATE OR REPLACE PACKAGE XXEIS.eis_rs_xxwc_com_rep_pkg
   AUTHID CURRENT_USER
--//============================================================================
--//
--// Object Name         :: xxeis.eis_rs_xxwc_com_rep_pkg
--//
--// Object Type         :: Package Specification
--//
--// Object Description  :: This Package will triger in before report and insert the values into Temp
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahender Reddy  07/07/2014    Initial Build --TMS#20140602-00298 by Mahender on 07-07-2014
--//============================================================================
IS

--//============================================================================
--//
--// Object Name         :: comm_rep_par
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure gets the record based on parameter and insert into temp table
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahender Reddy  07/07/2014    Initial Build --TMS#20140602-00298 by Mahender on 07-07-2014
--//============================================================================
   PROCEDURE comm_rep_par (p_period_name      IN VARCHAR2
                          ,p_operating_unit   IN VARCHAR2
                          ,p_invoice_source   IN VARCHAR2
                          );
END eis_rs_xxwc_com_rep_pkg;
/