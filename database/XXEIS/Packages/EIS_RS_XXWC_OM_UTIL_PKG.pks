CREATE OR REPLACE package XXEIS.EIS_RS_XXWC_OM_UTIL_PKG
   AUTHID CURRENT_USER
IS
   /*************************************************************************
     $Header EIS_RS_XXWC_OM_UTIL_PKG $
     Module Name: EIS_RS_XXWC_OM_UTIL_PKG.pks

     PURPOSE:   This package holds the utilities for EIS OrderManagement Reports

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------    -------------------------
     1.0        03/26/2014  Gopi Damuluri      Initial Version
                                               TMS# 20140326-00106 Performance Tuning of Open SalesOrders Report
   **************************************************************************/

   PROCEDURE open_sales_order_pre_trig(p_org_id  varchar2, p_salesrep_name  varchar2, p_ordered_date_from  varchar2, p_ordered_date_to  varchar2);

END EIS_RS_XXWC_OM_UTIL_PKG;
/
