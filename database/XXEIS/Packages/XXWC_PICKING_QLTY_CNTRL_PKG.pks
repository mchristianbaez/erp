CREATE OR REPLACE 
PACKAGE   XXEIS.XXWC_PICKING_QLTY_CNTRL_PKG AS
--//============================================================================
--//
--// Object Usage 				:: This Object Referred by "Picking Quality Control Leadership report - WC"
--//
--// Object Name         		:: XXWC_PICKING_QLTY_CNTRL_PKG
--//
--// Object Type         		:: Package Spec
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20180116-00278 
--//============================================================================

type T_VARCHAR_TYPE_VLDN_TBL is table of varchar2(200) index by varchar2(200);

g_avg_time_per_order_vldn_tbl   T_VARCHAR_TYPE_VLDN_TBL;
g_avg_time_per_order number;


FUNCTION GET_AVG_TIME_PER_ORDER_FUNC(
    P_ORGANIZATION VARCHAR2,
    P_FROM_DATE   DATE,
    P_TO_DATE DATE)
  RETURN number;
  --//============================================================================
--//
--// Object Name         :: GET_AVG_TIME_PER_ORDER_FUNC
--//
--// Object Usage 		 :: This Object Referred by "Picking Quality Control Leadership report - WC"
--//
--// Object Type         :: Function
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_PICKING_QLTY_CNTRL_V View.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20180116-00278 
--//============================================================================

procedure remove_rpt_columns(p_report_type varchar2,p_process_id number);
--//============================================================================
--//
--// Object Name         :: remove_rpt_columns
--//
--// Object Usage 		   :: This Object Referred by "Picking Quality Control Leadership report - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_PICKING_QLTY_CNTRL_V View.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20180116-00278 
--//============================================================================
 
Procedure Get_Picking_Qlty_Cntrl_Proc(
    p_REPORT_TYPE IN VARCHAR2,
    p_Branch      IN VARCHAR2,
    p_District    IN VARCHAR2,
    p_Region      IN VARCHAR2,
    p_From_Date   IN DATE,
    p_To_Date     IN DATE);
--//============================================================================
--//
--// Object Name         :: get_picking_qlty_cntrl_proc
--//
--// Object Usage 		   :: This Object Referred by "Picking Quality Control Leadership report - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_PICKING_QLTY_CNTRL_V View.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20180116-00278 
--//============================================================================

 type CURSOR_TYPE4 is ref cursor;



End;
/
