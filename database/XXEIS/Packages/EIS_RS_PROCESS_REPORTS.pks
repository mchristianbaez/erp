CREATE OR REPLACE PACKAGE XXEIS.eis_rs_process_reports
  AUTHID CURRENT_USER IS
--
-- Purpose: This package contains procedures for processing reports for EIS RS tool.
--
-- MODIFICATION HISTORY
-- Person      Date          Version        Comments
-- ---------   ------        --------       ------------------------------------------
-- ASR         20-Feb-2004   1.0            Created the pkg
-- KP          20-Jun-2004   1.4.3          Modified get_url to add p_request_id parameter
--
-- Ankur       03-Jan-2005   1.4.7          Added Code for report Scheduling, Notification , opening URL from reports.
--                                          for this purpose added get_display_order ,increment_date ,user_authorization procedures.
-- Ankur       03-Jan-2005   1.4.7          Changed submit_conc_report procedure as well concurrent program , 2 new parameters p_notify and p_server_url is added.
--                                          Changed submit_process procedure to enable scheduling.
-- Ankur       08-Jan-2005   1.4.7          Added code for showing Scheduled Requests Page in frontend.
--                                          Added show_schedule_requests,terminate_schedule_requests,get_report_name , get_paramameters_Details for this purpose.
-- jyothi      28-Apr-2005   2.0.4        added parameter g_as_of_date
-- jyothi      28-Apr-2005   2.0.4          added function as_of_date
-- Nagraj      07-Dec-2005   4.0.0          Modified submit_process procedure remove summary report code
-- KP          22-Dec-2005   4.0.0          Added column_count parameter to get_segment_prompt
-- Sunil       08-May-2008   6.3.0          Added grants on package with grant option


-- $Header: eis_rs_process_reports.pkh 6.02 2007/12/18 02:57:29 $

-- MODIFICATION HISTORY
-- Person     Version   Header-No   Bug-No  Issue-No  Date         Comments
-- ---------  ------    ----------  ------  --------  -----        ---------
-- Sunil      6.0.2     6.0.2                         18-Dec-2007  Modified for Version Controlling.
--                                                                 The new modification history is to keep track of
--                                                                 all the fixes that got fixed by mentioning
--                                                                 respective bug No's of Bugzilla and Issue No's of
--                                                                 Salesforce and the version number in which the
--                                                                 fix as went.
-- Ashwin  6.3.0    6.04                  19-Feb-08    Added g_report_process_id var to check against the new process specific tables
-- Rajani  6.3.0                      18-Apr-08    Added extra parameters to copy_report_parameter() to fix the bug#754
-- Rajani  6.3.0                                      07-May-08    Modified parameters to copy_report_parameter() to fix the bug#754
-- KP      6.3.0                      11-aug-08    added insert_html_table for bulk insert
-- Soujanya 6.3.2.8                                   08-Dec-09    Modified to add global variables used in dynamic view generation on html tables.
-- Praveen.P 6.3.2.9        3533        18-Mar-10 increased size of parse_tbl_def ,to fix the issue
-- Praveen.P 7.0.0          1338,                     10-Nov-10    Created new procedures convert_char_to_date, set_date_params for bug#1338 for supporting
--                          1339                                   canonical format and ability to submit in any date format
-- Sravani   7.0.0                      05-Jan-11    Commented convert_char_to_date()
-- Rajani    7.0.0                      05-Jan-11    Commented set_date_params()
-- Madhavi.S 7.0.0           5185                     05-Jan-11    Modified to add submission_source column in submit_process procedure
-- Rajani    7.0.1                                    15-Oct-11    Added get_summary_calc_display_cols for summary calculations enhancement
-- Harsha    7.0.2          20130920-00347      20-12-2013 Added table to for Performance Tuning for EiS OpenSalesOrderReport

  TYPE cursor_type is REF CURSOR ;
  TYPE parse_tbl_def IS TABLE OF VARCHAR2(10000) INDEX BY BINARY_INTEGER;
  TYPE url_def IS TABLE OF VARCHAR2(500) INDEX BY BINARY_INTEGER;

  --Bulk Insert
TYPE t_html_table IS TABLE OF eis_rs_html_rslt_dtl1%ROWTYPE INDEX BY binary_INTEGER ;
g_html_table t_html_table ;
g_record_count NUMBER := 0 ;

--Rasmi on 03 Aug 2012
g_bulk_html_row_count NUMBER;
--End


  g_data_tbl          parse_tbl_def ;
  g_debug                   BOOLEAN := FALSE   ;
    sql_table DBMS_SQL.VARCHAR2S;
  g_log VARCHAR2(32000);
    g_param_tbl          parse_tbl_def ;
    g_process_id         number ;

    g_as_of_date     DATE;   ---Added by jyothi
    g_report_process_id number ; --6.04

-- below 2 are created for dynamic view creation code
g_test1 parse_tbl_def;
g_test2 parse_tbl_def;

  --this function returns a number > 0, if the supplied param_name is a parameters

  FUNCTION is_param_there( p_param_name  IN VARCHAR2
                          ,p_report_id   IN NUMBER  ) RETURN NUMBER;

  --This procedure is used for parsing a string into individual items based on the suplied deimiter

  PROCEDURE PARSE_DELIMITED_DATA ( p_input_data IN VARCHAR2,
                                   p_demiliter IN varchar2 DEFAULT ',' );

  -- this function returns supplied column data type

  FUNCTION return_col_data_type ( p_report_id   IN  NUMBER
                                , p_col_name IN VARCHAR2 ) RETURN VARCHAR2;

  --this function is used to build the report query dynamically

  FUNCTION next_row
     ( string_in    IN VARCHAR2,
       start_inout  IN OUT BINARY_INTEGER,
       len_in       IN BINARY_INTEGER ) RETURN VARCHAR2;

  --his procedure is used to build the report query dynamically

  PROCEDURE build_dyn_sql (string_in IN VARCHAR2);

  -- This procedure is used to dynamically build the report sql (uses dbms_sql package)


  --this procedure gets the sql behind report query

  --PROCEDURE get_query (  param1         IN VARCHAR2
  --                      ,param2         IN VARCHAR2
  --                      ,param3         IN VARCHAR2
  --                      ,param4         IN VARCHAR2
  --                      ,p_report_id    IN NUMBER
  --                      ,p_utl_dir      IN VARCHAR2 );

  --this is one of the main procedures where report process is submitted (whether it could be interactive or Concurrent)

  FUNCTION get_url(p_url_str IN VARCHAR2, p_request_id number DEFAULT NULL ) RETURN VARCHAR2;

  PROCEDURE set_location ( p_message IN VARCHAR2 );
  --Updated by Praveen.P to fix Bug#1338 , 1339
  --PROCEDURE set_date_params ( p_process_id in NUMBER,
                           -- p_type IN VARCHAR2 );
 -- FUNCTION convert_char_to_date ( p_text_date IN VARCHAR2 ) RETURN DATE ;


 FUNCTION get_view_sql ( p_report_id              IN NUMBER
                        ,p_param1                 IN VARCHAR2 DEFAULT NULL
                        ,p_param2                 IN VARCHAR2 DEFAULT NULL
                        ,p_param3                 IN VARCHAR2 DEFAULT NULL
                        ,p_param4                 IN VARCHAR2 DEFAULT NULL
                        ,p_param5                 IN VARCHAR2 DEFAULT NULL
                        ,p_param6                 IN VARCHAR2 DEFAULT NULL
                        ,p_param7                 IN VARCHAR2 DEFAULT NULL
                        ,p_param8                 IN VARCHAR2 DEFAULT NULL
                        ,p_param9                 IN VARCHAR2 DEFAULT NULL
                        ,p_param10                IN VARCHAR2 DEFAULT NULL
                        ,p_param11                IN VARCHAR2 DEFAULT NULL
                        ,p_param12                IN VARCHAR2 DEFAULT NULL
                        ,p_param13                IN VARCHAR2 DEFAULT NULL
                        ,p_param14                IN VARCHAR2 DEFAULT NULL
                        ,p_param15                IN VARCHAR2 DEFAULT NULL
                        ,p_param16                IN VARCHAR2 DEFAULT NULL
                        ,p_param17                IN VARCHAR2 DEFAULT NULL
                        ,p_param18                IN VARCHAR2 DEFAULT NULL
                        ,p_param19                IN VARCHAR2 DEFAULT NULL
                        ,p_param20                IN VARCHAR2 DEFAULT NULL
                        ,p_param21                IN VARCHAR2 DEFAULT NULL
                        ,p_param22                IN VARCHAR2 DEFAULT NULL
                        ,p_param23                IN VARCHAR2 DEFAULT NULL
                        ,p_param24                IN VARCHAR2 DEFAULT NULL
                        ,p_param25                IN VARCHAR2 DEFAULT NULL
                        ,p_param26                IN VARCHAR2 DEFAULT NULL
                        ,p_param27                IN VARCHAR2 DEFAULT NULL
                        ,p_param28                IN VARCHAR2 DEFAULT NULL
                        ,p_param29                IN VARCHAR2 DEFAULT NULL
                        ,p_param30                IN VARCHAR2 DEFAULT NULL
                 ,p_col_process_id         IN NUMBER   DEFAULT NULL
                         ) RETURN VARCHAR2;

  PROCEDURE delete_report(p_report_id IN NUMBER, p_status OUT VARCHAR2);
  PROCEDURE get_clobs( p_process_id IN NUMBER
                     ,p_result OUT cursor_type );
  PROCEDURE submit_conc_report ( p_errbuf                 OUT VARCHAR2
                               ,p_retcode                OUT NUMBER
                               ,p_report_id              IN NUMBER DEFAULT NULL
                               ,p_report_name            IN VARCHAR2 DEFAULT NULL
                               ,p_process_id             IN NUMBER DEFAULT null
                               ,p_param1                 IN VARCHAR2 DEFAULT NULL
                               ,p_param2                 IN VARCHAR2 DEFAULT NULL
                               ,p_param3                 IN VARCHAR2 DEFAULT NULL
                               ,p_param4                 IN VARCHAR2 DEFAULT NULL
                               ,p_param5                 IN VARCHAR2 DEFAULT NULL
                               ,p_param6                 IN VARCHAR2 DEFAULT NULL
                               ,p_param7                 IN VARCHAR2 DEFAULT NULL
                               ,p_param8                 IN VARCHAR2 DEFAULT NULL
                               ,p_param9                 IN VARCHAR2 DEFAULT NULL
                               ,p_param10                IN VARCHAR2 DEFAULT NULL
                               ,p_param11                IN VARCHAR2 DEFAULT NULL
                               ,p_param12                IN VARCHAR2 DEFAULT NULL
                               ,p_param13                IN VARCHAR2 DEFAULT NULL
                               ,p_param14                IN VARCHAR2 DEFAULT NULL
                               ,p_param15                IN VARCHAR2 DEFAULT NULL
                               ,p_param16                IN VARCHAR2 DEFAULT NULL
                               ,p_param17                IN VARCHAR2 DEFAULT NULL
                               ,p_param18                IN VARCHAR2 DEFAULT NULL
                               ,p_param19                IN VARCHAR2 DEFAULT NULL
                               ,p_param20                IN VARCHAR2 DEFAULT NULL
                               ,p_param21                IN VARCHAR2 DEFAULT NULL
                               ,p_param22                IN VARCHAR2 DEFAULT NULL
                               ,p_param23                IN VARCHAR2 DEFAULT NULL
                               ,p_param24                IN VARCHAR2 DEFAULT NULL
                               ,p_param25                IN VARCHAR2 DEFAULT NULL
                               ,p_param26                IN VARCHAR2 DEFAULT NULL
                               ,p_param27                IN VARCHAR2 DEFAULT NULL
                               ,p_param28                IN VARCHAR2 DEFAULT NULL
                               ,p_param29                IN VARCHAR2 DEFAULT NULL
                               ,p_param30                IN VARCHAR2 DEFAULT NULL
                               ,p_user_id                IN NUMBER DEFAULT -1
                               ,p_session_id             IN NUMBER
                               ,p_notify                 IN VARCHAR2 DEFAULT NULL
                               ,p_server_url             IN VARCHAR2 DEFAULT NULL
                               );
  PROCEDURE get_display_order(p_report_id IN NUMBER ,
                            p_parameter_name IN  VARCHAR2 ,
                            p_display_index OUT NUMBER,
                            p_date OUT VARCHAR2 );
  FUNCTION get_parameters_details( p_request_id IN  NUMBER  ,
                                  p_report_id IN  NUMBER
                                ) RETURN VARCHAR2;
  PROCEDURE show_schedule_requests( p_user_name IN  VARCHAR2 ,
                                  p_main OUT cursor_type
                                );
  FUNCTION get_report_name(p_report_id IN  VARCHAR2 )
 RETURN VARCHAR2;
  PROCEDURE terminate_schedule_requests( p_request_id IN NUMBER );
  PROCEDURE get_schedule_request_count(p_user_name IN  VARCHAR2 ,
                                    p_schedule_count OUT  NUMBER );
  FUNCTION as_of_date
    RETURN DATE
   ;
  FUNCTION get_output_columns (
       p_report_id   NUMBER
    )
       RETURN VARCHAR2;


  FUNCTION parse_output_data(p_output_data IN VARCHAR2)
    RETURN VARCHAR2;
  PROCEDURE get_segment_prompt ( p_app_col_name     OUT VARCHAR2
                    ,p_form_left_prompt OUT VARCHAR2
                    ,p_report_id        IN NUMBER
                ,p_process_id       IN NUMBER
                              ,p_col_count        OUT NUMBER
                              ,p_segment_data_type OUT VARCHAR2 ); -- Added KP 22-Dec-2005
/*  FUNCTION get_view_datatype (p_report_id  NUMBER ,
                                p_column_name VARCHAR2) RETURN VARCHAR2; */
  PROCEDURE submit_report ( p_report_id              IN NUMBER
                          ,p_request_id             IN NUMBER
                          ,p_user_id                IN NUMBER
                          ,p_process_id             IN NUMBER
                          ,p_report_name            IN VARCHAR2 DEFAULT NULL
                          ,p_result                 OUT CLOB
                          ,p_view_sql               IN OUT VARCHAR2
                          ,p_error_msg              OUT VARCHAR2
                          ,p_param1                 IN VARCHAR2 DEFAULT NULL
                          ,p_param2                 IN VARCHAR2 DEFAULT NULL
                          ,p_param3                 IN VARCHAR2 DEFAULT NULL
                          ,p_param4                 IN VARCHAR2 DEFAULT NULL
                          ,p_param5                 IN VARCHAR2 DEFAULT NULL
                          ,p_param6                 IN VARCHAR2 DEFAULT NULL
                          ,p_param7                 IN VARCHAR2 DEFAULT NULL
                          ,p_param8                 IN VARCHAR2 DEFAULT NULL
                          ,p_param9                 IN VARCHAR2 DEFAULT NULL
                          ,p_param10                IN VARCHAR2 DEFAULT NULL
                          ,p_param11                IN VARCHAR2 DEFAULT NULL
                          ,p_param12                IN VARCHAR2 DEFAULT NULL
                          ,p_param13                IN VARCHAR2 DEFAULT NULL
                          ,p_param14                IN VARCHAR2 DEFAULT NULL
                          ,p_param15                IN VARCHAR2 DEFAULT NULL
                          ,p_param16                IN VARCHAR2 DEFAULT NULL
                          ,p_param17                IN VARCHAR2 DEFAULT NULL
                          ,p_param18                IN VARCHAR2 DEFAULT NULL
                          ,p_param19                IN VARCHAR2 DEFAULT NULL
                          ,p_param20                IN VARCHAR2 DEFAULT NULL
                          ,p_param21                IN VARCHAR2 DEFAULT NULL
                          ,p_param22                IN VARCHAR2 DEFAULT NULL
                          ,p_param23                IN VARCHAR2 DEFAULT NULL
                          ,p_param24                IN VARCHAR2 DEFAULT NULL
                          ,p_param25                IN VARCHAR2 DEFAULT NULL
                          ,p_param26                IN VARCHAR2 DEFAULT NULL
                          ,p_param27                IN VARCHAR2 DEFAULT NULL
                          ,p_param28                IN VARCHAR2 DEFAULT NULL
                          ,p_param29                IN VARCHAR2 DEFAULT NULL
                          ,p_param30                IN VARCHAR2 DEFAULT NULL
                   ,p_col_process_id         IN NUMBER   DEFAULT NULL
                          );
  PROCEDURE get_filenames(p_process_id       IN VARCHAR2,
                            p_summary_flag     IN VARCHAR2,
                            p_filenames        OUT VARCHAR2,
                            p_lastfile         OUT VARCHAR2);

  PROCEDURE user_authorization ( p_user_id      IN VARCHAR2
                               ,p_process_id   IN NUMBER
                               ,p_auth_user  OUT VARCHAR2
                               ,p_eis_home OUT VARCHAR2
                               ,p_start_time OUT VARCHAR2
                               ,p_end_time OUT VARCHAR2
                               ,p_request_id OUT VARCHAR2
                               ,p_rows OUT VARCHAR2
                               ,p_report_name OUT VARCHAR2
                               ,p_enc_session_id OUT VARCHAR2
                               ,p_user_name OUT VARCHAR2
                               ,p_submitted_by OUT VARCHAR2
                               ,p_summary_flag OUT VARCHAR2
                   ,p_report_id  OUT   VARCHAR2
                   ,p_portal_login IN VARCHAR2 DEFAULT NULL);

  PROCEDURE submit_process( p_report_id              IN NUMBER
                            ,p_submission_type        IN VARCHAR2
                            ,p_result                 OUT CLOB
                            ,p_process_id             OUT NUMBER
                            ,p_request_id             OUT NUMBER
                            ,p_error_msg              OUT VARCHAR2
                            ,p_session_id             IN NUMBER
                            ,p_summary_flag           IN VARCHAR2
                            ,p_param1                 IN VARCHAR2 DEFAULT NULL
                            ,p_param2                 IN VARCHAR2 DEFAULT NULL
                            ,p_param3                 IN VARCHAR2 DEFAULT NULL
                            ,p_param4                 IN VARCHAR2 DEFAULT NULL
                            ,p_param5                 IN VARCHAR2 DEFAULT NULL
                            ,p_param6                 IN VARCHAR2 DEFAULT NULL
                            ,p_param7                 IN VARCHAR2 DEFAULT NULL
                            ,p_param8                 IN VARCHAR2 DEFAULT NULL
                            ,p_param9                 IN VARCHAR2 DEFAULT NULL
                            ,p_param10                IN VARCHAR2 DEFAULT NULL
                            ,p_param11                IN VARCHAR2 DEFAULT NULL
                            ,p_param12                IN VARCHAR2 DEFAULT NULL
                            ,p_param13                IN VARCHAR2 DEFAULT NULL
                            ,p_param14                IN VARCHAR2 DEFAULT NULL
                            ,p_param15                IN VARCHAR2 DEFAULT NULL
                            ,p_param16                IN VARCHAR2 DEFAULT NULL
                            ,p_param17                IN VARCHAR2 DEFAULT NULL
                            ,p_param18                IN VARCHAR2 DEFAULT NULL
                            ,p_param19                IN VARCHAR2 DEFAULT NULL
                            ,p_param20                IN VARCHAR2 DEFAULT NULL
                            ,p_param21                IN VARCHAR2 DEFAULT NULL
                            ,p_param22                IN VARCHAR2 DEFAULT NULL
                            ,p_param23                IN VARCHAR2 DEFAULT NULL
                            ,p_param24                IN VARCHAR2 DEFAULT NULL
                            ,p_param25                IN VARCHAR2 DEFAULT NULL
                            ,p_param26                IN VARCHAR2 DEFAULT NULL
                            ,p_param27                IN VARCHAR2 DEFAULT NULL
                            ,p_param28                IN VARCHAR2 DEFAULT NULL
                            ,p_param29                IN VARCHAR2 DEFAULT NULL
                            ,p_param30                IN VARCHAR2 DEFAULT NULL
                            ,p_schedule_param         IN VARCHAR2 DEFAULT NULL
                            ,p_server_url             IN VARCHAR2 DEFAULT NULL
                ,p_col_process_id         IN NUMBER   DEFAULT NULL
                            ,p_submission_source      IN VARCHAR2 DEFAULT NULL);

FUNCTION get_process_output_columns ( p_report_id   NUMBER,
                                      p_process_id  NUMBER
                                     ) RETURN VARCHAR2;

PROCEDURE copy_report_parameters ( p_report_id           IN  NUMBER,
                                   p_process_id          OUT NUMBER,
                                   p_user_name           IN  VARCHAR2 DEFAULT NULL,
                                   p_resp_name         IN  VARCHAR2 DEFAULT NULL,
                                   p_existing_process_id IN  NUMBER DEFAULT NULL
                  );


PROCEDURE send_mail_to_users( p_to            IN VARCHAR2,
                              p_from          IN VARCHAR2,
                              p_subject       IN VARCHAR2,
                              p_text          IN VARCHAR2 DEFAULT NULL,
                              p_html          IN VARCHAR2 DEFAULT NULL,
                              p_smtp_hostname IN VARCHAR2,
                              p_smtp_portnum  IN VARCHAR2,
                              p_mail_status   OUT VARCHAR2);

PROCEDURE insert_html_table ( p_process_id    IN VARCHAR2,
                              p_output_col    IN VARCHAR2,
                              p_report_id     IN VARCHAR2,
                              p_table_name    IN VARCHAR2,
                              p_security_col_list IN VARCHAR2);

FUNCTION get_process_column_data_type( p_process_id  IN NUMBER,
                       p_view_id     IN NUMBER,
                       p_column_name IN VARCHAR2
                      ) RETURN VARCHAR2;

FUNCTION get_summary_calc_display_cols( p_process_id IN VARCHAR2,
                                        p_pivot_id   IN NUMBER) RETURN VARCHAR2;

END EIS_RS_PROCESS_REPORTS; -- Package spec
/
