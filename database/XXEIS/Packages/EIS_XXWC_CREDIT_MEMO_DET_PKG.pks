CREATE OR REPLACE PACKAGE   XXEIS.EIS_XXWC_CREDIT_MEMO_DET_PKG AS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  --TMS#20151216-00168  by Siva  on 04-12-2016
--//
--// Object Usage 				:: This Object Referred by "White Cap Credit Memo Report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_CREDIT_MEMO_DET_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     	Siva  			04/12/2016   Initial Build --TMS#20151216-00168  by Siva   	on 04-12-2016
--// 1.1     	Siva 			15-Mar-2017     TMS#20170224-00065 
--//============================================================================
 type CURSOR_TYPE4 is ref cursor; 
PROCEDURE POPULATE_CREDIT_INVOICES(P_PROCESS_ID in number,
                          P_PERIOD_YEAR in number,
                          P_PERIOD_MONTH in varchar2,
                          P_ORIGIN_DATE_FROM in date,
                          P_ORIGIN_DATE_TO in date,
                          p_location in varchar2
                          );
--//============================================================================
--//
--// Object Name         :: POPULATE_CREDIT_INVOICES  
--//
--// Object Usage 		 :: This Object Referred by "White Cap Credit Memo Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_AR_CREDIT_MEMO_DTLS_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20151216-00168  by Siva   	on 04-12-2016
--//============================================================================
						  
/* --Commented code for version 1.1
procedure clear_temp_tables ( p_process_id in number);   
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20151216-00168  by Siva   	on 04-12-2016
--//============================================================================  
*/ --Commented code for version 1.1
                        
End;
/
