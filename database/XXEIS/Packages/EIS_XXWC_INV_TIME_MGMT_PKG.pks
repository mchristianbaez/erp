CREATE OR REPLACE PACKAGE XXEIS.EIS_XXWC_INV_TIME_MGMT_PKG 
AS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Time Sensitive Mgmt - WC"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_INV_TIME_MGMT_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Venu  	      05-may-2016       Initial Build TMS#20160315-00168
--// 1.1     	Siva 		  15-Mar-2017     	TMS#20170224-00065 
--//============================================================================
TYPE cursor_csr_main_data
IS
  REF
  CURSOR;
  TYPE view_record
IS
  TABLE OF XXEIS.eis_xxwc_inv_time_mgmt_tbl%rowtype;
  g_record_tbl_type view_record := view_record ();
  g_insert_counter NUMBER       := 0;
  PROCEDURE MAIN(
      p_process_id    NUMBER,
      p_organization  VARCHAR2,
      p_item          VARCHAR2,
      p_subinventory  VARCHAR2,
      p_region        VARCHAR2,
      p_supplier      VARCHAR2,
      p_location_list VARCHAR2,
      p_item_list     VARCHAR2,
      p_supplier_list VARCHAR2,
      p_at_risk       VARCHAR2);
--//============================================================================
--//
--// Object Name         :: MAIN  
--//
--// Object Usage 		 :: This Object Referred by "Time Sensitive Mgmt - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Venu  			05-May-2016 		Initial Build   
--//============================================================================ 

/* --Commented code for version 1.1
  PROCEDURE clear_table(
      p_process_id NUMBER);
--//============================================================================
--//
--// Object Name         :: clear_table  
--//
--// Object Usage 		 :: This Object Referred by "Time Sensitive Mgmt - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in Afteer report and delete data from Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Venu  				05-May-2016 	Initial Build   
--//============================================================================
*/ --Commented code for version 1.1
	  
FUNCTION get_person (p_person_id NUMBER) RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: get_person  
--//
--// Object Usage 		 :: This Object Referred by "Time Sensitive Mgmt - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Venu        	05-May-2016       Initial Build 
--//============================================================================
END eis_xxwc_inv_time_mgmt_pkg;
/