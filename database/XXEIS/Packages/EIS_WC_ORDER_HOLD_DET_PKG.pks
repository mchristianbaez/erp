CREATE OR REPLACE PACKAGE XXEIS.EIS_WC_ORDER_HOLD_DET_PKG
AS        
   /*************************************************************************
     Package : EIS_WC_ORDER_HOLD_DET_PKG

     PURPOSE:   This will populate the staging table for the Eis report
                Bill Trust
     Parameter:
	 REVISIONS:
     Ver          Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        10/16/2015  Maharajan Shunmugam    TMS#20140516-00041 Credit - EIS Create Credit Hold Release Report
   ************************************************************************/
   PROCEDURE populate_stage;
 /*************************************************************************
     Procedure : populate_stage

     PURPOSE:   This will populate the staging table for the Eis report
                Bill Trust
     Parameter:
	 REVISIONS:
     Ver          Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        10/16/2015  Maharajan Shunmugam    TMS#20140516-00041 Credit - EIS Create Credit Hold Release Report
   ************************************************************************/
END;
/