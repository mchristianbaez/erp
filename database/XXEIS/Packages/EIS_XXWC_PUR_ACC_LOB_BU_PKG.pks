CREATE OR REPLACE package    XXEIS.EIS_XXWC_PUR_ACC_LOB_BU_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  --TMS#20160411-00105  by Pramod on 04-12-2016
--//
--// Object Usage 				:: This Object Referred by "PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_PUR_ACC_LOB_BU_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160411-00105  by Pramod on 04-12-2016
--//============================================================================
procedure GET_ACC_LOB_BU_DTLS (	p_process_id 	in number,
								P_Cal_Month 	in varchar2,
								p_Lob			in varchar2,
								P_Mvid			in varchar2,
								P_Calendar_Year in number,
								P_Agreement_Year	in number
							);
--//============================================================================
--//
--// Object Name         :: GET_ACC_LOB_BU_DTLS  
--//
--// Object Usage 		 :: This Object Referred by "PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the XXEIS.EIS_PUR_ACCRAL_LOB_BU_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160411-00105  by Pramod on 04-12-2016
--//============================================================================							
 type CURSOR_TYPE4 is ref cursor;
 
type T_ARRAY is table of varchar2(32000)
   INDEX BY BINARY_INTEGER;
   
procedure clear_temp_tables ( p_process_id in number); 
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160411-00105  by Pramod on 04-12-2016
--//============================================================================  
END;
/
