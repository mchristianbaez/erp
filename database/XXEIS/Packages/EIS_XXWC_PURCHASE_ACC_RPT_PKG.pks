CREATE OR REPLACE PACKAGE  XXEIS.EIS_XXWC_PURCHASE_ACC_RPT_PKG AS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue 
--//
--// Object Usage 				:: This Object Referred by "PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_PURCHASE_ACC_RPT_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     	Pramod  	04/12/2016   	Initial Build --TMS#20160412-00031	--Performance Tuning
--//============================================================================
procedure GET_ACC_PUR_DTLS (p_process_id      in number,
                            p_Lob_Branch 		  in varchar2,
                            p_Agreement_Year 	in number,
                            P_FISCAL_PERIOD 	in varchar2,
                            P_LOB 				    in varchar2,
                            P_VENDOR 			    IN VARCHAR2
							);
--//============================================================================
--//
--// Object Name         :: GET_ACC_PUR_DTLS  
--//
--// Object Usage 		 :: This Object Referred by "PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_PUR_ACC_OLD_TABLE_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160412-00031	--Performance Tuning
--//============================================================================							
 type CURSOR_TYPE4 is ref cursor;
 
type T_ARRAY is table of varchar2(32000)
   INDEX BY BINARY_INTEGER;
   
procedure clear_temp_tables ( p_process_id in number);  
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160412-00031	--Performance Tuning
--//============================================================================    

END;
/
