CREATE OR REPLACE 
PACKAGE XXEIS.eis_rs_fin_utility 
AUTHID CURRENT_USER 
IS

--
-- To modify this template, edit file PKGSPEC.TXT in TEMPLATE
-- directory of SQL Navigator
--
-- Purpose: Briefly explain the functionality of the package

-- $Header: eis_rs_fin_utility.pkh 6.02 2007/12/18 02:57:29 $

-- MODIFICATION HISTORY
-- Person     Version   Header-No   Bug-No  Issue-No  Date         Comments
-- ---------  ------    ----------  ------  --------  -----        ---------
-- Sunil      6.0.2     6.0.2                         18-Dec-2007  Modified for Version Controlling.
--                                                                 The new modification history is to keep track of
--                                                                 all the fixes that got fixed by mentioning
--                                                                 respective bug No's of Bugzilla and Issue No's of
--                                                                 Salesforce and the version number in which the
--                                                                 fix as went.
-- AG                   6.0.3                           30-Jan-2009 Implement hashing on eis_rs_fin_utility.decode_vset
-- AG                               1899                26AUG09     Modified the function create_gl_segment_lovs
-- AG                               1900                27AUG09     Removed the function create_gl_lovs
-- Prasad R                                             25-Feb-2010 Converted from char value to date for g_START_DATE





   FUNCTION get_segment_description (
      p_value_set_id          IN   NUMBER,
      p_segment               IN   VARCHAR2,
      p_code_combination_id   IN   NUMBER
   )
      RETURN VARCHAR2;



/*========================================================================================================*
**
**		Function 	  : decode_vset
**		Description   : Gets the Description for the Segment Value given the value set
**		Scope		  : Public
**		Date Created  : 17-Feb-08
**		Created By	  : Ashwin
**
**========================================================================================================*/

  function decode_vset
                ( p_vset_value  in   varchar2 ,
		          p_vset        in   varchar2,
			  p_dset_value  in   varchar2 default null
                  )
  return varchar2;

/*========================================================================================================*
**
**		Function 	  : CREATE_GL_SEGMENT_LOVS
**		Description   : Creates the LOV's for the Value Sets associated with segments for SOB's Chart Of Accounts
**		Scope		  : Public
**		Date Created  : 17-Feb-08
**		Created By	  : Ashwin
**
**========================================================================================================*/

PROCEDURE CREATE_GL_SEGMENT_LOVS
(
p_sob_id    number default null,
p_lov_vs_name   varchar2 default null
,p_gl_account_lov_creation varchar2 default 'N'
,p_id_flex_num number default null
);
/*===========================================================================================================
**
** Function      :GET_VENDOR_OR_SITE
   Description   : It returns the Global Variable value to view
   Date Created  :20-Aug-2008
   Created BY    :Sudha
**
**===========================================================================================================*/
g_VENDOR_OR_SITE VARCHAR2(35);
FUNCTION get_VENDOR_OR_SITE RETURN VARCHAR2;
g_START_DATE   DATE:=TO_DATE('01-Jan-2005','DD-Mon-YYYY');
FUNCTION get_date  RETURN DATE;
g_Act_InAct  VARCHAR2(10);
FUNCTION get_Act_InAct RETURN VARCHAR2;
g_batch_name   VARCHAR2(100);
FUNCTION get_batch_id RETURN NUMBER;
g_txn_type VARCHAR2(50);
FUNCTION get_txn_type_id RETURN NUMBER;

g_batch_src_name  VARCHAR2(50);
FUNCTION get_batch_src_id RETURN NUMBER;

FUNCTION get_qualifier_segnum(appl_id IN  NUMBER,
			      key_flex_code    IN  VARCHAR2,
			      structure_number IN  NUMBER,
			      flex_qual_name   IN  VARCHAR2
			      )
  RETURN number;
G_ENCUMBRANCE_TYPE VARCHAR2(200);
FUNCTION GET_ENCUMBRANCE_TYPE RETURN VARCHAR2;

FUNCTION get_tax_rate RETURN VARCHAR2;

FUNCTION get_account_segment_number return NUMBER;


--6.0.3
type t_value_set_tbl_typ is table of varchar2(2000) index by varchar2(200);
g_value_set_tbl t_value_set_tbl_typ;

END;
/

-- End of DDL Script for Package XXEIS.EIS_RS_FIN_UTILITY

