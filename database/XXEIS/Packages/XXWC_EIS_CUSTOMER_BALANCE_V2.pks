/* Formatted on 8/15/2013 9:39:48 PM (QP5 v5.206) */
-- Start of DDL Script for Package XXEIS.XXWC_EIS_CUSTOMER_BALANCE_V2
-- Generated 8/15/2013 9:39:46 PM from XXEIS@EBIZFQA

CREATE OR REPLACE PACKAGE xxeis.xxwc_eis_customer_balance_v2
IS
    --
    -- this package will populate the base table for MATERIALIZED VIEW xxeis.xxwc_ar_customer_balance_mv

    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  ------------------------------------------
    --Rasikha      7/31/2013
    -- Enter package declarations as shown below

    PROCEDURE main (p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER, p_tuning_factor NUMBER);

    PROCEDURE table_create_from_old (p_schema VARCHAR2, p_old_table_name VARCHAR2, p_new_table_name VARCHAR2);

    PROCEDURE alter_table_temp (p_owner VARCHAR2, p_table_name VARCHAR2);

    PROCEDURE drop_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2);

    FUNCTION create_copy_table (p_owner VARCHAR2, p_table_name VARCHAR2, p_new_table_name VARCHAR2)
        RETURN NUMBER;

    PROCEDURE write_log (p_text IN VARCHAR2);

    FUNCTION xxwc_add_tuning_parameter (p_table_name VARCHAR2, p_tuning_factor NUMBER)
        RETURN NUMBER;

    PROCEDURE elapsed_time (p_called_from VARCHAR2, p_start IN OUT NUMBER);
END;                                                                                                     -- Package spec
/

-- Grants for Package
GRANT EXECUTE ON xxeis.xxwc_eis_customer_balance_v2 TO interface_prism
/
GRANT EXECUTE ON xxeis.xxwc_eis_customer_balance_v2 TO interface_xxcus
/
GRANT EXECUTE ON xxeis.xxwc_eis_customer_balance_v2 TO apps
/
GRANT DEBUG ON xxeis.xxwc_eis_customer_balance_v2 TO interface_prism
/
GRANT DEBUG ON xxeis.xxwc_eis_customer_balance_v2 TO interface_xxcus
/
GRANT DEBUG ON xxeis.xxwc_eis_customer_balance_v2 TO apps
/

-- End of DDL Script for Package XXEIS.XXWC_EIS_CUSTOMER_BALANCE_V2
