CREATE OR REPLACE package XXEIS.XXWC_VENDOR_QUOTE_TST_PKG  as
-------------------------------------------------------------------------
/*************************************************************************
  $Header XXWC_VENDOR_QUOTE_TST_PKG.pkb $
  Module Name: XXWC_VENDOR_QUOTE_TST_PKG
  PURPOSE: EIS Vendor Qoute Batch Summary Report
  REVISIONS:
  Ver        Date         Author                Description
  ---------- -----------  ------------------    ----------------
  1.0        NA     		EIS        			Initial Version
  1.1        30-Oct-2017	Siva			    TMS#20170920-00108
*************************************************************************/
----------------------------------------------------------------------------  
    G_INV_START_DATE date;
    G_INV_END_DATE date;

--Start for Ver 1.1	
 type T_VARCHARTYPE_VLDN_TBL is table of varchar2(100) index by varchar2(100);
  G_PO_TRANS_COST_VLDN_TBL T_VARCHARTYPE_VLDN_TBL;
  G_PO_INT_TRAN_COST_VLDN_TBL T_VARCHARTYPE_VLDN_TBL;
  
  G_PO_TRANS_COST  number;
  G_PO_INT_TRANS_COST number;

  FUNCTION get_po_trans_cost(
    p_inventory_item_id IN NUMBER,
    p_organization_id   IN NUMBER,
    p_vendor_number     IN VARCHAR2)
  RETURN NUMBER;
   
  FUNCTION get_po_int_trans_cost(
    P_INVENTORY_ITEM_ID in number,
    P_ORGANIZATION_ID   in number,
    P_VENDOR_NUMBER     in varchar2)
  return number;
--End for Ver 1.1	
	
    procedure GET_HEADER_ID (P_PROCESS_ID IN NUMBER,P_INV_START_DATE in date,P_INV_END_DATE in date);
    end;
/
