/* Formatted on 10-Jul-2013 01:38:34 (QP5 v5.206) */
-- Start of DDL Script for Package XXEIS.EIS_PO_XXWC_ISR_UTIL_PKG
-- Generated 10-Jul-2013 01:38:33 from XXEIS@EBIZFQA

CREATE OR REPLACE PACKAGE xxeis.eis_po_xxwc_isr_util_pkg
    AUTHID CURRENT_USER
IS
    g_isr_rpt_dc_mod_sub   VARCHAR2 (50);

    FUNCTION get_demand_qty (p_org_id            NUMBER
                            ,p_subinv            VARCHAR2
                            ,p_level             NUMBER
                            ,p_item_id           NUMBER
                            ,p_d_cutoff          DATE
                            ,p_include_nonnet    NUMBER
                            ,p_net_rsv           NUMBER
                            ,p_net_unrsv         NUMBER
                            ,p_net_wip           NUMBER
                            /* nsinghi MIN-MAX INVCONV start */
                            ,p_process_org       VARCHAR2/* nsinghi MIN-MAX INVCONV end */
                             )
        RETURN NUMBER;

    FUNCTION get_vendor_name (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_vendor_number (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_inv_cat_class (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_inv_cat_seg1 (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_inv_vel_cat_class (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_isr_item_cost (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER;

    FUNCTION get_isr_bpa_doc (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER;

    FUNCTION get_isr_open_po_qty (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER;

    FUNCTION get_isr_avail_qty (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER;

    FUNCTION get_isr_ss_cnt (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER;

    FUNCTION get_isr_sourcing_rule (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_isr_ss (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER;

    FUNCTION get_int_req_so_qty (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER;

    FUNCTION get_isr_rpt_dc_mod_sub
        RETURN VARCHAR2;

    FUNCTION get_onhand_inv (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN NUMBER;

    FUNCTION get_primary_bin_loc (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_isr_open_req_qty (p_inventory_item_id NUMBER, p_organization_id NUMBER, p_req_type VARCHAR2)
        RETURN NUMBER;

    FUNCTION get_staged_qty (p_org_id            NUMBER
                            ,p_subinv            VARCHAR2
                            ,p_item_id           NUMBER
                            ,p_order_line_id     NUMBER
                            ,p_include_nonnet    NUMBER)
        RETURN NUMBER;

    FUNCTION get_pick_released_qty (p_org_id           NUMBER
                                   ,p_subinv           VARCHAR2
                                   ,p_item_id          NUMBER
                                   ,p_order_line_id    NUMBER)
        RETURN NUMBER;

    FUNCTION get_loaded_qty (p_org_id       NUMBER
                            ,p_subinv       VARCHAR2
                            ,p_level        NUMBER
                            ,p_item_id      NUMBER
                            ,p_net_rsv      NUMBER
                            ,p_net_unrsv    NUMBER)
        RETURN NUMBER;

    FUNCTION get_shipped_qty (p_organization_id IN NUMBER, p_inventory_item_id IN NUMBER, p_order_line_id IN NUMBER)
        RETURN NUMBER;

    FUNCTION get_supply_qty (p_org_id                NUMBER
                            ,p_subinv                VARCHAR2
                            ,p_item_id               NUMBER
                            ,p_postproc_lead_time    NUMBER
                            ,p_cal_code              VARCHAR2
                            ,p_except_id             NUMBER
                            ,p_level                 NUMBER
                            ,p_s_cutoff              DATE
                            ,p_include_po            NUMBER
                            ,p_include_mo            NUMBER
                            ,p_vmi_enabled           VARCHAR2
                            ,p_include_nonnet        NUMBER
                            ,p_include_wip           NUMBER
                            ,p_include_if            NUMBER
                            /* nsinghi MIN-MAX INVCONV start */
                            ,p_process_org           VARCHAR2/* nsinghi MIN-MAX INVCONV end */
                             )
        RETURN NUMBER;

    FUNCTION get_item_uom_code (p_uom_name VARCHAR2)
        RETURN VARCHAR2;

    FUNCTION get_planning_quantity (p_include_nonnet    NUMBER
                                   ,p_level             NUMBER
                                   ,p_org_id            NUMBER
                                   ,p_subinv            VARCHAR2
                                   ,p_item_id           NUMBER)
        RETURN NUMBER;

    FUNCTION get_org_rec_qty (p_inventory_item_id IN NUMBER, p_organization_id IN NUMBER)
        RETURN NUMBER;

    FUNCTION has_drop_ships (p_po_header_id NUMBER, p_req_line_id NUMBER, p_po_line_location_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION is_not_vmi (p_req_type VARCHAR2, p_req_line_id NUMBER, p_po_line_location_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION is_nettable_subinv (p_organization_id NUMBER, p_subinv_name VARCHAR2)
        RETURN VARCHAR2;

    PROCEDURE get_isr_open_req_qty (p_organization_id     IN     NUMBER
                                   ,p_inventory_item_id   IN     NUMBER
                                   ,p_both_qty               OUT NUMBER
                                   ,p_vendor_qty             OUT NUMBER
                                   ,p_inventory_qty          OUT NUMBER
                                   ,p_direct_qty             OUT NUMBER);
END eis_po_xxwc_isr_util_pkg;
/

-- Grants for Package
GRANT EXECUTE ON xxeis.eis_po_xxwc_isr_util_pkg TO apps
/
GRANT DEBUG ON xxeis.eis_po_xxwc_isr_util_pkg TO apps
/

-- End of DDL Script for Package XXEIS.EIS_PO_XXWC_ISR_UTIL_PKG
