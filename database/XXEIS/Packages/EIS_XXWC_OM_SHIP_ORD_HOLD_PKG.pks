create or replace 
PACKAGE   XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG
--//============================================================================
--//  
--// Object Name         		:: xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Temp Table
--//
--// Version Control
--//============================================================================
--// Vers    	Author           Date          				Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================
is

type t_varchar2_vldn_tbl IS TABLE OF VARCHAR2(100) INDEX BY VARCHAR2(100);
  
g_lookup_meaning_vldn_tbl t_varchar2_vldn_tbl;
g_lookup_meaning       VARCHAR2(240);

g_employee_name_vldn_tbl t_varchar2_vldn_tbl;
g_employee_name       VARCHAR2(240);

g_hdr_hold_name_vldn_tbl t_varchar2_vldn_tbl;
g_hdr_hold_name       VARCHAR2(240);

g_header_hold_name_vldn_tbl t_varchar2_vldn_tbl;
g_header_hold_name      VARCHAR2(240);

g_line_hold_name_vldn_tbl t_varchar2_vldn_tbl;
g_line_hold_name       VARCHAR2(240);

g_hdr_hold_date_vldn_tbl t_varchar2_vldn_tbl;
g_header_hold_date      date;

g_line_holdby_ntid_vldn_tbl t_varchar2_vldn_tbl;
g_line_hold_by_ntid      VARCHAR2(240);

g_hdr_holdby_ntid_vldn_tbl t_varchar2_vldn_tbl;
g_hdr_hold_by_ntid      VARCHAR2(240);

g_hdr_holdby_name_vldn_tbl t_varchar2_vldn_tbl;
g_hdr_hold_by_name      VARCHAR2(240);

g_line_holdby_name_vldn_tbl t_varchar2_vldn_tbl;
g_line_hold_by_name      VARCHAR2(240);
  
FUNCTION get_lookup_meaning(
      p_lookup_code IN VARCHAR2,
      p_lookup_type IN VARCHAR2)
    return varchar2;

FUNCTION get_employee_name(p_created_by in number)
    RETURN VARCHAR2;

FUNCTION get_header_hold_name(P_HEADER_ID IN NUMBER)
    RETURN VARCHAR2;
    
FUNCTION get_hdr_header_hold_name(P_HEADER_ID IN NUMBER)
    RETURN VARCHAR2;
    
FUNCTION get_line_hold_name(P_HEADER_ID IN NUMBER,P_LINE_ID IN NUMBER)
    RETURN VARCHAR2;

FUNCTION get_header_hold_date(P_HEADER_ID IN NUMBER)
    RETURN DATE;

FUNCTION get_line_hold_by_ntid(p_hold_source_id IN NUMBER,p_header_id IN NUMBER,p_line_id IN NUMBER)
    RETURN VARCHAR2;

FUNCTION get_hdr_hold_by_ntid(p_hold_source_id IN NUMBER,p_header_id IN NUMBER)
    RETURN VARCHAR2;

FUNCTION get_hdr_hold_by_name(p_hold_source_id IN NUMBER,p_header_id IN NUMBER)
    RETURN VARCHAR2;

FUNCTION get_line_hold_by_name(p_hold_source_id in number,p_header_id in number,p_line_id in number)
    RETURN VARCHAR2;
	
    
procedure order_hold_proc (p_warehouse       in varchar2,
							  p_order_date_from in date,
							  p_order_date_to   in date,
							  P_ORDER_TYPE      IN VARCHAR2,
							  P_HOLD_TYPE       IN VARCHAR2							  
							  );
--//============================================================================
--//
--// Object Name         :: ORDER_HOLD_PROC
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure gets the record based on parameter and insert into temp table
--//
--// Version Control
--//============================================================================
--// Vers    Author             Date            		Description
--//----------------------------------------------------------------------------
--// 1.0       Siva  		09-Jan-2018          	 TMS#20171228-00005 
--//============================================================================	  

END EIS_XXWC_OM_SHIP_ORD_HOLD_PKG;
/
