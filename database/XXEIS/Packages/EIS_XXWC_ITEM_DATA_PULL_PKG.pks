create or replace 
package       XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG
IS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package programs will call in EIS_XXWC_NEW_ITEM_DATA_RPT_V view to populate data
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      18-may-2016       Initial Build TMS#20160503-00087
--//============================================================================
type T_VARCHAR2_VLDN_TBL is  table of varchar2(200) index by varchar2(200);
  g_vendor_part_VLDN_TBL    T_VARCHAR2_VLDN_TBL;
  G_VENDOR_NAME_VLDN_TBL    T_VARCHAR2_VLDN_TBL;
  G_VENDOR_TIER_VLDN_TBL    T_VARCHAR2_VLDN_TBL;
  G_TIME_SENSITIVE_VLDN_TBL T_VARCHAR2_VLDN_TBL;
  G_LOC_CODE_VLDN_TBL   	T_VARCHAR2_VLDN_TBL;
  G_APPROVAL_DATE_VLDN_TBL  T_VARCHAR2_VLDN_TBL;
  G_CATLOG_GROUP_VLDN_TBL   T_VARCHAR2_VLDN_TBL;
  
  g_vendor_part_num      VARCHAR2(200);
  G_VENDOR_NAME			 VARCHAR2(200);
  G_VENDOR_TIER			 VARCHAR2(200);
  g_time_sensitive_cat   VARCHAR2(200);
  g_location_code   	 VARCHAR2(200);
  G_APPROVAL_DATE        date;
  G_catalog_group        VARCHAR2(200);
FUNCTION get_vendor_part_num(p_inventory_item_id number) RETURN varchar2;
--//============================================================================
--//
--// Object Name         :: get_vendor_part_num  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
FUNCTION GET_VENDOR_DETAILS (P_SEGMENT1 IN VARCHAR2,P_REQUEST_TYPE IN VARCHAR2) RETURN varchar2;
--//============================================================================
--//
--// Object Name         :: GET_VENDOR_DETAILS  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
FUNCTION get_time_sensitive_cat(p_inventory_item_id number) RETURN varchar2;
--//============================================================================
--//
--// Object Name         :: get_time_sensitive_cat  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
FUNCTION GET_LOCATION_NAME(p_employee_id number) RETURN varchar2;
--//============================================================================
--//
--// Object Name         :: GET_LOCATION_NAME  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
FUNCTION GET_APPROVAL_DATE(p_inventory_item_id number,p_organization_id number) RETURN date;
--//============================================================================
--//
--// Object Name         :: GET_APPROVAL_DATE  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
FUNCTION GET_ITEM_CATALOG_GROUP(p_item_catalog_group_id number) RETURN varchar2;
--//============================================================================
--//
--// Object Name         :: GET_ITEM_CATALOG_GROUP  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================ 
end EIS_XXWC_ITEM_DATA_PULL_PKG;
/
