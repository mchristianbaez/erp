CREATE OR REPLACE 
PACKAGE  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN  IS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Specials Report"
--//
--// Object Name         		:: XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	      04/12/2016    Initial Build --TMS#20151216-00168  --performance tuning
--// 1.1        Siva              06/14/2016     TMS#20160614-00010 --performance tuning
--// 1.2        Siva             08/02/2016     TMS#20160708-00081	
--// 1.3     	Siva 			 15-Mar-2017     TMS#20170224-00065 		
--//============================================================================
TYPE T_VARCHAR2_VLDN_TBL IS TABLE OF VARCHAR2(100) INDEX BY VARCHAR2(100);
TYPE T_number_tab IS TABLE OF number index by binary_integer;
TYPE T_date_tab IS TABLE OF date index by binary_integer;
G_CAT_CLASS_VLDN_TBL           T_VARCHAR2_VLDN_TBL;
G_LIST_PRICE_VLDN_TBL          T_VARCHAR2_VLDN_TBL;
G_VENDOR_NUMBER_VLDN_TBL       T_VARCHAR2_VLDN_TBL;
G_VENDOR_NAME_VLDN_TBL         T_VARCHAR2_VLDN_TBL;
G_OLDEST_BORN_DATE_VLDN_TBL    T_VARCHAR2_VLDN_TBL;
G_ONHAND_DATE_VLDN_TBL         T_VARCHAR2_VLDN_TBL;
G_OPEN_SALES_ORDERS_VLDN_TBL   T_VARCHAR2_VLDN_TBL;
G_ITEM_PURCHASED_VLDN_TBL      T_VARCHAR2_VLDN_TBL;
G_ITEM_COST_VLDN_TBL           T_VARCHAR2_VLDN_TBL;
G_CONCATENATED_SEGMENTS varchar2(200);
G_LIST_PRICE            number;
G_VENDOR_NAME           varchar2(200);
G_VENDOR_NUMBER         VARCHAR2(200);
G_ONHAND_DATE           date;
G_OLDEST_BORN_DATE      date;
G_OPEN_SALES_ORDERS     number;
G_ITEM_COST 		    number;
G_ITEM_PURCHASED        number;

FUNCTION GET_INV_CAT_CLASS (P_INVENTORY_ITEM_ID IN NUMBER,P_ORGANIZATION_ID IN NUMBER) RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: GET_INV_CAT_CLASS  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168  --performance tuning
--//============================================================================	
FUNCTION GET_LIST_PRICE (P_INVENTORY_ITEM_ID IN NUMBER,P_ORGANIZATION_ID IN NUMBER) RETURN number;
--//============================================================================
--//
--// Object Name         :: GET_LIST_PRICE  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
--FUNCTION GET_PO_VENDOR_NAME (P_INVENTORY_ITEM_ID IN NUMBER,P_ORGANIZATION_ID IN NUMBER) RETURN VARCHAR2; --Commented for version 1.1
--//============================================================================
--//
--// Object Name         :: GET_PO_VENDOR_NAME  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
--FUNCTION GET_PO_VENDOR_NUMBER (P_INVENTORY_ITEM_ID IN NUMBER,P_ORGANIZATION_ID IN NUMBER) RETURN VARCHAR2; --Commented for version 1.1
--//============================================================================
--//
--// Object Name         :: GET_PO_VENDOR_NUMBER  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
FUNCTION GET_ONHAND_DATE (P_INVENTORY_ITEM_ID IN NUMBER,P_ORGANIZATION_ID IN NUMBER) RETURN date;
--//============================================================================
--//
--// Object Name         :: GET_ONHAND_DATE  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
FUNCTION GET_OLDEST_BORN_DATE (P_INVENTORY_ITEM_ID IN NUMBER,P_ORGANIZATION_ID IN NUMBER) RETURN date;
--//============================================================================
--//
--// Object Name         :: GET_OLDEST_BORN_DATE  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
FUNCTION GET_OPEN_SALES_ORDERS (P_INVENTORY_ITEM_ID IN NUMBER,P_ORGANIZATION_ID IN NUMBER) RETURN number;
--//============================================================================
--//
--// Object Name         :: GET_OPEN_SALES_ORDERS  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
FUNCTION GET_ITEM_PURCHASED (P_INVENTORY_ITEM_ID IN NUMBER,P_ORGANIZATION_ID IN NUMBER) RETURN number;
--//============================================================================
--//
--// Object Name         :: GET_ITEM_PURCHASED  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
FUNCTION GET_ITEM_COST(P_NUM in number,P_INVENTORY_ITEM_ID in number,P_ORGANIZATION_ID in number) return number;
--//============================================================================
--//
--// Object Name         :: GET_ITEM_COST  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
FUNCTION GET_PO_VENDOR_DTLS (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number,P_TYPE VARCHAR2) RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: GET_PO_VENDOR_DTLS  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.1       Siva       	06/14/2016     Initial Build --20160614-00010 --performance tuning		
--//============================================================================
   
g_process_id NUMBER; --added for version 1.2
FUNCTION get_process_id RETURN NUMBER;
--//============================================================================
--//
--// Object Name         :: get_process_id  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.2        Siva             08/02/2016     TMS#20160708-00081	
--//============================================================================
   
PROCEDURE SPECIAL_REP_PRC (P_PROCESS_ID       IN NUMBER
                          ,P_ORGANIZATION_CODE      IN VARCHAR2);
--//============================================================================
--//
--// Object Name         :: SPECIAL_REP_PRC  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure dump the data in to custom table XXWC_INV_SPCL_ONHAND_TBL
--//                       and populates in view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.2        Siva             08/02/2016     TMS#20160708-00081	
--//============================================================================						  

/* --Commented code for version 1.3                          
PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER);
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.2      02-Aug-2016        Siva   		TMS#20160708-00081
--//============================================================================ 
*/ --Commented code for version 1.3
   
End;
/
