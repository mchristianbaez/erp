CREATE OR REPLACE PACKAGE   XXEIS.EIS_XXWC_CREDIT_ALL_DET_PKG AS
--//============================================================================
--//
--// Object Usage 				:: This Object Referred by "White Cap ALL Credit Memo Report - WC"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_CREDIT_ALL_DET_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     	Siva  			01-May-2017    Initial Build -- TMS#20161121-00172 
--//============================================================================
 type CURSOR_TYPE4 is ref cursor;
PROCEDURE POPULATE_CREDIT_INVOICES(P_PROCESS_ID in number,
                          P_PERIOD_YEAR in number,
                          P_PERIOD_MONTH in varchar2,
                          P_TRX_DATE_FROM in date,
                          P_TRX_DATE_TO in date,
                          p_location in varchar2
                          );
--//============================================================================
--//
--// Object Name         :: POPULATE_CREDIT_INVOICES
--//
--// Object Usage 		 :: This Object Referred by "White Cap ALL Credit Memo Report - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will insert the values into global temporary Table.
--// 						
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     	Siva  			01-May-2017    Initial Build -- TMS#20161121-00172 
--//============================================================================

end;
/
