create or replace
PACKAGE       eis_po_xxwc_isr_pkg AUTHID CURRENT_USER
Is
  
   Type Cursor_Type Is Ref Cursor;
   Type View_Tab Is Table Of Xxeis.Eis_Xxwc_Po_Isr_Rpt_V%Rowtype Index By Binary_Integer;
   G_View_Tab      View_Tab ;
   G_View_Record  Xxeis.Eis_Xxwc_Po_Isr_V%Rowtype ;
   G_View_Record1 Xxeis.EIS_XXWC_PO_ISR_LIVE_V%Rowtype ;
  
   Type Cursor_Type2 Is Ref Cursor;
   TYPE View_tab2 IS TABLE OF xxeis.eis_xxwc_po_isr_tab%rowtype Index By Binary_Integer;
   TYPE View_tab3 IS TABLE OF xxeis.eis_xxwc_po_isr_tab_v%rowtype Index By Binary_Integer;
   G_View_Tab2                     View_tab2;
   G_VIEW_RECORD2                  View_Tab3;
  
   Type Cursor_Type4 Is Ref Cursor;
   TYPE View_tab4 IS TABLE OF xxeis.EIS_XXWC_PO_ISR_PSALES_TAB%Rowtype Index By Binary_Integer;
   G_View_Tab4                     View_tab4;
   G_VIEW_RECORD4                  View_tab4;
  
   Type Cursor_Type5 Is Ref Cursor;
   TYPE View_tab5 IS TABLE OF xxeis.eis_xxwc_po_isr_posales_tab%Rowtype Index By Binary_Integer;
   G_View_Tab5                     View_tab5;
   G_VIEW_RECORD5                  View_tab5;
   
    TYPE View_tab6 IS TABLE OF xxeis.eis_xxwc_po_isr_hits_tab%Rowtype Index By Binary_Integer;
   G_View_Tab6                     View_tab6;
   G_VIEW_RECORD6                  View_tab6;

   TYPE View_tab7 IS TABLE OF xxeis.eis_xxwc_po_isr_ohits_tab%Rowtype Index By Binary_Integer;
   G_View_Tab7                     View_tab7;
   G_VIEW_RECORD7                  View_tab7;

   TYPE View_tab8 IS TABLE OF xxeis.eis_xxwc_po_isr_sales_tab%Rowtype Index By Binary_Integer;
   G_View_Tab8                     View_tab8;
   G_VIEW_RECORD8                  View_tab8;

   TYPE View_tab9 IS TABLE OF xxeis.eis_xxwc_po_isr_osales_tab%Rowtype Index By Binary_Integer;
   G_View_Tab9                     View_tab9;
   G_VIEW_RECORD9                  View_tab9;
   G_View_Record10 Xxeis.eis_xxwc_po_isr_live_v%Rowtype ;
  
   G_Log_Mode    Varchar2(10);

   procedure main;
   
   PROCEDURE isr_rpt_proc
                           (p_process_id           IN NUMBER,
                            p_region               IN VARCHAR2,
                            p_district             IN VARCHAR2,
                            p_location             IN VARCHAR2,
                            p_dc_mode              IN VARCHAR2,
                            p_tool_repair          IN VARCHAR2,
                            p_time_sensitive       IN VARCHAR2,                            
                            p_stk_items_with_hit4  IN VARCHAR2,
                            p_Report_Condition     in varchar2,
                            p_report_criteria      IN VARCHAR2,
                            p_report_criteria_val  in varchar2,
                            p_start_bin_loc        in varchar2,
                            p_end_bin_loc          in varchar2,
                            p_vendor               in varchar2,
                            P_ITEM                 IN VARCHAR2,
                            P_CAT_CLASS            IN VARCHAR2,
                            P_ORG_LIST             IN VARCHAR2,
                            P_ITEM_LIST            in varchar2,
                            P_SUPPLIER_LIST        in varchar2,
                            P_CAT_CLASS_LIST       in varchar2,
                            P_SOURCE_LIST          in varchar2,
                            P_INTANGIBLES          in varchar2 
                            );
                            
 PROCEDURE ISR_LIVE_RPT_PROC
                     (p_process_id           IN NUMBER,
                      p_region               IN VARCHAR2,
                      p_district             IN VARCHAR2,
                      p_location             IN VARCHAR2,
                      p_dc_mode              IN VARCHAR2,
                      p_tool_repair          IN VARCHAR2,
                      p_time_sensitive       IN VARCHAR2,   
                      p_stk_items_with_hit4  IN VARCHAR2,
                      p_Report_Condition     in varchar2,
                      p_report_criteria      in varchar2,
                      p_report_criteria_val  in varchar2,
                      p_start_bin_loc        in varchar2,
                      p_end_bin_loc          in varchar2,
                      p_vendor               in varchar2,
                      p_item                 in varchar2,
                      p_cat_class            in varchar2 ,
                      P_ORG_LIST             IN VARCHAR2,
                      P_ITEM_LIST            IN VARCHAR2,
                      p_supplier_list        in varchar2,
                      P_CAT_CLASS_LIST       in varchar2,
                      P_SOURCE_LIST          in varchar2
                      );                                   
                            
  procedure get_vendor_info (p_inventory_item_id in number, p_organization_id in number, p_vendor_name out varchar2, p_vendor_number out varchar2, p_vendor_site out varchar2);
END eis_po_xxwc_isr_pkg;
/