/* Formatted on 2/12/2014 11:08:48 PM (QP5 v5.206) */
-- Start of DDL Script for Package XXEIS.EIS_PO_XXWC_ISR_PKG_V2
-- Generated 2/12/2014 11:08:46 PM from XXEIS@EBIZFQA

CREATE OR REPLACE PACKAGE xxeis.eis_po_xxwc_isr_pkg_v2
   /*************************************************************************
     $Header EIS_PO_XXWC_ISR_PKG_V2.pkb $
     Module Name: EIS_PO_XXWC_ISR_PKG_V2

     PURPOSE: ISR Datamart Package

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.1        07/31/2014  Manjula Chellappan   Modified for TMS # 20140728-00130 
						 Added procedure populate_bpa_cost

   **************************************************************************/
IS
    TYPE cursor_type IS REF CURSOR;

    TYPE view_tab IS TABLE OF xxeis.eis_xxwc_po_isr_rpt_v%ROWTYPE
        INDEX BY BINARY_INTEGER;

    g_view_tab      view_tab;
    g_view_record   xxeis.eis_xxwc_po_isr_v%ROWTYPE;
    gr_start        NUMBER := DBMS_UTILITY.get_time;
    g_start         NUMBER := 0;

    PROCEDURE new_main2;

    --*********************************

    PROCEDURE main;

    PROCEDURE build_index;

    PROCEDURE populate_master;

    PROCEDURE isr_rpt_proc (p_process_id            IN NUMBER
                           ,p_region                IN VARCHAR2
                           ,p_district              IN VARCHAR2
                           ,p_location              IN VARCHAR2
                           ,p_dc_mode               IN VARCHAR2
                           ,p_tool_repair           IN VARCHAR2
                           ,p_time_sensitive        IN VARCHAR2
                           ,p_stk_items_with_hit4   IN VARCHAR2
                           ,p_report_condition      IN VARCHAR2
                           ,p_report_criteria       IN VARCHAR2
                           ,p_report_criteria_val   IN VARCHAR2
                           ,p_start_bin_loc         IN VARCHAR2
                           ,p_end_bin_loc           IN VARCHAR2
                           ,p_vendor                IN VARCHAR2
                           ,p_item                  IN VARCHAR2
                           ,p_cat_class             IN VARCHAR2
                           ,p_org_list              IN VARCHAR2
                           ,p_item_list             IN VARCHAR2
                           ,p_supplier_list         IN VARCHAR2
                           ,p_cat_class_list        IN VARCHAR2
                           ,p_source_list           IN VARCHAR2
                           ,p_intangibles           IN VARCHAR2);

    PROCEDURE wait_for_jobs (p_job_what VARCHAR2);

    PROCEDURE populate_demand_org (p_table_name VARCHAR2, p_organization_id NUMBER);

    PROCEDURE populate_delta_hits;

    PROCEDURE populate_hits_table;

    PROCEDURE populate_driving_tables;

    FUNCTION get_vendor_number (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2;

    PROCEDURE alter_table_temp (p_owner VARCHAR2, p_table_name VARCHAR2);

    PROCEDURE create_table_from_other_table (p_owner VARCHAR2, p_old_table_name VARCHAR2, p_new_table_name VARCHAR2);

    FUNCTION find_max_vendor_id (p_inventory_item_id NUMBER)
        RETURN NUMBER;

    PROCEDURE populate_vendor_tables;

    PROCEDURE populate_on_ord;

-- Revision 1.1 by Manjula on 31-Jul-2014 for TMS # 20140728-00130 begin

    PROCEDURE populate_bpa_Cost;

-- Revision 1.1 by Manjula on 31-Jul-2014 for TMS # 20140728-00130 end

END eis_po_xxwc_isr_pkg_v2;
/

-- Grants for Package
GRANT EXECUTE ON xxeis.eis_po_xxwc_isr_pkg_v2 TO apps
/

-- End of DDL Script for Package XXEIS.EIS_PO_XXWC_ISR_PKG_V2
