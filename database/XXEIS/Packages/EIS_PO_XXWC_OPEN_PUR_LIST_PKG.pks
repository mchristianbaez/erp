CREATE OR REPLACE PACKAGE XXEIS.EIS_PO_XXWC_OPEN_PUR_LIST_PKG
IS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Name         		:: XXEIS.EIS_PO_XXWC_OPEN_PUR_LIST_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package programs will call in EIS_XXWC_PO_OPEN_ORDERS_V view to populate data
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      18-may-2016       Initial Build TMS#20160503-00092
--// 1.1        Siva   		  02/07/2017	    TMS#20170105-00209 
--//============================================================================
type t_varchar2_vldn_tbl
IS
  TABLE OF VARCHAR2(100) INDEX BY VARCHAR2(100);
type t_number_tab
IS
  TABLE OF NUMBER INDEX BY binary_integer;
  g_po_type_vldn_tbl t_varchar2_vldn_tbl;
  g_lookup_meaning_vldn_tbl t_varchar2_vldn_tbl;
  g_author_status_vldn_tbl t_varchar2_vldn_tbl;
  g_actn_date_vldn_tbl t_varchar2_vldn_tbl;
  g_accep_flag_vldn_tbl t_varchar2_vldn_tbl;
  g_accep_type_vldn_tbl t_varchar2_vldn_tbl;
  g_accep_by_vldn_tbl t_varchar2_vldn_tbl;
  g_po_line_count_vldn_tbl t_number_tab;
  g_buyer_group_vldn_tbl t_varchar2_vldn_tbl; --added for version 1.1
  g_po_type              VARCHAR2(200);
  g_lookup_meaning       VARCHAR2(200);
  g_authorization_status VARCHAR2(200);
  g_po_lines_count       NUMBER;
  g_action_date          VARCHAR2(200);
  g_accepted_flag        VARCHAR2(200);
  g_acceptance_type      VARCHAR2(200);
  g_accepted_by          VARCHAR2(200);
  g_buyer_group          varchar2(200);  --added for version 1.1
  FUNCTION get_po_type(
      p_type_lookup_code IN VARCHAR2,
      p_org_id           IN NUMBER)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: get_po_type  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
  FUNCTION get_authorization_status(
      p_lookup_code IN VARCHAR2)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: get_authorization_status  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
  FUNCTION get_lookup_meaning(
      p_lookup_code IN VARCHAR2,
      p_lookup_type IN VARCHAR2)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: get_lookup_meaning  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
  FUNCTION get_po_line_count(
      p_header_id IN NUMBER)
    RETURN NUMBER;
--//============================================================================
--//
--// Object Name         :: get_po_line_count  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
  FUNCTION get_acceptance_details(
      p_po_header_id IN NUMBER,
      p_request_type IN VARCHAR2)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: get_acceptance_details  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
  FUNCTION get_buyer_group(
      p_agent_id in number)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: get_buyer_group  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter .
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.1        Siva   		  02/07/2017	    TMS#20170105-00209 
--//============================================================================		
END EIS_PO_XXWC_OPEN_PUR_LIST_PKG;
/
