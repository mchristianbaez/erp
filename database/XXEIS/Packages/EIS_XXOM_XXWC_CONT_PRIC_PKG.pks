create or replace 
PACKAGE            XXEIS.EIS_XXOM_XXWC_CONT_PRIC_PKG AS 
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "CONTRACT PRICING REPORT - INTERNAL"
--//
--// Object Name         		:: EIS_XXOM_XXWC_CONT_PRIC_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/20/2016      Initial Build  --TMS#20160426-00093   --Performance Tuning
--//============================================================================

   Type CURSOR_TYPE is ref cursor;
   Type View_Tab Is Table Of XXEIS.eis_xxwc_cont_modi_pricing%Rowtype Index By Binary_Integer;
   G_View_Tab      View_Tab ;
   G_VIEW_RECORD View_Tab;
   
   Type CURSOR_TYPE3 is ref cursor;
   Type View_Tab3 Is Table Of Xxeis.EIS_XXWC_ITEM_DATA%Rowtype Index By Binary_Integer;
   G_View_Tab3                   View_Tab3 ;
   G_VIEW_RECORD3                View_Tab3;
   
   Type CURSOR_TYPE2 is ref cursor;
   Type View_Tab2 Is Table Of Xxeis.EIS_XXWC_QUALIFIER_TMP%Rowtype Index By Binary_Integer;
   G_View_Tab2                    View_Tab2 ;
   G_VIEW_RECORD2                 VIEW_TAB2;
   
   TYPE CURSOR_TYPE4 IS REF CURSOR;
   TYPE View_tab4 IS TABLE OF XXEIS.EIS_XXWC_CONT_MODI_PRICING_V1%Rowtype Index By Binary_Integer;
   G_VIEW_RECORD4                    VIEW_TAB4 ;
   G_VIEW_TAB4                        VIEW_TAB4 ;

   
   Type CURSOR_TYPE5 is ref cursor;
   TYPE View_tab5 IS TABLE OF XXEIS.EIS_XXWC_LIST_PRICE%Rowtype Index By Binary_Integer;
   G_View_Tab5                    View_Tab5 ;
   G_VIEW_RECORD5                 VIEW_TAB5;
   
    TYPE CURSOR_TYPE8 IS REF CURSOR;
   TYPE VIEW_TAB8 IS TABLE OF XXEIS.EIS_XXWC_LIST_PRICE%ROWTYPE INDEX BY BINARY_INTEGER;
   G_VIEW_TAB8                    VIEW_TAB8 ;
   G_VIEW_RECORD8                 View_Tab8;

   Type CURSOR_TYPE6 is ref cursor;
   TYPE View_tab6 IS TABLE OF XXEIS.EIS_XXWC_LIST_PRICE%Rowtype Index By Binary_Integer;
   G_View_Tab6                    View_Tab6 ;
   G_VIEW_RECORD6                 View_Tab6;
   
   Type CURSOR_TYPE7 is ref cursor;
   TYPE View_tab7 IS TABLE OF xxeis.EIS_XXWC_UNIT_SALES_DATA_GT%Rowtype Index By Binary_Integer;
   G_View_Tab7                    View_Tab7;
   G_VIEW_RECORD7                 View_Tab7;





procedure CONTRACT_PRICING_MODIFIER  (         P_PROCESS_ID                   in number,
                                               P_START_DATE                   in date default null,
                                               P_END_DATE                     in date default null,
                                               P_Current_Loc                  in varchar2 default null,
                                               P_CATCLASS                     in varchar2 default null,
                                               P_CAT_PRICED_ITEMS             in varchar2  default null,
                                               P_RVP                          in varchar2  default null,
                                               P_DM                           in varchar2  default null,
                                               P_SALESPERSON                  IN VARCHAR2  DEFAULT NULL,
                                               P_JOB_ACCOUNT                  VARCHAR2,
                                               P_MASTER_ACCOUNT              	VARCHAR2,
                                               P_CUST_ID 					            Varchar2 DEFAULT NULL,
                                               P_Customer_id              		Varchar2 default null,
                                               P_MODIFIER_NAME              	VARCHAR2     default null
                                               );
--//============================================================================
--//
--// Object Name         :: contract_pricing_modifier  
--//
--// Object Usage 		 :: This Object Referred by "CONTRACT PRICING REPORT - INTERNAL"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_CONT_MODI_PRICING_V1 View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  04/22/2016       Initial Build  --TMS#20160426-00093  --Performance Tuning
--//============================================================================
end EIS_XXOM_XXWC_CONT_PRIC_PKG;
/
