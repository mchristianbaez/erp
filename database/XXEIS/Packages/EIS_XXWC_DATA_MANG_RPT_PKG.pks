CREATE OR REPLACE 
PACKAGE    XXEIS.EIS_XXWC_DATA_MANG_RPT_PKG AS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Data Management Audit Report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_DATA_MANG_RPT_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--// 1.1     	Siva  		15-Mar-2017      	 	 TMS#20170224-00065 
--// 1.2     	Siva  		24-Apr-2017      	 	TMS#20170306-00231 
--//============================================================================

type t_cashtype_vldn_tbl is table of varchar2(200) index by varchar2(200);

G_WEBSITE_ITEM_VLDN_TBL       T_CASHTYPE_VLDN_TBL;
G_COO_MEANING_VLDN_TBL        T_CASHTYPE_VLDN_TBL;
G_CAT_CLASS_VLDN_TBL          T_CASHTYPE_VLDN_TBL;
G_INV_CAT_CLASS_DESC_VLDN_TBL T_CASHTYPE_VLDN_TBL;
G_FLEX_START_DATE_VLDN_TBL    T_CASHTYPE_VLDN_TBL;
G_FLEX_END_DATE_VLDN_TBL      T_CASHTYPE_VLDN_TBL;
G_flex_enabled_flag_VLDN_TBL  T_CASHTYPE_VLDN_TBL;
G_flex_summary_flag_VLDN_TBL  T_CASHTYPE_VLDN_TBL;

G_WEBSITE_ITEM          VARCHAR2(200);
G_COO_MEANING           VARCHAR2(200);
G_INV_CAT_CLASS_DESC    VARCHAR2(200);
G_CONCATENATED_SEGMENTS VARCHAR2(200);
g_flex_enabled_flag 	varchar2(100);
g_flex_summary_flag 	varchar2(100);
G_FLEX_START_DATE 		date;
G_FLEX_END_DATE 		date;



FUNCTION WEBSITE_ITEM_HIERARCHY(P_INVENTORY_ITEM_ID IN NUMBER,P_ORGANIZATION_ID IN NUMBER,P_TYPE IN VARCHAR2) RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: WEBSITE_ITEM_HIERARCHY  
--//
--// Object Usage 		   :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Function
--//
--// Object Description  ::  This  Function is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View to populate
--//                          the website_item and wc_web_hierarchy
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--//============================================================================		

FUNCTION COO_MEANING(P_EMSY VARCHAR2) RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: COO_MEANING  
--//
--// Object Usage 		   :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Function
--//
--// Object Description  ::  This  Function is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View to populate
--//                          the country_of_origin values.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--//============================================================================		

FUNCTION GET_INV_CAT_CLASS (P_INVENTORY_ITEM_ID IN NUMBER,P_ORGANIZATION_ID IN NUMBER) RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: GET_INV_CAT_CLASS  
--//
--// Object Usage 		   :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Function
--//
--// Object Description  ::  This  Function is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View to populate
--//                          the INVENTORY_CATEGORY values.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--//============================================================================	

FUNCTION get_inv_cat_class_desc(P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number,P_request_type in varchar2)
 RETURN varchar2;
--//============================================================================
--//
--// Object Name         :: GET_INV_CAT_CLASS_DESC  
--//
--// Object Usage 		   :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Function
--//
--// Object Description  ::  This  Function is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View to populate
--//                          the INVENTORY_CATEGORY_DESCRIPTION values.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--//============================================================================	

FUNCTION get_flex_val_start_end_date(P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number,P_request_type in varchar2)
 RETURN date;
--//============================================================================
--//
--// Object Name         :: get_flex_val_start_end_date  
--//
--// Object Usage 		   :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Function
--//
--// Object Description  ::  This  Function is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View to populate
--//                          the INVENTORY_CATEGORY_DESCRIPTION values.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--//============================================================================	


PROCEDURE GET_DATA_MANG_DTLS (P_PROCESS_ID                 IN NUMBER,
                              P_ITEM_LIST                  IN VARCHAR2,
                              P_MASTER_VENDOR_LIST         IN VARCHAR2,
                              P_ORGANIZATION_LIST 	       IN VARCHAR2,
                              P_SOURCE_LIST 				       IN VARCHAR2,
                              P_SHORT_DESC 			           IN VARCHAR2,
                              P_Item_Number                IN VARCHAR2,
                              P_Master_Vendor_Number       IN VARCHAR2,
                              P_Source 	                   IN VARCHAR2,
                              P_Organization				       IN VARCHAR2,
                              P_Item_Status 			         IN VARCHAR2,
                              P_User_Item_Type             IN VARCHAR2,
                              P_Website_Item               IN VARCHAR2,
                              P_Master_Vendor_Name		     IN VARCHAR2,
                              P_ext_prod_attr_grp          IN VARCHAR2  --added for version 1.2
							);
--//============================================================================
--//
--// Object Name         :: GET_DATA_MANG_DTLS  
--//
--// Object Usage 		 :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  ::  This  procedure is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View.Due to the 
--// 						 performance issue we have converted the view to trigger based report.
--// 
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--// 1.2     	Siva  		15-Mar-2017      	 	TMS#20170306-00231 
--//============================================================================							
              
type CURSOR_TYPE4 is ref cursor;
 
type T_ARRAY is table of varchar2(32000)
   INDEX BY BINARY_INTEGER;

/*  --Commented code for version 1.1 
PROCEDURE clear_temp_tables(
        P_PROCESS_ID in number);
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--//============================================================================ 		
*/ --Commented code for version 1.1

END;
/
