CREATE OR REPLACE package  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      04/12/2016        Initial Build --TMS#20150921-00332 --performance tuning
--// 1.1		Siva		  07-Dec-2017	    TMS#20171206-00013 
--//============================================================================
TYPE T_number_tab IS TABLE OF number index by binary_integer;
TYPE T_varchar_tab IS TABLE OF VARCHAR2(200) INDEX BY VARCHAR2(200);
G_charge_amt_TAB            T_NUMBER_TAB;
G_order_line_cost_TAB       T_NUMBER_TAB;
G_line_modifier_name_tab    T_varchar_tab;
G_special_cost_tab          T_varchar_tab;
g_modifier_name_tab         T_varchar_tab;
g_cust_name_tab             T_varchar_tab;
g_cust_account_num_tab      T_varchar_tab;
G_PARTY_SITE_NUM            T_VARCHAR_TAB;
G_DISCOUNT_AMT_VLDN_TBL     T_VARCHAR_TAB;
G_FREIGHT_AMT_VLDN_TBL      T_VARCHAR_TAB;
g_charger_amt               number;
g_order_line_cost           number;
G_line_modifier_name        varchar2(200);
G_special_cost              varchar2(200);
g_modifier_name             varchar2(200);
g_cust_number               varchar2(200);
G_DISCOUNT_AMT              number;
G_FREIGHT_AMT               number;
g_cust_name varchar2(400);
G_PARTY_SITE_NUMBER           varchar2(200);
G_DATE_FROM DATE;
PROCEDURE SET_DATE_FROM (P_DATE_FROM IN DATE);
--//============================================================================
--//
--// Object Name         :: SET_DATE_FROM  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
FUNCTION GET_DATE_FROM  RETURN DATE;
--//============================================================================
--//
--// Object Name         :: GET_DATE_FROM  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
FUNCTION Get_Charge_amount(P_header_id in number) return number;
--//============================================================================
--//
--// Object Name         :: GET_CHARGE_AMOUNT  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
FUNCTION GET_ORDER_LINE_COST (p_line_id  IN NUMBER)RETURN NUMBER;
--//============================================================================
--//
--// Object Name         :: GET_ORDER_LINE_COST  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
FUNCTION GET_LINE_MODIFIER_NAME (P_HEADER_ID NUMBER, P_LINE_ID NUMBER) RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: GET_LINE_MODIFIER_NAME  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
FUNCTION GET_SPECIAL_MODIFER(P_HEADER_ID IN NUMBER,
                              P_LINE_ID IN NUMBER,
                            --  P_ITEM_ID IN NUMBER,
                              --P_organization_id in number,
                              P_SEGMENT IN VARCHAR2,
                              P_TYPE IN VARCHAR2
                              ) return varchar2;
--//============================================================================
--//
--// Object Name         :: GET_SPECIAL_MODIFER  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
Function Get_Customer_number (P_sold_to_org_id in NUMBER,P_type in varchar2) return varchar2 ;
--//============================================================================
--//
--// Object Name         :: GET_CUSTOMER_NUMBER  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
function get_party_site_num_f (p_invoice_to_org_id in number) return varchar2 ;
--//============================================================================
--//
--// Object Name         :: GET_PARTY_SITE_NUM_F  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
function GET_DISCOUNT_AMT (P_HEADER_ID number,P_LINE_ID number) return number;
--//============================================================================
--//
--// Object Name         :: GET_DISCOUNT_AMT  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
FUNCTION get_freight_amt (P_HEADER_ID NUMBER,P_LINE_ID NUMBER) RETURN number;
--//============================================================================
--//
--// Object Name         :: GET_FREIGHT_AMT  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================

-- Start version 1.1
G_SALESREP_NAME_VLDN_TBL      T_VARCHAR_TAB;
G_SALESREP_NUMBER_VLDN_TBL      T_VARCHAR_TAB;
g_salesrep_name              VARCHAR2(360);
g_salesrep_number            VARCHAR2(30);
-- End version 1.1

FUNCTION GET_SALESREP_DETAILS(
    p_salesrep_id NUMBER,
    p_org_id      NUMBER,
	  P_request_type varchar2)
  RETURN VARCHAR2;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Invoice Pre-Register report"
  --//
  --// Object Name          :: GET_SALESREP_DETAILS
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.1		  Siva		    07-Dec-2017		   TMS#20171206-00013 
  --//============================================================================ 

End;
/
