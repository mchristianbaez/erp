CREATE OR REPLACE 
PACKAGE  XXEIS.EIS_EDI_UTIL_ADOPTION_PKG AS
--//============================================================================
--//
--// Object Usage 				:: This Object Referred by "EDI Utilization and Adoption - WC"
--//
--// Object Name         		:: EIS_EDI_UTIL_ADOPTION_PKG
--//
--// Object Type         		:: Package Spec
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
--//============================================================================
g_region VARCHAR2(100);

type T_VARCHAR_TYPE_VLDN_TBL is table of varchar2(200) index by varchar2(200);

g_po_submitted_edi_vldn_tbl   T_VARCHAR_TYPE_VLDN_TBL;
g_po_submitted_edi number;

g_total_po_edi_sup_vldn_tbl T_VARCHAR_TYPE_VLDN_TBL;
g_total_po_edi_sup NUMBER;

g_po_confirmed_edi_vldn_tbl T_VARCHAR_TYPE_VLDN_TBL;
G_PO_CONFIRMED_EDI NUMBER;

G_PO_UNCONFIRMED_EDI_VLDN_TBL T_VARCHAR_TYPE_VLDN_TBL;
g_po_unconfirmed_edi number;

FUNCTION get_po_submitted_edi_func(
    P_REGION VARCHAR2,
    P_FROM_DATE   DATE,
    P_TO_DATE DATE,
    p_type varchar2)
  RETURN number;
--//============================================================================
--//
--// Object Name         :: get_po_submitted_edi_func
--//
--// Object Usage 		 :: This Object Referred by "EDI Utilization and Adoption - WC"
--//
--// Object Type         :: Function
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
--//============================================================================  

FUNCTION GET_TOTAL_PO_EDI_SUP_FUNC(
    P_REGION VARCHAR2,
    P_FROM_DATE   DATE,
    P_TO_DATE DATE,
    p_type varchar2)
  RETURN NUMBER;
--//============================================================================
--//
--// Object Name         :: GET_TOTAL_PO_EDI_SUP_FUNC
--//
--// Object Usage 		 :: This Object Referred by "EDI Utilization and Adoption - WC"
--//
--// Object Type         :: Function
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
--//============================================================================  

FUNCTION get_po_confirmed_edi_func(
    P_REGION VARCHAR2,
    P_FROM_DATE   DATE,
    P_TO_DATE DATE,
    p_type varchar2)
  RETURN number; 
--//============================================================================
--//
--// Object Name         :: get_po_confirmed_edi_func
--//
--// Object Usage 		 :: This Object Referred by "EDI Utilization and Adoption - WC"
--//
--// Object Type         :: Function
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
--//============================================================================  

FUNCTION get_po_unconfirmed_edi_func(
   P_FROM_DATE   DATE,
   P_TO_DATE date,
     p_type varchar2)
 RETURN NUMBER;
--//============================================================================
--//
--// Object Name         :: get_po_unconfirmed_edi_func
--//
--// Object Usage 		 :: This Object Referred by "EDI Utilization and Adoption - WC"
--//
--// Object Type         :: Function
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
--//============================================================================  

FUNCTION get_po_non_purchase_edi_func(
   P_TYPE VARCHAR2,
   P_FROM_DATE  DATE,
   P_TO_DATE date,
   p_pref_sup VARCHAR2)
 RETURN NUMBER;
--//============================================================================
--//
--// Object Name         :: get_po_non_purchase_edi_func
--//
--// Object Usage 		 :: This Object Referred by "EDI Utilization and Adoption - WC"
--//
--// Object Type         :: Function
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
--//============================================================================  
 
PROCEDURE get_edi_utilize_dtls_proc(
    p_region             IN VARCHAR2,
    p_fiscal_period      IN VARCHAR2,
    p_preferred_supplier IN VARCHAR2);
--//============================================================================
--//
--// Object Name         :: get_edi_utilize_dtls_proc
--//
--// Object Usage 		 :: This Object Referred by "EDI Utilization and Adoption - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_EDI_UTIL_ADOPTION_V View.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
--//============================================================================

 type CURSOR_TYPE4 is ref cursor;



End;
/
