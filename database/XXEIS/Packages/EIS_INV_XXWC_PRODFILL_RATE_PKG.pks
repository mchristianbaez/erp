CREATE OR REPLACE PACKAGE XXEIS.EIS_INV_XXWC_PRODFILL_RATE_PKG AS 

TYPE cursor_type IS REF CURSOR;
TYPE view_tab IS TABLE OF xxeis.eis_xxwc_inv_prod_fill_rate_v%rowtype;
g_view_tab      view_tab :=view_tab() ;
g_process_id   NUMBER;

FUNCTION  get_bo_aging_bucket(p_bucket_size NUMBER ,P_RELEASED_STATUS VARCHAR2,p_item_id NUMBER,p_org_id NUMBER, p_date DATE )RETURN NUMBER ;

PROCEDURE PROD_FILL_RATE
    (
    p_process_id            IN NUMBER,
    p_region                IN VARCHAR2,
    p_district              IN VARCHAR2,
    p_location              IN VARCHAR2,
    p_location_list         IN VARCHAR2,
    p_vendor_list           IN VARCHAR2,
    p_source_list           IN VARCHAR2,
    p_cat_class_list        IN VARCHAR2,
    p_item_list             IN VARCHAR2
    );
    
PROCEDURE LOAD_ORDER_INFO
    (
    p_process_id            IN NUMBER   
    );
        
FUNCTION get_process_id   RETURN number;
END EIS_INV_XXWC_PRODFILL_RATE_PKG;
/