--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_CNTR_ORD_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_OM_ORDER_EXP_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     06-May-2016        Siva   		TMS#20160503-00089  Performance Tuning
  1.1     15-Mar-2017      	 Siva   		TMS#20170224-00065 
********************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_CNTR_ORD_TAB --added for version 1.1
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_CNTR_ORD_TAB  --added for version 1.1
   (PROCESS_ID NUMBER, 
	HEADER_ID NUMBER, 
	LINE_ID NUMBER
)
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
