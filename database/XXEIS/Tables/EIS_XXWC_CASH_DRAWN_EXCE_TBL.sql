---------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_CASH_DRAWN_EXCE_TBL
  Description: This table is used to get data from XXEIS.EIS_XXWC_CASH_EXCEPTION_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva   		TMS#20160411-00103  Performance Tuning
  1.1     15-Mar-2017      	 Siva   		 TMS#20170224-00065
********************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_TBL CASCADE CONSTRAINTS --added for version 1.1
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_TBL  --added for version 1.1
  (
    ORDER_NUMBER          NUMBER,
    ORG_ID                NUMBER,
    PAYMENT_TYPE_CODE     VARCHAR2(150 BYTE),
    PAYMENT_TYPE_CODE_NEW VARCHAR2(80 BYTE),
    TAKEN_BY              VARCHAR2(150 BYTE),
    CASH_TYPE             VARCHAR2(12 BYTE),
    CHK_CARDNO            VARCHAR2(50 BYTE),
    CARD_NUMBER           VARCHAR2(12 BYTE),
    CARD_ISSUER_CODE      VARCHAR2(30 BYTE),
    CARD_ISSUER_NAME      VARCHAR2(100 BYTE),
    CASH_AMOUNT           NUMBER,
    CUSTOMER_NUMBER       VARCHAR2(150 BYTE),
    CUSTOMER_NAME         VARCHAR2(360 BYTE),
    BRANCH_NUMBER         VARCHAR2(30 BYTE),
    CHECK_NUMBER          VARCHAR2(50 BYTE),
    ORDER_DATE            DATE,
    CASH_DATE             DATE,
    PAYMENT_CHANNEL_NAME  VARCHAR2(100 BYTE),
    NAME                  VARCHAR2(30 BYTE),
    ORDER_AMOUNT          NUMBER,
    DIFF                  NUMBER,
    PARTY_ID              NUMBER(15,0),
    CUST_ACCOUNT_ID       NUMBER(15,0),
    INVOICE_NUMBER        VARCHAR2(240 BYTE),
    INVOICE_DATE          DATE,
    SEGMENT_NUMBER        VARCHAR2(25 BYTE),
    HEADER_ID             NUMBER,
    ON_ACCOUNT            NUMBER,
    DIST_DATE             DATE,
    PROCESS_ID            NUMBER
)
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
