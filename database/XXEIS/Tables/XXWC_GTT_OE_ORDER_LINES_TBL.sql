---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL $
  Module Name : Order Management
  PURPOSE	  : Open Sales Orders Report
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  20-Jun-16         Siva			  TMS# 20160617-00018  --performance Tuning
  1.1     15-Mar-2017      	Siva   		  	  TMS#20170224-00065
  1.2     11-Aug-2017		Siva			  TMS#20170727-00135 
**************************************************************************************************************/
DROP TABLE XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL CASCADE CONSTRAINTS  --added for version 1.1
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL --added for version 1.1
  (
    PROCESS_ID            NUMBER,
    LINE_ID               NUMBER NOT NULL ENABLE,
    HEADER_ID             NUMBER NOT NULL ENABLE,
    SOLD_FROM_ORG_ID      NUMBER,
    SHIP_FROM_ORG_ID      NUMBER,
    WAREHOUSE             VARCHAR2(30 BYTE),
    SHIPPING_METHOD_CODE  VARCHAR2(30 BYTE),
    ORDERED_QUANTITY      NUMBER,
    CREATION_DATE         DATE NOT NULL ENABLE,
    FLOW_STATUS_CODE      VARCHAR2(30 BYTE),
    UNIT_SELLING_PRICE    NUMBER,
    LINE_CATEGORY_CODE    VARCHAR2(30 BYTE) NOT NULL ENABLE,
    SCHEDULE_SHIP_DATE    DATE,
    INVENTORY_ITEM_ID     NUMBER NOT NULL ENABLE,
    ITEM_NUMBER           VARCHAR2(50 BYTE),
    USER_ITEM_DESCRIPTION VARCHAR2(1000 BYTE),
	REGION 				  VARCHAR2(150)  --added for Version 1.2
  )
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
