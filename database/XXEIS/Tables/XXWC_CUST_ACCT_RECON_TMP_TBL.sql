---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_CUST_ACCT_RECON_TMP_TBL $
  Module Name : Receivables
  PURPOSE	  : Customer Account Reconciliation report
  VERSION 		  DATE            AUTHOR(S)       	DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	   13-Apr-2017      	  Siva   		  TMS#20170117-00323 
**************************************************************************************************************/
DROP TABLE XXEIS.XXWC_CUST_ACCT_RECON_TMP_TBL CASCADE CONSTRAINTS  
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_CUST_ACCT_RECON_TMP_TBL
  (
    REPORT_VIEW             VARCHAR2(30 BYTE),
    CUSTOMER_NAME           VARCHAR2(360 BYTE),
    CUSTOMER_NUMBER         VARCHAR2(50 BYTE),
    PHONE_NUMBER            VARCHAR2(240 BYTE),
    CREDIT_MANAGER          VARCHAR2(240 BYTE),
    ACCOUNT_MANAGER         VARCHAR2(360 BYTE),
    PAYMENT_TERMS           VARCHAR2(30 BYTE),
    ACCOUNT_STATUS          VARCHAR2(240 BYTE),
    PARTY_SITE_NUMBER       VARCHAR2(30 BYTE),
    LOCATION                VARCHAR2(50 BYTE),
    TRANSACTION_NUMBER      VARCHAR2(30 BYTE),
    TRX_DATE                DATE,
    PO_NUMBER               VARCHAR2(50 BYTE),
    RECEIPT_NUMBER          VARCHAR2(30 BYTE),
    RECEIPT_DATE            DATE,
    TRX_AMOUNT_DUE_ORIGINAL NUMBER,
    TRANSACTION_BALANCE_DUE NUMBER,
    TAX_BALANCE_DUE         NUMBER,
    EARNED_DISCOUNT         NUMBER,
    DAYS_LATE               NUMBER,
    TRX_PAYMENT_TERMS       VARCHAR2(30 BYTE),
    PAYMENT_METHOD          VARCHAR2(240 BYTE),
    APPLIED_TRX_NUMBER      VARCHAR2(240 BYTE),
    APPLIED_AMOUNT          NUMBER,
    APPLIED_TRX_TYPE        VARCHAR2(240 BYTE),
    STATE                   VARCHAR2(240 BYTE),
    ACTIVITY_DATE           DATE,
    CREATED_BY              VARCHAR2(240 BYTE),
    REFERENCE               VARCHAR2(240 BYTE),
    UNEARNED_DISCOUNT       NUMBER,
    DEPOSIT_DATE            DATE,
    APPLIED_DATE            DATE,
    RECEIPT_COMMENTS        VARCHAR2(2000 BYTE),
    DISCREPANCY_CODE        VARCHAR2(150 BYTE),
    RECEIPT_AMOUNT          NUMBER,
    UNAPPLIED_AMOUNT        NUMBER,
    PREPAYMENT_AMOUNT       NUMBER,
    ON_ACCOUNT_AMOUNT       NUMBER
  )
  ON COMMIT PRESERVE ROWS
/
