/**************************************************************************************************
    $Header EIS_XXWC_ISR_PO_LIVE_RPT_TBL.sql $
    Module Name: EIS_XXWC_ISR_PO_LIVE_RPT_TBL
    PURPOSE: Table built for 'Item Attribute Validation Report - WC' performance Issue
    REVISIONS:
    Ver    Date         Author                Description
    ------ -----------  ------------------    ----------------
    1.0    20-Jul-16    Siva                  TMS#20160708-00029
**************************************************************************************************/
CREATE GLOBAL TEMPORARY TABLE 
XXEIS.EIS_XXWC_ISR_PO_LIVE_RPT_TBL(
ORG                          VARCHAR2(4000), 
PRE                          VARCHAR2(4000) ,
ITEM_NUMBER                  VARCHAR2(4000) ,
VENDOR_NUM                   VARCHAR2(4000) ,
VENDOR_NAME                  VARCHAR2(4000) ,
SOURCE                       VARCHAR2(4000) ,
ST                           VARCHAR2(4000) ,
DESCRIPTION                  VARCHAR2(4000) ,
CAT                          VARCHAR2(4000) ,
PPLT                         VARCHAR2(4000) ,
PLT                          VARCHAR2(4000) ,
UOM                          VARCHAR2(4000) ,
CL                           VARCHAR2(4000) ,
STK_FLAG                     VARCHAR2(4000) ,
PM                           VARCHAR2(4000) ,
MINN                         VARCHAR2(4000) ,
MAXN                         VARCHAR2(4000) ,
AMU                          VARCHAR2(4000) ,
MF_FLAG                      VARCHAR2(4000) ,
HIT6_SALES                   NUMBER         ,
AVER_COST                    NUMBER         ,
ITEM_COST                    NUMBER         ,
BPA_COST                     NUMBER         ,
BPA                          VARCHAR2(4000) ,
QOH                          NUMBER         ,
AVAILABLE                    NUMBER         ,
AVAILABLEDOLLAR              NUMBER         ,
JAN_SALES                    NUMBER         ,
FEB_SALES                    NUMBER         ,
MAR_SALES                    NUMBER         ,
APR_SALES                    NUMBER         ,
MAY_SALES                    NUMBER         ,
JUNE_SALES                   NUMBER         ,
JUL_SALES                    NUMBER         ,
AUG_SALES                    NUMBER         ,
SEP_SALES                    NUMBER         ,
OCT_SALES                    NUMBER         ,
NOV_SALES                    NUMBER         ,
DEC_SALES                    NUMBER         ,
HIT4_SALES                   NUMBER         ,
ONE_SALES                    NUMBER         ,
SIX_SALES                    NUMBER         ,
TWELVE_SALES                 NUMBER         ,
BIN_LOC                      VARCHAR2(4000) ,
MC                           VARCHAR2(4000) ,
FI_FLAG                      VARCHAR2(4000) ,
FREEZE_DATE                  DATE           ,
RES                          VARCHAR2(4000) ,
THIRTEEN_WK_AVG_INV          NUMBER         ,
THIRTEEN_WK_AN_COGS          NUMBER         ,
TURNS                        NUMBER         ,
BUYER                        VARCHAR2(4000) ,
TS                           VARCHAR2(4000) ,
SO                           NUMBER         ,
INVENTORY_ITEM_ID            NUMBER         ,
ORGANIZATION_ID              NUMBER         ,
SET_OF_BOOKS_ID              NUMBER         ,
ON_ORD                       NUMBER         ,
WT                           NUMBER         ,
SS                           NUMBER         ,
FML                          NUMBER         ,
OPEN_REQ                     NUMBER         ,
ORG_NAME                     VARCHAR2(4000) ,
DISTRICT                     VARCHAR2(4000) ,
REGION                       VARCHAR2(4000) ,
SOURCING_RULE                VARCHAR2(4000) ,
CLT                          NUMBER         ,
COMMON_OUTPUT_ID             NUMBER         ,
PROCESS_ID                   NUMBER         ,
AVAIL2                       NUMBER         ,
INT_REQ                      NUMBER         ,
DIR_REQ                      NUMBER         ,
DEMAND                       NUMBER         ,
SITE_VENDOR_NUM              VARCHAR2(4000) ,
VENDOR_SITE                  VARCHAR2(4000) 
) ON COMMIT PRESERVE ROWS
/
