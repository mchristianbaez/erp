--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_DAILY_FLASH_DTL_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_DAILY_FLASH_DTLS_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     20-Apr-2016        PRAMOD   		TMS#20160429-00034  Performance Tuning
  1.1     15-Mar-2017  		 Siva   	    TMS#20170224-00065 
********************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_DAILY_FLASH_DTL_TAB CASCADE CONSTRAINTS --added for version 1.1
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_DAILY_FLASH_DTL_TAB  --added for version 1.1
  (
    CURRENT_FISCAL_MONTH VARCHAR2(240 BYTE),
    CURRENT_FISCAL_YEAR  number,
    DELIVERY_TYPE        VARCHAR2(240 BYTE),
    BRANCH_LOCATION      VARCHAR2(240 BYTE),
    INVOICE_COUNT        NUMBER,
    SALES                NUMBER,
    PROCESS_ID           NUMBER
)
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
