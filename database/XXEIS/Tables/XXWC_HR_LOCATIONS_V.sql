--
-- XXWC_HR_LOCATIONS_V  (View)
--
--  Dependencies:
--   HR_LOCATIONS_ALL (Synonym)
--

CREATE OR REPLACE FORCE VIEW xxeis.xxwc_hr_locations_v
(
   location_id
  ,location_code
  ,inventory_organization_id
)
AS
   SELECT location_id, SUBSTR (location_code, 1, 3) location_code, inventory_organization_id FROM apps.hr_locations_all;
