--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_LOW_QTY_HDR_TBL
  Description: This table is used to load data from XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva       --TMS#20150928-00191   Performance Tuning
  1.1     15-Mar-2017      	 Siva   	  TMS#20170224-00065 
********************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_LOW_QTY_HDR_TBL CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE  XXEIS.EIS_XXWC_LOW_QTY_HDR_TBL --added for version 1.1
   (	process_id number, 
	header_id number
) ON COMMIT PRESERVE ROWS  --added for version 1.1
/
