--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_HEADER_LINE_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_OM_LINE_DISP_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     18-Apr-2016        PRAMOD   			TMS#20160426-00102  Performance Tuning
********************************************************************************/
DROP TABLE XXEIS.EIS_HEADER_LINE_TAB CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_HEADER_LINE_TAB 
   (	PROCESS_ID NUMBER, 
		HEADER_ID NUMBER,
	    LINE_ID NUMBER
)
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
