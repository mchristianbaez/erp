/* Formatted on 05-Aug-2013 18:25:57 (QP5 v5.206) */
-- Start of DDL Script for Table XXEIS.EIS_XXWC_PO_ISR_TAB_V2
-- Generated 05-Aug-2013 18:25:48 from XXEIS@EBIZFQA

CREATE TABLE xxeis.eis_xxwc_po_isr_tab_v2
(
    org                     VARCHAR2 (240 BYTE)
   ,pre                     VARCHAR2 (240 BYTE)
   ,item_number             VARCHAR2 (240 BYTE)
   ,vendor_num              VARCHAR2 (240 BYTE)
   ,vendor_name             VARCHAR2 (240 BYTE)
   ,source                  VARCHAR2 (240 BYTE)
   ,st                      VARCHAR2 (240 BYTE)
   ,description             VARCHAR2 (450 BYTE)
   ,cat                     VARCHAR2 (240 BYTE)
   ,pplt                    NUMBER
   ,plt                     NUMBER
   ,uom                     VARCHAR2 (240 BYTE)
   ,cl                      VARCHAR2 (240 BYTE)
   ,stk_flag                VARCHAR2 (10 BYTE)
   ,pm                      VARCHAR2 (240 BYTE)
   ,minn                    NUMBER
   ,maxn                    NUMBER
   ,amu                     VARCHAR2 (240 BYTE)
   ,mf_flag                 VARCHAR2 (10 BYTE)
   ,hit6_store_sales        NUMBER
   ,hit6_other_inv_sales    NUMBER
   ,aver_cost               NUMBER
   ,item_cost               NUMBER
   ,bpa                     VARCHAR2 (240 BYTE)
   ,qoh                     NUMBER
   ,available               NUMBER
   ,availabledollar         NUMBER
   ,one_store_sale          NUMBER
   ,six_store_sale          NUMBER
   ,twelve_store_sale       NUMBER
   ,one_other_inv_sale      NUMBER
   ,six_other_inv_sale      NUMBER
   ,twelve_other_inv_sale   NUMBER
   ,bin_loc                 VARCHAR2 (240 BYTE)
   ,mc                      VARCHAR2 (240 BYTE)
   ,fi_flag                 VARCHAR2 (10 BYTE)
   ,freeze_date             DATE
   ,res                     VARCHAR2 (240 BYTE)
   ,thirteen_wk_avg_inv     NUMBER
   ,thirteen_wk_an_cogs     NUMBER
   ,turns                   NUMBER
   ,buyer                   VARCHAR2 (240 BYTE)
   ,ts                      NUMBER
   ,jan_store_sale          NUMBER
   ,feb_store_sale          NUMBER
   ,mar_store_sale          NUMBER
   ,apr_store_sale          NUMBER
   ,may_store_sale          NUMBER
   ,jun_store_sale          NUMBER
   ,jul_store_sale          NUMBER
   ,aug_store_sale          NUMBER
   ,sep_store_sale          NUMBER
   ,oct_store_sale          NUMBER
   ,nov_store_sale          NUMBER
   ,dec_store_sale          NUMBER
   ,jan_other_inv_sale      NUMBER
   ,feb_other_inv_sale      NUMBER
   ,mar_other_inv_sale      NUMBER
   ,apr_other_inv_sale      NUMBER
   ,may_other_inv_sale      NUMBER
   ,jun_other_inv_sale      NUMBER
   ,jul_other_inv_sale      NUMBER
   ,aug_other_inv_sale      NUMBER
   ,sep_other_inv_sale      NUMBER
   ,oct_other_inv_sale      NUMBER
   ,nov_other_inv_sale      NUMBER
   ,dec_other_inv_sale      NUMBER
   ,hit4_store_sales        NUMBER
   ,hit4_other_inv_sales    NUMBER
   ,inventory_item_id       NUMBER
   ,organization_id         NUMBER
   ,set_of_books_id         NUMBER
   ,org_name                VARCHAR2 (150 BYTE)
   ,district                VARCHAR2 (100 BYTE)
   ,region                  VARCHAR2 (100 BYTE)
   ,common_output_id        NUMBER
   ,process_id              NUMBER
   ,inv_cat_seg1            VARCHAR2 (240 BYTE)
   ,on_ord                  NUMBER
   ,bpa_cost                NUMBER
   ,open_req                NUMBER
   ,wt                      NUMBER
   ,fml                     NUMBER
   ,sourcing_rule           VARCHAR2 (450 BYTE)
   ,so                      NUMBER
   ,ss                      NUMBER
   ,clt                     NUMBER
   ,avail2                  NUMBER
   ,int_req                 NUMBER
   ,dir_req                 NUMBER
   ,demand                  NUMBER
   ,item_status_code        VARCHAR2 (50 BYTE)
   ,site_vendor_num         VARCHAR2 (240 BYTE)
   ,vendor_site             VARCHAR2 (240 BYTE)
)
SEGMENT CREATION IMMEDIATE
PCTFREE 10
INITRANS 1
MAXTRANS 255
TABLESPACE xxeis_data
STORAGE (INITIAL 131072
         NEXT 131072
         PCTINCREASE 0
         MINEXTENTS 1
         MAXEXTENTS 2147483645)
NOCACHE
MONITORING
NOPARALLEL
LOGGING
/

-- End of DDL Script for Table XXEIS.EIS_XXWC_PO_ISR_TAB_V2
