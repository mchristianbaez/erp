--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: XXWC_PICKING_QLTY_SUMMARY_TBL
  Description: This table is used to get data from XXEIS.XXWC_PICKING_QLTY_CNTRL_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     28-Mar-2018        Siva   		TMS#TMS#20180116-00278
********************************************************************************/
DROP TABLE XXEIS.XXWC_PICKING_QLTY_SUMMARY_TBL CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_PICKING_QLTY_SUMMARY_TBL 
   (	REPORT_TYPE VARCHAR2(10 BYTE), 
	BRANCH VARCHAR2(3 BYTE), 
	DISTRICT VARCHAR2(150 BYTE), 
	REGION VARCHAR2(150 BYTE), 
	AVG_TIME_PER_ORDER NUMBER, 
	AVG_TIME_PER_LINE NUMBER, 
	PICKING_ACCURACY_PERCENT NUMBER, 
	TOTAL_NO_OF_ORDERS NUMBER, 
	TOTAL_NO_OF_LINES NUMBER, 
	ORGANIZATION_ID NUMBER, 
	OPERATING_UNIT NUMBER
   ) ON COMMIT PRESERVE ROWS
/
  