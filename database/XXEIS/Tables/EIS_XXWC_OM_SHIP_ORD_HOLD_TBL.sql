--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_OM_SHIP_ORD_HOLD_TBL
  Description: This table is used to get data from XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG Package.
  HISTORY
  ===============================================================================
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0        01-Jan-2018  			Siva       	Initial Version --20171213-00051 
********************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL
  (
    CUSTOMER_NUMBER        VARCHAR2(30),
    CUSTOMER_NAME          VARCHAR2(360),
    LOC                    VARCHAR2(30),
    SALES_ORDER_NUMBER     NUMBER,
    CREATED_BY             VARCHAR2(240),
    ORDER_HEADER_STATUS    VARCHAR2(240),
    ORDER_TYPE             VARCHAR2(2000),
    ORDERED_DATE           DATE,
    ORDER_LINE_STATUS      VARCHAR2(240),
    ORDER_LINE_NUMBER      VARCHAR2(360),
    ORDER_HEADER_HOLD_NAME VARCHAR2(360),
    ORDER_HEADER_HOLD      VARCHAR2(5),
    ORDER_LINE_HOLD_NAME   VARCHAR2(360),
    ORDER_LINE_HOLD        VARCHAR2(5),
    HEADER_HOLD_DATE       DATE,
    LINE_HOLD_DATE         DATE,
    HOLD_DATE              DATE,
    PART_NUMBER            VARCHAR2(240),
    PART_DESCRIPTION       VARCHAR2(240),
    HOLD_BY_NTID           VARCHAR2(240),
    HOLD_BY_ASSOCIATE_NAME VARCHAR2(240)
  ) ON COMMIT PRESERVE ROWS
;