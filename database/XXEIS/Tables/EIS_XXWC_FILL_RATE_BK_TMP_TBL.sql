BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE XXEIS.EIS_XXWC_FILL_RATE_BK_TMP_TBL';
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

CREATE GLOBAL TEMPORARY TABLE xxeis.eis_xxwc_fill_rate_bk_tmp_tbl
(
   inventory_item_id      NUMBER
  ,organization_id        NUMBER
  ,order_number           NUMBER
  ,ordered_quantity       NUMBER
  ,shipped_quantity       NUMBER
  ,backordered_quantity   NUMBER
  ,item                   VARCHAR2 (2000 BYTE)
  ,customer_number        VARCHAR2 (30 BYTE)
  ,salesrepname           VARCHAR2 (360 BYTE)
  ,salesrep_id            NUMBER (15, 0)
  ,process_id             NUMBER
) ON COMMIT PRESERVE ROWS;