/****************************************************************************************************
    *
    *  TABLE
    *  EIS_XXWC_AR_INACTIVE_ACC_STG
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       	DESCRIPTION
    * ------- ----------- --------------- 	----------------------------------------------------------------
    * 1.0    08/25/2015   Maharajan SShunmugam    TMS#20150825-00160 IT - need a view created for the Account Inactivation EiS report
    ******************************************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_AR_INACTIVE_ACC_STG
(  ACCOUNT_NUMBER	VARCHAR2(30),
   ACCOUNT_NAME		VARCHAR2(240),
   COLLECTOR		VARCHAR2(30),
   PROFILE_CLASS	VARCHAR2(30),
   LAST_SALE_DATE	DATE,
   LAST_PAYMENT_DATE	DATE,
   ORDER_DATE		DATE,
   AMOUNT_DUE_REMAINING NUMBER)
/