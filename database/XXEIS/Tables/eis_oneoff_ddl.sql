-- Script Name : eis_oneoff_ddl.sql
-- Purpose     : This script is to add oneoff_patch column in eis_patches table and insert corresponding patch details into it.
-- Version     : 7.0.0/8.00
-- MODIFICATION HISTORY
-- Person       Date  Comments
-- ---------    ------  ------------------------------------------
-- Santosh Punna     03-May-2013 Added oneoff_patch column in eis_patches table

SET serveroutput ON;
BEGIN
  EXECUTE IMMEDIATE 'ALTER TABLE xxeis.eis_patches add ( oneoff_patch VARCHAR2(1))';
EXCEPTION
WHEN OTHERS THEN
  dbms_output.put_line('oneoff_patch column already exists');
END;
/
DECLARE
	l_count number ;
    l_patch_number VARCHAR2(50) := 'eis_patch_case_9610_hdsupply';
	l_product_code VARCHAR2(50) := 'RS';
	l_build_number NUMBER := 1;
	l_description VARCHAR2(1000) := 'This patch provides for data issue for  "Back Order Report" on top of 70110_BE';
	l_released_date DATE := to_date('03-May-2013','DD-MON-RRRR');
BEGIN
  SELECT COUNT(*)
  INTO l_count
  FROM xxeis.eis_patches
  WHERE patch_number         = TRIM(l_patch_number)
  AND product_code           = l_product_code
  AND NVL(build_number,-999) = NVL(l_build_number,-999);
  
  IF l_count                 = 0 THEN
    INSERT
    INTO xxeis.eis_patches
      (
        patch_id ,
        patch_number ,
        product_code ,
        build_number ,
        description ,
        released_date ,
        applied_date ,
        patch_status,
		oneoff_patch
      )
      VALUES
      (
        xxeis.eis_patches_s.nextval,
        TRIM(l_patch_number),
        l_product_code,
        l_build_number,
        l_description,
        l_released_date,
        sysdate,
        'C',
		'Y'
      );
  ELSE
    UPDATE xxeis.eis_patches
    SET description            = l_description,
      released_date            = l_released_date
    WHERE patch_number         = TRIM(l_patch_number)
    AND NVL(build_number,-999) = NVL(l_build_number,-999)
    AND product_code           = l_product_code ;
  END IF;
  COMMIT ;
END;

/

