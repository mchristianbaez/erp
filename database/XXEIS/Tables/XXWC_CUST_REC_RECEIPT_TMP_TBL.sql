---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_CUST_REC_RECEIPT_TMP_TBL $
  Description	  : This table is used to get data from XXEIS.EIS_XXWC_CUST_ACC_RECON_PKG Package.
  REVISIONS   :
  VERSION 		  DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     		03-Oct-2016       		Siva   		 TMS#20130909-00776 
**************************************************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_CUST_REC_RECEIPT_TMP_TBL
    (
      CUSTOMER_NAME           VARCHAR2(360),
      CUSTOMER_NUMBER         VARCHAR2(50),
      PHONE_NUMBER            VARCHAR2(240),
      CREDIT_MANAGER          VARCHAR2(240),
      ACCOUNT_MANAGER         VARCHAR2(360),
      PAYMENT_TERMS           VARCHAR2(30),
      ACCOUNT_STATUS          VARCHAR2(240),
      PARTY_SITE_NUMBER       VARCHAR2(30),
      LOCATION                VARCHAR2(50),
      RECEIPT_NUMBER          VARCHAR2(30),
      RECEIPT_DATE            DATE,
      TRX_DATE                DATE,
      TRANSACTION_NUMBER      VARCHAR2(30),
      TRX_AMOUNT_DUE_ORIGINAL NUMBER,
	  TRANSACTION_BALANCE_DUE NUMBER,
      APPLIED_AMOUNT          NUMBER,
      STATE                   VARCHAR2(240),
      EARNED_DISCOUNT         NUMBER,
      UNEARNED_DISCOUNT       NUMBER,
      DEPOSIT_DATE            DATE,
      APPLIED_DATE            DATE,
      PAYMENT_METHOD          VARCHAR2(240),
      RECEIPT_COMMENTS        VARCHAR2(2000),
      DISCREPANCY_CODE        VARCHAR2(150),
      RECEIPT_AMOUNT          NUMBER,
      UNAPPLIED_AMOUNT        NUMBER,
      PREPAYMENT_AMOUNT       NUMBER,
      ON_ACCOUNT_AMOUNT       NUMBER
    )
ON COMMIT PRESERVE ROWS
/
