--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_PUR_ACC_FIS_PER_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     29-Apr-2016        PRAMOD   		TMS#20160429-00036  --Performance Tuning
********************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_TAB
  (
    PROCESS_ID     NUMBER,
    MVID           VARCHAR2(150 BYTE),
    VENDOR_NAME    VARCHAR2(150 BYTE),
    LOB            VARCHAR2(150 BYTE),
    BU             VARCHAR2(150 BYTE),
    AGREEMENT_YEAR NUMBER,
    FISCAL_PERIOD  VARCHAR2(150 BYTE),
    PURCHASES      NUMBER,
    REBATE         NUMBER,
    COOP           NUMBER,
    TOTAL_ACCRUALS NUMBER
)
/
