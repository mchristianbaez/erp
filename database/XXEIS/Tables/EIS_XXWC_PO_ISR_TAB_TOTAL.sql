CREATE TABLE XXEIS.EIS_XXWC_PO_ISR_TAB_TOTAL
(
  ORG                    VARCHAR2(240 BYTE),
  PRE                    VARCHAR2(240 BYTE),
  ITEM_NUMBER            VARCHAR2(240 BYTE),
  VENDOR_NUM             VARCHAR2(240 BYTE),
  VENDOR_NAME            VARCHAR2(240 BYTE),
  SOURCE                 VARCHAR2(240 BYTE),
  ST                     VARCHAR2(240 BYTE),
  DESCRIPTION            VARCHAR2(450 BYTE),
  CAT                    VARCHAR2(240 BYTE),
  PPLT                   NUMBER,
  PLT                    NUMBER,
  UOM                    VARCHAR2(240 BYTE),
  CL                     VARCHAR2(240 BYTE),
  STK_FLAG               VARCHAR2(10 BYTE),
  PM                     VARCHAR2(240 BYTE),
  MINN                   NUMBER,
  MAXN                   NUMBER,
  AMU                    VARCHAR2(240 BYTE),
  MF_FLAG                VARCHAR2(10 BYTE),
  HIT6_STORE_SALES       NUMBER,
  HIT6_OTHER_INV_SALES   NUMBER,
  AVER_COST              NUMBER,
  ITEM_COST              NUMBER,
  BPA                    VARCHAR2(240 BYTE),
  QOH                    NUMBER,
  AVAILABLE              NUMBER,
  AVAILABLEDOLLAR        NUMBER,
  ONE_STORE_SALE         NUMBER,
  SIX_STORE_SALE         NUMBER,
  TWELVE_STORE_SALE      NUMBER,
  ONE_OTHER_INV_SALE     NUMBER,
  SIX_OTHER_INV_SALE     NUMBER,
  TWELVE_OTHER_INV_SALE  NUMBER,
  BIN_LOC                VARCHAR2(240 BYTE),
  MC                     VARCHAR2(240 BYTE),
  FI_FLAG                VARCHAR2(10 BYTE),
  FREEZE_DATE            DATE,
  RES                    VARCHAR2(240 BYTE),
  THIRTEEN_WK_AVG_INV    NUMBER,
  THIRTEEN_WK_AN_COGS    NUMBER,
  TURNS                  NUMBER,
  BUYER                  VARCHAR2(240 BYTE),
  TS                     NUMBER,
  JAN_STORE_SALE         NUMBER,
  FEB_STORE_SALE         NUMBER,
  MAR_STORE_SALE         NUMBER,
  APR_STORE_SALE         NUMBER,
  MAY_STORE_SALE         NUMBER,
  JUN_STORE_SALE         NUMBER,
  JUL_STORE_SALE         NUMBER,
  AUG_STORE_SALE         NUMBER,
  SEP_STORE_SALE         NUMBER,
  OCT_STORE_SALE         NUMBER,
  NOV_STORE_SALE         NUMBER,
  DEC_STORE_SALE         NUMBER,
  JAN_OTHER_INV_SALE     NUMBER,
  FEB_OTHER_INV_SALE     NUMBER,
  MAR_OTHER_INV_SALE     NUMBER,
  APR_OTHER_INV_SALE     NUMBER,
  MAY_OTHER_INV_SALE     NUMBER,
  JUN_OTHER_INV_SALE     NUMBER,
  JUL_OTHER_INV_SALE     NUMBER,
  AUG_OTHER_INV_SALE     NUMBER,
  SEP_OTHER_INV_SALE     NUMBER,
  OCT_OTHER_INV_SALE     NUMBER,
  NOV_OTHER_INV_SALE     NUMBER,
  DEC_OTHER_INV_SALE     NUMBER,
  HIT4_STORE_SALES       NUMBER,
  HIT4_OTHER_INV_SALES   NUMBER,
  INVENTORY_ITEM_ID      NUMBER,
  ORGANIZATION_ID        NUMBER,
  SET_OF_BOOKS_ID        NUMBER,
  ORG_NAME               VARCHAR2(150 BYTE),
  DISTRICT               VARCHAR2(100 BYTE),
  REGION                 VARCHAR2(100 BYTE),
  COMMON_OUTPUT_ID       NUMBER,
  PROCESS_ID             NUMBER,
  INV_CAT_SEG1           VARCHAR2(240 BYTE),
  ON_ORD                 NUMBER,
  BPA_COST               NUMBER,
  OPEN_REQ               NUMBER,
  WT                     NUMBER,
  FML                    NUMBER,
  SOURCING_RULE          VARCHAR2(450 BYTE),
  SO                     NUMBER,
  SS                     NUMBER,
  CLT                    NUMBER,
  AVAIL2                 NUMBER,
  INT_REQ                NUMBER,
  DIR_REQ                NUMBER,
  DEMAND                 NUMBER,
  ITEM_STATUS_CODE       VARCHAR2(50 BYTE),
  SITE_VENDOR_NUM        VARCHAR2(240 BYTE),
  VENDOR_SITE            VARCHAR2(240 BYTE)
)
TABLESPACE XXEIS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX XXEIS.XXWC_PO_ISR_TAB_TOTAL_ITEMORG ON XXEIS.EIS_XXWC_PO_ISR_TAB_TOTAL
(INVENTORY_ITEM_ID, ORGANIZATION_ID)
TABLESPACE XXEIS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );
