--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_PUR_ACC_RPT_DATA_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_PURCHASE_ACC_RPT_PKG Package.
  HISTORY
  ===============================================================================
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        	Pramod   		TMS#20160412-00031  Performance Tuning
********************************************************************************/
DROP TABLE XXEIS.EIS_PUR_ACC_RPT_DATA_TAB CASCADE CONSTRAINTS;

CREATE TABLE XXEIS.EIS_PUR_ACC_RPT_DATA_TAB 
   (	PROCESS_ID NUMBER, 
	AGREEMENT_YEAR NUMBER, 
	PERIOD_ID NUMBER, 
	FISCAL_PERIOD VARCHAR2(15 BYTE), 
	MVID VARCHAR2(30 BYTE), 
	CUST_ID NUMBER, 
	VENDOR VARCHAR2(240 BYTE), 
	LOB VARCHAR2(150 BYTE), 
	BU VARCHAR2(150 BYTE), 
	LOB_BRANCH VARCHAR2(150 BYTE), 
	POSTED_FIN_LOCATION VARCHAR2(150 BYTE), 
	OFFER_NAME VARCHAR2(240 BYTE), 
	FISCAL_PURCHASES NUMBER, 
	ACCRUAL_PURCHASES NUMBER, 
	REBATE NUMBER, 
	COOP NUMBER, 
	TOTAL NUMBER
)
/
