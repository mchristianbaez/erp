---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL $
  Module Name : Order Management
  PURPOSE	  : Open Sales Orders Report
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  20-June-2016         Siva			  TMS#20160617-00018
  1.1     15-Mar-2017      	   Siva   		  TMS#20170224-00065
**************************************************************************************************************/
DROP TABLE XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL CASCADE CONSTRAINTS  --added for version 1.1
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL  --added for version 1.1
  (
    PROCESS_ID             NUMBER,
    HEADER_ID              NUMBER NOT NULL ENABLE,
    ORG_ID                 NUMBER,
    ORDER_TYPE_ID          NUMBER NOT NULL ENABLE,
    ORDER_NUMBER           NUMBER NOT NULL ENABLE,
    ORDERED_DATE           DATE,
    REQUEST_DATE           DATE,
    CREATION_DATE          DATE NOT NULL ENABLE,
    CREATED_BY             NUMBER NOT NULL ENABLE,
    CREATED_BY_NAME        VARCHAR2(200 BYTE),
    QUOTE_NUMBER           NUMBER,
    SOLD_TO_ORG_ID         NUMBER,
    PARTY_ID               NUMBER,
    CUST_ACCOUNT_ID        NUMBER,
    CUSTOMER_NUMBER        VARCHAR2(30 BYTE),
    CUSTOMER_NAME          VARCHAR2(300 BYTE),
    CUSTOMER_JOB_NAME      VARCHAR2(300 BYTE),
    CUST_ACCT_SITE_ID      NUMBER,
    SHIP_TO_CITY           VARCHAR2(300 BYTE),
    SHIP_TO_STE_ID         NUMBER,
    SHP_TO_PRT_ID          NUMBER,
    ZIP_CODE               VARCHAR2(300 BYTE),
    FLOW_STATUS_CODE       VARCHAR2(30 BYTE),
    TRANSACTION_PHASE_CODE VARCHAR2(30 BYTE),
    PAYMENT_TERM_ID        NUMBER(15,0),
    PAYMENT_TERMS          VARCHAR2(100 BYTE),
    SALESREP_ID            NUMBER(15,0),
    ORG_SALESREP_ID        NUMBER(15,0),
    SALES_PERSON_NAME      VARCHAR2(200 BYTE),
    SHIP_FROM_ORG_ID       NUMBER,
    SHIP_TO_ORG_ID         NUMBER
  )
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
