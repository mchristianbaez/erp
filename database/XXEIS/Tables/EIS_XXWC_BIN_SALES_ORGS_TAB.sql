---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_BIN_SALES_ORGS_TAB $
  Module Name : Inventory
  PURPOSE	  : Bin Location With Sales History
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  18-May-2016         Venu			  TMS#20160301-00067
  1.1     15-Mar-2017      	  Siva   		  TMS#20170224-00065 
**************************************************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_BIN_SALES_ORGS_TAB CASCADE CONSTRAINTS  --added for version 1.1
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_BIN_SALES_ORGS_TAB  --added for version 1.1
  (
    PROCESS_ID        NUMBER,
    INVENTORY_ITEM_ID NUMBER,
    ORGANIZATION_ID   NUMBER
  )
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
