CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_VENDOR_QUOTE_TEMP1
(
  NAME                   VARCHAR2(4000 BYTE),
  SALESPERSON_NAME       VARCHAR2(4000 BYTE),
  SALESPESON_NUMBER      VARCHAR2(4000 BYTE),
  MASTER_ACCOUNT_NAME    VARCHAR2(4000 BYTE),
  MASTER_ACCOUNT_NUMBER  VARCHAR2(4000 BYTE),
  JOB_NAME               VARCHAR2(4000 BYTE),
  ORGANIZATION_NAME      VARCHAR2(4000 BYTE),
  SOURCE_TYPE_CODE       VARCHAR2(4000 BYTE),
  JOB_NUMBER             VARCHAR2(4000 BYTE),
  VENDOR_NUMBER          VARCHAR2(4000 BYTE),
  VENDOR_NAME            VARCHAR2(4000 BYTE),
  ORACLE_QUOTE_NUMBER    VARCHAR2(4000 BYTE),
  INVOICE_NUMBER         VARCHAR2(4000 BYTE),
  INVOICE_DATE           DATE,
  PART_NUMBER            VARCHAR2(4000 BYTE),
  UOM                    VARCHAR2(4000 BYTE),
  DESCRIPTION            VARCHAR2(4000 BYTE),
  BPA                    NUMBER,
  PO_COST                NUMBER,
  AVERAGE_COST           NUMBER,
  SPECIAL_COST           NUMBER,
  UNIT_CLAIM_VALUE       NUMBER,
  REBATE                 NUMBER,
  GL_CODING              VARCHAR2(4000 BYTE),
  GL_STRING              VARCHAR2(4000 BYTE),
  LOC                    VARCHAR2(4000 BYTE),
  CREATED_BY             VARCHAR2(4000 BYTE),
  CUSTOMER_TRX_ID        NUMBER,
  ORDER_NUMBER           NUMBER,
  LINE_NUMBER            NUMBER,
  CREATION_DATE          DATE,
  LOCATION               VARCHAR2(4000 BYTE),
  QTY                    NUMBER,
  PROCESS_ID             NUMBER,
  COMMON_OUTPUT_ID       NUMBER,
  LIST_PRICE_PER_UNIT    NUMBER, --added for TMS#20160804-00030
  PO_TRANS_COST 		 NUMBER  --added for TMS#20170920-00108 
)
ON COMMIT PRESERVE ROWS
RESULT_CACHE (MODE DEFAULT)
NOCACHE;
