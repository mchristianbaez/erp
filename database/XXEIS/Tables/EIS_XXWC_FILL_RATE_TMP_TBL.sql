BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE XXEIS.EIS_XXWC_FILL_RATE_TMP_TBL';
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

CREATE GLOBAL TEMPORARY TABLE xxeis.eis_xxwc_fill_rate_tmp_tbl
(
   process_id   NUMBER
  ,header_id    NUMBER
) ON COMMIT PRESERVE ROWS;

CREATE INDEX xxeis.eis_xxwc_fill_rate_tmp_tbl_n1
   ON xxeis.eis_xxwc_fill_rate_tmp_tbl (process_id, header_id);