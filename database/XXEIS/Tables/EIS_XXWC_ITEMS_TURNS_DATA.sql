--------------------------------------------------------
--  File created - Thursday-April-25-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table EIS_XXWC_ITEMS_TURNS_DATA
--------------------------------------------------------

  CREATE TABLE "XXEIS"."EIS_XXWC_ITEMS_TURNS_DATA" 
   (	"INVENTORY_ITEM_ID" NUMBER, 
	"ORGANIZATION_ID" NUMBER, 
	"COGS_AMT" NUMBER, 
	"AVG_INV" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 131072 NEXT 131072 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "XXEIS_DATA" ;
REM INSERTING into XXEIS.EIS_XXWC_ITEMS_TURNS_DATA
SET DEFINE OFF;


