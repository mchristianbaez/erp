---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_BIN_LOC_DATA_TAB $
  Module Name : Inventory
  PURPOSE	  : Bin Location
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  18-May-2016         Siva			  TMS#20160429-00037
  1.1     15-Mar-2017      	  Siva   		 TMS#20170224-00065
  1.2	  13-Feb-2018		  Siva  		 TMS#20180125-00274
  1.3	   4-Jul-2018		  Siva			 TMS#20180619-00079
**************************************************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_BIN_LOC_DATA_TAB CASCADE CONSTRAINTS  --added for version 1.1
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_BIN_LOC_DATA_TAB --added for version 1.1
  (
    PROCESS_ID          NUMBER,
    ORGANIZATION        VARCHAR2(3 BYTE),
    PART_NUMBER         VARCHAR2(40 BYTE),
    DESCRIPTION         VARCHAR2(240 BYTE),
    UOM                 VARCHAR2(3 BYTE),
    SELLING_PRICE       NUMBER,
    VENDOR_NAME         VARCHAR2(240 BYTE),
    VENDOR_NUMBER       VARCHAR2(240 BYTE),
    MIN_MINMAX_QUANTITY NUMBER,
    MAX_MINMAX_QUANTITY NUMBER,
    AVERAGECOST         NUMBER,
    WEIGHT              NUMBER,
    CAT                 VARCHAR2(481 BYTE),
    ONHAND              NUMBER,
    MTD_SALES           VARCHAR2(200 BYTE),
    YTD_SALES           VARCHAR2(200 BYTE),
    PRIMARY_BIN_LOC     VARCHAR2(4000 BYTE),
    NO_BIN              VARCHAR2(3 BYTE),
    ALTERNATE_BIN_LOC   VARCHAR2(4000 BYTE),
    BIN_LOC             VARCHAR2(114 BYTE),
    LOCATION            VARCHAR2(240 BYTE),
    STK                 VARCHAR2(2 BYTE),
    OPEN_ORDER          VARCHAR2(2 BYTE),
    OPEN_DEMAND         VARCHAR2(2 BYTE),
    CATEGORY            VARCHAR2(163 BYTE),
    CATEGORY_SET_NAME   VARCHAR2(30 BYTE),
    INVENTORY_ITEM_ID   NUMBER,
    INV_ORGANIZATION_ID NUMBER,
    ORG_ORGANIZATION_ID NUMBER,
    APPLICATION_ID      NUMBER,
    SET_OF_BOOKS_ID     NUMBER,
    LAST_RECEIVED_DATE  DATE,
	SUBINVENTORY 		VARCHAR2(100),   --added for version 1.1
	BIN0                VARCHAR2(4000 BYTE),	--added for version 1.3
    BIN1                VARCHAR2(4000 BYTE),	--added for version 1.3
    BIN2                VARCHAR2(4000 BYTE),	--added for version 1.3
    BIN3                VARCHAR2(4000 BYTE)	--added for version 1.3
  )
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
