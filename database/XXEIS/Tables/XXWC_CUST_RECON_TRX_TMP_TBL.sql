---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_CUST_RECON_TRX_TMP_TBL $
  Description	  : This table is used to get data from XXEIS.EIS_XXWC_CUST_ACC_RECON_PKG Package.
  REVISIONS   :
  VERSION 		  DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     		03-Oct-2016       		Siva   		 TMS#20130909-00776 
**************************************************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_CUST_RECON_TRX_TMP_TBL
  (
    CUSTOMER_NAME           VARCHAR2(360),
    CUSTOMER_NUMBER         VARCHAR2(50),
    PHONE_NUMBER            VARCHAR2(240),
    CREDIT_MANAGER          VARCHAR2(240),
    ACCOUNT_MANAGER         VARCHAR2(360),
    PAYMENT_TERMS           VARCHAR2(30),
    ACCOUNT_STATUS          VARCHAR2(240),
    PARTY_SITE_NUMBER       VARCHAR2(30),
    LOCATION                VARCHAR2(50),
    TRANSACTION_NUMBER      VARCHAR2(30),
    TRX_DATE                DATE,
    PO_NUMBER               VARCHAR2(50),
    TRX_AMOUNT_DUE_ORIGINAL NUMBER,
    TRANSACTION_BALANCE_DUE NUMBER,
    TAX_BALANCE_DUE         NUMBER,
    EARNED_DISCOUNT         NUMBER,
    DAYS_LATE               NUMBER,
    TRX_PAYMENT_TERMS       VARCHAR2(30),
    APPLIED_TRX_TYPE        VARCHAR2(240),
    PAYMENT_METHOD          VARCHAR2(240),
    APPLIED_TRX_NUMBER      VARCHAR2(240),
    APPLIED_AMOUNT          NUMBER,
    STATE                   VARCHAR2(240),
    ACTIVITY_DATE           DATE,
    CREATED_BY              VARCHAR2(240),
    REFERENCE               VARCHAR2(240)
  )
  ON COMMIT PRESERVE ROWS
/
