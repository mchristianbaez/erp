--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_HEADER_HOLDS_TBL
  Description: This table is used to get data from XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG Package.
  HISTORY
  ===============================================================================
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0        01-Jan-2018  			Siva       	Initial Version --20171213-00051 
********************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_HEADER_HOLDS_TBL
  (
    HOLD_SOURCE_ID   NUMBER,
    HEADER_HOLD_DATE DATE,
    HEADER_ID        NUMBER
  ) ON COMMIT PRESERVE ROWS
/
