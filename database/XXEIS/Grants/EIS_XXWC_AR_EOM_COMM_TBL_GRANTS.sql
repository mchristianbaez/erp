--------------------------------------------------------------------------------------
/*******************************************************************************
  Grant : EIS_XXWC_AR_EOM_COMM_TBL_GRANTS.sql
  Description: To grant permissions on EIS_XXWC_AR_EOM_COMM_TBL table
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11/08/2016          P.vamshidhar   TMS#20161102-00266 -  Automation of "White Cap EOM Commissions Report"
********************************************************************************/
GRANT SELECT ON XXEIS.EIS_XXWC_AR_EOM_COMM_TBL TO INTERFACE_APEXWC;
/
--Run this file in XXEIS user