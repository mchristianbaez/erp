--Run this file in XXEIS user
-----------------------------------------------------------------------------------------------------------------------------
/*********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION 		DATE          		AUTHOR(S)       DESCRIPTION
-- ------- -----------   	--------------- 	------------------------------------------------------
--	1.0      19-Jul-2016       		 Siva   		 TMS#20160708-00110  --grant to apps                                     
***********************************************************************************************/
GRANT EXECUTE,DEBUG ON XXEIS.EIS_RS_XXWC_EOM_COMM_REP_PKG TO APPS
/
