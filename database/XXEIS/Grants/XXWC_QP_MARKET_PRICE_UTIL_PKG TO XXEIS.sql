-----------------------------------------------------------------------------------------------------------------------------
/*********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION   DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     05/22/2017       Siva	  		 --TMS#20170317-00082 --grant to xxeis                                     
***********************************************************************************************/
GRANT EXECUTE,DEBUG ON APPS.XXWC_QP_MARKET_PRICE_UTIL_PKG TO XXEIS
/
