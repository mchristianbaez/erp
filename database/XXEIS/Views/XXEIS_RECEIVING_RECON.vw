CREATE OR REPLACE FORCE VIEW xxeis.xxeis_receiving_recon
(
   organization_code
  ,segment1
  ,po_unit_price
  ,receiving_qty
  ,inventory_qty
  ,expense_qty
  ,balance
  ,ext_cost
)
AS
     SELECT b.organization_code
           ,b.segment1
           ,b.po_unit_price
           ,b.receiving_qty
           ,b.inventory_qty
           ,b.expense_qty
           ,b.balance
           ,b.balance * b.po_unit_price ext_cost
       FROM (  SELECT a.organization_code
                     ,a.segment1
                     ,a.po_unit_price
                     ,SUM (a.receiving_qty) receiving_qty
                     ,SUM (a.inventory_qty) inventory_qty
                     ,SUM (a.expense_qty) expense_qty
                     ,SUM (a.receiving_qty) - SUM (a.inventory_qty) - SUM (a.expense_qty) balance
                 FROM (  SELECT mp.organization_code
                               ,msib.segment1
                               ,SUM (rt.primary_quantity) receiving_qty
                               ,0 inventory_qty
                               ,0 expense_qty
                               ,rt.po_unit_price
                           FROM rcv_transactions rt
                               ,po_lines_all pla
                               ,mtl_system_items_b msib
                               ,mtl_parameters mp
                          WHERE     rt.po_line_id = pla.po_line_id
                                AND pla.item_id = msib.inventory_item_id
                                AND rt.organization_id = msib.organization_id
                                AND msib.organization_id = mp.organization_id
                                /*AND mp.organization_code =
                                       NVL (xxeis.xxeis_fin_com_util_pkg.get_organization_code
                                           ,mp.organization_code)*/
                                -- Commented for TMS#20140619-00144 by Mahesh on 15/07/14
                                AND msib.segment1 = NVL (xxeis.xxeis_fin_com_util_pkg.get_segment1, msib.segment1)
                                AND rt.destination_type_code = 'RECEIVING'
                                AND EXISTS
                                       (SELECT *
                                          FROM org_acct_periods oap
                                         WHERE     oap.period_name =
                                                      NVL (xxeis.xxeis_fin_com_util_pkg.get_period_name
                                                          ,oap.period_name)
                                               AND oap.organization_id = rt.organization_id
                                               AND rt.transaction_date BETWEEN oap.period_start_date
                                                                           AND oap.period_close_date)
                       GROUP BY mp.organization_code
                               ,msib.segment1
                               ,rt.destination_type_code
                               ,rt.po_unit_price
                       UNION
                         SELECT mp.organization_code
                               ,msib.segment1
                               ,0
                               ,SUM (rt.primary_quantity) inventory_qty
                               ,0 expense_qty
                               ,rt.po_unit_price
                           FROM rcv_transactions rt
                               ,po_lines_all pla
                               ,mtl_system_items_b msib
                               ,mtl_parameters mp
                          WHERE     rt.po_line_id = pla.po_line_id
                                AND pla.item_id = msib.inventory_item_id
                                AND rt.organization_id = msib.organization_id
                                AND msib.organization_id = mp.organization_id
                                /*AND mp.organization_code =
                                       NVL (xxeis.xxeis_fin_com_util_pkg.get_organization_code
                                           ,mp.organization_code)*/
                                -- Commented for TMS#20140619-00144 by Mahesh on 15/07/14
                                AND msib.segment1 = NVL (xxeis.xxeis_fin_com_util_pkg.get_segment1, msib.segment1)
                                AND rt.destination_type_code = 'INVENTORY'
                                AND EXISTS
                                       (SELECT *
                                          FROM org_acct_periods oap
                                         WHERE     oap.period_name =
                                                      NVL (xxeis.xxeis_fin_com_util_pkg.get_period_name
                                                          ,oap.period_name)
                                               AND oap.organization_id = rt.organization_id
                                               AND rt.transaction_date BETWEEN oap.period_start_date
                                                                           AND oap.period_close_date)
                       GROUP BY mp.organization_code
                               ,msib.segment1
                               ,rt.destination_type_code
                               ,rt.po_unit_price
                       UNION
                         SELECT mp.organization_code
                               ,msib.segment1
                               ,0
                               ,SUM (rt.primary_quantity) inventory_qty
                               ,0 expense_qty
                               ,rt.po_unit_price
                           FROM rcv_transactions rt
                               ,po_lines_all pla
                               ,mtl_system_items_b msib
                               ,mtl_parameters mp
                          WHERE     rt.po_line_id = pla.po_line_id
                                AND pla.item_id = msib.inventory_item_id
                                AND rt.organization_id = msib.organization_id
                                AND msib.organization_id = mp.organization_id
                                /*AND mp.organization_code =
                                       NVL (xxeis.xxeis_fin_com_util_pkg.get_organization_code
                                           ,mp.organization_code)*/
                                -- Commented for TMS#20140619-00144 by Mahesh on 15/07/14
                                AND msib.segment1 = NVL (xxeis.xxeis_fin_com_util_pkg.get_segment1, msib.segment1)
                                AND rt.destination_type_code = 'EXPENSE'
                                AND EXISTS
                                       (SELECT *
                                          FROM org_acct_periods oap
                                         WHERE     oap.period_name =
                                                      NVL (xxeis.xxeis_fin_com_util_pkg.get_period_name
                                                          ,oap.period_name)
                                               AND oap.organization_id = rt.organization_id
                                               AND rt.transaction_date BETWEEN oap.period_start_date
                                                                           AND oap.period_close_date)
                       GROUP BY mp.organization_code
                               ,msib.segment1
                               ,rt.destination_type_code
                               ,rt.po_unit_price) a
             GROUP BY a.organization_code, a.segment1, a.po_unit_price) b
      WHERE b.balance <> 0
   ORDER BY b.organization_code, b.segment1;