/* ***************************************************************
  $Header XXEIS.EIS_WC_OM_ORD_HLD_V.vw $
  Module Name: Order Management
  PURPOSE: Orders on Hold Report - WC
  TMS Task Id : 20151104-00031
  REVISIONS: Initial Version
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        02/12/2016  Manjula/Mahender Reddy        Initial Version
*************************************************************** */
CREATE OR REPLACE VIEW xxeis.eis_wc_om_ord_hld_v
AS
  SELECT ol.customer_number,
    ol.customer_name,
    ol.loc,
    ol.sales_order_number,
    ol.created_by,
    ol.order_header_status,
    ol.order_type,
    ol.ordered_date,
    ol.order_line_status,
    ol.order_line_number,
    ol.line_type,
    DECODE(ol.order_header_hold_name,NULL,'No','Yes') order_header_hold,
    ol.order_header_hold_name,
    DECODE(ol.order_line_hold_name,NULL,'No','Yes') order_line_hold,
    ol.order_line_hold_name,
    NVL(ol.header_hold_date,ol.line_hold_date) hold_date,
    ol.part_number,
    ol.part_description,
    ol.quantity,
    ol.unit_selling_price,
    (ol.quantity * unit_selling_price) extended_price,
    (
    CASE
      WHEN NVL (ol.unit_cost, 0) = 0
      THEN 100
      WHEN ol.unit_selling_price = 0
      THEN 0
      WHEN ol.quantity = 0
      THEN 0
      ELSE ( ( ol.quantity * ol.unit_selling_price - ol.quantity * NVL (ol.unit_cost, 0)) / (ol.quantity * ol.unit_selling_price) * 100)
    END) margin_per,
    ol.unit_cost,
    (NVL (ol.unit_cost, 0)              * ol.quantity) extended_cost,
    NVL((NVL (ol.unit_selling_price, 0) * ol.quantity), 0) - (NVL ( (NVL (ol.unit_cost, 0) * ol.quantity), 0)) gm$,
    ol.special_vqn_cost,
    (
    CASE
      WHEN ol.special_vqn_cost IS NULL
      THEN NULL
      WHEN (NVL((ol.quantity * ol.unit_selling_price),0) !=0
      AND NVL(ol.special_vqn_cost,0)                     !=0)
      THEN ((( (ol.quantity * ol.unit_selling_price)-(ol.special_vqn_cost * ol.quantity))/(ol.quantity * ol.unit_selling_price))*100)
      WHEN NVL(ol.special_vqn_cost,0) =0
      THEN 100
      WHEN NVL((ol.quantity * ol.unit_selling_price),0) != 0
      THEN (((ol.quantity   * ol.unit_selling_price)-(ol.special_vqn_cost * ol.quantity))/(ol.quantity * ol.unit_selling_price)*100)
      ELSE 0
    END )adjust_gm_per,
    ol.vqn_modifier_name
    --------
  FROM
    (SELECT hca.account_number customer_number ,
      NVL (hca.account_name, hzp.party_name) customer_name,
      (SELECT organization_code
      FROM mtl_parameters
      WHERE organization_id = ol.ship_from_org_id
      ) loc,
      oh.order_number sales_order_number,
      (SELECT full_name
      FROM per_all_people_f
      WHERE person_id = fu.employee_id
      AND rownum      =1
      ) created_by,
      (SELECT meaning
      FROM fnd_lookup_values
      WHERE lookup_type ='FLOW_STATUS'
      AND lookup_code   = oh.flow_status_code
      ) order_header_status ,
      (SELECT description
      FROM oe_transaction_types_tl
      WHERE transaction_type_id = oh.order_type_id
      ) order_type,
      TRUNC (oh.ordered_date) ordered_date ,
      (SELECT meaning
      FROM fnd_lookup_values
      WHERE lookup_type ='LINE_FLOW_STATUS'
      AND lookup_code   = ol.flow_status_code
      ) order_line_status ,
      DECODE (ol.option_number,'',(ol.line_number
      ||'.'
      ||ol.shipment_number),(ol.line_number
      ||'.'
      ||ol.shipment_number
      ||'.'
      ||ol.option_number)) order_line_number,
      (SELECT description
      FROM oe_transaction_types_tl
      WHERE transaction_type_id = ol.line_type_id
      ) line_type,
      (SELECT DECODE(COUNT(DISTINCT ohd.hold_id), 0 , NULL , 1 , MIN(ohd.name),'Multiple Holds Exist') header_hold
      FROM oe_order_holds hold,
        oe_hold_sources ohs,
        oe_hold_definitions ohd
      WHERE hold.header_id      = oh.header_id
      AND hold.line_id         IS NULL
      AND hold.hold_source_id   =ohs.hold_source_id
      AND hold.hold_release_id IS NULL
      AND ohd.name             <>'XXWC_PRICE_CHANGE_HOLD'
      AND ohs.hold_id           =ohd.hold_id
      GROUP BY hold.header_id,
        hold.line_id
      ) order_header_hold_name ,
      (SELECT DECODE(COUNT(DISTINCT ohd.hold_id), 0 , NULL , 1 , MIN(ohd.name),'Multiple Holds Exist') line_hold
      FROM oe_order_holds hold,
        oe_hold_sources ohs,
        oe_hold_definitions ohd
      WHERE hold.header_id      = oh.header_id
      AND hold.line_id          = ol.line_id
      AND hold.hold_source_id   =ohs.hold_source_id
      AND hold.hold_release_id IS NULL
      AND ohd.name             <>'XXWC_PRICE_CHANGE_HOLD'
      AND ohs.hold_id           =ohd.hold_id
      GROUP BY hold.header_id,
        hold.line_id
      ) order_line_hold_name,
      (SELECT MAX(hold.creation_date) header_hold_date
      FROM oe_order_holds hold,
        oe_hold_sources ohs,
        oe_hold_definitions ohd
      WHERE hold.header_id      = oh.header_id
      AND hold.line_id         IS NULL
      AND hold.hold_source_id   =ohs.hold_source_id
      AND hold.hold_release_id IS NULL
      AND ohd.name             <>'XXWC_PRICE_CHANGE_HOLD'
      AND ohs.hold_id           =ohd.hold_id
      GROUP BY hold.header_id
      ) header_hold_date,
      hold.line_hold_date ,
      msi.segment1 part_number,
      msi.description part_description,
      DECODE(ol.line_category_code,'RETURN',ol.ordered_quantity *-1,ol.ordered_quantity) quantity,
      NVL(ol.unit_selling_price,0) unit_selling_price,
      (
      CASE
        WHEN ((oh.order_type_id  = 1004
        AND ol.flow_status_code  ='CLOSED')
        OR (oh.order_type_id    != 1004
        AND ol.flow_status_code IN ('CLOSED','INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')))
        THEN NVL(
          (SELECT actual_cost
          FROM
            (SELECT NVL(mmt.transaction_cost, mmt.actual_cost) actual_cost,
              ol1.line_id
            FROM mtl_material_transactions mmt ,
              mtl_transaction_types mtt ,
              oe_order_lines_all ol1 ,
              oe_order_headers_all oh ,
              oe_transaction_types_tl ott
            WHERE mmt.transaction_type_id  = mtt.transaction_type_id
            AND mtt.transaction_type_name IN ('RMA Receipt','RMA Return','Sales Order Pick','Sales order issue')
            AND ol1.line_category_code     = 'ORDER'
            AND ol1.header_id              = oh.header_id
            AND oh.order_type_id           = ott.transaction_type_id
            AND ott.name NOT              IN ('COUNTER LINE' ,'COUNTER ORDER' ,'REPAIR ORDER')
            AND ott.language               = 'US'
            AND ol1.line_id                = mmt.trx_source_line_id
            AND ol1.line_id                = mmt.source_line_id
            AND mmt.source_code            = 'ORDER ENTRY'
            
            UNION
            
            SELECT NVL(mmt.transaction_cost, mmt.actual_cost) actual_cost,
              ol1.line_id
            FROM mtl_material_transactions mmt ,
              mtl_transaction_types mtt ,
              oe_order_lines_all ol1 ,
              oe_order_headers_all oh ,
              oe_transaction_types_tl ott
            WHERE mmt.transaction_type_id  = mtt.transaction_type_id
            AND mtt.transaction_type_name IN ('RMA Receipt','RMA Return','Sales Order Pick','Sales order issue')
            AND ol1.line_category_code     = 'ORDER'
            AND ol1.header_id              = oh.header_id
            AND oh.order_type_id           = ott.transaction_type_id
            AND ott.name                  IN ('COUNTER LINE' ,'COUNTER ORDER' ,'REPAIR ORDER')
            AND ott.language               = 'US'
            AND ol1.line_id                = mmt.trx_source_line_id
            
            UNION
            
            SELECT NVL(mmt.transaction_cost, mmt.actual_cost) actual_cost,
              ol1.line_id
            FROM mtl_material_transactions mmt ,
              mtl_transaction_types mtt ,
              oe_order_lines_all ol1
            WHERE mmt.transaction_type_id  = mtt.transaction_type_id
            AND mtt.transaction_type_name IN ('RMA Receipt','RMA Return','Sales Order Pick','Sales order issue')
            AND mmt.trx_source_line_id     = ol1.line_id
            AND ol1.line_category_code     = 'RETURN'
            ) x1
          WHERE x1.line_id = ol.line_id
          AND rownum       = 1
          ) ,0)
        ELSE NVL(
          (SELECT CIC.item_cost
          FROM cst_item_costs CIC
          WHERE CIC.inventory_item_id = msi.inventory_item_id
          AND cic.organization_id     = msi.organization_id
          AND cost_type_id            = 2
          ), 0)
      END) unit_cost,
      (SELECT xxcpl.special_cost
      FROM apps.oe_price_adjustments adj,
        apps.qp_list_lines qll,
        apps.qp_list_headers qlh,
        apps.xxwc_om_contract_pricing_hdr xxcph,
        apps.xxwc_om_contract_pricing_lines xxcpl
      WHERE xxcph.agreement_id    = TO_CHAR(qlh.attribute14)
      AND xxcpl.agreement_id      = xxcph.agreement_id
      AND xxcpl.agreement_type    = 'VQN'
      AND qlh.list_header_id      = qll.list_header_id
      AND qlh.list_header_id      = adj.list_header_id
      AND qll.list_line_id        = adj.list_line_id
      AND adj.list_line_type_code = 'DIS'
      AND adj.applied_flag        = 'Y'
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND xxcpl.product_value     = ol.ordered_item
      AND xxcpl.agreement_line_id = TO_CHAR(qll.attribute2)
      AND rownum                  = 1
      ) special_vqn_cost,
      (SELECT qlh.name
      FROM apps.oe_price_adjustments adj,
        apps.qp_list_lines qll,
        apps.qp_list_headers qlh,
        apps.xxwc_om_contract_pricing_hdr xxcph,
        apps.xxwc_om_contract_pricing_lines xxcpl
      WHERE xxcph.agreement_id    = TO_CHAR(qlh.attribute14)
      AND xxcpl.agreement_id      = xxcph.agreement_id
      AND xxcpl.agreement_type    = 'VQN'
      AND qlh.list_header_id      = qll.list_header_id
      AND qlh.list_header_id      = adj.list_header_id
      AND qll.list_line_id        = adj.list_line_id
      AND adj.list_line_type_code = 'DIS'
      AND adj.applied_flag        = 'Y'
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND xxcpl.product_value     = ol.ordered_item
      AND xxcpl.agreement_line_id = TO_CHAR(qll.attribute2)
      AND rownum                  = 1
      )vqn_modifier_name
    FROM
      (SELECT MAX(a.hold_source_id) hold_source_id,
        MAX(a.creation_date) line_hold_date,
        a.line_id,
        a.header_id
      FROM oe_order_holds a,
        oe_hold_sources b,
        oe_hold_definitions c
      WHERE a.line_id       IS NOT NULL
      AND a.hold_release_id IS NULL
      AND a.hold_source_id   = b.hold_source_id
      AND b.hold_id          = c.hold_id
      AND c.name            <>'XXWC_PRICE_CHANGE_HOLD'
      GROUP BY a.header_id,
        a.line_id
      ) hold,
      oe_hold_sources ohs,
      oe_hold_definitions ohd,
      oe_order_lines ol ,
      oe_order_headers oh,
      hz_cust_accounts hca ,
      hz_parties hzp,
      fnd_user fu,
      mtl_system_items_b msi
    WHERE 1 =1
      -- AND oh.order_number     IN ('17022319')
    AND hold.hold_source_id  =ohs.hold_source_id
    AND ohd.name            <> 'XXWC_PRICE_CHANGE_HOLD'
    AND ohs.hold_id          =ohd.hold_id
    AND hold.line_id         = ol.line_id
    AND ol.flow_status_code <>'CLOSED'
    AND ol.header_id         = oh.header_id
    AND oh.sold_to_org_id    = hca.cust_account_id
    AND hca.party_id         = hzp.party_id
    AND oh.created_by        = fu.user_id
    AND ol.ship_from_org_id  = msi.organization_id
    AND ol.inventory_item_id = msi.inventory_item_id
    
    UNION
    
    SELECT hca.account_number customer_number ,
      NVL (hca.account_name, hzp.party_name) customer_name,
      (SELECT organization_code
      FROM mtl_parameters
      WHERE organization_id = ol.ship_from_org_id
      ) loc,
      oh.order_number sales_order_number,
      (SELECT full_name
      FROM per_all_people_f
      WHERE person_id = fu.employee_id
      AND rownum      =1
      ) created_by,
      (SELECT meaning
      FROM fnd_lookup_values
      WHERE lookup_type ='FLOW_STATUS'
      AND lookup_code   = oh.flow_status_code
      ) order_header_status ,
      (SELECT description
      FROM oe_transaction_types_tl
      WHERE transaction_type_id = oh.order_type_id
      ) order_type,
      TRUNC (oh.ordered_date) ordered_date ,
      (SELECT meaning
      FROM fnd_lookup_values
      WHERE lookup_type ='LINE_FLOW_STATUS'
      AND lookup_code   = ol.flow_status_code
      ) order_line_status ,
      DECODE (ol.option_number,'',(ol.line_number
      ||'.'
      ||ol.shipment_number),(ol.line_number
      ||'.'
      ||ol.shipment_number
      ||'.'
      ||ol.option_number)) order_line_number,
      (SELECT description
      FROM oe_transaction_types_tl
      WHERE transaction_type_id = ol.line_type_id
      ) line_type,
      (SELECT DECODE(COUNT(DISTINCT ohd.hold_id), 0 , 'No Hold' , 1 , MIN(ohd.name),'Multiple Holds Exist') header_hold
      FROM oe_order_holds hold,
        oe_hold_sources ohs,
        oe_hold_definitions ohd
      WHERE hold.header_id      = oh.header_id
      AND hold.line_id         IS NULL
      AND hold.hold_source_id   =ohs.hold_source_id
      AND hold.hold_release_id IS NULL
      AND ohd.name             <>'XXWC_PRICE_CHANGE_HOLD'
      AND ohs.hold_id           =ohd.hold_id
      GROUP BY hold.header_id,
        hold.line_id
      ) order_header_hold_name ,
      NULL order_line_hold_name,
      hold.header_hold_date header_hold_date ,
      NULL line_hold_date,
      msi.segment1 part_number,
      msi.description part_description,
      DECODE(ol.line_category_code,'RETURN',ol.ordered_quantity *-1,ol.ordered_quantity) quantity,
      NVL(ol.unit_selling_price,0) unit_selling_price,
      (
      CASE
        WHEN ((oh.order_type_id  = 1004
        AND ol.flow_status_code  ='CLOSED')
        OR (oh.order_type_id    != 1004
        AND ol.flow_status_code IN ('CLOSED','INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')))
        THEN NVL(
          (SELECT actual_cost
          FROM
            (SELECT NVL(mmt.transaction_cost, mmt.actual_cost) actual_cost,
              ol1.line_id
            FROM mtl_material_transactions mmt ,
              mtl_transaction_types mtt ,
              oe_order_lines_all ol1 ,
              oe_order_headers_all oh ,
              oe_transaction_types_tl ott
            WHERE mmt.transaction_type_id  = mtt.transaction_type_id
            AND mtt.transaction_type_name IN ('RMA Receipt','RMA Return','Sales Order Pick','Sales order issue')
            AND ol1.line_category_code     = 'ORDER'
            AND ol1.header_id              = oh.header_id
            AND oh.order_type_id           = ott.transaction_type_id
            AND ott.name NOT              IN ('COUNTER LINE' ,'COUNTER ORDER' ,'REPAIR ORDER')
            AND ott.language               = 'US'
            AND ol1.line_id                = mmt.trx_source_line_id
            AND ol1.line_id                = mmt.source_line_id
            AND mmt.source_code            = 'ORDER ENTRY'
            
            UNION
            
            SELECT NVL(mmt.transaction_cost, mmt.actual_cost) actual_cost,
              ol1.line_id
            FROM mtl_material_transactions mmt ,
              mtl_transaction_types mtt ,
              oe_order_lines_all ol1 ,
              oe_order_headers_all oh ,
              oe_transaction_types_tl ott
            WHERE mmt.transaction_type_id  = mtt.transaction_type_id
            AND mtt.transaction_type_name IN ('RMA Receipt','RMA Return','Sales Order Pick','Sales order issue')
            AND ol1.line_category_code     = 'ORDER'
            AND ol1.header_id              = oh.header_id
            AND oh.order_type_id           = ott.transaction_type_id
            AND ott.name                  IN ('COUNTER LINE' ,'COUNTER ORDER' ,'REPAIR ORDER')
            AND ott.language               = 'US'
            AND ol1.line_id                = mmt.trx_source_line_id
            
            UNION
            
            SELECT NVL(mmt.transaction_cost, mmt.actual_cost) actual_cost,
              ol1.line_id
            FROM mtl_material_transactions mmt ,
              mtl_transaction_types mtt ,
              oe_order_lines_all ol1
            WHERE mmt.transaction_type_id  = mtt.transaction_type_id
            AND mtt.transaction_type_name IN ('RMA Receipt','RMA Return','Sales Order Pick','Sales order issue')
            AND mmt.trx_source_line_id     = ol1.line_id
            AND ol1.line_category_code     = 'RETURN'
            ) x1
          WHERE x1.line_id = ol.line_id
          AND rownum       = 1
          ) ,0)
        ELSE NVL(
          (SELECT CIC.item_cost
          FROM cst_item_costs CIC
          WHERE CIC.inventory_item_id = msi.inventory_item_id
          AND cic.organization_id     = msi.organization_id
          AND cost_type_id            = 2
          ), 0)
      END) unit_cost,
      (SELECT xxcpl.special_cost
      FROM apps.oe_price_adjustments adj,
        apps.qp_list_lines qll,
        apps.qp_list_headers qlh,
        apps.xxwc_om_contract_pricing_hdr xxcph,
        apps.xxwc_om_contract_pricing_lines xxcpl
      WHERE xxcph.agreement_id    = TO_CHAR(qlh.attribute14)
      AND xxcpl.agreement_id      = xxcph.agreement_id
      AND xxcpl.agreement_type    = 'VQN'
      AND qlh.list_header_id      = qll.list_header_id
      AND qlh.list_header_id      = adj.list_header_id
      AND qll.list_line_id        = adj.list_line_id
      AND adj.list_line_type_code = 'DIS'
      AND adj.applied_flag        = 'Y'
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND xxcpl.product_value     = ol.ordered_item
      AND xxcpl.agreement_line_id = TO_CHAR(qll.attribute2)
      AND rownum                  = 1
      ) special_vqn_cost,
      (SELECT qlh.name
      FROM apps.oe_price_adjustments adj,
        apps.qp_list_lines qll,
        apps.qp_list_headers qlh,
        apps.xxwc_om_contract_pricing_hdr xxcph,
        apps.xxwc_om_contract_pricing_lines xxcpl
      WHERE xxcph.agreement_id    = TO_CHAR(qlh.attribute14)
      AND xxcpl.agreement_id      = xxcph.agreement_id
      AND xxcpl.agreement_type    = 'VQN'
      AND qlh.list_header_id      = qll.list_header_id
      AND qlh.list_header_id      = adj.list_header_id
      AND qll.list_line_id        = adj.list_line_id
      AND adj.list_line_type_code = 'DIS'
      AND adj.applied_flag        = 'Y'
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND xxcpl.product_value     = ol.ordered_item
      AND xxcpl.agreement_line_id = TO_CHAR(qll.attribute2)
      AND rownum                  = 1
      )vqn_modifier_name
    FROM
      (SELECT MAX(hold_source_id) hold_source_id,
        TRUNC(MAX(creation_date)) header_hold_date,
        header_id
      FROM oe_order_holds
      WHERE line_id       IS NULL
      AND hold_release_id IS NULL
      GROUP BY header_id
      ) hold,
      oe_hold_sources ohs,
      oe_hold_definitions ohd,
      oe_order_lines ol,
      oe_order_headers oh,
      hz_cust_accounts hca ,
      hz_parties hzp,
      fnd_user fu,
      mtl_system_items_b msi
    WHERE 1 =1
      --AND oh.order_number ='17022319'
    AND NOT EXISTS
      (SELECT 1
      FROM oe_order_holds a ,
        oe_hold_sources b,
        oe_hold_definitions c
      WHERE a.header_id      = hold.header_id
      AND a.line_id          = ol.line_id
      AND a.hold_source_id   = b.hold_source_id
      AND b.hold_id          = c.hold_id
      AND c.name            <>'XXWC_PRICE_CHANGE_HOLD'
      AND a.hold_release_id IS NULL
      )
    AND hold.hold_source_id  =ohs.hold_source_id
    AND ohs.hold_id          =ohd.hold_id
    AND ol.header_id         = hold.header_id
    AND oh.header_id         = ol.header_id
    AND ol.flow_status_code <>'CLOSED'
    AND oh.sold_to_org_id    = hca.cust_account_id
    AND hca.party_id         = hzp.party_id
    AND oh.created_by        = fu.user_id
    AND ol.ship_from_org_id  = msi.organization_id
    AND ol.inventory_item_id = msi.inventory_item_id
    ) ol 
/