CREATE OR REPLACE FORCE VIEW xxeis.line_count_by_journal_view
(
   je_header_id
  ,je_category
  ,je_source
  ,period_name
  ,name
  ,created_by
  ,preparer
  ,posted_by
  ,poster
  ,line_count
)
AS
   SELECT gjh.je_header_id
         ,gjh.je_category
         ,gjh.je_source
         ,gjh.period_name
         ,gjh.name
         ,gjh.created_by
         ,fnd.email_address preparer
         ,gjb.posted_by
         ,fnd2.email_address poster
         , (SELECT COUNT (*)
              FROM gl.gl_je_lines gjl
             WHERE gjh.je_header_id = gjl.je_header_id)
             line_count
     FROM gl.gl_je_headers gjh
         ,apps.fnd_user fnd
         ,gl.gl_je_batches gjb
         ,apps.fnd_user fnd2
    WHERE     gjh.created_by = fnd.user_id
          AND gjb.posted_by = fnd2.user_id
          AND gjh.je_batch_id = gjb.je_batch_id;


