---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_RENTAL_ITEMS_CAT_V $
  Module Name : OM
  PURPOSE	  : Rental Utilization and Usage by Category - WC
  TMS Task Id : 20160503-00221
  REVISIONS   :
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     05/05/2016       		Siva   		 TMS#20160503-00221 
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_OM_RENTAL_ITEMS_CAT_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_RENTAL_ITEMS_CAT_V (LOCATION, REGION, ITEM_NUMBER, ITEM_DESCRIPTION, AVERAGE_COST, ACTUAL_RENT_DAYS, QTY_ON_RENT, ON_HAND, DEMAND_QTY, VARIANC, OWNED, UTILIZATION_RATE, CATEGORY, CATEGORY_CLASS, SUB_CATEGORY, CAT_CLASS_DESCRIPTION, RENTAL_TYPE,DISTRICT)
AS
  SELECT a.location ,
    a.region ,
    a.item_number ,
    a.item_description ,
    NVL ( xxeis.eis_rs_xxwc_com_util_pkg.get_unit_cost (a.inventory_item_id ,a.organization_id ,a.item_number) ,0) average_cost
    --         ,qty_on_hand
    ,
    qty_on_rent actual_rent_days ,
    a.qty_on_rent ,
    on_hand ,
    demand_qty ,
    (on_hand     - demand_qty) varianc ,
    (qty_on_rent + on_hand) owned ,
    (QTY_ON_RENT * 100) / (DECODE ( (QTY_ON_RENT + ON_HAND), 0, 1, (QTY_ON_RENT + ON_HAND))) UTILIZATION_RATE ,
    XXEIS.EIS_XXWC_OM_RENTAL_PKG.GET_ITEM_CATEGORY (a.INVENTORY_ITEM_ID,a.ORGANIZATION_ID,'CATEGORY') category -- TMS#20160503-00221 on 05/05/2016
    ,
    XXEIS.EIS_XXWC_OM_RENTAL_PKG.GET_ITEM_CATEGORY (a.INVENTORY_ITEM_ID,a.ORGANIZATION_ID,'CATEGORY_CLASS') CATEGORY_CLASS -- TMS#20160503-00221 on 05/05/2016
    ,
    XXEIS.EIS_XXWC_OM_RENTAL_PKG.GET_ITEM_CATEGORY (A.INVENTORY_ITEM_ID,A.ORGANIZATION_ID,'SUB_CATEGORY') SUB_CATEGORY -- TMS#20160503-00221 on 05/05/2016
    ,
    XXEIS.EIS_XXWC_OM_RENTAL_PKG.GET_ITEM_CATEGORY (A.INVENTORY_ITEM_ID,A.ORGANIZATION_ID,'CAT_CLASS_DESC') CAT_CLASS_DESCRIPTION, -- TMS#20160503-00221 on 05/05/2016
    CASE
      WHEN A.ITEM_NUMBER LIKE 'RR%'
      THEN 'ReRent'
      ELSE 'Rental'
    END RENTAL_TYPE, -- Added for version 1.0
    A.District --Added for version 1.0
  FROM
    (SELECT mtl.organization_code location ,
      mtl.attribute9 region ,
      msi.concatenated_segments item_number ,
      msi.description item_description ,
      (SELECT NVL (SUM (ordered_quantity), 0)
      FROM oe_order_lines_all
      WHERE inventory_item_id = msi.inventory_item_id
      AND flow_status_code    = 'AWAITING_RETURN'
      AND ship_from_org_id    = msi.organization_id
      ) qty_on_rent ,
      msi.inventory_item_id ,
      msi.organization_id
      --                   ,NVL (SUM (moq.transaction_quantity), 0) qty_on_hand
      ,
      NVL (SUM (moq.transaction_quantity), 0) on_hand ,
      (SELECT NVL (open_sales_orders, 0)
      FROM apps.xxwc_inv_item_search_v
      WHERE inventory_item_id = msi.inventory_item_id
      and organization_id     = msi.organization_id
      ) demand_qty,
      mtl.attribute8 District --Added for version 1.0
    FROM mtl_system_items_b_kfv msi,
      mtl_parameters mtl,
      apps.mtl_onhand_quantities moq
    WHERE msi.organization_id = mtl.organization_id
    AND moq.inventory_item_id = msi.inventory_item_id
    AND msi.organization_id   = moq.organization_id
    AND MSI.SEGMENT1 LIKE 'R%' -- Added by Mahender reddy for TMS#20150403-00045 on 04/22/2015
    -- AND msi.segment1 NOT LIKE 'RR%'-- commented for version 1.0
    GROUP BY mtl.organization_code ,
      mtl.attribute9 ,
      msi.concatenated_segments ,
      msi.description ,
      msi.inventory_item_id ,
      msi.organization_id,
      mtl.attribute8 --Added for version 1.0
    --Below union Query added by Mahender Reddy for    20150624-00016 on 07/02/2015
    UNION
    SELECT -- OL.ORDERED_ITEM,OLRR.ATTRIBUTE12,OL.LINE_ID,RCTL.SALES_ORDER,
      mtl.organization_code location ,
      mtl.attribute9 region, --Added By Mahender for TMS# 20141006-00045
      msi.concatenated_segments item_number ,
      msi.description item_description ,
      (SELECT NVL (SUM (ordered_quantity), 0)
      FROM oe_order_lines_all
      WHERE inventory_item_id = msi.inventory_item_id
      AND flow_status_code    = 'AWAITING_RETURN'
      AND ship_from_org_id    = msi.organization_id
      ) qty_on_rent --Added By Mahender for TMS# 20141006-00045
      ,
      msi.inventory_item_id ,
      msi.organization_id ,
      NVL ( xxeis.eis_rs_xxwc_com_util_pkg.get_item_onhand_qty (msi.inventory_item_id ,msi.organization_id) ,0) on_hand ,
      (SELECT NVL (open_sales_orders, 0)
      FROM apps.xxwc_inv_item_search_v
      WHERE inventory_item_id = msi.inventory_item_id
      AND organization_id     = msi.organization_id
      ) demand_qty, --Added By Mahender for TMS# 20141006-00045
      mtl.attribute8 District --Added for version 1.0
    FROM apps.oe_order_lines_all ol ,
      apps.oe_order_lines_all olrr ,
      apps.ra_customer_trx_lines_all rctl ,
      apps.mtl_system_items_b_kfv msi ,
      apps.mtl_parameters mtl
    WHERE rctl.interface_line_context  = 'ORDER ENTRY'
    AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)
    AND ol.link_to_line_id             = olrr.line_id
    AND msi.inventory_item_id          = olrr.inventory_item_id
    AND msi.organization_id            = olrr.ship_from_org_id
    AND MTL.ORGANIZATION_ID            = MSI.ORGANIZATION_ID
    AND MSI.SEGMENT1 LIKE 'R%'
      -- AND msi.segment1 NOT LIKE 'RR%' -- commented for version 1.0
    AND EXISTS
      (SELECT 1
      FROM apps.oe_order_lines_all ol2
      WHERE ol2.ordered_item  = 'Rental Charge'
      AND ol2.link_to_line_id = olrr.line_id
      )
    GROUP BY mtl.organization_code ,
      mtl.attribute9 ,
      msi.concatenated_segments ,
      msi.description ,
      msi.inventory_item_id ,
      msi.organization_id,
      mtl.attribute8 --Added for version 1.0
    ) A
/
