CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_ar_aging_srce_cod_v
(
   bill_to_customer_name
  ,bill_to_customer_number
  ,outstanding_amount
  ,bucket_current
  ,bucket_1_to_30
  ,bucket_31_to_60
  ,bucket_61_to_90
  ,bucket_91_to_180
  ,bucket_181_to_360
  ,bucket_361_days_and_above
  ,profile_class
  ,credit_hold
  ,customer_account_status
  ,collector
  ,credit_analyst_name
  ,salesrep_number
  ,account_manager
  ,source_code
)
AS
     SELECT xacb.customer_name bill_to_customer_name
           ,xacb.customer_account_number bill_to_customer_number
           ,SUM (xacb.transaction_remaining_balance) outstanding_amount
           ,SUM (xacb.current_balance) bucket_current
           ,SUM (xacb.thirty_days_bal) bucket_1_to_30
           ,SUM (xacb.sixty_days_bal) bucket_31_to_60
           ,SUM (xacb.ninety_days_bal) bucket_61_to_90
           ,SUM (xacb.one_eighty_days_bal) bucket_91_to_180
           ,SUM (xacb.three_sixty_days_bal) bucket_181_to_360
           ,SUM (xacb.over_three_sixty_days_bal) bucket_361_days_and_above
           ,xacb.customer_profile_class profile_class
           ,xacb.account_credit_hold credit_hold
           ,xacb.customer_account_status
           ,xacb.collector_name collector
           ,xacb.credit_analyst credit_analyst_name
           ,xacb.salesrep_number salesrep_number
           ,xacb.account_manager account_manager
           ,hca.attribute4 source_code
       FROM hz_cust_accounts hca, xxwc_ar_customer_balance_mv xacb
      WHERE hca.cust_account_id(+) = xacb.cust_account_id
   GROUP BY customer_name
           ,customer_account_number
           ,customer_profile_class
           ,account_credit_hold
           ,customer_account_status
           ,collector_name
           ,credit_analyst
           ,salesrep_number
           ,account_manager
           ,attribute4;