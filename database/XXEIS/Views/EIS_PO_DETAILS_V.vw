CREATE OR REPLACE FORCE VIEW xxeis.eis_po_details_v
(
   po_header_id
  ,po_last_update_date
  ,po_last_updated_by
  ,po_creation_date
  ,po_created_by
  ,po_buyer_name
  ,po_number
  ,po_type
  ,po_supplier_name
  ,po_supplier_site_code
  ,supplier_contact_first_name
  ,supplier_contact_last_name
  ,supplier_contact_area_code
  ,supplier_contact_phone
  ,pvc_vendor_contact_id
  ,po_summary
  ,po_enabled
  ,po_start_date
  ,po_end_date
  ,po_ship_to_location
  ,po_terms
  ,po_ship_via
  ,po_fob
  ,po_freight_terms
  ,po_status
  ,po_revision_num
  ,po_revised_date
  ,po_printed_date
  ,po_approved_date
  ,po_currency_code
  ,po_acceptance_due_date
  ,po_on_hold
  ,po_acceptance_required
  ,po_note_to_authorizer
  ,po_note_to_supplier
  ,po_note_to_receiver
  ,po_comments
  ,po_closed_code
  ,po_closed_date
  ,po_line_id
  ,pol_last_update_date
  ,pol_last_updated_by
  ,pol_creation_date
  ,pol_created_by
  ,po_line_type
  ,po_line_num
  ,c_flex_item
  ,msi_padded_concat_segments
  ,po_item_number
  ,msi_segment2
  ,msi_segment3
  ,msi_segment4
  ,msi_segment5
  ,msi_segment6
  ,msi_segment7
  ,msi_segment8
  ,msi_segment9
  ,msi_segment10
  ,msi_segment11
  ,msi_segment12
  ,msi_segment13
  ,msi_segment14
  ,msi_segment15
  ,outside_operation_unit_type
  ,unit_of_issue
  ,parent_id
  ,expense_id
  ,processing_time
  ,receipt_tolerance
  ,price_tolerance
  ,invoicing_close_tolerance
  ,receiving_close_tolerance
  ,days_early_receipt_allowed
  ,list_price
  ,days_late_receipt_allowed
  ,market_price
  ,rounding_factor
  ,routing_steps
  ,po_item_description
  ,po_unit_price
  ,po_quantity_ordered
  ,po_quantity_cancelled
  ,po_quantity_received
  ,po_quantity_due
  ,po_quantity_billed
  ,line_amount
  ,shipment_amount
  ,po_item_revision
  ,po_uom
  ,po_list_price
  ,po_market_price
  ,pol_cancel
  ,pol_cancel_date
  ,pol_cancel_reason
  ,po_taxable_flag
  ,po_tax_name
  ,po_type_1099
  ,pol_closed_code
  ,pol_closed_date
  ,pol_closed_reason
  ,line_location_id
  ,po_shipment_num
  ,po_shipment_type
  ,po_need_by_date
  ,po_promised_date
  ,po_last_accept_date
  ,po_4_way_match
  ,po_3_way_match
  ,po_qty_rcv_tolerance
  ,po_days_early_receipt_allowed
  ,po_days_late_receipt_allowed
  ,po_invoice_close_tolerance
  ,po_receive_close_tolerance
  ,po_accrue_on_receipt
  ,po_distribution_id
  ,po_distribution_num
  ,po_set_of_books
  ,po_charge_ccid
  ,po_charge_account
  ,po_accrual_ccid
  ,po_accrual_account
  ,po_variance_ccid
  ,po_variance_account
  ,project_name
  ,project_number
  ,hrl_location_id
  ,sob_set_of_books_id
  ,project_id
  ,c_flex_cat
  ,mca_padded_concat_segments
  ,mca_structure_id
  ,mca_segment1
  ,mca_segment2
  ,mca_segment3
  ,mca_segment4
  ,mca_segment5
  ,mca_segment6
  ,mca_segment7
  ,mca_segment8
  ,mca_segment9
  ,mca_segment10
  ,mca_segment11
  ,mca_segment12
  ,mca_segment13
  ,mca_segment14
  ,mca_segment15
  ,status
  ,TYPE
  ,operating_unit
  ,org_id
  ,org_code
  ,vendor_id
  ,agent_id
  ,code_combination_id
  ,pvs_vendor_site_id
  ,plt_line_type_id
  ,mca_category_id
  ,msi_organization_id
  ,quantity_over_billed
  ,requestor_name
  ,gcc2#branch
  ,gcc1#branch
  ,gcc#branch
  ,mca#cogs_account
  ,mca#sales_account
  ,msi#hds#lob
  ,msi#hds#drop_shipment_eligab
  ,msi#hds#invoice_uom
  ,msi#hds#product_id
  ,msi#hds#vendor_part_number
  ,msi#hds#unspsc_code
  ,msi#hds#upc_primary
  ,msi#hds#sku_description
  ,msi#wc#ca_prop_65
  ,msi#wc#country_of_origin
  ,msi#wc#orm_d_flag
  ,msi#wc#store_velocity
  ,msi#wc#dc_velocity
  ,msi#wc#yearly_store_velocity
  ,msi#wc#yearly_dc_velocity
  ,msi#wc#prism_part_number
  ,msi#wc#hazmat_description
  ,msi#wc#hazmat_container
  ,msi#wc#gtp_indicator
  ,msi#wc#last_lead_time
  ,msi#wc#amu
  ,msi#wc#reserve_stock
  ,msi#wc#taxware_code
  ,msi#wc#average_units
  ,msi#wc#product_code
  ,msi#wc#import_duty_
  ,msi#wc#keep_item_active
  ,msi#wc#pesticide_flag
  ,msi#wc#calc_lead_time
  ,msi#wc#voc_gl
  ,msi#wc#pesticide_flag_state
  ,msi#wc#voc_category
  ,msi#wc#voc_sub_category
  ,msi#wc#msds_#
  ,msi#wc#hazmat_packaging_grou
  ,poh#standardp#need_by_date
  ,poh#standardp#freight_terms_
  ,poh#standardp#carrier_terms_
  ,poh#standardp#fob_terms_tab
  ,mca#1#accountingcategory
  ,mca#1#accountingcategory_desc
  ,mca#3#code
  ,mca#3#code_desc
  ,mca#4#code
  ,mca#4#code_desc
  ,mca#5#code
  ,mca#5#code_desc
  ,mca#6#code
  ,mca#6#code_desc
  ,mca#101#category
  ,mca#101#category_desc
  ,mca#101#categoryclass
  ,mca#101#categoryclass_desc
  ,mca#201#itemcategory
  ,mca#201#itemcategory_desc
  ,mca#50136#interesttype
  ,mca#50136#interesttype_desc
  ,mca#50136#primarycode
  ,mca#50136#primarycode_desc
  ,mca#50136#secondarycode
  ,mca#50136#secondarycode_desc
  ,mca#50152#dummy
  ,mca#50152#dummy_desc
  ,mca#50153#cartongroup
  ,mca#50153#cartongroup_desc
  ,mca#50168#class
  ,mca#50168#class_desc
  ,mca#50168#subclass
  ,mca#50168#subclass_desc
  ,mca#50169#contractcategory
  ,mca#50169#contractcategory_des
  ,mca#50190#classification
  ,mca#50190#classification_desc
  ,mca#50190#commodity
  ,mca#50190#commodity_desc
  ,mca#50190#country
  ,mca#50190#country_desc
  ,mca#50190#defaultcategory
  ,mca#50190#defaultcategory_desc
  ,mca#50208#product
  ,mca#50208#product_desc
  ,mca#50210#product
  ,mca#50210#product_desc
  ,mca#50229#product
  ,mca#50229#product_desc
  ,mca#50268#product
  ,mca#50268#product_desc
  ,mca#50272#sequencedependentcla
  ,mca#50272#sequencedependentcl_
  ,mca#50308#gpccatalogcategoryde
  ,mca#50308#gpccatalogcategoryd_
  ,mca#50308#gpccatalogcategoryco
  ,mca#50308#gpccatalogcategoryc_
  ,mca#50348#suppliercode
  ,mca#50348#suppliercode_desc
  ,mca#50348#supplierclass
  ,mca#50348#supplierclass_desc
  ,mca#50388#client
  ,mca#50388#client_desc
  ,mca#50408#purchaseflag
  ,mca#50408#purchaseflag_desc
  ,mca#50409#privatelabel
  ,mca#50409#privatelabel_desc
  ,mca#50410#velocity
  ,mca#50410#velocity_desc
  ,msi#101#item
  ,msi#101#item_desc
  ,gcc#50328#account
  ,gcc#50328#account#descr
  ,gcc#50328#cost_center
  ,gcc#50328#cost_center#descr
  ,gcc#50328#furture_use
  ,gcc#50328#furture_use#descr
  ,gcc#50328#future_use_2
  ,gcc#50328#future_use_2#descr
  ,gcc#50328#location
  ,gcc#50328#location#descr
  ,gcc#50328#product
  ,gcc#50328#product#descr
  ,gcc#50328#project_code
  ,gcc#50328#project_code#descr
  ,gcc1#50328#account
  ,gcc1#50328#account#descr
  ,gcc1#50328#cost_center
  ,gcc1#50328#cost_center#descr
  ,gcc1#50328#furture_use
  ,gcc1#50328#furture_use#descr
  ,gcc1#50328#future_use_2
  ,gcc1#50328#future_use_2#descr
  ,gcc1#50328#location
  ,gcc1#50328#location#descr
  ,gcc1#50328#product
  ,gcc1#50328#product#descr
  ,gcc1#50328#project_code
  ,gcc1#50328#project_code#descr
  ,gcc2#50328#account
  ,gcc2#50328#account#descr
  ,gcc2#50328#cost_center
  ,gcc2#50328#cost_center#descr
  ,gcc2#50328#furture_use
  ,gcc2#50328#furture_use#descr
  ,gcc2#50328#future_use_2
  ,gcc2#50328#future_use_2#descr
  ,gcc2#50328#location
  ,gcc2#50328#location#descr
  ,gcc2#50328#product
  ,gcc2#50328#product#descr
  ,gcc2#50328#project_code
  ,gcc2#50328#project_code#descr
  ,gcc#50368#account
  ,gcc#50368#account#descr
  ,gcc#50368#department
  ,gcc#50368#department#descr
  ,gcc#50368#division
  ,gcc#50368#division#descr
  ,gcc#50368#future_use
  ,gcc#50368#future_use#descr
  ,gcc#50368#product
  ,gcc#50368#product#descr
  ,gcc#50368#subaccount
  ,gcc#50368#subaccount#descr
  ,gcc1#50368#account
  ,gcc1#50368#account#descr
  ,gcc1#50368#department
  ,gcc1#50368#department#descr
  ,gcc1#50368#division
  ,gcc1#50368#division#descr
  ,gcc1#50368#future_use
  ,gcc1#50368#future_use#descr
  ,gcc1#50368#product
  ,gcc1#50368#product#descr
  ,gcc1#50368#subaccount
  ,gcc1#50368#subaccount#descr
  ,gcc2#50368#account
  ,gcc2#50368#account#descr
  ,gcc2#50368#department
  ,gcc2#50368#department#descr
  ,gcc2#50368#division
  ,gcc2#50368#division#descr
  ,gcc2#50368#future_use
  ,gcc2#50368#future_use#descr
  ,gcc2#50368#product
  ,gcc2#50368#product#descr
  ,gcc2#50368#subaccount
  ,gcc2#50368#subaccount#descr
  ,edi
  ,accepted_y_n
  ,changes_requested_y_n
)
AS
   SELECT poh.po_header_id
         ,poh.last_update_date po_last_update_date
         ,poh.last_updated_by po_last_updated_by
         ,TRUNC (poh.creation_date) po_creation_date
         ,poh.created_by po_created_by
         ,poav.agent_name po_buyer_name
         ,poh.segment1 po_number
         ,poh.type_lookup_code po_type
         ,pv.vendor_name po_supplier_name
         ,pvs.vendor_site_code po_supplier_site_code
         ,pvc.first_name supplier_contact_first_name
         ,pvc.last_name supplier_contact_last_name
         ,pvc.area_code supplier_contact_area_code
         ,pvc.phone supplier_contact_phone
         ,pvc.vendor_contact_id pvc_vendor_contact_id
         ,DECODE (poh.summary_flag,  'Y', 'Yes',  'N', 'No') po_summary
         ,DECODE (poh.enabled_flag,  'Y', 'Yes',  'N', 'No') po_enabled
         ,poh.start_date_active po_start_date
         ,poh.end_date_active po_end_date
         ,hrl.location_code po_ship_to_location
         ,apt.name po_terms
         ,poh.ship_via_lookup_code po_ship_via
         ,poh.fob_lookup_code po_fob
         ,poh.freight_terms_lookup_code po_freight_terms
         ,poh.authorization_status po_status
         ,poh.revision_num po_revision_num
         ,poh.revised_date po_revised_date
         ,poh.printed_date po_printed_date
         ,poh.approved_date po_approved_date
         ,poh.currency_code po_currency_code
         ,poh.acceptance_due_date po_acceptance_due_date
         ,DECODE (poh.user_hold_flag,  'Y', 'Yes',  'N', 'No',  NULL) po_on_hold
         ,DECODE (poh.acceptance_required_flag,  'Y', 'Yes',  'N', 'No',  NULL) po_acceptance_required
         ,poh.note_to_authorizer po_note_to_authorizer
         ,poh.note_to_vendor po_note_to_supplier
         ,poh.note_to_receiver po_note_to_receiver
         ,poh.comments po_comments
         ,poh.closed_code po_closed_code
         ,poh.closed_date po_closed_date
         ,pol.po_line_id
         ,pol.last_update_date pol_last_update_date
         ,pol.last_updated_by pol_last_updated_by
         ,pol.creation_date pol_creation_date
         ,pol.created_by pol_created_by
         ,plt.line_type po_line_type
         ,pol.line_num po_line_num
         ,msi.concatenated_segments c_flex_item
         ,msi.padded_concatenated_segments msi_padded_concat_segments
         ,msi.segment1 po_item_number
         ,msi.segment2 msi_segment2
         ,msi.segment3 msi_segment3
         ,msi.segment4 msi_segment4
         ,msi.segment5 msi_segment5
         ,msi.segment6 msi_segment6
         ,msi.segment7 msi_segment7
         ,msi.segment8 msi_segment8
         ,msi.segment9 msi_segment9
         ,msi.segment10 msi_segment10
         ,msi.segment11 msi_segment11
         ,msi.segment12 msi_segment12
         ,msi.segment13 msi_segment13
         ,msi.segment14 msi_segment14
         ,msi.segment15 msi_segment15
         ,msi.outside_operation_uom_type outside_operation_unit_type
         ,msi.unit_of_issue unit_of_issue
         ,msi.inventory_item_id parent_id
         ,msi.expense_account expense_id
         ,msi.full_lead_time processing_time
         ,msi.qty_rcv_tolerance receipt_tolerance
         ,msi.price_tolerance_percent price_tolerance
         ,msi.invoice_close_tolerance invoicing_close_tolerance
         ,msi.receive_close_tolerance receiving_close_tolerance
         ,msi.days_early_receipt_allowed days_early_receipt_allowed
         ,msi.list_price_per_unit list_price
         ,msi.days_late_receipt_allowed days_late_receipt_allowed
         ,msi.market_price market_price
         ,msi.rounding_factor rounding_factor
         ,msi.receiving_routing_id routing_steps
         ,REPLACE (pol.item_description, '~') po_item_description
         ,TO_NUMBER (
             DECODE (plt.order_type_lookup_code, 'AMOUNT', 1, NVL (poll.price_override, pol.unit_price)))
             po_unit_price
         ,ROUND (
             DECODE (plt.order_type_lookup_code
                    ,'RATE', pod.amount_ordered
                    ,'FIXED PRICE', pod.amount_ordered
                    ,pod.quantity_ordered)
            ,xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision)
             po_quantity_ordered
         ,ROUND (
             DECODE (plt.order_type_lookup_code
                    ,'RATE', pod.amount_cancelled
                    ,'FIXED PRICE', pod.amount_cancelled
                    ,pod.quantity_cancelled)
            ,xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision)
             po_quantity_cancelled
         ,ROUND (
             DECODE (plt.order_type_lookup_code
                    ,'RATE', pod.amount_delivered
                    ,'FIXED PRICE', pod.amount_delivered
                    ,pod.quantity_delivered)
            ,xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision)
             po_quantity_received
         ,ROUND (
             (  (DECODE (plt.order_type_lookup_code
                        ,'RATE', pod.amount_ordered
                        ,'FIXED PRICE', pod.amount_ordered
                        ,pod.quantity_ordered))
              - (DECODE (plt.order_type_lookup_code
                        ,'RATE', pod.amount_cancelled
                        ,'FIXED PRICE', pod.amount_cancelled
                        ,pod.quantity_cancelled))
              - (DECODE (plt.order_type_lookup_code
                        ,'RATE', pod.amount_delivered
                        ,'FIXED PRICE', pod.amount_delivered
                        ,pod.quantity_delivered)))
            ,xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision)
             po_quantity_due
         ,ROUND (
             DECODE (plt.order_type_lookup_code
                    ,'RATE', pod.amount_billed
                    ,'FIXED PRICE', pod.amount_billed
                    ,pod.quantity_billed)
            ,xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision)
             po_quantity_billed
         ,DECODE (plt.order_type_lookup_code
                 ,'RATE', pol.amount
                 ,'FIXED PRICE', pol.amount
                 , (pol.quantity) * NVL (pol.base_unit_price, pol.unit_price))
             line_amount
         ,DECODE (plt.order_type_lookup_code
                 ,'RATE', poll.amount - NVL (poll.amount_cancelled, 0)
                 ,'FIXED PRICE', poll.amount - NVL (poll.amount_cancelled, 0)
                 , (poll.price_override * (poll.quantity - NVL (poll.quantity_cancelled, 0))))
             shipment_amount
         ,pol.item_revision po_item_revision
         ,pol.unit_meas_lookup_code po_uom
         ,pol.list_price_per_unit po_list_price
         ,pol.market_price po_market_price
         ,pol.cancel_flag pol_cancel
         ,pol.cancel_date pol_cancel_date
         ,pol.cancel_reason pol_cancel_reason
         ,NVL (pol.taxable_flag, poll.taxable_flag) po_taxable_flag
         ,pol.tax_name po_tax_name
         ,pol.type_1099 po_type_1099
         ,pol.closed_code pol_closed_code
         ,pol.closed_date pol_closed_date
         ,pol.closed_reason pol_closed_reason
         ,poll.line_location_id
         ,poll.shipment_num po_shipment_num
         ,poll.shipment_type po_shipment_type
         ,poll.need_by_date po_need_by_date
         ,poll.promised_date po_promised_date
         ,poll.last_accept_date po_last_accept_date
         ,DECODE (poll.inspection_required_flag,  'Y', 'Yes',  'N', 'No') po_4_way_match
         ,DECODE (poll.receipt_required_flag,  'Y', 'Yes',  'N', 'No') po_3_way_match
         ,poll.qty_rcv_tolerance po_qty_rcv_tolerance
         ,poll.days_early_receipt_allowed po_days_early_receipt_allowed
         ,poll.days_late_receipt_allowed po_days_late_receipt_allowed
         ,poll.invoice_close_tolerance po_invoice_close_tolerance
         ,poll.receive_close_tolerance po_receive_close_tolerance
         ,DECODE (poll.accrue_on_receipt_flag,  'Y', 'Yes',  'N', 'No') po_accrue_on_receipt
         ,pod.po_distribution_id
         ,pod.distribution_num po_distribution_num
         ,sob.name po_set_of_books
         ,gcc.code_combination_id po_charge_ccid
         ,gcc.concatenated_segments po_charge_account
         ,gcc1.code_combination_id po_accrual_ccid
         ,gcc1.concatenated_segments po_accrual_account
         ,gcc2.code_combination_id po_variance_ccid
         ,gcc2.concatenated_segments po_variance_account
         ,pp.name project_name
         ,pp.segment1 project_number
         ,                                                                                 -- Added PK Columns
          hrl.location_id hrl_location_id
         ,sob.set_of_books_id sob_set_of_books_id
         ,pp.project_id
         ,                                                                     ------- mtl categories kfv data
          mca.concatenated_segments c_flex_cat
         ,mca.padded_concatenated_segments mca_padded_concat_segments
         ,mca.structure_id mca_structure_id
         ,mca.segment1 mca_segment1
         ,mca.segment2 mca_segment2
         ,mca.segment3 mca_segment3
         ,mca.segment4 mca_segment4
         ,mca.segment5 mca_segment5
         ,mca.segment6 mca_segment6
         ,mca.segment7 mca_segment7
         ,mca.segment8 mca_segment8
         ,mca.segment9 mca_segment9
         ,mca.segment10 mca_segment10
         ,mca.segment11 mca_segment11
         ,mca.segment12 mca_segment12
         ,mca.segment13 mca_segment13
         ,mca.segment14 mca_segment14
         ,mca.segment15 mca_segment15
         ,   plc_sta.displayed_field
          || DECODE (poh.cancel_flag, 'Y', ', ' || plc_can.displayed_field, NULL)
          || DECODE (NVL (poh.closed_code, 'OPEN'), 'OPEN', NULL, ', ' || plc_clo.displayed_field)
          || DECODE (poh.frozen_flag, 'Y', ', ' || plc_fro.displayed_field, NULL)
          || DECODE (poh.user_hold_flag, 'Y', ', ' || plc_hld.displayed_field, NULL)
             status
         ,pdt.type_name TYPE
         ,hou.name operating_unit
         ,hou.organization_id org_id
         ,ood.organization_code org_code                --Added for TMS#20140407-00181 by Mahender on 15/07/14
         ,pv.vendor_id
         ,poav.agent_id
         ,gcc.code_combination_id
         ,pvs.vendor_site_id pvs_vendor_site_id
         ,plt.line_type_id plt_line_type_id
         ,mca.category_id mca_category_id
         ,msi.organization_id msi_organization_id
         ,CASE
             WHEN (pod.quantity_billed - pod.quantity_ordered) > 0
             THEN
                (pod.quantity_billed - pod.quantity_ordered)
          END
             quantity_over_billed
         ,papf.full_name requestor_name
         --descr#flexfield#start

         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc2.attribute1, 'I') gcc2#branch
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc1.attribute1, 'I') gcc1#branch
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I') gcc#branch
         ,mca.attribute1 mca#cogs_account
         ,mca.attribute2 mca#sales_account
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL) msi#hds#lob
         ,DECODE (msi.attribute_category
                 ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F')
                 ,NULL)
             msi#hds#drop_shipment_eligab
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL) msi#hds#invoice_uom
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL) msi#hds#product_id
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL) msi#hds#vendor_part_number
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL) msi#hds#unspsc_code
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL) msi#hds#upc_primary
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL) msi#hds#sku_description
         ,DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL) msi#wc#ca_prop_65
         ,DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL) msi#wc#country_of_origin
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute11, 'F')
                 ,NULL)
             msi#wc#orm_d_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL) msi#wc#store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL) msi#wc#dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL) msi#wc#yearly_store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL) msi#wc#yearly_dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL) msi#wc#prism_part_number
         ,DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL) msi#wc#hazmat_description
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msi.attribute18, 'I')
                 ,NULL)
             msi#wc#hazmat_container
         ,DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL) msi#wc#gtp_indicator
         ,DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL) msi#wc#last_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL) msi#wc#amu
         ,DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL) msi#wc#reserve_stock
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msi.attribute22, 'I')
                 ,NULL)
             msi#wc#taxware_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL) msi#wc#average_units
         ,DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL) msi#wc#product_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL) msi#wc#import_duty_
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute29, 'F')
                 ,NULL)
             msi#wc#keep_item_active
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute3, 'F')
                 ,NULL)
             msi#wc#pesticide_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL) msi#wc#calc_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL) msi#wc#voc_gl
         ,DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL) msi#wc#pesticide_flag_state
         ,DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL) msi#wc#voc_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL) msi#wc#voc_sub_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL) msi#wc#msds_#
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC_HAZMAT_PACKAGE_GROUP', msi.attribute9, 'I')
                 ,NULL)
             msi#wc#hazmat_packaging_grou
         ,DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute1, NULL)
             poh#standardp#need_by_date
         ,DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute2, NULL)
             poh#standardp#freight_terms_
         ,DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute3, NULL)
             poh#standardp#carrier_terms_
         ,DECODE (poh.attribute_category, 'Standard Purchase Order', poh.attribute4, NULL)
             poh#standardp#fob_terms_tab
         --descr#flexfield#end


         --kff#start
         ,mca.segment1 "MCA#1#ACCOUNTINGCATEGORY"
         ,xxeis.eis_rs_dff.decode_valueset ('AX_INV_ACCOUNTING_CATEGORY', mca.segment1, 'I')
             "MCA#1#ACCOUNTINGCATEGORY_DESC"
         ,mca.segment1 "MCA#3#CODE"
         ,xxeis.eis_rs_dff.decode_valueset ('30 Characters', mca.segment1, 'N') "MCA#3#CODE_DESC"
         ,mca.segment1 "MCA#4#CODE"
         ,xxeis.eis_rs_dff.decode_valueset ('30 Characters', mca.segment1, 'N') "MCA#4#CODE_DESC"
         ,mca.segment1 "MCA#5#CODE"
         ,xxeis.eis_rs_dff.decode_valueset ('30 Characters', mca.segment1, 'N') "MCA#5#CODE_DESC"
         ,mca.segment1 "MCA#6#CODE"
         ,xxeis.eis_rs_dff.decode_valueset ('30 Characters', mca.segment1, 'N') "MCA#6#CODE_DESC"
         ,mca.segment1 "MCA#101#CATEGORY"
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_CATEGORY_NUMBER', mca.segment1, 'I')
             "MCA#101#CATEGORY_DESC"
         ,mca.segment2 "MCA#101#CATEGORYCLASS"
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_CATEGORY_CLASS', mca.segment2, 'I')
             "MCA#101#CATEGORYCLASS_DESC"
         ,mca.segment1 "MCA#201#ITEMCATEGORY"
         ,xxeis.eis_rs_dff.decode_valueset ('PO_ITEM_CATEGORY', mca.segment1, 'N')
             "MCA#201#ITEMCATEGORY_DESC"
         ,mca.segment1 "MCA#50136#INTERESTTYPE"
         ,xxeis.eis_rs_dff.decode_valueset ('AS_INT_TYPES_VS', mca.segment1, 'F')
             "MCA#50136#INTERESTTYPE_DESC"
         ,mca.segment2 "MCA#50136#PRIMARYCODE"
         ,xxeis.eis_rs_dff.decode_valueset ('AS_PRIM_CODES_VS', mca.segment2, 'F')
             "MCA#50136#PRIMARYCODE_DESC"
         ,mca.segment3 "MCA#50136#SECONDARYCODE"
         ,xxeis.eis_rs_dff.decode_valueset ('AS_SEC_CODES_VS', mca.segment3, 'F')
             "MCA#50136#SECONDARYCODE_DESC"
         ,mca.segment1 "MCA#50152#DUMMY"
         ,xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50152#DUMMY_DESC"
         ,mca.segment1 "MCA#50153#CARTONGROUP"
         ,xxeis.eis_rs_dff.decode_valueset ('WMS_CARTONIZATION_GROUP', mca.segment1, 'N')
             "MCA#50153#CARTONGROUP_DESC"
         ,mca.segment1 "MCA#50168#CLASS"
         ,xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50168#CLASS_DESC"
         ,mca.segment2 "MCA#50168#SUBCLASS"
         ,xxeis.eis_rs_dff.decode_valueset ('', mca.segment2, '') "MCA#50168#SUBCLASS_DESC"
         ,mca.segment1 "MCA#50169#CONTRACTCATEGORY"
         ,xxeis.eis_rs_dff.decode_valueset ('CONTRACT_CATEGORY_ALPHANUMERIC', mca.segment1, 'N')
             "MCA#50169#CONTRACTCATEGORY_DES"
         ,mca.segment1 "MCA#50190#CLASSIFICATION"
         ,xxeis.eis_rs_dff.decode_valueset ('WSH_COMMODITY_CLASSIFICATION', mca.segment1, 'F')
             "MCA#50190#CLASSIFICATION_DESC"
         ,mca.segment2 "MCA#50190#COMMODITY"
         ,xxeis.eis_rs_dff.decode_valueset ('', mca.segment2, '') "MCA#50190#COMMODITY_DESC"
         ,mca.segment3 "MCA#50190#COUNTRY"
         ,xxeis.eis_rs_dff.decode_valueset ('', mca.segment3, '') "MCA#50190#COUNTRY_DESC"
         ,mca.segment4 "MCA#50190#DEFAULTCATEGORY"
         ,xxeis.eis_rs_dff.decode_valueset ('IEX_YES_NO', mca.segment4, 'F') "MCA#50190#DEFAULTCATEGORY_DESC"
         ,mca.segment1 "MCA#50208#PRODUCT"
         ,xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50208#PRODUCT_DESC"
         ,mca.segment1 "MCA#50210#PRODUCT"
         ,xxeis.eis_rs_dff.decode_valueset ('4 Digits', mca.segment1, 'N') "MCA#50210#PRODUCT_DESC"
         ,mca.segment1 "MCA#50229#PRODUCT"
         ,xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50229#PRODUCT_DESC"
         ,mca.segment1 "MCA#50268#PRODUCT"
         ,xxeis.eis_rs_dff.decode_valueset ('4 Digits', mca.segment1, 'N') "MCA#50268#PRODUCT_DESC"
         ,mca.segment1 "MCA#50272#SEQUENCEDEPENDENTCLA"
         ,xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50272#SEQUENCEDEPENDENTCL_"
         ,mca.segment1 "MCA#50308#GPCCATALOGCATEGORYDE"
         ,xxeis.eis_rs_dff.decode_valueset ('', mca.segment1, '') "MCA#50308#GPCCATALOGCATEGORYD_"
         ,mca.segment2 "MCA#50308#GPCCATALOGCATEGORYCO"
         ,xxeis.eis_rs_dff.decode_valueset ('', mca.segment2, '') "MCA#50308#GPCCATALOGCATEGORYC_"
         ,mca.segment1 "MCA#50348#SUPPLIERCODE"
         ,xxeis.eis_rs_dff.decode_valueset ('HDS_SUPPLIER_CODE', mca.segment1, 'I')
             "MCA#50348#SUPPLIERCODE_DESC"
         ,mca.segment2 "MCA#50348#SUPPLIERCLASS"
         ,xxeis.eis_rs_dff.decode_valueset ('HDS_SUPPLIER_CLASS', mca.segment2, 'D')
             "MCA#50348#SUPPLIERCLASS_DESC"
         ,mca.segment1 "MCA#50388#CLIENT"
         ,xxeis.eis_rs_dff.decode_valueset ('INV_LSP_CLIENTS', mca.segment1, 'F') "MCA#50388#CLIENT_DESC"
         ,mca.segment1 "MCA#50408#PURCHASEFLAG"
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_PURCHASE_FLAG', mca.segment1, 'I')
             "MCA#50408#PURCHASEFLAG_DESC"
         ,mca.segment1 "MCA#50409#PRIVATELABEL"
         ,xxeis.eis_rs_dff.decode_valueset ('30 char', mca.segment1, 'N') "MCA#50409#PRIVATELABEL_DESC"
         ,mca.segment1 "MCA#50410#VELOCITY"
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_SALES_VELOCITY', mca.segment1, 'I')
             "MCA#50410#VELOCITY_DESC"
         ,msi.segment1 "MSI#101#ITEM"
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_SYSTEM_ITEMS', msi.segment1, 'N') "MSI#101#ITEM_DESC"
         --kff#end
         --gl#accountff#start

         ,gcc.segment4 gcc#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ACCOUNT') gcc#50328#account#descr
         ,gcc.segment3 gcc#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_COSTCENTER')
             gcc#50328#cost_center#descr
         ,gcc.segment6 gcc#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_FUTURE_USE1')
             gcc#50328#furture_use#descr
         ,gcc.segment7 gcc#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7, 'XXCUS_GL_FUTURE_USE_2')
             gcc#50328#future_use_2#descr
         ,gcc.segment2 gcc#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_LOCATION') gcc#50328#location#descr
         ,gcc.segment1 gcc#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_PRODUCT') gcc#50328#product#descr
         ,gcc.segment5 gcc#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_PROJECT')
             gcc#50328#project_code#descr
         ,gcc1.segment4 gcc1#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4, 'XXCUS_GL_ACCOUNT') gcc1#50328#account#descr
         ,gcc1.segment3 gcc1#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3, 'XXCUS_GL_COSTCENTER')
             gcc1#50328#cost_center#descr
         ,gcc1.segment6 gcc1#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6, 'XXCUS_GL_FUTURE_USE1')
             gcc1#50328#furture_use#descr
         ,gcc1.segment7 gcc1#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment7, 'XXCUS_GL_FUTURE_USE_2')
             gcc1#50328#future_use_2#descr
         ,gcc1.segment2 gcc1#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2, 'XXCUS_GL_LOCATION') gcc1#50328#location#descr
         ,gcc1.segment1 gcc1#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1, 'XXCUS_GL_PRODUCT') gcc1#50328#product#descr
         ,gcc1.segment5 gcc1#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5, 'XXCUS_GL_PROJECT')
             gcc1#50328#project_code#descr
         ,gcc2.segment4 gcc2#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment4, 'XXCUS_GL_ACCOUNT') gcc2#50328#account#descr
         ,gcc2.segment3 gcc2#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment3, 'XXCUS_GL_COSTCENTER')
             gcc2#50328#cost_center#descr
         ,gcc2.segment6 gcc2#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment6, 'XXCUS_GL_FUTURE_USE1')
             gcc2#50328#furture_use#descr
         ,gcc2.segment7 gcc2#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment7, 'XXCUS_GL_FUTURE_USE_2')
             gcc2#50328#future_use_2#descr
         ,gcc2.segment2 gcc2#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment2, 'XXCUS_GL_LOCATION') gcc2#50328#location#descr
         ,gcc2.segment1 gcc2#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment1, 'XXCUS_GL_PRODUCT') gcc2#50328#product#descr
         ,gcc2.segment5 gcc2#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment5, 'XXCUS_GL_PROJECT')
             gcc2#50328#project_code#descr
         ,gcc.segment4 gcc#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ LTMR _ACCOUNT')
             gcc#50368#account#descr
         ,gcc.segment3 gcc#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc#50368#department#descr
         ,gcc.segment2 gcc#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_ LTMR _DIVISION')
             gcc#50368#division#descr
         ,gcc.segment6 gcc#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc#50368#future_use#descr
         ,gcc.segment1 gcc#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_LTMR_PRODUCT')
             gcc#50368#product#descr
         ,gcc.segment5 gcc#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc#50368#subaccount#descr
         ,gcc1.segment4 gcc1#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4, 'XXCUS_GL_ LTMR _ACCOUNT')
             gcc1#50368#account#descr
         ,gcc1.segment3 gcc1#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc1#50368#department#descr
         ,gcc1.segment2 gcc1#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2, 'XXCUS_GL_ LTMR _DIVISION')
             gcc1#50368#division#descr
         ,gcc1.segment6 gcc1#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc1#50368#future_use#descr
         ,gcc1.segment1 gcc1#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1, 'XXCUS_GL_LTMR_PRODUCT')
             gcc1#50368#product#descr
         ,gcc1.segment5 gcc1#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc1#50368#subaccount#descr
         ,gcc2.segment4 gcc2#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment4, 'XXCUS_GL_ LTMR _ACCOUNT')
             gcc2#50368#account#descr
         ,gcc2.segment3 gcc2#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc2#50368#department#descr
         ,gcc2.segment2 gcc2#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment2, 'XXCUS_GL_ LTMR _DIVISION')
             gcc2#50368#division#descr
         ,gcc2.segment6 gcc2#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc2#50368#future_use#descr
         ,gcc2.segment1 gcc2#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment1, 'XXCUS_GL_LTMR_PRODUCT')
             gcc2#50368#product#descr
         ,gcc2.segment5 gcc2#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc2#50368#subaccount#descr                                                    --gl#accountff#end
         /*,CASE
             WHEN (xxeis.eis_rs_xxwc_com_util_pkg.get_site_code (poh.po_header_id) NOT LIKE ('%NON%EDI%'))
             THEN
                'Y'
             ELSE
                'N'
          END*/

         , (SELECT xml_or_edi
              FROM xxwc.xxwc_po_comm_hist
             WHERE po_header_id = poh.po_header_id AND ROWNUM = 1)
             --             ,null
             edi                                        --Added for TMS#20140407-00181 by Mahender on 15/07/14
         ,NVL ( (SELECT accepted_flag
                   FROM po_acceptances_v
                  WHERE po_header_id = poh.po_header_id AND ROWNUM = 1)
              ,'N')
             --              , Null
             accepted_y_n                               --Added for TMS#20140407-00181 by Mahender on 15/07/14
         , (SELECT acceptance_type
              FROM po_acceptances_v
             WHERE po_header_id = poh.po_header_id AND ROWNUM = 1)
             --             , Null
             changes_requested_y_n                      --Added for TMS#20140407-00181 by Mahender on 15/07/14
     FROM po_headers poh
         ,po_lines pol
         ,po_line_locations poll
         ,po_distributions pod
         ,po_vendors pv
         ,po_vendor_sites pvs
         ,po_vendor_contacts pvc
         ,po_line_types plt
         ,mtl_system_items_b_kfv msi
         ,mtl_categories_b_kfv mca
         ,hr_locations_all hrl
         ,po_agents_v poav
         ,ap_terms apt
         ,gl_sets_of_books sob
         ,gl_code_combinations_kfv gcc
         ,gl_code_combinations_kfv gcc1
         ,gl_code_combinations_kfv gcc2
         ,po_lookup_codes plc_sta
         ,po_lookup_codes plc_can
         ,po_lookup_codes plc_clo
         ,po_lookup_codes plc_fro
         ,po_lookup_codes plc_hld
         ,po_document_types pdt
         ,pa_projects pp
         ,apps.hr_operating_units hou
         ,org_organization_definitions ood              --Added for TMS#20140407-00181 by Mahender on 15/07/14
         ,financials_system_parameters fsp
         ,per_all_people_f papf
    WHERE     poh.po_header_id = pol.po_header_id(+)
          AND pol.po_line_id = poll.po_line_id(+)
          AND poll.line_location_id = pod.line_location_id
          AND poh.vendor_id = pv.vendor_id
          AND poh.vendor_site_id = pvs.vendor_site_id
          AND poh.vendor_contact_id = pvc.vendor_contact_id(+)
          AND pol.line_type_id = plt.line_type_id
          AND pol.item_id = msi.inventory_item_id(+)
          ---AND pol.org_id = msi.organization_id(+)
          AND pol.category_id = mca.category_id
          AND poll.ship_to_location_id = hrl.location_id(+)
          AND poh.agent_id = poav.agent_id(+)
          AND poh.terms_id = apt.term_id(+)
          AND pod.set_of_books_id = sob.set_of_books_id
          AND pod.code_combination_id = gcc.code_combination_id
          AND pod.accrual_account_id = gcc1.code_combination_id
          AND pod.variance_account_id = gcc2.code_combination_id
          AND plc_sta.lookup_code = NVL (poh.authorization_status, 'INCOMPLETE')
          AND plc_sta.lookup_type IN ('PO APPROVAL', 'DOCUMENT STATE')
          AND plc_can.lookup_code = 'CANCELLED'
          AND plc_can.lookup_type = 'DOCUMENT STATE'
          AND plc_clo.lookup_code = NVL (poh.closed_code, 'OPEN')
          AND plc_clo.lookup_type = 'DOCUMENT STATE'
          AND plc_fro.lookup_code = 'FROZEN'
          AND plc_fro.lookup_type = 'DOCUMENT STATE'
          AND plc_hld.lookup_code = 'ON HOLD'
          AND plc_hld.lookup_type = 'DOCUMENT STATE'
          AND pdt.document_type_code = 'PO'
          AND pdt.document_subtype = poh.type_lookup_code
          AND poh.org_id = pdt.org_id                                                                    --tjx
          AND hou.organization_id = poh.org_id
          AND hou.organization_id = ood.operating_unit  --Added for TMS#20140407-00181 by Mahender on 15/07/14
          AND poll.ship_to_organization_id = ood.organization_id --Added for TMS#20140407-00181 by Mahender on 15/07/14
          AND pod.project_id = pp.project_id(+)
          AND fsp.inventory_organization_id = NVL (msi.organization_id, fsp.inventory_organization_id)
          AND pod.deliver_to_person_id = papf.person_id
          AND TRUNC (papf.effective_start_date) = (SELECT MAX (papf1.effective_start_date)
                                                     FROM per_all_people_f papf1
                                                    WHERE papf1.person_id = papf.person_id)
          AND fsp.org_id = poh.org_id;
