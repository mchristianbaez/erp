CREATE OR REPLACE FORCE VIEW xxeis.xxhds_appr_act_term_date
(
   approver_id
  ,approver_emp_number
  ,approver_name
  ,approver_acctual_termination
)
AS
   SELECT appr.person_id approver_id
         ,appr.employee_number approver_emp_number
         ,appr.full_name approver_name
         ,apprterm.actual_termination_date approver_acctual_termination
     FROM hr.per_all_people_f appr
         ,(SELECT pps.person_id, pps.actual_termination_date
             FROM hr.per_periods_of_service pps
            WHERE pps.actual_termination_date IS NOT NULL) apprterm
    WHERE     appr.person_id = apprterm.person_id(+)
          AND appr.effective_end_date = '31-DEC-4712';


