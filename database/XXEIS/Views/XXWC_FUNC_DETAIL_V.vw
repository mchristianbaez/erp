CREATE OR REPLACE FORCE VIEW xxeis.xxwc_func_detail_v
(
   responsibility_name
  ,responsibility_key
  ,responsibility_id
  ,user_function_name
  ,function_name
  ,function_id
  ,user_menu_name
  ,menu_name
  ,prompt
  ,entry_sequence
  ,level_num
  ,PATH
  ,pathonly
  ,code_path
  ,id_path
)
AS
     SELECT DISTINCT
            r.responsibility_name
           ,r.responsibility_key
           ,r.responsibility_id
           ,fft.user_function_name
           ,ff.function_name
           ,ff.function_id
           ,mut.user_menu_name
           ,m.menu_name
           ,me.prompt
           ,me.entry_sequence
           ,menus.level_num
           ,r.responsibility_name || menus.PATH || '>' || me.prompt AS PATH
           ,r.responsibility_name || menus.PATH AS pathonly
           ,r.responsibility_key || menus.code_path AS code_path
           ,menus.id_path AS id_path
       FROM apps.fnd_form_functions_tl fft
           ,apps.fnd_form_functions ff
           ,apps.fnd_menu_entries_vl me
           ,apps.fnd_menus_tl mut
           ,apps.fnd_menus m
           ,apps.fnd_responsibility_vl r
           ,(    SELECT DISTINCT
                        LEVEL level_num
                       ,me.menu_id
                       ,CONNECT_BY_ROOT me.menu_id AS root_id
                       ,SYS_CONNECT_BY_PATH (NVL (me.prompt, m.menu_name), '\')
                           AS PATH
                       ,SYS_CONNECT_BY_PATH (m.menu_name, '\') AS code_path
                       ,SYS_CONNECT_BY_PATH (m.menu_id, '\') AS id_path
                   FROM apps.fnd_menu_entries_vl me
                       ,apps.fnd_menus_tl metl
                       ,apps.fnd_menus m
                  WHERE me.menu_id = metl.menu_id AND me.menu_id = m.menu_id
             START WITH me.menu_id IN
                           (SELECT menu_id
                              FROM apps.fnd_responsibility fr
                                  ,apps.fnd_responsibility_tl frt
                             WHERE     fr.responsibility_id =
                                          frt.responsibility_id
                                   AND fr.responsibility_key LIKE 'XXWC%')
             CONNECT BY PRIOR me.sub_menu_id = me.menu_id) menus
      WHERE     fft.function_id = ff.function_id
            AND ff.function_id = me.function_id
            AND mut.menu_id = menus.menu_id
            AND mut.menu_id = m.menu_id
            AND me.menu_id = mut.menu_id
            AND r.responsibility_key LIKE 'XXWC%'
            -- and r.RESPONSIBILITY_NAME LIKE 'HDS IT Functional Configurator - WC'
            AND NVL (r.end_date, CURRENT_DATE) >= CURRENT_DATE
            AND NVL (r.start_date, CURRENT_DATE) <= CURRENT_DATE
            AND menus.root_id = r.menu_id
            AND ff.function_id NOT IN           -- Respect function exclusions
                   (SELECT frf.action_id
                      FROM apps.fnd_resp_functions frf
                     WHERE     frf.action_id = ff.function_id
                           AND frf.responsibility_id = r.responsibility_id
                           AND frf.rule_type = 'F')
            AND menus.menu_id NOT IN                -- Respect menu exclusions
                   (SELECT frf.action_id
                      FROM apps.fnd_resp_functions frf
                     WHERE     frf.action_id = me.menu_id
                           AND frf.responsibility_id = r.responsibility_id
                           AND frf.rule_type = 'M')
   ORDER BY r.responsibility_name                         --        ,level_num
           ,me.entry_sequence
           ,r.responsibility_name || menus.PATH
--,mut.user_menu_name
;


