/* Formatted on 12/20/2013 1:09:07 PM (QP5 v5.256.13226.35510) */
CREATE OR REPLACE FORCE VIEW XXEIS.XXEIS_OM_DETAIL_SO_VW
(
   SHIP_FROM_ORG_ID,
   FLOW_STATUS_CODE,
   HDR_STATUS,
   REGION,
   DISTRICT,
   BRANCH_NUMBER,
   BRANCH_NAME,
   HEADER_ORDER_DATE,
   LINE_CREATE_DATE,
   LINE_REQUEST_DATE,
   CREATED_BY_NTID,
   CREATED_BY_NAME,
   CUSTOMER_ACCT_NUMBER,
   CUSTOMER_ACCT_NAME,
   ORDER_TYPE,
   ORDER_NUMBER,
   HEADER_STATUS,
   LINE_NUMBER,
   LINE_STATUS,
   USER_ITEM_DESCRIPTION,
   UNIT_COST,
   ORDERED_QUANTITY,
   UNIT_SELLING_PRICE,
   PRICING_QUANTITY_UOM,
   AWAITING_SHIPPING_GROUP,
   SHIP_VIA,
   DELIVERY_CREATE_DATE,
   SKU,
   UOM
)
AS
   SELECT ool.ship_from_org_id,
          ool.flow_status_code,
          ooh.flow_status_code hdr_status,
          (SELECT mp.attribute9
             FROM apps.mtl_parameters mp
            WHERE mp.organization_id = ool.ship_from_org_id)
             Region,
          --  mp.attribute9 AS Region,
          (SELECT mp.attribute8
             FROM apps.mtl_parameters mp
            WHERE mp.organization_id = ool.ship_from_org_id)
             District,
          ood.organization_code BRANCH_NUMBER,
          ood.organization_name BRANCH_NAME,
          ooh.ordered_date AS Header_Order_Date,
          ool.creation_date AS Line_Create_Date,
          ool.request_date AS Line_Request_Date,
          (SELECT fu.user_name
             FROM fnd_user fu
            WHERE ool.created_by = fu.user_id)
             Created_By_NTID,
          -- fu.user_name AS Created_By_NTID,
          (SELECT fu.description
             FROM fnd_user fu
            WHERE ool.created_by = fu.user_id)
             Created_By_Name,
          (SELECT hca.account_number
             FROM HZ_CUST_ACCOUNTS hca
            WHERE hca.cust_account_id = ooh.sold_to_org_id)
             Customer_Acct_Number,
          (SELECT hca.account_name
             FROM HZ_CUST_ACCOUNTS hca
            WHERE hca.cust_account_id = ooh.sold_to_org_id)
             Customer_Acct_Name,
          (SELECT typ.name
             FROM apps.oe_order_types_v typ
            WHERE ooh.order_type_id = typ.order_type_id)
             Order_Type,
          --typ.name AS Order_Type,
          ooh.order_number AS Order_Number,
          ooh.flow_status_code AS Header_Status,
          ool.line_number AS Line_Number,
          ool.flow_status_code AS Line_Status,
          ool.user_item_description AS User_Item_Description,
          ool.unit_cost,
          ool.ordered_quantity,
          ool.unit_selling_price,
          ool.pricing_quantity_uom,
          (CASE
              WHEN (    ool.flow_status_code = 'AWAITING_SHIPPING'
                    AND (   ool.user_item_description = 'DELIVERED'
                         OR ool.user_item_description = 'OUT_FOR_DELIVERY'))
              THEN
                 'OUT_FOR_DELIVERY or DELIVERED'
              WHEN (    ool.flow_status_code = 'AWAITING_SHIPPING'
                    AND (   ool.user_item_description <> 'DELIVERED'
                         OR ool.user_item_description <> 'OUT_FOR_DELIVERY'
                         OR ool.user_item_description IS NULL))
              THEN
                 'NOT_SHIPPED'
              ELSE
                 '--'
           END)
             Awaiting_Shipping_Group,
          (SELECT wcsmv.ship_method_code_meaning
             FROM apps.wsh_carrier_ship_methods_v wcsmv
            WHERE     wcsmv.ship_method_code = ool.shipping_method_code
                  AND ROWNUM = 1)
             AS Ship_Via,
          (SELECT wss.creation_date
             FROM xxwc.xxwc_wsh_shipping_stg wss
            WHERE wss.header_id = ool.header_id AND wss.line_id = ool.line_id)
             Delivery_Create_Date,
          -- wss.creation_date AS Delivery_Create_Date,
          ool.ordered_item AS SKU,
          ool.pricing_quantity_uom AS UOM
     FROM oe_order_headers_all ooh,
          oe_order_lines_all ool,
          apps.org_organization_definitions ood
    WHERE     ooh.header_id = ool.header_id
          --AND ooh.flow_status_code = ool.flow_status_code(+)
          AND ooL.SHIP_FROM_ORG_ID = OOD.ORGANIZATION_ID
          AND ool.flow_status_code NOT IN ('CANCELLED', 'ENTERED', 'CLOSED')
          AND ooH.flow_status_code = 'BOOKED';
