/* ***************************************************************
  $Header XXEIS.XXEIS_WC_RUNTIME_V.vw $
  Module Name: EiS Admin Reports
  PURPOSE: Report Runtimes - WC
  TMS Task Id : 20160210-00047
  REVISIONS: Initial Version
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        02/11/2016  Mahender Reddy        Initial Version
*************************************************************** */
CREATE OR REPLACE FORCE VIEW "XXEIS"."XXEIS_WC_RUNTIME_V" ("PROCESS_ID", "REQUEST_ID", "REPORT_ID", "REPORT_NAME", "STATUS", "ROWS_RETRIEVED", "SESSION_ID", "USER_ID", "SUBMITTED_BY", "EMPLOYEE_FULL_NAME", "ORGANIZATION_ID", "START_TIME", "END_TIME", "START_DATE", "END_DATE", "REQUEST_DATE", "REQUESTED_START_DATE", "ACTUAL_START_DATE", "ACTUAL_COMPLETION_DATE", "CONC_EXE_TIME_IN_MINUTES", "REPORT_EXECUTION_TIM", "APPLICATION_NAME", "BUSINESS_GROUP_NAME", "CATEGORY_NAME", "RESPONSIBILITY_NAME", "VIEW_NAME") AS 
  SELECT earpv.process_id,
    earpv.request_id,
    earpv.report_id,
    earpv.report_name,
    earpv.status,
    earpv.rows_retrieved,
    earpv.session_id,
    earpv.user_id,
    fu.user_name submitted_by,
    fu.description employee_full_name,
    earpv.organization_id,
    earpv.start_time,
    earpv.end_time,
    TRUNC(to_date(earpv.start_time, 'DD-MON-YYYY HH24:MI:SS')) start_date,
    TRUNC(to_date(earpv.start_time, 'DD-MON-YYYY HH24:MI:SS')) end_date,
    earpv.request_date,
    earpv.requested_start_date,
    earpv.actual_start_date,
    earpv.actual_completion_date,
    (round((((to_date(earpv.end_time, 'DD-MON-YYYY HH24:MI:SS')) -
    (to_date(earpv.start_time, 'DD-MON-YYYY HH24:MI:SS')))*24*60))) conc_exe_time_in_minutes,
--    earpv.conc_exe_time_in_minutes,
	((round((((to_date(earpv.end_time, 'DD-MON-YYYY HH24:MI:SS')) -
		(to_date(earpv.start_time, 'DD-MON-YYYY HH24:MI:SS')))*24)))||'HRS :'||
	MOD((round((((to_date(earpv.end_time, 'DD-MON-YYYY HH24:MI:SS')) -
		(to_date(earpv.start_time, 'DD-MON-YYYY HH24:MI:SS')))*24*60))),60)||' MINS :'||
	MOD(round((((to_date(earpv.end_time, 'DD-MON-YYYY HH24:MI:SS')) -
		(to_date(earpv.start_time, 'DD-MON-YYYY HH24:MI:SS')))*24*60*60)),60)||' SECS')
    report_execution_tim,
   -- earpv.report_execution_time,
    earpv.application_name,
    earpv.business_group_name,
    earpv.category_name,
    earpv.responsibility_name,
    earpv.view_name
  FROM XXEIS.EIS_ADM_REPORTS_PROCESSES_V earpv,
    xxeis.eis_rs_sessions ers,
    apps.fnd_user fu
  WHERE earpv.session_id = ers.session_id
  AND ers.user_id        = fu.user_id
/