CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_INV_ORDER_DETAIL_V (ORDER_NUMBER, LINE_NUMBER, ORDERED_DATE, TRX_LINE_NUMBER, CUSTOMER_NAME, EXTENDED_PRICE, CREDIT_INVOICE_NUMBER, TRANSACTION_NAME, TRANSACTION_DESCRIPTION, SALES_ORDER, OPEN_FLAG, REJECT_REASON_CODE, QUANTITY_ORDERED, QUANTITY_CREDITED, RECEIVABLE_INV_QUANTITY, RECEIVABLE_LINE_TYPE, INTERFACE_LINE_CONTEXT, INVOICE_LINE_EXTENDED_AMOUNT, INVOICE_LINE_REVENUE_AMOUNT, INVOICE_LINE_TAX_EXT_AMNT, INVOICE_LINE_TAX_REV_AMNT, ORDER_LINE_CHARGE, ORDER_LINE_CHARGE_COST, INVOICE_CREATION_DATE, ORDERED_ITEM, ORDER_QUANTITY_UOM, CANCELLED_QUANTITY, SHIPPED_QUANTITY, ORDERED_QUANTITY, FULFILLED_QUANTITY, SHIPPING_QUANTITY, SHIPPING_QUANTITY_UOM, SOLD_TO_ORG_ID, SHIP_FROM_ORG_ID, SHIP_TO_ORG_ID, DELIVER_TO_ORG_ID, SOURCE_TYPE_CODE, PRICE_LIST_ID, SHIPMENT_NUMBER, AGREEMENT_ID, SHIPMENT_PRIORITY_CODE, SHIPPING_METHOD_CODE, FREIGHT_TERMS_CODE, FREIGHT_CARRIER_CODE, FOB_POINT_CODE, TAX_POINT_CODE, PAYMENT_TERM_ID, INVOICING_RULE_ID,
  ACCOUNTING_RULE_ID, ACCOUNTING_RULE_DURATION, SOURCE_DOCUMENT_TYPE_ID, ORIG_SYS_DOCUMENT_REF, SOURCE_DOCUMENT_ID, ORIG_SYS_LINE_RE, SOURCE_DOCUMENT_LINE_ID, REFERENCE_LINE_ID, REFERENCE_TYPE, REFERENCE_HEADER_ID, ITEM_REVISION, LINE_CATEGORY_CODE, CUSTOMER_TRX_LINE_ID, REFERENCE_CUSTOMER_TRX_LINE_ID, UNIT_SELLING_PRICE, UNIT_LIST_PRICE, ORDER_TAX_VALUE, MODEL_GROUP_NUMBER, OPTION_FLAG, DEP_PLAN_REQUIRED_FLAG, VISIBLE_DEMAND_FLAG, ACTUAL_ARRIVAL_DATE, ACTUAL_SHIPMENT_DATE, EARLIEST_ACCEPTABLE_DATE, LATEST_ACCEPTABLE_DATE, LINE_TYPE, PRICE_LIST, ROUNDING_FACTOR, ACCOUNTING_RULE, INVOICING_RULE, TERMS, SOLD_TO, CUSTOMER_NUMBER, SHIP_FROM, SUBINVENTORY, SHIP_TO, SHIP_TO_LOCATION, SHIP_TO_ADDRESS1, SHIP_TO_ADDRESS2, SHIP_TO_ADDRESS3, SHIP_TO_ADDRESS4, SHIP_TO_ADDRESS5, BILL_TO, BILL_TO_LOCATION, BILL_TO_ADDRESS1, BILL_TO_ADDRESS2, BILL_TO_ADDRESS3, BILL_TO_ADDRESS4, BILL_TO_ADDRESS5, SHIP_TO_CONTACT, BILL_TO_CONTACT, INVOICED_QUANTITY, ORDER_LINE_STATUS, ORDER_HEADER_STATUS,
  FULFILLMENT_DATE, PREFERRED_GRADE, ORDERED_QUANTITY2, ORDERED_QUANTITY_UOM2, SHIPPED_QUANTITY2, CANCELLED_QUANTITY2, SHIPPING_QUANTITY2, SHIPPING_QUANTITY_UOM2, FULFILLED_QUANTITY2, UPGRADED_FLAG, UNIT_SELLING_PRICE_PER_PQTY, RETURN_REASON_CODE, INVENTORY_ITEM_ID, OPERATING_UNIT, SHIPPING_ORG_ID, SHIP_SITE_USE_ID, SHIP_PS_PARTY_SITE_ID, SHIP_LOC_LOCATION_ID, SHIP_CAS_CUST_ACCT_SITE_ID, BILL_LOC_LOCATION_ID, BILL_PS_PARTY_SITE_ID, BILL_CAS_CUST_ACCT_SITE_ID, SHIP_PARTY_PARTY_ID, CUST_ACCT_CUST_ACCOUNT_ID, SROLES_CUST_ACCOUNT_ROLE_ID, SHIP_REL_RELATIONSHIP_ID, SHIP_ACCT_CUST_ACCOUNT_ID, IROLES_CUST_ACCOUNT_ROLE_ID, INVOICE_PARTY_PARTY_ID, INVOICE_REL_RELATIONSHIP_ID, INVOICE_ACCT_CUST_ACCOUNT_ID, ORDER_HEADER_HEADER_ID, ORDER_LINE_LINE_ID, PRICE_LIST_LIST_HEADER_ID, ACCRULE_RULE_ID, INVRULE_RULE_ID, TERM_TERM_ID, TRX_TYPE_TRANSACTION_TYPE_ID, CUST_TRX_TYPE_ID, OPERATING_UNIT_ID, PARTY_ID, BILL_SU_SITE_USE_ID, SHIP_REL_DIRECTIONAL_FLAG, INVOICE_REL_DIRCTNAL_FLAG, QLH_LIST_HEADER_ID,
  RCTL_CUSTOMER_TRX_LINE_ID, RCT_TX_CSTMR_TRX_LINE_ID, RCT_CUSTOMER_TRX_ID, RCTT_CUST_TRX_TYPE_ID, RCTT_ORG_ID, OCL_CHARGE_ID, RERENTAL_BILLING_TERMS)
AS
  SELECT oh.order_number order_number,
    ol.line_number
    ||'.'
    || ol.shipment_number line_number,
    TRUNC(oh.ordered_date) Ordered_date,
    rctl.line_number trx_line_number,
    party.party_name customer_name,
    ol.unit_selling_price* ol.ordered_quantity extended_price,
    rct.trx_number credit_invoice_number,
    rctt.NAME transaction_name,
    rctt.description transaction_description,
    rctl.sales_order sales_order,
    oh.open_flag open_flag,
    rctl.reason_code reject_reason_code,
    rctl.quantity_ordered quantity_ordered,
    rctl.quantity_credited quantity_credited,
    rctl.quantity_invoiced receivable_inv_quantity,
    rctl.line_type receivable_line_type,
    Rctl.Interface_Line_Context Interface_Line_Context,
    Rctl.Extended_Amount Invoice_Line_Extended_Amount,
    Rctl.Revenue_Amount Invoice_Line_Revenue_Amount,
    Rct_Tax.Extended_Amount Invoice_Line_Tax_Ext_Amnt,
    /*added by hari to include tax amount corresponding to each line*/
    Rct_Tax.Revenue_Amount Invoice_Line_Tax_Rev_Amnt,
    Ocl.Charge_Name Order_Line_Charge,
    /*freight line type present in invoice defaults from charges of the order*/
    ocl.charge_amount order_line_charge_cost,
    -- rctl.tax_rate tax_rate,
    -- rctl.vat_tax_id vat_tax_id,
    -- Rctl.Taxable_Amount Taxable_Amount,
    -- rctl.tax_line_id tax_line_id,
    TRUNC(rct.creation_date) invoice_creation_date,
    ol.ordered_item ordered_item,
    ol.order_quantity_uom order_quantity_uom,
    ol.cancelled_quantity cancelled_quantity,
    ol.shipped_quantity shipped_quantity,
    ol.ordered_quantity ordered_quantity,
    ol.fulfilled_quantity fulfilled_quantity,
    ol.shipping_quantity shipping_quantity,
    ol.shipping_quantity_uom shipping_quantity_uom,
    ol.sold_to_org_id sold_to_org_id,
    ol.ship_from_org_id ship_from_org_id,
    ol.ship_to_org_id ship_to_org_id,
    ol.deliver_to_org_id deliver_to_org_id,
    ol.source_type_code source_type_code,
    ol.price_list_id price_list_id,
    ol.shipment_number shipment_number,
    ol.agreement_id agreement_id,
    ol.shipment_priority_code shipment_priority_code,
    ol.shipping_method_code shipping_method_code,
    ol.freight_terms_code freight_terms_code,
    ol.freight_carrier_code freight_carrier_code,
    ol.fob_point_code fob_point_code,
    ol.tax_point_code tax_point_code,
    ol.payment_term_id payment_term_id,
    ol.invoicing_rule_id invoicing_rule_id,
    ol.accounting_rule_id accounting_rule_id,
    ol.accounting_rule_duration accounting_rule_duration,
    ol.source_document_type_id source_document_type_id,
    ol.orig_sys_document_ref orig_sys_document_ref,
    ol.source_document_id source_document_id,
    ol.orig_sys_line_ref orig_sys_line_re,
    ol.source_document_line_id source_document_line_id,
    ol.reference_line_id reference_line_id,
    ol.reference_type reference_type,
    ol.reference_header_id reference_header_id,
    ol.item_revision item_revision,
    ol.line_category_code line_category_code,
    ol.customer_trx_line_id customer_trx_line_id,
    ol.reference_customer_trx_line_id reference_customer_trx_line_id,
    ol.unit_selling_price unit_selling_price,
    Ol.Unit_List_Price Unit_List_Price,
    ol.tax_value order_tax_value,
    ol.model_group_number model_group_number,
    ol.option_flag option_flag,
    ol.dep_plan_required_flag dep_plan_required_flag,
    ol.visible_demand_flag visible_demand_flag,
    ol.actual_arrival_date actual_arrival_date,
    ol.actual_shipment_date actual_shipment_date,
    ol.earliest_acceptable_date earliest_acceptable_date,
    ol.latest_acceptable_date latest_acceptable_date,
    ott.NAME line_type,
    qlhvl.NAME price_list,
    qlh.rounding_factor rounding_factor,
    accrule.NAME accounting_rule,
    invrule.NAME invoicing_rule,
    term.NAME terms,
    party.party_name sold_to,
    cust_acct.account_number customer_number,
    ship_from_org.organization_code ship_from,
    ol.subinventory subinventory,
    ship_su.LOCATION ship_to,
    ship_su.LOCATION ship_to_location,
    ship_loc.address1 ship_to_address1,
    ship_loc.address2 ship_to_address2,
    ship_loc.address3 ship_to_address3,
    ship_loc.address4 ship_to_address4,
    DECODE (ship_loc.city, NULL, NULL, ship_loc.city
    || ', ' )
    || DECODE (ship_loc.state, NULL, ship_loc.province
    || ', ', ship_loc.state
    || ', ' )
    || DECODE (ship_loc.postal_code, NULL, NULL, ship_loc.postal_code
    || ', ' )
    || DECODE (ship_loc.country, NULL, NULL, ship_loc.country) ship_to_address5,
    bill_su.LOCATION Bill_to,
    bill_su.LOCATION Bill_to_location,
    bill_loc.address1 Bill_to_address1,
    bill_loc.address2 Bill_to_address2,
    bill_loc.address3 Bill_to_address3,
    bill_loc.address4 Bill_to_address4,
    DECODE (bill_loc.city, NULL, NULL, bill_loc.city
    || ', ' )
    || DECODE (bill_loc.state, NULL, bill_loc.province
    || ', ', bill_loc.state
    || ', ' )
    || DECODE (bill_loc.postal_code, NULL, NULL, bill_loc.postal_code
    || ', ' )
    || DECODE (bill_loc.country, NULL, NULL, bill_loc.country) Bill_to_address5,
    ship_party.person_last_name
    || DECODE (ship_party.person_first_name, NULL, NULL, ', '
    || ship_party.person_first_name )
    || DECODE (ship_arl.meaning, NULL, NULL, ' '
    || ship_arl.meaning) ship_to_contact,
    invoice_party.person_last_name
    || DECODE (invoice_party.person_first_name, NULL, NULL, ', '
    || invoice_party.person_first_name )
    || DECODE (invoice_arl.meaning, NULL, NULL, ' '
    || invoice_arl.meaning ) Bill_to_contact,
    Ol.Invoiced_Quantity Invoiced_Quantity,
    Ol.Flow_Status_Code Order_Line_Status,
    oh.flow_status_code order_header_status,
    ol.fulfillment_date fulfillment_date,
    ol.preferred_grade preferred_grade,
    ol.ordered_quantity2 ordered_quantity2,
    ol.ordered_quantity_uom2 ordered_quantity_uom2,
    ol.shipped_quantity2 shipped_quantity2,
    ol.cancelled_quantity2 cancelled_quantity2,
    ol.shipping_quantity2 shipping_quantity2,
    ol.shipping_quantity_uom2 shipping_quantity_uom2,
    ol.fulfilled_quantity2 fulfilled_quantity2,
    ol.upgraded_flag upgraded_flag,
    ol.unit_selling_price_per_pqty unit_selling_price_per_pqty,
    oh.return_reason_code return_reason_code,
    ol.inventory_item_id inventory_item_id,
    hou.name operating_unit,
    --Primary Keys
    ship_from_org.organization_id shipping_org_id,
    ship_su.site_use_id ship_site_use_id,
    ship_ps.party_site_id ship_ps_party_site_id,
    ship_loc.location_id ship_loc_location_id,
    ship_cas.cust_acct_site_id ship_cas_cust_acct_site_id,
    bill_loc.location_id bill_loc_location_id,
    bill_ps.party_site_id bill_ps_party_site_id,
    bill_cas.cust_acct_site_id bill_cas_cust_acct_site_id,
    ship_party.party_id ship_party_party_id,
    cust_acct.cust_account_id cust_acct_cust_account_id,
    ship_roles.CUST_ACCOUNT_ROLE_ID sroles_CUST_ACCOUNT_ROLE_ID,
    ship_rel.RELATIONSHIP_ID ship_rel_RELATIONSHIP_ID,
    ship_acct.cust_account_id ship_acct_cust_account_id,
    invoice_roles.CUST_ACCOUNT_ROLE_ID iroles_cust_account_role_id,
    invoice_party.party_id invoice_party_party_id,
    invoice_rel.RELATIONSHIP_ID invoice_rel_RELATIONSHIP_ID,
    invoice_acct.cust_account_id invoice_acct_cust_account_id,
    oh.header_id order_header_header_id,
    ol.line_id order_line_line_id,
    qlhvl.LIST_HEADER_ID price_list_LIST_HEADER_ID,
    accrule.rule_id accrule_rule_id,
    invrule.rule_id invrule_rule_id,
    term.term_id term_term_id,
    ott.TRANSACTION_TYPE_ID trx_type_TRANSACTION_TYPE_ID,
    rct.cust_trx_type_id cust_trx_type_id,
    hou.organization_id operating_unit_id,
    -- unique keys added for component joins
    PARTY.PARTY_ID PARTY_ID,
    BILL_SU.SITE_USE_ID BILL_SU_SITE_USE_ID,
    SHIP_REL.DIRECTIONAL_FLAG SHIP_REL_DIRECTIONAL_FLAG,
    invoice_rel.directional_flag invoice_rel_dirctnal_flag,
    qlh.list_header_id qlh_list_header_id,
    rctl.customer_trx_line_id rctl_customer_trx_line_id,
    rct_tax.customer_trx_line_id rct_tx_cstmr_trx_line_id,
    rct.customer_trx_id rct_customer_trx_id,
    RCTT.CUST_TRX_TYPE_ID RCTT_CUST_TRX_TYPE_ID,
    RCTT.ORG_ID RCTT_ORG_ID,
    OCL.CHARGE_ID OCL_CHARGE_ID,
    xxeis.eis_rs_dff.decode_valueset( 'ReRental Billing Type',OL.ATTRIBUTE2,'I') ReRental_Billing_Terms
    --  Rct_Tax.LINE_TYPE
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters ship_from_org,
    hz_cust_site_uses ship_su,
    hz_party_sites ship_ps,
    hz_locations ship_loc,
    hz_cust_acct_sites ship_cas,
    hz_cust_site_uses bill_su,
    hz_party_sites bill_ps,
    hz_locations bill_loc,
    hz_cust_acct_sites bill_cas,
    hz_parties party,
    hz_cust_accounts cust_acct,
    hz_cust_account_roles ship_roles,
    hz_parties ship_party,
    hz_relationships ship_rel,
    hz_cust_accounts ship_acct,
    ar_lookups ship_arl,
    hz_cust_account_roles invoice_roles,
    hz_parties invoice_party,
    hz_relationships invoice_rel,
    hz_cust_accounts invoice_acct,
    ar_lookups invoice_arl,
    oe_order_headers oh,
    oe_order_lines ol,
    qp_list_headers_vl qlhvl,
    qp_list_headers_b qlh ,
    ra_rules accrule,
    ra_rules invrule,
    ra_terms_vl term,
    oe_transaction_types_vl ott,
    Ra_Customer_Trx_Lines Rctl,
    ra_customer_trx_lines rct_tax
    /*added by hari TO INCLUDE TAX AMOUNT*/
    ,
    ra_customer_trx rct,
    ra_cust_trx_types rctt,
    Hr_Operating_Units Hou,
    oe_charge_lines_v ocl
    /*added by hari to include freight cost which defaults from om into receivables*/
  WHERE ol.line_type_id               = ott.transaction_type_id
  AND ol.price_list_id                = qlhvl.list_header_id(+)
  AND qlhvl.list_header_id            = qlh.list_header_id(+)
  AND ol.accounting_rule_id           = accrule.rule_id(+)
  AND ol.invoicing_rule_id            = invrule.rule_id(+)
  AND ol.payment_term_id              = term.term_id(+)
  AND ol.sold_to_org_id               = cust_acct.cust_account_id(+)
  AND cust_acct.party_id              = party.party_id(+)
  AND ol.ship_from_org_id             = ship_from_org.organization_id(+)
  AND ol.ship_to_org_id               = ship_su.site_use_id(+)
  AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
  AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
  AND ship_loc.location_id(+)         = ship_ps.location_id
  AND ol.invoice_to_org_id            = bill_su.site_use_id(+)
  AND bill_su.cust_acct_site_id       = bill_cas.cust_acct_site_id(+)
  AND bill_cas.party_site_id          = bill_ps.party_site_id(+)
  AND bill_loc.location_id(+)         = bill_ps.location_id
  AND ol.ship_to_contact_id           = ship_roles.cust_account_role_id(+)
  AND ship_roles.party_id             = ship_rel.party_id(+)
  AND ship_roles.role_type(+)         = 'CONTACT'
  AND ship_rel.subject_id             = ship_party.party_id(+)
  AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
  AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
  AND ship_arl.lookup_type(+)         = 'CONTACT_TITLE'
  AND ship_arl.lookup_code(+)         = ship_party.person_pre_name_adjunct
  AND ol.invoice_to_contact_id        = invoice_roles.cust_account_role_id(+)
  AND invoice_roles.party_id          = invoice_rel.party_id(+)
  AND invoice_roles.role_type(+)      = 'CONTACT'
  AND invoice_rel.subject_id          = invoice_party.party_id(+)
  AND invoice_roles.cust_account_id   = invoice_acct.cust_account_id(+)
  AND NVL (invoice_rel.object_id, -1) = NVL (invoice_acct.party_id, -1)
  AND invoice_arl.lookup_type(+)      = 'CONTACT_TITLE'
  AND invoice_arl.lookup_code(+)      = invoice_party.person_pre_name_adjunct
  AND ol.header_id                    = oh.header_id
  AND OL.BOOKED_FLAG                  = 'Y'
  AND ol.flow_status_code             ='CLOSED'
  AND OL.LINE_TYPE_ID                 = OTT.TRANSACTION_TYPE_ID
  AND rctl.interface_line_attribute6  = TO_CHAR(ol.line_id)
  AND RCTl.INTERFACE_line_ATTRIBUTE1  = oh.order_number
  AND rctl.interface_line_context     = 'ORDER ENTRY'
    -- AND ol.line_category_code IN ('RETURN')
    --  AND ol.invoice_interface_status_code  =   'YES'
    --AND rctl.sales_order_line IS NOT NULL
    -- And To_Char(Oh.Order_Number) = To_Char(Rct.Interface_Header_Attribute1)
    -- And Ol.Line_Number=Rctl.Sales_Order_Line/*added by hari to remove cartesian join*/
  AND RCT_TAX.LINK_TO_CUST_TRX_LINE_ID(+)=RCTL.CUSTOMER_TRX_LINE_ID
    /*added by hari to include tax amounts for corresponding line*/
    -- And Rct_Tax.Customer_Trx_Id=Rct.Customer_Trx_Id
  AND Rct_Tax.Line_Type(+) ='TAX'
  AND ocl.line_id(+)       =ol.line_id
  AND rct.customer_trx_id  = rctl.customer_trx_id
  AND rct.cust_trx_type_id = rctt.cust_trx_type_id
  AND Rct.Org_Id           = Rctt.Org_Id
  AND OH.ORG_ID            = HOU.ORGANIZATION_ID
/
