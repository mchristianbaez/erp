CREATE OR REPLACE FORCE VIEW xxeis.js
(
   account
  ,check_date
  ,check_number
  ,description
  ,distribution_amount
  ,gl_date
  ,image_link
  ,invoice_date
  ,invoice_num
  ,line_num
  ,location
  ,vendor_name
  ,vendor_number
  ,check_amount
  ,product
  ,check_status
)
AS
   SELECT o329936.account
         ,o329936.check_date
         ,o329936.check_number
         ,o329936.description
         ,o329936.distribution_amount
         ,o329936.gl_date
         ,o329936.image_link
         ,o329936.invoice_date
         ,o329936.invoice_num
         ,o329936.line_num
         ,o329936.location
         ,o329936.vendor_name
         ,o329936.vendor_number
         ,o329936.check_amount
         ,o329936.product
         ,o329936.check_status
     FROM (SELECT api.invoice_num
                 ,api.invoice_date
                 ,api.invoice_amount
                 ,api.amount_paid
                 ,api.payment_status_flag
                 ,api.vendor_id
                 ,apid.amount distribution_amount
                 ,apid.attribute1 pos_purchase_order
                 ,apid.attribute3 pos_bill_of_lading
                 ,apid.attribute4 u_m
                 ,apid.attribute5 edi_line_number
                 ,apid.attribute6 quantity
                 ,apid.attribute7 unit_price
                 ,api.attribute2 legacy_branch
                 ,api.attribute10 pass_to_pos
                 ,api.attribute9 edi_flag
                 ,apid.distribution_line_number line_num
                 ,gcc.segment1 product
                 ,gcc.segment2 location
                 ,gcc.segment3 cost_center
                 ,gcc.segment4 account
                 ,pov.vendor_name
                 ,pov.segment1 vendor_number
                 ,povs.vendor_site_code
                 ,pov.payment_method_lookup_code vendor_payment_method
                 ,povs.payment_method_lookup_code vendor_site_payment_method
                 ,sob.name set_of_books_name
                 ,api.invoice_currency_code
                 ,apb.batch_name
                 ,api.invoice_id
                 ,apid.accounting_date gl_date
                 ,apid.period_name
                 ,api.description invoice_description
                 ,api.ROWID z$ap_invoices_all_rowid
                 ,apip.amount inovoice_payment_amount
                 ,apip.bank_num invoice_payment_bank_num
                 ,apip.posted_flag invoice_payment_posted_flag
                 ,apip.reversal_flag invoice_payment_reverse_flag
                 ,glper.period_year
                 ,glper.quarter_num
                 ,glper.start_date period_start_date
                 ,glper.end_date period_end_date
                 ,apc.check_number
                 ,apc.check_date
                 ,apc.amount check_amount
                 ,apc.status_lookup_code check_status
                 ,apc.checkrun_name
                 ,apc.cleared_date check_cleared_date
                 ,apc.cleared_amount check_cleared_amount
                 ,apc.void_date check_void_date
                 ,ad1.flex_value_id
                 ,ad2.description
                 ,doc.file_name image_link
                 ,api.attribute12 pos_invoice_id
                 ,api.attribute11 invoice_type
                 ,api.attribute8 pos_system_code
                 ,TRUNC (api.creation_date) creation_date
                 ,api.creation_date creation_day
                 ,api.invoice_type_lookup_code
                 ,api.pay_group_lookup_code
                 ,api.attribute5 invoice_comments
             FROM apps.ap_invoices api
                 ,apps.ap_invoice_distributions apid
                 ,gl.gl_code_combinations gcc
                 ,ap.ap_suppliers pov
                 ,ap.ap_supplier_sites_all povs
                 ,gl.gl_ledgers sob
                 ,apps.ap_batches apb
                 ,gl.gl_periods glper
                 ,apps.ap_invoice_payments apip
                 ,apps.ap_checks apc
                 ,apps.fnd_attached_documents attach
                 ,apps.fnd_documents_tl doc
                 ,applsys.fnd_flex_values ad1
                 ,applsys.fnd_flex_values_tl ad2
            WHERE     apid.invoice_id = api.invoice_id
                  AND apip.invoice_id(+) = api.invoice_id
                  AND apc.check_id(+) = apip.check_id
                  AND api.vendor_id = pov.vendor_id
                  AND gcc.code_combination_id = apid.dist_code_combination_id
                  AND sob.ledger_id = api.set_of_books_id
                  AND povs.vendor_site_id = api.vendor_site_id
                  AND apb.batch_id(+) = api.batch_id
                  AND apid.period_name = glper.period_name
                  AND api.invoice_id = attach.pk1_value(+)
                  AND (    attach.entity_name(+) = 'AP_INVOICES'
                       AND attach.last_update_login(+) = -1)
                  AND (    attach.document_id = doc.document_id(+)
                       AND doc.description(+) = 'Image Link')
                  AND glper.period_set_name = '4-4-QTR'
                  AND ad1.flex_value(+) = gcc.segment4
                  AND ad1.enabled_flag = 'Y'
                  AND ad1.flex_value_id = ad2.flex_value_id
                  AND ad1.flex_value_set_id =
                         (SELECT flex_value_set_id
                            FROM applsys.fnd_flex_value_sets
                           WHERE flex_value_set_name = 'XXCUS_GL_ACCOUNT')) o329936;


