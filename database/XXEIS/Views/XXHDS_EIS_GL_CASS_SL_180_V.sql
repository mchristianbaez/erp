/**************************************************************************************************************
  $Header XXEIS.XXHDS_EIS_GL_CASS_SL_180_V.sql $
  Module Name: EiS Admin Reports
  PURPOSE: CASS freight detail Report
  TMS Task Id :20160411-00110 
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0       4/19/2016           Neha Saini      Initial Version TMS 20160411-00110 
  
  **************************************************************************************************************/
CREATE OR REPLACE FORCE VIEW XXEIS.XXHDS_EIS_GL_CASS_SL_180_V
(
   SOURCE,
   JE_CATEGORY,
   JE_HEADER_ID,
   JE_LINE_NUM,
   ACC_DATE,
   ENTRY,
   BATCH,
   LINE_DESCR,
   LSEQUENCE,
   HNUMBER,
   LLINE,
   LINE_ACCTD_DR,
   LINE_ACCTD_CR,
   LINE_ENT_DR,
   LINE_ENT_CR,
   SEQ_ID,
   SEQ_NUM,
   H_SEQ_ID,
   GL_SL_LINK_ID,
   CUSTOMER_OR_VENDOR,
   CUSTOMER_OR_VENDOR_NUMBER,
   ENTERED_DR,
   ENTERED_CR,
   ACCOUNTED_DR,
   ACCOUNTED_CR,
   SLA_LINE_ACCOUNTED_DR,
   SLA_LINE_ACCOUNTED_CR,
   SLA_LINE_ACCOUNTED_NET,
   SLA_LINE_ENTERED_DR,
   SLA_LINE_ENTERED_CR,
   SLA_LINE_ENTERED_NET,
   SLA_DIST_ENTERED_CR,
   SLA_DIST_ENTERED_DR,
   SLA_DIST_ENTERED_NET,
   SLA_DIST_ACCOUNTED_CR,
   SLA_DIST_ACCOUNTED_DR,
   SLA_DIST_ACCOUNTED_NET,
   ASSOCIATE_NUM,
   TRANSACTION_NUM,
   SALES_ORDER,
   GL_ACCOUNT_STRING,
   CURRENCY_CODE,
   PERIOD_NAME,
   TYPE,
   EFFECTIVE_PERIOD_NUM,
   NAME,
   BATCH_NAME,
   PO_NUMBER,
   BOL_NUMBER,
   AP_INV_SOURCE,
   SLA_EVENT_TYPE,
   PART_NUMBER,
   PART_DESCRIPTION,
   ITEM_UNIT_WT,
   ITEM_TXN_QTY,
   ITEM_MATERIAL_COST,
   ITEM_OVRHD_COST,
   XXCUS_LINE_DESC,
   XXCUS_PO_CREATED_BY,
   XXCUS_IMAGE_LINK,
   CODE_COMBINATION_ID,
   GCC50328PRODUCT,
   GCC50328PRODUCTDESCR,
   GCC50328LOCATION,
   GCC50328LOCATIONDESCR,
   GCC50328COST_CENTER,
   GCC50328COST_CENTERDESCR,
   GCC50328ACCOUNT,
   GCC50328ACCOUNTDESCR,
   GCC50328PROJECT_CODE,
   GCC50328PROJECT_CODEDESCR,
   GCC50328FURTURE_USE,
   GCC50328FURTURE_USEDESCR,
   GCC50328FUTURE_USE_2,
   GCC50328FUTURE_USE_2DESCR,
   GCC50368PRODUCT,
   GCC50368PRODUCTDESCR,
   GCC50368DIVISION,
   GCC50368DIVISIONDESCR,
   GCC50368DEPARTMENT,
   GCC50368DEPARTMENTDESCR,
   GCC50368ACCOUNT,
   GCC50368ACCOUNTDESCR,
   GCC50368SUBACCOUNT,
   GCC50368SUBACCOUNTDESCR,
   GCC50368FUTURE_USE,
   GCC50368FUTURE_USEDESCR,
   SOURCE_DIST_TYPE,
   SOURCE_DIST_ID_NUM_1,
   APPLIED_TO_SOURCE_ID_NUM_1,
   FETCH_SEQ,
   AP_INV_DATE,
   CARRIER_NAME,
   TRANSACTION_LINE_HAUL,
   TRANSACTION_OTHER,
   EST_TRANSACTION_DR,
   EST_TRANSACTION_CR,
   EST_TRANSACTION_NET,
   EST_LINE_HAUL_CHARGE,
   EST_FSC,
   EST_ACCESSORIALS,
   SHIPMENT_MODE,
   MOVEMENT_TYPE,
   ORIGIN_NAME,
   DESTINATION_NAME,
   ORIGIN_CITY,
   ORIGIN_STATE,
   ORIGIN_POSTAL_CODE,
   DESTINATION_CITY,
   DESTINATION_STATE,
   DESTINATION_POSTAL_CODE,
   AUDIT_MATCH,
   PAY_DATE
)
AS
   (SELECT jes.user_je_source_name SOURCE,
           jh.je_category,
           jl.je_header_id je_header_id,
           jl.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           NULL LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr line_ent_dr,
           jl.entered_cr line_ent_cr,
           NULL seq_id,
           NULL seq_num,
           jh.doc_sequence_id h_seq_id,
           NULL gl_sl_link_id,
           TO_CHAR (NULL) customer_or_vendor,
           TO_CHAR (NULL) customer_or_vendor_number,
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           jl.accounted_dr sla_line_accounted_dr,
           jl.accounted_cr sla_line_accounted_cr,
           NVL (jl.accounted_dr, 0) - NVL (jl.accounted_cr, 0)
              sla_line_accounted_net,
           jl.entered_dr sla_line_entered_dr,
           jl.entered_cr sla_line_entered_cr,
           NVL (jl.entered_dr, 0) - NVL (jl.entered_cr, 0)
              sla_line_entered_net,
           jl.entered_cr sla_dist_entered_cr,
           jl.entered_dr sla_dist_entered_dr,
           NVL (jl.entered_cr, 0) - NVL (jl.entered_dr, 0)
              sla_dist_entered_net,
           jl.accounted_cr sla_dist_accounted_cr,
           jl.accounted_dr sla_dist_accounted_dr,
           NVL (jl.accounted_dr, 0) - NVL (jl.accounted_cr, 0)
              sla_dist_accounted_net,
           TO_CHAR (NULL) associate_num,
           TO_CHAR (NULL) transaction_num,
           TO_CHAR (NULL) SALES_ORDER,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           NULL TYPE,
           gps.effective_period_num,
           gle.name,
           gb.name batch_name,
           TO_CHAR (NULL) PO_NUMBER,
           TO_CHAR (NULL) BOL_NUMBER,
           TO_CHAR (NULL) AP_INV_SOURCE,
           NULL SLA_EVENT_TYPE,
           TO_CHAR (NULL) PART_NUMBER,
           TO_CHAR (NULL) PART_DESCRIPTION,
           TO_NUMBER (NULL) ITEM_UNIT_WT,
           TO_NUMBER (NULL) ITEM_TXN_QTY,
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST,
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST,
           SUBSTR (jl.description, 1, 240) XXCUS_GSC_ENH_LINE_DESC,
           TO_CHAR (NULL) XXCUS_GSC_PO_CREATED_BY,
           TO_CHAR (NULL) XXCUS_GSC_IMAGE_LINK,
           gcc.code_combination_id,
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           TO_CHAR (NULL),
           TO_NUMBER (NULL),
           TO_NUMBER (NULL),
           1 FETCH_SEQ,
           TO_DATE (NULL) ap_inv_date,
           xcgi.CARRIER_NAME,
           xcgi.LINE_HAUL AS TRANSACTION_LINE_HAUL,
           xcgi.OTHER AS TRANSACTION_OTHER,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_DR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_CR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_NET,
           xcgi.LINE_HAUL AS EST_LINE_HAUL_CHARGE,
           xcgi.TMS_FSC AS EST_FSC,
           xcgi.TMS_ACCESSORIALS AS EST_ACCESSORIALS,
           xcgi.SHIPMENT_MODE,
           xcgi.MOVEMENT_TYPE,
           xcgi.ORIGIN_NAME,
           xcgi.DESTINATION_NAME,
           xcgi.ORIGIN_CITY,
           xcgi.ORIGIN_STATE,
           xcgi.ORIGIN_POSTAL_CODE,
           xcgi.DESTINATION_CITY,
           xcgi.DESTINATION_STATE,
           xcgi.DESTINATION_POSTAL_CODE,
           xcgi.AUDIT_MATCH,
           xcgi.SCHEDULED_PAY_DATE PAY_DATE
      FROM gl_je_lines jl,
           gl_je_headers jh,
           gl_code_combinations_kfv gcc,
           gl_ledgers gle,
           gl_period_statuses gps,
           gl_je_batches gb,
           gl_je_sources jes,
           XXWC.XXWC_CASS_GL_INTERFACE_VW xcgi
     WHERE     jl.status = 'P'
           AND jh.status = 'P'
           AND jh.actual_flag = 'A'
           AND jh.je_header_id = jl.je_header_id
           AND jes.user_je_source_name NOT IN ('Payables',
                                               'Receivables',
                                               'Cost Management') --bcoz these three sources are fetched down below separately
           AND gcc.code_combination_id = jl.code_combination_id
           AND gle.name = 'HD Supply USD'
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
           AND gps.ledger_id = gle.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
           AND jes.je_source_name = jh.je_source
           AND xcgi.record_id(+) = jl.attribute1
    --
    -- end of source other than ('Payables', 'Receivables', 'Cost Management') -Part 1
    --
    UNION ALL
    --
    -- begin of JE source 'Cost Management' and JE Category "Inventory" -Part 2
    --
    SELECT jes.user_je_source_name SOURCE,
           jh.je_category,
           jl.je_header_id je_header_id,
           jl.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           NULL LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr line_ent_dr,
           jl.entered_cr line_ent_cr,
           ir.subledger_doc_sequence_id seq_id,
           ir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           ir.gl_sl_link_id gl_sl_link_id,
           --
           CASE
              --
              WHEN xeh.event_type_code IN ('PO_DEL_INV',
                                           'PO_DEL_ADJ',
                                           'RET_RI_INV')
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         aps.vendor_name
                    FROM po_headers poh, ap_suppliers aps
                   WHERE     1 = 1
                         AND poh.po_header_id = mta.transaction_source_id --from main SQL
                         AND aps.vendor_id = poh.vendor_id)
              --
              WHEN xeh.event_type_code = 'FOB_RCPT_SENDER_RCPT_NO_TP'
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         hzp.party_name
                    FROM mtl_material_transactions mmt,
                         rcv_transactions rcv,
                         oe_order_lines_all oeol,
                         hz_cust_accounts hzca,
                         hz_parties hzp
                   WHERE     1 = 1
                         AND mmt.transaction_id = mta.transaction_id --from main SQL
                         AND rcv.transaction_id = mmt.rcv_transaction_id
                         AND oeol.source_document_id =
                                mta.transaction_source_id --internal requisition header id
                         AND oeol.source_document_line_id =
                                rcv.requisition_line_id --internal requisition line id
                         AND hzca.cust_account_id = oeol.sold_to_org_id
                         AND hzp.party_id = hzca.party_id
                         AND ROWNUM < 2)
              --
              WHEN xeh.event_type_code = 'AVG_COST_UPD'
              THEN
                 TO_CHAR (NULL)
              --
              ELSE
                 TO_CHAR (NULL)
           --
           END
              customer_or_vendor,
           --
           CASE
              --
              WHEN xeh.event_type_code IN ('PO_DEL_INV',
                                           'PO_DEL_ADJ',
                                           'RET_RI_INV')
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         aps.segment1
                    FROM po_headers poh, ap_suppliers aps
                   WHERE     1 = 1
                         AND poh.po_header_id = mta.transaction_source_id --from main SQL
                         AND aps.vendor_id = poh.vendor_id)
              --
              WHEN xeh.event_type_code = 'FOB_RCPT_SENDER_RCPT_NO_TP'
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         hzca.account_number
                    FROM mtl_material_transactions mmt,
                         rcv_transactions rcv,
                         oe_order_lines_all oeol,
                         hz_cust_accounts hzca
                   WHERE     1 = 1
                         AND mmt.transaction_id = mta.transaction_id --from main SQL
                         AND rcv.transaction_id = mmt.rcv_transaction_id
                         AND oeol.source_document_id =
                                mta.transaction_source_id --internal requisition header id
                         AND oeol.source_document_line_id =
                                rcv.requisition_line_id --internal requisition line id
                         AND hzca.cust_account_id = oeol.sold_to_org_id
                         AND ROWNUM < 2)
              --
              WHEN xeh.event_type_code = 'AVG_COST_UPD'
              THEN
                 TO_CHAR (NULL)
              --
              ELSE
                 TO_CHAR (NULL)
           --
           END
              customer_or_vendor_number,
           --
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           xel.unrounded_accounted_dr sla_line_accounted_dr,
           xel.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (xel.unrounded_accounted_dr, 0)
           - NVL (xel.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           xel.unrounded_entered_dr sla_line_entered_dr,
           xel.unrounded_entered_cr sla_line_entered_cr,
             NVL (xel.unrounded_entered_dr, 0)
           - NVL (xel.unrounded_entered_cr, 0)
              sla_line_entered_net,
           xdl.unrounded_entered_cr sla_dist_entered_cr,
           xdl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (xdl.unrounded_entered_cr, 0)
           - NVL (xdl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           xdl.unrounded_accounted_cr sla_dist_accounted_cr,
           xdl.unrounded_accounted_dr sla_dist_accounted_dr,
             NVL (xdl.unrounded_accounted_dr, 0)
           - NVL (xdl.unrounded_accounted_cr, 0)
              sla_dist_accounted_net,
           TO_CHAR (NULL) associate_num,
           --
           CASE
              WHEN xeh.event_type_code = 'AVG_COST_UPD' THEN TO_CHAR (NULL)
              ELSE TO_CHAR (NULL)
           END
              transaction_num,
           --
           CASE
              --
              WHEN xeh.event_type_code = 'FOB_RCPT_SENDER_RCPT_NO_TP'
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         TO_CHAR (oeoh.order_number)
                    FROM mtl_material_transactions mmt,
                         rcv_transactions rcv,
                         oe_order_lines oeol,
                         oe_order_headers oeoh
                   WHERE     1 = 1
                         AND mmt.transaction_id = mta.transaction_id --from main SQL
                         AND rcv.transaction_id = mmt.rcv_transaction_id
                         AND oeol.source_document_id =
                                mta.transaction_source_id --internal requisition header id
                         AND oeol.source_document_line_id =
                                rcv.requisition_line_id --internal requisition line id
                         AND oeoh.header_id = oeol.header_id
                         AND ROWNUM < 2)
              --
              WHEN xeh.event_type_code = 'AVG_COST_UPD'
              THEN
                 TO_CHAR (NULL)
              --
              ELSE
                 TO_CHAR (NULL)
           --
           END
              sales_order,
           --
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           xel.accounting_class_code TYPE,
           gps.effective_period_num,
           gle.name,
           gb.name batch_name,
           --
           CASE
              WHEN xeh.event_type_code IN ('PO_DEL_INV',
                                           'PO_DEL_ADJ',
                                           'RET_RI_INV')
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         poh.segment1
                    FROM po_headers poh
                   WHERE     1 = 1
                         AND poh.po_header_id = mta.transaction_source_id)
              --
              WHEN xeh.event_type_code = 'FOB_RCPT_SENDER_RCPT_NO_TP'
              THEN
                 TO_CHAR (NULL)
              --
              WHEN xeh.event_type_code = 'AVG_COST_UPD'
              THEN
                 TO_CHAR (NULL)
              --
              ELSE
                 TO_CHAR (NULL)
           --
           END
              PO_NUMBER,
           --
           TO_CHAR (NULL) BOL_NUMBER,
           TO_CHAR (NULL) AP_INV_SOURCE,
           --
           xeh.event_type_code SLA_EVENT_TYPE,
           --
           msi.segment1 PART_NUMBER,
           msi.description PART_DESCRIPTION,
           --
           msi.unit_weight ITEM_UNIT_WT,
           ABS (mta.primary_quantity) ITEM_TXN_QTY,
           (SELECT transaction_cost
              FROM mtl_material_transactions
             WHERE 1 = 1 AND transaction_id = mta.transaction_id)
              ITEM_TOTAL_TXN_COST,
           mta.rate_or_amount ITEM_OVRHD_CALC_COST,
           --
           TO_CHAR (NULL) XXCUS_GSC_ENH_LINE_DESC,
           TO_CHAR (NULL) XXCUS_GSC_PO_CREATED_BY,
           TO_CHAR (NULL) XXCUS_GSC_IMAGE_LINK,
           --
           gcc.code_combination_id,                      --descrflexfieldstart
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           xdl.source_distribution_type,
           xdl.source_distribution_id_num_1,
           xdl.applied_to_source_id_num_1,
           2 FETCH_SEQ,
           TO_DATE (NULL) ap_inv_date,
           xcgi.CARRIER_NAME,
           xcgi.LINE_HAUL AS TRANSACTION_LINE_HAUL,
           xcgi.OTHER AS TRANSACTION_OTHER,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_DR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_CR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_NET,
           xcgi.LINE_HAUL AS EST_LINE_HAUL_CHARGE,
           xcgi.TMS_FSC AS EST_FSC,
           xcgi.TMS_ACCESSORIALS AS EST_ACCESSORIALS,
           xcgi.SHIPMENT_MODE,
           xcgi.MOVEMENT_TYPE,
           xcgi.ORIGIN_NAME,
           xcgi.DESTINATION_NAME,
           xcgi.ORIGIN_CITY,
           xcgi.ORIGIN_STATE,
           xcgi.ORIGIN_POSTAL_CODE,
           xcgi.DESTINATION_CITY,
           xcgi.DESTINATION_STATE,
           xcgi.DESTINATION_POSTAL_CODE,
           xcgi.AUDIT_MATCH,
           xcgi.SCHEDULED_PAY_DATE PAY_DATE
      FROM gl_je_lines jl,
           gl_je_headers jh,
           gl_je_sources jes,
           gl_je_categories jec,
           gl_import_references ir,
           xla_ae_lines xel,
           xla_ae_headers xeh,
           xla_distribution_links xdl,
           --rcv_receiving_sub_ledger rsl,
           mtl_transaction_accounts mta,
           mtl_system_items msi,
           gl_code_combinations_kfv gcc,
           gl_ledgers gle,
           gl_period_statuses gps,
           gl_je_batches gb,
           XXWC.XXWC_CASS_GL_INTERFACE_VW xcgi
     WHERE     1 = 1
           AND gle.name = 'HD Supply USD'
           AND gps.application_id = 101
           AND jl.je_header_id = jh.je_header_id
           AND jes.je_source_name = jh.je_source
           AND jes.user_je_source_name = 'Cost Management'
           AND jec.je_category_name = 'Inventory'
           AND jh.je_category = jec.je_category_name
           AND ir.je_header_id = jl.je_header_id
           AND gcc.code_combination_id = jl.code_combination_id
           AND gps.period_name = jh.period_name
           AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
           AND gps.ledger_id = gle.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
           AND ir.je_line_num = jl.je_line_num
           AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
           AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
           AND xel.ae_header_id = xdl.ae_header_id(+)
           AND xel.ae_line_num = xdl.ae_line_num(+)
           AND xdl.application_id = 707
           AND xeh.ae_header_id(+) = xel.ae_header_id
           AND mta.inv_sub_ledger_id(+) = xdl.source_distribution_id_num_1
           AND msi.inventory_item_id(+) = mta.inventory_item_id
           AND msi.organization_id(+) = mta.organization_id
           AND xdl.source_distribution_type(+) = 'MTL_TRANSACTION_ACCOUNTS'
           AND xcgi.record_id(+) = jl.attribute1
    --
    -- end of JE source 'Cost Management' and JE Category "Inventory" -Part 2
    --
    --
    -- begin of JE source 'Cost Management' and JE Category other than "Inventory" -Part 3
    --
    UNION ALL
    --
    SELECT jes.user_je_source_name SOURCE,
           jh.je_category,
           jl.je_header_id je_header_id,
           jl.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           NULL LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr line_ent_dr,
           jl.entered_cr line_ent_cr,
           ir.subledger_doc_sequence_id seq_id,
           ir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           ir.gl_sl_link_id gl_sl_link_id,
           --
           CASE
              WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         aps.vendor_name
                    FROM po_headers poh, ap_suppliers aps
                   WHERE     1 = 1
                         AND poh.po_header_id = TO_NUMBER (rsl.reference2)
                         AND aps.vendor_id = poh.vendor_id)
              ELSE
                 pov.vendor_name
           END
              customer_or_vendor,
           --
           CASE
              WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         aps.segment1
                    FROM po_headers poh, ap_suppliers aps
                   WHERE     1 = 1
                         AND poh.po_header_id = TO_NUMBER (rsl.reference2)
                         AND aps.vendor_id = poh.vendor_id)
              ELSE
                 pov.segment1
           END
              customer_or_vendor_number,
           --
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           xel.unrounded_accounted_dr sla_line_accounted_dr,
           xel.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (xel.unrounded_accounted_dr, 0)
           - NVL (xel.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           xel.unrounded_entered_dr sla_line_entered_dr,
           xel.unrounded_entered_cr sla_line_entered_cr,
             NVL (xel.unrounded_entered_dr, 0)
           - NVL (xel.unrounded_entered_cr, 0)
              sla_line_entered_net,
           xdl.unrounded_entered_cr sla_dist_entered_cr,
           xdl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (xdl.unrounded_entered_cr, 0)
           - NVL (xdl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           xdl.unrounded_accounted_cr sla_dist_accounted_cr,
           xdl.unrounded_accounted_dr sla_dist_accounted_dr,
             NVL (xdl.unrounded_accounted_dr, 0)
           - NVL (xdl.unrounded_accounted_cr, 0)
              sla_dist_accounted_net,
           TO_CHAR (NULL) associate_num,
           TO_CHAR (NULL) transaction_num,
           TO_CHAR (NULL) sales_order,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           xel.accounting_class_code TYPE,
           gps.effective_period_num,
           gle.name,
           gb.name batch_name,
           --
           CASE
              WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
              THEN
                 rsl.reference4
              ELSE
                 poh.segment1
           END
              PO_NUMBER,
           --
           TO_CHAR (NULL) BOL_NUMBER,
           TO_CHAR (NULL) AP_INV_SOURCE,
           --
           xeh.event_type_code SLA_EVENT_TYPE,
           --
           CASE
              WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         mtlsi.segment1
                    FROM po_distributions pod,
                         po_lines pol,
                         mtl_system_items mtlsi
                   WHERE     1 = 1
                         AND pod.po_distribution_id =
                                TO_NUMBER (rsl.reference3)
                         AND pol.po_line_id = pod.po_line_id
                         AND mtlsi.inventory_item_id = pol.item_id
                         AND mtlsi.organization_id = 222)
              ELSE
                 msi.segment1                            --comes from main SQL
           END
              PART_NUMBER,
           --
           CASE
              WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         pol.item_description
                    FROM po_distributions pod, po_lines pol
                   WHERE     1 = 1
                         AND pod.po_distribution_id =
                                TO_NUMBER (rsl.reference3)
                         AND pol.po_line_id = pod.po_line_id)
              ELSE
                 msi.description                         --comes from main SQL
           END
              PART_DESCRIPTION,
           --
           msi.unit_weight ITEM_UNIT_WT,
           ABS (rsl.source_doc_quantity) ITEM_TXN_QTY,
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST,
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST,
           --
           CASE
              WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
              THEN
                    rsl.reference4
                 || (SELECT /*+ RESULT_CACHE */
                            pol.item_description
                       FROM po_distributions pod, po_lines pol
                      WHERE     1 = 1
                            AND pod.po_distribution_id =
                                   TO_NUMBER (rsl.reference3)
                            AND pol.po_line_id = pod.po_line_id)
              ELSE
                 NVL (msi.description, rl.item_description)
           END
              XXCUS_GSC_ENH_LINE_DESC,
           --
           CASE
              WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
              THEN
                 (SELECT D.DESCRIPTION      --User who created the Requisition
                    FROM FND_USER D,
                         PO_HEADERS_ALL A,
                         PO_DISTRIBUTIONS_ALL C,
                         PO_REQ_DISTRIBUTIONS_ALL B
                   WHERE     1 = 1
                         AND A.SEGMENT1 = rsl.reference4
                         AND D.USER_ID = B.CREATED_BY
                         AND B.DISTRIBUTION_ID = C.REQ_DISTRIBUTION_ID
                         AND C.PO_HEADER_ID = A.po_header_id
                         AND ROWNUM = 1)
              ELSE
                 (SELECT D.DESCRIPTION      --User who created the Requisition
                    FROM FND_USER D,
                         PO_DISTRIBUTIONS_ALL C,
                         PO_REQ_DISTRIBUTIONS_ALL B
                   WHERE     1 = 1
                         AND D.USER_ID = B.CREATED_BY
                         AND B.DISTRIBUTION_ID = C.REQ_DISTRIBUTION_ID
                         AND C.PO_HEADER_ID = poh.po_header_id
                         AND ROWNUM = 1)
           END
              XXCUS_GSC_PO_CREATED_BY,
           --
           CASE
              WHEN (    xeh.event_type_code = 'PERIOD_END_ACCRUAL'
                    AND poh.org_id IN (163, 167))
              THEN
                 (SELECT '''' || a.url
                    FROM fnd_attached_docs_form_vl a,
                         ap_invoice_distributions b,
                         po_distributions_all c
                   WHERE     1 = 1
                         AND c.po_distribution_id =
                                TO_NUMBER (rsl.reference3)
                         AND b.po_distribution_id = c.po_distribution_id
                         AND a.entity_name = 'AP_INVOICES'
                         AND a.pk1_value = TO_CHAR (b.invoice_id)
                         AND a.datatype_name = 'Web Page'
                         AND a.category_description = 'Invoice Internal'
                         AND a.document_description = 'Documentum Image URL'
                         AND a.function_name = 'APXINWKB'
                         AND ROWNUM < 2)
              WHEN (    xeh.event_type_code != 'PERIOD_END_ACCRUAL'
                    AND poh.org_id IN (163, 167))
              THEN
                 (SELECT '''' || a.url
                    FROM fnd_attached_docs_form_vl a, ap_invoices_all b
                   WHERE     1 = 1
                         AND b.quick_po_header_id = poh.po_header_id
                         AND a.entity_name = 'AP_INVOICES'
                         AND a.pk1_value = TO_CHAR (b.invoice_id)
                         AND a.datatype_name = 'Web Page'
                         AND a.category_description = 'Invoice Internal'
                         AND a.document_description = 'Documentum Image URL'
                         AND a.function_name = 'APXINWKB'
                         AND ROWNUM < 2)
              ELSE
                 TO_CHAR (NULL)
           END
              XXCUS_GSC_IMAGE_LINK,
           --
           gcc.code_combination_id,                      --descrflexfieldstart
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           xdl.source_distribution_type,
           xdl.source_distribution_id_num_1,
           xdl.applied_to_source_id_num_1,
           3 FETCH_SEQ,
           (SELECT TRUNC (invoice_date)
              FROM ap_invoices
             WHERE     1 = 1
                   AND quick_po_header_id = poh.po_header_id
                   AND ROWNUM < 2)
              ap_inv_date,
           xcgi.CARRIER_NAME,
           xcgi.LINE_HAUL AS TRANSACTION_LINE_HAUL,
           xcgi.OTHER AS TRANSACTION_OTHER,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_DR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_CR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_NET,
           xcgi.LINE_HAUL AS EST_LINE_HAUL_CHARGE,
           xcgi.TMS_FSC AS EST_FSC,
           xcgi.TMS_ACCESSORIALS AS EST_ACCESSORIALS,
           xcgi.SHIPMENT_MODE,
           xcgi.MOVEMENT_TYPE,
           xcgi.ORIGIN_NAME,
           xcgi.DESTINATION_NAME,
           xcgi.ORIGIN_CITY,
           xcgi.ORIGIN_STATE,
           xcgi.ORIGIN_POSTAL_CODE,
           xcgi.DESTINATION_CITY,
           xcgi.DESTINATION_STATE,
           xcgi.DESTINATION_POSTAL_CODE,
           xcgi.AUDIT_MATCH,
           xcgi.SCHEDULED_PAY_DATE PAY_DATE
      FROM apps.gl_ledgers gle,
           apps.gl_je_batches gb,
           apps.gl_je_headers jh,
           apps.gl_je_lines jl,
           gl_period_statuses gps,
           apps.gl_code_combinations_kfv gcc,
           apps.gl_je_sources jes,
           apps.gl_je_categories jec,
           apps.gl_import_references ir,
           apps.xla_ae_lines xel,
           apps.xla_ae_headers xeh,
           apps.xla_distribution_links xdl,
           apps.rcv_receiving_sub_ledger rsl,
           apps.rcv_transactions rt,
           apps.rcv_shipment_lines rl,
           apps.mtl_system_items msi,
           apps.rcv_shipment_headers rh,
           apps.po_vendors pov,
           apps.po_vendor_sites povs,
           apps.po_headers_all poh,
           XXWC.XXWC_CASS_GL_INTERFACE_VW xcgi
     WHERE     1 = 1
           AND gle.name = 'HD Supply USD'
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND gps.ledger_id = gle.ledger_id
           AND jh.je_source = 'Cost Management'
           AND jh.je_category <> 'Inventory'
           AND gle.ledger_id = jh.ledger_id
           AND gb.je_batch_id = jh.je_batch_id
           AND jh.je_header_id = jl.je_header_id
           AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id
           AND jl.code_combination_id = gcc.code_combination_id
           AND jh.je_source = jes.je_source_name
           AND jh.je_category = jec.je_category_name
           AND ir.je_header_id = jl.je_header_id
           AND ir.je_line_num = jl.je_line_num
           AND ir.gl_sl_link_id = xel.gl_sl_link_id(+)
           AND ir.gl_sl_link_table = xel.gl_sl_link_table(+)
           AND xel.ae_header_id = xdl.ae_header_id(+)
           AND xel.ae_line_num = xdl.ae_line_num(+)
           AND xdl.application_id = 707
           AND xel.ae_header_id = xeh.ae_header_id(+)
           AND xdl.source_distribution_id_num_1 = rsl.rcv_sub_ledger_id(+)
           AND xdl.source_distribution_type(+) = 'RCV_RECEIVING_SUB_LEDGER'
           AND rsl.rcv_transaction_id = rt.transaction_id(+)
           AND rt.shipment_line_id = rl.shipment_line_id(+)
           AND rl.shipment_header_id = rh.shipment_header_id(+)
           AND poh.po_header_id(+) = rl.po_header_id
           AND rh.vendor_id = pov.vendor_id(+)
           AND rh.vendor_site_id = povs.vendor_site_id(+)
           AND msi.inventory_item_id(+) = rl.item_id
           AND msi.organization_id(+) = rl.to_organization_id
           AND xcgi.record_id(+) = jl.attribute1
    --
    -- end of JE source 'Cost Management' and JE Category other than "Inventory" -Part 3
    --
    UNION ALL
    --
    -- begin of JE source "Receivables" -Part 4
    --
    SELECT jes.user_je_source_name SOURCE,
           jh.je_category,
           jir.je_header_id je_header_id,
           jir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           NULL LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr line_ent_dr,
           jl.entered_cr line_ent_cr,
           jir.subledger_doc_sequence_id seq_id,
           jir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           jir.gl_sl_link_id gl_sl_link_id,
           --
           (CASE
               WHEN (source_distribution_type = 'AR_DISTRIBUTIONS_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          hzp.party_name
                     FROM hz_cust_accounts hzca, hz_parties hzp
                    WHERE     hzca.cust_account_id = dist.third_party_id
                          AND hzp.party_id = hzca.party_id)
               WHEN (source_distribution_type =
                        'RA_CUST_TRX_LINE_GL_DIST_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          hzp.party_name
                     FROM hz_cust_accounts hzca,
                          hz_parties hzp,
                          ra_customer_trx trx
                    WHERE     1 = 1
                          AND trx.customer_trx_id = ra_dist.customer_trx_id
                          AND hzca.cust_account_id = trx.bill_to_customer_id
                          AND hzp.party_id = hzca.party_id)
               ELSE
                  TO_CHAR (NULL)
            END)
              CUSTOMER_OR_VENDOR,
           --
           (CASE
               WHEN (source_distribution_type = 'AR_DISTRIBUTIONS_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          hzca.account_number
                     FROM hz_cust_accounts hzca
                    WHERE hzca.cust_account_id = dist.third_party_id)
               WHEN (source_distribution_type =
                        'RA_CUST_TRX_LINE_GL_DIST_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          hzca.account_number
                     FROM hz_cust_accounts hzca, ra_customer_trx trx
                    WHERE     1 = 1
                          AND trx.customer_trx_id = ra_dist.customer_trx_id
                          AND hzca.cust_account_id = trx.bill_to_customer_id)
               ELSE
                  TO_CHAR (NULL)
            END)
              CUSTOMER_OR_VENDOR_NUMBER,
           --
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.AMOUNT_DR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, NULL,
                                   NVL (ra_dist.amount, 0)),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, -NVL (ra_dist.amount, 0),
                            NULL))))
              Entered_DR,
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.AMOUNT_cR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, -NVL (ra_dist.amount, 0),
                                   NULL),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, NULL,
                            NVL (ra_dist.amount, 0)))))
              Entered_CR,
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.ACCTD_AMOUNT_DR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, NULL,
                                   NVL (ra_dist.acctd_amount, 0)),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, -NVL (ra_dist.acctd_amount, 0),
                            NULL))))
              Accounted_DR,
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.ACCTD_AMOUNT_CR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, -NVL (ra_dist.acctd_amount, 0),
                                   NULL),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, NULL,
                            NVL (ra_DIST.acctd_amount, 0)))))
              Accounted_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           xdl.unrounded_entered_cr sla_dist_entered_cr,
           xdl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (xdl.unrounded_entered_cr, 0)
           - NVL (xdl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           xdl.unrounded_accounted_cr sla_dist_accounted_cr,
           xdl.unrounded_accounted_dr sla_dist_accounted_dr,
             NVL (xdl.unrounded_accounted_dr, 0)
           - NVL (xdl.unrounded_accounted_cr, 0)
              sla_dist_accounted_net,
           TO_CHAR (NULL) associate_num,
           (CASE
               WHEN (    xdl.source_distribution_type =
                            'AR_DISTRIBUTIONS_ALL'
                     AND dist.source_table = 'CRH'
                     AND dist.source_type = 'CASH')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          cr.receipt_number
                     FROM ar_cash_receipt_history_all crh,
                          ar_cash_receipts_all cr
                    WHERE     1 = 1
                          AND crh.cash_receipt_history_id = dist.source_id
                          AND cr.cash_receipt_id = crh.cash_receipt_id)
               WHEN (    xdl.source_distribution_type =
                            'AR_DISTRIBUTIONS_ALL'
                     AND dist.source_table = 'RA'
                     AND dist.source_type IN ('EDISC', 'REC'))
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          trx_number
                     FROM ra_customer_trx trx, ra_customer_trx_lines trxlines
                    WHERE     1 = 1
                          AND trxlines.customer_trx_line_id =
                                 dist.ref_customer_trx_line_id
                          AND trx.customer_trx_id = trxlines.customer_trx_id)
               WHEN (xdl.source_distribution_type =
                        'RA_CUST_TRX_LINE_GL_DIST_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          trx_number
                     FROM ra_customer_trx
                    WHERE 1 = 1 AND customer_trx_id = ra_dist.customer_trx_id)
               ELSE
                  TO_CHAR (NULL)
            END)
              TRANSACTION_NUM,
           --
           (CASE
               WHEN (    xdl.source_distribution_type =
                            'AR_DISTRIBUTIONS_ALL'
                     AND dist.source_table = 'CRH'
                     AND dist.source_type = 'CASH')
               THEN
                  (TO_CHAR (NULL))
               WHEN (    xdl.source_distribution_type =
                            'AR_DISTRIBUTIONS_ALL'
                     AND dist.source_table = 'RA'
                     AND dist.source_type IN ('EDISC', 'REC'))
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          ct_reference
                     FROM ra_customer_trx trx, ra_customer_trx_lines trxlines
                    WHERE     1 = 1
                          AND trxlines.customer_trx_line_id =
                                 dist.ref_customer_trx_line_id
                          AND trx.customer_trx_id = trxlines.customer_trx_id)
               WHEN (xdl.source_distribution_type =
                        'RA_CUST_TRX_LINE_GL_DIST_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          ct_reference
                     FROM ra_customer_trx
                    WHERE 1 = 1 AND customer_trx_id = ra_dist.customer_trx_id)
               ELSE
                  TO_CHAR (NULL)
            END)
              SALES_ORDER,
           --
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           NVL (dist.source_type, NULL) TYPE,
           gps.effective_period_num,
           gle.name,
           gb.name batch_name,
           TO_CHAR (NULL) PO_NUMBER,
           TO_CHAR (NULL) BOL_NUMBER,
           TO_CHAR (NULL) AP_INV_SOURCE,
           xlae.event_type_code SLA_EVENT_TYPE,
           --
           CASE
              WHEN (    xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
                    AND dist.source_table = 'CRH'
                    AND dist.source_type = 'CASH')
              THEN
                 (TO_CHAR (NULL))
              WHEN (    xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
                    AND dist.source_table = 'RA'
                    AND dist.source_type IN ('EDISC', 'REC'))
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         mtlsi.segment1
                    FROM ra_customer_trx_lines trxlines,
                         mtl_system_items mtlsi
                   WHERE     1 = 1
                         AND trxlines.customer_trx_line_id =
                                dist.ref_customer_trx_line_id
                         AND mtlsi.inventory_item_id =
                                trxlines.inventory_item_id
                         AND mtlsi.organization_id = 222)
              WHEN (xdl.source_distribution_type =
                       'RA_CUST_TRX_LINE_GL_DIST_ALL')
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         mtlsi.segment1
                    FROM ra_customer_trx_lines trxlines,
                         mtl_system_items mtlsi
                   WHERE     1 = 1
                         AND trxlines.customer_trx_line_id =
                                ra_dist.customer_trx_line_id
                         AND mtlsi.inventory_item_id =
                                trxlines.inventory_item_id
                         AND mtlsi.organization_id = 222)
              ELSE
                 TO_CHAR (NULL)
           END
              PART_NUMBER,
           --
           CASE
              WHEN (    xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
                    AND dist.source_table = 'CRH'
                    AND dist.source_type = 'CASH')
              THEN
                 (TO_CHAR (NULL))
              WHEN (    xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
                    AND dist.source_table = 'RA'
                    AND dist.source_type IN ('EDISC', 'REC'))
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         mtlsi.description
                    FROM ra_customer_trx_lines trxlines,
                         mtl_system_items mtlsi
                   WHERE     1 = 1
                         AND trxlines.customer_trx_line_id =
                                dist.ref_customer_trx_line_id
                         AND mtlsi.inventory_item_id =
                                trxlines.inventory_item_id
                         AND mtlsi.organization_id = 222)
              WHEN (source_distribution_type = 'RA_CUST_TRX_LINE_GL_DIST_ALL')
              THEN
                 (SELECT /*+ RESULT_CACHE */
                         mtlsi.description
                    FROM ra_customer_trx_lines trxlines,
                         mtl_system_items mtlsi
                   WHERE     1 = 1
                         AND trxlines.customer_trx_line_id =
                                ra_dist.customer_trx_line_id
                         AND mtlsi.inventory_item_id =
                                trxlines.inventory_item_id
                         AND mtlsi.organization_id = 222)
              ELSE
                 TO_CHAR (NULL)
           END
              PART_DESCRIPTION,
           --
           TO_NUMBER (NULL) ITEM_UNIT_WT,
           TO_NUMBER (NULL) ITEM_TXN_QTY,
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST,
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST,
           --
           SUBSTR (jl.description, 1, 240) XXCUS_GSC_ENH_LINE_DESC,
           TO_CHAR (NULL) XXCUS_GSC_PO_CREATED_BY,
           TO_CHAR (NULL) XXCUS_GSC_IMAGE_LINK,
           --
           gcc.code_combination_id,                      --descrflexfieldstart
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           xdl.source_distribution_type,
           xdl.source_distribution_id_num_1,
           xdl.applied_to_source_id_num_1,
           4 FETCH_SEQ,
           TO_DATE (NULL) ap_inv_date,
           xcgi.CARRIER_NAME,
           xcgi.LINE_HAUL AS TRANSACTION_LINE_HAUL,
           xcgi.OTHER AS TRANSACTION_OTHER,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_DR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_CR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_NET,
           xcgi.LINE_HAUL AS EST_LINE_HAUL_CHARGE,
           xcgi.TMS_FSC AS EST_FSC,
           xcgi.TMS_ACCESSORIALS AS EST_ACCESSORIALS,
           xcgi.SHIPMENT_MODE,
           xcgi.MOVEMENT_TYPE,
           xcgi.ORIGIN_NAME,
           xcgi.DESTINATION_NAME,
           xcgi.ORIGIN_CITY,
           xcgi.ORIGIN_STATE,
           xcgi.ORIGIN_POSTAL_CODE,
           xcgi.DESTINATION_CITY,
           xcgi.DESTINATION_STATE,
           xcgi.DESTINATION_POSTAL_CODE,
           xcgi.AUDIT_MATCH,
           xcgi.SCHEDULED_PAY_DATE PAY_DATE
      FROM gl_ledgers gle,
           gl_je_headers jh,
           gl_je_lines jl,
           gl_code_combinations_kfv gcc,
           gl_import_references jir,
           xla_distribution_links xdl,
           xla_ae_lines ael,
           xla_ae_headers aeh,
           xla_events xlae,
           ar_distributions_all dist,
           ra_cust_trx_line_gl_dist_all ra_dist,
           gl_period_statuses gps,
           gl_je_batches gb,
           gl_je_sources jes,
           XXWC.XXWC_CASS_GL_INTERFACE_VW xcgi
     WHERE     jh.je_header_id = jl.je_header_id
           AND jl.code_combination_id = gcc.code_combination_id
           AND jh.je_header_id = jir.je_header_id
           AND jl.je_line_num = jir.je_line_num
           AND jh.ledger_id = gle.ledger_id
           AND jes.user_je_source_name = 'Receivables'
           AND gle.name = 'HD Supply USD'
           AND jir.gl_sl_link_id = ael.gl_sl_link_id(+)
           AND jir.gl_sl_link_table = ael.gl_sl_link_table(+)
           AND ael.ae_line_num = xdl.ae_line_num(+)
           AND ael.ae_header_id = xdl.ae_header_id(+)
           AND ael.ae_header_id = aeh.ae_header_id(+)
           AND xlae.application_id(+) = aeh.application_id
           AND xlae.event_id(+) = aeh.event_id
           AND dist.line_id(+) =
                  DECODE (
                     xdl.source_distribution_type,
                     'AR_DISTRIBUTIONS_ALL', xdl.source_distribution_id_num_1,
                     NULL)
           AND ra_dist.cust_trx_line_gl_dist_id(+) =
                  DECODE (xdl.source_distribution_type,
                          'AR_DISTRIBUTIONS_ALL', NULL,
                          xdl.source_distribution_id_num_1)
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND gps.ledger_id = gle.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
           AND jes.je_source_name = jh.je_source
           AND xcgi.record_id(+) = jl.attribute1
    --
    -- end of JE source "Receivables" -Part 4
    --
    UNION ALL
    --
    -- begin of JE source "Payables" and xla_distribution_links source_distribution_type "AP_INV_DIST" -Part 5
    --
    SELECT jes.user_je_source_name SOURCE,
           jh.je_category,
           gir.je_header_id je_header_id,
           gir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           ds.name LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr ent_dr,
           jl.entered_cr ent_cr,
           gir.subledger_doc_sequence_id seq_id,
           gir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           gir.gl_sl_link_id gl_sl_link_id,
           --
           (CASE
               WHEN SIGN (ai.vendor_id) = -1
               THEN
                  'No Supplier'
               ELSE
                  (SELECT vendor_name
                     FROM po_vendors
                    WHERE 1 = 1 AND vendor_id = ai.vendor_id)
            END)
              customer_or_vendor,
           --
           (CASE
               WHEN adl.applied_to_source_id_num_1 IS NOT NULL
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          aps.segment1
                     FROM ap_invoices_all apia, ap_suppliers aps
                    WHERE     1 = 1
                          AND apia.invoice_id =
                                 adl.applied_to_source_id_num_1
                          AND aps.vendor_id = apia.vendor_id)
               ELSE
                  NULL
            END)
              customer_or_vendor_number,
           --
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           adl.unrounded_entered_cr sla_dist_entered_cr,
           adl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (adl.unrounded_entered_cr, 0)
           - NVL (adl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           adl.unrounded_accounted_cr sla_dist_accounted_cr,
           adl.unrounded_accounted_dr sla_dist_accounted_dr,
             NVL (adl.unrounded_accounted_dr, 0)
           - NVL (adl.unrounded_accounted_cr, 0)
              sla_dist_accounted_net,
           '' associate_num,
           AI.INVOICE_NUM transaction_num,
           ai.attribute3 SALES_ORDER,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           ael.accounting_class_code TYPE,
           gps.effective_period_num,
           le.name,
           gb.name batch_name,
           CASE
              WHEN aid.po_distribution_id IS NULL THEN ai.attribute4
              ELSE poh.segment1
           END
              PO_NUMBER,
           AI.ATTRIBUTE6 BOL_NUMBER,
           AI.SOURCE AP_INV_SOURCE,
           xlae.event_type_code SLA_EVENT_TYPE,
           --
           (CASE
               WHEN adl.applied_to_dist_id_num_1 IS NOT NULL
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          mtlsi.segment1
                     FROM ap_invoice_distributions_all apid,
                          ap_invoice_lines_all apil,
                          mtl_system_items mtlsi
                    WHERE     1 = 1
                          AND apid.invoice_distribution_id =
                                 adl.applied_to_dist_id_num_1
                          AND apil.invoice_id = apid.invoice_id
                          AND apil.line_number = apid.invoice_line_number
                          AND mtlsi.inventory_item_id =
                                 apil.inventory_item_id
                          AND mtlsi.organization_id = 222)
               ELSE
                  NULL
            END)
              PART_NUMBER,
           --
           (CASE
               WHEN adl.applied_to_dist_id_num_1 IS NOT NULL
               THEN
                  (SELECT /*+ RESULT_CACHE */
                          mtlsi.description
                     FROM ap_invoice_distributions_all apid,
                          ap_invoice_lines_all apil,
                          mtl_system_items mtlsi
                    WHERE     1 = 1
                          AND apid.invoice_distribution_id =
                                 adl.applied_to_dist_id_num_1
                          AND apil.invoice_id = apid.invoice_id
                          AND apil.line_number = apid.invoice_line_number
                          AND mtlsi.inventory_item_id =
                                 apil.inventory_item_id
                          AND mtlsi.organization_id = 222)
               ELSE
                  NULL
            END)
              PART_DESCRIPTION,
           --
           (SELECT msi.unit_weight
              FROM po_lines pol, mtl_system_items msi
             WHERE     1 = 1
                   AND pol.po_line_id = pod.po_line_id
                   AND msi.inventory_item_id = pol.item_id
                   AND msi.organization_id = pod.destination_organization_id)
              ITEM_UNIT_WT,
           aid.quantity_invoiced ITEM_TXN_QTY,
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST,
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST,
           (SELECT vendor_name
              FROM po_vendors
             WHERE     1 = 1
                   AND SIGN (ai.vendor_id) <> -1
                   AND vendor_id = ai.vendor_id)
              XXCUS_GSC_ENH_LINE_DESC,
           (SELECT D.DESCRIPTION            --User who created the Requisition
              FROM FND_USER D, PO_REQ_DISTRIBUTIONS_ALL B
             WHERE     1 = 1
                   AND B.DISTRIBUTION_ID = pod.REQ_DISTRIBUTION_ID
                   AND D.USER_ID = B.CREATED_BY
                   AND ROWNUM = 1)
              XXCUS_GSC_PO_CREATED_BY,
           (SELECT '''' || url image_link
              FROM fnd_attached_docs_form_vl
             WHERE     1 = 1
                   AND ai.org_id IN (163, 167)
                   AND entity_name = 'AP_INVOICES'
                   AND pk1_value = TO_CHAR (ai.invoice_id)
                   AND datatype_name = 'Web Page'
                   AND category_description = 'Invoice Internal'
                   AND document_description = 'Documentum Image URL'
                   AND function_name = 'APXINWKB'
                   AND ROWNUM < 2)
              XXCUS_GSC_IMAGE_LINK,
           --
           gcc.code_combination_id,                      --descrflexfieldstart
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           adl.source_distribution_type,
           adl.source_distribution_id_num_1,
           adl.applied_to_source_id_num_1,
           5 FETCH_SEQ,
           TRUNC (ai.invoice_date) ap_inv_date,
           xcgi.CARRIER_NAME,
           xcgi.LINE_HAUL AS TRANSACTION_LINE_HAUL,
           xcgi.OTHER AS TRANSACTION_OTHER,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_DR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_CR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_NET,
           xcgi.LINE_HAUL AS EST_LINE_HAUL_CHARGE,
           xcgi.TMS_FSC AS EST_FSC,
           xcgi.TMS_ACCESSORIALS AS EST_ACCESSORIALS,
           xcgi.SHIPMENT_MODE,
           xcgi.MOVEMENT_TYPE,
           xcgi.ORIGIN_NAME,
           xcgi.DESTINATION_NAME,
           xcgi.ORIGIN_CITY,
           xcgi.ORIGIN_STATE,
           xcgi.ORIGIN_POSTAL_CODE,
           xcgi.DESTINATION_CITY,
           xcgi.DESTINATION_STATE,
           xcgi.DESTINATION_POSTAL_CODE,
           xcgi.AUDIT_MATCH,
           xcgi.SCHEDULED_PAY_DATE PAY_DATE
      FROM gl_ledgers le,
           gl_je_headers jh,
           gl_je_lines jl,
           gl_je_sources jes,
           gl_import_references gir,
           gl_code_combinations_kfv gcc,
           xla_ae_lines ael,
           gl_code_combinations_kfv gcc1,
           xla_ae_headers aeh,
           xla_events xlae,
           xla_distribution_links adl,
           AP_INVOICES_ALL AI,
           AP_INVOICE_DISTRIBUTIONS_ALL AID,
           gl_period_statuses gps,
           FND_DOCUMENT_SEQUENCES ds,
           gl_je_batches gb,
           po_distributions_all pod,
           po_headers_all poh,
           XXWC.XXWC_CASS_GL_INTERFACE_VW xcgi
     WHERE     le.ledger_id = jh.ledger_id
           AND jh.je_header_id = jl.je_header_id
           AND jes.je_source_name = jh.je_source
           AND gir.je_header_id = jh.je_header_id
           AND gir.je_line_num = jl.je_line_num
           AND jl.code_combination_id = gcc.code_combination_id
           AND gir.gl_sl_link_id = ael.gl_sl_link_id(+)
           AND gir.gl_sl_link_table = ael.gl_sl_link_table(+)
           AND ael.ae_header_id = aeh.ae_header_id(+)
           AND xlae.application_id(+) = aeh.application_id
           AND xlae.event_id(+) = aeh.event_id
           AND ael.code_combination_id = gcc1.code_combination_id(+)
           AND ael.ae_header_id = adl.ae_header_id(+)
           AND ael.ae_line_num = adl.ae_line_num(+)
           AND adl.SOURCE_DISTRIBUTION_TYPE = 'AP_INV_DIST'
           AND adl.SOURCE_DISTRIBUTION_ID_NUM_1 = AID.INVOICE_DISTRIBUTION_ID
           AND AID.INVOICE_ID = AI.INVOICE_ID
           AND jes.user_je_source_name = 'Payables'
           AND le.name = 'HD Supply USD'
           AND le.object_type_code = 'L'
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND ds.doc_sequence_id(+) = gir.subledger_doc_sequence_id
           AND gps.ledger_id = le.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
           AND pod.po_distribution_id(+) = aid.po_distribution_id
           AND poh.po_header_id(+) = pod.po_header_id
           AND xcgi.record_id(+) = jl.attribute1
    --
    -- end of JE source "Payables" and xla_distribution_links source_distribution_type "AP_INV_DIST" -Part 5
    --
    UNION ALL
    --
    -- begin of JE source "Payables" and xla_distribution_links source_distribution_type "AP_PMT_DIST" -Part 6
    --
    SELECT jes.user_je_source_name SOURCE,
           jh.je_category,
           gir.je_header_id je_header_id,
           gir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           ds.name LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr ent_dr,
           jl.entered_cr ent_cr,
           gir.subledger_doc_sequence_id seq_id,
           gir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           gir.gl_sl_link_id gl_sl_link_id,
           (CASE
               WHEN SIGN (ai.vendor_id) = -1
               THEN
                  'No Supplier'
               ELSE
                  (SELECT vendor_name
                     FROM po_vendors
                    WHERE 1 = 1 AND vendor_id = ai.vendor_id)
            END)
              customer_or_vendor,
           TO_CHAR (NULL) customer_or_vendor_number,
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           adl.unrounded_entered_cr sla_dist_entered_cr,
           adl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (adl.unrounded_entered_cr, 0)
           - NVL (adl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           adl.unrounded_accounted_cr sla_dist_accounted_cr,
           ADL.UNROUNDED_ACCOUNTED_DR SLA_DIST_ACCOUNTED_DR,
             NVL (adl.unrounded_accounted_dr, 0)
           - NVL (adl.unrounded_accounted_cr, 0)
              sla_dist_accounted_net,
           TO_CHAR (
              DECODE (adl.SOURCE_DISTRIBUTION_TYPE,
                      'AP_PMT_DIST', AC.CHECK_NUMBER,
                      ''))
              associate_num,
           AI.INVOICE_NUM transaction_num,
           ai.attribute3 SALES_ORDER,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           ael.accounting_class_code TYPE,
           gps.effective_period_num,
           le.name,
           gb.name batch_name,
           CASE
              WHEN aid.po_distribution_id IS NULL THEN ai.attribute4
              ELSE poh.segment1
           END
              PO_NUMBER,
           AI.ATTRIBUTE6 BOL_NUMBER,
           AI.SOURCE AP_INV_SOURCE,
           xlae.event_type_code SLA_EVENT_TYPE,
           --
           TO_CHAR (NULL) PART_NUMBER,
           TO_CHAR (NULL) PART_DESCRIPTION,
           --
           TO_NUMBER (NULL) ITEM_UNIT_WT,
           TO_NUMBER (NULL) ITEM_TXN_QTY,
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST,
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST,
           (SELECT vendor_name
              FROM po_vendors
             WHERE     1 = 1
                   AND SIGN (ai.vendor_id) <> -1
                   AND vendor_id = ai.vendor_id)
              XXCUS_GSC_ENH_LINE_DESC,
           (SELECT D.DESCRIPTION            --User who created the Requisition
              FROM FND_USER D, PO_REQ_DISTRIBUTIONS_ALL B
             WHERE     1 = 1
                   AND B.DISTRIBUTION_ID = pod.REQ_DISTRIBUTION_ID
                   AND D.USER_ID = B.CREATED_BY
                   AND ROWNUM = 1)
              XXCUS_GSC_PO_CREATED_BY,
           (SELECT '''' || url image_link
              FROM fnd_attached_docs_form_vl
             WHERE     1 = 1
                   AND ai.org_id IN (163, 167)
                   AND entity_name = 'AP_INVOICES'
                   AND pk1_value = TO_CHAR (ai.invoice_id)
                   AND datatype_name = 'Web Page'
                   AND category_description = 'Invoice Internal'
                   AND document_description = 'Documentum Image URL'
                   AND function_name = 'APXINWKB'
                   AND ROWNUM < 2)
              XXCUS_GSC_IMAGE_LINK,
           --
           gcc.code_combination_id,                      --descrflexfieldstart
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           adl.source_distribution_type,
           adl.source_distribution_id_num_1,
           adl.applied_to_source_id_num_1,
           6 FETCH_SEQ,
           TRUNC (ai.invoice_date) ap_inv_date,
           xcgi.CARRIER_NAME,
           xcgi.LINE_HAUL AS TRANSACTION_LINE_HAUL,
           xcgi.OTHER AS TRANSACTION_OTHER,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_DR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_CR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_NET,
           xcgi.LINE_HAUL AS EST_LINE_HAUL_CHARGE,
           xcgi.TMS_FSC AS EST_FSC,
           xcgi.TMS_ACCESSORIALS AS EST_ACCESSORIALS,
           xcgi.SHIPMENT_MODE,
           xcgi.MOVEMENT_TYPE,
           xcgi.ORIGIN_NAME,
           xcgi.DESTINATION_NAME,
           xcgi.ORIGIN_CITY,
           xcgi.ORIGIN_STATE,
           xcgi.ORIGIN_POSTAL_CODE,
           xcgi.DESTINATION_CITY,
           xcgi.DESTINATION_STATE,
           xcgi.DESTINATION_POSTAL_CODE,
           xcgi.AUDIT_MATCH,
           xcgi.SCHEDULED_PAY_DATE PAY_DATE
      FROM gl_ledgers le,
           gl_je_headers jh,
           gl_je_lines jl,
           GL_JE_SOURCES JES,
           gl_import_references gir,
           gl_code_combinations_kfv gcc,
           xla_ae_lines ael,
           gl_code_combinations_kfv gcc1,
           xla_ae_headers aeh,
           xla_events xlae,
           xla_distribution_links adl,
           ap_payment_hist_dists APHD,
           AP_INVOICES_ALL AI,
           AP_INVOICE_DISTRIBUTIONS_ALL AID,
           AP_CHECKS_ALL AC,
           AP_INVOICE_PAYMENTS_ALL AIP,
           gl_period_statuses gps,
           FND_DOCUMENT_SEQUENCES ds,
           gl_je_batches gb,
           po_distributions_all pod,
           po_headers_all poh,
           XXWC.XXWC_CASS_GL_INTERFACE_VW xcgi
     WHERE     le.ledger_id = jh.ledger_id
           AND jh.je_header_id = jl.je_header_id
           AND jes.je_source_name = jh.je_source
           AND gir.je_header_id = jh.je_header_id
           AND gir.je_line_num = jl.je_line_num
           AND jl.code_combination_id = gcc.code_combination_id
           AND gir.gl_sl_link_id = ael.gl_sl_link_id(+)
           AND gir.gl_sl_link_table = ael.gl_sl_link_table(+)
           AND ael.ae_header_id = aeh.ae_header_id(+)
           AND xlae.application_id(+) = aeh.application_id
           AND xlae.event_id(+) = aeh.event_id
           AND ael.code_combination_id = gcc1.code_combination_id(+)
           AND ael.ae_header_id = adl.ae_header_id(+)
           AND ael.ae_line_num = adl.ae_line_num(+)
           AND adl.SOURCE_DISTRIBUTION_TYPE = 'AP_PMT_DIST'
           AND adl.SOURCE_DISTRIBUTION_ID_NUM_1 = APHD.PAYMENT_HIST_DIST_ID
           AND APHD.INVOICE_PAYMENT_ID = AIP.INVOICE_PAYMENT_ID
           AND AIP.CHECK_ID = AC.CHECK_ID(+)
           AND AID.INVOICE_ID = AI.INVOICE_ID
           AND APHD.INVOICE_DISTRIBUTION_ID = AID.INVOICE_DISTRIBUTION_ID
           AND jes.user_je_source_name = 'Payables'
           AND le.name = 'HD Supply USD'
           AND le.object_type_code = 'L'
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND ds.doc_sequence_id(+) = gir.subledger_doc_sequence_id
           AND gps.ledger_id = le.ledger_id
           AND jh.je_batch_id = gb.je_batch_id
           AND pod.po_distribution_id(+) = aid.po_distribution_id
           AND poh.po_header_id(+) = pod.po_header_id
           AND xcgi.record_id(+) = jl.attribute1
    --
    -- end of JE source "Payables" and xla_distribution_links source_distribution_type "AP_PMT_DIST" -Part 6
    --
    UNION ALL
    --
    -- begin of JE source "Payables" and xla_distribution_links source_distribution_type "AP_PREPAY" -Part 7
    --
    SELECT jes.user_je_source_name SOURCE,
           jh.je_category,
           gir.je_header_id je_header_id,
           gir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           ds.name LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr ent_dr,
           jl.entered_cr ent_cr,
           gir.subledger_doc_sequence_id seq_id,
           gir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           gir.gl_sl_link_id gl_sl_link_id,
           (CASE
               WHEN SIGN (ai.vendor_id) = -1
               THEN
                  'No Supplier'
               ELSE
                  (SELECT vendor_name
                     FROM po_vendors
                    WHERE 1 = 1 AND vendor_id = ai.vendor_id)
            END)
              customer_or_vendor,
           TO_CHAR (NULL) customer_or_vendor_number,
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           adl.unrounded_entered_cr sla_dist_entered_cr,
           adl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (adl.unrounded_entered_cr, 0)
           - NVL (adl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           adl.unrounded_accounted_cr sla_dist_accounted_cr,
           adl.unrounded_accounted_dr sla_dist_accounted_dr,
             NVL (adl.unrounded_accounted_dr, 0)
           - NVL (adl.unrounded_accounted_cr, 0)
              sla_dist_accounted_net,
           '' associate_num,
           AI.INVOICE_NUM transaction_num,
           ai.attribute3 SALES_ORDER,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           ael.accounting_class_code TYPE,
           gps.effective_period_num,
           le.name,
           gb.name batch_name,
           CASE
              WHEN aid.po_distribution_id IS NULL THEN ai.attribute4
              ELSE poh.segment1
           END
              PO_NUMBER,
           AI.ATTRIBUTE6 BOL_NUMBER,
           AI.SOURCE AP_INV_SOURCE,
           xlae.event_type_code SLA_EVENT_TYPE,
           --
           TO_CHAR (NULL) PART_NUMBER,
           TO_CHAR (NULL) PART_DESCRIPTION,
           --
           TO_NUMBER (NULL) ITEM_UNIT_WT,
           TO_NUMBER (NULL) ITEM_TXN_QTY,
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST,
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST,
           (SELECT vendor_name
              FROM po_vendors
             WHERE     1 = 1
                   AND SIGN (ai.vendor_id) <> -1
                   AND vendor_id = ai.vendor_id)
              XXCUS_GSC_ENH_LINE_DESC,
           (SELECT D.DESCRIPTION            --User who created the Requisition
              FROM FND_USER D, PO_REQ_DISTRIBUTIONS_ALL B
             WHERE     1 = 1
                   AND B.DISTRIBUTION_ID = pod.REQ_DISTRIBUTION_ID
                   AND D.USER_ID = B.CREATED_BY
                   AND ROWNUM = 1)
              XXCUS_GSC_PO_CREATED_BY,
           (SELECT '''' || url image_link
              FROM fnd_attached_docs_form_vl
             WHERE     1 = 1
                   AND ai.org_id IN (163, 167)
                   AND entity_name = 'AP_INVOICES'
                   AND pk1_value = TO_CHAR (ai.invoice_id)
                   AND datatype_name = 'Web Page'
                   AND category_description = 'Invoice Internal'
                   AND document_description = 'Documentum Image URL'
                   AND function_name = 'APXINWKB'
                   AND ROWNUM < 2)
              XXCUS_GSC_IMAGE_LINK,
           --
           gcc.code_combination_id,                      --descrflexfieldstart
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           adl.source_distribution_type,
           adl.source_distribution_id_num_1,
           adl.applied_to_source_id_num_1,
           7 FETCH_SEQ,
           TRUNC (ai.invoice_date) ap_inv_date,
           xcgi.CARRIER_NAME,
           xcgi.LINE_HAUL AS TRANSACTION_LINE_HAUL,
           xcgi.OTHER AS TRANSACTION_OTHER,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_DR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_CR,
           xcgi.SHIPMENT_ESTIMATE AS EST_TRANSACTION_NET,
           xcgi.LINE_HAUL AS EST_LINE_HAUL_CHARGE,
           xcgi.TMS_FSC AS EST_FSC,
           xcgi.TMS_ACCESSORIALS AS EST_ACCESSORIALS,
           xcgi.SHIPMENT_MODE,
           xcgi.MOVEMENT_TYPE,
           xcgi.ORIGIN_NAME,
           xcgi.DESTINATION_NAME,
           xcgi.ORIGIN_CITY,
           xcgi.ORIGIN_STATE,
           xcgi.ORIGIN_POSTAL_CODE,
           xcgi.DESTINATION_CITY,
           xcgi.DESTINATION_STATE,
           xcgi.DESTINATION_POSTAL_CODE,
           xcgi.AUDIT_MATCH,
           xcgi.SCHEDULED_PAY_DATE PAY_DATE
      FROM gl_ledgers le,
           gl_je_headers jh,
           gl_je_lines jl,
           gl_je_sources jes,
           gl_import_references gir,
           gl_code_combinations_kfv gcc,
           xla_ae_lines ael,
           gl_code_combinations_kfv gcc1,
           xla_ae_headers aeh,
           xla_events xlae,
           xla_distribution_links adl,
           ap_prepay_app_dists apad,
           AP_INVOICES_ALL AI,
           AP_INVOICE_DISTRIBUTIONS_ALL AID,
           gl_period_statuses gps,
           FND_DOCUMENT_SEQUENCES ds,
           gl_je_batches gb,
           po_distributions_all pod,
           po_headers_all poh,
           XXWC.XXWC_CASS_GL_INTERFACE_VW xcgi
     WHERE     le.ledger_id = jh.ledger_id
           AND jh.je_header_id = jl.je_header_id
           AND jes.je_source_name = jh.je_source
           AND gir.je_header_id = jh.je_header_id
           AND gir.je_line_num = jl.je_line_num
           AND jl.code_combination_id = gcc.code_combination_id
           AND gir.gl_sl_link_id = ael.gl_sl_link_id(+)
           AND gir.gl_sl_link_table = ael.gl_sl_link_table(+)
           AND ael.ae_header_id = aeh.ae_header_id(+)
           AND xlae.application_id(+) = aeh.application_id
           AND xlae.event_id(+) = aeh.event_id
           AND ael.code_combination_id = gcc1.code_combination_id(+)
           AND ael.ae_header_id = adl.ae_header_id(+)
           AND ael.ae_line_num = adl.ae_line_num(+)
           AND adl.SOURCE_DISTRIBUTION_TYPE = 'AP_PREPAY'
           AND adl.SOURCE_DISTRIBUTION_ID_NUM_1 = apad.prepay_app_dist_id
           AND apad.invoice_distribution_id = aid.invoice_distribution_id
           AND AID.INVOICE_ID = AI.INVOICE_ID
           AND jes.user_je_source_name = 'Payables'
           AND le.name = 'HD Supply USD'
           AND le.object_type_code = 'L'
           AND gps.application_id = 101
           AND gps.period_name = jh.period_name
           AND ds.doc_sequence_id(+) = gir.subledger_doc_sequence_id
           AND gps.ledger_id = le.ledger_id
           AND JH.JE_BATCH_ID = GB.JE_BATCH_ID
           AND pod.po_distribution_id(+) = aid.po_distribution_id
           AND poh.po_header_id(+) = pod.po_header_id
           AND xcgi.record_id(+) = jl.attribute1);