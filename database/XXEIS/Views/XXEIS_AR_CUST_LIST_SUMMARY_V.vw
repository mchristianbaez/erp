CREATE OR REPLACE VIEW XXEIS.XXEIS_AR_CUST_LIST_SUMMARY_V (CUSTOMER_NAME, CUSTOMER_NUMBER, SITE_USE, ADDRESS_ID, ADDRESS_LINE_1, CITY, STATE, ZIP_CODE, CUST_STATUS, CUSTOMER_ID, ORIG_SYSTEM_REFERENCE, CUSTOMER_CLASS_CODE, CUSTOMER_TYPE, TAX_CODE, FOB_POINT, SHIP_VIA, PRICE_LIST_ID, FREIGHT_TERM, ORDER_TYPE_ID, SALES_CHANNEL_CODE, WAREHOUSE_ID, PARTY_ID, PARTY_NUMBER, PARTY_TYPE, CUSTOMER_KEY, CUSTOMER_CATEGORY_CODE, TAX_REFERENCE, TAXPAYER_ID, EMPLOYEES_TOTAL, POTENTIAL_REVENUE_CURR_FY, POTENTIAL_REVENUE_NEXT_FY, FISCAL_YEAREND_MONTH, YEAR_ESTABLISHED, ANALYSIS_FY, MISSION_STATEMENT, SIC_CODE_TYPE, COMPETITOR_FLAG, REFERENCE_USE_FLAG, THIRD_PARTY_FLAG, PERSON_PRE_NAME_ADJUNCT, PERSON_FIRST_NAME, PERSON_MIDDLE_NAME, PERSON_LAST_NAME, PERSON_SUFFIX, PARTY_SITE_NUMBER, IDENTIFYING_ADDRESS_FLAG, COMPANY_NAME, FUNCTIONAL_CURRENCY, FUNCTIONAL_CURRENCY_PRECISION, COA_ID, ORG_ID, OPERATING_UNIT, PARTY_NAME, ACCOUNT_NAME, SITE_USES_TAX_CODE, CUST_ACCOUNT_ID, SITE_USE_ID, CUST_ACCT_SITE_ID,
  PARTY_SITE_ID, LOCATION_ID, COPYRIGHT, CUSTOMER_SITE_CLAS, CREATED_BY, PRIMARY_SALESREP_ID, CREATION_DATE, AUTHORIZED_BUYER_REQUIR, CUST_NOTE_LINE, CUST#PARTY_TYPE, CUST#LEGAL_PLACEMENT, CUST#AUTO_APPLY_CREDIT_MEMO, CUST#VNDR_CODE_AND_FRULOC, CUST#BRANCH_DESCRIPTION, CUST#CUSTOMER_SOURCE, CUST#LEGAL_COLLECTION_INDICA, CUST#PRISM_NUMBER, CUST#AUTHORIZED_BUYER_REQUIR, CUST#AUTHORIZED_BUYER_NOTES, CUST#PREDOMINANT_TRADE, CUST#YES#NOTE_LINE_#5, CUST#YES#NOTE_LINE_#1, CUST#YES#NOTE_LINE_#2, CUST#YES#NOTE_LINE_#3, CUST#YES#NOTE_LINE_#4, ACCT_SITE#PRINT_PRICES_ON_OR, ACCT_SITE#JOINT_CHECK_AGREEM, ACCT_SITE#OWNER_FINANCED, ACCT_SITE#JOB_INFORMATION_ON, ACCT_SITE#TAX_EXEMPTION_TYPE, ACCT_SITE#TAX_EXEMPT1, ACCT_SITE#PRISM_NUMBER, ACCT_SITE#MANDATORY_PO_NUMBE, ACCT_SITE#LIEN_RELEASE_DATE, ACCT_SITE#YES#NOTICE_TO_OWNE, ACCT_SITE#YES#NOTICE_TO_OWNE1, ACCT_SITE#YES#NOTICE_TO_OWNE2, SITE_USES#CUSTOMER_SITE_CLAS, SITE_USES#B2B_ADDRESS, SITE_USES#B2B_SHIPPING_WAREH, SITE_USES#GOVERNMENT_FUNDED,
  SITE_USES#THOMAS_GUIDE_PAGE, SITE_USES#DODGE_NUMBER, SITE_USES#FUTURE_USE, SITE_USES#SALESREP_#2, SITE_USES#SALESREP_SPILT_#1, SITE_USES#SALESREP_SPILT_#2, SITE_USES#PERMIT_NUMBER, LOC#PAY_TO_VENDOR_ID, LOC#LOB, LOC#PAY_TO_VENDOR_CODE, PARTY#101#PARTY_TYPE, PARTY#101#INTERCOMPANY_RECEI, PARTY#101#COOP_OVERRIDE, PARTY#101#PAYMENT_OVERRIDE, PARTY#101#DEFAULT_PAYMENT_AC, PARTY#101#DEFAULT_PAYMENT_LO, PARTY#101#COLLECTOR, PARTY#101#DEFAULT_PRODUCT_SE, PARTY#101#DEFAULT_LOCATION_S, PARTY#101#DEFAULT_COOP_ACCOU, PARTY#101#DEFAULT_REBATE_ACC, PARTY#101#DEFAULT_COST_CENTE, PARTY#101#INTERCOMPANY__PAYA, PARTY#102#PARTY_TYPE, PARTY#102#INTERCOMPANY_RECEI, PARTY#102#COOP_OVERRIDE, PARTY#102#PAYMENT_OVERRIDE, PARTY#102#DEFAULT_PAYMENT_AC, PARTY#102#DEFAULT_PAYMENT_LO, PARTY#102#COLLECTOR, PARTY#102#DEFAULT_PRODUCT_SE, PARTY#102#DEFAULT_LOCATION_S, PARTY#102#DEFAULT_COOP_ACCOU, PARTY#102#DEFAULT_REBATE_ACC, PARTY#102#DEFAULT_COST_CENTE, PARTY#102#INTERCOMPANY_PAYAB, PARTY_SITE#REBT_PAY_TO_VNDR_
  , PARTY_SITE#REBT_LOB, PARTY_SITE#REBT_VNDR_CODE, PARTY_SITE#REBT_VNDR_FLAG, PARTY_SITE#REBT_VNDR_TAX_ID_)
AS
  SELECT SUBSTRB (party.party_name, 1, 50) customer_name,
    cust.account_number customer_number,
    SUBSTRB (look.meaning, 1, 8) site_use,
    acct_site.cust_acct_site_id address_id,
    SUBSTRB (loc.address1, 1, 30) address_line_1,
    SUBSTRB (loc.city, 1, 15) city,
    SUBSTRB (loc.state, 1, 2) state,
    SUBSTRB (loc.postal_code, 1, 10) zip_code,
    cust.status cust_status,
    cust.cust_account_id customer_id,
    cust.orig_system_reference orig_system_reference,
    cust.customer_class_code customer_class_code,
    cust.customer_type customer_type,
    cust.tax_code tax_code,
    cust.fob_point fob_point,
    cust.ship_via ship_via,
    cust.price_list_id price_list_id,
    cust.freight_term freight_term,
    cust.order_type_id order_type_id,
    cust.sales_channel_code sales_channel_code,
    cust.warehouse_id warehouse_id,
    party.party_id,
    party.party_number,
    party.party_type,
    party.customer_key customer_key,
    party.category_code customer_category_code,
    party.tax_reference tax_reference,
    party.jgzz_fiscal_code taxpayer_id,
    party.employees_total,
    DECODE (party.party_type, 'ORGANIZATION', party.curr_fy_potential_revenue, TO_NUMBER (NULL) ) potential_revenue_curr_fy,
    DECODE (party.party_type, 'ORGANIZATION', party.next_fy_potential_revenue, TO_NUMBER (NULL) ) potential_revenue_next_fy,
    DECODE (party.party_type, 'ORGANIZATION', party.fiscal_yearend_month, NULL ) fiscal_yearend_month,
    DECODE (party.party_type, 'ORGANIZATION', party.year_established, TO_NUMBER (NULL) ) year_established,
    DECODE (party.party_type, 'ORGANIZATION', party.analysis_fy, NULL ) analysis_fy,
    DECODE (party.party_type, 'ORGANIZATION', party.mission_statement, NULL ) mission_statement,
    DECODE (party.party_type, 'ORGANIZATION', party.sic_code_type, NULL ) sic_code_type,
    party.competitor_flag competitor_flag,
    party.reference_use_flag reference_use_flag,
    party.third_party_flag third_party_flag,
    party.person_pre_name_adjunct,
    party.person_first_name,
    party.person_middle_name,
    party.person_last_name,
    party.person_name_suffix person_suffix,
    party_site.party_site_number,
    party_site.identifying_address_flag,
    (SELECT sob.NAME company_name
    FROM gl_sets_of_books sob,
      ar_system_parameters param,
      fnd_currencies cur
    WHERE sob.set_of_books_id = param.set_of_books_id
    AND sob.currency_code     = cur.currency_code
    AND sob.set_of_books_id   = fnd_profile.VALUE ('GL_SET_OF_BKS_ID')
    ) company_name,
    (SELECT sob.currency_code
    FROM gl_sets_of_books sob,
      ar_system_parameters param,
      fnd_currencies cur
    WHERE sob.set_of_books_id                                          = param.set_of_books_id
    AND SOB.CURRENCY_CODE                                              = CUR.CURRENCY_CODE
    AND xxeis.eis_gl_security_pkg.validate_access(sob.set_of_books_id) = 'TRUE'
    ) functional_currency,
    (SELECT cur.PRECISION
    FROM gl_sets_of_books sob,
      ar_system_parameters param,
      fnd_currencies cur
    WHERE sob.set_of_books_id                                          = param.set_of_books_id
    AND SOB.CURRENCY_CODE                                              = CUR.CURRENCY_CODE
    AND xxeis.eis_gl_security_pkg.validate_access(sob.set_of_books_id) = 'TRUE'
    ) functional_currency_precision,
    (SELECT sob.chart_of_accounts_id coa_id
    FROM gl_sets_of_books sob,
      ar_system_parameters param,
      fnd_currencies cur
    WHERE sob.set_of_books_id                                          = param.set_of_books_id
    AND SOB.CURRENCY_CODE                                              = CUR.CURRENCY_CODE
    AND xxeis.eis_gl_security_pkg.validate_access(sob.set_of_books_id) = 'TRUE'
    ) COA_ID,
    acct_site.org_id,
    HOU.NAME OPERATING_UNIT,
    PARTY.PARTY_NAME PARTY_NAME,
    cust.account_name,
    site_uses.tax_code SITE_USES_TAX_CODE,
    --Primary Keys
    cust.cust_account_id,
    site_uses.site_use_id,
    acct_site.cust_acct_site_id,
    party_site.party_site_id,
    loc.location_id ,
    'Copyright(c) 2001-'
    ||TO_CHAR(SYSDATE,'YYYY')
    ||' '
    ||'EiS Technologies Inc. All rights reserved.' Copyright,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_CUSTOMER_SITE_CLASS',SITE_USES.ATTRIBUTE1,'F') Customer_Site_Clas,
    cust.created_by,
    cust.PRIMARY_SALESREP_ID,
    TRUNC(cust.creation_date) creation_date,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',CUST.ATTRIBUTE7,'F') Authorized_Buyer_Requir ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE18, NULL) CUST_Note_Line
    --descr#flexfield#start
    ,
    xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',CUST.ATTRIBUTE1,'I') CUST#Party_Type ,
    CUST.ATTRIBUTE10 CUST#Legal_Placement ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_ON_OFF',CUST.ATTRIBUTE11,'F') CUST#Auto_Apply_Credit_Memo ,
    CUST.ATTRIBUTE2 CUST#Vndr_Code_and_FRULOC ,
    CUST.ATTRIBUTE3 CUST#Branch_Description ,
    CUST.ATTRIBUTE4 CUST#Customer_Source ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_LEGAL_COLLECTION',CUST.ATTRIBUTE5,'F') CUST#Legal_Collection_Indica ,
    CUST.ATTRIBUTE6 CUST#Prism_Number ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',CUST.ATTRIBUTE7,'F') CUST#Authorized_Buyer_Requir ,
    CUST.ATTRIBUTE8 CUST#Authorized_Buyer_Notes ,
    xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PREDOMINANT_TRADE',CUST.ATTRIBUTE9,'I') CUST#Predominant_Trade ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE16, NULL) CUST#Yes#Note_Line_#5 ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE17, NULL) CUST#Yes#Note_Line_#1 ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE18, NULL) CUST#Yes#Note_Line_#2 ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE19, NULL) CUST#Yes#Note_Line_#3 ,
    DECODE(CUST.ATTRIBUTE_CATEGORY ,'Yes',CUST.ATTRIBUTE20, NULL) CUST#Yes#Note_Line_#4 ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_PRINT_PRICES',ACCT_SITE.ATTRIBUTE1,'F') ACCT_SITE#Print_Prices_on_Or ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',ACCT_SITE.ATTRIBUTE12,'F') ACCT_SITE#Joint_Check_Agreem ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',ACCT_SITE.ATTRIBUTE13,'F') ACCT_SITE#Owner_Financed ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',ACCT_SITE.ATTRIBUTE14,'F') ACCT_SITE#Job_Information_on ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_TAX_EXEMPTION_TYPE',ACCT_SITE.ATTRIBUTE15,'I') ACCT_SITE#Tax_Exemption_Type ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_TAX_EXEMPT_CODES',ACCT_SITE.ATTRIBUTE16,'I') ACCT_SITE#Tax_Exempt1 ,
    ACCT_SITE.ATTRIBUTE17 ACCT_SITE#PRISM_Number ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',ACCT_SITE.ATTRIBUTE3,'F') ACCT_SITE#Mandatory_PO_Numbe ,
    ACCT_SITE.ATTRIBUTE5 ACCT_SITE#Lien_Release_Date ,
    DECODE(ACCT_SITE.ATTRIBUTE_CATEGORY ,'Yes',ACCT_SITE.ATTRIBUTE18, NULL) ACCT_SITE#Yes#Notice_to_Owne ,
    DECODE(ACCT_SITE.ATTRIBUTE_CATEGORY ,'Yes',ACCT_SITE.ATTRIBUTE19, NULL) ACCT_SITE#Yes#Notice_to_Owne1 ,
    DECODE(ACCT_SITE.ATTRIBUTE_CATEGORY ,'Yes',ACCT_SITE.ATTRIBUTE20, NULL) ACCT_SITE#Yes#Notice_to_Owne2 ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_CUSTOMER_SITE_CLASS',SITE_USES.ATTRIBUTE1,'F') SITE_USES#Customer_Site_Clas ,
    SITE_USES.ATTRIBUTE10 SITE_USES#B2B_Address ,
    SITE_USES.ATTRIBUTE11 SITE_USES#B2B_Shipping_Wareh ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',SITE_USES.ATTRIBUTE2,'F') SITE_USES#Government_Funded ,
    SITE_USES.ATTRIBUTE3 SITE_USES#Thomas_Guide_Page ,
    SITE_USES.ATTRIBUTE4 SITE_USES#Dodge_Number ,
    SITE_USES.ATTRIBUTE5 SITE_USES#FUTURE_USE ,
    xxeis.eis_rs_dff.decode_valueset( 'OM: Salesreps',SITE_USES.ATTRIBUTE6,'F') SITE_USES#Salesrep_#2 ,
    SITE_USES.ATTRIBUTE7 SITE_USES#Salesrep_Spilt_#1 ,
    SITE_USES.ATTRIBUTE8 SITE_USES#Salesrep_Spilt_#2 ,
    SITE_USES.ATTRIBUTE9 SITE_USES#Permit_Number ,
    LOC.ATTRIBUTE1 LOC#Pay_To_Vendor_ID ,
    LOC.ATTRIBUTE2 LOC#LOB ,
    LOC.ATTRIBUTE3 LOC#Pay_To_Vendor_Code ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',PARTY.ATTRIBUTE1,'I'), NULL) PARTY#101#Party_Type ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',PARTY.ATTRIBUTE10,'I'), NULL) PARTY#101#InterCompany_Recei ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'QP: Yes/No',PARTY.ATTRIBUTE11,'F'), NULL) PARTY#101#Coop_Override ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'QP: Yes/No',PARTY.ATTRIBUTE12,'F'), NULL) PARTY#101#Payment_Override ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',PARTY.ATTRIBUTE13,'I'), NULL) PARTY#101#Default_Payment_Ac ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_LOCATION',PARTY.ATTRIBUTE14,'I'), NULL) PARTY#101#Default_Payment_Lo ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUSOZF_AR_COLLECTORS',PARTY.ATTRIBUTE3,'F'), NULL) PARTY#101#Collector ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_PRODUCT',PARTY.ATTRIBUTE4,'I'), NULL) PARTY#101#Default_Product_Se ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_LOCATION',PARTY.ATTRIBUTE5,'I'), NULL) PARTY#101#Default_Location_S ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',PARTY.ATTRIBUTE6,'I'), NULL) PARTY#101#Default_Coop_Accou ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',PARTY.ATTRIBUTE7,'I'), NULL) PARTY#101#Default_Rebate_Acc ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_COSTCENTER',PARTY.ATTRIBUTE8,'I'), NULL) PARTY#101#Default_Cost_Cente ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',PARTY.ATTRIBUTE9,'I'), NULL) PARTY#101#InterCompany__Paya ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',PARTY.ATTRIBUTE1,'I'), NULL) PARTY#102#Party_Type ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',PARTY.ATTRIBUTE10,'I'), NULL) PARTY#102#InterCompany_Recei ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'QP: Yes/No',PARTY.ATTRIBUTE11,'F'), NULL) PARTY#102#Coop_Override ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'QP: Yes/No',PARTY.ATTRIBUTE12,'F'), NULL) PARTY#102#Payment_Override ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',PARTY.ATTRIBUTE13,'I'), NULL) PARTY#102#Default_Payment_Ac ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_LOCATION',PARTY.ATTRIBUTE14,'I'), NULL) PARTY#102#Default_Payment_Lo ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUSOZF_AR_COLLECTORS',PARTY.ATTRIBUTE3,'F'), NULL) PARTY#102#Collector ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_PRODUCT',PARTY.ATTRIBUTE4,'I'), NULL) PARTY#102#Default_Product_Se ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_LOCATION',PARTY.ATTRIBUTE5,'I'), NULL) PARTY#102#Default_Location_S ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',PARTY.ATTRIBUTE6,'I'), NULL) PARTY#102#Default_Coop_Accou ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',PARTY.ATTRIBUTE7,'I'), NULL) PARTY#102#Default_Rebate_Acc ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_COSTCENTER',PARTY.ATTRIBUTE8,'I'), NULL) PARTY#102#Default_Cost_Cente ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'102',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_GL_ACCOUNT',PARTY.ATTRIBUTE9,'I'), NULL) PARTY#102#InterCompany_Payab ,
    PARTY_SITE.ATTRIBUTE2 PARTY_SITE#Rebt_Pay_to_Vndr_ ,
    PARTY_SITE.ATTRIBUTE3 PARTY_SITE#Rebt_LOB ,
    PARTY_SITE.ATTRIBUTE4 PARTY_SITE#Rebt_Vndr_Code ,
    PARTY_SITE.ATTRIBUTE5 PARTY_SITE#Rebt_Vndr_Flag ,
    PARTY_SITE.ATTRIBUTE6 PARTY_SITE#Rebt_Vndr_Tax_ID_
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM ar_lookups look,
    ar_lookups look_status,
    --ar_lookups look_site_status, -- Bug fix 1033465
    hz_cust_accounts cust,
    hz_parties party,
    hz_cust_site_uses site_uses,
    hz_cust_acct_sites acct_site,
    hz_party_sites party_site,
    hz_locations loc,
    hr_operating_units hou
  WHERE cust.cust_account_id      = acct_site.cust_account_id(+)
  AND cust.party_id               = party.party_id
  AND acct_site.party_site_id     = party_site.party_site_id(+)
  AND loc.location_id(+)          = party_site.location_id
  AND acct_site.cust_acct_site_id = site_uses.cust_acct_site_id(+)
  AND look.lookup_type(+)         = 'SITE_USE_CODE'
  AND look.lookup_code(+)         = site_uses.site_use_code
  AND look_status.lookup_type(+)  = 'CODE_STATUS'
  AND look_status.lookup_code(+)  = NVL (cust.status, 'A')
  AND hou.organization_id(+)      = site_uses.org_id
/
