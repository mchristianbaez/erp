CREATE OR REPLACE FORCE VIEW xxeis.xxeis_cst_sla_rcvinv_item_v
(
   je_batch_id
  ,batch_name
  ,batch_description
  ,je_header_id
  ,ledger_name
  ,je_source
  ,je_category
  ,je_period
  ,je_name
  ,je_description
  ,je_currency
  ,je_status
  ,je_type
  ,je_posted_date
  ,je_creation_date
  ,je_created_by
  ,je_balanced
  ,accrual_reversal
  ,je_multi_bal_seg
  ,accrual_rev_effective_date
  ,accrual_rev_period_name
  ,accrual_rev_status
  ,accrual_rev_je_header_id
  ,accrual_rev_change_sign
  ,reversed_je_header_id
  ,je_curr_conv_rate
  ,je_curr_conv_type
  ,je_curr_conv_date
  ,je_external_ref
  ,je_line_num
  ,je_effective_date
  ,je_entered_dr
  ,je_entered_cr
  ,je_accounted_dr
  ,je_accounted_cr
  ,je_line_description
  ,gl_sl_link_id
  ,gl_sl_link_table
  ,reference_10
  ,code_combination_id
  ,gl_account_string
  ,budget_name
  ,budget_type
  ,budget_version_id
  ,budget_status
  ,budget_description
  ,encumbrance_type
  ,xdl_source_table
  ,rsl_rcv_txn_id
  ,receipt_num
  ,po_number
  ,supplier_number
  ,supplier
  ,supplier_site
  ,item_number
  ,item_description
  ,transaction_date
  ,transaction_quantity
  ,item_uom
  ,xdl_appln_id
  ,sub_ledger_id
  ,mmt_transaction_id
  ,sla_entered_dr
  ,sla_entered_cr
  ,sla_accounted_dr
  ,sla_accounted_cr
  ,sla_accounted_net
  ,sla_account_class
  ,sla_event_type
  ,transfer_date_from_sla_to_gl
  ,sla_accounting_date
  ,encumbrance_type_id
  ,ae_header_id
  ,ae_line_num
  ,application_id
  ,temp_line_num
  ,ref_ae_header_id
  ,gcc#branch
  ,jl#branch
  ,jl#dept
  ,jl#account
  ,jl#sub_acct
  ,jl#pos_branch
  ,jl#wc_formula
  ,gcc50328product
  ,gcc50328productdescr
  ,gcc50328location
  ,gcc50328locationdescr
  ,gcc50328cost_center
  ,gcc50328cost_centerdescr
  ,gcc50328account
  ,gcc50328accountdescr
  ,gcc50328project_code
  ,gcc50328project_codedescr
  ,gcc50328future_use
  ,gcc50328future_usedescr
  ,gcc50328future_use_2
  ,gcc50328future_use_2descr
  ,gcc50368product
  ,gcc50368productdescr
  ,gcc50368division
  ,gcc50368divisiondescr
  ,gcc50368department
  ,gcc50368departmentdescr
  ,gcc50368account
  ,gcc50368accountdescr
  ,gcc50368subaccount
  ,gcc50368subaccountdescr
  ,gcc50368future_use
  ,gcc50368future_usedescr
)
AS
   SELECT jb.je_batch_id
         ,jb.name batch_name
         ,jb.description batch_description
         ,jh.je_header_id
         ,le.name ledger_name
         ,jes.user_je_source_name je_source
         ,jec.user_je_category_name je_category
         ,jh.period_name je_period
         ,jh.name je_name
         ,jh.description je_description
         ,jh.currency_code je_currency
         ,DECODE (
             jh.status
            ,'P', 'Posted'
            ,'U', 'Unposted'
            ,'F', 'Error7 - Showing invalid journal entry lines or no journal entry lines'
            ,'K', 'Error10 - Showing unbalanced intercompany journal entry'
            ,'Z', 'Error7 - Showing invalid journal entry lines or no journal entry lines'
            ,'Unknown')
             je_status
         ,DECODE (jh.actual_flag
                 ,'A', 'Actual'
                 ,'B', 'Budget'
                 ,'E', 'Encumbrance')
             je_type
         ,jh.posted_date je_posted_date
         ,jh.creation_date je_creation_date
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_user_name (jh.created_by)
             je_created_by
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.balanced_je_flag)
             je_balanced
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_flag)
             accrual_reversal
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.multi_bal_seg_flag)
             je_multi_bal_seg
         ,jh.accrual_rev_effective_date
         ,jh.accrual_rev_period_name
         ,jh.accrual_rev_status
         ,jh.accrual_rev_je_header_id
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_change_sign_flag)
             accrual_rev_change_sign
         ,jh.reversed_je_header_id
         ,jh.currency_conversion_rate je_curr_conv_rate
         ,jh.currency_conversion_type je_curr_conv_type
         ,jh.currency_conversion_date je_curr_conv_date
         ,jh.external_reference je_external_ref
         ,jl.je_line_num je_line_num
         ,jl.effective_date je_effective_date
         ,jl.entered_dr je_entered_dr
         ,jl.entered_cr je_entered_cr
         ,jl.accounted_dr je_accounted_dr
         ,jl.accounted_cr je_accounted_cr
         ,jl.description je_line_description
         ,jl.gl_sl_link_id
         ,jl.gl_sl_link_table
         ,jl.reference_10
         ,gcc.code_combination_id
         ,gcc.concatenated_segments gl_account_string
         ,gbv.budget_name
         ,gbv.budget_type
         ,gbv.budget_version_id
         ,DECODE (gbv.status,  'O', 'Open',  'C', 'Closed',  'F', 'Future')
             budget_status
         ,gbv.description budget_description
         ,gle.encumbrance_type
         ,xdl.source_distribution_type xdl_source_table
         ,rsl.rcv_transaction_id rsl_rcv_txn_id
         ,TO_CHAR (NULL) receipt_num
         ,                                                     --added 9/26/12
          TO_CHAR (NULL) po_number
         ,                                                     --added 9/26/12
          TO_CHAR (NULL) supplier_number
         ,                                                     --added 9/26/12
          TO_CHAR (NULL) supplier
         ,                                                     --added 9/26/12
          TO_CHAR (NULL) supplier_site
         ,                                                     --added 9/26/12
          msi.segment1 item_number
         ,msi.description item_description
         ,mta.transaction_date transaction_date
         ,ABS (mta.primary_quantity) transaction_quantity
         ,msi.primary_unit_of_measure item_uom
         ,xdl.application_id xdl_appln_id
         ,xdl.source_distribution_id_num_1 sub_ledger_id
         ,mta.transaction_id mmt_transaction_id
         ,xdl.unrounded_entered_dr sla_entered_dr
         ,xdl.unrounded_entered_cr sla_entered_cr
         ,xdl.unrounded_accounted_dr sla_accounted_dr
         ,xdl.unrounded_accounted_cr sla_accounted_cr
         ,                   --            NVL (xdl.unrounded_accounted_cr, 0)
                             --          - NVL (xdl.unrounded_accounted_dr, 0)
                                             --             sla_accounted_net,
            NVL (xdl.unrounded_accounted_dr, 0)
          - NVL (xdl.unrounded_accounted_cr, 0)
             sla_accounted_net
         ,xel.accounting_class_code sla_account_class
         ,xeh.event_type_code sla_event_type
         ,xeh.gl_transfer_date transfer_date_from_sla_to_gl
         ,xeh.accounting_date sla_accounting_date
         ,gle.encumbrance_type_id
         ,xel.ae_header_id
         ,xel.ae_line_num
         ,xel.application_id
         ,xdl.temp_line_num
         ,xdl.ref_ae_header_id
         ,                                            -- descr#flexfield#start
          xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I')
             gcc#branch
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', jl.attribute1, 'I')
             jl#branch
         ,jl.attribute2 jl#dept
         ,jl.attribute3 jl#account
         ,jl.attribute4 jl#sub_acct
         ,jl.attribute5 jl#pos_branch
         ,jl.attribute6 jl#wc_formula
         ,                                                --gl#accountff#start
          gcc.segment1 gcc#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc#50328#product#descr
         ,gcc.segment2 gcc#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc#50328#location#descr
         ,gcc.segment3 gcc#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc#50328#cost_center#descr
         ,gcc.segment4 gcc#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc#50328#account#descr
         ,gcc.segment5 gcc#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc#50328#project_code#descr
         ,gcc.segment6 gcc#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc#50328#furture_use#descr
         ,gcc.segment7 gcc#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc#50328#future_use_2#descr
         ,gcc.segment1 gcc#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_LTMR_PRODUCT')
             gcc#50368#product#descr
         ,gcc.segment2 gcc#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_ LTMR _DIVISION')
             gcc#50368#division#descr
         ,gcc.segment3 gcc#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc#50368#department#descr
         ,gcc.segment4 gcc#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ LTMR _ACCOUNT')
             gcc#50368#account#descr
         ,gcc.segment5 gcc#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc#50368#subaccount#descr
         ,gcc.segment6 gcc#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc#50368#future_use#descr
     FROM apps.gl_ledgers le
         ,apps.gl_je_batches jb
         ,apps.gl_je_headers jh
         ,apps.gl_je_lines jl
         ,apps.gl_code_combinations_kfv gcc
         ,apps.gl_je_sources jes
         ,apps.gl_je_categories jec
         ,apps.gl_import_references gir
         ,apps.gl_budget_versions gbv
         ,apps.gl_encumbrance_types gle
         ,apps.xla_ae_lines xel
         ,apps.xla_ae_headers xeh
         ,apps.xla_distribution_links xdl
         ,apps.rcv_receiving_sub_ledger rsl
         ,apps.mtl_transaction_accounts mta
         ,                                                         --added 9/7
          apps.mtl_system_items msi                                --added 9/7
    WHERE     1 = 1
          --and jh.period_name ='Aug-2012'
          --and  GCC.SEGMENT4 ='121101'
          AND jh.je_source = 'Cost Management'
          AND jh.je_category = 'Inventory'
          AND le.ledger_id = jh.ledger_id
          AND jb.je_batch_id = jh.je_batch_id
          AND jh.je_header_id = jl.je_header_id
          AND jl.code_combination_id = gcc.code_combination_id
          AND jh.je_source = jes.je_source_name
          AND jh.je_category = jec.je_category_name
          AND jh.budget_version_id = gbv.budget_version_id(+)
          AND jh.encumbrance_type_id = gle.encumbrance_type_id(+)
          AND gir.je_header_id = jl.je_header_id
          AND gir.je_line_num = jl.je_line_num
          AND gir.gl_sl_link_id = xel.gl_sl_link_id(+)
          AND gir.gl_sl_link_table = xel.gl_sl_link_table(+)
          AND xel.ae_header_id = xdl.ae_header_id(+)
          AND xel.ae_line_num = xdl.ae_line_num(+)
          AND xdl.application_id = 707                           --added 10/16
          AND xel.ae_header_id = xeh.ae_header_id(+)
          AND xdl.source_distribution_id_num_1 = rsl.rcv_sub_ledger_id(+)
          AND mta.inv_sub_ledger_id(+) = xdl.source_distribution_id_num_1 --new 9/7
          AND msi.inventory_item_id(+) = mta.inventory_item_id       --new 9/7
          AND msi.organization_id(+) = mta.organization_id           --new 9/7
          AND xdl.source_distribution_type(+) = 'MTL_TRANSACTION_ACCOUNTS' --new 9/7
   UNION ALL
   SELECT jb.je_batch_id
         ,jb.name batch_name
         ,jb.description batch_description
         ,jh.je_header_id
         ,le.name ledger_name
         ,jes.user_je_source_name je_source
         ,jec.user_je_category_name je_category
         ,jh.period_name je_period
         ,jh.name je_name
         ,jh.description je_description
         ,jh.currency_code je_currency
         ,DECODE (
             jh.status
            ,'P', 'Posted'
            ,'U', 'Unposted'
            ,'F', 'Error7 - Showing invalid journal entry lines or no journal entry lines'
            ,'K', 'Error10 - Showing unbalanced intercompany journal entry'
            ,'Z', 'Error7 - Showing invalid journal entry lines or no journal entry lines'
            ,'Unknown')
             je_status
         ,DECODE (jh.actual_flag
                 ,'A', 'Actual'
                 ,'B', 'Budget'
                 ,'E', 'Encumbrance')
             je_type
         ,jh.posted_date je_posted_date
         ,jh.creation_date je_creation_date
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_user_name (jh.created_by)
             je_created_by
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.balanced_je_flag)
             je_balanced
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_flag)
             accrual_reversal
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.multi_bal_seg_flag)
             je_multi_bal_seg
         ,jh.accrual_rev_effective_date
         ,jh.accrual_rev_period_name
         ,jh.accrual_rev_status
         ,jh.accrual_rev_je_header_id
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_change_sign_flag)
             accrual_rev_change_sign
         ,jh.reversed_je_header_id
         ,jh.currency_conversion_rate je_curr_conv_rate
         ,jh.currency_conversion_type je_curr_conv_type
         ,jh.currency_conversion_date je_curr_conv_date
         ,jh.external_reference je_external_ref
         ,jl.je_line_num je_line_num
         ,jl.effective_date je_effective_date
         ,jl.entered_dr je_entered_dr
         ,jl.entered_cr je_entered_cr
         ,jl.accounted_dr je_accounted_dr
         ,jl.accounted_cr je_accounted_cr
         ,jl.description je_line_description
         ,jl.gl_sl_link_id
         ,jl.gl_sl_link_table
         ,jl.reference_10
         ,gcc.code_combination_id
         ,gcc.concatenated_segments gl_account_string
         ,gbv.budget_name
         ,gbv.budget_type
         ,gbv.budget_version_id
         ,DECODE (gbv.status,  'O', 'Open',  'C', 'Closed',  'F', 'Future')
             budget_status
         ,gbv.description budget_description
         ,gle.encumbrance_type
         ,xdl.source_distribution_type xdl_source_table
         ,rsl.rcv_transaction_id rsl_rcv_txn_id
         ,rh.receipt_num receipt_num
         ,                                                     --added 9/26/12
          poh.segment1 po_number
         ,                                                     --added 9/26/12
          pov.segment1 supplier_number
         ,                                                     --added 9/26/12
          pov.vendor_name supplier
         ,                                                     --added 9/26/12
          povs.vendor_site_code supplier_site
         ,                                                     --added 9/26/12
          msi.segment1 item_number
         ,msi.description item_description
         ,rt.transaction_date transaction_date
         ,ABS (rt.quantity) transaction_quantity
         ,msi.primary_unit_of_measure item_uom
         ,xdl.application_id xdl_appln_id
         ,xdl.source_distribution_id_num_1 sub_ledger_id
         ,TO_NUMBER (NULL) mmt_transaction_id
         ,xdl.unrounded_entered_dr sla_entered_dr
         ,xdl.unrounded_entered_cr sla_entered_cr
         ,xdl.unrounded_accounted_dr sla_accounted_dr
         ,xdl.unrounded_accounted_cr sla_accounted_cr
         ,                   --            NVL (xdl.unrounded_accounted_cr, 0)
                             --          - NVL (xdl.unrounded_accounted_dr, 0)
                                             --             sla_accounted_net,
            NVL (xdl.unrounded_accounted_dr, 0)
          - NVL (xdl.unrounded_accounted_cr, 0)
             sla_accounted_net
         ,xel.accounting_class_code sla_account_class
         ,xeh.event_type_code sla_event_type
         ,xeh.gl_transfer_date transfer_date_from_sla_to_gl
         ,xeh.accounting_date sla_accounting_date
         ,gle.encumbrance_type_id
         ,xel.ae_header_id
         ,xel.ae_line_num
         ,xel.application_id
         ,xdl.temp_line_num
         ,xdl.ref_ae_header_id
         ,                                             --descr#flexfield#start
          xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I')
             gcc#branch
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', jl.attribute1, 'I')
             jl#branch
         ,jl.attribute2 jl#dept
         ,jl.attribute3 jl#account
         ,jl.attribute4 jl#sub_acct
         ,jl.attribute5 jl#pos_branch
         ,jl.attribute6 jl#wc_formula
         ,                                                --gl#accountff#start
          gcc.segment1 gcc#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc#50328#product#descr
         ,gcc.segment2 gcc#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc#50328#location#descr
         ,gcc.segment3 gcc#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc#50328#cost_center#descr
         ,gcc.segment4 gcc#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc#50328#account#descr
         ,gcc.segment5 gcc#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc#50328#project_code#descr
         ,gcc.segment6 gcc#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc#50328#furture_use#descr
         ,gcc.segment7 gcc#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc#50328#future_use_2#descr
         ,gcc.segment1 gcc#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_LTMR_PRODUCT')
             gcc#50368#product#descr
         ,gcc.segment2 gcc#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_ LTMR _DIVISION')
             gcc#50368#division#descr
         ,gcc.segment3 gcc#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc#50368#department#descr
         ,gcc.segment4 gcc#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ LTMR _ACCOUNT')
             gcc#50368#account#descr
         ,gcc.segment5 gcc#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc#50368#subaccount#descr
         ,gcc.segment6 gcc#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc#50368#future_use#descr
     FROM apps.gl_ledgers le
         ,apps.gl_je_batches jb
         ,apps.gl_je_headers jh
         ,apps.gl_je_lines jl
         ,apps.gl_code_combinations_kfv gcc
         ,apps.gl_je_sources jes
         ,apps.gl_je_categories jec
         ,apps.gl_import_references gir
         ,apps.gl_budget_versions gbv
         ,apps.gl_encumbrance_types gle
         ,apps.xla_ae_lines xel
         ,apps.xla_ae_headers xeh
         ,apps.xla_distribution_links xdl
         ,apps.rcv_receiving_sub_ledger rsl
         ,apps.rcv_transactions rt
         ,apps.rcv_shipment_lines rl
         ,apps.mtl_system_items msi
         ,                                                        -- added 9/7
          apps.rcv_shipment_headers rh
         ,                                                        --added 9/26
          apps.po_vendors pov
         ,                                                        --added 9/26
          apps.po_vendor_sites povs
         ,                                                        --added 9/26
          apps.po_headers_all poh                                 --added 9/26
    WHERE     1 = 1
          --and jh.period_name ='Aug-2012'
          --and  GCC.SEGMENT4 ='121101'
          AND jh.je_source = 'Cost Management'
          AND jh.je_category <> 'Inventory'
          AND le.ledger_id = jh.ledger_id
          AND jb.je_batch_id = jh.je_batch_id
          AND jh.je_header_id = jl.je_header_id
          AND jl.code_combination_id = gcc.code_combination_id
          AND jh.je_source = jes.je_source_name
          AND jh.je_category = jec.je_category_name
          AND jh.budget_version_id = gbv.budget_version_id(+)
          AND jh.encumbrance_type_id = gle.encumbrance_type_id(+)
          AND gir.je_header_id = jl.je_header_id
          AND gir.je_line_num = jl.je_line_num
          AND gir.gl_sl_link_id = xel.gl_sl_link_id(+)
          AND gir.gl_sl_link_table = xel.gl_sl_link_table(+)
          AND xel.ae_header_id = xdl.ae_header_id(+)
          AND xel.ae_line_num = xdl.ae_line_num(+)
          AND xdl.application_id = 707                           --added 10/16
          AND xel.ae_header_id = xeh.ae_header_id(+)
          AND xdl.source_distribution_id_num_1 = rsl.rcv_sub_ledger_id(+)
          AND xdl.source_distribution_type(+) = 'RCV_RECEIVING_SUB_LEDGER'
          AND rsl.rcv_transaction_id = rt.transaction_id(+)
          AND rt.shipment_line_id = rl.shipment_line_id(+)        --added 9/26
          AND rl.shipment_header_id = rh.shipment_header_id(+)    --added 9/26
          AND poh.po_header_id(+) = rl.po_header_id               --added 9/26
          AND rh.vendor_id = pov.vendor_id(+)                     --added 9/26
          AND rh.vendor_site_id = povs.vendor_site_id(+)          --added 9/26
          AND msi.inventory_item_id(+) = rl.item_id               --added 9/26
          AND msi.organization_id(+) = rl.to_organization_id      --added 9/26
--          AND rt.shipment_line_id = rl.shipment_line_id(+) --commented 9/26
--          AND msi.inventory_item_id(+) = rl.item_id        --commented 9/26
--          AND msi.organization_id(+) = rl.to_organization_id --commented 9/26
;


