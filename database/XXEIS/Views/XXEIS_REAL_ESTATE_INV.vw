CREATE OR REPLACE FORCE VIEW xxeis.xxeis_real_estate_inv
(
   vendor_number
  ,vendor_site_code
  ,vendor_name
  ,invoice_date
  ,invoice_num
  ,ypayable
  ,payment_status_flag
  ,terms_description
  ,invoice_amount
  ,branch
  ,invtype
  ,description
  ,pay_group_lookup_code
  ,gl_date
  ,invoice_cancelled_date
  ,approval_status
  ,check_number
  ,check_date
  ,payment_amount
)
AS
   SELECT vnd.segment1 vendor_number
         ,sts.vendor_site_code vendor_site_code
         ,vnd.vendor_name vendor_name
         ,ivc.invoice_date invoice_date
         ,ivc.invoice_num invoice_num
         ,ivc.attribute12 ypayable
         ,ivc.payment_status_flag payment_status_flag
         ,trm.description terms_description
         ,ivc.invoice_amount invoice_amount
         ,ivc.attribute2 branch
         ,ivc.attribute11 invtype
         ,ivc.description description
         ,ivc.pay_group_lookup_code pay_group_lookup_code
         ,ivc.gl_date gl_date
         ,ivc.cancelled_date invoice_cancelled_date
         ,ivc.approval_status approval_status
         ,chk.check_number check_number
         ,chk.check_date check_date
         ,pay.amount payment_amount
     FROM apps.xxcusap_invoices_all ivc
          INNER JOIN ap.ap_suppliers vnd ON ivc.vendor_id = vnd.vendor_id
          INNER JOIN
          ap.ap_supplier_sites_all sts
             ON     ivc.vendor_id = sts.vendor_id
                AND ivc.vendor_site_id = sts.vendor_site_id
          INNER JOIN ap.ap_terms_tl trm ON ivc.terms_id = trm.term_id
          LEFT JOIN ap.ap_invoice_payments_all pay
             ON ivc.invoice_id = pay.invoice_id
          LEFT JOIN ap.ap_checks_all chk ON pay.check_id = chk.check_id;


