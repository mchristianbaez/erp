CREATE OR REPLACE FORCE VIEW xxeis.xxwc_customer_orders_v
(
   creation_date
  ,creation_less_7
  ,source
  ,order_type
  ,invoice_num
  ,trx_date
  ,branch
  ,bill_to_cust_num
  ,bill_to_cust_name
  ,rac_ship_to_customer_num
  ,rac_ship_to_customer_name
  ,su_ship_to_location
  ,ctt_type_name
  ,sales_rep_name
  ,sales_rep_id
  ,oracle_salesrep_num
  ,ship_via
  ,source_type
  ,user_name
  ,user_name_desc
  ,sales_order
  ,sales_order_line
  ,item
  ,item_desc
  ,item_category
  ,order_qty
  ,uom
  ,list_price
  ,sell_price
  ,ext_amount
  ,new_cost_2
  ,new_ext_cost_2
)
AS
     SELECT TRUNC (c.creation_date) AS creation_date
           ,c.creation_date - 7 AS creation_less_7
           ,c.interface_line_context AS source
           ,c.interface_line_attribute2 AS order_type
           ,a.trx_number invoice_num
           ,a.trx_date
           ,SUBSTR (c.warehouse_name, 1, 3) AS branch
           ,a.rac_bill_to_customer_num AS bill_to_cust_num
           ,a.rac_bill_to_customer_name AS bill_to_cust_name
           ,a.rac_ship_to_customer_num
           ,a.rac_ship_to_customer_name
           ,a.su_ship_to_location
           ,a.ctt_type_name
           ,a.ras_primary_salesrep_name AS sales_rep_name
           ,l.salesrep_id AS sales_rep_id
           , (SELECT jrs.salesrep_number
                FROM apps.jtf_rs_salesreps jrs
               WHERE jrs.salesrep_id = l.salesrep_id AND ROWNUM = 1)
               AS oracle_salesrep_num
           , (SELECT wcsmv.ship_method_code_meaning
                FROM apps.wsh_carrier_ship_methods_v wcsmv
               WHERE     wcsmv.ship_method_code = l.shipping_method_code
                     AND ROWNUM = 1)
               AS ship_via
           ,l.source_type_code AS source_type
           ,u.user_name
           ,u.description AS user_name_desc
           ,c.sales_order
           ,c.sales_order_line
           ,msi.segment1 AS item
           ,c.description AS item_desc
           ,cat.category_concat_segs item_category
           ,NVL (c.quantity_invoiced, c.quantity_credited) AS order_qty
           ,l.pricing_quantity_uom AS uom
           ,l.unit_list_price AS list_price
           ,c.net_unit_selling_price AS sell_price
           ,c.net_extended_amount AS ext_amount
           , -- apps.xxwc_mv_routines_pkg.get_order_line_cost (l.line_id) new_unit_cost,
 --(NVL (c.quantity_invoiced, c.quantity_credited) * apps.xxwc_mv_routines_pkg.get_order_line_cost (l.line_id)) AS new_ext_cost
            ROUND (
               (NVL (
                   apps.xxwc_mv_routines_pkg.get_vendor_quote_cost (l.line_id)
                  ,NVL (
                      apps.xxwc_mv_routines_pkg.get_order_line_cost (l.line_id)
                     ,0)))
              ,2)
               new_cost_2
           ,  (NVL (c.quantity_invoiced, c.quantity_credited))
            * ( (ROUND (
                    (NVL (
                        apps.xxwc_mv_routines_pkg.get_vendor_quote_cost (
                           l.line_id)
                       ,NVL (
                           apps.xxwc_mv_routines_pkg.get_order_line_cost (
                              l.line_id)
                          ,0)))
                   ,2)))
               new_ext_cost_2
       FROM apps.ra_customer_trx_v a
           ,apps.ra_customer_trx_lines_v c
           ,apps.mtl_system_items msi
           ,apps.oe_order_lines_all l
           ,apps.mtl_item_categories_v cat
           ,applsys.fnd_user u
      WHERE     a.interface_header_context = 'ORDER ENTRY'
            AND c.interface_line_context = 'ORDER ENTRY'
            AND a.customer_trx_id = c.customer_trx_id
            AND NVL (a.of_organization_id, 222) = 222
            AND c.line_type = 'LINE'
            AND msi.inventory_item_id = c.inventory_item_id
            AND msi.organization_id = 222
            AND c.oe_header_id = l.header_id
            AND c.interface_line_attribute6 = TO_CHAR (l.line_id)
            AND l.inventory_item_id = cat.inventory_item_id
            AND cat.organization_id = 222
            AND cat.category_set_id = 1100000062
            AND l.created_by = u.user_id
   -- and c.creation_date>trunc(sysdate-1)+.25

   ORDER BY c.warehouse_name
           ,a.trx_date
           ,c.sales_order
           ,c.sales_order_line;


