 /*************************************************************************
  $Header XXEIS.EIS_XXWC_AR_CRDT_MEMO_DTLS_V.vw $
  Module Name: Receivables
  PURPOSE: White Cap Credit Memo Report - WC
  ESMS Task Id : TMS# 20151216-00168 
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.1        25-Feb-2016  Mahender Reddy   Initial version

**************************************************************************/
CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_AR_CRDT_MEMO_DTLS_V" ("FISCAL_YEAR", "FISCAL_MONTH", "CUSTOMER_NUMBER",   "INVOICE_TYPE",  "LOCATION",   "INVOICE_NUMBER", "ORDER_NUMBER", "ORIGINAL_INVOICE_NUMBER", "ORIGINAL_INVOICE_DATE",  "CREDIT_REASON_SALE", "INVOICE_LINE_AMOUNT",  "MASTER_NAME", "ORIGIN_DATE", "LINE_TYPE")
 AS
SELECT gp.period_year                     FISCAL_YEAR,
         gp.period_name                     FISCAL_MONTH,
         PARTY_SITES.PARTY_SITE_NUMBER      CUSTOMER_NUMBER,
		  nvl(xocr.PAYMENT_TYPE_CODE,'ON ACCOUNT') invoice_type,
		  loc.organization_code              location,
		  rctr.trx_number                    invoice_number,
         ohr.order_number                   order_number,
		 rcto.trx_number                    original_invoice_number,
         rcto.trx_date                      original_invoice_date,
		 olr.Return_Reason_Code             credit_reason_sale,
		 rctlr.extended_amount              invoice_line_amount,
		 custb.account_name master_name,
		  (SELECT MAX(apply_date)
            FROM ar_receivable_applications
           WHERE application_type='CM'
             AND customer_trx_id   =rctr.customer_trx_id
         ) origin_date,
		 ott.name                line_type 
  FROM oe_order_headers             oho,
       oe_order_lines               olo,
       oe_order_headers             ohr,
       oe_order_lines               olr,
       ra_customer_trx              rctr,
       ra_customer_trx_lines        rctlr,
       ra_cust_trx_line_gl_dist     rctgr,
       ra_customer_trx              rcto,
       ra_customer_trx_lines        rctlo,
       ra_cust_trx_line_gl_dist     rctgo,
       ar_payment_schedules         ps,
       ar_payment_schedules         pso,
       hz_cust_accounts             custs,
       hz_parties                   partys,
       hz_cust_site_uses            sites,
       hz_cust_acct_sites           cust_sites,
       hz_party_sites               party_sites,
       hz_locations                 locs,
       hz_cust_accounts             custb,
       hz_parties                   partyb,
       hz_cust_site_uses            siteb,
       hz_cust_acct_sites           cust_siteb,
       hz_party_sites               party_siteb,
       hz_locations                 locb,
       hz_customer_profiles         hzp,
       AR_COLLECTORS                AC,
       jtf_rs_salesreps             jsr,
       org_organization_definitions loc,
       fnd_user                     fu,
       RA_CUST_TRX_TYPES            ctt,
       gl_periods                   gp,
       oe_transaction_types_vl      ott,
       XXWC_OM_CASH_REFUND_TBL      xocr
  WHERE ohr.header_id                   = olr.header_id
    --AND ohr.order_number                = 10000924
    AND olr.reference_header_id         = olo.header_id
    AND olr.line_type_id                = ott.transaction_type_id
    AND olr.reference_line_id           = olo.line_id
    AND oho.header_id                   = olo.header_id
    AND ohr.header_id                   = xocr.return_header_id(+)
    AND rctr.CUST_TRX_TYPE_ID           = CTT.CUST_TRX_TYPE_ID
    AND ctt.name                        = 'Credit Memo'
    AND rctr.interface_header_context   = 'ORDER ENTRY'
    AND TO_CHAR(ohr.order_number)       = rctr.interface_header_attribute1
    AND TO_CHAR(olr.line_id)            = rctlr.interface_line_attribute6
    AND rctr.customer_trx_id            = rctlr.customer_trx_id
    AND rcto.INTERFACE_header_CONTEXT   = 'ORDER ENTRY'
    AND TO_CHAR(oho.order_number)       = rcto.interface_header_attribute1
    AND TO_CHAR(olo.line_id)            = rctlo.interface_line_attribute6
    AND rcto.customer_trx_id            = rctlo.customer_trx_id
    AND rctgo.customer_trx_id           = rcto.customer_trx_id
    AND 'REC'                           = rctgo.account_class
    AND 'Y'                             = rctgo.latest_rec_flag
    AND rctgr.customer_trx_id           = rctr.customer_trx_id
    AND 'REC'                           = rctgr.account_class
    AND 'Y'                             = rctgr.latest_rec_flag    
    AND ps.customer_trx_id              = rctr.customer_trx_id
    AND pso.customer_trx_id             = rcto.customer_trx_id
    AND rctr.ship_to_customer_id        = custs.cust_account_id
    AND custs.party_id                  = partys.party_id
    AND RCTR.SHIP_TO_SITE_USE_ID        = SITES.SITE_USE_ID(+)
    AND CUST_SITES.CUST_ACCT_SITE_ID(+) = SITES.CUST_ACCT_SITE_ID    
    AND cust_sites.party_site_id        = party_sites.party_site_id(+)
    AND party_sites.location_id         = locs.location_id(+)
    AND rctr.bill_to_customer_id        = custb.cust_account_id
    AND custb.party_id                  = partyb.party_id
    AND rctr.bill_to_site_use_id        = siteb.site_use_id
    AND CUST_SITEB.CUST_ACCT_SITE_ID    = SITEB.CUST_ACCT_SITE_ID
    AND cust_siteb.party_site_id        = party_siteb.party_site_id
    AND party_siteb.location_id         = locb.location_id
    AND hzp.cust_account_id             = custb.cust_account_id
    AND hzp.site_use_id                 = siteb.site_use_id
    AND AC.COLLECTOR_ID                 = HZP.COLLECTOR_ID(+)
    AND jsr.salesrep_id                 = rctr.primary_salesrep_id
    AND jsr.org_id                      = rctr.org_id
    AND loc.organization_id             = olr.ship_from_org_id
    AND fu.user_id                      = ohr.created_by
    AND gp.period_set_name              ='4-4-QTR'
    AND rctgr.gl_date between gp.start_date and gp.end_date
  UNION
  SELECT gp.period_year                     FISCAL_YEAR,
         gp.period_name                     FISCAL_MONTH,
    PARTY_SITES.PARTY_SITE_NUMBER CUSTOMER_NUMBER,
	'PRISM RETURN' invoice_type,
	loc.organization_code location,
	rctr.trx_number invoice_number,
    ohr.order_number order_number, 
	NULL original_invoice_number,
    NULL original_invoice_date,
	olr.Return_Reason_Code credit_reason_sale, 
	rctlr.extended_amount invoice_line_amount,
	custb.account_name master_name,
	(SELECT MAX(apply_date)
    FROM ar_receivable_applications
    WHERE application_type='CM'
    AND customer_trx_id   =rctr.customer_trx_id
    ) origin_date,
	ott.name line_type
  FROM
    oe_order_headers ohr,
    oe_order_lines olr,
    ra_customer_trx rctr,
    ra_customer_trx_lines rctlr,
    ra_cust_trx_line_gl_dist rctgr,
    ar_payment_schedules ps,
    hz_cust_accounts custs,
    hz_parties partys,
    hz_cust_site_uses sites,
    hz_cust_acct_sites cust_sites,
    hz_party_sites party_sites,
    hz_locations locs,
    hz_cust_accounts custb,
    hz_parties partyb,
    hz_cust_site_uses siteb,
    hz_cust_acct_sites cust_siteb,
    hz_party_sites party_siteb,
    hz_locations locb,
    hz_customer_profiles hzp,
    AR_COLLECTORS AC,
    jtf_rs_salesreps jsr,
    org_organization_definitions loc,
    fnd_user fu,
    RA_CUST_TRX_TYPES            ctt,
    gl_periods                   gp,
    oe_transaction_types_vl      ott       
  WHERE ohr.header_id                   = olr.header_id
    AND olr.reference_header_id         IS NULL
    AND olr.line_type_id                = ott.transaction_type_id
    AND rctr.interface_header_context   = 'ORDER ENTRY'    
    AND TO_CHAR(ohr.order_number)       = rctr.interface_header_attribute1
    AND TO_CHAR(olr.line_id)            = rctlr.interface_line_attribute6
    AND rctr.CUST_TRX_TYPE_ID           = CTT.CUST_TRX_TYPE_ID
    AND ctt.name                        = 'Credit Memo' 
    AND rctgr.customer_trx_id           = rctr.customer_trx_id
    AND 'REC'                           = rctgr.account_class
    AND 'Y'                             = rctgr.latest_rec_flag
    AND ps.customer_trx_id              = rctr.customer_trx_id
    AND rctr.ship_to_customer_id        = custs.cust_account_id
    AND custs.party_id                  = partys.party_id
    AND RCTR.SHIP_TO_SITE_USE_ID        = SITES.SITE_USE_ID(+)
    AND CUST_SITES.CUST_ACCT_SITE_ID(+) = SITES.CUST_ACCT_SITE_ID
    AND cust_sites.party_site_id        = party_sites.party_site_id(+)
    AND party_sites.location_id         = locs.location_id(+)
    AND rctr.bill_to_customer_id        = custb.cust_account_id
    AND custb.party_id                  = partyb.party_id
    AND rctr.bill_to_site_use_id        = siteb.site_use_id
    AND CUST_SITEB.CUST_ACCT_SITE_ID    = SITEB.CUST_ACCT_SITE_ID
    AND cust_siteb.party_site_id        = party_siteb.party_site_id
    AND party_siteb.location_id         = locb.location_id
    AND hzp.cust_account_id             = custb.cust_account_id
    AND hzp.site_use_id                 = siteb.site_use_id
    AND AC.COLLECTOR_ID                 = HZP.COLLECTOR_ID(+)
    AND jsr.salesrep_id                 = rctr.primary_salesrep_id
    AND jsr.org_id                      = rctr.org_id
    AND loc.organization_id             = olr.ship_from_org_id
    AND fu.user_id                      = ohr.created_by
    AND ohr.order_type_id NOT IN (1013, 1014)
    AND gp.period_set_name              ='4-4-QTR'
    AND rctgr.gl_date between gp.start_date and gp.end_date
    AND NOT EXISTS
    (SELECT 1 FROM XXWC_OM_CASH_REFUND_TBL WHERE return_header_id =OHr.HEADER_ID
    )
/