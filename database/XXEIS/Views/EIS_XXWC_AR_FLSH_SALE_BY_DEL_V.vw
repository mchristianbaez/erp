CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_ar_flsh_sale_by_del_v
(
   current_fiscal_month
  ,current_fiscal_year
  ,delivery_type
  ,branch_location
  ,invoice_count
  ,sales
)
AS
     SELECT gp.period_name current_fiscal_month
           ,gp.period_year current_fiscal_year
           ,xxeis.eis_rs_xxwc_com_util_pkg.get_shipping_mthd3 (
               ol.shipping_method_code)
               delivery_type
           ,mp.organization_code branch_location
           ,COUNT (
               DISTINCT xxeis.eis_rs_xxwc_com_util_pkg.get_dist_invoice_num (
                           rct.customer_trx_id
                          ,rct.trx_number
                          ,otlh.name))
               invoice_count
           ,SUM (
                 NVL (rctl.quantity_invoiced, rctl.quantity_credited)
               * NVL (ol.unit_selling_price, 0))
               sales
       -- NVL(SUM(DECODE(otl.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) * NVL(Ol.Unit_Selling_Price,0)),0) sales
       -- SUM(oep.payment_amount) cash_sales,
       --   xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type payment_type
       FROM ra_customer_trx rct
           ,ra_customer_trx_lines rctl
           ,oe_order_headers oh
           ,oe_order_lines ol
           ,org_organization_definitions mp
           ,gl_periods gp
           ,gl_sets_of_books gsob
           ,oe_transaction_types_vl otl
           ,oe_transaction_types_vl otlh
      WHERE     1 = 1
            --AND TRUNC(rct.creation_date)       = TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
            /* AND rct.creation_date >= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
               || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
               || ' HH24:MI:SS') + 0.25
             AND rct.creation_date <= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
               || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
               || ' HH24:MI:SS') + 1.25*/
            AND rctl.customer_trx_id = rct.customer_trx_id
            AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)
            AND rctl.inventory_item_id = ol.inventory_item_id
            AND ol.header_id = oh.header_id
            AND otl.transaction_type_id = ol.line_type_id
            AND otlh.transaction_type_id = oh.order_type_id
            AND TO_CHAR (oh.order_number) = rct.interface_header_attribute1
            AND mp.organization_id(+) = ol.ship_from_org_id
            AND gsob.set_of_books_id = mp.set_of_books_id
            AND gsob.period_set_name = gp.period_set_name
            AND (   (    xxeis.eis_rs_xxwc_com_util_pkg.get_period_name_from
                            IS NULL
                     AND gp.period_name IN
                            (SELECT period_name
                               FROM gl_periods gp1
                              WHERE     gp1.period_set_name =
                                           gp.period_set_name
                                    AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                                                   gp.start_date)
                                                            AND TRUNC (
                                                                   gp.end_date)))
                 OR (    xxeis.eis_rs_xxwc_com_util_pkg.get_period_name_from
                            IS NOT NULL
                     AND gp.period_name =
                            xxeis.eis_rs_xxwc_com_util_pkg.get_period_name_from))
            AND rct.creation_date BETWEEN gp.start_date + 0.25
                                      AND gp.end_date + 1.25
            /*AND EXISTS
            (SELECT 1
            FROM oe_payments oep
            WHERE oep.header_id        = ol.header_id
            AND oep.payment_type_code IN ('CASH','CREDIT_CARD')
            )*/
            AND rctl.interface_line_context(+) = 'ORDER ENTRY'
            AND NOT EXISTS
                       (SELECT 1
                          FROM hz_customer_profiles hcp
                              ,hz_cust_profile_classes hcpc
                              ,hz_cust_accounts hca
                         WHERE     hca.party_id = hcp.party_id
                               AND oh.sold_to_org_id = hcp.cust_account_id
                               AND hcp.site_use_id IS NULL
                               AND hcp.profile_class_id =
                                      hcpc.profile_class_id(+)
                               AND hcpc.name LIKE 'WC%Branches%')
   /*AND NOT EXISTS
   (SELECT 1
   FROM OE_PRICE_ADJUSTMENTS_V
   WHERE HEADER_ID     = OL.HEADER_ID
   AND LINE_ID         = OL.LINE_ID
   AND adjustment_name ='BRANCH_COST_MODIFIER'
   )*/
   GROUP BY gp.period_name
           ,gp.period_year
           ,xxeis.eis_rs_xxwc_com_util_pkg.get_shipping_mthd3 (
               ol.shipping_method_code)
           ,mp.organization_code;


