------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_OM_SO_FORM_ADOPTION_V
   REVISIONS:
	Ver         	  Date               Author             Description
   ---------  	  ----------------    ---------------  ------------------------------------
	1.0     	   05-May-2017      	  Siva   		  TMS#20170501-00191 
******************************************************************************/
 CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_SO_FORM_ADOPTION_V (REGION, DISTRICT, BRANCH_NUMBER, BRANCH_NAME, ORDER_NUMBER, ORDER_TYPE, NTID, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, CREATION_DATE, CUSTOM_SALES_ORDER, OPERATING_UNIT, ADOPTION_PERCENTAGE, IMP_OVER_PREV_DAY, DISTRICT_ADOPTION_PERCENTAGE, DISTRICT_IMP_OVER_PREV_DAY, REGION_ADOPTION_PERCENTAGE, REGION_IMP_OVER_PREV_DAY)
 AS 
  SELECT mp.attribute9 region,
    mp.attribute8 district,
    mp.organization_code branch_number,
    haou.NAME branch_name,
    oh.order_number,
    ot.NAME order_type,
    fu.user_name ntid,
    pf.first_name,
    pf.last_name,
    fu.email_address ,
    trunc(oh.creation_date) creation_date,
    decode(oh.attribute6,'CUSTOM_SALES_ORDER','Y','N') custom_sales_order,
    hou.NAME operating_unit,
    (decode(row_number() OVER (PARTITION BY mp.organization_code,mp.attribute9,mp.attribute8 ORDER BY mp.organization_code,mp.attribute9,mp.attribute8),1,
    xxeis.eis_xxwc_inv_util_pkg.get_cust_so_adoption(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from,xxeis.eis_xxwc_inv_util_pkg.eis_get_date_to,mp.organization_code,mp.attribute9,mp.attribute8),0)) adoption_percentage,    
    ((decode(row_number() OVER (PARTITION BY mp.organization_code,mp.attribute9,mp.attribute8 ORDER BY mp.organization_code,mp.attribute9,mp.attribute8),1,
    xxeis.eis_xxwc_inv_util_pkg.get_cust_so_adoption(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from,xxeis.eis_xxwc_inv_util_pkg.eis_get_date_to,mp.organization_code,mp.attribute9,mp.attribute8),0))-
    ((decode(row_number() OVER (PARTITION BY mp.organization_code,mp.attribute9,mp.attribute8 ORDER BY mp.organization_code,mp.attribute9,mp.attribute8),1,
    xxeis.eis_xxwc_inv_util_pkg.get_cust_so_adoption(((decode(to_char(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from,'d'),'1',(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from-2),xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from))-1),xxeis.eis_xxwc_inv_util_pkg.eis_get_date_to,mp.organization_code,mp.attribute9,mp.attribute8),0)))) imp_over_prev_day,
    (decode(row_number() OVER (PARTITION BY mp.attribute9,mp.attribute8 ORDER BY mp.attribute9,mp.attribute8),1,
    xxeis.eis_xxwc_inv_util_pkg.get_district_adoption(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from,xxeis.eis_xxwc_inv_util_pkg.eis_get_date_to,mp.attribute9,mp.attribute8),0)) district_adoption_percentage,    
    ((decode(row_number() OVER (PARTITION BY mp.attribute9,mp.attribute8 ORDER BY mp.attribute9,mp.attribute8),1,
    xxeis.eis_xxwc_inv_util_pkg.get_district_adoption(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from,xxeis.eis_xxwc_inv_util_pkg.eis_get_date_to,mp.attribute9,mp.attribute8),0))-
    ((decode(row_number() OVER (PARTITION BY mp.attribute9,mp.attribute8 ORDER BY mp.attribute9,mp.attribute8),1,
    xxeis.eis_xxwc_inv_util_pkg.get_district_adoption(((decode(to_char(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from,'d'),'1',(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from-2),xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from))-1),xxeis.eis_xxwc_inv_util_pkg.eis_get_date_to,mp.attribute9,mp.attribute8),0)))) district_imp_over_prev_day,
    (decode(row_number() OVER (PARTITION BY mp.attribute9 ORDER BY mp.attribute9),1,
    xxeis.eis_xxwc_inv_util_pkg.get_region_adoption(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from,xxeis.eis_xxwc_inv_util_pkg.eis_get_date_to,mp.attribute9),0)) region_adoption_percentage,    
    ((decode(row_number() OVER (PARTITION BY mp.attribute9 ORDER BY mp.attribute9),1,
    xxeis.eis_xxwc_inv_util_pkg.get_region_adoption(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from,xxeis.eis_xxwc_inv_util_pkg.eis_get_date_to,mp.attribute9),0))-
    ((decode(row_number() OVER (PARTITION BY mp.attribute9 ORDER BY mp.attribute9),1,
    xxeis.eis_xxwc_inv_util_pkg.get_region_adoption(((decode(to_char(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from,'d'),'1',(xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from-2),xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from))-1),xxeis.eis_xxwc_inv_util_pkg.eis_get_date_to,mp.attribute9),0)))) region_imp_over_prev_day  
  FROM apps.oe_order_headers_all oh,
    apps.mtl_parameters mp,
    apps.fnd_user fu,
    apps.oe_transaction_types_tl ot,
    apps.hr_all_organization_units haou,
    apps.per_all_people_f pf,
    apps.hr_operating_units hou
  WHERE oh.ship_from_org_id = mp.organization_id
  AND oh.created_by         = fu.user_id
  AND oh.flow_status_code ! ='CANCELLED'
  AND oh.order_type_id      = ot.transaction_type_id
  AND ot.language(+)        = userenv('LANG')
  AND ot.NAME !             = 'INTERNAL ORDER'
  AND mp.organization_id    = haou.organization_id
  AND fu.employee_id        = pf.person_id(+)
  AND trunc(SYSDATE) BETWEEN pf.effective_start_date(+) AND pf.effective_end_date(+)
  AND oh.org_id = hou.organization_id
  AND trunc(oh.creation_date) >= xxeis.eis_xxwc_inv_util_pkg.eis_get_date_from
  AND trunc(oh.creation_date) <= xxeis.eis_xxwc_inv_util_pkg.eis_get_date_to
/
