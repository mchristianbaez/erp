CREATE OR REPLACE FORCE VIEW xxeis.eix_xxwc_ar_tst_summary_v
(
   bill_to_customer_name
  ,parent_customer_name
  ,bill_to_customer_number
  ,parent_customer_number
  ,outstanding_amount
  ,bucket_current
  ,bucket_1_to_30
  ,bucket_31_to_60
  ,bucket_61_to_90
  ,bucket_91_to_180
  ,bucket_181_to_360
  ,bucket_361_days_and_above
  ,profile_class
  ,credit_hold
  ,customer_account_status
  ,collector
  ,credit_analyst_name
)
AS
     SELECT main.bill_to_customer_name
           ,main.parent_customer_name
           ,main.bill_to_customer_number
           ,main.parent_customer_number
           ,SUM (main.outstanding_amount) outstanding_amount
           ,SUM (main.bucket_current) bucket_current
           ,SUM (main.bucket_1_to_30) bucket_1_to_30
           ,SUM (main.bucket_31_to_60) bucket_31_to_60
           ,SUM (main.bucket_61_to_90) bucket_61_to_90
           ,SUM (main.bucket_91_to_180) bucket_91_to_180
           ,SUM (main.bucket_181_to_360) bucket_181_to_360
           ,SUM (main.bucket_361_days_and_above) bucket_361_days_and_above
           ,main.profile_class
           ,main.credit_hold
           ,main.customer_account_status
           ,main.collector
           ,main.credit_analyst_name
       FROM (  SELECT NVL (cust.account_name, party.party_name)
                         bill_to_customer_name
                     ,NVL (parent_cust.account_name, parent_party.party_name)
                         parent_customer_name
                     ,cust.account_number bill_to_customer_number
                     ,parent_cust.account_number parent_customer_number
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,NULL
                             ,NULL
                             ,  (ps.amount_due_original)
                              * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         outstanding_amount
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,NULL
                             ,0
                             ,  (ps.amount_due_original)
                              * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_current
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,1
                             ,30
                             ,  (ps.amount_due_original)
                              * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_1_to_30
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,31
                             ,60
                             ,  (ps.amount_due_original)
                              * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_31_to_60
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,61
                             ,90
                             ,  (ps.amount_due_original)
                              * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_61_to_90
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,91
                             ,180
                             ,  (ps.amount_due_original)
                              * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_91_to_180
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,181
                             ,360
                             ,  (ps.amount_due_original)
                              * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_181_to_360
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,361
                             ,9999999
                             ,  (ps.amount_due_original)
                              * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_361_days_and_above
                     ,hzpc.name profile_class
                     ,hzp.credit_hold
                     ,customer_status.meaning customer_account_status
                     ,ac.name collector
                     ,jrdv.resource_name credit_analyst_name
                 FROM ra_cust_trx_types ctt
                     ,ra_customer_trx trx
                     ,hz_cust_accounts cust
                     ,hz_parties party
                     ,ar_payment_schedules ps
                     ,ra_cust_trx_line_gl_dist gld
                     ,hz_cust_acct_relate cust_rel
                     ,hz_cust_accounts parent_cust
                     ,hz_parties parent_party
                     ,ar_lookups customer_status
                     ,hz_customer_profiles hzp
                     ,hz_cust_profile_classes hzpc
                     ,ar_collectors ac
                     ,jtf_rs_defresources_v jrdv
                WHERE     TRUNC (ps.gl_date) <=
                             xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
                      AND trx.cust_trx_type_id = ctt.cust_trx_type_id
                      AND trx.bill_to_customer_id = cust.cust_account_id
                      AND cust.party_id = party.party_id
                      AND ps.customer_trx_id = trx.customer_trx_id
                      AND trx.customer_trx_id = gld.customer_trx_id
                      AND gld.account_class = 'REC'
                      AND gld.latest_rec_flag = 'Y'
                      --AND CTT.ORG_ID                                     = 162
                      AND cust_rel.related_cust_account_id(+) =
                             cust.cust_account_id
                      AND cust_rel.cust_account_id =
                             parent_cust.cust_account_id(+)
                      AND parent_cust.party_id = parent_party.party_id(+)
                      AND NVL (customer_status.lookup_type, 'ACCOUNT_STATUS') =
                             'ACCOUNT_STATUS'
                      AND customer_status.lookup_code(+) = hzp.account_status
                      AND NVL (cust_rel.status, 'A') = 'A'
                      AND hzp.cust_account_id = cust.cust_account_id
                      AND hzp.site_use_id IS NULL
                      AND hzp.profile_class_id = hzpc.profile_class_id
                      AND hzp.collector_id = ac.collector_id(+)
                      AND hzp.credit_analyst_id = jrdv.resource_id(+)
             -- AND PARTY.PARTY_ID=200813
             GROUP BY NVL (cust.account_name, party.party_name)
                     ,NVL (parent_cust.account_name, parent_party.party_name)
                     ,cust.account_number
                     ,parent_cust.account_number
                     ,hzpc.name
                     ,hzp.credit_hold
                     ,customer_status.meaning
                     ,ac.name
                     ,jrdv.resource_name
             UNION
               SELECT NVL (cust.account_name, party.party_name) customer_name
                     ,NVL (parent_cust.account_name, parent_party.party_name)
                         parent_customer_name
                     ,cust.account_number customer_number
                     ,parent_cust.account_number parent_customer_number
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,NULL
                             ,NULL
                             ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         outstanding_amount
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,NULL
                             ,0
                             ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_current
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,1
                             ,30
                             ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_1_to_30
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,31
                             ,60
                             ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_31_to_60
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,61
                             ,90
                             ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_61_to_90
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,91
                             ,180
                             ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_91_to_180
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,181
                             ,360
                             ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_181_to_360
                     ,SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                              ps.payment_schedule_id
                             ,ps.due_date
                             ,361
                             ,9999999
                             ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                             ,ps.amount_applied
                             ,ps.amount_credited
                             ,ps.amount_adjusted
                             ,ps.class))
                         bucket_361_days_and_above
                     ,hzpc.name profile_class
                     ,hzp.credit_hold
                     ,customer_status.meaning customer_account_status
                     ,ac.name collector
                     ,jrdv.resource_name credit_analyst_name
                 FROM ar_payment_schedules ps
                     ,ar_cash_receipts cr
                     ,hz_cust_accounts cust
                     ,hz_parties party
                     ,hz_cust_acct_relate cust_rel
                     ,hz_cust_accounts parent_cust
                     ,hz_parties parent_party
                     ,ar_lookups customer_status
                     ,hz_customer_profiles hzp
                     ,hz_cust_profile_classes hzpc
                     ,ar_collectors ac
                     ,jtf_rs_defresources_v jrdv
                --  Ra_Terms Rt
                WHERE     TRUNC (ps.gl_date) <=
                             xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
                      AND ps.cash_receipt_id = cr.cash_receipt_id
                      AND ps.customer_id = cust.cust_account_id(+)
                      AND cust.party_id = party.party_id(+)
                      AND cust_rel.related_cust_account_id(+) =
                             cust.cust_account_id
                      AND cust_rel.cust_account_id =
                             parent_cust.cust_account_id(+)
                      AND parent_cust.party_id = parent_party.party_id(+)
                      AND NVL (customer_status.lookup_type, 'ACCOUNT_STATUS') =
                             'ACCOUNT_STATUS'
                      AND customer_status.lookup_code(+) = hzp.account_status
                      AND NVL (cust_rel.status, 'A') = 'A'
                      AND hzp.cust_account_id = cust.cust_account_id
                      AND hzp.site_use_id IS NULL
                      AND hzp.profile_class_id = hzpc.profile_class_id
                      AND hzp.collector_id = ac.collector_id(+)
                      AND hzp.credit_analyst_id = jrdv.resource_id(+)
                      -- AND PARTY.PARTY_ID=200813
                      AND NOT EXISTS
                                 (SELECT 1
                                    FROM ar_cash_receipt_history crh1
                                   WHERE     crh1.cash_receipt_id =
                                                cr.cash_receipt_id
                                         AND TRUNC (crh1.gl_date) <=
                                                xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
                                         AND status = 'REVERSED')
             GROUP BY NVL (cust.account_name, party.party_name)
                     ,NVL (parent_cust.account_name, parent_party.party_name)
                     ,cust.account_number
                     ,parent_cust.account_number
                     ,hzpc.name
                     ,hzp.credit_hold
                     ,customer_status.meaning
                     ,ac.name
                     ,jrdv.resource_name) main
   GROUP BY main.bill_to_customer_name
           ,main.parent_customer_name
           ,main.bill_to_customer_number
           ,main.parent_customer_number
           ,main.profile_class
           ,main.credit_hold
           ,main.customer_account_status
           ,main.collector
           ,main.credit_analyst_name;


