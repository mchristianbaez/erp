CREATE OR REPLACE FORCE VIEW XXEIS.EIS_XXWC_SALESREP_LISTING_V
(
   RESOURCE_NAME,
   USER_NAME,
   SALESREP_ID,
   SALESREP_NUMBER,
   SOURCE_MGR_NAME,
   MANAGER_NTID,
   ACCOUNT_TYPE,
   ACCOUNT_TYPE_P
)
AS
 SELECT   c.resource_name,
          b.user_name,
          a.salesrep_id,
          a.salesrep_number,
          --b.source_mgr_name, --Commented for TMS#20140220-00116
          papfm.full_name   source_mgr_name,           
          /* (SELECT fu.user_name
              FROM fnd_user fu
              WHERE b.source_mgr_id = fu.employee_id) Managet_NT_ID*/ --Commented for TMS#20140220-00116
          fu1.user_name Managet_NTID,
          'Account Manager' Account_Type,
          'Account Manager' Account_Type_P
     FROM JTF.JTF_RS_SALESREPS a,
          JTF.JTF_RS_RESOURCE_EXTNS b,
          JTF.JTF_RS_Resource_Extns_TL c,
          apps.fnd_user              fu,
          apps.per_all_assignments_f paaf,
          apps.per_all_people_f      papfm,
          apps.fnd_user fu1
    WHERE a.status = 'A'
    AND a.resource_id = b.resource_id
    AND a.resource_id = c.resource_id
    AND c.category = 'EMPLOYEE'
    AND fu.user_id = b.user_id
    AND fu.employee_id = paaf.person_id
    AND fu1.employee_id = papfm.person_id
    AND paaf.supervisor_id = papfm.person_id
    AND SYSDATE BETWEEN paaf.effective_start_date AND paaf.effective_end_date
    AND SYSDATE BETWEEN papfm.effective_start_date AND papfm.effective_end_date
   UNION
   SELECT c.resource_name,
          b.user_name,
          a.salesrep_id,
          a.salesrep_number,
          dw.fullname,
          dw.ntid,
          'House Account' Account_Type,
          'House Account' Account_Type_P
     FROM JTF.JTF_RS_SALESREPS a,
          JTF.JTF_RS_Resource_Extns b,
          JTF.JTF_RS_Resource_Extns_TL c,
          XXWC.DW_HOUSEACCTS dw
    WHERE a.resource_id = b.resource_id
    AND a.resource_id = c.resource_id
    AND a.salesrep_number = dw.salesrepnumber(+)  
    AND c.category = 'OTHER'
   UNION
   SELECT c.resource_name,
          b.user_name,
          a.salesrep_id,
          a.salesrep_number,
          dw.fullname,
          dw.ntid,
          'House Account' Account_Type,
          'ALL' Account_Type_P
     FROM JTF.JTF_RS_SALESREPS a,
          JTF.JTF_RS_Resource_Extns b,
          JTF.JTF_RS_Resource_Extns_TL c,
          XXWC.DW_HOUSEACCTS dw
    WHERE a.resource_id = b.resource_id
    AND a.resource_id = c.resource_id
    AND a.salesrep_number = dw.salesrepnumber(+) 
    AND c.category = 'OTHER'
   UNION
   SELECT c.resource_name,
          b.user_name,
          a.salesrep_id,
          a.salesrep_number,
          --b.source_mgr_name, --Commented for TMS#20140220-00116          
          papfm.full_name   source_mgr_name,           
         /* (SELECT fu.user_name
             FROM fnd_user fu
            WHERE b.source_mgr_id = fu.employee_id) Managet_NT_ID,*/ --Commented for TMS#20140220-00116  
           fu1.user_name Managet_NTID,
          'Account Manager' Account_Type,
          'ALL' Account_Type_P
     FROM JTF.JTF_RS_SALESREPS a,
          JTF.JTF_RS_RESOURCE_EXTNS b,
          JTF.JTF_RS_Resource_Extns_TL c,
          apps.fnd_user              fu,
          apps.per_all_assignments_f paaf,
          apps.per_all_people_f      papfm,
          apps.fnd_user fu1
    WHERE     a.status = 'A'
    AND a.resource_id = b.resource_id
    AND a.resource_id = c.resource_id
    AND c.category = 'EMPLOYEE'
    AND fu.user_id = b.user_id
    AND fu.employee_id = paaf.person_id
    AND fu1.employee_id = papfm.person_id
    AND paaf.supervisor_id = papfm.person_id
    AND SYSDATE BETWEEN paaf.effective_start_date AND paaf.effective_end_date
    AND SYSDATE BETWEEN papfm.effective_start_date AND papfm.effective_end_date 
   ORDER BY resource_name;
