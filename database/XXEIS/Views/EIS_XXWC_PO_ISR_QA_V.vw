CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_po_isr_qa_v
(
   org
  ,pre
  ,item_number
  ,vendor_num
  ,vendor_name
  ,source
  ,st
  ,description
  ,cat
  ,pplt
  ,plt
  ,uom
  ,cl
  ,stk_flag
  ,pm
  ,minn
  ,maxn
  ,amu
  ,mf_flag
  ,hit6_sales
  ,aver_cost
  ,item_cost
  ,bpa_cost
  ,bpa
  ,qoh
  ,available
  ,availabledollar
  ,jan_sales
  ,feb_sales
  ,mar_sales
  ,apr_sales
  ,may_sales
  ,june_sales
  ,jul_sales
  ,aug_sales
  ,sep_sales
  ,oct_sales
  ,nov_sales
  ,dec_sales
  ,hit4_sales
  ,one_sales
  ,six_sales
  ,twelve_sales
  ,bin_loc
  ,mc
  ,fi_flag
  ,freeze_date
  ,res
  ,thirteen_wk_avg_inv
  ,thirteen_wk_an_cogs
  ,turns
  ,buyer
  ,ts
  ,so
  ,inventory_item_id
  ,organization_id
  ,set_of_books_id
  ,org_name
  ,district
  ,region
  ,on_ord
  ,inv_cat_seg1
  ,wt
  ,ss
  ,fml
  ,open_req
  ,sourcing_rule
  ,clt
  ,common_output_id
  ,process_id
  ,avail2
  ,int_req
  ,dir_req
  ,demand
)
AS
   SELECT "ORG"
         ,"PRE"
         ,"ITEM_NUMBER"
         ,"VENDOR_NUM"
         ,"VENDOR_NAME"
         ,"SOURCE"
         ,"ST"
         ,"DESCRIPTION"
         ,"CAT"
         ,"PPLT"
         ,"PLT"
         ,"UOM"
         ,"CL"
         ,"STK_FLAG"
         ,"PM"
         ,"MINN"
         ,"MAXN"
         ,"AMU"
         ,"MF_FLAG"
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (  NVL (hit6_store_sales, 0)
                     + NVL (hit6_other_inv_sales, 0))
            ,NVL (hit6_store_sales, 0))
             hit6_sales
         ,"AVER_COST"
         ,"ITEM_COST"
         ,"BPA_COST"
         ,"BPA"
         ,"QOH"
         ,"AVAILABLE"
         ,"AVAILABLEDOLLAR"
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (jan_store_sale, 0) + NVL (jan_other_inv_sale, 0))
            ,NVL (jan_store_sale, 0))
             jan_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (feb_store_sale, 0) + NVL (feb_other_inv_sale, 0))
            ,NVL (feb_store_sale, 0))
             feb_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (mar_store_sale, 0) + NVL (mar_other_inv_sale, 0))
            ,NVL (mar_store_sale, 0))
             mar_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (apr_store_sale, 0) + NVL (apr_other_inv_sale, 0))
            ,NVL (apr_store_sale, 0))
             apr_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (may_store_sale, 0) + NVL (may_other_inv_sale, 0))
            ,NVL (may_store_sale, 0))
             may_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (jun_store_sale, 0) + NVL (jun_other_inv_sale, 0))
            ,NVL (jun_store_sale, 0))
             june_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (jul_store_sale, 0) + NVL (jul_other_inv_sale, 0))
            ,NVL (jul_store_sale, 0))
             jul_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (aug_store_sale, 0) + NVL (aug_other_inv_sale, 0))
            ,NVL (aug_store_sale, 0))
             aug_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (sep_store_sale, 0) + NVL (sep_other_inv_sale, 0))
            ,NVL (sep_store_sale, 0))
             sep_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (oct_store_sale, 0) + NVL (oct_other_inv_sale, 0))
            ,NVL (oct_store_sale, 0))
             oct_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (nov_store_sale, 0) + NVL (nov_other_inv_sale, 0))
            ,NVL (nov_store_sale, 0))
             nov_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (dec_store_sale, 0) + NVL (dec_other_inv_sale, 0))
            ,NVL (dec_store_sale, 0))
             dec_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (  NVL (hit4_store_sales, 0)
                     + NVL (hit4_other_inv_sales, 0))
            ,NVL (hit4_store_sales, 0))
             hit4_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (one_store_sale, 0) + NVL (one_other_inv_sale, 0))
            ,NVL (one_store_sale, 0))
             one_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (NVL (six_store_sale, 0) + NVL (six_other_inv_sale, 0))
            ,NVL (six_store_sale, 0))
             six_sales
         ,DECODE (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_rpt_dc_mod_sub
            ,'Yes', (  NVL (twelve_store_sale, 0)
                     + NVL (twelve_other_inv_sale, 0))
            ,NVL (twelve_store_sale, 0))
             twelve_sales
         ,"BIN_LOC"
         ,"MC"
         ,"FI_FLAG"
         ,"FREEZE_DATE"
         ,"RES"
         ,"THIRTEEN_WK_AVG_INV"
         ,"THIRTEEN_WK_AN_COGS"
         ,"TURNS"
         ,"BUYER"
         ,"TS"
         ,"SO"
         ,"INVENTORY_ITEM_ID"
         ,"ORGANIZATION_ID"
         ,"SET_OF_BOOKS_ID"
         ,"ORG_NAME"
         ,"DISTRICT"
         ,"REGION"
         ,"ON_ORD"
         ,"INV_CAT_SEG1"
         ,wt
         ,ss
         ,fml
         ,open_req
         ,sourcing_rule
         ,clt
         ,common_output_id
         ,process_id
         ,avail2
         ,int_req
         ,dir_req
         ,demand
     --  xxeis.eis_rs_common_outputs_s.NEXTVAL common_output_id
     FROM xxeis.eis_xxwc_po_isr_qa_tab
    WHERE 1 = 1;


