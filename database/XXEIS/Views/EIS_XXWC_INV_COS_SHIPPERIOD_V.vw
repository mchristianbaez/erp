---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_COS_SHIPPERIOD_V $
  Module Name : Inventory
  PURPOSE	  : Inventory � COS, Shipped Prior Period
  REVISIONS   :
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     21-Sep-2016        	Siva   		 TMS#20160824-00059  Performance Tuning

**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_INV_COS_SHIPPERIOD_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_COS_SHIPPERIOD_V AS
SELECT /*+ INDEX(oap XXWC_OBIEE_ORG_ACT_PERIODS) use_nl(mmt,mp)*/ --added for version 1.1
  mmt.transaction_type_id,
  mmt.subinventory_code,
  mmt.actual_cost,
  mmt.acct_period_id,
  oap.period_name,
  msib.segment1 msib_segment1,
  mp.organization_code,
  mtt.transaction_type_name,
  oola.reference_line_id,
  oola.line_number,
  oola.shipment_number,
  ooha.order_number,
  mta.creation_date,
  mta.accounting_line_type,
  mta.primary_quantity,
  SUM(mta.base_transaction_value) sum_transaction_value,
  gcc.concatenated_segments,
  gcc.segment1,
  gcc.segment2,
  gcc.segment3,
  gcc.segment4,
  gcc.segment5,
  gcc.segment6,
  gcc.segment7,
  ml.meaning
FROM mtl_material_transactions mmt,
  org_acct_periods oap,
  mtl_system_items_b msib,
  mtl_parameters mp,
  mtl_transaction_types mtt,
  oe_order_lines_all oola,
  oe_order_headers_all ooha,
  mtl_transaction_accounts mta,
  apps.gl_code_combinations_kfv gcc,
  --  apps.oe_transaction_types_all otta, --commented for version 1.1
  apps.mfg_lookups ml
WHERE mmt.transaction_type_id = 10008
AND mmt.acct_period_id        = oap.acct_period_id
AND mmt.organization_id       = oap.organization_id --added for version 1.1
AND mmt.inventory_item_id     = msib.inventory_item_id
AND mmt.organization_id       = msib.organization_id
AND mmt.organization_id       = mp.organization_id
AND mmt.transaction_type_id   = mtt.transaction_type_id
AND mmt.trx_source_line_id    = oola.line_id
AND oola.header_id            = ooha.header_id
AND mmt.transaction_id        = mta.transaction_id
AND mta.accounting_line_type  = 35 -- Cost of Goods Sold
AND mta.reference_account     = gcc.code_combination_id
  --AND oola.line_type_id        = otta.transaction_type_id --commented for version 1.1
AND mta.accounting_line_type = ml.lookup_code(+)
AND ml.lookup_type(+)        = 'CST_ACCOUNTING_LINE_TYPE'
AND NOT EXISTS
  (SELECT 1
  FROM mtl_material_transactions mmt2
  WHERE mmt2.transaction_type_id = 33
  AND mmt2.trx_source_line_id    = mmt.trx_source_line_id
  AND mmt2.acct_period_id        = mmt.acct_period_id
  )
GROUP BY mmt.transaction_type_id,
  mmt.subinventory_code,
  mmt.actual_cost,
  mmt.acct_period_id,
  oap.period_name,
  msib.segment1 ,
  mp.organization_code,
  mtt.transaction_type_name,
  oola.reference_line_id,
  oola.line_number,
  oola.shipment_number,
  ooha.order_number,
  mta.creation_date,
  mta.accounting_line_type,
  mta.primary_quantity,
  gcc.concatenated_segments,
  gcc.segment1,
  gcc.segment2,
  gcc.segment3,
  gcc.segment4,
  gcc.segment5,
  gcc.segment6,
  gcc.segment7,
  ml.meaning
  --ORDER BY SUM(MTA.BASE_TRANSACTION_VALUE) DESC --commented for version 1.1
/
