CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_po_fill_rate_det_v
(
   order_number
  ,sales_order_line_number
  ,item
  ,user_item_description
  ,total_lines
  ,backordered_line
  ,region
  ,district
  ,org
  ,dpm_rpm
  ,status
)
AS
   SELECT /*+ no_parallel(isr) no_parallel(tmp) no_parallel(msi) no_parallel(ol) no_parallel(oh) */
         oh.order_number
         ,DECODE (ol.option_number
                 ,'', (ol.line_number || '.' || ol.shipment_number)
                 , (ol.line_number || '.' || ol.shipment_number || '.' || ol.option_number))
             sales_order_line_number
         ,msi.segment1 item
         ,user_item_description user_item_description
         ,CASE
             WHEN (   ol.split_from_line_id IS NOT NULL
                   OR msi.item_type = 'SPECIAL'
                   OR ol.source_type_code = 'EXTERNAL')
             THEN
                NULL
             ELSE
                1
          END
             total_lines
         ,CASE
             WHEN (    (   user_item_description = 'BACKORDERED'
                        OR user_item_description = 'PARTIAL BACKORDERED')
                   AND msi.item_type != 'SPECIAL'
                   AND ol.source_type_code != 'EXTERNAL')
             THEN
                1
             ELSE
                NULL
          END
             backordered_line
         ,isr.region
         ,isr.district
         ,isr.org
         ,ffv.attribute1 dpm_rpm
         ,ol.flow_status_code status
     FROM xxeis.eis_xxwc_po_isr_tab isr
         ,xxeis.eis_xxwc_fill_rate_tmp_tbl tmp
         ,apps.mtl_system_items_b_kfv msi
         ,apps.oe_order_lines_all ol
         ,apps.oe_order_headers_all oh
         ,apps.fnd_flex_value_sets ffvs
         ,apps.fnd_flex_values ffv
    WHERE     1 = 1
          AND isr.organization_id = ol.ship_from_org_id
          AND isr.inventory_item_id = ol.inventory_item_id
          AND msi.organization_id = isr.organization_id
          AND msi.inventory_item_id = isr.inventory_item_id
          AND ol.flow_status_code != 'CANCELLED'
          AND flex_value_set_name = 'XXWC_DISTRICT'
          AND msi.item_type != 'SPECIAL'
          AND ol.source_type_code != 'EXTERNAL'
          AND msi.inventory_item_status_code != 'Intangible'
          AND ffvs.flex_value_set_id = ffv.flex_value_set_id
          AND isr.district = ffv.flex_value
          AND tmp.header_id = oh.header_id
          AND tmp.process_id = xxeis.eis_inv_xxwc_prodfill_rate_pkg.get_process_id
          AND ol.header_id = tmp.header_id
          AND NOT EXISTS
                     (SELECT 1
                        FROM oe_order_lines_all ol1
                       WHERE     ol1.header_id = ol.header_id
                             AND TRUNC (actual_shipment_date) < xxeis.eis_rs_xxwc_com_util_pkg.get_date_from);