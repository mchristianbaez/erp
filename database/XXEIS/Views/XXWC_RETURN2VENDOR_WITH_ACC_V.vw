CREATE OR REPLACE FORCE VIEW xxeis.xxwc_return2vendor_with_acc_v
(
   return_type
  ,transaction_id
  ,po_number
  ,return_number
  ,branch
  ,org_id
  ,gl_date
  ,accounting_line_type
  ,gl_account_string
  ,gcc_product
  ,gcc_location
  ,gcc_account
  ,vendornum
  ,vendorname
  ,return_date
  ,price
  ,credit_applied
)
AS
     SELECT "RETURN_TYPE"
           ,"TRANSACTION_ID"
           ,"PO_NUMBER"
           ,"RETURN_NUMBER"
           ,"BRANCH"
           ,"ORG_ID"
           ,"GL_DATE"
           ,"ACCOUNTING_LINE_TYPE"
           ,"GL_ACCOUNT_STRING"
           ,"GCC_PRODUCT"
           ,"GCC_LOCATION"
           ,"GCC_ACCOUNT"
           ,"VENDORNUM"
           ,"VENDORNAME"
           ,"RETURN_DATE"
           ,"PRICE"
           ,"CREDIT_APPLIED"
       FROM (SELECT 'Non-Standard Return' return_type
                   ,mmt.transaction_id
                   ,NULL po_number
                   ,TO_CHAR (mmt.attribute2) return_number
                   ,invorg.organization_code branch
                   ,invorg.operating_unit org_id
                   ,TRUNC (mta.transaction_date) gl_date
                   ,xdl.accounting_line_code accounting_line_type
                   ,gcc.concatenated_segments gl_account_string
                   ,gcc.segment1 gcc_product
                   ,gcc.segment2 gcc_location
                   ,gcc.segment4 gcc_account
                   ,TO_CHAR (mmt.attribute1) vendornum
                   , (SELECT vendor_name
                        FROM ap_suppliers
                       WHERE segment1 = mmt.attribute1)
                       vendorname
                   ,mmt.transaction_date return_date
                   ,                       --mta.base_transaction_value amount
                    NVL (mmt.transaction_quantity, 0) * NVL (mmt.attribute3, 0)
                       price
                   ,DECODE (
                       (SELECT xx.segment1
                          FROM apps.xxwc_return_to_vendor_v xx
                         WHERE     xx.segment1 = NVL (mmt.attribute1, -1)
                               AND xx.attribute1 = TO_CHAR (mmt.transaction_id) --AND xx.segment1 = NVL (:p_vendor_num, xx.segment1)
                                                                               )
                      ,NULL, 'No'
                      ,'Yes')
                       credit_applied
               FROM mtl_material_transactions mmt
                   ,mtl_transaction_types mtt
                   ,mtl_transaction_accounts mta
                   ,gl_code_combinations_kfv gcc
                   ,xla_distribution_links xdl
                   ,org_organization_definitions invorg
              WHERE     1 = 1
                    AND invorg.organization_id = mmt.organization_id
                    AND mmt.transaction_type_id = mtt.transaction_type_id
                    AND mtt.transaction_type_name = 'WC Return to Vendor'
                    AND mta.transaction_id(+) = mmt.transaction_id
                    --AND mta.accounting_line_type(+) =2 --Accrual
                    AND gcc.code_combination_id(+) = mta.reference_account
                    AND application_id(+) = 707
                    AND source_distribution_type(+) =
                           'MTL_TRANSACTION_ACCOUNTS'
                    AND source_distribution_id_num_1(+) = mta.inv_sub_ledger_id
             --AND NVL (mmt.attribute1, -1) =NVL (:p_vendor_num, NVL (mmt.attribute1, -1))
             --AND TRUNC (mmt.transaction_date) BETWEEN fnd_date.canonical_to_date(:p_rvdatefrom) and fnd_date.canonical_to_date(:p_rvdateto))
             -- WHERE 1 =1
             --AND credit_applied = DECODE (:p_show_yn, 'Y', 'No', 'Yes')
             UNION ALL
             SELECT *
               FROM (SELECT DISTINCT
                            'Standard Return' return_type
                           ,rt.transaction_id
                           ,TO_CHAR (poh.segment1) po_number
                           ,TO_CHAR (rt.rma_reference) return_number
                           ,invorg.organization_code branch
                           ,invorg.operating_unit org_id
                           --                        ,TO_CHAR ((SELECT organization_code
                           --                                    FROM org_organization_definitions
                           --                                   WHERE organization_id = rt.organization_id)
                           --                                ) LOCATION
                           ,TRUNC (rsl.accounting_date) gl_date
                           ,UPPER (rsl.accounting_line_type)
                               accounting_line_type
                           ,gcc.concatenated_segments gl_account_string
                           ,gcc.segment1 gcc_product
                           ,gcc.segment2 gcc_location
                           ,gcc.segment4 gcc_account
                           ,TO_CHAR (pv.segment1) vendornum
                           ,pv.vendor_name vendorname
                           ,rt.transaction_date return_date
                           ,NVL (pol.unit_price, 0) * NVL (-rt.quantity, 0)
                               price
                           ,NVL (
                               (SELECT 'Yes'
                                  FROM ap_invoices a, ap_invoice_lines b
                                 WHERE     a.invoice_id = b.invoice_id
                                       AND a.invoice_type_lookup_code <>
                                              'STANDARD'
                                       AND b.po_distribution_id =
                                              pod.po_distribution_id
                                       AND ROWNUM = 1)
                              ,'No')
                               credit_applied
                       FROM rcv_transactions rt
                           ,po_headers poh
                           ,po_lines pol
                           ,po_distributions pod
                           ,                                           --added
                            ap_suppliers pv
                           ,po_line_locations pll
                           ,rcv_receiving_sub_ledger rsl
                           ,gl_code_combinations_kfv gcc
                           ,org_organization_definitions invorg
                      WHERE     1 = 1
                            AND invorg.organization_id = rt.organization_id
                            AND rt.po_line_id = pol.po_line_id
                            AND rt.po_header_id = poh.po_header_id
                            AND rt.po_distribution_id = pod.po_distribution_id
                            AND pv.vendor_id = rt.vendor_id
                            AND pol.po_line_id = pll.po_line_id
                            AND poh.po_header_id = pll.po_header_id
                            AND rt.transaction_type = 'RETURN TO VENDOR'
                            --AND TRUNC (rt.transaction_date) BETWEEN fnd_date.canonical_to_date(:p_rvdatefrom) and fnd_date.canonical_to_date(:p_rvdateto)
                            AND rsl.rcv_transaction_id(+) = rt.transaction_id
                            --AND rsl.accounting_line_type ='Accrual'
                            AND gcc.code_combination_id(+) =
                                   rsl.code_combination_id --AND pv.segment1 = NVL (:p_vendor_num, pv.segment1)
                                                          ))
   ORDER BY gl_date, transaction_id;


