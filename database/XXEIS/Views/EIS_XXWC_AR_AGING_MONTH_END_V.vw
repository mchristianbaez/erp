---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_AGING_MONTH_END_V $
  Module Name : Receivables
  PURPOSE	  : 'Accounts Receivable Aging - MonthEnd - WC'
  TMS Task Id : 20161020-00074 
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	   02-Nov-2016        	Siva   		 Initial version--TMS#20161020-00074
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_AGING_MONTH_END_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_AGING_MONTH_END_V (CUST_ACCOUNT_ID, CUSTOMER_ACCOUNT_NUMBER, PRISM_CUSTOMER_NUMBER, CUSTOMER_NAME, BILL_TO_SITE_USE_ID, BILL_TO_PARTY_SITE_NUMBER, BILL_TO_PRISM_SITE_NUMBER, BILL_TO_PARTY_SITE_NAME, BILL_TO_SITE_NAME, TRX_BILL_TO_SITE_NAME, TRX_PARTY_SITE_NUMBER, TRX_PARTY_SITE_NAME, BILL_TO_ADDRESS1, BILL_TO_ADDRESS2, BILL_TO_ADDRESS3, BILL_TO_ADDRESS4, BILL_TO_CITY, BILL_TO_CITY_PROVINCE, BILL_TO_ZIP_CODE, BILL_TO_COUNTRY, PAYMENT_SCHEDULE_ID, CUSTOMER_TRX_ID, RCTA_CCID, CASH_RECEIPT_ID, ACRA_CCID, INVOICE_NUMBER, RECEIPT_NUMBER, BRANCH_LOCATION, TRX_NUMBER, TRX_DATE, DUE_DATE, TRX_TYPE, TRANSATION_BALANCE, TRANSACTION_REMAINING_BALANCE, AGE, CURRENT_BALANCE, THIRTY_DAYS_BAL, SIXTY_DAYS_BAL, NINETY_DAYS_BAL, ONE_EIGHTY_DAYS_BAL, THREE_SIXTY_DAYS_BAL, OVER_THREE_SIXTY_DAYS_BAL, LAST_PAYMENT_DATE, CUSTOMER_ACCOUNT_STATUS, SITE_CREDIT_HOLD, CUSTOMER_PROFILE_CLASS, COLLECTOR_NAME, CREDIT_ANALYST, ACCOUNT_MANAGER, ACCOUNT_BALANCE, CUST_PAYMENT_TERM
  , REMIT_TO_ADDRESS_CODE, STMT_BY_JOB, SEND_STATEMENT_FLAG, SEND_CREDIT_BAL_FLAG, TRX_CUSTOMER_ID, TRX_BILL_SITE_USE_ID, CUSTOMER_PO_NUMBER, PMT_STATUS, SALESREP_NUMBER, CREDIT_LIMIT, TWELVE_MONTHS_SALES, TERMS, PHONE_NUMBER, IN_PROCESS, LARGEST_BALANCE, AVERAGE_DAYS, AMOUNT_PAID, ACCOUNT_STATUS, TERRITORY, TRX_BILL_TO_ADDRESS1, TRX_BILL_TO_ADDRESS2, TRX_BILL_TO_ADDRESS3, TRX_BILL_TO_ADDRESS4, TRX_BILL_TO_CITY, TRX_BILL_TO_CITY_PROV, TRX_BILL_TO_ZIP_CODE, TRX_BILL_TO_COUNTRY, TRX_ADDRESS)
AS
  SELECT cust_account_id ,
    customer_account_number ,
    prism_customer_number ,
    customer_name ,
    bill_to_site_use_id ,
    bill_to_party_site_number ,
    bill_to_prism_site_number ,
    bill_to_party_site_name ,
    bill_to_site_name ,
    REPLACE(trx_bill_to_site_name,'"','') trx_bill_to_site_name,
    trx_party_site_number ,
    trx_party_site_name ,
    bill_to_address1 ,
    bill_to_address2 ,
    bill_to_address3 ,
    bill_to_address4 ,
    bill_to_city ,
    bill_to_city_province ,
    bill_to_zip_code ,
    bill_to_country ,
    payment_schedule_id ,
    customer_trx_id ,
    rcta_ccid ,
    cash_receipt_id ,
    acra_ccid ,
    invoice_number ,
    receipt_number ,
    branch_location ,
    trx_number ,
    trx_date ,
    due_date ,
    trx_type ,
    transation_balance ,
    transaction_remaining_balance ,
    age ,
    current_balance ,
    thirty_days_bal ,
    sixty_days_bal ,
    ninety_days_bal ,
    one_eighty_days_bal ,
    three_sixty_days_bal ,
    over_three_sixty_days_bal ,
    last_payment_date ,
    customer_account_status ,
    site_credit_hold ,
    customer_profile_class ,
    collector_name ,
    credit_analyst ,
    account_manager ,
    account_balance ,
    cust_payment_term ,
    remit_to_address_code ,
    stmt_by_job ,
    send_statement_flag ,
    send_credit_bal_flag ,
    trx_customer_id ,
    trx_bill_site_use_id ,
    customer_po_number ,
    pmt_status ,
    salesrep_number salesrep_number ,
    yard_credit_limit credit_limit ,
    twelve_months_sales twelve_months_sales ,
    cust_payment_term terms ,
    acct_phone_number phone_number ,
    NULL in_process ,
    to_number (NULL) largest_balance ,
    to_number (NULL) average_days ,
    to_number (NULL) amount_paid ,
    account_status account_status ,
    NULL territory ,
    trx_bill_to_address1 ,
    trx_bill_to_address2 ,
    trx_bill_to_address3 ,
    trx_bill_to_address4 ,
    trx_bill_to_city ,
    trx_bill_to_city_prov ,
    trx_bill_to_zip_code ,
    trx_bill_to_country ,
    trx_bill_to_address1
    || ','
    || DECODE (trim (trx_bill_to_address2), NULL, NULL, trx_bill_to_address2
    || ',')
    || trx_bill_to_city
    || ','
    || trx_bill_to_city_prov
    || ','
    || trx_bill_to_zip_code trx_address
  FROM xxwc.xxwc_ar_cust_bal_mv_monthly
/
