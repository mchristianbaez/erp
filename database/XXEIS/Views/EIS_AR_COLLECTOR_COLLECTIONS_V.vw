---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_AR_COLLECTOR_COLLECTIONS_V $
  Module Name : Receivables
  PURPOSE	  : Collections by Collector
  TMS Task Id : 20160831-00033 
  REVISIONS   :
  VERSION 			DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
   1.0     		03-Oct-2016       		Siva   		 TMS#20160831-00033 
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_AR_COLLECTOR_COLLECTIONS_V;

CREATE OR REPLACE VIEW XXEIS.EIS_AR_COLLECTOR_COLLECTIONS_V (CR_DATE, CC_FLAG, COLLECTOR, CUSTOMER, CUSTOMER_NUMBER, CUST_TRX_TYPE_NAME, CR_AMOUNT, INVOICE_AMOUNT, INVOICE_NUMBER, DUE_DATE, FULLY_PAID, DAYS_LATE, CURRENCY_CODE, CUSTOMER_STATUS, ORIG_SYSTEM_REFERENCE, CUSTOMER_TYPE, CUSTOMER_CLASS_CODE, CATEGORY_CODE, OPERATING_UNIT, CUST_TRX_TYPE_ID, PARTY_ID, CUST_ACCOUNT_ID, COLLECTOR_ID, PROF_CUST_ACCOUNT_PROFILE_ID, PROF2_CUST_ACCOUNT_PROFILE_ID, CASH_RECEIPT_ID, PAYMENT_SCHEDULE_ID, RECEIVABLE_APPLICATION_ID, ORGANIZATION_ID)
AS
  SELECT /*+ INDEX(types RA_CUST_TRX_TYPES_U1) INDEX(prof XXWC_OBIEE_HZ_CUST_PRFLS_3) INDEX(prof2 XXWC_OBIEE_HZ_CUST_PRFLS_3)*/
    apps.apply_date cr_date,
    DECODE (SUM (apps.amount_applied_from), NULL, NULL, '*') cc_flag,
    coll.NAME collector,
    NVL (cust.account_name, party.party_name) customer,
    cust.account_number customer_number,
    TYPES.NAME cust_trx_type_name,
    SUM (apps.amount_applied) cr_amount,
    ps.amount_due_original invoice_amount,
    ps.trx_number invoice_number,
    ps.due_date due_date,
    DECODE (ps.status, 'OP', 'No', '') fully_paid,
    CEIL (apps.apply_date - ps.due_date) days_late,
    ps.invoice_currency_code currency_code,
    DECODE (cust.status, 'A', 'Active', 'I', 'Inactive', cust.status) customer_status,
    cust.orig_system_reference,
    cust.customer_type,
    cust.customer_class_code,
    party.category_code,
    hou.NAME operating_unit,
    --Primary Keys
    TYPES.cust_trx_type_id,
    party.party_id,
    cust.cust_account_id,
    coll.collector_id,
    prof.cust_account_profile_id prof_cust_account_profile_id,
    prof2.cust_account_profile_id prof2_cust_account_profile_id,
    cash.cash_receipt_id,
    ps.payment_schedule_id,
    apps.receivable_application_id,
    hou.organization_id
    --gl#accountff#start
    --gl#accountff#end
  FROM ar_receivable_applications apps,
    ar_payment_schedules ps,
    ar_cash_receipts cash,
    ra_cust_trx_types types,
    hr_operating_units hou,
    hz_cust_accounts cust,
    hz_parties party,
    hz_customer_profiles prof,
    hz_customer_profiles prof2,
    ar_collectors coll
  WHERE apps.applied_payment_schedule_id          = ps.payment_schedule_id
  AND apps.display                                = 'Y'
  AND NVL (apps.confirmed_flag, 'Y')              = 'Y'
  AND apps.cash_receipt_id                        = cash.cash_receipt_id
  AND cash.reversal_date                         IS NULL
  AND NVL (cash.confirmed_flag, 'Y')              = 'Y'
  AND ps.cust_trx_type_id                         = TYPES.cust_trx_type_id
  AND ps.org_id                                   = types.org_id
  AND types.org_id                                = hou.organization_id
  AND ps.customer_id + 0                          = cust.cust_account_id
  AND cust.party_id                               = party.party_id
  AND ps.customer_id                              = prof.cust_account_id
  AND prof.site_use_id                           IS NULL
  AND prof.collector_id                           = coll.collector_id
  AND ps.customer_id                              = prof2.cust_account_id(+)
  AND ps.customer_site_use_id                     = prof2.site_use_id(+)
  AND NVL (prof2.collector_id, prof.collector_id) = coll.collector_id
  GROUP BY apps.apply_date ,
    coll.NAME ,
    NVL (cust.account_name, party.party_name) ,
    cust.account_number ,
    TYPES.NAME ,
    ps.amount_due_original ,
    ps.trx_number ,
    ps.due_date ,
    DECODE (ps.status, 'OP', 'No', '') ,
    CEIL (apps.apply_date - ps.due_date) ,
    ps.invoice_currency_code ,
    DECODE (cust.status, 'A', 'Active', 'I', 'Inactive', cust.status) ,
    cust.orig_system_reference,
    cust.customer_type,
    cust.customer_class_code,
    party.category_code,
    hou.NAME ,
    --Primary Keys
    TYPES.cust_trx_type_id,
    party.party_id,
    cust.cust_account_id,
    coll.collector_id,
    prof.cust_account_profile_id ,
    prof2.cust_account_profile_id ,
    cash.cash_receipt_id,
    ps.payment_schedule_id,
    apps.receivable_application_id,
    hou.organization_id
    --gl#accountff#groupbystart
    --gl#accountff#groupbyend
/
