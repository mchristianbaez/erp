CREATE OR REPLACE FORCE VIEW XXEIS.EIS_XXWC_AR_DAILY_FLASH_NEW_V
(
   DATE1,
   BRANCH_LOCATION,
   SALE_AMT,
   SALE_COST,
   PAYMENT_TYPE,
   INVOICE_COUNT
)
AS
     SELECT DATE1,
            BRANCH_LOCATION,
            SUM (sale_amt) SALE_AMT,
            SUM (SALE_COST),
            PAYMENT_TYPE,
            COUNT (
               DISTINCT XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DIST_INVOICE_NUM (
                           a.CUSTOMER_TRX_ID,
                           a.TRX_NUMBER,
                           a.name))
               INVOICE_COUNT
       FROM (SELECT TRUNC (XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM) DATE1,
                    OOD.ORGANIZATION_CODE BRANCH_LOCATION,
                    ( (DECODE (
                          XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PROMO_ITEM (
                             OL.INVENTORY_ITEM_ID,
                             OL.SHIP_FROM_ORG_ID),
                          'N', (  NVL (RCTL.QUANTITY_INVOICED,
                                       RCTL.QUANTITY_CREDITED)
                                * NVL (OL.UNIT_SELLING_PRICE, 0)),
                          'Y', 0)))
                       sale_amt,
                    (DECODE (
                        XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PROMO_ITEM (
                           OL.INVENTORY_ITEM_ID,
                           OL.SHIP_FROM_ORG_ID),
                        'N',   NVL (RCTL.QUANTITY_INVOICED,
                                    RCTL.QUANTITY_CREDITED)
                             * NVL (
                                  APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST (
                                     OL.LINE_ID),
                                  0),
                        'Y',   -1
                             * (  NVL (RCTL.QUANTITY_INVOICED,
                                       RCTL.QUANTITY_CREDITED)
                                * NVL (OL.UNIT_SELLING_PRICE, 0))))
                       SALE_COST,
                    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE
                       PAYMENT_TYPE,
                    RCT.CUSTOMER_TRX_ID,
                    RCT.TRX_NUMBER,
                    OTLH.name
               ---Primary Keys
               FROM RA_CUSTOMER_TRX RCT,
                    RA_CUSTOMER_TRX_LINES RCTL,
                    OE_ORDER_HEADERS OH,
                    oe_order_lines ol,
                    org_organization_definitions ood,
                    OE_TRANSACTION_TYPES_VL OTL,
                    oe_transaction_types_vl otlh
              WHERE     1 = 1
                    --AND trunc(Rct.creation_date) = trunc(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
                    AND rctL.creation_date >=
                             TO_DATE (
                                TO_CHAR (
                                   xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                                      xxeis.eis_rs_utility.get_date_format
                                   || ' HH24:MI:SS'),
                                   xxeis.eis_rs_utility.get_date_format
                                || ' HH24:MI:SS')
                           + 0.25
                    AND rctL.creation_date <=
                             TO_DATE (
                                TO_CHAR (
                                   xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                                      xxeis.eis_rs_utility.get_date_format
                                   || ' HH24:MI:SS'),
                                   xxeis.eis_rs_utility.get_date_format
                                || ' HH24:MI:SS')
                           + 1.25
                    --and TO_CHAR(RCT.CREATION_DATE,'DD-MON-YY HH:MM:SS')    >= TO_CHAR(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM ,'DD-MON-YY HH:MM:SS') +0.25
                    -- AND TO_CHAR(Rct.creation_date ,'DD-MON-YY HH:MM:SS')   <= to_char(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from ,'DD-MON-YY HH:MM:SS') +1.25
                    AND TO_CHAR (oh.order_number) =
                           TO_CHAR (rct.interface_header_attribute1)
                    AND ood.organization_id(+) = ol.ship_from_org_id
                    AND TO_CHAR (rctl.interface_line_attribute6) =
                           TO_CHAR (ol.line_id)
                    AND rctl.inventory_item_id = ol.inventory_item_id
                    AND otl.transaction_type_id = ol.line_type_id
                    AND otlh.transaction_type_id = oh.order_type_id
                    AND RCTL.CUSTOMER_TRX_ID = RCT.CUSTOMER_TRX_ID
                    -- and oh.order_number='10394217'
                    --AND RCT.CUSTOMER_TRX_ID IN (2737270,2737271)
                    AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
                    /* AND NOT EXISTS(SELECT 1
                    from dual
                    where ol.ordered_item  in('CONTOFFSET','CONTBILL')
                    )*/
                    AND ol.header_id = oh.header_id
                    AND rctl.interface_line_context = 'ORDER ENTRY'
                    AND RCT.INTERFACE_HEADER_CONTEXT = 'ORDER ENTRY'
                    AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DIST_INVOICE_NUM (
                           RCT.CUSTOMER_TRX_ID,
                           RCT.TRX_NUMBER,
                           OTLH.NAME)
                           IS NOT NULL
                    AND RCTL.DESCRIPTION <> 'Delivery Charge'
                    AND NOT EXISTS
                               (SELECT 1
                                  FROM hz_customer_profiles hcp,
                                       hz_cust_profile_classes hcpc,
                                       hz_cust_accounts hca
                                 WHERE     hca.party_id = hcp.party_id
                                       AND oh.sold_to_org_id =
                                              hcp.cust_account_id
                                       AND hcp.site_use_id IS NULL
                                       AND hcp.profile_class_id =
                                              hcpc.profile_class_id(+)
                                       AND hcpc.name LIKE 'WC%Branches%')
                    AND NOT EXISTS
                               (SELECT 'Y'
                                  FROM MTL_SYSTEM_ITEMS_B MSI,
                                       GL_CODE_COMBINATIONS_KFV GCC
                                 WHERE     1 = 1
                                       AND MSI.INVENTORY_ITEM_ID =
                                              OL.INVENTORY_ITEM_ID
                                       AND MSI.ORGANIZATION_ID =
                                              OL.SHIP_FROM_ORG_ID
                                       AND GCC.CODE_COMBINATION_ID =
                                              MSI.COST_OF_SALES_ACCOUNT
                                       AND GCC.SEGMENT4 = '646080')
                    /* AND NOT EXISTS
                    (SELECT 1
                    FROM OE_PRICE_ADJUSTMENTS_V
                    WHERE HEADER_ID     = OL.HEADER_ID
                    AND LINE_ID         = OL.LINE_ID
                    AND adjustment_name ='BRANCH_COST_MODIFIER'
                    )*/
                    AND (   (    xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type =
                                    'CASH'
                             AND (   EXISTS
                                        (SELECT 1
                                           FROM oe_payments oep
                                          WHERE ( (    oep.payment_type_code IN ('CASH',
                                                                                 'CHECK',
                                                                                 'CREDIT_CARD')
                                                   AND oep.header_id =
                                                          ol.header_id)))
                                  OR (    otl.order_category_code = 'RETURN'
                                      AND EXISTS
                                             (SELECT 1
                                                FROM xxwc_om_cash_refund_tbl xoc
                                               WHERE xoc.return_header_id =
                                                        oh.header_id))))
                         OR (    xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type =
                                    'CHARGE'
                             AND (   (    NOT EXISTS
                                                 (SELECT 1
                                                    FROM oe_payments oep
                                                   WHERE oep.header_id =
                                                            ol.header_id)
                                      AND otl.order_category_code <> 'RETURN')
                                  OR (    otl.order_category_code = 'RETURN'
                                      AND NOT EXISTS
                                                 (SELECT 1
                                                    FROM xxwc_om_cash_refund_tbl xoc1
                                                   WHERE XOC1.RETURN_HEADER_ID =
                                                            OH.HEADER_ID)))))
             -- and RCT.TRX_NUMBER ='50001058945'
             --  and  OOD.ORGANIZATION_CODE ='042'-- '10001473803'
             UNION ALL
             SELECT TRUNC (XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM) DATE1,
                    OOD.ORGANIZATION_CODE BRANCH_LOCATION,
                    NVL (RCTL.EXTENDED_AMOUNT, 0) SALE_AMT,
                    0 SALE_COST,
                    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE
                       PAYMENT_TYPE,
                    RCT.CUSTOMER_TRX_ID,
                    RCT.TRX_NUMBER,
                    OTLH.name
               FROM RA_CUSTOMER_TRX RCT,
                    RA_CUSTOMER_TRX_LINES RCTL,
                    OE_ORDER_HEADERS OH,
                    ORG_ORGANIZATION_DEFINITIONS OOD,
                    MTL_PARAMETERS MP,
                    OE_TRANSACTION_TYPES_VL OTLH,
                    HZ_CUST_ACCOUNTS HCA,
                    hz_parties hp
              WHERE     1 = 1
                    AND rct.creation_date >=
                             TO_DATE (
                                TO_CHAR (
                                   xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                                      xxeis.eis_rs_utility.get_date_format
                                   || ' HH24:MI:SS'),
                                   xxeis.eis_rs_utility.get_date_format
                                || ' HH24:MI:SS')
                           + 0.25
                    AND rct.creation_date <=
                             TO_DATE (
                                TO_CHAR (
                                   xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                                      xxeis.eis_rs_utility.get_date_format
                                   || ' HH24:MI:SS'),
                                   xxeis.eis_rs_utility.get_date_format
                                || ' HH24:MI:SS')
                           + 1.25
                    AND TO_CHAR (OH.ORDER_NUMBER) =
                           TO_CHAR (RCT.INTERFACE_HEADER_ATTRIBUTE1)
                    AND OOD.ORGANIZATION_ID(+) =
                           TO_CHAR (RCTL.INTERFACE_LINE_ATTRIBUTE10)
                    AND RCTL.SALES_ORDER = TO_CHAR (OH.ORDER_NUMBER)
                    AND RCTL.CUSTOMER_TRX_ID = RCT.CUSTOMER_TRX_ID
                    AND MP.ORGANIZATION_ID = OOD.ORGANIZATION_ID
                    AND otlh.transaction_type_id = oh.order_type_id
                    AND rctl.interface_line_context = 'ORDER ENTRY'
                    AND RCT.INTERFACE_HEADER_CONTEXT = 'ORDER ENTRY'
                    AND rct.bill_to_customer_id = hca.cust_account_id
                    AND HP.PARTY_ID = HCA.PARTY_ID
                    AND RCTL.DESCRIPTION = 'Delivery Charge'
                    AND RCTL.LINE_TYPE = 'LINE'
                    -- and oh.order_number='10394217'
                    --AND RCT.CUSTOMER_TRX_ID IN (2737270,2737271)
                    AND NOT EXISTS
                               (SELECT 1
                                  FROM hz_customer_profiles hcp,
                                       hz_cust_profile_classes hcpc,
                                       hz_cust_accounts hca
                                 WHERE     hca.party_id = hcp.party_id
                                       AND oh.sold_to_org_id =
                                              hcp.cust_account_id
                                       AND hcp.site_use_id IS NULL
                                       AND hcp.profile_class_id =
                                              hcpc.profile_class_id(+)
                                       AND HCPC.name LIKE 'WC%Branches%')
                    AND (   (    xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type =
                                    'CASH'
                             AND (   EXISTS
                                        (SELECT 1
                                           FROM oe_payments oep
                                          WHERE ( (    OEP.PAYMENT_TYPE_CODE IN ('CASH',
                                                                                 'CHECK',
                                                                                 'CREDIT_CARD')
                                                   AND oep.header_id =
                                                          oh.header_id)))
                                  OR (    EXISTS
                                             (SELECT 1
                                                FROM OE_TRANSACTION_TYPES_VL OTL,
                                                     OE_ORDER_LINES_ALL OL
                                               WHERE     OTL.TRANSACTION_TYPE_ID =
                                                            OL.LINE_TYPE_ID
                                                     AND OL.HEADER_ID =
                                                            OH.HEADER_ID
                                                     AND OTL.ORDER_CATEGORY_CODE =
                                                            'RETURN')
                                      AND EXISTS
                                             (SELECT 1
                                                FROM xxwc_om_cash_refund_tbl xoc
                                               WHERE XOC.RETURN_HEADER_ID =
                                                        OH.HEADER_ID))))
                         OR (    xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type =
                                    'CHARGE'
                             AND (   (    NOT EXISTS
                                                 (SELECT 1
                                                    FROM OE_PAYMENTS OEP
                                                   WHERE OEP.HEADER_ID =
                                                            OH.HEADER_ID)
                                      AND (EXISTS
                                              (SELECT 1
                                                 FROM OE_TRANSACTION_TYPES_VL OTL,
                                                      OE_ORDER_LINES_ALL OL
                                                WHERE     OTL.TRANSACTION_TYPE_ID =
                                                             OL.LINE_TYPE_ID
                                                      AND OL.HEADER_ID =
                                                             OH.HEADER_ID
                                                      AND otl.order_category_code <>
                                                             'RETURN')))
                                  OR (    EXISTS
                                             (SELECT 1
                                                FROM OE_TRANSACTION_TYPES_VL OTL,
                                                     OE_ORDER_LINES_ALL OL
                                               WHERE     OTL.TRANSACTION_TYPE_ID =
                                                            OL.LINE_TYPE_ID
                                                     AND OL.HEADER_ID =
                                                            OH.HEADER_ID
                                                     AND OTL.ORDER_CATEGORY_CODE =
                                                            'RETURN')
                                      AND NOT EXISTS
                                                 (SELECT 1
                                                    FROM xxwc_om_cash_refund_tbl xoc1
                                                   WHERE XOC1.RETURN_HEADER_ID =
                                                            OH.HEADER_ID)) --  AND NOT EXISTS
                                                               --    (SELECT 1
                                       --    FROM xxwc_om_cash_refund_tbl xoc1
                              --    where XOC1.RETURN_HEADER_ID = OH.HEADER_ID
                                                                      --    ))
                                 )))      --and RCT.TRX_NUMBER = '50001058945'
                                          -- and  OOD.ORGANIZATION_CODE ='042'
            )                                                 --'10001473803';
             a
   GROUP BY DATE1, BRANCH_LOCATION, PAYMENT_TYPE;