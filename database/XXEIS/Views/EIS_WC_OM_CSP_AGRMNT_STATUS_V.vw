------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_WC_OM_CSP_AGRMNT_STATUS_V
   REVISIONS:
	Ver         Date            Author             Description
   ---------  ----------    ---------------  ------------------------------------
    1.0       14-Mar-2017       Siva	           TMS#20161128-00277 -- Initial Version
	1.1		  11-Apr-2017	    Siva			   TMS#20170331-00013 
******************************************************************************/
DROP VIEW XXEIS.EIS_WC_OM_CSP_AGRMNT_STATUS_V;

CREATE OR REPLACE VIEW XXEIS.EIS_WC_OM_CSP_AGRMNT_STATUS_V (AGREEMENT_ID, START_DATE, SKU, SPECIAL_COST, VQN_NUMBER, AGREEMENT_HEADER_STATUS, AGREEMENT_LINE_STATUS, VENDOR_NUMBER, VENDOR_NAME, BRANCH, REGION, MASTER_ACCOUNT_NAME, MASTER_ACCOUNT_NUMBER, JOB_NAME, JOB_NUMBER, ORGANIZATION_ID, SALESREP_NAME, SALESREP_NUMBER)
AS
  SELECT /*+ INDEX(xxcph SYS_C00483707)*/
    xxcph.agreement_id,
  	-- xxcph.creation_date start_date, 	--commented for version 1.1
    xxcpl.start_date,   				-- added for version 1.1
    xxcpl.product_value sku,
    xxcpl.special_cost,
    xxcpl.vendor_quote_number vqn_number,
    xxcph.agreement_status agreement_header_status,
    xxcpl.line_status agreement_line_status ,
    aps.segment1 vendor_number,
    aps.vendor_name ,
    mp.organization_code branch,
    mp.attribute9 region,
    hp.party_name master_account_name,
    hcs.account_number master_account_number,
    hzcs_ship_to.location job_name,
    hzps_ship_to.party_site_number job_number,
    mp.organization_id,
    ras.name salesrep_name,
    ras.salesrep_number
  FROM xxwc.xxwc_om_contract_pricing_hdr xxcph,
    xxwc.xxwc_om_contract_pricing_lines xxcpl,
    apps.hz_cust_accounts hcs,
    apps.hz_parties hp,
    apps.hz_cust_site_uses_all hzcs_ship_to,
    apps.hz_cust_acct_sites_all hcasa,
    apps.hz_party_sites hzps_ship_to,
    apps.ap_suppliers aps ,
    apps.mtl_parameters mp,
    apps.ra_salesreps ras
  WHERE xxcph.agreement_id             = xxcpl.agreement_id
  AND (xxcph.agreement_status         IN ('DRAFT','AWAITING_APPROVAL')
  OR xxcpl.line_status                IN ('DRAFT','AWAITING_APPROVAL'))
  AND xxcph.customer_id                = hcs.cust_account_id(+)
  AND hcs.party_id                     = hp.party_id(+)
  AND xxcph.customer_site_id           = hzcs_ship_to.site_use_id(+)
  AND hzcs_ship_to.cust_acct_site_id   = hcasa.cust_acct_site_id(+)
  AND hcasa.party_site_id              = hzps_ship_to.party_site_id(+)
  AND xxcpl.vendor_id                  = aps.vendor_id(+)
  AND xxcph.organization_id            = mp.organization_id
  AND hzcs_ship_to.primary_salesrep_id = ras.salesrep_id(+)
/
