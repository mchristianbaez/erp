---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_EIS_INV_ALIAS_TRANS_V $
  Module Name : Inventory
  PURPOSE	  : Account Alias Transactions - WC
  TMS Task Id : 20160429-00031  
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     28-Apr-2016        Siva   		 TMS#20160429-00031   Performance Tuning
**************************************************************************************************************/
DROP VIEW XXEIS.XXWC_EIS_INV_ALIAS_TRANS_V;

CREATE OR REPLACE VIEW XXEIS.XXWC_EIS_INV_ALIAS_TRANS_V(SOURCE, DISTRIBUTION_ACCOUNT, PRODUCT, SEGMENT2, SEGMENT3, SEGMENT4, SEGMENT5, SEGMENT6, ITEM, ITEM_DESCRIPTION, INV_ORG_NAME, ORG_CODE, CAT_CLASS, SUBINVENTORY, TRANSACTION_DATE, CREATION_DATE, CREATED_BY_USER, GL_PERIOD, GL_PERIOD_START_DATE, UNIT_COST, ITEM_REVISION, STOCK_LOCATORS, PROJECT_NAME, PROJECT_NUMBER, QUANTITY, VALUE, REASON, REASON_ID, TASK_NAME, TASK_NUMBER, TRANSACTION_ACTION, TRANSACTION_REFERENCE, TRANSACTION_SOURCE_TYPE_NAME, TRANSACTION_TYPE, UNIT_OF_MEASURE, VALUED_FLAG, MTT_TRANSACTION_TYPE_ID, INVENTORY_LOCATION_ID, TASK_ID, PROJECT_ID, ACCT_PERIOD_ID, TRANSACTION_ID, MMT_TRANSACTION_SOURCE_TYPE_ID, TRANSACTION_ACTION_ID, TRANSACTION_SOURCE_ID, DEPARTMENT_ID, ERROR_EXPLANATION, SUPPLIER_LOT, SOURCE_LINE_ID, PARENT_TRANSACTION_ID, SHIPMENT_NUMBER, WAYBILL_AIRBILL, FREIGHT_CODE, NUMBER_OF_CONTAINERS,
  RCV_TRANSACTION_ID, MOVE_TRANSACTION_ID, COMPLETION_TRANSACTION_ID, OPERTION_SEQUENCE, EXPENDITURE_TYPE, TRANSACTION_SET_ID, TRANSACTION_SOURCE_NAME, TRANSACTION_UOM, TRANSFER_SUBINVENTORY, TRANSFER_TRANSACTION_ID, MP_ORGANIZATION_ID, MIL_ORGANIZATION_ID, OAP_ORGANIZATION_ID, MSIL_INVENTORY_ITEM_ID, MSIL_ORGANIZATION_ID, LANGUAGE, INVENTORY_ITEM_ID, MSI_ORGANIZATION_ID, DISPOSITION_ID, MGP_ORGANIZATION_ID, HAOU_ORGANIZATION_ID, TRANSACTION_SOURCE_TYPE_ID, MMT#WCRENTALA#HOME_BRANCH, MMT#WCRENTALA#VENDOR, MMT#WCRENTALA#PO_NUMBER, MMT#WCRENTALA#ITEM_NUMBER, MMT#WCRENTALA#CER_NUMBER, MMT#WCRENTALA#MISC_ISSUE_TRA, MMT#WCRENTALA#CUSTODIAN_BRAN, MMT#WCRENTALA#MAJOR_CATEGORY, MMT#WCRENTALA#MINOR_CATEGORY, MMT#WCRENTALA#STATE, MMT#WCRENTALA#CITY, MMT#WCRENTALA#TRANSFER_TO_MA, MMT#WCRETURNT#SUPPLIER_NUMBE, MMT#WCRETURNT#RETURN_NUMBER, MMT#WCRETURNT#RETURN_UNIT_PR, MP#FACTORY_PLANNER_DATA_DIRE, MP#FRU,
  MP#LOCATION_NUMBER, MP#BRANCH_OPERATIONS_MANAGER, MP#DELIVER_CHARGE, MP#FACTORY_PLANNER_EXECUTABL, MP#FACTORY_PLANNER_USER, MP#FACTORY_PLANNER_HOST, MP#FACTORY_PLANNER_PORT_NUMB, MP#PRICING_ZONE, MP#ORG_TYPE, MP#DISTRICT, MP#REGION, MSI#HDS#LOB, MSI#HDS#DROP_SHIPMENT_ELIGAB, MSI#HDS#INVOICE_UOM, MSI#HDS#PRODUCT_ID, MSI#HDS#VENDOR_PART_NUMBER, MSI#HDS#UNSPSC_CODE, MSI#HDS#UPC_PRIMARY, MSI#HDS#SKU_DESCRIPTION, MSI#WC#CA_PROP_65, MSI#WC#COUNTRY_OF_ORIGIN, MSI#WC#ORM_D_FLAG, MSI#WC#STORE_VELOCITY, MSI#WC#DC_VELOCITY, MSI#WC#YEARLY_STORE_VELOCITY, MSI#WC#YEARLY_DC_VELOCITY, MSI#WC#PRISM_PART_NUMBER, MSI#WC#HAZMAT_DESCRIPTION, MSI#WC#HAZMAT_CONTAINER, MSI#WC#GTP_INDICATOR, MSI#WC#LAST_LEAD_TIME, MSI#WC#AMU, MSI#WC#RESERVE_STOCK, MSI#WC#TAXWARE_CODE, MSI#WC#AVERAGE_UNITS, MSI#WC#PRODUCT_CODE, MSI#WC#IMPORT_DUTY_, MSI#WC#KEEP_ITEM_ACTIVE, MSI#WC#PESTICIDE_FLAG, MSI#WC#CALC_LEAD_TIME, MSI#WC#VOC_GL,
  MSI#WC#PESTICIDE_FLAG_STATE, MSI#WC#VOC_CATEGORY, MSI#WC#VOC_SUB_CATEGORY, MSI#WC#MSDS_#, MSI#WC#HAZMAT_PACKAGING_GROU, MGP#101#ACCOUNTALIAS, MGP#101#ACCOUNTALIAS_DESC, MIL#101#STOCKLOCATIONS, MIL#101#STOCKLOCATIONS_DESC)
AS
  SELECT mgp.segment1 source,
    mgp.DISTRIBUTION_ACCOUNT,
    gcc.segment1 Product,
    gcc.segment2 ,
    gcc.segment3,
    gcc.segment4,
    gcc.segment5,
    gcc.segment6,
    msi.concatenated_segments item,
    REPLACE(MSIL.DESCRIPTION,'~','-') ITEM_DESCRIPTION,
    haou.organization_name inv_org_name,
    haou.organization_code org_code,
    mcvc.concatenated_segments cat_class,
    mmt.subinventory_code subinventory,
    TRUNC(mmt.transaction_date) transaction_date,
    mmt.creation_date creation_date,
    xxeis.EIS_INV_UTILITY_PKG.get_fnd_user_name(mmt.created_by) created_by_user,
    oap.period_name gl_period,
    oap.period_start_date gl_period_start_date,
    mmt.actual_cost unit_cost,
    mmt.revision item_revision,
    mil.concatenated_segments stock_locators,
    pp.name project_name,
    pp.segment1 project_number,
    mmt.primary_quantity quantity,
    (mmt.primary_quantity*mmt.actual_cost) value,
    mtr.reason_name reason,
    mtr.reason_id,
    pt.task_name task_name,
    pt.task_number task_number,
    xxeis.EIS_INV_UTILITY_PKG.get_lookup_meaning('MTL_TRANSACTION_ACTION',MMT.TRANSACTION_ACTION_ID) Transaction_Action,
    mmt.transaction_reference transaction_reference,
    mts.transaction_source_type_name,
    mtt.transaction_type_name transaction_type,
    msi.primary_unit_of_measure unit_of_measure,
    DECODE(MMT.costed_flag,'N','No',NVL(MMT.COSTED_FLAG,'Yes')) Valued_Flag,
    mtt.transaction_type_id mtt_transaction_type_id,
    mil.inventory_location_id,
    pt.task_id,
    pp.project_id,
    oap.acct_period_id,
    mmt.transaction_id,
    mmt.transaction_source_type_id mmt_transaction_source_type_id,
    mmt.transaction_action_id,
    mmt.transaction_source_id,
    mmt.department_id,
    mmt.error_explanation,
    mmt.vendor_lot_number supplier_lot,
    mmt.source_line_id,
    mmt.parent_transaction_id,
    mmt.shipment_number shipment_number,
    mmt.waybill_airbill waybill_airbill,
    mmt.freight_code freight_code,
    mmt.number_of_containers,
    mmt.rcv_transaction_id,
    mmt.move_transaction_id,
    mmt.completion_transaction_id,
    mmt.operation_seq_num opertion_sequence,
    mmt.expenditure_type ,
    mmt.transaction_set_id ,
    mmt.transaction_source_name ,
    mmt.transaction_uom ,
    mmt.transfer_subinventory ,
    mmt.transfer_transaction_id,
    mp.organization_id mp_organization_id,
    mil.organization_id mil_organization_id,
    oap.organization_id oap_organization_id,
    msil.inventory_item_id msil_inventory_item_id,
    msil.organization_id msil_organization_id,
    msil.language,
    msi.inventory_item_id,
    msi.organization_id msi_organization_id,
    mgp.disposition_id,
    mgp.organization_id mgp_organization_id,
    haou.organization_id haou_organization_id,
    mts.transaction_source_type_id
    --descr#flexfield#start
    ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_FRU',MMT.ATTRIBUTE1,'I'), NULL) MMT#WCRentalA#Home_Branch ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',MMT.ATTRIBUTE10, NULL) MMT#WCRentalA#Vendor ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',MMT.ATTRIBUTE11, NULL) MMT#WCRentalA#PO_Number ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',MMT.ATTRIBUTE12, NULL) MMT#WCRentalA#Item_Number ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',MMT.ATTRIBUTE13, NULL) MMT#WCRentalA#CER_Number ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',MMT.ATTRIBUTE14, NULL) MMT#WCRentalA#Misc_Issue_Tra ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_FRU',MMT.ATTRIBUTE2,'I'), NULL) MMT#WCRentalA#Custodian_Bran ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_FA_MAJOR_CATEGORY',MMT.ATTRIBUTE3,'I'), NULL) MMT#WCRentalA#Major_Category ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',MMT.ATTRIBUTE4, NULL) MMT#WCRentalA#Minor_Category ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_FA_STATE',MMT.ATTRIBUTE5,'I'), NULL) MMT#WCRentalA#State ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_FA_CITY',MMT.ATTRIBUTE6,'I'), NULL) MMT#WCRentalA#City ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Rental Asset Transfer',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MMT.ATTRIBUTE9,'F'), NULL) MMT#WCRentalA#Transfer_to_Ma ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Return to Vendor',xxeis.eis_rs_dff.decode_valueset( 'XXWC_INV_SUPPLIER_NUMBER',MMT.ATTRIBUTE1,'F'), NULL) MMT#WCReturnt#Supplier_Numbe ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Return to Vendor',MMT.ATTRIBUTE2, NULL) MMT#WCReturnt#Return_Number ,
    DECODE(MMT.ATTRIBUTE_CATEGORY ,'WC Return to Vendor',MMT.ATTRIBUTE3, NULL) MMT#WCReturnt#Return_Unit_Pr ,
    MP.ATTRIBUTE1 MP#Factory_Planner_Data_Dire ,
    MP.ATTRIBUTE10 MP#FRU ,
    MP.ATTRIBUTE11 MP#Location_Number ,
    xxeis.eis_rs_dff.decode_valueset( 'HR_DE_EMPLOYEES',MP.ATTRIBUTE13,'F') MP#Branch_Operations_Manager ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_DELIVERY_CHARGE_EXEMPT',MP.ATTRIBUTE14,'I') MP#Deliver_Charge ,
    MP.ATTRIBUTE2 MP#Factory_Planner_Executabl ,
    MP.ATTRIBUTE3 MP#Factory_Planner_User ,
    MP.ATTRIBUTE4 MP#Factory_Planner_Host ,
    MP.ATTRIBUTE5 MP#Factory_Planner_Port_Numb ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_PRICE_ZONES',MP.ATTRIBUTE6,'F') MP#Pricing_Zone ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_ORG_TYPE',MP.ATTRIBUTE7,'I') MP#Org_Type ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_DISTRICT',MP.ATTRIBUTE8,'I') MP#District ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_REGION',MP.ATTRIBUTE9,'I') MP#Region ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE1, NULL) MSI#HDS#LOB ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE10,'F'), NULL) MSI#HDS#Drop_Shipment_Eligab ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE15, NULL) MSI#HDS#Invoice_UOM ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE2, NULL) MSI#HDS#Product_ID ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE3, NULL) MSI#HDS#Vendor_Part_Number ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE4, NULL) MSI#HDS#UNSPSC_Code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE5, NULL) MSI#HDS#UPC_Primary ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE6, NULL) MSI#HDS#SKU_Description ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE1, NULL) MSI#WC#CA_Prop_65 ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE10, NULL) MSI#WC#Country_of_Origin ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE11,'F'), NULL) MSI#WC#ORM_D_Flag ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE12, NULL) MSI#WC#Store_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE13, NULL) MSI#WC#DC_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE14, NULL) MSI#WC#Yearly_Store_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE15, NULL) MSI#WC#Yearly_DC_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE16, NULL) MSI#WC#PRISM_Part_Number ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE17, NULL) MSI#WC#Hazmat_Description ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC HAZMAT CONTAINER',MSI.ATTRIBUTE18,'I'), NULL) MSI#WC#Hazmat_Container ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE19, NULL) MSI#WC#GTP_Indicator ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE2, NULL) MSI#WC#Last_Lead_Time ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE20, NULL) MSI#WC#AMU ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE21, NULL) MSI#WC#Reserve_Stock ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC TAXWARE CODE',MSI.ATTRIBUTE22,'I'), NULL) MSI#WC#Taxware_Code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE25, NULL) MSI#WC#Average_Units ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE26, NULL) MSI#WC#Product_code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE27, NULL) MSI#WC#Import_Duty_ ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE29,'F'), NULL) MSI#WC#Keep_Item_Active ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE3,'F'), NULL) MSI#WC#Pesticide_Flag ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE30, NULL) MSI#WC#Calc_Lead_Time ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE4, NULL) MSI#WC#VOC_GL ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE5, NULL) MSI#WC#Pesticide_Flag_State ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE6, NULL) MSI#WC#VOC_Category ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE7, NULL) MSI#WC#VOC_Sub_Category ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE8, NULL) MSI#WC#MSDS_# ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC_HAZMAT_PACKAGE_GROUP',MSI.ATTRIBUTE9,'I'), NULL) MSI#WC#Hazmat_Packaging_Grou
    --descr#flexfield#end
    --kff#start
    ,
    MGP.SEGMENT1 "MGP#101#ACCOUNTALIAS" ,
    xxeis.eis_rs_dff.decode_valueset('XXWC_ACCOUNT_ALIAS',MGP.SEGMENT1,'N') "MGP#101#ACCOUNTALIAS_DESC" ,
    MIL.SEGMENT1 "MIL#101#STOCKLOCATIONS" ,
    xxeis.eis_rs_dff.decode_valueset('XXWC_STOCK_LOCATIONS',MIL.SEGMENT1,'N') "MIL#101#STOCKLOCATIONS_DESC"
    --kff#end
  FROM mtl_material_transactions mmt,
    mtl_system_items_kfv msi,
    mtl_system_items_tl msil,
    mtl_transaction_types mtt,
    mtl_item_locations_kfv mil,
    mtl_txn_source_types mts,
    pa_tasks pt,
    pa_projects_all pp,
    org_acct_periods oap,
    mtl_transaction_reasons mtr,
    mtl_generic_dispositions mgp,
    mtl_parameters mp,
    mtl_item_categories micc,
    mtl_categories_kfv mcvc,
    org_organization_definitions haou,
    gl_code_combinations gcc
  WHERE mmt.inventory_item_id        = msi.inventory_item_id
  AND mmt.organization_id            = msi.organization_id   --TMS#20160429-00031
  AND msi.inventory_item_id          = msil.inventory_item_id (+)
  AND msi.organization_id            = msil.organization_id (+)
  AND msil.language (+)              = userenv('LANG')
  AND mmt.transaction_type_id        = mtt.transaction_type_id(+)
  AND mmt.locator_id                 = mil.inventory_location_id(+)
  AND mmt.organization_id            = mil.organization_id (+)
  AND mmt.transaction_source_type_id = 6
  AND mmt.transaction_source_type_id = mts.transaction_source_type_id
  AND mmt.project_id                 = pp.project_id (+)
  AND mmt.task_id                    = pt.task_id (+)
  AND mmt.organization_id            = oap.organization_id (+)
  AND mmt.acct_period_id             = oap.acct_period_id (+)
  AND mmt.reason_id                  = mtr.reason_id (+)
  AND mmt.transaction_source_id      = mgp.disposition_id
  AND mmt.organization_id            = mgp.organization_id  --TMS#20160429-00031
    --  AND mgp.organization_id          = mp.organization_id  --TMS#20160429-00031
  AND mmt.organization_id = mp.organization_id  --TMS#20160429-00031
    --  AND msi.organization_id            = mp.organization_id  --TMS#20160429-00031
  AND msi.inventory_item_id = micc.inventory_item_id
  AND msi.organization_id   = micc.organization_id
  AND micc.category_set_id  = 1100000062
  AND micc.category_id      = mcvc.category_id
  AND mcvc.structure_id     = 101
  AND mmt.organization_id   = haou.organization_id  --TMS#20160429-00031
    --AND mmt.DISTRIBUTION_ACCOUNT_ID  = gcc.code_combination_id(+)
  AND mgp.distribution_account = gcc.code_combination_id(+)
  AND EXISTS
    (SELECT 1
    FROM xxeis.eis_org_access_v
    WHERE organization_id = mmt.organization_id
    )
/
