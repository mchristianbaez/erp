CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_inv_cycle_cnt_adj_v
(
   item
  ,item_description
  ,subinventory
  ,transaction_date
  ,item_cost
  ,quantity
  ,VALUE
  ,transaction_type
  ,inv_org_name
  ,inv_org_code
  ,transaction_id
  ,created_by_user
  ,uom
  ,cat_class
  ,org
  ,gl_account
  ,mmt#wcrentala#home_branch
  ,mmt#wcrentala#vendor
  ,mmt#wcrentala#po_number
  ,mmt#wcrentala#item_number
  ,mmt#wcrentala#cer_number
  ,mmt#wcrentala#misc_issue_tra
  ,mmt#wcrentala#custodian_bran
  ,mmt#wcrentala#major_category
  ,mmt#wcrentala#minor_category
  ,mmt#wcrentala#state
  ,mmt#wcrentala#city
  ,mmt#wcrentala#transfer_to_ma
  ,mmt#wcreturnt#supplier_numbe
  ,mmt#wcreturnt#return_number
  ,mmt#wcreturnt#return_unit_pr
  ,mp#factory_planner_data_dire
  ,mp#fru
  ,mp#location_number
  ,mp#branch_operations_manager
  ,mp#deliver_charge
  ,mp#factory_planner_executabl
  ,mp#factory_planner_user
  ,mp#factory_planner_host
  ,mp#factory_planner_port_numb
  ,mp#pricing_zone
  ,mp#org_type
  ,mp#district
  ,mp#region
  ,msi#hds#lob
  ,msi#hds#drop_shipment_eligab
  ,msi#hds#invoice_uom
  ,msi#hds#product_id
  ,msi#hds#vendor_part_number
  ,msi#hds#unspsc_code
  ,msi#hds#upc_primary
  ,msi#hds#sku_description
  ,msi#wc#ca_prop_65
  ,msi#wc#country_of_origin
  ,msi#wc#orm_d_flag
  ,msi#wc#store_velocity
  ,msi#wc#dc_velocity
  ,msi#wc#yearly_store_velocity
  ,msi#wc#yearly_dc_velocity
  ,msi#wc#prism_part_number
  ,msi#wc#hazmat_description
  ,msi#wc#hazmat_container
  ,msi#wc#gtp_indicator
  ,msi#wc#last_lead_time
  ,msi#wc#amu
  ,msi#wc#reserve_stock
  ,msi#wc#taxware_code
  ,msi#wc#average_units
  ,msi#wc#product_code
  ,msi#wc#import_duty_
  ,msi#wc#keep_item_active
  ,msi#wc#pesticide_flag
  ,msi#wc#calc_lead_time
  ,msi#wc#voc_gl
  ,msi#wc#pesticide_flag_state
  ,msi#wc#voc_category
  ,msi#wc#voc_sub_category
  ,msi#wc#msds_#
  ,msi#wc#hazmat_packaging_grou
)
AS
   SELECT msi.concatenated_segments item
         ,REPLACE (msi.description, '~', '-') item_description -- Added by Mahender For TMS#20150116-00084 on 05/18/15
         --,REPLACE (msil.description, '~', '-') item_description -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
         ,mmt.subinventory_code subinventory
         ,TRUNC (mmt.transaction_date) transaction_date
         ,                                                            --mac.description abc_class_description,
          --mac.abc_class_name abc_class_name,
          --mmt.creation_date creation_date,
          --mcch.cycle_count_header_name cycle_count_name,
          --oap.period_name gl_period,
          --oap.period_start_date gl_period_start_date,
          mmt.actual_cost item_cost
         ,                                                                       --mmt.revision item_revision,
          --mil.concatenated_segments stock_locators,
          --mmt.new_cost new_cost,
          --pp.name project_name,
          --pp.segment1 project_number,
          mmt.primary_quantity quantity
         ,ROUND ( (mmt.primary_quantity * mmt.actual_cost), fnd_profile.VALUE ('REPORT_QUANTITY_PRECISION'))
             VALUE
         ,                                                                                    --mtr.reason_id,
          --mtr.reason_name reason,
          --pt.task_name task_name,
          --pt.task_number task_number,
          --xxeis.EIS_INV_UTILITY_PKG.get_lookup_meaning('MTL_TRANSACTION_ACTION',MMT.TRANSACTION_ACTION_ID) Transaction_Action,
          --mmt.transaction_reference transaction_reference,
          --mts.transaction_source_type_name,
          mtt.transaction_type_name transaction_type
         ,                                                      --msi.primary_unit_of_measure unit_of_measure,
          --decode(mmt.costed_flag,'N','No',nvl(mmt.costed_flag,'Yes')) valued_flag,
          --mcch.cycle_count_header_name,
          --mtt.transaction_type_id,
          --mil.inventory_location_id,
          --mcci.cycle_count_header_id,
          --mac.abc_class_id,
          --pp.project_id,
          --pt.task_id,
          --oap.acct_period_id,
          --msi.inventory_item_id,
          --msi.organization_id,
          haou.organization_name inv_org_name
         ,haou.organization_code inv_org_code            --Added by Mahender for TMS#40314-00007 on 09/25/2014
         ,mmt.transaction_id
         ,xxeis.eis_inv_utility_pkg.get_fnd_user_name (mmt.created_by) created_by_user
         ,                                                                        --mmt.transaction_action_id,
                                                                                  --mmt.transaction_source_id,
          --mmt.department_id,
          --mmt.error_explanation,
          --mmt.vendor_lot_number supplier_lot,
          --mmt.source_line_id,
          --mmt.parent_transaction_id,
          --mmt.shipment_number shipment_number,
          --mmt.waybill_airbill waybill_airbill,
          --mmt.freight_code freight_code,
          --mmt.number_of_containers,
          --mmt.rcv_transaction_id,
          --mmt.move_transaction_id,
          --mmt.completion_transaction_id,
          --mmt.operation_seq_num opertion_sequence,
          --mmt.expenditure_type ,
          --mmt.transaction_set_id mmt_transaction_set_id ,
          --mmt.transaction_source_name ,
          mmt.transaction_uom uom
         ,                                                                       --mmt.transfer_subinventory ,
          --mmt.transfer_transaction_id mmt_transfer_transaction_id,
          --mil.organization_id mil_organization_id,
          --mcch.cycle_count_header_id mcch_cycle_count_header_id,
          --mcci.inventory_item_id mcci_inventory_item_id,
          --oap.organization_id oap_organization_id,
          --msil.inventory_item_id msil_inventory_item_id,
          --msil.organization_id msil_organization_id,
          --msil.language,
          --mp.organization_id mp_organization_id,
          --haou.organization_id haou_organization_id,
          --mts.transaction_source_type_id mts_transaction_source_type_id,
          xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class (msi.inventory_item_id, msi.organization_id)
             cat_class
         ,mp.organization_code org
         ,gcc.segment4 gl_account
         ,                                                                             --descr#flexfield#start
          DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', mmt.attribute1, 'I')
            ,NULL)
             mmt#wcrentala#home_branch
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute10, NULL)
             mmt#wcrentala#vendor
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute11, NULL)
             mmt#wcrentala#po_number
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute12, NULL)
             mmt#wcrentala#item_number
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute13, NULL)
             mmt#wcrentala#cer_number
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute14, NULL)
             mmt#wcrentala#misc_issue_tra
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', mmt.attribute2, 'I')
            ,NULL)
             mmt#wcrentala#custodian_bran
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FA_MAJOR_CATEGORY'
                                                                          ,mmt.attribute3
                                                                          ,'I')
            ,NULL)
             mmt#wcrentala#major_category
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute4, NULL)
             mmt#wcrentala#minor_category
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FA_STATE'
                                                                          ,mmt.attribute5
                                                                          ,'I')
            ,NULL)
             mmt#wcrentala#state
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FA_CITY'
                                                                          ,mmt.attribute6
                                                                          ,'I')
            ,NULL)
             mmt#wcrentala#city
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('Yes_No', mmt.attribute9, 'F')
            ,NULL)
             mmt#wcrentala#transfer_to_ma
         ,DECODE (
             mmt.attribute_category
            ,'WC Return to Vendor', xxeis.eis_rs_dff.decode_valueset ('XXWC_INV_SUPPLIER_NUMBER'
                                                                     ,mmt.attribute1
                                                                     ,'F')
            ,NULL)
             mmt#wcreturnt#supplier_numbe
         ,DECODE (mmt.attribute_category, 'WC Return to Vendor', mmt.attribute2, NULL)
             mmt#wcreturnt#return_number
         ,DECODE (mmt.attribute_category, 'WC Return to Vendor', mmt.attribute3, NULL)
             mmt#wcreturnt#return_unit_pr
         ,mp.attribute1 mp#factory_planner_data_dire
         ,mp.attribute10 mp#fru
         ,mp.attribute11 mp#location_number
         ,xxeis.eis_rs_dff.decode_valueset ('HR_DE_EMPLOYEES', mp.attribute13, 'F')
             mp#branch_operations_manager
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DELIVERY_CHARGE_EXEMPT', mp.attribute14, 'I')
             mp#deliver_charge
         ,mp.attribute2 mp#factory_planner_executabl
         ,mp.attribute3 mp#factory_planner_user
         ,mp.attribute4 mp#factory_planner_host
         ,mp.attribute5 mp#factory_planner_port_numb
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_PRICE_ZONES', mp.attribute6, 'F') mp#pricing_zone
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_ORG_TYPE', mp.attribute7, 'I') mp#org_type
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DISTRICT', mp.attribute8, 'I') mp#district
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_REGION', mp.attribute9, 'I') mp#region
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL) msi#hds#lob
         ,DECODE (msi.attribute_category
                 ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F')
                 ,NULL)
             msi#hds#drop_shipment_eligab
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL) msi#hds#invoice_uom
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL) msi#hds#product_id
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL) msi#hds#vendor_part_number
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL) msi#hds#unspsc_code
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL) msi#hds#upc_primary
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL) msi#hds#sku_description
         ,DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL) msi#wc#ca_prop_65
         ,DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL) msi#wc#country_of_origin
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute11, 'F')
                 ,NULL)
             msi#wc#orm_d_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL) msi#wc#store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL) msi#wc#dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL) msi#wc#yearly_store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL) msi#wc#yearly_dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL) msi#wc#prism_part_number
         ,DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL) msi#wc#hazmat_description
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msi.attribute18, 'I')
                 ,NULL)
             msi#wc#hazmat_container
         ,DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL) msi#wc#gtp_indicator
         ,DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL) msi#wc#last_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL) msi#wc#amu
         ,DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL) msi#wc#reserve_stock
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msi.attribute22, 'I')
                 ,NULL)
             msi#wc#taxware_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL) msi#wc#average_units
         ,DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL) msi#wc#product_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL) msi#wc#import_duty_
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute29, 'F')
                 ,NULL)
             msi#wc#keep_item_active
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute3, 'F')
                 ,NULL)
             msi#wc#pesticide_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL) msi#wc#calc_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL) msi#wc#voc_gl
         ,DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL) msi#wc#pesticide_flag_state
         ,DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL) msi#wc#voc_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL) msi#wc#voc_sub_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL) msi#wc#msds_#
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC_HAZMAT_PACKAGE_GROUP', msi.attribute9, 'I')
                 ,NULL)
             msi#wc#hazmat_packaging_grou
     --descr#flexfield#end
     --kff#start
     --,
     --MIL.SEGMENT1 "MIL#101#STOCKLOCATIONS" ,
     --xxeis.eis_rs_dff.decode_valueset('XXWC_STOCK_LOCATIONS',MIL.SEGMENT1,'N') "MIL#101#STOCKLOCATIONS_DESC"
     --kff#end
     FROM mtl_cycle_count_headers mcch
         ,mtl_transaction_types mtt
         --,                                                                       --mtl_item_locations_kfv mil,
         --mtl_cycle_count_items mcci,
         --mtl_abc_classes mac,
         --pa_tasks pt,
         --pa_projects pp,
         --org_acct_periods oap,
         -- mtl_system_items_tl msil  -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
         ,mtl_system_items_kfv msi
         ,mtl_material_transactions mmt
         ,mtl_parameters mp
         ,gl_code_combinations_kfv gcc
         ,                                                                      --mtl_transaction_reasons mtr,
          org_organization_definitions haou
    --mtl_txn_source_types mts
    WHERE --msi.organization_id = mp.organization_id -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
         mmt  .organization_id = mp.organization_id
          AND mmt.organization_id = msi.organization_id -- Added by Mahender For TMS#20150116-00084 on 05/18/15
          AND mmt.inventory_item_id = msi.inventory_item_id
          AND mmt.transaction_source_type_id = 9
          --AND mcci.cycle_count_header_id (+) = mmt.transaction_source_id
          --AND mcci.inventory_item_id (+)     = mmt.inventory_item_id
          --AND mac.abc_class_id (+)           = mcci.abc_class_id
          --AND oap.organization_id (+)        = mmt.organization_id
          --AND oap.acct_period_id (+)         = mmt.acct_period_id
          --AND msil.inventory_item_id(+) = msi.inventory_item_id   -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
          --AND msil.organization_id(+) = msi.organization_id   -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
          --AND msil.language(+) = USERENV ('LANG')             -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
          --and mmt.project_id                 = pp.project_id (+)
          --and mmt.task_id                    = pt.task_id (+)
          --AND mmt.locator_id                 = mil.inventory_location_id(+)
          --AND mil.organization_id (+)        = mmt.organization_id
          AND mmt.transaction_type_id = mtt.transaction_type_id(+)
          AND mmt.transaction_source_id = mcch.cycle_count_header_id(+)
          AND mcch.organization_id(+) = mmt.organization_id
          --AND mtr.reason_id (+)              = mmt.reason_id
          AND haou.organization_id = msi.organization_id
          AND mcch.inventory_adjustment_account = gcc.code_combination_id(+)
          --AND mts.transaction_source_type_id = mmt.transaction_source_type_id
          --          AND TRUNC(mmt.transaction_date) >= TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)  -- Added by Mahender For TMS#20150116-00084 on 05/18/15
          --          AND TRUNC(mmt.transaction_date) <= TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_to)  -- Added by Mahender For TMS#20150116-00084 on 05/18/15
          AND mmt.transaction_date >=
                   TO_DATE (
                      TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                              ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                     ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 0.25                                -- Added by Mahender For TMS#20150116-00084 on 05/18/15
          AND mmt.transaction_date <=
                   TO_DATE (
                      TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
                              ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                     ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 1.25                                -- Added by Mahender For TMS#20150116-00084 on 05/18/15
          AND EXISTS
                 (SELECT 1
                    FROM xxeis.eis_org_access_v
                   WHERE organization_id = msi.organization_id);
