CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_ar_aging_bad_debt_v
(
   bill_to_customer_name
  ,parent_customer_name
  ,bill_to_customer_number
  ,parent_customer_number
  ,location
  ,salesrep_name
  ,trx_type_name
  ,trx_num
  ,invoice_date
  ,receipt_number
  ,receipt_date
  ,due_date
  ,invoice_amount
  ,days_past_due
  ,amount_due_remaining
  ,customer_account_status
  ,profile_class
  ,collector
  ,credit_analyst_name
  ,credit_manager
  ,credit_hold_status
  ,payment_terms
  ,bucket_current
  ,applied_date
  ,bucket_1_to_30
  ,bucket_31_to_60
  ,bucket_61_to_90
  ,bucket_91_to_180
  ,bucket_181_to_360
  ,bucket_361_days_and_above
  ,acct_bal
  ,site_number
  ,site_name
  ,outstanding_amount
)
AS
   SELECT main.bill_to_customer_name
         ,main.parent_customer_name
         ,main.bill_to_customer_number
         ,main.parent_customer_number
         ,main.location
         ,main.salesrep_name
         ,main.trx_type_name
         ,main.trx_num
         ,main.invoice_date
         ,main.receipt_number
         ,main.receipt_date
         ,main.due_date
         ,main.invoice_amount
         ,main.days_past_due
         ,main.amount_due_remaining
         ,main.customer_account_status
         ,main.profile_class
         ,main.collector
         ,main.credit_analyst_name
         ,main.credit_manager
         ,main.credit_hold_status
         ,main.payment_terms
         ,main.bucket_current
         ,main.applied_date
         ,main.bucket_1_to_30
         ,main.bucket_31_to_60
         ,main.bucket_61_to_90
         ,main.bucket_91_to_180
         ,main.bucket_181_to_360
         ,main.bucket_361_days_and_above
         ,main.acct_bal
         ,main.site_number
         ,main.site_name
         ,main.outstanding_amount
     --(main.bucket_current+ main.bucket_1_to_30+ main.bucket_31_to_60+ main.bucket_61_to_90+ main.bucket_181_to_360+ main.bucket_361_days_and_above)total,
     FROM (SELECT gcc.concatenated_segments concatenated_segments
                 ,NVL (cust.account_name, party.party_name)
                     bill_to_customer_name
                 ,NVL (parent_cust.account_name, parent_party.party_name)
                     parent_customer_name
                 ,cust.account_number bill_to_customer_number
                 ,parent_cust.account_number parent_customer_number
                 ,gcc.segment2 location
                 ,jsr.name salesrep_name
                 ,ctt.name trx_type_name
                 ,ctt.cust_trx_type_id trx_type_id
                 ,TO_CHAR (NULL) receipt_method_name
                 ,TO_NUMBER (NULL) receipt_method_id
                 ,siteb.site_use_id contact_site_id
                 ,siteb.site_use_code bill_site_code
                 ,   locb.address1
                  || locb.address2
                  || locb.address3
                  || locb.address4
                     bill_to_address
                 ,locb.province bill_to_province
                 ,locb.county bill_to_county
                 ,locb.city bill_to_city
                 ,locb.state bill_to_state
                 ,locb.country bill_to_country
                 ,locb.postal_code bill_to_postal_code
                 ,locb.address_key bill_to_address_key
                 ,cust.cust_account_id bill_to_customer_id
                 ,sites.site_use_id ship_contact_site_id
                 ,sites.site_use_code ship_site_code
                 ,   locs.address1
                  || locs.address2
                  || locs.address3
                  || locs.address4
                     ship_to_address
                 ,locs.province ship_to_province
                 ,locs.county ship_to_county
                 ,locs.city ship_to_city
                 ,locs.state ship_to_state
                 ,locs.country ship_to_country
                 ,locs.postal_code ship_to_postal_code
                 ,locs.address_key ship_to_address_key
                 ,ps.payment_schedule_id payment_sched_id
                 ,ps.class trx_class
                 ,ps.due_date due_date
                 ,ps.acctd_amount_due_remaining acct_amonut_due_remaining
                 ,ps.amount_due_remaining amount_due_remaining
                 ,ps.trx_number trx_num
                 ,trx.trx_date invoice_date
                 , (SELECT SUM (extended_amount)
                      FROM ra_customer_trx_lines rctl
                     WHERE rctl.customer_trx_id = trx.customer_trx_id)
                     invoice_amount
                 ,REPLACE (trx.purchase_order, '~', ' ') po_number
                 ,trx.interface_header_attribute1 sale_order_number
                 ,TO_CHAR (NULL) receipt_number
                 ,NULL receipt_date
                 ,CEIL (
                       xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
                     - ps.due_date)
                     days_past_due
                 ,ps.amount_adjusted amount_adjusted
                 ,ps.amount_applied amount_applied
                 ,ps.amount_credited amount_credited
                 ,ps.gl_date gl_date
                 ,DECODE (ps.invoice_currency_code
                         ,gle.currency_code, NULL
                         ,DECODE (ps.exchange_rate, NULL, '*', NULL))
                     data_converted
                 ,NVL (ps.exchange_rate, 1) ps_exchange_rate
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,NULL
                    ,NULL
                    , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     outstanding_amount
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,NULL
                    ,0
                    , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_current
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,1
                    ,30
                    , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_1_to_30
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,31
                    ,60
                    , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_31_to_60
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,61
                    ,90
                    , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_61_to_90
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,91
                    ,180
                    , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_91_to_180
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,181
                    ,360
                    , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_181_to_360
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,361
                    ,9999999
                    , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_361_days_and_above
                 , (  xxeis.eis_rs_xxwc_com_util_pkg.get_customer_total (
                         cust.cust_account_id)
                    + xxeis.eis_rs_xxwc_com_util_pkg.get_customer_receipt_total (
                         cust.cust_account_id))
                     acct_bal
                 ,hou.name operating_unit
                 ,hzpc.name profile_class
                 ,customer_status.meaning customer_account_status
                 ,ac.name collector
                 ,jrdv.resource_name credit_analyst_name
                 ,jrdv.source_mgr_name credit_manager
                 ,hzp.credit_hold credit_hold_status
                 ,rt.name payment_terms
                 , (SELECT MAX (NVL (ra.apply_date, ra.gl_date))
                      FROM ar_receivable_applications ra
                          ,ar_payment_schedules aps
                     WHERE     application_type = 'CASH'
                           AND ra.status = 'APP'
                           AND aps.payment_schedule_id =
                                  ra.payment_schedule_id
                           AND customer_id = ps.customer_id)
                     applied_date
                 ,party_siteb.party_site_number site_number
                 ,                    --Party_Siteb.PARTY_SITE_NAME site_name,
           --SUBSTR(Siteb.LOCATION,1,INSTR(Siteb.LOCATION,'-',1)-1) site_name,
                  siteb.location site_name
                 ,                                              --Primary Keys
                  ctt.cust_trx_type_id
                 ,ctt.org_id
                 ,cust.cust_account_id
                 ,party.party_id
                 ,gcc.code_combination_id
                 ,siteb.site_use_id
                 ,locb.location_id
                 ,cust_siteb.cust_acct_site_id
                 ,party_sites.party_site_id
                 ,hou.organization_id
                 ,ps.payment_schedule_id
             --descr#flexfield#start
             --descr#flexfield#end
             --gl#accountff#start
             --gl#accountff#end
             FROM ra_cust_trx_types ctt
                 ,ra_customer_trx trx
                 ,hz_cust_accounts cust
                 ,hz_parties party
                 ,ar_payment_schedules ps
                 ,ra_cust_trx_line_gl_dist gld
                 ,gl_code_combinations_kfv gcc
                 ,gl_ledgers gle
                 ,fnd_user fu1
                 ,fnd_user fu2
                 ,fnd_territories_vl ter
                 ,hz_cust_site_uses siteb
                 ,hz_locations locb
                 ,hz_cust_acct_sites cust_siteb
                 ,hz_party_sites party_siteb
                 ,hz_cust_site_uses sites
                 ,hz_locations locs
                 ,hz_cust_acct_sites cust_sites
                 ,hz_party_sites party_sites
                 ,hr_organization_units hou
                 ,hz_customer_profiles hzp
                 ,hz_cust_profile_classes hzpc
                 ,ar_collectors ac
                 ,jtf_rs_defresources_v jrdv
                 ,hz_cust_acct_relate cust_rel
                 ,hz_cust_accounts parent_cust
                 ,hz_parties parent_party
                 ,ar_lookups customer_status
                 ,ra_salesreps jsr
                 ,ra_terms rt
            WHERE     TRUNC (ps.gl_date) <=
                         xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
                  AND trx.cust_trx_type_id = ctt.cust_trx_type_id
                  AND trx.bill_to_customer_id = cust.cust_account_id
                  AND cust.party_id = party.party_id
                  AND ps.customer_trx_id = trx.customer_trx_id
                  AND trx.customer_trx_id = gld.customer_trx_id
                  AND gld.account_class = 'REC'
                  AND gld.latest_rec_flag = 'Y'
                  AND gld.code_combination_id = gcc.code_combination_id
                  AND gle.ledger_id = trx.set_of_books_id
                  AND fu1.user_id = trx.last_updated_by
                  AND fu2.user_id = trx.created_by
                  AND trx.bill_to_site_use_id = siteb.site_use_id
                  AND cust_siteb.cust_acct_site_id = siteb.cust_acct_site_id
                  AND cust_siteb.party_site_id = party_siteb.party_site_id
                  AND party_siteb.location_id = locb.location_id
                  AND trx.ship_to_site_use_id = sites.site_use_id(+)
                  AND cust_sites.cust_acct_site_id(+) =
                         sites.cust_acct_site_id
                  AND cust_sites.party_site_id = party_sites.party_site_id(+)
                  AND party_sites.location_id = locs.location_id(+)
                  AND locb.country = ter.territory_code
                  AND ctt.org_id = hou.organization_id
                  AND hzp.cust_account_id = cust.cust_account_id
                  AND hzp.site_use_id = siteb.site_use_id
                  AND hzp.profile_class_id = hzpc.profile_class_id
                  AND hzp.collector_id = ac.collector_id(+)
                  AND hzp.credit_analyst_id = jrdv.resource_id(+)
                  AND ps.term_id = rt.term_id(+)
                  AND cust_rel.related_cust_account_id(+) =
                         cust.cust_account_id
                  AND cust_rel.cust_account_id =
                         parent_cust.cust_account_id(+)
                  AND parent_cust.party_id = parent_party.party_id(+)
                  AND NVL (customer_status.lookup_type, 'ACCOUNT_STATUS') =
                         'ACCOUNT_STATUS'
                  AND customer_status.lookup_code(+) = hzp.account_status
                  AND NVL (cust_rel.status, 'A') = 'A'
                  AND jsr.salesrep_id(+) = trx.primary_salesrep_id
                  AND jsr.org_id(+) = trx.org_id
           --and Trx.trx_number='232047669-00'
           --   AND PARTY.PARTY_ID=200813
           UNION
           SELECT gcc.concatenated_segments concatenated_segments
                 ,NVL (cust.account_name, party.party_name) customer_name
                 ,NVL (parent_cust.account_name, parent_party.party_name)
                     parent_customer_name
                 ,cust.account_number customer_number
                 ,parent_cust.account_number parent_customer_number
                 ,gcc.segment2 location
                 ,NULL salesrep_name
                 ,TO_CHAR (NULL) trx_type_name
                 ,TO_NUMBER (NULL) trx_type_id
                 ,arm.name receipt_method_name
                 ,arm.receipt_method_id receipt_method_id
                 ,site.site_use_id contact_site_id
                 ,site.site_use_code site_code
                 ,   loc.address1
                  || loc.address2
                  || loc.address3
                  || loc.address4
                     address
                 ,loc.province province
                 ,loc.county county
                 ,loc.city city
                 ,loc.state state
                 ,loc.country country
                 ,loc.postal_code
                 ,loc.address_key
                 ,cust.cust_account_id customer_id
                 ,TO_NUMBER (NULL) ship_contact_site_id
                 ,TO_CHAR (NULL) ship_site_code
                 ,TO_CHAR (NULL) ship_to_address
                 ,TO_CHAR (NULL) ship_to_province
                 ,TO_CHAR (NULL) ship_to_county
                 ,TO_CHAR (NULL) ship_to_city
                 ,TO_CHAR (NULL) ship_to_state
                 ,TO_CHAR (NULL) ship_to_country
                 ,TO_CHAR (NULL) ship_to_postal_code
                 ,TO_CHAR (NULL) ship_to_address_key
                 ,ps.payment_schedule_id payment_sched_id
                 ,ps.class trx_class
                 ,ps.due_date due_date
                 ,ps.acctd_amount_due_remaining
                 ,ps.amount_due_remaining
                 ,TO_CHAR (NULL) trx_num
                 ,NULL invoice_date
                 ,0 invoice_amount
                 ,TO_CHAR (NULL) po_number
                 ,TO_CHAR (NULL) sale_order_number
                 ,ps.trx_number receipt_number
                 ,cr.receipt_date receipt_date
                 ,CEIL (
                       xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
                     - ps.due_date)
                     days_past_due
                 ,ps.amount_adjusted amount_adjusted
                 ,ps.amount_applied amount_applied
                 ,ps.amount_credited amount_credited
                 ,ps.gl_date gl_date
                 ,DECODE (ps.invoice_currency_code
                         ,gle.currency_code, NULL
                         ,DECODE (ps.exchange_rate, NULL, '*', NULL))
                     data_converted
                 ,NVL (ps.exchange_rate, 1) ps_exchange_rate
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,NULL
                    ,NULL
                    ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     outstanding_amount
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,NULL
                    ,0
                    ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_current
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,1
                    ,30
                    ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_1_to_30
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,31
                    ,60
                    ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_31_to_60
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,61
                    ,90
                    ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_61_to_90
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,91
                    ,180
                    ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_91_to_180
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,181
                    ,360
                    ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_181_to_360
                 ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id
                    ,ps.due_date
                    ,361
                    ,9999999
                    ,ps.amount_due_original * NVL (ps.exchange_rate, 1)
                    ,ps.amount_applied
                    ,ps.amount_credited
                    ,ps.amount_adjusted
                    ,ps.class)
                     bucket_361_days_and_above
                 , (  xxeis.eis_rs_xxwc_com_util_pkg.get_customer_total (
                         cust.cust_account_id)
                    + xxeis.eis_rs_xxwc_com_util_pkg.get_customer_receipt_total (
                         cust.cust_account_id))
                     acct_bal
                 ,hou.name operating_unit
                 ,hzpc.name profile_class
                 ,customer_status.meaning customer_account_status
                 ,ac.name collector
                 ,jrdv.resource_name credit_analyst_name
                 ,jrdv.source_mgr_name credit_manager
                 ,hzp.credit_hold credit_hold_status
                 ,rt.name payment_terms
                 , (SELECT MAX (NVL (ra.apply_date, ra.gl_date))
                      FROM ar_receivable_applications ra
                          ,ar_payment_schedules aps
                     WHERE     application_type = 'CASH'
                           AND ra.status = 'APP'
                           AND aps.payment_schedule_id =
                                  ra.payment_schedule_id
                           AND customer_id = ps.customer_id)
                     applied_date
                 ,party_sites.party_site_number site_number
                 ,                  --  party_sites.PARTY_SITE_NAME site_name,
             --SUBSTR(Site.LOCATION,1,INSTR(Site.LOCATION,'-',1)-1) site_name,
                  site.location site_name
                 ,                                              --Primary Keys
                  NULL cust_trx_type_id
                 ,NULL org_id
                 ,cust.cust_account_id
                 ,party.party_id
                 ,gcc.code_combination_id
                 ,site.site_use_id
                 ,loc.location_id
                 ,cust_sites.cust_acct_site_id
                 ,party_sites.party_site_id
                 ,hou.organization_id
                 ,ps.payment_schedule_id
             --descr#flexfield#start
             --descr#flexfield#end
             --gl#accountff#start
             --gl#accountff#end
             FROM ar_payment_schedules ps
                 ,ar_cash_receipts cr
                 ,ar_receipt_methods arm
                 ,ar_receipt_method_accounts arma
                 ,gl_code_combinations_kfv gcc
                 ,gl_ledgers gle
                 ,hz_cust_accounts cust
                 ,hz_parties party
                 ,hz_cust_site_uses site
                 ,hz_cust_acct_sites cust_sites
                 ,hz_party_sites party_sites
                 ,hz_locations loc
                 ,hr_operating_units hou
                 ,hz_customer_profiles hzp
                 ,hz_cust_profile_classes hzpc
                 ,ar_collectors ac
                 ,jtf_rs_defresources_v jrdv
                 ,hz_cust_acct_relate cust_rel
                 ,hz_cust_accounts parent_cust
                 ,hz_parties parent_party
                 ,ar_lookups customer_status
                 ,ra_terms rt
            WHERE     TRUNC (ps.gl_date) <=
                         xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
                  AND ps.cash_receipt_id = cr.cash_receipt_id
                  AND arm.receipt_method_id = cr.receipt_method_id
                  AND arm.receipt_method_id = arma.receipt_method_id
                  AND arma.receipt_method_id = cr.receipt_method_id
                  AND arma.unapplied_ccid = gcc.code_combination_id
                  AND gle.ledger_id = cr.set_of_books_id
                  AND ps.customer_id = cust.cust_account_id(+)
                  AND cust.party_id = party.party_id(+)
                  AND ps.customer_site_use_id = site.site_use_id(+)
                  AND site.cust_acct_site_id =
                         cust_sites.cust_acct_site_id(+)
                  AND cust_sites.party_site_id = party_sites.party_site_id(+)
                  AND party_sites.location_id = loc.location_id(+)
                  AND ps.org_id = hou.organization_id
                  AND hzp.cust_account_id = cust.cust_account_id
                  AND hzp.site_use_id = site.site_use_id
                  AND hzp.profile_class_id = hzpc.profile_class_id
                  AND hzp.collector_id = ac.collector_id(+)
                  AND hzp.credit_analyst_id = jrdv.resource_id(+)
                  AND ps.term_id = rt.term_id(+)
                  AND cust_rel.related_cust_account_id(+) =
                         cust.cust_account_id
                  AND cust_rel.cust_account_id =
                         parent_cust.cust_account_id(+)
                  AND parent_cust.party_id = parent_party.party_id(+)
                  AND NVL (customer_status.lookup_type, 'ACCOUNT_STATUS') =
                         'ACCOUNT_STATUS'
                  AND customer_status.lookup_code(+) = hzp.account_status
                  AND NVL (cust_rel.status, 'A') = 'A'
                  --  AND PARTY.PARTY_ID=200813
                  AND NOT EXISTS
                             (SELECT 1
                                FROM ar_cash_receipt_history crh1
                               WHERE     crh1.cash_receipt_id =
                                            cr.cash_receipt_id
                                     AND TRUNC (crh1.gl_date) <=
                                            xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
                                     AND status = 'REVERSED')) main;


