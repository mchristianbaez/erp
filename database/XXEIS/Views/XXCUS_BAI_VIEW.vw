CREATE OR REPLACE FORCE VIEW xxeis.xxcus_bai_view
(
   bank_account_num
  ,line_number
  ,trx_date
  ,trx_type
  ,trx_code
  ,description
  ,amount
  ,trx_text
  ,bank_trx_number
  ,customer_text
  ,invoice_text
  ,bank_account_text
  ,reference_txt
  ,je_status_flag
  ,accounting_date
  ,segment1
  ,segment2
  ,segment3
  ,segment4
  ,segment5
  ,segment6
)
AS
   SELECT                                                    /*ba.bank_code,*/
         ba.bank_account_num
         ,li.line_number
         ,li.trx_date
         ,li.trx_type
         ,tr.trx_code
         ,tr.description
         ,li.amount
         ,li.trx_text
         ,li.bank_trx_number
         ,li.customer_text
         ,li.invoice_text
         ,li.bank_account_text
         ,li.reference_txt
         ,li.je_status_flag
         ,li.accounting_date
         ,g.segment1
         ,g.segment2
         ,g.segment3
         ,g.segment4
         ,g.segment5
         ,g.segment6
     FROM ce.ce_statement_headers hd
         ,ce.ce_bank_accounts ba
         ,ce.ce_transaction_codes tr
         ,ce.ce_statement_lines li LEFT OUTER JOIN gl.gl_code_combinations g ON li.gl_account_ccid =
                                                                                   g.code_combination_id;


