CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_VENDOR_QUOTE_V
(
   name
  ,salesperson_name
  ,salespeson_number
  ,master_account_name
  ,master_account_number
  ,job_name
  ,organization_name
  ,source_type_code
  ,job_number
  ,vendor_number
  ,vendor_name
  ,oracle_quote_number
  ,invoice_number
  ,invoice_date
  ,part_number
  ,uom
  ,description
  ,bpa
  ,po_cost
  ,average_cost
  ,special_cost
  ,unit_claim_value
  ,rebate
  ,gl_coding
  ,gl_string
  ,loc
  ,created_by
  ,customer_trx_id
  ,order_number
  ,line_number
  ,creation_date
  ,location
  ,qty
  ,process_id
  ,common_output_id
  ,list_price_per_unit
  ,po_trans_cost
)
 /* *************************************************************************************************************
  $Header XXEIS.eis_xxwc_om_vendor_quote_v $
  Module Name: Order Managment
  PURPOSE: This view is for EIS Vendor Quote Batch Summary Rpt
  TMS Task Id : 
  REVISIONS:
  Ver        Date        	Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        NA		  		NA					Initial Version 
  1.1    08/10/2016			Siva   				TMS#20160804-00030
  1.2	 30-Oct-2017		Siva				TMS#20170920-00108  
  ************************************************************************************************************* */
AS
   SELECT NAME
         ,SALESPERSON_NAME
         ,SALESPESON_NUMBER
         ,MASTER_ACCOUNT_NAME
         ,MASTER_ACCOUNT_NUMBER
         ,JOB_NAME
         ,ORGANIZATION_NAME
         ,SOURCE_TYPE_CODE
         ,JOB_NUMBER
         ,VENDOR_NUMBER
         ,VENDOR_NAME
         ,ORACLE_QUOTE_NUMBER
         ,INVOICE_NUMBER
         ,INVOICE_DATE
         ,PART_NUMBER
         ,UOM
         ,DESCRIPTION
         ,BPA
         ,PO_COST
         ,AVERAGE_COST
         ,SPECIAL_COST
         ,UNIT_CLAIM_VALUE
         ,REBATE
         ,GL_CODING
         ,GL_STRING
         ,LOC
         ,CREATED_BY
         ,CUSTOMER_TRX_ID
         ,ORDER_NUMBER
         ,LINE_NUMBER
         ,CREATION_DATE
         ,LOCATION
         ,QTY
         ,PROCESS_ID
         ,COMMON_OUTPUT_ID
		 ,list_price_per_unit --Added for VER 1.1
		 ,po_trans_cost  --Added for Ver 1.2
     FROM xxeis.xxwc_vendor_quote_temp1
/