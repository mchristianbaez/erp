------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
   PURPOSE    :  Replaced the old view definition with new view.This view is created based on 
				 custom table XXEIS.EIS_XXWC_DAILY_FLASH_DTL_TAB and this custom table data
				 gets populated by XXEIS.EIS_XXWC_DAILY_FLASH_DTLS_PKG package.
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0		 20-APR-2016   PRAMOD  			TMS#20160429-00034  Performance Tuning                                         
******************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_FLSH_SALE_DL_NEW_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_FLSH_SALE_DL_NEW_V (CURRENT_FISCAL_MONTH, CURRENT_FISCAL_YEAR, DELIVERY_TYPE, BRANCH_LOCATION, INVOICE_COUNT, SALES, PROCESS_ID)
AS
  SELECT /*+ INDEX(EXDFDT EIS_XXWC_DAILY_FLASH_DTL_N1)*/ CURRENT_FISCAL_MONTH,    --TMS#20160429-00034    by PRAMOD  on 04-26-2016
    CURRENT_FISCAL_YEAR,
    DELIVERY_TYPE,
    BRANCH_LOCATION,
    COUNT(DISTINCT INVOICE_COUNT),
    SUM(SALES),
    PROCESS_ID
  FROM XXEIS.EIS_XXWC_DAILY_FLASH_DTL_TAB EXDFDT
  GROUP BY CURRENT_FISCAL_MONTH,
    CURRENT_FISCAL_YEAR,
    DELIVERY_TYPE,
    BRANCH_LOCATION,
    PROCESS_ID
/
