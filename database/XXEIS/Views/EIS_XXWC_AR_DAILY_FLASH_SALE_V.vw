CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_ar_daily_flash_sale_v
(
   date1
  ,branch_location
  ,sale_amt
  ,sale_cost
  ,payment_type
  ,invoice_count
)
AS
     SELECT TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from) date1
           ,ood.organization_code branch_location
           ,SUM (
                 NVL (rctl.quantity_invoiced, rctl.quantity_credited)
               * NVL (ol.unit_selling_price, 0))
               sale_amt
           ,SUM (
                 NVL (rctl.quantity_invoiced, rctl.quantity_credited)
               * NVL (
                    apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
                   ,0))
               sale_cost
           , --NVL(SUM(DECODE(otl.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) * NVL(Ol.Unit_Selling_Price,0)),0) SALE_AMT,
 --SUM (DECODE(otl.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)   *-1),NVL(Ol.ordered_quantity,0)) *NVL(ol.unit_cost,0))sale_cost,
                                        -- SUM(oep.payment_amount) cash_sales,
            xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type payment_type
           ,COUNT (
               DISTINCT xxeis.eis_rs_xxwc_com_util_pkg.get_dist_invoice_num (
                           rct.customer_trx_id
                          ,rct.trx_number
                          ,otlh.name))
               invoice_count
       ---Primary Keys
       FROM ra_customer_trx rct
           ,ra_customer_trx_lines rctl
           ,oe_order_headers oh
           ,oe_order_lines ol
           ,org_organization_definitions ood
           ,oe_transaction_types_vl otl
           ,oe_transaction_types_vl otlh
      WHERE     1 = 1
            --AND trunc(Rct.creation_date) = trunc(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
            AND rct.creation_date >=
                     TO_DATE (
                        TO_CHAR (
                           xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                          ,   xxeis.eis_rs_utility.get_date_format
                           || ' HH24:MI:SS')
                       ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                   + 0.25
            AND rct.creation_date <=
                     TO_DATE (
                        TO_CHAR (
                           xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                          ,   xxeis.eis_rs_utility.get_date_format
                           || ' HH24:MI:SS')
                       ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                   + 1.25
            --and TO_CHAR(RCT.CREATION_DATE,'DD-MON-YY HH:MM:SS')    >= TO_CHAR(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM ,'DD-MON-YY HH:MM:SS') +0.25
            -- AND TO_CHAR(Rct.creation_date ,'DD-MON-YY HH:MM:SS')   <= to_char(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from ,'DD-MON-YY HH:MM:SS') +1.25
            AND TO_CHAR (oh.order_number) =
                   TO_CHAR (rct.interface_header_attribute1)
            AND ood.organization_id(+) = ol.ship_from_org_id
            AND TO_CHAR (rctl.interface_line_attribute6) = TO_CHAR (ol.line_id)
            AND rctl.inventory_item_id = ol.inventory_item_id
            AND otl.transaction_type_id = ol.line_type_id
            AND otlh.transaction_type_id = oh.order_type_id
            AND rctl.customer_trx_id = rct.customer_trx_id
            AND ol.header_id = oh.header_id
            AND rctl.interface_line_context = 'ORDER ENTRY'
            AND rct.interface_header_context = 'ORDER ENTRY'
            AND NOT EXISTS
                       (SELECT 1
                          FROM hz_customer_profiles hcp
                              ,hz_cust_profile_classes hcpc
                              ,hz_cust_accounts hca
                         WHERE     hca.party_id = hcp.party_id
                               AND oh.sold_to_org_id = hcp.cust_account_id
                               AND hcp.site_use_id IS NULL
                               AND hcp.profile_class_id =
                                      hcpc.profile_class_id(+)
                               AND hcpc.name LIKE 'WC%Branches%')
            /* AND NOT EXISTS
            (SELECT 1
            FROM OE_PRICE_ADJUSTMENTS_V
            WHERE HEADER_ID     = OL.HEADER_ID
            AND LINE_ID         = OL.LINE_ID
            AND adjustment_name ='BRANCH_COST_MODIFIER'
            )*/
            AND (   (    xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type =
                            'CASH'
                     AND (   EXISTS
                                (SELECT 1
                                   FROM oe_payments oep
                                  WHERE ( (    oep.payment_type_code IN
                                                  ('CASH'
                                                  ,'CHECK'
                                                  ,'CREDIT_CARD')
                                           AND oep.header_id = ol.header_id)))
                          OR (    otl.order_category_code = 'RETURN'
                              AND EXISTS
                                     (SELECT 1
                                        FROM xxwc_om_cash_refund_tbl xoc
                                       WHERE xoc.return_header_id =
                                                oh.header_id))))
                 OR (    xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type =
                            'CHARGE'
                     AND (   (    NOT EXISTS
                                     (SELECT 1
                                        FROM oe_payments oep
                                       WHERE oep.header_id = ol.header_id)
                              AND otl.order_category_code <> 'RETURN')
                          OR (    otl.order_category_code = 'RETURN'
                              AND NOT EXISTS
                                         (SELECT 1
                                            FROM xxwc_om_cash_refund_tbl xoc1
                                           WHERE xoc1.return_header_id =
                                                    oh.header_id)))))
   GROUP BY TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
           ,ood.organization_code
           ,xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type;


