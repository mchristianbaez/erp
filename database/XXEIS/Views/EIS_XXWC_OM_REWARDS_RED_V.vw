CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_rewards_red_v
(
   customer_number
  ,customer_name
  ,sales_amount
  ,tax_value
  ,branch
  ,rebate_voucher_number
  ,rebate_voucher_amount
  ,date_redeemed
  ,order_number
  ,invoice_number
  ,salesrep_name
  ,salesrep_number
  ,bill_to_state
)
AS
     SELECT cust_acct.account_number customer_number
           ,NVL (cust_acct.account_name, party.party_name) customer_name
           ,SUM (
                 NVL (ol.unit_selling_price, 0)
               * DECODE (ol.line_category_code
                        ,'RETURN', (ol.ordered_quantity * -1)
                        ,ol.ordered_quantity))
               sales_amount
           ,SUM (
               NVL (
                  DECODE (ol.line_category_code
                         ,'RETURN', (ol.tax_value * -1)
                         ,ol.tax_value)
                 ,0))
               tax_value
           ,ship_from_org.organization_code branch
           ,MAX (oe.check_number) rebate_voucher_number
           ,SUM (oe.payment_amount) rebate_voucher_amount
           ,MAX (TRUNC (oe.creation_date)) date_redeemed
           ,oh.order_number
           ,rct.trx_number invoice_number
           ,rep.name salesrep_name
           ,rep.salesrep_number
           ,UPPER (hl.state) bill_to_state
       ---Primary Keys
       --descr#flexfield#start
       --descr#flexfield#end
       --gl#accountff#start
       --gl#accountff#end
       FROM mtl_parameters ship_from_org
           ,hz_parties party
           ,hz_parties partyb
           ,hz_cust_accounts cust_acct
           ,oe_order_headers oh
           ,oe_order_lines ol
           ,oe_payments oe
           ,ra_customer_trx rct
           ,hz_cust_site_uses hcsu
           ,hz_cust_acct_sites hcas
           ,hz_party_sites hps
           ,hz_locations hl
           ,ra_salesreps rep
           ,ar_receipt_methods arc
      WHERE     ol.sold_to_org_id = cust_acct.cust_account_id
            AND cust_acct.party_id = party.party_id
            AND oe.header_id = oh.header_id
            AND rct.bill_to_site_use_id = hcsu.site_use_id
            AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
            AND hcas.party_site_id = hps.party_site_id
            AND hps.location_id = hl.location_id
            AND partyb.party_id = hps.party_id
            AND oe.receipt_method_id = arc.receipt_method_id
            AND rct.interface_header_context = 'ORDER ENTRY'
            AND TO_CHAR (oh.order_number) =
                   TO_CHAR (rct.interface_header_attribute1)
            AND oh.salesrep_id = rep.salesrep_id(+)
            AND oh.org_id = rep.org_id(+)
            AND oh.ship_from_org_id = ship_from_org.organization_id(+)
            AND ol.header_id = oh.header_id
            AND UPPER (arc.name) LIKE UPPER ('%Rewards%Rebate%')
   /*AND (upper(arc.name) LIKE '%GIFT%'
   OR upper(arc.name) LIKE '%REBATE%')*/
   GROUP BY cust_acct.account_number
           ,NVL (cust_acct.account_name, party.party_name)
           ,ship_from_org.organization_code
           ,oe.check_number
           ,oh.order_number
           ,rct.trx_number
           ,rep.name
           ,rep.salesrep_number
           ,hl.state;


