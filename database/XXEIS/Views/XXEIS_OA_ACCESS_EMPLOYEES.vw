CREATE OR REPLACE FORCE VIEW xxeis.xxeis_oa_access_employees
(
   line_of_business
  ,line_of_business_name
  ,user_name
  ,employee_name
  ,employee_number
  ,email_address
  ,user_active_date
  ,user_inactive_date
  ,supervisor_name
  ,supervisor_employee_number
  ,responsibility_name
  ,responsibility_active_date
  ,responsibility_inactive_date
)
AS
   SELECT lob_info.entrp_entity line_of_business
         ,lob_name.description line_of_business_name
         ,u.user_name user_name
         ,NVL (papf.full_name, 'Seeded User') employee_name
         ,NVL (papf.employee_number, 'N/A') employee_number
         ,NVL (papf.email_address, u.email_address) email_address
         ,u.start_date user_active_date
         ,u.end_date user_inactive_date
         ,papf2.full_name supervisor_name
         ,papf2.employee_number supervisor_employee_number
         ,fr.responsibility_name responsibility_name
         ,urg.start_date responsibility_active_date
         ,urg.end_date responsibility_inactive_date
     FROM applsys.fnd_user u
         ,hr.per_all_people_f papf
         ,hr.per_all_assignments_f paaf
         ,hr.per_all_people_f papf2
         ,apps.fnd_user_resp_groups_all urg
         ,applsys.fnd_responsibility_tl fr
         ,applsys.fnd_responsibility resp
         ,apps.fnd_user_resp_groups_direct frgd
         ,(SELECT DISTINCT
                  lc.fru, REPLACE (lc.entrp_entity, 'P', '') entrp_entity
             FROM xxcus.xxcus_location_code_tbl lc) lob_info
         ,(SELECT fv.flex_value, fvt.description
             FROM applsys.fnd_flex_values fv
                 ,applsys.fnd_flex_values_tl fvt
                 ,applsys.fnd_flex_value_sets fvs
            WHERE     fv.flex_value_id = fvt.flex_value_id
                  AND fv.flex_value_set_id = fvs.flex_value_set_id
                  AND fvs.flex_value_set_name = 'XXCUS_GL_PRODUCT') lob_name
    WHERE     urg.user_id = u.user_id
          AND urg.responsibility_id = fr.responsibility_id
          AND fr.responsibility_id = resp.responsibility_id
          AND resp.responsibility_id = frgd.responsibility_id
          AND frgd.user_id = urg.user_id
          AND u.employee_id IS NOT NULL
          AND urg.end_date IS NULL
          AND u.employee_id = papf.person_id(+)
          AND NVL (papf.attribute1, '9999') = lob_info.fru
          AND lob_info.entrp_entity = lob_name.flex_value
          AND papf.person_id = paaf.person_id(+)
          AND paaf.supervisor_id = papf2.person_id(+)
          AND u.end_date IS NULL
          AND papf.effective_end_date = '31-DEC-4712'
          AND (   paaf.effective_end_date = '31-DEC-4712'
               OR paaf.effective_end_date IS NULL)
          AND papf2.effective_end_date = '31-DEC-4712'
          AND resp.end_date IS NULL
          AND fr.responsibility_name NOT LIKE '%Internet Expense%';


