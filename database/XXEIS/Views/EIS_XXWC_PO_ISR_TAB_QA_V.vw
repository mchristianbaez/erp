CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_po_isr_tab_qa_v
(
   org
  ,organization_name
  ,district
  ,region
  ,pre
  ,item_number
  ,vendor_name
  ,vendor_number
  ,source
  ,st
  ,description
  ,cat_class
  ,inv_cat_seg1
  ,pplt
  ,plt
  ,uom
  ,cl
  ,stk
  ,pm
  ,MIN
  ,MAX
  ,hit_6
  ,aver_cost
  ,item_cost
  ,bpa_cost
  ,bpa
  ,qoh
  ,on_ord
  ,avail
  ,avail_d
  ,bin_loc
  ,mc
  ,fi
  ,freeze_date
  ,res
  ,thirteen_wk_avg_inv
  ,thirteen_wk_an_cogs
  ,turns
  ,buyer
  ,ts
  ,amu
  ,so
  ,mf_flag
  ,wt
  ,ss
  ,sourcing_rule
  ,fml
  ,open_req
  ,inventory_item_id
  ,organization_id
  ,set_of_books_id
  ,clt
  ,avail2
  ,supply
  ,demand
  ,int_req
  ,dir_req
)
AS
   SELECT ood.organization_code org
         ,ood.organization_name organization_name
         ,mtp.attribute8 district
         ,mtp.attribute9 region
         ,SUBSTR (msi.segment1, 1, 3) pre
         ,msi.segment1 item_number
         ,pov.vendor_name vendor_name
         ,pov.segment1 vendor_number
         ,CASE
             WHEN item_source_type.meaning = 'Supplier'
             THEN
                pov.segment1
             WHEN item_source_type.meaning = 'Inventory'
             THEN
                (SELECT organization_code
                   FROM org_organization_definitions source_org
                  WHERE source_org.organization_id =
                           msi.source_organization_id)
             ELSE
                NULL
          END
             source
         ,CASE
             WHEN item_source_type.meaning = 'Supplier' THEN 'S'
             WHEN item_source_type.meaning = 'Inventory' THEN 'I'
             ELSE NULL
          END
             st
         ,msi.description description
         ,mcvc.segment2 cat_class
         ,mcvc.segment1 inv_cat_seg1
         ,preprocessing_lead_time pplt
         ,msi.full_lead_time plt
         ,msi.primary_uom_code uom
         ,mcvs.segment1 cl
         ,CASE
             WHEN (mcvs.segment1 IN
                      ('1', '2', '3', '4', '5', '6', '7', '8', '9', 'C', 'B'))
             THEN
                'Y'
             WHEN (    mcvs.segment1 IN ('E')
                   AND (min_minmax_quantity = 0 AND max_minmax_quantity = 0))
             THEN
                'N'
             WHEN (    mcvs.segment1 IN ('E')
                   AND (min_minmax_quantity > 0 AND max_minmax_quantity > 0))
             THEN
                'Y'
             WHEN (mcvs.segment1 IN ('N', 'Z'))
             --AND ITEM_TYPE                                                                                           ='NON-STOCK')
             THEN
                'N'
             ELSE
                'N'
          END
             stk
         ,mrp_planning_code.meaning pm
         ,min_minmax_quantity MIN
         ,max_minmax_quantity MAX
         ,NULL hit_6
         ,NVL (
             apps.cst_cost_api.get_item_cost (1
                                             ,msi.inventory_item_id
                                             ,msi.organization_id)
            ,0)
             aver_cost
         ,list_price_per_unit item_cost
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_item_cost (
             msi.inventory_item_id
            ,msi.organization_id)
             bpa_cost
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_bpa_doc (
             msi.inventory_item_id
            ,msi.organization_id)
             bpa
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_planning_quantity (
             2
            ,1
            ,msi.organization_id
            ,NULL
            ,msi.inventory_item_id)
             qoh
         ,NVL (
             xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_open_req_qty (
                msi.inventory_item_id
               ,msi.organization_id
               ,'BOTH')
            ,0)
             on_ord
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_avail_qty (
             msi.inventory_item_id
            ,msi.organization_id)
             avail
         ,0 avail_d
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_primary_bin_loc (
             msi.inventory_item_id
            ,msi.organization_id)
             bin_loc
         ,NULL mc
         ,mcvp.segment1 fi
         ,NULL freeze_date
         ,msi.attribute21 res
         ,NULL thirteen_wk_avg_inv
         ,NULL thirteen_wk_an_cogs
         ,NULL turns
         ,ppf.full_name buyer
         ,shelf_life_days ts
         ,msi.attribute20 amu
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_ss_cnt (
             msi.inventory_item_id
            ,msi.organization_id)
             so
         ,NULL mf_flag
         ,msi.unit_weight wt
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_ss (msi.inventory_item_id
                                                       ,msi.organization_id)
             ss
         ,msr.sourcing_rule_name sourcing_rule
         ,msi.fixed_lot_multiplier fml
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_open_req_qty (
             msi.inventory_item_id
            ,msi.organization_id
            ,'VENDOR')
             open_req
         ,msi.inventory_item_id
         ,msi.organization_id
         ,ood.set_of_books_id
         ,msi.attribute30 clt
         , -- xxeis.eis_po_xxwc_isr_util_qa_pkg.GET_INT_REQ_SO_QTY(msi.inventory_item_id, msi.organization_id)avail2
          0 avail2
         , --(xxeis.eis_po_xxwc_isr_util_qa_pkg.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID)                           - XXEIS.EIS_PO_XXWC_ISR_UTIL_QA_PKG.GET_DEMAND_QTY( MSI.ORGANIZATION_ID,NULL,1,MSI.INVENTORY_ITEM_ID,sysdate,2,1,1,1,'Y'))avail2,
          xxeis.eis_po_xxwc_isr_util_qa_pkg.get_supply_qty (
             msi.organization_id
            ,NULL
            ,msi.inventory_item_id
            ,msi.postprocessing_lead_time
            ,'WC STD'
            ,-1
            ,1
            ,SYSDATE
            ,1
            ,1
            ,'Y'
            ,2
            ,1
            ,1
            ,'Y')
             supply
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_demand_qty (
             msi.organization_id
            ,NULL
            ,1
            ,msi.inventory_item_id
            ,SYSDATE
            ,2
            ,1
            ,1
            ,1
            ,'Y')
             demand
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_open_req_qty (
             msi.inventory_item_id
            ,msi.organization_id
            ,'INVENTORY')
             int_req
         ,xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_open_req_qty (
             msi.inventory_item_id
            ,msi.organization_id
            ,'DIRECT')
             dir_req
     FROM mtl_system_items_b msi
         ,org_organization_definitions ood
         ,mtl_categories_kfv mcvs
         ,mtl_item_categories mics
         ,mtl_categories_kfv mcvp
         ,mtl_item_categories micp
         ,mtl_categories_kfv mcvc
         ,mtl_item_categories micc
         ,mrp_sr_assignments msa
         ,mrp_sr_receipt_org msro
         ,mrp_sr_source_org msso
         ,mrp_sourcing_rules msr
         ,po_vendors pov
         ,mfg_lookups mrp_planning_code
         ,mfg_lookups item_source_type
         ,per_people_x ppf
         ,mtl_parameters mtp
         ,mfg_lookups sfty_stk
    WHERE     msi.organization_id = ood.organization_id
          AND msi.buyer_id = ppf.person_id(+)
          AND msi.inventory_item_id = mics.inventory_item_id(+)
          AND msi.organization_id = mics.organization_id(+)
          AND mics.category_id = mcvs.category_id(+)
          AND mcvs.structure_id(+) = 50410
          AND mics.category_set_id(+) = 1100000044
          AND msi.inventory_item_id = micp.inventory_item_id(+)
          AND msi.organization_id = micp.organization_id(+)
          AND micp.category_id = mcvp.category_id(+)
          AND mcvp.structure_id(+) = 50408
          AND micp.category_set_id(+) = 1100000043
          AND msi.inventory_item_id = micc.inventory_item_id
          AND msi.organization_id = micc.organization_id
          AND micc.category_id = mcvc.category_id
          AND mcvc.structure_id = 101
          AND micc.category_set_id = 1100000062
          AND msi.inventory_item_id = msa.inventory_item_id(+)
          AND msi.organization_id = msa.organization_id(+)
          AND msa.sourcing_rule_id = msro.sourcing_rule_id(+)
          AND msa.sourcing_rule_id = msr.sourcing_rule_id(+)
          AND msro.sr_receipt_id = msso.sr_receipt_id(+)
          AND msso.vendor_id = pov.vendor_id(+)
          AND mrp_planning_code.lookup_type(+) = 'MTL_MATERIAL_PLANNING'
          AND mrp_planning_code.lookup_code(+) = msi.inventory_planning_code
          AND item_source_type.lookup_type(+) = 'MTL_SOURCE_TYPES'
          AND item_source_type.lookup_code(+) = msi.source_type
          AND msi.organization_id = mtp.organization_id
          AND sfty_stk.lookup_type(+) = 'MTL_SAFETY_STOCK_TYPE'
          AND sfty_stk.lookup_code(+) = msi.mrp_safety_stock_code
          AND NOT EXISTS
                     (SELECT 1
                        FROM mtl_parameters mp1
                       WHERE     mp1.organization_id = msi.organization_id
                             AND organization_code IN
                                    ('CAN', 'HDS', 'US1', 'CN1', 'MST', 'WCC'));


