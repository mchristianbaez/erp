CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_cost_sales_txn_v
(
   region
  ,branch
  ,invoice_creation_date
  ,invoice_number
  ,sales_order_number
  ,ordered_date
  ,order_type
  ,created_by
  ,account_manager_name
  ,acct_mngr_sales_rep_number
  ,sales_order_line_number
  ,sales_order_line_type
  ,fulfillment_source
  ,shipping_method
  ,item
  ,item_desc
  ,user_item_type
  ,item_status
  ,item_category
  ,invoiced_quantity
  ,pricing_qty_uom
  ,unit_selling_price
  ,ordered_quantity
  ,extended_sale_amount
  ,actual_unit_cost
  ,current_item_average_cost
  ,customer_name
  ,customer_number
  ,warehouse
  ,customer_trx_id
  ,line_number
  ,line_id
  ,organization_id
)
AS
     SELECT mp.attribute9 region
           ,ood.organization_name branch
           ,rctl.creation_date invoice_creation_date
           ,trx_number invoice_number
           ,oh.order_number sales_order_number
           ,TRUNC (oh.ordered_date) ordered_date
           ,otlh.name order_type
           ,oh.attribute7 created_by
           ,rep.name account_manager_name
           ,rep.salesrep_number acct_mngr_sales_rep_number
           ,ol.line_number sales_order_line_number
           ,otl.name sales_order_line_type
           ,ol.source_type_code fulfillment_source
           ,ol.freight_carrier_code shipping_method
           ,                                                                         --       ol.ordered_item,
            msi.concatenated_segments item
           ,msi.description item_desc
           ,msi.item_type user_item_type
           ,msi.inventory_item_status_code item_status
           ,xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class (msi.inventory_item_id, msi.organization_id)
               item_category
           ,NVL (rctl.quantity_invoiced, rctl.quantity_credited) invoiced_quantity
           ,ol.pricing_quantity_uom pricing_qty_uom
           ,rctl.unit_selling_price
           ,ol.ordered_quantity
           , (rctl.unit_selling_price * NVL (rctl.quantity_invoiced, rctl.quantity_credited))
               extended_sale_amount
           ,mmt.actual_cost actual_unit_cost
           ,          --NVL (APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST (OL.LINE_ID) , 0) Actual_Unit_Cost,
            NVL (apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
               current_item_average_cost
           ,                                 --          (NVL(rctl.quantity_invoiced, rctl.quantity_credited)*
 --          NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id, msi.organization_id), 0)) Projected_Extended_Cost,
 --          (rctl.unit_selling_price*ol.ORDERED_QUANTITY) - (NVL(rctl.quantity_invoiced, rctl.quantity_credited)*
 --          NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id, msi.organization_id), 0)) Projected_GP,
            NVL (hca.account_name, hp.party_name) customer_name
           ,hca.account_number customer_number
           ,mp.organization_code warehouse
           ,rct.customer_trx_id
           ,rctl.line_number
           ,ol.line_id
           ,msi.organization_id
       FROM oe_order_headers oh
           ,oe_order_lines ol
           ,ra_customer_trx rct
           ,ra_customer_trx_lines rctl
           ,mtl_parameters mp
           ,org_organization_definitions ood
           ,oe_transaction_types_vl otlh
           ,oe_transaction_types_vl otl
           ,ra_salesreps rep
           ,mtl_system_items_kfv msi
           ,mtl_material_transactions mmt
           ,hz_cust_accounts hca
           ,hz_parties hp
           ,gl_code_combinations gcc
      WHERE     1 = 1
            AND rct.interface_header_context = 'ORDER ENTRY'
            --AND mp.organization_code = '017'
            AND TO_CHAR (oh.order_number) = rct.interface_header_attribute1
            AND rctl.customer_trx_id = rct.customer_trx_id
            AND TO_CHAR (oh.order_number) = rctl.interface_line_attribute1
            AND TO_CHAR (ol.line_id) = rctl.interface_line_attribute6
            AND otlh.name = rctl.interface_line_attribute2
            AND TO_CHAR (mp.organization_id) = rctl.interface_line_attribute10
            AND otlh.transaction_type_id = oh.order_type_id
            AND oh.org_id = otlh.org_id
            AND oh.salesrep_id = rep.salesrep_id(+)
            AND oh.org_id = rep.org_id(+)
            AND ol.line_type_id = otl.transaction_type_id
            AND ol.org_id = otl.org_id
            AND mp.organization_id = msi.organization_id
            AND rctl.inventory_item_id = msi.inventory_item_id
            AND mmt.trx_source_line_id = ol.line_id
            AND rct.bill_to_customer_id = hca.cust_account_id
            AND hp.party_id = hca.party_id
            AND mp.organization_id = ood.organization_id
            AND NVL (mmt.actual_cost, 0) = 0
            --AND NVL (APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST (OL.LINE_ID) , 0) =0
            AND mmt.source_code = 'ORDER ENTRY'
            AND gcc.code_combination_id = msi.cost_of_sales_account
            -- and rct.customer_trx_id in (1858956,584979)
            --  AND OH.ORDER_NUMBER = '12342487'
            AND otlh.name NOT IN ('WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL', 'INTERNAL ORDER')
            AND otl.name NOT IN ('CREDIT ONLY', 'BILL ONLY')
            AND UPPER (msi.item_type) <> UPPER ('Intangible')
            AND UPPER (msi.inventory_item_status_code) <> UPPER ('Intangible')
            AND xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class (msi.inventory_item_id, msi.organization_id) NOT IN ('PR.PRMO'
                                                                                                                     ,'GC.GC10'
                                                                                                                     ,'DC.DC99'
                                                                                                                     ,'ML.ML99'
                                                                                                                     ,'99.99RR'
                                                                                                                     ,'99.99RT'
                                                                                                                     ,'RE.READ')
            AND rctl.description <> 'Delivery Charge'
            AND gcc.segment4 <> '646080'
            AND rctl.creation_date >=
                     TO_DATE (
                        TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                                ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                       ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                   + 0.25
            AND rctl.creation_date <=
                     TO_DATE (
                        TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
                                ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                       ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                   + 1.25
   --    AND NOT EXISTS
   --    (SELECT 'Y'
   --    FROM MTL_SYSTEM_ITEMS_B MSI,
   --      GL_CODE_COMBINATIONS_KFV GCC
   --    WHERE 1                     =1
   --    AND MSI.INVENTORY_ITEM_ID   = OL.INVENTORY_ITEM_ID
   --    AND MSI.ORGANIZATION_ID     = OL.SHIP_FROM_ORG_ID
   --    AND GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
   --    AND GCC.SEGMENT4            ='646080'
   --    )
   --   and oh.header_id = 2215672 --566180
   -- and mp.organization_code ='711'
   ORDER BY oh.header_id, ol.line_number;