-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_AHH_PRICE_VAR_V.vw $
  Module Name: Order Management
  PURPOSE: AHH Price Variance Report - WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 10/8/2018      Siva			   TMS#20181005-00001
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_AHH_PRICE_VAR_V (WAREHOUSE, ORDER_TYPE, ORDERED_DATE, LINE_FLOW_STATUS_CODE, ORDER_NUMBER, CREATED_BY, CUSTOMER_NUMBER, CUSTOMER_NAME, AVERAGE_COST, SALESREP_NAME, SALES, SALE_COST, ITEM, ITEM_DESC, QTY, UNIT_SELLING_PRICE, LINE_TYPE, DISTRICT, BRANCH_NAME, PRICE_SOURCE_TYPE, PRICE_TYPE, SYSTEM_PRICE, SXE_LAST_PRICE_PAID)
AS 
  SELECT ship_from_org.organization_code warehouse,
    oth.name order_type,
    TRUNC (oh.ordered_date) ordered_date,
    ol.flow_status_code line_flow_status_code,
    oh.order_number,
    oh.attribute7 created_by,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, 'ACCOUNTNUM') customer_number ,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, NULL) customer_name,
    CASE
      WHEN mcvc.segment2 = 'PRMO'
      THEN (-1 * ol.unit_selling_price)
      ELSE xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
    END average_cost,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_salesrep_details(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), (DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)) * NVL ( xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id), 0)), 0)) sale_cost,
    msi.segment1 item,
    CASE
      WHEN msi.segment1 = 'Rental Charge'
      THEN ol.user_item_description
      ELSE msi.description
    END item_desc,
    DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
    DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price) unit_selling_price,
    otl.name line_type,
    ship_from_org.attribute8 district,
    hou.name branch_name,
    NVL(
    (SELECT NVL(adj2.description,'MKT')
    FROM
      (SELECT vs.description,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'MKT') price_source_type,
    NVL(
    (SELECT
      CASE
        WHEN adj2.price_typ_automatic_flag = 'N'
        THEN 'MANUAL'
        WHEN adj2.attribute10 = 'Contract Pricing'
        OR name LIKE 'CSP%'
        THEN 'CONTRACT'
        WHEN adj2.attribute10 = 'Vendor Quote'
        THEN 'VQN'
        ELSE 'SYSTEM'
      END price_type
    FROM
      (SELECT lh.attribute10,
        lh.name,
        last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following)price_typ_automatic_flag,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'SYSTEM') price_type,
    NVL(
    (SELECT
      CASE
        WHEN NVL (adj2.aut_flag_for_selling_price, 'N') = 'N'
        THEN ol.unit_list_price
        WHEN adj2.arithmetic_operator = 'NEWPRICE'
        THEN ROUND (adj2.operand,4)
        WHEN adj2.arithmetic_operator = '%'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price * (100 - adj2.operand) / 100
        WHEN adj2.arithmetic_operator = 'AMT'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price - adj2.operand
        ELSE ol.unit_list_price
      END overridden_sell_price
    FROM
      (SELECT last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) aut_flag_for_selling_price,
        last_value(adj.arithmetic_operator) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) arithmetic_operator,
        last_value(adj.operand) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) operand,
        last_value(adj.list_line_type_code) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) list_line_type_code,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),ol.unit_list_price) system_price,
    xoal.last_price_paid sxe_last_price_paid
    --descr#flexfield#start
    --descr#flexfield#end
  FROM ra_customer_trx rct,
    ra_customer_trx_lines rctl,
    oe_order_headers oh,
    oe_order_lines ol,
    mtl_parameters ship_from_org ,
    hr_all_organization_units hou,
    oe_transaction_types_vl oth,
    oe_transaction_types_vl otl,
    mtl_system_items_kfv msi,
    mtl_item_categories micc,
    mtl_categories_kfv mcvc,
    xxwc.xxwc_ahh_oe_ais_lpp_tbl xoal
  WHERE rct.customer_trx_id            = rctl.customer_trx_id
  AND rctl.interface_line_attribute6   = TO_CHAR (ol.line_id)
  AND rctl.interface_line_attribute1   = TO_CHAR (oh.order_number)
  AND rctl.interface_line_context      = 'ORDER ENTRY'
  AND oh.header_id                     = ol.header_id
  AND ol.ship_from_org_id              = ship_from_org.organization_id(+)
  AND ship_from_org.organization_id    = hou.organization_id(+)
  AND oh.order_type_id                 = oth.transaction_type_id
  AND oh.org_id                        = oth.org_id
  AND ol.line_type_id                  = otl.transaction_type_id
  AND ol.org_id                        = otl.org_id
  AND ol.inventory_item_id             = msi.inventory_item_id(+)
  AND ol.ship_from_org_id              = msi.organization_id(+)
  AND msi.inventory_item_id            = micc.inventory_item_id(+)
  AND msi.organization_id              = micc.organization_id(+)
  AND micc.category_id                 = mcvc.category_id(+)
  AND mcvc.structure_id(+)             = 101
  AND micc.category_set_id(+)          = 1100000062
  AND ol.inventory_item_id             = xoal.inventory_item_id(+)
  AND ol.ship_to_org_id                = xoal.ship_to_org_id(+)
  AND ol.ordered_item NOT             IN ('CONTOFFSET', 'CONTBILL')
  AND TRUNC (oh.ordered_date)          < TRUNC (sysdate)
  AND ol.flow_status_code             IN ('CLOSED')
  AND ol.invoice_interface_status_code = 'YES'
  AND oth.name                        != 'INTERNAL ORDER'
  AND ol.source_type_code             IN ('INTERNAL', 'EXTERNAL')
  AND rctl.creation_date              >= to_date ( TO_CHAR (xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from ,xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') ,xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 0.25
  AND rctl.creation_date <= to_date ( TO_CHAR (xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from ,xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') ,xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 1.25
  AND NOT EXISTS
    (SELECT  /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
      'Y'
    FROM gl_code_combinations_kfv gcc
    WHERE gcc.code_combination_id = msi.cost_of_sales_account
    AND gcc.segment4              = '646080'
    )
  UNION /*-- Same Days Counter Orders*/
  SELECT /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N8)*/
    ship_from_org.organization_code warehouse,
    oth.name order_type,
    TRUNC (oh.ordered_date) ordered_date,
    ol.flow_status_code line_flow_status_code,
    oh.order_number,
    oh.attribute7 created_by,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, 'ACCOUNTNUM') customer_number,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, NULL) customer_name,
    CASE
      WHEN mcvc.segment2 = 'PRMO'
      THEN (                    -1 * ol.unit_selling_price)
      WHEN ol.flow_status_code IN ('CLOSED')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE DECODE ( NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) , 0, ol.unit_cost, apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id))
    END average_cost,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_salesrep_details(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
    CASE
      WHEN ol.flow_status_code IN ('CLOSED')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE DECODE ( NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0), 0, ol.unit_cost, apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id))
    END) * (DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
    msi.segment1 item,
    CASE
      WHEN msi.segment1 = 'Rental Charge'
      THEN ol.user_item_description
      ELSE msi.description
    END item_desc,
    DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
    DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price) unit_selling_price,
    otl.name line_type,
    ship_from_org.attribute8 district,
    hou.name branch_name,
    NVL(
    (SELECT NVL(adj2.description,'MKT')
    FROM
      (SELECT vs.description,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'MKT') price_source_type,
    NVL(
    (SELECT
      CASE
        WHEN adj2.price_typ_automatic_flag = 'N'
        THEN 'MANUAL'
        WHEN adj2.attribute10 = 'Contract Pricing'
        OR name LIKE 'CSP%'
        THEN 'CONTRACT'
        WHEN adj2.attribute10 = 'Vendor Quote'
        THEN 'VQN'
        ELSE 'SYSTEM'
      END price_type
    FROM
      (SELECT lh.attribute10,
        lh.name,
        last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following)price_typ_automatic_flag,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'SYSTEM') price_type,
    NVL(
    (SELECT
      CASE
        WHEN NVL (adj2.aut_flag_for_selling_price, 'N') = 'N'
        THEN ol.unit_list_price
        WHEN adj2.arithmetic_operator = 'NEWPRICE'
        THEN ROUND (adj2.operand,4)
        WHEN adj2.arithmetic_operator = '%'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price * (100 - adj2.operand) / 100
        WHEN adj2.arithmetic_operator = 'AMT'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price - adj2.operand
        ELSE ol.unit_list_price
      END overridden_sell_price
    FROM
      (SELECT last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) aut_flag_for_selling_price,
        last_value(adj.arithmetic_operator) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) arithmetic_operator,
        last_value(adj.operand) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) operand,
        last_value(adj.list_line_type_code) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) list_line_type_code,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),ol.unit_list_price) system_price,
    xoal.last_price_paid sxe_last_price_paid
    --descr#flexfield#start
    --descr#flexfield#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_parameters ship_from_org ,
    hr_all_organization_units hou,
    oe_transaction_types_vl oth,
    oe_transaction_types_vl otl,
    xxwc.xxwc_ahh_oe_ais_lpp_tbl xoal,
    mtl_system_items_kfv msi,
    mtl_item_categories micc,
    mtl_categories_kfv mcvc
  WHERE oh.header_id                = ol.header_id
  AND ol.ship_from_org_id           = ship_from_org.organization_id(+)
  AND ship_from_org.organization_id = hou.organization_id(+)
  AND oh.order_type_id              = oth.transaction_type_id
  AND oh.org_id                     = oth.org_id
  AND ol.line_type_id               = otl.transaction_type_id
  AND ol.org_id                     = otl.org_id
  AND ol.inventory_item_id          = xoal.inventory_item_id(+)
  AND ol.ship_to_org_id             = xoal.ship_to_org_id(+)
  AND ol.inventory_item_id          = msi.inventory_item_id(+)
  AND ol.ship_from_org_id           = msi.organization_id(+)
  AND msi.inventory_item_id         = micc.inventory_item_id(+)
  AND msi.organization_id           = micc.organization_id(+)
  AND micc.category_set_id(+)       = 1100000062
  AND micc.category_id              = mcvc.category_id(+)
  AND mcvc.structure_id(+)          = 101
  AND ol.flow_status_code          <> 'CANCELLED'
  AND ol.ordered_item NOT          IN ('CONTOFFSET', 'CONTBILL')
  AND oth.name                     != 'INTERNAL ORDER'
  AND TRUNC (sysdate)               = TRUNC (xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from)
  AND oth.name                      = 'COUNTER ORDER'
  AND oh.flow_status_code           = 'BOOKED'
  AND TRUNC (oh.ordered_date)       = TRUNC (sysdate)
  AND ol.source_type_code          IN ('INTERNAL', 'EXTERNAL')
  AND NOT EXISTS
    (SELECT  /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
      'Y'
    FROM gl_code_combinations_kfv gcc
    WHERE gcc.code_combination_id = msi.cost_of_sales_account
    AND gcc.segment4              = '646080'
    )
  --UNION 3
  UNION /* Invoiced Lines after 6 AM on the same day */
  SELECT /*+ INDEX(oh OE_ORDER_HEADERS_U1)*/
    ship_from_org.organization_code warehouse,
    oth.name order_type,
    TRUNC (oh.ordered_date) ordered_date,
    ol.flow_status_code line_flow_status_code,
    oh.order_number,
    oh.attribute7 created_by,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, 'ACCOUNTNUM') customer_number,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, NULL) customer_name,
    CASE
      WHEN mcvc.segment2 = 'PRMO'
      THEN (-1 * ol.unit_selling_price)
      ELSE xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
    END average_cost,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_salesrep_details(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), (DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)) * NVL ( xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id), 0)), 0)) sale_cost,
    msi.segment1 item,
    CASE
      WHEN msi.segment1 = 'Rental Charge'
      THEN ol.user_item_description
      ELSE msi.description
    END item_desc,
    DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
    DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price) unit_selling_price,
    otl.name line_type,
    ship_from_org.attribute8 district,
    hou.name branch_name,
    NVL(
    (SELECT NVL(adj2.description,'MKT')
    FROM
      (SELECT vs.description,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'MKT') price_source_type,
    NVL(
    (SELECT
      CASE
        WHEN adj2.price_typ_automatic_flag = 'N'
        THEN 'MANUAL'
        WHEN adj2.attribute10 = 'Contract Pricing'
        OR name LIKE 'CSP%'
        THEN 'CONTRACT'
        WHEN adj2.attribute10 = 'Vendor Quote'
        THEN 'VQN'
        ELSE 'SYSTEM'
      END price_type
    FROM
      (SELECT lh.attribute10,
        lh.name,
        last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following)price_typ_automatic_flag,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'SYSTEM') price_type,
    NVL(
    (SELECT
      CASE
        WHEN NVL (adj2.aut_flag_for_selling_price, 'N') = 'N'
        THEN ol.unit_list_price
        WHEN adj2.arithmetic_operator = 'NEWPRICE'
        THEN ROUND (adj2.operand,4)
        WHEN adj2.arithmetic_operator = '%'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price * (100 - adj2.operand) / 100
        WHEN adj2.arithmetic_operator = 'AMT'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price - adj2.operand
        ELSE ol.unit_list_price
      END overridden_sell_price
    FROM
      (SELECT last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) aut_flag_for_selling_price,
        last_value(adj.arithmetic_operator) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) arithmetic_operator,
        last_value(adj.operand) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) operand,
        last_value(adj.list_line_type_code) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) list_line_type_code,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),ol.unit_list_price) system_price,
    xoal.last_price_paid sxe_last_price_paid
    --descr#flexfield#start
    --descr#flexfield#end
  FROM ra_customer_trx rct,
    ra_customer_trx_lines rctl,
    oe_order_headers oh,
    oe_order_lines ol,
    mtl_parameters ship_from_org,
    hr_all_organization_units hou,
    oe_transaction_types_vl oth,
    oe_transaction_types_vl otl,
    xxwc.xxwc_ahh_oe_ais_lpp_tbl xoal,
    mtl_system_items_kfv msi,
    mtl_categories_kfv mcvc,
    mtl_item_categories micc
  WHERE rct.customer_trx_id          = rctl.customer_trx_id
  AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)
  AND rctl.interface_line_attribute1 = TO_CHAR (oh.order_number)
  AND rctl.interface_line_context    = 'ORDER ENTRY'
  AND oh.header_id                   = ol.header_id
  AND ol.ship_from_org_id            = ship_from_org.organization_id(+)
  AND ship_from_org.organization_id  = hou.organization_id(+)
  AND oh.order_type_id               = oth.transaction_type_id
  AND oh.org_id                      = oth.org_id
  AND ol.line_type_id                = otl.transaction_type_id
  AND ol.org_id                      = otl.org_id
  AND ol.inventory_item_id           = xoal.inventory_item_id(+)
  AND ol.ship_to_org_id              = xoal.ship_to_org_id(+)
  AND ol.inventory_item_id           = msi.inventory_item_id(+)
  AND ol.ship_from_org_id            = msi.organization_id(+)
  AND msi.inventory_item_id          = micc.inventory_item_id(+)
  AND msi.organization_id            = micc.organization_id(+)
  AND micc.category_id               = mcvc.category_id(+)
  AND mcvc.structure_id(+)           = 101
  AND micc.category_set_id(+)        = 1100000062
  AND ol.flow_status_code           <> 'CANCELLED'
  AND ol.ordered_item NOT           IN ('CONTOFFSET', 'CONTBILL')
  AND oth.name                      != 'INTERNAL ORDER'
  AND TRUNC (sysdate)                = TRUNC (xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from)
  AND rctl.creation_date            >= to_date ( TO_CHAR (xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from ,xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') ,xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS')      + 0.25
  AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
  AND NOT EXISTS
    (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
      'Y'
    FROM gl_code_combinations_kfv gcc
    WHERE gcc.code_combination_id = msi.cost_of_sales_account
    AND gcc.segment4              = '646080'
    )
  --- UNION 4-1
  UNION
  SELECT ship_from_org.organization_code warehouse,
    oth.name order_type,
    TRUNC (oh.ordered_date) ordered_date,
    ol.flow_status_code line_flow_status_code,
    oh.order_number,
    oh.attribute7 created_by,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, 'ACCOUNTNUM') customer_number,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, NULL) customer_name,
    CASE
      WHEN mcvc.segment2 = 'PRMO'
      THEN (                    -1 * ol.unit_selling_price)
      WHEN ol.flow_status_code IN ('CLOSED', 'INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost (ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
    END average_cost,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_salesrep_details(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
    CASE
      WHEN ol.flow_status_code IN ('CLOSED', 'INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id) --TMS#20150921-00332  by Siva    on 04-12-2016
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0)
    END) * (DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
    msi.segment1 item,
    CASE
      WHEN msi.segment1 = 'Rental Charge'
      THEN ol.user_item_description
      ELSE msi.description
    END item_desc,
    DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
    DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price) unit_selling_price,
    otl.name line_type,
    ship_from_org.attribute8 district,
    hou.name branch_name,
    NVL(
    (SELECT NVL(adj2.description,'MKT')
    FROM
      (SELECT vs.description,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'MKT') price_source_type,
    NVL(
    (SELECT
      CASE
        WHEN adj2.price_typ_automatic_flag = 'N'
        THEN 'MANUAL'
        WHEN adj2.attribute10 = 'Contract Pricing'
        OR name LIKE 'CSP%'
        THEN 'CONTRACT'
        WHEN adj2.attribute10 = 'Vendor Quote'
        THEN 'VQN'
        ELSE 'SYSTEM'
      END price_type
    FROM
      (SELECT lh.attribute10,
        lh.name,
        last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following)price_typ_automatic_flag,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'SYSTEM') price_type,
    NVL(
    (SELECT
      CASE
        WHEN NVL (adj2.aut_flag_for_selling_price, 'N') = 'N'
        THEN ol.unit_list_price
        WHEN adj2.arithmetic_operator = 'NEWPRICE'
        THEN ROUND (adj2.operand,4)
        WHEN adj2.arithmetic_operator = '%'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price * (100 - adj2.operand) / 100
        WHEN adj2.arithmetic_operator = 'AMT'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price - adj2.operand
        ELSE ol.unit_list_price
      END overridden_sell_price
    FROM
      (SELECT last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) aut_flag_for_selling_price,
        last_value(adj.arithmetic_operator) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) arithmetic_operator,
        last_value(adj.operand) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) operand,
        last_value(adj.list_line_type_code) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) list_line_type_code,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),ol.unit_list_price) system_price,
    xoal.last_price_paid sxe_last_price_paid
    --descr#flexfield#start
    --descr#flexfield#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_parameters ship_from_org ,
    hr_all_organization_units hou,
    oe_transaction_types_vl oth,
    oe_transaction_types_vl otl,
    xxwc.xxwc_ahh_oe_ais_lpp_tbl xoal,
    mtl_system_items_kfv msi,
    mtl_item_categories micc,
    mtl_categories_kfv mcvc
  WHERE oh.header_id                = ol.header_id
  AND ol.ship_from_org_id           = ship_from_org.organization_id(+)
  AND ship_from_org.organization_id = hou.organization_id(+)
  AND oh.order_type_id              = oth.transaction_type_id
  AND oh.org_id                     = oth.org_id
  AND ol.line_type_id               = otl.transaction_type_id
  AND ol.org_id                     = otl.org_id
  AND ol.inventory_item_id          = xoal.inventory_item_id(+)
  AND ol.ship_to_org_id             = xoal.ship_to_org_id(+)
  AND ol.inventory_item_id          = msi.inventory_item_id(+)
  AND ol.ship_from_org_id           = msi.organization_id(+)
  AND msi.inventory_item_id         = micc.inventory_item_id(+)
  AND msi.organization_id           = micc.organization_id(+)
  AND micc.category_id              = mcvc.category_id(+)
  AND mcvc.structure_id(+)          = 101
  AND micc.category_set_id(+)       = 1100000062
  AND ol.flow_status_code NOT      IN ('CLOSED', 'CANCELLED')
  AND ol.ordered_item NOT          IN ('CONTOFFSET', 'CONTBILL')
  AND oth.name                     != 'INTERNAL ORDER'
  AND oth.name                      = 'STANDARD ORDER'
  AND (ol.user_item_description     = 'DELIVERED'
  OR ( ol.source_type_code          = 'EXTERNAL'
  AND ol.flow_status_code           = 'INVOICE_HOLD'))
  AND TRUNC (sysdate)               = TRUNC (xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from)
  AND ol.source_type_code          IN ('INTERNAL', 'EXTERNAL')
  AND NOT EXISTS
    (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
      'Y'
    FROM gl_code_combinations_kfv gcc
    WHERE gcc.code_combination_id = msi.cost_of_sales_account
    AND gcc.segment4              = '646080'
    )
  --- UNION 4-2
  UNION
  SELECT /*+ INDEX(ol XXWC_OE_ORDER_LN_AL_N13) */
    ship_from_org.organization_code warehouse,
    oth.name order_type,
    TRUNC (oh.ordered_date) ordered_date,
    ol.flow_status_code line_flow_status_code,
    oh.order_number,
    oh.attribute7 created_by,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, 'ACCOUNTNUM') customer_number,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, NULL) customer_name,
    CASE
      WHEN mcvc.segment2 = 'PRMO'
      THEN (                    -1 * ol.unit_selling_price)
      WHEN ol.flow_status_code IN ('CLOSED', 'INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
    END average_cost,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_salesrep_details(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
    CASE
      WHEN ol.flow_status_code IN ('CLOSED', 'INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0)
    END) * (DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
    msi.segment1 item,
    CASE
      WHEN msi.segment1 = 'Rental Charge'
      THEN ol.user_item_description
      ELSE msi.description
    END item_desc,
    DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
    DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price) unit_selling_price,
    otl.name line_type,
    ship_from_org.attribute8 district,
    hou.name branch_name,
    NVL(
    (SELECT NVL(adj2.description,'MKT')
    FROM
      (SELECT vs.description,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'MKT') price_source_type,
    NVL(
    (SELECT
      CASE
        WHEN adj2.price_typ_automatic_flag = 'N'
        THEN 'MANUAL'
        WHEN adj2.attribute10 = 'Contract Pricing'
        OR name LIKE 'CSP%'
        THEN 'CONTRACT'
        WHEN adj2.attribute10 = 'Vendor Quote'
        THEN 'VQN'
        ELSE 'SYSTEM'
      END price_type
    FROM
      (SELECT lh.attribute10,
        lh.name,
        last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following)price_typ_automatic_flag,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'SYSTEM') price_type,
    NVL(
    (SELECT
      CASE
        WHEN NVL (adj2.aut_flag_for_selling_price, 'N') = 'N'
        THEN ol.unit_list_price
        WHEN adj2.arithmetic_operator = 'NEWPRICE'
        THEN ROUND (adj2.operand,4)
        WHEN adj2.arithmetic_operator = '%'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price * (100 - adj2.operand) / 100
        WHEN adj2.arithmetic_operator = 'AMT'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price - adj2.operand
        ELSE ol.unit_list_price
      END overridden_sell_price
    FROM
      (SELECT last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) aut_flag_for_selling_price,
        last_value(adj.arithmetic_operator) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) arithmetic_operator,
        last_value(adj.operand) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) operand,
        last_value(adj.list_line_type_code) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) list_line_type_code,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),ol.unit_list_price) system_price,
    xoal.last_price_paid sxe_last_price_paid
    --descr#flexfield#start
    --descr#flexfield#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_parameters ship_from_org ,
    hr_all_organization_units hou,
    oe_transaction_types_vl oth,
    oe_transaction_types_vl otl,
    xxwc.xxwc_ahh_oe_ais_lpp_tbl xoal,
    mtl_system_items_kfv msi,
    mtl_item_categories micc,
    mtl_categories_kfv mcvc
  WHERE oh.header_id                = ol.header_id
  AND ol.ship_from_org_id           = ship_from_org.organization_id(+)
  AND ship_from_org.organization_id = hou.organization_id(+)
  AND oh.order_type_id              = oth.transaction_type_id
  AND oh.org_id                     = oth.org_id
  AND ol.line_type_id               = otl.transaction_type_id
  AND ol.org_id                     = otl.org_id
  AND ol.inventory_item_id          = xoal.inventory_item_id(+)
  AND ol.ship_to_org_id             = xoal.ship_to_org_id(+)
  AND ol.inventory_item_id          = msi.inventory_item_id(+)
  AND ol.ship_from_org_id           = msi.organization_id(+)
  AND msi.inventory_item_id         = micc.inventory_item_id(+)
  AND msi.organization_id           = micc.organization_id(+)
  AND micc.category_id              = mcvc.category_id(+)
  AND mcvc.structure_id(+)          = 101
  AND micc.category_set_id(+)       = 1100000062
  AND ol.flow_status_code NOT      IN ('CLOSED', 'CANCELLED')
  AND ol.ordered_item NOT          IN ('CONTOFFSET', 'CONTBILL')
  AND oth.name                     != 'INTERNAL ORDER'
  AND oth.name                     IN ('REPAIR ORDER', 'RETURN ORDER')
  AND ol.flow_status_code           = 'INVOICE_HOLD'
  AND TRUNC (sysdate)               = TRUNC (xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from)
  AND ol.source_type_code          IN ('INTERNAL', 'EXTERNAL')
  AND NOT EXISTS
    (SELECT  /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
      'Y'
    FROM gl_code_combinations_kfv gcc
    WHERE gcc.code_combination_id = msi.cost_of_sales_account
    AND gcc.segment4              = '646080'
    )
  --- UNION 4-3
  UNION
  SELECT ship_from_org.organization_code warehouse,
    oth.name order_type,
    TRUNC (oh.ordered_date) ordered_date,
    ol.flow_status_code line_flow_status_code,
    oh.order_number,
    oh.attribute7 created_by,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, 'ACCOUNTNUM') customer_number,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, NULL) customer_name,
    CASE
      WHEN mcvc.segment2 = 'PRMO'
      THEN (                    -1 * ol.unit_selling_price)
      WHEN ol.flow_status_code IN ('CLOSED', 'INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
    END average_cost,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_salesrep_details(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
    CASE
      WHEN ol.flow_status_code IN ('CLOSED', 'INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0)
    END) * (DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
    msi.segment1 item,
    CASE
      WHEN msi.segment1 = 'Rental Charge'
      THEN ol.user_item_description
      ELSE msi.description
    END item_desc,
    DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
    DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price) unit_selling_price,
    otl.name line_type,
    ship_from_org.attribute8 district,
    hou.name branch_name,
    NVL(
    (SELECT NVL(adj2.description,'MKT')
    FROM
      (SELECT vs.description,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'MKT') price_source_type,
    NVL(
    (SELECT
      CASE
        WHEN adj2.price_typ_automatic_flag = 'N'
        THEN 'MANUAL'
        WHEN adj2.attribute10 = 'Contract Pricing'
        OR name LIKE 'CSP%'
        THEN 'CONTRACT'
        WHEN adj2.attribute10 = 'Vendor Quote'
        THEN 'VQN'
        ELSE 'SYSTEM'
      END price_type
    FROM
      (SELECT lh.attribute10,
        lh.name,
        last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following)price_typ_automatic_flag,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'SYSTEM') price_type,
    NVL(
    (SELECT
      CASE
        WHEN NVL (adj2.aut_flag_for_selling_price, 'N') = 'N'
        THEN ol.unit_list_price
        WHEN adj2.arithmetic_operator = 'NEWPRICE'
        THEN ROUND (adj2.operand,4)
        WHEN adj2.arithmetic_operator = '%'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price * (100 - adj2.operand) / 100
        WHEN adj2.arithmetic_operator = 'AMT'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price - adj2.operand
        ELSE ol.unit_list_price
      END overridden_sell_price
    FROM
      (SELECT last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) aut_flag_for_selling_price,
        last_value(adj.arithmetic_operator) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) arithmetic_operator,
        last_value(adj.operand) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) operand,
        last_value(adj.list_line_type_code) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) list_line_type_code,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),ol.unit_list_price) system_price,
    xoal.last_price_paid sxe_last_price_paid
    --descr#flexfield#start
    --descr#flexfield#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_parameters ship_from_org ,
    hr_all_organization_units hou,
    oe_transaction_types_vl oth,
    oe_transaction_types_vl otl,
    xxwc.xxwc_ahh_oe_ais_lpp_tbl xoal,
    mtl_system_items_kfv msi,
    mtl_item_categories micc,
    mtl_categories_kfv mcvc
  WHERE oh.header_id                = ol.header_id
  AND ol.ship_from_org_id           = ship_from_org.organization_id(+)
  AND ship_from_org.organization_id = hou.organization_id(+)
  AND oh.order_type_id              = oth.transaction_type_id
  AND oh.org_id                     = oth.org_id
  AND ol.line_type_id               = otl.transaction_type_id
  AND ol.org_id                     = otl.org_id
  AND ol.inventory_item_id          = xoal.inventory_item_id(+)
  AND ol.ship_to_org_id             = xoal.ship_to_org_id(+)
  AND ol.inventory_item_id          = msi.inventory_item_id(+)
  AND ol.ship_from_org_id           = msi.organization_id(+)
  AND msi.inventory_item_id         = micc.inventory_item_id(+)
  AND msi.organization_id           = micc.organization_id(+)
  AND micc.category_id              = mcvc.category_id(+)
  AND mcvc.structure_id(+)          = 101
  AND micc.category_set_id(+)       = 1100000062
  AND ol.flow_status_code NOT      IN ('CLOSED', 'CANCELLED')
  AND ol.ordered_item NOT          IN ('CONTOFFSET', 'CONTBILL')
  AND oth.name                     != 'INTERNAL ORDER'
  AND EXISTS
    (SELECT 1
    FROM ra_interface_lines_all ril
    WHERE ril.interface_line_context = 'ORDER ENTRY'
    AND TO_CHAR (ol.line_id)         = ril.interface_line_attribute6
    AND TO_CHAR (oh.order_number)    = ril.interface_line_attribute1
    )
  AND TRUNC (sysdate)      = TRUNC (xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from)
  AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
  AND NOT EXISTS
    (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
      'Y'
    FROM gl_code_combinations_kfv gcc
    WHERE gcc.code_combination_id = msi.cost_of_sales_account
    AND gcc.segment4              = '646080'
    )
  UNION
  -- for rental  orders before invoice
  SELECT
    /*+ INDEX(ol XXWC_OE_ORDER_LN_AL_N13))*/
    ship_from_org.organization_code warehouse,
    oth.name order_type,
    TRUNC (oh.ordered_date) ordered_date,
    ol.flow_status_code line_flow_status_code,
    oh.order_number,
    oh.attribute7 created_by,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, 'ACCOUNTNUM') customer_number,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, NULL) customer_name,
    CASE
      WHEN mcvc.segment2 = 'PRMO'
      THEN (                    -1 * ol.unit_selling_price)
      WHEN ol.flow_status_code IN ('CLOSED', 'INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost (ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
    END average_cost,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_salesrep_details(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
    CASE
      WHEN ol.flow_status_code IN ('CLOSED', 'INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0)
    END) * (DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
    msi.segment1 item,
    CASE
      WHEN msi.segment1 = 'Rental Charge'
      THEN ol.user_item_description
      ELSE msi.description
    END item_desc,
    DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
    DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price) unit_selling_price,
    otl.name line_type,
    ship_from_org.attribute8 district,
    hou.name branch_name,
    NVL(
    (SELECT NVL(adj2.description,'MKT')
    FROM
      (SELECT vs.description,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'MKT') price_source_type,
    NVL(
    (SELECT
      CASE
        WHEN adj2.price_typ_automatic_flag = 'N'
        THEN 'MANUAL'
        WHEN adj2.attribute10 = 'Contract Pricing'
        OR name LIKE 'CSP%'
        THEN 'CONTRACT'
        WHEN adj2.attribute10 = 'Vendor Quote'
        THEN 'VQN'
        ELSE 'SYSTEM'
      END price_type
    FROM
      (SELECT lh.attribute10,
        lh.name,
        last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following)price_typ_automatic_flag,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'SYSTEM') price_type,
    NVL(
    (SELECT
      CASE
        WHEN NVL (adj2.aut_flag_for_selling_price, 'N') = 'N'
        THEN ol.unit_list_price
        WHEN adj2.arithmetic_operator = 'NEWPRICE'
        THEN ROUND (adj2.operand,4)
        WHEN adj2.arithmetic_operator = '%'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price * (100 - adj2.operand) / 100
        WHEN adj2.arithmetic_operator = 'AMT'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price - adj2.operand
        ELSE ol.unit_list_price
      END overridden_sell_price
    FROM
      (SELECT last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) aut_flag_for_selling_price,
        last_value(adj.arithmetic_operator) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) arithmetic_operator,
        last_value(adj.operand) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) operand,
        last_value(adj.list_line_type_code) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) list_line_type_code,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),ol.unit_list_price) system_price,
    xoal.last_price_paid sxe_last_price_paid
    --descr#flexfield#start
    --descr#flexfield#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_parameters ship_from_org ,
    hr_all_organization_units hou,
    oe_transaction_types_vl oth,
    oe_transaction_types_vl otl,
    xxwc.xxwc_ahh_oe_ais_lpp_tbl xoal,
    mtl_system_items_kfv msi,
    mtl_item_categories micc,
    mtl_categories_kfv mcvc
  WHERE oh.header_id                = ol.header_id
  AND ol.ship_from_org_id           = ship_from_org.organization_id(+)
  AND ship_from_org.organization_id = hou.organization_id(+)
  AND oh.order_type_id              = oth.transaction_type_id
  AND oh.org_id                     = oth.org_id
  AND ol.line_type_id               = otl.transaction_type_id
  AND ol.org_id                     = otl.org_id
  AND ol.inventory_item_id          = xoal.inventory_item_id(+)
  AND ol.ship_to_org_id             = xoal.ship_to_org_id(+)
  AND ol.inventory_item_id          = msi.inventory_item_id(+)
  AND ol.ship_from_org_id           = msi.organization_id(+)
  AND msi.inventory_item_id         = micc.inventory_item_id(+)
  AND msi.organization_id           = micc.organization_id(+)
  AND micc.category_id              = mcvc.category_id(+)
  AND mcvc.structure_id(+)          = 101
  AND micc.category_set_id(+)       = 1100000062
  AND ol.flow_status_code          <> 'CANCELLED'
  AND ol.ordered_item NOT          IN ('CONTOFFSET', 'CONTBILL')
  AND oth.name                     != 'INTERNAL ORDER'
  AND ol.line_type_id NOT          IN (1015, 1007)
  AND ( ( oth.name                 IN ('WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL')
  AND ol.flow_status_code          IN ('INVOICE_HOLD', 'CLOSED')
  AND TRUNC (oh.creation_date)      = TRUNC (sysdate) )
  OR ( oth.name                    IN ('WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL')
  AND ol.flow_status_code NOT      IN ('CLOSED', 'CANCELLED')
  AND ol.flow_status_code          IN ('INVOICE_HOLD')
  AND TRUNC (sysdate)               = TRUNC (xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from)))
  AND ol.source_type_code          IN ('INTERNAL', 'EXTERNAL')
  AND NOT EXISTS
    (SELECT  /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
      'Y'
    FROM gl_code_combinations_kfv gcc
    WHERE 1                     = 1
    AND gcc.code_combination_id = msi.cost_of_sales_account
    AND gcc.segment4            = '646080'
    )
  UNION
  SELECT mp.organization_code warehouse,
    oth.name order_type,
    TRUNC (oh.ordered_date) ordered_date,
    NULL line_flow_status_code,
    oh.order_number,
    oh.attribute7 created_by,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( rct.bill_to_customer_id, 'ACCOUNTNUM') customer_number,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( rct.bill_to_customer_id, NULL) customer_name,
    NVL ( xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost ( msi.inventory_item_id, msi.organization_id), 0) average_cost,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_salesrep_details(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,
    NVL ( NVL (rctl.quantity_invoiced, rctl.quantity_credited) * NVL (rctl.unit_selling_price, 0), 0) sales,
    0 sale_cost,
    msi.segment1 item,
    msi.description item_desc,
    NVL (rctl.quantity_invoiced, rctl.quantity_credited) qty,
    rctl.unit_selling_price unit_selling_price,
    NULL line_type,
    mp.attribute8 district,
    hou.name branch_name,
    NULL price_source_type,
    NULL price_type,
    to_number(NULL) system_price,
    to_number(NULL) sxe_last_price_paid
    --descr#flexfield#start
    --descr#flexfield#end
  FROM ra_customer_trx rct,
    ra_customer_trx_lines rctl,
    oe_order_headers oh,
    oe_transaction_types_vl oth,
    mtl_parameters mp,
    hr_all_organization_units hou,
    mtl_system_items_kfv msi
  WHERE rct.customer_trx_id          = rctl.customer_trx_id
  AND rctl.interface_line_attribute1 = TO_CHAR (oh.order_number)
  AND rctl.interface_line_context    = 'ORDER ENTRY'
  AND rctl.creation_date            >= xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from + 0.25
  AND rctl.creation_date            <= xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from + 1.25
  AND oh.order_type_id               = oth.transaction_type_id
  AND oh.org_id                      = oth.org_id
  AND oh.ship_from_org_id            = mp.organization_id
  AND mp.organization_id             = hou.organization_id
  AND rct.interface_header_context   = 'ORDER ENTRY'
  AND rctl.interface_line_attribute2 = oth.name
  AND rctl.description               = 'Delivery Charge'
  AND rctl.line_type                 = 'LINE'
  AND mp.organization_id             = msi.organization_id
  AND rctl.inventory_item_id         = msi.inventory_item_id
  AND NOT EXISTS
    (SELECT 1
    FROM oe_order_lines_all
    WHERE line_id =rctl.interface_line_attribute6
    )
  UNION
  SELECT ship_from_org.organization_code warehouse,
    oth.name order_type,
    TRUNC (oh.ordered_date) ordered_date,
    ol.flow_status_code line_flow_status_code,
    oh.order_number,
    oh.attribute7 created_by,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, 'ACCOUNTNUM') customer_number,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_customer_number ( ol.sold_to_org_id, NULL) customer_name,
    CASE
      WHEN mcvc.segment2 = 'PRMO'
      THEN (                    -1 * ol.unit_selling_price)
      WHEN ol.flow_status_code IN ('CLOSED', 'INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
    END average_cost,
    xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_salesrep_details(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
    (NVL ( DECODE ( mcvc.segment2, 'PRMO',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
    CASE
      WHEN ol.flow_status_code IN ('CLOSED', 'INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE')
      THEN xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_order_line_cost ( ol.line_id)
      WHEN otl.name = 'BILL ONLY'
      THEN 0
      ELSE NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0)
    END) * (DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
    msi.segment1 item,
    CASE
      WHEN msi.segment1 = 'Rental Charge'
      THEN ol.user_item_description
      ELSE msi.description
    END item_desc,
    DECODE (ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
    DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price) unit_selling_price,
    otl.name line_type,
    ship_from_org.attribute8 district,
    hou.name branch_name,
    NVL(
    (SELECT NVL(adj2.description,'MKT')
    FROM
      (SELECT vs.description,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'MKT') price_source_type,
    NVL(
    (SELECT
      CASE
        WHEN adj2.price_typ_automatic_flag = 'N'
        THEN 'MANUAL'
        WHEN adj2.attribute10 = 'Contract Pricing'
        OR name LIKE 'CSP%'
        THEN 'CONTRACT'
        WHEN adj2.attribute10 = 'Vendor Quote'
        THEN 'VQN'
        ELSE 'SYSTEM'
      END price_type
    FROM
      (SELECT lh.attribute10,
        lh.name,
        last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following)price_typ_automatic_flag,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),'SYSTEM') price_type,
    NVL(
    (SELECT
      CASE
        WHEN NVL (adj2.aut_flag_for_selling_price, 'N') = 'N'
        THEN ol.unit_list_price
        WHEN adj2.arithmetic_operator = 'NEWPRICE'
        THEN ROUND (adj2.operand,4)
        WHEN adj2.arithmetic_operator = '%'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price * (100 - adj2.operand) / 100
        WHEN adj2.arithmetic_operator = 'AMT'
        AND adj2.list_line_type_code  = 'DIS'
        THEN ol.unit_list_price - adj2.operand
        ELSE ol.unit_list_price
      END overridden_sell_price
    FROM
      (SELECT last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) aut_flag_for_selling_price,
        last_value(adj.arithmetic_operator) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) arithmetic_operator,
        last_value(adj.operand) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) operand,
        last_value(adj.list_line_type_code) over (partition BY adj.header_id, adj.line_id order by adj.automatic_flag ,adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) list_line_type_code,
        row_number() over (partition BY adj.header_id,adj.line_id order by adj.last_update_date) rn
      FROM apps.oe_price_adjustments_v adj,
        (SELECT attribute10,
          name,
          list_header_id
        FROM apps.qp_secu_list_headers_vl lh
        WHERE context     =162
        AND automatic_flag='Y'
        )lh ,
        (SELECT NVL(ffv.description,'MKT') description,
          flex_value
        FROM apps.fnd_flex_values_vl ffv
        WHERE flex_value_set_id=1015252
        AND enabled_flag       ='Y'
        ) vs
      WHERE 1                     =1
      AND adj.header_id           = ol.header_id
      AND adj.line_id             = ol.line_id
      AND adj.list_header_id      = lh.list_header_id(+)
      AND lh.attribute10          = vs.flex_value(+)
      AND adj.applied_flag        ='Y'
      AND adj.list_line_type_code<>'FREIGHT_CHARGE'
      )adj2
    WHERE adj2.rn =1
    AND rownum    =1
    ),ol.unit_list_price) system_price,
    xoal.last_price_paid sxe_last_price_paid
    --descr#flexfield#start
    --descr#flexfield#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_parameters ship_from_org,
    hr_all_organization_units hou,
    oe_transaction_types_vl oth,
    oe_transaction_types_vl otl,
    xxwc.xxwc_ahh_oe_ais_lpp_tbl xoal,
    mtl_system_items_kfv msi,
    mtl_categories_kfv mcvc,
    mtl_item_categories micc
  WHERE oh.header_id                = ol.header_id
  AND ol.ship_from_org_id           = ship_from_org.organization_id(+)
  AND ship_from_org.organization_id = hou.organization_id(+)
  AND oh.order_type_id              = oth.transaction_type_id
  AND oh.org_id                     = oth.org_id
  AND ol.line_type_id               = otl.transaction_type_id
  AND ol.org_id                     = otl.org_id
  AND ol.inventory_item_id          = xoal.inventory_item_id(+)
  AND ol.ship_to_org_id             = xoal.ship_to_org_id(+)
  AND ol.inventory_item_id          = msi.inventory_item_id(+)
  AND msi.organization_id(+)        = ol.ship_from_org_id
  AND msi.inventory_item_id         = micc.inventory_item_id(+)
  AND msi.organization_id           = micc.organization_id(+)
  AND micc.category_id              = mcvc.category_id(+)
  AND mcvc.structure_id(+)          = 101
  AND micc.category_set_id(+)       = 1100000062
  AND ol.ordered_item NOT          IN ('CONTOFFSET', 'CONTBILL')
  AND ol.flow_status_code NOT      IN ('CLOSED', 'CANCELLED')
  AND oth.name                     != 'INTERNAL ORDER'
  AND oth.name                      = 'STANDARD ORDER'
  AND ol.user_item_description      = 'OUT_FOR_DELIVERY'
  AND ol.flow_status_code           = 'AWAITING_SHIPPING'
  AND TRUNC(sysdate)                = TRUNC(xxeis.eis_xxwc_inv_pre_reg_rpt_pkg.get_date_from)
  AND ol.source_type_code          IN ('INTERNAL', 'EXTERNAL')
  AND NOT EXISTS
    (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
      'Y'
    FROM gl_code_combinations_kfv gcc
    WHERE gcc.code_combination_id = msi.cost_of_sales_account
    AND gcc.segment4              = '646080'
    )
/
