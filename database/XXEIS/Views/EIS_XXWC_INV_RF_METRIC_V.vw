---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_RF_METRIC_V $
  Module Name : Inventory
  PURPOSE	  : RF Metrics Report
  TMS Task Id : 20160712-00024
  REVISIONS   :
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     2-Aug-2016       		Siva   		 TMS#20160712-00024 
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_INV_RF_METRIC_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_RF_METRIC_V (PROCESS_ID,QUERY_TYPE, REC_TRANSACTION_EMPLOYEE, REC_ORG, REC_TRANSACTION_TYPE_NAME, REC_REASON_NAME, REC_DESCRIPTION, REC_DOC_NUM, REC_TOTAL_LINES, REC_RCVD_DATE, REC_VENDOR_NUM, REC_VENDOR, REC_SUBINVENTORY_CODE, CC_ORG, CC_SUB_INV, CC_ITEM_NUM, CC_TRANSACTION_EMPLOYEE, CC_COUNT_DATE_CURRENT, BC_TRANSACTION_EMPLOYEE, BC_ORG, BC_SUB_INV, BC_BIN, BC_CREATED_VIA, BC_BIN_CREATE_DATE, BA_TRANSACTION_EMPLOYEE, BA_ORG, BA_ITEM_NUM, BA_SUB_INV, BA_BIN, BA_CREATED_VIA, BA_BIN_MAINT_DATE, AB_TRANSACTION_EMPLOYEE, AB_ITEM_NUM, AB_DESCRIPTION, AB_XREF_TYPE, AB_XREF_VALUE, AB_XREF_LENGTH, AB_XREF_CREATE_DATE, DAYS)
AS
  SELECT PROCESS_ID,
    QUERY_TYPE,
    REC_TRANSACTION_EMPLOYEE,
    REC_ORG,
    REC_TRANSACTION_TYPE_NAME,
    REC_REASON_NAME,
    REC_DESCRIPTION,
    REC_DOC_NUM,
    REC_TOTAL_LINES,
    REC_RCVD_DATE,
    REC_VENDOR_NUM,
    REC_VENDOR,
    REC_SUBINVENTORY_CODE,
    CC_ORG,
    CC_SUB_INV,
    CC_ITEM_NUM,
    CC_TRANSACTION_EMPLOYEE,
    CC_COUNT_DATE_CURRENT,
    BC_TRANSACTION_EMPLOYEE,
    BC_ORG,
    BC_SUB_INV,
    BC_BIN,
    BC_CREATED_VIA,
    BC_BIN_CREATE_DATE,
    BA_TRANSACTION_EMPLOYEE,
    BA_ORG,
    BA_ITEM_NUM,
    BA_SUB_INV,
    BA_BIN,
    BA_CREATED_VIA,
    BA_BIN_MAINT_DATE,
    AB_TRANSACTION_EMPLOYEE,
    AB_ITEM_NUM,
    AB_DESCRIPTION,
    AB_XREF_TYPE,
    AB_XREF_VALUE,
    AB_XREF_LENGTH,
    AB_XREF_CREATE_DATE,
    DAYS
  FROM XXEIS.XXWC_INV_RF_METRIC_RPT_TBL
/
