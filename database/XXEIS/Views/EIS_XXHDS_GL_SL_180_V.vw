CREATE OR REPLACE VIEW XXEIS.EIS_XXHDS_GL_SL_180_V (SOURCE, JE_CATEGORY, JE_HEADER_ID, JE_LINE_NUM, ACC_DATE, ENTRY, BATCH, LINE_DESCR, LSEQUENCE, HNUMBER, LLINE, LINE_ACCTD_DR, LINE_ACCTD_CR, LINE_ENT_DR, LINE_ENT_CR, SEQ_ID, SEQ_NUM, H_SEQ_ID, GL_SL_LINK_ID, CUSTOMER_OR_VENDOR, ENTERED_DR, ENTERED_CR, ACCOUNTED_DR, ACCOUNTED_CR, SLA_LINE_ACCOUNTED_DR, SLA_LINE_ACCOUNTED_CR, SLA_LINE_ACCOUNTED_NET, SLA_LINE_ENTERED_DR, SLA_LINE_ENTERED_CR, SLA_LINE_ENTERED_NET, SLA_DIST_ENTERED_CR, SLA_DIST_ENTERED_DR, SLA_DIST_ENTERED_NET, SLA_DIST_ACCOUNTED_CR, SLA_DIST_ACCOUNTED_DR, SLA_DIST_ACCOUNTED_NET, ASSOCIATE_NUM, TRANSACTION_NUM, GL_ACCOUNT_STRING, CURRENCY_CODE, PERIOD_NAME, TYPE, EFFECTIVE_PERIOD_NUM, NAME, BATCH_NAME, SEGMENT1, SEGMENT2, SEGMENT3, SEGMENT4, SEGMENT5, CODE_COMBINATION_ID, GCC#BRANCH, GCC#50328#ACCOUNT, GCC#50328#ACCOUNT#DESCR, GCC#50328#COST_CENTER, GCC#50328#COST_CENTER#DESCR, GCC#50328#FURTURE_USE, GCC#50328#FURTURE_USE#DESCR, GCC#50328#FUTURE_USE_2,
GCC#50328#FUTURE_USE_2#DESCR, GCC#50328#LOCATION, GCC#50328#LOCATION#DESCR, GCC#50328#PRODUCT, GCC#50328#PRODUCT#DESCR, GCC#50328#PROJECT_CODE, GCC#50328#PROJECT_CODE#DESCR, GCC#50368#ACCOUNT, GCC#50368#ACCOUNT#DESCR, GCC#50368#DEPARTMENT, GCC#50368#DEPARTMENT#DESCR, GCC#50368#DIVISION, GCC#50368#DIVISION#DESCR, GCC#50368#FUTURE_USE, GCC#50368#FUTURE_USE#DESCR, GCC#50368#PRODUCT, GCC#50368#PRODUCT#DESCR, GCC#50368#SUBACCOUNT, GCC#50368#SUBACCOUNT#DESCR)
AS 
  (SELECT   /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1)*/  --TMS#20160418-00109  by Siva  on 04-16-2016
    jh.je_source source,
    jh.je_category,
    jl.je_header_id je_header_id,
    jl.je_line_num je_line_num,
    jl.effective_date acc_date,
    jh.name entry,
    jh.je_batch_id batch,
    SUBSTR (jl.description, 1, 240) line_descr,
    NULL lsequence,
    jh.doc_sequence_value hnumber,
    jl.je_line_num lline,
    jl.accounted_dr line_acctd_dr,
    jl.accounted_cr line_acctd_cr,
    jl.entered_dr line_ent_dr,
    jl.entered_cr line_ent_cr,
    NULL seq_id,
    NULL seq_num,
    jh.doc_sequence_id h_seq_id,
    NULL gl_sl_link_id,
    NULL customer_or_vendor,
    jl.entered_dr entered_dr,
    jl.entered_cr entered_cr,
    jl.accounted_cr accounted_dr,
    jl.accounted_dr accounted_cr,
    jl.accounted_dr sla_line_accounted_dr,
    jl.accounted_cr sla_line_accounted_cr,
    NVL (jl.accounted_dr, 0) - NVL (jl.accounted_cr, 0) sla_line_accounted_net,
    jl.entered_dr sla_line_entered_dr,
    jl.entered_cr sla_line_entered_cr,
    NVL (jl.entered_dr, 0) - NVL (jl.entered_cr, 0) sla_line_entered_net,
    jl.entered_cr sla_dist_entered_cr,
    jl.entered_dr sla_dist_entered_dr,
    NVL (jl.entered_cr, 0) - NVL (jl.entered_dr, 0) sla_dist_entered_net,
    jl.accounted_cr sla_dist_accounted_cr,
    jl.accounted_dr sla_dist_accounted_dr,
    NVL (jl.accounted_cr, 0) - NVL (jl.accounted_dr, 0) sla_dist_accounted_net,
    '' associate_num,
    NULL transaction_num,
    gcc.concatenated_segments gl_account_string,
    jh.currency_code,
    jh.period_name,
    NULL type,
    gps.effective_period_num,
    gle.name,
    gb.name batch_name,
    gcc.segment1,
    gcc.segment2,
    gcc.segment3,
    gcc.segment4,
    gcc.segment5,
    gcc.code_combination_id
    --descr#flexfield#start
    ,
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I') gcc#branch 
	--descr#flexfield#end
    --gl#accountff#start
    ,
    gcc.segment4 gcc#50328#account,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ACCOUNT') gcc#50328#account#descr,
    gcc.segment3 gcc#50328#cost_center,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_COSTCENTER' ) gcc#50328#cost_center#descr,
    gcc.segment6 gcc#50328#furture_use,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_FUTURE_USE1') gcc#50328#furture_use#descr,
    gcc.segment7 gcc#50328#future_use_2,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc#50328#future_use_2#descr,
    gcc.segment2 gcc#50328#location,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_LOCATION') gcc#50328#location#descr,
    gcc.segment1 gcc#50328#product,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_PRODUCT') gcc#50328#product#descr,
    gcc.segment5 gcc#50328#project_code,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_PROJECT') gcc#50328#project_code#descr,
    gcc.segment4 gcc#50368#account,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc#50368#account#descr,
    gcc.segment3 gcc#50368#department,
    xxeis.eis_rs_fin_utility.decode_vset ( gcc.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc#50368#department#descr,
    gcc.segment2 gcc#50368#division,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc#50368#division#descr,
    gcc.segment6 gcc#50368#future_use,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc#50368#future_use#descr,
    gcc.segment1 gcc#50368#product,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc#50368#product#descr,
    gcc.segment5 gcc#50368#subaccount,
    xxeis.eis_rs_fin_utility.decode_vset ( gcc.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT') gcc#50368#subaccount#descr
    --gl#accountff#end
  FROM gl_je_lines jl,
    gl_je_headers jh,
    gl_je_batches gb,
    -- gl_import_references ir,
    --  FND_DOCUMENT_SEQUENCES  ds,
    gl_code_combinations_kfv gcc,
    gl_ledgers gle,
    gl_period_statuses gps
  WHERE jl.status     = 'P'
  AND jh.status       = 'P'
  AND jh.actual_flag  = 'A'
  AND jh.je_header_id = jl.je_header_id
    -- AND ir.je_header_id(+) = jl.je_header_id
    -- AND ir.je_line_num(+) = jl.je_line_num
    --  and ds.doc_sequence_id (+) = ir.subledger_doc_sequence_id
  AND jh.je_source NOT      IN ('Payables', 'Receivables')
  AND jh.je_batch_id         = gb.je_batch_id
  AND jl.code_combination_id = gcc.code_combination_id
  AND jh.ledger_id           = gle.ledger_id --bala
    --    AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id  --TMS#20160418-00109  by Siva  removed on 04-16-2016
  AND gps.application_id = 101
  AND gps.period_name    = jh.period_name
  AND gps.ledger_id      = gle.ledger_id
  UNION ALL
  SELECT  /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1)*/   --TMS#20160418-00109  by Siva  on 04-16-2016
    jh.je_source source,
    jh.je_category,
    jir.je_header_id je_header_id,
    jir.je_line_num je_line_num,
    jl.effective_date acc_date,
    jh.name entry,
    jh.je_batch_id batch,
    SUBSTR (jl.description, 1, 240) line_descr,
    NULL lsequence,
    jh.doc_sequence_value hnumber,
    jl.je_line_num lline,
    jl.accounted_dr line_acctd_dr,
    jl.accounted_cr line_acctd_cr,
    jl.entered_dr line_ent_dr,
    jl.entered_cr line_ent_cr,
    jir.subledger_doc_sequence_id seq_id,
    jir.subledger_doc_sequence_value seq_num,
    jh.doc_sequence_id h_seq_id,
    jir.gl_sl_link_id gl_sl_link_id,
    (SELECT cu.customer_name
    FROM ra_customers cu
    WHERE cu.customer_id = to_number (jir.reference_7)
    ) customer_or_vendor,
    DECODE ( ra_dist.cust_trx_line_gl_dist_id, NULL, dist.amount_dr, to_number ( DECODE ( ra_dist.account_class, 'REC', DECODE (SIGN (NVL ( ra_dist.amount, 0)),       -1, NULL, NVL (ra_dist.amount, 0)), DECODE (SIGN ( NVL (ra_dist.amount, 0)), -1, -NVL (ra_dist.amount, 0), NULL)))) entered_dr,
    DECODE ( ra_dist.cust_trx_line_gl_dist_id, NULL, dist.amount_cr, to_number ( DECODE ( ra_dist.account_class, 'REC', DECODE (SIGN (NVL ( ra_dist.amount, 0)),       -1, -NVL (ra_dist.amount, 0), NULL), DECODE (SIGN ( NVL (ra_dist.amount, 0)), -1, NULL, NVL (ra_dist.amount, 0))))) entered_cr,
    DECODE ( ra_dist.cust_trx_line_gl_dist_id, NULL, dist.acctd_amount_dr, to_number ( DECODE ( ra_dist.account_class, 'REC', DECODE (SIGN (NVL ( ra_dist.amount, 0)), -1, NULL, NVL (ra_dist.acctd_amount, 0)), DECODE ( SIGN (NVL (ra_dist.amount, 0)), -1, -NVL (ra_dist.acctd_amount, 0), NULL) ))) accounted_dr,
    DECODE ( ra_dist.cust_trx_line_gl_dist_id, NULL, dist.acctd_amount_cr, to_number ( DECODE ( ra_dist.account_class, 'REC', DECODE (SIGN (NVL ( ra_dist.amount, 0)), -1, -NVL (ra_dist.acctd_amount, 0), NULL), DECODE ( SIGN (NVL (ra_dist.amount, 0)), -1, NULL, NVL (ra_dist.acctd_amount, 0))) )) accounted_cr,
    ael.unrounded_accounted_dr sla_line_accounted_dr,
    ael.unrounded_accounted_cr sla_line_accounted_cr,
    NVL (ael.unrounded_accounted_dr, 0) - NVL (ael.unrounded_accounted_cr, 0) sla_line_accounted_net,
    ael.unrounded_entered_dr sla_line_entered_dr,
    ael.unrounded_entered_cr sla_line_entered_cr,
    NVL (ael.unrounded_entered_dr, 0) - NVL (ael.unrounded_entered_cr, 0) sla_line_entered_net,
    xdl.unrounded_entered_cr sla_dist_entered_cr,
    xdl.unrounded_entered_dr sla_dist_entered_dr,
    NVL (xdl.unrounded_entered_cr, 0) - NVL (xdl.unrounded_entered_dr, 0) sla_dist_entered_net,
    xdl.unrounded_accounted_cr sla_dist_accounted_cr,
    xdl.unrounded_accounted_dr sla_dist_accounted_dr,
    NVL (xdl.unrounded_accounted_cr, 0) - NVL (xdl.unrounded_accounted_dr, 0) sla_dist_accounted_net,
    DECODE ( jir.reference_10, 'AR_ADJUSTMENTS', jir.reference_4, 'AR_RECEIVABLE_APPLICATIONS', DECODE (jir.reference_8, 'TRADE', NULL, jir.reference_5)) associate_num,
    DECODE ( jir.reference_10, 'AR_ADJUSTMENTS', jir.reference_5, 'AR_CASH_RECEIPT_HISTORY', jir.reference_4, 'AR_RECEIVABLE_APPLICATIONS', DECODE (jir.reference_8, 'CMAPP', jir.reference_4, 'TRADE', jir.reference_4, NULL), jir.reference_4) transaction_num,
    gcc.concatenated_segments gl_account_string,
    jh.currency_code,
    jh.period_name,
    NVL (dist.source_type, NULL) type,
    gps.effective_period_num,
    gle.name,
    gb.name batch_name,
    gcc.segment1,
    gcc.segment2,
    gcc.segment3,
    gcc.segment4,
    gcc.segment5,
    gcc.code_combination_id
    --descr#flexfield#start
    ,
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I') gcc#branch 
	--descr#flexfield#end
    --gl#accountff#start
    ,
    gcc.segment4 gcc#50328#account,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ACCOUNT') gcc#50328#account#descr,
    gcc.segment3 gcc#50328#cost_center,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_COSTCENTER' ) gcc#50328#cost_center#descr,
    gcc.segment6 gcc#50328#furture_use,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_FUTURE_USE1') gcc#50328#furture_use#descr,
    gcc.segment7 gcc#50328#future_use_2,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc#50328#future_use_2#descr,
    gcc.segment2 gcc#50328#location,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_LOCATION') gcc#50328#location#descr,
    gcc.segment1 gcc#50328#product,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_PRODUCT') gcc#50328#product#descr,
    gcc.segment5 gcc#50328#project_code,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_PROJECT') gcc#50328#project_code#descr,
    gcc.segment4 gcc#50368#account,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc#50368#account#descr,
    gcc.segment3 gcc#50368#department,
    xxeis.eis_rs_fin_utility.decode_vset ( gcc.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc#50368#department#descr,
    gcc.segment2 gcc#50368#division,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc#50368#division#descr,
    gcc.segment6 gcc#50368#future_use,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc#50368#future_use#descr,
    gcc.segment1 gcc#50368#product,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc#50368#product#descr,
    gcc.segment5 gcc#50368#subaccount,
    xxeis.eis_rs_fin_utility.decode_vset ( gcc.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT') gcc#50368#subaccount#descr
    --gl#accountff#end
  FROM gl_je_headers jh,
    gl_je_lines jl,
    gl_ledgers gle,
    gl_period_statuses gps,
    gl_je_batches gb,
    gl_code_combinations_kfv gcc,
    gl_import_references jir,
    xla_ae_lines ael,
    xla_ae_headers aeh,
    xla_distribution_links xdl,
    ar_distributions_all dist,
    ra_cust_trx_line_gl_dist_all ra_dist
  WHERE jh.je_header_id                   = jl.je_header_id
  AND jh.je_source                        = 'Receivables'
  AND jh.ledger_id                        = gle.ledger_id
  AND gps.application_id                  = 101
  AND gps.period_name                     = jh.period_name
  AND gps.ledger_id                       = gle.ledger_id
  AND jh.je_batch_id                      = gb.je_batch_id
  AND jl.code_combination_id              = gcc.code_combination_id
  AND jh.je_header_id                     = jir.je_header_id
  AND jl.je_line_num                      = jir.je_line_num
  AND jir.gl_sl_link_id                   = ael.gl_sl_link_id(+)
  AND jir.gl_sl_link_table                = ael.gl_sl_link_table(+)
  AND ael.application_id(+)               = 222					--TMS#20160418-00109  by Siva  on 04-16-2016
  AND ael.ae_header_id                    = aeh.ae_header_id(+)
  AND ael.application_id                  = aeh.application_id(+) --TMS#20160418-00109  by Siva  on 04-16-2016
  AND ael.ae_line_num                     = xdl.ae_line_num(+)
  AND ael.ae_header_id                    = xdl.ae_header_id(+)
  AND ael.application_id                  = xdl.application_id(+)  --TMS#20160418-00109  by Siva  on 04-16-2016
  AND dist.line_id(+)                     = DECODE ( xdl.source_distribution_type, 'AR_DISTRIBUTIONS_ALL', xdl.source_distribution_id_num_1, NULL)
  AND ra_dist.cust_trx_line_gl_dist_id(+) = DECODE ( xdl.source_distribution_type, 'AR_DISTRIBUTIONS_ALL', NULL, xdl.source_distribution_id_num_1)
  UNION ALL
  SELECT  /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1)*/   --TMS#20160418-00109  by Siva  on 04-16-2016
    jh.je_source source,
    jh.je_category,
    gir.je_header_id je_header_id,
    gir.je_line_num je_line_num,
    jl.effective_date acc_date,
    jh.name entry,
    jh.je_batch_id batch,
    SUBSTR (jl.description, 1, 240) line_descr,
    ds.name lsequence,
    jh.doc_sequence_value hnumber,
    jl.je_line_num lline,
    jl.accounted_dr line_acctd_dr,
    jl.accounted_cr line_acctd_cr,
    jl.entered_dr ent_dr,
    jl.entered_cr ent_cr,
    gir.subledger_doc_sequence_id seq_id,
    gir.subledger_doc_sequence_value seq_num,
    jh.doc_sequence_id h_seq_id,
    gir.gl_sl_link_id gl_sl_link_id,
    pov.vendor_name customer_or_vendor,
    jl.entered_dr entered_dr,
    jl.entered_cr entered_cr,
    jl.accounted_cr accounted_dr,
    jl.accounted_dr accounted_cr,
    ael.unrounded_accounted_dr sla_line_accounted_dr,
    ael.unrounded_accounted_cr sla_line_accounted_cr,
    NVL (ael.unrounded_accounted_dr, 0) - NVL (ael.unrounded_accounted_cr, 0) sla_line_accounted_net,
    ael.unrounded_entered_dr sla_line_entered_dr,
    ael.unrounded_entered_cr sla_line_entered_cr,
    NVL (ael.unrounded_entered_dr, 0) - NVL (ael.unrounded_entered_cr, 0) sla_line_entered_net,
    adl.unrounded_entered_cr sla_dist_entered_cr,
    adl.unrounded_entered_dr sla_dist_entered_dr,
    NVL (adl.unrounded_entered_cr, 0) - NVL (adl.unrounded_entered_dr, 0) sla_dist_entered_net,
    adl.unrounded_accounted_cr sla_dist_accounted_cr,
    adl.unrounded_accounted_dr sla_dist_accounted_dr,
    NVL (adl.unrounded_accounted_cr, 0) - NVL (adl.unrounded_accounted_dr, 0) sla_dist_accounted_net,
    '' associate_num,
    ai.invoice_num transaction_mum,
    gcc.concatenated_segments gl_account_string,
    jh.currency_code,
    jh.period_name,
    ael.accounting_class_code type,
    gps.effective_period_num,
    le.name,
    gb.name batch_name,
    gcc.segment1,
    gcc.segment2,
    gcc.segment3,
    gcc.segment4,
    gcc.segment5,
    gcc.code_combination_id
    --descr#flexfield#start
    ,
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I') gcc#branch
	--descr#flexfield#end
    --gl#accountff#start
    ,
    gcc.segment4 gcc#50328#account,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ACCOUNT') gcc#50328#account#descr,
    gcc.segment3 gcc#50328#cost_center,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_COSTCENTER' ) gcc#50328#cost_center#descr,
    gcc.segment6 gcc#50328#furture_use,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_FUTURE_USE1') gcc#50328#furture_use#descr,
    gcc.segment7 gcc#50328#future_use_2,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc#50328#future_use_2#descr,
    gcc.segment2 gcc#50328#location,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_LOCATION') gcc#50328#location#descr,
    gcc.segment1 gcc#50328#product,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_PRODUCT') gcc#50328#product#descr,
    gcc.segment5 gcc#50328#project_code,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_PROJECT') gcc#50328#project_code#descr,
    gcc.segment4 gcc#50368#account,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc#50368#account#descr,
    gcc.segment3 gcc#50368#department,
    xxeis.eis_rs_fin_utility.decode_vset ( gcc.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc#50368#department#descr,
    gcc.segment2 gcc#50368#division,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc#50368#division#descr,
    gcc.segment6 gcc#50368#future_use,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc#50368#future_use#descr,
    gcc.segment1 gcc#50368#product,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc#50368#product#descr,
    gcc.segment5 gcc#50368#subaccount,
    xxeis.eis_rs_fin_utility.decode_vset ( gcc.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT') gcc#50368#subaccount#descr
    --gl#accountff#end
  FROM gl_je_headers jh,
    gl_je_lines jl,
    gl_je_batches gb,
    gl_ledgers le,
    gl_period_statuses gps,
    gl_import_references gir,
    gl_code_combinations_kfv gcc,
    xla_ae_lines ael,
    gl_code_combinations_kfv gcc1,
    xla_ae_headers aeh,
    xla_distribution_links adl,
    ap_invoices_all ai,
    ap_invoice_distributions_all aid,
    --  AP_CHECKS_ALL AC,
    -- AP_INVOICE_PAYMENTS_ALL AIP,
    po_vendors pov,
    fnd_document_sequences ds
  WHERE jh.je_header_id                = jl.je_header_id
  AND jh.je_source                     = 'Payables'
  AND jh.je_batch_id                   = gb.je_batch_id
  AND jh.ledger_id                     = le.ledger_id
  AND le.object_type_code              = 'L'
  AND gps.application_id               = 101
  AND gps.period_name                  = jh.period_name
  AND gps.ledger_id                    = le.ledger_id
  AND jh.je_header_id                  = gir.je_header_id
  AND jl.je_line_num                   = gir.je_line_num
  AND jl.code_combination_id           = gcc.code_combination_id
  AND gir.gl_sl_link_id                = ael.gl_sl_link_id(+)
  AND gir.gl_sl_link_table             = ael.gl_sl_link_table(+)
  AND ael.application_id (+)           = 200  				--TMS#20160418-00109  by Siva  on 04-16-2016
  AND ael.ae_header_id                 = aeh.ae_header_id(+)
  AND ael.application_id               = aeh.application_id(+) --TMS#20160418-00109  by Siva  on 04-16-2016
  AND ael.code_combination_id          = gcc1.code_combination_id(+)
  AND ael.ae_header_id                 = adl.ae_header_id(+)
  AND ael.ae_line_num                  = adl.ae_line_num(+)
  AND ael.application_id               = adl.application_id(+) --TMS#20160418-00109  by Siva  on 04-16-2016
  AND adl.source_distribution_type     = 'AP_INV_DIST'
  AND adl.source_distribution_id_num_1 = aid.invoice_distribution_id
    -- AND aip.invoice_id(+)=AID.INVOICE_DISTRIBUTION_ID
    -- AND AIP.CHECK_ID=AC.CHECK_ID(+)
  AND aid.invoice_id                = ai.invoice_id
  AND ai.vendor_id                  = pov.vendor_id
  AND gir.subledger_doc_sequence_id = ds.doc_sequence_id(+)
  UNION ALL
  SELECT  /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1)*/   --TMS#20160418-00109  by Siva  on 04-16-2016
    jh.je_source source,
    jh.je_category,
    gir.je_header_id je_header_id,
    gir.je_line_num je_line_num,
    jl.effective_date acc_date,
    jh.name entry,
    jh.je_batch_id batch,
    SUBSTR (jl.description, 1, 240) line_descr,
    ds.name lsequence,
    jh.doc_sequence_value hnumber,
    jl.je_line_num lline,
    jl.accounted_dr line_acctd_dr,
    jl.accounted_cr line_acctd_cr,
    jl.entered_dr ent_dr,
    jl.entered_cr ent_cr,
    gir.subledger_doc_sequence_id seq_id,
    gir.subledger_doc_sequence_value seq_num,
    jh.doc_sequence_id h_seq_id,
    gir.gl_sl_link_id gl_sl_link_id,
    pov.vendor_name customer_or_vendor,
    jl.entered_dr entered_dr,
    jl.entered_cr entered_cr,
    jl.accounted_cr accounted_dr,
    jl.accounted_dr accounted_cr,
    ael.unrounded_accounted_dr sla_line_accounted_dr,
    ael.unrounded_accounted_cr sla_line_accounted_cr,
    NVL (ael.unrounded_accounted_dr, 0) - NVL (ael.unrounded_accounted_cr, 0) sla_line_accounted_net,
    ael.unrounded_entered_dr sla_line_entered_dr,
    ael.unrounded_entered_cr sla_line_entered_cr,
    NVL (ael.unrounded_entered_dr, 0) - NVL (ael.unrounded_entered_cr, 0) sla_line_entered_net,
    adl.unrounded_entered_cr sla_dist_entered_cr,
    adl.unrounded_entered_dr sla_dist_entered_dr,
    NVL (adl.unrounded_entered_cr, 0) - NVL (adl.unrounded_entered_dr, 0) sla_dist_entered_net,
    adl.unrounded_accounted_cr sla_dist_accounted_cr,
    adl.unrounded_accounted_dr sla_dist_accounted_dr,
    NVL (adl.unrounded_accounted_cr, 0) - NVL (adl.unrounded_accounted_dr, 0) sla_dist_accounted_net,
    TO_CHAR ( DECODE (adl.source_distribution_type, 'AP_PMT_DIST', ac.check_number, '')) associate_num,
    ai.invoice_num transaction_mum,
    gcc.concatenated_segments gl_account_string,
    jh.currency_code,
    jh.period_name,
    ael.accounting_class_code type,
    gps.effective_period_num,
    le.name,
    gb.name batch_name,
    gcc.segment1,
    gcc.segment2,
    gcc.segment3,
    gcc.segment4,
    gcc.segment5,
    gcc.code_combination_id
    --descr#flexfield#start
    ,
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I') gcc#branch 
	--descr#flexfield#end
    --gl#accountff#start
    ,
    gcc.segment4 gcc#50328#account,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ACCOUNT') gcc#50328#account#descr,
    gcc.segment3 gcc#50328#cost_center,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_COSTCENTER' ) gcc#50328#cost_center#descr,
    gcc.segment6 gcc#50328#furture_use,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_FUTURE_USE1') gcc#50328#furture_use#descr,
    gcc.segment7 gcc#50328#future_use_2,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc#50328#future_use_2#descr,
    gcc.segment2 gcc#50328#location,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_LOCATION') gcc#50328#location#descr,
    gcc.segment1 gcc#50328#product,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_PRODUCT') gcc#50328#product#descr,
    gcc.segment5 gcc#50328#project_code,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_PROJECT') gcc#50328#project_code#descr,
    gcc.segment4 gcc#50368#account,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc#50368#account#descr,
    gcc.segment3 gcc#50368#department,
    xxeis.eis_rs_fin_utility.decode_vset ( gcc.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc#50368#department#descr,
    gcc.segment2 gcc#50368#division,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc#50368#division#descr,
    gcc.segment6 gcc#50368#future_use,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc#50368#future_use#descr,
    gcc.segment1 gcc#50368#product,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc#50368#product#descr,
    gcc.segment5 gcc#50368#subaccount,
    xxeis.eis_rs_fin_utility.decode_vset ( gcc.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT') gcc#50368#subaccount#descr
    --gl#accountff#end
  FROM gl_je_headers jh,
    gl_je_lines jl,
    gl_ledgers le,
    gl_je_batches gb,
    gl_period_statuses gps,
    gl_import_references gir,
    fnd_document_sequences ds,
    gl_code_combinations_kfv gcc,
    xla_ae_lines ael,
    gl_code_combinations_kfv gcc1,
    xla_ae_headers aeh,
    xla_distribution_links adl,
    ap_payment_hist_dists aphd,
    ap_invoice_payments_all aip,
    ap_checks_all ac,
    ap_invoice_distributions_all aid,
    ap_invoices_all ai,
    po_vendors pov
  WHERE jh.je_header_id                = jl.je_header_id
  AND jh.je_source                     = 'Payables'
  AND jh.ledger_id                     = le.ledger_id
  AND le.object_type_code              = 'L'
  AND jh.je_batch_id                   = gb.je_batch_id
  AND gps.application_id               = 101
  AND gps.period_name                  = jh.period_name
  AND gps.ledger_id                    = le.ledger_id
  AND gir.je_header_id                 = jh.je_header_id
  AND gir.je_line_num                  = jl.je_line_num
  AND gir.subledger_doc_sequence_id    = ds.doc_sequence_id(+)
  AND jl.code_combination_id           = gcc.code_combination_id
  AND gir.gl_sl_link_id                = ael.gl_sl_link_id(+)
  AND gir.gl_sl_link_table             = ael.gl_sl_link_table(+)
  AND ael.application_id(+)            = 200					--TMS#20160418-00109  by Siva  on 04-16-2016
  AND ael.ae_header_id                 = aeh.ae_header_id(+)
  AND ael.application_id               = aeh.application_id(+)	--TMS#20160418-00109  by Siva  on 04-16-2016
  AND ael.code_combination_id          = gcc1.code_combination_id(+)
  AND ael.ae_header_id                 = adl.ae_header_id(+)
  AND ael.ae_line_num                  = adl.ae_line_num(+)
  AND ael.application_id               = adl.application_id(+)	--TMS#20160418-00109  by Siva  on 04-16-2016
  AND adl.source_distribution_type     = 'AP_PMT_DIST'
  AND adl.source_distribution_id_num_1 = aphd.payment_hist_dist_id
  AND aphd.invoice_payment_id          = aip.invoice_payment_id
  AND aip.check_id                     = ac.check_id(+)
  AND aphd.invoice_distribution_id     = aid.invoice_distribution_id
  AND aid.invoice_id                   = ai.invoice_id
  AND ai.vendor_id                     = pov.vendor_id
  UNION ALL
  SELECT  /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1) INDEX(apad AP_PREPAY_APP_DISTS_U1)*/    --TMS#20160418-00109  by Siva  on 04-16-2016
    jh.je_source source,
    jh.je_category,
    gir.je_header_id je_header_id,
    gir.je_line_num je_line_num,
    jl.effective_date acc_date,
    jh.name entry,
    jh.je_batch_id batch,
    SUBSTR (jl.description, 1, 240) line_descr,
    ds.name lsequence,
    jh.doc_sequence_value hnumber,
    jl.je_line_num lline,
    jl.accounted_dr line_acctd_dr,
    jl.accounted_cr line_acctd_cr,
    jl.entered_dr ent_dr,
    jl.entered_cr ent_cr,
    gir.subledger_doc_sequence_id seq_id,
    gir.subledger_doc_sequence_value seq_num,
    jh.doc_sequence_id h_seq_id,
    gir.gl_sl_link_id gl_sl_link_id,
    pov.vendor_name customer_or_vendor,
    jl.entered_dr entered_dr,
    jl.entered_cr entered_cr,
    jl.accounted_cr accounted_dr,
    jl.accounted_dr accounted_cr,
    ael.unrounded_accounted_dr sla_line_accounted_dr,
    ael.unrounded_accounted_cr sla_line_accounted_cr,
    NVL (ael.unrounded_accounted_dr, 0) - NVL (ael.unrounded_accounted_cr, 0) sla_line_accounted_net,
    ael.unrounded_entered_dr sla_line_entered_dr,
    ael.unrounded_entered_cr sla_line_entered_cr,
    NVL (ael.unrounded_entered_dr, 0) - NVL (ael.unrounded_entered_cr, 0) sla_line_entered_net,
    adl.unrounded_entered_cr sla_dist_entered_cr,
    adl.unrounded_entered_dr sla_dist_entered_dr,
    NVL (adl.unrounded_entered_cr, 0) - NVL (adl.unrounded_entered_dr, 0) sla_dist_entered_net,
    adl.unrounded_accounted_cr sla_dist_accounted_cr,
    adl.unrounded_accounted_dr sla_dist_accounted_dr,
    NVL (adl.unrounded_accounted_cr, 0) - NVL (adl.unrounded_accounted_dr, 0) sla_dist_accounted_net,
    '' associate_num,
    ai.invoice_num transaction_mum,
    gcc.concatenated_segments gl_account_string,
    jh.currency_code,
    jh.period_name,
    ael.accounting_class_code type,
    gps.effective_period_num,
    le.name,
    gb.name batch_name,
    gcc.segment1,
    gcc.segment2,
    gcc.segment3,
    gcc.segment4,
    gcc.segment5,
    gcc.code_combination_id
    --descr#flexfield#start
    ,
    xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I') gcc#branch 
	--descr#flexfield#end
    --gl#accountff#start
    ,
    gcc.segment4 gcc#50328#account,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ACCOUNT') gcc#50328#account#descr,
    gcc.segment3 gcc#50328#cost_center,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3, 'XXCUS_GL_COSTCENTER' ) gcc#50328#cost_center#descr,
    gcc.segment6 gcc#50328#furture_use,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_FUTURE_USE1') gcc#50328#furture_use#descr,
    gcc.segment7 gcc#50328#future_use_2,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7, 'XXCUS_GL_FUTURE_USE_2') gcc#50328#future_use_2#descr,
    gcc.segment2 gcc#50328#location,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_LOCATION') gcc#50328#location#descr,
    gcc.segment1 gcc#50328#product,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_PRODUCT') gcc#50328#product#descr,
    gcc.segment5 gcc#50328#project_code,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5, 'XXCUS_GL_PROJECT') gcc#50328#project_code#descr,
    gcc.segment4 gcc#50368#account,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4, 'XXCUS_GL_ LTMR _ACCOUNT') gcc#50368#account#descr,
    gcc.segment3 gcc#50368#department,
    xxeis.eis_rs_fin_utility.decode_vset ( gcc.segment3, 'XXCUS_GL_ LTMR _DEPARTMENT') gcc#50368#department#descr,
    gcc.segment2 gcc#50368#division,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2, 'XXCUS_GL_ LTMR _DIVISION') gcc#50368#division#descr,
    gcc.segment6 gcc#50368#future_use,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6, 'XXCUS_GL_ LTMR _FUTUREUSE') gcc#50368#future_use#descr,
    gcc.segment1 gcc#50368#product,
    xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1, 'XXCUS_GL_LTMR_PRODUCT') gcc#50368#product#descr,
    gcc.segment5 gcc#50368#subaccount,
    xxeis.eis_rs_fin_utility.decode_vset ( gcc.segment5, 'XXCUS_GL_ LTMR _SUBACCOUNT') gcc#50368#subaccount#descr
    --gl#accountff#end
  FROM gl_je_headers jh,
    gl_je_lines jl,
    gl_je_batches gb,
    gl_ledgers le,
    gl_period_statuses gps,
    gl_code_combinations_kfv gcc,
    gl_import_references gir,
    fnd_document_sequences ds,
    xla_ae_lines ael,
    gl_code_combinations_kfv gcc1,
    xla_ae_headers aeh,
    xla_distribution_links adl,
    ap_prepay_app_dists apad,
    ap_invoice_distributions_all aid,
    ap_invoices_all ai,
    po_vendors pov
  WHERE jh.je_header_id                = jl.je_header_id
  AND jh.je_source                     = 'Payables'
  AND jh.je_batch_id                   = gb.je_batch_id
  AND jh.ledger_id                     = le.ledger_id
  AND le.object_type_code              = 'L'
  AND gps.application_id               = 101
  AND gps.period_name                  = jh.period_name
  AND gps.ledger_id                    = le.ledger_id
  AND jl.code_combination_id           = gcc.code_combination_id
  AND jh.je_header_id                  = gir.je_header_id
  AND jl.je_line_num                   = gir.je_line_num
  AND gir.subledger_doc_sequence_id    = ds.doc_sequence_id(+)
  AND gir.gl_sl_link_id                = ael.gl_sl_link_id(+)
  AND gir.gl_sl_link_table             = ael.gl_sl_link_table(+)
  AND ael.application_id(+)            = 200					--TMS#20160418-00109  by Siva  on 04-16-2016
  AND ael.ae_header_id                 = aeh.ae_header_id(+)
  AND ael.application_id               = aeh.application_id(+)	--TMS#20160418-00109  by Siva  on 04-16-2016
  AND ael.code_combination_id          = gcc1.code_combination_id(+)
  AND ael.ae_header_id                 = adl.ae_header_id(+)
  AND ael.ae_line_num                  = adl.ae_line_num(+)
  AND ael.application_id               = adl.application_id(+)	--TMS#20160418-00109  by Siva  on 04-16-2016
  AND adl.source_distribution_type     = 'AP_PREPAY'
  AND adl.source_distribution_id_num_1 = apad.prepay_app_dist_id
  AND apad.invoice_distribution_id     = aid.invoice_distribution_id
  AND aid.invoice_id                   = ai.invoice_id
  AND ai.vendor_id                     = pov.vendor_id
)
/
