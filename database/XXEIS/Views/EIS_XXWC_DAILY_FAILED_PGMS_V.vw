CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_DAILY_FAILED_PGMS_V" ("REQUEST_ID", "CONCURRENT_PROGRAM_NAME", "STATUS", "REQUESTOR", "DATE_STARTED", "DATE_COMPLETED", "PARAMETERS", "COMPLETION_TEXT") AS 
/* Query to fetch Reports completed with Error and Terminated which are submitted by XXWC_INT_SUPPLYCHAIN','XXWC_INT_SALESFULFILLMENT','REBINTERFACE','XXWC_INT_FINANCE', 'GLINTERFACE', 'HDSAPINTERFACE', 'HDSHRINTERFACE','OPNINTERFACE','HDSSYSADMIN'*/
  SELECT request_id 
,      program CONCURRENT_PROGRAM_NAME
,      DECODE(status_code, 'G', ' Warning ', 'X', ' Terminated ', 'Error') STATUS
,      requestor
,      ACTUAL_START_DATE DATE_STARTED
,      actual_completion_date DATE_COMPLETED
,      argument_text PARAMETERS
,      completion_text
FROM   apps.fnd_conc_req_summary_v
WHERE  phase_code = 'C'
AND    status_code IN ('X', 'E')
--AND    (trunc(request_date) >= trunc(SYSDATE - 1) or trunc(actual_start_date) >= trunc(SYSDATE - 1))
AND ((to_date(to_char(request_date,'DD-MM-YYYY HH24:MI:SS'),'DD-MM-YY HH24:MI:SS') 
     between to_date(sysdate-1||' 06:00:00','DD-MM-YY HH24:MI:SS') 
     AND to_date(sysdate|| ' 07:00:00','DD-MM-YY HH24:MI:SS')) 
     or (to_date(to_char(actual_start_date,'DD-MM-YY HH24:MI:SS'),'DD-MM-YY HH24:MI:SS') 
     between to_date(sysdate-1||' 06:00:00','DD-MM-YY HH24:MI:SS') 
     AND TO_DATE(SYSDATE||' 07:00:00','DD-MM-YY HH24:MI:SS')))
AND    requestor IN ('XXWC_INT_SUPPLYCHAIN','XXWC_INT_SALESFULFILLMENT','REBINTERFACE','XXWC_INT_FINANCE', 'GLINTERFACE', 'HDSAPINTERFACE', 'HDSHRINTERFACE','OPNINTERFACE','HDSSYSADMIN')
/* Query to fetch Reports completed with Error,Warning and Terminated which are submitted by Requestor start with name XXWC_INT_*/
UNION
SELECT request_id 
,      program CONCURRENT_PROGRAM_NAME
,      DECODE(status_code, 'G', ' Warning ', 'X', ' Terminated ', 'Error') STATUS
,      REQUESTOR
,      ACTUAL_START_DATE DATE_STARTED
,      actual_completion_date DATE_COMPLETED
,      argument_text PARAMETERS
,      completion_text
FROM   apps.fnd_conc_req_summary_v
WHERE  phase_code = 'C'
AND    status_code = 'G'
AND    program =  'XXWC Rental Recurring Invoice Engine'
--AND    (trunc(request_date) >= trunc(SYSDATE - 1) or trunc(actual_start_date) >= trunc(SYSDATE - 1))
AND ((to_date(to_char(request_date,'DD-MM-YYYY HH24:MI:SS'),'DD-MM-YY HH24:MI:SS') 
     between to_date(sysdate-1||' 06:00:00','DD-MM-YY HH24:MI:SS') 
     AND to_date(sysdate|| ' 07:00:00','DD-MM-YY HH24:MI:SS')) 
     or (to_date(to_char(actual_start_date,'DD-MM-YY HH24:MI:SS'),'DD-MM-YY HH24:MI:SS') 
     between to_date(sysdate-1||' 06:00:00','DD-MM-YY HH24:MI:SS') 
     AND to_date(sysdate||' 07:00:00','DD-MM-YY HH24:MI:SS')))
AND    REQUESTOR LIKE 'XXWC_INT_%'
ORDER BY 
       REQUESTOR  ASC
,      request_id DESC;
/