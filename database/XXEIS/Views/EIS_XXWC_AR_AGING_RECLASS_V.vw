---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_AGING_RECLASS_V $
  Module Name : Receivables
  PURPOSE	  : 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC'
  TMS Task Id : 20161020-00056
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	   02-Nov-2016        	Siva   		 Initial version--TMS#20161020-00056
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_AGING_RECLASS_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_AGING_RECLASS_V (CUST_ACCOUNT_ID, CUSTOMER_ACCOUNT_NUMBER, PRISM_CUSTOMER_NUMBER, CUSTOMER_NAME, BILL_TO_SITE_USE_ID, BILL_TO_PARTY_SITE_NUMBER, BILL_TO_PRISM_SITE_NUMBER, BILL_TO_PARTY_SITE_NAME, BILL_TO_SITE_NAME, TRX_BILL_TO_SITE_NAME, TRX_PARTY_SITE_NUMBER, TRX_PARTY_SITE_NAME, BILL_TO_ADDRESS1, BILL_TO_ADDRESS2, BILL_TO_ADDRESS3, BILL_TO_ADDRESS4, BILL_TO_CITY, BILL_TO_CITY_PROVINCE, BILL_TO_ZIP_CODE, BILL_TO_COUNTRY, PAYMENT_SCHEDULE_ID, CUSTOMER_TRX_ID, RCTA_CCID, CASH_RECEIPT_ID, ACRA_CCID, INVOICE_NUMBER, RECEIPT_NUMBER, BRANCH_LOCATION, TRX_NUMBER, TRX_DATE, DUE_DATE, TRX_TYPE, TRANSATION_BALANCE, TRANSACTION_REMAINING_BALANCE, AGE, CURRENT_BALANCE, THIRTY_DAYS_BAL, SIXTY_DAYS_BAL, NINETY_DAYS_BAL, ONE_EIGHTY_DAYS_BAL, THREE_SIXTY_DAYS_BAL, OVER_THREE_SIXTY_DAYS_BAL, LAST_PAYMENT_DATE, CUSTOMER_ACCOUNT_STATUS, SITE_CREDIT_HOLD, CUSTOMER_PROFILE_CLASS, COLLECTOR_NAME, CREDIT_ANALYST, ACCOUNT_MANAGER, ACCOUNT_BALANCE, CUST_PAYMENT_TERM
  , REMIT_TO_ADDRESS_CODE, STMT_BY_JOB, SEND_STATEMENT_FLAG, SEND_CREDIT_BAL_FLAG, TRX_CUSTOMER_ID, TRX_BILL_SITE_USE_ID, CUSTOMER_PO_NUMBER, PMT_STATUS, SALESREP_NUMBER, CREDIT_LIMIT, TWELVE_MONTHS_SALES, TERMS, PHONE_NUMBER, IN_PROCESS, LARGEST_BALANCE, AVERAGE_DAYS, AMOUNT_PAID, ACCOUNT_STATUS, TERRITORY, TRX_BILL_TO_ADDRESS1, TRX_BILL_TO_ADDRESS2, TRX_BILL_TO_ADDRESS3, TRX_BILL_TO_ADDRESS4, TRX_BILL_TO_CITY, TRX_BILL_TO_CITY_PROV, TRX_BILL_TO_ZIP_CODE, TRX_BILL_TO_COUNTRY, TRX_ADDRESS, SEGMENT2, CREATION_DATE)
AS
  SELECT exag.cust_account_id ,
    exag.customer_account_number ,
    exag.prism_customer_number ,
    exag.customer_name ,
    exag.bill_to_site_use_id ,
    exag.bill_to_party_site_number ,
    exag.bill_to_prism_site_number ,
    exag.bill_to_party_site_name ,
    exag.bill_to_site_name ,
    REPLACE(exag.trx_bill_to_site_name,'"','') trx_bill_to_site_name,
    exag.trx_party_site_number ,
    exag.trx_party_site_name ,
    exag.bill_to_address1 ,
    exag.bill_to_address2 ,
    exag.bill_to_address3 ,
    exag.bill_to_address4 ,
    exag.bill_to_city ,
    exag.bill_to_city_province ,
    exag.bill_to_zip_code ,
    exag.bill_to_country ,
    exag.payment_schedule_id ,
    exag.customer_trx_id ,
    exag.rcta_ccid ,
    exag.cash_receipt_id ,
    exag.acra_ccid ,
    exag.invoice_number ,
    exag.receipt_number ,
    exag.branch_location ,
    exag.trx_number ,
    exag.trx_date ,
    exag.due_date ,
    exag.trx_type ,
    exag.transation_balance ,
    exag.transaction_remaining_balance ,
    exag.age ,
    exag.current_balance ,
    exag.thirty_days_bal ,
    exag.sixty_days_bal ,
    exag.ninety_days_bal ,
    exag.one_eighty_days_bal ,
    exag.three_sixty_days_bal ,
    exag.over_three_sixty_days_bal ,
    exag.last_payment_date ,
    exag.customer_account_status ,
    exag.site_credit_hold ,
    exag.customer_profile_class ,
    exag.collector_name ,
    exag.credit_analyst ,
    exag.account_manager ,
    exag.account_balance ,
    exag.cust_payment_term ,
    exag.remit_to_address_code ,
    exag.stmt_by_job ,
    exag.send_statement_flag ,
    exag.send_credit_bal_flag ,
    exag.trx_customer_id ,
    exag.trx_bill_site_use_id ,
    exag.customer_po_number ,
    exag.pmt_status ,
    exag.salesrep_number salesrep_number ,
    exag.yard_credit_limit credit_limit ,
    exag.twelve_months_sales twelve_months_sales ,
    exag.cust_payment_term terms ,
    exag.acct_phone_number phone_number ,
    NULL in_process ,
    to_number (NULL) largest_balance ,
    to_number (NULL) average_days ,
    to_number (NULL) amount_paid ,
    exag.account_status account_status ,
    NULL territory ,
    exag.trx_bill_to_address1 ,
    exag.trx_bill_to_address2 ,
    exag.trx_bill_to_address3 ,
    exag.trx_bill_to_address4 ,
    exag.trx_bill_to_city ,
    exag.trx_bill_to_city_prov ,
    exag.trx_bill_to_zip_code ,
    exag.trx_bill_to_country ,
    exag.trx_bill_to_address1
    || ','
    || DECODE (trim (exag.trx_bill_to_address2), NULL, NULL, exag.trx_bill_to_address2
    || ',')
    || exag.trx_bill_to_city
    || ','
    || exag.trx_bill_to_city_prov
    || ','
    || exag.trx_bill_to_zip_code trx_address,
    gcc.segment2 ,
    TRUNC(rctlg.creation_date) creation_date
  FROM xxwc.xxwc_ar_cust_bal_mv_monthly exag,
    apps.ra_customer_trx_all rct ,
    apps.ra_cust_trx_line_gl_dist_all rctlg ,
    apps.gl_code_combinations gcc
  WHERE exag.customer_trx_id         = rct.customer_trx_id
  AND rct.customer_trx_id            = rctlg.customer_trx_id
  AND rctlg.code_combination_id      = gcc.code_combination_id
  AND rctlg.account_class            = 'REV'
  AND exag.branch_location           = 'BW080'
  AND rctlg.cust_trx_line_gl_dist_id =
    (SELECT MIN(cust_trx_line_gl_dist_id)
    FROM apps.ra_cust_trx_line_gl_dist_all rctlg1 ,
      xxwc.xxwc_ar_cust_bal_mv_monthly exag1 ,
      apps.ra_customer_trx_all rct1 ,
      apps.gl_code_combinations gcc1
    WHERE exag.customer_trx_id    = rct1.customer_trx_id
    AND rct.customer_trx_id       = rctlg1.customer_trx_id
    AND exag1.customer_trx_id     = rct1.customer_trx_id
    AND rct1.customer_trx_id      = rctlg1.customer_trx_id
    AND rctlg.code_combination_id = gcc1.code_combination_id
    AND rctlg1.account_class      = 'REV'
    AND exag1.branch_location     = 'BW080'
    )
/
