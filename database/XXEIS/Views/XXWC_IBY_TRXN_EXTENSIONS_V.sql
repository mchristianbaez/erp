-----------------------------------------------------------------------------------------------
/**************************************************************************************************************
  View Name   : XXEIS.XXWC_IBY_TRXN_EXTENSIONS_V
  PURPOSE	  : This view is used in XXEIS.EIS_XXWC_CASH_EXCEPTION_PKG for Cash Drawer Exception Report.
  TMS Task Id : 20160411-00103   
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva   		 TMS#20160411-00103   Performance Tuning
**************************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.XXWC_IBY_TRXN_EXTENSIONS_V
AS
  SELECT
    x.trxn_extension_id,
    p.payment_channel_name,
    c.masked_cc_number card_number,
    c.card_issuer_code,
    i.card_issuer_name
  FROM
    apps.iby_creditcard c,
    apps.iby_creditcard_issuers_vl i,
    apps.iby_fndcpt_pmt_chnnls_vl p,
    apps.iby_fndcpt_tx_extensions x,
    apps.iby_pmt_instr_uses_all u
  WHERE
    x.instr_assignment_id                                           = u.instrument_payment_use_id(+)
  AND DECODE(u.instrument_type, 'CREDITCARD',u.instrument_id, NULL) = c.instrid(+)
  AND x.payment_channel_code = p.payment_channel_code
  AND c.card_issuer_code     = i.card_issuer_code(+)
/
