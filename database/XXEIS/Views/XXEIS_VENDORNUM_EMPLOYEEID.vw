CREATE OR REPLACE FORCE VIEW xxeis.xxeis_vendornum_employeeid
(
   vendor_name
  ,vendor_num
  ,employee_number
)
AS
   SELECT v.vendor_name, v.segment1 vendor_num, papf.employee_number
     FROM ap.ap_suppliers v, hr.per_all_people_f papf
    WHERE     v.employee_id = papf.person_id
          AND v.vendor_type_lookup_code = 'EMPLOYEE'
          AND v.employee_id IS NOT NULL
          AND papf.effective_end_date = '31-DEC-4712';


