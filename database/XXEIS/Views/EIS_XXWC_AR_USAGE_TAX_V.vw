CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_ar_usage_tax_v
(
   fiscal_month
  ,segment1
  ,TYPE
  ,location
  ,description
  ,branch
  ,state
  ,city
  ,city_tax_rate
  ,tax_amt
  ,VALUE
  ,amt
)
AS
   SELECT oap.period_name fiscal_month
         ,mgp.segment1
         ,mgp.segment1 || ' Adjustment' TYPE
         ,haou.name location
         ,mgp.description
         ,gcc.segment2 branch
         ,                                 --  msi.concatenated_segments item,
          haou.region_2 state
         ,haou.town_or_city city
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_usetax_rate (haou.postal_code)
             city_tax_rate
         , --NVL(tax.curuserate,0)                                              +NVL(ctax.curuserate,0)+NVL(stax.curuserate,0) city_tax_rate,
           (NVL (mmt.primary_quantity, 0) * NVL (mmt.actual_cost, 0))
          * (xxeis.eis_rs_xxwc_com_util_pkg.get_usetax_rate (
                haou.postal_code))
             tax_amt
         , (NVL (mmt.primary_quantity, 0) * NVL (mmt.actual_cost, 0)) VALUE
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_usetax_amt (
             (mmt.primary_quantity * mmt.actual_cost)
            ,haou.postal_code)
             amt
     FROM mtl_transaction_types mtt
         ,org_acct_periods oap
         ,mtl_system_items_kfv msi
         ,mtl_material_transactions mmt
         ,mtl_generic_dispositions mgp
         ,mtl_parameters mp
         ,hr_organization_units_v haou
         ,gl_code_combinations gcc                                         --,
    -- taxlocltax tax,
    -- taxware.taxcntytax ctax,
    --taxware.taxsttax stax
    WHERE     msi.organization_id = mp.organization_id
          AND mmt.organization_id = mp.organization_id
          AND mmt.inventory_item_id = msi.inventory_item_id
          AND mmt.transaction_source_type_id = 6
          AND mgp.disposition_id = mmt.transaction_source_id
          AND mgp.organization_id = mp.organization_id
          AND oap.organization_id(+) = mmt.organization_id
          AND oap.acct_period_id(+) = mmt.acct_period_id
          --AND haou.postal_code               = tax.zipcode(+)
          --AND tax.cntycode                   = ctax.cntycode(+)
          --AND tax.stcode                     = stax.stcode
          AND mmt.transaction_type_id = mtt.transaction_type_id(+)
          AND haou.organization_id = msi.organization_id
          AND mgp.distribution_account = gcc.code_combination_id(+)
          AND mgp.segment1 IN
                 ('TYPE 6'
                 ,'TYPE 7'
                 ,'TYPE 8'
                 ,'TYPE 9'
                 ,'TYPE 11'
                 ,'TYPE 12'
                 ,'TYPE 25')
          AND EXISTS
                 (SELECT 1
                    FROM xxeis.eis_org_access_v
                   WHERE organization_id = msi.organization_id);


