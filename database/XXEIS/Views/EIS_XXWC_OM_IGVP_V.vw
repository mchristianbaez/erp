CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_igvp_v
(
   customer_number
  ,customer_name
  ,created_by
  ,inventory_organization
  ,sales_order_number
  ,sales_order_status
  ,order_type
  ,sales_order_line_status
  ,sales_order_line_number
  ,order_header_hold_name
  ,order_line_hold_name
  ,extended_cost
  ,part_number
  ,part_description
  ,quantity
  ,unit_selling_price
  ,extended_price
  ,margin_per
  ,unit_cost
  ,gross_percent_dollars
  ,gross_percentage
  ,ordered_date
  ,onhand_location
  ,average_cost
  ,margin
  ,unit_list_price
  ,hold_date
  ,header_id
  ,line_id
  ,party_id
  ,inventory_item_id
  ,organization_id
  ,hzp#party_type
  ,hzp#gvid_id
  ,mtp#factory_planner_data_dir
  ,mtp#fru
  ,mtp#location_number
  ,mtp#branch_operations_manage
  ,mtp#deliver_charge
  ,mtp#factory_planner_executab
  ,mtp#factory_planner_user
  ,mtp#factory_planner_host
  ,mtp#factory_planner_port_num
  ,mtp#pricing_zone
  ,mtp#org_type
  ,mtp#district
  ,mtp#region
  ,msi#hds#lob
  ,msi#hds#drop_shipment_eligab
  ,msi#hds#invoice_uom
  ,msi#hds#product_id
  ,msi#hds#vendor_part_number
  ,msi#hds#unspsc_code
  ,msi#hds#upc_primary
  ,msi#hds#sku_description
  ,msi#wc#ca_prop_65
  ,msi#wc#country_of_origin
  ,msi#wc#orm_d_flag
  ,msi#wc#store_velocity
  ,msi#wc#dc_velocity
  ,msi#wc#yearly_store_velocity
  ,msi#wc#yearly_dc_velocity
  ,msi#wc#prism_part_number
  ,msi#wc#hazmat_description
  ,msi#wc#hazmat_container
  ,msi#wc#gtp_indicator
  ,msi#wc#last_lead_time
  ,msi#wc#amu
  ,msi#wc#reserve_stock
  ,msi#wc#taxware_code
  ,msi#wc#average_units
  ,msi#wc#product_code
  ,msi#wc#import_duty_
  ,msi#wc#keep_item_active
  ,msi#wc#pesticide_flag
  ,msi#wc#calc_lead_time
  ,msi#wc#voc_gl
  ,msi#wc#pesticide_flag_state
  ,msi#wc#voc_category
  ,msi#wc#voc_sub_category
  ,msi#wc#msds_#
  ,msi#wc#hazmat_packaging_grou
  ,oh#rental_term
  ,oh#order_channel
  ,oh#picked_by
  ,oh#load_checked_by
  ,oh#delivered_by
  ,ol#estimated_return_date
  ,ol#rerent_po
  ,ol#force_ship
  ,ol#application_method
  ,ol#item_on_blowout
  ,ol#pof_std_line
  ,ol#rerental_billing_terms
  ,ol#pricing_guardrail
  ,ol#print_expired_product_dis
  ,ol#rental_charge
  ,ol#vendor_quote_cost
  ,ol#po_cost_for_vendor_quote
  ,ol#serial_number
  ,ol#engineering_cost
)
AS
   SELECT hca.account_number customer_number
         ,NVL (hca.account_name, hzp.party_name) customer_name
         ,ppf.full_name created_by
         ,mtp.organization_code inventory_organization
         ,oh.order_number sales_order_number
         ,NVL (flv_header_status.meaning
              ,INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             sales_order_status
         ,ott.name order_type
         ,NVL (
             flv_line_status.meaning
            ,INITCAP (
                REPLACE (
                   xxeis.eis_rs_xxwc_com_util_pkg.get_order_line_status (
                      ol.line_id)
                  ,'_'
                  ,' ')))
             sales_order_line_status
         ,DECODE (
             ol.option_number
            ,'', (ol.line_number || '.' || ol.shipment_number)
            , (   ol.line_number
               || '.'
               || ol.shipment_number
               || '.'
               || ol.option_number))
             sales_order_line_number
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_header_hold (ol.header_id
                                                         ,ol.line_id)
             order_header_hold_name
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_line_hold (ol.header_id
                                                       ,ol.line_id)
             order_line_hold_name
         ,NVL ( (NVL (ol.unit_cost, 0) * ol.ordered_quantity), 0)
             extended_cost
         ,msi.segment1 part_number
         ,NVL (msi.description, ol.user_item_description) part_description
         ,ol.ordered_quantity quantity
         ,NVL (ol.unit_selling_price, 0) unit_selling_price
         ,NVL ( (NVL (ol.unit_selling_price, 0) * ol.ordered_quantity), 0)
             extended_price
         ,                         --OSOL.MARGIN_PERCENT           MARGIN_PER,
           (CASE
               WHEN NVL (ol.unit_cost, 0) = 0
               THEN
                  100
               WHEN ol.unit_selling_price = 0
               THEN
                  0
               WHEN ol.ordered_quantity = 0
               THEN
                  0
               ELSE
                  (  (  ol.ordered_quantity * ol.unit_selling_price
                      - ol.ordered_quantity * NVL (ol.unit_cost, 0))
                   / (ol.ordered_quantity * ol.unit_selling_price)
                   * 100)
            END)
             margin_per
         ,NVL (ol.unit_cost, 0) unit_cost
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_sales_profit (ol.header_id)
             gross_percent_dollars
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_sales_profit_percentage (
             ol.header_id)
             gross_percentage
         , /*SUM((
           CASE
           WHEN NVL(Ol.Unit_Cost,0) = 0
           THEN 100
           ELSE OSOL.MARGIN_PERCENT
           END)) GROSS_MARGIN_PERCENTAGE,*/
                                        --TRUNC(OH.ORDERED_DATE) ORDERED_DATE,
          TRUNC (oh.ordered_date) ordered_date
         ,mtp.organization_code onhand_location
         ,NVL (
             apps.cst_cost_api.get_item_cost (1
                                             ,msi.inventory_item_id
                                             ,msi.organization_id)
            ,0)
             average_cost
         , (NVL (ol.unit_selling_price, 0) - NVL (ol.unit_cost, 0)) margin
         ,NVL (ol.unit_list_price, 0) unit_list_price
         ,NVL (
             xxeis.eis_rs_xxwc_com_util_pkg.get_header_hold_date (
                oh.header_id
               ,ol.line_id)
            ,xxeis.eis_rs_xxwc_com_util_pkg.get_line_hold_date (oh.header_id
                                                               ,ol.line_id))
             hold_date
         ,                                                     ---Primary Keys
          oh.header_id
         ,ol.line_id
         ,hzp.party_id
         ,msi.inventory_item_id
         ,msi.organization_id
         --descr#flexfield#start

         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_PARTY_TYPE'
                                           ,hzp.attribute1
                                           ,'I')
             hzp#party_type
         ,hzp.attribute2 hzp#gvid_id
         ,mtp.attribute1 mtp#factory_planner_data_dir
         ,mtp.attribute10 mtp#fru
         ,mtp.attribute11 mtp#location_number
         ,xxeis.eis_rs_dff.decode_valueset ('HR_DE_EMPLOYEES'
                                           ,mtp.attribute13
                                           ,'F')
             mtp#branch_operations_manage
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DELIVERY_CHARGE_EXEMPT'
                                           ,mtp.attribute14
                                           ,'I')
             mtp#deliver_charge
         ,mtp.attribute2 mtp#factory_planner_executab
         ,mtp.attribute3 mtp#factory_planner_user
         ,mtp.attribute4 mtp#factory_planner_host
         ,mtp.attribute5 mtp#factory_planner_port_num
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_PRICE_ZONES'
                                           ,mtp.attribute6
                                           ,'F')
             mtp#pricing_zone
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_ORG_TYPE'
                                           ,mtp.attribute7
                                           ,'I')
             mtp#org_type
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DISTRICT'
                                           ,mtp.attribute8
                                           ,'I')
             mtp#district
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_REGION'
                                           ,mtp.attribute9
                                           ,'I')
             mtp#region
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL)
             msi#hds#lob
         ,DECODE (
             msi.attribute_category
            ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                     ,msi.attribute10
                                                     ,'F')
            ,NULL)
             msi#hds#drop_shipment_eligab
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL)
             msi#hds#invoice_uom
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL)
             msi#hds#product_id
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL)
             msi#hds#vendor_part_number
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL)
             msi#hds#unspsc_code
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL)
             msi#hds#upc_primary
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL)
             msi#hds#sku_description
         ,DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL)
             msi#wc#ca_prop_65
         ,DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL)
             msi#wc#country_of_origin
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                    ,msi.attribute11
                                                    ,'F')
            ,NULL)
             msi#wc#orm_d_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL)
             msi#wc#store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL)
             msi#wc#dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL)
             msi#wc#yearly_store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL)
             msi#wc#yearly_dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL)
             msi#wc#prism_part_number
         ,DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL)
             msi#wc#hazmat_description
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER'
                                                    ,msi.attribute18
                                                    ,'I')
            ,NULL)
             msi#wc#hazmat_container
         ,DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL)
             msi#wc#gtp_indicator
         ,DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL)
             msi#wc#last_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL)
             msi#wc#amu
         ,DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL)
             msi#wc#reserve_stock
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE'
                                                    ,msi.attribute22
                                                    ,'I')
            ,NULL)
             msi#wc#taxware_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL)
             msi#wc#average_units
         ,DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL)
             msi#wc#product_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL)
             msi#wc#import_duty_
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                    ,msi.attribute29
                                                    ,'F')
            ,NULL)
             msi#wc#keep_item_active
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                    ,msi.attribute3
                                                    ,'F')
            ,NULL)
             msi#wc#pesticide_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL)
             msi#wc#calc_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL)
             msi#wc#voc_gl
         ,DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL)
             msi#wc#pesticide_flag_state
         ,DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL)
             msi#wc#voc_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL)
             msi#wc#voc_sub_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL)
             msi#wc#msds_#
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset (
                      'XXWC_HAZMAT_PACKAGE_GROUP'
                     ,msi.attribute9
                     ,'I')
            ,NULL)
             msi#wc#hazmat_packaging_grou
         ,xxeis.eis_rs_dff.decode_valueset ('RENTAL TYPE'
                                           ,oh.attribute1
                                           ,'I')
             oh#rental_term
         ,xxeis.eis_rs_dff.decode_valueset ('WC_CUS_ORDER_CHANNEL'
                                           ,oh.attribute2
                                           ,'I')
             oh#order_channel
         ,oh.attribute3 oh#picked_by
         ,oh.attribute4 oh#load_checked_by
         ,oh.attribute5 oh#delivered_by
         ,ol.attribute1 ol#estimated_return_date
         ,ol.attribute10 ol#rerent_po
         ,ol.attribute11 ol#force_ship
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_CSP_APP_METHOD'
                                           ,ol.attribute13
                                           ,'I')
             ol#application_method
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ol.attribute15, 'F')
             ol#item_on_blowout
         ,ol.attribute19 ol#pof_std_line
         ,xxeis.eis_rs_dff.decode_valueset ('ReRental Billing Type'
                                           ,ol.attribute2
                                           ,'I')
             ol#rerental_billing_terms
         ,ol.attribute20 ol#pricing_guardrail
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ol.attribute3, 'F')
             ol#print_expired_product_dis
         ,ol.attribute4 ol#rental_charge
         ,ol.attribute5 ol#vendor_quote_cost
         ,ol.attribute6 ol#po_cost_for_vendor_quote
         ,ol.attribute7 ol#serial_number
         ,ol.attribute8 ol#engineering_cost
     --descr#flexfield#end


     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,hz_cust_accounts hca
         ,hz_parties hzp
         ,mtl_parameters mtp
         ,hr_all_organization_units hrou
         ,mtl_system_items_kfv msi
         ,oe_transaction_types_vl ott
         ,                                     --   OE_SCH_ORDER_LINES_V OSOL,
          fnd_lookup_values_vl flv_header_status
         ,fnd_lookup_values_vl flv_line_status
         ,per_people_f ppf
         ,fnd_user fu
    WHERE     ol.header_id = oh.header_id
          AND oh.sold_to_org_id = hca.cust_account_id(+)
          AND hca.party_id = hzp.party_id(+)
          --AND OL.SHIP_FROM_ORG_ID                 = MTP.ORGANIZATION_ID(+)
          AND ol.ship_from_org_id = msi.organization_id
          AND msi.organization_id = mtp.organization_id
          AND hrou.organization_id = msi.organization_id
          AND msi.organization_id = ol.ship_from_org_id
          AND msi.inventory_item_id = ol.inventory_item_id
          AND oh.order_type_id = ott.transaction_type_id
          AND fu.user_id = oh.created_by
          AND fu.employee_id = ppf.person_id(+)
          AND ol.flow_status_code <> 'CLOSED'
          -- AND ol.line_id                          = osol.line_id
          AND flv_header_status.lookup_type(+) = 'FLOW_STATUS'
          AND flv_header_status.lookup_code(+) = oh.flow_status_code
          AND flv_line_status.lookup_type(+) = 'LINE_FLOW_STATUS'
          AND flv_line_status.lookup_code(+) = ol.flow_status_code
          AND (   ol.invoice_interface_status_code IS NULL
               OR EXISTS
                     (SELECT 1
                        FROM oe_order_holds hold
                       WHERE     hold.header_id = oh.header_id
                             AND hold.hold_release_id IS NULL))
          --and oh.order_number='10000037'
          AND EXISTS
                 (SELECT 1
                    FROM xxeis.eis_org_access_v
                   WHERE organization_id = msi.organization_id);


