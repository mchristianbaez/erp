CREATE OR REPLACE FORCE VIEW xxeis.unmapped_bai_detail
(
   bank_account_num
  ,trx_code
  ,description
  ,line_number
  ,trx_date
  ,trx_type
  ,amount
  ,trx_text
  ,bank_trx_number
  ,customer_text
  ,invoice_text
  ,bank_account_text
  ,reference_txt
  ,je_status_flag
  ,accounting_date
)
AS
   SELECT ba.bank_account_num
         ,tr.trx_code
         ,tr.description
         ,li.line_number
         ,li.trx_date
         ,li.trx_type
         ,li.amount
         ,li.trx_text
         ,li.bank_trx_number
         ,li.customer_text
         ,li.invoice_text
         ,li.bank_account_text
         ,li.reference_txt
         ,li.je_status_flag
         ,li.accounting_date
     FROM ce.ce_statement_lines li
         ,ce.ce_statement_headers hd
         ,ce.ce_bank_accounts ba
         ,(  SELECT trx_code, description, bank_account_id
               FROM ce.ce_transaction_codes tr
           GROUP BY trx_code, description, bank_account_id) tr
    WHERE     li.statement_header_id = hd.statement_header_id
          AND hd.bank_account_id = ba.bank_account_id
          AND li.trx_code = tr.trx_code
          AND ba.bank_account_id = tr.bank_account_id
          AND li.cashflow_id IS NULL;


