CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_man_price_v
(
   pricing_zone
  ,region
  ,branch
  ,customer
  ,customer_number
  ,created_by
  ,order_number
  ,item
  ,item_description
  ,cat_class
  ,cat_class_desc
  ,quantity
  ,list_price
  ,original_unit_selling_price
  ,final_selling_price
  ,reason_code
  ,application_method
  ,ordered_date
  ,average_cost
  ,modifier_type
  ,modifier_number
  ,header_id
  ,line_id
  ,list_line_type_code
  ,inventory_item_id
  ,msi_organization_id
  ,organization_id
  ,cust_account_id
)
AS
   SELECT mp.attribute6 pricing_zone
         ,mp.attribute9 region
         ,ood.organization_code branch
         ,hca.account_name customer
         ,hca.account_number customer_number
         ,ppf.full_name created_by
         ,oh.order_number order_number
         , /* (ol.line_number
           ||'.'
           ||ol.shipment_number) line_number,*/
          msi.segment1 item
         ,msi.description item_description
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class (
             msi.inventory_item_id
            ,msi.organization_id)
             cat_class
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc (
             msi.inventory_item_id
            ,msi.organization_id)
             cat_class_desc
         ,ol.ordered_quantity quantity
         ,ol.unit_list_price list_price
         , (  ol.unit_list_price
            - xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt (
                 ol.header_id
                ,ol.line_id))
             original_unit_selling_price
         ,ol.unit_selling_price final_selling_price
         ,oel.meaning reason_code
         , /* CASE
           WHEN upper(adj_man.adjustment_name) IN ('AMOUNT_LINE_DISCOUNT','NEW PRICE','PERCENT_LINE_DISCOUNT')
           THEN apps.qp_qp_form_pricing_attr.get_meaning(adj_man.arithmetic_operator, 'ARITHMETIC_OPERATOR')
           ELSE 'New Price'
           END*/
          apps.qp_qp_form_pricing_attr.get_meaning (
             adj_man.arithmetic_operator
            ,'ARITHMETIC_OPERATOR')
             application_method
         ,TRUNC (oh.ordered_date) ordered_date
         ,NVL (ol.unit_cost, 0) average_cost
         ,qph.attribute10 modifier_type
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_name (
             ol.header_id
            ,ol.line_id)
             modifer_number
         ,                           --adj_man.adjustment_name Modifer_Number,
                        --NVL(qph.attribute10, 'Line Override') modifier_type,
 --(NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0)) average_cost,
                                                               ---Primary Keys
          oh.header_id
         ,ol.line_id
         ,adj_man.list_line_type_code
         ,msi.inventory_item_id
         ,msi.organization_id msi_organization_id
         ,ood.organization_id
         ,hca.cust_account_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,mtl_system_items_kfv msi
         ,mtl_parameters mp
         ,org_organization_definitions ood
         ,hz_cust_accounts hca
         ,oe_price_adjustments_v adj_man
         ,                                --  oe_price_adjustments_v adj_auto,
          per_people_f ppf
         ,fnd_user fu
         ,oe_lookups oel
         ,qp_list_headers_vl qph
    WHERE     1 = 1                             --oh.order_number  ='10001157'
          AND ol.ship_from_org_id = ood.organization_id
          AND oh.sold_to_org_id = hca.cust_account_id
          AND oh.header_id = ol.header_id
          AND ol.ship_from_org_id = msi.organization_id
          AND ol.inventory_item_id = msi.inventory_item_id
          AND msi.organization_id = mp.organization_id
          AND ol.header_id = adj_man.header_id(+)
          AND ol.line_id = adj_man.line_id(+)
          AND adj_man.list_header_id = qph.list_header_id(+)
          AND adj_man.list_line_type_code(+) = 'DIS'
          -- AND Ol.Header_Id                    = NVL(adj_Auto.Header_Id,Ol.Header_Id)
          -- AND Ol.Line_Id                      = NVL(Adj_Auto.Line_Id,Ol.Line_Id)
          -- AND NVL(adj_auto.automatic_flag,'Y')='Y'
          --AND Ol.Created_By = Ppf.Person_Id
          AND fu.user_id = ol.created_by
          AND fu.employee_id = ppf.person_id(+)
          AND TRUNC (ol.creation_date) BETWEEN NVL (ppf.effective_start_date
                                                   ,TRUNC (ol.creation_date))
                                           AND NVL (ppf.effective_end_date
                                                   ,TRUNC (ol.creation_date))
          --AND oh.order_number='10001303'
          --AND ( adj_man.adjustment_name         IN ('Line Discount Amount','NEW PRICE_LINE_DISC','LINE DISCOUNT PERCENT'))
          AND (UPPER (adj_man.adjustment_name) IN
                  ('AMOUNT_LINE_DISCOUNT'
                  ,'NEW PRICE'
                  ,'PERCENT_LINE_DISCOUNT'
                  ,'NEW_PRICE_DISCOUNT'))
          /*OR(  adj_man.automatic_flag='N' /*((SELECT SUM(oe1.adjusted_amount)
          FROM oe_price_adjustments_v oe1
          WHERE oe1.list_line_type_code        ='DIS'
          AND ol.line_id                       = oe1.line_id) + nvl(ol.unit_list_price,0) ) <> NVL(ol.unit_selling_price,0)
          )
          )*/
          AND oel.lookup_code(+) = adj_man.change_reason_code
          AND NVL (oel.lookup_type, 'CHANGE_CODE') = 'CHANGE_CODE';


