CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_tools_repair_v
(
   organization_code
  ,request_number
  ,date_opened
  ,item_description
  ,qty
  ,repair_number
  ,item
  ,repair_type
  ,account_number
  ,customer_name
  ,request_type
  ,employee_name
  ,status
  ,owner
  ,technician
  ,customer_job_name
  ,incident_id
  ,party_id
  ,cust_account_id
  ,site_use_id
  ,person_id
  ,effective_start_date
  ,effective_end_date
  ,repair_type_id
  ,inventory_item_id
  ,organization_id
  ,incident_type_id
  ,repair_line_id
  ,resource_id
)
AS
   SELECT mp.organization_code organization_code
         ,sr.incident_number request_number
         ,sr.incident_date date_opened
         ,item.description item_description
         ,dra.quantity qty
         ,dra.repair_number repair_number
         ,item.segment1 item
         ,rtype.name repair_type
         ,hca.account_number
         ,NVL (hca.account_name, hzp.party_name) customer_name
         ,citb.name request_type
         ,ppf.full_name employee_name
         ,crf.flow_status_meaning status
         ,jrre.source_name owner
         ,jrre1.source_name technician
         ,hzsu.location customer_job_name
         ,                                                      --Primary Keys
          sr.incident_id
         ,hzp.party_id
         ,hca.cust_account_id
         ,hzsu.site_use_id
         ,ppf.person_id
         ,ppf.effective_start_date
         ,ppf.effective_end_date
         ,rtype.repair_type_id
         ,item.inventory_item_id
         ,item.organization_id
         ,citb.incident_type_id
         ,dra.repair_line_id
         ,jrre.resource_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM csd_repairs dra
         ,cs_incidents_all_b sr
         ,csd_repair_types_vl rtype
         ,hz_cust_accounts hca
         ,hz_parties hzp
         ,mtl_system_items_b item
         ,mtl_parameters mp
         ,csd_repair_flow_statuses_v crf
         ,per_all_people_f ppf
         ,cs_incident_types_vl citb
         ,jtf_rs_resource_extns jrre
         ,jtf_rs_resource_extns jrre1
         ,hz_cust_site_uses_all hzsu                                       --,
    -- hz_cust_acct_sites hzas,
    --HZ_PARTY_SITES hzps
    WHERE     dra.incident_id = sr.incident_id
          AND dra.repair_type_id = rtype.repair_type_id
          AND sr.customer_id = hzp.party_id
          AND sr.account_id = hca.cust_account_id(+)
          AND dra.inventory_item_id = item.inventory_item_id
          AND item.organization_id = dra.inventory_org_id
          AND mp.organization_id = item.organization_id
          AND dra.flow_status_id = crf.flow_status_id(+)
          --AND dra.status            = fnd2.lookup_code(+)
          AND sr.employee_id = ppf.person_id(+)
          AND sr.incident_date BETWEEN ppf.effective_start_date(+)
                                   AND ppf.effective_end_date(+)
          --AND fnd2.lookup_type(+)   = 'CSD_REPAIR_FLOW_STATUS'
          AND citb.incident_type_id = sr.incident_type_id
          -- and dra.repair_line_id     = 11007
          AND sr.incident_owner_id = jrre.resource_id(+)
          AND dra.resource_id = jrre1.resource_id(+)
          AND sr.ship_to_site_use_id = hzsu.site_use_id(+)
--AND hzsu.cust_acct_site_id = hzas.cust_acct_site_id(+)
-- AND hzas.party_site_id     = hzps.party_site_id(+);;
;


