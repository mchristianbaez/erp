CREATE OR REPLACE FORCE VIEW xxeis.xxeis_wc_om_returns_rma_v
(
   order_number
  ,ordered_date
  ,line_number
  ,ordered_item
  ,shipment_number
  ,salesrep_name
  ,option_number
  ,service_number
  ,ordered_quantity
  ,ordered_quantity2
  ,order_quantity_uom
  ,ordered_quantity_uom2
  ,invoiced_quantity
  ,unit_selling_price
  ,request_date
  ,schedule_ship_date
  ,schedule_status_code
  ,cancelled_quantity
  ,cancelled_quantity2
  ,tax_code
  ,flow_status_code
  ,fulfillment_date
  ,unit_list_price
  ,calculate_price_flag
  ,pricing_quantity
  ,pricing_quantity_uom
  ,pricing_date
  ,promise_date
  ,shipping_quantity2
  ,shipping_quantity_uom
  ,customer_number
  ,customer
  ,customer_status
  ,price_list_name
  ,agreement_name
  ,concatenated_segments
  ,tax_value
  ,source_type_code
  ,shipment_priority_code
  ,freight_terms_code
  ,booked_flag
  ,cancelled_flag
  ,open_flag
  ,actual_shipment_date
  ,shippable_flag
  ,shipped_quantity
  ,shipped_quantity2
  ,fulfilled_flag
  ,transaction_type
  ,item_type_code
  ,fulfilled_quantity
  ,fulfilled_quantity2
  ,line_category_code
  ,org_id
  ,sold_to_org_id
  ,ship_from_org_id
  ,commitment_id
  ,line_type_id
  ,salesrep_id
  ,price_list_id
  ,return_context
  ,return_attribute1
  ,return_attribute2
  ,preferred_grade
  ,cust_po_number
  ,service_duration
  ,service_period
  ,service_start_date
  ,service_end_date
  ,last_update_date
  ,last_updated_by
  ,creation_date
  ,created_by
  ,shipping_quantity
  ,delivery_lead_time
  ,tax_exempt_flag
  ,tax_exempt_number
  ,tax_exempt_reason_code
  ,subinventory
  ,ship_to_org_id
  ,invoice_to_org_id
  ,deliver_to_org_id
  ,ship_to_contact_id
  ,deliver_to_contact_id
  ,invoice_to_contact_id
  ,ship_tolerance_above
  ,ship_tolerance_below
  ,demand_bucket_type_code
  ,rla_schedule_type_code
  ,customer_dock_code
  ,customer_job
  ,customer_production_line
  ,cust_model_serial_number
  ,project_id
  ,task_id
  ,tax_date
  ,tax_rate
  ,demand_class_code
  ,price_request_code
  ,shipping_method_code
  ,freight_carrier_code
  ,fob_point_code
  ,tax_point_code
  ,payment_term_id
  ,invoicing_rule_id
  ,accounting_rule_id
  ,accounting_rule_duration
  ,order_source_id
  ,original_system_reference
  ,change_sequence
  ,original_system_line_reference
  ,orig_sys_shipment_ref
  ,source_document_type_id
  ,source_document_id
  ,source_document_line_id
  ,reference_type
  ,reference_line_id
  ,item_revision
  ,sort_order
  ,option_flag
  ,dep_plan_required_flag
  ,visible_demand_flag
  ,intmed_ship_to_org_id
  ,intmed_ship_to_contact_id
  ,actual_arrival_date
  ,ato_line_id
  ,auto_selected_quantity
  ,component_number
  ,earliest_acceptable_date
  ,explosion_date
  ,latest_acceptable_date
  ,schedule_arrival_date
  ,ship_model_complete_flag
  ,re_source_flag
  ,return_reason_code
  ,arrival_set_id
  ,ship_set_id
  ,invoice_interface_status_code
  ,split_from_line_id
  ,line_set_id
  ,split_by
  ,cust_production_seq_num
  ,authorized_to_ship_flag
  ,veh_cus_item_cum_key_id
  ,over_ship_reason_code
  ,over_ship_resolved_flag
  ,item_identifier_type
  ,shipping_interfaced_flag
  ,credit_invoice_line_id
  ,first_ack_code
  ,first_ack_date
  ,last_ack_code
  ,last_ack_date
  ,planning_priority
  ,sold_from_org_id
  ,end_item_unit_number
  ,shipping_instructions
  ,packing_instructions
  ,reference_customer_trx_line_id
  ,service_txn_reason_code
  ,service_txn_comments
  ,service_coterminate_flag
  ,unit_list_percent
  ,unit_selling_percent
  ,unit_percent_base_price
  ,service_reference_type_code
  ,model_remnant_flag
  ,fulfillment_method_code
  ,revenue_amount
  ,customer_line_number
  ,customer_shipment_number
  ,customer_payment_term_id
  ,customer_item_net_price
  ,marketing_source_code_id
  ,upgraded_flag
  ,mfg_lead_time
  ,unit_selling_price_per_pqty
  ,original_inventory_item_id
  ,original_ordered_item_id
  ,original_ordered_item
  ,original_item_identifier_type
  ,item_substitution_type_code
  ,override_atp_date_code
  ,late_demand_penalty_factor
  ,transaction_phase_code
  ,original_list_price
  ,order_firmed_date
  ,actual_fulfillment_date
  ,charge_periodicity_code
  ,accepted_quantity
  ,accepted_by
  ,revrec_comments
  ,revrec_reference_document
  ,revrec_signature
  ,revrec_signature_date
  ,revrec_implicit_flag
  ,transaction_type_code
  ,inspection_required_flag
  ,currency_code
  ,operating_unit
  ,item_description
  ,party_id
  ,cust_account_id
  ,header_id
  ,line_id
  ,transaction_type_id
  ,list_header_id
  ,agreement_id
  ,inventory_item_id
  ,operating_unit_id
  ,ship_to
  ,ship_to_address1
  ,ship_to_address2
  ,ship_to_address3
  ,ship_to_address4
  ,ship_to_city
  ,ship_to_state
  ,ship_to_county
  ,ship_to_postal_code
  ,bill_to
  ,bill_to_address1
  ,bill_to_address2
  ,bill_to_address3
  ,bill_to_address4
  ,bill_to_city
  ,bill_to_state
  ,bill_to_county
  ,bill_to_postal_code
  ,rma_receipt_num
  ,receipt_date
  ,receiving_subinvnetory
  ,receiving_quantity
  ,uom_code
  ,warehouse
  ,shipment_header_id
  ,transaction_id
  ,shipping_org_id
  ,order_type
  ,extended_price
  ,rs_salesrep_id
  ,rs_org_id
  ,upd_by_user_id
  ,crea_by_user_id
  ,msi_organization_id
  ,ship_to_hcsu_site_use_id
  ,ship_to_hcas_site_id
  ,ship_to_hps_prty_ste_id
  ,ship_to_hl_locn_id
  ,bill_to_hcsu_site_use_id
  ,bill_to_hcas_cust_id
  ,bill_to_hps_prty_ste_id
  ,bill_to_hl_locn_id
  ,cust_acct#party_type
  ,cust_acct#vndr_code_and_frul
  ,cust_acct#branch_description
  ,cust_acct#customer_source
  ,cust_acct#legal_collection_i
  ,cust_acct#prism_number
  ,cust_acct#yes#note_line_#5
  ,cust_acct#yes#note_line_#1
  ,cust_acct#yes#note_line_#2
  ,cust_acct#yes#note_line_#3
  ,cust_acct#yes#note_line_#4
  ,bill_to_hcas#print_prices_on
  ,ship_to_hcas#print_prices_on
  ,bill_to_hcas#job_information
  ,ship_to_hcas#job_information
  ,ship_to_hcas#tax_exemption_t
  ,bill_to_hcas#tax_exemption_t
  ,bill_to_hcas#tax_exempt1
  ,ship_to_hcas#tax_exempt1
  ,bill_to_hcas#prism_number
  ,ship_to_hcas#prism_number
  ,ship_to_hcas#mandatory_po_nu
  ,bill_to_hcas#mandatory_po_nu
  ,bill_to_hcas#lien_release_da
  ,ship_to_hcas#lien_release_da
  ,bill_to_hcas#yes#notice_to_o
  ,ship_to_hcas#yes#notice_to_o
  ,ship_to_hcas#yes#notice_to_o1
  ,bill_to_hcas#yes#notice_to_o1
  ,bill_to_hcas#yes#notice_to_o2
  ,ship_to_hcas#yes#notice_to_o2
  ,ship_to_hcsu#customer_site_c
  ,bill_to_hcsu#customer_site_c
  ,bill_to_hcsu#government_fund
  ,ship_to_hcsu#government_fund
  ,bill_to_hcsu#thomas_guide_pa
  ,ship_to_hcsu#thomas_guide_pa
  ,ship_to_hcsu#dodge_number
  ,bill_to_hcsu#dodge_number
  ,bill_to_hcsu#future_use
  ,ship_to_hcsu#future_use
  ,bill_to_hcsu#salesrep_#2
  ,ship_to_hcsu#salesrep_#2
  ,ship_to_hcsu#salesrep_spilt_
  ,bill_to_hcsu#salesrep_spilt_
  ,bill_to_hcsu#salesrep_spilt_1
  ,ship_to_hcsu#salesrep_spilt_1
  ,bill_to_hcsu#permit_number
  ,ship_to_hcsu#permit_number
  ,ship_to_hl#pay_to_vendor_id
  ,bill_to_hl#pay_to_vendor_id
  ,bill_to_hl#lob
  ,ship_to_hl#lob
  ,ship_to_hl#pay_to_vendor_cod
  ,bill_to_hl#pay_to_vendor_cod
  ,party#party_type
  ,party#gvid_id
  ,bill_to_hps#rebt_pay_to_vndr
  ,ship_to_hps#rebt_pay_to_vndr
  ,ship_to_hps#rebt_lob
  ,bill_to_hps#rebt_lob
  ,bill_to_hps#rebt_vndr_code
  ,ship_to_hps#rebt_vndr_code
  ,ship_to_hps#rebt_vndr_flag
  ,bill_to_hps#rebt_vndr_flag
  ,ship_to_hps#rebt_vndr_tax_id
  ,bill_to_hps#rebt_vndr_tax_id
  ,mtp#factory_planner_data_dir
  ,mtp#fru
  ,mtp#location_number
  ,mtp#branch_operations_manage
  ,mtp#deliver_charge
  ,mtp#factory_planner_executab
  ,mtp#factory_planner_user
  ,mtp#factory_planner_host
  ,mtp#factory_planner_port_num
  ,mtp#pricing_zone
  ,mtp#org_type
  ,mtp#district
  ,mtp#region
  ,msi#hds#lob
  ,msi#hds#drop_shipment_eligab
  ,msi#hds#invoice_uom
  ,msi#hds#product_id
  ,msi#hds#vendor_part_number
  ,msi#hds#unspsc_code
  ,msi#hds#upc_primary
  ,msi#hds#sku_description
  ,msi#wc#ca_prop_65
  ,msi#wc#country_of_origin
  ,msi#wc#orm_d_flag
  ,msi#wc#store_velocity
  ,msi#wc#dc_velocity
  ,msi#wc#yearly_store_velocity
  ,msi#wc#yearly_dc_velocity
  ,msi#wc#prism_part_number
  ,msi#wc#hazmat_description
  ,msi#wc#hazmat_container
  ,msi#wc#gtp_indicator
  ,msi#wc#last_lead_time
  ,msi#wc#amu
  ,msi#wc#reserve_stock
  ,msi#wc#taxware_code
  ,msi#wc#average_units
  ,msi#wc#product_code
  ,msi#wc#import_duty_
  ,msi#wc#keep_item_active
  ,msi#wc#pesticide_flag
  ,msi#wc#calc_lead_time
  ,msi#wc#voc_gl
  ,msi#wc#pesticide_flag_state
  ,msi#wc#voc_category
  ,msi#wc#voc_sub_category
  ,msi#wc#msds_#
  ,msi#wc#hazmat_packaging_grou
  ,oh#rental_term
  ,oh#order_channel
  ,oh#picked_by
  ,oh#load_checked_by
  ,oh#delivered_by
  ,ol#estimated_return_date
  ,ol#rerent_po
  ,ol#force_ship
  ,ol#application_method
  ,ol#item_on_blowout
  ,ol#pof_std_line
  ,ol#rerental_billing_terms
  ,ol#pricing_guardrail
  ,ol#print_expired_product_dis
  ,ol#rental_charge
  ,ol#vendor_quote_cost
  ,ol#po_cost_for_vendor_quote
  ,ol#serial_number
  ,ol#engineering_cost
  ,qlhvl#auto_renewal
  ,qlhvl#until_year
  ,qlhvl#contract_name
  ,ship_head#printed
)
AS
   SELECT oh.order_number order_number
         ,oh.ordered_date
         ,TO_CHAR (ol.line_number) || '.' || TO_CHAR (ol.shipment_number) line_number
         ,ol.ordered_item ordered_item
         ,ol.shipment_number shipment_number
         ,rs.name salesrep_name
         ,ol.option_number option_number
         ,ol.service_number service_number
         --       Mahender
         ,CASE
             WHEN ott.name = 'RETURN WITH RECEIPT' OR ott.name = 'CREDIT ONLY'
             THEN
                (ol.ordered_quantity) * (-1)
             ELSE
                (ol.ordered_quantity) * (1)
          END
             ordered_quantity
         --, (ol.ordered_quantity) * (-1) ordered_quantity
         , (ol.ordered_quantity2) * (-1) ordered_quantity2
         ,order_quantity_uom.unit_of_measure order_quantity_uom
         ,ordered_quantity_uom2.unit_of_measure ordered_quantity_uom2
         ,ol.invoiced_quantity invoiced_quantity
         ,ol.unit_selling_price unit_selling_price
         ,ol.request_date request_date
         ,ol.schedule_ship_date schedule_ship_date
         ,ol.schedule_status_code schedule_status_code
         ,ol.cancelled_quantity cancelled_quantity
         ,ol.cancelled_quantity2 cancelled_quantity2
         ,ol.tax_code tax_code
         ,ol.flow_status_code flow_status_code
         ,ol.fulfillment_date fulfillment_date
         ,ol.unit_list_price unit_list_price
         ,DECODE (ol.calculate_price_flag
                 ,'N', 'Freeze Price'
                 ,'P', 'Partial Price'
                 ,'Y', 'Calculate Price'
                 ,NULL)
             calculate_price_flag
         , (ol.pricing_quantity) * (-1) pricing_quantity
         ,pricing_quantity_uom.unit_of_measure pricing_quantity_uom
         ,ol.pricing_date pricing_date
         ,ol.promise_date promise_date
         ,ol.shipping_quantity2 shipping_quantity2
         ,shipping_quantity_uom2.unit_of_measure shipping_quantity_uom
         ,cust_acct.account_number customer_number
         ,party.party_name customer
         ,DECODE (cust_acct.status, 'A', 'Active', 'Inactive') customer_status
         ,qlhvl.name price_list_name
         ,oea.name agreement_name
         ,msi.concatenated_segments concatenated_segments
         ,ol.tax_value tax_value
         ,DECODE (ol.source_type_code,  'EXTERNAL', 'External',  'INTERNAL', 'Internal',  NULL)
             source_type_code
         ,DECODE (ol.shipment_priority_code
                 ,'High', 'High Priority'
                 ,'Standard', 'Standard Priority'
                 ,NULL)
             shipment_priority_code
         ,ol.freight_terms_code freight_terms_code
         ,DECODE (ol.booked_flag,  'N', 'No',  'Y', 'Yes',  NULL) booked_flag
         ,DECODE (ol.cancelled_flag,  'N', 'No',  'Y', 'Yes',  NULL) cancelled_flag
         ,DECODE (ol.open_flag,  'N', 'No',  'Y', 'Yes',  NULL) open_flag
         ,ol.actual_shipment_date actual_shipment_date
         ,DECODE (ol.shippable_flag,  'N', 'No',  'Y', 'Yes',  NULL) shippable_flag
         ,ol.shipped_quantity shipped_quantity
         ,ol.shipped_quantity2 shipped_quantity2
         ,DECODE (ol.fulfilled_flag,  'N', 'No',  'Y', 'Yes',  NULL) fulfilled_flag
         ,ott.name transaction_type
         ,ol.item_type_code item_type_code
         ,ol.fulfilled_quantity fulfilled_quantity
         ,ol.fulfilled_quantity2 fulfilled_quantity2
         ,ol.line_category_code line_category_code
         ,ol.org_id org_id
         ,ol.sold_to_org_id sold_to_org_id
         ,ol.ship_from_org_id ship_from_org_id
         ,ol.commitment_id commitment_id
         ,ol.line_type_id line_type_id
         ,ol.salesrep_id salesrep_id
         ,ol.price_list_id price_list_id
         ,ol.return_context return_context
         ,ol.return_attribute1 return_attribute1
         ,ol.return_attribute2 return_attribute2
         ,ol.preferred_grade preferred_grade
         ,ol.cust_po_number cust_po_number
         ,ol.service_duration service_duration
         ,ol.service_period service_period
         ,ol.service_start_date service_start_date
         ,ol.service_end_date service_end_date
         ,ol.last_update_date last_update_date
         ,                            --eis_rs_utility.get_fnd_user_name(ol.last_updated_by)  last_updated_by,
          upd_by.user_name last_updated_by
         ,ol.creation_date creation_date
         ,                                    -- eis_rs_utility.get_fnd_user_name(ol.created_by)   created_by,
          crea_by.user_name created_by
         ,ol.shipping_quantity shipping_quantity
         ,ol.delivery_lead_time delivery_lead_time
         ,ol.tax_exempt_flag tax_exempt_flag
         ,ol.tax_exempt_number tax_exempt_number
         ,ol.tax_exempt_reason_code tax_exempt_reason_code
         ,ol.subinventory subinventory
         ,ol.ship_to_org_id ship_to_org_id
         ,ol.invoice_to_org_id invoice_to_org_id
         ,ol.deliver_to_org_id deliver_to_org_id
         ,ol.ship_to_contact_id ship_to_contact_id
         ,ol.deliver_to_contact_id deliver_to_contact_id
         ,ol.invoice_to_contact_id invoice_to_contact_id
         ,ol.ship_tolerance_above ship_tolerance_above
         ,ol.ship_tolerance_below ship_tolerance_below
         ,ol.demand_bucket_type_code demand_bucket_type_code
         ,ol.rla_schedule_type_code rla_schedule_type_code
         ,ol.customer_dock_code customer_dock_code
         ,ol.customer_job customer_job
         ,ol.customer_production_line customer_production_line
         ,ol.cust_model_serial_number cust_model_serial_number
         ,ol.project_id project_id
         ,ol.task_id task_id
         ,ol.tax_date tax_date
         ,ol.tax_rate tax_rate
         ,ol.demand_class_code demand_class_code
         ,ol.price_request_code price_request_code
         ,DECODE (ol.shipping_method_code,  'DEL', 'Delivery',  'PS', 'Pick Slip',  NULL)
             shipping_method_code
         ,ol.freight_carrier_code freight_carrier_code
         ,ol.fob_point_code fob_point_code
         ,DECODE (ol.tax_point_code, 'INVOICE', 'AT INVOICING', NULL) tax_point_code
         ,ol.payment_term_id payment_term_id
         ,ol.invoicing_rule_id invoicing_rule_id
         ,ol.accounting_rule_id accounting_rule_id
         ,ol.accounting_rule_duration accounting_rule_duration
         ,ol.order_source_id order_source_id
         ,ol.orig_sys_document_ref original_system_reference
         ,ol.change_sequence change_sequence
         ,ol.orig_sys_line_ref original_system_line_reference
         ,ol.orig_sys_shipment_ref orig_sys_shipment_ref
         ,ol.source_document_type_id source_document_type_id
         ,ol.source_document_id source_document_id
         ,ol.source_document_line_id source_document_line_id
         ,ol.reference_type reference_type
         ,ol.reference_line_id reference_line_id
         ,ol.item_revision item_revision
         ,ol.sort_order sort_order
         ,DECODE (ol.option_flag,  'N', 'No',  'Y', 'Yes',  NULL) option_flag
         ,DECODE (ol.dep_plan_required_flag,  'N', 'No',  'Y', 'Yes',  NULL) dep_plan_required_flag
         ,DECODE (ol.visible_demand_flag,  'N', 'No',  'Y', 'Yes',  NULL) visible_demand_flag
         ,ol.intmed_ship_to_org_id intmed_ship_to_org_id
         ,ol.intmed_ship_to_contact_id intmed_ship_to_contact_id
         ,ol.actual_arrival_date actual_arrival_date
         ,ol.ato_line_id ato_line_id
         ,ol.auto_selected_quantity auto_selected_quantity
         ,ol.component_number component_number
         ,ol.earliest_acceptable_date earliest_acceptable_date
         ,ol.explosion_date explosion_date
         ,ol.latest_acceptable_date latest_acceptable_date
         ,ol.schedule_arrival_date schedule_arrival_date
         ,DECODE (ol.ship_model_complete_flag,  'N', 'No',  'Y', 'Yes',  NULL) ship_model_complete_flag
         ,DECODE (ol.re_source_flag,  'N', 'No',  'Y', 'Yes',  NULL) re_source_flag
         ,ol.return_reason_code return_reason_code
         ,ol.arrival_set_id arrival_set_id
         ,ol.ship_set_id ship_set_id
         ,ol.invoice_interface_status_code invoice_interface_status_code
         ,ol.split_from_line_id split_from_line_id
         ,ol.line_set_id line_set_id
         ,ol.split_by split_by
         ,ol.cust_production_seq_num cust_production_seq_num
         ,DECODE (ol.authorized_to_ship_flag,  'N', 'No',  'Y', 'Yes',  NULL) authorized_to_ship_flag
         ,ol.veh_cus_item_cum_key_id veh_cus_item_cum_key_id
         ,DECODE (ol.over_ship_reason_code
                 ,'CUST_APPROVAL', 'Customer Approved Shipping Over Tolerance'
                 ,'PACK_RESTRICT', 'Packaging Restrictions'
                 ,'SHIP_ERROR', 'Shipment Error'
                 ,'SHIP_OVERAGE', 'Shipper Overage'
                 ,NULL)
             over_ship_reason_code
         ,DECODE (ol.over_ship_resolved_flag,  'N', 'No',  'Y', 'Yes',  NULL) over_ship_resolved_flag
         ,ol.item_identifier_type item_identifier_type
         ,DECODE (ol.shipping_interfaced_flag,  'N', 'No',  'Y', 'Yes',  NULL) shipping_interfaced_flag
         ,ol.credit_invoice_line_id credit_invoice_line_id
         ,ol.first_ack_code first_ack_code
         ,ol.first_ack_date first_ack_date
         ,ol.last_ack_code last_ack_code
         ,ol.last_ack_date last_ack_date
         ,ol.planning_priority planning_priority
         ,ol.sold_from_org_id sold_from_org_id
         ,ol.end_item_unit_number end_item_unit_number
         ,ol.shipping_instructions shipping_instructions
         ,ol.packing_instructions packing_instructions
         ,ol.reference_customer_trx_line_id reference_customer_trx_line_id
         ,ol.service_txn_reason_code service_txn_reason_code
         ,ol.service_txn_comments service_txn_comments
         ,DECODE (ol.service_coterminate_flag,  'N', 'No',  'Y', 'Yes',  NULL) service_coterminate_flag
         ,ol.unit_list_percent unit_list_percent
         ,ol.unit_selling_percent unit_selling_percent
         ,ol.unit_percent_base_price unit_percent_base_price
         ,DECODE (ol.service_reference_type_code
                 ,'CUSTOMER_PRODUCT', 'Customer Product'
                 ,'ORDER', 'Order'
                 ,NULL)
             service_reference_type_code
         ,DECODE (ol.model_remnant_flag,  'N', 'No',  'Y', 'Yes',  NULL) model_remnant_flag
         ,ol.fulfillment_method_code fulfillment_method_code
         ,ol.revenue_amount revenue_amount
         ,ol.customer_line_number customer_line_number
         ,ol.customer_shipment_number customer_shipment_number
         ,ol.customer_payment_term_id customer_payment_term_id
         ,ol.customer_item_net_price customer_item_net_price
         ,ol.marketing_source_code_id marketing_source_code_id
         ,ol.upgraded_flag upgraded_flag
         ,ol.mfg_lead_time mfg_lead_time
         ,ol.unit_selling_price_per_pqty unit_selling_price_per_pqty
         ,ol.original_inventory_item_id original_inventory_item_id
         ,ol.original_ordered_item_id original_ordered_item_id
         ,ol.original_ordered_item original_ordered_item
         ,ol.original_item_identifier_type original_item_identifier_type
         ,ol.item_substitution_type_code item_substitution_type_code
         ,ol.override_atp_date_code override_atp_date_code
         ,ol.late_demand_penalty_factor late_demand_penalty_factor
         ,ol.transaction_phase_code transaction_phase_code
         ,ol.original_list_price original_list_price
         ,ol.order_firmed_date order_firmed_date
         ,ol.actual_fulfillment_date actual_fulfillment_date
         ,ol.charge_periodicity_code charge_periodicity_code
         ,ol.accepted_quantity accepted_quantity
         ,ol.accepted_by accepted_by
         ,ol.revrec_comments revrec_comments
         ,ol.revrec_reference_document revrec_reference_document
         ,ol.revrec_signature revrec_signature
         ,ol.revrec_signature_date revrec_signature_date
         ,ol.revrec_implicit_flag revrec_implicit_flag
         ,ott.transaction_type_code transaction_type_code
         ,ott.inspection_required_flag inspection_required_flag
         ,ott.currency_code currency_code
         ,hou.name operating_unit
         ,ol.user_item_description item_description
         ,                                                                                      --Primary Keys
          party.party_id party_id
         ,cust_acct.cust_account_id cust_account_id
         ,oh.header_id header_id
         ,ol.line_id line_id
         ,ott.transaction_type_id transaction_type_id
         ,qlhvl.list_header_id list_header_id
         ,oea.agreement_id agreement_id
         ,msi.inventory_item_id inventory_item_id
         ,hou.organization_id operating_unit_id
         ,ship_to_hcsu.location ship_to
         ,ship_to_hl.address1 ship_to_address1
         ,ship_to_hl.address2 ship_to_address2
         ,ship_to_hl.address3 ship_to_address3
         ,ship_to_hl.address4 ship_to_address4
         ,ship_to_hl.city ship_to_city
         ,ship_to_hl.state ship_to_state
         ,ship_to_hl.county ship_to_county
         ,ship_to_hl.postal_code ship_to_postal_code
         ,bill_to_hcsu.location bill_to
         ,bill_to_hl.address1 bill_to_address1
         ,bill_to_hl.address2 bill_to_address2
         ,bill_to_hl.address3 bill_to_address3
         ,bill_to_hl.address4 bill_to_address4
         ,bill_to_hl.city bill_to_city
         ,bill_to_hl.state bill_to_state
         ,bill_to_hl.county bill_to_county
         ,bill_to_hl.postal_code bill_to_postal_code
         ,ship_head.receipt_num rma_receipt_num
         ,rcv.transaction_date receipt_date
         ,rcv.subinventory receiving_subinvnetory
         ,rcv.primary_quantity receiving_quantity
         ,rcv.uom_code
         ,mtp.organization_code warehouse
         ,rcv.shipment_header_id
         ,rcv.transaction_id
         ,mtp.organization_id shipping_org_id
         ,oetv.name order_type
         -- Mahender
         ,CASE
             WHEN ott.name = 'RETURN WITH RECEIPT' OR ott.name = 'CREDIT ONLY'
             THEN
                (ol.unit_selling_price * ordered_quantity) * (-1)
             ELSE
                (ol.unit_selling_price * ordered_quantity) * (1)
          END
             extended_price
         --    , (ol.unit_selling_price * ordered_quantity) * (-1) extended_price
         ,rs.salesrep_id rs_salesrep_id
         ,rs.org_id rs_org_id
         ,upd_by.user_id upd_by_user_id
         ,crea_by.user_id crea_by_user_id
         ,msi.organization_id msi_organization_id
         ,ship_to_hcsu.site_use_id ship_to_hcsu_site_use_id
         ,ship_to_hcas.cust_acct_site_id ship_to_hcas_site_id
         ,ship_to_hps.party_site_id ship_to_hps_prty_ste_id
         ,ship_to_hl.location_id ship_to_hl_locn_id
         ,bill_to_hcsu.site_use_id bill_to_hcsu_site_use_id
         ,bill_to_hcas.cust_acct_site_id bill_to_hcas_cust_id
         ,bill_to_hps.party_site_id bill_to_hps_prty_ste_id
         ,bill_to_hl.location_id bill_to_hl_locn_id
         --descr#flexfield#start

         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_PARTY_TYPE', cust_acct.attribute1, 'I')
             cust_acct#party_type
         ,cust_acct.attribute2 cust_acct#vndr_code_and_frul
         ,cust_acct.attribute3 cust_acct#branch_description
         ,cust_acct.attribute4 cust_acct#customer_source
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_LEGAL_COLLECTION', cust_acct.attribute5, 'F')
             cust_acct#legal_collection_i
         ,cust_acct.attribute6 cust_acct#prism_number
         ,DECODE (cust_acct.attribute_category, 'Yes', cust_acct.attribute16, NULL)
             cust_acct#yes#note_line_#5
         ,DECODE (cust_acct.attribute_category, 'Yes', cust_acct.attribute17, NULL)
             cust_acct#yes#note_line_#1
         ,DECODE (cust_acct.attribute_category, 'Yes', cust_acct.attribute18, NULL)
             cust_acct#yes#note_line_#2
         ,DECODE (cust_acct.attribute_category, 'Yes', cust_acct.attribute19, NULL)
             cust_acct#yes#note_line_#3
         ,DECODE (cust_acct.attribute_category, 'Yes', cust_acct.attribute20, NULL)
             cust_acct#yes#note_line_#4
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_PRINT_PRICES', bill_to_hcas.attribute1, 'F')
             bill_to_hcas#print_prices_on
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_PRINT_PRICES', ship_to_hcas.attribute1, 'F')
             ship_to_hcas#print_prices_on
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', bill_to_hcas.attribute14, 'F')
             bill_to_hcas#job_information
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ship_to_hcas.attribute14, 'F')
             ship_to_hcas#job_information
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_TAX_EXEMPTION_TYPE', ship_to_hcas.attribute15, 'I')
             ship_to_hcas#tax_exemption_t
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_TAX_EXEMPTION_TYPE', bill_to_hcas.attribute15, 'I')
             bill_to_hcas#tax_exemption_t
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', bill_to_hcas.attribute16, 'F') bill_to_hcas#tax_exempt1
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ship_to_hcas.attribute16, 'F') ship_to_hcas#tax_exempt1
         ,bill_to_hcas.attribute17 bill_to_hcas#prism_number
         ,ship_to_hcas.attribute17 ship_to_hcas#prism_number
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ship_to_hcas.attribute3, 'F')
             ship_to_hcas#mandatory_po_nu
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', bill_to_hcas.attribute3, 'F')
             bill_to_hcas#mandatory_po_nu
         ,bill_to_hcas.attribute5 bill_to_hcas#lien_release_da
         ,ship_to_hcas.attribute5 ship_to_hcas#lien_release_da
         ,DECODE (bill_to_hcas.attribute_category, 'Yes', bill_to_hcas.attribute18, NULL)
             bill_to_hcas#yes#notice_to_o
         ,DECODE (ship_to_hcas.attribute_category, 'Yes', ship_to_hcas.attribute18, NULL)
             ship_to_hcas#yes#notice_to_o
         ,DECODE (ship_to_hcas.attribute_category, 'Yes', ship_to_hcas.attribute19, NULL)
             ship_to_hcas#yes#notice_to_o1
         ,DECODE (bill_to_hcas.attribute_category, 'Yes', bill_to_hcas.attribute19, NULL)
             bill_to_hcas#yes#notice_to_o1
         ,DECODE (bill_to_hcas.attribute_category, 'Yes', bill_to_hcas.attribute20, NULL)
             bill_to_hcas#yes#notice_to_o2
         ,DECODE (ship_to_hcas.attribute_category, 'Yes', ship_to_hcas.attribute20, NULL)
             ship_to_hcas#yes#notice_to_o2
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_CUSTOMER_SITE_CLASS', ship_to_hcsu.attribute1, 'F')
             ship_to_hcsu#customer_site_c
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_CUSTOMER_SITE_CLASS', bill_to_hcsu.attribute1, 'F')
             bill_to_hcsu#customer_site_c
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', bill_to_hcsu.attribute2, 'F')
             bill_to_hcsu#government_fund
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ship_to_hcsu.attribute2, 'F')
             ship_to_hcsu#government_fund
         ,bill_to_hcsu.attribute3 bill_to_hcsu#thomas_guide_pa
         ,ship_to_hcsu.attribute3 ship_to_hcsu#thomas_guide_pa
         ,ship_to_hcsu.attribute4 ship_to_hcsu#dodge_number
         ,bill_to_hcsu.attribute4 bill_to_hcsu#dodge_number
         ,bill_to_hcsu.attribute5 bill_to_hcsu#future_use
         ,ship_to_hcsu.attribute5 ship_to_hcsu#future_use
         ,xxeis.eis_rs_dff.decode_valueset ('OM: Salesreps', bill_to_hcsu.attribute6, 'F')
             bill_to_hcsu#salesrep_#2
         ,xxeis.eis_rs_dff.decode_valueset ('OM: Salesreps', ship_to_hcsu.attribute6, 'F')
             ship_to_hcsu#salesrep_#2
         ,ship_to_hcsu.attribute7 ship_to_hcsu#salesrep_spilt_
         ,bill_to_hcsu.attribute7 bill_to_hcsu#salesrep_spilt_
         ,bill_to_hcsu.attribute8 bill_to_hcsu#salesrep_spilt_1
         ,ship_to_hcsu.attribute8 ship_to_hcsu#salesrep_spilt_1
         ,bill_to_hcsu.attribute9 bill_to_hcsu#permit_number
         ,ship_to_hcsu.attribute9 ship_to_hcsu#permit_number
         ,ship_to_hl.attribute1 ship_to_hl#pay_to_vendor_id
         ,bill_to_hl.attribute1 bill_to_hl#pay_to_vendor_id
         ,bill_to_hl.attribute2 bill_to_hl#lob
         ,ship_to_hl.attribute2 ship_to_hl#lob
         ,ship_to_hl.attribute3 ship_to_hl#pay_to_vendor_cod
         ,bill_to_hl.attribute3 bill_to_hl#pay_to_vendor_cod
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_PARTY_TYPE', party.attribute1, 'I') party#party_type
         ,party.attribute2 party#gvid_id
         ,bill_to_hps.attribute2 bill_to_hps#rebt_pay_to_vndr
         ,ship_to_hps.attribute2 ship_to_hps#rebt_pay_to_vndr
         ,ship_to_hps.attribute3 ship_to_hps#rebt_lob
         ,bill_to_hps.attribute3 bill_to_hps#rebt_lob
         ,bill_to_hps.attribute4 bill_to_hps#rebt_vndr_code
         ,ship_to_hps.attribute4 ship_to_hps#rebt_vndr_code
         ,ship_to_hps.attribute5 ship_to_hps#rebt_vndr_flag
         ,bill_to_hps.attribute5 bill_to_hps#rebt_vndr_flag
         ,ship_to_hps.attribute6 ship_to_hps#rebt_vndr_tax_id
         ,bill_to_hps.attribute6 bill_to_hps#rebt_vndr_tax_id
         ,mtp.attribute1 mtp#factory_planner_data_dir
         ,mtp.attribute10 mtp#fru
         ,mtp.attribute11 mtp#location_number
         ,xxeis.eis_rs_dff.decode_valueset ('HR_DE_EMPLOYEES', mtp.attribute13, 'F')
             mtp#branch_operations_manage
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DELIVERY_CHARGE_EXEMPT', mtp.attribute14, 'I')
             mtp#deliver_charge
         ,mtp.attribute2 mtp#factory_planner_executab
         ,mtp.attribute3 mtp#factory_planner_user
         ,mtp.attribute4 mtp#factory_planner_host
         ,mtp.attribute5 mtp#factory_planner_port_num
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_PRICE_ZONES', mtp.attribute6, 'F') mtp#pricing_zone
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_ORG_TYPE', mtp.attribute7, 'I') mtp#org_type
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DISTRICT', mtp.attribute8, 'I') mtp#district
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_REGION', mtp.attribute9, 'I') mtp#region
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL) msi#hds#lob
         ,DECODE (msi.attribute_category
                 ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F')
                 ,NULL)
             msi#hds#drop_shipment_eligab
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL) msi#hds#invoice_uom
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL) msi#hds#product_id
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL) msi#hds#vendor_part_number
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL) msi#hds#unspsc_code
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL) msi#hds#upc_primary
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL) msi#hds#sku_description
         ,DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL) msi#wc#ca_prop_65
         ,DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL) msi#wc#country_of_origin
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute11, 'F')
                 ,NULL)
             msi#wc#orm_d_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL) msi#wc#store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL) msi#wc#dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL) msi#wc#yearly_store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL) msi#wc#yearly_dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL) msi#wc#prism_part_number
         ,DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL) msi#wc#hazmat_description
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msi.attribute18, 'I')
                 ,NULL)
             msi#wc#hazmat_container
         ,DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL) msi#wc#gtp_indicator
         ,DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL) msi#wc#last_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL) msi#wc#amu
         ,DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL) msi#wc#reserve_stock
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msi.attribute22, 'I')
                 ,NULL)
             msi#wc#taxware_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL) msi#wc#average_units
         ,DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL) msi#wc#product_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL) msi#wc#import_duty_
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute29, 'F')
                 ,NULL)
             msi#wc#keep_item_active
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute3, 'F')
                 ,NULL)
             msi#wc#pesticide_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL) msi#wc#calc_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL) msi#wc#voc_gl
         ,DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL) msi#wc#pesticide_flag_state
         ,DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL) msi#wc#voc_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL) msi#wc#voc_sub_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL) msi#wc#msds_#
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC_HAZMAT_PACKAGE_GROUP', msi.attribute9, 'I')
                 ,NULL)
             msi#wc#hazmat_packaging_grou
         ,xxeis.eis_rs_dff.decode_valueset ('RENTAL TYPE', oh.attribute1, 'I') oh#rental_term
         ,xxeis.eis_rs_dff.decode_valueset ('WC_CUS_ORDER_CHANNEL', oh.attribute2, 'I') oh#order_channel
         ,oh.attribute3 oh#picked_by
         ,oh.attribute4 oh#load_checked_by
         ,oh.attribute5 oh#delivered_by
         ,ol.attribute1 ol#estimated_return_date
         ,ol.attribute10 ol#rerent_po
         ,ol.attribute11 ol#force_ship
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_CSP_APP_METHOD', ol.attribute13, 'I') ol#application_method
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ol.attribute15, 'F') ol#item_on_blowout
         ,ol.attribute19 ol#pof_std_line
         ,xxeis.eis_rs_dff.decode_valueset ('ReRental Billing Type', ol.attribute2, 'I')
             ol#rerental_billing_terms
         ,ol.attribute20 ol#pricing_guardrail
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ol.attribute3, 'F') ol#print_expired_product_dis
         ,ol.attribute4 ol#rental_charge
         ,ol.attribute5 ol#vendor_quote_cost
         ,ol.attribute6 ol#po_cost_for_vendor_quote
         ,ol.attribute7 ol#serial_number
         ,ol.attribute8 ol#engineering_cost
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', qlhvl.attribute1, 'F') qlhvl#auto_renewal
         ,qlhvl.attribute2 qlhvl#until_year
         ,qlhvl.attribute4 qlhvl#contract_name
         ,ship_head.attribute1 ship_head#printed
     --descr#flexfield#end


     --gl#accountff#start
     --gl#accountff#end
     FROM hz_parties party
         ,hz_cust_accounts cust_acct
         ,oe_order_headers oh
         ,oe_order_lines ol
         ,oe_agreements oea
         ,oe_transaction_types_vl ott
         ,qp_list_headers_vl qlhvl
         ,mtl_parameters mtp
         ,mtl_system_items_kfv msi
         ,mtl_units_of_measure_vl order_quantity_uom
         ,mtl_units_of_measure_vl pricing_quantity_uom
         ,mtl_units_of_measure_vl ordered_quantity_uom2
         ,mtl_units_of_measure_vl shipping_quantity_uom2
         ,hr_operating_units hou
         ,hz_cust_site_uses ship_to_hcsu
         ,hz_cust_acct_sites ship_to_hcas
         ,hz_party_sites ship_to_hps
         ,hz_locations ship_to_hl
         ,hz_cust_site_uses bill_to_hcsu
         ,hz_cust_acct_sites bill_to_hcas
         ,hz_party_sites bill_to_hps
         ,hz_locations bill_to_hl
         ,ra_salesreps rs
         ,rcv_transactions rcv
         ,rcv_shipment_headers ship_head
         ,oe_order_types_v oetv
         ,fnd_user crea_by
         ,fnd_user upd_by
    WHERE     oh.header_id = ol.header_id
          AND ol.line_type_id = ott.transaction_type_id
          AND ol.agreement_id = oea.agreement_id(+)
          AND ol.price_list_id = qlhvl.list_header_id(+)
          AND ol.sold_to_org_id = cust_acct.cust_account_id(+)
          AND party.party_id(+) = cust_acct.party_id
          AND ol.inventory_item_id = msi.inventory_item_id(+)
          AND msi.organization_id = ol.ship_from_org_id
          AND ol.order_quantity_uom = order_quantity_uom.uom_code(+)
          AND ol.ordered_quantity_uom2 = ordered_quantity_uom2.uom_code(+)
          AND ol.pricing_quantity_uom = pricing_quantity_uom.uom_code(+)
          AND ol.shipping_quantity_uom2 = shipping_quantity_uom2.uom_code(+)
          AND oh.org_id = hou.organization_id
          AND ol.ship_to_org_id = ship_to_hcsu.site_use_id
          AND ship_to_hcsu.cust_acct_site_id = ship_to_hcas.cust_acct_site_id
          AND ship_to_hcas.party_site_id = ship_to_hps.party_site_id
          AND ship_to_hps.location_id = ship_to_hl.location_id
          AND party.party_id = ship_to_hps.party_id
          AND ol.invoice_to_org_id = bill_to_hcsu.site_use_id
          AND bill_to_hcsu.cust_acct_site_id = bill_to_hcas.cust_acct_site_id
          AND bill_to_hcas.party_site_id = bill_to_hps.party_site_id
          AND bill_to_hps.location_id = bill_to_hl.location_id
          AND party.party_id = bill_to_hps.party_id
          AND oh.salesrep_id = rs.salesrep_id(+)
          --          AND ol.line_category_code = 'RETURN' --Commented by Mahender for TMS#20150320-00148 on 18-May-2015
          AND rcv.oe_order_line_id(+) = ol.line_id
          AND source_document_code(+) = 'RMA'
          AND rcv.shipment_header_id = ship_head.shipment_header_id(+)
          AND ol.ship_from_org_id = mtp.organization_id(+)
          AND oh.order_type_id = oetv.order_type_id
          --          AND rcv.transaction_type = 'RECEIVE' --Commented by Mahender for TMS#20150320-00148 on 18-May-2015
          AND ol.created_by = crea_by.user_id
          AND ol.last_updated_by = upd_by.user_id;
