CREATE OR REPLACE FORCE VIEW xxeis.xxeis_open_invoices
(
   org_id
  ,gl_date
  ,vendor_number
  ,vendor_name
  ,vendor_site
  ,invoice_type
  ,invoice_number
  ,invoice_date
  ,invoice_amount
  ,description
  ,deduction_code
  ,case_identifier
  ,employee
  ,vendor_type
  ,pay_group
  ,payment_status
  ,cancelled_date
  ,invoice_approval_status
)
AS
   SELECT i.org_id org_id
         ,i.gl_date gl_date
         ,v.segment1 vendor_number
         ,v.vendor_name vendor_name
         ,s.vendor_site_code vendor_site
         ,i.invoice_type_lookup_code invoice_type
         ,i.invoice_num invoice_number
         ,i.invoice_date invoice_date
         ,i.invoice_amount invoice_amount
         ,i.description description
         ,i.attribute2 deduction_code
         ,i.attribute1 case_identifier
         ,i.attribute3 employee
         ,v.vendor_type_lookup_code vendor_type
         ,i.pay_group_lookup_code pay_group
         ,i.payment_status_flag payment_status
         ,i.cancelled_date cancelled_date
         ,CASE d.accrual_posted_flag
             WHEN 'Y' THEN 'Validated'
             WHEN 'N' THEN 'Needs Re-Validation'
          END
             invoice_approval_status
     FROM ap.ap_invoices_all i
         ,ap.ap_invoice_distributions_all d
         ,ap.ap_suppliers v
         ,ap.ap_supplier_sites_all s
    WHERE     i.vendor_id = v.vendor_id
          AND i.vendor_site_id = s.vendor_site_id
          AND i.invoice_id = d.invoice_id;


