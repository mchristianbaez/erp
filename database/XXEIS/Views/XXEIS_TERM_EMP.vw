CREATE OR REPLACE FORCE VIEW xxeis.xxeis_term_emp
(
   employee_number
  ,segment1
  ,name
  ,process_flag
  ,last_day_paid
  ,creation_date
  ,supervisor
)
AS
   SELECT DISTINCT
          hrs.stg_employee_number employee_number
         ,gcc.segment1 segment1
         ,hrs.stg_last_name || ', ' || hrs.stg_first_name name
         ,hrs.stg_process_flag process_flag
         ,hrs.stg_last_day_paid last_day_paid
         ,hrs.creation_date creation_date
         , (SELECT sup.last_name || ', ' || first_name
              FROM hr.per_all_people_f sup
             WHERE     sup.employee_number = TO_CHAR (hrs.supervisor_id)
                   AND sup.effective_end_date =
                          (SELECT MAX (f.effective_end_date)
                             FROM hr.per_all_assignments_f f
                            WHERE sup.person_id = f.person_id))
             supervisor
     FROM xxcus.xxcushr_interface_tbl hrs
         ,hr.per_all_people_f ppf
         ,hr.per_all_assignments_f ppa
         ,gl.gl_code_combinations gcc
    WHERE     hrs.stg_employee_number = ppf.employee_number
          AND ppf.person_id = ppa.person_id
          AND ppa.default_code_comb_id = gcc.code_combination_id
          AND stg_process_flag = 'T'
          AND ppa.effective_end_date = (SELECT MAX (f.effective_end_date)
                                          FROM hr.per_all_assignments_f f
                                         WHERE ppa.person_id = f.person_id);


