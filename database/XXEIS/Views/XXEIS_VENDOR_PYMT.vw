CREATE OR REPLACE FORCE VIEW xxeis.xxeis_vendor_pymt
(
   vendor_name
  ,vendor_number
  ,vendor_site
  ,total_disbursement_amount
  ,check_date
  ,org_id
  ,bank_account_name
)
AS
   SELECT v.vendor_name vendor_name
         ,v.segment1 vendor_number
         ,s.vendor_site_code vendor_site
         ,c.amount total_disbursement_amount
         ,c.check_date check_date
         ,c.org_id org_id
         ,c.bank_account_name bank_account_name
     FROM ap.ap_suppliers v, ap.ap_supplier_sites_all s, ap.ap_checks_all c
    WHERE     c.vendor_id = v.vendor_id
          AND v.vendor_id = s.vendor_id
          AND s.vendor_site_id = c.vendor_site_id;


