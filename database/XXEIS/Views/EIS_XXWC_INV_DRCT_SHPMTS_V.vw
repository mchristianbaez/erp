CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_inv_drct_shpmts_v
(
   req_number
  ,end_date
  ,period_name
  ,location
  ,organization_id
  ,branch_number
  ,segment_location
  ,po_number
  ,received_date
  ,po_cost
  ,order_number
  ,header_id
  ,line_id
  ,application_id
  ,set_of_books_id
  ,code_combination_id
  ,gcc#branch
  ,oh#rental_term
  ,oh#order_channel
  ,oh#picked_by
  ,oh#load_checked_by
  ,oh#delivered_by
  ,ol#estimated_return_date
  ,ol#rerent_po
  ,ol#force_ship
  ,ol#application_method
  ,ol#item_on_blowout
  ,ol#pof_std_line
  ,ol#rerental_billing_terms
  ,ol#pricing_guardrail
  ,ol#print_expired_product_dis
  ,ol#rental_charge
  ,ol#vendor_quote_cost
  ,ol#po_cost_for_vendor_quote
  ,ol#serial_number
  ,ol#engineering_cost
  ,gcc#50328#account
  ,gcc#50328#account#descr
  ,gcc#50328#cost_center
  ,gcc#50328#cost_center#descr
  ,gcc#50328#furture_use
  ,gcc#50328#furture_use#descr
  ,gcc#50328#future_use_2
  ,gcc#50328#future_use_2#descr
  ,gcc#50328#location
  ,gcc#50328#location#descr
  ,gcc#50328#product
  ,gcc#50328#product#descr
  ,gcc#50328#project_code
  ,gcc#50328#project_code#descr
  ,gcc#50368#account
  ,gcc#50368#account#descr
  ,gcc#50368#department
  ,gcc#50368#department#descr
  ,gcc#50368#division
  ,gcc#50368#division#descr
  ,gcc#50368#future_use
  ,gcc#50368#future_use#descr
  ,gcc#50368#product
  ,gcc#50368#product#descr
  ,gcc#50368#subaccount
  ,gcc#50368#subaccount#descr
)
AS
   SELECT DISTINCT
          req_h.segment1 req_number
         ,gps.end_date
         ,gps.period_name
         ,SUBSTR (gcc.segment2, 3) location
         ,mtp.organization_id
         ,mtp.organization_code branch_number
         ,gcc.segment2 segment_location
         ,ph.segment1 po_number
         ,TRUNC (rsh.creation_date) received_date
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_po_cost (
             od.requisition_header_id
            ,od.po_header_id)
             po_cost
         ,oh.order_number
         ,                                                     ---Primary Keys
          oh.header_id
         ,ol.line_id
         ,gps.application_id
         ,gps.set_of_books_id
         ,gcc.code_combination_id
         --descr#flexfield#start

         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I')
             gcc#branch
         ,xxeis.eis_rs_dff.decode_valueset ('RENTAL TYPE'
                                           ,oh.attribute1
                                           ,'I')
             oh#rental_term
         ,xxeis.eis_rs_dff.decode_valueset ('WC_CUS_ORDER_CHANNEL'
                                           ,oh.attribute2
                                           ,'I')
             oh#order_channel
         ,oh.attribute3 oh#picked_by
         ,oh.attribute4 oh#load_checked_by
         ,oh.attribute5 oh#delivered_by
         ,ol.attribute1 ol#estimated_return_date
         ,ol.attribute10 ol#rerent_po
         ,ol.attribute11 ol#force_ship
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_CSP_APP_METHOD'
                                           ,ol.attribute13
                                           ,'I')
             ol#application_method
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ol.attribute15, 'F')
             ol#item_on_blowout
         ,ol.attribute19 ol#pof_std_line
         ,xxeis.eis_rs_dff.decode_valueset ('ReRental Billing Type'
                                           ,ol.attribute2
                                           ,'I')
             ol#rerental_billing_terms
         ,ol.attribute20 ol#pricing_guardrail
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ol.attribute3, 'F')
             ol#print_expired_product_dis
         ,ol.attribute4 ol#rental_charge
         ,ol.attribute5 ol#vendor_quote_cost
         ,ol.attribute6 ol#po_cost_for_vendor_quote
         ,ol.attribute7 ol#serial_number
         ,ol.attribute8 ol#engineering_cost
         --descr#flexfield#end


         --gl#accountff#start

         ,gcc.segment4 gcc#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc#50328#account#descr
         ,gcc.segment3 gcc#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc#50328#cost_center#descr
         ,gcc.segment6 gcc#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc#50328#furture_use#descr
         ,gcc.segment7 gcc#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc#50328#future_use_2#descr
         ,gcc.segment2 gcc#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc#50328#location#descr
         ,gcc.segment1 gcc#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc#50328#product#descr
         ,gcc.segment5 gcc#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc#50328#project_code#descr
         ,gcc.segment4 gcc#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ LTMR _ACCOUNT')
             gcc#50368#account#descr
         ,gcc.segment3 gcc#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc#50368#department#descr
         ,gcc.segment2 gcc#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_ LTMR _DIVISION')
             gcc#50368#division#descr
         ,gcc.segment6 gcc#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc#50368#future_use#descr
         ,gcc.segment1 gcc#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_LTMR_PRODUCT')
             gcc#50368#product#descr
         ,gcc.segment5 gcc#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc#50368#subaccount#descr
     --gl#accountff#end


     FROM po_requisition_headers req_h
         ,po_headers ph
         ,oe_drop_ship_sources od
         ,oe_order_lines ol
         ,oe_order_headers oh
         ,hr_operating_units hou
         ,rcv_shipment_headers rsh
         ,rcv_shipment_lines rsl
         ,mtl_parameters mtp
         ,gl_period_statuses gps
         ,gl_code_combinations_kfv gcc
    WHERE     1 = 1
          AND od.header_id = oh.header_id
          AND od.line_id = ol.line_id
          AND req_h.interface_source_code = 'ORDER ENTRY'
          AND req_h.requisition_header_id = od.requisition_header_id
          AND od.po_header_id = ph.po_header_id
          AND ol.header_id = oh.header_id
          AND hou.organization_id = oh.org_id
          AND od.po_header_id = rsl.po_header_id(+)
          AND od.po_line_id = rsl.po_line_id(+)
          AND ol.ship_from_org_id = mtp.organization_id(+)
          AND rsl.shipment_header_id = rsh.shipment_header_id(+)
          AND gps.application_id = 101
          AND gps.set_of_books_id = hou.set_of_books_id
          AND TRUNC (oh.ordered_date) BETWEEN gps.start_date AND gps.end_date
          AND gcc.code_combination_id(+) = mtp.cost_of_sales_account
          AND EXISTS
                 (SELECT 1
                    FROM xxeis.eis_org_access_v
                   WHERE organization_id = mtp.organization_id);


