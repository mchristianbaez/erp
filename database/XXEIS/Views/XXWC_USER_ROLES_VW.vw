CREATE OR REPLACE FORCE VIEW xxeis.xxwc_user_roles_vw
(
   user_name
  ,employee_number
  ,full_name
  ,email_address
  ,sub_code
  ,sub_name
  ,super_code
  ,super_name
  ,assignment_reason
  ,last_assigned_by
  ,last_update_date
  ,effective_start_date
  ,effective_end_date
  ,supervisor_email
  ,supervisor_name
  ,role_orig_system
  ,buyer_employee_id
  ,is_buyer
  ,buyer_position
  ,buyer_job
  ,gl_segment
  ,scope
  ,job_description
  ,fru
  ,fru_description
  ,branch
  ,region
  ,lob
)
AS
     SELECT DISTINCT a.user_name
                    ,NVL (p1.employee_number, '-') AS employee_number
                    ,NVL (u.description, '-') AS full_name
                    ,u.email_address AS email_address
                    ,a.role_name child_code
                    ,rsub.display_name child_name
                    ,a.assigning_role parent_code
                    ,rsuper.display_name parent_name
                    ,a.assignment_reason
                    ,uupdated.description AS last_assigned_by
                    ,a.last_update_date                                                                                                                                                                                                                                                                                                                  -- Added By Mahender for TMS#20150609-00246 on 06/11/15
                    ,a.effective_start_date
                    ,a.effective_end_date
                    ,p2.email_address AS supervisor_email
                    ,p2.full_name AS supervisor_name
                    ,a.role_orig_system
                    ,buyers.buyer_employee_id
                    ,CASE WHEN buyers.buyer_employee_id IS NULL THEN 'N' ELSE 'Y' END is_buyer
                    ,pos.name buyer_position
                    ,pos.job_name buyer_job
                    ,NVL (gc.segment1, '-') gl_segment
                    ,CASE WHEN scope.user_name IS NULL THEN 'GSC' ELSE 'WC' END scope
                    ,ps.job_descr AS job_description
                    ,ps.attribute1 AS fru
                    ,lc.fru_descr AS fru_description
                    ,lc.lob_branch
                    ,mp.attribute9 AS region
                    ,lc.business_unit AS lob
       FROM applsys.wf_user_role_assignments a
           ,applsys.fnd_user u
            LEFT JOIN 
			(SELECT DISTINCT user_name
                         FROM (SELECT DISTINCT u.user_name
                                 FROM apps.per_people_f per
                                      INNER JOIN apps.per_all_assignments_f ass ON ass.person_id = per.person_id
                                      INNER JOIN apps.gl_code_combinations gc 
										ON gc.code_combination_id = ass.default_code_comb_id
                                      INNER JOIN apps.fnd_user u ON u.employee_id = per.person_id
                                WHERE                                                                                                                                                                                                                                                                                                                     /* Only include White Cap associates, segment1='0W' */
                                      gc.segment1 = '0W' 
										AND NVL (per.effective_end_date, SYSDATE + 1) > SYSDATE 
										AND NVL (u.end_date, SYSDATE + 1) > SYSDATE
                               UNION
                               SELECT DISTINCT a.user_name
                                 FROM applsys.wf_user_role_assignments a
                                WHERE a.role_name LIKE '%XXWC%' 
									AND a.effective_start_date <= SYSDATE 
									AND a.effective_end_date >= SYSDATE)) scope
               ON scope.user_name = u.user_name
            LEFT JOIN apps.per_people_f p1 ON u.employee_id = p1.person_id
            LEFT JOIN apps.per_all_assignments_f pa1 ON p1.person_id = pa1.person_id
            LEFT JOIN apps.gl_code_combinations gc ON gc.code_combination_id = pa1.default_code_comb_id
            LEFT JOIN apps.per_people_f p2 ON pa1.supervisor_id = p2.person_id
            LEFT JOIN xxeis.eis_po_buyer_listing_v buyers ON p1.person_id = buyers.buyer_employee_id AND (SYSDATE >= NVL (buyers.poa_start_date, SYSDATE - 1) AND SYSDATE < NVL (buyers.poa_end_date, SYSDATE + 1))                                                                                                                                 --Added by Mahender reddy for TMS#20150609-00246 on 06/11/15
            LEFT JOIN apps.per_positions_v pos ON pos.position_id = pa1.position_id
            LEFT JOIN xxcus.xxcushr_ps_emp_all_tbl ps ON ps.employee_number = p1.employee_number
            LEFT JOIN xxcus.xxcus_location_code_tbl lc ON ps.attribute1 = lc.fru
            LEFT JOIN apps.mtl_parameters mp ON mp.attribute10 = lc.fru AND mp.attribute11 = lc.lob_branch
           ,applsys.fnd_user uupdated
           ,applsys.wf_local_roles rsub
           ,applsys.wf_local_roles rsuper
      WHERE (SYSDATE >= u.start_date AND SYSDATE < NVL (u.end_date, SYSDATE + 1)) 
			AND a.user_name = u.user_name 
			AND a.last_updated_by = uupdated.user_id                                                                                                                                                                              --            AND (    SYSDATE >= NVL (buyers.poa_start_date, SYSDATE - 1)
                                                                                                                                                                                                                                                                     --                 AND SYSDATE < NVL (buyers.poa_end_date, SYSDATE + 1)) -- Commeneted by Mahender reddy for TMS#20150609-00246 on 06/11/15
            AND rsub.name = a.role_name                                                                                                                                                                                                                                                                                                                                          --AND U.USER_NAME = '10011289'
            AND rsuper.name = a.assigning_role 
			AND (a.assigning_role LIKE 'UMX%' OR a.assigning_role LIKE 'FND_RESP|%') 
			AND a.effective_start_date <= SYSDATE 
			AND a.effective_end_date >= SYSDATE
   --AND u.user_name = '10030020'
   ORDER BY full_name ASC
           ,a.user_name ASC
           ,UPPER (parent_name) ASC
           ,a.role_orig_system ASC
           ,UPPER (child_name) ASC;
