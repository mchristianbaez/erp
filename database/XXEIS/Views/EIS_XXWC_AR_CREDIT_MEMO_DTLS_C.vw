------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
   PURPOSE    :  Replaced the old view definition with new view.This view is created based on 
				 custom table XXEIS.EIS_XXWC_CREDIT_INV_TAB and this custom table data
				 gets populated by XXEIS.EIS_XXWC_CREDIT_STMT_PKG package.
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0		 04/16/2016   Siva  			--TMS#20160418-00101  Performance Tuning                                         
******************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_CREDIT_MEMO_DTLS_C;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_CREDIT_MEMO_DTLS_C (PROCESS_ID,
  FISCAL_YEAR, FISCAL_MONTH, CUSTOMER_NUMBER, ORIGIN_DATE, SHIP_NAME,
  INVOICE_TYPE, INVOICE_DATE, BUSINESS_DATE, TAX_RATE, FREIGHT_ORIGINAL,
  INVOICE_TOTAL, CREDIT, GLNO, AR_CUSTOMER_NUMBER, RESTOCK_FEE_PERCENT,
  location, AMOUNT_PAID, TAX_AMT, INVOICE_NUMBER, ORDER_NUMBER,
  ORIGINAL_INVOICE_NUMBER, PART_NUMBER, CREDIT_REASON_SALE, INVOICE_LINE_AMOUNT
  , QTY_SHIPPED, SALES_REP_NO, CUSTOMER_TERRITORY, CUSTOMER_NAME, MASTER_NAME,
  ORGINAL_ORDER_NUMBER, TAKEN_BY)
AS
  SELECT 
    process_id,
    fiscal_year,
    fiscal_month,
    customer_number,
    origin_date,
    ship_name,
    invoice_type,
    invoice_date,
    business_date,
    tax_rate,
    freight_original,
    invoice_total,
    credit,
    glno,
    ar_customer_number,
    restock_fee_percent,
    location,
    amount_paid,
    tax_amt,
    invoice_number,
    order_number,
    original_invoice_number,
    part_number,
    credit_reason_sale,
    invoice_line_amount,
    qty_shipped,
    sales_rep_no,
    customer_territory,
    customer_name,
    master_name,
    orginal_order_number,
    taken_by
  FROM
    XXEIS.EIS_XXWC_CREDIT_INV_TAB
/
