CREATE OR REPLACE FORCE VIEW xxeis.xxcusozf_unposted_credit_vw
(
   business_unit
  ,payment_method
  ,master_vendor
  ,mvid
  ,payment_number
  ,payment_date
  ,payment_amount
  ,currency
  ,image_url
  ,time_frame
  ,status
)
AS
   SELECT line_of_business business_unit
         ,payment_method
         ,party_name master_vendor
         ,customer_number mvid
         ,payment_number
         ,payment_date
         ,payment_amount
         ,currency_code currency
         ,image_url
         ,payment_time_frame time_frame
         ,rpt.status
     FROM xxcus.xxcusozf_rbt_payments_tbl rpt, hz_parties hp
    WHERE     hp.party_number = rpt.customer_number
          AND payment_method = 'REBATE_CREDIT'
          AND rpt.status = 'N';


