CREATE OR REPLACE FORCE VIEW xxeis.xxhds_mcc_code_acct_info
(
   mcc_code_n0
  ,mcc_code_acct
)
AS
   SELECT lu.lookup_code mcc_code_n0
         ,SUBSTR (aer.flex_concactenated, 4, 6) mcc_code_acct
     FROM applsys.fnd_lookup_values lu
         ,ap.ap_map_codes amc
         ,(  SELECT card_exp_type_lookup_code, MAX (parameter_id) param_id
               FROM apps.ap_card_parameters
           GROUP BY card_exp_type_lookup_code) cp
         ,ap.ap_expense_report_params_all aer
    WHERE     lu.lookup_type = 'XXCUS_AP_MCC_CODES'
          AND lu.lookup_code = amc.from_lookup_code(+)
          AND amc.to_lookup_code = cp.card_exp_type_lookup_code(+)
          AND cp.param_id = aer.parameter_id(+);


