---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_BIN_LOC_V $
  Module Name : Inventory
  PURPOSE	  : Bin Location
  TMS Task Id : 20160503-00093	 
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     16-May-2016        Siva   		 TMS#20160429-00037	   Performance Tuning
  1.1	  13-Feb-2018		 Siva			 TMS#20180125-00274 
  1.2      4-Jul-2018  		 Siva        	 TMS#20180619-00079
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_INV_BIN_LOC_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_BIN_LOC_V (PROCESS_ID, ORGANIZATION, PART_NUMBER, DESCRIPTION, UOM, SELLING_PRICE, VENDOR_NAME, VENDOR_NUMBER, MIN_MINMAX_QUANTITY, MAX_MINMAX_QUANTITY, AVERAGECOST, WEIGHT, CAT, ONHAND, MTD_SALES, YTD_SALES, PRIMARY_BIN_LOC, NO_BIN, ALTERNATE_BIN_LOC, BIN_LOC, LOCATION, STK, OPEN_ORDER, OPEN_DEMAND, CATEGORY, CATEGORY_SET_NAME, INVENTORY_ITEM_ID, INV_ORGANIZATION_ID, ORG_ORGANIZATION_ID, APPLICATION_ID, SET_OF_BOOKS_ID, LAST_RECEIVED_DATE, SUBINVENTORY, BIN0, BIN1, BIN2, BIN3)
AS
  SELECT PROCESS_ID ,
    ORGANIZATION ,
    PART_NUMBER ,
    DESCRIPTION ,
    UOM ,
    SELLING_PRICE ,
    VENDOR_NAME ,
    VENDOR_NUMBER ,
    MIN_MINMAX_QUANTITY ,
    MAX_MINMAX_QUANTITY ,
    AVERAGECOST ,
    WEIGHT ,
    CAT ,
    ONHAND ,
    MTD_SALES ,
    YTD_SALES ,
    PRIMARY_BIN_LOC ,
    NO_BIN ,
    ALTERNATE_BIN_LOC ,
    BIN_LOC ,
    LOCATION ,
    STK ,
    OPEN_ORDER ,
    OPEN_DEMAND ,
    CATEGORY ,
    CATEGORY_SET_NAME ,
    INVENTORY_ITEM_ID ,
    INV_ORGANIZATION_ID ,
    ORG_ORGANIZATION_ID ,
    APPLICATION_ID ,
    SET_OF_BOOKS_ID ,
    LAST_RECEIVED_DATE,
	SUBINVENTORY, --added for version 1.1
	bin0 , --added for version 1.2
    bin1 , --added for version 1.2
    bin2 , --added for version 1.2
    bin3 --added for version 1.2
  FROM XXEIS.EIS_XXWC_BIN_LOC_DATA_TAB
/
