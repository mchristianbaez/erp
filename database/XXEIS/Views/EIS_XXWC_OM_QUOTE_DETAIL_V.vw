-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_OM_QUOTE_DETAIL_V
  File Name: EIS_XXWC_OM_QUOTE_DETAIL_V.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        25-Jun-2018  SIVA        TMS#20180515-00089
****************************************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_QUOTE_DETAIL_V (CREATED_BY, QUOTE_NUMBER, REQUESTED_DATE, ORDER_NUMBER, ORDERED_DATE, CREATION_DATE, ORDER_TYPE, ORDER_HEADER_STATUS, CUSTOMER_NUMBER, CUSTOMER_NAME, CUSTOMER_JOB_NAME, ORDER_AMOUNT, QUOTE_CREATION_DATE, QUOTE_STATUS, SHIP_TO_SITE_NUMBER, SHIP_TO_SITE_NAME_LOCATION, SITE_SALESREP_EEID, SITE_SALESREP_NAME, SHIP_TO_SITE_SALESREP_JOB_DESC, SITE_CLASSIFICATION, BILL_CONTACT, SHIP_CONTACT, CUST_PO_NUMBER, RELATED_PO_NUMBER, ORDER_SOURCE, QUOTE_NAME, SUB_TOTAL, FREIGHT_CHARGE, INVOICE_NUMBER, OH_HEADER_ID, RCT_CUSTOMER_TRX_ID)
AS
  SELECT
    (SELECT PPF1.FULL_NAME
    FROM APPS.FND_USER FU,
      APPS.PER_ALL_PEOPLE_F PPF1
    WHERE FU.USER_ID   = OH.CREATED_BY
    AND FU.EMPLOYEE_ID = PPF1.PERSON_ID
    AND TRUNC(sysdate) BETWEEN PPF1.EFFECTIVE_START_DATE AND PPF1.EFFECTIVE_END_DATE
    )CREATED_BY,
    OH.QUOTE_NUMBER,
    TRUNC(OH.REQUEST_DATE) REQUESTED_DATE,
    OH.ORDER_NUMBER,
    OH.ORDERED_DATE,
    TRUNC(OH.creation_date)creation_date,
    OTT.NAME ORDER_TYPE,
    oh.flow_status_code order_header_status,
    hca.account_number customer_number,
    NVL(HZP.PARTY_NAME,HCA.ACCOUNT_NAME) CUSTOMER_NAME,
    HZCS_SHIP_TO.location CUSTOMER_JOB_NAME,
    (SELECT SUM(NVL(ROUND ( ( OL2.UNIT_SELLING_PRICE * DECODE (OL2.LINE_CATEGORY_CODE, 'RETURN', OL2.ORDERED_QUANTITY * -1, OL2.ORDERED_QUANTITY)),2 ),0)) ORDER_AMOUNT
    FROM APPS.OE_ORDER_LINES OL2
    WHERE OL2.HEADER_ID = OH.HEADER_ID
    ) ORDER_AMOUNT,
    TRUNC(OH.QUOTE_DATE) QUOTE_CREATION_DATE,
    oh.flow_status_code QUOTE_STATUS,
    HZPS_SHIP_TO.party_site_number SHIP_TO_SITE_NUMBER,
    hzcs_ship_to.location SHIP_TO_SITE_NAME_LOCATION,
    NVL(ppf.employee_number,res.source_number) SITE_SALESREP_EEID,
    RES.RESOURCE_NAME SITE_SALESREP_NAME,
    (SELECT XPEA.JOB_DESCR
    FROM XXCUS.XXCUSHR_PS_EMP_ALL_TBL XPEA
    WHERE XPEA.EMPLOYEE_NUMBER=PPF.EMPLOYEE_NUMBER
    AND rownum                =1
    )SHIP_TO_SITE_SALESREP_JOB_DESC,
    HZCS_SHIP_TO.ATTRIBUTE1 SITE_CLASSIFICATION,
    (SELECT invoice_party.PERSON_LAST_NAME
      || DECODE(invoice_party.PERSON_FIRST_NAME, NULL, NULL, ', '
      || invoice_party.PERSON_FIRST_NAME)
      || DECODE(INVOICE_ARL.MEANING,NULL, NULL, ' '
      ||INVOICE_ARL.MEANING) BILL_TO_CONTACT
    FROM APPS.HZ_CUST_ACCOUNT_ROLES INVOICE_ROLES,
      APPS.HZ_PARTIES INVOICE_PARTY,
      APPS.HZ_RELATIONSHIPS INVOICE_REL,
      APPS.HZ_CUST_ACCOUNTS INVOICE_ACCT,
      apps.AR_LOOKUPS INVOICE_ARL
    WHERE invoice_roles.cust_account_role_id = OH.invoice_to_contact_id
    AND invoice_roles.party_id               = invoice_rel.party_id(+)
    AND invoice_roles.role_type(+)           = 'CONTACT'
    AND invoice_roles.cust_account_id        = invoice_acct.cust_account_id(+)
    AND NVL(invoice_rel.object_id,-1)        = NVL(invoice_acct.party_id,-1)
    AND invoice_rel.subject_id               = invoice_party.party_id(+)
    AND INVOICE_ARL.LOOKUP_TYPE(+)           = 'CONTACT_TITLE'
    AND INVOICE_ARL.LOOKUP_CODE(+)           = INVOICE_PARTY.PERSON_PRE_NAME_ADJUNCT
    ) BILL_CONTACT,
    (SELECT ship_party.PERSON_LAST_NAME
      || DECODE(ship_party.PERSON_FIRST_NAME, NULL, NULL, ', '
      || ship_party.PERSON_FIRST_NAME)
      || DECODE(SHIP_ARL.MEANING,NULL, NULL, ' '
      ||SHIP_ARL.MEANING) SHIP_TO_CONTACT
    FROM APPS.HZ_CUST_ACCOUNT_ROLES SHIP_ROLES,
      APPS.HZ_PARTIES SHIP_PARTY,
      APPS.HZ_RELATIONSHIPS SHIP_REL,
      APPS.HZ_CUST_ACCOUNTS SHIP_ACCT,
      apps.AR_LOOKUPS SHIP_ARL
    WHERE ship_roles.cust_account_role_id= OH.ship_to_contact_id
    AND ship_roles.party_id              = ship_rel.party_id(+)
    AND ship_roles.role_type(+)          = 'CONTACT'
    AND ship_roles.cust_account_id       = ship_acct.cust_account_id(+)
    AND NVL(ship_rel.object_id,-1)       = NVL(ship_acct.party_id,-1)
    AND ship_rel.subject_id              = ship_party.party_id(+)
    AND SHIP_ARL.LOOKUP_TYPE(+)          = 'CONTACT_TITLE'
    AND SHIP_ARL.LOOKUP_CODE(+)          = SHIP_PARTY.PERSON_PRE_NAME_ADJUNCT
    ) Ship_Contact,
    Oh.CUST_PO_NUMBER,
    (SELECT PH.SEGMENT1
    FROM APPS.OE_DROP_SHIP_SOURCES ODSS,
      apps.PO_HEADERS PH
    WHERE ODSS.PO_HEADER_ID=PH.PO_HEADER_ID
    AND ODSS.HEADER_ID     =Oh.HEADER_ID
    AND rownum             =1
    )RELATED_PO_NUMBER,
    OOS.name ORDER_SOURCE,
    oh.sales_document_name quote_name,
    (SELECT SUM((NVL(OLL.ORDERED_QUANTITY,0)*NVL(OLL.UNIT_SELLING_PRICE,0))) SUB_TOTAL
    FROM APPS.OE_ORDER_LINES OLL
    WHERE OLL.HEADER_ID = OH.HEADER_ID
    ) SUB_TOTAL,
    (SELECT SUM(NVL((DECODE(OPD.LINE_ID, NULL, DECODE(OPD.CREDIT_OR_CHARGE_FLAG,'C',(-1) * OPD.OPERAND,OPD.OPERAND), DECODE(OPD.CREDIT_OR_CHARGE_FLAG,'C', DECODE(OPD.ARITHMETIC_OPERATOR, 'LUMPSUM', DECODE(OL.ORDERED_QUANTITY, 0, 0, (-1) * (OPD.OPERAND)), (-1) * (OL.ORDERED_QUANTITY*OPD.ADJUSTED_AMOUNT)), DECODE(OPD.ARITHMETIC_OPERATOR, 'LUMPSUM', DECODE(OL.ORDERED_QUANTITY, 0, 0, OPD.OPERAND), (OL.ORDERED_QUANTITY*OPD.ADJUSTED_AMOUNT))))),0)) FREIGHT_CHARGE
    FROM APPS.OE_PRICE_ADJUSTMENTS OPD,
      APPS.OE_ORDER_LINES OL
    WHERE OPD.HEADER_ID         = OH.HEADER_ID
    AND OPD.line_id             =OL.line_id(+)
    AND OPD.list_line_type_code = 'FREIGHT_CHARGE'
    AND OPD.APPLIED_FLAG        = 'Y'
    ) FREIGHT_CHARGE,
    RCT.TRX_NUMBER INVOICE_NUMBER,
    --Primary Key
    OH.HEADER_ID OH_HEADER_ID,
    RCT.CUSTOMER_TRX_ID RCT_CUSTOMER_TRX_ID
    --descr#flexfield#start
    --descr#flexfield#end
  FROM APPS.OE_ORDER_HEADERS OH,
    APPS.ra_customer_trx rct,
    APPS.OE_TRANSACTION_TYPES_TL OTT,
    apps.oe_order_sources oos,
    APPS.HZ_CUST_ACCOUNTS HCA,
    APPS.HZ_PARTIES HZP,
    APPS.HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
    APPS.HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
    APPS.HZ_PARTY_SITES HZPS_SHIP_TO,
    APPS.JTF_RS_SALESREPS REP,
    APPS.JTF_RS_RESOURCE_EXTNS_VL RES,
    APPS.PER_ALL_PEOPLE_F PPF
  WHERE TO_CHAR(OH.ORDER_NUMBER)       = RCT.INTERFACE_HEADER_ATTRIBUTE1(+)
  AND RCT.INTERFACE_HEADER_CONTEXT(+)  = 'ORDER ENTRY'
  AND OH.ORDER_TYPE_ID                 = OTT.TRANSACTION_TYPE_ID
  AND OTT.LANGUAGE                     = USERENV('LANG')
  AND OH.ORDER_SOURCE_ID               = OOS.ORDER_SOURCE_ID
  AND OH.SOLD_TO_ORG_ID                = HCA.CUST_ACCOUNT_ID
  AND HCA.PARTY_ID                     = HZP.PARTY_ID(+)
  AND oh.ship_to_org_id                = hzcs_ship_to.site_use_id(+)
  AND hzcs_ship_to.cust_acct_site_id   = hcas_ship_to.cust_acct_site_id(+)
  AND HCAS_SHIP_TO.PARTY_SITE_ID       = HZPS_SHIP_TO.PARTY_SITE_ID(+)
  AND HZCS_SHIP_TO.PRIMARY_SALESREP_ID = REP.SALESREP_ID(+)
  AND HZCS_SHIP_TO.org_id              = REP.org_id(+)
  AND rep.resource_id                  = res.resource_id(+)
  AND rep.person_id                    = ppf.person_id(+)
  AND TRUNC(sysdate) BETWEEN PPF.EFFECTIVE_START_DATE(+) AND PPF.EFFECTIVE_END_DATE(+)
  AND OTT.TRANSACTION_TYPE_ID =1001
  AND OH.QUOTE_NUMBER        IS NOT NULL
/
