--//============================================================================
--//
--// Object Name         :: XXEIS.EIS_XXWC_AR_ORDER_HOLD_DET_V
--//
--// Object Type         :: View
--//
--// Object Description  :: This view will give credit hold and case folder details
--//
--// Version Control
--//============================================================================
--// Vers    Author                  Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Maharajan Shunmugam     09/23/2015    TMS#20140516-00041 Credit - EIS Create Credit Hold Release Report
--// 1.1      Siva					 10/03/2015	    TMS#20160831-00143 -- Performance issue
--//============================================================================

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_ORDER_HOLD_DET_V
(  ORDER_NUMBER,
   CREATED_BY,
   TYPE,
   HOLD_NAME,
   RELEASED_FLAG,
   ORDER_DATE,
   HOLD_APPLIED_DATE,
   APPLIED_BY_ID,
   APPLIED_BY_NAME,
   HOLD_RELEASED_DATE,
   RELEASED_BY_USER_ID,
   RELEASED_BY_USER_NAME,
   RELEASE_REASON,
   RELEASE_COMMENT,
   BRANCH_NUMBER,
   ORDER_AMOUNT,
   ACCOUNT_NAME,
   ACCOUNT_NUMBER,
   SITE_NAME,
   SITE_NUMBER,
   CREDIT_CLASSIFICATION,
   COLLECTOR,
   CASE_FOLDER_RELEASED_DATE,
   CASE_FOLDER_NUMBER,
   BR_NAME,
   BR_NUMBER,
   INVOICE_DATE,
   INVOICE_NUMBER,
   INV_AMOUNT_DUE_ORIGINAL,
   INV_AMOUNT_DUE_REMAINING,
   INV_DUE_DATE)
AS
   SELECT /*+ INDEX(ohr XXWC_OE_HOLD_RELEASE_N3) INDEX(xlct XXCUS_LOCATION_CODE_TBL_N2)*/   -- added for version 1.1
         ooha.order_number,
          holds.created_by,
          flv.meaning TYPE,
          ohd.name HOLD_NAME,
          holds.released_flag,
          TO_CHAR (ooha.ordered_date, 'DD-MON-YYYY HH24:MI:SS') ORDER_DATE,
          TO_CHAR (holds.creation_date, 'DD-MON-YYYY HH24:MI:SS') HOLD_APPLIED_DATE,
          fu1.user_name APPLIED_BY_ID,
          fu1.description APPLIED_BY_NAME,
          --       to_char(ohr.creation_date,'DD-MON-YYYY HH24:MI:SS')  HOLD RELEASED DATE,
          TRUNC (ohr.creation_date) HOLD_RELEASED_DATE,
          fu.user_name RELEASED_BY_USER_ID,
          fu.description RELEASED_BY_USER_NAME,
          flv1.meaning RELEASE_REASON,
          ohr.release_comment,
          OD.ORGANIZATION_CODE BRANCH_NUMBER,
          (SELECT apps.oe_totals_grp.get_order_total (ooha.header_id,NULL,'ALL') FROM DUAL) ORDER_AMOUNT,
          hca.account_name,
          hca.account_number,
          hcsu.location site_name,
          hps.party_site_number site_number,
          hcp.credit_classification CREDIT_CLASSIFICATION,
          ac.name Collector,
          (SELECT xchcf.case_folder_released_date FROM xxwc.xxwc_cr_hold_case_folder_stg xchcf
            WHERE     xchcf.source_column1 = ooha.header_id
                  AND xchcf.created_by = holds.created_by
                  AND xchcf.case_folder_number IS NOT NULL
                  AND ohsa.hold_entity_code = xchcf.review_type
                  AND xchcf.FOLDER_CREATION_DATE 
						BETWEEN TO_CHAR (holds.creation_date,'DD-MON-YYYY HH24:MI:SS') AND TO_CHAR (holds.creation_date + 15 / 1440,'DD-MON-YYYY HH24:MI:SS')
                  AND ROWNUM = 1) CASE_FOLDER_RELEASED_DATE,
          (SELECT xchcf.case_folder_number  FROM xxwc.xxwc_cr_hold_case_folder_stg xchcf
            WHERE     xchcf.source_column1 = ooha.header_id
                  AND xchcf.created_by = holds.created_by
                  AND xchcf.case_folder_number IS NOT NULL
                  AND ohsa.hold_entity_code = xchcf.review_type
                  --AND  a.status= 'CLOSED'
                  AND xchcf.FOLDER_CREATION_DATE 
						BETWEEN TO_CHAR (holds.creation_date,'DD-MON-YYYY HH24:MI:SS') AND TO_CHAR (holds.creation_date + 15 / 1440,'DD-MON-YYYY HH24:MI:SS')
                  AND ROWNUM = 1) CASE_FOLDER_NUMBER,
          xlct.fru_descr BR_NAME,
          xlct.entrp_loc BR_NUMBER,
          TO_CHAR (rcta.trx_date, 'DD-MON-YYYY HH24:MI:SS') INVOICE_DATE,
          rcta.trx_number INVOICE_NUMBER,
          apsa.amount_due_original inv_amount_due_original,
          apsa.amount_due_remaining inv_amount_due_remaining,
          TO_CHAR (apsa.due_date, 'DD-MON-YYYY HH24:MI:SS') inv_due_date
     FROM apps.oe_order_headers_all ooha,
          --oe_order_lines_all ool,
          apps.oe_order_holds_all holds,
          apps.oe_hold_sources_all ohsa,
          apps.oe_hold_releases ohr,
          apps.oe_hold_definitions ohd,
          apps.fnd_lookup_values flv,
          apps.fnd_user fu,
          apps.fnd_user fu1,
          apps.mtl_parameters od,                     -- added for version 1.1
          -- apps.org_organization_definitions od,   -- Commented for version 1.1
          xxcus.xxcus_location_code_tbl xlct,
          apps.hz_cust_accounts hca,
          apps.hz_cust_site_uses_all hcsu,
          apps.hz_party_sites hps,
          apps.hz_cust_acct_sites_all hcas,
          apps.fnd_lookup_values flv1,
          apps.hz_customer_profiles hcp,
          apps.ar_collectors ac,
          apps.ra_customer_trx_all rcta,
          apps.ar_payment_schedules_all apsa
    WHERE     ooha.header_id = holds.header_id
          AND ooha.order_type_id IN (1001, 1004)
          AND holds.hold_source_id = ohsa.hold_source_id
          AND ohsa.hold_id = ohd.hold_id
          AND holds.hold_release_id = ohr.hold_release_id(+)
          AND ohd.hold_id = 1                           --Credit Check Failure
          AND ohr.created_by = fu.user_id(+)
          AND holds.created_by = fu1.user_id
          AND ohsa.hold_entity_code = flv.lookup_code
          AND flv.lookup_type = 'HOLD_ENTITY_DESC'
          AND flv.view_application_id = 660
          AND ohr.release_reason_code = flv1.lookup_code
          AND flv1.lookup_type = 'RELEASE_REASON'
          AND flv1.view_application_id = 660
          AND ooha.ship_from_org_id = od.organization_id
          AND xlct.lob_branch = od.organization_code
          AND xlct.entrp_loc LIKE 'BW%'
          AND ooha.sold_to_org_id = hca.cust_account_id
          AND ooha.invoice_to_org_id = hcsu.site_use_id
          AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
          AND hcas.party_site_id = hps.party_site_id
          AND hca.cust_account_id = hcp.cust_account_id
          AND hcp.site_use_id IS NULL
          AND hcp.credit_classification NOT IN ('CSALE')
          AND hcp.collector_id = ac.collector_id
          AND TO_CHAR (ooha.order_number) =
                 rcta.interface_header_attribute1(+)
          AND rcta.interface_header_context = 'ORDER ENTRY'
          AND rcta.customer_trx_id = apsa.customer_trx_id(+)
          AND fu.user_id NOT IN (1, 0, 15986)
/
