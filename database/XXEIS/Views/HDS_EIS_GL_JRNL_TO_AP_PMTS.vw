CREATE OR REPLACE FORCE VIEW xxeis.hds_eis_gl_jrnl_to_ap_pmts
(
   je_batch_id
  ,batch_name
  ,batch_description
  ,je_header_id
  ,ledger_id
  ,ledger_name
  ,je_source_name
  ,je_source
  ,je_category_name
  ,je_category
  ,je_period
  ,je_name
  ,je_description
  ,je_currency
  ,je_status
  ,je_type
  ,je_posted_date
  ,je_creation_date
  ,je_created_by
  ,je_balanced
  ,accrual_reversal
  ,je_multi_bal_seg
  ,accrual_rev_effective_date
  ,accrual_rev_period_name
  ,accrual_rev_status
  ,accrual_rev_je_header_id
  ,accrual_rev_change_sign
  ,reversed_je_header_id
  ,je_curr_conv_rate
  ,je_curr_conv_type
  ,je_curr_conv_date
  ,je_external_ref
  ,budget_name
  ,budget_type
  ,budget_version_id
  ,budget_status
  ,budget_description
  ,je_line_num
  ,je_effective_date
  ,je_entered_dr
  ,je_entered_cr
  ,je_accounted_dr
  ,je_accounted_cr
  ,je_line_description
  ,sla_entered_dr
  ,sla_entered_cr
  ,sla_entered_net
  ,sla_accounted_dr
  ,sla_accounted_cr
  ,sla_accounted_net
  ,check_id
  ,check_number
  ,check_currency_code
  ,check_date
  ,bank_account_num
  ,check_cleared_date
  ,check_cleared_amount
  ,check_doc_seq_value
  ,code_combination_id
  ,gl_account_string
  ,vendor_id
  ,vendor_site_id
  ,vendor_name
  ,vendor_site
  ,supplier_number
  ,employee_number
  ,person_id
  ,invoice_num
  ,invoice_id
  ,inoice_date
  ,invoice_currency_code
  ,invoice_amount
  ,invoice_doc_seq_value
  ,payment_amount
  ,gl_sl_link_id
  ,gl_sl_link_table
  ,sla_account_class
  ,sla_event_type
  ,transfer_date_from_sla_to_gl
  ,sla_accounting_date
  ,sla_code_combination_id
  ,sla_account
  ,ae_header_id
  ,ae_line_num
  ,application_id
  ,ref_ae_header_id
  ,temp_line_num
  ,payment_hist_dist_id
  ,invoice_payment_id
  ,pov_vendor_id
  ,gcc1#50328#product
  ,gcc1#50328#product#descr
  ,gcc#50328#product
  ,gcc#50328#product#descr
  ,gcc1#50328#location
  ,gcc1#50328#location#descr
  ,gcc#50328#location
  ,gcc#50328#location#descr
  ,gcc1#50328#cost_center
  ,gcc1#50328#cost_center#descr
  ,gcc#50328#cost_center
  ,gcc#50328#cost_center#descr
  ,gcc1#50328#account
  ,gcc1#50328#account#descr
  ,gcc#50328#account
  ,gcc#50328#account#descr
  ,gcc1#50328#project_code
  ,gcc1#50328#project_code#descr
  ,gcc#50328#project_code
  ,gcc#50328#project_code#descr
  ,gcc1#50328#furture_use
  ,gcc1#50328#furture_use#descr
  ,gcc#50328#furture_use
  ,gcc#50328#furture_use#descr
  ,gcc1#50328#future_use_2
  ,gcc1#50328#future_use_2#descr
  ,gcc#50328#future_use_2
  ,gcc#50328#future_use_2#descr
)
AS
   SELECT jb.je_batch_id je_batch_id
         ,jb.name batch_name
         ,jb.description batch_description
         ,jh.je_header_id je_header_id
         ,le.ledger_id ledger_id
         ,le.name ledger_name
         ,jes.je_source_name je_source_name
         ,jes.user_je_source_name je_source
         ,jec.je_category_name je_category_name
         ,jec.user_je_category_name je_category
         ,jh.period_name je_period
         ,jh.name je_name
         ,jh.description je_description
         ,jh.currency_code je_currency
         ,DECODE (
             jh.status
            ,'P', 'Posted'
            ,'U', 'Unposted'
            ,'F', 'Error7 - Showing invalid journal entry lines or no journal entry lines'
            ,'K', 'Error10 - Showing unbalanced intercompany journal entry'
            ,'Z', 'Error7 - Showing invalid journal entry lines or no journal entry lines'
            ,'Unknown')
             je_status
         ,DECODE (jh.actual_flag
                 ,'A', 'Actual'
                 ,'B', 'Budget'
                 ,'E', 'Encumbrance')
             je_type
         ,jh.posted_date je_posted_date
         ,jh.creation_date je_creation_date
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_user_name (jh.created_by)
             je_created_by
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.balanced_je_flag)
             je_balanced
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_flag)
             accrual_reversal
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.multi_bal_seg_flag)
             je_multi_bal_seg
         ,jh.accrual_rev_effective_date
         ,jh.accrual_rev_period_name
         ,jh.accrual_rev_status
         ,jh.accrual_rev_je_header_id
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_change_sign_flag)
             accrual_rev_change_sign
         ,jh.reversed_je_header_id
         ,jh.currency_conversion_rate je_curr_conv_rate
         ,jh.currency_conversion_type je_curr_conv_type
         ,jh.currency_conversion_date je_curr_conv_date
         ,jh.external_reference je_external_ref
         ,gbv.budget_name
         ,gbv.budget_type
         ,gbv.budget_version_id
         ,DECODE (gbv.status,  'O', 'Open',  'C', 'Closed',  'F', 'Future')
             budget_status
         ,gbv.description budget_description
         ,jl.je_line_num je_line_num
         ,jl.effective_date je_effective_date
         ,jl.entered_dr je_entered_dr
         ,jl.entered_cr je_entered_cr
         ,jl.accounted_dr je_accounted_dr
         ,jl.accounted_cr je_accounted_cr
         ,jl.description je_line_description
         ,DECODE (adl.application_id
                 ,NULL, ael.entered_dr
                 ,adl.unrounded_entered_dr)
             sla_entered_dr
         ,DECODE (adl.application_id
                 ,NULL, ael.entered_cr
                 ,adl.unrounded_entered_cr)
             sla_entered_cr
         , (  NVL (
                 DECODE (adl.application_id
                        ,NULL, ael.entered_dr
                        ,adl.unrounded_entered_dr)
                ,0)
            - NVL (
                 DECODE (adl.application_id
                        ,NULL, ael.entered_cr
                        ,adl.unrounded_entered_cr)
                ,0))
             sla_entered_net
         ,DECODE (adl.application_id
                 ,NULL, ael.accounted_dr
                 ,adl.unrounded_accounted_dr)
             sla_accounted_dr
         ,DECODE (adl.application_id
                 ,NULL, ael.accounted_cr
                 ,adl.unrounded_accounted_cr)
             sla_accounted_cr
         , (  NVL (
                 DECODE (adl.application_id
                        ,NULL, ael.accounted_dr
                        ,adl.unrounded_accounted_dr)
                ,0)
            - NVL (
                 DECODE (adl.application_id
                        ,NULL, ael.accounted_cr
                        ,adl.unrounded_accounted_cr)
                ,0))
             sla_accounted_net
         ,ac.check_id check_id
         ,ac.check_number check_number
         ,ac.currency_code check_currency_code
         ,ac.check_date check_date
         ,ac.bank_account_num bank_account_num
         ,ac.cleared_date check_cleared_date
         ,ac.cleared_amount check_cleared_amount
         ,ac.doc_sequence_value check_doc_seq_value
         ,gcc.code_combination_id
         ,gcc.concatenated_segments gl_account_string
         ,ael.party_id vendor_id
         ,ael.party_site_id vendor_site_id
         ,ac.vendor_name vendor_name
         ,ac.vendor_site_code vendor_site
         ,pov.segment1 supplier_number
         ,papf.employee_number
         ,papf.person_id
         ,ai.invoice_num invoice_num
         ,ai.invoice_id invoice_id
         ,ai.invoice_date inoice_date
         ,ai.invoice_currency_code invoice_currency_code
         ,ai.invoice_amount invoice_amount
         ,ai.doc_sequence_value invoice_doc_seq_value
         ,aip.amount payment_amount
         ,ael.gl_sl_link_id
         ,ael.gl_sl_link_table
         ,ael.accounting_class_code sla_account_class
         ,aeh.event_type_code sla_event_type
         ,aeh.gl_transfer_date transfer_date_from_sla_to_gl
         ,aeh.accounting_date sla_accounting_date
         ,gcc1.code_combination_id sla_code_combination_id
         ,gcc1.concatenated_segments sla_account
         ,                                                       --Unique Keys
          ael.ae_header_id
         ,ael.ae_line_num
         ,ael.application_id
         ,adl.ref_ae_header_id
         ,adl.temp_line_num
         ,aphd.payment_hist_dist_id
         ,aip.invoice_payment_id
         ,pov.vendor_id pov_vendor_id
         --descr#flexfield#start
         --descr#flexfield#end
         --gl#accountff#start

         ,gcc1.segment1 gcc1#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc1#50328#product#descr
         ,gcc.segment1 gcc#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc#50328#product#descr
         ,gcc1.segment2 gcc1#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc1#50328#location#descr
         ,gcc.segment2 gcc#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc#50328#location#descr
         ,gcc1.segment3 gcc1#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc1#50328#cost_center#descr
         ,gcc.segment3 gcc#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc#50328#cost_center#descr
         ,gcc1.segment4 gcc1#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc1#50328#account#descr
         ,gcc.segment4 gcc#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc#50328#account#descr
         ,gcc1.segment5 gcc1#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc1#50328#project_code#descr
         ,gcc.segment5 gcc#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc#50328#project_code#descr
         ,gcc1.segment6 gcc1#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc1#50328#furture_use#descr
         ,gcc.segment6 gcc#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc#50328#furture_use#descr
         ,gcc1.segment7 gcc1#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc1#50328#future_use_2#descr
         ,gcc.segment7 gcc#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc#50328#future_use_2#descr
     --gl#accountff#end


     FROM gl_ledgers le
         ,gl_je_batches jb
         ,gl_je_headers jh
         ,gl_je_lines jl
         ,gl_import_references gir
         ,gl_code_combinations_kfv gcc
         ,gl_je_sources jes
         ,gl_je_categories jec
         ,gl_budget_versions gbv
         ,xla_ae_lines ael
         ,gl_code_combinations_kfv gcc1
         ,xla_ae_headers aeh
         ,xla_distribution_links adl
         ,ap_payment_hist_dists aphd
         ,ap_invoice_payments_all aip
         ,ap_invoices_all ai
         ,ap_checks_all ac
         ,po_vendors pov
         ,hr.per_all_people_f papf
    -- to have multiple outer joins.. used for ap_invoice_payments.check_id
    WHERE     le.ledger_id = jh.ledger_id
          AND jb.je_batch_id = jh.je_batch_id
          AND jh.je_header_id = jl.je_header_id
          AND gir.je_header_id = jh.je_header_id
          AND gir.je_line_num = jl.je_line_num
          AND jl.code_combination_id = gcc.code_combination_id
          AND jh.je_source = jes.je_source_name
          AND jh.je_category = jec.je_category_name
          AND jh.budget_version_id = gbv.budget_version_id(+)
          AND gir.gl_sl_link_id = ael.gl_sl_link_id(+)
          AND gir.gl_sl_link_table = ael.gl_sl_link_table(+)
          AND ael.ae_header_id = aeh.ae_header_id(+)
          AND ael.code_combination_id = gcc1.code_combination_id(+)
          AND ael.ae_header_id = adl.ae_header_id(+)
          AND ael.ae_line_num = adl.ae_line_num(+)
          AND adl.source_distribution_id_num_1 = aphd.payment_hist_dist_id(+)
          AND aphd.invoice_payment_id = aip.invoice_payment_id(+)
          AND aip.check_id = ac.check_id(+)
          AND aip.invoice_id = ai.invoice_id(+)
          AND pov.vendor_id = ai.vendor_id
          AND pov.employee_id = papf.person_id
          AND jh.je_source = 'Payables'
          AND jh.je_category = 'Payments'
          AND le.object_type_code = 'L'
          AND papf.effective_end_date =
                 TO_DATE ('31-DEC-4712', 'DD-MON-YYYY');


