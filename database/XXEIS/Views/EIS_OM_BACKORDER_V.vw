  CREATE OR REPLACE  VIEW "XXEIS"."EIS_OM_BACKORDER_V"  AS 
  SELECT oh.order_number order_number,
wdd.requested_quantity backordered_qty,
    ol.line_number
    ||'.'
    || ol.shipment_number line_number,
    ol.ordered_item ordered_item,
    ol.request_date request_date,
    ol.promise_date promise_date,
    ol.schedule_arrival_date schedule_arrival_date,
    ol.schedule_ship_date schedule_ship_date,
    ol.order_quantity_uom order_quantity_uom,
    ol.pricing_quantity pricing_quantity,
    ol.pricing_quantity_uom pricing_quantity_uom,
    ol.cancelled_quantity cancelled_quantity,
    ol.shipped_quantity shipped_quantity,
    ol.ordered_quantity ordered_quantity,
    ol.fulfilled_quantity fulfilled_quantity,
    ol.shipping_quantity shipping_quantity,
    ol.shipping_quantity_uom shipping_quantity_uom,
    ol.delivery_lead_time delivery_lead_time,
    ol.auto_selected_quantity auto_selected_quantity,
    ol.tax_exempt_flag tax_exempt_flag,
    ol.tax_exempt_number tax_exempt_number,
    ol.tax_exempt_reason_code tax_exempt_reason_code,
    ol.cust_po_number Customer_PO,
    ol.ship_tolerance_above ship_tolerance_above,
    ol.ship_tolerance_below ship_tolerance_below,
    ol.demand_bucket_type_code demand_bucket_type_code,
    ol.rla_schedule_type_code rla_schedule_type_code,
    ol.customer_dock_code customer_dock_code,
    ol.customer_job customer_job,
    ol.customer_production_line customer_production_line,
    ol.cust_model_serial_number cust_model_serial_number,
    ol.project_id project_id,
    ol.task_id task_id,
    ol.end_item_unit_number end_item_unit_number,
    ol.sold_to_org_id sold_to_org_id,
    ol.ship_from_org_id ship_from_org_id,
    ol.ship_to_org_id ship_to_org_id,
    ol.deliver_to_org_id deliver_to_org_id,
    ol.return_reason_code return_reason_code,
    ol.intmed_ship_to_org_id intmed_ship_to_org_id,
    ol.invoice_to_org_id invoice_to_org_id,
    ol.ship_to_contact_id ship_to_contact_id,
    ol.deliver_to_contact_id deliver_to_contact_id,
    ol.intmed_ship_to_contact_id intmed_ship_to_contact_id,
    ol.invoice_to_contact_id invoice_to_contact_id,
    ol.inventory_item_id inventory_item_id,
    ol.tax_date tax_date,
    ol.tax_code tax_code,
    ol.tax_rate tax_rate,
    ol.demand_class_code demand_class_code,
    ol.schedule_status_code schedule_status_code,
    ol.source_type_code source_type_code,
    ol.price_list_id price_list_id,
    ol.pricing_date pricing_date,
    ol.shipment_number shipment_number,
    ol.agreement_id agreement_id,
    ol.shipment_priority_code shipment_priority_code,
    ol.shipping_method_code shipping_method_code,
    ol.freight_terms_code freight_terms_code,
    ol.freight_carrier_code freight_carrier_code,
    ol.fob_point_code fob_point_code,
    ol.tax_point_code tax_point_code,
    ol.payment_term_id payment_term_id,
    ol.invoicing_rule_id invoicing_rule_id,
    ol.accounting_rule_id accounting_rule_id,
    ol.accounting_rule_duration accounting_rule_duration,
    ol.source_document_type_id source_document_type_id,
    ol.orig_sys_document_ref orig_sys_document_ref,
    ol.source_document_id source_document_id,
    ol.orig_sys_line_ref orig_sys_line_ref,
    ol.source_document_line_id source_document_line_id,
    ol.reference_line_id reference_line_id,
    ol.reference_type reference_type,
    ol.reference_header_id reference_header_id,
    ol.item_revision item_revision,
    ol.line_category_code line_category_code,
    ol.customer_trx_line_id customer_trx_line_id,
    ol.reference_customer_trx_line_id reference_customer_trx_line_id,
    ol.unit_selling_price unit_selling_price,
    ol.unit_list_price unit_list_price,
    ol.tax_value tax_value,
    ol.model_group_number model_group_number,
    ol.option_flag option_flag,
    ol.dep_plan_required_flag dep_plan_required_flag,
    ol.visible_demand_flag visible_demand_flag,
    ol.actual_arrival_date actual_arrival_date,
    ol.actual_shipment_date actual_shipment_date,
    ol.earliest_acceptable_date earliest_acceptable_date,
    ol.latest_acceptable_date latest_acceptable_date,
    trx_type.NAME line_type,
    qphvl.NAME price_list,
    qph.rounding_factor rounding_factor,
    accrule.NAME accounting_rule,
    invrule.NAME invoicing_rule,
    term.NAME Payment_Terms,
    party.party_name Customer,
    cust_acct.account_number customer_number,
    ship_from_org.organization_code Warehouse,
    ol.subinventory subinventory,
    ship_su.LOCATION ship_to,
    ship_su.LOCATION Ship_To_Location,
    ship_loc.address1 ship_to_address1,
    ship_loc.address2 ship_to_address2,
    ship_loc.address3 ship_to_address3,
    ship_loc.address4 ship_to_address4,
    DECODE (ship_loc.city, NULL, NULL, ship_loc.city
    || ', ' )
    || DECODE (ship_loc.state, NULL, ship_loc.province
    || ', ', ship_loc.state
    || ', ' )
    || DECODE (ship_loc.postal_code, NULL, NULL, ship_loc.postal_code
    || ', ' )
    || DECODE (ship_loc.country, NULL, NULL, ship_loc.country) ship_to_address5,
    bill_su.LOCATION Bill_to,
    bill_su.LOCATION Bill_to_location,
    bill_loc.address1 Bill_to_address1,
    bill_loc.address2 Bill_to_address2,
    bill_loc.address3 Bill_to_address3,
    bill_loc.address4 Bill_to_address4,
    DECODE (bill_loc.city, NULL, NULL, bill_loc.city
    || ', ' )
    || DECODE (bill_loc.state, NULL, bill_loc.province
    || ', ', bill_loc.state
    || ', ' )
    || DECODE (bill_loc.postal_code, NULL, NULL, bill_loc.postal_code
    || ', ' )
    || DECODE (bill_loc.country, NULL, NULL, bill_loc.country) Bill_to_address5,
    ship_party.person_last_name
    || DECODE (ship_party.person_first_name, NULL, NULL, ', '
    || ship_party.person_first_name )
    || DECODE (ship_arl.meaning, NULL, NULL, ' '
    || ship_arl.meaning) ship_to_contact,
    NULL intmed_ship_to_contact,
    invoice_party.person_last_name
    || DECODE (invoice_party.person_first_name, NULL, NULL, ', '
    || invoice_party.person_first_name )
    || DECODE (invoice_arl.meaning, NULL, NULL, ' '
    || invoice_arl.meaning ) Bill_to_contact,
    oh.quote_number quote_number,
    oh.version_number version_number,
    OH.ORDER_TYPE_ID ORDER_TYPE_ID,
    trunc(OH.ORDERED_DATE) ORDERED_DATE,
    oh.order_source_id order_source_id,
    ol.salesrep_id salesrep_id,
    xxeis.eis_om_utility_pkg.get_salesperson_name(ol.salesrep_id) salesperson_name,
    ol.split_from_line_id split_from_line_id,
    ol.line_set_id line_set_id,
    ol.split_by split_by,
    ol.cust_production_seq_num cust_production_seq_num,
    ol.authorized_to_ship_flag authorized_to_ship_flag,
    ol.veh_cus_item_cum_key_id veh_cus_item_cum_key_id,
    ol.invoice_interface_status_code invoice_interface_status_code,
    ol.over_ship_reason_code over_ship_reason_code,
    ol.over_ship_resolved_flag over_ship_resolved_flag,
    ol.ship_set_id ship_set_id,
    ol.arrival_set_id arrival_set_id,
    ol.ordered_item_id ordered_item_id,
    ol.item_identifier_type item_identifier_type,
    ol.commitment_id commitment_id,
    ol.shipping_interfaced_flag shipping_interfaced_flag,
    ol.credit_invoice_line_id credit_invoice_line_id
,
    ol.first_ack_code first_ack_code,
    ol.first_ack_date first_ack_date,
    ol.last_ack_code last_ack_code,
    ol.last_ack_date last_ack_date,
    ol.planning_priority planning_priority,
    ol.config_header_id config_header_id,
    ol.config_rev_nbr config_rev_nbr,
    ol.booked_flag booked_flag,
    ol.cancelled_flag cancelled_flag,
    ol.open_flag open_flag,
    ol.sold_from_org_id sold_from_org_id,
    ol.fulfilled_flag fulfilled_flag,
    ol.shipping_instructions shipping_instructions,
    ol.packing_instructions packing_instructions,
    ol.invoiced_quantity invoiced_quantity,
    ol.service_txn_reason_code service_txn_reason_code,
    ol.service_txn_comments service_txn_comments,
    ol.service_duration service_duration,
    ol.service_period service_period,
    ol.service_start_date service_start_date,
    ol.service_end_date service_end_date,
    ol.service_coterminate_flag service_coterminate_flag,
    ol.unit_selling_percent unit_selling_percent,
    ol.unit_list_percent unit_list_percent,
    ol.unit_percent_base_price unit_percent_base_price,
    ol.service_number service_number,
    ol.service_reference_type_code service_reference_type_code,
    ol.service_reference_line_id service_reference_line_id,
    ol.service_reference_system_id service_reference_system_id,
    ol.flow_status_code flow_status_code,
    ol.model_remnant_flag model_remnant_flag,
    ol.fulfillment_method_code fulfillment_method_code,
    ol.calculate_price_flag calculate_price_flag,
    ol.revenue_amount revenue_amount,
    ol.fulfillment_date fulfillment_date,
    ol.preferred_grade preferred_grade,
    ol.ordered_quantity2 ordered_quantity2,
    ol.ordered_quantity_uom2 ordered_quantity_uom2,
    ol.shipped_quantity2 shipped_quantity2,
    ol.cancelled_quantity2 cancelled_quantity2,
    ol.shipping_quantity2 shipping_quantity2,
    ol.shipping_quantity_uom2 shipping_quantity_uom2,
    ol.fulfilled_quantity2 fulfilled_quantity2,
    ol.upgraded_flag upgraded_flag,
    ol.lock_control lock_control,
    ol.unit_selling_price_per_pqty unit_selling_price_per_pqty,
    ol.unit_list_price_per_pqty unit_list_price_per_pqty,
    ol.customer_line_number customer_line_number,
    ol.customer_shipment_number customer_shipment_number,
    ol.customer_item_net_price customer_item_net_price,
    ol.customer_payment_term_id customer_payment_term_id,
    ol.original_inventory_item_id original_inventory_item_id,
    ol.original_item_identifier_type original_item_identifier_type,
    ol.original_ordered_item_id original_ordered_item_id,
    ol.original_ordered_item original_ordered_item,
    ol.item_substitution_type_code item_substitution_type_code,
    ol.late_demand_penalty_factor late_demand_penalty_factor,
    ol.override_atp_date_code override_atp_date_code,
    ol.unit_cost unit_cost,
    ol.user_item_description user_item_description,
    ol.item_relationship_type item_relationship_type,
    ol.blanket_number blanket_number,
    ol.blanket_line_number blanket_line_number,
    ol.blanket_version_number blanket_version_number,
    ol.firm_demand_flag firm_demand_flag,
    ol.earliest_ship_date earliest_ship_date,
    ol.transaction_phase_code transaction_phase_code,
    ol.source_document_version_number source_document_version_number,
    ol.minisite_id minisite_id,
    ol.end_customer_id end_customer_id,
    ol.end_customer_contact_id end_customer_contact_id,
    ol.end_customer_site_use_id end_customer_site_use_id,
    ol.original_list_price original_list_price,
    ol.service_credit_eligible_code service_credit_eligible_code,
    ol.order_firmed_date order_firmed_date,
    ol.actual_fulfillment_date actual_fulfillment_date,
    ol.orig_sys_shipment_ref orig_sys_shipment_ref,
    ol.charge_periodicity_code charge_periodicity_code,
    trx_type.name Order_Type,
    oh.flow_status_code status,
    ol.pricing_context pricing_context,
    DECODE(wdd.released_status ,'B','Back Order') released_status,
    wdd.source_header_id source_header_id,
    wdd.source_header_number source_header_number,
    wdd.source_code source_code,
    wdd.source_header_type_name source_header_type_name ,
    --primary keys
    ship_from_org.organization_id SHIPPING_ORG_ID,
    ship_su.site_use_id SHIP_SITE_USE_ID,
    ship_ps.party_site_id SHIP_PS_PARTY_SITE_ID,
    ship_loc.location_id ship_loc_location_id,
    ship_cas.cust_acct_site_id ship_cas_cust_acct_site_id,
    bill_loc.location_id bill_loc_location_id,
    bill_ps.party_site_id bill_ps_party_site_id,
    bill_cas.cust_acct_site_id bill_cas_cust_acct_site_id,
    ship_party.party_id ship_party_party_id,
    cust_acct.cust_account_id cust_acct_cust_account_id,
    ship_roles.CUST_ACCOUNT_ROLE_ID SROLES_CUST_ACCOUNT_ROLE_ID,
    ship_rel.RELATIONSHIP_ID ship_rel_RELATIONSHIP_ID,
    ship_acct.cust_account_id ship_acct_cust_account_id,
    invoice_roles.CUST_ACCOUNT_ROLE_ID IROLES_CUST_ACCOUNT_ROLE_ID,
    invoice_party.party_id INVOICE_PARTY_PARTY_ID,
    invoice_rel.RELATIONSHIP_ID invoice_rel_RELATIONSHIP_ID,
    invoice_acct.cust_account_id INVOICE_ACCT_CUST_ACCOUNT_ID,
    oh.header_id ORDER_HEADER_HEADER_ID,
    ol.line_id ORDER_LINE_LINE_ID,
    qph.LIST_HEADER_ID PRICE_LIST_LIST_HEADER_ID,
    accrule.rule_id accrule_rule_id,
    invrule.rule_id invrule_rule_id,
    term.term_id term_term_id,
    trx_type.TRANSACTION_TYPE_ID TRX_TYPE_TRANSACTION_TYPE_ID,
    wdd.delivery_detail_id delivery_detail_id ,
    ol.ordered_quantity * ol.unit_list_price extented_price ,
    OH.TRANSACTIONAL_CURR_CODE CURRENCY,
    -- primary keys added for components and joins
    PARTY.PARTY_ID PARTY_ID ,
    BILL_SU.SITE_USE_ID BILL_SU_SITE_USE_ID,
    SHIP_REL.DIRECTIONAL_FLAG SHIP_REL_DIRECTIONAL_FLAG,
    INVOICE_REL.DIRECTIONAL_FLAG INVOIC_REL_DIRCTNL_FLAG
    --descr#flexfield#start
     --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters ship_from_org,
    hz_cust_site_uses ship_su,
    hz_party_sites ship_ps,
    hz_locations ship_loc,
    hz_cust_acct_sites ship_cas,
    hz_cust_site_uses bill_su,
    hz_party_sites bill_ps,
    hz_locations bill_loc,
    hz_cust_acct_sites bill_cas,
    hz_parties party,
    hz_cust_accounts cust_acct,
    hz_cust_account_roles ship_roles,
    hz_parties ship_party,
    hz_relationships ship_rel,
    hz_cust_accounts ship_acct,
    ar_lookups ship_arl,
    hz_cust_account_roles invoice_roles,
    hz_parties invoice_party,
    hz_relationships invoice_rel,
    hz_cust_accounts invoice_acct,
    ar_lookups invoice_arl,
    oe_order_headers oh,
    oe_order_lines_all ol,
    qp_list_headers_vl qphvl,
    qp_list_headers_b qph,
    ra_rules accrule,
    ra_rules invrule,
    ra_terms_vl term,
    oe_transaction_types_vl trx_type,
    WSH_DELIVERY_DETAILS wdd
  WHERE ol.line_type_id               = trx_type.transaction_type_id
  AND ol.price_list_id                = qphvl.list_header_id(+)
  AND qphvl.list_header_id            = qph.list_header_id(+)
  AND ol.accounting_rule_id           = accrule.rule_id(+)
  AND ol.invoicing_rule_id            = invrule.rule_id(+)
  AND ol.payment_term_id              = term.term_id(+)
  AND ol.sold_to_org_id               = cust_acct.cust_account_id(+)
  AND cust_acct.party_id              = party.party_id(+)
  AND ol.ship_from_org_id             = ship_from_org.organization_id(+)
  AND ol.ship_to_org_id               = ship_su.site_use_id(+)
  AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
  AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
  AND ship_loc.location_id(+)         = ship_ps.location_id
  AND ol.invoice_to_org_id            = bill_su.site_use_id(+)
  AND bill_su.cust_acct_site_id       = bill_cas.cust_acct_site_id(+)
  AND bill_cas.party_site_id          = bill_ps.party_site_id(+)
  AND bill_loc.location_id(+)         = bill_ps.location_id
  AND ol.ship_to_contact_id           = ship_roles.cust_account_role_id(+)
  AND ship_roles.party_id             = ship_rel.party_id(+)
  AND ship_roles.role_type(+)         = 'CONTACT'
  AND ship_rel.subject_id             = ship_party.party_id(+)
  AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
  AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
  AND ship_arl.lookup_type(+)         = 'CONTACT_TITLE'
  AND ship_arl.lookup_code(+)         = ship_party.person_pre_name_adjunct
  AND ol.invoice_to_contact_id        = invoice_roles.cust_account_role_id(+)
  AND invoice_roles.party_id          = invoice_rel.party_id(+)
  AND invoice_roles.role_type(+)      = 'CONTACT'
  AND invoice_rel.subject_id          = invoice_party.party_id(+)
  AND invoice_roles.cust_account_id   = invoice_acct.cust_account_id(+)
  AND NVL (invoice_rel.object_id, -1) = NVL (invoice_acct.party_id, -1)
  AND invoice_arl.lookup_type(+)      = 'CONTACT_TITLE'
  AND invoice_arl.lookup_code(+)      = invoice_party.person_pre_name_adjunct
  AND ol.header_id                    = oh.header_id
  AND oh.header_id                    = wdd.source_header_id
  and OL.LINE_ID                      = WDD.SOURCE_LINE_ID
  and WDD.RELEASED_STATUS             ='B';