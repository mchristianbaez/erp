CREATE OR REPLACE FORCE VIEW xxeis.xxeis_check_register
(
   org_id
  ,vendor_type
  ,check_number
  ,check_date
  ,check_amount
  ,payment_amount
  ,check_void_date
  ,check_status
  ,invoice_number
  ,invoice_date
  ,invoice_amount
  ,case_identifier
  ,deduction_code
  ,employee
  ,vendor_number
  ,description
  ,gl_date
  ,vendor_name
  ,vendor_site
  ,invoice_type
  ,pay_group
  ,payment_method
  ,invoice_payment_status
  ,period_name
  ,invoice_approval_status
)
AS
   SELECT i.org_id org_id
         ,v.vendor_type_lookup_code vendor_type
         ,c.check_number check_number
         ,c.check_date check_date
         ,c.amount check_amount
         ,p.amount payment_amount
         ,c.void_date check_void_date
         ,c.status_lookup_code check_status
         ,i.invoice_num invoice_number
         ,i.invoice_date invoice_date
         ,i.invoice_amount invoice_amount
         ,i.attribute1 case_identifier
         ,i.attribute2 deduction_code
         ,i.attribute3 employee
         ,v.segment1 vendor_number
         ,i.description description
         ,i.gl_date gl_date
         ,v.vendor_name vendor_name
         ,s.vendor_site_code vendor_site
         ,i.invoice_type_lookup_code invoice_type
         ,i.pay_group_lookup_code pay_group
         ,i.payment_method_lookup_code payment_method
         ,i.payment_status_flag invoice_payment_status
         ,p.period_name period_name
         ,CASE d.accrual_posted_flag
             WHEN 'Y' THEN 'Validated'
             WHEN 'N' THEN 'Needs Re-Validation'
          END
             invoice_approval_status
     FROM ap.ap_invoices_all i
         ,ap.ap_invoice_payments_all p
         ,ap.ap_invoice_distributions_all d
         ,ap.ap_suppliers v
         ,ap.ap_supplier_sites_all s
         ,ap.ap_checks_all c
    WHERE     i.vendor_id = v.vendor_id
          AND i.vendor_site_id = s.vendor_site_id
          AND i.invoice_id = p.invoice_id
          AND p.check_id = c.check_id
          AND i.invoice_id = d.invoice_id;


