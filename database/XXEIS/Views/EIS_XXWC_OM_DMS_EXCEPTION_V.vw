-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_DMS_EXCEPTION_V.vw $
  Module Name: Order Management
  PURPOSE: DMS 2.0 Exception Report - WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 3/12/2018      Siva				TMS#20180228-00244
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_DMS_EXCEPTION_V (BRANCH, ORDER_NUMBER, DELIVERY_ID, LINE_NUMBER, ITEM_NUMBER, STOP_EXCEPTION, LINE_EXCEPTION, DRIVER_NAME, ROUTE_NAME, CREATION_DATE, DISPATCH_DATE)
AS
  SELECT xomds.branch,
    xomds.order_number,
    xomds.delivery_id,
    xomds.line_number,
    xomds.item_number,
    xomds.stop_exception,
    xomds.line_exception,
    xomds.driver_name,
    xomds.route_name,
    TRUNC(xomds.creation_date)creation_date,
    xomds.dispatch_date
  FROM xxwc.xxwc_om_dms2_ship_confirm_tbl xomds
  WHERE 1                    =1
  AND (xomds.stop_exception IS NOT NULL
  OR xomds.line_exception   IS NOT NULL)
  AND XOMDS.DELIVERY_ID     IS NOT NULL
/
