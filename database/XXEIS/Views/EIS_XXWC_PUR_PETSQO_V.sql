------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_PUR_PETSQO_V
   PURPOSE    :  Replaced the old view definition with new view.This view is created based on 
				 custom table XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_TAB and this custom table data
				 gets populated by XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_PKG package.
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0		 04/29/2016   PRAMOD  			TMS#20160429-00036  Performance Tuning                                         
******************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_PUR_PETSQO_V (PROCESS_ID, MVID, VENDOR, LOB, BU, AGREEMENT_YEAR, FISCAL_PERIOD, PURCHASES, COOP, REBATE, TOTAL_ACCRUALS) AS 
  SELECT /*+ INDEX(PUR EIS_XXWC_PUR_ACC_FIS_PER_N1)*/PROCESS_ID,
    MVID,
    VENDOR_NAME VENDOR,
    LOB,
    BU,
    AGREEMENT_YEAR,
    fiscal_period,
    SUM (PURCHASES) PURCHASES,
    SUM (COOP) COOP,
    SUM (REBATE) REBATE,
    SUM (TOTAL_ACCRUALS) TOTAL_ACCRUALS
  FROM
    ( SELECT * FROM XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_TAB M   ) PUR
  GROUP BY process_id,
    MVID,
    VENDOR_NAME,
    LOB,
    BU,
    AGREEMENT_YEAR,
    FISCAL_PERIOD
/
