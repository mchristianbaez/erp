CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_simp_prof_v
(
   location
  ,branch_name
  ,master_account_number
  ,master_account_name
  ,cat_class_number
  ,cat_class_name
  ,salesrep_number
  ,salesrep_name
  ,ship_to_site_number
  ,ship_to_location
  ,site_salesrep_number
  ,site_salesrep_name
  ,sales
  ,extended_branch_cost
  ,ordered_date
  ,order_number
  ,line_number
  ,item_number
  ,header_id
  ,line_id
  ,cust_account_id
  ,party_id
  ,salesrep_id
  ,organization_id
  ,inventory_item_id
  ,msi_organization_id
)
AS
   SELECT ood.organization_code location
         ,ood.organization_name branch_name
         ,hca_bill.account_number master_account_number
         ,hca_bill.account_name master_account_name
         ,mcv.segment2 cat_class_number
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc (
             msi.inventory_item_id
            ,msi.organization_id)
             cat_class_name
         ,rep.salesrep_number salesrep_number
         ,rep.name salesrep_name
         ,hzps_ship_to.party_site_number ship_to_site_number
         ,hzcs_ship_to.location ship_to_location
         ,rep_site.salesrep_number site_salesrep_number
         ,rep_site.name site_salesrep_name
         , (ol.ordered_quantity * NVL (ol.unit_selling_price, 0)) sales
         , (  (ol.ordered_quantity)
            * NVL (
                 apps.cst_cost_api.get_item_cost (1
                                                 ,msi.inventory_item_id
                                                 ,msi.organization_id)
                ,0))
             extended_branch_cost
         ,TRUNC (oh.ordered_date) ordered_date
         ,oh.order_number
         ,DECODE (
             ol.option_number
            ,'', (ol.line_number || '.' || ol.shipment_number)
            , (   ol.line_number
               || '.'
               || ol.shipment_number
               || '.'
               || ol.option_number))
             line_number
         ,msi.segment1 item_number
         ,                                                     ---Primary Keys
          oh.header_id
         ,ol.line_id
         ,hca_bill.cust_account_id
         ,hzp_bill.party_id
         ,rep.salesrep_id
         ,ood.organization_id
         ,msi.inventory_item_id
         ,msi.organization_id msi_organization_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,hz_cust_accounts hca_bill
         ,hz_parties hzp_bill
         ,hz_cust_site_uses hzcs_bill_to
         ,hz_cust_site_uses hzcs_ship_to
         ,hz_cust_acct_sites hcas_ship_to
         ,hz_party_sites hzps_ship_to
         ,hz_locations hzl_ship_to
         ,ra_salesreps rep
         ,org_organization_definitions ood
         ,mtl_system_items_kfv msi
         ,mtl_categories_kfv mcv
         ,ra_salesreps rep_site
         ,mtl_category_sets mcs
         ,mtl_item_categories mic
         ,fnd_id_flex_segments ffs
         ,fnd_flex_values_vl flex_val
    WHERE     oh.header_id = ol.header_id
          AND oh.sold_to_org_id = hca_bill.cust_account_id(+)
          AND hca_bill.party_id = hzp_bill.party_id(+)
          AND oh.invoice_to_org_id = hzcs_bill_to.site_use_id(+)
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.ship_to_org_id = hzcs_ship_to.site_use_id(+)
          AND hzcs_ship_to.cust_acct_site_id =
                 hcas_ship_to.cust_acct_site_id(+)
          AND hcas_ship_to.party_site_id = hzps_ship_to.party_site_id(+)
          AND hzl_ship_to.location_id(+) = hzps_ship_to.location_id
          AND oh.ship_from_org_id = ood.organization_id
          AND ol.inventory_item_id = msi.inventory_item_id
          AND ol.ship_from_org_id = msi.organization_id
          AND hzcs_ship_to.primary_salesrep_id = rep_site.salesrep_id(+)
          AND mcs.category_set_name = 'Inventory Category'
          AND mcs.structure_id = mcv.structure_id
          AND mic.inventory_item_id = msi.inventory_item_id
          AND mic.organization_id = msi.organization_id
          AND mic.category_set_id = mcs.category_set_id
          AND mic.category_id = mcv.category_id
          AND flex_val.flex_value = mcv.segment2
          AND ffs.flex_value_set_id = flex_val.flex_value_set_id
          AND ffs.id_flex_num = 101
          AND EXISTS
                 (SELECT 1
                    FROM fnd_flex_value_sets fs, fnd_flex_values_vl fv
                   WHERE     fs.flex_value_set_id = fv.flex_value_set_id
                         AND flex_value_set_name = 'XXEIS_SIMPSON_CUSTOMER'
                         AND fv.flex_value_meaning = hca_bill.account_number)
          AND ffs.id_flex_code = 'MCAT'
          AND ffs.application_column_name = 'SEGMENT2'
          -- and ffvs.flex_value_set_name         ='XXCUS CATEGORY CLASS'
          --AND upper(flex_val.description) LIKE 'SIMPSON%'
          --and Mcv.segment2 like '9000%'
          AND flex_val.flex_value BETWEEN '9000' AND '9030'
          AND ol.flow_status_code = 'CLOSED';


