---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_CUST_RECON_RECPT_V $
  Module Name : Receivables
  PURPOSE	  : Customer Account Recon - Receipt View
  TMS Task Id : 20130909-00776 
  REVISIONS   :
  VERSION 		  DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
   1.0     		03-Oct-2016       		Siva   		 TMS#20130909-00776  
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_CUST_RECON_RECPT_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_CUST_RECON_RECPT_V
AS
  SELECT CUSTOMER_NAME,
    CUSTOMER_NUMBER,
    PHONE_NUMBER,
    CREDIT_MANAGER,
    ACCOUNT_MANAGER,
    PAYMENT_TERMS,
    ACCOUNT_STATUS,
    PARTY_SITE_NUMBER,
    LOCATION,
    RECEIPT_NUMBER,
    RECEIPT_DATE,
    TRX_DATE,
    TRANSACTION_NUMBER,
    TRX_AMOUNT_DUE_ORIGINAL,
	TRANSACTION_BALANCE_DUE,
    APPLIED_AMOUNT,
    STATE,
    EARNED_DISCOUNT,
    UNEARNED_DISCOUNT,
    DEPOSIT_DATE,
    APPLIED_DATE,
    PAYMENT_METHOD ,
    RECEIPT_COMMENTS,
    DISCREPANCY_CODE,
    RECEIPT_AMOUNT,
    UNAPPLIED_AMOUNT,
    PREPAYMENT_AMOUNT,
    ON_ACCOUNT_AMOUNT
FROM XXEIS.XXWC_CUST_REC_RECEIPT_TMP_TBL
/
