CREATE OR REPLACE FORCE VIEW apps.xxeis_631484_agwgwy_v
(
   party_creation_date
  ,account_creation_date
  ,account_number
  ,cleansed_name
  ,account_name
  ,profile_class_name
  ,predominant_trade
  ,customer__classification
  ,sales_channel
  ,billing_address_1
  ,billing_address_2
  ,billing_address_3
  ,billing_address_4
  ,billing_city
  ,billing_state
  ,billing_zip
  ,sic_code
  ,account_manager_name
  ,eeid
  ,phone_number
  ,email_address
  ,yard_limit
)
AS
   SELECT hp.creation_date party_creation_date
         ,hca.creation_date account_creation_date
         ,hca.account_number account_number
         ,TRIM (
             REPLACE (
                REPLACE (REPLACE (REPLACE (REPLACE (hca.account_name, 'CASH', ''), 'cash', ''), '%', ' ')
                        ,'/'
                        ,'')
               ,'Cash'
               ,''))
             cleansed_name
         ,hca.account_name account_name
         ,hcpc.name profile_class_name
         ,hca.attribute9 predominant_trade
         ,hca.customer_class_code customer__classification
         ,hca.sales_channel_code sales_channel
         ,hl.address1 billing_address_1
         ,hl.address2 billing_address_2
         ,hl.address3 billing_address_3
         ,hl.address4 billing_address_4
         ,hl.city billing_city
         ,hl.state billing_state
         ,hl.postal_code billing_zip
         ,hop.sic_code sic_code
         ,jrdv.resource_name account_manager_name
         ,jrdv.source_number eeid
         ,hcpts.raw_phone_number phone_number
         ,hcpts2.email_address email_address
         ,                                                     ---- 7/23/2014 diane added start 20140220-00170
           (SELECT b.overall_credit_limit
              FROM ar.hz_cust_site_uses_all a, ar.hz_cust_profile_amts b, ar.hz_cust_acct_sites_all c
             WHERE     1 = 1
                   AND c.cust_account_id = hca.cust_account_id
                   AND a.status = 'A'
                   AND c.status = 'A'
                   AND c.cust_acct_site_id = a.cust_acct_site_id
                   AND c.cust_account_id = b.cust_account_id
                   AND a.site_use_id = b.site_use_id
                   AND a.attribute1 = 'YARD'
                   AND ROWNUM = 1)
             yard_limit                                       --- 7/23/2014 diane added end TMS 20140220-00170
     FROM ar.hz_organization_profiles hop
         ,ar.hz_party_sites hps
         ,ar.hz_locations hl
         ,ar.hz_customer_profiles hcp
         ,ar.hz_cust_profile_classes hcpc
         ,ar.hz_parties hp
          INNER JOIN ar.hz_cust_accounts hca ON hp.party_id = hca.party_id
          INNER JOIN ar.hz_cust_acct_sites_all hcasa ON hca.cust_account_id = hcasa.cust_account_id
          INNER JOIN ar.hz_cust_site_uses_all hcsua ON hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
          LEFT OUTER JOIN apps.jtf_rs_salesreps jrs ON jrs.salesrep_id = hcsua.primary_salesrep_id
          LEFT OUTER JOIN apps.jtf_rs_defresources_v jrdv ON jrs.resource_id = jrdv.resource_id
          LEFT OUTER JOIN ar.hz_contact_points hcpts
             ON     hcpts.owner_table_id = hp.party_id
                AND hcpts.contact_point_type = 'PHONE'
                AND hcpts.primary_flag = 'Y'
                AND hcpts.status = 'A'
                AND hcpts.owner_table_name = 'HZ_PARTIES'
          LEFT OUTER JOIN ar.hz_contact_points hcpts2
             ON     hcpts2.owner_table_id = hp.party_id
                AND hcpts2.contact_point_type = 'EMAIL'
                AND hcpts2.primary_flag = 'Y'
                AND hcpts2.status = 'A'
                AND hcpts2.owner_table_name = 'HZ_PARTIES'
    WHERE     hop.party_id = hp.party_id
          AND hcasa.party_site_id = hps.party_site_id
          AND hps.location_id = hl.location_id
          AND hca.cust_account_id = hcp.cust_account_id
          AND hcp.profile_class_id = hcpc.profile_class_id
          AND hcsua.site_use_code = 'BILL_TO'
          AND hcsua.primary_flag = 'Y'
          AND hcp.site_use_id IS NULL
          AND (jrs.org_id = '162' OR (jrs.org_id IS NULL AND hcsua.primary_salesrep_id IS NULL))
          AND hop.effective_end_date IS NULL
          AND hcpc.name NOT IN ('Intercompany Customers', 'WC Branches')
          AND hcsua.org_id = '162'
	  AND hca.status = 'A'      		--Added By Mahender Reddy For TMS#20150521-00185 on 06/09/2015
          AND TRUNC (hca.creation_date) > TRUNC (SYSDATE) - 7;