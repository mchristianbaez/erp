-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.EIS_XXWC_CUSTOMER_SALES_V.vw $
  Module Name: Order Management
  PURPOSE: Customer Sales Report - WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 6/19/2018      Siva			   TMS#20180605-00069
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_CUSTOMER_SALES_V (PROMOTION_LINES, CUSTOMER_NAME, CUSTOMER_NUMBER, CUSTOMER_CLASSIFICATION, SALE_AMOUNT, CREATION_DATE, PERIOD_NAME, PERIOD_YEAR)
AS
  SELECT 'Add' Promotion_lines,
    customer_name,
    customer_number,
    CUSTOMER_CLASSIFICATION,
    SUM(NVL(extended_amount_sales,0)) sale_amount,
    CREATION_DATE,
    period_name,
    period_year
  FROM
    (SELECT /*+ INDEX(rbs RA_BATCH_SOURCES_U1) INDEX(rct XXWC_RA_CUSTOMER_TRX_N34)*/
      NVL(hca.account_name,party_name) customer_name,
      hca.account_number customer_number,
      FLV.MEANING CUSTOMER_CLASSIFICATION,
      (SELECT SUM(NVL(extended_amount,0)) extended_amount_sales
      FROM apps.ra_customer_trx_lines rctl
      WHERE line_type         ='LINE'
      AND rctl.customer_trx_id=rct.customer_trx_id
      ) EXTENDED_AMOUNT_SALES,
      TRUNC(rct.CREATION_DATE) CREATION_DATE,
      gp.period_name,
      gp.period_year
    FROM apps.ra_customer_trx rct,
      apps.hz_cust_accounts hca,
      apps.hz_parties hp,
      apps.fnd_lookup_values_vl flv,
      apps.gl_periods gp,
      apps.RA_BATCH_SOURCES rbs
    WHERE rct.bill_to_customer_id=hca.cust_account_id
    AND hca.party_id             =hp.party_id
    AND hca.customer_class_code  =flv.lookup_code(+)
    AND flv.lookup_type(+)       ='CUSTOMER CLASS'
    AND gp.period_set_name       ='4-4-QTR'
    AND TRUNC(rct.creation_date) BETWEEN gp.start_date AND gp.end_date
    AND RCT.BATCH_SOURCE_ID=RBS.BATCH_SOURCE_ID
    AND rbs.NAME          IN ('WC MANUAL', 'MANUAL-OTHER', 'REBILL', 'REBILL-CM')
	and RBS.name not in  ('CONVERSION-AHHARRIS','HARRIS','HARRIS REFUND')
    AND RCT.ORG_ID         =RBS.ORG_ID
    UNION ALL
    SELECT  /*+ INDEX(rbs RA_BATCH_SOURCES_U1) INDEX(rct XXWC_RA_CUSTOMER_TRX_N34)*/
      NVL(hca.account_name,party_name) customer_name,
      hca.account_number customer_number,
      FLV.MEANING CUSTOMER_CLASSIFICATION,
      (SELECT SUM(NVL(extended_amount,0)) extended_amount_sales
      FROM apps.ra_customer_trx_lines rctl
      WHERE line_type         ='LINE'
      AND rctl.customer_trx_id=rct.customer_trx_id
      ) EXTENDED_AMOUNT_SALES,
      (TRUNC(rct.creation_date) - 1) creation_date,
      gp.period_name,
      gp.period_year
    FROM apps.ra_customer_trx rct,
      apps.hz_cust_accounts hca,
      apps.hz_parties hp,
      apps.fnd_lookup_values_vl flv,
      apps.gl_periods gp,
      apps.RA_BATCH_SOURCES rbs
    WHERE rct.bill_to_customer_id=hca.cust_account_id
    AND hca.party_id             =hp.party_id
    AND hca.customer_class_code  =flv.lookup_code(+)
    AND flv.lookup_type(+)       ='CUSTOMER CLASS'
    AND GP.PERIOD_SET_NAME       ='4-4-QTR'
    AND TRUNC(rct.creation_date)-1 BETWEEN gp.start_date AND gp.end_date
    AND RCT.BATCH_SOURCE_ID=RBS.BATCH_SOURCE_ID
    AND rbs.NAME NOT      IN ('WC MANUAL', 'MANUAL-OTHER', 'REBILL', 'REBILL-CM','CONVERSION-AHHARRIS','HARRIS','HARRIS REFUND')
    AND rct.org_id         =rbs.org_id
    )
  GROUP BY customer_name,
    customer_number,
    customer_classification ,
    CREATION_DATE,
    period_name,
    PERIOD_YEAR
  UNION ALL
  SELECT 'Remove' Promotion_lines,
    customer_name,
    customer_number,
    CUSTOMER_CLASSIFICATION,
    SUM(NVL(extended_amount_sales,0)) sale_amount,
    CREATION_DATE,
    period_name,
    period_year
  FROM
    (SELECT /*+ INDEX(rbs RA_BATCH_SOURCES_U1) INDEX(rct XXWC_RA_CUSTOMER_TRX_N34)*/
      NVL(hca.account_name,party_name) customer_name,
      hca.account_number customer_number,
      FLV.MEANING CUSTOMER_CLASSIFICATION,
      (SELECT SUM(NVL(extended_amount,0)) extended_amount_sales
      FROM apps.ra_customer_trx_lines rctl
      WHERE line_type         ='LINE'
      AND rctl.customer_trx_id=rct.customer_trx_id
	  AND RCTL.INTERFACE_LINE_CONTEXT = 'ORDER ENTRY'
      AND NOT EXISTS
        (SELECT 1
        FROM APPS.OE_ORDER_LINES OOL,
          APPS.MTL_SYSTEM_ITEMS_B MSI,
          APPS.MTL_ITEM_CATEGORIES MIC,
          apps.mtl_categories_b mc
        WHERE RCTL.INTERFACE_LINE_CONTEXT = 'ORDER ENTRY'
        AND TO_CHAR (OOL.LINE_ID)         = RCTL.INTERFACE_LINE_ATTRIBUTE6
        AND OOL.INVENTORY_ITEM_ID         = MSI.INVENTORY_ITEM_ID
        AND OOL.SHIP_FROM_ORG_ID          = MSI.ORGANIZATION_ID
        AND MSI.INVENTORY_ITEM_ID         = MIC.INVENTORY_ITEM_ID
        AND MSI.ORGANIZATION_ID           = MIC.ORGANIZATION_ID
        AND MIC.CATEGORY_ID               = MC.CATEGORY_ID
        AND MC.segment1                   ='PR'
        AND MC.SEGMENT2                   ='PRMO'
        )
      ) EXTENDED_AMOUNT_SALES,
      TRUNC(rct.CREATION_DATE) CREATION_DATE,
      gp.period_name,
      gp.period_year
    FROM apps.ra_customer_trx rct,
      apps.hz_cust_accounts hca,
      apps.hz_parties hp,
      apps.fnd_lookup_values_vl flv,
      apps.gl_periods gp,
      apps.RA_BATCH_SOURCES rbs
    WHERE rct.bill_to_customer_id=hca.cust_account_id
    AND hca.party_id             =hp.party_id
    AND hca.customer_class_code  =flv.lookup_code(+)
    AND flv.lookup_type(+)       ='CUSTOMER CLASS'
    AND gp.period_set_name       ='4-4-QTR'
    AND TRUNC(rct.creation_date) BETWEEN gp.start_date AND gp.end_date
    AND RCT.BATCH_SOURCE_ID=RBS.BATCH_SOURCE_ID
    AND rbs.NAME          IN ('WC MANUAL', 'MANUAL-OTHER', 'REBILL', 'REBILL-CM')
	and RBS.name not in  ('CONVERSION-AHHARRIS','HARRIS','HARRIS REFUND')
    AND RCT.ORG_ID         =RBS.ORG_ID
    /*AND NOT EXISTS
    (SELECT 1
    FROM APPS.HZ_CUSTOMER_PROFILES HCP,
      apps.HZ_CUST_PROFILE_CLASSES hcpc
    WHERE HCP.SITE_USE_ID   = RCT.BILL_TO_SITE_USE_ID
    AND HCP.CUST_ACCOUNT_ID = RCT.BILL_TO_CUSTOMER_ID
    AND HCP.PROFILE_CLASS_ID= HCPC.PROFILE_CLASS_ID
    AND upper(hcpc.name)    = UPPER('WC Branches')
    )*/
    AND NOT EXISTS
    (SELECT 1
    FROM APPS.OE_ORDER_HEADERS OOHA,
      APPS.OE_TRANSACTION_TYPES_TL OTT
    WHERE OOHA.ORDER_TYPE_ID           =OTT.TRANSACTION_TYPE_ID
    AND RCT.INTERFACE_HEADER_CONTEXT   = 'ORDER ENTRY'
    AND RCT.INTERFACE_HEADER_ATTRIBUTE1=TO_CHAR(OOHA.ORDER_NUMBER)
    and UPPER(OTT.name) like '%INTERNAL%'
    )
    UNION ALL
    SELECT /*+ INDEX(rbs RA_BATCH_SOURCES_U1) INDEX(rct XXWC_RA_CUSTOMER_TRX_N34)*/
      NVL(hca.account_name,party_name) customer_name,
      hca.account_number customer_number,
      FLV.MEANING CUSTOMER_CLASSIFICATION,
      (SELECT SUM(NVL(extended_amount,0)) extended_amount_sales
      FROM apps.ra_customer_trx_lines rctl
      WHERE line_type         ='LINE'
      AND rctl.customer_trx_id=rct.customer_trx_id
	  AND RCTL.INTERFACE_LINE_CONTEXT = 'ORDER ENTRY'
      AND NOT EXISTS
        (SELECT 1
        FROM APPS.OE_ORDER_LINES OOL,
          APPS.MTL_SYSTEM_ITEMS_B MSI,
          APPS.MTL_ITEM_CATEGORIES MIC,
          apps.mtl_categories_b mc
        WHERE RCTL.INTERFACE_LINE_CONTEXT = 'ORDER ENTRY'
        AND TO_CHAR (OOL.LINE_ID)         = RCTL.INTERFACE_LINE_ATTRIBUTE6
        AND OOL.INVENTORY_ITEM_ID         = MSI.INVENTORY_ITEM_ID
        AND OOL.SHIP_FROM_ORG_ID          = MSI.ORGANIZATION_ID
        AND MSI.INVENTORY_ITEM_ID         = MIC.INVENTORY_ITEM_ID
        AND MSI.ORGANIZATION_ID           = MIC.ORGANIZATION_ID
        AND MIC.CATEGORY_ID               = MC.CATEGORY_ID
        AND MC.segment1                   ='PR'
        AND MC.SEGMENT2                   ='PRMO'
        )
      ) EXTENDED_AMOUNT_SALES,
      (TRUNC(rct.creation_date) - 1) creation_date,
      gp.period_name,
      gp.period_year
    FROM apps.ra_customer_trx rct,
      apps.hz_cust_accounts hca,
      apps.hz_parties hp,
      apps.fnd_lookup_values_vl flv,
      apps.gl_periods gp,
      apps.RA_BATCH_SOURCES rbs
    WHERE rct.bill_to_customer_id=hca.cust_account_id
    AND hca.party_id             =hp.party_id
    AND hca.customer_class_code  =flv.lookup_code(+)
    AND flv.lookup_type(+)       ='CUSTOMER CLASS'
    AND GP.PERIOD_SET_NAME       ='4-4-QTR'
    AND TRUNC(rct.creation_date)-1 BETWEEN gp.start_date AND gp.end_date
    AND RCT.BATCH_SOURCE_ID=RBS.BATCH_SOURCE_ID
    AND rbs.NAME NOT      IN ('WC MANUAL', 'MANUAL-OTHER', 'REBILL', 'REBILL-CM','CONVERSION-AHHARRIS','HARRIS','HARRIS REFUND')
    AND rct.org_id         =rbs.org_id
	/*AND NOT EXISTS
    (SELECT 1
    FROM APPS.HZ_CUSTOMER_PROFILES HCP,
      apps.HZ_CUST_PROFILE_CLASSES hcpc
    WHERE HCP.SITE_USE_ID   = RCT.BILL_TO_SITE_USE_ID
    AND HCP.CUST_ACCOUNT_ID = RCT.BILL_TO_CUSTOMER_ID
    AND HCP.PROFILE_CLASS_ID= HCPC.PROFILE_CLASS_ID
    AND upper(hcpc.name)    = UPPER('WC Branches')
    )*/
    AND NOT EXISTS
    (SELECT 1
    FROM APPS.OE_ORDER_HEADERS OOHA,
      APPS.OE_TRANSACTION_TYPES_TL OTT
    WHERE OOHA.ORDER_TYPE_ID           =OTT.TRANSACTION_TYPE_ID
    AND RCT.INTERFACE_HEADER_CONTEXT   = 'ORDER ENTRY'
    AND RCT.INTERFACE_HEADER_ATTRIBUTE1=TO_CHAR(OOHA.ORDER_NUMBER)
    AND upper(OTT.name) LIKE '%INTERNAL%'
    )
    )
  GROUP BY customer_name,
    customer_number,
    customer_classification ,
    CREATION_DATE,
    PERIOD_NAME,
    PERIOD_YEAR
/
