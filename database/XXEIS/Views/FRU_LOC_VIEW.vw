CREATE OR REPLACE FORCE VIEW xxeis.fru_loc_view
(
   fru
  ,fru_descr
  ,inactive
  ,lob_branch
  ,lob_dept
  ,entrp_entity
  ,entrp_loc
  ,entrp_cc
  ,ap_docum_flag
  ,hc_flag
  ,creation_dt
  ,system_cd
  ,system_code
  ,iexp_fru_override
)
AS
   SELECT "FRU"
         ,"FRU_DESCR"
         ,"INACTIVE"
         ,"LOB_BRANCH"
         ,"LOB_DEPT"
         ,"ENTRP_ENTITY"
         ,"ENTRP_LOC"
         ,"ENTRP_CC"
         ,"AP_DOCUM_FLAG"
         ,"HC_FLAG"
         ,"CREATION_DT"
         ,"SYSTEM_CD"
         ,"SYSTEM_CODE"
         ,"IEXP_FRU_OVERRIDE"
     FROM apps.xxcus_location_code_vw;


