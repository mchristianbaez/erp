CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_po_isr_rpt_v
(
   org
  ,pre
  ,item_number
  ,vendor_num
  ,vendor_name
  ,source
  ,st
  ,description
  ,cat
  ,pplt
  ,plt
  ,uom
  ,cl
  ,stk_flag
  ,pm
  ,minn
  ,maxn
  ,amu
  ,mf_flag
  ,hit6_sales
  ,aver_cost
  ,item_cost
  ,bpa_cost
  ,bpa
  ,qoh
  ,available
  ,availabledollar
  ,jan_sales
  ,feb_sales
  ,mar_sales
  ,apr_sales
  ,may_sales
  ,june_sales
  ,jul_sales
  ,aug_sales
  ,sep_sales
  ,oct_sales
  ,nov_sales
  ,dec_sales
  ,hit4_sales
  ,one_sales
  ,six_sales
  ,twelve_sales
  ,bin_loc
  ,mc
  ,fi_flag
  ,freeze_date
  ,res
  ,thirteen_wk_avg_inv
  ,thirteen_wk_an_cogs
  ,turns
  ,buyer
  ,ts
  ,so
  ,inventory_item_id
  ,organization_id
  ,set_of_books_id
  ,on_ord
  ,wt
  ,ss
  ,fml
  ,open_req
  ,org_name
  ,district
  ,region
  ,sourcing_rule
  ,clt
  ,common_output_id
  ,process_id
  ,avail2
  ,int_req
  ,dir_req
  ,demand
)
AS
   SELECT varchar2_col1 "ORG"
         ,varchar2_col2 "PRE"
         ,varchar2_col3 "ITEM_NUMBER"
         ,varchar2_col4 "VENDOR_NUM"
         ,varchar2_col5 "VENDOR_NAME"
         ,varchar2_col6 "SOURCE"
         ,varchar2_col7 "ST"
         ,varchar2_col8 "DESCRIPTION"
         ,varchar2_col9 "CAT"
         ,varchar2_col10 "PPLT"
         ,varchar2_col11 "PLT"
         ,varchar2_col12 "UOM"
         ,varchar2_col13 "CL"
         ,varchar2_col14 "STK_FLAG"
         ,varchar2_col15 "PM"
         ,varchar2_col16 "MINN"
         ,varchar2_col17 "MAXN"
         ,varchar2_col18 "AMU"
         ,varchar2_col19 "MF_FLAG"
         ,number_col1 hit6_sales
         ,number_col2 "AVER_COST"
         ,number_col3 "ITEM_COST"
         ,number_col4 "BPA_COST"
         ,varchar2_col20 "BPA"
         ,number_col5 "QOH"
         ,number_col6 "AVAILABLE"
         ,number_col7 "AVAILABLEDOLLAR"
         ,number_col8 jan_sales
         ,number_col9 feb_sales
         ,number_col10 mar_sales
         ,number_col11 apr_sales
         ,number_col12 may_sales
         ,number_col13 june_sales
         ,number_col14 jul_sales
         ,number_col15 aug_sales
         ,number_col16 sep_sales
         ,number_col17 oct_sales
         ,number_col18 nov_sales
         ,number_col19 dec_sales
         ,number_col20 hit4_sales
         ,number_col21 one_sales
         ,number_col22 six_sales
         ,number_col23 twelve_sales
         ,varchar2_col21 "BIN_LOC"
         ,varchar2_col22 "MC"
         ,varchar2_col23 "FI_FLAG"
         ,date_col1 "FREEZE_DATE"
         ,varchar2_col24 "RES"
         ,number_col24 "THIRTEEN_WK_AVG_INV"
         ,number_col25 "THIRTEEN_WK_AN_COGS"
         ,number_col26 "TURNS"
         ,varchar2_col25 "BUYER"
         ,varchar2_col26 "TS"
         ,number_col27 "SO"
         ,number_col28 "INVENTORY_ITEM_ID"
         ,number_col29 "ORGANIZATION_ID"
         ,number_col30 "SET_OF_BOOKS_ID"
         ,number_col31 "ON_ORD"
         ,number_col32 "WT"
         ,number_col33 "SS"
         ,number_col34 "FML"
         ,number_col35 "OPEN_REQ"
         ,varchar2_col27 "ORG_NAME"
         ,varchar2_col28 "DISTRICT"
         ,varchar2_col29 "REGION"
         ,varchar2_col30 "SOURCING_RULE"
         ,number_col36 "CLT"
         ,common_output_id
         ,process_id
         ,number_col37 "AVAIL2"
         ,number_col38 "INT_REQ"
         ,number_col39 "DIR_REQ"
         ,number_col40 "DEMAND"
     FROM xxeis.eis_rs_common_outputs;


