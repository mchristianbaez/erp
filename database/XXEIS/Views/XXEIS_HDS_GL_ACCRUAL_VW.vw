CREATE OR REPLACE FORCE VIEW xxeis.xxeis_hds_gl_accrual_vw
(
   line_amount
  ,oracle_product
  ,oracle_location
  ,oracle_cost_center
  ,oracle_account
  ,project
  ,future
  ,full_name
  ,merchant_name
  ,transaction_date
  ,item_description
  ,fru
  ,card_program_name
  ,mcc_code
  ,start_expense_date
  ,end_expense_date
)
AS
   SELECT xxcus_te_accrual_hist_tbl.line_amount
         ,NVL (xxcus_te_accrual_hist_tbl.oracle_product, '48') oracle_product
         ,NVL (xxcus_te_accrual_hist_tbl.oracle_location, 'Z0186')
             oracle_location
         ,NVL (xxcus_te_accrual_hist_tbl.oracle_cost_center, '0000')
             oracle_cost_center
         ,NVL (xxcus_te_accrual_hist_tbl.oracle_account, '658020')
             oracle_account
         ,NVL (xxcus_te_accrual_hist_tbl.segment5, '00000') project
         ,NVL (xxcus_te_accrual_hist_tbl.segment6, '00000') future
         ,NVL (xxcus_te_accrual_hist_tbl.full_name, 'No Employee') full_name
         ,xxcus_te_accrual_hist_tbl.merchant_name
         ,xxcus_te_accrual_hist_tbl.transaction_date
         ,xxcus_te_accrual_hist_tbl.item_description
         ,xxcus_te_accrual_hist_tbl.fru
         ,xxcus_te_accrual_hist_tbl.card_program_name
         ,xxcus_te_accrual_hist_tbl.mcc_code
         ,xxcus_te_accrual_hist_tbl.start_expense_date
         ,xxcus_te_accrual_hist_tbl.end_expense_date
     FROM xxcus.xxcusgl_te_accrual_tbl xxcus_te_accrual_hist_tbl
    WHERE        xxcus_te_accrual_hist_tbl.imported_to_gl = 'N'
             AND NVL (xxcus_te_accrual_hist_tbl.item_description, 'X') <>
                    'Personal'
             AND NVL (xxcus_te_accrual_hist_tbl.credit_card_trx_id, -1) <>
                    652460
             AND NVL (xxcus_te_accrual_hist_tbl.merchant_name, 'X') <>
                    'CR BAL REFUND'
             AND xxcus_te_accrual_hist_tbl.card_program_name <>
                    'Out of Pocket'
             AND xxcus_te_accrual_hist_tbl.credit_card_trx_id IN
                    (SELECT t.trx_id
                       FROM ap.ap_credit_card_trxns_all t
                      WHERE     t.trx_id =
                                   xxcus_te_accrual_hist_tbl.credit_card_trx_id
                            AND NVL (t.category, 'X') <> 'PERSONAL')
             AND NVL (xxcus_te_accrual_hist_tbl.mcc_code_no, 'X') <> '0'
          OR xxcus_te_accrual_hist_tbl.card_program_name = 'Out of Pocket';


