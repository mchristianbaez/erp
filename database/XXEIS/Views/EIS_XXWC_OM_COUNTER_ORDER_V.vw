CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_counter_order_v
(
   order_number
  ,line_number
  ,reason
  ,inventory_item
  ,item_description
  ,ordered_quantity
  ,order_date
  ,line_status
  ,warehouse
  ,created_by
  ,onhand_location
  ,customer_sold_to
  ,creation_date
  ,header_id
  ,line_id
  ,party_id
  ,mtp_organization_id
  ,inventory_item_id
  ,organization_id
  ,transaction_type_id
  ,line_transaction_type_id
  ,hzp#party_type
  ,hzp#gvid_id
  ,mtp#factory_planner_data_dir
  ,mtp#fru
  ,mtp#location_number
  ,mtp#branch_operations_manage
  ,mtp#deliver_charge
  ,mtp#factory_planner_executab
  ,mtp#factory_planner_user
  ,mtp#factory_planner_host
  ,mtp#factory_planner_port_num
  ,mtp#pricing_zone
  ,mtp#org_type
  ,mtp#district
  ,mtp#region
  ,msi#hds#lob
  ,msi#hds#drop_shipment_eligab
  ,msi#hds#invoice_uom
  ,msi#hds#product_id
  ,msi#hds#vendor_part_number
  ,msi#hds#unspsc_code
  ,msi#hds#upc_primary
  ,msi#hds#sku_description
  ,msi#wc#ca_prop_65
  ,msi#wc#country_of_origin
  ,msi#wc#orm_d_flag
  ,msi#wc#store_velocity
  ,msi#wc#dc_velocity
  ,msi#wc#yearly_store_velocity
  ,msi#wc#yearly_dc_velocity
  ,msi#wc#prism_part_number
  ,msi#wc#hazmat_description
  ,msi#wc#hazmat_container
  ,msi#wc#gtp_indicator
  ,msi#wc#last_lead_time
  ,msi#wc#amu
  ,msi#wc#reserve_stock
  ,msi#wc#taxware_code
  ,msi#wc#average_units
  ,msi#wc#product_code
  ,msi#wc#import_duty_
  ,msi#wc#keep_item_active
  ,msi#wc#pesticide_flag
  ,msi#wc#calc_lead_time
  ,msi#wc#voc_gl
  ,msi#wc#pesticide_flag_state
  ,msi#wc#voc_category
  ,msi#wc#voc_sub_category
  ,msi#wc#msds_#
  ,msi#wc#hazmat_packaging_grou
  ,oh#rental_term
  ,oh#order_channel
  ,oh#picked_by
  ,oh#load_checked_by
  ,oh#delivered_by
  ,ol#estimated_return_date
  ,ol#rerent_po
  ,ol#force_ship
  ,ol#application_method
  ,ol#item_on_blowout
  ,ol#pof_std_line
  ,ol#rerental_billing_terms
  ,ol#pricing_guardrail
  ,ol#print_expired_product_dis
  ,ol#rental_charge
  ,ol#vendor_quote_cost
  ,ol#po_cost_for_vendor_quote
  ,ol#serial_number
  ,ol#engineering_cost
)
AS
   SELECT oh.order_number
         ,DECODE (
             ol.option_number
            ,'', (ol.line_number || '.' || ol.shipment_number)
            , (   ol.line_number
               || '.'
               || ol.shipment_number
               || '.'
               || ol.option_number))
             line_number
         ,NVL (
             xxeis.eis_rs_xxwc_com_util_pkg.get_line_hold (oh.header_id
                                                          ,ol.line_id)
            ,NVL (
                xxeis.eis_rs_xxwc_com_util_pkg.get_header_hold (oh.header_id
                                                               ,ol.line_id)
               ,xxeis.eis_rs_xxwc_com_util_pkg.get_lot_status (
                   msi.inventory_item_id
                  ,msi.organization_id
                  ,ol.line_id)))
             reason
         ,msi.segment1 inventory_item
         ,msi.description item_description
         ,ol.ordered_quantity ordered_quantity
         ,TRUNC (oh.ordered_date) order_date
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_order_line_status (ol.line_id)
             line_status
         ,mtp.organization_code warehouse
         ,NVL (ppf.full_name, fu.user_name) created_by
         ,mtp.organization_code onhand_location
         ,hca.account_name customer_sold_to
         ,TRUNC (ol.creation_date) creation_date
         ,                                                    ---Prrimary Keys
          oh.header_id
         ,ol.line_id
         ,hzp.party_id
         ,mtp.organization_id mtp_organization_id
         ,msi.inventory_item_id
         ,msi.organization_id
         ,header_order_type.transaction_type_id
         ,line_order_type.transaction_type_id line_transaction_type_id
         --descr#flexfield#start

         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_PARTY_TYPE'
                                           ,hzp.attribute1
                                           ,'I')
             hzp#party_type
         ,hzp.attribute2 hzp#gvid_id
         ,mtp.attribute1 mtp#factory_planner_data_dir
         ,mtp.attribute10 mtp#fru
         ,mtp.attribute11 mtp#location_number
         ,xxeis.eis_rs_dff.decode_valueset ('HR_DE_EMPLOYEES'
                                           ,mtp.attribute13
                                           ,'F')
             mtp#branch_operations_manage
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DELIVERY_CHARGE_EXEMPT'
                                           ,mtp.attribute14
                                           ,'I')
             mtp#deliver_charge
         ,mtp.attribute2 mtp#factory_planner_executab
         ,mtp.attribute3 mtp#factory_planner_user
         ,mtp.attribute4 mtp#factory_planner_host
         ,mtp.attribute5 mtp#factory_planner_port_num
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_PRICE_ZONES'
                                           ,mtp.attribute6
                                           ,'F')
             mtp#pricing_zone
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_ORG_TYPE'
                                           ,mtp.attribute7
                                           ,'I')
             mtp#org_type
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DISTRICT'
                                           ,mtp.attribute8
                                           ,'I')
             mtp#district
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_REGION'
                                           ,mtp.attribute9
                                           ,'I')
             mtp#region
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL)
             msi#hds#lob
         ,DECODE (
             msi.attribute_category
            ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                     ,msi.attribute10
                                                     ,'F')
            ,NULL)
             msi#hds#drop_shipment_eligab
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL)
             msi#hds#invoice_uom
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL)
             msi#hds#product_id
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL)
             msi#hds#vendor_part_number
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL)
             msi#hds#unspsc_code
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL)
             msi#hds#upc_primary
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL)
             msi#hds#sku_description
         ,DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL)
             msi#wc#ca_prop_65
         ,DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL)
             msi#wc#country_of_origin
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                    ,msi.attribute11
                                                    ,'F')
            ,NULL)
             msi#wc#orm_d_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL)
             msi#wc#store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL)
             msi#wc#dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL)
             msi#wc#yearly_store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL)
             msi#wc#yearly_dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL)
             msi#wc#prism_part_number
         ,DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL)
             msi#wc#hazmat_description
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER'
                                                    ,msi.attribute18
                                                    ,'I')
            ,NULL)
             msi#wc#hazmat_container
         ,DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL)
             msi#wc#gtp_indicator
         ,DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL)
             msi#wc#last_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL)
             msi#wc#amu
         ,DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL)
             msi#wc#reserve_stock
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE'
                                                    ,msi.attribute22
                                                    ,'I')
            ,NULL)
             msi#wc#taxware_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL)
             msi#wc#average_units
         ,DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL)
             msi#wc#product_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL)
             msi#wc#import_duty_
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                    ,msi.attribute29
                                                    ,'F')
            ,NULL)
             msi#wc#keep_item_active
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                    ,msi.attribute3
                                                    ,'F')
            ,NULL)
             msi#wc#pesticide_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL)
             msi#wc#calc_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL)
             msi#wc#voc_gl
         ,DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL)
             msi#wc#pesticide_flag_state
         ,DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL)
             msi#wc#voc_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL)
             msi#wc#voc_sub_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL)
             msi#wc#msds_#
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset (
                      'XXWC_HAZMAT_PACKAGE_GROUP'
                     ,msi.attribute9
                     ,'I')
            ,NULL)
             msi#wc#hazmat_packaging_grou
         ,xxeis.eis_rs_dff.decode_valueset ('RENTAL TYPE'
                                           ,oh.attribute1
                                           ,'I')
             oh#rental_term
         ,xxeis.eis_rs_dff.decode_valueset ('WC_CUS_ORDER_CHANNEL'
                                           ,oh.attribute2
                                           ,'I')
             oh#order_channel
         ,oh.attribute3 oh#picked_by
         ,oh.attribute4 oh#load_checked_by
         ,oh.attribute5 oh#delivered_by
         ,ol.attribute1 ol#estimated_return_date
         ,ol.attribute10 ol#rerent_po
         ,ol.attribute11 ol#force_ship
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_CSP_APP_METHOD'
                                           ,ol.attribute13
                                           ,'I')
             ol#application_method
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ol.attribute15, 'F')
             ol#item_on_blowout
         ,ol.attribute19 ol#pof_std_line
         ,xxeis.eis_rs_dff.decode_valueset ('ReRental Billing Type'
                                           ,ol.attribute2
                                           ,'I')
             ol#rerental_billing_terms
         ,ol.attribute20 ol#pricing_guardrail
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ol.attribute3, 'F')
             ol#print_expired_product_dis
         ,ol.attribute4 ol#rental_charge
         ,ol.attribute5 ol#vendor_quote_cost
         ,ol.attribute6 ol#po_cost_for_vendor_quote
         ,ol.attribute7 ol#serial_number
         ,ol.attribute8 ol#engineering_cost
     --descr#flexfield#end


     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,hz_cust_accounts hca
         ,hz_parties hzp
         ,mtl_parameters mtp
         ,mtl_system_items_kfv msi
         ,oe_transaction_types_vl header_order_type
         ,oe_transaction_types_vl line_order_type
         ,per_people_f ppf
         ,fnd_user fu
         ,fnd_lookup_values_vl flv
    WHERE     ol.header_id = oh.header_id
          AND oh.sold_to_org_id = hca.cust_account_id(+)
          AND hca.party_id = hzp.party_id(+)
          AND ol.ship_from_org_id = mtp.organization_id(+)
          AND msi.organization_id = ol.ship_from_org_id
          AND msi.inventory_item_id = ol.inventory_item_id
          AND oh.order_type_id = header_order_type.transaction_type_id
          --AND OH.CREATED_BY         = PPF.PERSON_ID
          AND fu.user_id = oh.created_by
          AND fu.employee_id = ppf.person_id(+)
          AND TRUNC (oh.creation_date) BETWEEN NVL (ppf.effective_start_date
                                                   ,TRUNC (oh.creation_date))
                                           AND NVL (ppf.effective_end_date
                                                   ,TRUNC (oh.creation_date))
          AND UPPER (header_order_type.name) = 'COUNTER ORDER'
          AND header_order_type.org_id = oh.org_id
          AND ol.line_type_id = line_order_type.transaction_type_id
          AND UPPER (line_order_type.name) = 'COUNTER LINE'
          AND line_order_type.org_id = ol.org_id
          AND ol.flow_status_code NOT IN ('CLOSED', 'ENTERED', 'CANCELLED')
          AND flv.lookup_type(+) = 'LINE_FLOW_STATUS'
          AND flv.lookup_code(+) = ol.flow_status_code;


