---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_BIN_LOC_SALES_V $
  Module Name : Inventory
  PURPOSE	  : Bin Location With Sales History
  TMS Task Id : 20160301-00067 , 20160601-00139 
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     16-May-2016        Siva   		 TMS#20160301-00067,TMS#20160601-00139	   Performance Tuning
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_INV_BIN_LOC_SALES_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_BIN_LOC_SALES_V (PROCESS_ID, ORGANIZATION, PART_NUMBER, DESCRIPTION, UOM, SELLING_PRICE, VENDOR_NAME, VENDOR_NUMBER, MIN_MINMAX_QUANTITY, MAX_MINMAX_QUANTITY, AVERAGECOST, WEIGHT, CAT, ONHAND, MTD_SALES, YTD_SALES, PRIMARY_BIN_LOC, ALTERNATE_BIN_LOC, BIN_LOC, LOCATION, STK, OPEN_ORDER, OPEN_DEMAND, CATEGORY, CATEGORY_SET_NAME, INVENTORY_ITEM_ID, INV_ORGANIZATION_ID, ORG_ORGANIZATION_ID, APPLICATION_ID, SET_OF_BOOKS_ID, LAST_RECEIVED_DATE)
AS
  SELECT PROCESS_ID,
    ORGANIZATION,
    PART_NUMBER,
    DESCRIPTION,
    UOM,
    SELLING_PRICE,
    VENDOR_NAME,
    VENDOR_NUMBER,
    MIN_MINMAX_QUANTITY,
    MAX_MINMAX_QUANTITY,
    AVERAGECOST,
    WEIGHT,
    CAT,
    ONHAND,
    MTD_SALES,
    YTD_SALES,
    PRIMARY_BIN_LOC,
    ALTERNATE_BIN_LOC,
    BIN_LOC,
    LOCATION,
    STK,
    OPEN_ORDER,
    OPEN_DEMAND,
    CATEGORY,
    CATEGORY_SET_NAME,
    INVENTORY_ITEM_ID,
    INV_ORGANIZATION_ID,
    ORG_ORGANIZATION_ID,
    APPLICATION_ID,
    SET_OF_BOOKS_ID,
    LAST_RECEIVED_DATE
  FROM XXEIS.EIS_XXWC_INV_BIN_LOC_SALES_TBL
/
