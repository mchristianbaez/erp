------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_OM_DEFERRED_RENTAL_V
   PURPOSE    :  
   REVISIONS:
	Ver        		Date        	Author             Description
   ---------  	-------------  	---------------  ------------------------------------
    1.0        	 27-Feb-2017    	Siva		       TMS#20160829-00183  -- Initial Version
******************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_OM_DEFERRED_RENTAL_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_DEFERRED_RENTAL_V (ORDER_NUMBER, EST_RENTAL_RETURN_DATE, CREATED_BY, LINE_NUMBER, ORDER_TYPE, CUSTOMER_NAME, CUSTOMER_JOB_NAME, SALES_PERSON_NAME, ITEM_NUMBER, ITEM_DESCRIPTION, QUANTITY_ON_RENT, RENTAL_START_DATE, RENTAL_LOCATION,
 DAYS_ON_RENT, ATTRIBUTE12, RENTAL_DATE, PRI_BILL_TO_PH, BILL_TO_CUST_NAME, BILL_TO_CUST_PH_NO, RENTAL_BILLING_TERMS, RENTAL_RATE, INVOICE_NUMBER, INVOICE_DATE, DEFERRAL_DAYS, OL_LINE_ID, OLL_LINE_ID, OH_HEADER_ID, HZP_PARTY_ID, MSI_INVENTORY_ITEM_ID, MSI_ORGANIZATION_ID, ORGANIZATION_ID)
AS
   SELECT oh.order_number,
    DECODE ( SUBSTR (ol1.attribute1, 5, 1), '/', TO_CHAR ( to_date (SUBSTR (ol1.attribute1, 1, 10), 'yyyy/mm/dd'), 'DD-MON-YYYY'), ol1.attribute1) est_rental_return_date,
    ppf.full_name created_by,
    DECODE ( ol.option_number, '', (ol.line_number
    || '.'
    || ol.shipment_number), ( ol.line_number
    || '.'
    || ol.shipment_number
    || '.'
    || ol.option_number)) line_number,
    ott.name order_type,
    hzp.party_name customer_name,
    hzcs_ship_to.location customer_job_name,
    rep.name sales_person_name,
    msi.segment1 item_number,
    msi.description item_description,
    ol.ordered_quantity quantity_on_rent,
    TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_rental_start_date ( ol.link_to_line_id)) rental_start_date,
    mtp.organization_code rental_location,
    ( TRUNC (sysdate) - TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_rental_start_date ( ol.link_to_line_id)-1)) days_on_rent,
    ol1.attribute12,
    TRUNC(to_date(TO_CHAR(to_date(SUBSTR (ol1.attribute12, 1, 10), 'yyyy/mm/dd'),xxeis.eis_rsc_utility.get_date_format),xxeis.eis_rsc_utility.get_date_format)) rental_date,
    (SELECT raw_phone_number
    FROM apps.hz_contact_points
    WHERE 1                = 1
    AND owner_table_id     = hzp.party_id
    AND owner_table_name   = 'HZ_PARTIES'
    AND contact_point_type = 'PHONE'
    AND primary_flag       = 'Y'
    AND rownum             = 1
    ) pri_bill_to_ph,
    btc.party_name bill_to_cust_name,
    btc.phone_number bill_to_cust_ph_no ,
    ol1.attribute2 rental_billing_terms,
    NVL(ol1.attribute4, ol1.unit_selling_price) rental_rate,
    (SELECT rct.trx_number
    FROM apps.ra_customer_trx_all rct ,
      apps.ra_customer_trx_lines_all rctl
    WHERE rct.customer_trx_id     = rctl.customer_trx_id
    AND interface_line_attribute6 =
      (SELECT TO_CHAR(MAX(line_id))
      FROM apps.oe_order_lines_all ool
      WHERE 1              =1
      AND link_to_line_id  = ol.line_id
      AND ool.line_type_id = 1003
      AND ool.header_id    = oh.header_id
      AND flow_status_code = 'CLOSED'
      )
    AND rownum =1
    ) invoice_number,
    (SELECT rct.trx_date
    FROM apps.ra_customer_trx_all rct ,
      apps.ra_customer_trx_lines_all rctl
    WHERE rct.customer_trx_id     = rctl.customer_trx_id
    AND interface_line_attribute6 =
      (SELECT TO_CHAR(MAX(line_id))
      FROM apps.oe_order_lines_all ool
      WHERE 1              =1
      AND link_to_line_id  = ol.line_id
      AND ool.line_type_id = 1003
      AND ool.header_id    = oh.header_id
      AND flow_status_code = 'CLOSED'
      )
    AND rownum =1
    ) invoice_date,
    28-( TRUNC (sysdate) - TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_rental_start_date ( ol.link_to_line_id)-1))  deferral_days,
    --primary keys
    ol.line_id ol_line_id,
    ol1.line_id oll_line_id,
    oh.header_id oh_header_id,
    hzp.party_id hzp_party_id,
    msi.inventory_item_id msi_inventory_item_id,
    msi.organization_id msi_organization_id,
    mtp.organization_id
    --descr#flexfield#start
    --descr#flexfield#end
  FROM apps.oe_order_lines_all ol,
    apps.oe_order_lines_all ol1,
    apps.oe_order_headers_all oh,
    apps.oe_transaction_types_vl ott,
    apps.ra_salesreps rep,
    apps.hz_cust_accounts hca,
    apps.hz_parties hzp,
    apps.fnd_user fu,
    apps.per_people_f ppf,
    apps.mtl_system_items_kfv msi,
    apps.mtl_parameters mtp,
    apps.hz_cust_site_uses hzcs_ship_to,
    apps.hz_cust_acct_sites hcas_ship_to,
    apps.hz_party_sites hzps_ship_to,
    apps.hz_locations hzl_ship_to,
    (SELECT party.party_name party_name,
      DECODE ( hcp.phone_number, NULL, NULL, '('
      || hcp.phone_area_code
      || ') '
      || hcp.phone_number
      || ' EX '
      || NVL (hcp.phone_extension, '-')) phone_number,
      acct_role.cust_account_role_id cust_account_role_id
    FROM apps.hz_cust_account_roles acct_role,
      apps.hz_parties party,
      apps.hz_relationships rel,
      apps.hz_org_contacts org_cont,
      apps.hz_contact_points hcp
    WHERE 1                            = 1
    AND acct_role.party_id             = rel.party_id
    AND acct_role.role_type            = 'CONTACT'
    AND org_cont.party_relationship_id = rel.relationship_id
    AND rel.subject_table_name         = 'HZ_PARTIES'
    AND rel.object_table_name          = 'HZ_PARTIES'
    AND rel.directional_flag           = 'F'
    AND rel.subject_id                 = party.party_id
    AND hcp.owner_table_id(+)          = acct_role.party_id
    AND hcp.owner_table_name(+)        = 'HZ_PARTIES'
    AND hcp.contact_point_type(+)      = 'PHONE'
    ) btc
  WHERE ol.flow_status_code = 'AWAITING_RETURN'
  AND ol.link_to_line_id    = ol1.line_id
  AND ol.header_id          = oh.header_id
  AND oh.order_type_id      = ott.transaction_type_id
  AND oh.org_id             = ott.org_id
  AND oh.salesrep_id        = rep.salesrep_id(+)
  AND oh.org_id             = rep.org_id(+)
  AND oh.sold_to_org_id     = hca.cust_account_id(+)
  AND hca.party_id          = hzp.party_id(+)
  AND oh.created_by         = fu.user_id
  AND fu.employee_id        = ppf.person_id(+)
  AND TRUNC (sysdate) BETWEEN ppf.effective_start_date(+) AND ppf.effective_end_date(+)
  AND ol.ship_from_org_id                                                                                                                                          = msi.organization_id
  AND ol.inventory_item_id                                                                                                                                         = msi.inventory_item_id
  AND ol.ship_from_org_id                                                                                                                                          = mtp.organization_id(+)
  AND oh.ship_to_org_id                                                                                                                                            = hzcs_ship_to.site_use_id(+)
  AND hzcs_ship_to.cust_acct_site_id                                                                                                                               = hcas_ship_to.cust_acct_site_id(+)
  AND hcas_ship_to.party_site_id                                                                                                                                   = hzps_ship_to.party_site_id(+)
  AND hzps_ship_to.location_id                                                                                                                                     = hzl_ship_to.location_id(+)
  AND ott.name                                                                                                                                                    IN ('WC LONG TERM RENTAL', 'WC SHORT TERM RENTAL')
  AND oh.invoice_to_contact_id                                                                                                                                     = btc.cust_account_role_id(+)
  AND TRUNC(to_date(TO_CHAR(to_date(SUBSTR (ol1.attribute12, 1, 10), 'yyyy/mm/dd'),xxeis.eis_rsc_utility.get_date_format),xxeis.eis_rsc_utility.get_date_format)) >= (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from -28)
  AND TRUNC(to_date(TO_CHAR(to_date(SUBSTR (ol1.attribute12, 1, 10), 'yyyy/mm/dd'),xxeis.eis_rsc_utility.get_date_format),xxeis.eis_rsc_utility.get_date_format)) <= xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
/
