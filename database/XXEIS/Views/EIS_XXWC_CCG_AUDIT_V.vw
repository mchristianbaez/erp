-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.EIS_XXWC_CCG_AUDIT_V.vw $
  Module Name: EiS eXpress Administrator
  PURPOSE: CCG EBS Audit Report - WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 5/28/2018      Siva			   TMS#20180504-00247 
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_CCG_AUDIT_V (CHANGE_OBJECT_ID, TARGET_NAME, OBJECT_NAME, OWNING_APPLICATION_NAME, TABLE_NAME, TABLE_DESC, AUDIT_DATE, AUDIT_TYPE, APP_USER_NAME, RESPONSIBILITY_NAME, DB_USER, OSUSER, OBJECT_PRIMARY_KEY, OBJECT_PRIMARY_DISPLAY, PRIMARY_KEY_VALUE, PRIMARY_KEY_DISPLAY, COLUMN_DESC, OLD_DISPLAY, NEW_DISPLAY, CHANGED_FLAG, PRIMARY_KEY_DESCRIPTION, OBJ_PK_DESCRIPTION)
AS
  SELECT  /*+ USE_HASH(AMT,MA) ORDERED */
    ao.CHANGE_OBJECT_ID ,
    amt.target_name ,
    o.target_object_name object_name ,
    ma.application_description owning_application_name ,
    t.target_table_name table_name ,
    T.TARGET_TABLE_DESCRIPTION TABLE_DESC ,
    TRUNC(ao.change_date) audit_date ,
    DECODE(at.CHANGE_TYPE ,'I' ,'Insert' ,'U' ,'Update' ,'D' ,'Delete' ,' ') audit_type ,
    ao.ATTRIBUTE1 app_user_name ,
    ao.ATTRIBUTE2 responsibility_name ,
    ao.db_user ,
    ao.os_user osuser ,
    ao.pk_value OBJECT_PRIMARY_KEY ,
    REPLACE(ao.pk_disp ,'|' ,',') OBJECT_PRIMARY_DISPLAY ,
    at.pk_value PRIMARY_KEY_VALUE ,
    REPLACE(at.pk_disp ,'|' ,',') PRIMARY_KEY_DISPLAY ,
    c.target_column_description column_desc ,
    ac.old_display ,
    ac.new_display ,
    ac.changed_flag ,
    t.primary_key_description ,
    o.target_pk_desc OBJ_PK_DESCRIPTION
  FROM TICK_CHG_TRACK_OBJECTS@EBS2CCG AO ,
    TICK_CHG_TRACK_TABLES@EBS2CCG AT ,
    TICK_CHG_TRACK_COLUMNS@EBS2CCG AC ,
    TICK_TARGET_OBJECTS@EBS2CCG O ,
    TICK_TARGET_TABLES@EBS2CCG T ,
    TICK_TARGET_OBJECTS_TABLES@EBS2CCG OT ,
    TICK_TARGET_COLUMNS@EBS2CCG C ,
    TICK_TARGET_APPLICATIONS@EBS2CCG MA ,
    am_targets@ebs2ccg amt
  WHERE ao.change_object_id   = at.change_object_id
  AND at.change_table_id      = ac.change_table_id
  AND ao.target_object_id     = o.target_object_id
  AND t.target_table_id       = at.target_table_id
  AND ac.target_column_id     = c.target_column_id
  AND t.target_table_id       = c.target_table_id
  AND o.target_application_id = ma.application_id
  AND ot.target_object_id     = ao.target_object_id
  AND ot.target_object_id     = o.target_object_id
  AND ot.target_table_id      = t.target_table_id
  AND amt.target_id           = ao.target_instance_id
  AND ac.changed_flag         = 'Y'
  AND O.TARGET_OBJECT_NAME    = 'Party Tax Profile-Legal Entity'
  AND T.TARGET_TABLE_NAME    IN ('AP_BANK_ACCOUNT_USES_ALL', 'AP_SUPPLIERS', 'AP_SUPPLIER_SITES_ALL', 'IBY_EXT_BANK_ACCOUNTS')
/
  