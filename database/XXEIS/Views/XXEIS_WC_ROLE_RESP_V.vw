CREATE OR REPLACE FORCE VIEW XXEIS.XXEIS_WC_ROLE_RESP_V
(
   USER_NAME
  ,DESCRIPTION
  ,ROLE_CODE
  ,RESPONSIBILITY_NAME
  ,EMAIL_ADDRESS
  ,RESPONSIBILITY_KEY
  ,SOURCE
  ,START_DATE
  ,END_DATE
  ,CREATED_BY
  ,UPDATED_BY
)
AS
     SELECT DISTINCT fu.user_name
                    ,role_names.display_name description
                    ,role_names.name role_code
                    ,frv.responsibility_name
                    ,fu.email_address
                    ,frv.RESPONSIBILITY_KEY
                    ,furga.source
                    ,furga.start_date
                    ,furga.end_date
                    ,fu2.description Created_By
                    ,fu3.description Updated_By
       FROM apps.fnd_user fu
           ,(SELECT user_id
                   ,responsibility_id
                   ,start_date
                   ,end_date
                   ,created_by
                   ,last_updated_by
                   ,'direct' source
               FROM apps.fnd_user_resp_groups_direct
             UNION
             SELECT user_id
                   ,responsibility_id
                   ,start_date
                   ,end_date
                   ,created_by
                   ,last_updated_by
                   ,'indirect' source
               FROM apps.fnd_user_resp_groups_indirect) furga
           ,apps.fnd_responsibility_vl frv
           ,apps.fnd_user fu2
           ,apps.fnd_user fu3
           ,(  SELECT REPLACE (name, 'UMX|', '') AS Name, display_name
                 FROM wf_local_roles lr
                WHERE lr.name LIKE 'UMX|XXWC_ROLE%' AND lr.status = 'ACTIVE'
             ORDER BY display_name ASC) role_names
      WHERE     fu.user_id = furga.user_id
            AND fu2.user_id = furga.created_by
            AND furga.responsibility_id = frv.responsibility_id
            AND furga.start_date <= SYSDATE
            AND NVL (furga.end_date, SYSDATE + 1) > SYSDATE
            AND frv.start_date <= SYSDATE
            AND NVL (frv.end_date, SYSDATE + 1) > SYSDATE
            AND fu.user_name LIKE 'XXWC_REF%'
            AND frv.RESPONSIBILITY_KEY <> 'HDS_INTERNET_EXPENSES_USER'
            AND REPLACE (fu.user_name, '_REF_', '_ROLE_') = role_names.name
            AND fu3.user_id = furga.last_updated_by
   ORDER BY role_names.display_name ASC, frv.responsibility_name ASC;


