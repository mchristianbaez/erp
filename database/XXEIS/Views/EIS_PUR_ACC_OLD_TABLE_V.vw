---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_INT_OPEN_ORDERS_V $
  PURPOSE	  : PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
  TMS Task Id : 20160412-00031 
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- 	-----------------  --------------- -----------------------------------------
  1.0     	12-Apr-2016        		Pramod   		 TMS#20160412-00031   Performance Tuning
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_OM_INT_OPEN_ORDERS_V;

CREATE OR REPLACE VIEW XXEIS.EIS_PUR_ACC_OLD_TABLE_V (PROCESS_ID, AGREEMENT_YEAR, PERIOD_ID, FISCAL_PERIOD, MVID, CUST_ID, VENDOR, LOB, BU, LOB_BRANCH, POSTED_FIN_LOCATION, FISCAL_PURCHASES, ACCRUAL_PURCHASES, REBATE, COOP, TOTAL) AS 
  SELECT PROCESS_ID, 
    AGREEMENT_YEAR ,
    PERIOD_ID ,
    FISCAL_PERIOD ,
    MVID ,
    cust_id,
    VENDOR ,
    LOB ,
    BU ,
    LOB_BRANCH ,
    POSTED_FIN_LOCATION,
    SUM(FISCAL_PURCHASES)FISCAL_PURCHASES ,
    MAX(ACCRUAL_PURCHASES) ACCRUAL_PURCHASES ,
    SUM(REBATE) REBATE ,
    SUM(COOP) COOP ,
    SUM(TOTAL) TOTAL
  FROM
    (SELECT PROCESS_ID,
      AGREEMENT_YEAR ,
      PERIOD_ID ,
      FISCAL_PERIOD ,
      MVID ,
      cust_id,
      VENDOR ,
      LOB ,
      BU ,
      LOB_BRANCH ,
      POSTED_FIN_LOCATION,
      FISCAL_PURCHASES ,
      ACCRUAL_PURCHASES,
      REBATE,
      COOP,
      TOTAL
    FROM XXEIS.EIS_PUR_ACC_RPT_DATA_TAB
    )
  GROUP BY PROCESS_ID,
    AGREEMENT_YEAR ,
    PERIOD_ID ,
    FISCAL_PERIOD ,
    MVID ,
    cust_id,
    VENDOR ,
    LOB ,
    BU ,
	LOB_BRANCH,
    posted_fin_location
/
