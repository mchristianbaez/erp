CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_ar_aging_summary_v
(
   bill_to_customer_name
  ,bill_to_customer_number
  ,outstanding_amount
  ,bucket_current
  ,bucket_1_to_30
  ,bucket_31_to_60
  ,bucket_61_to_90
  ,bucket_91_to_180
  ,bucket_181_to_360
  ,bucket_361_days_and_above
  ,profile_class
  ,credit_hold
  ,customer_account_status
  ,collector
  ,credit_analyst_name
  ,salesrep_number
  ,account_manager
)
AS
     SELECT customer_name bill_to_customer_name
           ,customer_account_number bill_to_customer_number
           ,SUM (transaction_remaining_balance) outstanding_amount
           ,SUM (current_balance) bucket_current
           ,SUM (thirty_days_bal) bucket_1_to_30
           ,SUM (sixty_days_bal) bucket_31_to_60
           ,SUM (ninety_days_bal) bucket_61_to_90
           ,SUM (one_eighty_days_bal) bucket_91_to_180
           ,SUM (three_sixty_days_bal) bucket_181_to_360
           ,SUM (over_three_sixty_days_bal) bucket_361_days_and_above
           ,customer_profile_class profile_class
           ,account_credit_hold credit_hold
           ,customer_account_status
           ,collector_name collector
           ,credit_analyst credit_analyst_name
           ,salesrep_number salesrep_number
           ,account_manager account_manager
       FROM xxeis.xxwc_ar_customer_balance_mv
   GROUP BY customer_name
           ,customer_account_number
           ,customer_profile_class
           ,account_credit_hold
           ,customer_account_status
           ,collector_name
           ,credit_analyst
           ,salesrep_number
           ,account_manager;


