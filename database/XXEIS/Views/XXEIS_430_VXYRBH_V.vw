CREATE OR REPLACE FORCE VIEW xxeis.xxeis_430_vxyrbh_v
(
   status
  ,currency_code
  ,je_source
  ,je_category
  ,priod_name
  ,batch_name
  ,description
  ,je_line_num
  ,product
  ,location
  ,cost_center
  ,account_number
  ,project
  ,future_use
  ,gl_debit
  ,gl_credit
  ,je_line_desc
  ,branch
  ,inv_pay_date
  ,invoice_num
  ,check_num
  ,inv_check_gl_date
  ,vendor_num
  ,vendor_name
  ,tax_code
  ,tax_code_desc
  ,amount
  ,ael_acct_dr
  ,ael_acct_cr
  ,pos_invoice_type
  ,pos_invoice_id
  ,pos_system_code
  ,inv_check_type
  ,line_type
  ,bank_account
  ,ae_line_id
  ,applied_invoice
  ,image_link
)
AS
   SELECT l.status
         ,h.currency_code currency_code
         ,h.je_source je_source
         ,h.je_category je_category
         ,h.period_name priod_name
         ,b.name batch_name
         ,h.description description
         ,l.je_line_num je_line_num
         ,cc.segment1 product
         ,cc.segment2 location
         ,cc.segment3 cost_center
         ,cc.segment4 account_number
         ,cc.segment5 project
         ,cc.segment6 future_use
         ,l.accounted_dr gl_debit
         ,l.accounted_cr gl_credit
         ,l.description je_line_desc
         ,inv.attribute2 branch
         ,inv.invoice_date inv_pay_date
         ,inv.invoice_num invoice_num
         ,NULL check_num
         ,inv.gl_date inv_check_gl_date
         ,v.segment1 vendor_num
         ,v.vendor_name vendor_name
         ,t.name tax_code
         ,t.tax_type || ' - ' || t.tax_rate tax_code_desc
         ,dist.amount amount
         ,ael.accounted_dr ael_acct_dr
         ,ael.accounted_cr ael_acct_cr
         ,inv.attribute11 pos_invoice_type
         ,inv.attribute12 pos_invoice_id
         ,inv.attribute8 pos_system_code
         ,inv.invoice_type_lookup_code inv_check_type
         ,dist.line_type_lookup_code line_type
         ,NULL bank_account
         ,ael.ae_line_id ae_line_id
         ,NULL applied_invoice
         ,SUBSTR (doc.file_name, 8, 100) image_link
     FROM gl.gl_je_batches b
         ,gl.gl_je_headers h
         ,gl.gl_code_combinations cc
         ,gl.gl_je_lines l
         ,ap.ap_invoices_all inv
         ,ap.ap_invoice_distributions_all dist
         ,ap.ap_ae_lines_all ael
         ,ap.ap_suppliers v
         ,ap.ap_supplier_sites_all s
         ,ap.ap_tax_codes_all t
         ,apps.fnd_attached_documents attach
         ,apps.fnd_documents_tl doc
    WHERE     b.je_batch_id = h.je_batch_id
          AND h.je_header_id = l.je_header_id
          AND h.je_category = 'Purchase Invoices'
          AND h.je_source = 'Payables'
          AND l.ledger_id = 2042
          AND l.code_combination_id = cc.code_combination_id
          AND cc.chart_of_accounts_id = 50328
          AND l.gl_sl_link_id = ael.gl_sl_link_id
          AND inv.invoice_id = dist.invoice_id
          AND dist.invoice_distribution_id = ael.source_id
          AND ael.source_table = 'AP_INVOICE_DISTRIBUTIONS'
          AND t.tax_id(+) = ael.tax_code_id
          AND inv.vendor_id = v.vendor_id
          AND inv.vendor_site_id = s.vendor_site_id
          AND inv.invoice_id = attach.pk1_value(+)
          AND (    attach.entity_name(+) = 'AP_INVOICES'
               AND attach.last_update_login(+) = -1)         /* handle dups */
          AND (    attach.document_id = doc.document_id(+)
               AND doc.description(+) = 'Image Link')
   UNION ALL
   SELECT l.status
         ,h.currency_code
         ,h.je_source
         ,h.je_category
         ,h.period_name
         ,b.name batch_name
         ,h.description
         ,l.je_line_num
         ,cc.segment1 product
         ,cc.segment2 location
         ,cc.segment3 cost_center
         ,cc.segment4 account_number
         ,cc.segment5 project
         ,cc.segment6 future_use
         ,l.accounted_dr gl_debit
         ,l.accounted_cr gl_credit
         ,l.description je_line_desc
         ,NULL branch
         ,ch.check_date inv_pay_date
         ,NULL invoice_num
         ,ch.check_number
         ,aeh.accounting_date inv_check_gl_date
         ,v.segment1 vendor_num
         ,v.vendor_name
         ,t.name tax_code
         ,t.tax_type || ' - ' || t.tax_rate tax_code_desc
         ,ch.amount
         ,ael.accounted_dr ael_acct_dr
         ,ael.accounted_cr ael_acct_cr
         ,NULL pos_invoice_type
         ,NULL pos_invoice_id
         ,NULL pos_system_code
         ,ch.payment_method_lookup_code inv_check_type
         ,NULL line_type
         ,ch.bank_account_name
         ,ael.ae_line_id
         ,NULL applied_invoice
         ,NULL image_link
     FROM gl.gl_je_batches b
         ,gl.gl_je_headers h
         ,gl.gl_code_combinations cc
         ,gl.gl_je_lines l
         ,ap.ap_checks_all ch
         ,ap.ap_ae_lines_all ael
         ,ap.ap_ae_headers_all aeh
         ,ap.ap_suppliers v
         ,ap.ap_tax_codes_all t
    WHERE     b.je_batch_id = h.je_batch_id
          AND h.je_category = 'Payments'
          AND h.je_source = 'Payables'
          AND h.je_header_id = l.je_header_id
          AND l.ledger_id = 2042
          AND l.code_combination_id = cc.code_combination_id
          AND cc.chart_of_accounts_id = 50328
          AND l.gl_sl_link_id = ael.gl_sl_link_id
          AND ael.source_table = 'AP_CHECKS'
          AND ael.source_id = ch.check_id
          AND ael.ae_header_id = aeh.ae_header_id
          AND t.tax_id(+) = ael.tax_code_id
          AND ch.vendor_id = v.vendor_id
   UNION ALL
   SELECT l.status
         ,h.currency_code
         ,h.je_source
         ,h.je_category
         ,h.period_name
         ,b.name batch_name
         ,h.description
         ,l.je_line_num
         ,cc.segment1 product
         ,cc.segment2 location
         ,cc.segment3 cost_center
         ,cc.segment4 account_number
         ,cc.segment5 project
         ,cc.segment6 future_use
         ,l.accounted_dr gl_debit
         ,l.accounted_cr gl_credit
         ,l.description je_line_desc
         ,NULL branch
         ,ch.check_date inv_pay_date
         ,NULL invoice_num
         ,ch.check_number
         ,pay.accounting_date inv_check_gl_date
         ,v.segment1 vendor_num
         ,v.vendor_name
         ,t.name tax_code
         ,t.tax_type || ' - ' || t.tax_rate tax_code_desc
         ,pay.amount
         ,ael.accounted_dr ael_acct_dr
         ,ael.accounted_cr ael_acct_cr
         ,NULL pos_invoice_type
         ,NULL pos_invoice_id
         ,NULL pos_system_code
         ,ch.payment_method_lookup_code inv_check_type
         ,NULL line_type
         ,ch.bank_account_name
         ,ael.ae_line_id
         ,inv.invoice_num applied_invoice
         ,NULL image_link
     FROM gl.gl_je_batches b
         ,gl.gl_je_headers h
         ,gl.gl_code_combinations cc
         ,gl.gl_je_lines l
         ,ap.ap_invoice_payments_all pay
         ,ap.ap_checks_all ch
         ,ap.ap_ae_lines_all ael
         ,ap.ap_suppliers v
         ,ap.ap_tax_codes_all t
         ,ap.ap_invoices_all inv
    WHERE     b.je_batch_id = h.je_batch_id
          AND h.je_header_id = l.je_header_id
          AND h.je_category = 'Payments'
          AND h.je_source = 'Payables'
          AND cc.code_combination_id = l.code_combination_id
          AND cc.chart_of_accounts_id = 50328
          AND l.ledger_id = 2042
          AND l.gl_sl_link_id = ael.gl_sl_link_id
          AND pay.invoice_payment_id = ael.source_id
          AND ael.source_table = 'AP_INVOICE_PAYMENTS'
          AND t.tax_id(+) = ael.tax_code_id
          AND pay.check_id = ch.check_id
          AND ch.vendor_id = v.vendor_id
          AND pay.invoice_id = inv.invoice_id
   UNION ALL
   SELECT l.status
         ,h.currency_code
         ,h.je_source
         ,h.je_category
         ,h.period_name
         ,b.name batch_name
         ,h.description
         ,l.je_line_num
         ,cc.segment1 product
         ,cc.segment2 location
         ,cc.segment3 cost_center
         ,cc.segment4 account_number
         ,cc.segment5 project
         ,cc.segment6 future_use
         ,l.accounted_dr gl_debit
         ,l.accounted_cr gl_credit
         ,l.description je_line_desc
         ,inv.attribute2 branch
         ,inv.invoice_date inv_pay_date
         ,inv.invoice_num
         ,NULL check_num
         ,inv.gl_date inv_check_gl_date
         ,v.segment1 vendor_num
         ,v.vendor_name
         ,t.name tax_code
         ,t.tax_type || ' - ' || t.tax_rate tax_code_desc
         ,inv.invoice_amount
         ,ael.accounted_dr ael_acct_dr
         ,ael.accounted_cr ael_acct_cr
         ,inv.attribute11 pos_invoice_type
         ,inv.attribute12 pos_invoice_id
         ,inv.attribute8 pos_system_code
         ,inv.invoice_type_lookup_code inv_check_type
         ,NULL line_type
         ,NULL bank_account
         ,ael.ae_line_id
         ,NULL applied_invoice
         ,SUBSTR (doc.file_name, 8, 100) image_link
     FROM gl.gl_je_batches b
         ,gl.gl_je_headers h
         ,gl.gl_code_combinations cc
         ,gl.gl_je_lines l
         ,ap.ap_invoices_all inv
         ,ap.ap_ae_lines_all ael
         ,ap.ap_suppliers v
         ,ap.ap_supplier_sites_all s
         ,ap.ap_tax_codes_all t
         ,apps.fnd_attached_documents attach
         ,apps.fnd_documents_tl doc
    WHERE     b.je_batch_id = h.je_batch_id
          AND h.je_header_id = l.je_header_id
          AND h.je_category = 'Purchase Invoices'
          AND h.je_source = 'Payables'
          AND l.ledger_id = 2042
          AND l.code_combination_id = cc.code_combination_id
          AND cc.chart_of_accounts_id = 50328
          AND l.gl_sl_link_id = ael.gl_sl_link_id
          AND inv.invoice_id = ael.source_id
          AND ael.source_table = 'AP_INVOICES'
          AND t.tax_id(+) = ael.tax_code_id
          AND inv.vendor_id = v.vendor_id
          AND inv.vendor_site_id = s.vendor_site_id
          AND inv.invoice_id = attach.pk1_value(+)
          AND (    attach.entity_name(+) = 'AP_INVOICES'
               AND attach.last_update_login(+) = -1)         /* handle dups */
          AND (    attach.document_id = doc.document_id(+)
               AND doc.description(+) = 'Image Link')
   UNION ALL
   SELECT l.status
         ,h.currency_code
         ,h.je_source
         ,h.je_category
         ,h.period_name
         ,b.name batch_name
         ,h.description
         ,l.je_line_num
         ,cc.segment1 product
         ,cc.segment2 location
         ,cc.segment3 cost_center
         ,cc.segment4 account_number
         ,cc.segment5 project
         ,cc.segment6 future_use
         ,l.accounted_dr gl_debit
         ,l.accounted_cr gl_credit
         ,l.description je_line_desc
         ,NULL branch
         ,NULL inv_pay_date
         ,NULL invoice_num
         ,NULL check_num
         ,NULL inv_check_gl_date
         ,NULL vendor_num
         ,NULL vendor_name
         ,NULL tax_code
         ,NULL tax_code_desc
         ,NULL amount
         ,NULL ael_acct_dr
         ,NULL ael_acct_cr
         ,NULL pos_invoice_type
         ,NULL pos_invoice_id
         ,NULL pos_system_code
         ,NULL inv_check_type
         ,NULL line_type
         ,NULL bank_account
         ,NULL ae_line_id
         ,NULL applied_invoice
         ,NULL image_link
     FROM gl.gl_je_batches b
         ,gl.gl_je_headers h
         ,gl.gl_code_combinations cc
         ,gl.gl_je_lines l
    WHERE     b.je_batch_id = h.je_batch_id
          AND h.je_header_id = l.je_header_id
          AND h.je_source <> 'Payables'
          AND l.ledger_id = 2042
          AND l.code_combination_id = cc.code_combination_id
          AND cc.chart_of_accounts_id = 50328;


