CREATE OR REPLACE FORCE VIEW xxeis.xxeis_oa_access_non_employees
(
   user_name
  ,employee_alias_name
  ,email_address
  ,user_active_date
  ,user_inactive_date
  ,date_of_last_login
  ,responsibility_name
  ,responsibility_active_date
  ,responsibility_inactive_date
)
AS
     SELECT u.user_name user_name
           ,u.description employee_alias_name
           ,u.email_address email_address
           ,u.start_date user_active_date
           ,u.end_date user_inactive_date
           ,u.last_logon_date date_of_last_login
           ,fr.responsibility_name responsibility_name
           ,urg.start_date responsibility_active_date
           ,urg.end_date responsibility_inactive_date
       FROM applsys.fnd_user u
           ,apps.fnd_user_resp_groups_all urg
           ,applsys.fnd_responsibility_tl fr
           ,applsys.fnd_responsibility resp
           ,apps.fnd_user_resp_groups_direct frgd
      WHERE     urg.user_id = u.user_id
            AND urg.responsibility_id = fr.responsibility_id
            AND fr.responsibility_id = resp.responsibility_id
            AND resp.responsibility_id = frgd.responsibility_id
            AND frgd.user_id = urg.user_id
            AND u.employee_id IS NULL
            AND urg.end_date IS NULL
            AND u.end_date IS NULL
            AND resp.end_date IS NULL
            AND fr.responsibility_name NOT LIKE '%INTERNET EXPENSES%'
   ORDER BY u.user_name;


