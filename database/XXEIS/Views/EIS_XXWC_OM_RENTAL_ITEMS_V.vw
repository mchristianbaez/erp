CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_rental_items_v
(
   date_from
  ,date_to
  ,location
  ,region
  ,item_number
  ,item_description
  ,average_cost
  ,qty_on_hand
  ,actual_rent_days
  ,qty_on_rent
  ,on_hand
  ,demand_qty
  ,varianc
  ,owned
  ,utilization_rate
)
AS
   SELECT date_from
         ,date_to
         ,a.location
         ,a.region                                                 --Added By Mahender for TMS# 20141006-00045
         ,a.item_number
         ,a.item_description
         ,NVL (
             xxeis.eis_rs_xxwc_com_util_pkg.get_unit_cost (a.inventory_item_id
                                                          ,a.organization_id
                                                          ,a.item_number)
            ,0)
             average_cost
         ,qty_on_hand
         , -- and DATE_RECEIVED between xxeis.eis_rs_xxwc_com_util_pkg.get_date_from and xxeis.eis_rs_xxwc_com_util_pkg.get_date_to)
          -- - NVL(ISR.QOH,0)  - NVL(ISR.demand,0)+NVL(ISR.ON_ORD,0)) QUANTITY_ONHAND,
          qty_on_rent actual_rent_days
         --         ,a.revenue_generated   --Commented By Mahender for TMS# 20141006-00045
         ,a.qty_on_rent
         /*,                                                                                         -- LINE_ID,
          NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_item_onhand_qty (a.inventory_item_id, a.organization_id)
              ,0)
             on_hand*/
         ,on_hand
         ,demand_qty                                               --Added By Mahender for TMS# 20141006-00045
         , (on_hand - demand_qty) varianc                          --Added By Mahender for TMS# 20141006-00045
         , (qty_on_rent + on_hand) owned                           --Added By Mahender for TMS# 20141006-00045
         , (qty_on_rent * 100) / (DECODE ( (qty_on_rent + on_hand), 0, 1, (qty_on_rent + on_hand)))
             utilization_rate                                      --Added By Mahender for TMS# 20141006-00045
     FROM (  SELECT                           -- OL.ORDERED_ITEM,OLRR.ATTRIBUTE12,OL.LINE_ID,RCTL.SALES_ORDER,
                   xxeis.eis_rs_xxwc_com_util_pkg.get_date_from date_from
                   ,xxeis.eis_rs_xxwc_com_util_pkg.get_date_to date_to
                   ,mtl.organization_code location
                   ,mtl.attribute9 region                          --Added By Mahender for TMS# 20141006-00045
                   ,msi.concatenated_segments item_number
                   ,msi.description item_description
                   --,SUM (olrr.ordered_quantity) qty_on_rent  --Commented By Mahender for TMS# 20141006-00045
                   , (SELECT NVL (SUM (ordered_quantity), 0)
                        FROM oe_order_lines_all
                       WHERE     inventory_item_id = msi.inventory_item_id
                             AND flow_status_code = 'AWAITING_RETURN'
                             AND ship_from_org_id = msi.organization_id)
                       qty_on_rent                                 --Added By Mahender for TMS# 20141006-00045
                   --                   ,NVL (SUM (NVL (rctl.extended_amount, 0) + NVL (ol.tax_value, 0)), 0) revenue_generated  --Commented By Mahender for TMS# 20141006-00045
                   ,msi.inventory_item_id
                   ,msi.organization_id
                   ,MAX (
                       xxeis.eis_po_xxwc_isr_util_pkg.get_onhand_inv (msi.inventory_item_id
                                                                     ,msi.organization_id))
                       qty_on_hand
                   ,NVL (
                       xxeis.eis_rs_xxwc_com_util_pkg.get_item_onhand_qty (msi.inventory_item_id
                                                                          ,msi.organization_id)
                      ,0)
                       on_hand
                   , (SELECT NVL (open_sales_orders, 0)
                        FROM apps.xxwc_inv_item_search_v
                       WHERE     inventory_item_id = msi.inventory_item_id
                             AND organization_id = msi.organization_id)
                       demand_qty                                  --Added By Mahender for TMS# 20141006-00045
               FROM apps.oe_order_lines_all ol
                   ,apps.oe_order_lines_all olrr
                   ,apps.ra_customer_trx_lines_all rctl
                   ,apps.mtl_system_items_b_kfv msi
                   ,apps.mtl_parameters mtl
              WHERE     rctl.interface_line_context = 'ORDER ENTRY'
                    AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)
                    AND ol.link_to_line_id = olrr.line_id
--                    AND msi.inventory_item_id = olrr.inventory_item_id  -- --Commented By Mahender for TMS# 20150127-00162
                    AND msi.organization_id = olrr.ship_from_org_id
                    AND mtl.organization_id = msi.organization_id
                    AND TRUNC (TO_DATE (olrr.attribute12, 'YYYY/MM/DD HH24:MI:SS')) >=
                           xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                    AND TRUNC (TO_DATE (olrr.attribute12, 'YYYY/MM/DD HH24:MI:SS')) <=
                           xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
                    --  and TRUNC(RCTL.CREATION_DATE)     >= TO_dATE('01-MAY-2014','DD-MON-YYYY')+0.25
                    --  AND TRUNC(RCTL.CREATION_DATE)     <= TO_dATE('31-MAY-2014','DD-MON-YYYY')+1.25
                    --  and OLRR.INVENTORY_ITEM_ID         =3067601
                    --  and OLRR.SHIP_FROM_ORG_ID          = 251
                    --    and OLRR.ORDERED_ITEM    = 'R176B46H90QP'
                    --      and MTL.organization_code= '025'
                    AND EXISTS
                           (SELECT 1
                              FROM apps.oe_order_lines_all ol2
                             WHERE ol2.ordered_item = 'Rental Charge' AND ol2.link_to_line_id = olrr.line_id)
           GROUP BY xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                   ,xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
                   ,mtl.attribute9
                   ,mtl.organization_code
                   ,msi.concatenated_segments
                   ,msi.description
                   ,msi.inventory_item_id
                   ,msi.organization_id      -- ,olrr.line_id  --Commented By Mahender for TMS# 20141006-00045
                                       ) a;
