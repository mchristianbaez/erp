CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_inv_prod_fill_rate_v
(
   common_output_id
  ,process_id
  ,bin_loc
  ,bpm_rpm
  ,part_number
  ,source
  ,part_desc
  ,rvw
  ,lt
  ,cl
  ,stk
  ,MIN
  ,MAX
  ,amu
  ,lg
  ,hit4_sales
  ,hit6_sales
  ,l_30_bo
  ,l_60_bo
  ,l_90_bo
  ,qty_ord
  ,shipped
  ,qty_bo
  ,oh
  ,qoh
  ,on_ord
  ,avail
  ,buyer
  ,region
  ,dist
  ,jan_sales
  ,feb_sales
  ,mar_sales
  ,apr_sales
  ,may_sales
  ,june_sales
  ,jul_sales
  ,aug_sales
  ,sep_sales
  ,oct_sales
  ,nov_sales
  ,dec_sales
  ,comp_orders
  ,bo_orders
  ,total_orders
  ,comp_lines
  ,bo_lines
  ,total_lines
  ,report_type
  ,velocity_class
  ,location
  ,sbuyer
  ,bl_comp_lines
  ,bl_bo_lines
  ,bl_total_lines
  ,svelocity
  ,svl_comp_lines
  ,svl_bo_lines
  ,svl_total_lines
  ,branch
  ,order_number
  ,customer_number
  ,salesrep_name
  ,bo_flag
  ,bo_region
  ,bo_district
  ,org_name
  ,avail2
  ,reg_sum_dpm_rpm
  ,buyer_dpm_sum
  ,sv_dpm_sum
  ,vendor_num
  ,vendor_name
  ,pplt
  ,plt
  ,clt
  ,safety_stock
  ,source_orgs
  ,uom
  ,res_qty
  ,cat
  ,st
  ,reg_bk_comp_lines
  ,by_bk_comp_lines
  ,svl_bk_comp_lines
  ,loc_comp_orders
  ,loc_bo_orders
  ,loc_tot_orders
  ,loc_comp_lines
  ,loc_bo_lines
  ,loc_tot_lines
  ,loc_bk_comp_lines
  ,loc_reg_sum
  ,loc_dist_sum
  ,loc_org_sum
  ,loc_stk_sum
  ,loc_dpm_rpm_sum
  ,location_comp_lines
  ,location_bo_lines
  ,location_tot_lines
  ,location_bk_comp_lines
  ,location_reg_sum
  ,location_dist_sum
  ,location_org_sum
  ,location_stk_sum
  ,location_dpm_rpm_sum
)
AS
   SELECT common_output_id
         ,process_id process_id
         ,varchar2_col1 bin_loc
         ,varchar2_col2 bpm_rpm
         ,varchar2_col3 part_number
         ,varchar2_col4 source
         ,varchar2_col5 part_desc
         ,number_col1 rvw
         ,number_col2 lt
         ,varchar2_col6 cl
         ,varchar2_col7 stk
         ,number_col3 MIN
         ,number_col4 MAX
         ,varchar2_col8 amu
         ,varchar2_col9 lg
         ,number_col5 hit4_sales
         ,number_col6 hit6_sales
         ,number_col7 l_30_bo
         ,number_col8 l_60_bo
         ,number_col9 l_90_bo
         ,number_col10 qty_ord
         ,number_col11 shipped
         ,number_col12 qty_bo
         ,varchar2_col10 oh
         ,number_col13 qoh
         ,number_col14 on_ord
         ,number_col15 avail
         ,varchar2_col11 buyer
         ,varchar2_col12 region
         ,varchar2_col13 dist
         ,number_col16 jan_sales
         ,number_col17 feb_sales
         ,number_col18 mar_sales
         ,number_col19 apr_sales
         ,number_col20 may_sales
         ,number_col21 june_sales
         ,number_col22 jul_sales
         ,number_col23 aug_sales
         ,number_col24 sep_sales
         ,number_col25 oct_sales
         ,number_col26 nov_sales
         ,number_col27 dec_sales
         ,number_col28 comp_orders
         ,number_col29 bo_orders
         ,number_col30 total_orders
         ,number_col31 comp_lines
         ,number_col32 bo_lines
         ,number_col33 total_lines
         ,varchar2_col14 report_type
         ,varchar2_col15 velocity_class
         ,varchar2_col16 location
         ,varchar2_col17 sbuyer
         ,number_col34 bl_comp_lines
         ,number_col35 bl_bo_lines
         ,number_col36 bl_total_lines
         ,varchar2_col18 svelocity
         ,number_col37 svl_comp_lines
         ,number_col38 svl_bo_lines
         ,number_col39 svl_total_lines
         ,varchar2_col19 branch
         ,varchar2_col20 order_number
         ,                                                                                    --- ORDER_NUMBER
          varchar2_col21 customer_number
         ,                                                                                 --- Customer_NUMBER
          varchar2_col22 salesrep_name
         ,                                                                                  --- SALES REP NAME
          varchar2_col23 bo_flag
         ,                                                                                         --- BO_FLAG
          varchar2_col24 bo_region
         ,                                                                                          --- Region
          varchar2_col25 bo_district
         ,                                                                                        --- District
          varchar2_col26 org_name
         ,                                                                                                 ---
          number_col40 avail2
         ,varchar2_col27 reg_sum_dpm_rpm
         ,varchar2_col28 buyer_dpm_sum
         ,varchar2_col29 sv_dpm_sum
         ,varchar2_col30 vendor_num
         ,                                                                                         --- Vendor#
          varchar2_col31 vendor_name
         ,                                                                                     --- Vendor_name
          number_col41 pplt
         ,                                                                                         --- Vendor#
          number_col42 plt
         ,                                                                                     --- Vendor_name
          number_col43 clt
         ,                                                                                         --- Vendor#
          number_col44 safety_stock
         ,                                                                                     --- Vendor_name
          number_col45 source_orgs
         ,varchar2_col32 uom
         ,varchar2_col33 res_qty
         ,varchar2_col34 cat
         ,varchar2_col35 st
         ,number_col46 reg_bk_comp_lines
         ,number_col47 by_bk_comp_lines
         ,number_col48 svl_bk_comp_lines
         ,number_col49 loc_comp_orders
         ,number_col50 loc_bo_orders
         ,number_col51 loc_tot_orders
         ,number_col52 loc_comp_lines
         ,number_col53 loc_bo_lines
         ,number_col54 loc_tot_lines
         ,number_col54 loc_bk_comp_lines
         ,varchar2_col36 loc_reg_sum
         ,varchar2_col37 loc_dist_sum
         ,varchar2_col38 loc_org_sum
         ,varchar2_col39 loc_stk_sum
         ,varchar2_col40 loc_dpm_rpm_sum
         ,number_col56 location_comp_lines
         ,                                                                             -- LOCATION_COMP_LINES,
          number_col57 location_bo_lines
         ,                                                                               -- LOCATION_BO_LINES,
          number_col58 location_tot_lines
         ,                                                                                   -- LOC_TOT_LINES,
          number_col59 location_bk_comp_lines
         ,                                                                               -- LOC_BK_COMP_LINES,
          varchar2_col41 location_reg_sum
         ,                                                                                     -- LOC_REG_SUM,
          varchar2_col42 location_dist_sum
         ,                                                                                    -- LOC_DIST_SUM,
          varchar2_col43 location_org_sum
         ,                                                                                     -- LOC_ORG_SUM,
          varchar2_col44 location_stk_sum
         ,                                                                                     -- LOC_STK_SUM,
          varchar2_col45 location_dpm_rpm_sum                                               -- LOC_DPM_RPM_SUM
     FROM xxeis.eis_rs_common_outputs;