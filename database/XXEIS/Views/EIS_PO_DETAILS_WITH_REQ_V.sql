/*************************************************************************
  $Header EIS_PO_DETAILS_WITH_REQ_V.sql $
  Module Name: EIS_PO_DETAILS_WITH_REQ_V
  PURPOSE: Used by EIS report HDS GSC iPro Purchase Orders with Requisition Details. 
  Note: There could be other EIS report or other forms of reports using the view. 
  REVISIONS:
  Ver        Date                   Author                          Description
  ---------  ----------------   ------------------             ----------------
  1.0        29-Apr-2016   Balaguru Seshadri     TMS 20151223-00078
**************************************************************************/
CREATE OR REPLACE FORCE VIEW XXEIS.EIS_PO_DETAILS_WITH_REQ_V
(
   PO_HEADER_ID,
   PO_LAST_UPDATE_DATE,
   PO_LAST_UPDATED_BY,
   PO_LAST_UPDATED_BY_NAME, --TMS 20151223-00078
   PO_CREATION_DATE,
   PO_CREATED_BY,
   PO_CREATED_BY_NAME, --TMS 20151223-00078
   PO_BUYER_NAME,
   PO_NUMBER,
   PO_TYPE,
   PO_SUPPLIER_NAME,
   PO_SUPPLIER_SITE_CODE,
   SUPPLIER_CONTACT_FIRST_NAME,
   SUPPLIER_CONTACT_LAST_NAME,
   SUPPLIER_CONTACT_AREA_CODE,
   SUPPLIER_CONTACT_PHONE,
   PO_SUMMARY,
   PO_ENABLED,
   PO_START_DATE,
   PO_END_DATE,
   PO_SHIP_TO_LOCATION,
   PO_TERMS,
   PO_SHIP_VIA,
   PO_FOB,
   PO_FREIGHT_TERMS,
   PO_STATUS,
   PO_REVISION_NUM,
   PO_REVISED_DATE,
   PO_PRINTED_DATE,
   PO_APPROVED_DATE,
   PO_CURRENCY_CODE,
   PO_ACCEPTANCE_DUE_DATE,
   PO_ON_HOLD,
   PO_ACCEPTANCE_REQUIRED,
   PO_NOTE_TO_AUTHORIZER,
   PO_NOTE_TO_SUPPLIER,
   PO_NOTE_TO_RECEIVER,
   PO_COMMENTS,
   PO_CLOSED_CODE,
   PO_CLOSED_DATE,
   PO_LINE_ID,
   POL_LAST_UPDATE_DATE,
   POL_LAST_UPDATED_BY,
   POL_CREATION_DATE,
   POL_CREATED_BY,
   POL_CREATED_BY_NAME, --TMS 20151223-00078
   PO_LINE_TYPE,
   PO_LINE_NUM,
   PO_ITEM_NUMBER,
   PO_ITEM_CATEGORY,
   PO_ITEM_DESCRIPTION,
   PO_UNIT_PRICE,
   PO_QUANTITY_ORDERED,
   PO_QUANTITY_CANCELLED,
   PO_QUANTITY_RECEIVED,
   PO_QUANTITY_DUE,
   PO_QUANTITY_BILLED,
   PO_AMOUNT_ORDERED,
   PO_AMOUNT_CANCELLED,
   PO_AMOUNT_RECEIVED,
   PO_AMOUNT_DUE,
   PO_AMOUNT_BILLED,
   PO_ITEM_REVISION,
   PO_UOM,
   PO_LIST_PRICE,
   PO_MARKET_PRICE,
   POL_CANCEL,
   POL_CANCEL_DATE,
   POL_CANCEL_REASON,
   PO_TAXABLE_FLAG,
   PO_TAX_NAME,
   PO_TYPE_1099,
   POL_CLOSED_CODE,
   POL_CLOSED_DATE,
   POL_CLOSED_REASON,
   LINE_LOCATION_ID,
   PO_SHIPMENT_NUM,
   PO_SHIPMENT_TYPE,
   PO_NEED_BY_DATE,
   PO_PROMISED_DATE,
   PO_LAST_ACCEPT_DATE,
   PO_4_WAY_MATCH,
   PO_3_WAY_MATCH,
   PO_QTY_RCV_TOLERANCE,
   PO_DAYS_EARLY_RECEIPT_ALLOWED,
   PO_DAYS_LATE_RECEIPT_ALLOWED,
   PO_INVOICE_CLOSE_TOLERANCE,
   PO_RECEIVE_CLOSE_TOLERANCE,
   PO_ACCRUE_ON_RECEIPT,
   PO_DISTRIBUTION_ID,
   PO_DISTRIBUTION_NUM,
   PO_SET_OF_BOOKS,
   PO_CHARGE_CCID,
   PO_CHARGE_ACCOUNT,
   PO_ACCRUAL_CCID,
   PO_ACCRUAL_ACCOUNT,
   PO_VARIANCE_CCID,
   PO_VARIANCE_ACCOUNT,
   DISTRIBUTION_ID,
   REQUISITION_LINE_ID,
   REQUISITION_HEADER_ID,
   REQ_NUMBER,
   REQ_DESCRIPTION,
   REQ_STATUS,
   REQ_TYPE,
   REQ_LINE_NUM,
   REQ_LINE_ITEM_DESCRIPTION,
   REQ_LINE_UNIT_PRICE,
   REQ_LINE_QTY,
   OPERATING_UNIT,
   ORG_ID,
   VENDOR_ID,
   INVENTORY_ITEM_ID,
   LOCATION_ID,
   AGENT_ID,
   CODE_COMBINATION_ID,
   PROJECT_NUMBER,
   PROJECT_NAME,
   PROJECT_ID,
   GCC#BRANCH,
   MSI#HDS#LOB,
   MSI#HDS#DROP_SHIPMENT_ELIGAB,
   MSI#HDS#INVOICE_UOM,
   MSI#HDS#PRODUCT_ID,
   MSI#HDS#VENDOR_PART_NUMBER,
   MSI#HDS#UNSPSC_CODE,
   MSI#HDS#UPC_PRIMARY,
   MSI#HDS#SKU_DESCRIPTION,
   MSI#WC#CA_PROP_65,
   MSI#WC#COUNTRY_OF_ORIGIN,
   MSI#WC#ORM_D_FLAG,
   MSI#WC#STORE_VELOCITY,
   MSI#WC#DC_VELOCITY,
   MSI#WC#YEARLY_STORE_VELOCITY,
   MSI#WC#YEARLY_DC_VELOCITY,
   MSI#WC#PRISM_PART_NUMBER,
   MSI#WC#HAZMAT_DESCRIPTION,
   MSI#WC#HAZMAT_CONTAINER,
   MSI#WC#GTP_INDICATOR,
   MSI#WC#LAST_LEAD_TIME,
   MSI#WC#AMU,
   MSI#WC#RESERVE_STOCK,
   MSI#WC#TAXWARE_CODE,
   MSI#WC#AVERAGE_UNITS,
   MSI#WC#PRODUCT_CODE,
   MSI#WC#IMPORT_DUTY_,
   MSI#WC#KEEP_ITEM_ACTIVE,
   MSI#WC#PESTICIDE_FLAG,
   MSI#WC#CALC_LEAD_TIME,
   MSI#WC#VOC_GL,
   MSI#WC#PESTICIDE_FLAG_STATE,
   MSI#WC#VOC_CATEGORY,
   MSI#WC#VOC_SUB_CATEGORY,
   MSI#WC#MSDS_#,
   MSI#WC#HAZMAT_PACKAGING_GROU,
   POH#STANDARDP#NEED_BY_DATE,
   POH#STANDARDP#FREIGHT_TERMS_,
   POH#STANDARDP#CARRIER_TERMS_,
   POH#STANDARDP#FOB_TERMS_TAB,
   GCC#50328#PRODUCT,
   GCC#50328#PRODUCT#DESCR,
   GCC#50328#LOCATION,
   GCC#50328#LOCATION#DESCR,
   GCC#50328#COST_CENTER,
   GCC#50328#COST_CENTER#DESCR,
   GCC#50328#ACCOUNT,
   GCC#50328#ACCOUNT#DESCR,
   GCC#50328#PROJECT_CODE,
   GCC#50328#PROJECT_CODE#DESCR,
   GCC#50328#FURTURE_USE,
   GCC#50328#FURTURE_USE#DESCR,
   GCC#50328#FUTURE_USE_2,
   GCC#50328#FUTURE_USE_2#DESCR,
   GCC#50368#ACCOUNT,
   GCC#50368#ACCOUNT#DESCR,
   GCC#50368#DEPARTMENT,
   GCC#50368#DEPARTMENT#DESCR,
   GCC#50368#DIVISION,
   GCC#50368#DIVISION#DESCR,
   GCC#50368#FUTURE_USE,
   GCC#50368#FUTURE_USE#DESCR,
   GCC#50368#PRODUCT,
   GCC#50368#PRODUCT#DESCR,
   GCC#50368#SUBACCOUNT,
   GCC#50368#SUBACCOUNT#DESCR
)
AS
   SELECT poh.po_header_id,
          poh.last_update_date po_last_update_date,
          poh.last_updated_by po_last_updated_by,
          -- Begin TMS 20151223-00078
          (SELECT /*+ RESULT_CACHE */
                 description
             FROM apps.fnd_user
            WHERE 1 = 1 AND user_id = poh.last_updated_by)
             po_last_updated_by_name,
          -- End TMS 20151223-00078
          poh.creation_date po_creation_date,
          poh.created_by po_created_by,
          -- Begin TMS 20151223-00078
          (SELECT /*+ RESULT_CACHE */
                 description
             FROM apps.fnd_user
            WHERE 1 = 1 AND user_id = poh.created_by)
             po_created_by_name,
          -- End TMS 20151223-00078
          poav.agent_name po_buyer_name,
          poh.segment1 po_number,
          poh.type_lookup_code po_type,
          pv.vendor_name po_supplier_name,
          pvs.vendor_site_code po_supplier_site_code,
          pvc.first_name supplier_contact_first_name,
          pvc.last_name supplier_contact_last_name,
          pvc.area_code supplier_contact_area_code,
          pvc.phone supplier_contact_phone,
          DECODE (poh.summary_flag,  'Y', 'Yes',  'N', 'No') po_summary,
          DECODE (poh.enabled_flag,  'Y', 'Yes',  'N', 'No') po_enabled,
          poh.start_date_active po_start_date,
          poh.end_date_active po_end_date,
          hrl.location_code po_ship_to_location,
          apt.NAME po_terms,
          poh.ship_via_lookup_code po_ship_via,
          poh.fob_lookup_code po_fob,
          poh.freight_terms_lookup_code po_freight_terms,
          poh.authorization_status po_status,
          poh.revision_num po_revision_num,
          poh.revised_date po_revised_date,
          poh.printed_date po_printed_date,
          poh.approved_date po_approved_date,
          poh.currency_code po_currency_code,
          poh.acceptance_due_date po_acceptance_due_date,
          DECODE (poh.user_hold_flag,  'Y', 'Yes',  'N', 'No',  NULL)
             po_on_hold,
          DECODE (poh.acceptance_required_flag,
                  'Y', 'Yes',
                  'N', 'No',
                  NULL)
             po_acceptance_required,
          poh.note_to_authorizer po_note_to_authorizer,
          poh.note_to_vendor po_note_to_supplier,
          poh.note_to_receiver po_note_to_receiver,
          poh.comments po_comments,
          poh.closed_code po_closed_code,
          poh.closed_date po_closed_date,
          pol.po_line_id,
          pol.last_update_date pol_last_update_date,
          pol.last_updated_by pol_last_updated_by,
          pol.creation_date pol_creation_date,
          pol.created_by pol_created_by,
          -- Begin TMS 20151223-00078
          (SELECT /*+ RESULT_CACHE */
                 description
             FROM apps.fnd_user
            WHERE 1 = 1 AND user_id = pol.created_by)
             pol_created_by_name,
          -- End TMS 20151223-00078
          plt.line_type po_line_type,
          pol.line_num po_line_num,
          msi.concatenated_segments po_item_number,
          mca.concatenated_segments po_item_category,
          -- Begin TMS 20151223-00078                         --
          --REPLACE (pol.item_description, '~') po_item_description, --????
          REGEXP_REPLACE (REPLACE (pol.item_description, '~'),
                          '([^[:print:]])',
                          ' ')
             po_item_description,
          -- End TMS 20151223-00078
          TO_NUMBER (
             DECODE (plt.order_type_lookup_code,
                     'AMOUNT', 1,
                     NVL (poll.price_override, pol.unit_price)))
             po_unit_price,
          ROUND (pod.quantity_ordered,
                 xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision)
             po_quantity_ordered,
          ROUND (pod.quantity_cancelled,
                 xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision)
             po_quantity_cancelled,
          ROUND (pod.quantity_delivered,
                 xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision)
             po_quantity_received,
          ROUND (
             (  pod.quantity_ordered
              - pod.quantity_cancelled
              - pod.quantity_delivered),
             xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision)
             po_quantity_due,
          ROUND (pod.quantity_billed,
                 xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision)
             po_quantity_billed,
          (  TO_NUMBER (
                DECODE (plt.order_type_lookup_code,
                        'AMOUNT', 1,
                        NVL (poll.price_override, pol.unit_price)))
           * ROUND (pod.quantity_ordered,
                    xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision))
             po_amount_ordered,
          (  TO_NUMBER (
                DECODE (plt.order_type_lookup_code,
                        'AMOUNT', 1,
                        NVL (poll.price_override, pol.unit_price)))
           * ROUND (pod.quantity_cancelled,
                    xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision))
             po_amount_cancelled,
          (  TO_NUMBER (
                DECODE (plt.order_type_lookup_code,
                        'AMOUNT', 1,
                        NVL (poll.price_override, pol.unit_price)))
           * ROUND (pod.quantity_delivered,
                    xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision))
             po_amount_received,
          (  TO_NUMBER (
                DECODE (plt.order_type_lookup_code,
                        'AMOUNT', 1,
                        NVL (poll.price_override, pol.unit_price)))
           * ROUND (
                (  pod.quantity_ordered
                 - pod.quantity_cancelled
                 - pod.quantity_delivered),
                xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision))
             po_amount_due,
          (  TO_NUMBER (
                DECODE (plt.order_type_lookup_code,
                        'AMOUNT', 1,
                        NVL (poll.price_override, pol.unit_price)))
           * ROUND (pod.quantity_billed,
                    xxeis.eis_rs_po_fin_com_util_pkg.get_qty_precision))
             po_amount_billed,
          pol.item_revision po_item_revision,
          pol.unit_meas_lookup_code po_uom,
          pol.list_price_per_unit po_list_price,
          pol.market_price po_market_price,
          pol.cancel_flag pol_cancel,
          pol.cancel_date pol_cancel_date,
          pol.cancel_reason pol_cancel_reason,
          NVL (pol.taxable_flag, poll.taxable_flag) po_taxable_flag,
          pol.tax_name po_tax_name,
          pol.type_1099 po_type_1099,
          pol.closed_code pol_closed_code,
          pol.closed_date pol_closed_date,
          pol.closed_reason pol_closed_reason,
          poll.line_location_id,
          poll.shipment_num po_shipment_num,
          poll.shipment_type po_shipment_type,
          poll.need_by_date po_need_by_date,
          poll.promised_date po_promised_date,
          poll.last_accept_date po_last_accept_date,
          DECODE (poll.inspection_required_flag,  'Y', 'Yes',  'N', 'No')
             po_4_way_match,
          DECODE (poll.receipt_required_flag,  'Y', 'Yes',  'N', 'No')
             po_3_way_match,
          poll.qty_rcv_tolerance po_qty_rcv_tolerance,
          poll.days_early_receipt_allowed po_days_early_receipt_allowed,
          poll.days_late_receipt_allowed po_days_late_receipt_allowed,
          poll.invoice_close_tolerance po_invoice_close_tolerance,
          poll.receive_close_tolerance po_receive_close_tolerance,
          DECODE (poll.accrue_on_receipt_flag,  'Y', 'Yes',  'N', 'No')
             po_accrue_on_receipt,
          pod.po_distribution_id,
          pod.distribution_num po_distribution_num,
          sob.NAME po_set_of_books,
          gcc.code_combination_id po_charge_ccid,
          gcc.concatenated_segments po_charge_account,
          gcc1.code_combination_id po_accrual_ccid,
          gcc1.concatenated_segments po_accrual_account,
          gcc2.code_combination_id po_variance_ccid,
          gcc2.concatenated_segments po_variance_account,
          pord.distribution_id,
          porl.requisition_line_id,
          porh.requisition_header_id,
          porh.segment1 req_number,
          porh.description req_description,
          porh.authorization_status req_status,
          porh.type_lookup_code req_type,
          porl.line_num req_line_num,
          -- Begin TMS 20151223-00078
          --REPLACE (porl.item_description, '~') req_line_item_description,
          REGEXP_REPLACE (REPLACE (porl.item_description, '~'),
                          '([^[:print:]])',
                          ' ')
             req_line_item_description,
          -- End TMS 20151223-00078
          porl.unit_price req_line_unit_price,
          porl.quantity req_line_qty,
          hou.name operating_unit,
          hou.organization_id org_id,
          pv.vendor_id,
          msi.inventory_item_id,
          hrl.location_id,
          poav.agent_id,
          gcc.code_combination_id,
          Pa.Segment1 Project_Number,
          pa.name project_name,
          pa.project_id                                --descr#flexfield#start
                       ,
          xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', GCC.ATTRIBUTE1, 'I')
             GCC#Branch,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'HDS', MSI.ATTRIBUTE1, NULL)
             MSI#HDS#LOB,
          DECODE (
             MSI.ATTRIBUTE_CATEGORY,
             'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No',
                                                      MSI.ATTRIBUTE10,
                                                      'F'),
             NULL)
             MSI#HDS#Drop_Shipment_Eligab,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'HDS', MSI.ATTRIBUTE15, NULL)
             MSI#HDS#Invoice_UOM,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'HDS', MSI.ATTRIBUTE2, NULL)
             MSI#HDS#Product_ID,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'HDS', MSI.ATTRIBUTE3, NULL)
             MSI#HDS#Vendor_Part_Number,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'HDS', MSI.ATTRIBUTE4, NULL)
             MSI#HDS#UNSPSC_Code,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'HDS', MSI.ATTRIBUTE5, NULL)
             MSI#HDS#UPC_Primary,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'HDS', MSI.ATTRIBUTE6, NULL)
             MSI#HDS#SKU_Description,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE1, NULL)
             MSI#WC#CA_Prop_65,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE10, NULL)
             MSI#WC#Country_of_Origin,
          DECODE (
             MSI.ATTRIBUTE_CATEGORY,
             'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No',
                                                     MSI.ATTRIBUTE11,
                                                     'F'),
             NULL)
             MSI#WC#ORM_D_Flag,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE12, NULL)
             MSI#WC#Store_Velocity,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE13, NULL)
             MSI#WC#DC_Velocity,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE14, NULL)
             MSI#WC#Yearly_Store_Velocity,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE15, NULL)
             MSI#WC#Yearly_DC_Velocity,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE16, NULL)
             MSI#WC#PRISM_Part_Number,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE17, NULL)
             MSI#WC#Hazmat_Description,
          DECODE (
             MSI.ATTRIBUTE_CATEGORY,
             'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER',
                                                     MSI.ATTRIBUTE18,
                                                     'I'),
             NULL)
             MSI#WC#Hazmat_Container,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE19, NULL)
             MSI#WC#GTP_Indicator,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE2, NULL)
             MSI#WC#Last_Lead_Time,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE20, NULL)
             MSI#WC#AMU,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE21, NULL)
             MSI#WC#Reserve_Stock,
          DECODE (
             MSI.ATTRIBUTE_CATEGORY,
             'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE',
                                                     MSI.ATTRIBUTE22,
                                                     'I'),
             NULL)
             MSI#WC#Taxware_Code,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE25, NULL)
             MSI#WC#Average_Units,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE26, NULL)
             MSI#WC#Product_code,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE27, NULL)
             MSI#WC#Import_Duty_,
          DECODE (
             MSI.ATTRIBUTE_CATEGORY,
             'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No',
                                                     MSI.ATTRIBUTE29,
                                                     'F'),
             NULL)
             MSI#WC#Keep_Item_Active,
          DECODE (
             MSI.ATTRIBUTE_CATEGORY,
             'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No',
                                                     MSI.ATTRIBUTE3,
                                                     'F'),
             NULL)
             MSI#WC#Pesticide_Flag,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE30, NULL)
             MSI#WC#Calc_Lead_Time,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE4, NULL)
             MSI#WC#VOC_GL,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE5, NULL)
             MSI#WC#Pesticide_Flag_State,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE6, NULL)
             MSI#WC#VOC_Category,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE7, NULL)
             MSI#WC#VOC_Sub_Category,
          DECODE (MSI.ATTRIBUTE_CATEGORY, 'WC', MSI.ATTRIBUTE8, NULL)
             MSI#WC#MSDS_#,
          DECODE (
             MSI.ATTRIBUTE_CATEGORY,
             'WC', xxeis.eis_rs_dff.decode_valueset (
                      'XXWC_HAZMAT_PACKAGE_GROUP',
                      MSI.ATTRIBUTE9,
                      'I'),
             NULL)
             MSI#WC#Hazmat_Packaging_Grou,
          DECODE (POH.ATTRIBUTE_CATEGORY,
                  'Standard Purchase Order', POH.ATTRIBUTE1,
                  NULL)
             POH#StandardP#Need_By_Date,
          DECODE (POH.ATTRIBUTE_CATEGORY,
                  'Standard Purchase Order', POH.ATTRIBUTE2,
                  NULL)
             POH#StandardP#Freight_Terms_,
          DECODE (POH.ATTRIBUTE_CATEGORY,
                  'Standard Purchase Order', POH.ATTRIBUTE3,
                  NULL)
             POH#StandardP#Carrier_TERMS_,
          DECODE (POH.ATTRIBUTE_CATEGORY,
                  'Standard Purchase Order', POH.ATTRIBUTE4,
                  NULL)
             POH#StandardP#FOB_TERMS_Tab                 --descr#flexfield#end
                                        --gl#accountff#start
          ,
          GCC.SEGMENT1 GCC#50328#PRODUCT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                'XXCUS_GL_PRODUCT')
             GCC#50328#PRODUCT#DESCR,
          GCC.SEGMENT2 GCC#50328#LOCATION,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                'XXCUS_GL_LOCATION')
             GCC#50328#LOCATION#DESCR,
          GCC.SEGMENT3 GCC#50328#COST_CENTER,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                'XXCUS_GL_COSTCENTER')
             GCC#50328#COST_CENTER#DESCR,
          GCC.SEGMENT4 GCC#50328#ACCOUNT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                'XXCUS_GL_ACCOUNT')
             GCC#50328#ACCOUNT#DESCR,
          GCC.SEGMENT5 GCC#50328#PROJECT_CODE,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                'XXCUS_GL_PROJECT')
             GCC#50328#PROJECT_CODE#DESCR,
          GCC.SEGMENT6 GCC#50328#FURTURE_USE,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                'XXCUS_GL_FUTURE_USE1')
             GCC#50328#FURTURE_USE#DESCR,
          GCC.SEGMENT7 GCC#50328#FUTURE_USE_2,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                'XXCUS_GL_FUTURE_USE_2')
             GCC#50328#FUTURE_USE_2#DESCR,
          GCC.SEGMENT4 GCC#50368#ACCOUNT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                'XXCUS_GL_ LTMR _ACCOUNT')
             GCC#50368#ACCOUNT#DESCR,
          GCC.SEGMENT3 GCC#50368#DEPARTMENT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                'XXCUS_GL_ LTMR _DEPARTMENT')
             GCC#50368#DEPARTMENT#DESCR,
          GCC.SEGMENT2 GCC#50368#DIVISION,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                'XXCUS_GL_ LTMR _DIVISION')
             GCC#50368#DIVISION#DESCR,
          GCC.SEGMENT6 GCC#50368#FUTURE_USE,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                'XXCUS_GL_ LTMR _FUTUREUSE')
             GCC#50368#FUTURE_USE#DESCR,
          GCC.SEGMENT1 GCC#50368#PRODUCT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                'XXCUS_GL_LTMR_PRODUCT')
             GCC#50368#PRODUCT#DESCR,
          GCC.SEGMENT5 GCC#50368#SUBACCOUNT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                'XXCUS_GL_ LTMR _SUBACCOUNT')
             GCC#50368#SUBACCOUNT#DESCR
     --gl#accountff#end



     FROM po_headers poh,
          po_lines pol,
          po_line_locations poll,
          po_distributions pod,
          ap_suppliers pv,
          po_vendor_sites pvs,
          po_vendor_contacts pvc,
          po_line_types plt,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mca,
          hr_locations_all hrl,
          po_agents_v poav,
          ap_terms apt,
          gl_sets_of_books sob,
          gl_code_combinations_kfv gcc,
          gl_code_combinations_kfv gcc1,
          gl_code_combinations_kfv gcc2,
          po_requisition_headers porh,
          po_requisition_lines porl,
          po_req_distributions pord,
          pa_projects pa,
          APPS.hr_operating_units hou
    WHERE     poh.po_header_id = pol.po_header_id(+)
          AND pol.po_line_id = poll.po_line_id(+)
          AND poll.line_location_id = pod.line_location_id(+)
          AND poh.vendor_id = pv.vendor_id
          AND poh.vendor_site_id = pvs.vendor_site_id
          AND poh.vendor_contact_id = pvc.vendor_contact_id(+)
          AND pol.line_type_id = plt.line_type_id
          AND pol.item_id = msi.inventory_item_id(+)
          AND pol.org_id = msi.organization_id(+)
          AND pol.category_id = mca.category_id
          AND poll.ship_to_location_id = hrl.location_id(+)
          AND poh.agent_id = poav.agent_id(+)
          AND poh.terms_id = apt.term_id(+)
          AND pod.set_of_books_id = sob.set_of_books_id
          AND pod.code_combination_id = gcc.code_combination_id
          AND pod.accrual_account_id = gcc1.code_combination_id
          AND pod.variance_account_id = gcc2.code_combination_id
          AND pod.req_distribution_id = pord.distribution_id(+)
          AND pord.requisition_line_id = porl.requisition_line_id(+)
          AND porl.requisition_header_id = porh.requisition_header_id(+)
          AND pa.project_id(+) = pod.project_id
          AND hou.organization_id = poh.org_id
/*AND xxeis.eis_gl_security_pkg.validate_access (sob.set_of_books_id,
                                               pod.code_combination_id
                                              ) = 'TRUE'
AND xxeis.eis_gl_security_pkg.validate_access (sob.set_of_books_id,
                                               pod.accrual_account_id
                                              ) = 'TRUE'
AND xxeis.eis_gl_security_pkg.validate_access (sob.set_of_books_id,
                                               pod.variance_account_id
                                              ) = 'TRUE' */
;
COMMENT ON TABLE XXEIS.EIS_PO_DETAILS_WITH_REQ_V IS 'TMS 20151223-00078';