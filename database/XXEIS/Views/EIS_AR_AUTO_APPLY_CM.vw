 /******************************************************************************
   NAME       :  XXEIS.EIS_AR_AUTO_APPLY_CM
   PURPOSE    :  This is a custom view to fetch account attribute and AR details

   REVISIONS:
   Ver        Date        Author           	Description
   ---------  ----------  ---------------  	------------------------------------
   1.0        09/07/2015 Maharajan Shunmugam 	TMS#20150123-00197 AR - Customer Flag as indicator to auto apply credits
                                           
******************************************************************************/
CREATE OR REPLACE FORCE VIEW XXEIS.EIS_AR_AUTO_APPLY_CM
 (customer_number
 ,customer_name
 ,auto_apply
 ,credit_hold
 ,account_status
 ,profile_class
 ,collector
 ,source_code
 ,Total_ar_balance
 ,credit_memo_remaining_bal
 ,credit_memo_transaction_count )
AS
 SELECT   cust.account_number
         ,cust.account_name
         ,DECODE(cust.attribute11,'Y','ON','N','OFF',NULL)
         ,prof.credit_hold
         ,NVL (cust.status,'A')
         ,pc.name profile_class
         ,coll.name collector
         ,cust.attribute4 source_code
         ,MAX((SELECT SUM(amount_due_remaining) 
           FROM ar_payment_schedules_all 
           WHERE customer_id = cust.cust_account_id
            AND status = 'OP'))Total_ar_balance
         ,SUM(amount_due_remaining) credit_memo_remaining_bal
        , COUNT(rcta.trx_number) credit_memo_transaction_count      
    FROM AR.hz_cust_accounts cust,
         AR.hz_customer_profiles prof,
         AR.hz_cust_profile_classes pc,    
         AR.ar_collectors coll,
         AR.ar_payment_schedules_all aps,
         AR.ra_customer_trx_all rcta
   WHERE cust.cust_account_id   =   prof.cust_account_id    
    AND prof.profile_class_id   = pc.profile_class_id
    AND prof.collector_id       = coll.collector_id(+)
    AND aps.customer_id = cust.cust_account_id
    AND rcta.sold_to_customer_id = cust.cust_account_id
    AND rcta.cust_trx_type_id = 2
    AND aps.customer_trx_id = rcta.customer_trx_id
    AND prof.site_use_id IS NULL
    AND aps.status = 'OP'
    GROUP BY cust.account_number
            ,cust.account_name
            ,DECODE(cust.attribute11,'Y','ON','N','OFF',NULL)
	    ,prof.credit_hold
            ,NVL (cust.status,'A')
            ,pc.name 
            ,coll.name
            ,cust.attribute4
/