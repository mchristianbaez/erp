CREATE OR REPLACE FORCE VIEW xxeis.xxeis_wc_po_buyer_listing_v
(
   buyer_employee_id
  ,buyer_employee_num
  ,buyer_full_name
  ,buyer_first_name
  ,buyer_middle_name
  ,buyer_last_name
  ,buyer_prefix
  ,buyer_expense_check_addr_flag
  ,buyer_email_address
  ,location_id
  ,location_code
  ,location_description
  ,location_ship_to_site_flag
  ,location_receiving_site_flag
  ,location_bill_to_site_flag
  ,location_in_organization_flag
  ,location_office_site_flag
  ,location_style
  ,location_address_line_1
  ,location_address_line_2
  ,location_address_line_3
  ,location_town_or_city
  ,location_country
  ,location_postal_code
  ,location_region_1
  ,location_region_2
  ,location_region_3
  ,location_telephone_number_1
  ,location_telephone_number_2
  ,poa_agent_id
  ,poa_authorization_limit
  ,poa_program_update_date
  ,poa_start_date
  ,poa_end_date
  ,c_flex_cat
  ,mca_padded_concat_segments
  ,mca_structure_id
  ,mca_segment1
  ,mca_segment2
  ,mca_segment3
  ,mca_segment4
  ,mca_segment5
  ,mca_segment6
  ,mca_segment7
  ,mca_segment8
  ,mca_segment9
  ,mca_segment10
  ,mca_segment11
  ,mca_segment12
  ,mca_segment13
  ,mca_segment14
  ,mca_segment15
  ,c_company
  ,c_organization_id
  ,gl_currency
  ,structure_acc
  ,structure_cat
  ,c_category_set_id
  ,c_yes
  ,c_no
  ,status
  ,po_approval_level
  ,employee_title
)
AS
   SELECT DISTINCT                                                            -- hr_employees buyer table data
          hre.employee_id buyer_employee_id
         ,hre.employee_num buyer_employee_num
         ,hre.full_name buyer_full_name
         ,hre.first_name buyer_first_name
         ,hre.middle_name buyer_middle_name
         ,hre.last_name buyer_last_name
         ,hre.prefix buyer_prefix
         ,hre.expense_check_address_flag buyer_expense_check_addr_flag
         ,hre.email_address buyer_email_address
         ,hrl.location_id location_id
         ,hrl.location_code location_code
         ,hrl.description location_description
         ,hrl.ship_to_site_flag location_ship_to_site_flag
         ,hrl.receiving_site_flag location_receiving_site_flag
         ,hrl.bill_to_site_flag location_bill_to_site_flag
         ,hrl.in_organization_flag location_in_organization_flag
         ,hrl.office_site_flag location_office_site_flag
         ,hrl.style location_style
         ,hrl.address_line_1 location_address_line_1
         ,hrl.address_line_2 location_address_line_2
         ,hrl.address_line_3 location_address_line_3
         ,hrl.town_or_city location_town_or_city
         ,hrl.country location_country
         ,hrl.postal_code location_postal_code
         ,hrl.region_1 location_region_1
         ,hrl.region_2 location_region_2
         ,hrl.region_3 location_region_3
         ,hrl.telephone_number_1 location_telephone_number_1
         ,hrl.telephone_number_2 location_telephone_number_2
         ,poa.agent_id poa_agent_id
         ,poa.authorization_limit poa_authorization_limit
         ,poa.program_update_date poa_program_update_date
         ,poa.start_date_active poa_start_date
         ,NVL (poa.end_date_active, fu.end_date) poa_end_date
         ,mca.concatenated_segments c_flex_cat
         ,mca.padded_concatenated_segments mca_padded_concat_segments
         ,mca.structure_id mca_structure_id
         ,mca.segment1 mca_segment1
         ,mca.segment2 mca_segment2
         ,mca.segment3 mca_segment3
         ,mca.segment4 mca_segment4
         ,mca.segment5 mca_segment5
         ,mca.segment6 mca_segment6
         ,mca.segment7 mca_segment7
         ,mca.segment8 mca_segment8
         ,mca.segment9 mca_segment9
         ,mca.segment10 mca_segment10
         ,mca.segment11 mca_segment11
         ,mca.segment12 mca_segment12
         ,mca.segment13 mca_segment13
         ,mca.segment14 mca_segment14
         ,mca.segment15 mca_segment15
         ,gle.name c_company
         ,fsp.inventory_organization_id c_organization_id
         ,gle.currency_code gl_currency
         ,gle.chart_of_accounts_id structure_acc
         ,mdv.structure_id structure_cat
         ,mdv.category_set_id c_category_set_id
         ,flo1.meaning c_yes
         ,flo2.meaning c_no
         , (CASE
               WHEN (    SYSDATE >= NVL (poa.start_date_active, SYSDATE - 1)
                     AND SYSDATE < NVL (poa.end_date_active, SYSDATE + 1)
                     AND SYSDATE >= NVL (fu.start_date, SYSDATE - 1)
                     AND SYSDATE < NVL (fu.end_date, SYSDATE + 1))
               THEN
                  'A'
               ELSE
                  'I'
            END)
             status
         ,pj.name po_approval_level
         ,ps.job_descr employee_title
     FROM po_agents poa
         ,hr_locations hrl
         ,hr_employees hre
         ,mtl_categories_kfv mca
         ,financials_system_parameters fsp
         ,gl_ledgers gle
         ,fnd_lookups flo1
         ,fnd_lookups flo2
         ,mtl_default_sets_view mdv
         ,fnd_user fu
         ,apps.per_all_people_f p1
         ,per_all_assignments_f paaf
         ,per_jobs pj
         ,xxcus.xxcushr_ps_emp_all_tbl ps
    WHERE     poa.agent_id = hre.employee_id
          AND poa.category_id = mca.category_id(+)
          AND NVL (poa.location_id, -1) = hrl.location_id(+)
          AND mdv.structure_id(+) = mca.structure_id
          AND gle.ledger_id = fsp.set_of_books_id
          AND NVL (mdv.functional_area_id, 2) = 2
          AND flo1.lookup_type = 'YES_NO'
          AND flo1.lookup_code = 'Y'
          AND flo2.lookup_type = 'YES_NO'
          AND flo2.lookup_code = 'N'
          AND fu.employee_id = hre.employee_id
          AND fu.employee_id = p1.person_id(+)
          AND paaf.job_id = pj.job_id(+)
          AND p1.person_id = paaf.person_id(+)
          AND p1.employee_number = ps.employee_number(+);
