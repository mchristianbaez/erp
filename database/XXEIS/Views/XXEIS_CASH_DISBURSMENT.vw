CREATE OR REPLACE FORCE VIEW xxeis.xxeis_cash_disbursment
(
   bank_account_name
  ,check_date
  ,check_number
  ,payment_status_descr
  ,void_date
  ,stop_payment_recorded_date
  ,vendor_name
  ,address_line1
  ,address_line2
  ,address_line3
  ,vendor_city
  ,vendor_postal_code
  ,vendor_state
  ,vendor_site_code
  ,payment_type
  ,pay_group
  ,supplier_number
  ,cleared_date
  ,payment_priority
  ,amount
  ,payment_method_code
)
AS
   SELECT c.bank_account_name bank_account_name
         ,TRUNC (c.check_date) check_date
         ,c.check_number check_number
         ,c.status_lookup_code payment_status_descr
         ,c.void_date void_date
         ,c.stopped_date stop_payment_recorded_date
         ,c.vendor_name vendor_name
         ,c.address_line1 address_line1
         ,c.address_line2 address_line2
         ,c.address_line3 address_line3
         ,c.city vendor_city
         ,c.zip vendor_postal_code
         ,c.state vendor_state
         ,c.vendor_site_code vendor_site_code
         ,c.payment_type_flag payment_type
         ,vs.pay_group_lookup_code pay_group
         ,v.segment1 supplier_number
         ,c.cleared_date cleared_date
         ,vs.payment_priority payment_priority
         ,c.amount amount
         ,c.payment_method_code
     FROM ap.ap_supplier_sites_all vs
         ,ap.ap_suppliers v
         ,ap.ap_checks_all c
         ,ap.ap_invoices_all i
    WHERE     vs.vendor_site_id = c.vendor_site_id
          AND vs.vendor_site_id = i.vendor_site_id
          AND v.vendor_id = vs.vendor_id;


