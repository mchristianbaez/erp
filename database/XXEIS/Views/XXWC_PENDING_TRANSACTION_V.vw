CREATE OR REPLACE FORCE VIEW xxeis.xxwc_pending_transaction_v
(
   order_by
  ,period_name
  ,resolution_type
  ,transaction_type
  ,organization_code
  ,organization_name
  ,COUNT
)
AS
   SELECT order_by
         ,period_name
         ,resolution_type
         ,transaction_type
         ,organization_code
         ,organization_name
         ,COUNT
     FROM (  --Resolution Required
             --1.Unprocessed Material
             SELECT '1' order_by
                   ,oap.period_name
                   ,'Resolution Required' resolution_type
                   ,'Unprocessed Material' transaction_type
                   ,ood.organization_code
                   ,ood.organization_name
                   ,COUNT (1) COUNT
               FROM apps.mtl_material_transactions_temp mmtt
                   ,apps.org_organization_definitions ood
                   ,apps.org_acct_periods oap
              WHERE     transaction_date <= oap.schedule_close_date
                    AND NVL (transaction_status, 0) <> 2
                    AND mmtt.organization_id = ood.organization_id
                    AND mmtt.organization_id = oap.organization_id
           GROUP BY oap.period_name, ood.organization_code, ood.organization_name
           UNION ALL
             --2.Uncosted Material
             SELECT '2' order_by
                   ,oap.period_name
                   ,'Resolution Required' resolution_type
                   ,'Uncosted Material' transaction_type
                   ,ood.organization_code
                   ,ood.organization_name
                   ,COUNT (1) COUNT
               FROM apps.mtl_material_transactions mmt
                   ,apps.org_organization_definitions ood
                   ,apps.org_acct_periods oap
              WHERE     mmt.transaction_date <= oap.schedule_close_date
                    AND mmt.costed_flag IS NOT NULL
                    AND mmt.organization_id = ood.organization_id
                    AND mmt.organization_id = oap.organization_id
           GROUP BY oap.period_name, ood.organization_code, ood.organization_name
           UNION ALL
             --3.Pending WIP
             SELECT '3' order_by
                   ,oap.period_name
                   ,'Resolution Required' resolution_type
                   ,'Pending WIP' transaction_type
                   ,ood.organization_code
                   ,ood.organization_name
                   ,COUNT (1) COUNT
               FROM apps.wip_cost_txn_interface wcti
                   ,apps.org_organization_definitions ood
                   ,apps.org_acct_periods oap
              WHERE     transaction_date <= oap.schedule_close_date
                    AND wcti.organization_id = ood.organization_id
                    AND wcti.organization_id = oap.organization_id
           GROUP BY oap.period_name, ood.organization_code, ood.organization_name
           UNION ALL
             --4.Uncosted WSM
             SELECT '4' order_by
                   ,oap.period_name
                   ,'Resolution Required' resolution_type
                   ,'Uncosted WSM' transaction_type
                   ,ood.organization_code
                   ,ood.organization_name
                   ,COUNT (1) COUNT
               FROM apps.wsm_split_merge_transactions wsmt
                   ,apps.org_organization_definitions ood
                   ,apps.org_acct_periods oap
              WHERE     costed <> 4
                    AND transaction_date <= oap.schedule_close_date
                    AND wsmt.organization_id = ood.organization_id
                    AND wsmt.organization_id = oap.organization_id
           GROUP BY oap.period_name, ood.organization_code, ood.organization_name
           UNION ALL
             --5.Pending WMS Interface
             SELECT '5' order_by
                   ,oap.period_name
                   ,'Resolution Required' resolution_type
                   ,'Pending WMS Interface' transaction_type
                   ,ood.organization_code
                   ,ood.organization_name
                   ,COUNT (1) COUNT
               FROM apps.wsm_split_merge_txn_interface wsmti
                   ,apps.org_organization_definitions ood
                   ,apps.org_acct_periods oap
              WHERE     process_status <> 4
                    AND transaction_date <= oap.schedule_close_date
                    AND wsmti.organization_id = ood.organization_id
                    AND wsmti.organization_id = oap.organization_id
           GROUP BY oap.period_name, ood.organization_code, ood.organization_name
           UNION ALL
             --6.Pending Transactions
             SELECT '6' order_by
                   ,oap.period_name
                   ,'Resolution Required' resolution_type
                   ,'Unprocessed Shipping Transactions' transaction_type
                   ,ood.organization_code
                   ,ood.organization_name
                   ,COUNT (1) COUNT
               FROM apps.wsh_delivery_details wdd
                   ,apps.wsh_delivery_assignments wda
                   ,apps.wsh_new_deliveries wnd
                   ,apps.wsh_delivery_legs wdl
                   ,apps.wsh_trip_stops wts
                   ,apps.org_organization_definitions ood
                   ,apps.org_acct_periods oap
              WHERE     wdd.source_code = 'OE'
                    AND wdd.released_status = 'C'
                    AND wdd.inv_interfaced_flag IN ('N', 'P')
                    AND wda.delivery_detail_id = wdd.delivery_detail_id
                    AND wnd.delivery_id = wda.delivery_id
                    AND wnd.status_code IN ('CL', 'IT')
                    AND wdl.delivery_id = wnd.delivery_id
                    AND wts.pending_interface_flag = 'Y'
                    AND TRUNC (wts.actual_departure_date) <= oap.schedule_close_date
                    AND wdl.pick_up_stop_id = wts.stop_id
                    AND wdd.organization_id = ood.organization_id
                    AND wdd.organization_id = oap.organization_id
           GROUP BY oap.period_name, ood.organization_code, ood.organization_name
           UNION ALL
             --7.Pending Material- Order Entry
             SELECT '7' order_by
                   ,oap.period_name
                   ,'Resolution Required' resolution_type
                   ,'Pending Material - Order Entry' transaction_type
                   ,ood.organization_code
                   ,ood.organization_name
                   ,COUNT (1) COUNT
               FROM apps.mtl_transactions_interface mti
                   ,apps.org_organization_definitions ood
                   ,apps.org_acct_periods oap
              WHERE     mti.transaction_date <= oap.schedule_close_date
                    AND process_flag <> 9
                    AND mti.source_code = 'ORDER ENTRY'
                    AND mti.organization_id = ood.organization_id
                    AND mti.organization_id = oap.organization_id
           GROUP BY oap.period_name, ood.organization_code, ood.organization_name
           UNION ALL
             --Resolution Recommended
             --8.Pending Receiving
             SELECT '8' order_by
                   ,oap.period_name
                   ,'Resolution Recommended' resolution_type
                   ,'Pending Receiving' transaction_type
                   ,ood.organization_code
                   ,ood.organization_name
                   ,COUNT (1) COUNT
               FROM apps.rcv_transactions_interface rti
                   ,apps.org_organization_definitions ood
                   ,apps.org_acct_periods oap
              WHERE     rti.transaction_date <= oap.schedule_close_date
                    AND rti.destination_type_code = 'INVENTORY'
                    AND rti.to_organization_id = ood.organization_id
                    AND rti.to_organization_id = oap.organization_id
           GROUP BY oap.period_name, ood.organization_code, ood.organization_name
           UNION ALL
             --9.Pending Material
             SELECT '9' order_by
                   ,oap.period_name
                   ,'Resolution Recommended' resolution_type
                   ,'Pending Material' transaction_type
                   ,ood.organization_code
                   ,ood.organization_name
                   ,COUNT (1) COUNT
               FROM apps.mtl_transactions_interface mti
                   ,apps.org_organization_definitions ood
                   ,apps.org_acct_periods oap
              WHERE     mti.transaction_date <= oap.schedule_close_date
                    AND process_flag <> 9
                    AND mti.source_code NOT IN ('ORDER ENTRY')
                    AND mti.organization_id = ood.organization_id
                    AND mti.organization_id = oap.organization_id
           GROUP BY oap.period_name, ood.organization_code, ood.organization_name
           UNION ALL
             --10.Pending Shop Floor Move
             SELECT '10' order_by
                   ,oap.period_name
                   ,'Resolution Recommended' resolution_type
                   ,'Pending Shop Floor Move' transaction_type
                   ,ood.organization_code
                   ,ood.organization_name
                   ,COUNT (1) COUNT
               FROM apps.wip_move_txn_interface wmti
                   ,apps.org_organization_definitions ood
                   ,apps.org_acct_periods oap
              WHERE     transaction_date <= oap.schedule_close_date
                    AND wmti.organization_id = ood.organization_id
                    AND wmti.organization_id = oap.organization_id
           GROUP BY oap.period_name, ood.organization_code, ood.organization_name);