---------------------------------------------------------------------------------------------------------
/******************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_ONHAND_QUANTITY_V $
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     		nil       			nil   		 initial version
  1.1  	    14-Mar-2017		 		Siva		TMS#20170301-00261 
**********************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_ONHAND_QUANTITY_V (INV_ORG_NAME, ORGANIZATION_ID, ORGANIZATION_CODE, MASTER_ORG_CODE, SUBINVENTORY_CODE, LOCATOR_ID, LOCATOR, INVENTORY_ITEM_ID, ITEM, DESCRIPTION, REVISION, UNIT_OF_MEASURE, ON_HAND, UNPACKED, PACKED, COST_GROUP_ID, LOT_NUMBER, PROJECT_ID, TASK_ID, SUBINVENTORY_STATUS_ID, LOCATOR_STATUS_ID, LOT_STATUS_ID, PLANNING_TP_TYPE, PLANNING_ORGANIZATION_ID, PLANNING_ORGANIZATION, OWNING_TP_TYPE, OWNING_ORGANIZATION_ID, OWNING_ORGANIZATION, PROJECT_NAME, LOT_EXPIRY_DATE, COST_GROUP, LPN, LPN_ID, STATUS_CODE, UNIT_NUMBER, SUBINVENTORY_TYPE, SECONDARY_INVENTORY_NAME, DATE_RECEIVED, ONHAND_QUANTITIES_ID, INVENTORY_LOCATION_ID, LANGUAGE, MIN_MINMAX_QUANTITY, MAX_MINMAX_QUANTITY, LIST_PRICE_PER_UNIT, MP_ORGANIZATION_ID, MIL_ORGANIZATION_ID, MSI_ORGANIZATION_ID, MSIV_INVENTORY_ITEM_ID, MSIV_ORGANIZATION_ID, MTV_TASK_ID, CCG_COST_GROUP_ID, CCGA_COST_GROUP_ID, CCGA_ORGANIZATION_ID, MMS_STATUS_ID, OOD_ORGANIZATION_ID, OODW_ORGANIZATION_ID,
  OODP_ORGANIZATION_ID, HAOU_ORGANIZATION_ID, MLN_INVENTORY_ITEM_ID, MLN_ORGANIZATION_ID, MLN_LOT_NUMBER, MP#FACTORY_PLANNER_DATA_DIRE, MP#FRU, MP#LOCATION_NUMBER, MP#BRANCH_OPERATIONS_MANAGER, MP#DELIVER_CHARGE, MP#FACTORY_PLANNER_EXECUTABL, MP#FACTORY_PLANNER_USER, MP#FACTORY_PLANNER_HOST, MP#FACTORY_PLANNER_PORT_NUMB, MP#PRICING_ZONE, MP#ORG_TYPE, MP#DISTRICT, MP#REGION, MSIV#HDS#LOB, MSIV#HDS#DROP_SHIPMENT_ELIGA, MSIV#HDS#INVOICE_UOM, MSIV#HDS#PRODUCT_ID, MSIV#HDS#VENDOR_PART_NUMBER, MSIV#HDS#UNSPSC_CODE, MSIV#HDS#UPC_PRIMARY, MSIV#HDS#SKU_DESCRIPTION, MSIV#WC#CA_PROP_65, MSIV#WC#COUNTRY_OF_ORIGIN, MSIV#WC#ORM_D_FLAG, MSIV#WC#STORE_VELOCITY, MSIV#WC#DC_VELOCITY, MSIV#WC#YEARLY_STORE_VELOCIT, MSIV#WC#YEARLY_DC_VELOCITY, MSIV#WC#PRISM_PART_NUMBER, MSIV#WC#HAZMAT_DESCRIPTION, MSIV#WC#HAZMAT_CONTAINER, MSIV#WC#GTP_INDICATOR, MSIV#WC#LAST_LEAD_TIME, MSIV#WC#AMU, MSIV#WC#RESERVE_STOCK, MSIV#WC#TAXWARE_CODE, MSIV#WC#AVERAGE_UNITS, MSIV#WC#PRODUCT_CODE, MSIV#WC#IMPORT_DUTY_,
  MSIV#WC#KEEP_ITEM_ACTIVE, MSIV#WC#PESTICIDE_FLAG, MSIV#WC#CALC_LEAD_TIME, MSIV#WC#VOC_GL, MSIV#WC#PESTICIDE_FLAG_STATE, MSIV#WC#VOC_CATEGORY, MSIV#WC#VOC_SUB_CATEGORY, MSIV#WC#MSDS_#, MSIV#WC#HAZMAT_PACKAGING_GRO, MIL#101#STOCKLOCATIONS, MIL#101#STOCKLOCATIONS_DESC, SALES_VELOCITY, OPEN_DEMAND, AVERAGE_COST, CREATION_DATE, REGION)
AS
SELECT haou.name inv_org_name ,
  moq.organization_id organization_id ,
  mp.organization_code organization_code ,
  ood.organization_code master_org_code ,
  moq.subinventory_code subinventory_code ,
  moq.locator_id locator_id ,
  mil.concatenated_segments locator ,
  moq.inventory_item_id inventory_item_id ,
  msiv.concatenated_segments item ,
  msiv.description description ,
  moq.revision revision ,
  Muom.Unit_Of_Measure ,
  /*  --Commented by siva for TMS#20160601-00025
  (SELECT SUM (NVL(moq1.Primary_Transaction_Quantity,0))
  FROM Mtl_Onhand_Quantities_Detail Moq1
  WHERE Moq1.Inventory_Item_Id = Msiv.Inventory_Item_Id
  AND Moq1.Organization_Id     = Msiv.Organization_Id
  AND Moq1.subinventory_code   = msi.secondary_inventory_name
  ) on_hand --Added for TMS#20140714-00037 by Mahender on 15/07/14
  --        ,moq.primary_transaction_quantity on_hand    --Commented for TMS#20140714-00037 by Mahender on 15/07/14
  --         ,DECODE (moq.containerized_flag, 1, 0, moq.primary_transaction_quantity) unpacked   --Commented for TMS#20140714-00037 by Mahender on 15/07/14
  ,
  DECODE ( moq.containerized_flag ,1, 0 ,
  (SELECT SUM (NVL(moq1.Primary_Transaction_Quantity,0))
  FROM Mtl_Onhand_Quantities_Detail Moq1
  WHERE Moq1.Inventory_Item_Id = Msiv.Inventory_Item_Id
  AND moq1.Organization_Id     = Msiv.Organization_Id
  AND Moq1.subinventory_code   = msi.secondary_inventory_name
  )) unpacked --Added for TMS#20140714-00037 by Mahender on 15/07/14
  --         ,DECODE (moq.containerized_flag, 1, moq.primary_transaction_quantity, 0) packed   --Commented for TMS#20140714-00037 by Mahender on 15/07/14
  ,
  DECODE ( moq.containerized_flag ,1,
  (SELECT SUM (NVL(Moq1.Primary_Transaction_Quantity,0))
  FROM Mtl_Onhand_Quantities_Detail Moq1
  WHERE Moq1.Inventory_Item_Id = Msiv.Inventory_Item_Id
  AND moq1.Organization_Id     = Msiv.Organization_Id
  AND Moq1.subinventory_code   = msi.secondary_inventory_name
  ) ,0) packed --Added for TMS#20140714-00037 by Mahender on 15/07/14
  ,*/
  Xxeis.Eis_Xxwc_Inv_Util_Pkg.Get_Onhand_Qty(Msiv.Inventory_Item_Id,Msiv.Organization_Id,Msi.Secondary_Inventory_Name) On_Hand, --added by siva for TMS#20160601-00025
  DECODE ( Moq.Containerized_Flag ,1, 0 ,Xxeis.Eis_Xxwc_Inv_Util_Pkg.Get_Onhand_Qty(Msiv.Inventory_Item_Id,Msiv.Organization_Id,Msi.Secondary_Inventory_Name)) Unpacked, --added by siva for TMS#20160601-00025
  DECODE ( moq.containerized_flag ,1,Xxeis.Eis_Xxwc_Inv_Util_Pkg.Get_Onhand_Qty(Msiv.Inventory_Item_Id,Msiv.Organization_Id,Msi.Secondary_Inventory_Name) ,0) packed , --added by siva for TMS#20160601-00025
  moq.cost_group_id cost_group_id ,
  moq.lot_number lot_number , -- TO_CHAR (NULL) serial_number,
  mil.project_id project_id ,
  mil.task_id task_id ,
  msi.status_id subinventory_status_id ,
  mil.status_id locator_status_id ,
  mln.status_id lot_status_id ,
  moq.planning_tp_type planning_tp_type ,
  moq.planning_organization_id planning_organization_id ,
  oodp.organization_name planning_organization ,
  moq.owning_tp_type owning_tp_type ,
  moq.owning_organization_id owning_organization_id ,
  oodw.organization_name owning_organization ,
  mpv.project_name ,
  mln.expiration_date lot_expiry_date ,
  ccg.cost_group ,
  mlv.lpn ,
  moq.lpn_id ,
  mms.status_code ,
  pun.unit_number ,
  msi.subinventory_type ,
  msi.secondary_inventory_name ,
  moq.date_received ,
  moq.onhand_quantities_id ,
  mil.inventory_location_id ,
  muom.language ,
  msiv.min_minmax_quantity ,
  msiv.max_minmax_quantity ,
  msiv.list_price_per_unit ,
  -- primary keys added for component_joins
  mp.organization_id mp_organization_id ,
  mil.organization_id mil_organization_id ,
  msi.organization_id msi_organization_id ,
  msiv.inventory_item_id msiv_inventory_item_id ,
  msiv.organization_id msiv_organization_id ,
  mtv.task_id mtv_task_id ,
  ccg.cost_group_id ccg_cost_group_id ,
  ccga.cost_group_id ccga_cost_group_id ,
  ccga.organization_id ccga_organization_id ,
  mms.status_id mms_status_id ,
  ood.organization_id ood_organization_id ,
  oodw.organization_id oodw_organization_id ,
  oodp.organization_id oodp_organization_id ,
  haou.organization_id haou_organization_id ,
  mln.inventory_item_id mln_inventory_item_id ,
  mln.organization_id mln_organization_id ,
  mln.lot_number mln_lot_number
  --,msiv.SEGMENT1 item_num
  --descr#flexfield#start
  ,
  mp.attribute1 mp#factory_planner_data_dire ,
  mp.attribute10 mp#fru ,
  mp.attribute11 mp#location_number ,
  xxeis.eis_rs_dff.decode_valueset ('HR_DE_EMPLOYEES', mp.attribute13, 'F') mp#branch_operations_manager ,
  xxeis.eis_rs_dff.decode_valueset ('XXWC_DELIVERY_CHARGE_EXEMPT', mp.attribute14, 'I') mp#deliver_charge ,
  mp.attribute2 mp#factory_planner_executabl ,
  mp.attribute3 mp#factory_planner_user ,
  mp.attribute4 mp#factory_planner_host ,
  mp.attribute5 mp#factory_planner_port_numb ,
  xxeis.eis_rs_dff.decode_valueset ('XXWC_PRICE_ZONES', mp.attribute6, 'F') mp#pricing_zone ,
  xxeis.eis_rs_dff.decode_valueset ('XXWC_ORG_TYPE', mp.attribute7, 'I') mp#org_type ,
  xxeis.eis_rs_dff.decode_valueset ('XXWC_DISTRICT', mp.attribute8, 'I') mp#district ,
  xxeis.eis_rs_dff.decode_valueset ('XXWC_REGION', mp.attribute9, 'I') mp#region ,
  DECODE (msiv.attribute_category, 'HDS', msiv.attribute1, NULL) msiv#hds#lob ,
  DECODE (msiv.attribute_category ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msiv.attribute10, 'F') ,NULL) msiv#hds#drop_shipment_eliga ,
  DECODE (msiv.attribute_category, 'HDS', msiv.attribute15, NULL) msiv#hds#invoice_uom ,
  DECODE (msiv.attribute_category, 'HDS', msiv.attribute2, NULL) msiv#hds#product_id ,
  DECODE (msiv.attribute_category, 'HDS', msiv.attribute3, NULL) msiv#hds#vendor_part_number ,
  DECODE (msiv.attribute_category, 'HDS', msiv.attribute4, NULL) msiv#hds#unspsc_code ,
  DECODE (msiv.attribute_category, 'HDS', msiv.attribute5, NULL) msiv#hds#upc_primary ,
  DECODE (msiv.attribute_category, 'HDS', msiv.attribute6, NULL) msiv#hds#sku_description ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute1, NULL) msiv#wc#ca_prop_65 ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute10, NULL) msiv#wc#country_of_origin ,
  DECODE (msiv.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msiv.attribute11, 'F') ,NULL) msiv#wc#orm_d_flag ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute12, NULL) msiv#wc#store_velocity ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute13, NULL) msiv#wc#dc_velocity ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute14, NULL) msiv#wc#yearly_store_velocit ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute15, NULL) msiv#wc#yearly_dc_velocity ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute16, NULL) msiv#wc#prism_part_number ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute17, NULL) msiv#wc#hazmat_description ,
  DECODE (msiv.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msiv.attribute18, 'I') ,NULL) msiv#wc#hazmat_container ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute19, NULL) msiv#wc#gtp_indicator ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute2, NULL) msiv#wc#last_lead_time ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute20, NULL) msiv#wc#amu ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute21, NULL) msiv#wc#reserve_stock ,
  DECODE (msiv.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msiv.attribute22, 'I') ,NULL) msiv#wc#taxware_code ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute25, NULL) msiv#wc#average_units ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute26, NULL) msiv#wc#product_code ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute27, NULL) msiv#wc#import_duty_ ,
  DECODE (msiv.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msiv.attribute29, 'F') ,NULL) msiv#wc#keep_item_active ,
  DECODE (msiv.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msiv.attribute3, 'F') ,NULL) msiv#wc#pesticide_flag ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute30, NULL) msiv#wc#calc_lead_time ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute4, NULL) msiv#wc#voc_gl ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute5, NULL) msiv#wc#pesticide_flag_state ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute6, NULL) msiv#wc#voc_category ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute7, NULL) msiv#wc#voc_sub_category ,
  DECODE (msiv.attribute_category, 'WC', msiv.attribute8, NULL) msiv#wc#msds_# ,
  DECODE (msiv.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC_HAZMAT_PACKAGE_GROUP', msiv.attribute9, 'I') ,NULL) msiv#wc#hazmat_packaging_gro
  --descr#flexfield#end
  --kff#start
  ,
  Mil.Segment1 Mil#101#Stocklocations ,
  Xxeis.Eis_Rs_Dff.Decode_Valueset ('XXWC_STOCK_LOCATIONS', Mil.Segment1, 'N') Mil#101#Stocklocations_Desc ,
  --kff#end
  (
  SELECT mic.segment1
  FROM mtl_item_categories_v mic
  WHERE mic.category_set_name = 'Sales Velocity'
  AND mic.inventory_item_id   = msiv.inventory_item_id
  AND mic.organization_id     = msiv.organization_id
  ) SALES_VELOCITY --Added for TMS#20140714-00037 by Mahender on 15/07/14
  ,
  (SELECT SUM(NVL( -1 * ( mdorv.primary_uom_quantity - mdorv.total_reservation_quantity - mdorv.completed_quantity ), 0)) quantity
  FROM apps.mtl_parameters mp,
    apps.mtl_system_items msi,
    apps.bom_calendar_dates bcd,
    apps.mrp_demand_om_reservations_v mdorv,
    apps.oe_order_headers_all ooha,
    apps.oe_order_lines_all oola,
    apps.mfg_lookups ml,
    (SELECT DECODE (demand_source_type, 2, DECODE (reservation_type, 1, 2, 3, 23, 9), 8, DECODE (reservation_type, 1, 21, 22), demand_source_type ) supply_demand_source_type,
      demand_id
    FROM apps.mrp_demand_om_reservations_v
    ) dx
  WHERE mdorv.open_flag                     = 'Y'
  AND ml.lookup_type                        = 'MTL_SUPPLY_DEMAND_SOURCE_TYPE'
  AND ml.lookup_code                        = dx.supply_demand_source_type
  AND ml.lookup_code                       IN (2,21)
  AND mdorv.demand_id                       = dx.demand_id
  AND ooha.header_id                        = oola.header_id
  AND oola.line_id                          = mdorv.demand_id
  AND mdorv.organization_id                 = msiv.organization_id
  AND mdorv.primary_uom_quantity            > (mdorv.total_reservation_quantity + mdorv.completed_quantity )
  AND mdorv.inventory_item_id               = MSIV.INVENTORY_ITEM_ID
  AND ( mdorv.visible_demand_flag           = 'Y'
  OR ( NVL (mdorv.visible_demand_flag, 'N') = 'N'
  AND mdorv.ato_line_id                    IS NOT NULL ))
  AND msi.organization_id                   = mdorv.organization_id
  AND msi.inventory_item_id                 = mdorv.inventory_item_id
  AND mp.organization_id                    =mdorv.organization_id
  AND mp.calendar_code                      = bcd.calendar_code
  AND mp.calendar_exception_set_id          = bcd.exception_set_id
  AND bcd.calendar_date                     = TRUNC (mdorv.requirement_date)
  AND mdorv.inventory_item_id               = DECODE (mdorv.reservation_type, 1, DECODE (mdorv.parent_demand_id, NULL, mdorv.inventory_item_id, -1 ), 2, mdorv.inventory_item_id, 3, mdorv.inventory_item_id, -1 )
  ) Open_Demand --Added for TMS#20150710-00136 by Mahender on 21/08/15
  ,(select sum(nvl(tab.aver_cost,0)) averagecost 
    from xxeis.eis_xxwc_po_isr_tab tab
    where tab.inventory_item_id   = msiv.inventory_item_id
      and tab.organization_id     = msiv.organization_id  
    )average_cost  --added for version 1.1
   ,moq.creation_date  --added for version 1.1
   ,mp.attribute9 region  --added for version 1.1
  --gl#accountff#start
  --gl#accountff#end
FROM Mtl_Onhand_Quantities_Detail Moq ,
  mtl_parameters mp ,
  mtl_item_locations_kfv mil ,
  mtl_secondary_inventories msi ,
  mtl_lot_numbers mln ,
  mtl_system_items_kfv msiv ,
  mtl_project_v mpv ,
  mtl_task_v mtv ,
  mtl_units_of_measure_tl muom ,
  cst_cost_groups ccg ,
  Cst_Cost_Group_Accounts Ccga ,
  mtl_onhand_lpn_v mlv ,
  mtl_material_statuses_vl mms ,
  pjm_unit_numbers pun ,
  org_organization_definitions ood ,
  org_organization_definitions oodw ,
  org_organization_definitions oodp ,
  hr_all_organization_units haou
WHERE moq.organization_id        = mp.organization_id
AND moq.organization_id          = mil.organization_id(+)
AND moq.locator_id               = mil.inventory_location_id(+)
AND moq.organization_id          = msi.organization_id
AND moq.subinventory_code        = msi.secondary_inventory_name
AND moq.organization_id          = mln.organization_id(+)
AND moq.inventory_item_id        = mln.inventory_item_id(+)
AND moq.lot_number               = mln.lot_number(+)
AND moq.organization_id          = msiv.organization_id
AND moq.inventory_item_id        = msiv.inventory_item_id
AND Mil.Project_Id               = Mpv.Project_Id(+)
AND Mil.Task_Id                  = Mtv.Task_Id(+)
AND msiv.primary_uom_code        = muom.uom_code
AND muom.language                = USERENV ('LANG')
AND Moq.Cost_Group_Id            = Ccg.Cost_Group_Id
AND Ccg.Cost_Group_Id            = Ccga.Cost_Group_Id
AND Moq.Lpn_Id                   = Mlv.Lpn_Id(+)
AND Mil.Status_Id                = Mms.Status_Id(+)
AND moq.organization_id          = pun.master_organization_id(+)
AND mp.master_organization_id    = ood.organization_id
AND moq.planning_organization_id = oodp.organization_id
AND Moq.Owning_Organization_Id   = Oodw.Organization_Id
AND msi.organization_id          = haou.organization_id
AND EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE Organization_Id = Msi.Organization_Id
  )
/
