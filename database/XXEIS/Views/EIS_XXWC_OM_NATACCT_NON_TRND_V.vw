CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_natacct_non_trnd_v
(
   parent_name
  ,invoice_date_cond
  ,invoice_date
  ,invoice_number
  ,account_number
  ,account_name
  ,job_number
  ,job_name
  ,customer_job
  ,customer_po
  ,loc
  ,salesrep_number
  ,part_number
  ,part_description
  ,vendor_name
  ,vendor_number
  ,inv_cat_class
  ,qty
  ,sales
  ,cost
  ,unit_selling_price
  ,unit_cost
  ,order_number
  ,order_line
  ,ordered_date_cond
  ,ordered_date
  ,customer_class_code
  ,header_id
  ,line_id
  ,organization_id
  ,party_id
  ,cust_account_id
  ,inventory_item_id
  ,msi_organization_id
  ,cust_acct#party_type
  ,cust_acct#vndr_code_and_frul
  ,cust_acct#branch_description
  ,cust_acct#customer_source
  ,cust_acct#legal_collection_i
  ,cust_acct#prism_number
  ,cust_acct#yes#note_line_#5
  ,cust_acct#yes#note_line_#1
  ,cust_acct#yes#note_line_#2
  ,cust_acct#yes#note_line_#3
  ,cust_acct#yes#note_line_#4
  ,party#101#party_type
  ,party#101#collector
  ,ship_from_org#factory_planne
  ,ship_from_org#fru
  ,ship_from_org#location_numbe
  ,ship_from_org#branch_operati
  ,ship_from_org#deliver_charge
  ,ship_from_org#factory_planne1
  ,ship_from_org#factory_planne2
  ,ship_from_org#factory_planne3
  ,ship_from_org#factory_planne4
  ,ship_from_org#pricing_zone
  ,ship_from_org#org_type
  ,ship_from_org#district
  ,ship_from_org#region
  ,msi#hds#lob
  ,msi#hds#drop_shipment_eligab
  ,msi#hds#invoice_uom
  ,msi#hds#product_id
  ,msi#hds#vendor_part_number
  ,msi#hds#unspsc_code
  ,msi#hds#upc_primary
  ,msi#hds#sku_description
  ,msi#wc#ca_prop_65
  ,msi#wc#country_of_origin
  ,msi#wc#orm_d_flag
  ,msi#wc#store_velocity
  ,msi#wc#dc_velocity
  ,msi#wc#yearly_store_velocity
  ,msi#wc#yearly_dc_velocity
  ,msi#wc#prism_part_number
  ,msi#wc#hazmat_description
  ,msi#wc#hazmat_container
  ,msi#wc#gtp_indicator
  ,msi#wc#last_lead_time
  ,msi#wc#amu
  ,msi#wc#reserve_stock
  ,msi#wc#taxware_code
  ,msi#wc#average_units
  ,msi#wc#product_code
  ,msi#wc#import_duty_
  ,msi#wc#keep_item_active
  ,msi#wc#pesticide_flag
  ,msi#wc#calc_lead_time
  ,msi#wc#voc_gl
  ,msi#wc#pesticide_flag_state
  ,msi#wc#voc_category
  ,msi#wc#voc_sub_category
  ,msi#wc#msds_#
  ,msi#wc#hazmat_packaging_grou
  ,oh#rental_term
  ,oh#order_channel
  ,ol#estimated_return_date
  ,ol#rerent_po
  ,ol#force_ship
  ,ol#rental_date
  ,ol#item_on_blowout
  ,ol#pof_std_line
  ,ol#rerental_billing_terms
  ,ol#pricing_guardrail
  ,ol#print_expired_product_dis
  ,ol#rental_charge
  ,ol#vendor_quote_cost
  ,ol#po_cost_for_vendor_quote
  ,ol#serial_number
  ,ol#engineering_cost
  ,ol#application_method
)
AS
   SELECT cust_acct.attribute4 parent_name
         ,TRUNC (rct.trx_date) invoice_date_cond
         , (TO_CHAR (rct.trx_date, 'MM/DD/YYYY')) invoice_date
         ,rct.trx_number invoice_number
         ,cust_acct.account_number
         ,NVL (cust_acct.account_name, party.party_name) account_name
         ,hzps_ship_to.party_site_number job_number
         ,CASE
             WHEN INSTR (hzcs_ship_to.location, '-') > 0
             THEN
                SUBSTR (hzcs_ship_to.location
                       ,INSTR (hzcs_ship_to.location, '-') + 1)
             ELSE
                NULL
          END
             job_name
         ,                  --SUBSTR(Hzps_Ship_To.Party_Site_Name,5) Job_Name,
          hzcs_ship_to.location customer_job
         ,oh.cust_po_number customer_po
         ,ship_from_org.organization_code loc
         ,rep.salesrep_number
         ,DECODE (
             msi.segment1
            ,'Rental Charge', xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item (
                                 ol.link_to_line_id)
            ,msi.segment1)
             part_number
         ,DECODE (
             msi.segment1
            ,'Rental Charge', xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item_desc (
                                 ol.link_to_line_id)
            ,msi.description)
             part_description
         ,DECODE (
             msi.segment1
            ,'Rental Charge', xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name (
                                 ol.link_to_line_id)
            ,xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name (
                msi.inventory_item_id
               ,msi.organization_id))
             vendor_name
         ,DECODE (
             msi.segment1
            ,'Rental Charge', xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number (
                                 ol.link_to_line_id)
            ,xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number (
                msi.inventory_item_id
               ,msi.organization_id))
             vendor_number
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class (
             msi.inventory_item_id
            ,msi.organization_id)
             inv_cat_class
         ,NVL (rctl.quantity_invoiced, rctl.quantity_credited) qty
         ,                                 --NVL(RCTL.QUANTITY_ORDERED,0) QTY,
          ROUND (
             (  NVL (ol.unit_selling_price, 0)
              * NVL (rctl.quantity_invoiced, rctl.quantity_credited))
            ,2)
             sales
         ,ROUND (
             (  NVL (
                   apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
                  ,0)
              * NVL (rctl.quantity_invoiced, rctl.quantity_credited))
            ,2)
             cost
         ,NVL (ol.unit_selling_price, 0) unit_selling_price
         ,NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id), 0)
             unit_cost
         ,                                   --NVL(Ol.Unit_Cost,0) Unit_Cost ,
          oh.order_number order_number
         ,DECODE (
             ol.option_number
            ,'', (ol.line_number || '.' || ol.shipment_number)
            , (   ol.line_number
               || '.'
               || ol.shipment_number
               || '.'
               || ol.option_number))
             order_line
         ,TRUNC (oh.ordered_date) ordered_date_cond
         , (TO_CHAR (oh.ordered_date, 'MM/DD/YYYY')) ordered_date
         ,cust_acct.customer_class_code
         ,                                                     ---Primary Keys
          oh.header_id
         ,ol.line_id
         ,ship_from_org.organization_id
         ,party.party_id
         ,cust_acct.cust_account_id
         ,msi.inventory_item_id
         ,msi.organization_id msi_organization_id
         --descr#flexfield#start
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_PARTY_TYPE'
                                           ,cust_acct.attribute1
                                           ,'I')
             cust_acct#party_type
         ,cust_acct.attribute2 cust_acct#vndr_code_and_frul
         ,cust_acct.attribute3 cust_acct#branch_description
         ,cust_acct.attribute4 cust_acct#customer_source
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_LEGAL_COLLECTION'
                                           ,cust_acct.attribute5
                                           ,'F')
             cust_acct#legal_collection_i
         ,cust_acct.attribute6 cust_acct#prism_number
         ,DECODE (cust_acct.attribute_category
                 ,'Yes', cust_acct.attribute16
                 ,NULL)
             cust_acct#yes#note_line_#5
         ,DECODE (cust_acct.attribute_category
                 ,'Yes', cust_acct.attribute17
                 ,NULL)
             cust_acct#yes#note_line_#1
         ,DECODE (cust_acct.attribute_category
                 ,'Yes', cust_acct.attribute18
                 ,NULL)
             cust_acct#yes#note_line_#2
         ,DECODE (cust_acct.attribute_category
                 ,'Yes', cust_acct.attribute19
                 ,NULL)
             cust_acct#yes#note_line_#3
         ,DECODE (cust_acct.attribute_category
                 ,'Yes', cust_acct.attribute20
                 ,NULL)
             cust_acct#yes#note_line_#4
         ,DECODE (
             party.attribute_category
            ,'101', xxeis.eis_rs_dff.decode_valueset ('XXCUS_PARTY_TYPE'
                                                     ,party.attribute1
                                                     ,'I')
            ,NULL)
             party#101#party_type
         ,DECODE (
             party.attribute_category
            ,'101', xxeis.eis_rs_dff.decode_valueset (
                       'XXCUSOZF_AR_COLLECTORS'
                      ,party.attribute3
                      ,'F')
            ,NULL)
             party#101#collector
         ,ship_from_org.attribute1 ship_from_org#factory_planne
         ,ship_from_org.attribute10 ship_from_org#fru
         ,ship_from_org.attribute11 ship_from_org#location_numbe
         ,xxeis.eis_rs_dff.decode_valueset ('HR_DE_EMPLOYEES'
                                           ,ship_from_org.attribute13
                                           ,'F')
             ship_from_org#branch_operati
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DELIVERY_CHARGE_EXEMPT'
                                           ,ship_from_org.attribute14
                                           ,'I')
             ship_from_org#deliver_charge
         ,ship_from_org.attribute2 ship_from_org#factory_planne1
         ,ship_from_org.attribute3 ship_from_org#factory_planne2
         ,ship_from_org.attribute4 ship_from_org#factory_planne3
         ,ship_from_org.attribute5 ship_from_org#factory_planne4
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_PRICE_ZONES'
                                           ,ship_from_org.attribute6
                                           ,'F')
             ship_from_org#pricing_zone
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_ORG_TYPE'
                                           ,ship_from_org.attribute7
                                           ,'I')
             ship_from_org#org_type
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DISTRICT'
                                           ,ship_from_org.attribute8
                                           ,'I')
             ship_from_org#district
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_REGION'
                                           ,ship_from_org.attribute9
                                           ,'I')
             ship_from_org#region
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL)
             msi#hds#lob
         ,DECODE (
             msi.attribute_category
            ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                     ,msi.attribute10
                                                     ,'F')
            ,NULL)
             msi#hds#drop_shipment_eligab
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL)
             msi#hds#invoice_uom
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL)
             msi#hds#product_id
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL)
             msi#hds#vendor_part_number
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL)
             msi#hds#unspsc_code
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL)
             msi#hds#upc_primary
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL)
             msi#hds#sku_description
         ,DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL)
             msi#wc#ca_prop_65
         ,DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL)
             msi#wc#country_of_origin
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                    ,msi.attribute11
                                                    ,'F')
            ,NULL)
             msi#wc#orm_d_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL)
             msi#wc#store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL)
             msi#wc#dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL)
             msi#wc#yearly_store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL)
             msi#wc#yearly_dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL)
             msi#wc#prism_part_number
         ,DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL)
             msi#wc#hazmat_description
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER'
                                                    ,msi.attribute18
                                                    ,'I')
            ,NULL)
             msi#wc#hazmat_container
         ,DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL)
             msi#wc#gtp_indicator
         ,DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL)
             msi#wc#last_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL)
             msi#wc#amu
         ,DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL)
             msi#wc#reserve_stock
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE'
                                                    ,msi.attribute22
                                                    ,'I')
            ,NULL)
             msi#wc#taxware_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL)
             msi#wc#average_units
         ,DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL)
             msi#wc#product_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL)
             msi#wc#import_duty_
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                    ,msi.attribute29
                                                    ,'F')
            ,NULL)
             msi#wc#keep_item_active
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No'
                                                    ,msi.attribute3
                                                    ,'F')
            ,NULL)
             msi#wc#pesticide_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL)
             msi#wc#calc_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL)
             msi#wc#voc_gl
         ,DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL)
             msi#wc#pesticide_flag_state
         ,DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL)
             msi#wc#voc_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL)
             msi#wc#voc_sub_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL)
             msi#wc#msds_#
         ,DECODE (
             msi.attribute_category
            ,'WC', xxeis.eis_rs_dff.decode_valueset (
                      'XXWC_HAZMAT_PACKAGE_GROUP'
                     ,msi.attribute9
                     ,'I')
            ,NULL)
             msi#wc#hazmat_packaging_grou
         ,xxeis.eis_rs_dff.decode_valueset ('RENTAL TYPE'
                                           ,oh.attribute1
                                           ,'I')
             oh#rental_term
         ,xxeis.eis_rs_dff.decode_valueset ('WC_CUS_ORDER_CHANNEL'
                                           ,oh.attribute2
                                           ,'I')
             oh#order_channel
         ,ol.attribute1 ol#estimated_return_date
         ,ol.attribute10 ol#rerent_po
         ,ol.attribute11 ol#force_ship
         ,ol.attribute12 ol#rental_date
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ol.attribute15, 'F')
             ol#item_on_blowout
         ,ol.attribute19 ol#pof_std_line
         ,xxeis.eis_rs_dff.decode_valueset ('ReRental Billing Type'
                                           ,ol.attribute2
                                           ,'I')
             ol#rerental_billing_terms
         ,ol.attribute20 ol#pricing_guardrail
         ,xxeis.eis_rs_dff.decode_valueset ('Yes_No', ol.attribute3, 'F')
             ol#print_expired_product_dis
         ,ol.attribute4 ol#rental_charge
         ,ol.attribute5 ol#vendor_quote_cost
         ,ol.attribute6 ol#po_cost_for_vendor_quote
         ,ol.attribute7 ol#serial_number
         ,ol.attribute8 ol#engineering_cost
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_CSP_APP_METHOD'
                                           ,ol.attribute9
                                           ,'I')
             ol#application_method
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org
         ,hz_parties party
         ,hz_cust_accounts cust_acct
         ,oe_order_headers oh
         ,oe_order_lines ol
         ,ra_customer_trx rct
         ,ra_customer_trx_lines rctl
         ,hr_all_organization_units hou
         ,ra_salesreps rep
         ,ra_cust_trx_line_gl_dist gd
         ,hz_cust_site_uses hzcs_ship_to
         ,hz_cust_acct_sites hcas_ship_to
         ,hz_party_sites hzps_ship_to
         ,hz_locations hzl_ship_to
         ,mtl_system_items_kfv msi
    WHERE     ol.sold_to_org_id = cust_acct.cust_account_id
          AND cust_acct.party_id = party.party_id
          AND ol.ship_from_org_id = ship_from_org.organization_id
          -- AND ol.link_to_line_id             = olr.line_id (+)
          AND ol.header_id = oh.header_id
          AND TO_CHAR (oh.order_number) =
                 TO_CHAR (rct.interface_header_attribute1)
          AND TO_CHAR (ol.line_id) = TO_CHAR (rctl.interface_line_attribute6)
          AND rctl.interface_line_context = 'ORDER ENTRY'
          AND rct.interface_header_context = 'ORDER ENTRY'
          AND rct.customer_trx_id = rctl.customer_trx_id
          AND msi.organization_id = hou.organization_id
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          AND msi.inventory_item_id = ol.inventory_item_id
          AND msi.organization_id = ol.ship_from_org_id
          AND rct.customer_trx_id = gd.customer_trx_id(+)
          AND 'REC' = NVL (gd.account_class, 'REC')
          AND 'Y' = NVL (gd.latest_rec_flag, 'Y')
          AND oh.ship_to_org_id = hzcs_ship_to.site_use_id(+)
          AND hzcs_ship_to.cust_acct_site_id =
                 hcas_ship_to.cust_acct_site_id(+)
          AND hcas_ship_to.party_site_id = hzps_ship_to.party_site_id(+)
          AND hzl_ship_to.location_id(+) = hzps_ship_to.location_id
          AND UPPER (cust_acct.attribute4) LIKE 'NAT%'
--AND oh.order_number ='10001010'
;


