CREATE OR REPLACE FORCE VIEW xxeis.hdsgltoarview
(
   je_batch_id
  ,batch_name
  ,batch_description
  ,je_header_id
  ,ledger_name
  ,party_name
  ,party_number
  ,customer_account_name
  ,customer_account_number
  ,je_source_name
  ,je_source
  ,je_category_name
  ,je_category
  ,je_period
  ,je_name
  ,je_description
  ,je_currency
  ,je_status
  ,je_type
  ,je_posted_date
  ,je_creation_date
  ,je_created_by
  ,je_balanced
  ,accrual_reversal
  ,je_multi_bal_seg
  ,accrual_rev_effective_date
  ,accrual_rev_period_name
  ,accrual_rev_status
  ,accrual_rev_je_header_id
  ,accrual_rev_change_sign
  ,reversed_je_header_id
  ,je_curr_conv_rate
  ,je_curr_conv_type
  ,je_curr_conv_date
  ,je_external_ref
  ,je_line_num
  ,je_effective_date
  ,je_entered_dr
  ,je_entered_cr
  ,je_accounted_dr
  ,je_accounted_cr
  ,je_line_description
  ,gl_code_combination_id
  ,gl_account_string
  ,sla_code_combination_id
  ,sla_account
  ,budget_name
  ,budget_type
  ,budget_version_id
  ,budget_status
  ,budget_description
  ,gl_sl_link_id
  ,gl_sl_link_table
  ,ae_line_num
  ,ae_header_id
  ,event_id
  ,sla_line_accounted_dr
  ,sla_line_accounted_cr
  ,sla_line_entered_dr
  ,sla_line_entered_cr
  ,sla_dist_accounted_dr
  ,sla_dist_accounted_cr
  ,sla_dist_entered_dr
  ,sla_dist_entered_cr
  ,sla_entered_net
  ,sla_accounted_net
  ,receipt_number
  ,receipt_date
  ,currency_code
  ,amount
  ,doc_sequence_value
  ,receipt_method
  ,event_type_code
  ,accounting_class_code
  ,accounting_date
  ,transfer_date_from_sla_to_gl
  ,description
  ,cash_receipt_id
  ,customer_trx_id
  ,trx_number
  ,trx_date
  ,invoice_currency_code
  ,gcc#branch
  ,gcc1#branch
  ,jl#branch
  ,jl#dept
  ,jl#account
  ,jl#sub_acct
  ,jl#pos_branch
  ,jl#wc_formula
  ,gcc1#50328#product
  ,gcc1#50328#product#descr
  ,gcc#50328#product
  ,gcc#50328#product#descr
  ,gcc1#50328#location
  ,gcc1#50328#location#descr
  ,gcc#50328#location
  ,gcc#50328#location#descr
  ,gcc1#50328#cost_center
  ,gcc1#50328#cost_center#descr
  ,gcc#50328#cost_center
  ,gcc#50328#cost_center#descr
  ,gcc1#50328#account
  ,gcc1#50328#account#descr
  ,gcc#50328#account
  ,gcc#50328#account#descr
  ,gcc1#50328#project_code
  ,gcc1#50328#project_code#descr
  ,gcc#50328#project_code
  ,gcc#50328#project_code#descr
  ,gcc1#50328#furture_use
  ,gcc1#50328#furture_use#descr
  ,gcc#50328#furture_use
  ,gcc#50328#furture_use#descr
  ,gcc1#50328#future_use_2
  ,gcc1#50328#future_use_2#descr
  ,gcc#50328#future_use_2
  ,gcc#50328#future_use_2#descr
  ,gcc#50368#account
  ,gcc#50368#account#descr
  ,gcc#50368#department
  ,gcc#50368#department#descr
  ,gcc#50368#division
  ,gcc#50368#division#descr
  ,gcc#50368#future_use
  ,gcc#50368#future_use#descr
  ,gcc#50368#product
  ,gcc#50368#product#descr
  ,gcc#50368#subaccount
  ,gcc#50368#subaccount#descr
  ,gcc1#50368#account
  ,gcc1#50368#account#descr
  ,gcc1#50368#department
  ,gcc1#50368#department#descr
  ,gcc1#50368#division
  ,gcc1#50368#division#descr
  ,gcc1#50368#future_use
  ,gcc1#50368#future_use#descr
  ,gcc1#50368#product
  ,gcc1#50368#product#descr
  ,gcc1#50368#subaccount
  ,gcc1#50368#subaccount#descr
)
AS
   SELECT jb.je_batch_id
         ,jb.name batch_name
         ,jb.description batch_description
         ,jh.je_header_id
         ,gle.name ledger_name
         ,party.party_name party_name
         ,party.party_number party_number
         ,cust.account_name customer_account_name
         ,cust.account_number customer_account_number
         ,jes.je_source_name je_source_name
         ,jes.user_je_source_name je_source
         ,jec.je_category_name je_category_name
         ,jec.user_je_category_name je_category
         ,jh.period_name je_period
         ,jh.name je_name
         ,jh.description je_description
         ,jh.currency_code je_currency
         ,DECODE (
             jh.status
            ,'P', 'Posted'
            ,'U', 'Unposted'
            ,'F', 'Error7 - Showing invalid journal entry lines or no journal entry lines'
            ,'K', 'Error10 - Showing unbalanced intercompany journal entry'
            ,'Z', 'Error7 - Showing invalid journal entry lines or no journal entry lines'
            ,'Unknown')
             je_status
         ,DECODE (jh.actual_flag
                 ,'A', 'Actual'
                 ,'B', 'Budget'
                 ,'E', 'Encumbrance')
             je_type
         ,jh.posted_date je_posted_date
         ,jh.creation_date je_creation_date
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_user_name (jh.created_by)
             je_created_by
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.balanced_je_flag)
             je_balanced
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_flag)
             accrual_reversal
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.multi_bal_seg_flag)
             je_multi_bal_seg
         ,jh.accrual_rev_effective_date
         ,jh.accrual_rev_period_name
         ,jh.accrual_rev_status
         ,jh.accrual_rev_je_header_id
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_change_sign_flag)
             accrual_rev_change_sign
         ,jh.reversed_je_header_id
         ,jh.currency_conversion_rate je_curr_conv_rate
         ,jh.currency_conversion_type je_curr_conv_type
         ,jh.currency_conversion_date je_curr_conv_date
         ,jh.external_reference je_external_ref
         ,jl.je_line_num je_line_num
         ,jl.effective_date je_effective_date
         ,jl.entered_dr je_entered_dr
         ,jl.entered_cr je_entered_cr
         ,jl.accounted_dr je_accounted_dr
         ,jl.accounted_cr je_accounted_cr
         ,jl.description je_line_description
         ,gcc.code_combination_id gl_code_combination_id
         ,gcc.concatenated_segments gl_account_string
         ,gcc1.code_combination_id sla_code_combination_id
         ,gcc1.concatenated_segments sla_account
         ,gbv.budget_name
         ,gbv.budget_type
         ,gbv.budget_version_id
         ,DECODE (gbv.status,  'O', 'Open',  'C', 'Closed',  'F', 'Future')
             budget_status
         ,gbv.description budget_description
         ,jir.gl_sl_link_id
         ,jir.gl_sl_link_table
         ,ael.ae_line_num
         ,ael.ae_header_id
         ,aeh.event_id
         ,ael.accounted_dr sla_line_accounted_dr
         ,ael.accounted_cr sla_line_accounted_cr
         ,ael.entered_dr sla_line_entered_dr
         ,ael.entered_cr sla_line_entered_cr
         ,DECODE (xdl.application_id
                 ,NULL, ael.accounted_dr
                 ,xdl.unrounded_accounted_dr)
             sla_dist_accounted_dr
         ,DECODE (xdl.application_id
                 ,NULL, ael.accounted_cr
                 ,xdl.unrounded_accounted_cr)
             sla_dist_accounted_cr
         ,DECODE (xdl.application_id
                 ,NULL, ael.entered_dr
                 ,xdl.unrounded_entered_dr)
             sla_dist_entered_dr
         ,DECODE (xdl.application_id
                 ,NULL, ael.entered_cr
                 ,xdl.unrounded_entered_cr)
             sla_dist_entered_cr
         , (  NVL (
                 DECODE (xdl.application_id
                        ,NULL, ael.entered_dr
                        ,xdl.unrounded_entered_dr)
                ,0)
            - NVL (
                 DECODE (xdl.application_id
                        ,NULL, ael.entered_cr
                        ,xdl.unrounded_entered_cr)
                ,0))
             sla_entered_net
         , (  NVL (
                 DECODE (xdl.application_id
                        ,NULL, ael.accounted_dr
                        ,xdl.unrounded_accounted_dr)
                ,0)
            - NVL (
                 DECODE (xdl.application_id
                        ,NULL, ael.accounted_cr
                        ,xdl.unrounded_accounted_cr)
                ,0))
             sla_accounted_net
         ,acr.receipt_number
         ,acr.receipt_date
         ,acr.currency_code
         ,acr.amount
         ,acr.doc_sequence_value
         ,arm.name receipt_method
         ,aeh.event_type_code
         ,ael.accounting_class_code
         ,aeh.accounting_date
         ,aeh.gl_transfer_date transfer_date_from_sla_to_gl
         ,aeh.description
         ,acr.cash_receipt_id
         ,NULL customer_trx_id
         ,NULL trx_number
         ,NULL trx_date
         ,NULL invoice_currency_code
         --descr#flexfield#start

         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I')
             gcc#branch
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU'
                                           ,gcc1.attribute1
                                           ,'I')
             gcc1#branch
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', jl.attribute1, 'I')
             jl#branch
         ,jl.attribute2 jl#dept
         ,jl.attribute3 jl#account
         ,jl.attribute4 jl#sub_acct
         ,jl.attribute5 jl#pos_branch
         ,jl.attribute6 jl#wc_formula
         --descr#flexfield#end

         --gl#accountff#start

         ,gcc1.segment1 gcc1#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc1#50328#product#descr
         ,gcc.segment1 gcc#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc#50328#product#descr
         ,gcc1.segment2 gcc1#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc1#50328#location#descr
         ,gcc.segment2 gcc#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc#50328#location#descr
         ,gcc1.segment3 gcc1#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc1#50328#cost_center#descr
         ,gcc.segment3 gcc#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc#50328#cost_center#descr
         ,gcc1.segment4 gcc1#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc1#50328#account#descr
         ,gcc.segment4 gcc#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc#50328#account#descr
         ,gcc1.segment5 gcc1#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc1#50328#project_code#descr
         ,gcc.segment5 gcc#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc#50328#project_code#descr
         ,gcc1.segment6 gcc1#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc1#50328#furture_use#descr
         ,gcc.segment6 gcc#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc#50328#furture_use#descr
         ,gcc1.segment7 gcc1#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc1#50328#future_use_2#descr
         ,gcc.segment7 gcc#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc#50328#future_use_2#descr
         ,gcc.segment4 gcc#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ LTMR _ACCOUNT')
             gcc#50368#account#descr
         ,gcc.segment3 gcc#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc#50368#department#descr
         ,gcc.segment2 gcc#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_ LTMR _DIVISION')
             gcc#50368#division#descr
         ,gcc.segment6 gcc#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc#50368#future_use#descr
         ,gcc.segment1 gcc#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_LTMR_PRODUCT')
             gcc#50368#product#descr
         ,gcc.segment5 gcc#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc#50368#subaccount#descr
         ,gcc1.segment4 gcc1#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4
                                               ,'XXCUS_GL_ LTMR _ACCOUNT')
             gcc1#50368#account#descr
         ,gcc1.segment3 gcc1#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3
                                               ,'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc1#50368#department#descr
         ,gcc1.segment2 gcc1#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2
                                               ,'XXCUS_GL_ LTMR _DIVISION')
             gcc1#50368#division#descr
         ,gcc1.segment6 gcc1#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6
                                               ,'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc1#50368#future_use#descr
         ,gcc1.segment1 gcc1#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1
                                               ,'XXCUS_GL_LTMR_PRODUCT')
             gcc1#50368#product#descr
         ,gcc1.segment5 gcc1#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5
                                               ,'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc1#50368#subaccount#descr
     --gl#accountff#end


     FROM /*
          we are not able to show the ar_distributions_all amount.. all the line_id's in ar distribution table not there in xla_distribution_links table
          */
         gl_ledgers gle
         ,gl_je_batches jb
         ,gl_je_headers jh
         ,gl_je_lines jl
         ,gl_code_combinations_kfv gcc
         ,gl_code_combinations_kfv gcc1
         ,gl_je_sources jes
         ,gl_je_categories jec
         ,gl_budget_versions gbv
         ,gl_import_references jir
         ,xla_ae_headers aeh
         ,xla_ae_lines ael
         ,xla_distribution_links xdl
         ,ar_distributions_all ada
         ,ar_cash_receipt_history_all acrh
         ,ar_cash_receipts_all acr
         ,ar_receipt_methods arm
         ,hz_cust_accounts cust
         ,hz_parties party
    WHERE     jh.ledger_id = gle.ledger_id
          AND jb.je_batch_id = jh.je_batch_id
          AND jh.je_header_id = jl.je_header_id
          AND jl.code_combination_id = gcc.code_combination_id
          AND ael.code_combination_id = gcc1.code_combination_id(+)
          AND jh.je_source = jes.je_source_name
          AND jh.je_category = jec.je_category_name
          AND jh.budget_version_id = gbv.budget_version_id(+)
          AND jb.je_batch_id = jir.je_batch_id
          AND jh.je_header_id = jir.je_header_id
          AND jl.je_line_num = jir.je_line_num
          AND jh.je_source = 'Receivables'
          AND jh.je_category IN ('Receipts', 'Trade Receipts')
          AND jir.gl_sl_link_id = ael.gl_sl_link_id(+)
          AND jir.gl_sl_link_table = ael.gl_sl_link_table(+)
          AND ael.ae_header_id = aeh.ae_header_id(+)
          AND ael.ae_header_id = xdl.ae_header_id(+)
          AND ael.ae_line_num = xdl.ae_line_num(+)
          AND xdl.source_distribution_type(+) = 'AR_DISTRIBUTIONS_ALL'
          AND xdl.source_distribution_id_num_1 = ada.line_id(+)
          AND ada.source_table = 'CRH'
          AND ada.source_id = acrh.cash_receipt_history_id(+)
          AND acrh.cash_receipt_id = acr.cash_receipt_id(+)
          AND acr.receipt_method_id = arm.receipt_method_id(+)
          AND acr.pay_from_customer = cust.cust_account_id(+)
          AND cust.party_id = party.party_id(+)
          AND gl_security_pkg.validate_access (gle.ledger_id) = 'TRUE'
          AND xxeis.eis_gl_security_pkg.validate_access (
                 jh.ledger_id
                ,gcc.code_combination_id) = 'TRUE'
   UNION ALL
   SELECT jb.je_batch_id
         ,jb.name batch_name
         ,jb.description batch_description
         ,jh.je_header_id
         ,gle.name ledger_name
         ,party.party_name party_name
         ,party.party_number party_number
         ,cust.account_name customer_account_name
         ,cust.account_number customer_account_number
         ,jes.je_source_name je_source_name
         ,jes.user_je_source_name je_source
         ,jec.je_category_name je_category_name
         ,jec.user_je_category_name je_category
         ,jh.period_name je_period
         ,jh.name je_name
         ,jh.description je_description
         ,jh.currency_code je_currency
         ,DECODE (
             jh.status
            ,'P', 'Posted'
            ,'U', 'Unposted'
            ,'F', 'Error7 - Showing invalid journal entry lines or no journal entry lines'
            ,'K', 'Error10 - Showing unbalanced intercompany journal entry'
            ,'Z', 'Error7 - Showing invalid journal entry lines or no journal entry lines'
            ,'Unknown')
             je_status
         ,DECODE (jh.actual_flag
                 ,'A', 'Actual'
                 ,'B', 'Budget'
                 ,'E', 'Encumbrance')
             je_type
         ,jh.posted_date je_posted_date
         ,jh.creation_date je_creation_date
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_user_name (jh.created_by)
             je_created_by
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.balanced_je_flag)
             je_balanced
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_flag)
             accrual_reversal
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.multi_bal_seg_flag)
             je_multi_bal_seg
         ,jh.accrual_rev_effective_date
         ,jh.accrual_rev_period_name
         ,jh.accrual_rev_status
         ,jh.accrual_rev_je_header_id
         ,xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_change_sign_flag)
             accrual_rev_change_sign
         ,jh.reversed_je_header_id
         ,jh.currency_conversion_rate je_curr_conv_rate
         ,jh.currency_conversion_type je_curr_conv_type
         ,jh.currency_conversion_date je_curr_conv_date
         ,jh.external_reference je_external_ref
         ,jl.je_line_num je_line_num
         ,jl.effective_date je_effective_date
         ,jl.entered_dr je_entered_dr
         ,jl.entered_cr je_entered_cr
         ,jl.accounted_dr je_accounted_dr
         ,jl.accounted_cr je_accounted_cr
         ,jl.description je_line_description
         ,gcc.code_combination_id gl_code_combination_id
         ,gcc.concatenated_segments gl_account_string
         ,gcc1.code_combination_id sla_code_combination_id
         ,gcc1.concatenated_segments sla_account
         ,gbv.budget_name
         ,gbv.budget_type
         ,gbv.budget_version_id
         ,DECODE (gbv.status,  'O', 'Open',  'C', 'Closed',  'F', 'Future')
             budget_status
         ,gbv.description budget_description
         ,jir.gl_sl_link_id
         ,jir.gl_sl_link_table
         ,ael.ae_line_num
         ,ael.ae_header_id
         ,aeh.event_id
         ,ael.accounted_dr sla_line_accounted_dr
         ,ael.accounted_cr sla_line_accounted_cr
         ,ael.entered_dr sla_line_entered_dr
         ,ael.entered_cr sla_line_entered_cr
         ,DECODE (xdl.application_id
                 ,NULL, ael.accounted_dr
                 ,xdl.unrounded_accounted_dr)
             sla_dist_accounted_dr
         ,DECODE (xdl.application_id
                 ,NULL, ael.accounted_cr
                 ,xdl.unrounded_accounted_cr)
             sla_dist_accounted_cr
         ,DECODE (xdl.application_id
                 ,NULL, ael.entered_dr
                 ,xdl.unrounded_entered_dr)
             sla_dist_entered_dr
         ,DECODE (xdl.application_id
                 ,NULL, ael.entered_cr
                 ,xdl.unrounded_entered_cr)
             sla_dist_entered_cr
         , (  NVL (
                 DECODE (xdl.application_id
                        ,NULL, ael.entered_dr
                        ,xdl.unrounded_entered_dr)
                ,0)
            - NVL (
                 DECODE (xdl.application_id
                        ,NULL, ael.entered_cr
                        ,xdl.unrounded_entered_cr)
                ,0))
             sla_entered_net
         , (  NVL (
                 DECODE (xdl.application_id
                        ,NULL, ael.accounted_dr
                        ,xdl.unrounded_accounted_dr)
                ,0)
            - NVL (
                 DECODE (xdl.application_id
                        ,NULL, ael.accounted_cr
                        ,xdl.unrounded_accounted_cr)
                ,0))
             sla_accounted_net
         ,acr.receipt_number
         ,acr.receipt_date
         ,acr.currency_code
         ,acr.amount
         ,acr.doc_sequence_value
         ,arm.name receipt_method
         ,aeh.event_type_code
         ,ael.accounting_class_code
         ,aeh.accounting_date
         ,aeh.gl_transfer_date transfer_date_from_sla_to_gl
         ,aeh.description
         ,acr.cash_receipt_id
         ,trx.customer_trx_id customer_trx_id
         ,trx.trx_number trx_number
         ,trx.trx_date trx_date
         ,trx.invoice_currency_code invoice_currency_code
         --descr#flexfield#start

         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', gcc.attribute1, 'I')
             gcc#branch
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU'
                                           ,gcc1.attribute1
                                           ,'I')
             gcc1#branch
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', jl.attribute1, 'I')
             jl#branch
         ,jl.attribute2 jl#dept
         ,jl.attribute3 jl#account
         ,jl.attribute4 jl#sub_acct
         ,jl.attribute5 jl#pos_branch
         ,jl.attribute6 jl#wc_formula
         --descr#flexfield#end


         --gl#accountff#start

         ,gcc1.segment1 gcc1#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc1#50328#product#descr
         ,gcc.segment1 gcc#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc#50328#product#descr
         ,gcc1.segment2 gcc1#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc1#50328#location#descr
         ,gcc.segment2 gcc#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc#50328#location#descr
         ,gcc1.segment3 gcc1#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc1#50328#cost_center#descr
         ,gcc.segment3 gcc#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc#50328#cost_center#descr
         ,gcc1.segment4 gcc1#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc1#50328#account#descr
         ,gcc.segment4 gcc#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc#50328#account#descr
         ,gcc1.segment5 gcc1#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc1#50328#project_code#descr
         ,gcc.segment5 gcc#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc#50328#project_code#descr
         ,gcc1.segment6 gcc1#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc1#50328#furture_use#descr
         ,gcc.segment6 gcc#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc#50328#furture_use#descr
         ,gcc1.segment7 gcc1#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc1#50328#future_use_2#descr
         ,gcc.segment7 gcc#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc#50328#future_use_2#descr
         ,gcc.segment4 gcc#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment4
                                               ,'XXCUS_GL_ LTMR _ACCOUNT')
             gcc#50368#account#descr
         ,gcc.segment3 gcc#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment3
                                               ,'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc#50368#department#descr
         ,gcc.segment2 gcc#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment2
                                               ,'XXCUS_GL_ LTMR _DIVISION')
             gcc#50368#division#descr
         ,gcc.segment6 gcc#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment6
                                               ,'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc#50368#future_use#descr
         ,gcc.segment1 gcc#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment1
                                               ,'XXCUS_GL_LTMR_PRODUCT')
             gcc#50368#product#descr
         ,gcc.segment5 gcc#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc.segment5
                                               ,'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc#50368#subaccount#descr
         ,gcc1.segment4 gcc1#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4
                                               ,'XXCUS_GL_ LTMR _ACCOUNT')
             gcc1#50368#account#descr
         ,gcc1.segment3 gcc1#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3
                                               ,'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc1#50368#department#descr
         ,gcc1.segment2 gcc1#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2
                                               ,'XXCUS_GL_ LTMR _DIVISION')
             gcc1#50368#division#descr
         ,gcc1.segment6 gcc1#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6
                                               ,'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc1#50368#future_use#descr
         ,gcc1.segment1 gcc1#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1
                                               ,'XXCUS_GL_LTMR_PRODUCT')
             gcc1#50368#product#descr
         ,gcc1.segment5 gcc1#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5
                                               ,'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc1#50368#subaccount#descr
     --gl#accountff#end



     FROM /*
          we are not able to show the ar_distributions_all amount.. all the line_id's in ar distribution table not there in xla_distribution_links table
          */
         gl_ledgers gle
         ,gl_je_batches jb
         ,gl_je_headers jh
         ,gl_je_lines jl
         ,gl_code_combinations_kfv gcc
         ,gl_code_combinations_kfv gcc1
         ,gl_je_sources jes
         ,gl_je_categories jec
         ,gl_budget_versions gbv
         ,gl_import_references jir
         ,xla_ae_headers aeh
         ,xla_ae_lines ael
         ,xla_distribution_links xdl
         ,ar_distributions_all ada
         ,ar_receivable_applications_all ara
         ,ar_cash_receipts_all acr
         ,ar_receipt_methods arm
         ,hz_cust_accounts cust
         ,hz_parties party
         ,ra_customer_trx_all trx
    WHERE     jh.ledger_id = gle.ledger_id
          AND jb.je_batch_id = jh.je_batch_id
          AND jh.je_header_id = jl.je_header_id
          AND jl.code_combination_id = gcc.code_combination_id
          AND ael.code_combination_id = gcc1.code_combination_id(+)
          AND jh.je_source = jes.je_source_name
          AND jh.je_category = jec.je_category_name
          AND jh.budget_version_id = gbv.budget_version_id(+)
          AND jb.je_batch_id = jir.je_batch_id
          AND jh.je_header_id = jir.je_header_id
          AND jl.je_line_num = jir.je_line_num
          AND jh.je_source = 'Receivables'
          AND jh.je_category IN ('Receipts', 'Trade Receipts')
          AND jir.gl_sl_link_id = ael.gl_sl_link_id(+)
          AND jir.gl_sl_link_table = ael.gl_sl_link_table(+)
          AND ael.ae_header_id = aeh.ae_header_id(+)
          AND ael.ae_header_id = xdl.ae_header_id(+)
          AND ael.ae_line_num = xdl.ae_line_num(+)
          AND xdl.source_distribution_type(+) = 'AR_DISTRIBUTIONS_ALL'
          AND xdl.source_distribution_id_num_1 = ada.line_id(+)
          AND ada.source_table = 'RA'
          AND ada.source_id = ara.receivable_application_id(+)
          AND ara.cash_receipt_id = acr.cash_receipt_id(+)
          AND acr.receipt_method_id = arm.receipt_method_id(+)
          AND acr.pay_from_customer = cust.cust_account_id(+)
          AND cust.party_id = party.party_id(+)
          AND ara.applied_customer_trx_id = trx.customer_trx_id(+)
          AND gl_security_pkg.validate_access (gle.ledger_id) = 'TRUE'
          AND xxeis.eis_gl_security_pkg.validate_access (
                 jh.ledger_id
                ,gcc.code_combination_id) = 'TRUE';


