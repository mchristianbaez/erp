CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_back_order_inv_v
(
   order_number
  ,action
  ,line_number
  ,ordered_item
  ,item_description
  ,quantity
  ,ordered_date
  ,sub_inv
  ,created_by
  ,last_updated_by
  ,warehouse
  ,header_id
  ,line_id
  ,inventory_item_id
  ,organization_id
  ,delivery_detail_id
)
AS
   SELECT oh.order_number order_number
         , /*CASE WHEN oh.flow_status_code  <>'ENTERED' AND ol.flow_status_code ='CANCELLED'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Full Cancellation / Hold Reserve'
           WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code ='CANCELLED'
           AND ol.subinventory NOT IN('RENTAL','GENERAL')
           THEN 'Full Cancellation'
           WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Partial Cancellation / Hold Reserve'
           WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory NOT IN('RENTAL','GENERAL')
           THEN 'Partial Cancellation'
           WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code <>'AWAITING_SHIPPING'
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Hold Reserve'
           END action,*/
          'Full Cancellation / Hold Reserve' action
         ,DECODE (
             ol.option_number
            ,'', (ol.line_number || '.' || ol.shipment_number)
            , (   ol.line_number
               || '.'
               || ol.shipment_number
               || '.'
               || ol.option_number))
             line_number
         ,msi.segment1 ordered_item
         ,msi.description item_description
         ,ol.cancelled_quantity quantity
         ,oh.ordered_date ordered_date
         ,ol.subinventory sub_inv
         ,                                         --hre.full_name created_by,
          NVL (ppf.full_name, fu.user_name) created_by
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_last_updt_by (ol.line_id)
             last_updated_by
         ,ood.organization_code warehouse
         ,                                                      --Primary Keys
          oh.header_id
         ,ol.line_id
         ,msi.inventory_item_id
         ,msi.organization_id
         ,wdd.delivery_detail_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,mtl_system_items_kfv msi
         ,                                                 --hr_employees hre,
          wsh_delivery_details wdd
         ,oe_order_lines_history olh
         ,org_organization_definitions ood
         ,fnd_user fu
         ,per_people_f ppf
    WHERE     oh.header_id = ol.header_id
          AND ol.inventory_item_id = msi.inventory_item_id
          AND ol.ship_from_org_id = msi.organization_id
          --AND oh.created_by           = hre.employee_id(+)
          AND fu.user_id = oh.created_by
          AND fu.employee_id = ppf.person_id(+)
          AND TRUNC (oh.ordered_date) BETWEEN NVL (ppf.effective_start_date
                                                  ,TRUNC (oh.ordered_date))
                                          AND NVL (ppf.effective_end_date
                                                  ,TRUNC (oh.ordered_date))
          AND ol.header_id = wdd.source_header_id(+)
          AND ol.line_id = wdd.source_line_id(+)
          -- AND wdd.released_status     = 'B'
          AND ol.header_id = olh.header_id(+)
          AND ol.line_id = olh.line_id(+)
          AND ood.organization_id = msi.organization_id
          AND oh.flow_status_code <> 'ENTERED'
          AND ol.flow_status_code = 'CANCELLED'
          AND UPPER (ol.subinventory) IN ('RENTALHLD', 'GENERALHLD')
          AND TRUNC (ol.last_update_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                                              AND xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
   --and ol.last_update_date <=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM
   --and ol.last_update_date >=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
   UNION
   SELECT oh.order_number order_number
         , /*CASE WHEN oh.flow_status_code  <>'ENTERED' AND ol.flow_status_code ='CANCELLED'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Full Cancellation / Hold Reserve'
           WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code ='CANCELLED'
           AND ol.subinventory NOT IN('RENTAL','GENERAL')
           THEN 'Full Cancellation'
           WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Partial Cancellation / Hold Reserve'
           WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory NOT IN('RENTAL','GENERAL')
           THEN 'Partial Cancellation'
           WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code <>'AWAITING_SHIPPING'
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Hold Reserve'
           END action,*/
          'Full Cancellation' action
         ,DECODE (
             ol.option_number
            ,'', (ol.line_number || '.' || ol.shipment_number)
            , (   ol.line_number
               || '.'
               || ol.shipment_number
               || '.'
               || ol.option_number))
             line_number
         ,msi.segment1 ordered_item
         ,msi.description item_description
         ,ol.cancelled_quantity quantity
         ,oh.ordered_date ordered_date
         ,ol.subinventory sub_inv
         ,                                         --hre.full_name created_by,
          NVL (ppf.full_name, fu.user_name) created_by
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_last_updt_by (ol.line_id)
             last_updated_by
         ,ood.organization_code warehouse
         ,                                                      --Primary Keys
          oh.header_id
         ,ol.line_id
         ,msi.inventory_item_id
         ,msi.organization_id
         ,wdd.delivery_detail_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,mtl_system_items_kfv msi
         ,                                                 --hr_employees hre,
          wsh_delivery_details wdd
         ,oe_order_lines_history olh
         ,org_organization_definitions ood
         ,fnd_user fu
         ,per_people_f ppf
    WHERE     oh.header_id = ol.header_id
          AND ol.inventory_item_id = msi.inventory_item_id
          AND ol.ship_from_org_id = msi.organization_id
          --AND oh.created_by               = hre.employee_id(+)
          AND fu.user_id = oh.created_by
          AND fu.employee_id = ppf.person_id(+)
          AND TRUNC (oh.ordered_date) BETWEEN NVL (ppf.effective_start_date
                                                  ,TRUNC (oh.ordered_date))
                                          AND NVL (ppf.effective_end_date
                                                  ,TRUNC (oh.ordered_date))
          AND ol.header_id = wdd.source_header_id(+)
          AND ol.line_id = wdd.source_line_id(+)
          --  AND wdd.released_status         = 'B'
          AND ol.header_id = olh.header_id(+)
          AND ol.line_id = olh.line_id(+)
          AND ood.organization_id = msi.organization_id
          AND oh.flow_status_code <> 'ENTERED'
          AND ol.flow_status_code = 'CANCELLED'
          AND UPPER (ol.subinventory) IN ('RENTAL', 'GENERAL')
          AND TRUNC (ol.last_update_date) BETWEEN eis_rs_xxwc_com_util_pkg.get_date_from
                                              AND eis_rs_xxwc_com_util_pkg.get_date_to
   --and ol.last_update_date <=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM
   --and OL.LAST_UPDATE_DATE >=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
   UNION
   SELECT oh.order_number order_number
         , /*CASE WHEN oh.flow_status_code  <>'ENTERED' AND ol.flow_status_code ='CANCELLED'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Full Cancellation / Hold Reserve'
           WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code ='CANCELLED'
           AND ol.subinventory NOT IN('RENTAL','GENERAL')
           THEN 'Full Cancellation'
           WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Partial Cancellation / Hold Reserve'
           WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory NOT IN('RENTAL','GENERAL')
           THEN 'Partial Cancellation'
           WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code <>'AWAITING_SHIPPING'
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Hold Reserve'
           END action,*/
          'Partial Cancellation / Hold Reserve' action
         ,DECODE (
             ol.option_number
            ,'', (ol.line_number || '.' || ol.shipment_number)
            , (   ol.line_number
               || '.'
               || ol.shipment_number
               || '.'
               || ol.option_number))
             line_number
         ,msi.segment1 ordered_item
         ,msi.description item_description
         ,ol.cancelled_quantity quantity
         ,oh.ordered_date ordered_date
         ,ol.subinventory sub_inv
         ,                                         --hre.full_name created_by,
          NVL (ppf.full_name, fu.user_name) created_by
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_last_updt_by (ol.line_id)
             last_updated_by
         ,ood.organization_code warehouse
         ,                                                      --Primary Keys
          oh.header_id
         ,ol.line_id
         ,msi.inventory_item_id
         ,msi.organization_id
         ,wdd.delivery_detail_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,mtl_system_items_kfv msi
         ,                                                 --hr_employees hre,
          wsh_delivery_details wdd
         ,oe_order_lines_history olh
         ,org_organization_definitions ood
         ,fnd_user fu
         ,per_people_f ppf
    WHERE     oh.header_id = ol.header_id
          AND ol.inventory_item_id = msi.inventory_item_id
          AND ol.ship_from_org_id = msi.organization_id
          --AND oh.created_by           = hre.employee_id(+)
          AND fu.user_id = oh.created_by
          AND fu.employee_id = ppf.person_id(+)
          AND TRUNC (oh.ordered_date) BETWEEN NVL (ppf.effective_start_date
                                                  ,TRUNC (oh.ordered_date))
                                          AND NVL (ppf.effective_end_date
                                                  ,TRUNC (oh.ordered_date))
          AND ol.header_id = wdd.source_header_id(+)
          AND ol.line_id = wdd.source_line_id(+)
          -- AND wdd.released_status     = 'B'
          AND ol.header_id = olh.header_id(+)
          AND ol.line_id = olh.line_id(+)
          AND ood.organization_id = msi.organization_id
          AND oh.flow_status_code <> 'ENTERED'
          AND (ol.cancelled_quantity <> 0 AND ol.ordered_quantity <> 0)
          AND olh.hist_type_code = 'CANCELLATION'
          AND TRUNC (olh.hist_creation_date) BETWEEN eis_rs_xxwc_com_util_pkg.get_date_from
                                                 AND eis_rs_xxwc_com_util_pkg.get_date_to
          --And Olh.Hist_Creation_Date <=Eis_Rs_Xxwc_Com_Util_Pkg.Get_Date_From
          --and OLH.HIST_CREATION_DATE >=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
          AND UPPER (ol.subinventory) IN ('RENTALHLD', 'GENERALHLD')
   UNION
   SELECT oh.order_number order_number
         , /*CASE WHEN oh.flow_status_code  <>'ENTERED' AND ol.flow_status_code ='CANCELLED'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Full Cancellation / Hold Reserve'
           WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code ='CANCELLED'
           AND ol.subinventory NOT IN('RENTAL','GENERAL')
           THEN 'Full Cancellation'
           WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Partial Cancellation / Hold Reserve'
           WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory NOT IN('RENTAL','GENERAL')
           THEN 'Partial Cancellation'
           WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code <>'AWAITING_SHIPPING'
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Hold Reserve'
           END action,*/
          'Partial Cancellation' action
         ,DECODE (
             ol.option_number
            ,'', (ol.line_number || '.' || ol.shipment_number)
            , (   ol.line_number
               || '.'
               || ol.shipment_number
               || '.'
               || ol.option_number))
             line_number
         ,msi.segment1 ordered_item
         ,msi.description item_description
         ,ol.cancelled_quantity quantity
         ,oh.ordered_date ordered_date
         ,ol.subinventory sub_inv
         ,                                         --hre.full_name created_by,
          NVL (ppf.full_name, fu.user_name) created_by
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_last_updt_by (ol.line_id)
             last_updated_by
         ,ood.organization_code warehouse
         ,                                                      --Primary Keys
          oh.header_id
         ,ol.line_id
         ,msi.inventory_item_id
         ,msi.organization_id
         ,wdd.delivery_detail_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,mtl_system_items_kfv msi
         ,                                                 --hr_employees hre,
          wsh_delivery_details wdd
         ,oe_order_lines_history olh
         ,org_organization_definitions ood
         ,fnd_user fu
         ,per_people_f ppf
    WHERE     oh.header_id = ol.header_id
          AND ol.inventory_item_id = msi.inventory_item_id
          AND ol.ship_from_org_id = msi.organization_id
          --AND oh.created_by           = hre.employee_id(+)
          AND fu.user_id = oh.created_by
          AND fu.employee_id = ppf.person_id(+)
          AND TRUNC (oh.ordered_date) BETWEEN NVL (ppf.effective_start_date
                                                  ,TRUNC (oh.ordered_date))
                                          AND NVL (ppf.effective_end_date
                                                  ,TRUNC (oh.ordered_date))
          AND ol.header_id = wdd.source_header_id(+)
          AND ol.line_id = wdd.source_line_id(+)
          -- AND wdd.released_status     = 'B'
          AND ol.header_id = olh.header_id(+)
          AND ol.line_id = olh.line_id(+)
          AND ood.organization_id = msi.organization_id
          AND oh.flow_status_code <> 'ENTERED'
          AND (ol.cancelled_quantity <> 0 AND ol.ordered_quantity <> 0)
          AND olh.hist_type_code = 'CANCELLATION'
          AND TRUNC (olh.hist_creation_date) BETWEEN eis_rs_xxwc_com_util_pkg.get_date_from
                                                 AND eis_rs_xxwc_com_util_pkg.get_date_to
          --and olh.HIST_CREATION_DATE <=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM
          --and OLH.HIST_CREATION_DATE >=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
          AND UPPER (ol.subinventory) NOT IN ('RENTAL', 'GENERAL')
   UNION
   SELECT oh.order_number order_number
         , /*CASE WHEN oh.flow_status_code  <>'ENTERED' AND ol.flow_status_code ='CANCELLED'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Full Cancellation / Hold Reserve'
           WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code ='CANCELLED'
           AND ol.subinventory NOT IN('RENTAL','GENERAL')
           THEN 'Full Cancellation'
           WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Partial Cancellation / Hold Reserve'
           WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory NOT IN('RENTAL','GENERAL')
           THEN 'Partial Cancellation'
           WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code <>'AWAITING_SHIPPING'
           AND olh.hist_type_code = 'CANCELLATION'
           AND ol.subinventory IN('RENTAL','GENERAL')
           THEN 'Hold Reserve'
           END action,*/
          'Hold Reserve' action
         ,DECODE (
             ol.option_number
            ,'', (ol.line_number || '.' || ol.shipment_number)
            , (   ol.line_number
               || '.'
               || ol.shipment_number
               || '.'
               || ol.option_number))
             line_number
         ,msi.segment1 ordered_item
         ,msi.description item_description
         ,ol.ordered_quantity quantity
         ,oh.ordered_date ordered_date
         ,ol.subinventory sub_inv
         ,                                         --hre.full_name created_by,
          NVL (ppf.full_name, fu.user_name) created_by
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_last_updt_by (ol.line_id)
             last_updated_by
         ,ood.organization_code warehouse
         ,                                                      --Primary Keys
          oh.header_id
         ,ol.line_id
         ,msi.inventory_item_id
         ,msi.organization_id
         ,wdd.delivery_detail_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,mtl_system_items_kfv msi
         ,                                                 --hr_employees hre,
          wsh_delivery_details wdd
         ,oe_order_lines_history olh
         ,org_organization_definitions ood
         ,fnd_user fu
         ,per_people_f ppf
    WHERE     oh.header_id = ol.header_id
          AND ol.inventory_item_id = msi.inventory_item_id
          AND ol.ship_from_org_id = msi.organization_id
          --AND oh.created_by           = hre.employee_id(+)
          AND fu.user_id = oh.created_by
          AND fu.employee_id = ppf.person_id(+)
          AND TRUNC (oh.ordered_date) BETWEEN NVL (ppf.effective_start_date
                                                  ,TRUNC (oh.ordered_date))
                                          AND NVL (ppf.effective_end_date
                                                  ,TRUNC (oh.ordered_date))
          AND ol.header_id = wdd.source_header_id(+)
          AND ol.line_id = wdd.source_line_id(+)
          --AND wdd.released_status     = 'B'
          AND ol.header_id = olh.header_id(+)
          AND ol.line_id = olh.line_id(+)
          AND ood.organization_id = msi.organization_id
          AND oh.flow_status_code <> 'ENTERED'
          AND ol.cancelled_quantity = 0
          AND ol.flow_status_code = 'AWAITING_SHIPPING'
          AND UPPER (ol.subinventory) IN ('RENTALHLD', 'GENERALHLD');


