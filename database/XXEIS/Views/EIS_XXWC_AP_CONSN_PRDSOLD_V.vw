CREATE OR REPLACE FORCE VIEW XXEIS.EIS_XXWC_AP_CONSN_PRDSOLD_V
(
   LOCATION,
   PART_NUMBER,
   DESCRIPTION,
   ORDERED_QUANTITY,
   PO_COST,
   ONHAND_QTY,
   SUPPLIER_NUMBER
)
AS
     SELECT mtp.organization_code Location,
            Msi.Segment1 Part_Number,
            Msi.Description Description,
            --SUM (NVL (rctl.quantity_invoiced, rctl.quantity_credited))  -- Commented by Mahender Reddy for TMS#20140507-00013 on 16/05/2014
            NVL (SUM (rctl.quantity_invoiced), SUM (rctl.quantity_credited)) -- Added by Mahender Reddy for TMS#20140507-00013 on 16/05/2014
               ORDERED_QUANTITY,
            /* SUM (
                DECODE (
                   NVL (
                      APPS.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id),
                      0),
                   0, ol.unit_cost,
                   --APPS.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id))) -- Commented by Mahender Reddy for TMS#20140507-00013 on 16/05/2014
                   msi.list_price_per_unit))*/
            msi.list_price_per_unit Po_Cost, -- Added by Mahender Reddy for TMS#20140507-00013 on 16/05/2014
            -- Commented by Mahender Reddy for TMS#20140507-00013 on 16/05/2014
            /*Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.get_onhand_qty (
               Msi.Inventory_Item_Id,
               Msi.Organization_Id)
               Onhand_Qty,*/
            -- Added by Mahender Reddy for TMS#20140507-00013 on 16/05/2014
            Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.get_item_onhand_qty (
               Msi.Inventory_Item_Id,
               Msi.Organization_Id)
               Onhand_Qty,
            Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Vendor_Number (
               Msi.Inventory_Item_Id,
               Msi.Organization_Id)
               Supplier_Number
       --Oh.Order_Number Order_Number,
       --TRUNC(Rct.Trx_Date) Invoice_Date,
       --TRUNC(Oh.Ordered_Date) Ordered_Date,
       --HCA.ACCOUNT_NUMBER CUSTOMER_NUMBER,
       --NVL(Hca.Account_Name,Hzp.party_name) Customer_Name
       --descr#flexfield#start
       --descr#flexfield#end
       --gl#accountff#start
       --gl#accountff#end
       FROM Oe_Order_Headers Oh,
            Oe_Order_Lines Ol,
            Hz_Cust_Accounts Hca,
            Hz_Parties Hzp,
            Mtl_Parameters Mtp,
            Gl_Code_Combinations_Kfv Gcc,
            Mtl_System_Items_Kfv Msi,
            Ra_Customer_Trx Rct,
            Ra_customer_trx_lines rctl
      WHERE     ol.header_id = oh.header_id
            AND oh.sold_to_org_id = hca.cust_account_id(+)
            AND hca.party_id = hzp.party_id(+)
            AND ol.ship_from_org_id = mtp.organization_id
            AND mtp.cost_of_sales_account = gcc.code_combination_id
            AND ol.inventory_item_id = msi.inventory_item_id
            AND ol.ship_from_org_id = msi.organization_id
            AND msi.consigned_flag = 1
            AND rctl.customer_trx_id = rct.customer_trx_id
            AND rctl.interface_line_context = 'ORDER ENTRY'
            AND rct.interface_header_context = 'ORDER ENTRY'
            AND TRUNC (rct.trx_date) >=
                   NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                        TRUNC (rct.trx_date))
            AND TRUNC (rct.trx_date) <=
                   NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_date_to,
                        TRUNC (rct.trx_date))
            AND TO_CHAR (rctl.interface_line_attribute6) = TO_CHAR (ol.line_id)
            AND TO_CHAR (oh.order_number) = rct.interface_header_attribute1
            AND EXISTS
                   (SELECT 1
                      FROM xxeis.eis_org_access_v
                     WHERE organization_id = mtp.organization_id)
   GROUP BY mtp.organization_code,
            Msi.Segment1,
            Msi.Description,
            msi.list_price_per_unit,
            Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.get_item_onhand_qty (
               Msi.Inventory_Item_Id,
               Msi.Organization_Id),
            Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Vendor_Number (
               Msi.Inventory_Item_Id,
               Msi.Organization_Id);
