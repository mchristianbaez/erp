--Report Name            : WC - CASS Freight Detail Subledger Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for WC - CASS Freight Detail Subledger Report
set scan off define off
DECLARE
BEGIN 
--Inserting View XXHDS_EIS_GL_CASS_SL_180_V
xxeis.eis_rs_ins.v( 'XXHDS_EIS_GL_CASS_SL_180_V',101,'','','','','NS057223','XXEIS','Xxhds Eis Gl Cass Sl 180 V','XEGCS1V','','');
--Delete View Columns for XXHDS_EIS_GL_CASS_SL_180_V
xxeis.eis_rs_utility.delete_view_rows('XXHDS_EIS_GL_CASS_SL_180_V',101,FALSE);
--Inserting View Columns for XXHDS_EIS_GL_CASS_SL_180_V
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','CARRIER_NAME',101,'Carrier Name','CARRIER_NAME','','','','NS057223','VARCHAR2','','','Carrier Name','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','AP_INV_DATE',101,'Ap Inv Date','AP_INV_DATE','','','','NS057223','DATE','','','Ap Inv Date','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','FETCH_SEQ',101,'Fetch Seq','FETCH_SEQ','','','','NS057223','NUMBER','','','Fetch Seq','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','APPLIED_TO_SOURCE_ID_NUM_1',101,'Applied To Source Id Num 1','APPLIED_TO_SOURCE_ID_NUM_1','','','','NS057223','NUMBER','','','Applied To Source Id Num 1','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SOURCE_DIST_ID_NUM_1',101,'Source Dist Id Num 1','SOURCE_DIST_ID_NUM_1','','','','NS057223','NUMBER','','','Source Dist Id Num 1','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SOURCE',101,'Source','SOURCE','','','','NS057223','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SOURCE_DIST_TYPE',101,'Source Dist Type','SOURCE_DIST_TYPE','','','','NS057223','VARCHAR2','','','Source Dist Type','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368FUTURE_USEDESCR',101,'Gcc50368future Usedescr','GCC50368FUTURE_USEDESCR','','','','NS057223','VARCHAR2','','','Gcc50368future Usedescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368FUTURE_USE',101,'Gcc50368future Use','GCC50368FUTURE_USE','','','','NS057223','VARCHAR2','','','Gcc50368future Use','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368SUBACCOUNTDESCR',101,'Gcc50368subaccountdescr','GCC50368SUBACCOUNTDESCR','','','','NS057223','VARCHAR2','','','Gcc50368subaccountdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368SUBACCOUNT',101,'Gcc50368subaccount','GCC50368SUBACCOUNT','','','','NS057223','VARCHAR2','','','Gcc50368subaccount','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368ACCOUNTDESCR',101,'Gcc50368accountdescr','GCC50368ACCOUNTDESCR','','','','NS057223','VARCHAR2','','','Gcc50368accountdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368ACCOUNT',101,'Gcc50368account','GCC50368ACCOUNT','','','','NS057223','VARCHAR2','','','Gcc50368account','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368DEPARTMENTDESCR',101,'Gcc50368departmentdescr','GCC50368DEPARTMENTDESCR','','','','NS057223','VARCHAR2','','','Gcc50368departmentdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368DEPARTMENT',101,'Gcc50368department','GCC50368DEPARTMENT','','','','NS057223','VARCHAR2','','','Gcc50368department','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368DIVISIONDESCR',101,'Gcc50368divisiondescr','GCC50368DIVISIONDESCR','','','','NS057223','VARCHAR2','','','Gcc50368divisiondescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368DIVISION',101,'Gcc50368division','GCC50368DIVISION','','','','NS057223','VARCHAR2','','','Gcc50368division','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368PRODUCTDESCR',101,'Gcc50368productdescr','GCC50368PRODUCTDESCR','','','','NS057223','VARCHAR2','','','Gcc50368productdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50368PRODUCT',101,'Gcc50368product','GCC50368PRODUCT','','','','NS057223','VARCHAR2','','','Gcc50368product','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328FUTURE_USE_2DESCR',101,'Gcc50328future Use 2descr','GCC50328FUTURE_USE_2DESCR','','','','NS057223','VARCHAR2','','','Gcc50328future Use 2descr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328FUTURE_USE_2',101,'Gcc50328future Use 2','GCC50328FUTURE_USE_2','','','','NS057223','VARCHAR2','','','Gcc50328future Use 2','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328FURTURE_USEDESCR',101,'Gcc50328furture Usedescr','GCC50328FURTURE_USEDESCR','','','','NS057223','VARCHAR2','','','Gcc50328furture Usedescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328FURTURE_USE',101,'Gcc50328furture Use','GCC50328FURTURE_USE','','','','NS057223','VARCHAR2','','','Gcc50328furture Use','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328PROJECT_CODEDESCR',101,'Gcc50328project Codedescr','GCC50328PROJECT_CODEDESCR','','','','NS057223','VARCHAR2','','','Gcc50328project Codedescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328PROJECT_CODE',101,'Gcc50328project Code','GCC50328PROJECT_CODE','','','','NS057223','VARCHAR2','','','Gcc50328project Code','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328ACCOUNTDESCR',101,'Gcc50328accountdescr','GCC50328ACCOUNTDESCR','','','','NS057223','VARCHAR2','','','Gcc50328accountdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328ACCOUNT',101,'Gcc50328account','GCC50328ACCOUNT','','','','NS057223','VARCHAR2','','','Gcc50328account','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328COST_CENTERDESCR',101,'Gcc50328cost Centerdescr','GCC50328COST_CENTERDESCR','','','','NS057223','VARCHAR2','','','Gcc50328cost Centerdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328COST_CENTER',101,'Gcc50328cost Center','GCC50328COST_CENTER','','','','NS057223','VARCHAR2','','','Gcc50328cost Center','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328LOCATIONDESCR',101,'Gcc50328locationdescr','GCC50328LOCATIONDESCR','','','','NS057223','VARCHAR2','','','Gcc50328locationdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328LOCATION',101,'Gcc50328location','GCC50328LOCATION','','','','NS057223','VARCHAR2','','','Gcc50328location','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328PRODUCTDESCR',101,'Gcc50328productdescr','GCC50328PRODUCTDESCR','','','','NS057223','VARCHAR2','','','Gcc50328productdescr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GCC50328PRODUCT',101,'Gcc50328product','GCC50328PRODUCT','','','','NS057223','VARCHAR2','','','Gcc50328product','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','CODE_COMBINATION_ID',101,'Code Combination Id','CODE_COMBINATION_ID','','','','NS057223','NUMBER','','','Code Combination Id','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','XXCUS_IMAGE_LINK',101,'Xxcus Image Link','XXCUS_IMAGE_LINK','','','','NS057223','VARCHAR2','','','Xxcus Image Link','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','XXCUS_PO_CREATED_BY',101,'Xxcus Po Created By','XXCUS_PO_CREATED_BY','','','','NS057223','VARCHAR2','','','Xxcus Po Created By','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','XXCUS_LINE_DESC',101,'Xxcus Line Desc','XXCUS_LINE_DESC','','','','NS057223','VARCHAR2','','','Xxcus Line Desc','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ITEM_OVRHD_COST',101,'Item Ovrhd Cost','ITEM_OVRHD_COST','','','','NS057223','NUMBER','','','Item Ovrhd Cost','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ITEM_MATERIAL_COST',101,'Item Material Cost','ITEM_MATERIAL_COST','','','','NS057223','NUMBER','','','Item Material Cost','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ITEM_TXN_QTY',101,'Item Txn Qty','ITEM_TXN_QTY','','','','NS057223','NUMBER','','','Item Txn Qty','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ITEM_UNIT_WT',101,'Item Unit Wt','ITEM_UNIT_WT','','','','NS057223','NUMBER','','','Item Unit Wt','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','PART_DESCRIPTION',101,'Part Description','PART_DESCRIPTION','','','','NS057223','VARCHAR2','','','Part Description','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','PART_NUMBER',101,'Part Number','PART_NUMBER','','','','NS057223','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_EVENT_TYPE',101,'Sla Event Type','SLA_EVENT_TYPE','','','','NS057223','VARCHAR2','','','Sla Event Type','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','AP_INV_SOURCE',101,'Ap Inv Source','AP_INV_SOURCE','','','','NS057223','VARCHAR2','','','Ap Inv Source','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','BOL_NUMBER',101,'Bol Number','BOL_NUMBER','','','','NS057223','VARCHAR2','','','Bol Number','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','PO_NUMBER',101,'Po Number','PO_NUMBER','','','','NS057223','VARCHAR2','','','Po Number','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','NS057223','VARCHAR2','','','Batch Name','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','NAME',101,'Name','NAME','','','','NS057223','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','EFFECTIVE_PERIOD_NUM',101,'Effective Period Num','EFFECTIVE_PERIOD_NUM','','','','NS057223','NUMBER','','','Effective Period Num','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','TYPE',101,'Type','TYPE','','','','NS057223','VARCHAR2','','','Type','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','NS057223','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','CURRENCY_CODE',101,'Currency Code','CURRENCY_CODE','','','','NS057223','VARCHAR2','','','Currency Code','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','NS057223','VARCHAR2','','','Gl Account String','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SALES_ORDER',101,'Sales Order','SALES_ORDER','','','','NS057223','VARCHAR2','','','Sales Order','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','TRANSACTION_NUM',101,'Transaction Num','TRANSACTION_NUM','','','','NS057223','VARCHAR2','','','Transaction Num','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ASSOCIATE_NUM',101,'Associate Num','ASSOCIATE_NUM','','','','NS057223','VARCHAR2','','','Associate Num','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_DIST_ACCOUNTED_NET',101,'Sla Dist Accounted Net','SLA_DIST_ACCOUNTED_NET','','','','NS057223','NUMBER','','','Sla Dist Accounted Net','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_DIST_ACCOUNTED_DR',101,'Sla Dist Accounted Dr','SLA_DIST_ACCOUNTED_DR','','','','NS057223','NUMBER','','','Sla Dist Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_DIST_ACCOUNTED_CR',101,'Sla Dist Accounted Cr','SLA_DIST_ACCOUNTED_CR','','','','NS057223','NUMBER','','','Sla Dist Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_DIST_ENTERED_NET',101,'Sla Dist Entered Net','SLA_DIST_ENTERED_NET','','','','NS057223','NUMBER','','','Sla Dist Entered Net','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_DIST_ENTERED_DR',101,'Sla Dist Entered Dr','SLA_DIST_ENTERED_DR','','','','NS057223','NUMBER','','','Sla Dist Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_DIST_ENTERED_CR',101,'Sla Dist Entered Cr','SLA_DIST_ENTERED_CR','','','','NS057223','NUMBER','','','Sla Dist Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_LINE_ENTERED_NET',101,'Sla Line Entered Net','SLA_LINE_ENTERED_NET','','','','NS057223','NUMBER','','','Sla Line Entered Net','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_LINE_ENTERED_CR',101,'Sla Line Entered Cr','SLA_LINE_ENTERED_CR','','','','NS057223','NUMBER','','','Sla Line Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_LINE_ENTERED_DR',101,'Sla Line Entered Dr','SLA_LINE_ENTERED_DR','','','','NS057223','NUMBER','','','Sla Line Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_LINE_ACCOUNTED_NET',101,'Sla Line Accounted Net','SLA_LINE_ACCOUNTED_NET','','','','NS057223','NUMBER','','','Sla Line Accounted Net','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_LINE_ACCOUNTED_CR',101,'Sla Line Accounted Cr','SLA_LINE_ACCOUNTED_CR','','','','NS057223','NUMBER','','','Sla Line Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SLA_LINE_ACCOUNTED_DR',101,'Sla Line Accounted Dr','SLA_LINE_ACCOUNTED_DR','','','','NS057223','NUMBER','','','Sla Line Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ACCOUNTED_CR',101,'Accounted Cr','ACCOUNTED_CR','','','','NS057223','NUMBER','','','Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ACCOUNTED_DR',101,'Accounted Dr','ACCOUNTED_DR','','','','NS057223','NUMBER','','','Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ENTERED_CR',101,'Entered Cr','ENTERED_CR','','','','NS057223','NUMBER','','','Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ENTERED_DR',101,'Entered Dr','ENTERED_DR','','','','NS057223','NUMBER','','','Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','CUSTOMER_OR_VENDOR_NUMBER',101,'Customer Or Vendor Number','CUSTOMER_OR_VENDOR_NUMBER','','','','NS057223','VARCHAR2','','','Customer Or Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','CUSTOMER_OR_VENDOR',101,'Customer Or Vendor','CUSTOMER_OR_VENDOR','','','','NS057223','VARCHAR2','','','Customer Or Vendor','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','NS057223','NUMBER','','','Gl Sl Link Id','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','H_SEQ_ID',101,'H Seq Id','H_SEQ_ID','','','','NS057223','NUMBER','','','H Seq Id','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SEQ_NUM',101,'Seq Num','SEQ_NUM','','','','NS057223','NUMBER','','','Seq Num','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SEQ_ID',101,'Seq Id','SEQ_ID','','','','NS057223','NUMBER','','','Seq Id','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','LINE_ENT_CR',101,'Line Ent Cr','LINE_ENT_CR','','','','NS057223','NUMBER','','','Line Ent Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','LINE_ENT_DR',101,'Line Ent Dr','LINE_ENT_DR','','','','NS057223','NUMBER','','','Line Ent Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','LINE_ACCTD_CR',101,'Line Acctd Cr','LINE_ACCTD_CR','','','','NS057223','NUMBER','','','Line Acctd Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','LINE_ACCTD_DR',101,'Line Acctd Dr','LINE_ACCTD_DR','','','','NS057223','NUMBER','','','Line Acctd Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','LLINE',101,'Lline','LLINE','','','','NS057223','NUMBER','','','Lline','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','HNUMBER',101,'Hnumber','HNUMBER','','','','NS057223','NUMBER','','','Hnumber','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','LSEQUENCE',101,'Lsequence','LSEQUENCE','','','','NS057223','VARCHAR2','','','Lsequence','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','LINE_DESCR',101,'Line Descr','LINE_DESCR','','','','NS057223','VARCHAR2','','','Line Descr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','BATCH',101,'Batch','BATCH','','','','NS057223','NUMBER','','','Batch','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ENTRY',101,'Entry','ENTRY','','','','NS057223','VARCHAR2','','','Entry','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ACC_DATE',101,'Acc Date','ACC_DATE','','','','NS057223','DATE','','','Acc Date','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','JE_LINE_NUM',101,'Je Line Num','JE_LINE_NUM','','','','NS057223','NUMBER','','','Je Line Num','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','JE_HEADER_ID',101,'Je Header Id','JE_HEADER_ID','','','','NS057223','NUMBER','','','Je Header Id','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','NS057223','VARCHAR2','','','Je Category','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','PAY_DATE',101,'Pay Date','PAY_DATE','','','','NS057223','VARCHAR2','','','Pay Date','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','AUDIT_MATCH',101,'Audit Match','AUDIT_MATCH','','','','NS057223','VARCHAR2','','','Audit Match','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','DESTINATION_POSTAL_CODE',101,'Destination Postal Code','DESTINATION_POSTAL_CODE','','','','NS057223','VARCHAR2','','','Destination Postal Code','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','DESTINATION_STATE',101,'Destination State','DESTINATION_STATE','','','','NS057223','VARCHAR2','','','Destination State','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','DESTINATION_CITY',101,'Destination City','DESTINATION_CITY','','','','NS057223','VARCHAR2','','','Destination City','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ORIGIN_POSTAL_CODE',101,'Origin Postal Code','ORIGIN_POSTAL_CODE','','','','NS057223','VARCHAR2','','','Origin Postal Code','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ORIGIN_STATE',101,'Origin State','ORIGIN_STATE','','','','NS057223','VARCHAR2','','','Origin State','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ORIGIN_CITY',101,'Origin City','ORIGIN_CITY','','','','NS057223','VARCHAR2','','','Origin City','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','DESTINATION_NAME',101,'Destination Name','DESTINATION_NAME','','','','NS057223','VARCHAR2','','','Destination Name','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','ORIGIN_NAME',101,'Origin Name','ORIGIN_NAME','','','','NS057223','VARCHAR2','','','Origin Name','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','MOVEMENT_TYPE',101,'Movement Type','MOVEMENT_TYPE','','','','NS057223','VARCHAR2','','','Movement Type','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','SHIPMENT_MODE',101,'Shipment Mode','SHIPMENT_MODE','','','','NS057223','VARCHAR2','','','Shipment Mode','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','EST_ACCESSORIALS',101,'Est Accessorials','EST_ACCESSORIALS','','','','NS057223','VARCHAR2','','','Est Accessorials','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','EST_FSC',101,'Est Fsc','EST_FSC','','','','NS057223','VARCHAR2','','','Est Fsc','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','EST_LINE_HAUL_CHARGE',101,'Est Line Haul Charge','EST_LINE_HAUL_CHARGE','','','','NS057223','VARCHAR2','','','Est Line Haul Charge','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','EST_TRANSACTION_NET',101,'Est Transaction Net','EST_TRANSACTION_NET','','','','NS057223','VARCHAR2','','','Est Transaction Net','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','EST_TRANSACTION_CR',101,'Est Transaction Cr','EST_TRANSACTION_CR','','','','NS057223','VARCHAR2','','','Est Transaction Cr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','EST_TRANSACTION_DR',101,'Est Transaction Dr','EST_TRANSACTION_DR','','','','NS057223','VARCHAR2','','','Est Transaction Dr','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','TRANSACTION_OTHER',101,'Transaction Other','TRANSACTION_OTHER','','','','NS057223','VARCHAR2','','','Transaction Other','','','');
xxeis.eis_rs_ins.vc( 'XXHDS_EIS_GL_CASS_SL_180_V','TRANSACTION_LINE_HAUL',101,'Transaction Line Haul','TRANSACTION_LINE_HAUL','','','','NS057223','VARCHAR2','','','Transaction Line Haul','','','');
--Inserting View Components for XXHDS_EIS_GL_CASS_SL_180_V
--Inserting View Component Joins for XXHDS_EIS_GL_CASS_SL_180_V
END;
/
set scan on define on
prompt Creating Report Data for WC - CASS Freight Detail Subledger Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC - CASS Freight Detail Subledger Report
xxeis.eis_rs_utility.delete_report_rows( 'WC - CASS Freight Detail Subledger Report' );
--Inserting Report - WC - CASS Freight Detail Subledger Report
xxeis.eis_rs_ins.r( 101,'WC - CASS Freight Detail Subledger Report','','This report will provide details from general ledger and all subledgers for all entries posted for the account in general ledger imported from CASS.  

Created from TMS Task ID: 20160411-00110
XXHDS_EIS_GL_CASS_SL_180_V(XEGCS1V)','','','','NS057223','XXHDS_EIS_GL_CASS_SL_180_V','Y','','','NS057223','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - WC - CASS Freight Detail Subledger Report
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'ACC_DATE','Accounting Date','Acc Date','','','default','','47','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'AP_INV_SOURCE','Payables Invoice Source','Ap Inv Source','','','default','','7','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'AUDIT_MATCH','Audit Match','Audit Match','','','default','','44','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'BATCH','Batch','Batch','','~~~','default','','48','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','49','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'BOL_NUMBER','Bol Number','Bol Number','','','default','','25','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'CARRIER_NAME','Carrier Name','Carrier Name','','','default','','10','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'CURRENCY_CODE','Currency Code','Currency Code','','','default','','50','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'CUSTOMER_OR_VENDOR','Customer Or Vendor Name','Customer Or Vendor','','','default','','8','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'CUSTOMER_OR_VENDOR_NUMBER','Customer Or Vendor Number','Customer Or Vendor Number','','','default','','9','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'DESTINATION_CITY','Destination City','Destination City','','','default','','33','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'DESTINATION_NAME','Destination Name','Destination Name','','','default','','29','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'DESTINATION_POSTAL_CODE','Destination ZIP/Postal Code','Destination Postal Code','','','default','','35','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'DESTINATION_STATE','Destination State','Destination State','','','default','','34','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'ENTRY','Entry','Entry','','','default','','51','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'EST_ACCESSORIALS','Estimate  Accessorials','Est Accessorials','','','default','','23','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'EST_FSC','Estimate  Fsc','Est Fsc','','','default','','22','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'EST_LINE_HAUL_CHARGE','Estimate  Line Haul Charge','Est Line Haul Charge','','','default','','21','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'EST_TRANSACTION_CR','Estimate  Transaction Cr','Est Transaction Cr','','','default','','19','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'EST_TRANSACTION_DR','Estimate Transaction Dr','Est Transaction Dr','','','default','','18','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'EST_TRANSACTION_NET','Estimate  Transaction Net','Est Transaction Net','','','default','','20','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'GCC50328ACCOUNT','GL Account','Gcc50328account','','','default','','3','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'GCC50328ACCOUNTDESCR','GL Account Description','Gcc50328accountdescr','','','default','','4','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'GCC50328LOCATION','Branch BW','Gcc50328location','','','default','','1','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'GCC50328LOCATIONDESCR','Branch Name','Gcc50328locationdescr','','','default','','2','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'GL_ACCOUNT_STRING','Gl Account String','Gl Account String','','','default','','52','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'ITEM_MATERIAL_COST','Item Material Cost','Item Material Cost','','~~~','default','','40','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'ITEM_OVRHD_COST','Item Ovrhd Cost','Item Ovrhd Cost','','~~~','default','','39','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'ITEM_TXN_QTY','Item Txn Qty','Item Txn Qty','','~~~','default','','38','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'ITEM_UNIT_WT','Item Unit Wt','Item Unit Wt','','~~~','default','','42','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'JE_CATEGORY','Je Category','Je Category','','','default','','53','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'JE_LINE_NUM','Je Line Number','Je Line Num','','~~~','default','','54','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'LINE_DESCR','Je Line Description','Line Descr','','','default','','55','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'MOVEMENT_TYPE','Movement Type','Movement Type','','','default','','27','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'NAME','Business Name','Name','','','default','','56','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'ORIGIN_CITY','Origin City','Origin City','','','default','','30','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'ORIGIN_NAME','Origin Name','Origin Name','','','default','','28','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'ORIGIN_POSTAL_CODE','Origin ZIP/Postal Code','Origin Postal Code','','','default','','32','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'ORIGIN_STATE','Origin State','Origin State','','','default','','31','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'PART_DESCRIPTION','Part Description','Part Description','','','default','','37','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'PART_NUMBER','Part Number','Part Number','','','default','','36','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'PAY_DATE','Pay Date','Pay Date','','','default','','45','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'PERIOD_NAME','Accounting Month','Period Name','','','default','','46','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'PO_NUMBER','Po Number','Po Number','','','default','','12','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'SALES_ORDER','Sales Order Number','Sales Order','','','default','','11','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'SHIPMENT_MODE','Shipment Mode','Shipment Mode','','','default','','26','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'SLA_EVENT_TYPE','Subledger Event Type','Sla Event Type','','','default','','6','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'SOURCE','Transaction Source','Source','','','default','','5','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'TRANSACTION_LINE_HAUL','Transaction Line Haul','Transaction Line Haul','','','default','','16','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'TRANSACTION_NUM','Transaction Number','Transaction Num','','','default','','24','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'TRANSACTION_OTHER','Transaction Other','Transaction Other','','','default','','17','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'TYPE','Type','Type','','','default','','57','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'SLA_DIST_ACCOUNTED_CR','Transaction Cr','Sla Dist Accounted Cr','','~~~','default','','14','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'SLA_DIST_ACCOUNTED_DR','Transaction Dr','Sla Dist Accounted Dr','','~~~','default','','13','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'SLA_DIST_ACCOUNTED_NET','Transaction Net','Sla Dist Accounted Net','','~~~','default','','15','N','','','','','','','','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'CALC_MTL_TRANSACTION_COST','Calc Material Transaction Cost','Sla Dist Accounted Net','NUMBER','~~~','default','','41','Y','','','','','','','XEGCS1V.ITEM_TXN_QTY*XEGCS1V.ITEM_MATERIAL_COST','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'WC - CASS Freight Detail Subledger Report',101,'CALC_MTL_TXN_WT','Calc Material Transaction Weight','Sla Dist Accounted Net','NUMBER','~~~','default','','43','Y','','','','','','','DECODE(XEGCS1V.ITEM_UNIT_WT,0,0,XEGCS1V.ITEM_TXN_QTY*XEGCS1V.ITEM_UNIT_WT)','NS057223','N','N','','XXHDS_EIS_GL_CASS_SL_180_V','','');
--Inserting Report Parameters - WC - CASS Freight Detail Subledger Report
xxeis.eis_rs_ins.rp( 'WC - CASS Freight Detail Subledger Report',101,'Period Name','Period Name','PERIOD_NAME','IN','','','VARCHAR2','Y','Y','1','','Y','CONSTANT','NS057223','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC - CASS Freight Detail Subledger Report',101,'Product','Gcc50328product','GCC50328PRODUCT','IN','','','VARCHAR2','N','Y','2','','Y','CONSTANT','NS057223','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC - CASS Freight Detail Subledger Report',101,'Location','Gcc50328location','GCC50328LOCATION','IN','','','VARCHAR2','N','Y','3','','Y','CONSTANT','NS057223','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC - CASS Freight Detail Subledger Report',101,'Account','Gcc50328account','GCC50328ACCOUNT','IN','','','VARCHAR2','Y','Y','4','','Y','CONSTANT','NS057223','Y','N','','','');
--Inserting Report Conditions - WC - CASS Freight Detail Subledger Report
xxeis.eis_rs_ins.rcn( 'WC - CASS Freight Detail Subledger Report',101,'PERIOD_NAME','IN',':Period Name','','','Y','1','Y','NS057223');
xxeis.eis_rs_ins.rcn( 'WC - CASS Freight Detail Subledger Report',101,'GCC50328PRODUCT','IN',':Product','','','Y','2','Y','NS057223');
xxeis.eis_rs_ins.rcn( 'WC - CASS Freight Detail Subledger Report',101,'GCC50328LOCATION','IN',':Location','','','Y','3','Y','NS057223');
xxeis.eis_rs_ins.rcn( 'WC - CASS Freight Detail Subledger Report',101,'GCC50328LOCATION','IN',':Gcc50328location','','','Y','4','Y','NS057223');
xxeis.eis_rs_ins.rcn( 'WC - CASS Freight Detail Subledger Report',101,'GCC50328ACCOUNT','IN',':Account','','','Y','5','Y','NS057223');
xxeis.eis_rs_ins.rcn( 'WC - CASS Freight Detail Subledger Report',101,'GCC50328ACCOUNT','IN',':Gcc50328account','','','Y','6','Y','NS057223');
--Inserting Report Sorts - WC - CASS Freight Detail Subledger Report
--Inserting Report Triggers - WC - CASS Freight Detail Subledger Report
--Inserting Report Templates - WC - CASS Freight Detail Subledger Report
--Inserting Report Portals - WC - CASS Freight Detail Subledger Report
--Inserting Report Dashboards - WC - CASS Freight Detail Subledger Report
--Inserting Report Security - WC - CASS Freight Detail Subledger Report
--Inserting Report Pivots - WC - CASS Freight Detail Subledger Report
END;
/
set scan on define on
