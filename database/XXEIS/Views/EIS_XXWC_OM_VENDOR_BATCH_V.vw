CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_vendor_batch_v
(
   salesperson_name
  ,salespeson_number
  ,master_account_name
  ,master_account_number
  ,job_name
  ,request_number
  ,job_number
  ,vendor_number
  ,vendor_name
  ,vendor_quote_number
  ,invoice_number
  ,invoice_date
  ,part_number
  ,uom
  ,description
  ,po_cost
  ,list_price
  ,special_cost
  ,unit_claim_value
  ,claim_value
  ,qty
  ,rebate
  ,gl_coding
  ,gl_string
  ,loc
  ,location
  ,created_by
  ,credit_memo
  ,rebeat_creditmemo_amount
  ,claim_number
  ,customer_trx_id
  ,vendor_clearing_account
  ,order_number
  ,line_number
  ,creation_date
  ,request_id
  ,batch_id
)
AS
   SELECT ppf.full_name salesperson_name
         ,jrs.salesrep_number salespeson_number
         ,hcs.account_name master_account_name
         ,hcs.account_number master_account_number
         ,hcsu.location job_name
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_ssd_req_number (oeh.header_id
                                                            ,oel.line_id
                                                            ,osbl.item_id)
             request_number
         ,        --       HPS.PARTY_SITE_NUMBER                   JOB_NUMBER,
          SUBSTR (hcsu.location, INSTR (hcsu.location, '-', 1) + 1)
             job_number
         ,pv.segment1 vendor_number
         ,pv.vendor_name vendor_name
         ,osbl.agreement_number vendor_quote_number
         ,NVL (
             rct.trx_number
            ,xxeis.eis_rs_xxwc_com_util_pkg.get_venquote_glstring (
                oeh.order_number
               ,'TRXNUM'
               ,oel.line_id))
             invoice_number
         ,NVL (
             rct.trx_date
            ,xxeis.eis_rs_xxwc_com_util_pkg.get_venquote_glstring (
                oeh.order_number
               ,'TRXDATE'
               ,oel.line_id))
             invoice_date
         ,msi.concatenated_segments part_number
         ,msi.primary_unit_of_measure uom
         ,msi.description description
         ,osbl.list_price po_cost
         ,osbl.list_price list_price
         ,osbl.agreement_price special_cost
         ,NVL (osbl.list_price, 0) - NVL (osbl.agreement_price, 0)
             unit_claim_value
         ,osbl.claim_amount claim_value
         ,osbl.quantity_shipped qty
         ,osbl.claim_amount rebate
         ,   '400-900'
          || '-'
          || NVL (
                gcc.segment2
               ,xxeis.eis_rs_xxwc_com_util_pkg.get_venquote_glstring (
                   oeh.order_number
                  ,'LOCATION'
                  ,oel.line_id))
             gl_coding
         ,NVL (
             gcc.concatenated_segments
            ,xxeis.eis_rs_xxwc_com_util_pkg.get_venquote_glstring (
                oeh.order_number
               ,'STRING'
               ,oel.line_id))
             gl_string
         ,NVL (
             gcc.segment2
            ,xxeis.eis_rs_xxwc_com_util_pkg.get_venquote_glstring (
                oeh.order_number
               ,'LOCATION'
               ,oel.line_id))
             loc
         ,ood.organization_code location
         ,ppf1.full_name created_by
         , -- ap.invoice_num                                        credit_memo,
 --ap.invoice_amount                                     rebeat_creditmemo_amount,
          DECODE (
             ap.invoice_num
            ,'', (SELECT ap.invoice_num
                    FROM ap_invoices ap, ozf_claims_all oca1
                   WHERE     oca1.batch_id = osbh.batch_id
                         AND ap.reference_key1 = oca1.claim_id)
            ,ap.invoice_num)
             credit_memo
         ,DECODE (
             ap.invoice_amount
            ,'', (SELECT ap.invoice_amount
                    FROM ap_invoices ap, ozf_claims_all oca1
                   WHERE     oca1.batch_id = osbh.batch_id
                         AND ap.reference_key1 = oca1.claim_id)
            ,ap.invoice_amount)
             rebeat_creditmemo_amount
         , --Oca.Claim_Number                                      Claim_Number,
          DECODE (oca.claim_number
                 ,'', (SELECT oca2.claim_number
                         FROM ozf_claims_all oca2
                        WHERE oca2.batch_id = osbh.batch_id)
                 ,oca.claim_number)
             claim_number
         ,rct.customer_trx_id
         ,gcck.concatenated_segments vendor_clearing_account
         ,oeh.order_number
         ,oel.line_number
         ,TRUNC (osbh.creation_date) creation_date
         ,osbl.request_id
         ,osbl.batch_id
     FROM ozf_sd_batch_headers_all osbh
         ,ozf_sd_batch_lines_all osbl
         ,ra_customer_trx rct
         ,ra_customer_trx_lines rctl
         ,ra_cust_trx_line_gl_dist rctlgl
         ,oe_order_headers oeh
         ,oe_order_lines oel
         ,po_vendors pv
         ,po_vendor_sites pvs
         ,mtl_system_items_b_kfv msi
         ,org_organization_definitions ood
         ,jtf_rs_salesreps jrs
         ,per_all_people_f ppf
         ,gl_code_combinations_kfv gcc
         ,hz_parties hp
         ,hz_cust_accounts hcs
         ,hz_cust_site_uses hcsu
         ,hz_cust_acct_sites hcas
         ,hz_party_sites hps
         ,per_all_people_f ppf1
         ,fnd_user fu
         ,                              -- RA_CUSTOMER_TRX               RCTC,
                                        --  Ar_Receivable_Applications    Arp,
          ozf_claims_all oca
         ,ap_invoices ap
         ,ozf_sys_parameters_all osp
         ,gl_code_combinations_kfv gcck
    WHERE     osbh.batch_id = osbl.batch_id
          AND rct.customer_trx_id(+) = osbl.invoice_number
          AND rctl.customer_trx_line_id(+) = osbl.invoice_line_number
          AND rctlgl.customer_trx_id(+) = rctl.customer_trx_id
          AND rctlgl.customer_trx_line_id(+) = rctl.customer_trx_line_id
          AND rctl.line_type(+) = 'LINE'
          AND oeh.header_id = oel.header_id
          AND oeh.header_id = osbl.order_header_id
          AND oel.line_id = osbl.order_line_id
          AND oeh.flow_status_code = 'CLOSED'
          AND pv.vendor_id = pvs.vendor_id
          AND pv.vendor_id = osbh.vendor_id
          AND pvs.vendor_site_id = osbh.vendor_site_id
          AND pvs.org_id = osbh.org_id
          AND msi.inventory_item_id = osbl.item_id
          AND msi.organization_id = osbl.org_id
          AND ood.organization_id = osbl.org_id
          -- AND RCT.PRIMARY_SALESREP_ID           = JRS.SALESREP_ID(+)
          AND oeh.salesrep_id = jrs.salesrep_id(+)
          --AND Rct.Org_Id                        = Jrs.Org_Id(+)
          AND oeh.org_id = jrs.org_id(+)
          AND ppf.person_id(+) = jrs.person_id
          -- and nvl(trunc(OEH.ORDERED_DATE),sysdate) >= decode(ppf.effective_start_date,'',nvl(trunc(OEH.ORDERED_DATE),sysdate))
          -- and nvl(trunc(OEH.ORDERED_DATE),sysdate) <= decode(ppf.effective_start_date,'',NVL(trunc(OEH.ORDERED_DATE),sysdate))
          AND hp.party_id = hcs.party_id
          AND hp.party_id = osbl.sold_to_customer_id
          AND hcsu.site_use_id = osbl.ship_to_org_id
          AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
          AND hcas.party_site_id = hps.party_site_id
          AND gcc.code_combination_id(+) = rctlgl.code_combination_id
          AND fu.user_id = osbh.created_by
          AND fu.employee_id = ppf1.person_id
          -- and nvl(trunc(oeh.ordered_date),sysdate) >= decode(ppf1.effective_start_date,'',nvl(trunc(oeh.ordered_date),sysdate))
          -- and nvl(trunc(OEH.ORDERED_DATE),sysdate) <= decode(ppf1.effective_start_date,'',NVL(trunc(OEH.ORDERED_DATE),sysdate))
          AND oca.claim_id(+) = osbh.claim_id
          /* and( oca.claim_id                   = osbh.claim_id
          OR oca.batch_id  = osbh.batch_id)*/
          AND ap.reference_key1(+) = oca.claim_id
          AND osp.org_id = osbh.org_id
          -- and oeh.header_id=9264
          AND osp.gl_id_ded_clearing = gcck.code_combination_id;


