create or replace view XXEIS.EIS_XXWC_FND_CONC_REQ_DET_V(
user_conc_prog_name
,request_id
,pending_normal_time
,running_time
,end_to_end
,request_date
,actual_start_date
,actual_completion_date
,user_name
,phase_code
,status_code
,argument_text
,PRINTER
,Concurrent_Manager)
  /**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_FND_CONC_REQ_DET_V.vw $
  Module Name: EIS Admin Reports
  PURPOSE: View for EIS Report "WC Critical Report CFD Timings"
  To extract the CFD timings after PROD migration which has impacted the CFD�s
  TMS Task Id : 20150303-00254
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        10/12/2015  Mahender Reddy        Initial Version
  
  **************************************************************************************************************/
AS
SELECT DISTINCT c.user_concurrent_program_name user_conc_prog_name
,a.request_id request_id
,round(((a.actual_start_date-a.request_Date)*24*60*60),2) Pending_Normal_time
,round(((a.actual_completion_date-a.actual_start_date)*24*60*60),2) running_time
,round(((a.actual_completion_date-a.request_Date)*24*60*60),2) end_to_end
,To_Char(a.request_date,'DD-MON-YY HH24:MI:SS') Request_date
,To_Char(a.actual_start_date,'DD-MON-YY HH24:MI:SS') Actual_Start_date
,To_Char(a.actual_completion_date,'DD-MON-YY HH24:MI:SS') Actual_completion_date
,d.user_name user_name
,a.phase_code phase_code
,a.status_code status_code
,a.argument_text argument_text
,a.printer printer
,fcq.user_concurrent_queue_name Concurrent_Manager
FROM apps.fnd_concurrent_requests a,
 apps.fnd_concurrent_programs b ,
 apps.FND_CONCURRENT_PROGRAMS_TL c,
 apps.fnd_user d,
 apps.fnd_concurrent_processes fcp,
 apps.fnd_concurrent_queues_vl fcq
WHERE a.concurrent_program_id= b.concurrent_program_id 
AND b.concurrent_program_id=c.concurrent_program_id 
and a.REQUESTED_BY =D.USER_ID 
AND c.USER_CONCURRENT_PROGRAM_NAME in ('XXWC OM Packing Slip','XXWC OM Sales Receipt Report','XXWC OM Customer Sales Receipt Report','XXWC OM Pick Slip')
AND fcp.CONCURRENT_QUEUE_ID=fcq.CONCURRENT_QUEUE_ID
AND fcp.CONCURRENT_PROCESS_ID=a.controlling_manager
AND a.requested_start_date like sysdate --To see all the requests submitted today 
--and round(((a.actual_start_date-a.request_Date)*24*60*60/60),2) > '0.5'
 --and a.requested_by=5200
 --and a.request_id in (13938095,13938096,13938097,13938099,13938103,13938104,13938105,13938110,13938111,13938112)
 order by ROUND(((a.ACTUAL_START_DATE-a.REQUEST_DATE)*24*60*60),2) desc, USER_CONCURRENT_PROGRAM_NAME
 /