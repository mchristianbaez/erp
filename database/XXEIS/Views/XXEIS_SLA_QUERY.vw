---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXEIS_SLA_QUERY $
  Module Name : Inventory
  PURPOSE	  : Inventory SLA Detail
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     	   27-Sep-2016        	Siva   		 TMS#20160824-00063  --Changed the position of all tables and conditions in view
															and added hints for fixing the for fixing the Performance issue
  1.2     	   9-Dec-2016        	Siva   		 TMS#20161107-00009 
**************************************************************************************************************/
DROP VIEW XXEIS.XXEIS_SLA_QUERY;

CREATE OR REPLACE VIEW XXEIS.XXEIS_SLA_QUERY (CONCATENATED_SEGMENTS, EVENT_TYPE_CODE, ACCOUNTING_DATE, PERIOD_NAME, AE_HEADER_ID, JE_CATEGORY_NAME, ACCOUNTING_CLASS_CODE, ACCOUNTED_DR, ACCOUNTED_CR, ENTERED_DR, ENTERED_CR, UNROUNDED_ACCOUNTED_DR, UNROUNDED_ACCOUNTED_CR, GL_SL_LINK_ID, GL_SL_LINK_TABLE, REF1, REF2, REF3, REF4, REF5, REF6, REF7, REF8, REF9, REF10, REF11, REF12, REF13, REF14)
AS
  SELECT a.concatenated_segments,
    a.event_type_code,
    a.accounting_date,
    a.period_name,
    a.ae_header_id,
    a.je_category_name,
    a.accounting_class_code,
    a.accounted_dr,
    a.accounted_cr,
    a.entered_dr,
    a.entered_cr,
    a.unrounded_accounted_dr,
    a.unrounded_accounted_cr,
    a.gl_sl_link_id,
    a.gl_sl_link_table,
    a.ref1,                                              --ITEM NUMBER
    a.ref2,                                              --PO / INVOICE / CHECK NUMBER
    a.ref3,                                              --PRIMARY TRANSACTION QTY
    a.ref4,                                              --TRANSACTION COST
    a.ref5,                                              --NEW COST
    a.ref6,                                              --PRIOR COST
    a.ref7,                                              --ACTUAL COST
    a.ref8,                                              --ORGANIZATION CODE
    a.ref9,                                              --CREATION DATE
    a.ref10,                                             --LAST_UPDATE
    ROUND((ABS(NVL(a.ref3,0) * NVL(a.ref4,0))),2) ref11, --PRIMARY QTY * TRANSACTION COST
    ROUND((ABS(NVL(a.ref3,0) * NVL(a.ref5,0))),2) ref12, --PRIMARY QTY * NEW COST
    ROUND((ABS(NVL(a.ref3,0) * NVL(a.ref6,0))),2) ref13, --PRIMARY QTY * PRIOR COST
    ROUND((ABS(NVL(a.ref3,0) * NVL(a.ref7,0))),2) ref14  --PRIMARY QTY * ACTUAL COST
  FROM
    (
    ---------------------------------
    --Receiving Subledger Detail-----
    --POs with Item Numbers
    ---------------------------------
    SELECT /*+ INDEX(rrsl RCV_RECEIVING_SUB_LEDGER_N3) INDEX(gcc GL_CODE_COMBINATIONS_U1)*/    --added for version 1.2
      gcc.concatenated_segments,
      xah.event_type_code,
      xah.accounting_date,
      -- xah.period_name, --Commented for version 1.2
      jh.period_name, --added for version 1.2
      xal.ae_header_id,
      xah.je_category_name,
      xal.accounting_class_code,
      xal.accounted_dr,
      xal.accounted_cr,
      xal.entered_dr,
      xal.entered_cr,
      xal.unrounded_accounted_dr,
      xal.unrounded_accounted_cr,
      xal.gl_sl_link_id,
      xal.gl_sl_link_table,
      TO_CHAR(msib.segment1) ref1,
      TO_CHAR(pha.segment1) ref2,
      TO_CHAR(rt.primary_quantity) ref3,
      TO_CHAR(rt.po_unit_price) ref4,
      '' ref5,
      '' ref6,
      '' ref7,
      TO_CHAR(mp.organization_code) ref8,
      rt.creation_date ref9,
      rt.last_update_date ref10
    FROM apps.gl_je_headers jh,      --added for version 1.2
      apps.gl_je_lines jl,           --added for version 1.2
      apps.gl_import_references gir, --added for version 1.2
      apps.xla_ae_lines xal,
      xla.xla_distribution_links xdl,
      apps.xla_ae_headers xah,
      apps.gl_code_combinations_kfv gcc ,
      po.rcv_receiving_sub_ledger rrsl,
      po.rcv_transactions rt,
      po.po_headers_all pha,
      po.po_lines_all pla,
      inv.mtl_system_items_b msib,
      inv.mtl_parameters mp
    WHERE jh.je_header_id                = jl.je_header_id         --added for version 1.2
    AND jh.je_source                     = 'Cost Management'       --added for version 1.2
    AND jh.actual_flag                   = 'A'                     --added for version 1.2
    AND jl.je_header_id                  = gir.je_header_id        --added for version 1.2
    AND jl.je_line_num                   = gir.je_line_num         --added for version 1.2
    AND gir.gl_sl_link_id                = xal.gl_sl_link_id(+)    --added for version 1.2
    AND gir.gl_sl_link_table             = xal.gl_sl_link_table(+) --added for version 1.2
    AND xal.application_id               = 707                     --added for version 1.2
    AND xal.ae_header_id                 = xdl.ae_header_id        --changed for version 1.1
    AND xal.ae_line_num                  = xdl.ae_line_num
    AND xal.application_id               = xdl.application_id --added for version 1.1
    AND xah.gl_transfer_status_code      = 'Y'
    AND xal.ae_header_id                 = xah.ae_header_id
    AND xal.application_id               = xah.application_id --added for version 1.1
    AND xah.event_id                     = xdl.event_id
    AND xal.code_combination_id          = gcc.code_combination_id
    AND xdl.source_distribution_id_num_1 = rrsl.rcv_sub_ledger_id
    AND xdl.source_distribution_type     = 'RCV_RECEIVING_SUB_LEDGER'
    AND rrsl.rcv_transaction_id          = rt.transaction_id
      --AND rrsl.reference3                  = rt.po_distribution_id  --Commented for version 1.2
    AND rt.po_header_id      			 = pha.po_header_id
    AND rt.po_line_id        			 = pla.po_line_id
    AND pla.item_id         			IS NOT NULL
    AND pla.item_id         			 = msib.inventory_item_id
    AND rt.organization_id  			 = msib.organization_id
    AND msib.organization_id 			 = mp.organization_id(+)
    ---------------------------------
    --Receiving Subledger Detail-----
    --POs without Item Numbers
    ---------------------------------
    UNION ALL
    SELECT /*+ INDEX(pla PO_LINES_N1) INDEX(gcc GL_CODE_COMBINATIONS_U1)*/  --added for version 1.2
      gcc.concatenated_segments,
      xah.event_type_code,
      xah.accounting_date,
      -- xah.period_name, --Commented for version 1.2
      jh.period_name, --added for version 1.2
      xal.ae_header_id,
      xah.je_category_name,
      xal.accounting_class_code,
      xal.accounted_dr,
      xal.accounted_cr,
      xal.entered_dr,
      xal.entered_cr,
      xal.unrounded_accounted_dr,
      xal.unrounded_accounted_cr,
      xal.gl_sl_link_id,
      xal.gl_sl_link_table,
      NULL ref1,
      TO_CHAR(pha.segment1) ref2,
      TO_CHAR(rt.primary_quantity) ref3,
      '' ref4,
      '' ref5,
      '' ref6,
      '' ref7,
      TO_CHAR(mp.organization_code) ref8,
      rt.creation_date ref9,
      rt.last_update_date ref10
    FROM apps.gl_je_headers jh,      --added for version 1.2
      apps.gl_je_lines jl,           --added for version 1.2
      apps.gl_import_references gir, --added for version 1.2
      apps.xla_ae_lines xal,
      xla.xla_distribution_links xdl,
      apps.xla_ae_headers xah,
      apps.gl_code_combinations_kfv gcc ,
      po.rcv_receiving_sub_ledger rrsl,
      po.rcv_transactions rt,
      po.po_headers_all pha,
      po.po_lines_all pla,
      inv.mtl_parameters mp
    WHERE jh.je_header_id                = jl.je_header_id         --added for version 1.2
    AND jh.je_source                     = 'Cost Management'       --added for version 1.2
    AND jh.actual_flag                   = 'A'                     --added for version 1.2
    AND jl.je_header_id                  = gir.je_header_id        --added for version 1.2
    AND jl.je_line_num                   = gir.je_line_num         --added for version 1.2
    AND gir.gl_sl_link_id                = xal.gl_sl_link_id(+)    --added for version 1.2
    AND gir.gl_sl_link_table             = xal.gl_sl_link_table(+) --added for version 1.2
    AND xal.application_id               = 707                     --added for version 1.2
    AND xal.ae_header_id                 = xdl.ae_header_id        --added for version 1.1
    AND xal.ae_line_num                  = xdl.ae_line_num
    AND xal.application_id               = xdl.application_id --added for version 1.1
    AND xah.gl_transfer_status_code      = 'Y'
    AND xal.ae_header_id                 = xah.ae_header_id
    AND xal.application_id               = xah.application_id --added for version 1.1
    AND xah.event_id                     = xdl.event_id
    AND xal.code_combination_id          = gcc.code_combination_id
    AND xdl.source_distribution_id_num_1 = rrsl.rcv_sub_ledger_id
    AND xdl.source_distribution_type     = 'RCV_RECEIVING_SUB_LEDGER'
    AND rrsl.rcv_transaction_id          = rt.transaction_id
      --AND rrsl.reference3                  = rt.po_distribution_id  --Commented for version 1.2
    AND rt.po_header_id    				 = pha.po_header_id
    AND rt.po_line_id     				 = pla.po_line_id
    AND pla.item_id      				IS NULL
    AND rt.organization_id 				 = mp.organization_id(+)
    ---------------------------------
    --Cost Management Subledger Detail--
    ---------------------------------
    UNION ALL
    SELECT /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1)*/  --added for version 1.2
      gcc.concatenated_segments,
      xah.event_type_code,
      xah.accounting_date,
      -- xah.period_name, --Commented for version 1.2
      jh.period_name, --added for version 1.2
      xal.ae_header_id,
      xah.je_category_name,
      xal.accounting_class_code,
      xal.accounted_dr,
      xal.accounted_cr,
      xal.entered_dr,
      xal.entered_cr,
      xal.unrounded_accounted_dr,
      xal.unrounded_accounted_cr,
      xal.gl_sl_link_id,
      xal.gl_sl_link_table,
      msib.segment1 ref1,
      '' ref2,
      TO_CHAR(mmt.primary_quantity ) ref3,
      TO_CHAR(mmt.transaction_cost) ref4,
      TO_CHAR(mmt.new_cost) ref5,
      TO_CHAR(mmt.prior_cost) ref6,
      TO_CHAR(mmt.actual_cost) ref7,
      TO_CHAR(mp.organization_code) ref8,
      mmt.creation_date ref9,
      mmt.last_update_date ref10
    FROM apps.gl_je_headers jh,      --added for version 1.2
      APPS.GL_JE_LINES JL,           --added for version 1.2
      apps.gl_import_references gir, --added for version 1.2
      apps.xla_ae_lines xal,
      apps.xla_distribution_links xdl,
      apps.xla_ae_headers xah,
      apps.gl_code_combinations_kfv gcc,
      apps.mtl_transaction_accounts mta,
      apps.mtl_material_transactions mmt,
      apps.mtl_system_items_b msib,
      apps.mtl_parameters mp
    WHERE jh.je_header_id                = jl.je_header_id         --added for version 1.2
    AND jh.je_source                     = 'Cost Management'       --added for version 1.2
    AND jh.actual_flag                   = 'A'                     --added for version 1.2
    AND jl.je_header_id                  = gir.je_header_id        --added for version 1.2
    AND jl.je_line_num                   = gir.je_line_num         --added for version 1.2
    AND GIR.GL_SL_LINK_ID                = XAL.GL_SL_LINK_ID(+)    --added for version 1.2
    AND gir.gl_sl_link_table             = xal.gl_sl_link_table(+) --added for version 1.2
    AND xal.application_id               = 707                     --added for version 1.2
    AND xal.ae_header_id                 = xdl.ae_header_id        --added for version 1.1
    AND xal.ae_line_num                  = xdl.ae_line_num
    AND xal.application_id               = xdl.application_id --added for version 1.1
    AND xah.gl_transfer_status_code      = 'Y'
    AND xal.ae_header_id                 = xah.ae_header_id
    AND xal.application_id               = xah.application_id --added for version 1.1
    AND xah.event_id                     = xdl.event_id
    AND xal.code_combination_id          = gcc.code_combination_id
    AND xdl.source_distribution_type     = 'MTL_TRANSACTION_ACCOUNTS'
    AND xdl.source_distribution_id_num_1 = mta.inv_sub_ledger_id
    AND mta.transaction_id               = mmt.transaction_id
    AND mmt.inventory_item_id            = msib.inventory_item_id
    AND MMT.ORGANIZATION_ID              = MSIB.ORGANIZATION_ID(+)
    AND MMT.ORGANIZATION_ID              = MP.ORGANIZATION_ID(+)
    ---------------------------------
    --Payables Subledger Detail-----
    --Invoice Distributions
    ---------------------------------
    UNION ALL
    SELECT /*+ INDEX(xal XLA_AE_LINES_N4) INDEX(gcc GL_CODE_COMBINATIONS_U1)*/   --added for version 1.2
      gcc.concatenated_segments,
      xah.event_type_code,
      xah.accounting_date,
      -- xah.period_name, --Commented for version 1.2
      jh.period_name, --added for version 1.2
      xal.ae_header_id,
      xah.je_category_name,
      xal.accounting_class_code,
      xal.accounted_dr,
      xal.accounted_cr,
      xal.entered_dr,
      xal.entered_cr,
      xal.unrounded_accounted_dr,
      xal.unrounded_accounted_cr,
      xal.gl_sl_link_id,
      xal.gl_sl_link_table,
      '' ref1,
      TO_CHAR(aia.invoice_num) ref2,
      '' ref3,
      '' ref4,
      '' ref5,
      '' ref6,
      '' ref7,
      '' ref8,
      aida.creation_date ref9,
      aida.last_update_date ref10
    FROM apps.gl_je_headers jh,      --added for version 1.2
      apps.gl_je_lines jl,           --added for version 1.2
      apps.gl_import_references gir, --added for version 1.2
      apps.xla_ae_lines xal,
      apps.xla_distribution_links xdl,
      apps.xla_ae_headers xah,
      apps.gl_code_combinations_kfv gcc,
      apps.ap_invoice_distributions_all aida,
      apps.ap_invoices_all aia
    WHERE jh.je_header_id                = jl.je_header_id         --added for version 1.2
    AND jh.je_source                     = 'Payables'              --added for version 1.2
    AND jh.actual_flag                   = 'A'                     --added for version 1.2
    AND jl.je_header_id                  = gir.je_header_id        --added for version 1.2
    AND jl.je_line_num                   = gir.je_line_num         --added for version 1.2
    AND gir.gl_sl_link_id                = xal.gl_sl_link_id(+)    --added for version 1.2
    AND gir.gl_sl_link_table             = xal.gl_sl_link_table(+) --added for version 1.2
    AND xal.application_id               = 200                     --added for version 1.2
    AND xal.ae_header_id                 = xdl.ae_header_id        --added for version 1.1
    AND xal.ae_line_num                  = xdl.ae_line_num
    AND xal.application_id               = xdl.application_id --added for version 1.1
    AND xah.gl_transfer_status_code      = 'Y'
    AND xal.ae_header_id                 = xah.ae_header_id
    AND xal.application_id               = xah.application_id --added for version 1.1
    AND xah.event_id                     = xdl.event_id
    AND xal.code_combination_id          = gcc.code_combination_id
    AND xdl.source_distribution_type     = 'AP_INV_DIST'
    AND xdl.event_id                     = aida.accounting_event_id(+)
    AND XDL.SOURCE_DISTRIBUTION_ID_NUM_1 = AIDA.INVOICE_DISTRIBUTION_ID(+)
    AND aida.invoice_id                  = aia.invoice_id(+)
    ---------------------------------
    --Payables Subledger Detail-----
    --Payments Distributions
    ---------------------------------
    UNION ALL
    SELECT  /*+ INDEX(xal XLA_AE_LINES_N4) INDEX(gcc GL_CODE_COMBINATIONS_U1)*/    --added for version 1.2
      gcc.concatenated_segments,
      xah.event_type_code,
      xah.accounting_date,
      -- xah.period_name, --Commented for version 1.2
      jh.period_name, --added for version 1.2
      xal.ae_header_id,
      xah.je_category_name,
      xal.accounting_class_code,
      xal.accounted_dr,
      xal.accounted_cr,
      xal.entered_dr,
      xal.entered_cr,
      xal.unrounded_accounted_dr,
      xal.unrounded_accounted_cr,
      xal.gl_sl_link_id,
      xal.gl_sl_link_table,
      '' ref1,
      TO_CHAR(aca.check_number) ref2,
      '' ref3,
      '' ref4,
      '' ref5,
      '' ref6,
      '' ref7,
      '' ref8,
      aphd.creation_date ref9,
      aphd.last_update_date ref10
    FROM apps.gl_je_headers jh,      --added for version 1.2
      apps.gl_je_lines jl,           --added for version 1.2
      apps.gl_import_references gir, --added for version 1.2
      apps.xla_ae_lines xal,
      apps.xla_distribution_links xdl,
      apps.xla_ae_headers xah,
      apps.gl_code_combinations_kfv gcc,
      apps.ap_payment_hist_dists aphd,
      apps.ap_payment_history_all apha,
      apps.ap_checks_all aca
    WHERE jh.je_header_id                = jl.je_header_id         --added for version 1.2
    AND jh.je_source                     = 'Payables'              --added for version 1.2
    AND jh.actual_flag                   = 'A'                     --added for version 1.2
    AND jl.je_header_id                  = gir.je_header_id        --added for version 1.2
    AND jl.je_line_num                   = gir.je_line_num         --added for version 1.2
    AND gir.gl_sl_link_id                = xal.gl_sl_link_id(+)    --added for version 1.2
    AND gir.gl_sl_link_table             = xal.gl_sl_link_table(+) --added for version 1.2
    AND xal.application_id               = 200                     --added for version 1.2
    AND xal.ae_header_id                 = xdl.ae_header_id        --added for version 1.1
    AND xal.ae_line_num                  = xdl.ae_line_num
    AND xal.application_id               = xdl.application_id --added for version 1.1
    AND xah.gl_transfer_status_code      = 'Y'
    AND xal.ae_header_id                 = xah.ae_header_id
    AND xal.application_id               = xah.application_id --added for version 1.1
    AND xah.event_id                     = xdl.event_id
    AND xal.code_combination_id          = gcc.code_combination_id
    AND xdl.source_distribution_type     = 'AP_PMT_DIST'
    AND xdl.event_id                     = aphd.accounting_event_id(+)
    AND xdl.source_distribution_id_num_1 = aphd.payment_hist_dist_id(+)
    AND aphd.payment_history_id          = apha.payment_history_id(+)
    AND apha.check_id                    = aca.check_id(+)
    ) A
/
	