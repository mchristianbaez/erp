CREATE OR REPLACE FORCE VIEW xxeis.xxeis_active_vendor_details
(
   vendor_name
  ,vendor_number
  ,vendor_creation
  ,vendor_inactive_date
  ,vendor_paygroup
  ,vendor_site_code
  ,vendor_site_id
  ,site_creation
  ,address_line1
  ,address_line2
  ,city
  ,state
  ,zip
  ,country
  ,site_paygroup
  ,org_id
  ,name
  ,description
  ,num_1099
  ,attribute1
  ,attribute2
  ,pay_site_flag
  ,creation_date
  ,operating_unit
)
AS
   SELECT pv.vendor_name
         ,pv.segment1 vendor_number
         ,pv.creation_date vendor_creation
         ,pv.end_date_active vendor_inactive_date
         ,pv.pay_group_lookup_code vendor_paygroup
         ,pvs.vendor_site_code
         ,pvs.vendor_site_id
         ,pvs.creation_date site_creation
         ,pvs.address_line1
         ,pvs.address_line2
         ,pvs.city
         ,pvs.state
         ,pvs.zip
         ,pvs.country
         ,pvs.pay_group_lookup_code site_paygroup
         ,pvs.org_id
         ,aptl.name
         ,aptl.description
         ,pv.num_1099
         ,pv.attribute1
         ,pv.attribute2
         ,pvs.pay_site_flag
         ,ai.creation_date
         ,hou.name operating_unit
     FROM ap.ap_suppliers pv
         ,ap.ap_supplier_sites_all pvs
         ,ap.ap_terms_tl aptl
         ,ap.ap_invoices_all ai
         ,hr_operating_units hou
    WHERE     pvs.inactive_date IS NULL
          AND pvs.vendor_id = pv.vendor_id
          AND pvs.terms_id = aptl.term_id
          AND pvs.vendor_site_id = ai.vendor_site_id(+)
          AND pvs.org_id = hou.organization_id(+);


