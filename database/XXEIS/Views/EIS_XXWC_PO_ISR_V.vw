CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_po_isr_v
(
   org
  ,pre
  ,item_number
  ,vendor_num
  ,vendor_name
  ,source
  ,st
  ,description
  ,cat
  ,pplt
  ,plt
  ,uom
  ,cl
  ,stk_flag
  ,pm
  ,minn
  ,maxn
  ,amu
  ,mf_flag
  ,hit6_sales
  ,aver_cost
  ,item_cost
  ,bpa_cost
  ,bpa
  ,qoh
  ,available
  ,availabledollar
  ,jan_sales
  ,feb_sales
  ,mar_sales
  ,apr_sales
  ,may_sales
  ,june_sales
  ,jul_sales
  ,aug_sales
  ,sep_sales
  ,oct_sales
  ,nov_sales
  ,dec_sales
  ,hit4_sales
  ,one_sales
  ,six_sales
  ,twelve_sales
  ,bin_loc
  ,mc
  ,fi_flag
  ,freeze_date
  ,res
  ,thirteen_wk_avg_inv
  ,thirteen_wk_an_cogs
  ,turns
  ,buyer
  ,ts
  ,so
  ,inventory_item_id
  ,organization_id
  ,set_of_books_id
  ,org_name
  ,district
  ,region
  ,on_ord
  ,inv_cat_seg1
  ,wt
  ,ss
  ,fml
  ,open_req
  ,sourcing_rule
  ,clt
  ,common_output_id
  ,process_id
  ,avail2
  ,int_req
  ,dir_req
  ,demand
  ,item_status_code
  ,site_vendor_num
  ,vendor_site
)
AS
   SELECT "ORG"
         ,"PRE"
         ,"ITEM_NUMBER"
         ,"VENDOR_NUM"
         ,"VENDOR_NAME"
         ,"SOURCE"
         ,"ST"
         ,"DESCRIPTION"
         ,"CAT"
         ,"PPLT"
         ,"PLT"
         ,"UOM"
         ,"CL"
         ,"STK_FLAG"
         ,"PM"
         ,"MINN"
         ,"MAXN"
         ,"AMU"
         ,"MF_FLAG"
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (ht.hit6_store_sales, 0) + NVL (oht.hit6_other_inv_sales, 0)), NVL (ht.hit6_store_sales, 0)) hit6_sales
         ,"AVER_COST"
         ,"ITEM_COST"
         ,"BPA_COST"
         ,"BPA"
         ,"QOH"
         ,"AVAILABLE"
         ,"AVAILABLEDOLLAR"
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.jan_store_sale, 0) + NVL (opt.jan_other_inv_sale, 0)), NVL (pt.jan_store_sale, 0)) jan_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.feb_store_sale, 0) + NVL (opt.feb_other_inv_sale, 0)), NVL (pt.feb_store_sale, 0)) feb_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.mar_store_sale, 0) + NVL (opt.mar_other_inv_sale, 0)), NVL (pt.mar_store_sale, 0)) mar_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.apr_store_sale, 0) + NVL (opt.apr_other_inv_sale, 0)), NVL (pt.apr_store_sale, 0)) apr_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.may_store_sale, 0) + NVL (opt.may_other_inv_sale, 0)), NVL (pt.may_store_sale, 0)) may_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.jun_store_sale, 0) + NVL (opt.jun_other_inv_sale, 0)), NVL (pt.jun_store_sale, 0)) june_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.jul_store_sale, 0) + NVL (opt.jul_other_inv_sale, 0)), NVL (pt.jul_store_sale, 0)) jul_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.aug_store_sale, 0) + NVL (opt.aug_other_inv_sale, 0)), NVL (pt.aug_store_sale, 0)) aug_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.sep_store_sale, 0) + NVL (opt.sep_other_inv_sale, 0)), NVL (pt.sep_store_sale, 0)) sep_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.oct_store_sale, 0) + NVL (opt.oct_other_inv_sale, 0)), NVL (pt.oct_store_sale, 0)) oct_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.nov_store_sale, 0) + NVL (opt.nov_other_inv_sale, 0)), NVL (pt.nov_store_sale, 0)) nov_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (pt.dec_store_sale, 0) + NVL (opt.dec_other_inv_sale, 0)), NVL (pt.dec_store_sale, 0)) dec_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (ht.hit4_store_sales, 0) + NVL (oht.hit4_other_inv_sales, 0)), NVL (ht.hit4_store_sales, 0)) hit4_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (st.one_store_sale, 0) + NVL (ost.one_other_inv_sale, 0)), NVL (st.one_store_sale, 0)) one_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (st.six_store_sale, 0) + NVL (ost.six_other_inv_sale, 0)), NVL (st.six_store_sale, 0)) six_sales
         ,DECODE (xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub, 'Yes', (NVL (st.twelve_store_sale, 0) + NVL (ost.twelve_other_inv_sale, 0)), NVL (st.twelve_store_sale, 0)) twelve_sales
         ,"BIN_LOC"
         ,"MC"
         ,"FI_FLAG"
         ,"FREEZE_DATE"
         ,"RES"
         ,"THIRTEEN_WK_AVG_INV"
         ,"THIRTEEN_WK_AN_COGS"
         ,"TURNS"
         ,"BUYER"
         ,"TS"
         ,"SO"
         ,it.inventory_item_id
         ,it.organization_id
         ,"SET_OF_BOOKS_ID"
         ,"ORG_NAME"
         ,"DISTRICT"
         ,"REGION"
         ,"ON_ORD"
         ,"INV_CAT_SEG1"
         ,wt
         ,ss
         ,fml
         ,open_req
         ,sourcing_rule
         ,clt
         ,common_output_id
         ,process_id
         ,avail2
         ,int_req
         ,dir_req
         ,demand
         ,item_status_code
         ,site_vendor_num
         ,vendor_site
     --  xxeis.eis_rs_common_outputs_s.NEXTVAL common_output_id
     FROM xxeis.eis_xxwc_po_isr_tab it
         ,xxeis.eis_xxwc_po_isr_psales_tab pt
         ,xxeis.eis_xxwc_po_isr_posales_tab opt
         ,xxeis.eis_xxwc_po_isr_hits_tab ht
         ,xxeis.eis_xxwc_po_isr_ohits_tab oht
         ,xxeis.eis_xxwc_po_isr_sales_tab st
         ,xxeis.eis_xxwc_po_isr_osales_tab ost
    WHERE     1 = 1
          AND it.organization_id = pt.organization_id(+)
          AND it.inventory_item_id = pt.inventory_item_id(+)
          AND it.organization_id = opt.organization_id(+)
          AND it.inventory_item_id = opt.inventory_item_id(+)
          AND it.organization_id = ht.organization_id(+)
          AND it.inventory_item_id = ht.inventory_item_id(+)
          AND it.organization_id = oht.organization_id(+)
          AND it.inventory_item_id = oht.inventory_item_id(+)
          AND it.organization_id = st.organization_id(+)
          AND it.inventory_item_id = st.inventory_item_id(+)
          AND it.organization_id = ost.organization_id(+)
          AND it.inventory_item_id = ost.inventory_item_id(+);
