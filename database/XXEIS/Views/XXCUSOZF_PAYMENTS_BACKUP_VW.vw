CREATE OR REPLACE FORCE VIEW xxeis.xxcusozf_payments_backup_vw
(
   business_unit
  ,payment_method
  ,name
  ,receipt_reference
  ,mvid
  ,master_vendor
  ,payment_number
  ,payment_date
  ,payment_amount
  ,currency_code
  ,creation_date
  ,image_url
  ,income_type
  ,receipt_timeframe
  ,program_year
)
AS
   SELECT hp1.party_name business_unit
         ,rm.printed_name payment_method
         ,rm.name
         ,cr.customer_receipt_reference receipt_reference
         ,hca.account_number mvid
         ,hp.party_name master_vendor
         ,cr.receipt_number payment_number
         ,cr.receipt_date payment_date
         ,cr.amount payment_amount
         ,cr.currency_code currency_code
         ,cr.creation_date creation_date
         ,cr.comments image_url
         ,DECODE (amv.description
                 ,'COOP', 'Coop'
                 ,'COOP_MIN', 'Coop'
                 ,'Rebate')
             income_type
         ,cr.attribute2 receipt_timeframe
         ,qlhv.attribute7 program_year
     FROM ar_cash_receipts_all cr
         ,ra_customer_trx_all rct
         ,apps.ozf_offers oo
         ,qp_list_headers_vl qlhv
         ,apps.ams_media_vl amv
         ,ar_receipt_methods rm
         ,hz_parties hp
         ,hz_cust_accounts hca
         ,hz_parties hp1
    WHERE     1 = 1
          AND cr.attribute1 = TO_CHAR (hp1.party_id)
          AND cr.receipt_method_id = rm.receipt_method_id
          AND rct.customer_trx_id = cr.attribute3
          AND rct.attribute5 = oo.qp_list_header_id
          AND oo.qp_list_header_id = qlhv.list_header_id
          AND amv.media_id = oo.activity_media_id
          AND rm.name = 'REBATE_DEDUCTION'
          AND hca.cust_account_id = cr.pay_from_customer
          AND hca.party_id = hp.party_id
          AND cr.org_id IN (101, 102)
          AND cr.cash_receipt_id = 117298
   UNION ALL
   SELECT hp1.party_name business_unit
         ,rm.printed_name payment_method
         ,rm.name
         ,cr.customer_receipt_reference receipt_reference
         ,hca.account_number mvid
         ,hp.party_name master_vendor
         ,cr.receipt_number payment_number
         ,cr.receipt_date payment_date
         ,cr.amount payment_amount
         ,cr.currency_code currency_code
         ,cr.creation_date creation_date
         ,cr.comments image_url
         ,NULL income_type
         ,cr.attribute2 receipt_timeframe
         ,NULL program_year
     FROM ar_cash_receipts_all cr
         ,ar_receipt_methods rm
         ,hz_parties hp
         ,hz_cust_accounts hca
         ,hz_parties hp1
    WHERE     1 = 1
          AND cr.attribute1 = TO_CHAR (hp1.party_id)
          AND cr.receipt_method_id = rm.receipt_method_id
          AND hca.cust_account_id = cr.pay_from_customer
          AND hca.party_id = hp.party_id
          AND cr.org_id IN (101, 102)
          AND rm.name = 'REBATE_CREDIT'
   ORDER BY 8;


