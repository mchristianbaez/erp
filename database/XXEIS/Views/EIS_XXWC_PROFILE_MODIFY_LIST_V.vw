/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_PROFILE_MODIFY_LIST_V.vw $
  Module Name: EiS eXpress Administrator
  PURPOSE: Profile Options Modifications Listing - WC
  TMS Task Id : 20170607-00033 
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 8/08/2017	 Siva 					Initial Version -- TMS#20170807-00241 
**************************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_PROFILE_MODIFY_LIST_V (APPLICATION_SHORT_NAME, APPLICATION_NAME, PROFILE_OPTION_NAME, USER_PROFILE_OPTION_NAME, LEVEL_TYPE, LEVEL_VALUE, DETAILS, PROFILE_OPTION_VALUE, LAST_UPDATE_DATE, LAST_UPDATED_BY, LAST_UPDATED_BY_NAME)
AS
  SELECT i.application_short_name,
    j.application_name,
    a.profile_option_name,
    a.user_profile_option_name,
    DECODE(b.level_id, 10001, 'Site', 10002, 'Application', 10003, 'Responsibility', 10004, 'User', 10005, 'Server', 10006, 'Org', '???') level_type,
    b.level_value,
    DECODE(b.level_id, 10001, '', 10002, d.application_short_name, 10003, e.responsibility_key, 10004, c.user_name, 10005, f.node_name, 10006, g.name) details,
    b.profile_option_value,
    b.last_update_date,
    h.user_name last_updated_by,
    h.description last_updated_by_name
  FROM apps.fnd_profile_options_vl a,
    applsys.fnd_profile_option_values b,
    applsys.fnd_user c,
    applsys.fnd_application d,
    applsys.fnd_responsibility e,
    applsys.fnd_nodes f,
    hr.hr_all_organization_units g,
    applsys.fnd_user h,
    applsys.fnd_application i,
    applsys.fnd_application_tl j
  WHERE a.application_id           = b.application_id(+)
  AND a.profile_option_id          = b.profile_option_id(+)
  AND b.level_value                = c.user_id(+)
  AND b.level_value                = d.application_id(+)
  AND b.level_value                = e.responsibility_id(+)
  AND b.level_value_application_id = e.application_id(+)
  AND b.level_value                = f.node_id(+)
  AND b.level_value                = g.organization_id(+)
  AND b.last_updated_by            = h.user_id(+)
  AND a.application_id             = i.application_id(+)
  AND a.application_id             = j.application_id(+)
  AND j.language (+)               = userenv('LANG')
  AND a.profile_option_name       IN( 'CST_ALLOW_EARLY_PERIOD_CLOSE', 'CST_UPDATE_COSTS', 'XXWC_INV_AGE_DAYS1', 'XXWC_INV_AGE_DAYS2', 'XXWC_INV_AGE_DAYS3',
  'XXWC_INV_FA_ACCOUNT_ALIAS', 'XXWC_AR_MAX_CASH_REFUND', 'XXWC_FREIGHT_COSTING_HOOK_ENABLED', 'XXWC_BT_RECEIPT_IB_PATH', 'XXWC_CATEGORY_IMAGE_PATH',
  'XXWC_OM_CC_REFUND_ACTIVITY', 'XXWC_PAYMENTTERMS_SCHED', 'XXWC_FULL_IMAGE_PROD', 'XXWC_OM_REFUND_REQ_EMAIL', 'XXWC_WCAPPS_APEX_URL', 
  'CSD_ALLOW_ACT_OVERRIDE_CHARGE', 'CSD_ALLOW_CHARGE_OVERRIDE', 'CSD_ALLOW_PRICE_OVERRIDE', 'GLDI_FORCE_JOURNAL_BALANCE', 'GL_REVAL_INC_ACC_RULE', 
  'INV:EXPENSE_TO_ASSET_TRANSFER', 'INV_ALLOW_CC_TXNS_ONHAND_NEG', 'INV_OVERRIDE_RSV_FOR_BACKFLUSH', 'INV_USE_QP_FOR_INTERCOMPANY', 'INV_USE_QP_FOR_INTERORG',
  'OM_OVER_SHIPMENT_TOLERANCE', 'ONT_NEGATIVE_PRICING', 'ONT_UNIT_PRICE_PRECISION_TYPE', 'OM_UNDER_SHIPMENT_TOLERANCE', 'PO_AUTOCREATE_DATE', 
  'PO_VALIDATE_GL_PERIOD', 'PO_PRICE_UPDATE_TOLERANCE', 'AR_ALLOW_SALESCREDIT_UPDATE', 'AR_CHANGE_CUST_NAME', 'AR_JG_CREATE_BANK_CHARGES', 
  'AR_UPDATE_DUE_DATE', 'AR_ADJUST_CREDIT_UNCONFIRMED_INVOICE', 'OIR_AGING_BUCKETS', 'XLA_USE_TRANSACTION_SECURITY' )
/
