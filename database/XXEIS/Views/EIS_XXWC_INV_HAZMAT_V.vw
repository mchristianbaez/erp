CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_INV_HAZMAT_V" ("LOCATION", "PART_NUMBER", "DESCRIPTION", "CAPROP65", "CA_PROP_65","PESTICIDE_FLAG", "VOCNUMBER", "PESTICIDE_FLAG_STATE", "VOC_CATEGORY", "VOC_SUB_CATEGORY", "MSDS_NUMBER", "HAZMAT_FLAG", "WEIGHT", "UN_NUMBER", "UN_DESCRIPTION", "HAZARD_CLASS", "HAZARDOUS_MATERIAL_DESCRIPTION", "DOT_PROPER_SHIPPING_NAME", "CONTAINER_TYPE", "ORMD", "PACKING_GROUP", "PACKAGING_GROUP", "COUNTRY_OF_ORIGIN", "UOM", "ONHAND", "PERIOD_NAME", "MINIMUM", "MAXIMUM", "PRIMARY_BIN_LOC", "ALTERNATE_BIN_LOC", "SHELF_LIFE_DAYS", "INVENTORY_ITEM_ID", "INV_ORGANIZATION_ID", "ORG_ORGANIZATION_ID", "MSI#HDS#LOB", "MSI#HDS#DROP_SHIPMENT", "MSI#HDS#INVOICE_UOM", "MSI#HDS#PRODUCT_ID", "MSI#HDS#VENDOR_PART_NUMBER", "MSI#HDS#UNSPSC_CODE", "MSI#HDS#UPC_PRIMARY", "MSI#HDS#SKU_DESCRIPTION", "MSI#WC#CA_PROP_65", "MSI#WC#COUNTRY_OF_ORIGIN", "MSI#WC#ORM_D_FLAG", "MSI#WC#STORE_VELOCITY", "MSI#WC#DC_VELOCITY", "MSI#WC#YEARLY_STORE_VELOCITY", "MSI#WC#YEARLY_DC_VELOCITY", "MSI#WC#PRISM_PART_NUMBER", "MSI#WC#HAZMAT_DESCRIPTION",
  "MSI#WC#HAZMAT_CONTAINER", "MSI#WC#GTP_INDICATOR", "MSI#WC#LAST_LEAD_TIME", "MSI#WC#AMU", "MSI#WC#RESERVE_STOCK", "MSI#WC#TAXWARE_CODE", "MSI#WC#AVERAGE_UNITS", "MSI#WC#PRODUCT_CODE", "MSI#WC#IMPORT_DUTY_", "MSI#WC#KEEP_ITEM_ACTIVE", "MSI#WC#PESTICIDE_FLAG", "MSI#WC#CALC_LEAD_TIME", "MSI#WC#VOC_GL", "MSI#WC#PESTICIDE_FLAG_STATE", "MSI#WC#VOC_CATEGORY", "MSI#WC#VOC_SUB_CATEGORY", "MSI#WC#MSDS_#", "MSI#WC#HAZMAT_PACKAGING_GROU", "MSI#HDS#DROP_SHIPMENT_ELIGAB",
  "LIMITED_QTY_FLAG","VOC_CONTENT", "MSDS")
  /**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_HAZMAT_V.vw $
  Module Name: Inventory
  PURPOSE: View for EIS Report Hazmat Report
  TMS Task Id : NA
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        09/18/2015  Mahender Reddy         Initial Version
  1.1        01/19/2016  Mahender Reddy        TMS# 51210-00028
  **************************************************************************************************************/
AS
  SELECT ood.organization_code location,
    msi.segment1 part_number,
    msi.description,
    msi.attribute1 caprop65,
	emib.CA_PROP_65 CA_PROP_65, --Added for Ver 1.1
    --msi.attribute3 pesticide_flag,  --Commented for Ver 1.1
	emib.Pesticide_Flag Pesticide_Flag,--Added for Ver 1.1
    msi.attribute4 vocnumber,
    --msi.attribute5 pesticide_flag_state,  --Commented for Ver 1.1
	emib.Pesticide_Flag_State Pesticide_Flag_State,--Added for Ver 1.1
   -- msi.attribute6 voc_category,  --Commented for Ver 1.1
	emib.VOC_CATEGORY VOC_CATEGORY, --Added for Ver 1.1
    --msi.attribute7 voc_sub_category,  --Commented for Ver 1.1
	emib.VOC_Sub_Category VOC_Sub_Category, --Added for Ver 1.1
    msi.attribute8 msds_number,
    msi.hazardous_material_flag hazmat_flag,
    msi.unit_weight weight,
    pun.un_number un_number,
    pun.description un_description,
    phc.hazard_class hazard_class,
    msi.attribute17 hazardous_material_description,
	emib.DOT_PROPER_SHIPPING_NAME DOT_PROPER_SHIPPING_NAME,--Added for Ver 1.1
    --msi.attribute18 container_type,  --Commented for Ver 1.1
	emib.container_type container_type, --Added for Ver 1.1
    msi.attribute11 ormd,
    msi.attribute9 packing_group,
	emib.PACKAGING_GROUP PACKAGING_GROUP, --Added for Ver 1.1	
    msi.attribute10 Country_of_Origin,
    msi.PRIMARY_UOM_CODE UOM,
    xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv(msi.inventory_item_id,msi.organization_id) onhand,
    gps.period_name,
    msi.min_minmax_quantity minimum,
    msi.max_minmax_quantity maximum,
    xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc(msi.inventory_item_id,msi.organization_id) primary_bin_loc,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ALTERNATE_BIN_LOC(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) ALTERNATE_BIN_LOC,
    shelf_life_days shelf_life_days,
    --msi.hazard_class_id hazard_class_id,
    --primary keys
    msi.inventory_item_id,
    msi.organization_id inv_organization_id,
    ood.organization_id org_organization_id
    --descr#flexfield#start
    ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE1, NULL) MSI#HDS#LOB ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE10,'F'), NULL) MSI#HDS#Drop_Shipment ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE15, NULL) MSI#HDS#Invoice_UOM ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE2, NULL) MSI#HDS#Product_ID ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE3, NULL) MSI#HDS#Vendor_Part_Number ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE4, NULL) MSI#HDS#UNSPSC_Code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE5, NULL) MSI#HDS#UPC_Primary ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE7, NULL) MSI#HDS#SKU_Description ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE1, NULL) MSI#WC#CA_Prop_65 ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE10, NULL) MSI#WC#Country_of_Origin ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE11,'F'), NULL) MSI#WC#ORM_D_Flag ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE12, NULL) MSI#WC#Store_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE13, NULL) MSI#WC#DC_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE14, NULL) MSI#WC#Yearly_Store_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE15, NULL) MSI#WC#Yearly_DC_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE16, NULL) MSI#WC#PRISM_Part_Number ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE17, NULL) MSI#WC#Hazmat_Description ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC HAZMAT CONTAINER',MSI.ATTRIBUTE18,'I'), NULL) MSI#WC#Hazmat_Container ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE19, NULL) MSI#WC#GTP_Indicator ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE2, NULL) MSI#WC#Last_Lead_Time ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE20, NULL) MSI#WC#AMU ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE21, NULL) MSI#WC#Reserve_Stock ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC TAXWARE CODE',MSI.ATTRIBUTE22,'I'), NULL) MSI#WC#Taxware_Code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE25, NULL) MSI#WC#Average_Units ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE26, NULL) MSI#WC#Product_code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE27, NULL) MSI#WC#Import_Duty_ ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE29,'F'), NULL) MSI#WC#Keep_Item_Active ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE3,'F'), NULL) MSI#WC#Pesticide_Flag ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE30, NULL) MSI#WC#Calc_Lead_Time ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE4, NULL) MSI#WC#VOC_GL ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE5, NULL) MSI#WC#Pesticide_Flag_State ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE6, NULL) MSI#WC#VOC_Category ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE7, NULL) MSI#WC#VOC_Sub_Category ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE8, NULL) MSI#WC#MSDS_# ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC_HAZMAT_PACKAGE_GROUP',MSI.ATTRIBUTE9,'I'), NULL) MSI#WC#Hazmat_Packaging_Grou ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE10,'F'), NULL) MSI#HDS#Drop_Shipment_Eligab,
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
	emib.Limited_Qty_Flag LIMITED_QTY_FLAG, --Added for Ver 1.1
	emib.VOC_CONTENT VOC_CONTENT, --Added for Ver 1.1
	emib.MSDS MSDS --Added for Ver 1.1
  FROM MTL_SYSTEM_ITEMS_KFV MSI,
    ORG_ORGANIZATION_DEFINITIONS OOD,
    gl_period_statuses gps,
    po_hazard_classes phc,
    po_un_numbers pun,
	(select emsi.inventory_item_id,
                emsi.organization_id,
		emsi.C_EXT_ATTR2 PACKAGING_GROUP,
		--emsi.C_EXT_ATTR3 DOT_PROPER_SHIPPING_NAME,
		emsit.TL_EXT_ATTR1 DOT_Proper_Shipping_Name,
		emsi.C_EXT_ATTR4 Limited_Qty_Flag,
		emsi.C_EXT_ATTR5 Container_Type,
		emsi.C_EXT_ATTR6 CA_PROP_65,
		emsi.C_EXT_ATTR7 VOC_CONTENT,
		emsi.C_EXT_ATTR8 VOC_CATEGORY,
		emsi.C_EXT_ATTR9 VOC_Sub_Category,
		emsi.C_EXT_ATTR10 Pesticide_Flag,
		emsi.C_EXT_ATTR11 Pesticide_Flag_State,
		emsi.C_EXT_ATTR12 MSDS		
      from apps.EGO_MTL_SY_ITEMS_EXT_B emsi,
	  apps.ego_mtl_sy_items_ext_tl emsit
      where emsi.organization_id = emsit.organization_id
		AND emsi.inventory_item_id = emsit.inventory_item_id(+)
		AND emsi.attr_group_id = emsit.attr_group_id
		AND emsi.attr_group_id = 1021
        and emsi.organization_id = 222) emib -- Added for Ver 1.1
  WHERE 1                        =1
  AND msi.organization_id        =ood.organization_id
  AND msi.inventory_item_id        =emib.inventory_item_id -- Added for Ver 1.1
  AND MSI.ATTRIBUTE_CATEGORY     ='WC'
  AND msi.hazardous_material_flag='Y'
  AND msi.hazard_class_id        = phc.hazard_class_id
  AND msi.un_number_id           = pun.un_number_id (+)
  AND gps.application_id         =101
  AND gps.set_of_books_id        =ood.set_of_books_id
  AND TRUNC(SYSDATE) BETWEEN gps.start_date AND gps.end_date
  AND EXISTS
    (SELECT 1
    FROM XXEIS.EIS_ORG_ACCESS_V
    WHERE ORGANIZATION_ID = MSI.ORGANIZATION_ID
    )
/