/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_DMS_TRUCK_DELIVERY_V.vw $
  Module Name: Order Management
  PURPOSE: DMS Data Metrics for Truck Deliveries - WC
  TMS Task Id : 20170607-00033 
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 7/08/2017	 Siva 					Initial Version -- TMS#20170607-00033
**************************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_DMS_TRUCK_DELIVERY_V (REGION, DISTRICT, BRANCH, ASSOCIATE_NAME, ORDER_NUMBER, ORDER_TYPE, CUSTOMER_NUMBER, CUSTOMER_NAME, SHIP_METHOD, SHIPMENT_DATE, ORDER_CREATION_DATE, ORDER_TYPE_ID, OOH_HEADER_ID, OOD_ORGANIZATION_ID, HCA_CUST_ACCOUNT_ID, HZ_PARTY_ID)
AS
  SELECT ood.attribute9 region,
    ood.attribute8 district,
    ood.organization_code branch,
    (SELECT ppf.full_name
    FROM fnd_user fu,
      per_all_people_f ppf
    WHERE fu.user_id   =ooh.created_by
    AND fu.employee_id =ppf.person_id
    AND TRUNC(sysdate) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
    ) associate_name,
    ooh.order_number,
    oot.name order_type,
    hca.account_number customer_number,
    hz.party_name customer_name,
    (SELECT flv.meaning
    FROM apps.fnd_lookup_values flv,
      apps.oe_order_lines_all ool
    WHERE flv.lookup_type ='XXWC_DMS_SHIPPING_METHOD_CODE'
    AND flv.meaning       =ool.shipping_method_code
    AND ool.header_id     =ooh.header_id
    AND rownum            =1
    ) ship_method,
    TRUNC(
    (SELECT MAX(actual_shipment_date)
    FROM oe_order_lines_all ool
    WHERE ool.header_id=ooh.header_id
    )) shipment_date,
    TRUNC(ooh.creation_date) order_creation_date,
    ooh.order_type_id,
    --Primary Keys
    ooh.header_id ooh_header_id,
    ood.organization_id ood_organization_id,
    hca.cust_account_id hca_cust_account_id,
    hz.party_id hz_party_id
    --descr#flexfield#start
    --descr#flexfield#end
  FROM apps.oe_order_headers_all ooh,
    apps.oe_transaction_types_tl oot,
    apps.mtl_parameters ood,
    apps.hz_cust_accounts hca,
    apps.hz_parties hz
  WHERE ooh.order_type_id NOT IN(1011,1006)
  AND ooh.flow_status_code     ='CLOSED'
  AND ooh.order_type_id        = oot.transaction_type_id
  AND oot.language             = userenv('LANG')
  AND ooh.ship_from_org_id     =ood.organization_id
  AND ooh.sold_to_org_id       = hca.cust_account_id
  AND hca.party_id             = hz.party_id
  AND EXISTS
    (SELECT 1
    FROM apps.oe_order_lines_all ool
    WHERE ool.header_id     =ooh.header_id
    AND ool.source_type_code='INTERNAL'
    AND ool.flow_status_code='CLOSED'
    )
  AND EXISTS
    (SELECT 1
    FROM apps.fnd_lookup_values flv,
      apps.oe_order_lines_all ool1
    WHERE lookup_type ='XXWC_DMS_SHIPPING_METHOD_CODE'
    AND meaning       =ool1.shipping_method_code
    AND ool1.header_id=ooh.header_id
    )
  AND NOT EXISTS
    (SELECT 1
    FROM xxwc.xxwc_om_dms_ship_confirm_bkp scb
    WHERE scb.order_number =ooh.order_number
    )
  AND NOT EXISTS
    (SELECT 1
    FROM xxwc.xxwc_om_dms_ship_confirm_tbl xdsc
    WHERE xdsc.order_number =ooh.order_number
    )
/

