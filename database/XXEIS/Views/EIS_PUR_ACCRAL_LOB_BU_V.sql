---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_PUR_ACCRAL_LOB_BU_V $
  PURPOSE	  : PURCHASES and ACCRUALS - by Vendor, Agreement Year, Cal Year,Cal Month, LOB, BU(NEW)
  TMS Task Id : TMS#20160411-00105  
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Pramod   		 TMS#20160411-00105  Performance Tuning
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_PUR_ACCRAL_LOB_BU_V;

CREATE OR REPLACE VIEW XXEIS.EIS_PUR_ACCRAL_LOB_BU_V (PROCESS_ID, AGREEMENT_YEAR, MVID, VENDOR_NAME, LOB, BU, CAL_YEAR, CAL_MONTH, CALENDAR_PURCHASES, ACCRUAL_PURCHASES, REBATE, COOP, TOTAL_ACCRUALS)
AS
  SELECT process_id,  
    AGREEMENT_YEAR,
    MVID,
    VENDOR_NAME,
    LOB,
    BU,
    CAL_YEAR ,
    CAL_MONTH,
    SUM(CALENDAR_PURCHASES)CALENDAR_PURCHASES,
    SUM(ACCRUAL_PURCHASES) ACCRUAL_PURCHASES,
    SUM(REBATE) REBATE,
    SUM(COOP) COOP,
    SUM(TOTAL_ACCRUALS) TOTAL_ACCRUALS
  FROM
    (SELECT process_id,
      AGREEMENT_YEAR ,
      MVID ,
      VENDOR_NAME ,
      LOB ,
      BU ,
      CAL_YEAR ,
      CAL_MONTH ,
      CALENDAR_PURCHASES,
      ACCRUAL_PURCHASES ,
      REBATE ,
      COOP ,
      TOTAL_ACCRUALS
    FROM xxeis.EIS_XXWC_ACCR_LOB_BU_TAB
    )
  GROUP BY process_id,
    AGREEMENT_YEAR,
    MVID,
    VENDOR_NAME,
    LOB,
    BU,
    CAL_YEAR ,
    CAL_MONTH
/
