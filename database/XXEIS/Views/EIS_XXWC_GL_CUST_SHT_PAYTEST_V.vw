/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_GL_CUST_SHT_PAYTEST_V $
  Module Name : General Ledger
  PURPOSE	  : Customer Short Pay Report
  TMS Task Id : 20160426-00100 
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     22-Apr-2016        Siva   		 --TMS#20160426-00100   Performance Tuning
  1.1	  07-Jul-2016		 Siva   		 --TMS#20160606-00037 
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_GL_CUST_SHT_PAYTEST_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_GL_CUST_SHT_PAYTEST_V (CREDIT_ANALYST, CUSTOMER_NUMBER, CUSTOMER_NAME, ORDER_NUMBER, INVOICE_DATE, INVOICE_NUMBER, AMOUNT_APPLIED, AMOUNT_PAID, RECEIPT_DATE, PERIOD_NAME, CUSTOMER_TRX_ID)
AS
  SELECT /*+ USE_NL(ara,ct) USE_NL(ct,aps) USE_NL(ct,hca)*/ --added for version 1.1
    jrdv.resource_name credit_analyst,
    hca.account_number customer_number,
    hca.account_name customer_name,
    --ct.interface_header_attribute1 order_number,
    --acr.receipt_number,
    ct.ct_reference order_number,
    ct.trx_date invoice_date,
    ct.trx_number invoice_number,
    SUM(aps.amount_due_original) amount_applied,
    SUM(aps.amount_due_original-aps.amount_due_remaining) amount_paid,
    -- MAX(acr.receipt_date) receipt_date,
    -- XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_RECEIPT_DATE_PERIOD(CT.CUSTOMER_TRX_ID) RECEIPT_DATE,
    -- XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_TRX_PERIOD_NAME(CT.CUSTOMER_TRX_ID) period_name,
    MAX (acr.receipt_date) receipt_date, --TMS# 20160426-00100
    gp.period_name period_name,  --TMS# 20160426-00100
    -- xxeis.eis_rs_xxwc_com_util_pkg.get_period_name(ara.set_of_books_id,ara.gl_date) period_name,
    --primary keys
    ct.customer_trx_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
    /* commented by srinivas
    FROM hz_cust_accounts hca,
    ra_customer_trx ct,
    ar_receivable_applications ara,
    ar_cash_receipts acr,
    ar_payment_schedules aps,
    (SELECT MAX(credit_analyst_id) credit_analyst,
    cust_Account_id
    FROM hz_customer_profiles
    GROUP BY cust_Account_id
    ) hzp,
    jtf_rs_defresources_v jrdv
    WHERE ct.bill_to_customer_id = hca.cust_account_id
    AND ct.customer_trx_id       =ara.applied_customer_trx_id
    AND ara.display              ='Y'
    AND ara.status               ='APP'
    AND ara.application_type     ='CASH'
    AND ara.cash_receipt_id      =acr.cash_receipt_id
    --AND ct.trx_number IN('121050028-00','121052509-00')
    AND hzp.cust_account_id = hca.cust_account_id
    AND hzp.credit_analyst  = jrdv.resource_id
    AND aps.customer_trx_id = ct.customer_trx_id*/
  FROM
    (SELECT  /*+ INDEX(arra XXWC_AR_RECEIVABLE_APPL_N9)*/		--added for version 1.1
      MAX(arra.receivable_application_id) receivable_application_id,
      MAX(acrc.cash_receipt_id) cash_receipt_id
    FROM ar_receivable_applications arra,
      ar_cash_receipts acrc
    WHERE arra.display        = 'Y'
    AND arra.status           = 'APP'
    AND arra.application_type = 'CASH'
    AND arra.cash_receipt_id  = acrc.cash_receipt_id
    GROUP BY arra.applied_customer_trx_id
    ) arcr,   --TMS# 20160426-00100 added by siva on 4/22/16
    ar_receivable_applications ara, --TMS# 20160426-00100
    ra_customer_trx ct,
    ar_payment_schedules aps,
    hz_cust_accounts hca,
    gl_periods gp,
    (SELECT  /*+ INDEX(hcp HZ_CUSTOMER_PROFILES_N1)*/  --TMS# 20160426-00100
      MAX(hcp.credit_analyst_id) credit_analyst,
      hcp.cust_account_id
    FROM hz_customer_profiles hcp
    GROUP BY hcp.cust_account_id
    ) hzp,
    jtf_rs_resource_extns_vl jrdv,
    ar_cash_receipts acr  --TMS# 20160426-00100
  WHERE arcr.receivable_application_id =ara.receivable_application_id
  AND arcr.cash_receipt_id             = acr.cash_receipt_id
    --  and arcr.cash_receipt_id  =  ara.cash_receipt_id
    --  ara.display          = 'Y'
    --  AND ara.status             = 'APP'
    --  AND ara.application_type   = 'CASH'
  AND gp.period_set_name     ='4-4-QTR'
  AND ct.customer_trx_id     =ara.applied_customer_trx_id
  AND aps.customer_trx_id    = ct.customer_trx_id
  AND ct.bill_to_customer_id = hca.cust_account_id
  AND hzp.cust_account_id    = hca.cust_account_id
  AND hzp.credit_analyst     = jrdv.resource_id
    --AND ct.trx_number='213060773-00'
    --  AND gp.period_name ='Jun-2012'
  AND TRUNC(ara.gl_date) BETWEEN TRUNC(gp.start_date) AND TRUNC(gp.end_date)
  GROUP BY jrdv.resource_name,
    hca.account_number,
    hca.account_name,
    ct.ct_reference,
    ct.trx_date,
    ct.trx_number,
    period_name,
    ct.customer_trx_id
/
