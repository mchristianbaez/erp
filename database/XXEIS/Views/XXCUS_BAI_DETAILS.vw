CREATE OR REPLACE FORCE VIEW xxeis.xxcus_bai_details
(
   bank_account_num
  ,trx_code
  ,description
  ,line_number
  ,trx_date
  ,trx_type
  ,amount
  ,trx_text
  ,bank_trx_number
  ,customer_text
  ,invoice_text
  ,bank_account_text
  ,reference_txt
  ,je_status_flag
  ,accounting_date
  ,segment1
  ,segment2
  ,segment3
  ,segment4
  ,segment5
  ,segment6
)
AS
   SELECT ba.bank_account_num
         ,tr.trx_code
         ,tr.description
         ,li.line_number
         ,li.trx_date
         ,li.trx_type
         ,li.amount
         ,li.trx_text
         ,li.bank_trx_number
         ,li.customer_text
         ,li.invoice_text
         ,li.bank_account_text
         ,li.reference_txt
         ,li.je_status_flag
         ,li.accounting_date
         ,g.segment1
         ,g.segment2
         ,g.segment3
         ,g.segment4
         ,g.segment5
         ,g.segment6
     FROM ce.ce_statement_lines li
         ,ce.ce_statement_headers hd
         ,ce.ce_bank_accounts ba
         ,ce.ce_cashflows ca
         ,gl.gl_code_combinations g
         ,(  SELECT trx_code, description, bank_account_id
               FROM ce.ce_transaction_codes tr
           GROUP BY trx_code, description, bank_account_id) tr
    WHERE     li.statement_header_id = hd.statement_header_id
          AND hd.bank_account_id = ba.bank_account_id
          AND li.trx_code = tr.trx_code
          AND ba.bank_account_id = tr.bank_account_id
          AND ca.cashflow_id = li.cashflow_id
          AND ca.offset_ccid = g.code_combination_id;


