CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_gl_intercomp_v
(
   customer_number
  ,customer_name
  ,gl_date
  ,invoice_number
  ,invoice_date
  ,due_date
  ,invoice_amount
  ,age
  ,total
  ,bucket_current
  ,bucket_1_to_30
  ,bucket_31_to_60
  ,bucket_61_to_90
  ,bucket_91_to_180
  ,bucket_181_to_360
  ,bucket_361_days_and_above
  ,profile_class
  ,period_name
  ,application_id
  ,set_of_books_id
  ,territory_id
  ,cust_account_id
  ,party_id
  ,customer_trx_id
  ,payment_schedule_id
  ,hca#party_type
  ,hca#vndr_code_and_fruloc
  ,hca#branch_description
  ,hca#customer_source
  ,hca#legal_collection
  ,hca#collection_agency
  ,hca#yes#note_line_#5
  ,hca#yes#note_line_#1
  ,hca#yes#note_line_#2
  ,hca#yes#note_line_#3
  ,hca#yes#note_line_#4
  ,party#party_type
  ,party#gvid_id
  ,hca#legal_collection_indicat
  ,hca#prism_number
  ,ct#162#prism_data___do_not_u
  ,ct#162#bill_trust_invoice_pr
  ,ct#do_not_use___internal_1
  ,ct#do_not_use___internal_2
  ,ct#do_not_use___internal_3
)
AS
   SELECT hca.account_number customer_number
         ,party.party_name customer_name
         ,ps.gl_date
         ,ct.trx_number invoice_number
         ,ct.trx_date invoice_date
         ,arpt_sql_func_util.get_first_real_due_date (ct.customer_trx_id
                                                     ,ct.term_id
                                                     ,ct.trx_date)
             due_date
         ,ps.acctd_amount_due_remaining invoice_amount
         ,CEIL (xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date - ps.due_date)
             age
         ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
             ps.payment_schedule_id
            ,ps.due_date
            ,NULL
            ,NULL
            , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
            ,ps.amount_applied
            ,ps.amount_credited
            ,ps.amount_adjusted
            ,ps.class                                                   --null
                     )
             total
         ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
             ps.payment_schedule_id
            ,ps.due_date
            ,NULL
            ,0
            , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
            ,ps.amount_applied
            ,ps.amount_credited
            ,ps.amount_adjusted
            ,ps.class                                                   --null
                     )
             bucket_current
         ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
             ps.payment_schedule_id
            ,ps.due_date
            ,1
            ,30
            , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
            ,ps.amount_applied
            ,ps.amount_credited
            ,ps.amount_adjusted
            ,ps.class                                                   --null
                     )
             bucket_1_to_30
         ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
             ps.payment_schedule_id
            ,ps.due_date
            ,31
            ,60
            , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
            ,ps.amount_applied
            ,ps.amount_credited
            ,ps.amount_adjusted
            ,ps.class                                                   --null
                     )
             bucket_31_to_60
         ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
             ps.payment_schedule_id
            ,ps.due_date
            ,61
            ,90
            , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
            ,ps.amount_applied
            ,ps.amount_credited
            ,ps.amount_adjusted
            ,ps.class                                                   --null
                     )
             bucket_61_to_90
         ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
             ps.payment_schedule_id
            ,ps.due_date
            ,91
            ,180
            , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
            ,ps.amount_applied
            ,ps.amount_credited
            ,ps.amount_adjusted
            ,ps.class                                                   --null
                     )
             bucket_91_to_180
         ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
             ps.payment_schedule_id
            ,ps.due_date
            ,181
            ,360
            , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
            ,ps.amount_applied
            ,ps.amount_credited
            ,ps.amount_adjusted
            ,ps.class                                                   --null
                     )
             bucket_181_to_360
         ,xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
             ps.payment_schedule_id
            ,ps.due_date
            ,361
            ,9999999
            , (ps.amount_due_original) * NVL (ps.exchange_rate, 1)
            ,ps.amount_applied
            ,ps.amount_credited
            ,ps.amount_adjusted
            ,ps.class                                                   --null
                     )
             bucket_361_days_and_above
         ,'Intercompany Customers' profile_class
         ,gps.period_name
         ,gps.application_id
         ,gps.set_of_books_id
         ,rt.territory_id
         ,hca.cust_account_id
         ,party.party_id
         ,ct.customer_trx_id
         ,ps.payment_schedule_id
         --descr#flexfield#start

         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_PARTY_TYPE'
                                           ,hca.attribute1
                                           ,'I')
             hca#party_type
         ,hca.attribute2 hca#vndr_code_and_fruloc
         ,hca.attribute3 hca#branch_description
         ,hca.attribute4 hca#customer_source
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_LEGAL_COLLECTION'
                                           ,hca.attribute5
                                           ,'F')
             hca#legal_collection
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_LEGAL_AGENCY'
                                           ,hca.attribute6
                                           ,'F')
             hca#collection_agency
         ,DECODE (hca.attribute_category, 'Yes', hca.attribute16, NULL)
             hca#yes#note_line_#5
         ,DECODE (hca.attribute_category, 'Yes', hca.attribute17, NULL)
             hca#yes#note_line_#1
         ,DECODE (hca.attribute_category, 'Yes', hca.attribute18, NULL)
             hca#yes#note_line_#2
         ,DECODE (hca.attribute_category, 'Yes', hca.attribute19, NULL)
             hca#yes#note_line_#3
         ,DECODE (hca.attribute_category, 'Yes', hca.attribute20, NULL)
             hca#yes#note_line_#4
         ,xxeis.eis_rs_dff.decode_valueset ('XXCUS_PARTY_TYPE'
                                           ,party.attribute1
                                           ,'I')
             party#party_type
         ,party.attribute2 party#gvid_id
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_LEGAL_COLLECTION'
                                           ,hca.attribute5
                                           ,'F')
             hca#legal_collection_indicat
         ,hca.attribute6 hca#prism_number
         ,DECODE (ct.attribute_category, '162', ct.attribute14, NULL)
             ct#162#prism_data___do_not_u
         ,DECODE (ct.attribute_category, '162', ct.attribute15, NULL)
             ct#162#bill_trust_invoice_pr
         ,ct.attribute1 ct#do_not_use___internal_1
         ,ct.attribute2 ct#do_not_use___internal_2
         ,ct.attribute3 ct#do_not_use___internal_3
     --descr#flexfield#end


     --gl#accountff#start
     --gl#accountff#end
     FROM hz_cust_accounts hca
         ,hz_parties party
         ,ra_customer_trx ct
         ,ra_territories rt
         ,ar_payment_schedules ps
         ,hr_operating_units hou
         ,gl_period_statuses gps
    WHERE     1 = 1
          AND hca.party_id = party.party_id
          AND ct.bill_to_customer_id = hca.cust_account_id
          AND ct.territory_id = rt.territory_id(+)
          AND ps.customer_trx_id = ct.customer_trx_id
          AND ct.org_id = hou.organization_id
          AND gps.application_id = 101
          AND gps.set_of_books_id = hou.set_of_books_id
          AND EXISTS
                 (SELECT 1
                    FROM hz_customer_profiles hcp
                        ,hz_cust_profile_classes hcpc
                   WHERE     1 = 1
                         AND hca.cust_account_id = hcp.cust_account_id
                         AND hcp.profile_class_id = hcpc.profile_class_id
                         AND hcpc.name = 'Intercompany Customers')
          AND TRUNC (ps.gl_date) BETWEEN gps.start_date AND gps.end_date
          AND TRUNC (ps.gl_date) <=
                 xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date;


