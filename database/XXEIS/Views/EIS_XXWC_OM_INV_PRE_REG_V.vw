---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_INV_PRE_REG_V $
  Module Name : Order Management
  PURPOSE	  : Invoice Pre-Register report
  TMS Task Id : 20150921-00332 
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva   		 TMS#20150921-00332   Performance Tuning
  1.1 	  03-05-2016         Siva			 TMS#20150810-00058 
  1.2     06-08-2016		 Siva  			 TMS#20160606-00009
  1.3     06-08-2016		 Siva  			 TMS#20160609-00097  
  1.4	  07-08-2016		 Siva			 TMS#20160630-00004
  1.5	  07-Dec-2017        Siva		     TMS#20171206-00013   
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_OM_INV_PRE_REG_V;

/* Formatted on 5/3/2016 4:13:51 PM (QP5 v5.265.14096.38000) */
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_INV_PRE_REG_V
(
   ORDER_NUMBER,
   ORDER_SOURCE,
   JOB_NUMBER,
   ORDERED_DATE,
   CREATED_BY,
   ORDER_TYPE,
   ORDER_STATUS,
   CUSTOMER_NUMBER,
   CUSTOMER_NAME,
   GROSS_AMOUNT,
   DISCOUNT_AMT,
   TAX_VALUE,
   FREIGHT_AMT,
   AVERAGE_COST,
   PROFIT,
   SALESREP_NAME,
   TRX_NUMBER,
   SALESREP_NUMBER,
   WAREHOUSE,
   INV_AMOUNT,
   SALES,
   SALE_COST,
   PAYMENT_TERM,
   PAYMENT_DESC,
   ITEM,
   ITEM_DESC,
   QTY,
   UOM,
   UNIT_SELLING_PRICE,
   ORDER_LINE,
   INVOICE_DATE,
   MODIFIER_NAME,
   LINE_FLOW_STATUS_CODE,
   CAT,
   SPECIAL_COST,
   VQN_MODIFIER_NAME,
   HEADER_ID,
   PARTY_ID,
   ORGANIZATION_ID,
   LINE_TYPE
)
AS
   SELECT oh.order_number,
          oos.name order_source,
          -- hps.party_site_number job_number ## mv to HF
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Party_Site_num_f (
             oh.invoice_to_org_id)
             job_number,        --TMS#20150921-00332  by Siva    on 04-12-2016
          TRUNC (oh.ordered_date) ordered_date,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status -- ,cust_acct.account_number customer_number ## mv to HF
                         ,
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             'ACCOUNTNUM')
             CUSTOMER_NUMBER    --TMS#20150921-00332  by Siva    on 04-12-2016
                            -- ,NVL (cust_acct.account_name, party.party_name) customer_name  ## mv to HF
          ,
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             NULL)
             CUSTOMER_NAME,     --TMS#20150921-00332  by Siva    on 04-12-2016
            NVL (ol.unit_selling_price, 0)
          * DECODE (ol.line_category_code,
                    'RETURN', (ol.ordered_quantity * -1),
                    ol.ordered_quantity)
             gross_amount,
          NVL (
             xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_discount_amt (
                ol.header_id,
                ol.line_id),
             0)
             discount_amt,
          NVL (
             DECODE (ol.line_category_code,
                     'RETURN', (ol.tax_value * -1),
                     ol.tax_value),
             0)
             tax_value,
          NVL (
             xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_freight_amt (
                ol.header_id,
                ol.line_id),
             0)
             freight_amt,       --TMS#20150921-00332  by Siva    on 04-12-2016
          /*decode(mcvc.segment2,
          'PRMO',
          (-1 * ol.unit_selling_price),
          apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,
          */
          -- Changed
          CASE
             WHEN mcvc.segment2 = 'PRMO'
             THEN
                (-1 * ol.unit_selling_price)
             -- ELSE apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) ## MV to HF
             ELSE
                xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                   OL.LINE_ID)  --TMS#20150921-00332  by Siva    on 04-12-2016
          --      ELSE NVL(apps.cst_cost_api.get_item_cost(1, msi.inventory_item_id, msi.organization_id), 0)
          END
             average_cost, --((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY)- (DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * OL.ORDERED_QUANTITY)) PROFIT,
          --((NVL(ol.unit_selling_price,0) * NVL (rctl.quantity_invoiced, rctl.quantity_credited) )- (NVL (rctl.quantity_invoiced, rctl.quantity_credited)* apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id),0 )) profit,
          0 profit,
--          rep.name salesrep_name, --Commented for version 1.5
          xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,  --Added for version 1.5
          rct.trx_number trx_number,
--          rep.salesrep_number,  --Commented for version 1.5
		  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNUM') salesrep_number, --Added for version 1.5
          ship_from_org.organization_code warehouse,
            NVL ( (rctl.extended_amount + NVL (rctl.tax_recoverable, 0)), 0)
          + xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Charge_amount (
               oh.header_id) -- + xxeis.eis_rs_xxwc_com_util_pkg.get_charger_amt (oh.header_id) ## MV to HF
             inv_amount, --TMS#20150921-00332  by Siva    on 04-12-2016                                                                                                                                                              --((DECODE(xxeis.eis_rs_xxwc_com_util_pkg.get_promo_item(ol.inventory_item_id,ol.ship_from_org_id), 'N', ( NVL (rctl.quantity_invoiced, rctl.quantity_credited) * NVL (ol.unit_selling_price, 0)), 'Y', 0 ))+ NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_delivery_charge_amt(rct.customer_trx_id),0)) sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales, --(NVL(DECODE(mcvc.segment2, 'PRMO', 0, (NVL(rctl.quantity_invoiced, rctl.quantity_credited) * NVL(ol.unit_selling_price, 0))), 0)) sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))
                 --  * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id), 0)) ## MV to HF
                 * NVL (
                      xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                         OL.LINE_ID),
                      0)),
              0))
             sale_cost,         --TMS#20150921-00332  by Siva    on 04-12-2016
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          rct.creation_date invoice_date --  ,xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (oh.header_id, ol.line_id) modifier_name ## MV to HF
                                        ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,     --TMS#20150921-00332  by Siva    on 04-12-2016
          ol.flow_status_code line_flow_status_code, -- 'Previous Day Invoices' TYPE,
          mcvc.segment2 cat /* , (SELECT xxcpl.special_cost
                            FROM apps.oe_price_adjustments adj
                            ,apps.qp_list_lines qll
                            ,apps.qp_list_headers qlh
                            ,apps.xxwc_om_contract_pricing_hdr xxcph
                            ,apps.xxwc_om_contract_pricing_lines xxcpl
                            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                            AND xxcpl.agreement_id = xxcph.agreement_id
                            AND xxcpl.agreement_type = 'VQN'
                            AND qlh.list_header_id = qll.list_header_id
                            AND qlh.list_header_id = adj.list_header_id
                            AND qll.list_line_id = adj.list_line_id
                            AND adj.list_line_type_code = 'DIS'
                            AND adj.applied_flag = 'Y'
                            AND adj.header_id = ol.header_id
                            AND adj.line_id = ol.line_id
                            AND xxcpl.product_value = msi.segment1
                            AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                            AND ROWNUM = 1)
                            special_cost*/
                           -- ## MV to HF
          ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             'SPECIAL')
             SPECIAL_COST       --TMS#20150921-00332  by Siva    on 04-12-2016
                         /*, (SELECT qlh.name
                         FROM apps.oe_price_adjustments adj
                         ,apps.qp_list_lines qll
                         ,apps.qp_list_headers qlh
                         ,apps.xxwc_om_contract_pricing_hdr xxcph
                         ,apps.xxwc_om_contract_pricing_lines xxcpl
                         WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                         AND xxcpl.agreement_id = xxcph.agreement_id
                         AND xxcpl.agreement_type = 'VQN'
                         AND qlh.list_header_id = qll.list_header_id
                         AND qlh.list_header_id = adj.list_header_id
                         AND qll.list_line_id = adj.list_line_id
                         AND adj.list_line_type_code = 'DIS'
                         AND adj.applied_flag = 'Y'
                         AND adj.header_id = ol.header_id
                         AND adj.line_id = ol.line_id
                         AND xxcpl.product_value = msi.segment1
                         AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                         AND ROWNUM = 1)
                         vqn_modifier_name*/
                         --## MV To HF
          ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             NULL)
             VQN_MODIFIER_NAME, --TMS#20150921-00332  by Siva    on 04-12-2016
          ---Primary Keys
          oh.header_id,
          NULL party_id,        --TMS#20150921-00332  by Siva    on 04-12-2016
          ship_from_org.organization_id,
          otl.name line_type
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org --    ,hz_parties party                 --TMS#20150921-00332  by Siva    on 04-12-2016
                                      --     ,hz_cust_accounts cust_acct      --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
--          ra_salesreps rep, --Commented for version 1.5
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          fnd_lookup_values_vl flv,                        --per_people_f ppf,
          --fnd_user fu ,
          ra_customer_trx rct,
          ra_customer_trx_lines rctl,           --oe_charge_lines_v      oclv,
          ra_terms rt --,hz_cust_site_uses_all hcsu             --TMS#20150921-00332  by Siva    on 04-12-2016                                             -- ????? > Start
                     --,hz_cust_acct_sites_all hcas            --TMS#20150921-00332  by Siva    on 04-12-2016
                     --,hz_party_sites hps                     --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          oe_order_sources oos                                  -- ????? < End
    WHERE --  ol.sold_to_org_id = cust_acct.cust_account_id(+)       --TMS#20150921-00332  by Siva    on 04-12-2016
              -- AND hcsu.site_use_id = oh.invoice_to_org_id               --TMS#20150921-00332  by Siva    on 04-12-2016                          -- ????? > Start
              --AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id        --TMS#20150921-00332  by Siva    on 04-12-2016
              --AND hps.party_site_id = hcas.party_site_id                 --TMS#20150921-00332  by Siva    on 04-12-2016
              OOS.ORDER_SOURCE_ID = OH.ORDER_SOURCE_ID          -- ????? < End
          -- AND cust_acct.party_id = party.party_id(+)                --TMS#20150921-00332  by Siva    on 04-12-2016
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
--          AND oh.salesrep_id = rep.salesrep_id(+) --Commented for version 1.5
--          AND oh.org_id = rep.org_id(+)  --Commented for version 1.5
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.org_id = otl.org_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND flv.lookup_code(+) = oh.flow_status_code
          /* AND rctl.creation_date >=
          TO_DATE (
          TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          + 0.25
          AND rctl.creation_date <=
          TO_DATE (
          TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          + 1.25*/		  
		  AND rctl.creation_date >=
          TO_DATE (
          TO_CHAR (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          + 0.25
          AND rctl.creation_date <=
          TO_DATE (
          TO_CHAR (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          + 1.25
          --TMS#20150921-00332  by Siva    on 05-13-2016 
          AND TRUNC (oh.ordered_date) < TRUNC (SYSDATE)
          AND ol.flow_status_code IN ('CLOSED')
          AND ol.invoice_interface_status_code = 'YES'
          AND oth.name != 'INTERNAL ORDER'
          --AND fu.user_id                       =ol.created_by
          --AND fu.employee_id                   =ppf.person_id(+)
          AND rct.customer_trx_id = rctl.customer_trx_id
          AND TO_CHAR (ol.line_id) = rctl.interface_line_attribute6
          AND TO_CHAR (oh.order_number) = rctl.interface_line_attribute1
          AND rctl.interface_line_context = 'ORDER ENTRY'
          --AND oh.header_id = oclv.header_id(+)
          --AND TRUNC (ol.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (ol.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (ol.creation_date) )
          --added
          ---AND ol.fulfilled_flag    ='Y'
          AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
          -- and OH.ORDER_NUMBER = 12916546
          AND NOT EXISTS
                 (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
                         'Y'    --TMS#20150921-00332  by Siva    on 04-12-2016
                    FROM gl_code_combinations_kfv gcc
                   WHERE     GCC.CODE_COMBINATION_ID =
                                MSI.COST_OF_SALES_ACCOUNT
                         AND GCC.SEGMENT4 = '646080')
   UNION -- Same Days Counter Orders 
   SELECT /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N8)*/
          --TMS#20150921-00332  by Siva    on 04-12-2016
          --'B' dbug,
          oh.order_number,
          oos.name order_source                                       -- ?????
                               --  hps.party_site_number job_number
          ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Party_Site_num_f (
             oh.invoice_to_org_id)
             job_number, -- ?????   --TMS#20150921-00332  by Siva    on 04-12-2016
          TRUNC (oh.ordered_date) ordered_date,    --ppf.full_name created_by,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status         -- ,cust_acct.account_number customer_number
                         -- ,NVL (cust_acct.account_name, party.party_name) customer_name
          ,
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             'ACCOUNTNUM')
             CUSTOMER_NUMBER,   --TMS#20150921-00332  by Siva    on 04-12-2016
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             NULL)
             CUSTOMER_NAME,     --TMS#20150921-00332  by Siva    on 04-12-2016
          (  NVL (ol.unit_selling_price, 0)
           * DECODE (ol.line_category_code,
                     'RETURN', (ol.ordered_quantity * -1),
                     ol.ordered_quantity))
             gross_amount,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_discount_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             discount_amt,      --TMS#20150921-00332  by Siva    on 04-12-2016
          (NVL (
              DECODE (ol.line_category_code,
                      'RETURN', (ol.tax_value * -1),
                      ol.tax_value),
              0))
             tax_value,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_freight_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             freight_amt, --TMS#20150921-00332  by Siva    on 04-12-2016   --decode(decode(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost),0,xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id,msi.organization_id),nvl(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost,
          /*decode(mcvc.segment2,
          'PRMO',
          (-1 * ol.unit_selling_price),
          apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,*/
          -- Changed
          CASE
             WHEN mcvc.segment2 = 'PRMO'
             THEN
                (-1 * ol.unit_selling_price)
             WHEN ol.flow_status_code IN ('CLOSED')
             THEN
                --  apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
                xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                   OL.LINE_ID)  --TMS#20150921-00332  by Siva    on 04-12-2016
             WHEN otl.name = 'BILL ONLY'
             THEN
                0
             ELSE
                DECODE (
                   NVL (
                      apps.cst_cost_api.get_item_cost (1,
                                                       msi.inventory_item_id,
                                                       msi.organization_id),
                      0)                                             --Pending
                        ,
                   0, ol.unit_cost,
                   apps.cst_cost_api.get_item_cost (1,
                                                    msi.inventory_item_id,
                                                    msi.organization_id))
          END
             average_cost, --(((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY)- (DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * OL.ORDERED_QUANTITY))) PROFIT,
          --((NVL(ol.unit_selling_price,0) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))- (DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity) * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id),0))) profit,
          0 PROFIT,
--          rep.name salesrep_name,  --Commented for version 1.5
		  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name, --Added for version 1.5
          NULL trx_number,
--          rep.salesrep_number,  --Commented for version 1.5
		  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNUM') salesrep_number, --Added for version 1.5
          ship_from_org.organization_code warehouse,
          NULL inv_amount,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (CASE
                       WHEN ol.flow_status_code IN ('CLOSED')
                       THEN
                          --  apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
                          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                             OL.LINE_ID) --TMS#20150921-00332  by Siva    on 04-12-2016
                       WHEN otl.name = 'BILL ONLY'
                       THEN
                          0
                       ELSE
                          DECODE (
                             NVL (
                                apps.cst_cost_api.get_item_cost (
                                   1,
                                   msi.inventory_item_id,
                                   msi.organization_id),
                                0),
                             0, ol.unit_cost,
                             apps.cst_cost_api.get_item_cost (
                                1,
                                msi.inventory_item_id,
                                msi.organization_id))
                    END)
                 * (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))),
              0))
             sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          NULL invoice_date --,xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (oh.header_id, ol.line_id) modifier_name
                           ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,     --TMS#20150921-00332  by Siva    on 04-12-2016
          ol.flow_status_code line_flow_status_code, -- 'Same Day Counter Orders' TYPE,
          mcvc.segment2 cat /* , (SELECT xxcpl.special_cost
                            FROM apps.oe_price_adjustments adj
                            ,apps.qp_list_lines qll
                            ,apps.qp_list_headers qlh
                            ,apps.xxwc_om_contract_pricing_hdr xxcph
                            ,apps.xxwc_om_contract_pricing_lines xxcpl
                            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                            AND xxcpl.agreement_id = xxcph.agreement_id
                            AND xxcpl.agreement_type = 'VQN'
                            AND qlh.list_header_id = qll.list_header_id
                            AND qlh.list_header_id = adj.list_header_id
                            AND qll.list_line_id = adj.list_line_id
                            AND adj.list_line_type_code = 'DIS'
                            AND adj.applied_flag = 'Y'
                            AND adj.header_id = ol.header_id
                            AND adj.line_id = ol.line_id
                            AND xxcpl.product_value = msi.segment1
                            AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                            AND ROWNUM = 1)
                            special_cost
                            , (SELECT qlh.name
                            FROM apps.oe_price_adjustments adj
                            ,apps.qp_list_lines qll
                            ,apps.qp_list_headers qlh
                            ,apps.xxwc_om_contract_pricing_hdr xxcph
                            ,apps.xxwc_om_contract_pricing_lines xxcpl
                            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                            AND xxcpl.agreement_id = xxcph.agreement_id
                            AND xxcpl.agreement_type = 'VQN'
                            AND qlh.list_header_id = qll.list_header_id
                            AND qlh.list_header_id = adj.list_header_id
                            AND qll.list_line_id = adj.list_line_id
                            AND adj.list_line_type_code = 'DIS'
                            AND adj.applied_flag = 'Y'
                            AND adj.header_id = ol.header_id
                            AND adj.line_id = ol.line_id
                            AND xxcpl.product_value = msi.segment1
                            AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                            AND ROWNUM = 1)
                            vqn_modifier_name
                            ,                                                                                     ---Primary Keys*/
                           ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             'SPECIAL')
             SPECIAL_COST,      --TMS#20150921-00332  by Siva    on 04-12-2016
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             NULL)
             VQN_MODIFIER_NAME, --TMS#20150921-00332  by Siva    on 04-12-2016
          oh.header_id,
          NULL party_id,
          ship_from_org.organization_id,
          otl.name line_type
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org -- ,hz_parties party                                   --TMS#20150921-00332  by Siva    on 04-12-2016
                                      --,hz_cust_accounts cust_acct                          --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
--          ra_salesreps rep, --Commented for version 1.5
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          fnd_lookup_values_vl flv,                        --per_people_f ppf,
          --fnd_user fu ,
          ra_terms rt --,hz_cust_site_uses_all hcsu                                         --TMS#20150921-00332  by Siva    on 04-12-2016                 -- ????? > Start
                     -- ,hz_cust_acct_sites_all hcas                                       --TMS#20150921-00332  by Siva    on 04-12-2016
                     -- ,hz_party_sites hps                                                --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          oe_order_sources oos                                  -- ????? < End
    --added
    ---ra_customer_trx rct,
    ---ra_customer_trx_lines rctl
    WHERE -- ol.sold_to_org_id = cust_acct.cust_account_id(+)                --TMS#20150921-00332  by Siva    on 04-12-2016
              --  AND hcsu.site_use_id = oh.invoice_to_org_id                        --TMS#20150921-00332  by Siva    on 04-12-2016                 -- ????? > Start
              -- AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id                 --TMS#20150921-00332  by Siva    on 04-12-2016
              -- AND hps.party_site_id = hcas.party_site_id                          --TMS#20150921-00332  by Siva    on 04-12-2016
              oos.order_source_id = oh.order_source_id          -- ????? < End
          --AND cust_acct.party_id = party.party_id(+)                           --TMS#20150921-00332  by Siva    on 04-12-2016
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
--          AND oh.salesrep_id = rep.salesrep_id(+) --Commented for version 1.5
--          AND oh.org_id = rep.org_id(+)  --Commented for version 1.5
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.flow_status_code <> 'CANCELLED'
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND ol.org_id = otl.org_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND oth.name != 'INTERNAL ORDER'
          AND FLV.LOOKUP_CODE(+) = OH.FLOW_STATUS_CODE
          AND TRUNC (SYSDATE) =
                 TRUNC (XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from) --TMS#20150921-00332  by Siva    on 04-12-2016
          AND oth.name = 'COUNTER ORDER'
          AND OH.FLOW_STATUS_CODE = 'BOOKED'
          AND TRUNC (oh.ordered_date) = TRUNC (SYSDATE) 
          --AND fu.user_id               = ol.created_by
          --AND fu.employee_id           = ppf.person_id(+)
          --AND TRUNC (ol.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (ol.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (ol.creation_date) )
          --added
          --AND ol.fulfilled_flag           ='Y'
          --AND rct.customer_trx_id         = rctl.customer_trx_id
          --AND TO_CHAR(ol.line_id)         = rctl.interface_line_attribute6
          --AND TO_CHAR (oh.order_number)   = rctl.interface_line_attribute1
          --AND rctl.interface_line_context = 'ORDER ENTRY'
          AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
          AND NOT EXISTS
                 (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
                         'Y'    --TMS#20150921-00332  by Siva    on 04-12-2016
                    FROM gl_code_combinations_kfv gcc
                   WHERE     gcc.code_combination_id =
                                msi.cost_of_sales_account
                         AND GCC.SEGMENT4 = '646080')
   --UNION 3
   UNION
   /* Invoiced Lines after 6 AM on the same day */
   SELECT /*+ INDEX(oh OE_ORDER_HEADERS_U1)*/
          oh.order_number,
          oos.name order_source,                                      -- ?????
          --  hps.party_site_number job_number
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Party_Site_num_f (
             oh.invoice_to_org_id)
             job_number, -- ????? --TMS#20150921-00332  by Siva    on 04-12-2016
          TRUNC (oh.ordered_date) ordered_date,    --ppf.full_name created_by,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status          --,cust_acct.account_number customer_number
                         --,NVL (cust_acct.account_name, party.party_name) customer_name
          ,
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             'ACCOUNTNUM')
             CUSTOMER_NUMBER,   --TMS#20150921-00332  by Siva    on 04-12-2016
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             NULL)
             CUSTOMER_NAME,     --TMS#20150921-00332  by Siva    on 04-12-2016
          (  NVL (ol.unit_selling_price, 0)
           * DECODE (ol.line_category_code,
                     'RETURN', (ol.ordered_quantity * -1),
                     ol.ordered_quantity))
             gross_amount,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_discount_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             discount_amt,      --TMS#20150921-00332  by Siva    on 04-12-2016
          (NVL (
              DECODE (ol.line_category_code,
                      'RETURN', (ol.tax_value * -1),
                      ol.tax_value),
              0))
             tax_value,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_freight_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             freight_amt,       --TMS#20150921-00332  by Siva    on 04-12-2016
          /*decode(mcvc.segment2,
          'PRMO',
          (-1 * ol.unit_selling_price),
          apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,*/
          -- Changed
          CASE
             WHEN mcvc.segment2 = 'PRMO'
             THEN
                (-1 * ol.unit_selling_price) /*      WHEN ol.flow_status_code IN ('CLOSED','INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE', 'INVOICE_NOT_APPLICABLE', 'INVOICE_RFR', 'INVOICE_UNEXPECTED_ERROR')
                                             THEN apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id) */
 --  ELSE apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) /*NVL(apps.cst_cost_api.get_item_cost(1, msi.inventory_item_id, msi.organization_id), 0)*/
             ELSE
                xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                   OL.LINE_ID)  --TMS#20150921-00332  by Siva    on 04-12-2016
          END
             average_cost, --(((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY)- (DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * OL.ORDERED_QUANTITY))) PROFIT,
          --    ((NVL(ol.unit_selling_price,0) * NVL (rctl.quantity_invoiced, rctl.quantity_credited)) -(NVL (rctl.quantity_invoiced, rctl.quantity_credited)* NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0))) profit,
          0 profit,
--          rep.name, --Commented for version 1.5
		 xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name, --Added for version 1.5
          rct.trx_number trx_number,
--          rep.salesrep_number,  --Commented for version 1.5
		 xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNUM') salesrep_number, --Added for version 1.5
          ship_from_org.organization_code warehouse,
            NVL ( (rctl.extended_amount + NVL (rctl.tax_recoverable, 0)), 0) --  + xxeis.eis_rs_xxwc_com_util_pkg.get_charger_amt (oh.header_id)
          + xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Charge_amount (
               oh.header_id)
             inv_amount, --TMS#20150921-00332  by Siva    on 04-12-2016     --(NVL(DECODE(mcvc.segment2, 'PRMO', 0, (NVL(rctl.quantity_invoiced, rctl.quantity_credited) * NVL(ol.unit_selling_price, 0))), 0)) sales,
          -- (NVL(DECODE(mcvc.segment2, 'PRMO',                                                         -1 * (NVL(rctl.quantity_invoiced, rctl.quantity_credited) * NVL(ol.unit_selling_price, 0)), NVL(rctl.quantity_invoiced, rctl.quantity_credited) * NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id), 0)), 0)) sale_cost,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales, --(NVL(DECODE(mcvc.segment2, 'PRMO', 0, (NVL(rctl.quantity_invoiced, rctl.quantity_credited) * NVL(ol.unit_selling_price, 0))), 0)) sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))
                 --  * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id), 0))
                 * NVL (
                      xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                         OL.LINE_ID),
                      0)),
              0))
             sale_cost,         --TMS#20150921-00332  by Siva    on 04-12-2016
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          rct.creation_date invoice_date --,xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (oh.header_id, ol.line_id) modifier_name
                                        ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,     --TMS#20150921-00332  by Siva    on 04-12-2016
          ol.flow_status_code line_flow_status_code, -- 'Same Day Invoices' TYPE,
          mcvc.segment2 cat /*  , (SELECT xxcpl.special_cost
                            FROM apps.oe_price_adjustments adj
                            ,apps.qp_list_lines qll
                            ,apps.qp_list_headers qlh
                            ,apps.xxwc_om_contract_pricing_hdr xxcph
                            ,apps.xxwc_om_contract_pricing_lines xxcpl
                            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                            AND xxcpl.agreement_id = xxcph.agreement_id
                            AND xxcpl.agreement_type = 'VQN'
                            AND qlh.list_header_id = qll.list_header_id
                            AND qlh.list_header_id = adj.list_header_id
                            AND qll.list_line_id = adj.list_line_id
                            AND adj.list_line_type_code = 'DIS'
                            AND adj.applied_flag = 'Y'
                            AND adj.header_id = ol.header_id
                            AND adj.line_id = ol.line_id
                            AND xxcpl.product_value = msi.segment1
                            AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                            AND ROWNUM = 1)
                            special_cost
                            , (SELECT qlh.name
                            FROM apps.oe_price_adjustments adj
                            ,apps.qp_list_lines qll
                            ,apps.qp_list_headers qlh
                            ,apps.xxwc_om_contract_pricing_hdr xxcph
                            ,apps.xxwc_om_contract_pricing_lines xxcpl
                            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                            AND xxcpl.agreement_id = xxcph.agreement_id
                            AND xxcpl.agreement_type = 'VQN'
                            AND qlh.list_header_id = qll.list_header_id
                            AND qlh.list_header_id = adj.list_header_id
                            AND qll.list_line_id = adj.list_line_id
                            AND adj.list_line_type_code = 'DIS'
                            AND adj.applied_flag = 'Y'
                            AND adj.header_id = ol.header_id
                            AND adj.line_id = ol.line_id
                            AND xxcpl.product_value = msi.segment1
                            AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                            AND ROWNUM = 1)
                            vqn_modifier_name
                            ,                                                                                     ---Primary Keys*/
                           ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             'SPECIAL')
             SPECIAL_COST,      --TMS#20150921-00332  by Siva    on 04-12-2016
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             NULL)
             VQN_MODIFIER_NAME, --TMS#20150921-00332  by Siva    on 04-12-2016
          oh.header_id,
          NULL party_id,
          ship_from_org.organization_id,
          otl.name line_type
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org,
		-- ,hz_parties party                             --TMS#20150921-00332  by Siva    on 04-12-2016
        -- ,hz_cust_accounts cust_acct                   --TMS#20150921-00332  by Siva    on 04-12-2016
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
--          ra_salesreps rep, --Commented for version 1.5
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          fnd_lookup_values_vl flv,                        --per_people_f ppf,
          --fnd_user fu ,
          ra_customer_trx rct,
          ra_customer_trx_lines rctl,
          ra_terms rt -- ,hz_cust_site_uses_all hcsu                      --TMS#20150921-00332  by Siva    on 04-12-2016                                    -- ????? > Start
                     -- ,hz_cust_acct_sites_all hcas                     --TMS#20150921-00332  by Siva    on 04-12-2016
                     --,hz_party_sites hps                               --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          oe_order_sources oos                                  -- ????? < End
    WHERE --  ol.sold_to_org_id = cust_acct.cust_account_id(+)                   --TMS#20150921-00332  by Siva    on 04-12-2016
              -- AND hcsu.site_use_id = oh.invoice_to_org_id                                         -- ????? > Start
              -- AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
              --AND hps.party_site_id = hcas.party_site_id             --TMS#20150921-00332  by Siva    on 04-12-2016
              oos.order_source_id = oh.order_source_id          -- ????? < End
          -- AND cust_acct.party_id = party.party_id(+)
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
--          AND oh.salesrep_id = rep.salesrep_id(+) --Commented for version 1.5
--          AND oh.org_id = rep.org_id(+) --Commented for version 1.5
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.flow_status_code <> 'CANCELLED'
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND ol.org_id = otl.org_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND oth.name != 'INTERNAL ORDER'
          AND flv.lookup_code(+) = oh.flow_status_code
          AND TRUNC (SYSDATE) =
                 TRUNC (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from) --TMS#20150921-00332  by Siva    on 04-12-2016
          /*   AND rctl.creation_date >=
          TO_DATE (
          TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          + 0.25*/          --TMS#20150921-00332  by Siva    on 04-12-2016
		  AND rctl.creation_date >=
          TO_DATE (
          TO_CHAR (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          + 0.25  --TMS#20150921-00332  by Siva    on 05-13-2016
          --AND fu.user_id          = ol.created_by
          --AND fu.employee_id      = ppf.person_id(+)
          AND rct.customer_trx_id = rctl.customer_trx_id
          AND TO_CHAR (ol.line_id) = rctl.interface_line_attribute6
          /* -- AND TO_CHAR (OH.ORDER_NUMBER)      = RCTL.INTERFACE_LINE_ATTRIBUTE1
          -- AND RCTL.INTERFACE_LINE_CONTEXT    = 'ORDER ENTRY'*/
		  --added again
		  AND TO_CHAR (OH.ORDER_NUMBER)      = RCTL.INTERFACE_LINE_ATTRIBUTE1
          AND RCTL.INTERFACE_LINE_CONTEXT    = 'ORDER ENTRY'
          --AND TRUNC (ol.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (ol.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (ol.creation_date) )
          --added
          --  AND ol.fulfilled_flag    ='Y'
          AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
          AND NOT EXISTS
                 (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
                         'Y'
                    FROM gl_code_combinations_kfv gcc
                   WHERE     GCC.CODE_COMBINATION_ID =
                                MSI.COST_OF_SALES_ACCOUNT
                         AND GCC.SEGMENT4 = '646080')
   --- UNION 4-1
   UNION
   SELECT                                           
         oh.order_number,
         oos.name order_source                                        -- ?????
                              --   hps.party_site_number job_number
         ,
         xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Party_Site_num_f (
            oh.invoice_to_org_id)
            job_number          --TMS#20150921-00332  by Siva    on 04-12-2016
                      -- ?????
         ,
         TRUNC (oh.ordered_date) ordered_date      --ppf.full_name created_by,
                                             ,
         oh.attribute7 created_by,
         oth.name order_type,
         NVL (flv.meaning, INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
            order_status          -- ,cust_acct.account_number customer_number
                        --,NVL (cust_acct.account_name, party.party_name) customer_name
         ,
         XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
            OL.SOLD_TO_ORG_ID,
            'ACCOUNTNUM')
            CUSTOMER_NUMBER,    --TMS#20150921-00332  by Siva    on 04-12-2016
         XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
            OL.SOLD_TO_ORG_ID,
            NULL)
            CUSTOMER_NAME,      --TMS#20150921-00332  by Siva    on 04-12-2016
         (  NVL (ol.unit_selling_price, 0)
          * DECODE (ol.line_category_code,
                    'RETURN', (ol.ordered_quantity * -1),
                    ol.ordered_quantity))
            gross_amount,
         (NVL (
             xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_discount_amt (ol.header_id,
                                                                  ol.line_id),
             0))
            discount_amt,       --TMS#20150921-00332  by Siva    on 04-12-2016
         (NVL (
             DECODE (ol.line_category_code,
                     'RETURN', (ol.tax_value * -1),
                     ol.tax_value),
             0))
            tax_value,
         (NVL (
             xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_freight_amt (ol.header_id,
                                                                 ol.line_id),
             0))
            freight_amt,        --TMS#20150921-00332  by Siva    on 04-12-2016
         --DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost, cm
         --DECODE(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost),0,xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id,msi.organization_id),NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost,
         /*decode(mcvc.segment2,
         'PRMO',
         (-1 * ol.unit_selling_price),
         apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,*/
         -- Changed
         CASE
            WHEN mcvc.segment2 = 'PRMO'
            THEN
               (-1 * ol.unit_selling_price)
            WHEN ol.flow_status_code IN ('CLOSED',
                                         'INVOICED',
                                         'INVOICED_PARTIAL',
                                         'INVOICE_DELIVERY',
                                         'INVOICE_HOLD',
                                         'INVOICE_INCOMPLETE')
            THEN
               --  apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) sv
               xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (OL.LINE_ID) --TMS#20150921-00332  by Siva    on 04-12-2016
            WHEN otl.name = 'BILL ONLY'
            THEN
               0
            ELSE
               NVL (
                  apps.cst_cost_api.get_item_cost (1,
                                                   msi.inventory_item_id,
                                                   msi.organization_id),
                  0)
         END
            average_cost, --(((nvl(ol.unit_selling_price,0) * ol.ordered_quantity)-
         --(NVL(DECODE(APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID),0,
         --OL.UNIT_COST,APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID) ),0) * OL.ORDERED_QUANTITY))) PROFIT,
         --((NVL(ol.unit_selling_price,0) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))- (DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity) * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) , 0))) profit,
         0 profit,
--         rep.name, --Commented for version 1.5
		 xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,  --Added for version 1.5
         NULL trx_number,
--         rep.salesrep_number, --Commented for version 1.5
		 xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNUM') salesrep_number,  --Added for version 1.5
         ship_from_org.organization_code warehouse,
         NULL inv_amount, -- (NVL(OL.UNIT_SELLING_PRICE,0)                                                                                                                                 * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY))SALES,
         --(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))sale_cost,
         (NVL (
             DECODE (
                mcvc.segment2,
                'PRMO', 0,
                  NVL (ol.unit_selling_price, 0)
                * DECODE (ol.line_category_code,
                          'RETURN', (ol.ordered_quantity * -1),
                          ol.ordered_quantity)),
             0))
            sales,
         (NVL (
             DECODE (
                mcvc.segment2,
                'PRMO',   -1
                        * (  NVL (ol.unit_selling_price, 0)
                           * DECODE (ol.line_category_code,
                                     'RETURN', (ol.ordered_quantity * -1),
                                     ol.ordered_quantity)),
                  (CASE
                      WHEN ol.flow_status_code IN ('CLOSED',
                                                   'INVOICED',
                                                   'INVOICED_PARTIAL',
                                                   'INVOICE_DELIVERY',
                                                   'INVOICE_HOLD',
                                                   'INVOICE_INCOMPLETE')
                      THEN
                         --  apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) sv
                         xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                            OL.LINE_ID) --TMS#20150921-00332  by Siva    on 04-12-2016
                      WHEN otl.name = 'BILL ONLY'
                      THEN
                         0
                      ELSE
                         NVL (
                            apps.cst_cost_api.get_item_cost (
                               1,
                               msi.inventory_item_id,
                               msi.organization_id),
                            0)
                   END)
                * (DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity))),
             0))
            sale_cost,
         rt.name payment_term,
         rt.description payment_desc,
         msi.segment1 item,
         CASE
            WHEN msi.segment1 = 'Rental Charge' THEN ol.user_item_description
            ELSE msi.description
         END
            item_desc,
         DECODE (ol.line_category_code,
                 'RETURN', (ol.ordered_quantity * -1),
                 ol.ordered_quantity)
            qty,
         ol.order_quantity_uom uom,
         DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
            unit_selling_price,
         DECODE (
            ol.option_number,
            '', (ol.line_number || '.' || ol.shipment_number),
            (   ol.line_number
             || '.'
             || ol.shipment_number
             || '.'
             || ol.option_number))
            order_line,
         NULL invoice_date -- ,xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (oh.header_id, ol.line_id) modifier_name
                          ,
         xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_line_modifier_name (oh.header_id,
                                                                    ol.line_id)
            modifier_name,      --TMS#20150921-00332  by Siva    on 04-12-2016
         ol.flow_status_code line_flow_status_code, -- 'Waiting for Invoices' TYPE,
         mcvc.segment2 cat /*, (SELECT xxcpl.special_cost
                           FROM apps.oe_price_adjustments adj
                           ,apps.qp_list_lines qll
                           ,apps.qp_list_headers qlh
                           ,apps.xxwc_om_contract_pricing_hdr xxcph
                           ,apps.xxwc_om_contract_pricing_lines xxcpl
                           WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                           AND xxcpl.agreement_id = xxcph.agreement_id
                           AND xxcpl.agreement_type = 'VQN'
                           AND qlh.list_header_id = qll.list_header_id
                           AND qlh.list_header_id = adj.list_header_id
                           AND qll.list_line_id = adj.list_line_id
                           AND adj.list_line_type_code = 'DIS'
                           AND adj.applied_flag = 'Y'
                           AND adj.header_id = ol.header_id
                           AND adj.line_id = ol.line_id
                           AND xxcpl.product_value = msi.segment1
                           AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                           AND ROWNUM = 1)
                           special_cost
                           , (SELECT qlh.name
                           FROM apps.oe_price_adjustments adj
                           ,apps.qp_list_lines qll
                           ,apps.qp_list_headers qlh
                           ,apps.xxwc_om_contract_pricing_hdr xxcph
                           ,apps.xxwc_om_contract_pricing_lines xxcpl
                           WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                           AND xxcpl.agreement_id = xxcph.agreement_id
                           AND xxcpl.agreement_type = 'VQN'
                           AND qlh.list_header_id = qll.list_header_id
                           AND qlh.list_header_id = adj.list_header_id
                           AND qll.list_line_id = adj.list_line_id
                           AND adj.list_line_type_code = 'DIS'
                           AND adj.applied_flag = 'Y'
                           AND adj.header_id = ol.header_id
                           AND adj.line_id = ol.line_id
                           AND xxcpl.product_value = msi.segment1
                           AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                           AND ROWNUM = 1)
                           vqn_modifier_name*/
                          ,
         xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (ol.header_id,
                                                                 ol.line_id,
                                                                 msi.segment1,
                                                                 'SPECIAL')
            SPECIAL_COST,       --TMS#20150921-00332  by Siva    on 04-12-2016
         xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (ol.header_id,
                                                                 ol.line_id,
                                                                 msi.segment1,
                                                                 NULL)
            VQN_MODIFIER_NAME,  --TMS#20150921-00332  by Siva    on 04-12-2016
         ---Primary Keys
         oh.header_id,
         NULL party_id,                                     -- Rct.trx_number,
         ship_from_org.organization_id,
         otl.name line_type
    --ol.ship_from_org_id organization_id
    --added
    --NULL extended_sale,
    --NULL extended_cost
    --    msi.inventory_item_id ,
    --    msi.organization_id msi_organization_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
    FROM mtl_parameters ship_from_org --,hz_parties party               --TMS#20150921-00332  by Siva    on 04-12-2016
                                     --,hz_cust_accounts cust_acct     --TMS#20150921-00332  by Siva    on 04-12-2016
         ,
         oe_order_headers oh,
         oe_order_lines ol,
         hr_all_organization_units hou,
--         ra_salesreps rep, --Commented for version 1.5
         oe_transaction_types_vl oth,
         oe_transaction_types_vl otl,
         mtl_system_items_kfv msi,
         mtl_categories_kfv mcvc,
         mtl_item_categories micc,
         fnd_lookup_values_vl flv,
         ra_terms rt -- ,hz_cust_site_uses_all hcsu                     --TMS#20150921-00332  by Siva    on 04-12-2016                                     -- ????? > Start
                    --,hz_cust_acct_sites_all hcas                     --TMS#20150921-00332  by Siva    on 04-12-2016
                    -- ,hz_party_sites hps                             --TMS#20150921-00332  by Siva    on 04-12-2016
         ,
         oe_order_sources oos                                   -- ????? < End
   WHERE --    ol.sold_to_org_id = cust_acct.cust_account_id(+)    --TMS#20150921-00332  by Siva    on 04-12-2016
             -- AND hcsu.site_use_id = oh.invoice_to_org_id                                         -- ????? > Start
             --  AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
             -- AND hps.party_site_id = hcas.party_site_id
             oos.order_source_id = oh.order_source_id           -- ????? < End
         -- AND cust_acct.party_id = party.party_id(+)                                    --TMS#20150921-00332  by Siva    on 04-12-2016
         AND ol.ship_from_org_id = ship_from_org.organization_id(+)
         AND ol.header_id = oh.header_id
         AND msi.organization_id = hou.organization_id
--         AND oh.salesrep_id = rep.salesrep_id(+) --Commented for version 1.5
--         AND oh.org_id = rep.org_id(+) --Commented for version 1.5
         AND oh.payment_term_id = rt.term_id
         AND msi.inventory_item_id(+) = ol.inventory_item_id
         AND msi.organization_id(+) = ol.ship_from_org_id
         AND msi.inventory_item_id = micc.inventory_item_id(+)
         AND msi.organization_id = micc.organization_id(+)
         AND micc.category_id = mcvc.category_id(+)
         AND mcvc.structure_id(+) = 101
         AND micc.category_set_id(+) = 1100000062
         AND oh.order_type_id = oth.transaction_type_id
         AND oh.org_id = oth.org_id
         AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
         AND ol.line_type_id = otl.transaction_type_id
         AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
         AND ol.org_id = otl.org_id
         AND flv.lookup_type(+) = 'FLOW_STATUS'
         AND oth.name != 'INTERNAL ORDER'
         AND FLV.LOOKUP_CODE(+) = OH.FLOW_STATUS_CODE
         AND OTH.name = 'STANDARD ORDER'
         AND (OL.USER_ITEM_DESCRIPTION = 'DELIVERED'
             OR (    OL.SOURCE_TYPE_CODE = 'EXTERNAL'
                 AND OL.FLOW_STATUS_CODE = 'INVOICE_HOLD')) 
         /* AND (   (    oth.name = 'STANDARD ORDER'
         AND (   ol.user_item_description = 'DELIVERED'
         or (OL.SOURCE_TYPE_CODE = 'EXTERNAL' and OL.FLOW_STATUS_CODE = 'INVOICE_HOLD')))*/
         --  OR (OTH.NAME                 IN ('WC SHORT TERM RENTAL','WC LONG TERM RENTAL')
         --  AND ol.flow_status_code      IN ('INVOICE_HOLD','CLOSED'))     --TMS#20150921-00332  by Siva    on 04-12-2016
         /*OR (EXISTS
         (SELECT 1
         FROM ra_interface_lines_all ril
         WHERE     ril.interface_line_context = 'ORDER ENTRY'
         AND TO_CHAR (ol.line_id) = ril.interface_line_attribute6
         AND TO_CHAR (OH.ORDER_NUMBER) = RIL.INTERFACE_LINE_ATTRIBUTE1))*/
         --     )
         AND TRUNC (SYSDATE) =
                TRUNC (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from) --TMS#20150921-00332  by Siva    on 04-12-2016
         AND OL.SOURCE_TYPE_CODE IN ('INTERNAL', 'EXTERNAL')
         -- and OH.ORDER_NUMBER IN( 12916583,12916584)
         --   AND OH.HEADER_ID = 36228149
         AND NOT EXISTS
                (SELECT  /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/	--Added for version 1.4  
                        'Y'     --TMS#20150921-00332  by Siva    on 04-12-2016
                   FROM gl_code_combinations_kfv gcc
                  WHERE     gcc.code_combination_id =
                               msi.cost_of_sales_account
                        AND GCC.SEGMENT4 = '646080')                ----proble
  --- UNION 4-2   
	UNION
   SELECT /*+ INDEX(ol XXWC_OE_ORDER_LN_AL_N13) USE_NL(ol,oh) USE_NL(ol,msi)*/
          oh.order_number,
          oos.name order_source                                       -- ?????
                               --   hps.party_site_number job_number
          ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Party_Site_num_f (
             oh.invoice_to_org_id)
             job_number         --TMS#20150921-00332  by Siva    on 04-12-2016
                       -- ?????
          ,
          TRUNC (oh.ordered_date) ordered_date     --ppf.full_name created_by,
                                              ,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status         -- ,cust_acct.account_number customer_number
                         --,NVL (cust_acct.account_name, party.party_name) customer_name
          ,
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             'ACCOUNTNUM')
             CUSTOMER_NUMBER,   --TMS#20150921-00332  by Siva    on 04-12-2016
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             NULL)
             CUSTOMER_NAME,     --TMS#20150921-00332  by Siva    on 04-12-2016
          (  NVL (ol.unit_selling_price, 0)
           * DECODE (ol.line_category_code,
                     'RETURN', (ol.ordered_quantity * -1),
                     ol.ordered_quantity))
             gross_amount,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_discount_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             discount_amt,      --TMS#20150921-00332  by Siva    on 04-12-2016
          (NVL (
              DECODE (ol.line_category_code,
                      'RETURN', (ol.tax_value * -1),
                      ol.tax_value),
              0))
             tax_value,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_freight_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             freight_amt,       --TMS#20150921-00332  by Siva    on 04-12-2016
          --DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost, cm
          --DECODE(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost),0,xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id,msi.organization_id),NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost,
          /*decode(mcvc.segment2,
          'PRMO',
          (-1 * ol.unit_selling_price),
          apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,*/
          -- Changed
          CASE
             WHEN mcvc.segment2 = 'PRMO'
             THEN
                (-1 * ol.unit_selling_price)
             WHEN ol.flow_status_code IN ('CLOSED',
                                          'INVOICED',
                                          'INVOICED_PARTIAL',
                                          'INVOICE_DELIVERY',
                                          'INVOICE_HOLD',
                                          'INVOICE_INCOMPLETE')
             THEN
                --  apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) sv
                xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                   OL.LINE_ID)
             WHEN otl.name = 'BILL ONLY'
             THEN
                0
             ELSE
                NVL (
                   apps.cst_cost_api.get_item_cost (1,
                                                    msi.inventory_item_id,
                                                    msi.organization_id),
                   0)
          END
             average_cost, --(((nvl(ol.unit_selling_price,0) * ol.ordered_quantity)-
          --(NVL(DECODE(APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID),0,
          --OL.UNIT_COST,APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID) ),0) * OL.ORDERED_QUANTITY))) PROFIT,
          --((NVL(ol.unit_selling_price,0) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))- (DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity) * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) , 0))) profit,
          0 profit,
--          rep.name,  --Commented for version 1.5
		  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name,  --Added for version 1.5
          NULL trx_number,
--          rep.salesrep_number,  --Commented for version 1.5
		  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNUM') salesrep_number,  --Added for version 1.5
          ship_from_org.organization_code warehouse,
          NULL inv_amount, -- (NVL(OL.UNIT_SELLING_PRICE,0)                                                                                                                                 * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY))SALES,
          --(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))sale_cost,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (CASE
                       WHEN ol.flow_status_code IN ('CLOSED',
                                                    'INVOICED',
                                                    'INVOICED_PARTIAL',
                                                    'INVOICE_DELIVERY',
                                                    'INVOICE_HOLD',
                                                    'INVOICE_INCOMPLETE')
                       THEN
                          --  apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) sv
                          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                             OL.LINE_ID) --TMS#20150921-00332  by Siva    on 04-12-2016
                       WHEN otl.name = 'BILL ONLY'
                       THEN
                          0
                       ELSE
                          NVL (
                             apps.cst_cost_api.get_item_cost (
                                1,
                                msi.inventory_item_id,
                                msi.organization_id),
                             0)
                    END)
                 * (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))),
              0))
             sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          NULL invoice_date -- ,xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (oh.header_id, ol.line_id) modifier_name
                           ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,     --TMS#20150921-00332  by Siva    on 04-12-2016
          ol.flow_status_code line_flow_status_code, -- 'Waiting for Invoices' TYPE,
          mcvc.segment2 cat /*, (SELECT xxcpl.special_cost
                            FROM apps.oe_price_adjustments adj
                            ,apps.qp_list_lines qll
                            ,apps.qp_list_headers qlh
                            ,apps.xxwc_om_contract_pricing_hdr xxcph
                            ,apps.xxwc_om_contract_pricing_lines xxcpl
                            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                            AND xxcpl.agreement_id = xxcph.agreement_id
                            AND xxcpl.agreement_type = 'VQN'
                            AND qlh.list_header_id = qll.list_header_id
                            AND qlh.list_header_id = adj.list_header_id
                            AND qll.list_line_id = adj.list_line_id
                            AND adj.list_line_type_code = 'DIS'
                            AND adj.applied_flag = 'Y'
                            AND adj.header_id = ol.header_id
                            AND adj.line_id = ol.line_id
                            AND xxcpl.product_value = msi.segment1
                            AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                            AND ROWNUM = 1)
                            special_cost
                            , (SELECT qlh.name
                            FROM apps.oe_price_adjustments adj
                            ,apps.qp_list_lines qll
                            ,apps.qp_list_headers qlh
                            ,apps.xxwc_om_contract_pricing_hdr xxcph
                            ,apps.xxwc_om_contract_pricing_lines xxcpl
                            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                            AND xxcpl.agreement_id = xxcph.agreement_id
                            AND xxcpl.agreement_type = 'VQN'
                            AND qlh.list_header_id = qll.list_header_id
                            AND qlh.list_header_id = adj.list_header_id
                            AND qll.list_line_id = adj.list_line_id
                            AND adj.list_line_type_code = 'DIS'
                            AND adj.applied_flag = 'Y'
                            AND adj.header_id = ol.header_id
                            AND adj.line_id = ol.line_id
                            AND xxcpl.product_value = msi.segment1
                            AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                            AND ROWNUM = 1)
                            vqn_modifier_name*/
                           ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             'SPECIAL')
             SPECIAL_COST,      --TMS#20150921-00332  by Siva    on 04-12-2016
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             NULL)
             VQN_MODIFIER_NAME, --TMS#20150921-00332  by Siva    on 04-12-2016
          ---Primary Keys
          oh.header_id,
          NULL party_id,                                    -- Rct.trx_number,
          ship_from_org.organization_id,
          otl.name line_type
     --ol.ship_from_org_id organization_id
     --added
     --NULL extended_sale,
     --NULL extended_cost
     --    msi.inventory_item_id ,
     --    msi.organization_id msi_organization_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org --,hz_parties party                         --TMS#20150921-00332  by Siva    on 04-12-2016
                                      --,hz_cust_accounts cust_acct               --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
--          ra_salesreps rep, --Commented for version 1.5
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          fnd_lookup_values_vl flv,
          ra_terms rt -- ,hz_cust_site_uses_all hcsu                --TMS#20150921-00332  by Siva    on 04-12-2016                                           -- ????? > Start
                     --,hz_cust_acct_sites_all hcas
                     -- ,hz_party_sites hps                        --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          oe_order_sources oos                                  -- ????? < End
    WHERE --    ol.sold_to_org_id = cust_acct.cust_account_id(+)      --TMS#20150921-00332  by Siva    on 04-12-2016
              -- AND hcsu.site_use_id = oh.invoice_to_org_id                                         -- ????? > Start
              --  AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
              -- AND hps.party_site_id = hcas.party_site_id
              oos.order_source_id = oh.order_source_id          -- ????? < End
          -- AND cust_acct.party_id = party.party_id(+)                                    --TMS#20150921-00332  by Siva    on 04-12-2016
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
--          AND oh.salesrep_id = rep.salesrep_id(+) --Commented for version 1.5
--          AND oh.org_id = rep.org_id(+) --Commented for version 1.5
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND ol.org_id = otl.org_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND oth.name != 'INTERNAL ORDER'
          AND FLV.LOOKUP_CODE(+) = OH.FLOW_STATUS_CODE
          /*  AND (   (    oth.name = 'STANDARD ORDER'
          AND (   ol.user_item_description = 'DELIVERED'
          or (OL.SOURCE_TYPE_CODE = 'EXTERNAL' and OL.FLOW_STATUS_CODE = 'INVOICE_HOLD')))*/
          AND OTH.name IN ('REPAIR ORDER', 'RETURN ORDER')
          AND ol.flow_status_code = 'INVOICE_HOLD'
          --  OR (OTH.NAME                 IN ('WC SHORT TERM RENTAL','WC LONG TERM RENTAL')
          --  AND ol.flow_status_code      IN ('INVOICE_HOLD','CLOSED'))
          /*OR (EXISTS
          (SELECT 1
          FROM ra_interface_lines_all ril
          WHERE     ril.interface_line_context = 'ORDER ENTRY'
          AND TO_CHAR (ol.line_id) = ril.interface_line_attribute6
          AND TO_CHAR (OH.ORDER_NUMBER) = RIL.INTERFACE_LINE_ATTRIBUTE1))*/
          --  )
          AND TRUNC (SYSDATE) =
                 TRUNC (XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM)
          AND OL.SOURCE_TYPE_CODE IN ('INTERNAL', 'EXTERNAL')
          AND NOT EXISTS
                 (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/  --Added for version 1.4  
                         'Y'    
                    FROM gl_code_combinations_kfv gcc
                   WHERE     gcc.code_combination_id =
                                msi.cost_of_sales_account
                         AND GCC.SEGMENT4 = '646080')
	--- UNION 4-3  
 	UNION
   SELECT 
          oh.order_number,
          oos.name order_source                                       -- ?????
                               --   hps.party_site_number job_number
          ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Party_Site_num_f (
             oh.invoice_to_org_id)
             job_number         --TMS#20150921-00332  by Siva    on 04-12-2016
                       -- ?????
          ,
          TRUNC (oh.ordered_date) ordered_date     --ppf.full_name created_by,
                                              ,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status         -- ,cust_acct.account_number customer_number
                         --,NVL (cust_acct.account_name, party.party_name) customer_name
          ,
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             'ACCOUNTNUM')
             CUSTOMER_NUMBER,   --TMS#20150921-00332  by Siva    on 04-12-2016
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             NULL)
             CUSTOMER_NAME,     --TMS#20150921-00332  by Siva    on 04-12-2016
          (  NVL (ol.unit_selling_price, 0)
           * DECODE (ol.line_category_code,
                     'RETURN', (ol.ordered_quantity * -1),
                     ol.ordered_quantity))
             gross_amount,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_discount_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             discount_amt,      --TMS#20150921-00332  by Siva    on 04-12-2016
          (NVL (
              DECODE (ol.line_category_code,
                      'RETURN', (ol.tax_value * -1),
                      ol.tax_value),
              0))
             tax_value,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_freight_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             freight_amt,       --TMS#20150921-00332  by Siva    on 04-12-2016
          --DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost, cm
          --DECODE(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost),0,xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id,msi.organization_id),NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost,
          /*decode(mcvc.segment2,
          'PRMO',
          (-1 * ol.unit_selling_price),
          apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,*/
          -- Changed
          CASE
             WHEN mcvc.segment2 = 'PRMO'
             THEN
                (-1 * ol.unit_selling_price)
             WHEN ol.flow_status_code IN ('CLOSED',
                                          'INVOICED',
                                          'INVOICED_PARTIAL',
                                          'INVOICE_DELIVERY',
                                          'INVOICE_HOLD',
                                          'INVOICE_INCOMPLETE')
             THEN
                --  apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) sv
                xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                   OL.LINE_ID)  --TMS#20150921-00332  by Siva    on 04-12-2016
             WHEN otl.name = 'BILL ONLY'
             THEN
                0
             ELSE
                NVL (
                   apps.cst_cost_api.get_item_cost (1,
                                                    msi.inventory_item_id,
                                                    msi.organization_id),
                   0)
          END
             average_cost, --(((nvl(ol.unit_selling_price,0) * ol.ordered_quantity)-
          --(NVL(DECODE(APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID),0,
          --OL.UNIT_COST,APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID) ),0) * OL.ORDERED_QUANTITY))) PROFIT,
          --((NVL(ol.unit_selling_price,0) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))- (DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity) * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) , 0))) profit,
          0 profit,
--          rep.name,  --Commented for version 1.5
		 xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name, --Added for version 1.5
          NULL trx_number,
--          rep.salesrep_number, --Commented for version 1.5
		 xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNUM') salesrep_number, --Added for version 1.5
          ship_from_org.organization_code warehouse,
          NULL inv_amount, -- (NVL(OL.UNIT_SELLING_PRICE,0)                                                                                                                                 * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY))SALES,
          --(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))sale_cost,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (CASE
                       WHEN ol.flow_status_code IN ('CLOSED',
                                                    'INVOICED',
                                                    'INVOICED_PARTIAL',
                                                    'INVOICE_DELIVERY',
                                                    'INVOICE_HOLD',
                                                    'INVOICE_INCOMPLETE')
                       THEN
                          --  apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) sv
                          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                             OL.LINE_ID) --TMS#20150921-00332  by Siva    on 04-12-2016
                       WHEN otl.name = 'BILL ONLY'
                       THEN
                          0
                       ELSE
                          NVL (
                             apps.cst_cost_api.get_item_cost (
                                1,
                                msi.inventory_item_id,
                                msi.organization_id),
                             0)
                    END)
                 * (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))),
              0))
             sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          NULL invoice_date -- ,xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (oh.header_id, ol.line_id) modifier_name
                           ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,     --TMS#20150921-00332  by Siva    on 04-12-2016
          ol.flow_status_code line_flow_status_code, -- 'Waiting for Invoices' TYPE,
          mcvc.segment2 cat /*, (SELECT xxcpl.special_cost
                            FROM apps.oe_price_adjustments adj
                            ,apps.qp_list_lines qll
                            ,apps.qp_list_headers qlh
                            ,apps.xxwc_om_contract_pricing_hdr xxcph
                            ,apps.xxwc_om_contract_pricing_lines xxcpl
                            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                            AND xxcpl.agreement_id = xxcph.agreement_id
                            AND xxcpl.agreement_type = 'VQN'
                            AND qlh.list_header_id = qll.list_header_id
                            AND qlh.list_header_id = adj.list_header_id
                            AND qll.list_line_id = adj.list_line_id
                            AND adj.list_line_type_code = 'DIS'
                            AND adj.applied_flag = 'Y'
                            AND adj.header_id = ol.header_id
                            AND adj.line_id = ol.line_id
                            AND xxcpl.product_value = msi.segment1
                            AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                            AND ROWNUM = 1)
                            special_cost
                            , (SELECT qlh.name
                            FROM apps.oe_price_adjustments adj
                            ,apps.qp_list_lines qll
                            ,apps.qp_list_headers qlh
                            ,apps.xxwc_om_contract_pricing_hdr xxcph
                            ,apps.xxwc_om_contract_pricing_lines xxcpl
                            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                            AND xxcpl.agreement_id = xxcph.agreement_id
                            AND xxcpl.agreement_type = 'VQN'
                            AND qlh.list_header_id = qll.list_header_id
                            AND qlh.list_header_id = adj.list_header_id
                            AND qll.list_line_id = adj.list_line_id
                            AND adj.list_line_type_code = 'DIS'
                            AND adj.applied_flag = 'Y'
                            AND adj.header_id = ol.header_id
                            AND adj.line_id = ol.line_id
                            AND xxcpl.product_value = msi.segment1
                            AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                            AND ROWNUM = 1)
                            vqn_modifier_name*/
                           ,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             'SPECIAL')
             SPECIAL_COST,      --TMS#20150921-00332  by Siva    on 04-12-2016
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             NULL)
             VQN_MODIFIER_NAME, --TMS#20150921-00332  by Siva    on 04-12-2016
          ---Primary Keys
          oh.header_id,
          NULL party_id,                                    -- Rct.trx_number,
          ship_from_org.organization_id,
          otl.name line_type
     --ol.ship_from_org_id organization_id
     --added
     --NULL extended_sale,
     --NULL extended_cost
     --    msi.inventory_item_id ,
     --    msi.organization_id msi_organization_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org --,hz_parties party                            --TMS#20150921-00332  by Siva    on 04-12-2016
                                      --,hz_cust_accounts cust_acct                  --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
--          ra_salesreps rep,  --Commented for version 1.5
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          fnd_lookup_values_vl flv,
          ra_terms rt -- ,hz_cust_site_uses_all hcsu                --TMS#20150921-00332  by Siva    on 04-12-2016                                           -- ????? > Start
                     --,hz_cust_acct_sites_all hcas                --TMS#20150921-00332  by Siva    on 04-12-2016
                     -- ,hz_party_sites hps                        --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          oe_order_sources oos                                  -- ????? < End
    WHERE --    ol.sold_to_org_id = cust_acct.cust_account_id(+)       --TMS#20150921-00332  by Siva    on 04-12-2016
              -- AND hcsu.site_use_id = oh.invoice_to_org_id                                         -- ????? > Start
              --  AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id               --TMS#20150921-00332  by Siva    on 04-12-2016
              -- AND hps.party_site_id = hcas.party_site_id                          --TMS#20150921-00332  by Siva    on 04-12-2016
              oos.order_source_id = oh.order_source_id          -- ????? < End
          -- AND cust_acct.party_id = party.party_id(+)                                     --TMS#20150921-00332  by Siva    on 04-12-2016
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
--          AND oh.salesrep_id = rep.salesrep_id(+)  --Commented for version 1.5
--          AND oh.org_id = rep.org_id(+)  --Commented for version 1.5
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND ol.org_id = otl.org_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND oth.name != 'INTERNAL ORDER'
          AND FLV.LOOKUP_CODE(+) = OH.FLOW_STATUS_CODE
          /*  AND (   (    oth.name = 'STANDARD ORDER'
          AND (   ol.user_item_description = 'DELIVERED'
          or (OL.SOURCE_TYPE_CODE = 'EXTERNAL' and OL.FLOW_STATUS_CODE = 'INVOICE_HOLD')))*/
          --AND OTH.name IN ('REPAIR ORDER', 'RETURN ORDER')
          --AND ol.flow_status_code = 'INVOICE_HOLD'
          --  OR (OTH.NAME                 IN ('WC SHORT TERM RENTAL','WC LONG TERM RENTAL')
          --  AND ol.flow_status_code      IN ('INVOICE_HOLD','CLOSED'))*/
          AND (EXISTS
                  (SELECT 1
                     FROM ra_interface_lines_all ril
                    WHERE     ril.interface_line_context = 'ORDER ENTRY'
                          AND TO_CHAR (ol.line_id) =
                                 ril.interface_line_attribute6
                          AND TO_CHAR (OH.ORDER_NUMBER) =
                                 RIL.INTERFACE_LINE_ATTRIBUTE1))
          AND TRUNC (SYSDATE) =
                 TRUNC (XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM)
          AND OL.SOURCE_TYPE_CODE IN ('INTERNAL', 'EXTERNAL')
          -- and OH.ORDER_NUMBER IN( 12916583,12916584)
          --   AND OH.HEADER_ID = 36228149
          AND NOT EXISTS
                 (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/ --Added for version 1.4  
                         'Y'    
                    FROM gl_code_combinations_kfv gcc
                   WHERE     gcc.code_combination_id =
                                msi.cost_of_sales_account
                         AND GCC.SEGMENT4 = '646080')
   UNION
      -- for rental  orders before invoice
   SELECT /*+ INDEX(ol XXWC_OE_ORDER_LN_AL_N13) USE_NL(ol,oh) USE_NL(ol,msi)*/ --Added for version 1.4  
         oh.order_number,
         oos.name order_source,                                       -- ?????
         --  hps.party_site_number job_number
         xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Party_Site_num_f (
            oh.invoice_to_org_id)
            job_number, --TMS#20150921-00332  by Siva    on 04-12-2016  -- ?????
         TRUNC (oh.ordered_date) ordered_date,     --ppf.full_name created_by,
         oh.attribute7 created_by,
         oth.name order_type,
         NVL (flv.meaning, INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
            order_status         --  ,cust_acct.account_number customer_number
                        -- ,NVL (cust_acct.account_name, party.party_name) customer_name
         ,
         XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
            OL.SOLD_TO_ORG_ID,
            'ACCOUNTNUM')
            CUSTOMER_NUMBER,    --TMS#20150921-00332  by Siva    on 04-12-2016
         XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
            OL.SOLD_TO_ORG_ID,
            NULL)
            CUSTOMER_NAME,      --TMS#20150921-00332  by Siva    on 04-12-2016
         (  NVL (ol.unit_selling_price, 0)
          * DECODE (ol.line_category_code,
                    'RETURN', (ol.ordered_quantity * -1),
                    ol.ordered_quantity))
            gross_amount,
         (NVL (
             xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_discount_amt (ol.header_id,
                                                                  ol.line_id),
             0))
            discount_amt,       --TMS#20150921-00332  by Siva    on 04-12-2016
         (NVL (
             DECODE (ol.line_category_code,
                     'RETURN', (ol.tax_value * -1),
                     ol.tax_value),
             0))
            tax_value,
         (NVL (
             xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_freight_amt (ol.header_id,
                                                                 ol.line_id),
             0))
            freight_amt,        --TMS#20150921-00332  by Siva    on 04-12-2016
         --DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost, cm
         --DECODE(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost),0,xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id,msi.organization_id),NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost,
         /*decode(mcvc.segment2,
         'PRMO',
         (-1 * ol.unit_selling_price),
         apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,*/
         -- Changed
         CASE
            WHEN mcvc.segment2 = 'PRMO'
            THEN
               (-1 * ol.unit_selling_price)
            WHEN ol.flow_status_code IN ('CLOSED',
                                         'INVOICED',
                                         'INVOICED_PARTIAL',
                                         'INVOICE_DELIVERY',
                                         'INVOICE_HOLD',
                                         'INVOICE_INCOMPLETE')
            THEN
               --  apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
               xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (OL.LINE_ID) --TMS#20150921-00332  by Siva    on 04-12-2016
            WHEN otl.name = 'BILL ONLY'
            THEN
               0
            ELSE
               NVL (
                  apps.cst_cost_api.get_item_cost (1,
                                                   msi.inventory_item_id,
                                                   msi.organization_id),
                  0)
         END
            average_cost, --(((nvl(ol.unit_selling_price,0) * ol.ordered_quantity)-
         --(NVL(DECODE(APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID),0,
         --OL.UNIT_COST,APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID) ),0) * OL.ORDERED_QUANTITY))) PROFIT,
         --((NVL(ol.unit_selling_price,0) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))- (DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity) * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) , 0))) profit,
         0 profit,
--         rep.name,  --Commented for version 1.5
		xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name, --Added for version 1.5
         NULL trx_number,
--         rep.salesrep_number,  --Commented for version 1.5
		xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNUM') salesrep_number, --Added for version 1.5
         ship_from_org.organization_code warehouse,
         NULL inv_amount, -- (NVL(OL.UNIT_SELLING_PRICE,0)                                                                                                                                 * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY))SALES,
         --(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))sale_cost,
         (NVL (
             DECODE (
                mcvc.segment2,
                'PRMO', 0,
                  NVL (ol.unit_selling_price, 0)
                * DECODE (ol.line_category_code,
                          'RETURN', (ol.ordered_quantity * -1),
                          ol.ordered_quantity)),
             0))
            sales,
         (NVL (
             DECODE (
                mcvc.segment2,
                'PRMO',   -1
                        * (  NVL (ol.unit_selling_price, 0)
                           * DECODE (ol.line_category_code,
                                     'RETURN', (ol.ordered_quantity * -1),
                                     ol.ordered_quantity)),
                  (CASE
                      WHEN ol.flow_status_code IN ('CLOSED',
                                                   'INVOICED',
                                                   'INVOICED_PARTIAL',
                                                   'INVOICE_DELIVERY',
                                                   'INVOICE_HOLD',
                                                   'INVOICE_INCOMPLETE')
                      THEN
                         --   apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
                         xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                            OL.LINE_ID) --TMS#20150921-00332  by Siva    on 04-12-2016
                      WHEN otl.name = 'BILL ONLY'
                      THEN
                         0
                      ELSE
                         NVL (
                            apps.cst_cost_api.get_item_cost (
                               1,
                               msi.inventory_item_id,
                               msi.organization_id),
                            0)
                   END)
                * (DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity))),
             0))
            sale_cost,
         rt.name payment_term,
         rt.description payment_desc,
         msi.segment1 item,
         CASE
            WHEN msi.segment1 = 'Rental Charge' THEN ol.user_item_description
            ELSE msi.description
         END
            item_desc,
         DECODE (ol.line_category_code,
                 'RETURN', (ol.ordered_quantity * -1),
                 ol.ordered_quantity)
            qty,
         ol.order_quantity_uom uom,
         DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
            unit_selling_price,
         DECODE (
            ol.option_number,
            '', (ol.line_number || '.' || ol.shipment_number),
            (   ol.line_number
             || '.'
             || ol.shipment_number
             || '.'
             || ol.option_number))
            order_line,
         NULL invoice_date -- ,xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (oh.header_id, ol.line_id) modifier_name
                          ,
         xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_line_modifier_name (oh.header_id,
                                                                    ol.line_id)
            modifier_name,      --TMS#20150921-00332  by Siva    on 04-12-2016
         ol.flow_status_code line_flow_status_code, -- 'Waiting for Invoices' TYPE,
         mcvc.segment2 cat /* , (SELECT xxcpl.special_cost
                           FROM apps.oe_price_adjustments adj
                           ,apps.qp_list_lines qll
                           ,apps.qp_list_headers qlh
                           ,apps.xxwc_om_contract_pricing_hdr xxcph
                           ,apps.xxwc_om_contract_pricing_lines xxcpl
                           WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                           AND xxcpl.agreement_id = xxcph.agreement_id
                           AND xxcpl.agreement_type = 'VQN'
                           AND qlh.list_header_id = qll.list_header_id
                           AND qlh.list_header_id = adj.list_header_id
                           AND qll.list_line_id = adj.list_line_id
                           AND adj.list_line_type_code = 'DIS'
                           AND adj.applied_flag = 'Y'
                           AND adj.header_id = ol.header_id
                           AND adj.line_id = ol.line_id
                           AND xxcpl.product_value = msi.segment1
                           AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                           AND ROWNUM = 1)
                           special_cost
                           , (SELECT qlh.name
                           FROM apps.oe_price_adjustments adj
                           ,apps.qp_list_lines qll
                           ,apps.qp_list_headers qlh
                           ,apps.xxwc_om_contract_pricing_hdr xxcph
                           ,apps.xxwc_om_contract_pricing_lines xxcpl
                           WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                           AND xxcpl.agreement_id = xxcph.agreement_id
                           AND xxcpl.agreement_type = 'VQN'
                           AND qlh.list_header_id = qll.list_header_id
                           AND qlh.list_header_id = adj.list_header_id
                           AND qll.list_line_id = adj.list_line_id
                           AND adj.list_line_type_code = 'DIS'
                           AND adj.applied_flag = 'Y'
                           AND adj.header_id = ol.header_id
                           AND adj.line_id = ol.line_id
                           AND xxcpl.product_value = msi.segment1
                           AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                           AND ROWNUM = 1)
                           vqn_modifier_name*/
                          ,
         xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (ol.header_id,
                                                                 ol.line_id,
                                                                 msi.segment1,
                                                                 'SPECIAL')
            SPECIAL_COST,       --TMS#20150921-00332  by Siva    on 04-12-2016
         xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (ol.header_id,
                                                                 ol.line_id,
                                                                 msi.segment1,
                                                                 NULL)
            VQN_MODIFIER_NAME,  --TMS#20150921-00332  by Siva    on 04-12-2016
         ---Primary Keys
         oh.header_id,
         NULL party_id,                                     -- Rct.trx_number,
         ship_from_org.organization_id,
         otl.name line_type
    --ol.ship_from_org_id organization_id
    --added
    --NULL extended_sale,
    --NULL extended_cost
    --    msi.inventory_item_id ,
    --    msi.organization_id msi_organization_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
    FROM mtl_parameters ship_from_org 
-- ,hz_parties party                  --TMS#20150921-00332  by Siva    on 04-12-2016                            
 --,hz_cust_accounts cust_acct         --TMS#20150921-00332  by Siva    on 04-12-2016
         ,      oe_order_headers oh,
         oe_order_lines ol,
         hr_all_organization_units hou,
--         ra_salesreps rep, --Commented for version 1.5
         oe_transaction_types_vl oth,
         oe_transaction_types_vl otl,
         mtl_system_items_kfv msi,
         mtl_categories_kfv mcvc,
         mtl_item_categories micc,
         fnd_lookup_values_vl flv,
         ra_terms rt --  ,hz_cust_site_uses_all hcsu                --TMS#20150921-00332  by Siva    on 04-12-2016                                          -- ????? > Start
                    --  ,hz_cust_acct_sites_all hcas    --TMS#20150921-00332  by Siva    on 04-12-2016
                    --   ,hz_party_sites hps      --TMS#20150921-00332  by Siva    on 04-12-2016
         ,
         oe_order_sources oos                                   -- ????? < End
   WHERE --  ol.sold_to_org_id = cust_acct.cust_account_id(+)           --TMS#20150921-00332  by Siva    on 04-12-2016
             --AND hcsu.site_use_id = oh.invoice_to_org_id                                      --TMS#20150921-00332  by Siva    on 04-12-2016    -- ????? > Start
             --AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id                  --TMS#20150921-00332  by Siva    on 04-12-2016
             -- AND hps.party_site_id = hcas.party_site_id                 --TMS#20150921-00332  by Siva    on 04-12-2016
             oos.order_source_id = oh.order_source_id           -- ????? < End
         --AND cust_acct.party_id = party.party_id(+)   --TMS#20150921-00332  by Siva    on 04-12-2016
         AND ol.ship_from_org_id = ship_from_org.organization_id(+)
         AND ol.header_id = oh.header_id
         AND msi.organization_id = hou.organization_id
--         AND oh.salesrep_id = rep.salesrep_id(+) --Commented for version 1.5
--         AND oh.org_id = rep.org_id(+) --Commented for version 1.5
         AND oh.payment_term_id = rt.term_id
         AND msi.inventory_item_id(+) = ol.inventory_item_id
         AND msi.organization_id(+) = ol.ship_from_org_id
         AND msi.inventory_item_id = micc.inventory_item_id(+)
         AND msi.organization_id = micc.organization_id(+)
         AND micc.category_id = mcvc.category_id(+)
         AND mcvc.structure_id(+) = 101
         AND micc.category_set_id(+) = 1100000062
         AND oh.order_type_id = oth.transaction_type_id
         AND oh.org_id = oth.org_id
         AND ol.flow_status_code <> 'CANCELLED'
         -- AND ol.flow_status_code NOT  IN ('CLOSED', 'CANCELLED')
         AND ol.line_type_id = otl.transaction_type_id
         AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
         AND ol.org_id = otl.org_id
         AND flv.lookup_type(+) = 'FLOW_STATUS'
         AND oth.name != 'INTERNAL ORDER'
         AND flv.lookup_code(+) = oh.flow_status_code
         AND ol.line_type_id NOT IN (1015, 1007)
         AND (   (    oth.name IN ('WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL')
                   AND ol.flow_status_code IN ('INVOICE_HOLD', 'CLOSED')
                   AND TRUNC (oh.creation_date) = TRUNC (SYSDATE)
                   --  AND TRUNC(OL.CREATION_DATE)       >= to_date(TO_CHAR(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
                                                 --    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
                                                                                --    || ' HH24:MI:SS') + 0.25
 --  AND TRUNC(OL.CREATION_DATE) <= to_date(TO_CHAR(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
                                                 --    || ' HH24:MI:SS'), XXEIS.EIS_RS_UTILITY.GET_DATE_FORMAT
                                                                        --    || ' HH24:MI:SS') + 1.25
                  )
               --  OR (EXISTS
               --    (SELECT 1
               --    FROM ra_interface_lines_all ril
               --    WHERE ril.interface_line_context = 'ORDER ENTRY'
               --    AND TO_CHAR(OL.LINE_ID)          = RIL.INTERFACE_LINE_ATTRIBUTE6
               --      AND TO_CHAR (OH.ORDER_NUMBER)      = RIL.INTERFACE_LINE_ATTRIBUTE1
               --    ))
               OR (    oth.name IN ('WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL')
                   AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
                   AND ol.flow_status_code IN ('INVOICE_HOLD')
                   AND TRUNC (SYSDATE) = TRUNC (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from)))
          -- AND TRUNC(SYSDATE)       = TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
          AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
          AND NOT EXISTS
                     (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/ 'Y' --Added for version 1.4  
                        FROM gl_code_combinations_kfv gcc
                       WHERE     1 = 1
                             AND GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
                             AND gcc.segment4 = '646080')
   UNION
   SELECT oh.order_number,
          oos.name order_source,                                      -- ?????
          -- hps.party_site_number job_number
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Party_Site_num_f (
             oh.invoice_to_org_id)
             job_number, -- ????? --TMS#20150921-00332  by Siva    on 04-12-2016
          TRUNC (oh.ordered_date) ordered_date,
          oh.attribute7 created_by,
          otlh.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status               -- ,hca.account_number customer_number
                         --,NVL (hca.account_name, hp.party_name) customer_name
          ,
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             rct.bill_to_customer_id,
             'ACCOUNTNUM')
             CUSTOMER_NUMBER,   --TMS#20150921-00332  by Siva    on 04-12-2016
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             rct.bill_to_customer_id,
             NULL)
             CUSTOMER_NAME,     --TMS#20150921-00332  by Siva    on 04-12-2016
          (  NVL (rctl.unit_selling_price, 0)
           * DECODE (
                otlh.name,
                'RETURN', (  NVL (rctl.quantity_invoiced,
                                  rctl.quantity_credited)
                           * -1),
                NVL (rctl.quantity_invoiced, rctl.quantity_credited)))
             gross_amount,
          NULL discount_amt,                                 --NULL tax_value,
          (NVL (
              DECODE (otlh.name,
                      'RETURN', (rctl.taxable_amount * -1),
                      rctl.taxable_amount),
              0))
             tax_value,
          NULL freight_amt,
          NVL (
             xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost (
                msi.inventory_item_id,
                msi.organization_id),
             0)
             average_cost,      --TMS#20150921-00332  by Siva    on 04-12-2016
          0 profit,
--          rep.name, --Commented for version 1.5
		  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name, --Added for version 1.5
          rct.trx_number trx_number,
--          rep.salesrep_number salesrep_number, --Commented for version 1.5
		  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNUM') salesrep_number, --Added for version 1.5
          mp.organization_code warehouse,
          rctl.extended_amount inv_amount,                       --null sales,
          --null sale_cost,
          --nvl(DECODE(ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)* NVL(rctl.unit_selling_price, 0), 0) sales,
          NVL (
               NVL (rctl.quantity_invoiced, rctl.quantity_credited)
             * NVL (rctl.unit_selling_price, 0),
             0)
             sales,
          0 sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          msi.description item_desc,                               --null qty,
          NVL (rctl.quantity_invoiced, rctl.quantity_credited) qty,
          rctl.uom_code uom,
          rctl.unit_selling_price unit_selling_price,
          NULL order_line,
          rct.trx_date invoice_date,
          NULL modifier_name,
          NULL line_flow_status_code,              -- 'Delivery Charges' TYPE,
          NULL cat,
          NULL special_cost,
          NULL vqn_modifier_name,                              ---Primary Keys
          oh.header_id,
          NULL party_id,
          mp.organization_id,
          NULL line_type
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM ra_customer_trx rct,
          ra_customer_trx_lines rctl,
          oe_order_headers oh,             --ORG_ORGANIZATION_DEFINITIONS OOD,
          oe_transaction_types_vl otlh,
          mtl_parameters mp,
          mtl_system_items_kfv msi --  ,hz_cust_accounts hca        --TMS#20150921-00332  by Siva    on 04-12-2016
                                  --  ,hz_parties hp            --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          fnd_lookup_values_vl flv,
--          ra_salesreps rep, --Commented for version 1.5
          ra_terms rt -- ,hz_cust_site_uses_all hcsu              --TMS#20150921-00332  by Siva    on 04-12-2016                                             -- ????? > Start
                     -- ,hz_cust_acct_sites_all hcas       --TMS#20150921-00332  by Siva    on 04-12-2016
                     -- ,hz_party_sites hps         --TMS#20150921-00332  by Siva    on 04-12-2016
          ,
          oe_order_sources oos                                  -- ????? < End
    WHERE -- AND hcsu.site_use_id = oh.invoice_to_org_id                        --TMS#20150921-00332  by Siva    on 04-12-2016                 -- ????? > Start
              -- AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id     --TMS#20150921-00332  by Siva    on 04-12-2016
              --AND hps.party_site_id = hcas.party_site_id     --TMS#20150921-00332  by Siva    on 04-12-2016
              oos.order_source_id = oh.order_source_id          -- ????? < End
          /* AND rctl.creation_date >=
          TO_DATE (
          TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          + 0.25
          AND rctl.creation_date <=
          TO_DATE (
          TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')                 --TMS#20150921-00332  by Siva    on 04-12-2016
          ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
          + 1.25*/
          AND RCTL.CREATION_DATE >=
                 XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM + 0.25 --TMS#20150921-00332  by Siva    on 04-12-2016
          AND RCTL.CREATION_DATE <=
                 XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM + 1.25 --TMS#20150921-00332  by Siva    on 04-12-2016
          AND rctl.customer_trx_id = rct.customer_trx_id
          --AND RCT.INTERFACE_HEADER_ATTRIBUTE1  ='11435332'
          AND mp.organization_id = oh.ship_from_org_id
          AND rct.interface_header_context = 'ORDER ENTRY'
          --AND mp.organization_code = '017'
          -- AND TO_CHAR (oh.order_number) = rct.interface_header_attribute1 sv
          --   AND TO_CHAR (ol.line_id) = rctl.interface_line_attribute6
          AND TO_CHAR (oh.order_number) = rctl.interface_line_attribute1
          AND rctl.interface_line_context = 'ORDER ENTRY'
          --  AND TO_CHAR (oh.order_number) = rctl.interface_line_attribute1 sv
          AND otlh.name = rctl.interface_line_attribute2
          AND rctl.description = 'Delivery Charge'
          AND rctl.line_type = 'LINE'
          AND otlh.transaction_type_id = oh.order_type_id
          AND mp.organization_id = msi.organization_id
          AND RCTL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
          --AND RCT.BILL_TO_CUSTOMER_ID = HCA.CUST_ACCOUNT_ID
          --AND hp.party_id = hca.party_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND flv.lookup_code(+) = oh.flow_status_code
--          AND oh.salesrep_id 	= rep.salesrep_id(+) --Commented for version 1.5
--          AND oh.org_id 		= rep.org_id(+) --Commented for version 1.5
          AND OH.PAYMENT_TERM_ID = RT.TERM_ID
		  AND NOT EXISTS    --added for version 1.2 TMS#20160606-00009
			(SELECT 1
			FROM OE_ORDER_LINES_ALL
			WHERE LINE_ID =RCTL.INTERFACE_LINE_ATTRIBUTE6
			)
   UNION  --Added new union for --TMS#20150810-00058  by Siva    on 05-03-2016
	/* --Commented for version 1.3
   SELECT 
         OH.ORDER_NUMBER,
          oos.name order_source,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Party_Site_num_f (
             oh.invoice_to_org_id)
             job_number         --TMS#20150921-00332  by Siva    on 04-12-2016
                       ,
          TRUNC (oh.ordered_date) ordered_date,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status,
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             'ACCOUNTNUM')
             CUSTOMER_NUMBER,   --TMS#20150921-00332  by Siva    on 04-12-2016
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             NULL)
             CUSTOMER_NAME,     --TMS#20150921-00332  by Siva    on 04-12-2016
          (  NVL (ol.unit_selling_price, 0)
           * DECODE (ol.line_category_code,
                     'RETURN', (ol.ordered_quantity * -1),
                     ol.ordered_quantity))
             gross_amount,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_discount_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             discount_amt,      --TMS#20150921-00332  by Siva    on 04-12-2016
          (NVL (
              DECODE (ol.line_category_code,
                      'RETURN', (ol.tax_value * -1),
                      ol.tax_value),
              0))
             tax_value,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_freight_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             freight_amt,       --TMS#20150921-00332  by Siva    on 04-12-2016
          CASE
             WHEN mcvc.segment2 = 'PRMO'
             THEN
                (-1 * ol.unit_selling_price)
             WHEN ol.flow_status_code IN ('CLOSED',
                                          'INVOICED',
                                          'INVOICED_PARTIAL',
                                          'INVOICE_DELIVERY',
                                          'INVOICE_HOLD',
                                          'INVOICE_INCOMPLETE')
             THEN
                xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                   OL.LINE_ID)  --TMS#20150921-00332  by Siva    on 04-12-2016
             WHEN otl.name = 'BILL ONLY'
             THEN
                0
             ELSE
                NVL (
                   apps.cst_cost_api.get_item_cost (1,
                                                    msi.inventory_item_id,
                                                    msi.organization_id),
                   0)
          END
             average_cost,
          0 profit,
          rep.name,
          NULL trx_number,
          rep.salesrep_number,
          ship_from_org.organization_code warehouse,
          NULL inv_amount,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (CASE
                       WHEN ol.flow_status_code IN ('CLOSED',
                                                    'INVOICED',
                                                    'INVOICED_PARTIAL',
                                                    'INVOICE_DELIVERY',
                                                    'INVOICE_HOLD',
                                                    'INVOICE_INCOMPLETE')
                       THEN
                          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                             OL.LINE_ID) --TMS#20150921-00332  by Siva    on 04-12-2016
                       WHEN otl.name = 'BILL ONLY'
                       THEN
                          0
                       ELSE
                          NVL (
                             apps.cst_cost_api.get_item_cost (
                                1,
                                msi.inventory_item_id,
                                msi.organization_id),
                             0)
                    END)
                 * (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))),
              0))
             sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          NULL invoice_date,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,     --TMS#20150921-00332  by Siva    on 04-12-2016
          ol.flow_status_code line_flow_status_code,
          mcvc.segment2 cat,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             'SPECIAL')
             SPECIAL_COST,      --TMS#20150921-00332  by Siva    on 04-12-2016
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             NULL)
             VQN_MODIFIER_NAME, --TMS#20150921-00332  by Siva    on 04-12-2016
          ---Primary Keys
          oh.header_id,
          NULL party_id,
          ship_from_org.organization_id,
          otl.name line_type
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
          ra_salesreps rep,
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          FND_LOOKUP_VALUES_VL FLV,
          ra_terms rt,
          OE_ORDER_SOURCES OOS
    WHERE     oos.order_source_id = oh.order_source_id
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND ol.org_id = otl.org_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND oth.name != 'INTERNAL ORDER'
          AND FLV.LOOKUP_CODE(+) = OH.FLOW_STATUS_CODE
          AND OTH.name = 'STANDARD ORDER'
          AND OL.USER_ITEM_DESCRIPTION = 'OUT_FOR_DELIVERY'
          AND OL.FLOW_STATUS_CODE = 'AWAITING_SHIPPING'
          AND TRUNC(SYSDATE)       = TRUNC(xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from)   --TMS#20150921-00332  by Siva    on 04-12-2016
          AND OL.SOURCE_TYPE_CODE IN ('INTERNAL', 'EXTERNAL')
          AND NOT EXISTS
                 (SELECT
                         'Y'   
                    FROM gl_code_combinations_kfv gcc
                   WHERE     gcc.code_combination_id =
                                msi.cost_of_sales_account
                         AND GCC.SEGMENT4 = '646080')
          AND EXISTS
                 (SELECT 1
                    FROM XXWC.XXWC_SIGNATURE_CAPTURE_TBL XSCT
                   WHERE XSCT.ID = OH.HEADER_ID)		
   UNION  */  --Commented for version 1.3
 SELECT 
         OH.ORDER_NUMBER,
          oos.name order_source,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Party_Site_num_f (
             oh.invoice_to_org_id)
             job_number         --TMS#20150921-00332  by Siva    on 04-12-2016
                       ,
          TRUNC (oh.ordered_date) ordered_date,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status,
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             'ACCOUNTNUM')
             CUSTOMER_NUMBER,   --TMS#20150921-00332  by Siva    on 04-12-2016
          XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER (
             OL.SOLD_TO_ORG_ID,
             NULL)
             CUSTOMER_NAME,     --TMS#20150921-00332  by Siva    on 04-12-2016
          (  NVL (ol.unit_selling_price, 0)
           * DECODE (ol.line_category_code,
                     'RETURN', (ol.ordered_quantity * -1),
                     ol.ordered_quantity))
             gross_amount,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_discount_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             discount_amt,      --TMS#20150921-00332  by Siva    on 04-12-2016
          (NVL (
              DECODE (ol.line_category_code,
                      'RETURN', (ol.tax_value * -1),
                      ol.tax_value),
              0))
             tax_value,
          (NVL (
              xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_freight_amt (
                 ol.header_id,
                 ol.line_id),
              0))
             freight_amt,       --TMS#20150921-00332  by Siva    on 04-12-2016
          CASE
             WHEN mcvc.segment2 = 'PRMO'
             THEN
                (-1 * ol.unit_selling_price)
             WHEN ol.flow_status_code IN ('CLOSED',
                                          'INVOICED',
                                          'INVOICED_PARTIAL',
                                          'INVOICE_DELIVERY',
                                          'INVOICE_HOLD',
                                          'INVOICE_INCOMPLETE')
             THEN
                xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                   OL.LINE_ID)  --TMS#20150921-00332  by Siva    on 04-12-2016
             WHEN otl.name = 'BILL ONLY'
             THEN
                0
             ELSE
                NVL (
                   apps.cst_cost_api.get_item_cost (1,
                                                    msi.inventory_item_id,
                                                    msi.organization_id),
                   0)
          END
             average_cost,
          0 profit,
--          rep.name, --Commented for version 1.5
		  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNAME') salesrep_name, --Added for version 1.5
          NULL trx_number,
--          rep.salesrep_number, --Commented for version 1.5
		  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,'SALESREPNUM') salesrep_number, --Added for version 1.5
          ship_from_org.organization_code warehouse,
          NULL inv_amount,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (CASE
                       WHEN ol.flow_status_code IN ('CLOSED',
                                                    'INVOICED',
                                                    'INVOICED_PARTIAL',
                                                    'INVOICE_DELIVERY',
                                                    'INVOICE_HOLD',
                                                    'INVOICE_INCOMPLETE')
                       THEN
                          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (
                             OL.LINE_ID) --TMS#20150921-00332  by Siva    on 04-12-2016
                       WHEN otl.name = 'BILL ONLY'
                       THEN
                          0
                       ELSE
                          NVL (
                             apps.cst_cost_api.get_item_cost (
                                1,
                                msi.inventory_item_id,
                                msi.organization_id),
                             0)
                    END)
                 * (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))),
              0))
             sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          NULL invoice_date,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,     --TMS#20150921-00332  by Siva    on 04-12-2016
          ol.flow_status_code line_flow_status_code,
          mcvc.segment2 cat,
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             'SPECIAL')
             SPECIAL_COST,      --TMS#20150921-00332  by Siva    on 04-12-2016
          xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.Get_Special_Modifer (
             ol.header_id,
             ol.line_id,
             msi.segment1,
             NULL)
             VQN_MODIFIER_NAME, --TMS#20150921-00332  by Siva    on 04-12-2016
          ---Primary Keys
          oh.header_id,
          NULL party_id,
          ship_from_org.organization_id,
          otl.name line_type
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
--          ra_salesreps rep, --Commented for version 1.5
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          FND_LOOKUP_VALUES_VL FLV,
          ra_terms rt,
          OE_ORDER_SOURCES OOS
    WHERE     oos.order_source_id = oh.order_source_id
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
--          AND oh.salesrep_id = rep.salesrep_id(+) --Commented for version 1.5
--          AND oh.org_id = rep.org_id(+) --Commented for version 1.5
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND ol.org_id = otl.org_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND oth.name != 'INTERNAL ORDER'
          AND FLV.LOOKUP_CODE(+) = OH.FLOW_STATUS_CODE
          AND OTH.name = 'STANDARD ORDER'
          AND OL.USER_ITEM_DESCRIPTION = 'OUT_FOR_DELIVERY'
          AND OL.FLOW_STATUS_CODE = 'AWAITING_SHIPPING'
          AND TRUNC(SYSDATE)       = TRUNC(xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from)   --TMS#20150921-00332  by Siva    on 04-12-2016
          AND OL.SOURCE_TYPE_CODE IN ('INTERNAL', 'EXTERNAL')
          AND NOT EXISTS
                 (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
                         'Y'    --TMS#20150921-00332  by Siva    on 04-12-2016
                    FROM gl_code_combinations_kfv gcc
                   WHERE     gcc.code_combination_id =
                                msi.cost_of_sales_account
                         AND GCC.SEGMENT4 = '646080')
        --  AND EXISTS --Commented for version 1.3
          --       (SELECT /*+INDEX(XSCT XXWC_OM_DMS_SHIP_CONFIRM_N1)*/ 1  
            --        from XXWC.XXWC_OM_DMS_SHIP_CONFIRM_TBL XSCT
             --      where XSCT.ORDER_NUMBER = OH.ORDER_NUMBER) --Commented for version 1.3
/				   
