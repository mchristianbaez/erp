CREATE OR REPLACE FORCE VIEW apps.xxeis_wc_jobs_no_ntcd_rev
(
   job_number
  ,prism_job_number
  ,site_location
  ,site_creation_date
  ,site_limit
  ,site_credit_hold
  ,cust_acct_status
  ,site_address_1
  ,site_address_2
  ,site_address_3
  ,site_address_4
  ,site_city
  ,site_state
  ,zip_code
  ,account_number
  ,prism_account_number
  ,account_name
  ,account_profile_class
  ,job_info_on_file
  ,min_trx_date
)
AS
     SELECT DISTINCT e.party_site_number AS job_number
                    ,a.attribute17 AS prism_job_number
                    ,REGEXP_REPLACE (i.location, '[^[:alnum:]'' '']', ' ') AS site_location -- Added by Mahender For TMS#20150527-00286 on 06/10/2015
                    --,i.location AS site_location   -- Commeneted by Mahender For TMS#20150527-00286 on 06/10/2015
                    ,a.creation_date AS site_creation_date
                    ,c.overall_credit_limit AS site_limit
                    ,b.credit_hold AS site_credit_hold
                    ,b.account_status AS cust_acct_status
                    ,REGEXP_REPLACE (d.address1, '[^[:alnum:]'' '']', ' ') AS site_address_1 -- Added by Mahender For TMS#20150527-00286 on 06/10/2015
                    --                    ,d.address1 AS site_address_1  -- Commeneted by Mahender For TMS#20150527-00286 on 06/10/2015
                    ,REGEXP_REPLACE (d.address2, '[^[:alnum:]'' '']', ' ') AS site_address_2 -- Added by Mahender For TMS#20150527-00286 on 06/10/2015
                    --                    ,d.address2 AS site_address_2  -- Commeneted by Mahender For TMS#20150527-00286 on 06/10/2015
                    ,REGEXP_REPLACE (d.address3, '[^[:alnum:]'' '']', ' ') AS site_address_3 -- Added by Mahender For TMS#20150527-00286 on 06/10/2015
                    --                    ,d.address3 AS site_address_3  -- Commeneted by Mahender For TMS#20150527-00286 on 06/10/2015
                    ,REGEXP_REPLACE (d.address4, '[^[:alnum:]'' '']', ' ') AS site_address_4 -- Added by Mahender For TMS#20150527-00286 on 06/10/2015
                    --                    ,d.address4 AS site_address_4  -- Commeneted by Mahender For TMS#20150527-00286 on 06/10/2015
                    ,REGEXP_REPLACE (d.city, '[^[:alnum:]'' '']', ' ') AS site_city -- Added by Mahender For TMS#20150527-00286 on 06/10/2015
                    --                    ,d.city AS site_city  -- Commeneted by Mahender For TMS#20150527-00286 on 06/10/2015
                    ,REGEXP_REPLACE (d.state, '[^[:alnum:]'' '']', ' ') AS site_state -- Added by Mahender For TMS#20150527-00286 on 06/10/2015
                    --                    ,d.state AS site_state  -- Commeneted by Mahender For TMS#20150527-00286 on 06/10/2015
                    ,REGEXP_REPLACE (d.postal_code, '[^[:alnum:]'' '']', ' ') AS zip_code -- Added by Mahender For TMS#20150527-00286 on 06/10/2015
                    --                    ,d.postal_code AS zip_code   -- Commeneted by Mahender For TMS#20150527-00286 on 06/10/2015
                    ,f.account_number AS account_number
                    ,f.attribute6 AS prism_account_number
                    ,f.account_name AS account_name
                    ,g.name AS account_profile_class
                    ,a.attribute14 AS job_info_on_file
                    ,MIN (NVL (k.trx_date, '01-JAN-00')) min_trx_date
       FROM ar.hz_cust_acct_sites_all a
           ,ar.hz_cust_site_uses_all i
           ,ar.hz_cust_profile_amts c
           ,ar.hz_party_sites e
           ,ar.hz_locations d
           ,ar.hz_customer_profiles b
           ,ar.hz_cust_accounts f
           ,ar.hz_cust_profile_classes g
           ,ar.ar_payment_schedules_all k
      WHERE     a.cust_acct_site_id = i.cust_acct_site_id
            AND a.cust_account_id = f.cust_account_id
            AND k.customer_site_use_id = i.site_use_id
            AND i.site_use_id = b.site_use_id
            AND b.cust_account_profile_id = c.cust_account_profile_id
            AND a.party_site_id = e.party_site_id
            AND e.location_id = d.location_id
            AND f.cust_account_id = b.cust_account_id
            AND g.profile_class_id = b.profile_class_id
            AND a.creation_date > '11-NOV-11'
            AND i.attribute1 = 'JOB'
            AND (a.attribute_category = 'No' OR a.attribute_category IS NULL)
            AND (    b.account_status <> 'NO NOTICE'
                 AND b.account_status <> 'LEGAL'
                 AND b.account_status <> 'BANKRUPTCY')
   GROUP BY e.party_site_number
           ,a.attribute17
           ,i.location
           ,a.creation_date
           ,c.overall_credit_limit
           ,b.credit_hold
           ,b.account_status
           ,d.address1
           ,d.address2
           ,d.address3
           ,d.address4
           ,d.city
           ,d.state
           ,d.postal_code
           ,f.account_number
           ,f.attribute6
           ,f.account_name
           ,g.name
           ,a.attribute14;
