CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_gsa_sales_v
(
   customer_number
  ,customer_name
  ,schedule_parts
  ,product
  ,product_name
  ,month1
  ,quarter_num
  ,region
  ,sales
  ,period_year
  ,averagecost
  ,gross_profit
  ,ordered_date
  ,invoice_number
  ,invoice_date
  ,qty
  ,unit_price
  ,order_number
  ,header_id
  ,line_id
  ,cust_account_id
  ,party_id
  ,site_use_id
  ,cust_acct_site_id
  ,party_site_id
  ,location_id
  ,organization_id
  ,inventory_item_id
  ,msi_organization_id
)
AS
   SELECT hca_bill.account_number customer_number
         ,NVL (hca_bill.account_name, hzp.party_name) customer_name
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_schedule_part_flag (ol.header_id
                                                                ,ol.line_id)
             schedule_parts
         ,msi.segment1 product
         ,msi.description product_name
         ,gps.period_name month1
         ,gps.period_year || 'Quarter' || gps.quarter_num quarter_num
         ,mp.attribute9 region
         ,  NVL (ol.unit_selling_price, 0)
          * NVL (rctl.quantity_invoiced, rctl.quantity_credited)
             sales
         ,gps.period_year
         , (  NVL (rctl.quantity_invoiced, rctl.quantity_credited)
            * (NVL (
                  apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
                 ,0)))
             averagecost
         , (  (  NVL (ol.unit_selling_price, 0)
               * NVL (rctl.quantity_invoiced, rctl.quantity_credited))
            - (  NVL (rctl.quantity_invoiced, rctl.quantity_credited)
               * NVL (
                    apps.xxwc_mv_routines_pkg.get_order_line_cost (
                       ol.line_id)
                   ,0)))
             gross_profit
         ,TRUNC (oh.ordered_date) ordered_date
         ,rct.trx_number invoice_number
         ,TRUNC (rct.trx_date) invoice_date
         ,NVL (rctl.quantity_invoiced, rctl.quantity_credited) qty
         ,NVL (ol.unit_selling_price, 0) unit_price
         ,oh.order_number order_number
         ,                                                     ---Primary Keys
          oh.header_id
         ,ol.line_id
         ,hca_bill.cust_account_id
         ,hzp.party_id
         ,hzcsu_ship_to.site_use_id
         ,hcas_ship_to.cust_acct_site_id
         ,hzps_ship_to.party_site_id
         ,hzl_ship_to.location_id
         ,ood.organization_id
         ,msi.inventory_item_id
         ,msi.organization_id msi_organization_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,ra_customer_trx rct
         ,ra_customer_trx_lines rctl
         ,hz_cust_accounts hca_bill
         ,hz_cust_accounts hca_ship
         ,hz_parties hzp
         ,hz_cust_site_uses hzcsu_ship_to
         ,hz_cust_acct_sites hcas_ship_to
         ,hz_party_sites hzps_ship_to
         ,hz_locations hzl_ship_to
         ,org_organization_definitions ood
         ,mtl_system_items_kfv msi
         ,gl_period_statuses gps
         ,mtl_parameters mp
    WHERE     oh.header_id = ol.header_id
          AND rctl.interface_line_context = 'ORDER ENTRY'
          AND rct.interface_header_context = 'ORDER ENTRY'
          AND TO_CHAR (oh.order_number) = rct.interface_header_attribute1
          AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)
          AND oh.sold_to_org_id = hca_bill.cust_account_id
          AND oh.ship_to_org_id = hca_ship.cust_account_id(+)
          AND hca_ship.cust_account_id = hcas_ship_to.cust_account_id(+)
          AND hcas_ship_to.party_site_id = hzps_ship_to.party_site_id(+)
          AND hcas_ship_to.cust_acct_site_id =
                 hzcsu_ship_to.cust_acct_site_id(+)
          AND hzps_ship_to.location_id = hzl_ship_to.location_id(+)
          AND oh.ship_from_org_id = ood.organization_id
          AND ol.inventory_item_id = msi.inventory_item_id
          AND ol.ship_from_org_id = msi.organization_id
          AND gps.application_id = 101
          AND ood.set_of_books_id = gps.set_of_books_id
          AND TRUNC (oh.ordered_date) BETWEEN gps.start_date AND gps.end_date
          AND mp.organization_id = ol.ship_from_org_id
          AND hca_bill.party_id = hzp.party_id
          AND (   hzp.gsa_indicator_flag = 'Y'
               OR hzcsu_ship_to.gsa_indicator = 'Y');


