CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_dms_ship_confirm_vw
(
   order_number
  ,delivery_id
  ,branch
  ,signed_by
  ,ship_confirm_status
  ,ship_confirm_exception
  ,creation_date
  ,customer_name
)
AS
   SELECT DISTINCT a.order_number
                  ,delivery_id
                  ,branch
                  ,signed_by
                  ,ship_confirm_status
                  ,ship_confirm_exception
                  ,TRUNC (a.creation_date) creation_date
                  ,hp.party_name customer_name      -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
     FROM xxwc.xxwc_om_dms_ship_confirm_tbl a
         ,hz_cust_accounts hca                      -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
         ,hz_parties hp                             -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
         ,oe_order_headers_all oeh                  -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
    WHERE     ship_confirm_status NOT IN ('NO', 'COMPLETED')
          AND hp.party_id = hca.party_id            -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
          AND hca.cust_account_id = oeh.sold_to_org_id -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
          AND oeh.order_number = a.order_number     -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
          AND a.creation_date IN (SELECT MAX (creation_date)
                                    FROM xxwc.xxwc_om_dms_ship_confirm_tbl b
                                   WHERE b.order_number = a.order_number AND b.delivery_id = a.delivery_id)
   UNION
   SELECT DISTINCT oh.order_number order_number
                  ,sc.delivery_id delivery_id
                  ,SUBSTR (hl.location_code, 1, INSTR (hl.location_code, ' ') - 1) branch
                  ,sc.signature_name accepted_by
                  ,ship_confirm_status
                  ,ship_confirm_exception
                  ,TRUNC (sc.creation_date) creation_date
                  ,hp.party_name customer_name      -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
     FROM xxwc.xxwc_signature_capture_tbl sc
         ,oe_order_headers_all oh
         ,hz_cust_accounts hca                      -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
         ,hz_parties hp                             -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
         ,hr_all_organization_units hou
         ,hr_locations_all hl
         ,fnd_lookup_values lv
    WHERE     1 = 1
          AND sc.id = oh.header_id
          AND oh.flow_status_code = 'BOOKED'
          AND hp.party_id = hca.party_id            -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
          AND hca.cust_account_id = oh.sold_to_org_id -- Added by Mahender for TMS# 20150325-00104 on 04/14/2015
          AND TO_CHAR (oh.order_type_id) = lv.lookup_code
          AND ship_confirm_status <> 'COMPLETED'
          AND hou.location_id = hl.location_id
          AND oh.ship_from_org_id = hou.organization_id
          AND bill_to_site_flag = 'Y'
          AND sc.creation_date IN (SELECT MAX (creation_date)
                                     FROM xxwc.xxwc_signature_capture_tbl sc2
                                    WHERE sc2.id = sc.id AND sc2.delivery_id = sc.delivery_id)
-- Below union query added by Mahender for 	TMS#20150420-00089  on 04/21/2015								
 UNION
   SELECT DISTINCT oh.order_number order_number
                  ,sac.delivery_id delivery_id
                  ,SUBSTR (hl.location_code, 1, INSTR (hl.location_code, ' ') - 1) branch
                  ,sac.signature_name accepted_by
                  ,ship_confirm_status
                  ,ship_confirm_exception
                  ,TRUNC (sac.creation_date) creation_date
                  ,hp.party_name customer_name      
     FROM xxwc.XXWC_SIGNATURE_CAPTURE_ARC_TBL sac
         ,oe_order_headers_all oh
         ,hz_cust_accounts hca                      
         ,hz_parties hp                            
         ,hr_all_organization_units hou
         ,hr_locations_all hl
         ,fnd_lookup_values lv
    WHERE     1 = 1
          AND sac.id = oh.header_id
          AND oh.flow_status_code = 'BOOKED'
          AND hp.party_id = hca.party_id            
          AND hca.cust_account_id = oh.sold_to_org_id 
          AND TO_CHAR (oh.order_type_id) = lv.lookup_code
          AND ship_confirm_status <> 'COMPLETED'
          AND hou.location_id = hl.location_id
          AND oh.ship_from_org_id = hou.organization_id
          AND bill_to_site_flag = 'Y'
          AND sac.creation_date IN (SELECT MAX (creation_date)
                                     FROM xxwc.XXWC_SIGNATURE_CAPTURE_ARC_TBL sac2
                                    WHERE sac2.id = sac.id AND sac2.delivery_id = sac.delivery_id);
