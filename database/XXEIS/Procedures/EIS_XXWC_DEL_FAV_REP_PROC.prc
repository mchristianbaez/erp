CREATE OR REPLACE
PROCEDURE XXEIS.EIS_XXWC_DEL_FAV_REP_PROC(
    p_report_name IN VARCHAR2,
    p_user_name   IN VARCHAR2)
AS
--//============================================================================
--//
--// Object Name         :: EIS_XXWC_DEL_FAV_REP_PROC  
--//
--// Object Usage 		 :: This Object Referred by "EIS Delete Favorite Report List" report
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure deletes the records based on report name parameter .
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--//  1.0      26-Jul-2016        Siva   		 TMS#20160628-00049 
--//============================================================================
  L_STMT VARCHAR2(32000);
  l_desc varchar2(32000);
BEGIN
  l_stmt         :='DELETE FROM XXEIS.EIS_RS_REPORT_FAVORITES ERRF WHERE REPORT_ID IN (SELECT REPORT_ID FROM XXEIS.EIS_RS_REPORTS WHERE REPORT_NAME='''||p_report_name||''' and ENABLE_REPORT=''N'')';
  IF P_USER_NAME IS NOT NULL THEN
    l_stmt       :=l_stmt||'AND EXISTS   (SELECT ''1'' FROM APPS.FND_USER FU WHERE FU.USER_ID = ERRF.USER_ID AND FU.USER_NAME IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => TRIM (p_user_name ) ), '%%', ', ' ) || '))';
  END IF;
  FND_FILE.PUT_LINE(FND_FILE.LOG,'l_stmt: '||L_STMT);
  EXECUTE IMMEDIATE L_STMT;
  if sql%rowcount=0
  then
  fnd_file.put_line (fnd_file.LOG,'No Records Exists in favorites for Deletion');
  else
  FND_FILE.PUT_LINE (FND_FILE.LOG,'Report Deleted Successfully from favorites');
  l_desc:= p_report_name||' is deleted successfully';
  if P_USER_NAME is not null
  THEN 
  l_desc:= p_report_name||' is deleted successfully under the user '||P_USER_NAME;
  END IF;   
 INSERT INTO XXEIS.XXWC_DEL_FAV_REP_TBL VALUES(L_DESC);
END IF;
COMMIT;
EXCEPTION
WHEN OTHERS THEN
  fnd_file.put_line (fnd_file.LOG,'Exception Error==>' || SUBSTR (SQLERRM, 1, 240));
END;
/
