CREATE OR REPLACE
PROCEDURE  XXEIS.EIS_XXWC_PURGE_CUST_TAB_PROC
AS
--//============================================================================
--//
--// Object Name         :: EIS_XXWC_PURGE_CUST_TAB_PROC  
--//
--// Object Usage 		 :: This Object Referred by "Purge Custom Tables Data Weekly Maintenance - WC" report
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure truncates the records from custom tables .
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--//  1.0       15-Dec-2016        Siva   		 TMS#20161215-00020 
--//============================================================================
l_desc VARCHAR2(4000);
cursor c1 is
SELECT TABLE_NAME
FROM all_tables
WHERE table_name IN ( 'EIS_XXWC_VALID_BIN_SALES_ORGS', 'EIS_XXWC_BIN_SALES_ONHAND_TAB', 'EIS_XXWC_BIN_SALES_ORGS_TAB', 'EIS_XXWC_INV_BIN_LOC_SALES_TBL', 'EIS_XXWC_VALID_BIN_ORGS',
'EIS_XXWC_BIN_LOC_DATA_TAB', 'EIS_XXWC_BIN_ONHAND_TAB', 'EIS_XXWC_BIN_ORGS_TAB', 'EIS_XXWC_VALID_ORGS_TAB', 'XXWC_LOW_QTY_MAN_PRIC_TBL', 'EIS_XXWC_LOW_QTY_HDR_TBL',
'XXWC_INV_SPCL_ONHAND_TBL', 'XXWC_INV_RF_METRIC_RPT_TBL', 'XXWC_GTT_WSH_SHIPPING_STG_TBL', 'XXWC_GTT_OPEN_ORDERS_TBL', 'XXWC_GTT_OE_ORDER_LINES_TBL', 'XXWC_GTT_OE_ORDER_HEADERS_TBL',
'XXWC_GTT_EIS_PRINT_LOG_TBL', 'EIS_XXWC_CASH_DRAWN_EXCE_TBL', 'EIS_XXWC_CASH_EXCE_ORDERS', 'EIS_XXWC_CASH_EXCEP_TBL', 'EIS_XXWC_CASH_EXCEP_TMP_TBL', 'EIS_XXWC_CASHEXCE_MAIN_TAB',
'EIS_XXWC_ORDER_EXP_TAB', 'EIS_XXWC_CNTR_ORD_TAB', 'EIS_XXWC_SUBINV_ONHAND_DTLS', 'EIS_XXWC_ONHAND_DTLS', 'EIS_XXWC_OM_LINE_DISP_TAB', 'EIS_XXWC_INV_TIME_MGMT_TBL',
'EIS_XXWC_INV_LIST_TBL', 'EIS_XXWC_AR_EOM_COMM_TBL', 'EIS_XXWC_INTERNAL_SALES_TAB', 'EIS_XXWC_INT_ORD_TAB', 'EIS_XXWC_FLEX_VAL_TAB', 'EIS_XXWC_DAILY_FLASH_DTL_TAB',
'EIS_XXWC_CREDIT_INV_TAB', 'EIS_XXWC_CREDIT_MEMO_DET_TAB', 'EIS_DATA_MANAGMNT_DTLS', 'EIS_HEADER_LINE_TAB')
AND owner='XXEIS';
BEGIN
FOR i IN c1 loop
   begin
   EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.'||I.table_name;
   l_desc:= I.table_name||' truncated successfully';
   fnd_file.put_line (fnd_file.LOG,l_desc);
   exception WHEN others THEN
   l_desc:= I.table_name||' has errors while processing'||' : '|| SUBSTR (SQLERRM, 1, 240);
   fnd_file.put_line (fnd_file.LOG,l_desc);
   END;
INSERT INTO XXEIS.XXWC_DEL_FAV_REP_TBL VALUES(L_DESC);
END loop;

EXCEPTION
WHEN OTHERS THEN
  fnd_file.put_line (fnd_file.LOG,'Exception Error==>' || SUBSTR (SQLERRM, 1, 240));
END EIS_XXWC_PURGE_CUST_TAB_PROC;
/
