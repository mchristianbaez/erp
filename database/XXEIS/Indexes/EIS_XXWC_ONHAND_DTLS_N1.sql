-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_ONHAND_DTLS_N1
  File Name: EIS_XXWC_ONHAND_DTLS_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        16-Apr-2016  Pramod        TMS#20160503-00085 Inventory Listing Report - Program Running for longer time
	 1.1        15-Mar-2017   Siva   	   TMS#20170321-00004
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_ONHAND_DTLS_N1 ON XXEIS.EIS_XXWC_ONHAND_DTLS (INVENTORY_ITEM_ID,ORGANIZATION_ID,PROCESS_ID)
-- TABLESPACE APPS_TS_TX_DATA  --Commented for version 1.1
/
