-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_PUR_ACC_FIS_PER_N1
  File Name: EIS_XXWC_PUR_ACC_FIS_PER_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        29-Apr-2016  PRAMOD        TMS#20160429-00036 PURCHASES AND ACCRUALS - By VENDOR, LOB, BU,AGREEMENT YEAR, FISCAL PERIOD(NEW)
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_N1 
 ON XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_TAB (PROCESS_ID) TABLESPACE APPS_TS_TX_DATA
/
