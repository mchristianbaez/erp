---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_GTT_WSH_SHIPPING_STG_N11
  File Name: XXWC_GTT_WSH_SHIPPING_STG_N11.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        20-Jun-16    Siva        TMS# 20160617-00018  --performance Tuning
	 1.1        15-Mar-2017    Siva   	   TMS#20170321-00004
****************************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_GTT_WSH_SHIPPING_STG_N11 ON XXEIS.XXWC_GTT_WSH_SHIPPING_STG_TBL
  (
    PROCESS_ID,
    HEADER_ID,
    LINE_ID,
    INVENTORY_ITEM_ID,
    SHIP_FROM_ORG_ID,
    TO_CHAR(DELIVERY_ID)
  )
 -- TABLESPACE APPS_TS_TX_DATA 	--Commented for version 1.1
/

