-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_INT_ORD_TAB_N2
  File Name: EIS_XXWC_INT_ORD_TAB_N2.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        06-JUL-2016  SIVA        TMS#20160614-00094 Orders Exception Report - Program Running for longer time
	 1.1       15-Mar-2017   Siva   	 TMS#20170321-00004
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_INT_ORD_TAB_N2 ON XXEIS.EIS_XXWC_INT_ORD_TAB (HEADER_ID,LINE_ID,PROCESS_ID)
-- TABLESPACE APPS_TS_TX_DATA --Commented for version 1.1
/
