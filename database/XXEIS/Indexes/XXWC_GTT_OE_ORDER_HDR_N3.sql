---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_GTT_OE_ORDER_HDR_N3
  File Name: XXWC_GTT_OE_ORDER_HDR_N3.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        20-Jun-16      Siva        TMS# 20160617-00018  --performance Tuning
	 1.1        15-Mar-2017    Siva   	   TMS#20170321-00004
****************************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_GTT_OE_ORDER_HDR_N3 ON XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL
  (
    PROCESS_ID,
    HEADER_ID
  )
 -- TABLESPACE APPS_TS_TX_DATA  --Commented for version 1.1
/
