-----------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_AR_CUST_BAL_MV_N1
  File Name: XXWC_AR_CUST_BAL_MV_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        12-Apr-2016  Siva        TMS#20160411-00103 Cash Drawer Exception Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_AR_CUST_BAL_MV_N1
 ON XXEIS.XXWC_AR_CUSTOMER_BALANCE_MV (CUST_ACCOUNT_ID) TABLESPACE APPS_TS_TX_DATA
/
