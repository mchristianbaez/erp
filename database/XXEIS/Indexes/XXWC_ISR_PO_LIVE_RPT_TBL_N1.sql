-----------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_ISR_PO_LIVE_RPT_TBL_N1
  File Name: XXWC_ISR_PO_LIVE_RPT_TBL_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
      1.0    20-Jul-16    Siva            TMS#20160708-00029 'Item Attribute Validation Report - WC'- Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_ISR_PO_LIVE_RPT_TBL_N1 ON XXEIS.EIS_XXWC_ISR_PO_LIVE_RPT_TBL (PROCESS_ID,ORGANIZATION_ID,INVENTORY_ITEM_ID)
/
