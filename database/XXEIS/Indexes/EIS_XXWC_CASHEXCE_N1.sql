--------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_CASHEXCE_N1
  File Name: EIS_XXWC_CASHEXCE_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        12-Apr-2016  Siva        TMS#20160411-00103 Cash Drawer Exception Report - Program Running for longer time
	 1.1     	15-Mar-2017  Siva   	 TMS#20170321-00004
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_CASHEXCE_N1
 ON XXEIS.EIS_XXWC_CASH_EXCE_ORDERS (PROCESS_ID,TO_CHAR(ORDER_NUMBER)) 
 --TABLESPACE APPS_TS_TX_DATA  --Commented for version 1.1
/
