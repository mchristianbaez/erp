 /*************************************************************************************************
  
   File Name: XXCUS_AP_EXP_RPT_DISTS_ALL_N2
  
   PROGRAM TYPE: Trigger
  
   PURPOSE: Index on AP.AP_EXP_REPORT_DISTS_ALL
  
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       		DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     03/11/2015   Maharajan Shunmugam 	ESMS#567606 and eRFC# 43101 HDS OIE Bullet Train Extract Process Fails 
				        				because of tablespace TEMP1 issue
*************************************************************************************************/

CREATE INDEX AP.XXCUS_AP_EXP_RPT_DISTS_ALL_N2 ON AP.AP_EXP_REPORT_DISTS_ALL (NVL(SEGMENT1, '1'), NVL(SEGMENT2, '2'), NVL(SEGMENT3, '3'), NVL(SEGMENT4, '4'), NVL(SEGMENT5, '5'), NVL(SEGMENT6, '6'), NVL(SEGMENT7, '7'));

