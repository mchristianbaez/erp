 /*************************************************************************************************
  
   File Name: XXCUS_AP_EXP_RPT_DISTS_ALL_N1
  
   PROGRAM TYPE: Trigger
  
   PURPOSE: Index on AP.AP_EXP_REPORT_DISTS_ALL
  
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       		DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     03/11/2015   Maharajan Shunmugam 	ESMS#567606 and eRFC# 43101 HDS OIE Bullet Train Extract Process Fails 
				        				because of tablespace TEMP1 issue
*************************************************************************************************/

CREATE INDEX AP.XXCUS_AP_EXP_RPT_DISTS_ALL_N1 ON AP.AP_EXP_REPORT_DISTS_ALL (CODE_COMBINATION_ID ,0);

