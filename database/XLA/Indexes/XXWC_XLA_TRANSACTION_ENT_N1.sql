/********************************************************************************
   $Header XXWC_XLA_TRANSACTION_ENT_N1.SQL $
   Module Name: XXWC_XLA_TRANSACTION_ENT_N1

   PURPOSE:   Index on table XLA_TRANSACTION_ENTITIES

   REVISIONS:
   Ver        Date        Author                  Description
   ---------  ----------  ---------------         -------------------------
   1.0        03/14/2016  Manjula Chellappan      TMS# 20160314-00202 - Create Index on XLA_TRANSACTION_ENTITIES
********************************************************************************/ 
 CREATE INDEX XLA.XXWC_XLA_TRANSACTION_ENT_N1
 ON XLA.XLA_TRANSACTION_ENTITIES 
(ENTITY_CODE);