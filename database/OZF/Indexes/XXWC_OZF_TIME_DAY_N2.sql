-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_PURCHASE_ACC_RPT_PKG
  File Name: EIS_XXWC_PURCHASE_ACC_RPT_PKG.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        12-Apr-2016  Pramod        TMS#20160412-00031 PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
****************************************************************************************************************************/
CREATE INDEX OZF.XXWC_OZF_TIME_DAY_N2 
 ON OZF.OZF_TIME_DAY (REPORT_DATE,SUBSTR(TO_CHAR("MONTH_ID"),0,4)) TABLESPACE APPS_TS_TX_DATA
/
