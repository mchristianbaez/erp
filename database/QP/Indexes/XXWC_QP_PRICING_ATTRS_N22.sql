--
-- XXWC_QP_PRICING_ATTRS_N22  (Index) 
--
CREATE INDEX QP.XXWC_QP_PRICING_ATTRS_N22 ON QP.QP_PRICING_ATTRIBUTES
(LIST_LINE_ID, PRICING_ATTRIBUTE)
TABLESPACE XXWC_IDX;


