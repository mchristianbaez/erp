---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_MTL_SYSTEM_ITM_B_N30
  File Name: XXWC_MTL_SYSTEM_ITM_B_N30.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        12-Apr-2016  Siva        TMS#20160407-00201 --Specials Report  Performance Tuning
****************************************************************************************************************************/
CREATE INDEX INV.XXWC_MTL_SYSTEM_ITM_B_N30
 ON INV.MTL_SYSTEM_ITEMS_B (ITEM_TYPE,INVENTORY_ITEM_ID,ORGANIZATION_ID) TABLESPACE APPS_TS_TX_DATA
/
