--*********************************************************
--*File Name: XXWC_CSP_HDR_INTERFACE.ctl                   *
--*Author: Shankar Hariharan                      *
--*Creation Date: 06-Dec-2012                             *
--*Last Updated Date: 06-Dec-2012                         *
--*Data Source:                                           *
--*Description: This is the control file. 
--*********************************************************
LOAD DATA
REPLACE INTO TABLE XXWC_CSP_HDR_INTERFACE
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  reference_id                     "TRIM(:reference_id)"
 , price_type                       "TRIM(:price_type)"
 , CUSTOMER_NUMBER                  "TRIM(:CUSTOMER_NUMBER)"
 , job_location                     "TRIM(:job_location)"
 , incompatability                  "TRIM(:incompatability)"
 , branch                           "rtrim(TRIM(:branch),chr(13))"
 , process_flag                     constant 'N'
)