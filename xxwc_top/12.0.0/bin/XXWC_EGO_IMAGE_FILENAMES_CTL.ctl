--*********************************************************
--*File Name: XXWC_EGO_IMAGE_FILENAMES_CTL.ctl            *
--*Author: Praveen Pawar                                  *
--*Creation Date: 12-Mar-2014                             *
--*Last Updated Date: 12-Mar-2014                         *
--*Data Source: Image Repository csv FILE                 *
--*Description: This is the control file. SQL Loader will *
--*             use to load the CustProfileAmt Stg data   *
--*********************************************************

LOAD DATA
REPLACE INTO TABLE XXWC.XXWC_EGO_IMAGE_FILENAMES
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
( XXWC_IMAGE_FILENAME_ATTR          "TRIM(:XXWC_IMAGE_FILENAME_ATTR)"
, XXWC_IMAGE_FILETYPE_ATTR          "TRIM(:XXWC_IMAGE_FILETYPE_ATTR)"
, LAST_UPDATE_DATE                  SYSDATE
 )
