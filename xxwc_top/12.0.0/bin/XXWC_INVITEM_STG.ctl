-- ******************************************************************************
-- *  Copyright (c) 2011 HDS Supply
-- *  All rights reserved.
-- ******************************************************************************
-- *   $Header XXWC_INVITEM_STG.ctl $
-- *   Module Name: XXWC_INVITEM_STG.ctl
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXWC_INVITEM_STG
-- *
-- *   REVISIONS:
-- *   Ver Date AuthorDescription
-- *   ---------  ----------  ---------------  -------------------------
-- * ****************************************************************************

LOAD DATA
INFILE *
INTO TABLE XXWC.XXWC_INVITEM_STG
APPEND
FIELDS TERMINATED BY '|'
optionally enclosed by '"'
TRAILING NULLCOLS
(PARTNUMBER
      ,AHH_PARTNUMBER
      ,DESCRIPTION
      ,EXTENDEDDESCRIPTION
      ,MST_ITEM_STATUS_CODE
      ,COMPANY_NUM
      ,ORG_ITEM_STATUS_CODE
      ,SHELF_LIFE_CODE
      ,VENDOR_OWNER_NUM
      ,VENDOR_NAME
      ,VENDORPARTNUMBER
      ,FIXED_LOT
      ,UNITOFMEASURE
      ,PRODUCT_LINE
      ,LISTPRICE
      ,BLANK1
      ,UNITLENGTH
      ,UNITHEIGHT
      ,UNITWIDTH
      ,UNITWEIGHT
      ,PRODUCT_CATEGORY
      ,ORGANIZATION_CODE
      ,ORG_ITEM_STATUS_CODE1
      ,SOURCING_FROM
      ,INV_ORG
      ,BLANK2
      ,ORG_FIXED_LOT
      ,VELOCITYCLASSIFICATION
      ,INVENTORY_PLANNING_CODE
      ,THRESHOLD
      ,MINMAXMIN
      ,MINMAXMAX
      ,BLANK3
      ,SOURCE_ORG_VENDOR_NUM
      ,BLANK4
      ,BLANK5
      ,USAGE
      ,LEADTIME
      ,REVIEWTIME
      ,BLANK6
      ,BUYER
      ,BLANK7
      ,BLANK8
      ,BLANK9
      ,BLANK10
      ,BLANK11
      ,BLANK12
      ,BLANK13
      ,BLANK14
      ,BLANK15
      ,BLANK16
      ,BLANK17
      ,BLANK18
      ,BLANK19
      ,BLANK20
      ,BLANK21
      ,CATEGORY
      ,BLANK22
      ,NONTAXTY
      ,TAXABLETY
      ,BLANK23
      ,TAXGROUP
      ,BLANK24
      ,TAXTYPE
      ,ITEMLOCATION1
      ,ITEMLOCATION2
      ,ITEMLOCATION3
      ,ITEMLOCATION4
      ,ITEMLOCATION5
      ,QTY_ONHAND
      ,QTY_UNAVAILABLE
      ,AVGCOST
      ,VAL_FROM_ICRTT
      ,SALESVELOCITYYEARLYSTORECOUNT
      ,DOTCODE
      ,SHIPPINGNM
      ,FREIGHTCLASS
      ,HMCOLUMNENTRY
      ,UNNUMBER
      ,ERGNO
      ,TECHEMNM
      ,ATTRIBUTE17
      ,HAZARDYN
      ,HAZARDCLASS
      ,ATTRIBUTE9
      ,NMFCCODE
      ,ENTERDT
      ,ICSW_MINTHRESEXDT)