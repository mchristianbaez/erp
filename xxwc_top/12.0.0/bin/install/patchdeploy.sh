#!/bin/bash
# patchdeploy.sh is a wrapper around the Install.pl perl script
#
# Parameters: $1 = apps Password
#			  $2 = Task ID
#			  $3 = xxeis Password
echo Usage: patchdeploy.sh [apps password] [patch folder name]
echo Executing Install.pl
export FORM_PATH="$AU_TOP/forms/US:$FORM_PATH:$AU_TOP/resource/US"
export FORMS_PATH="$FORMS_PATH:$AU_TOP/forms/US:$AU_TOP/resource/US:S:$AU_TOP/resource"
export CONFIG_FILE="$XXWC_TOP/bin/install/install.txt"
set -x #echo on
perl $XXWC_TOP/bin/install/Install.pl $1 $XXWC_TOP/patch/custom_patches/$2 debug=true $3
set +x #echo off
echo Log path: $XXWC_TOP/patch/custom_patches/$2
echo Patch deployment script has finished running. Review logs for errors!
echo End patchdeploy.sh