#!/usr/bin/perl 
# Releasedeploy.pl will execute the patchdeploy.sh script once
# for each file in the specified release folder's patches driver.
# 
# The single parameter is a Release Task ID. This must correspond
# to a folder in the custom_releases folder, and that folder must
# have a patches.txt file with a list of task IDs. Those task IDs
# must correspond to patch folders in the custom_patches folder.

# (1) quit unless we have the correct number of command-line args
$num_args = $#ARGV + 1;
if ($num_args != 1) {
    print "\nUsage: perl releasedeploy.pl release_task_id\n";
    exit;
}

$release_task_id = $ARGV[0];

chdir "/obase/ebiz/apps/apps_st/appl/xxwc/12.0.0/bin/install";

print "Beginning release deployment for release: $release_task_id \n";

# Open input patches file
print "Trying to open patches file: " . "$ENV{'XXWC_TOP'}/patch/custom_releases/$release_task_id/patches.txt" . "\n";
open my $patches, "<", "$ENV{'XXWC_TOP'}/patch/custom_releases/$release_task_id/patches.txt" or die "Can't open release patches: $1";

print "Type your APPS password and press [Enter]:";
chomp(my $password = <STDIN>);

print "Type your XXEIS password and press [Enter]:";
chomp(my $xxeispassword = <STDIN>);

# loop over all patches in patches file
while (my $patch_task_id = <$patches>) {
	$cmd = "";
	chomp $patch_task_id;
	
	$cmd = ". $ENV{'XXWC_TOP'}/bin/install/patchdeploy.sh $password $patch_task_id $xxeispassword \n";

	print "Executing: ";
	print $cmd;

	`$cmd`;
}

close $patches or die "Can't close patches file: $!";

print "Finished executing release deployment. Check for errors! \n";
