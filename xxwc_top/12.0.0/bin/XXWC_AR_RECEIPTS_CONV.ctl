-- ******************************************************************************
-- *  Copyright (c) 2011 Lucidity Consulting Group
-- *  All rights reserved.
-- ******************************************************************************
-- *   $Header XXWC.XXWC_AR_RECEIPTS_CONV.ctl $
-- *   Module Name: XXWC.XXWC_AR_RECEIPTS_CONV.ctl
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXWC_AR_RECEIPTS_CONV
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------
-- *   1.0        10/11/2011  Vivek Lakaman             Initial Version
-- *   1.1        03/04/2018  Ashwin Sridhar       Added for TMS#20180319-00242
-- * ****************************************************************************

LOAD DATA
APPEND
INTO TABLE XXWC.XXWC_AR_RECEIPTS_CONV
FIELDS TERMINATED BY "|" OPTIONALLY ENCLOSED BY '"' 
TRAILING NULLCOLS
( RECEIPT_NUMBER        "TRIM(:RECEIPT_NUMBER)"
 ,RECEIPT_METHOD        "TRIM(:RECEIPT_METHOD)"                
 ,RECEIPT_DATE          "TO_DATE(:RECEIPT_DATE,'MM/DD/RR')"     
 ,RECEIPT_AMOUNT    
 ,CURRENCY_CODE         "TRIM(:CURRENCY_CODE)"
 ,LEGACY_CUSTOMER_NUM   "TRIM(:LEGACY_CUSTOMER_NUM)"        
 ,LEGACY_CUSTOMER_SITE  "TRIM(:LEGACY_CUSTOMER_SITE)"        
 ,BANK_BRANCH_NAME      "TRIM(:BANK_BRANCH_NAME)"        
 ,BANK_ACCOUNT_NUMBER   "TRIM(:BANK_ACCOUNT_NUMBER)"   
 ,COMMENTS              "TRIM(:COMMENTS)"   
 ,GL_DATE               "TO_DATE(:RECEIPT_DATE,'MM/DD/RR')"     
 ,CREATION_DATE         "sysdate"                
 ,CREATED_BY            "fnd_global.user_id"     
 ,LAST_UPDATED_BY       "fnd_global.user_id"     
 ,LAST_UPDATE_DATE      "SYSDATE"                
 ,LAST_UPDATE_LOGIN     "fnd_global.login_id"    
 ,PROCESS_STATUS        CONSTANT 'R'         
 ,ERROR_MESSAGE      
 ,RECEIPT_REFERENCE     "TRIM(:RECEIPT_REFERENCE)"      
 ,DISPUTE_CODE          "REPLACE(REPLACE(TRIM(:DISPUTE_CODE),CHR(13),''),CHR(10),'')"      
 )