# ------------------------------------------------------------------------
# ------------------------------------------------------------------------
#!/bin/sh
# ------------------------------------------------------------------------
# copyright 2012, All Rights Reserved, Kathy Poling
# Version 1.0
#
# Name        : HDS_AR_SALES_COGS.sh 
# Date Written: 
# Author      : Krishna
#
# Description : Zip files prior to being SFTP to NASDRIVE using FTPHelper
#               in UC4
#
# Usasage     : ./HDS_AR_SALES_COGS.sh
#
# Modification History:
#
# When         Who        Did what
# --------------------------------------------------------------------------
# 03-Dec-2016  Krishna    Created
# --------------------------------------------------------------------------
inv_file="HDS_SALES_COGS_SUMMARY_*.txt"
inv_file1="HDS_SALES_COGS_GRAND_SUMMARY_*.txt"

dir= $XXWC_TOP/ebsqa/outbound
now=`date "+%m%d%Y"`

invfile="HDS_SALES_COGS_SUMMARY_"$now
invfile1="HDS_SALES_COGS_GRAND_SUMMARY_"$now

echo $invfile" file name for ftp..."

#fndit=`ls $inv_file`
fndit=`ls $dir/$inv_file`
fndit1=`ls $dir/$inv_file`
#echo $fndit" File to zip..."
fndnv=$?
if [ $fndnv=$? ];
 then

   echo $fndit" exist..."
   #echo "zip " $invfile " " $fndit  
   zip $invfile $fndit
   zip $invfile1 $fndit1
   #gzip $fndit
   #mv $fndit $archive
 fi
