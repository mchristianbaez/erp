-- ******************************************************************************************************************
-- *  Copyright (c) 2011 Lucidity Consulting Group
-- *  All rights reserved.
-- ******************************************************************************************************************
-- *   $Header XXWC_AR_INV_STG_TBL_SQLLDR.ctl $
-- *   Module Name: XXWC_AR_INV_STG_TBL_SQLLDR.ctl
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXWC_AR_INV_STG_TBL
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         ---------------------------------------------------------------
-- *   1.0        27/04/2018  Ashwin Sridhar          TMS#20180708-00002 - AH HARRIS AR Debit Memo/Invoice Interface
-- * ****************************************************************************************************************
OPTIONS (SKIP=0)
LOAD DATA
APPEND
INTO TABLE XXWC.XXWC_AR_INV_STG_TBL
FIELDS TERMINATED BY "|" OPTIONALLY ENCLOSED BY '"' 
TRAILING NULLCOLS
(
INTF_BATCH_ID                      "TRIM(:INTF_BATCH_ID)"
,INTF_STG_ID                        "APPS.XXWC_AR_INV_STG_TBL_SEQ.NEXTVAL"
,UNIT_SELLING_PRICE                 "TRIM(:UNIT_SELLING_PRICE)"
,AMOUNT                             "TRIM(:AMOUNT)"
,TRX_DATE                           "TO_DATE(:TRX_DATE,'MM/DD/RR')"
,QUANTITY_ORDERED                   "TRIM(:QUANTITY_ORDERED)"
,WAREHOUSE_ID                       "TRIM(:WAREHOUSE_ID)"
,PURCHASE_ORDER                     "TRIM(:PURCHASE_ORDER)"
,SALES_ORDER                        "TRIM(:SALES_ORDER)"
,PRIMARY_SALESREP_NUMBER            "TRIM(:PRIMARY_SALESREP_NUMBER)"
,PRIMARY_SALESREP_ID                "TRIM(:PRIMARY_SALESREP_ID)"
,SALES_ORDER_DATE                   "TO_DATE(:SALES_ORDER_DATE,'MM/DD/RR')"
,TRX_NUMBER                         "TRIM(:TRX_NUMBER)"
,TERM_NAME                          "TRIM(:TERM_NAME)"
,TERM_ID                            "TRIM(:TERM_ID)"
,LEGAL_ENTITY_ID                    "TRIM(:LEGAL_ENTITY_ID)"
,ORG_ID                             "TRIM(:ORG_ID)"
,PAYING_CUSTOMER_ID                 "TRIM(:PAYING_CUSTOMER_ID)"
,PAYING_SITE_USE_ID                 "TRIM(:PAYING_SITE_USE_ID)"
,GL_DATE                            "TO_DATE(:GL_DATE,'MM/DD/RR')"
,REASON_CODE                        "TRIM(:REASON_CODE)"
,SHIP_VIA                           "TRIM(:SHIP_VIA)"
,CODE_COMBINATION_ID                "TRIM(:CODE_COMBINATION_ID)"
,OVERRIDE_AUTO_ACCOUNTING_FLAG      "TRIM(:OVERRIDE_AUTO_ACCOUNTING_FLAG)"
,LINE_TYPE                          "TRIM(:LINE_TYPE)"
,ORIG_SYSTEM_BILL_CUSTOMER_REF      "TRIM(:ORIG_SYSTEM_BILL_CUSTOMER_REF)"
,ORIG_SYSTEM_SHIP_CUSTOMER_REF      "TRIM(:ORIG_SYSTEM_SHIP_CUSTOMER_REF)"
,TAX_RATE                           "TRIM(:TAX_RATE)"
,TAX_CODE                           "TRIM(:TAX_CODE)"
,TAX_PRECEDENCE                     "TRIM(:TAX_PRECEDENCE)"
,INVENTORY_ITEM_NUM                 "TRIM(:INVENTORY_ITEM_NUM)"
,ITEM_DESCRIPTION                   "TRIM(:ITEM_DESCRIPTION)"
,REVENUE_AMOUNT                     "TRIM(:REVENUE_AMOUNT)"
,FREIGHT_AMOUNT                     "TRIM(:FREIGHT_AMOUNT)"
,TAX_AMOUNT                         "TRIM(:TAX_AMOUNT)"
,REVENUE_ACCOUNT                    "TRIM(:REVENUE_ACCOUNT)"
,FREIGHT_ACCOUNT                    "TRIM(:FREIGHT_ACCOUNT)"
,TAX_ACCOUNT                        "TRIM(:TAX_ACCOUNT)"
,UOM_CODE                           "TRIM(:UOM_CODE)"
,UOM_NAME                           "TRIM(:UOM_NAME)"
,LINE_NUMBER                        "TRIM(:LINE_NUMBER)"
,ORIG_SYSTEM_BILL_CUSTOMER_ID       "TRIM(:ORIG_SYSTEM_BILL_CUSTOMER_ID)"
,CUST_TRX_TYPE_ID                   "TRIM(:CUST_TRX_TYPE_ID)"
,TRANSACTION_CLASS                  "TRIM(:TRANSACTION_CLASS)"
,TAKEN_BY                           "TRIM(:TAKEN_BY)"
,QUANTITY_BACK_ORDERED              "TRIM(:QUANTITY_BACK_ORDERED)"
,QUANTITY_SHIPPED                   "TRIM(:QUANTITY_SHIPPED)"
,HDS_SITE_FLAG                      "TRIM(:HDS_SITE_FLAG)"
,LEGACY_PARTY_SITE_NUMBER           "TRIM(:LEGACY_PARTY_SITE_NUMBER)"
,CATEGORY_CLASS                     "TRIM(:CATEGORY_CLASS)"
,DIRECT_FLAG                        "TRIM(:DIRECT_FLAG)"
,PRISM_INV_TYPE                     "TRIM(:PRISM_INV_TYPE)"
,RECEIVED_BY                        "TRIM(:RECEIVED_BY)"
,ORDERED_BY                         "TRIM(:ORDERED_BY)"
,STG_ATTRIBUTE1                     "TRIM(:STG_ATTRIBUTE1)"
,STG_ATTRIBUTE2                     "TRIM(:STG_ATTRIBUTE2)"
,STG_ATTRIBUTE3                     "TRIM(:STG_ATTRIBUTE3)"
,STG_ATTRIBUTE4                     "TRIM(:STG_ATTRIBUTE4)"
,STG_ATTRIBUTE5                     "TRIM(:STG_ATTRIBUTE5)"
,STG_ATTRIBUTE6                     "TRIM(:STG_ATTRIBUTE6)"
,STG_ATTRIBUTE7                     "TRIM(:STG_ATTRIBUTE7)" 
,STG_ATTRIBUTE8                     "TRIM(:STG_ATTRIBUTE8)"
,STG_ATTRIBUTE9                     "TRIM(:STG_ATTRIBUTE9)"
,STG_ATTRIBUTE10                    "TRIM(:STG_ATTRIBUTE10)"
,STG_ATTRIBUTE11                    "TRIM(:STG_ATTRIBUTE11)"
,STG_ATTRIBUTE12                    "TRIM(:STG_ATTRIBUTE12)"
,STG_ATTRIBUTE13                    "TRIM(:STG_ATTRIBUTE13)"
,STG_ATTRIBUTE14                    "TRIM(:STG_ATTRIBUTE14)"
,STG_ATTRIBUTE15                    "TRIM(:STG_ATTRIBUTE15)"
,STG_ATTRIBUTE16                    "TRIM(:STG_ATTRIBUTE16)"
,STG_ATTRIBUTE17                    "TRIM(:STG_ATTRIBUTE17)"
,STG_ATTRIBUTE18                    "TRIM(:STG_ATTRIBUTE18)"
,STG_ATTRIBUTE19                    "TRIM(:STG_ATTRIBUTE19)"
,STG_ATTRIBUTE20                    "TRIM(:STG_ATTRIBUTE20)"
,STG_ATTRIBUTE21                    "TRIM(:STG_ATTRIBUTE21)"
,RENTAL_START_DATE                  "TO_DATE(:RENTAL_START_DATE,'MM/DD/RR')"
,RENTAL_END_DATE                    "TO_DATE(:RENTAL_END_DATE,'MM/DD/RR')"
,STATUS                             CONSTANT "NEW"
,ERROR_MESSAGE                      "TRIM(:ERROR_MESSAGE)"
,CREATED_BY                         CONSTANT "-1"
,CREATION_DATE                      SYSDATE
,LAST_UPDATED_BY                    CONSTANT "-1"
,LAST_UPDATE_DATE                   SYSDATE
)                    