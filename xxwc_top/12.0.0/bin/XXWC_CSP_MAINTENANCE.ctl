--*********************************************************
--*File Name: XXWC_CSP_MAINTENANCE.ctl                   *
--*Author: Consuelo Gonzalez                      *
--*Creation Date: 18-Sept-2013                             *
--*Last Updated Date: 18-Sept-2012                         *
--*Data Source:                                           *
--*Description: This is the control file. 
--*********************************************************
LOAD DATA
REPLACE INTO TABLE XXWC.XXWC_CSP_MAINTENANCE
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  action				    "TRIM(:action)"
 , reference_id                     "TRIM(:reference_id)"
 , agreement_type                   "TRIM(:agreement_type)"
 , product_attribute                "TRIM(:product_attribute)"
 , product_value                    "TRIM(:product_value)"
 , app_method                       "TRIM(:app_method)"
 , modifier_value                   "TRIM(:modifier_value)"
 , vq_number                        "TRIM(:vq_number)"
 , special_cost                     "TRIM(:special_cost)"
 , vendor_number                    "rtrim(TRIM(:vendor_number),chr(13))"
)