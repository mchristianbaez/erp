--*********************************************************
--*File Name: XXWC_INVITEM_XREF_STG.ctl                   *
--*Author: Kiran .T                                       *
--*Creation Date: 15-Sep-2011                             *
--*Last Updated Date: 16-Sep-2011                         *
--*Data Source:   XXWC_INVITEM_STG                        *
--*Description: This is the control file. SQL Loader will *
--*           use to load the Ap Invoice information.     *
--*********************************************************

LOAD DATA
REPLACE INTO TABLE XXWC_INVITEM_XREF_STG
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  PARTNUMBER                       "TRIM(:PARTNUMBER)"
 , XREF_PART                        "TRIM(:XREF_PART)"
 , UPC_FLAG                         "TRIM(:UPC_FLAG)"
 )
