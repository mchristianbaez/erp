-- ******************************************************************************
-- *  Copyright (c) 2011 Lucidity Consulting Group
-- *  All rights reserved.
-- ******************************************************************************
-- *   $Header XXWC_CUST_CONTACTS_IFACE_CTL.ctl $
-- *   Module Name: XXWC_CUST_CONTACTS_IFACE_CTL.ctl
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXWC_AR_CUST_CONTACT_IFACE_STG
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------
-- *   1.0        30/04/2018  Ashwin Sridhar             Initial Version
-- * ****************************************************************************
OPTIONS (SKIP=1)
LOAD DATA 
INFILE 'XXWC_AR_CUST_CONTACT_IFACE_STG.txt' 
BADFILE 'XXWC_AR_CUST_CONTACT_IFACE_STG.bad'
DISCARDFILE 'XXWC_AR_CUST_CONTACT_IFACE_STG.dsc'
APPEND
INTO TABLE XXWC_AR_CUST_CONTACT_IFACE_STG
FIELDS TERMINATED BY "|"
TRAILING NULLCOLS
(
CUST_CONTACT_IFACE_STG_ID          "XXWC.XXWC_AR_CUST_CONTACT_IFACE_S.NEXTVAL"      
,ARC_JOB_NO                        "TRIM(:ARC_JOB_NO)"  
,OWNER_TABLE_NAME                  "TRIM(:OWNER_TABLE_NAME)"  
,OWNER_TABLE_ID                    "TRIM(:OWNER_TABLE_ID)"  
,LEGACY_PARTY_NUMBER               "TRIM(:LEGACY_PARTY_NUMBER)"  
,LEGACY_PARTY_SITE_NUMBER          "TRIM(:LEGACY_PARTY_SITE_NUMBER)"  
,PRIMARY_FLAG                      "TRIM(:PRIMARY_FLAG)"  
,ORIG_SYSTEM_REFERENCE             "TRIM(:ORIG_SYSTEM_REFERENCE)"  
,ORIG_SYSTEM                       "TRIM(:ORIG_SYSTEM)"  
,EDI_TRANSACTION_HANDLING          "TRIM(:EDI_TRANSACTION_HANDLING)"  
,EDI_ID_NUMBER                     "TRIM(:EDI_ID_NUMBER)" 
,EDI_PAYMENT_METHOD                "TRIM(:EDI_PAYMENT_METHOD)"  
,EDI_PAYMENT_FORMAT                "TRIM(:EDI_PAYMENT_FORMAT)"  
,EDI_REMITTANCE_METHOD             "TRIM(:EDI_REMITTANCE_METHOD)"  
,EDI_REMITTANCE_INSTRUCTION        "TRIM(:EDI_REMITTANCE_INSTRUCTION)"  
,EDI_TP_HEADER_ID                  "TRIM(:EDI_TP_HEADER_ID)"  
,EDI_ECE_TPE_LOCATION_CODE         "TRIM(:EDI_ECE_TPE_LOCATION_CODE)"  
,EMAIL_FORMAT                      "TRIM(:EMAIL_FORMAT)"  
,EMAIL_ADDRESS                     "TRIM(:EMAIL_ADDRESS)"  
,FAX_AREA_CODE                     "TRIM(:FAX_AREA_CODE)"  
,FAX_COUNTRY_CODE                  "TRIM(:FAX_COUNTRY_CODE)" 
,FAX_NUMBER                        "TRIM(:FAX_NUMBER)" 
,FAX_EXTENSION                     "TRIM(:FAX_EXTENSION)"  
,PHONE_AREA_CODE                   "TRIM(:PHONE_AREA_CODE)"  
,PHONE_COUNTRY_CODE                "TRIM(:PHONE_COUNTRY_CODE)"  
,PHONE_NUMBER                      "TRIM(:PHONE_NUMBER)"  
,PHONE_EXTENSION                   "TRIM(:PHONE_EXTENSION)"  
,TELEX_NUMBER                      "TRIM(:TELEX_NUMBER)"  
,WEB_TYPE                          "TRIM(:WEB_TYPE)"  
,URL                               "TRIM(:URL)"  
,PROCESS_STATUS                    "TRIM(:PROCESS_STATUS)"
,PROCESS_ERROR                     "TRIM(:PROCESS_ERROR)"  
,HDS_SITE_FLAG                     "TRIM(:HDS_SITE_FLAG)"  
,CREATION_DATE                     "TRIM(:CREATION_DATE)"  
,CREATED_BY                        "TRIM(:CREATED_BY)"  
,LAST_UPDATE_DATE                  NULLIF LAST_UPDATE_DATE=BLANKS  
,LAST_UPDATED_BY                   "TRIM(:LAST_UPDATED_BY)"  
,PROCESSING_DATE                   "TRIM(:PROCESSING_DATE)"  
,ARCHIVE_DATE                      "TRIM(:ARCHIVE_DATE)"  
,REQUEST_ID              NULLIF REQUEST_ID=BLANKS            
,ORG_ID                  NULLIF ORG_ID=BLANKS        
)