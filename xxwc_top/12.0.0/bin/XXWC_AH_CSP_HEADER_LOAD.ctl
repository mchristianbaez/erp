--*********************************************************
--*File Name: XXWC_CSP_HEADER_UPLOAD.ctl                  *
--*Author: Niraj K Ranjan                                 *
--*Creation Date: 31-May-2018                             *
--*Last Updated Date: 31-May-2018                         *
--*Data Source: File                                      *
--*Description: This is the control file. 
--*********************************************************
OPTIONS (SKIP=1)
LOAD DATA
REPLACE INTO TABLE XXWC.XXWC_CSP_HEADER_UPLOAD_TBL
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  PRICE_TYPE                     "rtrim(TRIM(:PRICE_TYPE),chr(13))"
 , ACCOUNT_NUMBER                 "rtrim(TRIM(:ACCOUNT_NUMBER),chr(13))"
 , ACCOUNT_NAME                   "rtrim(TRIM(:ACCOUNT_NAME),chr(13))"
 , SITE_NUMBER                    "rtrim(TRIM(:SITE_NUMBER),chr(13))"
 , BRANCH                         "rtrim(TRIM(:BRANCH),chr(13))"
 , CREATION_DATE                   SYSDATE
 , CREATED_BY          constant    "-1"
 , LAST_UPDATE_DATE               SYSDATE
 , LAST_UPDATED_BY     constant   "-1"
 , PROCESS_FLAG        constant   "ENTERED"
)