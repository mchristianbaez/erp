--*********************************************************
--*File Name: XXWC_INVITEM_CTL.ctl                        *
--*Author: Kiran .T                                       *
--*Creation Date: 15-Sep-2011                             *
--*Last Updated Date: 16-Sep-2011                         *
--*Data Source:   XXWC_INVITEM_STG                        *
--*Description: This is the control file. SQL Loader will *
--*           use to load the Ap Invoice information.     *
--*History:                                               *
--* 31-Aug-2012 Added new column private_label_category   *
--*********************************************************
OPTIONS(errors=999)
LOAD DATA
REPLACE INTO TABLE XXWC_INVITEM_STG
FIELDS TERMINATED BY '|'
TRAILING NULLCOLS
(   ORGANIZATION_CODE                "TRIM(:ORGANIZATION_CODE)"
 , PARTNUMBER                       "TRIM(:PARTNUMBER)"
 , PRIMSPARTNUMBER                  "TRIM(:PRIMSPARTNUMBER)"
 , DESCRIPTION                      "TRIM(:DESCRIPTION)"
 , EXTENDEDDESCRIPTION              CHAR(2000)
 , UNITOFMEASURE                    "TRIM(:UNITOFMEASURE)"
 , UNITWEIGHT                       "TRIM(:UNITWEIGHT)"
 , CONTAINERTYPE                    "TRIM(:CONTAINERTYPE)"
 , UNITLENGTH                       "TRIM(:UNITLENGTH)"
 , UNITWIDTH                        "TRIM(:UNITWIDTH)"
 , UNITHEIGHT                       "TRIM(:UNITHEIGHT)"
 , UNNUMBER                         "TRIM(:UNNUMBER)"
 , HAZARDCLASS                      "TRIM(:HAZARDCLASS)"
 , LISTPRICE                        "TRIM(:LISTPRICE)"
 , LEADTIME                         "TRIM(:LEADTIME)"
 , TAX_CODE                         "TRIM(:TAX_CODE)"
 , VENDORPARTNUMBER                 "TRIM(:VENDORPARTNUMBER)"
 , VELOCITYCLASSIFICATION           "TRIM(:VELOCITYCLASSIFICATION)"
 , CATEGORY                         "TRIM(:CATEGORY)"
 , MINMAXMIN                        "TRIM(:MINMAXMIN)"
 , MINMAXMAX                        "TRIM(:MINMAXMAX)"
 , SHELF_LIFE_CODE                  "TRIM(:SHELF_LIFE_CODE)"
 , sourcing_from                    "TRIM(:sourcing_from)"
 , inv_org                          "TRIM(:inv_org)"
 , PURCHASINGFLAG                   "TRIM(:PURCHASINGFLAG)"
 , CAPROP65                         "TRIM(:CAPROP65)"
 , PESTICIDEFLAG                    "TRIM(:PESTICIDEFLAG)"
 , VOCNUMBER                        "TRIM(:VOCNUMBER)"
 , PESTICIDEFLAGSTATE               "TRIM(:PESTICIDEFLAGSTATE)"
 , VOCCATEGORY                      "TRIM(:VOCCATEGORY)"
 , VOCSUBCATEGORY                   "TRIM(:VOCSUBCATEGORY)"
 , MSDSNUMBER                       "TRIM(:MSDSNUMBER)"
 , TEMPLATE                         "TRIM(:TEMPLATE)"
 , ITEMLOCATION1                    "TRIM(:ITEMLOCATION1)"
 , ITEMLOCATION2                    "TRIM(:ITEMLOCATION2)"
 , ITEMLOCATION3                    "TRIM(:ITEMLOCATION3)"
 , PACKAGEGROUP                     "TRIM(:PACKAGEGROUP)"
 , GTPINDICATOR                     "TRIM(:GTPINDICATOR)"
 , KITFLAG                          "TRIM(:KITFLAG)"
 , AVERAGEUNITS                     "TRIM(:AVERAGEUNITS)"
 , LOTSERIAL                        "TRIM(:LOTSERIAL)"
 , PRODUCTCODE                      "TRIM(:PRODUCTCODE)"
 , HAZARDYN                         "TRIM(:HAZARDYN)"
 , BUYER                            "TRIM(:BUYER)"
 , DUTYRATE                         "TRIM(:DUTYRATE)"
 , INTANGIBLE                       "TRIM(:INTANGIBLE)"
 , ATTRIBUTE11                      "TRIM(:ATTRIBUTE11)"
 , ATTRIBUTE10                      "TRIM(:ATTRIBUTE10)"
 , ATTRIBUTE17                      "TRIM(:ATTRIBUTE17)"
 , ATTRIBUTE9                       "TRIM(:ATTRIBUTE9)"
 , reservedstock                    "TRIM(:RESERVEDSTOCK)"
 , reviewtime                       "TRIM(:REVIEWTIME)"
 , intl_flag                        "TRIM(:INTL_FLAG)"
 , fixed_lot                        "TRIM(:fixed_lot)"
 , hazarddesc                       "TRIM(:hazarddesc)"
 , revenue_account                  "TRIM(:revenue_account)"
 , cogs_account                     "TRIM(:cogs_account)"
 , consigned_flag                   "TRIM(:consigned_flag)"
 , PROCESS_FLAG                      FILLER
 , private_label_category           "TRIM(:private_label_category)"
 , ERROR_MESSAGE		     FILLER)
