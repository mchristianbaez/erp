line=75
str='   VIEW_PRIV = "XXWC_EGO_ITEM_VIEW_PRIVILEGE"'
DPATH="/obase/ebiz/apps/apps_st/appl/ego/12.0.0/patch/115/export/*.ldt"
TFILE="/tmp/out.tmp.$$"
for f in $DPATH
do
if [ -f $f -a -r $f ]; then
sed "$line a\ $str" "$f" > $TFILE && mv $TFILE "$f"
else
echo "Error: Cannot read $f"
fi
done