--*********************************************************
--*File Name: XXWC_PO_CONV_STG_TBL.ctl                   
--*Author: Gopi Damuluri                                  
--*Creation Date: 10-Feb-2013                             
--*Last Updated Date: 10-Feb-2013                         
--*Data Source: XXWC_PO_CONV_STG_TBL                      
--*Description: This is the control file. SQL Loader is   
--*           used to load ASL PO information.            
--*********************************************************

LOAD DATA
APPEND INTO TABLE XXWC.XXWC_PO_CONV_STG_TBL
FIELDS TERMINATED BY '|'  optionally enclosed by '"'
TRAILING NULLCOLS
(PARTNUMBER              "TRIM(:PARTNUMBER)"
, ORGANIZATION_CODE      "LPAD(TRIM(:ORGANIZATION_CODE),3,'0')"
, ONHAND_QTY             "TRIM(:ONHAND_QTY)"
, AVG_COST               "TRIM(:AVG_COST)"
, VENDOR_NAME            CONSTANT "LM SCOFIELD"
, VENDOR_SITE_CODE       CONSTANT "PUR-LOS ANGE-CA"
, STATUS                 CONSTANT "N"
, CREATED_BY             CONSTANT "-1"
, CREATION_DATE          SYSDATE
, LAST_UPDATED_BY        CONSTANT "-1"
, LAST_UPDATE_DATE       SYSDATE
)