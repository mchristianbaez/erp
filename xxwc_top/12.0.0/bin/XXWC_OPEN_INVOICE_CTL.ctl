--*********************************************************************
--*File Name: XXWC_OPEN_INVOICE_CTL.ctl                               *
--*Author: Thiruvengadam Rajaiah.N                                    *
--*Creation Date: 15-Sep-2011                                         *
--*Last Updated Date: 21-May-2018 - Vamshi                            *
--*Data Source: AHH                                                   *
--*Description: This is the control file. SQL Loader will             *
--*           use to load the Ap Invoice information.                 *
--*           TMS#20180319-00237 - AH HARRIS AP Invoices Conversion   * 
--*********************************************************************
OPTIONS(SKIP=0,DIRECT=FALSE,PARALLEL=TRUE,ROWS=30000,BINDSIZE=20970000)
LOAD DATA
REPLACE INTO TABLE XXWC.XXWC_AP_INVOICE_INT
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( INVOICE_ID                        FILLER
 , INVOICE_NUM                      "TRIM(:INVOICE_NUM)"
 , INVOICE_TYPE_LOOKUP_CODE         FILLER
 , INVOICE_DATE                     "to_date(:INVOICE_DATE,'MM/DD/YY')"
 , VENDOR_NUM                       "TRIM(:VENDOR_NUM)"
 , VENDOR_NAME                      "TRIM(:VENDOR_NAME)"
 , VENDOR_SITE_ID                   FILLER
 , VENDOR_SITE_CODE                 "TRIM(:VENDOR_SITE_CODE)"
 , INVOICE_AMOUNT                   "TRIM(:INVOICE_AMOUNT)"
 , INVOICE_CURRENCY_CODE            "TRIM(:INVOICE_CURRENCY_CODE)"
 , TERMS_ID                         "TRIM(:TERMS_ID)" 
 , DESCRIPTION                      "TRIM(:DESCRIPTION)"
 , AWT_GROUP_ID                     FILLER
 , AWT_GROUP_NAME                   FILLER
 , ATTRIBUTE_CATEGORY              "TRIM(:ATTRIBUTE_CATEGORY)" 
 , ATTRIBUTE1                       "TRIM(:ATTRIBUTE1)" 
 , TERMS_NAME                       "REPLACE(REPLACE(TRIM(:TERMS_NAME),CHR(13),''),CHR(10),'')"  
)