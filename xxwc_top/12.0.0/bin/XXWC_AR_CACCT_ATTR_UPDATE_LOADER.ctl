--  Filename:      XXWC_AR_CACCT_ATTR_UPDATE_LOADER.ctl
--  Description:   Control file For Asset Number Import

--  Version     Date            Author             Modification
--  -------    --------       -----------        ----------------
--   

--  Usage   :  Loads the data 
--  Caution :  Loads  data into stage table XXWC.XXWC_CACCT_ATTR_UPDATE_STG

OPTIONS (SKIP=1)
LOAD DATA
INFILE *
TRUNCATE INTO TABLE XXWC.XXWC_CACCT_ATTR_UPDATE_STG
FIELDS TERMINATED BY ','  OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
       (   
        ACCOUNT_NUMBER               "Trim(:ACCOUNT_NUMBER)",
        ACCOUNT_NAME                 "Trim(:ACCOUNT_NAME)",
 	PRED_TRADE                   "Trim(:PRED_TRADE)",
        CUST_CLASS                   "Trim(:CUST_CLASS)"
	)

