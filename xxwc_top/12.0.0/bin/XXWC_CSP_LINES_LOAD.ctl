--*********************************************************
--*File Name: XXWC_CSP_LINES_INTERFACE.ctl                   *
--*Author: Shankar Hariharan                      *
--*Creation Date: 06-Dec-2012                             *
--*Last Updated Date: 06-Dec-2012                         *
--*Data Source:                                           *
--*Description: This is the control file. 
--*********************************************************
LOAD DATA
REPLACE INTO TABLE XXWC_CSP_LINES_INTERFACE
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(  reference_id                     "TRIM(:reference_id)"
 , agreement_type                   "TRIM(:agreement_type)"
 , start_date                       "to_date(:start_DATE,'MM/DD/YYYY')" 
 , end_date                         "to_date(:end_DATE,'MM/DD/YYYY')" 
 , product_attribute                "TRIM(:product_attribute)"
 , product_value                    "TRIM(:product_value)"
 , app_method                       "TRIM(:app_method)"
 , modifier_value                   "TRIM(:modifier_value)"
 , vq_number                        "TRIM(:vq_number)"
 , special_cost                     "TRIM(:special_cost)"
 , vendor_number                    "rtrim(TRIM(:vendor_number),chr(13))"
 , process_flag                     constant 'N'
)