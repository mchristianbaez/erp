--**************************************************************************
--*File Name: XXWC_RENTAL_SO_CONV_TBL.ctl                                  *
--*Author: Gopi Damuluri                                                   *
--*Creation Date: 31-Aug-2012                                              *
--*Last Updated Date: 31-Aug-2012                                          *
--*Data Source: PRISM                                                      *
--*Description: This is the control file. SQL Loader will                  *
--*           use to load the Rental  information.                         *
--**************************************************************************
-- 1.1   4/22/2013    Lucidity CG    Add shipping_method to data file (task 20130408-03457)
--                                   add last_invoice_date as filler field to reduce manual changes to data file
--                                    
--**************************************************************************
LOAD DATA
REPLACE INTO TABLE XXWC.XXWC_RENTAL_SO_CONV_TBL
FIELDS TERMINATED BY '|'  optionally enclosed by '"'
TRAILING NULLCOLS
( ORDER_TYPE                    "TRIM(:ORDER_TYPE)"
, LEGACY_PARTY_SITE_NUMBER      "TRIM(:LEGACY_PARTY_SITE_NUMBER)"
, SHIP_TO_ADDRESS               "TRIM(:SHIP_TO_ADDRESS)"
, SHIP_TO_CITY                  "TRIM(:SHIP_TO_CITY)"
, SHIP_TO_STATE                 "TRIM(:SHIP_TO_STATE)"
, SHIP_TO_ZIP                   "TRIM(:SHIP_TO_ZIP)"
, SHIP_TO_CONTACT               "TRIM(:SHIP_TO_CONTACT)"
, SHIP_TO_CONTACT_LN            "TRIM(:SHIP_TO_CONTACT_LN)"
, BILL_TO_CONTACT               "TRIM(:BILL_TO_CONTACT)"
, BILL_TO_CONTACT_LN            "TRIM(:BILL_TO_CONTACT_LN)"
, CUST_PO_NUMBER                "TRIM(:CUST_PO_NUMBER)"
, WAREHOUSE                     "LPAD(TRIM(:WAREHOUSE),3,'0')"
, ITEM_NUMBER                   "TRIM(:ITEM_NUMBER)"
, QUANTITY                      "TRIM(:QUANTITY)"
, RENTAL_CHARGE                 "TRIM(:RENTAL_CHARGE)"
, RENTAL_START_DATE             "TO_DATE(:RENTAL_START_DATE,'MM/DD/RRRR')"
, LAST_INVOICE_DATE             FILLER
, PRISM_SALES_ORDER_NUM         "TRIM(:PRISM_SALES_ORDER_NUM)"
, SHIPPING_METHOD               "TRIM(:SHIPPING_METHOD)"                -- rev 1.1 4/22/2013  task 20130408-03457
, CREATED_BY                    CONSTANT "-1"
, CREATION_DATE                 SYSDATE
, LAST_UPDATED_BY               CONSTANT "-1"
, LAST_UPDATE_DATE              SYSDATE
, STATUS                        CONSTANT "N"
 )