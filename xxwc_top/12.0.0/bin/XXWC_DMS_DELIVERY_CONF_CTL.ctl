--*********************************************************************************************************
--*File Name: XXWC_DMSS_DELIVERY_CONF_CTL.ctl      
--*Author: Kathy Poling                                   
--*Creation Date: 01-Jul-2014                             
--*Last Updated Date: 01-Jul-2014                         
--*Description: This is the control file. SQL Loader will 
--*use to load MyLogistics delivery Confirmation files    
--*REVISIONS:
--*VERSION DATE          AUTHOR(S)     DESCRIPTION
--*------- -----------   ------------  -------------------------
--*1.0     22-FEB-2016   Gopi Damuluri TMS# 20160217-00165 - Changes to B2B POD - Include DMS Delivery_Id
--*********************************************************************************************************
LOAD DATA
APPEND 
INTO TABLE XXWC.XXWC_OM_DMS_SHIP_CONFIRM_TBL
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( ORDER_NUMBER             "TRIM(:ORDER_NUMBER)"
, LINE_NUMBER      		"TRIM(:LINE_NUMBER)"
, DELIVERY_ID              "TRIM(:DELIVERY_ID)"
, BRANCH         		"TRIM(:BRANCH)"
, DELIVERED_QTY          	"TRIM(:DELIVERED_QTY)"
, STOP_EXCEPTION        	"TRIM(:STOP_EXCEPTION)"
, LINE_EXCEPTION        	"TRIM(:LINE_EXCEPTION)"
, SIGNED_BY			"TRIM(:SIGNED_BY)"
, NOTES        			"TRIM(:NOTES)"
, DRIVER_NAME            	"TRIM(:DRIVER_NAME)"
, ROUTE_NUMBER           	"TRIM(:ROUTE_NUMBER)"
, ROUTE_NAME         		"TRIM(:ROUTE_NAME)"
, DISPATCH_DATE     		"TO_DATE(:DISPATCH_DATE, 'MM/DD/YYYY')"
, FILE_NAME                     "TRIM(:FILE_NAME)" -- Version# 1.1
, CREATION_DATE        	SYSDATE
, CREATED_BY        		CONSTANT "-1"
, LAST_UPDATE_DATE        	SYSDATE
, LAST_UPDATED_BY        	CONSTANT "-1"
, SHIP_CONFIRM_STATUS		CONSTANT "NO"
, ORG_ID				CONSTANT "162")



