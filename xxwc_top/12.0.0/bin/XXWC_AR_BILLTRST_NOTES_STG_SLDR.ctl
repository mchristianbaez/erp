-- ******************************************************************************************************************
-- *   $Header XXWC_AR_BILLTRST_NOTES_STG_SLDR.ctl $
-- *   Module Name: XXWC_AR_BILLTRST_NOTES_STG_SLDR
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXWC_AR_BILLTRST_NOTES_STG_TBL
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author           Description
-- * ------  ----------  ---------------  ------------------------------------------------------------------------------------------------
-- *  1.0    09/20/2018  P.Vamshidhar     20180910-00004_Billtrust Notes related to AHH Invoices 
-- * ****************************************************************************************************************
OPTIONS (SKIP=0)
LOAD DATA
APPEND
INTO TABLE XXWC.XXWC_AR_BILLTRST_NOTES_STG_TBL
FIELDS TERMINATED BY "|" OPTIONALLY ENCLOSED BY '"' 
TRAILING NULLCOLS
(
ACCOUNT_NUMBER             "TRIM(:ACCOUNT_NUMBER)",
CUST_SITE_NUMBER           "TRIM(:CUST_SITE_NUMBER)",
ORDERNO                    "TRIM(:ORDERNO)",
ORDERSUF                   "TRIM(:ORDERSUF)",
NOTELN                     CHAR(4000) NULLIF (NOTELN=BLANKS),
PRINTFL                    "TRIM(:PRINTFL)",
PRINTFL5                   "TRIM(:PRINTFL5)",
NOTE_SOURCE                CONSTANT "AHH"
)                    