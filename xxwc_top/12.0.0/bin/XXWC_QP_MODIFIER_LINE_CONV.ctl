--*********************************************************
--*File Name: XXWC_QP_MODIFIER_LINE.ctl                   *
--*Author: Satish U                                       *
--*Creation Date: 16-Jan-2012                             *
--*Last Updated Date: 16-Jan-2012                         *
--*Data Source: XXWC_QP_MODIFIER_LINE.txt                 *
--*Description: This is the control file. SQL Loader will *
--*         use to load the Modifier header information.  *
--*********************************************************


OPTIONS(DIRECT=FALSE,PARALLEL=TRUE,ROWS=30000,BINDSIZE=20970000)
LOAD DATA
APPEND
INTO TABLE XXWC_QP_MODIFIER_LINE_CNV
FIELDS TERMINATED BY '|'
TRAILING NULLCOLS 
(  MODIFIER_LIST_NUMBER             "TRIM(:MODIFIER_LIST_NUMBER)"
 , LN_MODIFIER_LEVEL_CODE            CONSTANT 'LINE'
 , LN_LIST_LINE_TYPE_CODE            CONSTANT 'DIS' 
 , LN_START_DATE_ACTIVE             "TO_DATE(:LN_START_DATE_ACTIVE,'MM/DD/YYYY')"
 , LN_END_DATE_ACTIVE               "TO_DATE(:LN_END_DATE_ACTIVE,'MM/DD/YYYY')"
 , LN_AUTOMATIC_FLAG                 CONSTANT 'Y'
 , LN_OVERRIDE_FLAG                  CONSTANT 'N'
 , LN_PRICING_PHASE_ID               CONSTANT 2
 , LN_INCOMPATIBILITY_GRP_CODE       CONSTANT 'EXCL'
 , LN_PRICING_GROUP_SEQUENCE         CONSTANT 1
 , PA_PRODUCT_ATTRIBUTE_CONTEXT      CONSTANT 'ITEM'
 , PA_PRODUCT_ATTRIBUTE             "TRIM(:PA_PRODUCT_ATTRIBUTE)"
 , PA_PRODUCT_ATTR_VALUE            "TRIM(:PA_PRODUCT_ATTR_VALUE)"
 , LN_PRODUCT_PRECEDENCE             CONSTANT 220
 , PA_PRICING_ATTRIBUTE             "TRIM(:PA_PRICING_ATTRIBUTE)"
 , LN_PRICE_BREAK_TYPE_CODE         "TRIM(:LN_PRICE_BREAK_TYPE_CODE)"
 , PA_COMPARISON_OPERATOR_CODE      "TRIM(:PA_COMPARISON_OPERATOR_CODE)"
 , PA_PRODUCT_UOM_CODE              "TRIM(:PA_PRODUCT_UOM_CODE)"
 , LN_INCLUDE_ON_RETURNS_FLAG        CONSTANT 'Y' 
 , LN_FORMULA                       "TRIM(:LN_FORMULA)"
 , LN_ARITHMETIC_OPERATOR_TYPE      "TRIM(:LN_ARITHMETIC_OPERATOR_TYPE)"
,  LN_LIST_LINE_ID                   INTEGER EXTERNAL(15) 
 , PA_PRICING_ATTRIBUTE_ID           INTEGER EXTERNAL(15) 
 , LN_OPERAND                       "TRIM(:LN_OPERAND)"
 , LN_ACCRUAL_FLAG                   CONSTANT 'N'
 , PA_EXCLUDER_FLAG                  CONSTANT 'N' 
 , CREATED_BY         CONSTANT "-1"
 , CREATION_DATE      SYSDATE
 , LAST_UPDATED_BY    CONSTANT "-1"
 , LAST_UPDATE_DATE   SYSDATE
)
 
