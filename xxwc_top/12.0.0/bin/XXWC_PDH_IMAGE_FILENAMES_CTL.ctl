--//============================================================================
--//
--// Object Name         :: xxwc_pdh_image_filenames_ctl
--//
--// Object Type         :: Control File
--//
--// Object Description  :: This is the control file for SQL Loader to load into
--//                        xwc.xxwc_pdh_image_filenames_tmp
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     15/03/2014    Initial Build - TMS#20140207-00083
--//============================================================================
LOAD DATA
APPEND INTO TABLE xxwc.xxwc_pdh_image_filenames_tmp
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
( XXWC_IMAGE_FILENAME_ATTR  "TRIM(:XXWC_IMAGE_FILENAME_ATTR)"
, XXWC_IMAGE_FILETYPE_ATTR  "TRIM(:XXWC_IMAGE_FILETYPE_ATTR)"
, LAST_UPDATE_DATE          SYSDATE
)
