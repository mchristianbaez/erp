--********************************************************************************************************************************************************************
--   File Name: XXWC_B2B_SO_HDRS_STG_TBL_CTL.ctl            
--   Description: This is the control file. SQL Loader will use to load B2B SalesOrder files
--   
--   ==============================================================================
--     VERSION DATE               AUTHOR(S)       DESCRIPTION
--     ------- -----------------  --------------- -----------------------------------------
--     1.1     04-Apr-2015        Gopi Damuluri   TMS# 20150302-00006
--                                                Added the following columns PROJECT_ID, PROJECT_NAME, COMPANY_ID, ACCOUNT_NUMBER to XXWC.XXWC_B2B_SO_HDRS_STG_TBL
--                                                TMS# 20150317-00069
--                                                Added Notes column to XXWC.XXWC_B2B_SO_HDRS_STG_TBL and XXWC.XXWC_B2B_SO_LINES_STG_TBL tables
--********************************************************************************************************************************************************************
LOAD DATA
APPEND 
CONTINUEIF THIS PRESERVE(1:1)='OH'
INTO TABLE XXWC.XXWC_B2B_SO_HDRS_STG_TBL
WHEN (01) = 'OH'
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( REC_TYPE                "TRIM(:REC_TYPE)"
, CUSTOMER_PO_NUMBER      "TRIM(:CUSTOMER_PO_NUMBER)"
, ORDER_TOTAL             "TRIM(:ORDER_TOTAL)"
, CUSTOMER_NUMBER         "TRIM(:CUSTOMER_NUMBER)"
, SHIP_TO_NUMBER          "TRIM(:SHIP_TO_NUMBER)"
, SHIP_TO_ADDRESS1        "TRIM(:SHIP_TO_ADDRESS1)"
, SHIP_TO_ADDRESS2        "TRIM(:SHIP_TO_ADDRESS2)"
, SHIP_TO_ADDRESS3        "TRIM(:SHIP_TO_ADDRESS3)"
, SHIP_TO_ADDRESS4        "TRIM(:SHIP_TO_ADDRESS4)"
, SHIP_TO_CITY            "TRIM(:SHIP_TO_CITY)"
, SHIP_TO_STATE           "TRIM(:SHIP_TO_STATE)"
, SHIP_TO_COUNTRY         "TRIM(:SHIP_TO_COUNTRY)"
, SHIP_TO_POSTAL_CODE     "TRIM(:SHIP_TO_POSTAL_CODE)"
, BILL_TO_ADDRESS1        "TRIM(:BILL_TO_ADDRESS1)"
, BILL_TO_ADDRESS2        "TRIM(:BILL_TO_ADDRESS2)"
, BILL_TO_ADDRESS3        "TRIM(:BILL_TO_ADDRESS3)"
, BILL_TO_ADDRESS4        "TRIM(:BILL_TO_ADDRESS4)"
, BILL_TO_CITY            "TRIM(:BILL_TO_CITY)"
, BILL_TO_STATE           "TRIM(:BILL_TO_STATE)"
, BILL_TO_COUNTRY         "TRIM(:BILL_TO_COUNTRY)"
, BILL_TO_POSTAL_CODE     "TRIM(:BILL_TO_POSTAL_CODE)"
, DELIVER_TO_ADDRESS1     "TRIM(:DELIVER_TO_ADDRESS1)"
, DELIVER_TO_ADDRESS2     "TRIM(:DELIVER_TO_ADDRESS2)"
, DELIVER_TO_ADDRESS3     "TRIM(:DELIVER_TO_ADDRESS3)"
, DELIVER_TO_ADDRESS4     "TRIM(:DELIVER_TO_ADDRESS4)"
, DELIVER_TO_CITY         "TRIM(:DELIVER_TO_CITY)"
, DELIVER_TO_STATE        "TRIM(:DELIVER_TO_STATE)"
, DELIVER_TO_COUNTRY      "TRIM(:DELIVER_TO_COUNTRY)"
, DELIVER_TO_POSTAL_CODE  "TRIM(:DELIVER_TO_POSTAL_CODE)"
, SHIPPING_INSTRUCTIONS   "TRIM(:SHIPPING_INSTRUCTIONS)"
, CONTACT_FIRST_NAME      "TRIM(:CONTACT_FIRST_NAME)"
, CONTACT_LAST_NAME       "TRIM(:CONTACT_LAST_NAME)"
, EMAIL                   "TRIM(:EMAIL)"
, CONTACT_NUM             "TRIM(:CONTACT_NUM)"
, FAX_NUM                 "TRIM(:FAX_NUM)"
, SHIP_TO_CONTACT_FIRST_NAME      "TRIM(:SHIP_TO_CONTACT_FIRST_NAME)"       -- TMS# 20140911-00299
, SHIP_TO_CONTACT_LAST_NAME       "TRIM(:SHIP_TO_CONTACT_LAST_NAME)"        -- TMS# 20140911-00299
, PROJECT_ID              "TRIM(:PROJECT_ID)"                               -- Version# 1.1
, PROJECT_NAME            "TRIM(:PROJECT_NAME)"                             -- Version# 1.1
, COMPANY_ID              "TRIM(:COMPANY_ID)"                               -- Version# 1.1
, ACCOUNT_NUMBER          "TRIM(:ACCOUNT_NUMBER)"                           -- Version# 1.1
, NOTES                   "TRIM(:NOTES)"                                    -- Version# 1.1
, CREATED_BY              CONSTANT "-1"
, CREATION_DATE           SYSDATE
, LAST_UPDATED_BY         CONSTANT "-1"
, LAST_UPDATE_DATE        SYSDATE
, PROCESS_FLAG            CONSTANT "N")


INTO TABLE XXWC.XXWC_B2B_SO_LINES_STG_TBL
WHEN (01) = 'OL'
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( rec_skip filler        POSITION(1)
, CUSTOMER_PO_NUMBER    "TRIM(:CUSTOMER_PO_NUMBER)"
, LINE_NUMBER           "TRIM(:LINE_NUMBER)"
, INVENTORY_ITEM        "TRIM(:INVENTORY_ITEM)"
, ORDERED_QUANTITY      "TRIM(:ORDERED_QUANTITY)"
, UNIT_OF_MEASURE       "TRIM(:UNIT_OF_MEASURE)"
, UNIT_SELLING_PRICE    "TRIM(:UNIT_SELLING_PRICE)"
, REQUEST_DATE           DATE 'YYYYMMDD'
, ITEM_DESCRIPTION      "TRIM(:ITEM_DESCRIPTION)"
, NOTES                 "TRIM(:NOTES)"                                      -- Version# 1.1
, CREATED_BY            CONSTANT "-1"
, CREATION_DATE         SYSDATE
, LAST_UPDATED_BY       CONSTANT "-1"
, LAST_UPDATE_DATE      SYSDATE
, PROCESS_FLAG          CONSTANT "N")
