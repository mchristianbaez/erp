--*********************************************************
--* File Name: XXWC_PRL_HEADERS.CTL				   *
--* Author: Consuelo Gonzalez					   *
--* Creation Date: 06-May-2013					   *
--* Last Updated Date: 06-May-2013                        *
--*********************************************************

LOAD DATA
APPEND INTO TABLE XXWC.XXWC_PRL_HEADER_STG
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(   NAME                             "TRIM(:NAME)"
 , DESCRIPTION                      "TRIM(:DESCRIPTION)"
 , WAREHOUSE                        "TRIM(:WAREHOUSE)"
)