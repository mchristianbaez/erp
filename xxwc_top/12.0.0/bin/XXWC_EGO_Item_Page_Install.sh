###############################################################################
# Application       :                                                         #
# Program Name      : Single Page - OAF Install Script                        #
# Version #         : 1.0                                                     #
# Remarks           : Shell Script                                            #
# Created by        : Shankar Vanga                                           #
# Creation  Date    : Mar-03-2015                                             #
# Description       : Shell script for Release1 OAF. Expects apps             #
#                      $1 as password                                         #
#                    , $2 as Database server name,                            # 
#                    , $3 as Port Number                                      #
#                    , $4 as SID                                              #
# Change History:                                                             #
#=============================================================================#
# Date        | Name             | Remarks                                    #
#=============================================================================#
# 03-Mar-2015 | Shankar Vanga    | Initial Creation                           #
###############################################################################

logFile=xxwc_item_page_install.log
oafZipFile=xxwc_ego_item_page_oaf.zip
oafZipClassFiles=xxwc_ego_item_classes.zip
dbHost=ebizdev.hdsupply.net
dbPort=1521
dbSid=ebizdev
serverName=ebizdev
dbPwd=test
fileList=FilesList
customizationFileList=
startTime=`date`

echo $startTime > $logFile
echo "Start Single Item Create and Maintain Install script.." >> $logFile

echo "Unzipiing file to "$JAVA_TOP >> LogFile
unzip -o $XXWC_TOP/java/$oafZipFile -d $JAVA_TOP

echo "Unzipiing file to "$JAVA_TOP >> LogFile
unzip -o $XXWC_TOP/java/$oafZipClassFiles -d $JAVA_TOP

#echo "Compiling JAVA Files " >> $logFile
#javac $JAVA_TOP/xxwc/oracle/apps/ego/item/eu/server/*.java
#javac $JAVA_TOP/xxwc/oracle/apps/ego/item/eu/webui/*.java
#javac $JAVA_TOP/xxwc/oracle/apps/ego/item/eu/util/*.java

echo "Importing OAF pages..." >> $logFile
#---------------------------------------------------------------------------------


if [ ! -z "$1"  ]; then
  dbHost=$1
else
  echo "Enter DB host [$dbHost]:"
  read a 
  if [ ! -z "$a" ]; then
   dbHost=$a 
  fi
fi

if [ ! -z "$2"  ]; then
  dbPort=$2
else
  echo "Enter DB port [$dbPort]:"
  read a 
  if [ ! -z "$a" ]; then
   dbPort=$a 
  fi
fi
  
if [ ! -z "$3"  ]; then
  dbSid=$3
else
  echo "Enter DB SERVICE or SID [$dbSid]:"
  read a 
  if [ ! -z "$a" ]; then
   dbSid=$a 
  fi
fi

if [ ! -z "$4"  ]; then
  dbPwd=$4
else
  echo "Enter APPS password [$dbPwd]:"
  read a 
  if [ ! -z "$a" ]; then
   dbPwd=$a 
  fi
fi


echo "DB Host :  $dbHost"
echo "DB Port :  $dbPort"
echo "DB Service : $dbSid"

echo "Verify the database parameters, do you want to continue to import the files (Y/N)"
read a 
if [ "$a" = "N" ]; then
   exit 
fi


#------------------------------------------------------------------------------
#                     List of OAF XML files
#------------------------------------------------------------------------------
fileList="/xxwc/oracle/apps/ego/item/eu/webui/VendorNameLOVRN.xml \
         /xxwc/oracle/apps/ego/item/eu/webui/XXWCSingleItemPG.xml \
         /xxwc/oracle/apps/ego/item/eu/webui/XXWCItemSearchPG.xml \
         /xxwc/oracle/apps/ego/item/eu/webui/WorkflowMonitorPG.xml"

#customizationFileList="/xxwc/oracle/apps/ego/item/eu/webui/XXWCSingleItemPG.xml"


rootDir=$JAVA_TOP
for fileName in $fileList
do
  echo "Importing file $fileName" >>$logFile
  java oracle.jrad.tools.xml.importer.XMLImporter -platform linux \
  $rootDir/$fileName \
  -rootdir $rootDir  -userId 1  -username apps -password $dbPwd \
 -dbconnection "(description = (address_list = (address = (community = tcp.world)(protocol = tcp)(host =$dbHost)(port = $dbPort)))(connect_data = (SERVICE_NAME=$dbSid)))"
done

#if [ ! -z "$customizationFileList" ]; then
#  for fileName in $customizationFileList
#  do
#    echo "Importing file $fileName" >>$logFile
#    java oracle.jrad.tools.xml.importer.XMLImporter -platform linux \
#    $rootDir/$fileName \
#    -rootdir $rootDir/xxwc  -userId 1  -username apps -password $dbPwd \
#    -dbconnection "(description = (address_list = (address = (community = tcp.world)(protocol = tcp)(host =$dbHost)(port = $dbPort)))(connect_data = (SERVICE_NAME=$dbSid)))"
#  done
#fi
#-----------------------------------------------------------------