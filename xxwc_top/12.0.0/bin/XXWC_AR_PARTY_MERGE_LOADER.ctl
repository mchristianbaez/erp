--  Filename:      XXWC_AR_PARTY_MERGE_LOADER.ctl
--  Description:   Control file For Asset Number Import

--  Version     Date            Author             Modification
--  -------    --------       -----------        ----------------
--   

--  Usage   :  Loads the data 
--  Caution :  Loads  data into stage table XXWC.XXWC_AR_PARTY_MERGE_STG

OPTIONS (SKIP=1)
LOAD DATA
INFILE *
TRUNCATE INTO TABLE XXWC.XXWC_AR_PARTY_MERGE_STG
FIELDS TERMINATED BY ','  OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
   (   
        FROM_PARTY_NO                "Trim(:FROM_PARTY_NO)",
        TO_PARTY_NO                 "Trim(:TO_PARTY_NO)"
	)

