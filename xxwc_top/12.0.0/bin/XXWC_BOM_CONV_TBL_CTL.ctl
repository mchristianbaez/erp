--*********************************************************
--*File Name: XXWC_BOM_CONV_TBL.ctl                       *
--*Author: Gopi Damuluri                                  *
--*Creation Date: 31-Aug-2012                             *
--*Last Updated Date: 31-Aug-2012                         *
--*Data Source: PRISM                                     *
--*Description: This is the control file. SQL Loader will *
--*             use to load the BOM  information.         *
--*********************************************************

LOAD DATA
REPLACE INTO TABLE XXWC.XXWC_BOM_CONV_TBL
FIELDS TERMINATED BY '|'  optionally enclosed by '"'
TRAILING NULLCOLS
( WAREHOUSE          "LPAD(TRIM(:WAREHOUSE),3,'0')"
, ITEM_NUMBER        "TRIM(:ITEM_NUMBER)"
, ITEM_SEQ           "TRIM(:ITEM_SEQ)"
, COMPONENT          "TRIM(:COMPONENT)"
, UOM                "TRIM(:UOM)"
, QUANTITY           "TRIM(:QUANTITY)"
, CREATED_BY         CONSTANT "-1"
, CREATION_DATE      SYSDATE
, LAST_UPDATED_BY    CONSTANT "-1"
, LAST_UPDATE_DATE   SYSDATE
, STATUS             CONSTANT "N"
 )
