--*********************************************************
--*File Name: XXWC_ITEM_DFF.ctl                   
--*Author: Scott Spivey, Lucidity CG
--*Creation Date: 22-MAR-2013
--*Last Updated Date: 22-MAR-2013
--*Data Source: XXWC.ITEM_DFF
--*Description: This is the control file. SQL Loader is   
--*           used to load Taxware Code information.            
--*********************************************************
OPTIONS (SKIP=1)
LOAD DATA

TRUNCATE INTO TABLE XXWC.ITEM_DFF
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
(ITEM_CODE                CHAR  "Trim(:ITEM_CODE)"
,ATTRIBUTE22              CHAR  "Trim(:ATTRIBUTE22)"
)
