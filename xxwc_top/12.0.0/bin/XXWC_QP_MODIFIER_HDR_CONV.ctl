--*********************************************************
--*File Name: XXWC_QP_MODIFIER_HDR.ctl                    *
--*Author: Satish U                                       *
--*Creation Date: 16-Jan-2012                             *
--*Last Updated Date: 16-Jan-2012                         *
--*Data Source: XXWC_QP_MODIFIER_HDR.txt                  *
--*Description: This is the control file. SQL Loader will *
--*         use to load the Modifier header information.  *
-- OPTIONALLY ENCLOSED BY '"'
--*********************************************************

LOAD DATA
APPEND INTO TABLE XXWC_QP_MODIFIER_HDR_CNV
FIELDS TERMINATED BY '|'
TRAILING NULLCOLS
 ( LIST_TYPE_CODE                    CONSTANT 'DLT'
 , MODIFIER_LIST_NUMBER             "TRIM(:MODIFIER_LIST_NUMBER)"
 , MODIFIER_TYPE                     "TRIM(:MODIFIER_TYPE)"
 , ACTIVE_FLAG                       CONSTANT 'Y'
 , AUTOMATIC_FLAG                    CONSTANT 'Y'
 , MODIFIER_LIST_NAME               "TRIM(:MODIFIER_LIST_NAME)"
 , GLOBAL_FLAG                       CONSTANT 'Y'
 , START_DATE_ACTIVE                "TO_DATE(:START_DATE_ACTIVE,'MM/DD/YYYY')" 
 , END_DATE_ACTIVE                  "TO_DATE(:END_DATE_ACTIVE,'MM/DD/YYYY')"
 , CURRENCY_CODE                     CONSTANT 'USD'
 , COMMENTS                         "TRIM(:COMMENTS)"
 , SOURCE_SYSTEM_CODE                CONSTANT  'QP'
 , PTE_CODE                          CONSTANT  'ORDFUL'
 , QL_QUALIFIER_GROUPING_NO          CONSTANT '1'
 , QL_QUALIFIER_CONTEXT              CONSTANT 'CUSTOMER'
 , QL_QUALIFIER_ATTRIBUTE            "TRIM(:QL_QUALIFIER_ATTRIBUTE)" 
 , QL_QUALIFIER_ATTR_VALUE           "TRIM(:QL_QUALIFIER_ATTR_VALUE)"
 , QL_COMPARISON_OPERATOR_CODE       CONSTANT '='
 , QL_QUALIFIER_PRECEDENCE           CONSTANT '220'
 , QL_ACTIVE_FLAG                    CONSTANT 'Y' 
 , CREATED_BY         CONSTANT "-1"
 , CREATION_DATE      SYSDATE
 , LAST_UPDATED_BY    CONSTANT "-1"
 , LAST_UPDATE_DATE   SYSDATE
 )
 
