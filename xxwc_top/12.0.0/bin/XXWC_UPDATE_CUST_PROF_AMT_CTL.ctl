--*********************************************************
--*File Name: XXWC_UPDATE_CUST_PROF_AMT_CTL.ctl           *
--*Author: Gopi Damuluri                                  *
--*Creation Date: 10-Jan-2013                             *
--*Last Updated Date: 10-Jan-2013                         *
--*Data Source: PRISM                                     *
--*Description: This is the control file. SQL Loader will *
--*             use to load the CustProfileAmt Stg data   *
--*********************************************************

LOAD DATA
REPLACE INTO TABLE XXWC.XXWC_UPDATE_CUST_PROF_AMT_TBL
FIELDS TERMINATED BY '|'
TRAILING NULLCOLS
( CUST_ACCT_PROFILE_AMT_ID          "TRIM(:CUST_ACCT_PROFILE_AMT_ID)"
, OBJECT_VERSION_NUMBER             "TRIM(:OBJECT_VERSION_NUMBER)"
, UPDATED_CREDIT_LIMIT              "TRIM(:UPDATED_CREDIT_LIMIT)"
, STATUS                            CONSTANT "N"
 )
