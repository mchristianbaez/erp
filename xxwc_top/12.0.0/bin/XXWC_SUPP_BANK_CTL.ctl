--*********************************************************
--*File Name: XXWC_OPEN_INVOICE_CTL.ctl                   *
--*Author: Thiruvengadam Rajaiah.N                        *
--*Creation Date: 15-Sep-2011                             *
--*Last Updated Date: 16-Sep-2011                         *
--*Data Source:                                           *
--*Description: This is the control file. SQL Loader will *
--*           use to load the Ap Open Invoice information.*
--*********************************************************

LOAD DATA
REPLACE INTO TABLE XXWC_IBY_BANK_ACCOUNTS
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( VENDOR_NUMBER                    "TRIM(:VENDOR_NUMBER)"
 , BANK_NUMBER                      "TRIM(:BANK_NUMBER)"
 , BRANCH_NUMBER                    "TRIM(:BRANCH_NUMBER)"
 , BANK_ACCOUNT_NAME                "TRIM(:BANK_ACCOUNT_NAME)"
 , BANK_ACCOUNT_NUM                 "TRIM(:BANK_ACCOUNT_NUM)"
 , CURRENCY_CODE                    "TRIM(:CURRENCY_CODE)"
 , IBAN                             "TRIM(:IBAN)"
 , CHECK_DIGITS                     "TRIM(:CHECK_DIGITS)"
 , BANK_ACCOUNT_TYPE                "TRIM(:BANK_ACCOUNT_TYPE)"
 , START_DATE                       "to_date(:START_DATE,'RR/MM/DD')")