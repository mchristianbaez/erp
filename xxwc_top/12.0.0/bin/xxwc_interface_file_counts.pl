#!/usr/bin/perl

# Version 1.0
#
# Name        : xxwc_interface_file_counts.pl 
# Date Written: 11/7/2016
# Author      : Andre Rivas (ls command written by Govardhan)
#
# Description : File counts in Interface directories
#	This script takes a single parameter of the target name
# 	on which it is being executed. It uses this target name
#	to determine the EBS instance name, and also the path
#	to a file containing the folder list and definitions.
#
#	See this path for an example: /xx_iface/ebsqa/outbound/oem_monitors/filecountfolderlist.txt
#
#	The File Count Folder List file must be present on the 
# 	host and it must be in the correct path based on the 
#	instance name.
#
#	This script splits the first parameter by the dash (-) to
#	find the instance name. This is for OEM compatibility.
#
# Usage     : perl xxwc_interface_file_counts.pl ebsqa-Infrastructure ebsqa_ghdebs01lqs-APPL_TOP Context
#
# Modification History:
#
# When         Who           Did what
# --------------------------------------------------------------------------
# 02-Nov-2016  Govardhan    Created
# --------------------------------------------------------------------------

my $target = @ARGV[0];

# Variable for the instance
my $instance;
# Set the instance based on the target.

# If the target has a dash in it
if ($target =~ m/[a-zA-Z]*-.*/) {
	my @targetsplit = split("-", $target);	# split the instance name by -
	$instance = @targetsplit[0];				# the first piece is the instance
	chomp($instance);
} else {die "Unknown host for this script: $target\n";}

# Define a file handle for the file containing a list of folders
my $folders;
# Open the folders file
open($folders, "<", "/xx_iface/$instance/outbound/oem_monitors/filecountfolderlist.txt") or die "Couldn't open $folderfile: $!\n";

# Do this for every line in the folders file
while (<$folders>) {
	if (m/#.*/) { next; } # Skip comment lines
	
	my @folderinfo;
	@folderinfo = split();
	
	my $foldername = @folderinfo[0];
	my $folderpath = @folderinfo[1];
	
	my $filecount = `ls -F /xx_iface/$instance/$folderpath | grep -v \"/\" | wc -l`;
	chomp($filecount);
	print "$foldername|/xx_iface/$instance/$folderpath|$filecount\n";
}

# Clean up and close the folders file.
close($folders);
