--*********************************************************
--*File Name:         XXWC_STAGE_UDA_TAB_CTL.ctl          *
--*Author:            Praveen Pawar                       *
--*Creation Date:     18-Apr-2014                         *
--*Last Updated Date: 21-Apr-2014                         *
--*Data Source:       UDA Mass Upload csv FILE            *
--*Description:       This is the control file. SQL Loader*
--*                   will use to load the UDA data       *
--*********************************************************

OPTIONS (SKIP = 1)
LOAD DATA
APPEND INTO TABLE XXWC.XXWC_STAGE_UDA_TAB
FIELDS TERMINATED BY '|' TRAILING NULLCOLS
( ITEM_NUMBER                     "TRIM(:ITEM_NUMBER)"
, INVENTORY_ITEM_ID               Constant ''
, ORGANIZATION_CODE               "TRIM(:ORGANIZATION_CODE)"
, ORGANIZATION_ID                 Constant ''
, ITEM_CATALOG_CATEGORY           "TRIM(:ITEM_CATALOG_CATEGORY)"
, ITEM_CATALOG_GROUP_ID           Constant ''
, ATTRIBUTE_GROUP_INTERNAL_NAME   "TRIM(:ATTRIBUTE_GROUP_INTERNAL_NAME)"
, ATTRIBUTE_GROUP_ID              Constant ''
, ATTRIBUTE_INTERNAL_NAME         "TRIM(:ATTRIBUTE_INTERNAL_NAME)"
, ATTRIBUTE_VALUE                 "TRIM(:ATTRIBUTE_VALUE)"
, DATA_TYPE_CODE                  
, STATUS                          Constant 'NEW'       
, ERROR_MSG                       
, LAST_UPDATED_BY                 
, LAST_UPDATE_DATE                SYSDATE
)