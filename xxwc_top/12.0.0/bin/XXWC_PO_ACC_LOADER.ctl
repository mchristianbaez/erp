OPTIONS (ERRORS = 0)
LOAD DATA
APPEND
CONTINUEIF THIS PRESERVE(1:3)='ENV'
INTO TABLE XXWC.XXWC_PO_ACCEPTANCE_HEADERS
WHEN (01) = 'ENV'
FIELDS TERMINATED BY ","
TRAILING NULLCOLS

(TAG                       filler 
,INTERCHANGE_SENDER_ID     filler 
,INTERCHANGE_RECEIVER_ID   filler 
,CONFIRMATION_DATE         DATE "YYMMDD"
,INTERCHANGE_TIME          filler 
,INTERCHANGE_CONTROL_NO    filler 
,TEST_PROD                 filler 
,GROUP_SENDER_ID           filler 
,GROUP_RECEIVER_ID         filler 
,GROUP_DATE                filler 
,GROUP_TIME                filler 
,GROUP_CONTROL_NO          filler 
,TRANSACTION_CONTROL_NO    filler 
,ACC_HEADER_ID EXPRESSION "XXWC.XXWC_PO_ACC_HEADER_S.NEXTVAL"
,CREATION_DATE EXPRESSION "SYSDATE"
,PURPOSE_CODE
,PO_TYPE
,PO_NUMBER
,PO_DATE                    DATE "YYYYMMDD"       
,REVISION_NUMBER
,DELIVERY_DATE              DATE "YYYYMMDD"
,WHITE_CAP_LOCATION_NAME
,WHITE_CAP_LOCATION_CODE
,WHITE_CAP_ADDRESS_1
,WHITE_CAP_ADDRESS_2
,WHITE_CAP_CITY
,WHITE_CAP_STATE
,WHITE_CAP_POSTAL_CODE
,VENDOR_LOCATION_NAME
,VENDOR_LOCATION_CODE
,VENDOR_ADDRESS_1
,VENDOR_ADDRESS_2
,VENDOR_CITY
,VENDOR_STATE
,VENDOR_POSTAL_CODE
,FREIGHT_ACCOUNT_NUMBER
,CARRIER_NAME
,SCAC_CODE
,NUMBER_OF_LINE_ITEMS
,HASH_TOTAL
,STATUS CONSTANT 'READY')

INTO TABLE XXWC.XXWC_PO_ACCEPTANCE_LINES
WHEN (01) = 'DTL'
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(rec_skip filler POSITION(1)
,ACC_DETAIL_ID EXPRESSION "XXWC.XXWC_PO_ACC_LINES_S.NEXTVAL"
,ACC_HEADER_ID EXPRESSION "XXWC.XXWC_PO_ACC_HEADER_S.CURRVAL" 
,ASSIGNED_ID
,QUANTITY_ORDERED
,PO_UNIT_OF_MEASURE
,UNIT_PRICE
,WHITE_CAP_ITEM_NUMBER
,VENDOR_ITEM_NUMBER
,UPC_NUMBER
,ITEM_DESCRIPTION
,LINE_ITEM_STATUS_CODE
,QUANTITY
,ACK_UNIT_OF_MEASURE
,SCHEDULED_SHIP_DATE   DATE "YYYYMMDD")

 