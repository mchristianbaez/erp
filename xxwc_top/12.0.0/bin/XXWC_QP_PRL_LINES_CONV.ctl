--*********************************************************
--* File Name: XXWC_PRL_LINES.CTL				   *
--* Author: Consuelo Gonzalez					   *
--* Creation Date: 06-May-2013					   *
--* Last Updated Date: 06-May-2013                        *
--*********************************************************

LOAD DATA
APPEND INTO TABLE XXWC.XXWC_PRL_LINES_STG
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(PRL_NAME                         "TRIM(:PRL_NAME)"
 , PRODUCT_ATTRIBUTE                "TRIM(:PRODUCT_ATTRIBUTE)"
 , PRODUCT_VALUE                    "TRIM(:PRODUCT_VALUE)"
 , VALUE                            "TRIM(:VALUE)"
 , FORMULA_NAME                     "TRIM(:FORMULA_NAME)"
 , CALL_FOR_PRICE			    "TRIM(:CALL_FOR_PRICE)"
 --, LIST_HEADER_ID                   "TRIM(:LIST_HEADER_ID)"
 --, PRODUCT_ID                       "TRIM(:PRODUCT_ID)"
 --, FORMULA_ID                       "TRIM(:FORMULA_ID)"
 --, STATUS                           "TRIM(:STATUS)"
 --, ERR_MESSAGE                      "TRIM(:ERR_MESSAGE)"
)