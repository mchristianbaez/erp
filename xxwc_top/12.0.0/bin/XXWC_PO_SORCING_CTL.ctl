--*********************************************************
--*File Name: XXWC_PO_SORCING_CTL.ctl                     *
--*Author: Kiran .T                                       *
--*Creation Date: 15-Oct-2011                             *
--*Last Updated Date: 16-Oct-2011                         *
--*Data Source:   XXWC_SOURCING_RULES_CONV		    *
--*Description: This is the control file. SQL Loader will *
--*           use to load the PO Sourcing  information.   *
--*********************************************************

LOAD DATA
APPEND INTO TABLE XXWC_SOURCING_RULES_CONV
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( SOURCING_RULE_NAME               "TRIM(:SOURCING_RULE_NAME)"
 , SOURCING_RULE_DESC               "TRIM(:SOURCING_RULE_DESC)"
 , SOURCING_RULE_TYPE               "TRIM(:SOURCING_RULE_TYPE)"
 , STATUS                           "TRIM(:STATUS)"
 , OPERATION                        "TRIM(:OPERATION)"
 , PLANNING_ACTIVE                  "TRIM(:PLANNING_ACTIVE)"
 , ATTRIBUTE_CATEGORY               FILLER
 , ATTRIBUTE1                       FILLER
 , ATTRIBUTE2                       FILLER
 , ATTRIBUTE3                       FILLER
 , ATTRIBUTE4                       FILLER
 , DISABLE_DATE                     FILLER
 , EFFECTIVE_DATE                   FILLER
,RECEIPT_ORGANIZATION_CODE          "TRIM(:RECEIPT_ORGANIZATION_CODE)"
 ,ALLOCATION_PERCENT               FILLER
 , RANK                             FILLER
 , SHIP_METHOD                      FILLER
 , SOURCE_ORGANIZATION_CODE           FILLER
, SOURCE_TYPE                      "TRIM(:SOURCE_TYPE)"
 , VENDOR_NUM                       "TRIM(:VENDOR_NUM)"
 , VENDOR_SITE_CODE                 "TRIM(:VENDOR_SITE_CODE)"
 , RECEIVING_ORG_INDEX              FILLER
 , SECONDARY_INVENTORY              FILLER
,PROCESS_STATUS                   "TRIM(:PROCESS_STATUS)"
 , ERROR_MESSAGE                    FILLER
 )
