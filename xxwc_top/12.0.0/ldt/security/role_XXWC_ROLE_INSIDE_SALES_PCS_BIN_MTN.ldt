# $Header$

# dbdrv: exec fnd bin FNDLOAD bin &phase=daa+54 checkfile:~PROD:~PATH:~FILE &ui_apps 0 Y UPLOAD @FND:patch/115/import/afrole.lct @~PROD:~PATH/~FILE
LANGUAGE = "US"
LDRCONFIG = "afrole.lct 120.2.12010000.3"

#Source Database ebizprd

#RELEASE_NAME 12.1.3

# -- Begin Entity Definitions -- 

DEFINE WF_ROLE
  KEY   ROLE_NAME                       VARCHAR2(320)
  TRANS DISPLAY_NAME                    VARCHAR2(360)
  TRANS DESCRIPTION                     VARCHAR2(1000)
  BASE  NOTIFICATION_PREFERENCE         VARCHAR2(8)
  BASE  LANGUAGE                        VARCHAR2(30)
  BASE  TERRITORY                       VARCHAR2(30)
  BASE  EMAIL_ADDRESS                   VARCHAR2(320)
  BASE  FAX                             VARCHAR2(240)
  BASE  ORIG_SYSTEM                     VARCHAR2(30)
  BASE  ORIG_SYSTEM_ID                  VARCHAR2(50)
  BASE  START_DATE                      VARCHAR2(10)
  BASE  STATUS                          VARCHAR2(8)
  BASE  EXPIRATION_DATE                 VARCHAR2(10)
  BASE  SEC_GROUP                       REFERENCES FND_SECURITY_GROUPS
  BASE  RESP                            REFERENCES FND_RESPONSIBILITY
  BASE  LAST_UPDATE_DATE                VARCHAR2(10)
  BASE  OWNER_TAG                       VARCHAR2(50)
  CTX   OWNER                           VARCHAR2(4000)

  DEFINE FND_LOOKUP_ASSIGNMENTS
    KEY   LOOKUP_TYPE                     VARCHAR2(30)
    KEY   LOOKUP_CODE                     VARCHAR2(30)
    BASE  OBJ_NAME                        VARCHAR2(30)
    BASE  INSTANCE_PK1_VALUE              REFERENCES WF_ROLE
    BASE  INSTANCE_PK2_VALUE              VARCHAR2(320)
    BASE  INSTANCE_PK3_VALUE              VARCHAR2(320)
    BASE  INSTANCE_PK4_VALUE              VARCHAR2(320)
    BASE  INSTANCE_PK5_VALUE              VARCHAR2(320)
    BASE  DISPLAY_SEQUENCE                VARCHAR2(50)
    CTX   OWNER                           VARCHAR2(4000)
    CTX   LAST_UPDATE_DATE                VARCHAR2(10)
  END FND_LOOKUP_ASSIGNMENTS

    DEFINE WF_ROLE_HIERARCHY
      KEY   SUPER_NAME                      VARCHAR2(320)
      KEY   SUB_NAME                        VARCHAR2(320)
      CTX   OWNER                           VARCHAR2(4000)
      CTX   LAST_UPDATE_DATE                VARCHAR2(10)
      BASE  ENABLED_FLAG                    VARCHAR2(1)
    END WF_ROLE_HIERARCHY

      DEFINE WF_USER_ROLE
        KEY   USER_NAME                       VARCHAR2(320)
        KEY   ROLE_NAME                       VARCHAR2(320)
        BASE  USER_ORIG_SYSTEM                VARCHAR2(30)
        BASE  USER_ORIG_SYSTEM_ID             VARCHAR2(50)
        BASE  ROLE_ORIG_SYSTEM                VARCHAR2(30)
        BASE  ROLE_ORIG_SYSTEM_ID             VARCHAR2(50)
        BASE  START_DATE                      VARCHAR2(10)
        BASE  EXPIRATION_DATE                 VARCHAR2(10)
        CTX   OWNER                           VARCHAR2(4000)
        CTX   LAST_UPDATE_DATE                VARCHAR2(10)
      END WF_USER_ROLE

        DEFINE GRANT
          KEY   GRANT_GUID                      VARCHAR2(32)
          BASE  GRANTEE_TYPE                    VARCHAR2(8)
          BASE  GNT_MENU_NAME                   VARCHAR2(30)
          BASE  START_DATE                      VARCHAR2(11)
          BASE  END_DATE                        VARCHAR2(11)
          BASE  GNT_OBJ_NAME                    VARCHAR2(30)
          BASE  INSTANCE_TYPE                   VARCHAR2(8)
          BASE  GNT_ISN                         VARCHAR2(30)
          BASE  INSTANCE_PK1_VALUE              VARCHAR2(256)
          BASE  INSTANCE_PK2_VALUE              VARCHAR2(256)
          BASE  INSTANCE_PK3_VALUE              VARCHAR2(256)
          BASE  INSTANCE_PK4_VALUE              VARCHAR2(256)
          BASE  INSTANCE_PK5_VALUE              VARCHAR2(256)
          BASE  PARAMETER1                      VARCHAR2(256)
          BASE  PARAMETER2                      VARCHAR2(256)
          BASE  PARAMETER3                      VARCHAR2(256)
          BASE  PARAMETER4                      VARCHAR2(256)
          BASE  PARAMETER5                      VARCHAR2(256)
          BASE  PARAMETER6                      VARCHAR2(256)
          BASE  PARAMETER7                      VARCHAR2(256)
          BASE  PARAMETER8                      VARCHAR2(256)
          BASE  PARAMETER9                      VARCHAR2(256)
          BASE  PARAMETER10                     VARCHAR2(256)
          BASE  CTX_SECURITY_GROUP_KEY          REFERENCES FND_SECURITY_GROUPS
          BASE  CTX_RSP                         REFERENCES FND_RESPONSIBILITY
          BASE  CTX_ORGANIZATION                VARCHAR2(60)
          BASE  PROGRAM_NAME                    VARCHAR2(30)
          BASE  PROGRAM_TAG                     VARCHAR2(30)
          BASE  LAST_UPDATE_DATE                VARCHAR2(10)
          BASE  NAME                            VARCHAR2(80)
          BASE  DESCRIPTION                     VARCHAR2(240)
          CTX   OWNER                           VARCHAR2(4000)
        END GRANT
END WF_ROLE

DEFINE FND_LOOKUP_VALUE
  KEY   LOOKUP_TYPE                     VARCHAR2(30)
  KEY   LOOKUP_CODE                     VARCHAR2(30)
  BASE  VIEW_APPSNAME                   VARCHAR2(50)
  BASE  ENABLED_FLAG                    VARCHAR2(1)
  BASE  START_DATE_ACTIVE               VARCHAR2(10)
  BASE  END_DATE_ACTIVE                 VARCHAR2(10)
  BASE  TERRITORY_CODE                  VARCHAR2(2)
  BASE  TAG                             VARCHAR2(150)
  BASE  ATTRIBUTE_CATEGORY              VARCHAR2(30)
  BASE  ATTRIBUTE1                      VARCHAR2(150)
  BASE  ATTRIBUTE2                      VARCHAR2(150)
  BASE  ATTRIBUTE3                      VARCHAR2(150)
  BASE  ATTRIBUTE4                      VARCHAR2(150)
  BASE  ATTRIBUTE5                      VARCHAR2(150)
  BASE  ATTRIBUTE6                      VARCHAR2(150)
  BASE  ATTRIBUTE7                      VARCHAR2(150)
  BASE  ATTRIBUTE8                      VARCHAR2(150)
  BASE  ATTRIBUTE9                      VARCHAR2(150)
  BASE  ATTRIBUTE10                     VARCHAR2(150)
  BASE  ATTRIBUTE11                     VARCHAR2(150)
  BASE  ATTRIBUTE12                     VARCHAR2(150)
  BASE  ATTRIBUTE13                     VARCHAR2(150)
  BASE  ATTRIBUTE14                     VARCHAR2(150)
  BASE  ATTRIBUTE15                     VARCHAR2(150)
  CTX   OWNER                           VARCHAR2(4000)
  BASE  LAST_UPDATE_DATE                VARCHAR2(10)
  TRANS MEANING                         VARCHAR2(80)
  TRANS DESCRIPTION                     VARCHAR2(240)
END FND_LOOKUP_VALUE

DEFINE FND_SECURITY_GROUPS
  KEY   SECURITY_GROUP_KEY              VARCHAR2(30)
  CTX   OWNER                           VARCHAR2(4000)
  BASE  LAST_UPDATE_DATE                VARCHAR2(10)
  TRANS SECURITY_GROUP_NAME             VARCHAR2(80)
  TRANS DESCRIPTION                     VARCHAR2(240)
END FND_SECURITY_GROUPS

DEFINE FND_RESPONSIBILITY
  KEY   APPLICATION_SHORT_NAME          VARCHAR2(50)
  KEY   RESP_KEY                        VARCHAR2(30)
  BASE  RESPONSIBILITY_ID               VARCHAR2(50)
  TRANS RESPONSIBILITY_NAME             VARCHAR2(100)
  CTX   OWNER                           VARCHAR2(4000)
  BASE  LAST_UPDATE_DATE                VARCHAR2(10)
  BASE  DATA_GROUP_APP_SHORT_NAME       VARCHAR2(50)
  BASE  DATA_GROUP_NAME                 VARCHAR2(50)
  BASE  MENU_NAME                       VARCHAR2(80)
  BASE  START_DATE                      VARCHAR2(11)
  BASE  END_DATE                        VARCHAR2(11)
  TRANS DESCRIPTION                     VARCHAR2(240)
  BASE  GROUP_APP_SHORT_NAME            VARCHAR2(50)
  BASE  REQUEST_GROUP_NAME              VARCHAR2(30)
  BASE  VERSION                         VARCHAR2(1)
  BASE  WEB_HOST_NAME                   VARCHAR2(80)
  BASE  WEB_AGENT_NAME                  VARCHAR2(80)
END FND_RESPONSIBILITY

# -- End Entity Definitions -- 


BEGIN WF_ROLE "ZZZ_LAST_DUMMY_ROLE"
  DISPLAY_NAME = "ZZZ_LAST_DUMMY_ROLE"
  DESCRIPTION =
 "This is a fake role for post UPLOAD processing only. Not in table."
  NOTIFICATION_PREFERENCE = "MAILHTML"
  LANGUAGE = "US"
  TERRITORY = "AMERICA"
  ORIG_SYSTEM = "WF_LOCAL_ROLES"
  ORIG_SYSTEM_ID = "0"
  START_DATE = "1951/01/01"
  EXPIRATION_DATE = "2015/10/15"
  SEC_GROUP = ""
  RESP = "" ""
  LAST_UPDATE_DATE = "2015/10/16"
  OWNER_TAG = "AFROLE.LCT"
  OWNER = "ORACLE"




END WF_ROLE

BEGIN FND_LOOKUP_VALUE "UMX_CATEGORY_LOOKUP" "MISC"
  VIEW_APPSNAME = "FND"
  ENABLED_FLAG = "Y"
  START_DATE_ACTIVE = "2003/12/31"
  OWNER = "ORACLE12.1.3"
  LAST_UPDATE_DATE = "2004/04/23"
  MEANING = "Miscellaneous"
  DESCRIPTION = "Miscellaneous"
END FND_LOOKUP_VALUE

BEGIN WF_ROLE "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
  DISPLAY_NAME = "WC Role - All User EIS Modules"
  DESCRIPTION =
 "This role provides access to all EiS reporting modules. The user will still need to be assigned another role that provides a responsibility \"allowed\" to view specific EiS reports."
  NOTIFICATION_PREFERENCE = "QUERY"
  LANGUAGE = "AMERICAN"
  TERRITORY = "AMERICA"
  ORIG_SYSTEM = "UMX"
  ORIG_SYSTEM_ID = "0"
  START_DATE = "2012/06/06"
  STATUS = "ACTIVE"
  SEC_GROUP = ""
  RESP = "" ""
  LAST_UPDATE_DATE = "2012/06/06"
  OWNER_TAG = "XXWC"
  OWNER = "APPSMGR"



  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_PUBLISHED_REPORT|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/06"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_OIE_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_TM_USER_REBATES|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_PURCHASING_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/06"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_GL_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/06"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_WIP_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_CE_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/06"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_BOM_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/06"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_REPORTING_ADMIN|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY
 "FND_RESP|XXEIS|XXEIS_RS_RECEIVABLES_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/06"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_ADMIN_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_INV_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/06"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_OM_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/06"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_ASSETS_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/06"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_PN_MANGER|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_ASCP_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_PAYABLES_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/06"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  

  BEGIN FND_LOOKUP_ASSIGNMENTS "UMX_CATEGORY_LOOKUP" "MISC"
    OBJ_NAME = "UMX_ACCESS_ROLE"
    INSTANCE_PK1_VALUE = "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
    OWNER = "10011985"
    LAST_UPDATE_DATE = "2012/06/06"
  END FND_LOOKUP_ASSIGNMENTS
  
END WF_ROLE

BEGIN FND_LOOKUP_VALUE "UMX_CATEGORY_LOOKUP" "SALES_OPS_FIELD"
  VIEW_APPSNAME = "FND"
  ENABLED_FLAG = "Y"
  START_DATE_ACTIVE = "2012/03/20"
  OWNER = "10011985"
  LAST_UPDATE_DATE = "2012/03/20"
  MEANING = "WC Sales & Ops, Field"
  DESCRIPTION = "White Cap Sales & Ops, Field"
END FND_LOOKUP_VALUE

BEGIN WF_ROLE "UMX|XXWC_ROLE_ASSOCIATE"
  DISPLAY_NAME = "WC Role - Associate"
  DESCRIPTION =
 "XXWC_ROLE_ASSOCIATE \n\
Base role for all WC associates. This role is assigned to all security roles, not directly to associates."
  NOTIFICATION_PREFERENCE = "MAILHTML"
  LANGUAGE = "AMERICAN"
  TERRITORY = "AMERICA"
  ORIG_SYSTEM = "UMX"
  ORIG_SYSTEM_ID = "0"
  START_DATE = "2012/04/11"
  STATUS = "ACTIVE"
  SEC_GROUP = ""
  RESP = "" ""
  LAST_UPDATE_DATE = "2013/08/02"
  OWNER_TAG = "XXWC"
  OWNER = "10011289"

  BEGIN GRANT "07595BE60160E0D3E050E00BFC15FE4F"
    GRANTEE_TYPE = "GROUP"
    GNT_MENU_NAME = "FND_CP_REQ_OPS"
    START_DATE = "2014/11/08"
    GNT_OBJ_NAME = "FND_CONCURRENT_REQUESTS"
    INSTANCE_TYPE = "SET"
    GNT_ISN = "WC_DMS_SIGNATURE_ACCESS"
    INSTANCE_PK1_VALUE = "*NULL*"
    INSTANCE_PK2_VALUE = "*NULL*"
    INSTANCE_PK3_VALUE = "*NULL*"
    INSTANCE_PK4_VALUE = "*NULL*"
    INSTANCE_PK5_VALUE = "*NULL*"
    CTX_SECURITY_GROUP_KEY = "*GLOBAL*"
    CTX_RSP = "*GLOBAL*" "*GLOBAL*"
    CTX_ORGANIZATION = "*GLOBAL*"
    PROGRAM_NAME = "GRANTS_UI"
    LAST_UPDATE_DATE = "2014/11/08"
    NAME = "WC Delivery Management System concurrent request set access"
    DESCRIPTION = "Ability to query requests which are signature captured"
    OWNER = "RV003897"
  END GRANT
  


  BEGIN WF_ROLE_HIERARCHY "FND_RESP|INV|XXWC_MOV_STOCKING|STANDARD"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "10011985"
    LAST_UPDATE_DATE = "2015/05/22"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|PO|XXWC_PURCHASING_INQUIRY|STANDARD"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "10011985"
    LAST_UPDATE_DATE = "2013/01/25"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "UMX|XXWC_ROLE_ALL_USER_EIS_MODULES"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "10011289"
    LAST_UPDATE_DATE = "2012/06/07"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY
 "FND_RESP|XXEIS|XXEIS_RS_RECEIVABLES_REPORTS|STANDARD"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|CS|XXWC_KNOWLEDGE_BASE|STANDARD"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "10011985"
    LAST_UPDATE_DATE = "2013/10/15"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|CSD|XXWC_DEPOT_REPAIR_WC|STANDARD"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|INV|XXWC_AO_BIN_MTN_PO_RPT|STANDARD"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "10011985"
    LAST_UPDATE_DATE = "2013/04/16"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|AR|ARI_INTERNAL|STANDARD"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXWC|XXWC_RESOURCES|STANDARD"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/06/09"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|AR|XXWC_AR_IRECEIVABLES|STANDARD"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "10011289"
    LAST_UPDATE_DATE = "2012/06/07"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|XXEIS|XXEIS_RS_PUBLISHED_REPORT|STANDARD"
 "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/07/14"
    ENABLED_FLAG = "N"
  END WF_ROLE_HIERARCHY
  

  BEGIN FND_LOOKUP_ASSIGNMENTS "UMX_CATEGORY_LOOKUP" "SALES_OPS_FIELD"
    OBJ_NAME = "UMX_ACCESS_ROLE"
    INSTANCE_PK1_VALUE = "UMX|XXWC_ROLE_ASSOCIATE"
    OWNER = "10011289"
    LAST_UPDATE_DATE = "2012/06/07"
  END FND_LOOKUP_ASSIGNMENTS
  
END WF_ROLE

BEGIN FND_LOOKUP_VALUE "UMX_CATEGORY_LOOKUP" "WC_SALES_BRANCH"
  VIEW_APPSNAME = "FND"
  ENABLED_FLAG = "Y"
  START_DATE_ACTIVE = "2011/09/20"
  OWNER = "10011289"
  LAST_UPDATE_DATE = "2011/09/20"
  MEANING = "WC Sales, Branch"
  DESCRIPTION = "White Cap Branch Sales Roles"
END FND_LOOKUP_VALUE

BEGIN WF_ROLE "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
  DISPLAY_NAME = "WC Role - Inside Sales (OM Pricing Limited, PO, Cycle Count)"
  DESCRIPTION = "Approval required from Branch Manager"
  NOTIFICATION_PREFERENCE = "MAILHTML"
  LANGUAGE = "AMERICAN"
  TERRITORY = "AMERICA"
  ORIG_SYSTEM = "UMX"
  ORIG_SYSTEM_ID = "0"
  START_DATE = "2011/12/14"
  STATUS = "ACTIVE"
  SEC_GROUP = ""
  RESP = "" ""
  LAST_UPDATE_DATE = "2014/02/07"
  OWNER_TAG = "XXWC"
  OWNER = "10011985"



  BEGIN WF_ROLE_HIERARCHY "FND_RESP|INV|XXWC_MOV_INV_WC|STANDARD"
 "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
    OWNER = "10011985"
    LAST_UPDATE_DATE = "2015/05/22"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY
 "FND_RESP|SQLAP|XXWC_PAY_VENDOR_MSTR_INQUIRY|STANDARD"
 "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
    OWNER = "10011985"
    LAST_UPDATE_DATE = "2013/08/16"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|INV|XXWC_AO_CYCLE_COUNT|STANDARD"
 "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
    OWNER = "10011985"
    LAST_UPDATE_DATE = "2013/06/10"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD"
 "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
    OWNER = "10011289"
    LAST_UPDATE_DATE = "2012/07/26"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|PO|XXWC_AO_PO_AUTOCREATE|STANDARD"
 "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/05/07"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|INV|XXWC_AO_BIN_MTN|STANDARD"
 "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/05/07"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|ONT|XXWC_ORDER_MGMT_PRICING_LTD|STANDARD"
 "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/05/07"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "UMX|XXWC_ROLE_ASSOCIATE"
 "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/05/07"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  
  BEGIN WF_ROLE_HIERARCHY "FND_RESP|CSD|XXWC_DEPOT_REPAIR_WC|STANDARD"
 "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
    OWNER = "APPSMGR"
    LAST_UPDATE_DATE = "2012/05/19"
    ENABLED_FLAG = "Y"
  END WF_ROLE_HIERARCHY
  

  BEGIN FND_LOOKUP_ASSIGNMENTS "UMX_CATEGORY_LOOKUP" "WC_SALES_BRANCH"
    OBJ_NAME = "UMX_ACCESS_ROLE"
    INSTANCE_PK1_VALUE = "UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN"
    OWNER = "10011985"
    LAST_UPDATE_DATE = "2011/12/14"
  END FND_LOOKUP_ASSIGNMENTS
  
END WF_ROLE

