--Report Name            : Bin Location With Sales History
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator... 
prompt Creating View Data for Bin Location With Sales History
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_BIN_LOC_SALES_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_BIN_LOC_SALES_V',401,'','','','','SA059956','XXEIS','Eis Rs Xxwc Inv Bin Loc V','EXIBLV','','');
--Delete View Columns for EIS_XXWC_INV_BIN_LOC_SALES_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_BIN_LOC_SALES_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_BIN_LOC_SALES_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','YTD_SALES',401,'Ytd Sales','YTD_SALES','','','','SA059956','NUMBER','DERIVED COLUMN','DERIVED COLUMN','Ytd Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','MTD_SALES',401,'Mtd Sales','MTD_SALES','','','','SA059956','NUMBER','DERIVED COLUMN','DERIVED COLUMN','Mtd Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','ONHAND',401,'Onhand','ONHAND','','','','SA059956','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','TRANSACTION_QUANTITY','Onhand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','','','SA059956','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','MAX_MINMAX_QUANTITY',401,'Max Minmax Quantity','MAX_MINMAX_QUANTITY','','','','SA059956','NUMBER','MTL_SYSTEM_ITEMS_KFV','MAX_MINMAX_QUANTITY','Max Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','MIN_MINMAX_QUANTITY',401,'Min Minmax Quantity','MIN_MINMAX_QUANTITY','','','','SA059956','NUMBER','MTL_SYSTEM_ITEMS_KFV','MIN_MINMAX_QUANTITY','Min Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','UOM',401,'Uom','UOM','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','PRIMARY_UOM_CODE','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','DESCRIPTION','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','SEGMENT1','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','ORGANIZATION',401,'Organization','ORGANIZATION','','','','SA059956','VARCHAR2','','','Organization','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','ALTERNATE_BIN_LOC',401,'Alternate Bin Loc','ALTERNATE_BIN_LOC','','','','SA059956','VARCHAR2','','','Alternate Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','BIN_LOC',401,'Bin Loc','BIN_LOC','','','','SA059956','VARCHAR2','','','Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','LOCATION',401,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','PRIMARY_BIN_LOC',401,'Primary Bin Loc','PRIMARY_BIN_LOC','','','','SA059956','VARCHAR2','','','Primary Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','OPEN_DEMAND',401,'Open Demand','OPEN_DEMAND','','','','SA059956','CHAR','','','Open Demand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','OPEN_ORDER',401,'Open Order','OPEN_ORDER','','','','SA059956','CHAR','','','Open Order','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','STK',401,'Stk','STK','','','','SA059956','CHAR','','','Stk','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','CATEGORY',401,'Category','CATEGORY','','','','SA059956','VARCHAR2','','','Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','CATEGORY_SET_NAME',401,'Category Set Name','CATEGORY_SET_NAME','','','','SA059956','VARCHAR2','','','Category Set Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','LAST_RECEIVED_DATE',401,'Last Received Date','LAST_RECEIVED_DATE','','','','SA059956','DATE','','','Last Received Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','WEIGHT',401,'Weight','WEIGHT','','','','SA059956','NUMBER','MTL_SYSTEM_ITEMS_KFV','UNIT_WEIGHT','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','SA059956','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','','','SA059956','NUMBER','MTL_SYSTEM_ITEMS_KFV','LIST_PRICE_PER_UNIT','Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','CAT',401,'Cat','CAT','','','','SA059956','VARCHAR2','MTL_CATEGORIES','SEGMENT2','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','SA059956','NUMBER','MTL_SYSTEM_ITEMS_KFV','INVENTORY_ITEM_ID','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','SA059956','NUMBER','MTL_SYSTEM_ITEMS_KFV','ORGANIZATION_ID','Inv Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','SA059956','NUMBER','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_ID','Org Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','APPLICATION_ID',401,'Application Id','APPLICATION_ID','','','','SA059956','NUMBER','GL_PERIOD_STATUSES','APPLICATION_ID','Application Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','SET_OF_BOOKS_ID',401,'Set Of Books Id','SET_OF_BOOKS_ID','','','','SA059956','NUMBER','GL_PERIOD_STATUSES','SET_OF_BOOKS_ID','Set Of Books Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_SALES_V','PROCESS_ID',401,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_INV_BIN_LOC_SALES_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_SALES_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_KFV','MSI','MSI','SA059956','SA059956','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_SALES_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','SA059956','SA059956','-1','Categories','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_SALES_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','SA059956','SA059956','-1','Calendar Period Statuses','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_SALES_V','GL_CODE_COMBINATIONS_KFV',401,'GL_CODE_COMBINATIONS','GCC','GCC','SA059956','SA059956','-1','GL Code Combinations','','','','');
--Inserting View Component Joins for EIS_XXWC_INV_BIN_LOC_SALES_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_SALES_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIBLV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_SALES_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIBLV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_SALES_V','MTL_CATEGORIES','MCV',401,'EXIBLV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_SALES_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_SALES_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_SALES_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_SALES_V','GL_CODE_COMBINATIONS_KFV','GCC',401,'EXIBLV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','','SA059956','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Bin Location With Sales History
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Bin Location With Sales History
xxeis.eis_rs_ins.lov( 401,'SELECT   category_set_name,
         description
    FROM mtl_category_sets
   WHERE mult_item_cat_assign_flag = ''N''
ORDER BY category_set_name','','EIS_INV_CATEGORY_SETS_LOV','Category Sets','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments Item_Category,
                description
           FROM mtl_categories_kfv
           order by concatenated_segments','','EIS_INV_ITEM_CATEGORIES_LOV','Item Categories','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments Item_Category,
                description
           FROM mtl_categories_kfv
           order by concatenated_segments','','EIS_INV_ITEM_CATEGORIES_LOV','Item Categories','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT distinct substr(SEGMENT1,3) bin_location FROM MTL_ITEM_LOCATIONS_KFV','','INV Bin Location','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT distinct substr(SEGMENT1,3) bin_location FROM MTL_ITEM_LOCATIONS_KFV','','INV Bin Location','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','XXWC INV ORGANIZATIONS LOV','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Bin Location With Sales History
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Bin Location With Sales History
xxeis.eis_rs_utility.delete_report_rows( 'Bin Location With Sales History' );
--Inserting Report - Bin Location With Sales History
xxeis.eis_rs_ins.r( 401,'Bin Location With Sales History','','Items without Primary Bin Locations Assigned','','','','SA059956','EIS_XXWC_INV_BIN_LOC_SALES_V','Y','','','SA059956','','N','White Cap Reports','','Pivot Excel,EXCEL,','');
--Inserting Report Columns - Bin Location With Sales History
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'ALTERNATE_BIN_LOC','Alternate Bin Loc','Alternate Bin Loc','','','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'EXTENDED VALUE','Extended Value','Alternate Bin Loc','NUMBER','~T~D~2','default','','11','Y','','','','','','','(EXIBLV.ONHAND*EXIBLV.AVERAGECOST)','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'LOCATION','Location','Location','','','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'PRIMARY_BIN_LOC','Primary Bin','Primary Bin Loc','','','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'PRIMARY BIN','Primary Loc Exists','Primary Bin Loc','NUMBER','~~~','default','','19','Y','','','','','','','decode(EXIBLV.primary_bin_loc,null,''N'',''Y'')','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'OPEN_DEMAND','Open Demand','Open Demand','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'AVERAGECOST','Average Cost','Averagecost','','~T~D~2','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'OPEN_ORDER','Open Order','Open Order','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'STK','Stk','Stk','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'CATEGORY','Category','Category','','','','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'CATEGORY_SET_NAME','Category Set Name','Category Set Name','','','','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'LAST_RECEIVED_DATE','Last Received Date','Last Received Date','','','','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'MAX_MINMAX_QUANTITY','Max','Max Minmax Quantity','','~~~','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'MIN_MINMAX_QUANTITY','Min','Min Minmax Quantity','','~~~','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'MTD_SALES','MTD Units','Mtd Sales','','~T~D~2','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'ONHAND','Qty Onhand','Onhand','','~~~','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'PART_NUMBER','Item Number','Part Number','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'UOM','UOM','Uom','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'YTD_SALES','YTD Units','Ytd Sales','','~T~D~2','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'VENDOR NUMBER-NAME','Vendor Number-Name','Ytd Sales','VARCHAR2','','default','','4','Y','','','','','','','(EXIBLV.VENDOR_NUMBER||''-''||EXIBLV.VENDOR_NAME)','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location With Sales History',401,'ORGANIZATION','Organization','Organization','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_BIN_LOC_SALES_V','','');
--Inserting Report Parameters - Bin Location With Sales History
xxeis.eis_rs_ins.rp( 'Bin Location With Sales History',401,'Organization','Organization','ORGANIZATION','IN','XXWC INV ORGANIZATIONS LOV','','VARCHAR2','N','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location With Sales History',401,'Starting Bin Location','Starting Bin Location','BIN_LOC','>=','INV Bin Location','','VARCHAR2','N','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location With Sales History',401,'Ending Bin Location','Ending Bin Location','BIN_LOC','<=','INV Bin Location','','VARCHAR2','N','Y','3','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location With Sales History',401,'Category Set','Category Set','CATEGORY_SET_NAME','IN','EIS_INV_CATEGORY_SETS_LOV','SELECT category_set_name FROM mtl_category_sets a,mtl_default_category_sets b WHERE b.functional_area_id = 5 AND a.category_set_id = b.category_set_id','VARCHAR2','N','Y','4','','N','SQL','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location With Sales History',401,'Category From','Category From','CATEGORY','>=','EIS_INV_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','5','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location With Sales History',401,'Category To','Category To','CATEGORY','<=','EIS_INV_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','6','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Bin Location With Sales History
xxeis.eis_rs_ins.rcn( 'Bin Location With Sales History',401,'','','','','AND PROCESS_ID  =:SYSTEM.PROCESS_ID','Y','1','','SA059956');
--Inserting Report Sorts - Bin Location With Sales History
xxeis.eis_rs_ins.rs( 'Bin Location With Sales History',401,'PART_NUMBER','ASC','SA059956','','');
--Inserting Report Triggers - Bin Location With Sales History
xxeis.eis_rs_ins.rt( 'Bin Location With Sales History',401,'begin

xxeis.EIS_XXWC_BIN_LOCA_sales_PKG.g_start_bin := :Starting Bin Location;
xxeis.EIS_XXWC_BIN_LOCA_sales_PKG.g_end_bin  := :Ending Bin Location;
xxeis.EIS_XXWC_BIN_LOCA_sales_PKG.get_bin_location_info (p_process_id=>:SYSTEM.PROCESS_ID,
                                                                                         p_category_from  =>:Category From,
                                                                                         p_category_to     =>:Category To,
                                                                                         p_organization     =>:Organization ,
                                                                                         p_category_set =>:Category Set);
end;','B','Y','SA059956');
xxeis.eis_rs_ins.rt( 'Bin Location With Sales History',401,'begin
xxeis.EIS_XXWC_BIN_LOCA_sales_PKG.CLEAR_TEMP_TABLES(p_process_id=>:SYSTEM.PROCESS_ID);
end;','A','Y','SA059956');
--Inserting Report Templates - Bin Location With Sales History
--Inserting Report Portals - Bin Location With Sales History
--Inserting Report Dashboards - Bin Location With Sales History
--Inserting Report Security - Bin Location With Sales History
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','20005','','50900',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50884',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50855',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50981',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50882',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50883',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','51004',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50619',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50851',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50867',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50849',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50848',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50866',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50864',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50863',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50862',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50846',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50865',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50845',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Bin Location With Sales History','401','','50844',401,'SA059956','','');
--Inserting Report Pivots - Bin Location With Sales History
xxeis.eis_rs_ins.rpivot( 'Bin Location With Sales History',401,'Pivot','1','0,0|1,0,1','1,1,0,0|PivotStyleDark1|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location With Sales History',401,'Pivot','LOCATION','ROW_FIELD','','Location','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location With Sales History',401,'Pivot','ONHAND','DATA_FIELD','SUM','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location With Sales History',401,'Pivot','PART_NUMBER','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location With Sales History',401,'Pivot','PRIMARY BIN','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location With Sales History',401,'Pivot','STK','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location With Sales History',401,'Pivot','OPEN_DEMAND','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location With Sales History',401,'Pivot','PRIMARY_BIN_LOC','DATA_FIELD','COUNT','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location With Sales History',401,'Pivot','LAST_RECEIVED_DATE','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
