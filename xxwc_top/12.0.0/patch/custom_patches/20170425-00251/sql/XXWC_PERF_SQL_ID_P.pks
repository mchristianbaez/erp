create or replace package APPS.XXWC_PERF_SQL_ID_P as
  /*************************************************************************
       $Header XXWC_PERF_SQL_ID_P $
       Module Name: XXWC_PERF_SQL_ID_P
  
       REVISIONS:
       Ver        Date        Author                     Description
       ---------  ----------  ---------------         -------------------------
       1.0        05/02/2017  Nancy Pahwa            TMS#20170425-00251 -Function to debug Grid Info
  **************************************************************************/

  type t_result_rec is record(
    Inst_Id               number,
    SID                   number,
    concurrent_request_id NUMBER(15),
    USERNAME              varchar2(30),
    Client_Identifier     varchar2(64),
    Osuser                VARCHAR2(30),
    Status                VARCHAR2(8),
    --Logon_Time                   gv$session.LOGON_TIME%type,
    logon_time                   varchar2(50),
    spid_dbnode                  VARCHAR2(24),
    Machine                      VARCHAR2(64),
    Module                       VARCHAR2(64),
    Action                       VARCHAR2(64),
    Program                      VARCHAR2(48),
    Sql_Id                       VARCHAR2(13),
    Optimizer_Mode               VARCHAR2(10),
    Hash_Value                   NUMBER,
    Address                      RAW(8),
    Sql_Text                     VARCHAR2(1000),
    concurrent_program_name      VARCHAR2(30),
    user_concurrent_program_name VARCHAR2(240),
    argument_text                VARCHAR2(240));
  type t_result_tab is table of t_result_rec;
  type t_result_rec2 is record(
    request_id    number,
    phase_code    VARCHAR2(1),
    status_code   VARCHAR2(1),
    oracle_sid    NUMBER,
    serial#       NUMBER,
    osuser        VARCHAR2(30),
    process       VARCHAR2(24),
    os_process_id VARCHAR2(24));
  type t_result_tab2 is table of t_result_rec2;
  type t_result_rec3 is record(
    sid      NUMBER,
    sql_text VARCHAR2(1000));
  type t_result_tab3 is table of t_result_rec3;
  type t_result_rec4 is record(
    sql_text      VARCHAR2(1000),
    request_id    number,
    phase_code    VARCHAR2(1),
    status_code   VARCHAR2(1),
    oracle_sid    NUMBER,
    serial#       NUMBER,
    osuser        VARCHAR2(30),
    process       VARCHAR2(24),
    os_process_id VARCHAR2(24));
  type t_result_tab4 is table of t_result_rec4;
  -- search docx content
  function PERF_SQL_ID(p_sql_id varchar2 DEFAULT NULL) return t_result_tab
    pipelined;
  FUNCTION GET_CON_REQUEST_INFO(p_conc_request_id IN NUMBER DEFAULT NULL)
    return t_result_tab2
    pipelined;
  FUNCTION GET_SQL_INFO(p_oracle_sid IN NUMBER) return t_result_tab3
    pipelined;
  FUNCTION GET_CON_REQ_SQL_INFO(p_conc_request_id IN NUMBER)
    return t_result_tab4
    pipelined;
end;
/