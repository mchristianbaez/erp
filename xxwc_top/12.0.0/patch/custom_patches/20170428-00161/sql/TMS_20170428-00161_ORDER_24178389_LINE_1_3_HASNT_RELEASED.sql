/*************************************************************************
 $Header TMS_20170428-00161_AWAITING_INVOICE_TO_CLOSE.sql $
 Module Name: TMS_20170428-00161 Data Fix script for SO# 24178389

 PURPOSE: Data fix script for 24178389--No permanent fix in process (But patch available per oracle)

 REVISIONS:
 Ver        Date        Author              Description
 ---------  ----------- ------------------  --------------------------
 1.0        31-May-2017  Krishna            TMS_20170428-00161 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
 DBMS_OUTPUT.put_line ('TMS_20170428-00161 , Before Update');

UPDATE  APPS.OE_ORDER_LINES_ALL
SET     FLOW_STATUS_CODE='CANCELLED',
        CANCELLED_FLAG = 'Y',
        OPEN_FLAG = 'N'
WHERE   LINE_ID =94140593
AND     OPEN_FLAG='Y'
AND     CANCELLED_FLAG='N';

 DBMS_OUTPUT.put_line (
 'TMS:20170428-00161 -Sales order lines updated (Expected:1): '
 || SQL%ROWCOUNT);

 COMMIT;

 DBMS_OUTPUT.put_line ('TMS:20170428-00161  , End Update');
EXCEPTION
 WHEN OTHERS
 THEN
 ROLLBACK;
 DBMS_OUTPUT.put_line ('TMS: 20170428-00161 , Errors : ' || SQLERRM);
END;
/