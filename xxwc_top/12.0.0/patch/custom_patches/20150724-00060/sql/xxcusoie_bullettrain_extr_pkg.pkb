CREATE OR REPLACE PACKAGE BODY APPS.xxcusoie_bullettrain_extr_pkg IS
  /*************************************************************************************************
  
   File Name: xxcusoie_bullettrain_extr_pkg
  
   PROGRAM TYPE: PL/SQL Package spec and body
  
   PURPOSE: Procedures and functions for populating the Oracle T and  E Bullet Train Table --removed ampersands...
  
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     07/27/2011    Mani Kumar      Initial creation of the package
   1.1     02/03/2012    Kathy Poling    Change to function get_exp_rep_param_info
                                         exception for account changed to 658020
   1.2     04/11/2012    Kathy Poling    SR 132174 Change to query 5 to pull bill
                                         amount instead of transaction amount 
                                         SR 130248 change to stop transactions going 
                                         into query 5 they are on expense report but 
                                         no exp report dists lines  
   1.3     06/07/2012    Kathy Poling    SR 141622 adding column cc_trns_category from
                                         credit card transactions to bullet train tables
                                         and accrual history table  
   1.4     07/27/2012    Kathy Poling    Change for stop itemized parent record from
                                         being included in the bullet train table
                                         RFC 34757        
   2.0     10-15-2013    Luong Vu        Add transactions for Canada OIE, org_id 167 
                                         per Ivy also excluded transaction type 62
                                         fixed OOP transactions for the tax (11/14/14 KP) 
                                         RFC 42470 S272756
   3.0    02/19/2015    Maharajan 
                        Shunmugam        eRFC#42953 HDS OIE Bullet Train Extract Process Fails in 
                                         EBIZPRD due to space issues - no impact
   4.0    02/20/2015    Maharajan 
                        Shunmugam        eRFC#42953 HDS OIE Bullet Train Extract Process Fails in 
                                         EBIZPRD due to space issues - no impact     
   5.0    03/11/2015    Maharajan
                        Shunmugam        ESMS#567606 and eRFC# 43101 HDS OIE Bullet Train Extract Process Fails 
				         because of tablespace TEMP1 issue
   6.0    03/13/2015    Maharajan
                        Shunmugam        ESMS#281818 Remove Appxcmn delivery portion of T and E bullet train 
   7.0   07/28/2015  Balaguru Seshadri -20150724-00060 --No code changes, just replace all ampersand symbols with word "and".                        
**************************************************************************************************/
  l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  PROCEDURE load_iexp_int_table(p_errbuf  OUT VARCHAR2
                               ,p_retcode OUT NUMBER) IS
    --Intialize Variables
    l_sec               VARCHAR2(200);
    l_procedure_name    VARCHAR2(75) := 'xxcusoie_bullettrain_extr_pkg.load_iexp_int_table';
    l_req_id            NUMBER := fnd_global.conc_request_id;
    l_lob_value_set_id  fnd_flex_value_sets.flex_value_set_id%TYPE;
    l_loc_value_set_id  fnd_flex_value_sets.flex_value_set_id%TYPE;
    l_cc_value_set_id   fnd_flex_value_sets.flex_value_set_id%TYPE;
    l_acc_value_set_id  fnd_flex_value_sets.flex_value_set_id%TYPE;
    l_proj_value_set_id fnd_flex_value_sets.flex_value_set_id%TYPE;
    
--Added below by Maha for version# 3.0 <<Start
    TYPE l_xxcus_bullet IS TABLE OF xxcus.xxcus_bullet_iexp_tbl%ROWTYPE INDEX BY BINARY_INTEGER;
    l_rec l_xxcus_bullet;

    CURSOR bullet_train_c1 IS
    SELECT (SELECT period_name
                 FROM gl.gl_periods u
                WHERE u.period_set_name = '4-4-QTR'
                  AND start_date <= aia.gl_date
                  AND end_date >= aia.gl_date
                  AND period_num != 13) period_name
             ,(SELECT period_name
                 FROM gl.gl_periods u
                WHERE u.period_set_name = '4-4-QTR'
                  AND start_date <= apcct.posted_date
                  AND end_date >= apcct.posted_date
                  AND period_num != 13) cc_fiscal_period
             ,acct_struc.gcc_seg1 oracle_product
             ,acct_struc.prod_descr product_descr
             ,acct_struc.gcc_seg2 oracle_location
             ,acct_struc.loc_descr location_descr
             ,acct_struc.gcc_seg3 oracle_cost_center
             ,acct_struc.cc_descr cost_center_descr
             ,acct_struc.gcc_seg4 oracle_account
             ,acct_struc.acc_descr account_descr
             ,acct_struc.gcc_seg1 || '.' || acct_struc.gcc_seg2 || '.' ||
              acct_struc.gcc_seg3 || '.' || acct_struc.gcc_seg4 oracle_accounts
             ,(CASE
                WHEN (aerl.item_description IS NULL AND
                     ((nvl(apcct.sic_code, 0) = 0) OR codes.meaning IS NULL)) THEN
                 'NOT MAPPED'
                ELSE
                 nvl(TRIM(aerl.item_description), 'NO DESCRIPTION')
              END) item_description
             ,(SELECT DISTINCT flv.tag category
                 FROM applsys.fnd_lookup_values flv
                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
                  AND flv.meaning = (CASE
                        WHEN (aerl.item_description IS NULL AND
                             ((nvl(apcct.sic_code, 0) = 0) OR
                             codes.meaning IS NULL)) THEN
                         'NOT MAPPED'
                        ELSE
                         nvl(aerl.item_description, 'NOT MAPPED')
                      END)
                  AND rownum < 2) category
             ,(SELECT DISTINCT flv.description sub_category
                 FROM applsys.fnd_lookup_values flv
                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
                  AND flv.meaning = (CASE
                        WHEN (aerl.item_description IS NULL AND
                             ((nvl(apcct.sic_code, 0) = 0) OR
                             codes.meaning IS NULL)) THEN
                         'NOT MAPPED'
                        ELSE
                         nvl(aerl.item_description, 'NOT MAPPED')
                      END)
                  AND rownum < 2) sub_category
             ,(CASE
                WHEN aerl.item_description = 'Personal' THEN
                 (nvl(aerd.amount, aerl.amount) * -1) --added NVL 09112012
                ELSE
                --nvl(aerd.amount, apcct.billed_amount)  --09112012
                 nvl(aerd.amount, aerl.amount)
              END) line_amount
             ,aerl.currency_code currency_code
             ,aerh.invoice_num INVOICE_NUMBER
             ,aerl.distribution_line_number || '.' ||
              (aerd.sequence_num + 1) distribution_line_number
             ,pep.full_name
             ,pep.employee_number
             ,pep.segment1 emp_default_prod
             ,pep.segment2 emp_default_loc
             ,pep.segment3 emp_default_costctr
             ,pep.fru
             ,aerh.creation_date
             ,aerh.report_submitted_date
             ,aerh.description
             ,aerl.merchant_name vendor_name
             ,apcct.merchant_name1 MERCHANT_NAME
             ,NULL business_purpose
             ,aerl.justification
             ,aerl.start_expense_date
             ,aerl.end_expense_date
             ,NULL remarks
             ,aerl.attendees
             ,NULL attendees_emp
             ,NULL attendees_te
             ,NULL attendees_cti1
             ,NULL attendees_cti2
             ,apcct.trx_id CREDIT_CARD_TRX_ID
             ,xcv.chname cardmember_name --aca.cardmember_name,    kp  
             ,apcct.transaction_date
             ,apcct.posted_date POSTED_DATE--trans_post_date
             ,apcct.merchant_city || ', ' || apcct.merchant_province_state merchant_location
             ,aerl.destination_from
             ,aerl.destination_to
             ,aerl.distance_unit_code
             ,aerl.daily_distance 
             ,aerl.trip_distance
             ,aerl.avg_mileage_rate
             ,aia.gl_date dt_sentto_gl
             ,aerh.report_header_id expense_report_number
             ,xcv.card_program_id --acpa.card_program_id,    kp
             ,xcv.card_program_name --  acpa.card_program_name,    kp
             ,nvl(apcct.sic_code, 0) mcc_code_no
             ,(CASE
                WHEN ((nvl(apcct.sic_code, 0) = 0) OR codes.meaning IS NULL) THEN
                 'NOT MAPPED'
                ELSE
                 codes.meaning
              END) mcc_code
             ,aia.gl_date accounting_date
             ,aerh.total report_total
             ,aerl.daily_distance MILES
             ,aerl.avg_mileage_rate RATE_PER_MILE
             ,aerl.attribute_category
             ,aerl.category_code
             ,apcct.transaction_amount
             ,aerl.receipt_currency_amount
             ,aerh.amt_due_ccard_company
             ,aerh.amt_due_employee
             ,decode(aerh.source, 'SelfService', 'Y', 'N') imported_to_ap
             ,(CASE
                WHEN aia.gl_date IS NOT NULL THEN
                 'Y'
                ELSE
                 'N'
              END) imported_to_gl
             ,nvl(decode(aerh.workflow_approved_flag
                        ,'A'
                        ,'Y'
                        ,'Y'
                        ,'Y'
                        ,'M'
                        ,'Y'
                        ,'N')
                 ,'N') workflow_approved_flag
             ,aerh.expense_current_approver_id
             ,aerh.override_approver_id approver_id
             ,aerh.override_approver_name approver_name
             ,aerp.report_type
             ,aerh.advance_invoice_to_apply
             ,aerh.prepay_dist_num advance_distribution_number
             ,nvl(aerh.prepay_apply_flag, 'N') advance_flag
             ,aerh.prepay_gl_date advance_gl_date
             ,aerh.prepay_num advance_number
             ,aerh.reject_code report_reject_code
             ,nvl(decode(aerh.workflow_approved_flag
                        ,'A'
                        ,'Y'
                        ,'Y'
                        ,'Y'
                        ,'M'
                        ,'Y'
                        ,'N')
                 ,'N') || (CASE
                             WHEN aia.gl_date IS NOT NULL THEN
                              'Y'
                             ELSE
                              'N'
                           END) app_post_flag
             ,decode(nvl(decode(aerh.workflow_approved_flag
                               ,'A'
                               ,'Y'
                               ,'Y'
                               ,'Y'
                               ,'M'
                               ,'Y'
                               ,'N')
                        ,'N') || (CASE
                                    WHEN aia.gl_date IS NOT NULL THEN
                                     'Y'
                                    ELSE
                                     'N'
                                  END)
                    ,'YY'
                    ,'Y'
                    ,'NN'
                    ,'N'
                    ,'N') approved_in_gl
             ,aerh.vouchno
             ,'1' QUERY_NUM--query_no 
             ,'1 - Processed Transaction in AP/GL' query_descr
             ,aerh.expense_status_code expense_report_status
             ,apcct.reference_number reference_number
             ,acct_struc.gcc_seg5 PROJECT_NUMBER
             ,acct_struc.proj_descr PROJECT_NAME
             ,acct_struc.gcc_seg6 ORACLE_SEGMENT6
             ,acct_struc.gcc_seg7 ORACLE_SEGMENT7
             ,apcct.category CC_TRNS_CATEGORY--Version 1.3
             ,aerl.attribute10 TAX_PROVINCE-- v2.0 start
             ,aerl.attribute11 GST_AMOUNT
             ,aerl.attribute12 PST_AMOUNT
             ,aerl.attribute13 QST_AMOUNT
             ,aerl.attribute14 HST_AMOUNT
             ,aerh.org_id -- v2.0 end
         FROM ap_credit_card_trxns_all apcct
              --,ap_card_programs_all acpa          kp
              --,ap_cards_all aca                   kp
             ,xxcusie_creditcard_vw xcv --kp
             ,ap_expense_reports_all aerp
             ,ap_expense_report_headers_all aerh
             ,ap_expense_report_lines_all aerl
             ,ap_exp_report_dists_all aerd
             ,ap_invoices_all aia
             ,xxcus.xxcusoie_emp_ccid_pull_tbl pep
             ,(SELECT lu1.meaning, lu1.lookup_code
                 FROM applsys.fnd_lookup_values lu1
                WHERE lu1.lookup_type = 'XXCUS_AP_MCC_CODES') codes
             ,(SELECT gcc.code_combination_id
                     ,prod.flex_value         gcc_seg1
                     ,prod.description        prod_descr
                     ,loc.flex_value          gcc_seg2
                     ,loc.description         loc_descr
                     ,cc.flex_value           gcc_seg3
                     ,cc.description          cc_descr
                     ,acc.flex_value          gcc_seg4
                     ,acc.description         acc_descr
                     ,proj.flex_value         gcc_seg5
                     ,proj.description        proj_descr
                     ,gcc.segment6            gcc_seg6
                     ,gcc.segment7            gcc_seg7
                 FROM gl_code_combinations gcc
                     ,fnd_flex_values_vl   prod
                     ,fnd_flex_values_vl   loc
                     ,fnd_flex_values_vl   cc
                     ,fnd_flex_values_vl   acc
                     ,fnd_flex_values_vl   proj
                WHERE gcc.segment1 = prod.flex_value
                  AND gcc.segment2 = loc.flex_value
                  AND gcc.segment3 = cc.flex_value
                  AND gcc.segment4 = acc.flex_value
                  AND gcc.segment5 = proj.flex_value
                  AND prod.flex_value_set_id = l_lob_value_set_id --1014547
                  AND loc.flex_value_set_id = l_loc_value_set_id --1014548
                  AND cc.flex_value_set_id = l_cc_value_set_id --1014549
                  AND acc.flex_value_set_id = l_acc_value_set_id --1014550
                  AND proj.flex_value_set_id = l_proj_value_set_id --1014551
               ) acct_struc
        WHERE nvl(apcct.category, 'UNKNOWN') IN
              ('BUSINESS', 'PERSONAL', 'UNKNOWN')
          AND apcct.transaction_type NOT IN ('30', '31', '62') --v2.0
          AND apcct.report_header_id IS NOT NULL
             --and acpa.card_program_id = aca.card_program_id       kp 
             --and aca.card_program_id = apcct.card_program_id      kp
             --and apcct.card_number = aca.card_number              kp
          AND apcct.card_program_id = xcv.card_program_id --kp     
          AND apcct.card_id = xcv.card_id --kp
          AND aerl.credit_card_trx_id = apcct.trx_id
          AND aerl.credit_card_trx_id NOT IN
              (SELECT xbq.credit_card_trx_id
                 FROM xxcus.xxcusoie_bullet_train_q1_tbl xbq
                WHERE xbq.credit_card_trx_id = aerl.credit_card_trx_id)
          AND apcct.sic_code = codes.lookup_code(+)
          AND xcv.employee_id = pep.person_id(+) --kp
          AND aerl.report_header_id = aerh.report_header_id
          AND aerp.expense_report_id = aerh.expense_report_id
             --AND aerd.report_header_id = aerh.report_header_id   --Version 1.2
          AND aerd.report_line_id(+) = aerl.report_line_id --Version 1.2 added outer join
          AND aerd.code_combination_id = acct_struc.code_combination_id
          AND aerh.vouchno = aia.invoice_id
          AND nvl(aerl.itemization_parent_id, 0) <> -1;
          
          
    CURSOR bullet_train_c2 IS
    SELECT NULL period_name
             ,(SELECT period_name
                 FROM gl.gl_periods u
                WHERE u.period_set_name = '4-4-QTR'
                  AND start_date <= apcct.posted_date
                  AND end_date >= apcct.posted_date
                  AND period_num != 13) cc_fiscal_period
             ,acct_struc.gcc_seg1 oracle_product
             ,acct_struc.prod_descr product_descr
             ,acct_struc.gcc_seg2 oracle_location
             ,acct_struc.loc_descr location_descr
             ,acct_struc.gcc_seg3 oracle_cost_center
             ,acct_struc.cc_descr cost_center_descr
             ,acct_struc.gcc_seg4 oracle_account
             ,acct_struc.acc_descr account_descr
             ,acct_struc.gcc_seg1 || '.' || acct_struc.gcc_seg2 || '.' ||
              acct_struc.gcc_seg3 || '.' || acct_struc.gcc_seg4 oracle_accounts
             ,(CASE
                WHEN (aerl.item_description IS NULL AND
                     ((nvl(apcct.sic_code, 0) = 0) OR codes.meaning IS NULL)) THEN
                 'NOT MAPPED'
                ELSE
                 nvl(TRIM(aerl.item_description), 'NO DESCRIPTION')
              END) item_description
             ,(SELECT DISTINCT flv.tag category
                 FROM applsys.fnd_lookup_values flv
                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
                  AND flv.meaning = (CASE
                        WHEN (aerl.item_description IS NULL AND
                             ((nvl(apcct.sic_code, 0) = 0) OR
                             codes.meaning IS NULL)) THEN
                         'NOT MAPPED'
                        ELSE
                         nvl(aerl.item_description, 'NOT MAPPED')
                      END)
                  AND rownum < 2) category
             ,(SELECT DISTINCT flv.description sub_category
                 FROM applsys.fnd_lookup_values flv
                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
                  AND flv.meaning = (CASE
                        WHEN (aerl.item_description IS NULL AND
                             ((nvl(apcct.sic_code, 0) = 0) OR
                             codes.meaning IS NULL)) THEN
                         'NOT MAPPED'
                        ELSE
                         nvl(aerl.item_description, 'NOT MAPPED')
                      END)
                  AND rownum < 2) sub_category
             ,(CASE
                WHEN aerl.item_description = 'Personal' THEN
                 (nvl(aerd.amount, aerl.amount) * -1) --added NVL 09112012
                ELSE
                --nvl(aerd.amount, apcct.billed_amount)               --09112012 
                 nvl(aerd.amount, aerl.amount)
              END) line_amount
             ,aerl.currency_code currency_code
             ,aerh.invoice_num
             ,aerl.distribution_line_number || '.' ||
              (aerd.sequence_num + 1) distribution_line_number
             ,pep.full_name
             ,pep.employee_number
             ,pep.segment1 emp_default_prod
             ,pep.segment2 emp_default_loc
             ,pep.segment3 emp_default_costctr
             ,pep.fru
             ,aerh.creation_date
             ,aerh.report_submitted_date
             ,aerh.description
             ,aerl.merchant_name vendor_name
             ,apcct.merchant_name1 MERCHANT_NAME
             ,NULL business_purpose
             ,aerl.justification
             ,aerl.start_expense_date
             ,aerl.end_expense_date
             ,NULL remarks
             ,aerl.attendees
             ,NULL attendees_emp
             ,NULL attendees_te
             ,NULL attendees_cti1
             ,NULL attendees_cti2
             ,apcct.trx_id CREDIT_CARD_TRX_ID
             ,xcv.chname cardmember_name --aca.cardmember_name,    kp
             ,apcct.transaction_date
             ,apcct.posted_date POSTED_DATE--trans_post_date
             ,apcct.merchant_city || ', ' || apcct.merchant_province_state merchant_location
             ,aerl.destination_from
             ,aerl.destination_to
             ,aerl.distance_unit_code
             ,aerl.daily_distance
             ,aerl.trip_distance
             ,aerl.avg_mileage_rate
             ,aerh.accounting_date dt_sentto_gl
             ,aerh.report_header_id expense_report_number
             ,xcv.card_program_id --acpa.card_program_id,    kp
             ,xcv.card_program_name --  acpa.card_program_name,    kp
             ,nvl(apcct.sic_code, 0) mcc_code_no
             ,(CASE
                WHEN ((nvl(apcct.sic_code, 0) = 0) OR codes.meaning IS NULL) THEN
                 'NOT MAPPED'
                ELSE
                 codes.meaning
              END) mcc_code
             ,aerh.accounting_date accounting_date
             ,aerh.total report_total
             ,aerl.daily_distance MILES
             ,aerl.avg_mileage_rate RATE_PER_MILE
             ,aerl.attribute_category
             ,aerl.category_code
             ,apcct.transaction_amount
             ,aerl.receipt_currency_amount
             ,aerh.amt_due_ccard_company
             ,aerh.amt_due_employee
             ,decode(aerh.source, 'SelfService', 'Y', 'N') imported_to_ap
             ,(CASE
                WHEN aerh.accounting_date IS NOT NULL THEN
                 'Y'
                ELSE
                 'N'
              END) imported_to_gl
             ,nvl(decode(aerh.workflow_approved_flag
                        ,'A'
                        ,'Y'
                        ,'Y'
                        ,'Y'
                        ,'M'
                        ,'Y'
                        ,'N')
                 ,'N') workflow_approved_flag
             ,aerh.expense_current_approver_id
             ,aerh.override_approver_id approver_id
             ,aerh.override_approver_name approver_name
             ,aerp.report_type
             ,aerh.advance_invoice_to_apply
             ,aerh.prepay_dist_num advance_distribution_number
             ,nvl(aerh.prepay_apply_flag, 'N') advance_flag
             ,aerh.prepay_gl_date advance_gl_date
             ,aerh.prepay_num advance_number
             ,aerh.reject_code report_reject_code
             ,nvl(decode(aerh.workflow_approved_flag
                        ,'A'
                        ,'Y'
                        ,'Y'
                        ,'Y'
                        ,'M'
                        ,'Y'
                        ,'N')
                 ,'N') || (CASE
                             WHEN aerh.accounting_date IS NOT NULL THEN
                              'Y'
                             ELSE
                              'N'
                           END) app_post_flag
             ,decode(nvl(decode(aerh.workflow_approved_flag
                               ,'A'
                               ,'Y'
                               ,'Y'
                               ,'Y'
                               ,'M'
                               ,'Y'
                               ,'N')
                        ,'N') || (CASE
                                    WHEN aerh.accounting_date IS NOT NULL THEN
                                     'Y'
                                    ELSE
                                     'N'
                                  END)
                    ,'YY'
                    ,'Y'
                    ,'NN'
                    ,'N'
                    ,'N') approved_in_gl
             ,aerh.vouchno
             ,'2' QUERY_NUM--query_no 
             ,'2 - In iExpense Not processed through to AP' query_descr
             ,aerh.expense_status_code expense_report_status
             ,apcct.reference_number reference_number
             ,acct_struc.gcc_seg5 PROJECT_NUMBER
             ,acct_struc.proj_descr PROJECT_NAME
             ,acct_struc.gcc_seg6 ORACLE_SEGMENT6
             ,acct_struc.gcc_seg7 ORACLE_SEGMENT7
             ,apcct.category CC_TRNS_CATEGORY--Version 1.3
             ,aerl.attribute10 TAX_PROVINCE-- v2.0 start
             ,aerl.attribute11 GST_AMOUNT
             ,aerl.attribute12 PST_AMOUNT
             ,aerl.attribute13 QST_AMOUNT
             ,aerl.attribute14 HST_AMOUNT
             ,aerh.org_id -- v2.0 end
         FROM ap_credit_card_trxns_all apcct
              --ap_card_programs_all acpa,           kp
              --ap_cards_all aca,                    kp
             ,xxcusie_creditcard_vw xcv --kp
             ,ap_expense_reports_all aerp
             ,ap_expense_report_headers_all aerh
             ,ap_expense_report_lines_all aerl
             ,ap_exp_report_dists_all aerd
             ,xxcus.xxcusoie_emp_ccid_pull_tbl pep
             ,(SELECT lu1.meaning, lu1.lookup_code
                 FROM applsys.fnd_lookup_values lu1
                WHERE lu1.lookup_type = 'XXCUS_AP_MCC_CODES') codes
             ,(SELECT gcc.code_combination_id
                     ,prod.flex_value         gcc_seg1
                     ,prod.description        prod_descr
                     ,loc.flex_value          gcc_seg2
                     ,loc.description         loc_descr
                     ,cc.flex_value           gcc_seg3
                     ,cc.description          cc_descr
                     ,acc.flex_value          gcc_seg4
                     ,acc.description         acc_descr
                     ,proj.flex_value         gcc_seg5
                     ,proj.description        proj_descr
                     ,gcc.segment6            gcc_seg6
                     ,gcc.segment7            gcc_seg7
                 FROM gl_code_combinations gcc
                     ,fnd_flex_values_vl   prod
                     ,fnd_flex_values_vl   loc
                     ,fnd_flex_values_vl   cc
                     ,fnd_flex_values_vl   acc
                     ,fnd_flex_values_vl   proj
                WHERE gcc.segment1 = prod.flex_value
                  AND gcc.segment2 = loc.flex_value
                  AND gcc.segment3 = cc.flex_value
                  AND gcc.segment4 = acc.flex_value
                  AND gcc.segment5 = proj.flex_value
                  AND prod.flex_value_set_id = l_lob_value_set_id --1014547
                  AND loc.flex_value_set_id = l_loc_value_set_id --1014548
                  AND cc.flex_value_set_id = l_cc_value_set_id --1014549
                  AND acc.flex_value_set_id = l_acc_value_set_id --1014550
                  AND proj.flex_value_set_id = l_proj_value_set_id --1014551
               ) acct_struc
        WHERE nvl(apcct.category, 'UNKNOWN') IN
              ('BUSINESS', 'PERSONAL', 'UNKNOWN')
          AND apcct.transaction_type NOT IN ('30', '31', '62') --v2.0
          AND apcct.report_header_id IS NOT NULL
             --and acpa.card_program_id = aca.card_program_id       kp 
             --and aca.card_program_id = apcct.card_program_id      kp
             --and apcct.card_number = aca.card_number              kp
          AND apcct.card_program_id = xcv.card_program_id --kp     
          AND apcct.card_id = xcv.card_id --kp
          AND xcv.employee_id = pep.person_id(+) --kp
          AND apcct.sic_code = codes.lookup_code(+)
          AND aerl.credit_card_trx_id = apcct.trx_id
          AND aerl.report_header_id = aerh.report_header_id
          AND aerp.expense_report_id = aerh.expense_report_id
             --AND aerd.report_header_id = aerh.report_header_id  --Version 1.2
          AND aerd.report_line_id(+) = aerl.report_line_id --Version 1.2 added outer join
          AND acct_struc.code_combination_id = aerd.code_combination_id --Added this condition and commented below by Maha for Ver#4
--        AND acct_struc.code_combination_id =   
--              (CASE
--                 WHEN aerd.code_combination_id IS NOT NULL THEN
--                  aerd.code_combination_id
--                 WHEN aerd.code_combination_id IS NULL OR
--                      aerd.code_combination_id IN (200, -1) THEN
--                  (CASE
--                    WHEN aerl.code_combination_id IS NOT NULL THEN
--                     aerl.code_combination_id
--                    WHEN aerl.code_combination_id IS NULL THEN
--                     (SELECT gcc2.code_combination_id
--                        FROM gl.gl_code_combinations gcc2
--                       WHERE nvl(aerd.segment1, pep.segment1) = gcc2.segment1
--                         AND nvl(aerd.segment2, pep.segment2) = gcc2.segment2
--                         AND nvl(aerd.segment3, pep.segment3) = gcc2.segment3
--                         AND nvl(aerd.segment4
--                                ,get_exp_rep_param_info(aerh.expense_report_id
--                                                       ,apcct.folio_type
--                                                       ,'ACCOUNT')) =
--                             gcc2.segment4
--                         AND nvl(aerd.segment5, '00000') = gcc2.segment5
--                         AND nvl(aerd.segment6, '00000') = gcc2.segment6
--                         AND nvl(aerd.segment7, '00000') = gcc2.segment7)
--                  END)
--               END)
          AND nvl(aerh.vouchno, 0) = 0
          AND nvl(aerl.itemization_parent_id, 0) <> -1
          AND aerd.code_combination_id IS NOT NULL;         -- Added by Maha for ver#4
         
-- Added below by Maha for ver#4

   CURSOR bullet_train_c3 IS
    SELECT NULL period_name
             ,(SELECT period_name
                 FROM gl.gl_periods u
                WHERE u.period_set_name = '4-4-QTR'
                  AND start_date <= apcct.posted_date
                  AND end_date >= apcct.posted_date
                  AND period_num != 13) cc_fiscal_period
             ,acct_struc.gcc_seg1 oracle_product
             ,acct_struc.prod_descr product_descr
             ,acct_struc.gcc_seg2 oracle_location
             ,acct_struc.loc_descr location_descr
             ,acct_struc.gcc_seg3 oracle_cost_center
             ,acct_struc.cc_descr cost_center_descr
             ,acct_struc.gcc_seg4 oracle_account
             ,acct_struc.acc_descr account_descr
             ,acct_struc.gcc_seg1 || '.' || acct_struc.gcc_seg2 || '.' ||
              acct_struc.gcc_seg3 || '.' || acct_struc.gcc_seg4 oracle_accounts
             ,(CASE
                WHEN (aerl.item_description IS NULL AND
                     ((nvl(apcct.sic_code, 0) = 0) OR codes.meaning IS NULL)) THEN
                 'NOT MAPPED'
                ELSE
                 nvl(TRIM(aerl.item_description), 'NO DESCRIPTION')
              END) item_description
             ,(SELECT DISTINCT flv.tag category
                 FROM applsys.fnd_lookup_values flv
                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
                  AND flv.meaning = (CASE
                        WHEN (aerl.item_description IS NULL AND
                             ((nvl(apcct.sic_code, 0) = 0) OR
                             codes.meaning IS NULL)) THEN
                         'NOT MAPPED'
                        ELSE
                         nvl(aerl.item_description, 'NOT MAPPED')
                      END)
                  AND rownum < 2) category
             ,(SELECT DISTINCT flv.description sub_category
                 FROM applsys.fnd_lookup_values flv
                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
                  AND flv.meaning = (CASE
                        WHEN (aerl.item_description IS NULL AND
                             ((nvl(apcct.sic_code, 0) = 0) OR
                             codes.meaning IS NULL)) THEN
                         'NOT MAPPED'
                        ELSE
                         nvl(aerl.item_description, 'NOT MAPPED')
                      END)
                  AND rownum < 2) sub_category
             ,(CASE
                WHEN aerl.item_description = 'Personal' THEN
                 (nvl(aerd.amount, aerl.amount) * -1) --added NVL 09112012
                ELSE
                --nvl(aerd.amount, apcct.billed_amount)               --09112012 
                 nvl(aerd.amount, aerl.amount)
              END) line_amount
             ,aerl.currency_code currency_code
             ,aerh.invoice_num
             ,aerl.distribution_line_number || '.' ||
              (aerd.sequence_num + 1) distribution_line_number
             ,pep.full_name
             ,pep.employee_number
             ,pep.segment1 emp_default_prod
             ,pep.segment2 emp_default_loc
             ,pep.segment3 emp_default_costctr
             ,pep.fru
             ,aerh.creation_date
             ,aerh.report_submitted_date
             ,aerh.description
             ,aerl.merchant_name vendor_name
             ,apcct.merchant_name1 MERCHANT_NAME
             ,NULL business_purpose
             ,aerl.justification
             ,aerl.start_expense_date
             ,aerl.end_expense_date
             ,NULL remarks
             ,aerl.attendees
             ,NULL attendees_emp
             ,NULL attendees_te
             ,NULL attendees_cti1
             ,NULL attendees_cti2
             ,apcct.trx_id CREDIT_CARD_TRX_ID
             ,xcv.chname cardmember_name --aca.cardmember_name,    kp
             ,apcct.transaction_date
             ,apcct.posted_date POSTED_DATE--trans_post_date
             ,apcct.merchant_city || ', ' || apcct.merchant_province_state merchant_location
             ,aerl.destination_from
             ,aerl.destination_to
             ,aerl.distance_unit_code
             ,aerl.daily_distance
             ,aerl.trip_distance
             ,aerl.avg_mileage_rate
             ,aerh.accounting_date dt_sentto_gl
             ,aerh.report_header_id expense_report_number
             ,xcv.card_program_id --acpa.card_program_id,    kp
             ,xcv.card_program_name --  acpa.card_program_name,    kp
             ,nvl(apcct.sic_code, 0) mcc_code_no
             ,(CASE
                WHEN ((nvl(apcct.sic_code, 0) = 0) OR codes.meaning IS NULL) THEN
                 'NOT MAPPED'
                ELSE
                 codes.meaning
              END) mcc_code
             ,aerh.accounting_date accounting_date
             ,aerh.total report_total
             ,aerl.daily_distance MILES
             ,aerl.avg_mileage_rate RATE_PER_MILE
             ,aerl.attribute_category
             ,aerl.category_code
             ,apcct.transaction_amount
             ,aerl.receipt_currency_amount
             ,aerh.amt_due_ccard_company
             ,aerh.amt_due_employee
             ,decode(aerh.source, 'SelfService', 'Y', 'N') imported_to_ap
             ,(CASE
                WHEN aerh.accounting_date IS NOT NULL THEN
                 'Y'
                ELSE
                 'N'
              END) imported_to_gl
             ,nvl(decode(aerh.workflow_approved_flag
                        ,'A'
                        ,'Y'
                        ,'Y'
                        ,'Y'
                        ,'M'
                        ,'Y'
                        ,'N')
                 ,'N') workflow_approved_flag
             ,aerh.expense_current_approver_id
             ,aerh.override_approver_id approver_id
             ,aerh.override_approver_name approver_name
             ,aerp.report_type
             ,aerh.advance_invoice_to_apply
             ,aerh.prepay_dist_num advance_distribution_number
             ,nvl(aerh.prepay_apply_flag, 'N') advance_flag
             ,aerh.prepay_gl_date advance_gl_date
             ,aerh.prepay_num advance_number
             ,aerh.reject_code report_reject_code
             ,nvl(decode(aerh.workflow_approved_flag
                        ,'A'
                        ,'Y'
                        ,'Y'
                        ,'Y'
                        ,'M'
                        ,'Y'
                        ,'N')
                 ,'N') || (CASE
                             WHEN aerh.accounting_date IS NOT NULL THEN
                              'Y'
                             ELSE
                              'N'
                           END) app_post_flag
             ,decode(nvl(decode(aerh.workflow_approved_flag
                               ,'A'
                               ,'Y'
                               ,'Y'
                               ,'Y'
                               ,'M'
                               ,'Y'
                               ,'N')
                        ,'N') || (CASE
                                    WHEN aerh.accounting_date IS NOT NULL THEN
                                     'Y'
                                    ELSE
                                     'N'
                                  END)
                    ,'YY'
                    ,'Y'
                    ,'NN'
                    ,'N'
                    ,'N') approved_in_gl
             ,aerh.vouchno
             ,'2' QUERY_NUM--query_no 
             ,'2 - In iExpense Not processed through to AP' query_descr
             ,aerh.expense_status_code expense_report_status
             ,apcct.reference_number reference_number
             ,acct_struc.gcc_seg5 PROJECT_NUMBER
             ,acct_struc.proj_descr PROJECT_NAME
             ,acct_struc.gcc_seg6 ORACLE_SEGMENT6
             ,acct_struc.gcc_seg7 ORACLE_SEGMENT7
             ,apcct.category CC_TRNS_CATEGORY--Version 1.3
             ,aerl.attribute10 TAX_PROVINCE-- v2.0 start
             ,aerl.attribute11 GST_AMOUNT
             ,aerl.attribute12 PST_AMOUNT
             ,aerl.attribute13 QST_AMOUNT
             ,aerl.attribute14 HST_AMOUNT
             ,aerh.org_id -- v2.0 end
         FROM ap_credit_card_trxns_all apcct
              --ap_card_programs_all acpa,           kp
              --ap_cards_all aca,                    kp
             ,xxcusie_creditcard_vw xcv --kp
             ,ap_expense_reports_all aerp
             ,ap_expense_report_headers_all aerh
             ,ap_expense_report_lines_all aerl
             ,ap_exp_report_dists_all aerd
             ,xxcus.xxcusoie_emp_ccid_pull_tbl pep
             ,(SELECT lu1.meaning, lu1.lookup_code
                 FROM applsys.fnd_lookup_values lu1
                WHERE lu1.lookup_type = 'XXCUS_AP_MCC_CODES') codes
             ,(SELECT gcc.code_combination_id
                     ,prod.flex_value         gcc_seg1
                     ,prod.description        prod_descr
                     ,loc.flex_value          gcc_seg2
                     ,loc.description         loc_descr
                     ,cc.flex_value           gcc_seg3
                     ,cc.description          cc_descr
                     ,acc.flex_value          gcc_seg4
                     ,acc.description         acc_descr
                     ,proj.flex_value         gcc_seg5
                     ,proj.description        proj_descr
                     ,gcc.segment6            gcc_seg6
                     ,gcc.segment7            gcc_seg7
                 FROM gl_code_combinations gcc
                     ,fnd_flex_values_vl   prod
                     ,fnd_flex_values_vl   loc
                     ,fnd_flex_values_vl   cc
                     ,fnd_flex_values_vl   acc
                     ,fnd_flex_values_vl   proj
                WHERE gcc.segment1 = prod.flex_value
                  AND gcc.segment2 = loc.flex_value
                  AND gcc.segment3 = cc.flex_value
                  AND gcc.segment4 = acc.flex_value
                  AND gcc.segment5 = proj.flex_value
                  AND prod.flex_value_set_id = l_lob_value_set_id --1014547
                  AND loc.flex_value_set_id = l_loc_value_set_id --1014548
                  AND cc.flex_value_set_id = l_cc_value_set_id --1014549
                  AND acc.flex_value_set_id = l_acc_value_set_id --1014550
                  AND proj.flex_value_set_id = l_proj_value_set_id --1014551
               ) acct_struc
        WHERE nvl(apcct.category, 'UNKNOWN') IN
              ('BUSINESS', 'PERSONAL', 'UNKNOWN')
          AND apcct.transaction_type NOT IN ('30', '31', '62') --v2.0
          AND apcct.report_header_id IS NOT NULL
             --and acpa.card_program_id = aca.card_program_id       kp 
             --and aca.card_program_id = apcct.card_program_id      kp
             --and apcct.card_number = aca.card_number              kp
          AND apcct.card_program_id = xcv.card_program_id --kp     
          AND apcct.card_id = xcv.card_id --kp
          AND xcv.employee_id = pep.person_id(+) --kp
          AND apcct.sic_code = codes.lookup_code(+)
          AND aerl.credit_card_trx_id = apcct.trx_id
          AND aerl.report_header_id = aerh.report_header_id
          AND aerp.expense_report_id = aerh.expense_report_id
             --AND aerd.report_header_id = aerh.report_header_id  --Version 1.2
          AND aerd.report_line_id(+) = aerl.report_line_id --Version 1.2 added outer join
          AND acct_struc.code_combination_id = (SELECT GCC2.code_combination_id 
						 FROM gl_code_combinations gcc2
 				                WHERE --aerd.code_combination_id is null
			--                         aerd.segment1 = gcc2.segment1
			--                         AND aerd.segment2 = gcc2.segment2
			--                         AND aerd.segment3 = gcc2.segment3
			--                         AND aerd.segment4 = gcc2.segment4 
			--                         AND aerd.segment5 = gcc2.segment5
			--                         AND aerd.segment6 = gcc2.segment6
			--                         AND aerd.segment7 = gcc2.segment7)
--commented above segment conditions and added below for ver# 5.0
		                             NVL2(aerd.segment1, aerd.segment1, '1') =gcc2.segment1 
                		         AND NVL2(aerd.segment2, aerd.segment2, '2') =gcc2.segment2 
                         		 AND NVL2(aerd.segment3, aerd.segment3, '3') =gcc2.segment3 
                         		 AND NVL2(aerd.segment4, aerd.segment4, '4') =gcc2.segment4 
                         		 AND NVL2(aerd.segment5, aerd.segment5, '5') =gcc2.segment5 
                         		 AND NVL2(aerd.segment6, aerd.segment6, '6') =gcc2.segment6 
                         		 AND NVL2(aerd.segment7, aerd.segment7, '7') =gcc2.segment7 ) 
  
          AND nvl(aerh.vouchno, 0) = 0
          AND nvl(aerl.itemization_parent_id, 0) <> -1
          AND aerd.code_combination_id IS NULL;         
 --<< version#4 End
  
  BEGIN
  
    l_sec := 'Start Main Program; ';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
  
    l_sec := 'Get Product Value Set ID';
    BEGIN
      SELECT flex_value_set_id
        INTO l_lob_value_set_id
        FROM fnd_flex_value_sets
       WHERE flex_value_set_name = 'XXCUS_GL_PRODUCT';
    END;
  
    l_sec := 'Get Location Value Set ID';
    BEGIN
      SELECT flex_value_set_id
        INTO l_loc_value_set_id
        FROM fnd_flex_value_sets
       WHERE flex_value_set_name = 'XXCUS_GL_LOCATION';
    END;
  
    l_sec := 'Get Cost Center Value Set ID';
    BEGIN
      SELECT flex_value_set_id
        INTO l_cc_value_set_id
        FROM fnd_flex_value_sets
       WHERE flex_value_set_name = 'XXCUS_GL_COSTCENTER';
    END;
  
    l_sec := 'Get Account Value Set ID';
    BEGIN
      SELECT flex_value_set_id
        INTO l_acc_value_set_id
        FROM fnd_flex_value_sets
       WHERE flex_value_set_name = 'XXCUS_GL_ACCOUNT';
    END;
  
    l_sec := 'Get Project Value Set ID';
    BEGIN
      SELECT flex_value_set_id
        INTO l_proj_value_set_id
        FROM fnd_flex_value_sets
       WHERE flex_value_set_name = 'XXCUS_GL_PROJECT';
    END;
  
    --Truncate the Summary Table
    BEGIN
      l_sec := 'Truncate the XXCUS.XXCUS_BULLET_IEXP_TBL before loading specified time frame.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
    
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_BULLET_IEXP_TBL';
    
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
    
    EXCEPTION
      WHEN OTHERS THEN
        p_retcode := 900021;
        p_errbuf  := 'XXCUSOIE: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                     ' in ' || l_procedure_name || ' at ' || l_sec;
        fnd_file.put_line(fnd_file.log, p_errbuf);
        -- Calling ERROR API
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                            ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => p_retcode
                                            ,p_error_desc        => p_errbuf
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'XXCUS');
    END;
  
    --Truncate the Query1 CC Summary Table
    BEGIN
      l_sec := 'Truncate the XXCUSOIE_BULLET_TRAIN_Q1_TBL table before loading specified time frame.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
    
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSOIE_BULLET_TRAIN_Q1_TBL';
    
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
    
    EXCEPTION
      WHEN OTHERS THEN
        p_retcode := 900022;
        p_errbuf  := 'XXCUSOIE: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                     ' in ' || l_procedure_name || ' at ' || l_sec;
        fnd_file.put_line(fnd_file.log, p_errbuf);
        -- Calling ERROR API
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                            ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => p_retcode
                                            ,p_error_desc        => p_errbuf
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'XXCUS');
    END;
  
    l_sec := 'Truncate XXCUSOIE_EMP_CCID_TBL';
  
    --Truncate the Employee Summary Table
    BEGIN
      l_sec := 'Truncate the employee summary table before loading specified time frame.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSOIE_EMP_CCID_TBL';
    
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
    EXCEPTION
      WHEN OTHERS THEN
        p_retcode := 900023;
        p_errbuf  := 'XXCUSOIE: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                     ' in ' || l_procedure_name || ' at ' || l_sec;
        fnd_file.put_line(fnd_file.log, p_errbuf);
        -- Calling ERROR API
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                            ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => p_retcode
                                            ,p_error_desc        => p_errbuf
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'XXCUS');
    END;
  
    l_sec := 'Truncate XXCUSOIE_EMP_CCID_PULL_TBL';
    --Truncate the Emp Pull Summary Table
    BEGIN
      l_sec := 'Truncate the emp pull summary table before loading specified time frame.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSOIE_EMP_CCID_PULL_TBL';
    
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
    
    EXCEPTION
      WHEN OTHERS THEN
        p_retcode := 900024;
        p_errbuf  := 'XXCUSOIE: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                     ' in ' || l_procedure_name || ' at ' || l_sec;
        fnd_file.put_line(fnd_file.log, p_errbuf);
        -- Calling ERROR API
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                            ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => p_retcode
                                            ,p_error_desc        => p_errbuf
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'XXCUS');
    END;
  
    l_sec := 'Load Employee Default CCID';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcusoie_emp_ccid_tbl
      (SELECT DISTINCT employee_number
                      ,a.person_id
                      ,a.last_name
                      ,a.first_name
                      ,a.full_name
                      ,nvl(default_code_comb_id
                          ,(SELECT aa.default_code_comb_id
                             FROM hr.per_all_assignments_f aa
                            WHERE aa.person_id = a.person_id
                              AND aa.default_code_comb_id IS NOT NULL
                              AND aa.effective_end_date =
                                  (SELECT MAX(t_ed.effective_end_date)
                                     FROM hr.per_all_assignments_f t_ed
                                    WHERE t_ed.person_id = aa.person_id
                                      AND t_ed.default_code_comb_id IS NOT NULL)
                              AND rownum < 2))
         FROM xxcus.xxcushr_per_people_curr_tbl a
             , --xxcus.xxcus_per_people_curr a,
              hr.per_all_assignments_f          t
             , --per_all_assignments_f    t,
              gl.gl_code_combinations           g
        WHERE a.person_id = t.person_id
          AND t.primary_flag = 'Y'
          AND t.effective_end_date =
              (SELECT MAX(t_ed.effective_end_date)
                 FROM hr.per_all_assignments_f t_ed
                WHERE t_ed.person_id = t.person_id)
          AND t.default_code_comb_id = g.code_combination_id(+)
          AND a.effective_end_date = to_date('31-DEC-4712', 'DD-MON-YYYY'));
  
    COMMIT;
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcusoie_emp_ccid_pull_tbl
      (SELECT DISTINCT employee_number
                      ,a.person_id
                      ,a.last_name
                      ,a.first_name
                      ,a.full_name
                      ,a.emp_ccid
                      ,g.segment1
                      ,g.segment2
                      ,g.segment3
                      ,(SELECT DISTINCT lc.fru
                          FROM xxcus_location_code_vw lc
                         WHERE lc.entrp_entity = g.segment1
                           AND lc.entrp_loc = g.segment2
                           AND rownum < 2) fru
         FROM xxcus.xxcusoie_emp_ccid_tbl a, gl.gl_code_combinations g
        WHERE a.emp_ccid = g.code_combination_id(+)
          AND g.segment1 NOT IN ('11', '42', '47')
       UNION
       SELECT DISTINCT employee_number
                      ,a.person_id
                      ,a.last_name
                      ,a.first_name
                      ,a.full_name
                      ,a.emp_ccid
                      ,g.segment1
                      ,g.segment2
                      ,g.segment3
                      ,(SELECT DISTINCT lc.fru
                          FROM xxcus_location_code_vw lc
                         WHERE lc.system_cd IN ('ORCL-FM', 'ORCL-CTI')
                           AND lc.entrp_entity = g.segment1
                           AND lc.entrp_loc = g.segment2
                           AND rownum < 2) fru
         FROM xxcus.xxcusoie_emp_ccid_tbl a, gl.gl_code_combinations g
        WHERE a.emp_ccid = g.code_combination_id(+)
          AND g.segment1 IN ('11', '42', '47'));
  
    COMMIT;
  
    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
  
    l_sec := 'Insert Query 1 and  6 CC Table and Main Table from Backup Table'; --remove ampersand
  
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcus_bullet_iexp_tbl
      (SELECT *
         FROM xxcus.xxcusoie_bullet_train_bu_tbl btu
        WHERE btu.query_num = '1'
          AND btu.expense_report_status = 'PAID'
       UNION
       SELECT *
         FROM xxcus.xxcusoie_bullet_train_bu_tbl btu
        WHERE btu.query_num = '6');
  
    COMMIT;
  
    l_sec := 'Build credit card transaction ID table for PAID Query Number 1 records';
    INSERT /*+ APPEND */
    INTO xxcus.xxcusoie_bullet_train_q1_tbl
      (SELECT DISTINCT xbi.credit_card_trx_id
         FROM xxcus.xxcus_bullet_iexp_tbl xbi
        WHERE xbi.query_num = '1'
          AND xbi.expense_report_status = 'PAID');
  
    COMMIT;
  
    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
  
    l_sec := 'Insert Query 1 Records';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
  --Added by Maha for version#3 <<Start                    
     OPEN bullet_train_c1;
     LOOP
     FETCH bullet_train_c1 BULK COLLECT INTO l_rec limit 10000;
     EXIT WHEN l_rec.count =0;
     IF l_rec.COUNT >0 THEN
      BEGIN
      FORALL i IN l_rec.FIRST .. l_rec.LAST 
       INSERT INTO xxcus.xxcus_bullet_iexp_tbl VALUES l_rec(i);
       COMMIT;
      END;
      END IF;
     END LOOP;
    CLOSE bullet_train_c1;

  --commented by Maha for version#3 <<Start
--    INSERT /*+ APPEND */
--    INTO xxcus.xxcus_bullet_iexp_tbl
--      (SELECT (SELECT period_name
--                 FROM gl.gl_periods u
--                WHERE u.period_set_name = '4-4-QTR'
--                  AND start_date <= aia.gl_date
--                  AND end_date >= aia.gl_date
--                  AND period_num != 13) period_name
--             ,(SELECT period_name
--                 FROM gl.gl_periods u
--                WHERE u.period_set_name = '4-4-QTR'
--                  AND start_date <= apcct.posted_date
--                  AND end_date >= apcct.posted_date
--                  AND period_num != 13) cc_fiscal_period
--             ,acct_struc.gcc_seg1 oracle_product
--             ,acct_struc.prod_descr product_descr
--             ,acct_struc.gcc_seg2 oracle_location
--             ,acct_struc.loc_descr location_descr
--             ,acct_struc.gcc_seg3 oracle_cost_center
--             ,acct_struc.cc_descr cost_center_descr
--             ,acct_struc.gcc_seg4 oracle_account
--             ,acct_struc.acc_descr account_descr
--             ,acct_struc.gcc_seg1 || '.' || acct_struc.gcc_seg2 || '.' ||
--              acct_struc.gcc_seg3 || '.' || acct_struc.gcc_seg4 oracle_accounts
--             ,(CASE
--                WHEN (aerl.item_description IS NULL AND
--                     ((nvl(apcct.sic_code, 0) = 0) OR codes.meaning IS NULL)) THEN
--                 'NOT MAPPED'
--                ELSE
--                 nvl(TRIM(aerl.item_description), 'NO DESCRIPTION')
--              END) item_description
--             ,(SELECT DISTINCT flv.tag category
--                 FROM applsys.fnd_lookup_values flv
--                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
--                  AND flv.meaning = (CASE
--                        WHEN (aerl.item_description IS NULL AND
--                             ((nvl(apcct.sic_code, 0) = 0) OR
--                             codes.meaning IS NULL)) THEN
--                         'NOT MAPPED'
--                        ELSE
--                         nvl(aerl.item_description, 'NOT MAPPED')
--                      END)
--                  AND rownum < 2) category
--             ,(SELECT DISTINCT flv.description sub_category
--                 FROM applsys.fnd_lookup_values flv
--                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
--                  AND flv.meaning = (CASE
--                        WHEN (aerl.item_description IS NULL AND
--                             ((nvl(apcct.sic_code, 0) = 0) OR
--                             codes.meaning IS NULL)) THEN
--                         'NOT MAPPED'
--                        ELSE
--                         nvl(aerl.item_description, 'NOT MAPPED')
--                      END)
--                  AND rownum < 2) sub_category
--             ,(CASE
--                WHEN aerl.item_description = 'Personal' THEN
--                 (nvl(aerd.amount, aerl.amount) * -1) --added NVL 09112012
--                ELSE
--                --nvl(aerd.amount, apcct.billed_amount)  --09112012
--                 nvl(aerd.amount, aerl.amount)
--              END) line_amount
--             ,aerl.currency_code currency_code
--             ,aerh.invoice_num
--             ,aerl.distribution_line_number || '.' ||
--              (aerd.sequence_num + 1) distribution_line_number
--             ,pep.full_name
--             ,pep.employee_number
--             ,pep.segment1 emp_default_prod
--             ,pep.segment2 emp_default_loc
--             ,pep.segment3 emp_default_costctr
--             ,pep.fru
--             ,aerh.creation_date
--             ,aerh.report_submitted_date
--             ,aerh.description
--             ,aerl.merchant_name vendor_name
--             ,apcct.merchant_name1
--             ,NULL business_purpose
--             ,aerl.justification
--             ,aerl.start_expense_date
--             ,aerl.end_expense_date
--             ,NULL remarks
--             ,aerl.attendees
--             ,NULL attendees_emp
--             ,NULL attendees_te
--             ,NULL attendees_cti1
--             ,NULL attendees_cti2
--             ,apcct.trx_id
--             ,xcv.chname cardmember_name --aca.cardmember_name,    kp  
--             ,apcct.transaction_date
--             ,apcct.posted_date trans_post_date
--             ,apcct.merchant_city || ', ' || apcct.merchant_province_state merchant_location
--             ,aerl.destination_from
--             ,aerl.destination_to
--             ,aerl.distance_unit_code
--             ,aerl.daily_distance
--             ,aerl.trip_distance
--             ,aerl.avg_mileage_rate
--             ,aia.gl_date dt_sentto_gl
--             ,aerh.report_header_id expense_report_number
--             ,xcv.card_program_id --acpa.card_program_id,    kp
--             ,xcv.card_program_name --  acpa.card_program_name,    kp
--             ,nvl(apcct.sic_code, 0) mcc_code_no
--             ,(CASE
--                WHEN ((nvl(apcct.sic_code, 0) = 0) OR codes.meaning IS NULL) THEN
--                 'NOT MAPPED'
--                ELSE
--                 codes.meaning
--              END) mcc_code
--             ,aia.gl_date accounting_date
--             ,aerh.total report_total
--             ,aerl.daily_distance
--             ,aerl.avg_mileage_rate
--             ,aerl.attribute_category
--             ,aerl.category_code
--             ,apcct.transaction_amount
--             ,aerl.receipt_currency_amount
--             ,aerh.amt_due_ccard_company
--             ,aerh.amt_due_employee
--             ,decode(aerh.source, 'SelfService', 'Y', 'N') imported_to_ap
--             ,(CASE
--                WHEN aia.gl_date IS NOT NULL THEN
--                 'Y'
--                ELSE
--                 'N'
--              END) imported_to_gl
--             ,nvl(decode(aerh.workflow_approved_flag
--                        ,'A'
--                        ,'Y'
--                        ,'Y'
--                        ,'Y'
--                        ,'M'
--                        ,'Y'
--                        ,'N')
--                 ,'N') workflow_approved_flag
--             ,aerh.expense_current_approver_id
--             ,aerh.override_approver_id approver_id
--             ,aerh.override_approver_name approver_name
--             ,aerp.report_type
--             ,aerh.advance_invoice_to_apply
--             ,aerh.prepay_dist_num advance_distribution_number
--             ,nvl(aerh.prepay_apply_flag, 'N') advance_flag
--             ,aerh.prepay_gl_date advance_gl_date
--             ,aerh.prepay_num advance_number
--             ,aerh.reject_code report_reject_code
--             ,nvl(decode(aerh.workflow_approved_flag
--                        ,'A'
--                        ,'Y'
--                        ,'Y'
--                        ,'Y'
--                        ,'M'
--                        ,'Y'
--                        ,'N')
--                 ,'N') || (CASE
--                             WHEN aia.gl_date IS NOT NULL THEN
--                              'Y'
--                             ELSE
--                              'N'
--                           END) app_post_flag
--             ,decode(nvl(decode(aerh.workflow_approved_flag
--                               ,'A'
--                               ,'Y'
--                               ,'Y'
--                               ,'Y'
--                               ,'M'
--                               ,'Y'
--                               ,'N')
--                        ,'N') || (CASE
--                                    WHEN aia.gl_date IS NOT NULL THEN
--                                     'Y'
--                                    ELSE
--                                     'N'
--                                  END)
--                    ,'YY'
--                    ,'Y'
--                    ,'NN'
--                    ,'N'
--                    ,'N') approved_in_gl
--             ,aerh.vouchno
--             ,'1' query_no
--             ,'1 - Processed Transaction in AP/GL' query_descr
--             ,aerh.expense_status_code expense_report_status
--             ,apcct.reference_number reference_number
--             ,acct_struc.gcc_seg5
--             ,acct_struc.proj_descr
--             ,acct_struc.gcc_seg6
--             ,acct_struc.gcc_seg7
--             ,apcct.category --Version 1.3
--             ,aerl.attribute10 -- v2.0 start
--             ,aerl.attribute11
--             ,aerl.attribute12
--             ,aerl.attribute13
--             ,aerl.attribute14
--             ,aerh.org_id -- v2.0 end
--         FROM ap_credit_card_trxns_all apcct
--              --,ap_card_programs_all acpa          kp
--              --,ap_cards_all aca                   kp
--             ,xxcusie_creditcard_vw xcv --kp
--             ,ap_expense_reports_all aerp
--             ,ap_expense_report_headers_all aerh
--             ,ap_expense_report_lines_all aerl
--             ,ap_exp_report_dists_all aerd
--             ,ap_invoices_all aia
--             ,xxcus.xxcusoie_emp_ccid_pull_tbl pep
--             ,(SELECT lu1.meaning, lu1.lookup_code
--                 FROM applsys.fnd_lookup_values lu1
--                WHERE lu1.lookup_type = 'XXCUS_AP_MCC_CODES') codes
--             ,(SELECT gcc.code_combination_id
--                     ,prod.flex_value         gcc_seg1
--                     ,prod.description        prod_descr
--                     ,loc.flex_value          gcc_seg2
--                     ,loc.description         loc_descr
--                     ,cc.flex_value           gcc_seg3
--                     ,cc.description          cc_descr
--                     ,acc.flex_value          gcc_seg4
--                     ,acc.description         acc_descr
--                     ,proj.flex_value         gcc_seg5
--                     ,proj.description        proj_descr
--                     ,gcc.segment6            gcc_seg6
--                     ,gcc.segment7            gcc_seg7
--                 FROM gl_code_combinations gcc
--                     ,fnd_flex_values_vl   prod
--                     ,fnd_flex_values_vl   loc
--                     ,fnd_flex_values_vl   cc
--                     ,fnd_flex_values_vl   acc
--                     ,fnd_flex_values_vl   proj
--                WHERE gcc.segment1 = prod.flex_value
--                  AND gcc.segment2 = loc.flex_value
--                  AND gcc.segment3 = cc.flex_value
--                  AND gcc.segment4 = acc.flex_value
--                  AND gcc.segment5 = proj.flex_value
--                  AND prod.flex_value_set_id = l_lob_value_set_id --1014547
--                  AND loc.flex_value_set_id = l_loc_value_set_id --1014548
--                  AND cc.flex_value_set_id = l_cc_value_set_id --1014549
--                  AND acc.flex_value_set_id = l_acc_value_set_id --1014550
--                  AND proj.flex_value_set_id = l_proj_value_set_id --1014551
--               ) acct_struc
--        WHERE nvl(apcct.category, 'UNKNOWN') IN
--              ('BUSINESS', 'PERSONAL', 'UNKNOWN')
--          AND apcct.transaction_type NOT IN ('30', '31', '62') --v2.0
--          AND apcct.report_header_id IS NOT NULL
--             --and acpa.card_program_id = aca.card_program_id       kp 
--             --and aca.card_program_id = apcct.card_program_id      kp
--             --and apcct.card_number = aca.card_number              kp
--          AND apcct.card_program_id = xcv.card_program_id --kp     
--          AND apcct.card_id = xcv.card_id --kp
--          AND aerl.credit_card_trx_id = apcct.trx_id
--          AND aerl.credit_card_trx_id NOT IN
--              (SELECT xbq.credit_card_trx_id
--                 FROM xxcus.xxcusoie_bullet_train_q1_tbl xbq
--                WHERE xbq.credit_card_trx_id = aerl.credit_card_trx_id)
--          AND apcct.sic_code = codes.lookup_code(+)
--          AND xcv.employee_id = pep.person_id(+) --kp
--          AND aerl.report_header_id = aerh.report_header_id
--          AND aerp.expense_report_id = aerh.expense_report_id
--             --AND aerd.report_header_id = aerh.report_header_id   --Version 1.2
--          AND aerd.report_line_id(+) = aerl.report_line_id --Version 1.2 added outer join
--          AND aerd.code_combination_id = acct_struc.code_combination_id
--          AND aerh.vouchno = aia.invoice_id
--          AND nvl(aerl.itemization_parent_id, 0) <> -1 --Version 1.4
--       );
--  
--    COMMIT;
  
    l_sec := 'Insert Query 2 Records';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
--Added by Maha for version#3 <<Start
 OPEN bullet_train_c2;
     LOOP
     FETCH bullet_train_c2 BULK COLLECT INTO l_rec limit 10000;
     EXIT WHEN l_rec.count =0;
     IF l_rec.COUNT >0 THEN
      BEGIN
      FORALL i IN l_rec.FIRST..l_rec.LAST 
       INSERT INTO xxcus.xxcus_bullet_iexp_tbl VALUES l_rec(i);
       COMMIT;
      END;
      END IF;
     END LOOP;  
   CLOSE bullet_train_c2;
     --<<end


--Added by Maha for version#4 <<Start
 OPEN bullet_train_c3;
     LOOP
     FETCH bullet_train_c3 BULK COLLECT INTO l_rec limit 10000;
     EXIT WHEN l_rec.count =0;
     IF l_rec.COUNT >0 THEN
      BEGIN
      FORALL i IN l_rec.FIRST..l_rec.LAST 
       INSERT INTO xxcus.xxcus_bullet_iexp_tbl VALUES l_rec(i);
       COMMIT;
      END;
      END IF;
     END LOOP;  
   CLOSE bullet_train_c3;
   
fnd_file.put_line(fnd_file.log,'************************************************************');
fnd_file.put_line(fnd_file.log,'Expense report details not having valid accounting segments');
fnd_file.put_line(fnd_file.log,'************************************************************');
fnd_file.put_line(fnd_file.log,'Emloyee id'||'         '||'Expense report'||'         '||'Total'||'         '||'org id'||'         '||'Account segments');

FOR I IN (SELECT aeh.employee_id,
       aeh.invoice_num,
       aeh.total,
       aer.org_id,
       aer.segment1,
       aer.segment2,
       aer.segment3,
       aer.segment4,
       aer.segment5,
       aer.segment6,
       aer.segment7
  FROM ap_exp_report_dists_all aer
       ,ap_expense_report_headers_all aeh
          WHERE aer.report_header_id = aeh.report_header_id
            AND aer.code_combination_id IS NULL
            AND aer.report_header_id NOT IN (SELECT aerd.report_header_id
                                               FROM gl_code_combinations gcc2,
                                                    ap_exp_report_dists_all aerd
                                             WHERE aerd.code_combination_id is null
                                               AND aerd.segment1 = gcc2.segment1
                                               AND aerd.segment2 = gcc2.segment2
                                               AND aerd.segment3 = gcc2.segment3
                                               AND aerd.segment4 = gcc2.segment4
                                               AND aerd.segment5 = gcc2.segment5
                                               AND aerd.segment6 = gcc2.segment6
                                               AND aerd.segment7 = gcc2.segment7))
LOOP
BEGIN
fnd_file.put_line(fnd_file.log,i.employee_id||'         '||i.invoice_num||'         '||i.total||'         '||i.org_id||'         '||i.segment1||'-'||i.segment2||'-'||i.segment3||'-'||
i.segment4||'-'||i.segment4||'-'||i.segment6||'-'||i.segment7);
END;
END LOOP;

  --<<end ver#4

--commented by Maha for version#3
--    INSERT /*+ APPEND */
--    INTO xxcus.xxcus_bullet_iexp_tbl
--      (SELECT NULL period_name
--             ,(SELECT period_name
--                 FROM gl.gl_periods u
--                WHERE u.period_set_name = '4-4-QTR'
--                  AND start_date <= apcct.posted_date
--                  AND end_date >= apcct.posted_date
--                  AND period_num != 13) cc_fiscal_period
--             ,acct_struc.gcc_seg1 oracle_product
--             ,acct_struc.prod_descr product_descr
--             ,acct_struc.gcc_seg2 oracle_location
--             ,acct_struc.loc_descr location_descr
--             ,acct_struc.gcc_seg3 oracle_cost_center
--             ,acct_struc.cc_descr cost_center_descr
--             ,acct_struc.gcc_seg4 oracle_account
--             ,acct_struc.acc_descr account_descr
--             ,acct_struc.gcc_seg1 || '.' || acct_struc.gcc_seg2 || '.' ||
--              acct_struc.gcc_seg3 || '.' || acct_struc.gcc_seg4 oracle_accounts
--             ,(CASE
--                WHEN (aerl.item_description IS NULL AND
--                     ((nvl(apcct.sic_code, 0) = 0) OR codes.meaning IS NULL)) THEN
--                 'NOT MAPPED'
--                ELSE
--                 nvl(TRIM(aerl.item_description), 'NO DESCRIPTION')
--              END) item_description
--             ,(SELECT DISTINCT flv.tag category
--                 FROM applsys.fnd_lookup_values flv
--                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
--                  AND flv.meaning = (CASE
--                        WHEN (aerl.item_description IS NULL AND
--                             ((nvl(apcct.sic_code, 0) = 0) OR
--                             codes.meaning IS NULL)) THEN
--                         'NOT MAPPED'
--                        ELSE
--                         nvl(aerl.item_description, 'NOT MAPPED')
--                      END)
--                  AND rownum < 2) category
--             ,(SELECT DISTINCT flv.description sub_category
--                 FROM applsys.fnd_lookup_values flv
--                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
--                  AND flv.meaning = (CASE
--                        WHEN (aerl.item_description IS NULL AND
--                             ((nvl(apcct.sic_code, 0) = 0) OR
--                             codes.meaning IS NULL)) THEN
--                         'NOT MAPPED'
--                        ELSE
--                         nvl(aerl.item_description, 'NOT MAPPED')
--                      END)
--                  AND rownum < 2) sub_category
--             ,(CASE
--                WHEN aerl.item_description = 'Personal' THEN
--                 (nvl(aerd.amount, aerl.amount) * -1) --added NVL 09112012
--                ELSE
--                --nvl(aerd.amount, apcct.billed_amount)               --09112012 
--                 nvl(aerd.amount, aerl.amount)
--              END) line_amount
--             ,aerl.currency_code currency_code
--             ,aerh.invoice_num
--             ,aerl.distribution_line_number || '.' ||
--              (aerd.sequence_num + 1) distribution_line_number
--             ,pep.full_name
--             ,pep.employee_number
--             ,pep.segment1 emp_default_prod
--             ,pep.segment2 emp_default_loc
--             ,pep.segment3 emp_default_costctr
--             ,pep.fru
--             ,aerh.creation_date
--             ,aerh.report_submitted_date
--             ,aerh.description
--             ,aerl.merchant_name vendor_name
--             ,apcct.merchant_name1
--             ,NULL business_purpose
--             ,aerl.justification
--             ,aerl.start_expense_date
--             ,aerl.end_expense_date
--             ,NULL remarks
--             ,aerl.attendees
--             ,NULL attendees_emp
--             ,NULL attendees_te
--             ,NULL attendees_cti1
--             ,NULL attendees_cti2
--             ,apcct.trx_id
--             ,xcv.chname cardmember_name --aca.cardmember_name,    kp
--             ,apcct.transaction_date
--             ,apcct.posted_date trans_post_date
--             ,apcct.merchant_city || ', ' || apcct.merchant_province_state merchant_location
--             ,aerl.destination_from
--             ,aerl.destination_to
--             ,aerl.distance_unit_code
--             ,aerl.daily_distance
--             ,aerl.trip_distance
--             ,aerl.avg_mileage_rate
--             ,aerh.accounting_date dt_sentto_gl
--             ,aerh.report_header_id expense_report_number
--             ,xcv.card_program_id --acpa.card_program_id,    kp
--             ,xcv.card_program_name --  acpa.card_program_name,    kp
--             ,nvl(apcct.sic_code, 0) mcc_code_no
--             ,(CASE
--                WHEN ((nvl(apcct.sic_code, 0) = 0) OR codes.meaning IS NULL) THEN
--                 'NOT MAPPED'
--                ELSE
--                 codes.meaning
--              END) mcc_code
--             ,aerh.accounting_date accounting_date
--             ,aerh.total report_total
--             ,aerl.daily_distance
--             ,aerl.avg_mileage_rate
--             ,aerl.attribute_category
--             ,aerl.category_code
--             ,apcct.transaction_amount
--             ,aerl.receipt_currency_amount
--             ,aerh.amt_due_ccard_company
--             ,aerh.amt_due_employee
--             ,decode(aerh.source, 'SelfService', 'Y', 'N') imported_to_ap
--             ,(CASE
--                WHEN aerh.accounting_date IS NOT NULL THEN
--                 'Y'
--                ELSE
--                 'N'
--              END) imported_to_gl
--             ,nvl(decode(aerh.workflow_approved_flag
--                        ,'A'
--                        ,'Y'
--                        ,'Y'
--                        ,'Y'
--                        ,'M'
--                        ,'Y'
--                        ,'N')
--                 ,'N') workflow_approved_flag
--             ,aerh.expense_current_approver_id
--             ,aerh.override_approver_id approver_id
--             ,aerh.override_approver_name approver_name
--             ,aerp.report_type
--             ,aerh.advance_invoice_to_apply
--             ,aerh.prepay_dist_num advance_distribution_number
--             ,nvl(aerh.prepay_apply_flag, 'N') advance_flag
--             ,aerh.prepay_gl_date advance_gl_date
--             ,aerh.prepay_num advance_number
--             ,aerh.reject_code report_reject_code
--             ,nvl(decode(aerh.workflow_approved_flag
--                        ,'A'
--                        ,'Y'
--                        ,'Y'
--                        ,'Y'
--                        ,'M'
--                        ,'Y'
--                        ,'N')
--                 ,'N') || (CASE
--                             WHEN aerh.accounting_date IS NOT NULL THEN
--                              'Y'
--                             ELSE
--                              'N'
--                           END) app_post_flag
--             ,decode(nvl(decode(aerh.workflow_approved_flag
--                               ,'A'
--                               ,'Y'
--                               ,'Y'
--                               ,'Y'
--                               ,'M'
--                               ,'Y'
--                               ,'N')
--                        ,'N') || (CASE
--                                    WHEN aerh.accounting_date IS NOT NULL THEN
--                                     'Y'
--                                    ELSE
--                                     'N'
--                                  END)
--                    ,'YY'
--                    ,'Y'
--                    ,'NN'
--                    ,'N'
--                    ,'N') approved_in_gl
--             ,aerh.vouchno
--             ,'2' query_no
--             ,'2 - In iExpense Not processed through to AP' query_descr
--             ,aerh.expense_status_code expense_report_status
--             ,apcct.reference_number reference_number
--             ,acct_struc.gcc_seg5
--             ,acct_struc.proj_descr
--             ,acct_struc.gcc_seg6
--             ,acct_struc.gcc_seg7
--             ,apcct.category --Version 1.3
--             ,aerl.attribute10 -- v2.0 start
--             ,aerl.attribute11
--             ,aerl.attribute12
--             ,aerl.attribute13
--             ,aerl.attribute14
--             ,aerh.org_id -- v2.0 end
--         FROM ap_credit_card_trxns_all apcct
--              --ap_card_programs_all acpa,           kp
--              --ap_cards_all aca,                    kp
--             ,xxcusie_creditcard_vw xcv --kp
--             ,ap_expense_reports_all aerp
--             ,ap_expense_report_headers_all aerh
--             ,ap_expense_report_lines_all aerl
--             ,ap_exp_report_dists_all aerd
--             ,xxcus.xxcusoie_emp_ccid_pull_tbl pep
--             ,(SELECT lu1.meaning, lu1.lookup_code
--                 FROM applsys.fnd_lookup_values lu1
--                WHERE lu1.lookup_type = 'XXCUS_AP_MCC_CODES') codes
--             ,(SELECT gcc.code_combination_id
--                     ,prod.flex_value         gcc_seg1
--                     ,prod.description        prod_descr
--                     ,loc.flex_value          gcc_seg2
--                     ,loc.description         loc_descr
--                     ,cc.flex_value           gcc_seg3
--                     ,cc.description          cc_descr
--                     ,acc.flex_value          gcc_seg4
--                     ,acc.description         acc_descr
--                     ,proj.flex_value         gcc_seg5
--                     ,proj.description        proj_descr
--                     ,gcc.segment6            gcc_seg6
--                     ,gcc.segment7            gcc_seg7
--                 FROM gl_code_combinations gcc
--                     ,fnd_flex_values_vl   prod
--                     ,fnd_flex_values_vl   loc
--                     ,fnd_flex_values_vl   cc
--                     ,fnd_flex_values_vl   acc
--                     ,fnd_flex_values_vl   proj
--                WHERE gcc.segment1 = prod.flex_value
--                  AND gcc.segment2 = loc.flex_value
--                  AND gcc.segment3 = cc.flex_value
--                  AND gcc.segment4 = acc.flex_value
--                  AND gcc.segment5 = proj.flex_value
--                  AND prod.flex_value_set_id = l_lob_value_set_id --1014547
--                  AND loc.flex_value_set_id = l_loc_value_set_id --1014548
--                  AND cc.flex_value_set_id = l_cc_value_set_id --1014549
--                  AND acc.flex_value_set_id = l_acc_value_set_id --1014550
--                  AND proj.flex_value_set_id = l_proj_value_set_id --1014551
--               ) acct_struc
--        WHERE nvl(apcct.category, 'UNKNOWN') IN
--              ('BUSINESS', 'PERSONAL', 'UNKNOWN')
--          AND apcct.transaction_type NOT IN ('30', '31', '62') --v2.0
--          AND apcct.report_header_id IS NOT NULL
--             --and acpa.card_program_id = aca.card_program_id       kp 
--             --and aca.card_program_id = apcct.card_program_id      kp
--             --and apcct.card_number = aca.card_number              kp
--          AND apcct.card_program_id = xcv.card_program_id --kp     
--          AND apcct.card_id = xcv.card_id --kp
--          AND xcv.employee_id = pep.person_id(+) --kp
--          AND apcct.sic_code = codes.lookup_code(+)
--          AND aerl.credit_card_trx_id = apcct.trx_id
--          AND aerl.report_header_id = aerh.report_header_id
--          AND aerp.expense_report_id = aerh.expense_report_id
--             --AND aerd.report_header_id = aerh.report_header_id  --Version 1.2
--          AND aerd.report_line_id(+) = aerl.report_line_id --Version 1.2 added outer join
--          AND acct_struc.code_combination_id =
--              (CASE
--                 WHEN aerd.code_combination_id IS NOT NULL THEN
--                  aerd.code_combination_id
--                 WHEN aerd.code_combination_id IS NULL OR
--                      aerd.code_combination_id IN (200, -1) THEN
--                  (CASE
--                    WHEN aerl.code_combination_id IS NOT NULL THEN
--                     aerl.code_combination_id
--                    WHEN aerl.code_combination_id IS NULL THEN
--                     (SELECT gcc2.code_combination_id
--                        FROM gl.gl_code_combinations gcc2
--                       WHERE nvl(aerd.segment1, pep.segment1) = gcc2.segment1
--                         AND nvl(aerd.segment2, pep.segment2) = gcc2.segment2
--                         AND nvl(aerd.segment3, pep.segment3) = gcc2.segment3
--                         AND nvl(aerd.segment4
--                                ,get_exp_rep_param_info(aerh.expense_report_id
--                                                       ,apcct.folio_type
--                                                       ,'ACCOUNT')) =
--                             gcc2.segment4
--                         AND nvl(aerd.segment5, '00000') = gcc2.segment5
--                         AND nvl(aerd.segment6, '00000') = gcc2.segment6
--                         AND nvl(aerd.segment7, '00000') = gcc2.segment7)
--                  END)
--               END)
--          AND nvl(aerh.vouchno, 0) = 0
--          AND nvl(aerl.itemization_parent_id, 0) <> -1) --Version 1.4
--    ;
  
 --   COMMIT;
  
    l_sec := 'Insert Query 3 Records';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    INSERT /*+ APPEND */
    INTO xxcus.xxcus_bullet_iexp_tbl
      (SELECT NULL period_name
             ,(SELECT period_name
                 FROM gl.gl_periods u
                WHERE u.period_set_name = '4-4-QTR'
                  AND start_date <= apcct.posted_date
                  AND end_date >= apcct.posted_date
                  AND period_num != 13) cc_fiscal_period
             ,fv_prod.flex_value
             ,fv_prod.description
             ,fv_location.flex_value
             ,fv_location.description
             ,fv_cost_ctr.flex_value
             ,fv_cost_ctr.description
             ,get_exp_rep_param_info(lkp_val.tag
                                    ,apcct.folio_type
                                    ,'ACCOUNT') acct
             ,(SELECT description
                 FROM fnd_flex_values_vl
                WHERE flex_value_set_id = l_acc_value_set_id --1014550
                  AND flex_value =
                      get_exp_rep_param_info(lkp_val.tag
                                            ,apcct.folio_type
                                            ,'ACCOUNT')) acct_desc
             ,fv_prod.flex_value || '.' || fv_location.flex_value || '.' ||
              fv_cost_ctr.flex_value || '.' ||
              get_exp_rep_param_info(lkp_val.tag
                                    ,apcct.folio_type
                                    ,'ACCOUNT') oracle_accounts
             ,nvl((CASE
                    WHEN acpa.card_program_name =
                         'WC PNC Inventory Card Program' THEN
                     'INVENTORY - CREDIT CARD PURCHASES'
                    WHEN acpa.card_program_name = 'HDS Company Pay Program' THEN
                     get_exp_rep_param_info(lkp_val.tag
                                           ,apcct.folio_type
                                           ,'PROMPT')
                    WHEN acpa.card_program_name =
                         'CB PNC Employee-Pay Card Program' THEN
                     get_exp_rep_param_info(lkp_val.tag
                                           ,apcct.folio_type
                                           ,'PROMPT')
                    ELSE
                     'UNKNOWN'
                  END)
                 ,'NOT MAPPED') item_description
             ,nvl((SELECT DISTINCT flv.tag category
                    FROM applsys.fnd_lookup_values flv
                   WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
                     AND flv.meaning = (CASE
                           WHEN acpa.card_program_name =
                                'WC PNC Inventory Card Program' THEN
                            'INVENTORY - CREDIT CARD PURCHASES'
                           WHEN acpa.card_program_name =
                                'HDS Company Pay Program' THEN
                            get_exp_rep_param_info(lkp_val.tag
                                                  ,apcct.folio_type
                                                  ,'PROMPT')
                           WHEN acpa.card_program_name LIKE
                                'CB PNC Employee-Pay Card Program' THEN
                            get_exp_rep_param_info(lkp_val.tag
                                                  ,apcct.folio_type
                                                  ,'PROMPT')
                           ELSE
                            'UNKNOWN'
                         END))
                 ,'UNKNOWN') category
             ,nvl((SELECT DISTINCT flv.description sub_category
                    FROM applsys.fnd_lookup_values flv
                   WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
                     AND flv.meaning = (CASE
                           WHEN acpa.card_program_name =
                                'WC PNC Inventory Card Program' THEN
                            'INVENTORY - CREDIT CARD PURCHASES'
                           WHEN acpa.card_program_name =
                                'HDS Company Pay Program' THEN
                            xxcusoie_bullettrain_extr_pkg.get_exp_rep_param_info(lkp_val.tag
                                                                                ,apcct.folio_type
                                                                                ,'PROMPT')
                           WHEN acpa.card_program_name LIKE
                                'CB PNC Employee-Pay Card Program' THEN
                            xxcusoie_bullettrain_extr_pkg.get_exp_rep_param_info(lkp_val.tag
                                                                                ,apcct.folio_type
                                                                                ,'PROMPT')
                           ELSE
                            'UNKNOWN'
                         END))
                 ,'UNKNOWN') sub_category
             ,apcct.billed_amount line_amount
             ,apcct.billed_currency_code currency_code
             ,NULL invoice_number
             ,NULL dist_line_number
             ,pep.full_name
             ,pep.employee_number
             ,pep.segment1 emp_default_prod
             ,pep.segment2 emp_default_loc
             ,pep.segment3 emp_default_costctr
             ,pep.fru
             ,NULL create_dt
             ,NULL sub_dt
             ,NULL rep_desc
             ,apcct.merchant_name1 vendor_name
             ,apcct.merchant_name1
             ,NULL busi_purp
             ,NULL justification
             ,NULL st_exp_dt
             ,NULL end_exp_dt
             ,NULL remarks
             ,NULL attendees
             ,NULL attendees_emp
             ,NULL attendees_te
             ,NULL attendees_cti1
             ,NULL attendess_cti2
             ,apcct.trx_id cc_trx_id
             ,xcv.chname cardmember_name --aca.cardmember_name,    kp
             ,apcct.transaction_date
             ,apcct.posted_date
             ,apcct.merchant_city || ', ' || apcct.merchant_province_state merchant_location
             ,NULL destination_from
             ,NULL destination_to
             ,NULL distance_unit_code
             ,NULL daily_distance
             ,NULL trip_distance
             ,NULL avg_mileage_rate
             ,NULL dt_sentto_gl
             ,NULL expense_report_number
             ,acpa.card_program_id
             ,acpa.card_program_name
             ,nvl(apcct.sic_code, 0) mcc_code_no
             ,(CASE
                WHEN nvl(apcct.sic_code, 0) = 0 THEN
                 'NOT MAPPED'
                ELSE
                 nvl(codes.meaning, 'NOT MAPPED')
              END) mcc_code
             ,NULL accounting_date
             ,NULL report_total
             ,NULL daily_distance
             ,NULL avg_mileage_rate
             ,NULL attribute_category
             ,NULL category_code
             ,apcct.transaction_amount
             ,NULL receipt_currency_amount
             ,NULL amt_due_ccard_company
             ,NULL amt_due_employee
             ,'N' imported_to_ap
             ,'N' imported_to_gl
             ,NULL workflow_approved_flag
             ,NULL expense_current_approver_id
             ,NULL approver_id
             ,NULL approver_name
             ,NULL report_type
             ,NULL advance_invoice_to_apply
             ,NULL advance_distribution_number
             ,NULL advance_flag
             ,NULL advance_gl_date
             ,NULL advance_number
             ,NULL report_reject_code
             ,NULL app_post_flag
             ,NULL approved_in_gl
             ,NULL app_post_flag
             ,'3' query_no
             ,'3 - CC Transactions not in iExpense, AP or GL' query_descr
             ,NULL expense_report_status
             ,apcct.reference_number reference_number
             ,'00000' proj
             ,'DEFAULT' proj_desc
             ,'00000' futrure_use1
             ,'00000' future_use2
             ,apcct.category --Version 1.3
             ,NULL attribute10 -- v2.0 start
             ,NULL attribute11
             ,NULL attribute12
             ,NULL attribute13
             ,NULL attribute14
             ,apcct.org_id -- v2.0 end
         FROM ap_credit_card_trxns_all apcct
              --ap_cards_all aca,                  kp
             ,xxcusie_creditcard_vw xcv --kp
             ,ap_card_programs_all acpa
             ,(SELECT lu1.meaning, lu1.lookup_code
                 FROM applsys.fnd_lookup_values lu1
                WHERE lu1.lookup_type = 'XXCUS_AP_MCC_CODES') codes
             ,xxcus.xxcusoie_emp_ccid_pull_tbl pep
             ,fnd_lookup_values lkp_val
             ,(SELECT fvt1.description, fvt1.flex_value
                 FROM apps.fnd_flex_values_vl fvt1
                WHERE fvt1.flex_value_set_id = l_lob_value_set_id --1014547
               ) fv_prod
             ,(SELECT fvt2.description, fvt2.flex_value
                 FROM apps.fnd_flex_values_vl fvt2
                WHERE fvt2.flex_value_set_id = l_loc_value_set_id) fv_location
             ,(SELECT fvt3.description, fvt3.flex_value
                 FROM apps.fnd_flex_values_vl fvt3
                WHERE fvt3.flex_value_set_id = l_cc_value_set_id) fv_cost_ctr
        WHERE apcct.report_header_id IS NULL
          AND apcct.transaction_type NOT IN ('30', '31', '62') --v2.0
          AND nvl(apcct.category, 'UNKNOWN') IN
              ('BUSINESS', 'PERSONAL', 'UNKNOWN')
          AND xcv.card_program_id = acpa.card_program_id
          AND (xcv.employee_id IS NOT NULL OR
              xcv.card_number LIKE '%000080899') --kp
             --AND apcct.card_number = aca.card_number
          AND apcct.card_id = xcv.card_id --kp
          AND apcct.card_program_id = acpa.card_program_id
          AND apcct.sic_code = codes.lookup_code(+)
          AND xcv.employee_id = pep.person_id(+) --kp
          AND lkp_val.lookup_type = 'HDS_AP_EXPENSE_TEMPLATE'
          AND (CASE
                WHEN pep.segment1 = lkp_val.lookup_code THEN
                 1
                WHEN pep.segment1 NOT IN
                     (SELECT lookup_code
                        FROM fnd_lookup_values
                       WHERE lookup_type = 'HDS_AP_EXPENSE_TEMPLATE') AND
                     lkp_val.lookup_code = '0' THEN
                 1
              END) = 1
          AND pep.segment1 = fv_prod.flex_value(+)
          AND pep.segment2 = fv_location.flex_value(+)
          AND pep.segment3 = fv_cost_ctr.flex_value(+));
  
    COMMIT;
  
    DELETE /*+ index(y, XXHSI_BULLET_IEXP_TBL_N1) */
    FROM xxcus.xxcus_bullet_iexp_tbl y
     WHERE y.query_num = '1'
       AND y.credit_card_trx_id IN
           (SELECT yy.credit_card_trx_id
              FROM xxcus.xxcus_bullet_iexp_tbl yy
             WHERE yy.query_num IN ('2', '3')
               AND yy.credit_card_trx_id = y.credit_card_trx_id)
       AND y.credit_card_trx_id IS NOT NULL;
  
    COMMIT;
  
    --Insert Global Header GTT
    l_sec := 'Insert Query 4; ';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcus_bullet_iexp_tbl
      (SELECT (CASE
                WHEN ai.gl_date IS NOT NULL THEN
                 (SELECT gp.period_name
                    FROM gl.gl_periods gp
                   WHERE gp.period_set_name = '4-4-QTR'
                     AND gp.start_date <= ai.gl_date
                     AND gp.end_date >= ai.gl_date
                     AND gp.period_num != 13)
                ELSE
                 NULL
              END) period_name
             ,NULL cc_fiscal_period
             ,line_acct.segment1 oracle_product
             ,(SELECT fvt.description
                 FROM apps.fnd_flex_values_tl fvt, apps.fnd_flex_values ffv
                WHERE fvt.flex_value_id = ffv.flex_value_id
                  AND ffv.flex_value_set_id = l_lob_value_set_id --1014547
                  AND ffv.flex_value = line_acct.segment1) product_descrip
             ,line_acct.segment2 oracle_location
             ,(SELECT fvt.description
                 FROM apps.fnd_flex_values_tl fvt, apps.fnd_flex_values ffv
                WHERE fvt.flex_value_id = ffv.flex_value_id
                  AND ffv.flex_value_set_id = l_loc_value_set_id --1014548
                  AND ffv.flex_value = line_acct.segment2) location_descrip
             ,line_acct.segment3 oracle_cost_center
             ,(SELECT fvt.description
                 FROM apps.fnd_flex_values_tl fvt, apps.fnd_flex_values ffv
                WHERE fvt.flex_value_id = ffv.flex_value_id
                  AND ffv.flex_value_set_id = l_cc_value_set_id --1014549
                  AND ffv.flex_value = line_acct.segment3) cost_center_descrip
             ,line_acct.segment4 oracle_account
             ,(SELECT fvt.description
                 FROM apps.fnd_flex_values_tl fvt, apps.fnd_flex_values ffv
                WHERE fvt.flex_value_id = ffv.flex_value_id
                  AND ffv.flex_value_set_id = l_acc_value_set_id --1014550
                  AND ffv.flex_value = line_acct.segment4) account_descrip
             ,line_acct.segment1 || '.' || line_acct.segment2 || '.' ||
              line_acct.segment3 || '.' || line_acct.segment4 oracle_accounts
             ,exp.item_description
             ,(SELECT DISTINCT flv.tag category
                 FROM applsys.fnd_lookup_values flv
                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
                  AND flv.meaning = exp.item_description) category
             ,(SELECT DISTINCT flv.description sub_category
                 FROM applsys.fnd_lookup_values flv
                WHERE flv.lookup_type = 'HDS_FINANCE_IEXP_CATEGORIES'
                  AND flv.meaning = exp.item_description) sub_category
             ,exp.line_amount
             ,exp.currency_code
             ,exp.invoice_num
             ,exp.distribution_line_number
             ,emp.full_name
             ,emp.employee_number
             ,empdflt.segment1 emp_default_prod
             ,empdflt.segment2 emp_default_loc
             ,empdflt.segment3 emp_default_costctr
             ,emp.attribute1 emp_branch
             ,trunc(exp.creation_date) creation_date
             ,exp.report_submitted_date
             ,exp.description
             ,exp.merchant_name vendor_name
             ,NULL merchant_name1 --Only for CC trx 
             ,exp.business_purpose
             ,exp.justification
             ,exp.start_expense_date
             ,exp.end_expense_date
             ,exp.remarks
             ,exp.attendees
             ,exp.attendees_emp
             ,exp.attendees_te
             ,exp.attendees_cti1
             ,exp.attendees_cti2
             ,exp.credit_card_trx_id
             ,NULL cardmember_name --aca.cardmember_name
             ,NULL transaction_date --Credit Card Transaction Date
             ,NULL posted_date --Only for CC Trx - Visa posted date 
             ,NULL merchant_location
             ,exp.destination_from
             ,exp.destination_to
             ,exp.distance_unit_code
             ,exp.daily_distance
             ,exp.trip_distance
             ,exp.avg_mileage_rate
             ,ai.gl_date dt_sentto_gl
             ,exp.expense_report_num expense_report_number
             ,NULL card_program_id
             ,NULL
             ,0 mcc_code_no
             ,'NOT MAPPED' mcc_code
             ,ai.gl_date accounting_date
             ,exp.report_total
             ,NULL miles
             ,NULL rate_per_mile
             ,exp.attribute_category
             ,exp.category_code
             ,NULL transaction_amount --Credit Card Transaction Amount
             ,exp.receipt_currency_amount
             ,exp.amt_due_ccard_company
             ,exp.amt_due_employee
             ,nvl(exp.imported_to_ap, 'N') imported_to_ap
             ,decode(ai.gl_date, NULL, 'N', 'Y') posted_to_gl
             ,nvl(decode(exp.workflow_approved_flag
                        ,'A'
                        ,'Y'
                        ,'Y'
                        ,'Y'
                        ,'M'
                        ,'Y'
                        ,'N')
                 ,'N') workflow_approved_flag
             ,exp.expense_current_approver_id
             ,exp.override_approver_id approver_id
             ,exp.override_approver_name approver_name
             ,exprp.report_type report_type
             ,aia.invoice_num advance_invoice_to_apply
             ,exp.advance_distribution_number
             ,exp.advance_flag
             ,exp.advance_gl_date
             ,exp.advance_number
             ,exp.report_reject_code
             ,exp.workflow_approved_flag ||
              decode(ai.gl_date, NULL, 'N', 'Y') app_post_flag
             ,decode(exp.workflow_approved_flag ||
                     decode(ai.gl_date, NULL, 'N', 'Y')
                    ,'MY'
                    ,'N'
                    ,'YY'
                    ,'N'
                    ,'AY'
                    ,'N'
                    ,'Y') approved_in_gl
             ,exp.vouchno
             ,'4' query_no
             ,'4 - Out of Pocket Expenses' query_descr
             ,exp.expense_report_status
             ,NULL reference_number
             ,line_acct.segment5 project_number
             ,(SELECT fvt.description
                 FROM apps.fnd_flex_values_tl fvt, apps.fnd_flex_values ffv
                WHERE fvt.flex_value_id = ffv.flex_value_id
                  AND ffv.flex_value_set_id = l_proj_value_set_id
                  AND ffv.flex_value = line_acct.segment5) project_name
             ,'00000'
             ,'00000'
             ,NULL category --Version 1.3
             ,exp.attribute10 -- v2.0 start
             ,exp.attribute11
             ,exp.attribute12
             ,exp.attribute13
             ,exp.attribute14
             ,exp.org_id_h -- v2.0 end
         FROM ap.ap_invoices_all ai
             ,ap.ap_invoices_all aia
             ,gl.gl_code_combinations line_acct
             ,gl.gl_code_combinations empdflt
             ,hr.per_all_people_f emp
             ,ap.ap_expense_reports_all exprp
             ,(SELECT aerd.amount line_amount --exprl.amount 
                     ,exprh.invoice_num
                     ,exprl.distribution_line_number || '.' ||
                      (aerd.sequence_num + 1) distribution_line_number
                     ,exprh.accounting_date
                     ,exprh.description
                     ,exprh.total report_total
                     ,exprh.creation_date
                     ,exprh.report_submitted_date
                     ,exprl.item_description
                     ,exprl.daily_distance
                     ,exprl.trip_distance
                     ,exprl.distance_unit_code
                     ,exprl.avg_mileage_rate
                     ,exprl.destination_from
                     ,exprl.destination_to
                     ,exprl.justification
                     ,exprl.credit_card_trx_id
                     ,exprl.start_expense_date
                     ,exprl.end_expense_date
                     ,NULL remarks
                     ,exprl.attendees
                     ,NULL attendees_emp
                     ,NULL business_purpose
                     ,NULL attendees_te
                     ,NULL attendees_cti1
                     ,NULL attendees_cti2
                     ,exprl.currency_code
                     ,exprl.receipt_currency_amount
                     ,decode(exprh.source, 'SelfService', 'Y', 'N') imported_to_ap
                     ,exprh.employee_id
                     ,exprh.employee_ccid
                     ,aerd.code_combination_id code_combination_id
                     ,exprh.advance_invoice_to_apply
                     ,exprh.vouchno
                     ,exprh.org_id org_id_h
                     ,exprl.org_id org_id_l
                     ,exprh.expense_report_id
                     ,exprl.submitted_amount
                     ,exprl.attribute_category
                     ,exprl.category_code
                     ,exprh.amt_due_employee
                     ,exprh.amt_due_ccard_company
                     ,exprh.workflow_approved_flag
                     ,exprh.expense_current_approver_id
                     ,exprh.override_approver_id
                     ,exprh.override_approver_name
                     ,exprh.prepay_apply_amount advance_amount
                     ,exprh.prepay_dist_num advance_distribution_number
                     ,nvl(prepay_apply_flag, 'N') advance_flag
                     ,exprh.prepay_gl_date advance_gl_date
                     ,exprh.prepay_num advance_number
                     ,exprl.merchant_name merchant_name
                     ,exprh.reject_code report_reject_code
                     ,exprh.report_header_id expense_report_num
                     ,exprh.expense_status_code expense_report_status
                     ,exprl.attribute10 -- v2.0 start
                     ,exprl.attribute11
                     ,exprl.attribute12
                     ,exprl.attribute13
                     ,exprl.attribute14
               -- v2.0 end
                 FROM ap.ap_expense_report_lines_all   exprl
                     ,ap.ap_expense_report_headers_all exprh
                     ,ap.ap_exp_report_dists_all       aerd
                WHERE exprl.report_header_id = exprh.report_header_id
                  AND exprl.report_line_id = aerd.report_line_id
                  AND exprl.credit_card_trx_id IS NULL) exp
             ,(SELECT d.person_id, d.full_name app_full_name
                 FROM hr.per_all_people_f d
                WHERE d.effective_end_date =
                      (SELECT MAX(d_ed.effective_end_date)
                         FROM hr.per_all_people_f d_ed
                        WHERE d.person_id = d_ed.person_id)) appid
        WHERE exp.vouchno = ai.invoice_id(+)
          AND exp.expense_report_id = exprp.expense_report_id(+)
          AND exp.expense_current_approver_id = appid.person_id(+)
          AND exp.employee_id = emp.person_id(+)
          AND (exp.employee_id IS NULL OR
              to_char(emp.effective_start_date, 'YYYYMMDDHH24MISS') ||
              to_char(emp.effective_end_date, 'YYYYMMDDHH24MISS') =
              (SELECT MAX(to_char(effective_start_date, 'YYYYMMDDHH24MISS') ||
                           to_char(effective_end_date, 'YYYYMMDDHH24MISS'))
                  FROM hr.per_all_people_f
                 WHERE person_id = exp.employee_id))
          AND exp.employee_ccid = empdflt.code_combination_id(+)
          AND exp.code_combination_id = line_acct.code_combination_id(+)
          AND exp.advance_invoice_to_apply = aia.invoice_id(+)
          AND exp.credit_card_trx_id IS NULL
          AND line_acct.segment4 != '211125');
  
    COMMIT;
  
    l_sec := 'Insert Query 5 Records';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcus_bullet_iexp_tbl
      (SELECT NULL period_name
             ,(SELECT period_name
                 FROM gl.gl_periods /*@prd*/ u
                WHERE u.period_set_name = '4-4-QTR'
                  AND start_date <= apcct.posted_date
                  AND end_date >= apcct.posted_date
                  AND period_num != 13) cc_fiscal_period
             ,pep.segment1
             ,NULL
             ,pep.segment2
             ,NULL
             ,pep.segment3
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,apcct.billed_amount --apcct.transaction_amount   Version 1.2
             ,apcct.billed_currency_code --NULL   -- V2.0
             ,NULL
             ,NULL
             ,pep.full_name
             ,pep.employee_number
             ,pep.segment1 emp_default_prod
             ,pep.segment2 emp_default_loc
             ,pep.segment3 emp_default_costctr
             ,pep.fru emp_branch
             ,trunc(apcct.creation_date) creation_date
             ,NULL
             ,NULL
             ,NULL
             ,apcct.merchant_name1
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,apcct.trx_id credit_card_trx_id
             ,cards.cardmember_name
             ,apcct.transaction_date
             ,apcct.posted_date trans_post_date
             ,apcct.merchant_city || ', ' || apcct.merchant_province_state merchant_location
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,nvl(apcct.report_header_id, '') expense_report_number
             ,cards.card_program_id
             ,cards.card_program_name
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,NULL
             ,'5' query_no
             ,'5 - Catch All CC Transactions that were not included in other queries. Period is the period CC transaction was Posted to the CC.' query_descr
             ,NULL expense_report_status
             ,NULL reference_number
             ,'00000' project_number
             ,'DEFAULT' project_name
             ,'00000' segment6
             ,'00000' segment7
             ,apcct.category --Version 1.3
             ,NULL attribute10 -- v2.0 start
             ,NULL attribute11
             ,NULL attribute12
             ,NULL attribute13
             ,NULL attribute14
             ,apcct.org_id -- v2.0 end
         FROM ap.ap_credit_card_trxns_all apcct
             ,(SELECT xcv.employee_id
                     ,xcv.card_number
                     ,xcv.cardmember_name
                     ,xcv.card_program_name
                     ,xcv.card_program_id
                     ,xcv.card_id
                 FROM xxcusie_creditcard_vw xcv
               --ap.ap_cards_all aca, ap.ap_card_programs_all acpa     kp
                WHERE --aca.card_program_id = acpa.card_program_id       kp
                (xcv.employee_id IS NOT NULL OR
                xcv.card_number LIKE '%000080899')) cards
             ,(SELECT DISTINCT a.employee_number
                              ,a.person_id
                              ,a.last_name
                              ,a.first_name
                              ,a.full_name
                              ,a.segment1
                              ,a.segment2
                              ,a.segment3
                              ,lkp_val.tag         template_id
                              ,lkp_val.description template_name
                              ,a.fru
                 FROM xxcus.xxcusoie_emp_ccid_pull_tbl a
                     ,fnd_lookup_values                lkp_val
                WHERE lkp_val.lookup_type = 'HDS_AP_EXPENSE_TEMPLATE'
                  AND (CASE
                        WHEN a.segment1 = lkp_val.lookup_code THEN
                         1
                        WHEN a.segment1 NOT IN
                             (SELECT lookup_code
                                FROM fnd_lookup_values
                               WHERE lookup_type = 'HDS_AP_EXPENSE_TEMPLATE') AND
                             lkp_val.lookup_code = '0' THEN
                         1
                      END) = 1) pep
        WHERE apcct.card_id = cards.card_id(+)
          AND apcct.card_program_id = cards.card_program_id(+)
          AND cards.employee_id = pep.person_id(+)
          AND apcct.transaction_type NOT IN ('30', '31', '62') --v2.0
          AND nvl(apcct.category, 'UNKNOWN') IN
              ('BUSINESS', 'PERSONAL', 'UNKNOWN')
          AND apcct.trx_id NOT IN
              (SELECT /*+ index(y, XXCUS_BULLET_IEXP_TBL_N2) */
                r.credit_card_trx_id
                 FROM xxcus.xxcus_bullet_iexp_tbl r
                WHERE r.credit_card_trx_id = apcct.trx_id));
  
    COMMIT;
--Commented below Appxcmn delivery portion for Ver# 6.0 <<Start
/*
    --Move iEXP data to APXCMMN
    --Remove for DEV instances       
    l_sec := 'Insert to APXCMMN; ';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || 'for ' ||
                      l_sec);
  
    apxcmmn.hds_apxcmmn_ddl_pkg.truncate_iexpense_view@apxprd_lnk.hsi.hughessupply.com;
  
    INSERT INTO apxcmmn.hds_bullet_iexp_tbl@apxprd_lnk.hsi.hughessupply.com
      (SELECT period_name
             ,cc_fiscal_period
             ,oracle_product
             ,product_descr
             ,oracle_location
             ,location_descr
             ,oracle_cost_center
             ,cost_center_descr
             ,oracle_account
             ,account_descr
             ,oracle_accounts
             ,item_description
             ,category
             ,sub_category
             ,line_amount
             ,currency_code
             ,invoice_number
             ,distribution_line_number
             ,full_name
             ,employee_number
             ,emp_default_prod
             ,emp_default_loc
             ,emp_default_costctr
             ,fru
             ,creation_date
             ,report_submitted_date
             ,description
             ,vendor_name
             ,merchant_name
             ,business_purpose
             ,justification
             ,start_expense_date
             ,end_expense_date
             ,remarks
             ,attendees
             ,attendees_emp
             ,attendees_te
             ,attendees_cti1
             ,attendees_cti2
             ,credit_card_trx_id
             ,cardmember_name
             ,transaction_date
             ,posted_date
             ,merchant_location
             ,destination_from
             ,destination_to
             ,distance_unit_code
             ,daily_distance
             ,trip_distance
             ,avg_mileage_rate
             ,dt_sentto_gl
             ,expense_report_number
             ,card_program_id
             ,card_program_name
             ,mcc_code_no
             ,mcc_code
             ,accounting_date
             ,report_total
             ,miles
             ,rate_per_mile
             ,attribute_category
             ,category_code
             ,transaction_amount
             ,receipt_currency_amount
             ,amt_due_ccard_company
             ,amt_due_employee
             ,imported_to_ap
             ,imported_to_gl
             ,workflow_approved_flag
             ,expense_current_approver_id
             ,approver_id
             ,approver_name
             ,report_type
             ,advance_invoice_to_apply
             ,advance_distribution_number
             ,advance_flag
             ,advance_gl_date
             ,advance_number
             ,report_reject_code
             ,app_post_flag
             ,approved_in_gl
             ,vouchno
             ,query_num
             ,query_descr
             ,expense_report_status
             ,reference_number
             ,project_number
             ,project_name
             ,tax_province
             ,gst_amount
             ,pst_amount
             ,qst_amount
             ,hst_amount
             ,org_id
         FROM xxcus.xxcus_bullet_iexp_tbl);
  
    COMMIT; */
--Commented Appxcmn delivery portion for Ver# 6.0 <<End
  
  EXCEPTION
    WHEN OTHERS THEN
      p_retcode := 900025;
      p_errbuf  := 'XXCUSOIE: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                   ' in ' || l_procedure_name || ' at ' || l_sec;
      fnd_file.put_line(fnd_file.log, p_errbuf);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => p_retcode
                                          ,p_error_desc        => p_errbuf
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'XXCUS');
  END load_iexp_int_table;

  PROCEDURE bup_iexp_int_table(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) IS
    --Intialize Variables    
    l_procedure_name VARCHAR2(75) := 'xxcusoie_bullettrain_extr_pkg.bup_iexp_int_table';
    l_location       VARCHAR2(75) := NULL;
    l_req_id         NUMBER := fnd_global.conc_request_id;
  BEGIN
    l_location := 'Start Backup Program; ';
    fnd_file.put_line(fnd_file.log, l_location);
    fnd_file.put_line(fnd_file.output, l_location);
  
    --Truncate the Backup Table
    BEGIN
      l_location := 'Truncate the backup table before backing up the summary table.';
      fnd_file.put_line(fnd_file.log, l_location);
      fnd_file.put_line(fnd_file.output, l_location);
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSOIE_BULLET_TRAIN_BU_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        p_retcode := 900026;
        p_errbuf  := 'XXCUSOIE: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                     ' in ' || l_procedure_name || ' at ' || l_location;
        fnd_file.put_line(fnd_file.log, p_errbuf);
        -- Calling ERROR API
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                            ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => p_retcode
                                            ,p_error_desc        => p_errbuf
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'XXCUS');
    END;
  
    --Populate backup table with previous day's information prior to 
    --starting the repopulation of the main data table    
    l_location := 'Copy previous day data to backup table.';
    fnd_file.put_line(fnd_file.log, l_location);
    fnd_file.put_line(fnd_file.output, l_location);
  
    INSERT /*+ APPEND*/
    INTO xxcus.xxcusoie_bullet_train_bu_tbl
      (SELECT * FROM xxcus.xxcus_bullet_iexp_tbl);
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      p_retcode := 900027;
      p_errbuf  := 'XXCUSOIE: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                   ' in ' || l_procedure_name || ' at ' || l_location;
      fnd_file.put_line(fnd_file.log, p_errbuf);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => p_retcode
                                          ,p_error_desc        => p_errbuf
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'XXCUS');
  END bup_iexp_int_table;

  PROCEDURE restore_iexp_int_table(p_errbuf  OUT VARCHAR2
                                  ,p_retcode OUT NUMBER) IS
    --Intialize Variables   
    l_procedure_name VARCHAR2(75) := 'xxcusoie_bullettrain_extr_pkg.restore_iexp_int_table';
    l_location       VARCHAR2(75) := NULL;
    l_req_id         NUMBER := fnd_global.conc_request_id;
  BEGIN
    l_location := 'Restoring data to main table from backup table.';
    fnd_file.put_line(fnd_file.log, l_location);
    fnd_file.put_line(fnd_file.output, l_location);
  
    EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_BULLET_IEXP_TBL';
  
    INSERT /*+ APPEND*/
    INTO xxcus.xxcus_bullet_iexp_tbl
      (SELECT * FROM xxcus.xxcusoie_bullet_train_bu_tbl);
  
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      p_retcode := 900028;
      p_errbuf  := 'XXCUSOIE: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                   ' in ' || l_procedure_name || ' at ' || l_location;
      fnd_file.put_line(fnd_file.log, p_errbuf);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => p_retcode
                                          ,p_error_desc        => p_errbuf
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'XXCUS');
  END restore_iexp_int_table;

  PROCEDURE populate_accrual_hist_tbl(errbuf    OUT VARCHAR2
                                     ,retcode   OUT NUMBER
                                     ,p_fperiod IN VARCHAR2) IS
    --Intialize Variables
    l_sec            VARCHAR2(200);
    l_procedure_name VARCHAR2(75) := 'xxcusoie_bullettrain_extr_pkg.POPULATE_ACCRUAL_HIST_TBL';
    l_start_date     gl_periods.start_date%TYPE;
    l_end_date       gl_periods.end_date%TYPE;
    l_curr_period    gl_periods.period_name%TYPE;
    l_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR';
  
  BEGIN
    l_sec := 'Setup Variables';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    SELECT period_name, start_date, end_date
      INTO l_curr_period, l_start_date, l_end_date
      FROM gl.gl_periods
     WHERE period_set_name = l_calendar
       AND period_name = p_fperiod
       AND period_num <> 13;
  
    --Copy accrual data for current month from 
    BEGIN
      l_sec := 'Copying accrual data from ';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      INSERT /*+APPEND*/
      INTO xxcus.xxcusoie_te_accrual_hist_tbl
        (SELECT oracle_accounts
               ,oracle_product
               ,oracle_location
               ,oracle_cost_center
               ,oracle_account
               ,project_number
               ,oracle_segment6
               ,oracle_segment7
               ,item_description
               ,line_amount
               ,currency_code
               ,full_name
               ,employee_number
               ,emp_default_prod
               ,emp_default_loc
               ,emp_default_costctr
               ,fru
               ,merchant_name
               ,credit_card_trx_id
               ,transaction_date
               ,card_program_id
               ,card_program_name
               ,mcc_code_no
               ,mcc_code
               ,imported_to_ap
               ,imported_to_gl
               ,query_num
               ,query_descr
               ,(SELECT description
                   FROM fnd_lookup_values
                  WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
                    AND (lookup_code LIKE 'CARDPROGRAM_%' AND
                        substr(lookup_code, 13, 87) = card_program_name OR
                        lookup_code LIKE 'OUTOFPOCKET_' || oracle_product AND
                        card_program_name IS NULL)) parm_value
               ,nvl(expense_report_status, 'No Status')
               ,start_expense_date
               ,end_expense_date
               ,l_curr_period
               ,expense_report_number
               ,distribution_line_number
               ,cc_trns_category
               ,org_id
           FROM xxcus.xxcus_bullet_iexp_tbl
          WHERE nvl(merchant_name, 'X') <> 'CR BAL REFUND'
            AND nvl(imported_to_gl, 'N') = 'N'
            AND (transaction_date <= l_end_date OR
                 end_expense_date <= l_end_date)
            AND (query_num = '4' AND end_expense_date >= l_end_date - 90)
             OR query_num IN ('2', '3', '5'));
      l_sec := 'Committing transactions in XXCUSOIE_TE_ACCRUAL_HIST_TBL';
      COMMIT;
      l_sec := 'Completed inserting data from SQA into XXCUSOIE_TE_ACCRUAL_HIST_TBL';
    EXCEPTION
      WHEN OTHERS THEN
        retcode := SQLCODE;
        errbuf  := substr(SQLERRM, 1, 2000);
        fnd_file.put_line(fnd_file.log
                         ,'Error in ' || l_procedure_name || l_sec);
        fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
        fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
      
    END;
  END populate_accrual_hist_tbl;

  FUNCTION get_exp_rep_param_info(p_exp_rep_id        IN NUMBER
                                 ,p_card_exp_lkp_code IN VARCHAR2
                                 ,p_output_type       IN VARCHAR2)
    RETURN VARCHAR2 IS
    l_procedure_name VARCHAR2(75) := 'xxcusoie_bullettrain_extr_pkg.get_exp_rep_param_info';
    lv_account_info  VARCHAR2(10) := NULL;
    lv_prompt        ap_expense_report_params_all.prompt%TYPE := NULL;
    l_req_id         NUMBER := fnd_global.conc_request_id;
  BEGIN
  
    IF p_output_type = 'ACCOUNT'
    
    THEN
      SELECT substr(flex_concactenated, 4, 6)
        INTO lv_account_info
        FROM ap.ap_expense_report_params_all t
       WHERE t.expense_report_id = p_exp_rep_id
         AND t.parameter_id IN
             (SELECT parameter_id
                FROM ap.ap_card_parameters
               WHERE card_exp_type_lookup_code = p_card_exp_lkp_code);
    
      RETURN lv_account_info;
    ELSIF p_output_type = 'PROMPT'
    THEN
      SELECT prompt
        INTO lv_prompt
        FROM ap.ap_expense_report_params_all t
       WHERE t.expense_report_id = p_exp_rep_id
         AND t.parameter_id IN
             (SELECT parameter_id
                FROM ap.ap_card_parameters
               WHERE card_exp_type_lookup_code = p_card_exp_lkp_code);
    
      RETURN lv_prompt;
    
    END IF;
  EXCEPTION
    WHEN no_data_found THEN
      IF p_output_type = 'ACCOUNT'
      THEN
        RETURN '658020';
      ELSIF p_output_type = 'PROMPT'
      THEN
        RETURN NULL;
      END IF;
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => 900045
                                          ,p_error_desc        => SQLERRM
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'XXCUS');
  END get_exp_rep_param_info;

  /********************************************************************************
  
   File Name: xxcusoie_bullettrain_extr_pkg
  
   PROGRAM TYPE: PL/SQL Package spec and body
  
   PURPOSE: Procedure for populating the Oracle Month End T and E Bullet Train 
            Table that can be used to reporting 
  
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------  
   1.0     06/08/2012    Kathy Poling    SR 141622 creation of month end snapshot 
                                         of Bullet Train                                                                       
  ********************************************************************************/
  PROCEDURE monthend_iexp_table(p_errbuf  OUT VARCHAR2
                               ,p_retcode OUT NUMBER) IS
    --Intialize Variables    
    l_procedure_name VARCHAR2(75) := 'xxcusoie_bullettrain_extr_pkg.monthend_iexp_table';
    l_location       VARCHAR2(75) := NULL;
    l_req_id         NUMBER := fnd_global.conc_request_id;
  BEGIN
    l_location := 'Start Backup Program; ';
    fnd_file.put_line(fnd_file.log, l_location);
    fnd_file.put_line(fnd_file.output, l_location);
  
    --Truncate the Backup Table
    BEGIN
      l_location := 'Truncate the monthend table.';
      fnd_file.put_line(fnd_file.log, l_location);
      fnd_file.put_line(fnd_file.output, l_location);
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSOIE_BULLET_TRAIN_ME_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        p_retcode := 900026;
        p_errbuf  := 'XXCUSOIE: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                     ' in ' || l_procedure_name || ' at ' || l_location;
        fnd_file.put_line(fnd_file.log, p_errbuf);
        -- Calling ERROR API
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                            ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => p_retcode
                                            ,p_error_desc        => p_errbuf
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'XXCUS');
    END;
  
    --Populate month end table with information after bullet train has been refreshed during month end close 
    --starting the population of data    
    l_location := 'Copy data to month end table.';
    fnd_file.put_line(fnd_file.log, l_location);
    fnd_file.put_line(fnd_file.output, l_location);
  
    INSERT /*+ APPEND*/
    INTO xxcus.xxcusoie_bullet_train_me_tbl
      (SELECT * FROM xxcus.xxcus_bullet_iexp_tbl);
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      p_retcode := 900027;
      p_errbuf  := 'XXCUSOIE: ' || p_retcode || substr(SQLERRM, 1, 1900) ||
                   ' in ' || l_procedure_name || ' at ' || l_location;
      fnd_file.put_line(fnd_file.log, p_errbuf);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for xxcusoie_bullettrain_extr_pkg'
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => p_retcode
                                          ,p_error_desc        => p_errbuf
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'XXCUS');
  END monthend_iexp_table;

END xxcusoie_bullettrain_extr_pkg;
/
