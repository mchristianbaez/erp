/*
TMS: 20160216-00041
Date: 02/16/2016
Notes: data fix script to process month end transactions
*/
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
  UPDATE apps.wsh_delivery_details
  SET released_status      = 'D',
    src_requested_quantity = 0,
    requested_quantity     = 0,
    shipped_quantity       = 0,
    cycle_count_quantity   = 0,
    cancelled_quantity     = 0,
    subinventory           = NULL,
    locator_id             = NULL,
    lot_number             = NULL,
    revision               = NULL,
    inv_interfaced_flag    = 'X',
    oe_interfaced_flag     = 'X'
  WHERE delivery_detail_id =14399584;
  
  DBMS_OUTPUT.put_line ('Rows Updated in wsh_delivery_details: '||sql%rowcount);
  
  --1 row expected to be updated
  UPDATE apps.wsh_delivery_assignments
  SET delivery_id             = NULL,
    parent_delivery_detail_id = NULL
  WHERE delivery_detail_id    = 14399584;
  
  DBMS_OUTPUT.put_line ('Rows Updated in wsh_delivery_assignments: '||sql%rowcount);
  
  --1 row expected to be updated
  UPDATE apps.oe_order_lines_all
  SET flow_status_code='CANCELLED',
    cancelled_flag    ='Y'
  WHERE line_id       =64453010
  AND headeR_id       =39324315;
  
  DBMS_OUTPUT.put_line ('Rows Updated in oe_order_lines_all: '||sql%rowcount);
  
  --1 row expected to be updated
  COMMIT;
  
  DBMS_OUTPUT.put_line ('Update Committed');
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.put_line ('TMS: 20160216-00041 Errors ='||SQLERRM);
END;
/
