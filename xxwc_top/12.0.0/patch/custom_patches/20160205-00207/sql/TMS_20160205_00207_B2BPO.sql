/*************************************************************************
  $Header TMS_20160205_00207_B2BPO.sql $
  Module Name: TMS_20160205_00207 Script to process B2B PO# 15765

  PURPOSE: TMS_20160205_00207 Script to process B2B PO# 15765

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        05-Feb-2016  Gopi Damuluri         TMS#20160205-00207

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160205-00207   , Before Update');

UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl 
set process_flag  = 'N'
  , error_message = NULL
WHERE customer_po_number IN ('15765'); 

   DBMS_OUTPUT.put_line (
         'TMS: 20160205-00207 B2B PO Header Updated and number of records (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160205-00207   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160205-00207, Errors : ' || SQLERRM);
END;
/