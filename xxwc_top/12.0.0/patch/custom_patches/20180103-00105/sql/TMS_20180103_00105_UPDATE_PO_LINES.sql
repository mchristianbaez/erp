/*************************************************************************
  $Header TMS_20180103_00105_UPDATE_PO.sql $
  Module Name: TMS_20180103_00105   Data Script Fix to update PO Qty

  PURPOSE: Data Script Fix to update PO Qty incorrect on order

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        19-JAN-2018  Krishna               TMS#20180103-00105

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
DBMS_OUTPUT.put_line ('Before update');

UPDATE apps.po_requisition_lines_all
SET quantity = 0
WHERE requisition_line_id IN (41484425,
41484472,
40995968);

DBMS_OUTPUT.put_line ('Records updated1-' || SQL%ROWCOUNT);

UPDATE apps.po_req_distributions_all
SET req_line_quantity = 0
WHERE requisition_line_id IN (SELECT REQUISITION_LINE_ID
FROM po_requisition_lines_all
WHERE requisition_line_id IN (41484425,
41484472,
40995968));


DBMS_OUTPUT.put_line ('Records updated2-' || SQL%ROWCOUNT);
COMMIT;
EXCEPTION
WHEN OTHERS
THEN
DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/
