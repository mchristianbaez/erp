--Report Name            : Item Attribute Validation Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Item Attribute Validation Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_NEW_PO_ISR_RPT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_NEW_PO_ISR_RPT_V',201,'','','','','MR020532','XXEIS','Eis Xxwc Po Isr V','EXPIV','','');
--Delete View Columns for EIS_XXWC_NEW_PO_ISR_RPT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_NEW_PO_ISR_RPT_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_NEW_PO_ISR_RPT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','BPA',201,'Bpa','BPA','','','','MR020532','VARCHAR2','','','Bpa','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','ITEM_COST',201,'Item Cost','ITEM_COST','','','','MR020532','NUMBER','','','Item Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','AVER_COST',201,'Aver Cost','AVER_COST','','','','MR020532','NUMBER','','','Aver Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','AMU',201,'Amu','AMU','','','','MR020532','VARCHAR2','','','Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','PM',201,'Pm','PM','','','','MR020532','VARCHAR2','','','Pm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','STK_FLAG',201,'Stk Flag','STK_FLAG','','','','MR020532','VARCHAR2','','','Stk Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','CL',201,'Cl','CL','','','','MR020532','VARCHAR2','','','Cl','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','UOM',201,'Uom','UOM','','','','MR020532','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','CAT',201,'Cat','CAT','','','','MR020532','VARCHAR2','','','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','DESCRIPTION',201,'Description','DESCRIPTION','','','','MR020532','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','ST',201,'St','ST','','','','MR020532','VARCHAR2','','','St','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','SOURCE',201,'Source','SOURCE','','','','MR020532','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','MR020532','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','VENDOR_NUM',201,'Vendor Num','VENDOR_NUM','','','','MR020532','VARCHAR2','','','Vendor Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','ITEM_NUMBER',201,'Item Number','ITEM_NUMBER','','','','MR020532','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','PRE',201,'Pre','PRE','','','','MR020532','VARCHAR2','','','Pre','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','ORG',201,'Org','ORG','','','','MR020532','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','BUYER',201,'Buyer','BUYER','','','','MR020532','VARCHAR2','','','Buyer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','TURNS',201,'Turns','TURNS','','','','MR020532','NUMBER','','','Turns','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','THIRTEEN_WK_AN_COGS',201,'Thirteen Wk An Cogs','THIRTEEN_WK_AN_COGS','','','','MR020532','NUMBER','','','Thirteen Wk An Cogs','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','THIRTEEN_WK_AVG_INV',201,'Thirteen Wk Avg Inv','THIRTEEN_WK_AVG_INV','','','','MR020532','NUMBER','','','Thirteen Wk Avg Inv','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','RES',201,'Res','RES','','','','MR020532','VARCHAR2','','','Res','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','FREEZE_DATE',201,'Freeze Date','FREEZE_DATE','','','','MR020532','DATE','','','Freeze Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','FI_FLAG',201,'Fi Flag','FI_FLAG','','','','MR020532','VARCHAR2','','','Fi Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','MC',201,'Mc','MC','','','','MR020532','VARCHAR2','','','Mc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','BIN_LOC',201,'Bin Loc','BIN_LOC','','','','MR020532','VARCHAR2','','','Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','AVAILABLEDOLLAR',201,'Availabledollar','AVAILABLEDOLLAR','','','','MR020532','NUMBER','','','Availabledollar','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','AVAILABLE',201,'Available','AVAILABLE','','','','MR020532','NUMBER','','','Available','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','QOH',201,'Qoh','QOH','','','','MR020532','NUMBER','','','Qoh','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','APR_SALES',201,'Apr Sales','APR_SALES','','','','MR020532','NUMBER','','','Apr Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','AUG_SALES',201,'Aug Sales','AUG_SALES','','','','MR020532','NUMBER','','','Aug Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','DEC_SALES',201,'Dec Sales','DEC_SALES','','','','MR020532','NUMBER','','','Dec Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','FEB_SALES',201,'Feb Sales','FEB_SALES','','','','MR020532','NUMBER','','','Feb Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','HIT4_SALES',201,'Hit4 Sales','HIT4_SALES','','','','MR020532','NUMBER','','','Hit4 Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','HIT6_SALES',201,'Hit6 Sales','HIT6_SALES','','','','MR020532','NUMBER','','','Hit6 Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','JAN_SALES',201,'Jan Sales','JAN_SALES','','','','MR020532','NUMBER','','','Jan Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','JUL_SALES',201,'Jul Sales','JUL_SALES','','','','MR020532','NUMBER','','','Jul Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','JUNE_SALES',201,'June Sales','JUNE_SALES','','','','MR020532','NUMBER','','','June Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','MAR_SALES',201,'Mar Sales','MAR_SALES','','','','MR020532','NUMBER','','','Mar Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','MAY_SALES',201,'May Sales','MAY_SALES','','','','MR020532','NUMBER','','','May Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','NOV_SALES',201,'Nov Sales','NOV_SALES','','','','MR020532','NUMBER','','','Nov Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','OCT_SALES',201,'Oct Sales','OCT_SALES','','','','MR020532','NUMBER','','','Oct Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','SEP_SALES',201,'Sep Sales','SEP_SALES','','','','MR020532','NUMBER','','','Sep Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','ONE_SALES',201,'One Sales','ONE_SALES','','','','MR020532','NUMBER','','','One Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','SIX_SALES',201,'Six Sales','SIX_SALES','','','','MR020532','NUMBER','','','Six Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','TWELVE_SALES',201,'Twelve Sales','TWELVE_SALES','','','','MR020532','NUMBER','','','Twelve Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','DISTRICT',201,'District','DISTRICT','','','','MR020532','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','REGION',201,'Region','REGION','','','','MR020532','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','MAXN',201,'Maxn','MAXN','','','','MR020532','VARCHAR2','','','Maxn','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','MINN',201,'Minn','MINN','','','','MR020532','VARCHAR2','','','Minn','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','TS',201,'Ts','TS','','','','MR020532','VARCHAR2','','','Ts','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','BPA_COST',201,'Bpa Cost','BPA_COST','','','','MR020532','NUMBER','','','Bpa Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','ON_ORD',201,'On Ord','ON_ORD','','','','MR020532','NUMBER','','','On Ord','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','FML',201,'Fml','FML','','','','MR020532','NUMBER','','','Fml','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','OPEN_REQ',201,'Open Req','OPEN_REQ','','','','MR020532','NUMBER','','','Open Req','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','WT',201,'Wt','WT','','','','MR020532','NUMBER','','','Wt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','SO',201,'So','SO','','','','MR020532','NUMBER','','','So','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','SOURCING_RULE',201,'Sourcing Rule','SOURCING_RULE','','','','MR020532','VARCHAR2','','','Sourcing Rule','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','SS',201,'Ss','SS','','','','MR020532','NUMBER','','','Ss','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','CLT',201,'Clt','CLT','','','','MR020532','NUMBER','','','Clt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','AVAIL2',201,'Avail2','AVAIL2','','','','MR020532','NUMBER','','','Avail2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','INT_REQ',201,'Int Req','INT_REQ','','','','MR020532','NUMBER','','','Int Req','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','DIR_REQ',201,'Dir Req','DIR_REQ','','','','MR020532','NUMBER','','','Dir Req','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','SITE_VENDOR_NUM',201,'Site Vendor Num','SITE_VENDOR_NUM','','','','MR020532','VARCHAR2','','','Site Vendor Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','VENDOR_SITE',201,'Vendor Site','VENDOR_SITE','','','','MR020532','VARCHAR2','','','Vendor Site','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','ORG_NAME',201,'Org Name','ORG_NAME','','','','MR020532','VARCHAR2','','','Org Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','MF_FLAG',201,'Mf Flag','MF_FLAG','','','','MR020532','VARCHAR2','','','Mf Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','SET_OF_BOOKS_ID',201,'Set Of Books Id','SET_OF_BOOKS_ID','','','','MR020532','NUMBER','','','Set Of Books Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','ORGANIZATION_ID',201,'Organization Id','ORGANIZATION_ID','','','','MR020532','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','COMMON_OUTPUT_ID',201,'Common Output Id','COMMON_OUTPUT_ID','','','','MR020532','NUMBER','','','Common Output Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','PROCESS_ID',201,'Process Id','PROCESS_ID','','','','MR020532','NUMBER','','','Process Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','PLT',201,'Plt','PLT','','','','MR020532','NUMBER','','','Plt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V','PPLT',201,'Pplt','PPLT','','','','MR020532','NUMBER','','','Pplt','','','');
--Inserting View Components for EIS_XXWC_NEW_PO_ISR_RPT_V
--Inserting View Component Joins for EIS_XXWC_NEW_PO_ISR_RPT_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Item Attribute Validation Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'    SELECT distinct Mcv.SEGMENT2 cat_class
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            MTL_ITEM_CATEGORIES MIC,
            mtl_system_items_kfv msi
            Where   Mcs.Category_Set_Name      = ''Inventory Category''
          AND MCS.STRUCTURE_ID                 = MCV.STRUCTURE_ID
          AND MIC.INVENTORY_ITEM_ID            = MSI.INVENTORY_ITEM_ID
          And Mic.Organization_Id              = msi.Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id
order by Mcv.SEGMENT2','','Catclass Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Exclude,Include,Tool Repair Only','Tool Repair','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','  ,Vendor Number(%),3 Digit Prefix,Item number,Source,2 Digit Cat,4 Digit Cat Class,Default Buyer(%)','Report Criteria','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Active small,All items,All items on hand, Stock items only,Non stock only,Active large, Non-stock on hand,Stock items with 0/0 min/max','EIS PO XXWC ISR REPORT COND','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Include,Time Sensitive Only','EIS PO XXWC Time Sensitive','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Yes,No','EIS PO XXWC DC Mode','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Source'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Source List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'SELECT DISTINCT segment1 Item
FROM mtl_system_items_b msi
WHERE NOT EXISTS
  (SELECT 1
  FROM mtl_parameters mp
  WHERE organization_code IN (''CAN'',''HDS'',''US1'',''CN1'')
  AND msi.organization_id  = mp.organization_id
  )
order by segment1 ASC','','XXWC Item','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct pov.segment1 vendor_number, pov.vendor_name from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC Vendors','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'SELECT ood.organization_code organization_code,
  ood.organization_name organization_name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE organization_code NOT IN(''CAN'',''HDS'',''US1'',''CN1'')
ORDER BY organization_code','','XXWC Org Lov','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Item Attribute Validation Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Item Attribute Validation Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'Item Attribute Validation Report - WC' );
--Inserting Report - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.r( 201,'Item Attribute Validation Report - WC','','This report displays info needed to make inventory replenishment decisions ( by vendor, part number) at selected locations.','','','','MR020532','EIS_XXWC_NEW_PO_ISR_RPT_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AVER_COST','Aver Cost','Aver Cost','','~T~D~2','default','','27','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'BIN_LOC','Bin Loc','Bin Loc','','','default','','40','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'BPA','Bpa#','Bpa','','','default','','30','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'BUYER','Buyer','Buyer','','','default','','48','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'CAT','Cat/Cl','Cat','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'CL','Cl','Cl','','','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'DEC_SALES','Dec','Dec Sales','','~~~','default','','61','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'DESCRIPTION','Description','Description','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'FEB_SALES','Feb','Feb Sales','','~~~','default','','51','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'FI_FLAG','FI','Fi Flag','','','default','','42','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'FREEZE_DATE','Freeze Date','Freeze Date','','','default','','43','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'HIT4_SALES','Hit4','Hit4 Sales','','~~~','default','','26','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'HIT6_SALES','Hit6','Hit6 Sales','','~~~','default','','25','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ITEM_COST','Item Cost','Item Cost','','~T~D~2','default','','28','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ITEM_NUMBER','Item Number','Item Number','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'JAN_SALES','Jan','Jan Sales','','~~~','default','','50','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AUG_SALES','Aug','Aug Sales','','~~~','default','','57','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AVAILABLE','Avail','Available','','~~~','default','','34','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AVAILABLEDOLLAR','Ext$','Availabledollar','','~~~','default','','36','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'JUL_SALES','Jul','Jul Sales','','~~~','default','','56','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'JUNE_SALES','June','June Sales','','~~~','default','','55','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MAR_SALES','Mar','Mar Sales','','~~~','default','','52','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MAXN','Max','Maxn','NUMBER','~T~D~0','default','','23','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MAY_SALES','May','May Sales','','~~~','default','','54','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MC','MC','Mc','','','default','','41','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'QOH','QOH','Qoh','','~~~','default','','31','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'RES','Res','Res','NUMBER','~T~D~0','default','','44','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SEP_SALES','Sep','Sep Sales','','~~~','default','','58','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SIX_SALES','6','Six Sales','','~~~','default','','38','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SOURCE','Source','Source','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ST','ST','St','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'STK_FLAG','Stk','Stk Flag','','','default','','20','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'THIRTEEN_WK_AN_COGS','13 Wk An COGS $','Thirteen Wk An Cogs','','~~~','default','','46','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'THIRTEEN_WK_AVG_INV','13 Wk Av Inv $','Thirteen Wk Avg Inv','','~~~','default','','45','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'TS','TS','Ts','NUMBER','~T~D~0','default','','49','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'TURNS','Turns','Turns','','~~~','default','','47','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'TWELVE_SALES','12','Twelve Sales','','~~~','default','','39','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'UOM','UOM','Uom','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'VENDOR_NAME','Vendor','Vendor Name','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MINN','Min','Minn','NUMBER','~T~D~0','default','','22','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'VENDOR_NUM','Vend #','Vendor Num','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AMU','AMU','Amu','NUMBER','~T~D~0','default','','24','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'FML','FLM','Fml','','~~~','default','','17','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'OPEN_REQ','Req','Open Req','','~~~','default','','32','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'WT','Wt','Wt','','~~~','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SO','So','So','','~~~','default','','18','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SOURCING_RULE','Sourcing Rule','Sourcing Rule','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SS','Ss','Ss','','~~~','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'CLT','CLT','Clt','','~~~','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AVAIL2','Avail2','Avail2','','~~~','default','','35','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'BPA_COST','Bpa Cost','Bpa Cost','','~T~D~2','default','','29','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ON_ORD','On Ord','On Ord','','~~~','default','','33','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'APR_SALES','Apr','Apr Sales','','~~~','default','','53','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'INT_REQ','Int Req','Int Req','','~~~','default','','62','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'DIR_REQ','Dir Req','Dir Req','','~~~','default','','63','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'NOV_SALES','Nov','Nov Sales','','~~~','default','','60','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'OCT_SALES','Oct','Oct Sales','','~~~','default','','59','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ONE_SALES','1','One Sales','','~~~','default','','37','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ORG','Org','Org','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'PM','PM','Pm','','','default','','21','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'PRE','Pre','Pre','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SITE_VENDOR_NUM','Supplier Number','Site Vendor Num','','','default','','64','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'VENDOR_SITE','Supplier Site','Vendor Site','','','default','','65','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'PLT','PLT','Plt','','~T~D~0','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'PPLT','PPLT','Pplt','','~T~D~0','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V','','');
--Inserting Report Parameters - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Items w/ 4 mon sales hits','Stock items w/ 4 mon sales hits > x','','IN','','','NUMERIC','N','Y','9','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Report Criteria','Report Criteria','','IN','Report Criteria','','VARCHAR2','N','Y','10','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Report Criteria Value','Report Criteria Value','','IN','','','VARCHAR2','N','Y','11','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'DC Mode','DC Mode','','IN','EIS PO XXWC DC Mode','''No''','VARCHAR2','N','Y','5','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Starting Bin','Starting Bin','BIN_LOC','>=','Bin Location Lov','','VARCHAR2','N','Y','12','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Ending Bin','Ending Bin','BIN_LOC','<=','Bin Location Lov','','VARCHAR2','N','Y','13','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Tool Repair','Tool Repair','','IN','Tool Repair','''Include''','VARCHAR2','N','Y','6','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Time Sensitive','Time Sensitive','','IN','EIS PO XXWC Time Sensitive','','VARCHAR2','N','Y','7','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Vendor','Vendor','VENDOR_NUM','IN','XXWC Vendors','','VARCHAR2','N','Y','14','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Item','Item','ITEM_NUMBER','IN','XXWC Item','','VARCHAR2','N','Y','15','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Cat Class','Cat Class','CAT','IN','Catclass Lov','','VARCHAR2','N','Y','16','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Report Filter','Report Condition','','IN','EIS PO XXWC ISR REPORT COND','','VARCHAR2','N','Y','8','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','17','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Vendor List','Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','18','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Cat Class List','Cat Class List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','19','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Source List','Source List','','IN','XXWC Source List','','VARCHAR2','N','Y','20','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Organization List','Organization List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Organization','Organization','ORG','IN','XXWC Org Lov','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'DISTRICT','IN',':District','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'BIN_LOC','>=',':Starting Bin','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'PROCESS_ID','IN',':SYSTEM.PROCESS_ID','','','Y','1','N','MR020532');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'REGION','IN',':Region','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'BIN_LOC','<=',':Ending Bin','','','Y','6','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'VENDOR_NUM','IN',':Vendor','','','Y','7','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'ITEM_NUMBER','IN',':Item','','','Y','8','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'CAT','IN',':Cat Class','','','Y','9','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'ORG','IN',':Organization','','','Y','4','Y','MR020532');
--Inserting Report Sorts - Item Attribute Validation Report - WC
--Inserting Report Triggers - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.rt( 'Item Attribute Validation Report - WC',201,'begin
xxeis.eis_new_po_xxwc_isr_util_pkg.G_isr_rpt_dc_mod_sub:=:DC Mode;
xxeis.eis_new_po_xxwc_isr_pkg.Isr_live_Rpt_Proc(
P_process_id => :SYSTEM.PROCESS_ID,
P_Region =>:Region,
P_District =>:District,
P_Location =>:Organization,
P_Dc_Mode =>:DC Mode,
P_Tool_Repair =>:Tool Repair,
P_Time_Sensitive =>:Time Sensitive,
P_Stk_Items_With_Hit4 =>:Items w/ 4 mon sales hits,
p_Report_Condition =>:Report Filter,
P_Report_Criteria            =>:Report Criteria,
P_Report_Criteria_val            =>:Report Criteria Value,
p_start_bin_loc =>:Starting Bin,
p_end_bin_loc =>:Ending Bin,
p_vendor =>:Vendor,
p_item =>:Item,
p_cat_class =>:Cat Class,
p_org_list =>:Organization List,
P_ITEM_LIST =>:Item List,
P_SUPPLIER_LIST =>:Vendor List,
P_CAT_CLASS_LIST =>:Cat Class List,
P_SOURCE_LIST =>:Source List
);
end;','B','Y','MR020532');
--Inserting Report Templates - Item Attribute Validation Report - WC
--Inserting Report Portals - Item Attribute Validation Report - WC
--Inserting Report Dashboards - Item Attribute Validation Report - WC
--Inserting Report Security - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','201','','50983',201,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','201','','50893',201,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','20005','','50900',201,'MR020532','','');
--Inserting Report Pivots - Item Attribute Validation Report - WC
END;
/
set scan on define on
