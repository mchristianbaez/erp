create or replace package             xxeis.eis_new_po_xxwc_isr_util_pkg authid current_user is
/*************************************************************************
  $Header eis_new_po_xxwc_isr_util_pkg.pkb $
  Module Name: Purchasing
  PURPOSE: Item attribute validation report - WC
  ESMS Task Id : TMS# 20160129-00212
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.1        24-Feb-2016  Mahender Reddy   Initial version

  **************************************************************************/
   G_ISR_RPT_DC_MOD_SUB varchar2(50);        
   FUNCTION get_demand_qty( p_org_id          NUMBER
                           , p_subinv          VARCHAR2
                           , p_level           NUMBER
                           , p_item_id         NUMBER
                           , p_d_cutoff        DATE
                           , p_include_nonnet  NUMBER
                           , p_net_rsv         NUMBER
                           , p_net_unrsv       NUMBER
                           , p_net_wip         NUMBER
                           /* nsinghi MIN-MAX INVCONV start */
                           , p_process_org     VARCHAR2
                           /* nsinghi MIN-MAX INVCONV end */
                           ) RETURN NUMBER;
   

   FUNCTION get_vendor_name (p_inventory_item_id NUMBER, p_organization_id NUMBER) RETURN VARCHAR2;

   FUNCTION get_vendor_number (p_inventory_item_id NUMBER, p_organization_id NUMBER) RETURN VARCHAR2;

   Function Get_Inv_Cat_Class (P_Inventory_Item_Id Number ,P_Organization_Id Number) Return Varchar2;

   function Get_inv_cat_seg1 (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2;

   function Get_inv_vel_cat_class (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2;

   function get_isr_item_cost (p_inventory_item_id number,p_organization_id number) return number;

   function get_isr_bpa_doc (p_inventory_item_id number,p_organization_id number) return number;

   function get_isr_open_po_qty (p_inventory_item_id number,p_organization_id number) return number;

   function get_isr_avail_qty (p_inventory_item_id number,p_organization_id number) return number;

   function get_isr_ss_cnt (p_inventory_item_id number,p_organization_id number) return number;

   function get_isr_sourcing_rule (P_INVENTORY_ITEM_ID NUMBER,P_ORGANIZATION_ID NUMBER) return varchar2;

   function get_isr_ss (P_INVENTORY_ITEM_ID NUMBER,P_ORGANIZATION_ID NUMBER) return NUMBER;
   function GET_INT_REQ_SO_QTY  (P_INVENTORY_ITEM_ID number,P_ORGANIZATION_ID number) return number;
   function get_isr_rpt_dc_mod_sub  return varchar2;
   Function Get_Onhand_Inv (P_Inventory_Item_Id Number, P_Organization_Id Number) Return Number;
   function GET_PRIMARY_BIN_LOC (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number) return varchar2;
   function get_isr_open_req_qty (P_INVENTORY_ITEM_ID number,P_ORGANIZATION_ID number,p_req_type varchar2) return number;   
   FUNCTION get_staged_qty( p_org_id          NUMBER
                            , p_subinv          VARCHAR2
                            , p_item_id         NUMBER
                            , P_ORDER_LINE_ID   number
                            , P_INCLUDE_NONNET  number) return number ;
  FUNCTION get_pick_released_qty( p_org_id          NUMBER
                                  , P_SUBINV          varchar2
                                  , P_ITEM_ID         number
                                  , P_ORDER_LINE_ID   number) return number;
  FUNCTION get_loaded_qty(    p_org_id          NUMBER
                              , p_subinv          VARCHAR2
                              , p_level           NUMBER
                              , P_ITEM_ID         number
                              , P_NET_RSV         number
                              , P_NET_UNRSV       number ) return number;
  FUNCTION get_shipped_qty( p_organization_id    IN      NUMBER
                            , P_INVENTORY_ITEM_ID  in      number
                            , P_ORDER_LINE_ID      in      number) return number ;

  FUNCTION get_supply_qty( p_org_id              NUMBER
                           , p_subinv              VARCHAR2
                           , p_item_id             NUMBER
                           , p_postproc_lead_time  NUMBER
                           , p_cal_code            VARCHAR2
                           , p_except_id           NUMBER
                           , p_level               NUMBER
                           , p_s_cutoff            DATE
                              , p_include_po          NUMBER
                           , p_include_mo          NUMBER
                              , p_vmi_enabled         VARCHAR2
                           , p_include_nonnet      NUMBER
                           , p_include_wip         NUMBER
                           , p_include_if          NUMBER
                           /* nsinghi MIN-MAX INVCONV start */
                           , p_process_org         VARCHAR2
                           /* nsinghi MIN-MAX INVCONV end */
                           ) return number ;
       function GET_ITEM_UOM_CODE (P_UOM_NAME   varchar2) return varchar2;
  
  FUNCTION GET_PLANNING_QUANTITY(
     P_INCLUDE_NONNET  NUMBER
   , P_LEVEL           NUMBER
   , P_ORG_ID          NUMBER
   , P_SUBINV          VARCHAR2
   , P_ITEM_ID         NUMBER
) RETURN NUMBER ;

Function get_org_rec_qty (p_inventory_item_id in number, p_organization_id in number) return number;
   function has_drop_ships(p_po_header_id number,p_req_line_id number,p_po_line_location_id number)
    return varchar2;
   function is_not_vmi(p_req_type varchar2,p_req_line_id number ,p_po_line_location_id number)
   return varchar2;
    function is_nettable_subinv(p_organization_id number,p_subinv_name varchar2)
    return varchar2;
procedure get_isr_open_req_qty (p_organization_id in number ,
                                                 p_inventory_item_id in number,
                                                 p_both_qty out number,
                                                 p_vendor_qty out number,
                                                 p_inventory_qty out number,
                                                 p_direct_qty out number 
                                                ); 

END eis_new_po_xxwc_isr_util_pkg;
/