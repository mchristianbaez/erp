/*
TMS: 20160726-00140 
Date: 08/17/2016
Notes: data fix script to clsoe the order line

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
  --1 row expected to be updated

  UPDATE apps.oe_order_lines_all
     SET invoice_interface_status_code = 'NOT_ELIGIBLE'
        ,open_flag                     = 'N'
        ,flow_status_code              = 'CLOSED'
   WHERE line_id                       = 66866937
     AND headeR_id                     = 40844904;
  
  DBMS_OUTPUT.put_line ('Rows Updated in oe_order_lines_all: '||sql%rowcount);
  
  COMMIT;
  
  DBMS_OUTPUT.put_line ('Update Committed');
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.put_line ('TMS: 20160726-00140 Errors ='||SQLERRM);
END;*/

/*************************************************************************
  $Header TMS_20160726-00140_Stuck_in_shipped_status.sql $
  Module Name: TMS#20160726-00140 - Order 21409241  -- Data fix Script 

  PURPOSE: Data fix to update the order status

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        14-OCT-2016 Pattabhi Avula         TMS#20160726-00140 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160726-00140    , Before Update');
   
UPDATE   apps.oe_order_lines_all
SET      INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
        ,open_flag='N'
        ,flow_status_code='CLOSED'
WHERE   line_id =66866937
AND     headeR_id=40844904;

 DBMS_OUTPUT.put_line (
         'TMS: 20160726-00140  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160726-00140    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160726-00140 , Errors : ' || SQLERRM);
END;
/