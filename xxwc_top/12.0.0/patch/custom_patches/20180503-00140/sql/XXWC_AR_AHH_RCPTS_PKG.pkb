CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_AHH_RCPTS_PKG
IS
   /**************************************************************************
   File Name: XXWC_AR_AHH_RCPTS_PKG

   PROGRAM TYPE: SQL Script

   PURPOSE:      package is used to create lockbox files for AH Harries Cash Receipts

   HISTORY
   VERSION DATE          AUTHOR(S)          DESCRIPTION
   ------- -----------   -----------------  ------------------------------------
   1.0     04-May-2018   P.Vamshidhar       Creation this package
                                            TMS#20180503-00140-AH Harries Cash Receipts Interface
   =============================================================================
   *****************************************************************************/

   l_distro_list    VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_err_callfrom   VARCHAR2 (75) DEFAULT 'XXWC_AR_AHH_RCPTS_PKG';


   PROCEDURE UC4_LOAD_AHH (errbuf                OUT VARCHAR2,
                           retcode               OUT NUMBER,
                           p_directory        IN     VARCHAR2,
                           p_user             IN     VARCHAR2,
                           p_responsibility   IN     VARCHAR2)
   IS
      l_dflt_email           fnd_user.email_address%TYPE
                                := 'HDSOracleDevelopers@hdsupply.com';
      l_sender               VARCHAR2 (100);
      l_host                 VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
      l_hostport             VARCHAR2 (20) := '25';
      l_sid                  VARCHAR2 (8);
      l_subject              VARCHAR2 (32767) DEFAULT NULL;
      l_body                 VARCHAR2 (32767) DEFAULT NULL;
      l_body_header          VARCHAR2 (32767) DEFAULT NULL;
      l_body_detail          VARCHAR2 (32767) DEFAULT NULL;
      l_body_footer          VARCHAR2 (32767) DEFAULT NULL;
      l_package              VARCHAR2 (50) := 'XXWC_AR_AHH_RCPTS_PKG.UC4_LOAD_AHH';
      l_email                fnd_user.email_address%TYPE;

      l_req_id               NUMBER NULL;
      v_phase                VARCHAR2 (50);
      v_status               VARCHAR2 (50);
      v_dev_status           VARCHAR2 (50);
      v_dev_phase            VARCHAR2 (50);
      v_message              VARCHAR2 (250);
      v_error_message        VARCHAR2 (3000);
      v_supplier_id          NUMBER;
      v_rec_cnt              NUMBER := 0;
      l_message              VARCHAR2 (150);
      l_errormessage         VARCHAR2 (3000);
      pl_errorstatus         NUMBER;
      l_can_submit_request   BOOLEAN := TRUE;
      l_globalset            VARCHAR2 (100);
      l_err_msg              VARCHAR2 (3000);
      l_err_code             NUMBER;
      l_sec                  VARCHAR2 (255);
      l_statement            VARCHAR2 (9000);
      l_user                 fnd_user.user_id%TYPE;
      l_directory            VARCHAR2 (100);
      l_file_name            VARCHAR2 (100);
      l_file_dir             VARCHAR2 (100);
      l_file_handle          UTL_FILE.file_type;
      l_file_name_archive    VARCHAR2 (100);

      -- Error DEBUG
      l_err_callfrom         VARCHAR2 (75) DEFAULT 'XXWC_AR_AHH_RCPTS_PKG';
      l_err_callpoint        VARCHAR2 (75) DEFAULT 'START';
      l_distro_list          VARCHAR2 (75)
                                DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      SELECT LOWER (NAME) INTO l_sid FROM v$database;

      --Database Sender
      l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';

      SELECT user_id
        INTO l_user
        FROM fnd_user
       WHERE user_name = UPPER (p_user);

      l_file_dir := 'XXWC_AR_AHH_LOCKBOX_IB_DIR';

      --  Setup parameters for running FND JOBS!
      l_can_submit_request :=
         xxcus_misc_pkg.set_responsibility (UPPER (p_user), p_responsibility);

      IF l_can_submit_request
      THEN
         l_globalset := 'Global Variables are set.';
      ELSE
         l_globalset := 'Global Variables are not set.';
         l_sec := 'Global Variables are not set for the XXCUS INTERFACE.';
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         RAISE PROGRAM_ERROR;
      END IF;

      BEGIN
         SELECT COUNT (*)
           INTO v_rec_cnt
           FROM XXWC.XXWC_AR_AHH_IB_FILES_TBL
          WHERE file_name LIKE 'AHH_%';

         IF v_rec_cnt > 0
         THEN
            BEGIN
               FOR c_file IN (SELECT file_name
                                FROM XXWC.XXWC_AR_AHH_IB_FILES_TBL
                               WHERE file_name LIKE 'AHH_%')
               LOOP
                  l_sec :=
                        'UC4 call to run concurrent request WC SQL*Loader-WC Lockbox file from AHH for file '
                     || c_file.file_name;

                  l_req_id :=
                     fnd_request.submit_request (
                        'XXWC',
                        'XXWC_AR_LOCKBX_AHH' --WC SQL*Loader-WC Lockbox file from AHH
                                            ,
                        NULL,
                        SYSDATE,
                        FALSE,
                        p_directory || c_file.file_name);
                  COMMIT;

                  IF (l_req_id != 0)
                  THEN
                     IF fnd_concurrent.wait_for_request (l_req_id,
                                                         6,
                                                         0,
                                                         v_phase,
                                                         v_status,
                                                         v_dev_phase,
                                                         v_dev_status,
                                                         v_message)
                     THEN
                        v_error_message :=
                              CHR (10)
                           || 'ReqID='
                           || l_req_id
                           || ' DPhase '
                           || v_dev_phase
                           || ' DStatus '
                           || v_dev_status
                           || CHR (10)
                           || ' MSG - '
                           || v_message;

                        IF    v_dev_phase != 'COMPLETE'
                           OR v_dev_status != 'NORMAL'
                        THEN
                           l_sec :=
                                 'An error/warning occured in the SQL LOADER for AHH Receipts, please review the Log for concurrent request '
                              || l_req_id
                              || ' - '
                              || v_error_message
                              || '.';
                           fnd_file.put_line (fnd_file.LOG, l_sec);
                           fnd_file.put_line (fnd_file.output, l_sec);

                           l_subject :=
                                 l_sid
                              || ' *** AHH SQLLDR for receipts Error/Warning for lockbox processing ***';
                           l_body_header :=
                              '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';

                           l_body_detail :=
                                 l_body_detail
                              || '<BR>AHH SQLLDR for receipts - concurrent request:  '
                              || l_req_id
                              || ' Processing file '
                              || c_file.file_name;
                           l_body_footer := l_body_footer;

                           l_body :=
                              l_body_header || l_body_detail || l_body_footer;
                           xxcus_misc_pkg.html_email (
                              p_to              => l_dflt_email,
                              p_from            => l_sender,
                              p_text            => 'test',
                              p_subject         => l_subject,
                              p_html            => l_body,
                              p_smtp_hostname   => l_host,
                              p_smtp_portnum    => l_hostport);
                        END IF;
                     ELSE
                        l_sec :=
                              'Concurrent program wait timed out on file "'
                           || c_file.file_name;
                        DBMS_OUTPUT.put_line (l_sec);
                     END IF;
                  ELSE
                     l_sec :=
                           'Concurrent Request not initated for AHH file "'
                        || c_file.file_name
                        || ' - '
                        || v_error_message
                        || '.';

                     l_subject :=
                        '*** AHH SQLLDR for receipts Error/Warning for lockbox processing ***';
                     l_body_header :=
                        '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';

                     l_body_detail :=
                           l_body_detail
                        || '<BR>AHH SQLLDR for receipts - file:  '
                        || c_file.file_name;
                     l_body_footer := l_body_footer;

                     l_body := l_body_header || l_body_detail || l_body_footer;
                     xxcus_misc_pkg.html_email (
                        p_to              => l_dflt_email,
                        p_from            => l_sender,
                        p_text            => 'test',
                        p_subject         => l_subject,
                        p_html            => l_body,
                        p_smtp_hostname   => l_host,
                        p_smtp_portnum    => l_hostport);
                  END IF;

                  --Version 1.3
                  l_file_name := c_file.file_name;

                  l_file_name_archive := 'LOADED_' || c_file.file_name;
                  fnd_file.put_line (fnd_file.LOG, l_file_name_archive);

                  BEGIN
                     UTL_FILE.frename (l_file_dir,
                                       l_file_name,
                                       l_file_dir,
                                       l_file_name_archive);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG,
                              'Exception...'
                           || l_file_name
                           || ' file does not exist to rename/archive...');
                  END;
               END LOOP;
            END;
         END IF;
      END;

      retcode := 0;
      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := l_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWC_AR_AHH_RCPTS_PKG.UC4_LOAD_AHH package with PROGRAM ERROR',
            p_distribution_list   => l_distro_list,
            p_module              => 'TM');

         fnd_file.put_line (fnd_file.output, 'Fix the error!');
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := l_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWC_AR_AHH_RCPTS_PKG.UC4_LOAD_AHH package with OTHERS Exception',
            p_distribution_list   => l_distro_list,
            p_module              => 'TM');
   END uc4_load_ahh;


   PROCEDURE AHH_LOCKBOX (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      l_file_name              VARCHAR2 (100);
      l_file_dir               VARCHAR2 (100);
      l_file_handle            UTL_FILE.file_type;
      l_file_name_temp         VARCHAR2 (100);
      l_file_name_archive      VARCHAR2 (100);
      l_file_log               VARCHAR2 (100);
      l_sid                    VARCHAR2 (8);
      l_trans_string           VARCHAR2 (1000);
      l_header_string          VARCHAR2 (5000);
      l_pmt_string             VARCHAR2 (10000);
      l_ovrflw_string          VARCHAR2 (10000);
      l_batch_trailer_string   VARCHAR2 (1000);
      l_lckbx_trailer_string   VARCHAR2 (1000);
      l_trans_trailer_string   VARCHAR2 (1000);
      l_control                VARCHAR2 (15);
      l_count                  NUMBER := 0;
      l_ck_cnt                 NUMBER := 0;
      l_cnt_inv                NUMBER;
      l_cnt_ovrflw             NUMBER;
      l_chk_count              NUMBER;
      l_chk_amt                NUMBER;
      l_batch                  NUMBER;
      l_req_id                 NUMBER := fnd_global.conc_request_id;
      l_resp                   VARCHAR2 (100)
                                  := 'HDS Credit Assoc Cash App Mgr - WC';
      l_org_name      CONSTANT hr_all_organization_units.name%TYPE
                                  := 'HDS White Cap - Org' ;
      l_org                    hr_all_organization_units.organization_id%TYPE;

      --submit lockbox
      v_new_trans_flag         VARCHAR2 (16) := 'Y';
      v_run_import             VARCHAR2 (16) := 'Y';
      v_run_validation         VARCHAR2 (16) := 'Y';
      v_post_partial           VARCHAR2 (16) := 'Y'; -- post partial amout as unapplied
      v_quick_cash             VARCHAR2 (16) := 'Y';  --submit post quick cash
      v_submiss_type           VARCHAR2 (16) := 'L';         --submission type
      v_pay_inv                VARCHAR2 (16) := 'N'; -- Pay Unrelated Invoices
      v_c_batch_only           VARCHAR2 (16) := 'N';  -- Complete Batches Only
      v_alt_serach             VARCHAR2 (16) := 'N'; -- Alternate name search option
      v_trans_format_id        NUMBER;
      v_lockbox_id             NUMBER;
      v_org_id                 NUMBER;
      v_req_id                 NUMBER;
      v_report_format          VARCHAR2 (16) := 'A'; --R 'Rejects Only', 'A' -all
      v_user_name              VARCHAR2 (164) := 'xxwc_int_finance';
      v_user_id                NUMBER;
      v_lockbox_directory      VARCHAR2 (512);
      v_data_file              VARCHAR2 (25) := 'WC_LOCKBOX_FILE';
      v_control_file           VARCHAR2 (25) := 'XXWCAR_DCTM_LOCKBOX';
      --v_trans_format           VARCHAR2 (25) := 'XXWC_LOCKBOX_DCTM';
      v_trans_format           VARCHAR2 (25) := 'XXWC_LOCKBOX_CONV';
      v_trans_name             VARCHAR2 (25) := 'DCTM_';

      v_resp_id                NUMBER;
      v_resp_appl_id           NUMBER;

      v_set_req_status         BOOLEAN;
      v_req_phase              VARCHAR2 (80);
      v_req_status             VARCHAR2 (80);
      v_dev_phase              VARCHAR2 (30);
      v_dev_status             VARCHAR2 (30);
      v_message                VARCHAR2 (255);
      v_interval               NUMBER := 10;                     -- In seconds
      v_max_time               NUMBER := 15000;                  -- In seconds
      v_completion_status      VARCHAR2 (30) := 'NORMAL';
      v_error_message          VARCHAR2 (3000);
      v_file_dir               VARCHAR2 (100);
      v_instance               VARCHAR2 (8);
      v_shortname              VARCHAR2 (30);
      v_count                  NUMBER;

      l_can_submit_request     BOOLEAN := TRUE;
      l_globalset              VARCHAR2 (100);
      l_err_msg                VARCHAR2 (3000);
      l_err_code               NUMBER;
      l_sec                    VARCHAR2 (255);
      l_message                VARCHAR2 (150);
      l_statement              VARCHAR2 (9000);
      l_err_callfrom           VARCHAR2 (75) DEFAULT 'XXWC_AR_AHH_RCPTS_PKG';
      l_err_callpoint          VARCHAR2 (75) DEFAULT 'START';
      --l_distro_list        VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_procedure_name         VARCHAR2 (75)
                                  := 'XXWC_AR_AHH_RCPTS_PKG.AHH_LOCKBOX';
      lvc_bank_acct_num        CE_BANK_ACCOUNTS.BANK_ACCOUNT_NUM%TYPE;
      lvc_customer_num         VARCHAR2 (100);

      CURSOR c_header (
         p_date        DATE,
         p_bank_nbr    VARCHAR2)
      IS
         SELECT DISTINCT l.lockbox_number,
                         r.deposit_date,
                         '4100067610' dest_acct,
                         l.bank_origination_number
           FROM xxwc.xxwcar_cash_rcpts_tbl r,
                ar.ar_lockboxes_all l,
                apps.fnd_lookup_values h
          WHERE     UPPER (r.operator_code) = h.lookup_code
                AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                AND h.enabled_flag = 'Y'
                AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                AND h.meaning = l.lockbox_number
                AND lockbox_number NOT LIKE 'PRISM%'
                AND l.status = 'A'
                AND l.org_id = l_org
                AND r.status = 'NEW'
                AND r.deposit_date = p_date
                AND l.bank_origination_number = p_bank_nbr;

      CURSOR c_control_trans (
         p_lockbox    VARCHAR2,
         p_date       DATE)
      IS
         SELECT DISTINCT l.lockbox_number, r.deposit_date, control_nbr
           FROM xxwc.xxwcar_cash_rcpts_tbl r,
                ar.ar_lockboxes_all l,
                apps.fnd_lookup_values h
          WHERE     UPPER (r.operator_code) = h.lookup_code
                AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                AND h.enabled_flag = 'Y'
                AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                AND h.meaning = l.lockbox_number
                AND lockbox_number NOT LIKE 'PRISM%'
                AND l.status = 'A'
                AND l.org_id = l_org
                AND r.status = 'NEW'
                AND l.lockbox_number = p_lockbox
                AND r.deposit_date = p_date;

      CURSOR c_check (
         p_lockbox    VARCHAR2,
         p_date       DATE,
         p_control    VARCHAR2)
      IS
           SELECT DISTINCT r.check_nbr,
                           r.check_amt,
                           r.deposit_date,
                           l.lockbox_number,
                           r.url_link,
                           r.comments
             FROM xxwc.xxwcar_cash_rcpts_tbl r,
                  ar.ar_lockboxes_all l,
                  apps.fnd_lookup_values h
            WHERE     UPPER (r.operator_code) = h.lookup_code
                  AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                  AND h.enabled_flag = 'Y'
                  AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                  AND h.meaning = l.lockbox_number
                  AND lockbox_number NOT LIKE 'PRISM%'
                  AND l.status = 'A'
                  AND l.org_id = l_org
                  AND r.status = 'NEW'
                  AND l.lockbox_number = p_lockbox
                  AND r.deposit_date = p_date
                  AND r.control_nbr = p_control
         ORDER BY r.check_nbr;
   BEGIN
      SELECT LOWER (NAME) INTO l_sid FROM v$database;

      v_lockbox_directory := '/xx_iface/' || l_sid || '/inbound/ar/lockbox/';

      SELECT organization_id
        INTO l_org
        FROM hr_all_organization_units
       WHERE NAME = l_org_name;

      SELECT COUNT (*)
        INTO l_count
        FROM xxwc.xxwcar_cash_rcpts_tbl r,
             ar.ar_lockboxes_all l,
             apps.fnd_lookup_values h
       WHERE     UPPER (r.operator_code) = h.lookup_code
             AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
             AND h.enabled_flag = 'Y'
             AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
             AND h.meaning = l.lockbox_number
             AND lockbox_number NOT LIKE 'PRISM%'
             AND l.status = 'A'
             AND l.org_id = l_org
             AND r.status = 'NEW';

      IF l_count > 0
      THEN
         DBMS_OUTPUT.put_line (
            'Number of records to process from Documentum:  ' || l_count);

         BEGIN
            SELECT user_id
              INTO v_user_id
              FROM fnd_user
             WHERE user_name = UPPER (v_user_name);
         EXCEPTION
            WHEN OTHERS
            THEN
               v_user_id := 0;
         END;

         l_file_dir := 'XXWC_AR_AHH_LOCKBOX_IB_DIR';

         l_sec := 'Starting the AR Lockbox file creation from DCTM; ';
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);

         -----------------------------------------------------------
         -- Deriving Bank Account
         -----------------------------------------------------------
         BEGIN
            SELECT CBA.BANK_ACCOUNT_NUM
              INTO lvc_bank_acct_num
              FROM APPS.AR_RECEIPT_METHOD_ACCOUNTS_ALL ARM,
                   APPS.AR_RECEIPT_METHODS ARA,
                   CE_BANK_ACCT_USES_ALL CBAU,
                   CE_BANK_ACCOUNTS CBA
             WHERE     ARA.RECEIPT_METHOD_ID = ARM.RECEIPT_METHOD_ID
                   AND ARA.NAME = 'WC HARRIS'
                   AND ARM.REMIT_BANK_ACCT_USE_ID = CBAU.BANK_ACCT_USE_ID
                   AND CBAU.BANK_ACCOUNT_ID = CBA.BANK_ACCOUNT_ID;
         EXCEPTION
            WHEN OTHERS
            THEN
               lvc_bank_acct_num := NULL;
         END;

         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'lvc_bank_acct_num:' || lvc_bank_acct_num);


         BEGIN
            FOR c_trans
               IN (SELECT DISTINCT
                          r.deposit_date,
                          lvc_bank_acct_num dest_acct,
                          l.bank_origination_number
                     FROM xxwc.xxwcar_cash_rcpts_tbl r,
                          ar.ar_lockboxes_all l,
                          apps.fnd_lookup_values h
                    WHERE     UPPER (r.operator_code) = h.lookup_code
                          AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                          AND h.enabled_flag = 'Y'
                          AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                          AND h.meaning = l.lockbox_number
                          AND lockbox_number NOT LIKE 'PRISM%'
                          AND l.status = 'A'
                          AND l.org_id = l_org
                          AND r.status = 'NEW')
            LOOP
               BEGIN
                  l_sec := 'Truncate the summary table before loading.';

                  EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWCAR_LOCKBOX_TBL';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_msg :=
                           'Failed to Truncate XXWC.XXWCAR_LOCKBOX_TBL: '
                        || SQLERRM;

                     DBMS_OUTPUT.put_line (l_err_msg);
                     RAISE PROGRAM_ERROR;
               END;

               INSERT INTO xxwc.xxwcar_lockbox_tbl
                  SELECT DISTINCT check_nbr,
                                  l.lockbox_number,
                                  r.deposit_date,
                                  check_amt,
                                  control_nbr,
                                  l.bank_origination_number
                    FROM xxwc.xxwcar_cash_rcpts_tbl r,
                         ar.ar_lockboxes_all l,
                         apps.fnd_lookup_values h
                   WHERE     UPPER (r.operator_code) = h.lookup_code
                         AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                         AND h.enabled_flag = 'Y'
                         AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                         AND h.meaning = l.lockbox_number
                         AND lockbox_number NOT LIKE 'PRISM%'
                         AND l.status = 'A'
                         AND l.org_id = l_org
                         AND r.status = 'NEW'
                         AND r.deposit_date = c_trans.deposit_date;

               COMMIT;                                           --Version 1.5
               --Set the file name and open the file
               l_file_name_temp := 'TEMP_' || 'WC_LOCKBOX_FILE.txt';

               l_file_name :=
                     'WC_LOCKBOX_FILE_'
                  || TO_CHAR (SYSDATE, 'YYYYMMDD_HH24MISS')
                  || '.txt';

               fnd_file.put_line (fnd_file.LOG,
                                  'Filename generated : ' || l_file_name);
               fnd_file.put_line (fnd_file.output,
                                  'Filename generated : ' || l_file_name);
               DBMS_OUTPUT.put_line ('Filename generated : ' || l_file_name);

               l_file_handle :=
                  UTL_FILE.fopen (l_file_dir, l_file_name_temp, 'w');

               fnd_file.put_line (fnd_file.LOG,
                                  'l_file_name_temp : ' || l_file_name_temp);


               BEGIN
                  l_trans_string :=
                        '1'
                     || RPAD (c_trans.dest_acct, 25, ' ')
                     || RPAD (c_trans.bank_origination_number, 12, ' ')
                     || RPAD (c_trans.deposit_date, 9, ' ');

                  UTL_FILE.put_line (l_file_handle, l_trans_string);
                  FND_FILE.PUT_LINE (FND_FILE.LOG,
                                     'l_trans_string:' || l_trans_string);

                  FOR c_lckbx
                     IN c_header (c_trans.deposit_date,
                                  c_trans.bank_origination_number)
                  LOOP
                     -- Added to not pickup PRISM data
                     IF c_lckbx.lockbox_number NOT LIKE 'PRISM%'
                     THEN
                        l_header_string :=
                              '2'
                           || RPAD (c_lckbx.lockbox_number, 30, ' ')
                           || RPAD (c_trans.deposit_date, 9, ' ')
                           || RPAD (c_trans.dest_acct, 24, ' ')
                           || RPAD (c_trans.bank_origination_number, 11, ' ');
                        FND_FILE.PUT_LINE (
                           FND_FILE.LOG,
                           'l_header_string:' || l_header_string);
                     END IF;

                     UTL_FILE.put_line (l_file_handle, l_header_string);

                     FOR c_batch
                        IN c_control_trans (c_lckbx.lockbox_number,
                                            c_lckbx.deposit_date)
                     LOOP
                        FOR c_chk
                           IN c_check (c_lckbx.lockbox_number,
                                       c_lckbx.deposit_date,
                                       c_batch.control_nbr)
                        LOOP
--                           FOR c_chk_cust
--                              IN (SELECT check_amt,
--                                         deposit_date,
--                                         lockbox_number,
--                                         url_link,
--                                         invoice_total,
--                                         account_number,
--                                         check_nbr,
--                                         control_nbr
--                                    FROM (  SELECT r.check_amt,
--                                                   r.deposit_date,
--                                                   l.lockbox_number,
--                                                   r.url_link,
--                                                   SUM (r.invoice_amt)
--                                                      invoice_total,
--                                                   ca.account_number,
--                                                   r.check_nbr,
--                                                   control_nbr
--                                              FROM xxwc.xxwcar_cash_rcpts_tbl r,
--                                                   ar.ar_lockboxes_all l,
--                                                   apps.fnd_lookup_values h,
--                                                   ar.hz_cust_accounts ca
--                                             WHERE     UPPER (r.operator_code) =
--                                                          h.lookup_code
--                                                   AND h.lookup_type =
--                                                          'XXWC_AR_LOCKBOX_DCTM'
--                                                   AND h.enabled_flag = 'Y'
--                                                   AND NVL (h.end_date_active,
--                                                            SYSDATE) >= SYSDATE
--                                                   AND h.meaning =
--                                                          l.lockbox_number
--                                                   AND lockbox_number NOT LIKE
--                                                          'PRISM%'
--                                                   AND l.status = 'A'
--                                                   AND l.org_id = l_org
--                                                   AND r.status = 'NEW'
--                                                   AND r.customer_nbr =
--                                                          ca.account_number(+)
--                                                   AND l.lockbox_number =
--                                                          c_chk.lockbox_number
--                                                   AND r.deposit_date =
--                                                          c_chk.deposit_date
--                                                   AND r.control_nbr =
--                                                          c_batch.control_nbr --Version 1.5
--                                                   AND check_nbr =
--                                                          c_chk.check_nbr
--                                                   AND r.check_amt =
--                                                          c_chk.check_amt --Version 1.5
--                                          GROUP BY r.check_nbr,
--                                                   r.check_amt,
--                                                   r.deposit_date,
--                                                   l.lockbox_number,
--                                                   r.url_link,
--                                                   r.comments,
--                                                   ca.account_number,
--                                                   control_nbr
--                                          ORDER BY r.check_nbr,
--                                                   SUM (r.invoice_amt) DESC)
--                                   WHERE ROWNUM < 2              --Version 1.5
--                                                   )
--                           LOOP
                           FOR c_chk_cust
                              IN (SELECT check_amt,
                                         deposit_date,
                                         lockbox_number,
                                         url_link,
                                         invoice_total,
                                         account_number,
                                         check_nbr,
                                         control_nbr
                                    FROM (  SELECT r.check_amt,
                                                   r.deposit_date,
                                                   l.lockbox_number,
                                                   r.url_link,
                                                   SUM (r.invoice_amt)
                                                      invoice_total,
                                                   r.customer_nbr account_number,
                                                   r.check_nbr,
                                                   control_nbr
                                              FROM xxwc.xxwcar_cash_rcpts_tbl r,
                                                   ar.ar_lockboxes_all l,
                                                   apps.fnd_lookup_values h
                                                   --ar.hz_cust_accounts ca
                                             WHERE     UPPER (r.operator_code) =
                                                          h.lookup_code
                                                   AND h.lookup_type =
                                                          'XXWC_AR_LOCKBOX_DCTM'
                                                   AND h.enabled_flag = 'Y'
                                                   AND NVL (h.end_date_active,
                                                            SYSDATE) >= SYSDATE
                                                   AND h.meaning =
                                                          l.lockbox_number
                                                   AND lockbox_number NOT LIKE
                                                          'PRISM%'
                                                   AND l.status = 'A'
                                                   AND l.org_id = l_org
                                                   AND r.status = 'NEW'
                                                   --AND r.customer_nbr = ca.account_number(+)
                                                   AND l.lockbox_number =
                                                          c_chk.lockbox_number
                                                   AND r.deposit_date =
                                                          c_chk.deposit_date
                                                   AND r.control_nbr =
                                                          c_batch.control_nbr --Version 1.5
                                                   AND check_nbr =
                                                          c_chk.check_nbr
                                                   AND r.check_amt =
                                                          c_chk.check_amt --Version 1.5
                                          GROUP BY r.check_nbr,
                                                   r.check_amt,
                                                   r.deposit_date,
                                                   l.lockbox_number,
                                                   r.url_link,
                                                   r.comments,
                                                   r.customer_nbr,
                                                   control_nbr
                                          ORDER BY r.check_nbr,
                                                   SUM (r.invoice_amt) DESC)
                                   WHERE ROWNUM < 2              --Version 1.5
                                                   )
                           LOOP

                              lvc_customer_num := NULL;
                              l_ck_cnt := l_ck_cnt + 1;
                              FND_FILE.PUT_LINE (FND_FILE.LOG,
                                                 'l_ck_cnt:' || l_ck_cnt);

                              FND_FILE.PUT_LINE(FND_FILE.LOG,'c_chk_cust.account_number:'||c_chk_cust.account_number);
                              BEGIN
                                 SELECT TRIM(ORACLE_CUST_NUM)
                                   INTO lvc_customer_num
                                   FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T
                                  WHERE     TRIM(CUST_NUM) =
                                               c_chk_cust.account_number
                                        AND ROWNUM = 1;
                              EXCEPTION
                                 WHEN OTHERS
                                 THEN
                                    lvc_customer_num := NULL;
                              END;

                              IF lvc_customer_num IS NULL
                              THEN
                                 BEGIN
                                    SELECT hca.account_number
                                      INTO lvc_customer_num
                                      FROM apps.hz_cust_accounts hca,
                                           hz_cust_acct_sites_all hcas
                                     WHERE     hca.cust_account_id =
                                                  hcas.cust_account_id
                                           AND NVL (hcas.attribute17, '123') =
                                                  c_chk_cust.account_number
                                           AND hca.attribute4 = 'AHH'
                                           AND hca.status = 'A'
                                           AND BILL_TO_FLAG = 'P';
                                 EXCEPTION
                                    WHEN OTHERS
                                    THEN
                                       lvc_customer_num := NULL;
                                 END;
                              END IF;

                              FND_FILE.PUT_LINE(FND_FILE.LOG,'lvc_customer_num:'||lvc_customer_num);
                              -- Added to not pickup PRISM data
                              IF c_lckbx.lockbox_number NOT LIKE 'PRISM%'
                              THEN
                                 l_pmt_string :=
                                       '3'
                                    || RPAD (c_chk_cust.control_nbr, 20, ' ')
                                    || LPAD (l_ck_cnt, 4, '0')
                                    || LPAD (c_chk_cust.check_amt, 16, ' ')
                                    || LPAD (c_chk_cust.check_nbr, 30, ' ')
                                    || 'USD'
                                    || RPAD (NVL (lvc_customer_num, ' '),
                                             30,
                                             ' ')
                                    || RPAD (c_chk_cust.deposit_date, 9, ' ')
                                    || RPAD (c_chk_cust.lockbox_number,
                                             30,
                                             ' ')
                                    || c_chk.url_link;
                              END IF;

                              UTL_FILE.put_line (l_file_handle, l_pmt_string);
                              FND_FILE.PUT_LINE (
                                 FND_FILE.LOG,
                                 'l_pmt_string:' || l_pmt_string);

                              l_cnt_inv := NULL;

                              SELECT COUNT (*)
                                INTO l_cnt_inv
                                FROM xxwc.xxwcar_cash_rcpts_tbl r,
                                     apps.fnd_lookup_values h
                               WHERE     r.status = 'NEW'
                                     AND r.deposit_date =
                                            c_lckbx.deposit_date
                                     AND r.check_nbr = c_chk.check_nbr
                                     AND r.control_nbr =
                                            c_chk_cust.control_nbr --Version 1.5
                                     AND r.check_amt = c_chk_cust.check_amt --Version 1.5
                                     AND UPPER (r.operator_code) =
                                            h.lookup_code
                                     AND h.lookup_type =
                                            'XXWC_AR_LOCKBOX_DCTM'
                                     AND h.enabled_flag = 'Y'
                                     AND NVL (h.end_date_active, SYSDATE) >=
                                            SYSDATE
                                     AND h.meaning = c_lckbx.lockbox_number;

                              FND_FILE.PUT_LINE (FND_FILE.LOG,
                                                 'l_cnt_inv:' || l_cnt_inv);

                              FOR c_pay
                                 IN (  SELECT r.ROWID,
                                              r.check_nbr,
                                              r.check_amt,
                                              r.invoice_nbr,
                                              r.invoice_amt,
                                              r.deposit_date,
                                              l.lockbox_number,
                                              r.control_nbr,
                                              ROWNUM nbr_count
                                         FROM xxwc.xxwcar_cash_rcpts_tbl r,
                                              ar.ar_lockboxes_all l,
                                              apps.fnd_lookup_values h
                                        WHERE     UPPER (r.operator_code) =
                                                     h.lookup_code
                                              AND h.lookup_type =
                                                     'XXWC_AR_LOCKBOX_DCTM'
                                              AND h.enabled_flag = 'Y'
                                              AND NVL (h.end_date_active,
                                                       SYSDATE) >= SYSDATE
                                              AND h.meaning = l.lockbox_number
                                              AND lockbox_number NOT LIKE
                                                     'PRISM%'
                                              AND l.status = 'A'
                                              AND l.org_id = l_org
                                              AND r.status = 'NEW'
                                              AND l.lockbox_number =
                                                     c_lckbx.lockbox_number
                                              AND r.deposit_date =
                                                     c_lckbx.deposit_date
                                              AND r.check_nbr = c_chk.check_nbr
                                              AND r.control_nbr =
                                                     c_chk_cust.control_nbr
                                              AND r.check_amt =
                                                     c_chk_cust.check_amt
                                     ORDER BY nbr_count)
                              LOOP
                                 IF l_cnt_inv = c_pay.nbr_count
                                 THEN
                                    l_cnt_ovrflw := 9;
                                 ELSE
                                    l_cnt_ovrflw := 0;
                                 END IF;

                                 IF c_lckbx.lockbox_number NOT LIKE 'PRISM%'
                                 THEN
                                    l_ovrflw_string :=
                                          '4'
                                       || RPAD (c_pay.control_nbr, 20, ' ')
                                       || LPAD (l_ck_cnt, 4, '0')
                                       || LPAD (c_pay.nbr_count, 3, '0')
                                       || l_cnt_ovrflw
                                       || RPAD (
                                             NVL (c_pay.invoice_nbr, 'ZZZZZ'),
                                             20,
                                             ' ')
                                       || RPAD (
                                             NVL (c_pay.invoice_amt,
                                                  c_pay.check_amt),
                                             16,
                                             ' ');
                                 END IF;

                                 UTL_FILE.put_line (l_file_handle,
                                                    l_ovrflw_string);

                                 FND_FILE.PUT_LINE (
                                    FND_FILE.LOG,
                                    'l_ovrflw_string:' || l_ovrflw_string);

                                 UPDATE xxwc.xxwcar_cash_rcpts_tbl
                                    SET status = 'AHH_PROCESSED'
                                  WHERE     check_nbr = c_pay.check_nbr
                                        AND NVL (invoice_nbr, 'ZZZZZ') =
                                               NVL (c_pay.invoice_nbr,
                                                    'ZZZZZ')
                                        AND deposit_date = c_pay.deposit_date
                                        AND ROWID = c_pay.ROWID;
                              END LOOP;           --invoice details by receipt
                           END LOOP;                         --receipt details
                        END LOOP;                            --receipt summary

                        SELECT COUNT (*), SUM (check_amt)
                          INTO l_chk_count, l_chk_amt
                          FROM xxwc.xxwcar_lockbox_tbl
                         WHERE     lockbox_number = c_lckbx.lockbox_number
                               AND control_nbr = c_batch.control_nbr;

                        l_batch_trailer_string :=
                              '7'
                           || RPAD (c_batch.control_nbr, 20, ' ')
                           || RPAD (c_lckbx.lockbox_number, 30, ' ')
                           || RPAD (c_lckbx.deposit_date, 9, ' ')
                           || RPAD (l_chk_count, 4, ' ')
                           || RPAD (l_chk_amt, 16, ' ');

                        UTL_FILE.put_line (l_file_handle,
                                           l_batch_trailer_string);
                     END LOOP;     --c_batch totals by lockbox and control nbr

                     SELECT COUNT (*), SUM (check_amt)
                       INTO l_chk_count, l_chk_amt
                       FROM xxwc.xxwcar_lockbox_tbl
                      WHERE lockbox_number = c_lckbx.lockbox_number;

                     SELECT COUNT (DISTINCT control_nbr)
                       INTO l_batch
                       FROM xxwc.xxwcar_lockbox_tbl
                      WHERE lockbox_number = c_lckbx.lockbox_number;

                     l_lckbx_trailer_string :=
                           '8'
                        || RPAD (c_lckbx.lockbox_number, 30, ' ')
                        || c_lckbx.deposit_date
                        || RPAD (l_chk_count, 4, ' ')
                        || RPAD (l_chk_amt, 16, ' ')
                        || RPAD (l_batch, 4, ' ');

                     UTL_FILE.put_line (l_file_handle,
                                        l_lckbx_trailer_string);
                  END LOOP;                        --c_lckbx  total by lockbox

                  --Close the file
                  l_sec := 'Closing the File; ';
                  fnd_file.put_line (fnd_file.LOG, l_sec);
                  fnd_file.put_line (fnd_file.output, l_sec);
                  UTL_FILE.fclose (l_file_handle);
                  UTL_FILE.frename (l_file_dir,
                                    l_file_name_temp,
                                    l_file_dir,
                                    l_file_name);

                  -- File moving to Lockbox directory  -- Vamshi
                  UTL_FILE.frename (l_file_dir,
                                    l_file_name,
                                    'XXWC_AR_AHH_LOCKBOX_DIR',
                                    l_file_name,
                                    TRUE);


                  FND_FILE.PUT_LINE (FND_FILE.LOG, 'File Closed');
               END;

               BEGIN
                  SELECT organization_id
                    INTO v_org_id
                    FROM hr_all_organization_units
                   WHERE NAME = 'HDS White Cap - Org';

                  SELECT transmission_format_id
                    INTO v_trans_format_id
                    FROM ar_transmission_formats
                   --WHERE format_name = 'XXWC_LOCKBOX_DCTM';  commented by Vamshi
                   WHERE format_name = 'XXWC_LOCKBOX_CONV';   -- Added by Vamshi.

                  BEGIN
                     SELECT responsibility_id, application_id
                       INTO v_resp_id, v_resp_appl_id
                       FROM fnd_responsibility_vl
                      WHERE     responsibility_name =
                                   'HDS Credit Assoc Cash App Mgr - WC'
                            AND SYSDATE BETWEEN start_date
                                            AND NVL (end_date,
                                                     TRUNC (SYSDATE) + 1);
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        l_err_msg :=
                              'Responsibility - '
                           || 'HDS Credit Assoc Cash App Mgr - WC'
                           || ' not defined in Oracle';

                        DBMS_OUTPUT.put_line (l_err_msg);
                        RAISE PROGRAM_ERROR;
                     WHEN OTHERS
                     THEN
                        l_err_msg :=
                              'Error deriving Responsibility_id for ResponsibilityName - '
                           || 'HDS Credit Assoc Cash App Mgr - WC';

                        DBMS_OUTPUT.put_line (l_err_msg);
                        RAISE PROGRAM_ERROR;
                  END;

                  fnd_global.apps_initialize (
                     user_id        => v_user_id,
                     resp_id        => v_resp_id,
                     resp_appl_id   => v_resp_appl_id);

                  mo_global.init ('AR');
                  mo_global.set_policy_context ('S', l_org);     --Version 3.1
                  fnd_request.set_org_id (l_org);                --Version 3.1

                  v_req_id :=
                     fnd_request.submit_request (
                        'AR',
                        'ARLPLB',
                        NULL,
                        NULL,
                        FALSE,
                        v_new_trans_flag,
                        NULL                                -- Transmission Id
                            ,
                        NULL                            -- Original Request Id
                            ,
                           v_trans_name
                        || TO_CHAR (SYSDATE, 'mmddyyyy HH24:MI:SS') -- Transmission Name
                                                                   ,
                        v_run_import                          -- Submit Import
                                    ,
                        v_lockbox_directory || l_file_name        -- Data File
                                                          ,
                        v_control_file                         -- Control File
                                      ,
                        v_trans_format_id                --Transmission Format
                                         ,
                        v_run_validation                  -- Submit Validation
                                        ,
                        v_pay_inv                    -- Pay Unrelated Invoices
                                 ,
                        v_lockbox_id                             -- Lockbox Id
                                    ,
                        NULL                                        -- GL Date
                            ,
                        v_report_format                       -- Report Format
                                       ,
                        v_c_batch_only                -- Complete Batches Only
                                      ,
                        v_quick_cash                      -- Submit Post batch
                                    ,
                        v_alt_serach           -- Alternate name search option
                                    ,
                        v_post_partial --Post Partial Amount or Reject Entire Receipt
                                      ,
                        NULL                         -- USSGL transaction code
                            ,
                        v_org_id,
                        'Y', --Apply unearn discount         --Added by Maha for ver 2.0 on 7/23/14
                        1, --No of instances                  --Added by Maha for ver 3.0 on 7/30/14
                        v_submiss_type,                     -- submission type
                        NULL,
                        CHR (0),
                        CHR (0));
                  COMMIT;

                  fnd_file.put_line (fnd_file.LOG, v_req_id);
                  fnd_file.put_line (fnd_file.output, v_req_id);

                  --Write detail Line Log rows
                  l_sec := 'Waiting for Lockbox to finish ' || v_req_id;
                  fnd_file.put_line (fnd_file.LOG, l_sec);
                  fnd_file.put_line (fnd_file.output, l_sec);

                  DBMS_OUTPUT.put_line (
                     'Submitting Lockbox for file:  ' || l_file_name);
                  DBMS_OUTPUT.put_line (
                        'Concurrent Program Process Lockbox Submitted. Request ID: '
                     || v_req_id);

                  --Wait for Lockbox
                  IF (v_req_id != 0)
                  THEN
                     IF fnd_concurrent.wait_for_request (v_req_id,
                                                         v_interval,
                                                         v_max_time,
                                                         v_req_phase,
                                                         v_req_status,
                                                         v_dev_phase,
                                                         v_dev_status,
                                                         v_message)
                     THEN
                        v_error_message :=
                              'ReqID = '
                           || v_req_id
                           || '   DPhase = '
                           || v_dev_phase
                           || '   DStatus = '
                           || v_dev_status
                           || '   MSG = '
                           || v_message;

                        IF    v_dev_phase != 'COMPLETE'
                           OR v_dev_status != 'NORMAL'
                        THEN
                           l_statement :=
                                 'An error occured in the running of the XXWC_AR_AHH_RCPTS_PKG HDS AR AHH Lockbox Process'
                              || v_error_message
                              || '.';
                           fnd_file.put_line (fnd_file.LOG, l_statement);
                           fnd_file.put_line (fnd_file.output, l_statement);

                           DBMS_OUTPUT.put_line (l_statement);

                           retcode := 2;
                           RAISE PROGRAM_ERROR;
                        END IF;
                     -- Then Success!

                     ELSE
                        l_statement :=
                              'Wait Timed Out-running the XXWC_AR_AHH_RCPTS_PKG.AHH_LOCKBOX Process Lockbox'
                           || v_error_message
                           || '.';

                        l_err_msg := l_statement;
                        fnd_file.put_line (fnd_file.LOG, l_err_msg);
                        fnd_file.put_line (fnd_file.output, l_err_msg);
                        DBMS_OUTPUT.put_line (l_statement);
                        RAISE PROGRAM_ERROR;
                     END IF;
                  ELSE
                     l_statement :=
                        'An error occured XXWC_AR_AHH_RCPTS_PKG.AHH_LOCKBOX - Process Lockbox not initated';

                     l_err_msg := l_statement;
                     fnd_file.put_line (fnd_file.LOG, l_err_msg);
                     fnd_file.put_line (fnd_file.output, l_err_msg);
                     DBMS_OUTPUT.put_line (l_statement);
                     RAISE PROGRAM_ERROR;
                  END IF;

                  retcode := 0;
               END;
            END LOOP;
         END;
      ELSE
         DBMS_OUTPUT.put_line (
            'Number of records to process from Documentum:  0');
         retcode := 0;
      END IF;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         l_err_callpoint := l_sec;

         DBMS_OUTPUT.put_line (l_err_msg);

         UTL_FILE.fclose (l_file_handle);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWC_AR_AHH_RCPTS_PKG with PROGRAM ERROR',
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
      WHEN OTHERS
      THEN
         ROLLBACK;

         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         l_err_callpoint := l_sec;

         DBMS_OUTPUT.put_line (l_err_msg);

         UTL_FILE.fclose (l_file_handle);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWC_AR_AHH_RCPTS_PKG with OTHERS Exception',
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
   END ahh_lockbox;
END XXWC_AR_AHH_RCPTS_PKG;
/