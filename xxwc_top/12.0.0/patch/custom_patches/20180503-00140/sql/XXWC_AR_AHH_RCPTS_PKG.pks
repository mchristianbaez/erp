CREATE OR REPLACE PACKAGE APPS.XXWC_AR_AHH_RCPTS_PKG
IS
   /**************************************************************************
   File Name: XXWC_AR_AHH_RCPTS_PKG

   PROGRAM TYPE: SQL Script

   PURPOSE:      package is used to create lockbox files for AH Harries Cash Receipts

   HISTORY
   VERSION DATE          AUTHOR(S)          DESCRIPTION
   ------- -----------   -----------------  ------------------------------------
   1.0     04-May-2018   P.Vamshidhar       Creation this package
   =============================================================================
   *****************************************************************************/

   -- PROCEDURE ahh_lockbox (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   PROCEDURE uc4_load_ahh (errbuf                OUT VARCHAR2,
                           retcode               OUT NUMBER,
                           p_directory        IN     VARCHAR2,
                           p_user             IN     VARCHAR2,
                           p_responsibility   IN     VARCHAR2);

   PROCEDURE AHH_LOCKBOX (errbuf OUT VARCHAR2, retcode OUT NUMBER);
END XXWC_AR_AHH_RCPTS_PKG;
/