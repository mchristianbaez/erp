/*************************************************************************
  $Header XXWC_AR_AHH_LOCKBOX_IB_DIR.sql $
  Module Name: XXWC_AR_AHH_LOCKBOX_IB_DIR
  PURPOSE: To access AH Harries Cash receipts data files.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
   1.0        05/04/2018  P.Vamshidhar          TMS#20180503-00140-AH Harries Cash Receipts Interface
*******************************************************************************************************************************************/ 
DECLARE
   lvc_environment   VARCHAR2 (100);
   lvc_sql           VARCHAR2 (4000);
BEGIN
   SELECT '/xx_iface/' || LOWER (NAME) || '/inbound/ar/ahh_rcpts'
     INTO lvc_environment
     FROM v$database;

   lvc_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_AR_AHH_LOCKBOX_IB_DIR AS '
      || ''''
      || lvc_environment
      || '''';

   EXECUTE IMMEDIATE lvc_sql;
END;
/