#!/bin/sh
cd /xx_iface/ebsprd/inbound/ar/ahh_rcpts
/bin/ls -l /xx_iface/ebsprd/inbound/ar/ahh_rcpts
# This file is called by the XXWC_AR_AHH_IB_FILES_TBL external table to
# show the files in the directory above.

# -- Create table
# create table XXWC.XXWC_AR_AHH_IB_FILES_TBL
# (
  # file_permissions  VARCHAR2(50),
  # file_type         VARCHAR2(64),
  # file_owner        VARCHAR2(64),
  # file_group        VARCHAR2(64),
  # file_size         VARCHAR2(64),
  # file_month        VARCHAR2(64),
  # file_day          VARCHAR2(64),
  # file_time  	      VARCHAR2(64),
  # file_name         VARCHAR2(612)
# )
# organization external
# (
  # type ORACLE_LOADER
  # default directory XXWC_AR_AHH_LOCKBOX_IB_DIR
  # access parameters 
  # (
    # RECORDS DELIMITED BY NEWLINE
    # LOAD WHEN file_permissions != 'total'
    # PREPROCESSOR XXWC_AR_AHH_LOCKBOX_IB_DIR: 'list_files.sh'
	# NOBADFILE
    # NODISCARDFILE
    # NOLOGFILE
    # FIELDS TERMINATED BY WHITESPACE
  # )
  # location (XXWC_AR_AHH_LOCKBOX_IB_DIR:'list_files_dummy_source.txt')
# )
# reject limit UNLIMITED;
