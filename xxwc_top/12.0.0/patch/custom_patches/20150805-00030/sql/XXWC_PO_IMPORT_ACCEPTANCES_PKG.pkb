CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PO_IMPORT_ACCEPTANCES_PKG
AS
   /*************************************************************************
     $Header xxwc_po_acceptances_pkg $
     Module Name: xxwc_po_acceptances_pkg.pkb

     PURPOSE:   This package imports inbound EDI 855 PO Acceptances, calls XML Publisher, bursts files, attaches and emails them to Buyers.

     REVISIONS:
     Ver        Date        Author                             Description
     ---------  ----------  -------------------------------    -------------------------
     1.0        03/05/2014  Jason Price (Focused E-commerce)   Initial Version
     1.1        05/25/2014  Jason Price (Focused E-commerce)   Added "Accepted with Changes" Status to PO Acceptance for Backordered items
                                                               Add logic (new step 4) to derive po line number if one wasn't sent.
     1.2        06/13/2014  Jason Price (Focused E-commerce)   Added logic to derive the acceptance po line number if vendor did not send one. TMS # 20140502-00107.
     1.3        04/24/2015  Manjula Chellappan                 TMS# 20150325-00030 - Commented the file removal part as it Errors due to permission Issue
	 1.4        10/09/2015  M Hari Prasad                      TMS # 20150805-00030 - Interface - Fix error with XXWC PO Imported 855 Acceptances Wrapper
   **************************************************************************/



   PROCEDURE Process_PO_Acceptances (errbuf    OUT VARCHAR2,
                                     retcode   OUT VARCHAR2)
   IS
      g_distro_list               VARCHAR2 (75)
                                     DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_req_id                    NUMBER (10);
      l_dat_file                  fnd_lookup_values.attribute1%TYPE;
      l_dat_file_path             fnd_lookup_values.attribute2%TYPE;
      l_ctl_file                  fnd_lookup_values.attribute3%TYPE;
      l_ctl_file_path             fnd_lookup_values.attribute4%TYPE;
      l_log_file_path             fnd_lookup_values.attribute5%TYPE;

      l_loader_req_id             NUMBER (10);
      l_get_loader_req_status     BOOLEAN;
      l_loader_phase              VARCHAR2 (100);
      l_loader_status             VARCHAR2 (100);
      l_loader_del_phase          VARCHAR2 (100);
      l_loader_del_status         VARCHAR2 (100);
      l_loader_message            VARCHAR2 (1000);

      l_report_req_id             NUMBER (10);
      l_get_report_req_status     BOOLEAN;
      l_report_phase              VARCHAR2 (100);
      l_report_status             VARCHAR2 (100);
      l_report_del_phase          VARCHAR2 (100);
      l_report_del_status         VARCHAR2 (100);
      l_report_message            VARCHAR2 (1000);

      l_bursting_req_id           NUMBER (10);
      l_get_bursting_req_status   BOOLEAN;
      l_bur_phase                 VARCHAR2 (100);
      l_bur_status                VARCHAR2 (100);
      l_bur_del_phase             VARCHAR2 (100);
      l_bur_del_status            VARCHAR2 (100);
      l_bur_message               VARCHAR2 (1000);

      l_user_id                   NUMBER (10);
      l_last_update_login         NUMBER (10);
      l_attachment_count          NUMBER (10) := 0;
      l_total_acceptances_count   NUMBER (10) := 0;
      l_sec                       VARCHAR2 (2000);
      l_pos_to_process            NUMBER (10);
      request_exception           EXCEPTION;
      NO_FILES_TO_PROCESS         EXCEPTION;

      CURSOR cursor_acceptances
      IS
         SELECT acc_header_id,
                fu.user_name Buyer_Role_Name,
                po_header_id,
                revision_number,
                confirmation_date,
                poh.revision_num po_revision_number,
                po_number po_number/* Changed 5/25/14 ,nvl((select 'CHANGES REQUESTED' from XXWC.XXWC_PO_ACCEPTANCE_LINES AL WHERE nvl(LINE_ITEM_STATUS_CODE, 'NULL') <> 'IA' and rownum < 2 and AL.acc_header_id = AH.acc_header_id),'Accepted Terms') acceptance_lookup_code */
                ,
                CASE
                   WHEN (SELECT 'EXISTS'
                           FROM XXWC.XXWC_PO_ACCEPTANCE_LINES AL
                          WHERE     NVL (
                                       REPLACE (
                                          REPLACE (LINE_ITEM_STATUS_CODE,
                                                   CHR (10)),
                                          CHR (13)),
                                       'NULL') NOT IN ('IA', 'IB')
                                AND ROWNUM < 2
                                AND AL.acc_header_id = AH.acc_header_id)
                           IS NOT NULL
                   THEN
                      'CHANGES REQUESTED'
                   WHEN (SELECT 'EXISTS'
                           FROM XXWC.XXWC_PO_ACCEPTANCE_LINES AL
                          WHERE     NVL (
                                       REPLACE (
                                          REPLACE (LINE_ITEM_STATUS_CODE,
                                                   CHR (10)),
                                          CHR (13)),
                                       'NULL') IN ('IB')
                                AND ROWNUM < 2
                                AND AL.acc_header_id = AH.acc_header_id)
                           IS NOT NULL
                   THEN
                      'ACCEPT WITH CHANGES'
                   ELSE
                      'Accepted Terms'
                END
                   acceptance_lookup_code,
                NVL (VENDOR_LOCATION_NAME, POV.VENDOR_NAME) VENDOR_NAME,
                   'PO_ACCEPTANCE_FOR_'
                || TO_CHAR (ah.po_number)
                || '_'
                || NVL (ah.revision_number, poh.revision_num)
                || '_'
                || TO_CHAR (ah.acc_header_id)
                || '.rtf'
                   file_name
           FROM XXWC.XXWC_PO_ACCEPTANCE_HEADERS AH,
                PO_HEADERS_ALL POH,
                FND_USER FU,
                PO_VENDORS POV
          WHERE     ah.status = 'READY'
                AND ah.po_number = poh.segment1(+)
                AND poh.vendor_id = POV.vendor_id(+)
                AND poh.agent_id = fu.employee_id(+)
				------Added below For Ver# 1.4
				and (
               INSTR(AH.PO_NUMBER,'"') = 0 and INSTR(AH.PO_NUMBER,'/')=0
               and 
               INSTR(AH.PO_NUMBER,'�') = 0 and INSTR(AH.PO_NUMBER,'_')=0
               and INSTR(AH.PO_NUMBER,'.')=0
                ----Added End For Ver# 1.4
               );

      c_acceptances               cursor_acceptances%ROWTYPE;

      CURSOR cursor_acceptance_lines
      IS
         SELECT ah.po_number, al.acc_detail_id
           FROM XXWC.XXWC_PO_ACCEPTANCE_HEADERS AH,
                XXWC.XXWC_PO_ACCEPTANCE_LINES AL 
          WHERE     ah.status = 'READY'
                AND ah.acc_header_id = al.acc_header_id
                AND al.assigned_id IS NULL
	            ------Added below For Ver# 1.4
			   and (
               INSTR(AH.PO_NUMBER,'"') = 0 and INSTR(AH.PO_NUMBER,'/')=0
               and 
               INSTR(AH.PO_NUMBER,'�') = 0 and INSTR(AH.PO_NUMBER,'_')=0
               and INSTR(AH.PO_NUMBER,'.')=0
                ----Added End For Ver# 1.4
               );

						

      c_acceptance_lines          cursor_acceptance_lines%ROWTYPE;
   BEGIN
      --Overview
      --1. Import files into staging table
      --2. Call Acceptance report
      --3. Call Bursting to write each Acceptance as an rtf file on the file system
      --4 Derive PO line number for each blank line sent
      --5. Attach Acceptances to the corresponding PO
      --6. Call attachment API for each PO
      --7. Call workflow notification
      --8. Delete the temporary bursted files on the file system
      --9. Call workflow notification
      --10. Delete the temporary bursted files on the file system..
      retcode := 0;
      errbuf := NULL;
      l_req_id := fnd_global.conc_request_id;
      l_user_id := fnd_profile.VALUE ('USER_ID');
      l_last_update_login := fnd_profile.VALUE ('LOGIN_ID');
	  
	  	   
      -- Purge the temp table older than 60 days. Allows us to debug values coming in from the flat files.
      DELETE FROM XXWC.XXWC_PO_ACCEPTANCE_LINES AL
            WHERE AL.ACC_HEADER_ID IN (SELECT AH.ACC_HEADER_ID
                                         FROM XXWC.XXWC_PO_ACCEPTANCE_HEADERS AH
                                        WHERE     AH.ACC_HEADER_ID =
                                                     AL.ACC_HEADER_ID
                                              AND AH.CREATION_DATE <
                                                     SYSDATE - 60);

      DELETE FROM XXWC.XXWC_PO_ACCEPTANCE_HEADERS AH
            WHERE AH.CREATION_DATE < SYSDATE - 60;

      COMMIT;


      --1. Looking up Value in Common Lookups for Submitting XXWC Genric Loader Program
      BEGIN
         fnd_file.put_line (FND_FILE.LOG, ' ');
         l_sec :=
            '1. Looking up Value in Common Lookups for Submitting XXWC Genric Loader Program';
         fnd_file.put_line (
            fnd_file.LOG,
               'l_sec - '
            || l_sec
            || ' Date - '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         fnd_file.put_line (FND_FILE.LOG, ' ');

         SELECT attribute1 DAT_FILE,
                attribute2 DAT_FILE_PATH,
                attribute3 CTL_FILE,
                attribute4 CTL_FILE_PATH,
                attribute5 LOG_FILE_PATH
           INTO l_DAT_FILE,
                l_DAT_FILE_PATH,
                l_CTL_FILE,
                l_CTL_FILE_PATH,
                l_LOG_FILE_PATH
           FROM fnd_lookup_values
          WHERE     lookup_type = 'XXWC_LOADER_COMMON_LOOKUP'
                AND LANGUAGE = USERENV ('LANG')
                AND MEANING =
                       (SELECT CONCURRENT_PROGRAM_NAME
                          FROM FND_CONCURRENT_PROGRAMS
                         WHERE CONCURRENT_PROGRAM_ID =
                                  FND_GLOBAL.CONC_PROGRAM_ID)
                AND ENABLED_FLAG = 'Y'
                AND TRUNC (SYSDATE) BETWEEN TRUNC (START_DATE_ACTIVE)
                                        AND TRUNC (
                                               NVL (END_DATE_ACTIVE, SYSDATE));
      EXCEPTION
         WHEN OTHERS
         THEN
            l_sec := l_sec || '   ' || SQLERRM;
            RAISE;                  -- (FATAL Error. Abort the entire process)
      END;

      --2. Submitting XXWC Genric Loader Program
      BEGIN
         l_sec := '2. Submitting XXWC Genric Loader Program';
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         FND_FILE.PUT_LINE (
            fnd_file.LOG,
               'l_sec - '
            || l_sec
            || ' Date - '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         l_loader_req_id :=
            fnd_request.submit_request (application   => 'XXWC',
                                        program       => 'XXWCGLP',
                                        description   => NULL,
                                        start_time    => SYSDATE,
                                        sub_request   => FALSE,
                                        argument1     => l_dat_file,
                                        argument2     => l_dat_file_path,
                                        argument3     => l_ctl_file,
                                        argument4     => l_ctl_file_path,
                                        argument5     => l_log_file_path);
         COMMIT;
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            'Request for ' || l_sec || ' ' || l_loader_req_id);
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         l_get_loader_req_status :=
            FND_CONCURRENT.WAIT_FOR_REQUEST (l_loader_req_id,
                                             5,
                                             0,
                                             l_loader_phase,
                                             l_loader_status,
                                             l_loader_del_phase,
                                             l_loader_del_status,
                                             l_loader_message);

         IF UPPER (l_loader_status) <> 'NORMAL'
         THEN
            RAISE REQUEST_EXCEPTION;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN --  We want any files that were successful to continue to be processed here so no raising exceptions at this point
            FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Error in '
               || l_sec
               || '  '
               || l_loader_message
               || '  '
               || SQLERRM);
            FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
            retcode := 2;
      END;
	      ------Added below For Ver# 1.4
	  --fnd_file.put_line (FND_FILE.LOG, 'before update ');
	  UPDATE XXWC.XXWC_PO_ACCEPTANCE_HEADERS AH
               SET AH.STATUS = 'ERROR'
             WHERE AH.STATUS = 'READY'
			 and (
               INSTR(AH.PO_NUMBER,'"') <>0 or INSTR(AH.PO_NUMBER,'/')<>0
               or 
               INSTR(AH.PO_NUMBER,'�') <> 0 or INSTR(AH.PO_NUMBER,'_')<>0
               or INSTR(AH.PO_NUMBER,'.')<>0
                
               );
           
              commit;
       ----Added End For Ver# 1.4
      -- 3. Check for files to process
      BEGIN
         l_sec := '3. Check for files to process';
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         FND_FILE.PUT_LINE (
            fnd_file.LOG,
               'l_sec - '
            || l_sec
            || ' Date - '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');

         SELECT COUNT (1)
           INTO l_pos_to_process
           FROM XXWC.XXWC_PO_ACCEPTANCE_HEADERS AH
          WHERE ah.status = 'READY';

         IF l_pos_to_process = 0
         THEN
            l_sec := l_sec || '   ' || SQLERRM;
            RAISE NO_FILES_TO_PROCESS;   -- (Exit the entire process Normally)
         END IF;
      END;

      -- Begin Version 1.2 Added By Jason Price on 6/13/2014 for TMS # 20140502-00107
      --4 Derive PO line number for each blank line sent
      -- This finds all acceptance lines without po line numbers and get the lowest line number from the lines on the po that are not already matched to an acceptance line.
      BEGIN
         l_sec :=
            '4 Derive PO Lines for transactions with null po line numbers';
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         fnd_file.put_line (
            fnd_file.LOG,
               'l_sec - '
            || l_sec
            || ' Date - '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         fnd_file.put_line (FND_FILE.LOG, ' ');

         FOR c_acceptance_lines IN cursor_acceptance_lines
         LOOP
            UPDATE XXWC.XXWC_PO_ACCEPTANCE_LINES pal
               SET pal.assigned_id =
                      NVL (
                         (SELECT MIN (pol.line_num)
                            FROM po_lines_all pol,
                                 po_line_locations_all poll,
                                 po_headers_all poh,
                                 mtl_system_items msi
                           WHERE     c_acceptance_lines.po_number =
                                        poh.segment1
                                 AND poh.po_header_id = pol.po_header_id
                                 AND pol.item_id = msi.inventory_item_id
                                 AND pol.po_line_id = poll.po_line_id
                                 AND poll.ship_to_organization_id =
                                        msi.organization_id
                                 AND msi.segment1 = pal.white_cap_item_number
                                 AND NOT EXISTS
                                            (SELECT 'Exists'
                                               FROM XXWC.XXWC_PO_ACCEPTANCE_LINES pal2
                                              WHERE     pol.line_num =
                                                           pal2.assigned_id
                                                    AND pal.acc_header_id =
                                                           pal2.acc_header_id)),
                         (SELECT MIN (pol.line_num)
                            FROM po_lines_all pol, po_headers_all poh
                           WHERE     c_acceptance_lines.po_number =
                                        poh.segment1
                                 AND poh.po_header_id = pol.po_header_id
                                 AND pol.vendor_product_num =
                                        pal.vendor_item_number
                                 AND NOT EXISTS
                                            (SELECT 'Exists'
                                               FROM XXWC.XXWC_PO_ACCEPTANCE_LINES pal2
                                              WHERE     pol.line_num =
                                                           pal2.assigned_id
                                                    AND pal.acc_header_id =
                                                           pal2.acc_header_id)))
             WHERE pal.acc_detail_id = c_acceptance_lines.acc_detail_id;
         END LOOP;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_sec := l_sec || '   ' || SQLERRM;

            UPDATE XXWC.XXWC_PO_ACCEPTANCE_HEADERS AH
               SET STATUS = 'ERROR'
             WHERE STATUS = 'READY';

            COMMIT;
            RAISE;
      END;

      -- End Version 1.2 Added By Jason Price on 6/13/2014 for TMS # 20140502-00107



      --5. Call XXWC PO Imported 855 Acceptances Report
      BEGIN
         l_sec := '5. Call XXWC PO Imported 855 Acceptances Report';
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         fnd_file.put_line (
            fnd_file.LOG,
               'l_sec - '
            || l_sec
            || ' Date - '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         fnd_file.put_line (FND_FILE.LOG, ' ');
         l_report_req_id :=
            fnd_request.submit_request (application   => 'XXWC',
                                        program       => 'XXWC_PO_ACC');
         COMMIT;
         l_get_report_req_status :=
            fnd_concurrent.wait_for_request (l_report_req_id,
                                             5,
                                             0,
                                             l_report_phase,
                                             l_report_status,
                                             l_report_del_phase,
                                             l_report_del_status,
                                             l_report_message);

         IF UPPER (l_report_status) <> 'NORMAL'
         THEN
            l_sec := l_sec || '   ' || l_report_message || '  ' || SQLERRM;
            RAISE REQUEST_EXCEPTION;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_sec := l_sec || '   ' || SQLERRM;

            UPDATE XXWC.XXWC_PO_ACCEPTANCE_HEADERS AH
               SET STATUS = 'ERROR'
             WHERE STATUS = 'READY';

            COMMIT;
            RAISE;
      END;

      --6. Call Bursting XXWC PO Imported 855 Acceptances Report (XML Publisher Report Bursting Program)
      BEGIN
         fnd_file.put_line (FND_FILE.LOG, ' ');
         l_sec :=
            '6. Call Bursting XXWC PO Imported 855 Acceptances Report (XML Publisher Report Bursting Program)';
         fnd_file.put_line (
            fnd_file.LOG,
               'l_sec - '
            || l_sec
            || ' Date - '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         fnd_file.put_line (FND_FILE.LOG, ' ');
         l_bursting_req_id :=
            fnd_request.submit_request (application   => 'XDO',
                                        program       => 'XDOBURSTREP',
                                        argument1     => 'Y',
                                        argument2     => l_report_req_id,
                                        argument3     => 'N');
         COMMIT;
         l_get_bursting_req_status :=
            fnd_concurrent.wait_for_request (l_bursting_req_id,
                                             5,
                                             0,
                                             l_bur_phase,
                                             l_bur_status,
                                             l_bur_del_phase,
                                             l_bur_del_status,
                                             l_bur_message);

         IF UPPER (l_bur_status) <> 'NORMAL'
         THEN
            l_sec := l_sec || '   ' || l_bur_message || '   ' || SQLERRM;
            RAISE REQUEST_EXCEPTION;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_sec := l_sec || ' ' || SQLERRM;

            UPDATE XXWC.XXWC_PO_ACCEPTANCE_HEADERS AH
               SET STATUS = 'ERROR'
             WHERE STATUS = 'READY';

            COMMIT;
            RAISE;
      END;



      -- Open cursor and grab bursted files on the file system
      FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
      fnd_file.put_line (fnd_file.LOG,
                         'Processing PO Acceptances in Staging Table');
      FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');

      FOR c_acceptances IN cursor_acceptances
      LOOP
         DECLARE
            l_po_acc_error_msg         VARCHAR2 (32000);
            --      l_file_name                varchar2(2000);
            l_rowid                    ROWID;
            l_attached_document_id     NUMBER;
            l_document_id              NUMBER;
            l_media_id                 NUMBER;
            l_category_id              NUMBER;
            l_description              fnd_documents_tl.description%TYPE
                                          := 'Acceptance attached to PO';
            l_directory                VARCHAR2 (2000)
                                          := 'XXWC_EDI_PO_PDF_BURST_STAGE';
            l_seq_num                  NUMBER;
            l_blob                     BLOB;
            l_bfile                    BFILE;
            l_byte                     NUMBER;
            l_record_acceptance_id     PO_ACCEPTANCES.ACCEPTANCE_ID%TYPE;
            l_acceptance_lookup_code   PO_ACCEPTANCES.ACCEPTANCE_LOOKUP_CODE%TYPE;
            l_accepted_flag            PO_ACCEPTANCES.ACCEPTED_FLAG%TYPE;
         BEGIN
            --7. Attach Acceptances to the corresponding PO
            BEGIN
               l_sec := '7. Attach Acceptances to the corresponding PO';

               SELECT category_id
                 INTO l_category_id
                 FROM apps.fnd_document_categories_tl
                WHERE user_name LIKE 'Documents' AND language = 'US';

               SELECT FND_DOCUMENTS_S.NEXTVAL INTO l_document_id FROM DUAL;

               SELECT FND_ATTACHED_DOCUMENTS_S.NEXTVAL
                 INTO l_attached_document_id
                 FROM DUAL;

               SELECT NVL (MAX (seq_num), 0) + 10
                 INTO l_seq_num
                 FROM fnd_attached_documents
                WHERE     pk1_value = c_acceptances.po_header_id
                      AND pk2_value = c_acceptances.po_revision_number
                      AND entity_name = 'PO_HEAD';

               --        l_file_name := 'PO_ACCEPTANCE_FOR_'||to_char(c_acceptances.po_number)||'.rtf';
               fnd_documents_pkg.insert_row (
                  X_ROWID               => l_rowid,
                  X_DOCUMENT_ID         => l_document_id,
                  X_CREATION_DATE       => SYSDATE,
                  X_CREATED_BY          => l_user_id,
                  X_LAST_UPDATE_DATE    => SYSDATE,
                  X_LAST_UPDATED_BY     => l_user_id,
                  X_LAST_UPDATE_LOGIN   => l_last_update_login,
                  X_DATATYPE_ID         => 6                           -- FILE
                                            ,
                  X_CATEGORY_ID         => l_category_id,
                  X_SECURITY_TYPE       => 4,
                  X_PUBLISH_FLAG        => 'Y',
                  X_USAGE_TYPE          => 'O',
                  X_LANGUAGE            => 'US',
                  X_DESCRIPTION         => l_description--            , X_FILE_NAME         => l_file_name
                  ,
                  X_FILE_NAME           => c_acceptances.file_name,
                  X_MEDIA_ID            => l_media_id);

               fnd_documents_pkg.insert_tl_row (
                  X_DOCUMENT_ID         => l_document_id,
                  X_CREATION_DATE       => SYSDATE,
                  X_CREATED_BY          => l_user_id,
                  X_LAST_UPDATE_DATE    => SYSDATE,
                  X_LAST_UPDATED_BY     => l_user_id,
                  X_LAST_UPDATE_LOGIN   => l_last_update_login,
                  X_LANGUAGE            => 'US',
                  X_DESCRIPTION         => l_description);

               INSERT INTO fnd_lobs (file_id,
                                     file_name,
                                     file_content_type,
                                     upload_date,
                                     expiration_date,
                                     program_name,
                                     program_tag,
                                     file_data,
                                     language,
                                     oracle_charset,
                                     file_format)
                    VALUES (l_media_id,
                            c_acceptances.file_name            /*l_file_name*/
                                                   ,
                            'text/plain',
                            SYSDATE,
                            NULL,
                            'FNDATTCH',
                            NULL,
                            EMPTY_BLOB (),
                            'US',
                            'UTF8',
                            'binary')
                 RETURNING file_data
                      INTO l_blob;

               DBMS_LOB.CREATETEMPORARY (l_blob, TRUE);
               l_bfile := BFILENAME (l_directory,             /* l_file_name*/
                                                 c_acceptances.file_name);

               fnd_file.put_line (FND_FILE.LOG, 'Directory ' || l_directory);
               fnd_file.put_line (FND_FILE.LOG,
                                  'FILE_NAME ' || c_acceptances.file_name);

               l_byte := DBMS_LOB.getlength (l_bfile);
               DBMS_LOB.fileopen (l_bfile);
               DBMS_LOB.loadfromfile (l_blob, l_bfile, l_byte);
               DBMS_LOB.fileclose (l_bfile);

               UPDATE fnd_lobs
                  SET file_data = l_blob
                WHERE file_id = l_media_id;

               fnd_attached_documents_pkg.insert_row (
                  X_ROWID                      => l_rowid,
                  X_ATTACHED_DOCUMENT_ID       => l_attached_document_id,
                  X_DOCUMENT_ID                => l_document_id,
                  X_CREATION_DATE              => SYSDATE,
                  X_CREATED_BY                 => l_user_id,
                  X_LAST_UPDATE_DATE           => SYSDATE,
                  X_LAST_UPDATED_BY            => l_user_id,
                  X_LAST_UPDATE_LOGIN          => l_last_update_login,
                  X_SEQ_NUM                    => l_seq_num,
                  X_ENTITY_NAME                => 'PO_HEAD',
                  X_COLUMN1                    => NULL,
                  X_PK1_VALUE                  => c_acceptances.po_header_id,
                  X_PK2_VALUE                  => c_acceptances.po_revision_number,
                  X_PK3_VALUE                  => NULL,
                  X_PK4_VALUE                  => NULL,
                  X_PK5_VALUE                  => NULL,
                  X_AUTOMATICALLY_ADDED_FLAG   => 'N',
                  X_DATATYPE_ID                => 6,
                  X_CATEGORY_ID                => l_category_id,
                  X_SECURITY_TYPE              => 4,
                  X_PUBLISH_FLAG               => 'Y',
                  X_LANGUAGE                   => 'US',
                  X_DESCRIPTION                => l_description,
                  X_FILE_NAME                  =>             /*l_file_name */
                                                  c_acceptances.file_name,
                  X_MEDIA_ID                   => l_media_id);
            -- l_attachment_count := l_attachment_count + 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_sec := l_sec || '   ' || SQLERRM;
                  RAISE;
            END;

            -- 8. Call attachment API for each PO
            BEGIN
               l_sec :=
                  '8. Call po_change_api1_s.record_acceptance API for each PO';

               IF c_acceptances.acceptance_lookup_code IN ('Accepted Terms',
                                                           'ACCEPT WITH CHANGES')
               THEN
                  l_accepted_flag := 'Y';
               ELSE
                  l_accepted_flag := 'N';
               END IF;

               l_record_acceptance_id :=
                  po_change_api1_s.record_acceptance (
                     X_PO_NUMBER                => c_acceptances.po_number,
                     X_RELEASE_NUMBER           => NULL,
                     X_REVISION_NUMBER          => c_acceptances.po_revision_number,
                     X_ACTION                   => 'Acknowledged by Supplier',
                     X_ACTION_DATE              => c_acceptances.confirmation_date,
                     X_EMPLOYEE_ID              => NULL,
                     X_ACCEPTED_FLAG            => l_accepted_flag,
                     X_ACCEPTANCE_LOOKUP_CODE   => c_acceptances.acceptance_lookup_code,
                     X_NOTE                     => NULL,
                     X_INTERFACE_TYPE           => NULL,
                     X_TRANSACTION_ID           => NULL,
                     VERSION                    => '1.0',
                     p_org_id                   => NULL);

               IF l_record_acceptance_id < 1
               THEN
                  l_sec := l_sec;
                  RAISE REQUEST_EXCEPTION;
               END IF;
            END;

            --9. Call workflow notification
            BEGIN
               l_sec := '9. Call workflow notification';
               wf_engine.createprocess (
                  itemtype   => 'XXWCPOAC',
                  itemkey    => l_media_id,
                  process    => 'XXWC_PO_ACCEPTANCE_PROCESS');
               wf_engine.setitemattrtext (
                  itemtype   => 'XXWCPOAC',
                  itemkey    => l_media_id,
                  aname      => 'XXWC_PO_BUYER_ROLE_NAME',
                  avalue     => c_acceptances.buyer_role_name);
               wf_engine.setitemattrtext (
                  itemtype   => 'XXWCPOAC',
                  itemkey    => l_media_id,
                  aname      => 'XXWC_PO_PO_NUMBER',
                  avalue     => c_acceptances.po_number);
               wf_engine.setitemattrtext (
                  itemtype   => 'XXWCPOAC',
                  itemkey    => l_media_id,
                  aname      => 'XXWC_PO_VENDOR_NAME',
                  avalue     => c_acceptances.vendor_name);
               wf_engine.setitemattrdocument (
                  itemtype     => 'XXWCPOAC',
                  itemkey      => l_media_id,
                  aname        => 'XXWC_PO_ACCEPTANCE_ATTACHMENT',
                  documentid   =>    'PLSQLBLOB:xxwc_po_import_acceptances_pkg.wwxc_notif_attach_procedure/'
                                  || TO_CHAR (l_media_id));
               wf_engine.startprocess (itemtype   => 'XXWCPOAC',
                                       itemkey    => l_media_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_sec := l_sec || '   ' || SQLERRM;
                  RAISE;
            END;

    -- Commented for Revision 1.3 Begin <<
    
            --10. Delete the temporary bursted files on the file system.
--            BEGIN -- Just want to make a note of this issue and not raise a job errror.
--               l_sec :=
--                  '10. Delete the temporary bursted files on the file system';
--               UTL_FILE.FREMOVE ('XXWC_EDI_PO_PDF_BURST_STAGE', /*l_file_name*/
--                                 c_acceptances.file_name);
--            EXCEPTION
--               WHEN OTHERS
--               THEN
--                  FND_FILE.PUT_LINE (
--                     FND_FILE.LOG,
--                        'Warning in '
--                     || l_sec
--                     || ' for PO number: '
--                     || TO_CHAR (c_acceptances.po_number)
--                     || '     '
--                     || SQLERRM);
--            END;

    -- Revision 1.3 End >>
            
            -- update Acceptance interface staging table status
            UPDATE XXWC.XXWC_PO_ACCEPTANCE_HEADERS
               SET STATUS = 'PROCESSED'
             WHERE ACC_HEADER_ID = c_acceptances.acc_header_id;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 2;
               FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     'ERROR Processing PO '
                  || TO_CHAR (c_acceptances.po_number)
                  || '   '
                  || l_sec);
               FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
               ROLLBACK;

               UPDATE XXWC.XXWC_PO_ACCEPTANCE_HEADERS
                  SET STATUS = 'ERROR'
                WHERE ACC_HEADER_ID = c_acceptances.acc_header_id;

               COMMIT;
         /*            xxcus_error_pkg.xxcus_error_main_api (
                       p_called_from         => 'XXWC_PO_IMPORT_ACCEPTANCES_PKG.Process_PO_Acceptances'
                      ,p_calling             => l_sec
                      ,p_request_id          => l_req_id
                      ,p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000)
                      ,p_error_desc          => 'OTHERS Exception'
                      ,p_distribution_list   => g_distro_list
                      ,p_module              => 'XXCUS');
         */
         END;
      END LOOP;
   EXCEPTION
      WHEN NO_FILES_TO_PROCESS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         FND_FILE.PUT_LINE (
            fnd_file.LOG,
            'Files were either not found or were unable to be inserted into the custom Oracle Staging tables.');
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         errbuf :=
            'Files were either not found or were unable to be inserted into the custom Oracle Staging tables.';
         retcode := 0;
		 ---Added begin for ver # 1.4
   WHEN request_exception
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         FND_FILE.PUT_LINE (
            fnd_file.LOG,
            'Files were failed due to request_exception'||l_sec||' '||SQLERRM);
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         errbuf :=
            'Files were failed due to request_exception'||l_sec||' '||SQLERRM;
         retcode := 0;
		 ---Added end for ver # 1.4
      WHEN OTHERS
      THEN
         ROLLBACK;
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         FND_FILE.PUT_LINE (fnd_file.LOG, 'ERROR in ' || l_sec);
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
         errbuf := 'ERROR in ' || l_sec;
         retcode := 2;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_IMPORT_ACCEPTANCES_PKG.Process_PO_Acceptances',
            p_calling             => l_sec,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_sec, 1, 2000),
            p_error_desc          => 'OTHERS Exception',
            p_distribution_list   => g_distro_list,
            p_module              => 'XXCUS');
         COMMIT;
   END Process_PO_Acceptances;



   PROCEDURE wwxc_notif_attach_procedure (p_document_id     IN     VARCHAR2,
                                          p_display_type    IN     VARCHAR2,
                                          p_document        IN OUT BLOB,
                                          p_document_type   IN OUT VARCHAR2)
   IS
      l_lob_id         NUMBER;
      l_bdoc           BLOB;
      l_content_type   VARCHAR2 (100);
      l_filename       VARCHAR2 (300);
   BEGIN
      l_lob_id := TO_NUMBER (p_document_id);

      -- Obtain the BLOB version of the document
      SELECT file_name, file_content_type, file_data
        INTO l_filename, l_content_type, l_bdoc
        FROM fnd_lobs
       WHERE file_id = l_lob_id;

      p_document_type := l_content_type || ';name=' || l_filename;
      DBMS_LOB.COPY (p_document, l_bdoc, DBMS_LOB.getlength (l_bdoc));
   END wwxc_notif_attach_procedure;
END XXWC_PO_IMPORT_ACCEPTANCES_PKG;
/