CREATE OR REPLACE PACKAGE APPS.XXWC_CUST_BAL_MV_PKG 
AS 
/*******************************************************************************
  * Procedure:   xxwc_cust_bal_mv_pkg
  * Description: Customer MV refresh data
                 
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     12/04/2017    Pattabhi Avula  TMS#20171204-00063 Initial Version
  
********************************************************************************/
PROCEDURE main(p_errbuff OUT VARCHAR2, p_retcode OUT VARCHAR2,p_tunning IN NUMBER);
END;
/