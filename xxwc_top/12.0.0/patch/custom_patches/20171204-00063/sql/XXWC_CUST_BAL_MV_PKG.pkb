CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CUST_BAL_MV_PKG
AS
/*******************************************************************************
  * Procedure:   xxwc_cust_bal_mv_pkg
  * Description: Customer MV refresh data
                 
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     12/04/2017    Pattabhi Avula  TMS#20171204-00063 Initial Version
  
********************************************************************************/

   -- global error handling variables
   g_err_callfrom          VARCHAR2 (75) := 'XXWC_CUST_BAL_MV_PKG.main';
   g_err_callpoint         VARCHAR2 (75) := 'START';
   g_distro_list           VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_message               VARCHAR2 (2000);
   
PROCEDURE main(p_errbuff OUT VARCHAR2, p_retcode OUT VARCHAR2,p_tunning IN NUMBER)
IS

l_errbuff        VARCHAR2(2000);
l_retcode        VARCHAR2(100);

BEGIN
fnd_file.put_line(fnd_file.LOG,'Before executing the script');

 xxeis.xxwc_eis_customer_balance_v2.main(l_errbuff,l_retcode,p_tunning);
 
 fnd_file.put_line(fnd_file.LOG,'l_errbuff - '||l_errbuff);
 fnd_file.put_line(fnd_file.LOG,'l_retcode - '||l_retcode);
 fnd_file.put_line(fnd_file.LOG,'p_tunning - '||p_tunning);
 
EXCEPTION
WHEN OTHERS THEN
g_message:=SQLERRM;

fnd_file.put_line(fnd_file.LOG,'Error is :'||g_message);
END;
END;
/