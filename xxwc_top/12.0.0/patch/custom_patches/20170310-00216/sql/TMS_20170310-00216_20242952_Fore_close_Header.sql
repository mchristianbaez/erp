/*************************************************************************
  $Header TMS_20170310-00216_20242952_Fore_close_Header.sql $
  Module Name: TMS_20170310-00216


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0       10-APR-2017  Pattabhi Avula        TMS#20170310-00216  
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE

BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170310-00216    , Before Update');

UPDATE apps.oe_order_headers_all
   SET flow_status_code='CLOSED',
       open_flag='N'
 WHERE header_id =41801996;

   DBMS_OUTPUT.put_line (
         'TMS: 20170310-00216  Updated record count: '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170310-00216     , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170310-00216 , Errors : ' || SQLERRM);
END;
/