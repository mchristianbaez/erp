/*
 TMS: 20160505-00064 
LM Scofield consigned setting on the shipment line does not match the consigned attribute on the ASL 
 */
SET SERVEROUTPUT ON SIZE 1000000;

DECLARE
   CURSOR C_ASL_ATTR
   IS
      SELECT paa.*
        FROM PO_ASL_ATTRIBUTES paa, po_approved_supplier_list pasl
       WHERE     paa.CONSIGNED_FROM_SUPPLIER_FLAG = 'Y'
             AND paa.asl_id = pasl.asl_id
             AND paa.item_id = pasl.item_id
             AND paa.vendor_id = pasl.vendor_id
             AND paa.vendor_site_id = pasl.vendor_site_id
             AND paa.USING_ORGANIZATION_ID = pasl.using_organization_id
             AND NVL (pasl.DISABLE_FLAG, 'N') = 'Y'
             --AND pasl.last_updated_by = 16991
             AND paa.VENDOR_ID = (SELECT vendor_id
                                    FROM po_vendors
                                   WHERE VENDOR_NAME = 'LM SCOFIELD'); --LM SCOFIELD

   l_count   NUMBER := 0;
BEGIN
   DBMS_OUTPUT.put_line ('Before update');

   FOR i IN C_ASL_ATTR
   LOOP
      UPDATE PO_ASL_ATTRIBUTES PAA
         SET CONSIGNED_FROM_SUPPLIER_FLAG = NULL,
             last_update_date = SYSDATE,
             last_updated_by = 16991
       WHERE     asl_id = i.asl_id
             AND vendor_id = i.vendor_id
             AND vendor_site_id = i.vendor_site_id
             AND item_id = i.item_id
             AND USING_ORGANIZATION_ID = i.USING_ORGANIZATION_ID;

      l_count := l_count + 1;
   END LOOP;

   COMMIT;
   DBMS_OUTPUT.put_line ('Records updated-' || l_count);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update PO_ASL_ATTRIBUTES Table and Error is: ' || SQLERRM);
END;
/