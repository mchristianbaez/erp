CREATE OR REPLACE VIEW apps.XXWC_SR_SALESREPS_VW
(
   SALESREP_ID,
   RESOURCE_ID,
   EEID,
   NTID,
   PRISM_SR_NUMBER,
   SR_NAME,
   SR_TYPE,
   AUTO_UPDATE,
   SR_TITLE,
   HOME_BR,
   HOME_MARKET_CODE,
   HOME_MARKET,
   HOME_DISTRICT_CODE,
   HOME_DISTRICT,
   HOME_REGION_CODE,
   HOME_REGION,
   START_DATE,
   END_DATE,
   LAST_UPDATE_DATE,
   LAST_UPDATE_USER
)
AS
/***************************************************************************
    $Header APPS.XXWC_SR_SALESREPS_VW $
    Module Name: XXWC_SR_SALESREPS_VW
    PURPOSE: View used for Sales Rep listing for apps which need it.
 
    REVISIONS:
    Ver    Date       	Author                Description
    ------ ---------  	------------------    ----------------
     2.0    25-FEB-2016    Andre Rivas        TMS# 20160225-00106  Fix to ensure EEID is always correct. 
												(Removed undeployed MO change. See previous commit.)
                                                
/***************************************************************************/
   SELECT a.salesrep_id,
          a.resource_id,
          COALESCE(people.EMPLOYEE_NUMBER, b.source_number) AS EEID,
          b.user_name,
          c.prism_sr_number,
          b.resource_name,
          b.category,
          c.auto_update,
          c.SR_TITLE,
          c.HOME_BR,
          c.HOME_MARKET_CODE,
          c.HOME_MARKET,
          c.HOME_DISTRICT_CODE,
          c.HOME_DISTRICT,
          c.HOME_REGION_CODE,
          c.HOME_REGION,
          c.START_DATE,
          c.END_DATE,
          c.LAST_UPDATE_DATE,
          c.LAST_UPDATE_USER
     FROM apps.JTF_RS_SALESREPS a
	  left join apps.per_all_people_f people on
	      people.PERSON_ID = a.PERSON_ID AND current_date >= people.EFFECTIVE_START_DATE AND current_date <= people.EFFECTIVE_END_DATE, 
	  apps.JTF_RS_DEFRESOURCES_V b, xxwc.xxwc_sr_salesreps c
    WHERE a.salesrep_id = c.salesrep_id AND a.resource_id = b.resource_id
/

grant select on apps.XXWC_SR_SALESREPS_VW to xxwc
/
grant select on apps.XXWC_SR_SALESREPS_VW to interface_dstage 
/
grant select on apps.XXWC_SR_SALESREPS_VW to INTERFACE_APEXWC 
/
