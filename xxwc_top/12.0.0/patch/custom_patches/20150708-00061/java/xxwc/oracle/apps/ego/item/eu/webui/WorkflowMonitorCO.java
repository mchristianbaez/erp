/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 |  09-Jul-2015  -   Include back button to Item create/Maintenance search   |
 |  31-Jul-2015  -   TMS#20150708-00061 Retain search result between pages   |
 +===========================================================================*/
package xxwc.oracle.apps.ego.item.eu.webui;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

import xxwc.oracle.apps.ego.item.eu.server.WorkflowMonitorVOImpl;
import xxwc.oracle.apps.ego.item.eu.server.XXWCEditItemUpdateAMImpl;
import xxwc.oracle.apps.ego.item.eu.util.ItemUtil;

/**
 * Controller for ...
 */
public class WorkflowMonitorCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
    XXWCEditItemUpdateAMImpl am = (XXWCEditItemUpdateAMImpl)pageContext.getApplicationModule(webBean);
    if(pageContext.getParameter("GoButton") != null)
    {
     String ChangeOrderTypeQ = pageContext.getParameter("ChangeOrderTypeQ"), 
            HazardousMaterialFlagQ = pageContext.getParameter("HazardousMaterialFlagQ"),
            ApprovalGroupQ = pageContext.getParameter("ApprovalGroupQ"),
            StatusQ = pageContext.getParameter("StatusQ"),
            VendorNumberFormValueItem = pageContext.getParameter("VendorNumberFormValueItem"),
            ItemCatFormValue = pageContext.getParameter("ItemCatFormValue"),
            ChangeOrderQ = pageContext.getParameter("ChangeOrderQ"),
            WcItemNumberQ = pageContext.getParameter("WcItemNumberQ"),
            RequestorNameQ = pageContext.getParameter("RequestorNameQ"),
            CreationDateFrom = pageContext.getParameter("CreationDateFrom"),
            CreationDateTo = pageContext.getParameter("CreationDateTo");
     StringBuffer buildQuery = new StringBuffer("");
     boolean isAndRequired = false;
     if(!ItemUtil.isNull(ChangeOrderTypeQ))
     {
      buildQuery = buildQuery.append("Change_Order_Type like '"+ChangeOrderTypeQ+"'");
      if(!isAndRequired) isAndRequired = true;
     }
     if(!ItemUtil.isNull(HazardousMaterialFlagQ))
     {
      if(isAndRequired)
       buildQuery = buildQuery.append(" and ");
      buildQuery = buildQuery.append("Hazardous_Material_Flag = '"+HazardousMaterialFlagQ+"'");
      if(!isAndRequired) isAndRequired = true;
     }
        if(!ItemUtil.isNull(ApprovalGroupQ))
        {
         if(isAndRequired)
          buildQuery = buildQuery.append(" and ");
         buildQuery = buildQuery.append("Approval_Group = '"+ApprovalGroupQ+"'");
         if(!isAndRequired) isAndRequired = true;
        }
        if(!ItemUtil.isNull(StatusQ))
        {
         if(isAndRequired)
          buildQuery = buildQuery.append(" and ");
         buildQuery = buildQuery.append("Status_Name = '"+StatusQ+"'");
         if(!isAndRequired) isAndRequired = true;
        }
        if(!ItemUtil.isNull(VendorNumberFormValueItem))
        {
         if(isAndRequired)
          buildQuery = buildQuery.append(" and ");
         buildQuery = buildQuery.append("Xxwc_Vendor_Number_Attr = '"+VendorNumberFormValueItem+"'");
         if(!isAndRequired) isAndRequired = true;
        }
        if(!ItemUtil.isNull(ItemCatFormValue))
        {
         if(isAndRequired)
          buildQuery = buildQuery.append(" and ");
         buildQuery = buildQuery.append("Item_Cat = '"+ItemCatFormValue+"'");
         if(!isAndRequired) isAndRequired = true;
        }
        if(!ItemUtil.isNull(ChangeOrderQ))
        {
         if(isAndRequired)
          buildQuery = buildQuery.append(" and ");
         buildQuery = buildQuery.append("Change_Notice like '"+ChangeOrderQ+"'");
         if(!isAndRequired) isAndRequired = true;
        }
        if(!ItemUtil.isNull(WcItemNumberQ))
        {
         if(isAndRequired)
          buildQuery = buildQuery.append(" and ");
         buildQuery = buildQuery.append("Wc_Item_Number like '"+WcItemNumberQ+"'");
         if(!isAndRequired) isAndRequired = true;
        }
        if(!ItemUtil.isNull(RequestorNameQ))
        {
         if(isAndRequired)
          buildQuery = buildQuery.append(" and ");
         buildQuery = buildQuery.append("Requestor like '"+RequestorNameQ+"'");
         if(!isAndRequired) isAndRequired = true;
        }
        if(!ItemUtil.isNull(CreationDateFrom))
        {
         if(isAndRequired)
          buildQuery = buildQuery.append(" and ");
         buildQuery = buildQuery.append("Creation_Date >= '"+CreationDateFrom+"'");
         if(!isAndRequired) isAndRequired = true;
        }
        if(!ItemUtil.isNull(CreationDateTo))
        {
         if(isAndRequired)
          buildQuery = buildQuery.append(" and ");
         buildQuery = buildQuery.append("Creation_Date <= '"+CreationDateTo+"'");
         if(!isAndRequired) isAndRequired = true;
        }
        WorkflowMonitorVOImpl wfvo = am.getWorkflowMonitorVO1();
        if(buildQuery != null && !"".equals(buildQuery.toString().trim()))
         wfvo.setWhereClause(buildQuery.toString());
        else
         wfvo.setWhereClause(null);
        wfvo.executeQuery();
    }
    else if(pageContext.getParameter("BackButton") != null)
    {
     String sourceMenu = (String)pageContext.getSessionValue("sourceMenu");
     //TMS#20150708-00061 Retain search result between pages
     pageContext.setForwardURL(((!ItemUtil.isNull(sourceMenu) && "XXWC_ADD".equals(sourceMenu))?"OA.jsp?OAFunc=XXWC_ITEM_CREATE&retainAM=N":"OA.jsp?page=/xxwc/oracle/apps/ego/item/eu/webui/XXWCItemSearchPG&retainAM=N"), 
                                            null, 
                                            OAWebBeanConstants.KEEP_MENU_CONTEXT, 
                                            null, null, false, 
                                            OAWebBeanConstants.ADD_BREAD_CRUMB_NO, 
                                            OAWebBeanConstants.IGNORE_MESSAGES);
    }
  }

}
