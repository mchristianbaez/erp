package xxwc.oracle.apps.ego.item.eu.util;

import oracle.pdq.api1.api.client.WfgClient;
import oracle.pdq.api1.api.client.WfgResultLine;
import oracle.pdq.api1.api.util.Priorities;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.OARow;
import oracle.apps.fnd.framework.OAViewObject;

/*
 * 09-Jul-2015  -   Change EDQP mappings for Item Level, two image attributes.
 */
public class EdqpItemValidation {
    public EdqpItemValidation() {
    }

    WfgClient m_wfgClient;
    String DSAName;
    static String m_envName = "DEV";

    public static void main(String[] args) {
        /* Input fields order
                        Item Level
                        Part Number
                        Description
                        Long Description
                        UOM
                        Shelf Life Days
                        PO Cost
                        Suggested Retail Price
                        Vendor
                        Conflict Minerals
                        Item Type
                        Item Status
                        HazMat Item
                        Weight
                        Weight UoM
                        CatClass
                        Retail UOM
                        Country of Origin
                        UPC1
                        UPC2
                        UPC3
                        Vendor1
                        Vendor2
                        Vendor3
                        TaxWare Code
                        Proposed ICC
                        Web_Description
                        Web_Long_Description
                        Features
                        Specifications
                        Web_Keywords
                        Brand
                        Full Image Name
                        Thumbnail Image Name

        Additional Output Fields
                        Bulky Item

        */
        long starttime, endtime;
        String inputData[][] = //Item Level
        //Part Number
        //Description
        //Long Description
        //UOM
        //Shelf Life Days
        //PO Cost
        //Suggested Retail Price
        //Vendor
        //Conflict Minerals
        //Item Type
        //Item Status
        //HazMat Item
        //Weight
        //Weight UoM
        //CatClass
        //Retail UOM
        //Country of Origin
        //UPC1
        //UPC2
        //UPC3
        //Vendor1
        //Vendor2
        //Vendor3
        //TaxWare Code
        //Proposed ICC
        //Web_Description
        //Web_Long_Description
        //Features
        //Specifications
        //Web_Keywords
        //Brand 
        //Full Image Name
        //Thumbnail Image Name


        { { "L1", "12343ad", "TEST", "Long |~ test", "", "", "1.23433", 
            "2.43234", "DEWALT", "No", "NON-STOCK", "Pending", "", "321", "", 
            "WOOD to WOOD", "LBS", "", "123456789012", "", "", "", "", "", "", 
            "", "TEST ** TEST", "Long *~", "", "", "", "BRAND", "", "" } };

        String output[] = validateItem(m_envName, inputData);
        for (int i = 0; i < output.length; ++i) {
            System.out.println(output[i]);
        }

    }


    public static String[] validateItem(OAApplicationModule am, String envName, 
                                            String[][] inputData) {
            ArrayList<String> outputData = new ArrayList<String>();
            long starttime;

            EdqpItemValidation wrap1 = 
                new EdqpItemValidation(envName, "XXWC_Item_SingleItem_DSA");
//            System.out.println(" Getting results ");

            //for (int i=0;i<=10;i++)
            for (int i = 0; i < inputData.length; i++) {
                starttime = System.currentTimeMillis();
                List TestData = 
                    wrap1.validateItemWithEDQP(inputData, "00_Results");
                Iterator TestIterator = TestData.iterator();

                while (TestIterator.hasNext()) {
                    String outField = (String)TestIterator.next();
                    outputData.add(outField);
                    //System.out.print(outField + " , " );
                }
//                System.out.println(" done " + 
//                                   (System.currentTimeMillis() - starttime));
                try {
                    //Thread.sleep(10000);
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            //This block to be used only if exceptions are expected out of the DSA
            /*
                System.out.println(" Getting Exceptions.. " + System.currentTimeMillis() );

                List TestData1 = wrap1.validateItemWithEDQP(m_inputData, "20_Exception_Output");
                 Iterator TestIterator1 = TestData1.iterator();
                                        while (TestIterator1.hasNext())
                                        {
                                                String outField1 = (String)TestIterator1.next();
                                                System.out.print(outField1 + " , " );
                                        }
                                        System.out.println(" done " + System.currentTimeMillis() );
                */

            //Start Addeed to apply edqp results to vo
            Object attrValue;
            //TMS no#20150526-00042
            String itemAttrs[][] =
            /*{"","Segment1","Description","LongDescription","ShelfLifeDays","ListPricePerUnit","","","",
                   "ItemType","InventoryItemStatusCode", "HazardousMaterialFlag","UnitWeight","","GlobalAttribute1"
                 };*/

             {{ "CExtAttr6"},{ "Segment1", "Description", "LongDescription", "PrimaryUomCode", 
          "ShelfLifeDays", "ListPricePerUnit", "", "", "" ,"ItemType","","HazardousMaterialFlag","UnitWeight","","",""},
          {"CExtAttr2"},
          {"Attribute22","ItemCatalogGroupId"},
        {"CExtAttr14","TlExtAttr2","","","","CExtAttr1","CExtAttr15","CExtAttr20"},
        {"GlobalAttribute6"},
          {"CrossReference"},
        {"CrossReference"},
        {"CExtAttr3"},
        {"GlobalAttribute5"}
             };
            String voList[] = { "XxwcChgEgoItemAttrisVO1","XxwcMtlSyItemsChgBVO1","XxwcChgEgoItemAttrisVO1","XxwcMtlSyItemsChgBVO1","XxwcChgEgoItemAttrsLevel3VO1","XxwcMtlSyItemsChgBVO1","XxwcChgMtlCrossRefUPCVO1","XxwcChgMtlCrossRefVendorVO1","XxwcChgEgoItemAttrsLevel3VO1","XxwcMtlSyItemsChgBVO1" };
            int voRowCount[] = {1,1,1,1,1,1,10,10,1,1};
            String edqpresult[] = 
                outputData.toArray(new String[outputData.size()]);

            for (int i = 0, k = 0; i < voList.length; i++) {
                OAViewObject vo = (OAViewObject)am.findViewObject(voList[i]);
                vo.reset();
//                for(OARow row = (OARow)vo.next(); row != null; row = (OARow)vo.next())
                for(int l = 0;l< voRowCount[i];l++)
                {
                OARow row = (OARow)vo.getRowAtRangeIndex(l);
                
                    for (int j = 0; j < itemAttrs[i].length; j++,k++) {
//                    System.out.println("---GsrC--- itemAttrs[i][j] : "+itemAttrs[i][j]+" edqpresult[k + 1] : "+edqpresult[k + 1]+" edqpresult[k] : "+edqpresult[k]+" row : "+row);
                        if (!"".equals(itemAttrs[i][j]) && 
                            edqpresult[k + 1] != null && row != null) {
                            if((voList[i].equals("XxwcChgMtlCrossRefUPCVO1") || voList[i].equals("XxwcChgMtlCrossRefVendorVO1")))
                            {
                             if("Y".equals(row.getAttribute("IsRequiredValidation")))
                             {
//                              if(voList[i].equals("XxwcChgMtlCrossRefUPCVO1"))
//                               edqpresult[k + 1] = edqpresult[k + 1].substring(((String)row.getAttribute("CrossReferenceType")).length()+1);
                              row.setAttribute(itemAttrs[i][j], edqpresult[k + 1]);
                            //inputData[0][k]=(attrValue==null)?"":attrValue.toString();
                             }
                             else continue;
                            }
                            else
                            {
                             row.setAttribute(itemAttrs[i][j], edqpresult[k + 1]);
                            }
                        }
//                        k++;
                    }

                }

            }
        //End added to apply edqp results to vo               

        return outputData.toArray(new String[outputData.size()]);
    }


    public EdqpItemValidation(String Env, String CallDSAName) {
//        if (Env == "DEV")
//            m_wfgClient = 
//                    new WfgClient("gutpdq01lds.hdsupply.net", 2229, false, "", 
//                                  "Single Item Validation");
//        if (Env == "QA")
//            m_wfgClient = 
//                    new WfgClient("gutpdq01lqs.hdsupply.net", 2229, false, "", 
//                                  "Single Item Validation");
//        if (Env == "PROD")
//            m_wfgClient = 
//                    new WfgClient("gutpdq01lps.hdsupply.net", 2229, false, "", 
//                                  "Single Item Validation");
        m_wfgClient = 
                new WfgClient(Env, 2229, false, "", 
                              "Single Item Validation");

        this.DSAName = CallDSAName;
    }

    public List validateItemWithEDQP(String[][] m_inputData, String step) {
        int m_jobID = 0;

        List ResultData = new ArrayList();

        // Setup this list of String Fields for the request
        List<List<String>> list = new ArrayList<List<String>>();
        for (int i = 0; i < m_inputData.length; i++) {
            List<String> fields = 
                new ArrayList<String>(); // Create a List of Strings
            for (int j = 0; j < m_inputData[i].length; j++) {
                fields.add(new String(m_inputData[i][j]));
            }
            list.add(fields);
        }

        // Start the DSA job with our data
        // NOTE: Input data with a List containing a list of string attributes.
        m_wfgClient.setLinesFromFields(list);

        try {
            m_wfgClient.setPriority(Priorities.PRIORITY_ULTRAHIGH);
            m_jobID = m_wfgClient.runJob(DSAName, "SingleItemWrapper");
//            System.out.println(m_jobID);
        } catch (Exception f) {
//            System.out.println("Error retriving job ID" + f);
            //Error in EDQP process.
            f.printStackTrace();
            throw new OAException("A Change Request cannot be generated at this time. Please contact the Help Desk for Support. Thank You");
        }


        // Get the DSA Results!
        boolean waitForResults = true;
        try {
            ResultData = 
                    m_wfgClient.getResultData(m_jobID, step, waitForResults);
        } catch (Exception f) {
            // Check if the job has not completed yet
//            System.out.println("Job not completed yet" + f);
            f.printStackTrace();
            throw new OAException("A Change Request cannot be generated at this time. Please contact the Help Desk for Support. Thank You");
        }
        Iterator ResultIter = ResultData.iterator();
        List ResultFields = new ArrayList();
        if (ResultIter.hasNext()) {
            WfgResultLine resultLine = (WfgResultLine)ResultIter.next();
            ResultFields = resultLine.getDataFields();
        } 
//        else
//            System.out.println("NO RESULTS RETURNED");


//        System.out.println("Job completed");
        return ResultFields;
    }

    public static String[] validateItem(OAApplicationModule am) {
        String envName = am.getOADBTransaction().getProfile("XXWC_EDQP_ENVIRONMENT_NAME");
//        System.out.println("call validateItem with AM ");
        String inputData[][] = //0. Item Level
        //1. Part Number
        //2. Description
        //3. Long Description
        //4. UOM
        //5.Shelf Life Days
        //6. PO Cost
        //7. Suggested Retail Price
        //8. Vendor
        //9. Conflict Minerals
        //10. Item Type
        //11. Item Status
        //12. HazMat Item
        //13. Weight
        //14. Weight UoM
        //15. CatClass
        //16. Retail UOM
        //17. Country of Origin
        //18. TaxWare Code
        //19. Proposed ICC
        //20. Web_Description
        //21. Web_Long_Description
        //22. Features
        //23. Specifications
        //24. Web_Keywords
        //25. Brand 
        //26. Full Image Name
        //27. Thumbnail Image Name
        //28. CoreStatus_Category
        //29. UPC1
        //30. UPC2
        //31. UPC3
        //32. UPC4
        //33. UPC5
        //34. UPC6
        //35. UPC7
        //36. UPC8
        //37. UPC9
        //38. UPC10
        //39. Vendor1
        //40. Vendor2
        //41. Vendor3
        //42. Vendor4
        //43. Vendor5
        //44. Vendor6
        //45. Vendor7
        //46. Vendor8
        //47. Vendor9
        //48. Vendor10
        { { "L1", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
            "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
            "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
            "","","" ,"",""} };
        Object attrValue;
        //TMS no#20150526-00042
        String itemAttrs[][] =
        
        {{ "CExtAttr6"},{ "Segment1", "Description", "LongDescription", "PrimaryUomCode", 
          "ShelfLifeDays", "ListPricePerUnit", "", "", "" ,"ItemType","","HazardousMaterialFlag","UnitWeight","","",""},
          {"CExtAttr2"},
          {"Attribute22","ItemCatalogGroupId"},
        {"CExtAttr14","TlExtAttr2","","","","CExtAttr1","CExtAttr15","CExtAttr20"},
        {"GlobalAttribute6"},
          {"CrossReference"},
        {"CrossReference"},
        {"CExtAttr3"},
        {"GlobalAttribute5"}
        };
        String voList[] = { "XxwcChgEgoItemAttrisVO1","XxwcMtlSyItemsChgBVO1","XxwcChgEgoItemAttrisVO1","XxwcMtlSyItemsChgBVO1","XxwcChgEgoItemAttrsLevel3VO1","XxwcMtlSyItemsChgBVO1","XxwcChgMtlCrossRefUPCVO1","XxwcChgMtlCrossRefVendorVO1","XxwcChgEgoItemAttrsLevel3VO1","XxwcMtlSyItemsChgBVO1" };
        int voRowCount[] = {1,1,1,1,1,1,10,10,1,1};
        for (int i = 0, k = 0; i < voList.length; i++) {
            OAViewObject vo = (OAViewObject)am.findViewObject(voList[i]);
//            System.out.println(" vo name : "+voList[i]+" vo fetchedcount : "+vo.getFetchedRowCount());
            vo.reset();
//            for(OARow row = (OARow)vo.next();row != null;row = (OARow)vo.next())
            for(int l = 0; l<voRowCount[i];l++)
            {
             OARow row = (OARow) vo.getRowAtRangeIndex(l);
                for (int j = 0; j < itemAttrs[i].length; j++,k++) {
                    if (!"".equals(itemAttrs[i][j]) && row != null) {
                     if((voList[i].equals("XxwcChgMtlCrossRefUPCVO1") || voList[i].equals("XxwcChgMtlCrossRefVendorVO1")))
                     {
                      if("Y".equals(row.getAttribute("IsRequiredValidation")))
                      {
                        attrValue = row.getAttribute(itemAttrs[i][j]);
//                        if(voList[i].equals("XxwcChgMtlCrossRefUPCVO1"))
//                         attrValue = (attrValue == null) ? "" : row.getAttribute("CrossReferenceType")+"-"+attrValue.toString();
                        inputData[0][k] = 
                                (attrValue == null) ? "" : attrValue.toString();
                      }
                      else continue;
                     }
                      else
                          {
                            attrValue = row.getAttribute(itemAttrs[i][j]);
                            inputData[0][k] = 
                                    (attrValue == null) ? "" : attrValue.toString();
                          }                      
                    }
//                    k++;
                }
            }

        }
//        System.out.println(" caling  validateItem(am, envName, inputData)");
        return validateItem(am, envName, inputData);
        // return validateItem(envName,inputData);

    }


    public static String[] validateItem(String envName, String[][] inputData) {
        ArrayList<String> outputData = new ArrayList<String>();
        long starttime;

        EdqpItemValidation wrap1 = 
            new EdqpItemValidation(envName, "XXWC_Item_SingleItem_DSA");
//        System.out.println(" Getting results " + inputData.toString());

        //for (int i=0;i<=10;i++)
        //for (int i = 0; i < inputData.length; i++) {
            //starttime = System.currentTimeMillis();
            List TestData = 
                wrap1.validateItemWithEDQP(inputData, "00_Results");
//        System.out.println("CAlled EDQP "); 
            Iterator TestIterator = TestData.iterator();

            while (TestIterator.hasNext()) {
                String outField = (String)TestIterator.next();
                outputData.add(outField);
                //System.out.print(outField + " , " );
            }
//                 System.out.println("EDQP result " + TestData.toString());
            //System.out.println(" done " + 
             //                  (System.currentTimeMillis() - starttime));
            
        //}
        //This block to be used only if exceptions are expected out of the DSA
        /*
           System.out.println(" Getting Exceptions.. " + System.currentTimeMillis() );

           List TestData1 = wrap1.validateItemWithEDQP(m_inputData, "20_Exception_Output");
            Iterator TestIterator1 = TestData1.iterator();
                                   while (TestIterator1.hasNext())
                                   {
                                           String outField1 = (String)TestIterator1.next();
                                           System.out.print(outField1 + " , " );
                                   }
                                   System.out.println(" done " + System.currentTimeMillis() );
           */

        return outputData.toArray(new String[outputData.size()]);
    }


}
