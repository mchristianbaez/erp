/*
 +===========================================================================+
 |  HISTORY                                                                  |
 |  31-Jul-2015 -   TMS#20150708-00061                                       |
 +===========================================================================*/

package xxwc.oracle.apps.ego.item.eu.server;

import oracle.apps.fnd.framework.server.OAViewRowImpl;

import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.AttributeDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class XxwcUDALevel1RedLineVORowImpl extends OAViewRowImpl {
    public static final int EXTENSIONID = 0;
    public static final int ORGANIZATIONID = 1;
    public static final int INVENTORYITEMID = 2;
    public static final int ITEMCATALOGGROUPID = 3;
    public static final int ATTRGROUPID = 4;
    public static final int DATALEVELID = 5;
    public static final int CREATEDBY = 6;
    public static final int CREATIONDATE = 7;
    public static final int LASTUPDATEDBY = 8;
    public static final int LASTUPDATEDATE = 9;
    public static final int PRIMARYVENDOR = 10;
    public static final int SUGGESTEDRETAILPRICE = 11;
    public static final int RETAILSELLINGUOM = 12;
    public static final int CONFLICTMATERIAL = 13;
    public static final int COUNTRYOFORIGIN = 14;
    public static final int FULFILLMENT = 15;

    /**This is the default constructor (do not remove)
     */
    public XxwcUDALevel1RedLineVORowImpl() {
    }

    /**Gets the attribute value for the calculated attribute ExtensionId
     */
    public Number getExtensionId() {
        return (Number)getAttributeInternal(EXTENSIONID);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute ExtensionId
     */
    public void setExtensionId(Number value) {
        setAttributeInternal(EXTENSIONID, value);
    }

    /**Gets the attribute value for the calculated attribute OrganizationId
     */
    public Number getOrganizationId() {
        return (Number)getAttributeInternal(ORGANIZATIONID);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute OrganizationId
     */
    public void setOrganizationId(Number value) {
        setAttributeInternal(ORGANIZATIONID, value);
    }

    /**Gets the attribute value for the calculated attribute InventoryItemId
     */
    public Number getInventoryItemId() {
        return (Number)getAttributeInternal(INVENTORYITEMID);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute InventoryItemId
     */
    public void setInventoryItemId(Number value) {
        setAttributeInternal(INVENTORYITEMID, value);
    }

    /**Gets the attribute value for the calculated attribute ItemCatalogGroupId
     */
    public Number getItemCatalogGroupId() {
        return (Number)getAttributeInternal(ITEMCATALOGGROUPID);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute ItemCatalogGroupId
     */
    public void setItemCatalogGroupId(Number value) {
        setAttributeInternal(ITEMCATALOGGROUPID, value);
    }

    /**Gets the attribute value for the calculated attribute AttrGroupId
     */
    public Number getAttrGroupId() {
        return (Number)getAttributeInternal(ATTRGROUPID);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute AttrGroupId
     */
    public void setAttrGroupId(Number value) {
        setAttributeInternal(ATTRGROUPID, value);
    }

    /**Gets the attribute value for the calculated attribute DataLevelId
     */
    public Number getDataLevelId() {
        return (Number)getAttributeInternal(DATALEVELID);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute DataLevelId
     */
    public void setDataLevelId(Number value) {
        setAttributeInternal(DATALEVELID, value);
    }

    /**Gets the attribute value for the calculated attribute CreatedBy
     */
    public Number getCreatedBy() {
        return (Number)getAttributeInternal(CREATEDBY);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute CreatedBy
     */
    public void setCreatedBy(Number value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**Gets the attribute value for the calculated attribute CreationDate
     */
    public Date getCreationDate() {
        return (Date)getAttributeInternal(CREATIONDATE);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute CreationDate
     */
    public void setCreationDate(Date value) {
        setAttributeInternal(CREATIONDATE, value);
    }

    /**Gets the attribute value for the calculated attribute LastUpdatedBy
     */
    public Number getLastUpdatedBy() {
        return (Number)getAttributeInternal(LASTUPDATEDBY);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute LastUpdatedBy
     */
    public void setLastUpdatedBy(Number value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**Gets the attribute value for the calculated attribute LastUpdateDate
     */
    public Date getLastUpdateDate() {
        return (Date)getAttributeInternal(LASTUPDATEDATE);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute LastUpdateDate
     */
    public void setLastUpdateDate(Date value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**Gets the attribute value for the calculated attribute PrimaryVendor
     */
    public String getPrimaryVendor() {
        return (String)getAttributeInternal(PRIMARYVENDOR);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute PrimaryVendor
     */
    public void setPrimaryVendor(String value) {
        setAttributeInternal(PRIMARYVENDOR, value);
    }

    /**Gets the attribute value for the calculated attribute SuggestedRetailPrice
     */
    public String getSuggestedRetailPrice() {
        return (String)getAttributeInternal(SUGGESTEDRETAILPRICE);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute SuggestedRetailPrice
     */
    public void setSuggestedRetailPrice(String value) {
        setAttributeInternal(SUGGESTEDRETAILPRICE, value);
    }

    /**Gets the attribute value for the calculated attribute RetailSellingUom
     */
    public String getRetailSellingUom() {
        return (String)getAttributeInternal(RETAILSELLINGUOM);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute RetailSellingUom
     */
    public void setRetailSellingUom(String value) {
        setAttributeInternal(RETAILSELLINGUOM, value);
    }

    /**Gets the attribute value for the calculated attribute ConflictMaterial
     */
    public String getConflictMaterial() {
        return (String)getAttributeInternal(CONFLICTMATERIAL);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute ConflictMaterial
     */
    public void setConflictMaterial(String value) {
        setAttributeInternal(CONFLICTMATERIAL, value);
    }

    /**getAttrInvokeAccessor: generated method. Do not modify.
     */
    protected Object getAttrInvokeAccessor(int index, 
                                           AttributeDefImpl attrDef) throws Exception {
        switch (index) {
        case EXTENSIONID:
            return getExtensionId();
        case ORGANIZATIONID:
            return getOrganizationId();
        case INVENTORYITEMID:
            return getInventoryItemId();
        case ITEMCATALOGGROUPID:
            return getItemCatalogGroupId();
        case ATTRGROUPID:
            return getAttrGroupId();
        case DATALEVELID:
            return getDataLevelId();
        case CREATEDBY:
            return getCreatedBy();
        case CREATIONDATE:
            return getCreationDate();
        case LASTUPDATEDBY:
            return getLastUpdatedBy();
        case LASTUPDATEDATE:
            return getLastUpdateDate();
        case PRIMARYVENDOR:
            return getPrimaryVendor();
        case SUGGESTEDRETAILPRICE:
            return getSuggestedRetailPrice();
        case RETAILSELLINGUOM:
            return getRetailSellingUom();
        case CONFLICTMATERIAL:
            return getConflictMaterial();
        case COUNTRYOFORIGIN:
            return getCountryOfOrigin();
        case FULFILLMENT:
            return getFulfillment();
        default:
            return super.getAttrInvokeAccessor(index, attrDef);
        }
    }

    /**setAttrInvokeAccessor: generated method. Do not modify.
     */
    protected void setAttrInvokeAccessor(int index, Object value, 
                                         AttributeDefImpl attrDef) throws Exception {
        switch (index) {
        case COUNTRYOFORIGIN:
            setCountryOfOrigin((String)value);
            return;
        case FULFILLMENT:
            setFulfillment((String)value);
            return;
        default:
            super.setAttrInvokeAccessor(index, value, attrDef);
            return;
        }
    }

    /**Gets the attribute value for the calculated attribute CountryOfOrigin
     */
    public String getCountryOfOrigin() {
        return (String) getAttributeInternal(COUNTRYOFORIGIN);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute CountryOfOrigin
     */
    public void setCountryOfOrigin(String value) {
        setAttributeInternal(COUNTRYOFORIGIN, value);
    }

    /**Gets the attribute value for the calculated attribute Fulfillment
     */
    public String getFulfillment() {
        return (String) getAttributeInternal(FULFILLMENT);
    }

    /**Sets <code>value</code> as the attribute value for the calculated attribute Fulfillment
     */
    public void setFulfillment(String value) {
        setAttributeInternal(FULFILLMENT, value);
    }
}
