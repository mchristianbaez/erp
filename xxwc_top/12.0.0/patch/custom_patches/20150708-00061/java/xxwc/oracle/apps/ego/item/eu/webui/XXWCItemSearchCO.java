/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 +===========================================================================*/
package xxwc.oracle.apps.ego.item.eu.webui;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OADialogPage;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OASwitcherBean;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

import oracle.apps.fnd.framework.webui.beans.form.OASubmitButtonBean;

import oracle.apps.fnd.framework.webui.beans.layout.OAHeaderBean;

import xxwc.oracle.apps.ego.item.eu.server.XXWCEditItemUpdateAMImpl;
import xxwc.oracle.apps.ego.item.eu.util.ItemUtil;

/**
 * Controller for ...
 */
public class XXWCItemSearchCO extends OAControllerImpl {
    public static final String RCS_ID = "$Header$";
    public static final boolean RCS_ID_RECORDED = 
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

    /**
     * Layout and page setup logic for a region.
     * @param pageContext the current OA page context
     * @param webBean the web bean corresponding to the region
     */
    public void processRequest(OAPageContext pageContext, OAWebBean webBean) {
        super.processRequest(pageContext, webBean);
        if (pageContext.isBackNavigationFired(false)) {
            OADialogPage dialogPage = new OADialogPage(NAVIGATION_ERROR);
            pageContext.redirectToDialogPage(dialogPage);
        }
        String itemCreateTitle = "Search for Item Create", itemEditTitle = "Search for Item Maintenance";
        String sourceMenu = pageContext.getParameter("sourceMenu");
        OAHeaderBean hBean = (OAHeaderBean) webBean.findChildRecursive("HeaderRN");
        if(!ItemUtil.isNull(sourceMenu) && "XXWC_ADD".equals(sourceMenu))
        {
         pageContext.putSessionValue("sourceMenu",sourceMenu);
         hBean.setText(pageContext,itemCreateTitle);
         OASwitcherBean switchBean = (OASwitcherBean)webBean.findChildRecursive("CreateCO");
         switchBean.setRendered(false);
        }
        else
        {
         OASubmitButtonBean createPartBean = (OASubmitButtonBean)webBean.findChildRecursive("CreateItem");
         if(createPartBean != null)
          createPartBean.setRendered(false);
         pageContext.removeSessionValue("sourceMenu");
         hBean.setText(pageContext,itemEditTitle);
        }
    }

    /**
     * Procedure to handle form submissions for form elements in
     * a region.
     * @param pageContext the current OA page context
     * @param webBean the web bean corresponding to the region
     */
    public void processFormRequest(OAPageContext pageContext, 
                                   OAWebBean webBean) {
        super.processFormRequest(pageContext, webBean);
        XXWCEditItemUpdateAMImpl am = 
            (XXWCEditItemUpdateAMImpl)pageContext.getApplicationModule(webBean);
        String itemNumber = null;
        if (pageContext.getParameter("GoItem") != null) {
            String partNumber = 
                pageContext.getParameter("PartNumber"), descption = 
                pageContext.getParameter("SearchDescription");
            String searchError = am.serachParts(partNumber, descption);
            if (searchError != null) {
                throw new OAException(searchError);
            }
//            String sourceMenu = (String)pageContext.getSessionValue("sourceMenu");
//            if(!ItemUtil.isNull(sourceMenu) && "XXWC_ADD".equals(sourceMenu) && am.getXXWCEgoMtlSystemItemsVO1().getRowCount() <= 0)
//            {
//                itemNumber = pageContext.getParameter("PartNumber");
//                am.getOADBTransaction().putTransientValue("pageMode", "Create");
//                am.getOADBTransaction().putTransientValue("itemNumber",itemNumber);
//                    pageContext.setForwardURL("OA.jsp?page=/xxwc/oracle/apps/ego/item/eu/webui/XXWCSingleItemPG", 
//                                              null, 
//                                              OAWebBeanConstants.KEEP_MENU_CONTEXT, 
//                                              null, null, true, 
//                                              OAWebBeanConstants.ADD_BREAD_CRUMB_YES, 
//                                              OAWebBeanConstants.IGNORE_MESSAGES);
//            }
        } else if (pageContext.getParameter("ClearItem") != null) {
            pageContext.forwardImmediatelyToCurrentPage(null, false, null);
        }
        else if (pageContext.getParameter("CreateItem") != null) {
            itemNumber = pageContext.getParameter("PartNumber");
            if(ItemUtil.isNull(itemNumber))
            {
             throw new OAException("XXWC","XXWC_VALID_ITEM_NUMBER");
            }
            am.serachParts(itemNumber, null);
            if(am.getXXWCEgoMtlSystemItemsVO1().getRowCount() <= 0)
            {
            am.getOADBTransaction().putTransientValue("pageMode", "Create");
            am.getOADBTransaction().putTransientValue("itemNumber", 
                                                      itemNumber);
                pageContext.setForwardURL("OA.jsp?page=/xxwc/oracle/apps/ego/item/eu/webui/XXWCSingleItemPG", 
                                          null, 
                                          OAWebBeanConstants.KEEP_MENU_CONTEXT, 
                                          null, null, true, 
                                          OAWebBeanConstants.ADD_BREAD_CRUMB_YES, 
                                          OAWebBeanConstants.IGNORE_MESSAGES);
            }
            else throw new OAException("XXWC","XXWC_ITEM_ALREADY_EXISTS");
        }

    }

}
