/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 |  09-Jul-2015 -   EDQP Validation condition                                |
 |  31-Jul-2015 -   TMS#20150708-00061                                       |
 +===========================================================================*/
package xxwc.oracle.apps.ego.item.eu.webui;

import java.io.Serializable;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.OARow;
import oracle.apps.fnd.framework.OAViewObject;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OADialogPage;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAProcessingPage;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

import oracle.apps.fnd.framework.webui.beans.layout.OAAttachmentTableBean;
import oracle.apps.fnd.framework.webui.beans.layout.OAPageLayoutBean;
import oracle.apps.fnd.framework.webui.beans.layout.OASubTabLayoutBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageChoiceBean;

import oracle.apps.fnd.framework.webui.beans.message.OAMessageStyledTextBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageTextInputBean;

import oracle.jbo.domain.Number;

import xxwc.oracle.apps.ego.item.eu.server.XXWCEditItemUpdateAMImpl;
import xxwc.oracle.apps.ego.item.eu.server.XXWCSecurityVOImpl;
import xxwc.oracle.apps.ego.item.eu.server.XXWCSecurityVORowImpl;
import xxwc.oracle.apps.ego.item.eu.server.XxwcMtlSyItemsChgBVOImpl;
import xxwc.oracle.apps.ego.item.eu.server.XxwcMtlSyItemsChgBVORowImpl;
import xxwc.oracle.apps.ego.item.eu.util.EdqpItemValidation;
import xxwc.oracle.apps.ego.item.eu.util.ItemUtil;

/**
 * Controller for ...
 */
public class XXWCSingleItemCO extends OAControllerImpl {
    public static final String RCS_ID = "$Header$";
    public static final boolean RCS_ID_RECORDED = 
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

    /**
     * Layout and page setup logic for a region.
     * @param pageContext the current OA page context
     * @param webBean the web bean corresponding to the region
     */
    public void processRequest(OAPageContext pageContext, OAWebBean webBean) {
        super.processRequest(pageContext, webBean);


        if (pageContext.isBackNavigationFired(false)) {
            OADialogPage dialogPage = new OADialogPage(NAVIGATION_ERROR);
            pageContext.redirectToDialogPage(dialogPage);
        }
        //To check navigation back from Attachments page.
        String mode = pageContext.getParameter("Mode");
        if(ItemUtil.isNull(mode))
        {
         mode = (String) pageContext.getSessionValue("anyAttachAction");
         pageContext.removeSessionValue("anyAttachAction");
        }

        OAApplicationModule am = pageContext.getRootApplicationModule();
        
        //Change the page title based on user activity.
        OAPageLayoutBean pageLayoutBean = (OAPageLayoutBean)webBean;
        String itemCreateTitle = "Item Create", itemEditTitle = "Item Maintenance";

//        OAMessageChoiceBean choiceCOBean = 
//            (OAMessageChoiceBean)webBean.findChildRecursive("choiceCO");
//        if (choiceCOBean != null) {
//            OADefaultDoubleColumnBean region5634 = 
//                (OADefaultDoubleColumnBean)webBean.findChildRecursive("region5634");
//            OAMessageTextInputBean WCPartNum1Bean = 
//                (OAMessageTextInputBean)region5634.findIndexedChildRecursive("WCPartNum1");
//            choiceCOBean.setPickListCacheEnabled(false);
//            //WCPartNum1Bean.setValue(pageContext, "999");
//
//            //choiceCOBean.setListVOBoundContainerColumn(0,region5634,"WCPartNum1");
//        }
         OAAttachmentTableBean attBean = (OAAttachmentTableBean) webBean.findIndexedChildRecursive("AttachmentsTableRN");
         OAAttachmentTableBean attROBean = (OAAttachmentTableBean) webBean.findIndexedChildRecursive("AttachmentsTableRORN");
            if(!ItemUtil.isNull(mode))
            {
             String attachRegion = (String)pageContext.getSessionValue("attachmentRegion");
             if(!ItemUtil.isNull(attachRegion))
             {
              if(attachRegion.equals("AttachmentsTableRORN"))
              {
                 XxwcMtlSyItemsChgBVOImpl vo = ((XXWCEditItemUpdateAMImpl)am).getXxwcMtlSyItemsChgBVO1();
                 XxwcMtlSyItemsChgBVORowImpl voRow = (XxwcMtlSyItemsChgBVORowImpl) vo.first();
                 if(voRow != null && voRow.getCreatedBy() != null && voRow.getCreatedBy().compareTo(pageContext.getUserId()) != 0)
                 {
                  attROBean.setUpdateable(true);
                 }
              }
              attBean.setRendered(attachRegion.equals("AttachmentsTableRN"));
              attROBean.setRendered(attachRegion.equals("AttachmentsTableRORN"));
             }
            }
            else
            {
             pageContext.removeSessionValue("attachmentRegion");
            }
        
        String pageMode = 
            (String)am.getOADBTransaction().getTransientValue("pageMode");
        if (!ItemUtil.isNull(pageMode) && pageMode.equals("Create") && ItemUtil.isNull(mode)) {
            pageLayoutBean.setTitle(itemCreateTitle);
            pageLayoutBean.setWindowTitle(itemCreateTitle);
            String itemNumber = (String)am.getOADBTransaction().getTransientValue("itemNumber");
            ((XXWCEditItemUpdateAMImpl)am).createItem(itemNumber);
            attBean.setRendered(true);
            pageContext.putSessionValue("attachmentRegion","AttachmentsTableRN");
        }
        OAMessageTextInputBean WCPartNumBean = (OAMessageTextInputBean)webBean.findChildRecursive("WCPartNum1");
        OAMessageChoiceBean UOMBean = (OAMessageChoiceBean)webBean.findChildRecursive("SellUOM");
        OAMessageTextInputBean POCost = (OAMessageTextInputBean)webBean.findChildRecursive("POCost");
        OAMessageChoiceBean itemType = (OAMessageChoiceBean)webBean.findChildRecursive("ItemType");
        if ("Create".equals(pageMode))
        {
          POCost.setDisabled(false);
          UOMBean.setReadOnly(false);
          itemType.setReadOnly(false);
        }
        else 
        {
         POCost.setDisabled(true);
         itemType.setReadOnly(true);
        }
        String fromPage = pageContext.getParameter("FromPG"), changeId = pageContext.getParameter("ChangeId");
        if(!ItemUtil.isNull(fromPage))
        {
         pageContext.putSessionValue("FromPG",fromPage);
        }
        if (!ItemUtil.isNull(fromPage) && !ItemUtil.isNull(changeId) && ItemUtil.isNull(mode)) {
            ((XXWCEditItemUpdateAMImpl)am).searchChangeOrder(changeId);
            pageContext.putTransactionTransientValue("EventChangeId", 
                                                     changeId);
            pageContext.putTransactionTransientValue("ChangeOrderSelected", 
                                                     "Y");
//            if (choiceCOBean != null)
//                choiceCOBean.setSelectedValue(changeId);
//            choiceCOBean.setDisabled(true);
             //Enable Part Number & UOM fields in Approver login
             XxwcMtlSyItemsChgBVOImpl vo = ((XXWCEditItemUpdateAMImpl)am).getXxwcMtlSyItemsChgBVO1();
             XxwcMtlSyItemsChgBVORowImpl voRow = (XxwcMtlSyItemsChgBVORowImpl) vo.first();
             vo.setCurrentRow(voRow);
//             XXWCRequestRelatedVOImpl rVo = ((XXWCEditItemUpdateAMImpl)am).getXXWCRequestRelatedVO1();
//             XXWCRequestRelatedVORowImpl rVoRow = (XXWCRequestRelatedVORowImpl) rVo.first();
             if(voRow != null && voRow.getCreatedBy() != null && voRow.getCreatedBy().compareTo(pageContext.getUserId()) != 0)
             {
              attROBean.setUpdateable(true);
             }
             attROBean.setRendered(true);
             pageContext.putSessionValue("attachmentRegion","AttachmentsTableRORN");
             if(voRow != null && voRow.getCreatedBy() != null && voRow.getCreatedBy().compareTo(pageContext.getUserId()) != 0 && "CI".equals(voRow.getGlobalAttribute4()))
             {
               if(WCPartNumBean != null)
                WCPartNumBean.setReadOnly(false);
               if(UOMBean != null)
                UOMBean.setReadOnly(false);
               POCost.setDisabled(false);
             }
             if(voRow != null && voRow.getGlobalAttribute4() != null && "CI".equals(voRow.getGlobalAttribute4()))
             {
              //Do not display redline info for create item.
              OAMessageStyledTextBean vendorCrossRefType = (OAMessageStyledTextBean) webBean.findChildRecursive("RedLineCrossRefTypeVendor");
              OAMessageStyledTextBean vendorCrossRefName = (OAMessageStyledTextBean) webBean.findChildRecursive("RedLineVenNameVendor");
              OAMessageStyledTextBean upcCrossRefType = (OAMessageStyledTextBean) webBean.findChildRecursive("RedLineCrossRefTypeUPC");
              OAMessageStyledTextBean upcCrossRefName = (OAMessageStyledTextBean) webBean.findChildRecursive("RedLineVenNameUPC");
              OAMessageStyledTextBean upcCrossRefUPCType = (OAMessageStyledTextBean) webBean.findChildRecursive("RedLineCrossRefTypeUPCType");
              vendorCrossRefType.setRendered(false);
              vendorCrossRefName.setRendered(false);
              upcCrossRefType.setRendered(false);
              upcCrossRefName.setRendered(false);
              upcCrossRefUPCType.setRendered(false);
              pageLayoutBean.setTitle(itemCreateTitle);
              pageLayoutBean.setWindowTitle(itemCreateTitle);
             }
             else
             {
                 pageLayoutBean.setTitle(itemEditTitle);
                 pageLayoutBean.setWindowTitle(itemEditTitle);
             }
        }
        changeId = 
                (String)pageContext.getTransactionTransientValue("EventChangeId");
//        if (changeId != null) {
//            choiceCOBean.setSelectedValue(changeId);
//        }

//        String voInitialized = 
//            (String)pageContext.getTransactionTransientValue("voInitialized");

//        if (voInitialized == null) {
//            am.invokeMethod("initQuery");
//            pageContext.putTransactionTransientValue("voInitialized", "N");
//        }
        String partNum = pageContext.getParameter("partNum"), changeOrderSelected = (String)pageContext.getTransactionTransientValue("ChangeOrderSelected");
        if (partNum != null && ItemUtil.isNull(changeOrderSelected) && ItemUtil.isNull(mode))
        {
            ((XXWCEditItemUpdateAMImpl)am).searchItem(partNum);
            attBean.setRendered(true);
            pageContext.putSessionValue("attachmentRegion","AttachmentsTableRN");
            pageLayoutBean.setTitle(itemEditTitle);
            pageLayoutBean.setWindowTitle(itemEditTitle);
        }

        //Capture Item Number & UIM into session for future validation.
        ((XXWCEditItemUpdateAMImpl)am).captureData();
        
        //Hide/Show tabs based on access level
        ((XXWCEditItemUpdateAMImpl)am).initSecurity(pageMode);
        OASubTabLayoutBean subTabLayoutRN = 
            (OASubTabLayoutBean)webBean.findIndexedChildRecursive("SubTabLayoutRN");
        if (subTabLayoutRN != null) {
            XXWCSecurityVOImpl securityVO = 
                ((XXWCEditItemUpdateAMImpl)am).getXXWCSecurityVO1();
            XXWCSecurityVORowImpl securityVORow = 
                (XXWCSecurityVORowImpl)securityVO.first();
            if (securityVORow != null) {
                subTabLayoutRN.hideSubTab(0, !securityVORow.getL1Access());
                subTabLayoutRN.hideSubTab(1, !securityVORow.getL2Access());
                subTabLayoutRN.hideSubTab(2, !securityVORow.getL3Access());
            }
        }
        //End of Hide/Show tabs based on access level
    }

    /**
     * Procedure to handle form submissions for form elements in
     * a region.
     * @param pageContext the current OA page context
     * @param webBean the web bean corresponding to the region
     */
    public void processFormRequest(OAPageContext pageContext, 
                                   OAWebBean webBean) {
        super.processFormRequest(pageContext, webBean);

        OAApplicationModule am = pageContext.getRootApplicationModule();
        String errorMessage;
        
        if (pageContext.getParameter("saveBtn") != null) {
        
            //TMS no#20150526-00042
            ((XXWCEditItemUpdateAMImpl)am).validateMandatory();
            //////////////////////////////////////////////////////////
            // EDQP Integration
            String edqpProfile = pageContext.getProfile("XXWC_EDQP_AVAILABILITY"),result = null;
            if(!ItemUtil.isNull(edqpProfile) && edqpProfile.equalsIgnoreCase("Y"))
             result = validateItem(am);
            else
             result = "SUCCESS";
            if(result!=null && result.startsWith("SUCCESS"))
            {
            //showDialogPage(pageContext,"EDQP Result : "+result,"Successfully Created Change Order "+changeName);
            // showDialogPage(pageContext,"EDQP Result : "+result,"");
            //throw new OAException("Successfully Created Change Order "+changeName,OAException.INFORMATION);
            // End 
            //////////////////////////////////////////////////////////
            String changeId = (String)pageContext.getTransactionTransientValue("EventChangeId");
            String changeName = (String)am.getOADBTransaction().getTransientValue("CHANGE_NAME");
            Number changeIdNum = null;
            if (changeId == null) {
                String pageMode = 
                    (String)am.getOADBTransaction().getTransientValue("pageMode");
                String newItemFlag = "N";
                if ("Create".equals(pageMode)) {
                    newItemFlag = "Y";
                }

                Serializable params[] = { newItemFlag };
                am.invokeMethod("createChangeOrder", params);
                changeName = 
                        (String)am.getOADBTransaction().getTransientValue("CHANGE_NAME");
                changeId = 
                        am.getOADBTransaction().getTransientValue("CHANGE_ID").toString();
                pageContext.putTransactionTransientValue("EventChangeId", 
                                                         changeId);
            }
            ((XXWCEditItemUpdateAMImpl)am).commitChanges();
            if(changeId != null)
            {
                ((XXWCEditItemUpdateAMImpl)am).updateAttachDetails();
            }
            am.getOADBTransaction().removeTransientValue("pageMode");
            am.getOADBTransaction().removeTransientValue("itemNumber");
//            am.invokeMethod("refreshData");
            OAProcessingPage page = new OAProcessingPage("xxwc.oracle.apps.ego.item.eu.webui.ProcessingCO");
            page.setConciseMessage(pageContext.getMessage("XXWC","XXWC_PROCESSING_MSG",null));
            page.setDetailedMessage(pageContext.getMessage("XXWC","XXWC_PROCESSING_DETAIL_MSG",null));
            page.setProcessName(pageContext.getMessage("XXWC","XXWC_PROCESSING_PROC",null));
            pageContext.forwardToProcessingPage(page);
        }
        else 
        {
          String[] parts = result.split(",");
          result = parts[0].replace("ERROR","").replace("|","").replace(":",",");
          throw new OAException("Validation Error : "+result);
        }
        }
        else if(pageContext.getParameter("ItemSearchBtn") != null)
        {
         String fromPage = (String)pageContext.getSessionValue("FromPG");
         if(!ItemUtil.isNull(fromPage) && fromPage.equalsIgnoreCase("WFPG"))
         {
            pageContext.removeSessionValue("FromPG");
            //TMS#20150708-00061
            pageContext.setForwardURL("OA.jsp?page=/xxwc/oracle/apps/ego/item/eu/webui/WorkflowMonitorPG&retainAM=Y",
                                          null, 
                                          OAWebBeanConstants.KEEP_MENU_CONTEXT, 
                                          null, null, true, 
                                          OAWebBeanConstants.ADD_BREAD_CRUMB_NO, 
                                          OAWebBeanConstants.IGNORE_MESSAGES);
         }
         else
         {
         String sourceMenu = (String)pageContext.getSessionValue("sourceMenu");
         pageContext.setForwardURL(((!ItemUtil.isNull(sourceMenu) && "XXWC_ADD".equals(sourceMenu))?"OA.jsp?OAFunc=XXWC_ITEM_CREATE&retainAM=N":"OA.jsp?page=/xxwc/oracle/apps/ego/item/eu/webui/XXWCItemSearchPG&retainAM=N"), 
                                      null, 
                                      OAWebBeanConstants.KEEP_MENU_CONTEXT, 
                                      null, null, false, 
                                      OAWebBeanConstants.ADD_BREAD_CRUMB_NO, 
                                      OAWebBeanConstants.IGNORE_MESSAGES);
         }
        }
        else if("DeleteVendorEvent".equals(pageContext.getParameter(EVENT_PARAM)))
        {
         String crossRefId = pageContext.getParameter("DeleteVenCrossRefId");
         ((XXWCEditItemUpdateAMImpl)am).deleteCrossRefId("V",crossRefId);
        }
        else if("DeleteUPCEvent".equals(pageContext.getParameter(EVENT_PARAM)))
        {
         String crossRefId = pageContext.getParameter("DeleteUPCCrossRefId");
         ((XXWCEditItemUpdateAMImpl)am).deleteCrossRefId("U",crossRefId);
        }
        else if("oaUpdateAttachment".equals(pageContext.getParameter(EVENT_PARAM)) || "oaDeleteAttachment".equals(pageContext.getParameter(EVENT_PARAM)) || "oaAddAttachment".equals(pageContext.getParameter(EVENT_PARAM)))
        {
         pageContext.putSessionValue("anyAttachAction",pageContext.getParameter(EVENT_PARAM));
        }
        //TMS#20150708-00061
        else if("WebSuteEvent".equals(pageContext.getParameter(EVENT_PARAM)))
        {
         String changeId = pageContext.getParameter("WebSuteEventChgId");
         ((XXWCEditItemUpdateAMImpl)am).enableDisableL3UDAMandatory(changeId);
        }
//        else if("UPCCodeEvent".equals(pageContext.getParameter(EVENT_PARAM)))
//        {
//         String crossRefId = pageContext.getParameter("UPCCodeEventVal");
//         System.out.println("---GsrC--- crossRefId : "+crossRefId);
//         ((XXWCEditItemUpdateAMImpl)am).fetchUPCCodeTypeValue(crossRefId);
//        }
            
        //      if (pageContext.getParameter("searchBtn") != null) {
        //      
        //         String itemNumber = pageContext.getParameter("searchItem1");
        //         
        //         if (itemNumber != null) {
        //             Serializable aserializable1[] = {itemNumber};
        //             am.invokeMethod("searchItem",aserializable1);
        //             //pageContext.forwa
        //         }
        //         
        //      }


//        String eventChangeId = (String)pageContext.getParameter("choiceCO");
//
//        if (eventChangeId != null && 
//            "EventChangeOrderSelect".equals(pageContext.getParameter(EVENT_PARAM))) {
//
//            Serializable aserializable1[] = { eventChangeId };
//            am.invokeMethod("searchChangeOrder", aserializable1);
//            pageContext.putTransactionTransientValue("EventChangeId", 
//                                                     eventChangeId);
//            pageContext.putTransactionTransientValue("ChangeOrderSelected", 
//                                                     "Y");
//            pageContext.removeTransactionTransientValue("voInitialized");
//            pageContext.forwardImmediatelyToCurrentPage(new HashMap(), true, 
//                                                        null);
//        }
    }

    protected String validateItem(OAApplicationModule am) {

        StringBuffer result = new StringBuffer();
        String edqpResult[] = EdqpItemValidation.validateItem(am);
//        if(edqpResult != null && edqpResult.length > 0 && "SUCCESS".equals(edqpResult[0]))
//         return true;
//        else
//         return false;
        for (int i = 0; i < edqpResult.length; ++i) {
//            System.out.println(edqpResult[i]);
            result = result.append(edqpResult[i]).append(",");
        }
//        System.out.println(result.toString());
        return result.toString();
    }

    //  protected void applySecuriy(OAPageContext pageContext, OAWebBean webBean){
    //      OAApplicationModule am=pageContext.getRootApplicationModule();
    //      String coSelected=(String)pageContext.getTransactionTransientValue("ChangeOrderSelected");
    //      if ( "Y".equals(coSelected)){
    //          String changeIdStr=(String)pageContext.getTransactionTransientValue("EventChangeId");
    //          OAViewObject vo=(OAViewObject)am.findViewObject("XXWCItemChgOrdersVO1");
    //          OARow row=getMatchingRow(vo,"ChangeId",changeIdStr);
    //
    //          String approvalStatus=(String)row.getAttribute("ApprovalStatus");
    //          String changeNotice=(String)row.getAttribute("ChangeNotice");
    //          String coCreatedUser=(String)row.getAttribute("CoCreatedBy");
    //          System.out.println("Approval Status ="+approvalStatus);
    //          String loggedInUser=pageContext.getUserName();
    //          if ("Approval requested".equals(approvalStatus) && coCreatedUser.equals(loggedInUser))
    //          {
    //            ClientUtil.setViewOnlyRecursive(pageContext, webBean);
    //            OAWebBean pageButtonsBean = (OAWebBean) pageContext.getPageLayoutBean().getPageButtons();
    //            OAWebBean pageButtonBean = (OAWebBean) pageButtonsBean.findIndexedChildRecursive("saveBtn");
    //            pageButtonBean.setRendered(false);
    //          }  
    //      }
    //      
    //  }

    private OARow getMatchingRow(OAViewObject vo, String attrName, 
                                 String attrValue) {
        Object attrV = null;
        vo.reset();
        OARow row = null;
        while (vo.hasNext()) {
            row = (OARow)vo.next();
            attrV = row.getAttribute(attrName);
            if (attrValue.equals(attrV.toString())) {
                return row;
            }
        }
        return row;
    }
}
