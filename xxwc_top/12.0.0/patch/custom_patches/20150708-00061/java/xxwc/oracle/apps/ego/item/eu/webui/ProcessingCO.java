/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 +===========================================================================*/
package xxwc.oracle.apps.ego.item.eu.webui;

import oracle.apps.fnd.common.MessageToken;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

import xxwc.oracle.apps.ego.item.eu.server.XXWCEditItemUpdateAMImpl;
import xxwc.oracle.apps.ego.item.eu.util.ItemUtil;

/**
 * Controller for ...
 */
public class ProcessingCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
    OAApplicationModule am = pageContext.getRootApplicationModule();
    String changeName = (String)am.getOADBTransaction().getTransientValue("CHANGE_NAME");
    int requestId = -1;
    String fromPage = (String)pageContext.getSessionValue("FromPG"), toPage = null;
    if(!ItemUtil.isNull(fromPage) && fromPage.equalsIgnoreCase("WFPG"))
    {
     pageContext.removeSessionValue("FromPG");
     toPage = "OA.jsp?page=/xxwc/oracle/apps/ego/item/eu/webui/WorkflowMonitorPG&retainAM=N";
    }
    else
    {
     String sourceMenu = (String)pageContext.getSessionValue("sourceMenu");
     toPage = ((!ItemUtil.isNull(sourceMenu) && "XXWC_ADD".equals(sourceMenu))?"OA.jsp?OAFunc=XXWC_ITEM_CREATE&retainAM=N":"OA.jsp?page=/xxwc/oracle/apps/ego/item/eu/webui/XXWCItemSearchPG&retainAM=N");
    }

    try
    {
      //Check is Item Number or UOM changed, If yes, call xxwc_pim_chg_item_mgmt.REPLACE_ITEM
      requestId = ((XXWCEditItemUpdateAMImpl)am).checkCapturedData();
      ((XXWCEditItemUpdateAMImpl)am).commitChanges();
    }
    catch(OAException e)
    {
     pageContext.putDialogMessage(e);
     pageContext.forwardImmediately(toPage,
                null,
                OAWebBeanConstants.KEEP_MENU_CONTEXT,
                null,
                null,
                true, // retain AM
                OAWebBeanConstants.ADD_BREAD_CRUMB_NO);
    }
    if(requestId == -1)
    {
     MessageToken[] mt = { new MessageToken("CHANGE_NAME", changeName) };
     pageContext.redirectToDialogPage(OAException.CONFIRMATION,new OAException(""),new OAException(pageContext.getMessage("XXWC","XXWC_CREATED_CHG_ORDER",mt)),APPS_HTML_DIRECTORY +toPage,null);
    }
    else
    {
     MessageToken[] mt = { new MessageToken("REQUEST_ID", requestId+""),new MessageToken("CHANGE_NAME", changeName) };
     pageContext.redirectToDialogPage(OAException.CONFIRMATION,new OAException(""),new OAException(pageContext.getMessage("XXWC","XXWC_CP_SUBMITTED",mt)),APPS_HTML_DIRECTORY +toPage,null);
    }
  }
}
