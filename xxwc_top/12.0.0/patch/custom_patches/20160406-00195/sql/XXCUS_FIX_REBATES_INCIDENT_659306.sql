/*
 TMS:  20160406-00195
 Incident: 659306 
 Date: 04/06/2016
*/
set serveroutput on size 1000000;
declare
 --

 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     print_log('Before create of backup table xxcus.xxcus_rebt_cust_tbl_backup as select * from xxcus.xxcus_rebt_cust_tbl');
     print_log('');
    --
    execute immediate 'create table xxcus.xxcus_rebt_cust_tbl_backup as select * from xxcus.xxcus_rebt_cust_tbl';
    --
     print_log('');     
     print_log('After create of backup table xxcus.xxcus_rebt_cust_tbl_backup as select * from xxcus.xxcus_rebt_cust_tbl');
     print_log('');  
     --  
     print_log('');     
     print_log('Before delete of xxcus.xxcus_rebt_cust_tbl where status_code =CLOSED');
     print_log('');
    --     
    delete xxcus.xxcus_rebt_cust_tbl where status_code ='CLOSED';
    --
     print_log('');     
     print_log('After delete of xxcus.xxcus_rebt_cust_tbl where status_code =CLOSED, rowcount ='||sql%rowcount);
     print_log('');
   --    
    commit;
    --
     print_log('');     
     print_log('Commit issued.');
     print_log('');
     --    
exception
 when others then
  rollback;
  print_log('Outer block, message ='||sqlerrm);
end;
/