CREATE OR REPLACE VIEW xxwc_rma_sales_order_v
/*************************************************************************
     $Header    : XXWC_RMA_SALES_ORDER_V
     Module Name: XXWC_RMA_SALES_ORDER_V
      
     REVISIONS:

     Ver        Date         Author              Description
     ---------  -----------  ---------------     -------------------------
     1.0        01/Jun/2016  Niraj K Ranjan      Initial Version for TMS#20160323-00165 
	 1.1        16/Feb/2016  Niraj K Ranjan      TMS#20170202-00005 Custom RMA return form Pilot issue
	 1.2        08/Mar/2017  Niraj K Ranjan      TMS#20170228-00054   Order Management - Return Sales 
	                                             Order error when searching by invoice number
     1.3        17/Jul/2017  Pattabhi Avula      TMS#20170622-00066 -- Fixed duplicate Records issue
	 1.4        14/Sep/2017  Niraj K Ranjan      TMS#20170913-00175   Resolve custom RMA same day returns
**************************************************************************/
AS
(-- Added this union query for Ver 1.4
 SELECT ooh.order_number
       ,ooh.header_id
	   ,ool.line_id
	   ,ool.line_number
       ,(ool.line_number||decode(shipment_number,null,null, '.'||shipment_number)||
                         decode(option_number,null,null,'.'||option_number)||
                         decode(component_number,null,null,'.'||component_number)||
                         decode(service_number,null,null,'.'||service_number)
        ) line_nbr_shp_nbr
       ,ool.ordered_item
	   ,ool.ordered_item_id
	   ,ool.inventory_item_id
	   ,ool.ordered_quantity
	   ,ool.shipped_quantity
	   ,(decode(oty.name,'COUNTER ORDER',ool.ordered_quantity
                        ,'STANDARD ORDER',ool.shipped_quantity
               ) - 
         NVL((SELECT sum(ool2.ordered_quantity)
              FROM apps.oe_order_lines_all ool2
              WHERE LEVEL > 1
              START WITH ool2.line_id = ool.line_id
              CONNECT BY PRIOR ool2.line_id = ool2.reference_line_id
              GROUP BY ool2.reference_line_id
            ),0)
        ) rma_quantity
	   ,ool.unit_list_price
	   ,ool.unit_selling_price
	   ,(ool.ordered_quantity * ool.unit_selling_price) Line_price
       ,NULL invoice_number
       ,ooh.org_id
	   ,ool.ship_from_org_id ol_ship_from_org_id
    FROM  apps.oe_order_headers_all ooh
         ,apps.oe_order_lines_all ool
         ,apps.oe_transaction_types_tl oty
    WHERE 1=1
    and   ooh.header_id=ool.header_Id
     AND  ooh.order_type_id = oty.transaction_type_id
     AND  oty.language = USERENV('LANG')
     AND  oty.name in ('STANDARD ORDER','COUNTER ORDER')
     AND  ooh.flow_status_code IN ('BOOKED','CLOSED')
     AND  ool.flow_status_code IN ('BOOKED','CLOSED')
    AND  NOT EXISTS(SELECT /*+ INDEX(rctla XXWC_RA_CUST_TRX_LINES_N14)*/ 1 
                    FROM apps.ra_customer_trx_lines_all rctla
                    WHERE 1=1
	                AND  rctla.interface_line_attribute6 = TO_CHAR (ool.line_id)
                    AND  rctla.interface_line_attribute1 = TO_CHAR (ooh.order_number)
					AND  ooh.sold_from_org_id = rctla.org_id
                   ) 
 UNION --Ver 1.4
 SELECT ooh.order_number
       ,ooh.header_id
	   ,ool.line_id
	   ,ool.line_number
       ,(ool.line_number||decode(shipment_number,null,null, '.'||shipment_number)||
                         decode(option_number,null,null,'.'||option_number)||
                         decode(component_number,null,null,'.'||component_number)||
                         decode(service_number,null,null,'.'||service_number)
        ) line_nbr_shp_nbr
       ,ool.ordered_item
	   ,ool.ordered_item_id
	   ,ool.inventory_item_id --ver 1.1
	   ,ool.ordered_quantity
	   ,ool.shipped_quantity
	   ,(decode(oty.name,'COUNTER ORDER',ool.ordered_quantity
                        ,'STANDARD ORDER',ool.shipped_quantity
               ) - 
         NVL((SELECT sum(ool2.ordered_quantity)
              FROM oe_order_lines_all ool2
              WHERE LEVEL > 1
              START WITH ool2.line_id = ool.line_id
              CONNECT BY PRIOR ool2.line_id = ool2.reference_line_id
              GROUP BY ool2.reference_line_id
            ),0)
        ) rma_quantity
	   ,ool.unit_list_price
	   ,ool.unit_selling_price
	   ,(ool.ordered_quantity * ool.unit_selling_price) Line_price
       ,rct.trx_number invoice_number --added for ver 1.2
       ,ooh.org_id
	   ,ool.ship_from_org_id ol_ship_from_org_id
    FROM  oe_order_headers_all ooh
         ,oe_order_lines_all ool
         ,oe_transaction_types_tl oty
         ,ra_customer_trx_all rct --ver 1.2
		 ,apps.ra_customer_trx_lines_all rctla
    WHERE ooh.header_id=ool.header_Id
     AND  ooh.order_type_id = oty.transaction_type_id
     AND  oty.language = USERENV('LANG')
     AND  oty.name in ('STANDARD ORDER','COUNTER ORDER')
     AND  ooh.flow_status_code IN ('BOOKED','CLOSED')
     AND  ool.flow_status_code IN ('BOOKED','CLOSED')
	 AND  ooh.sold_from_org_id = rct.org_id --Ver 1.4
	 AND  rct.org_id = rctla.org_id --Ver 1.4
	 AND  rct.customer_trx_id            = rctla.customer_trx_id          -- Added for Ver# 1.3
	 AND  rctla.interface_line_attribute6 = TO_CHAR (ool.line_id)         -- Added for Ver# 1.3
     AND  rctla.interface_line_attribute1 = TO_CHAR (ooh.order_number)    -- Added for Ver# 1.3
	 AND  rct.cust_trx_type_id = 1 --Invoice --Ver 1.4
    -- AND  rct.interface_header_attribute1(+)=to_char(ooh.order_number) --ver 1.2 -- Commented for Ver# 1.3
     --AND  ooh.order_number=nvl(10332000,ooh.order_number)   ---10011646  , 10332000
     --AND  ooh.org_id=nvl(162,ooh.org_id)
);