/**************************************************************************
      *
      * PACKAGE
      * XXCUS_AP_FREIGHT_DATA_FIX
      *
      * DESCRIPTION
      *  To process stucked freight invoices from staging
      *
      *
      * HISTORY
      * =======
      *
      * VERSION DATE        AUTHOR(S)       DESCRIPTION
      * ------- ----------- --------------- ------------------------------------
      * 1.00    07/31/2015   Maharajan S     Initial creation TMS# 20150731-00191 
      *
      *************************************************************************/
update xxcus.XXCUSAP_TMS_INV_STG_TBL
set status = 'NEW_1'
WHERE status = 'NEW'
/
commit
/
update xxcus.XXCUSAP_TMS_INV_STG_TBL
set status = 'NEW'
WHERE status = 'NEW_1'
and batch_no = 'TMS_31-JUL-2015-1712'
/
commit
/