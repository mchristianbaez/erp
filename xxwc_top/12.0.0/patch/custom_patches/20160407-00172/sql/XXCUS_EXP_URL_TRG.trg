DROP TRIGGER APPS.XXCUS_EXP_URL_TRG;

CREATE OR REPLACE TRIGGER APPS.XXCUS_EXP_URL_TRG
AFTER  INSERT  ON FND_DOCUMENTS_TL
REFERENCING
 NEW AS NEW
 OLD AS OLD
FOR EACH ROW
WHEN (
substr(NEW.FILE_NAME,1,7) = 'http://'
      )
DECLARE
  /**********************************************************************************************
   File Name: APPLSYS.FND_DOCUMENTS_TL
  
   PROGRAM TYPE: TRIGGER
  
   PURPOSE: Trigger to submit the URL load notifications
  
   HISTORY
   =============================================================================
          Last Update Date : 08/28/2009
   =============================================================================
   =============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------
   1.0     28-AUG-2009   Jason Sloan     Created this package.
   2.0     14-OCT-2009   Jason Sloan     Changed email formatting as requested.
   3.0     15-MAR-2011   Murali Krishnan Extended to R12 upgrade
                 References 3 Tables 
                 xxcus.xxcushr_per_people_curr_tbl, xxcus.xxcus_per_assign_curr
              XXCUS.XXCUSIE_CB_DELEGATE_TBL and lookup code
              HDS_IEXP_LOB_FOR_REC_NOTIF
   3.1      28-Feb-2012   Kathy Poling   changed the mail host    
   3.2      28-Feb-2012   Luong Vu       changed sendmailjpkg to use 
                                         xxcus_misc_pkg.html_email      
   3.3      04/08/2016 Balaguru Seshadri  TMS 20160407-00172 / ESMS 322629, EBSPRD migration related fixes FOR GSC objects                                          
  ***********************************************************************************************/

  l_message     VARCHAR2(240);
  l_error_code  NUMBER := 0;
  l_email_mesg  VARCHAR2(1000);
  l_email_subj  VARCHAR2(100);
  l_status      NUMBER;
  l_system_code VARCHAR2(50);
  v_name        VARCHAR2(10);

  --Submission variables
  l_sec VARCHAR2(150);

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_EXP_URL_TRG';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'hds-HDSFASTap-u1@hdsupply.com';

  pl_count NUMBER := 0;

  --Email Variables
  pl_dflt_email   fnd_user.email_address%TYPE := 'hds-HDSFASTap-u1@hdsupply.com';
  pl_email        fnd_user.email_address%TYPE;
  pl_instance     VARCHAR2(100);
  pl_sender       VARCHAR2(100);
  pl_errorstatus  NUMBER;
  p_host          VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
  l_port          VARCHAR2(10) := '25';
  pl_errormessage VARCHAR2(4000);
  l_subject       VARCHAR2(100);
  l_body          VARCHAR2(250);

  --Variable Store
  l_rephdrid   NUMBER;
  l_invoicenum VARCHAR2(500);
  l_empname    VARCHAR2(500);
  l_empnum     VARCHAR2(500);
  l_empemail   VARCHAR2(500);
  l_supname    VARCHAR2(500);
  l_supnum     VARCHAR2(500);
  l_supemail   VARCHAR2(500);
  l_total      VARCHAR2(60);
  p1_count     NUMBER := -1;
  l_req_id     NUMBER := 0;
  l_cbcount    NUMBER := 0;
  l_empgcc     VARCHAR2(25);
  l_seg1found  VARCHAR2(25) := 'N';

BEGIN

  l_err_callpoint := 'Set database.';
  BEGIN
    SELECT UPPER(NAME) --3.3
      INTO pl_instance
      FROM v$database;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

  l_err_callpoint := 'Set email subject and body.';

  --Turn off if not PRD
  --IF pl_instance = 'EBIZPRD' THEN --3.3
IF pl_instance = 'EBSPRD' THEN --3.3  
    --Valid iExpense Row that was entered
    l_err_callpoint := 'Detect if iExpense Entry.';
    BEGIN
      SELECT COUNT(*)
        INTO p1_count
        FROM applsys.fnd_attached_documents a
       WHERE a.document_id = :new.document_id
         AND a.entity_name = 'OIE_HEADER_ATTACHMENTS'
         AND a.pk1_value IN
             (SELECT y.report_header_id
                FROM ap.ap_expense_report_headers_all y
               WHERE y.invoice_num = a.pk1_value);
    EXCEPTION
      WHEN OTHERS THEN
        RAISE program_error;
    END;
  
    IF nvl(p1_count, 0) > 0 THEN
      --Valid iExpense Row detected proceeding with email send
      l_err_callpoint := 'Valid iExpense Row detected.';
    
      BEGIN
        SELECT apexp.report_header_id
              ,apexp.invoice_num
              ,emp.emp_name
              ,emp.emp_num
              ,gc.segment1
              ,emp_cre.emp_email
              ,(SELECT 'Y'
                  FROM fnd_lookup_values lv
                 WHERE lv.lookup_type = 'HDS_IEXP_LOB_FOR_REC_NOTIF'
                   AND enabled_flag = 'Y'
                   AND (end_date_active IS NULL OR
                       end_date_active >= trunc(SYSDATE))
                   AND lv.lookup_code = gc.segment1) gcfound
              ,sup.sup_name
              ,sup.sup_num
              ,nvl(sup.sup_email, pl_dflt_email)
              ,ltrim(to_char((nvl(apexp.amt_due_ccard_company, 0) +
                             nvl(apexp.amt_due_employee, 0)),
                             '$99999999999.99'), ' ') total
          INTO l_rephdrid
              ,l_invoicenum
              ,l_empname
              ,l_empnum
              ,l_empgcc
              ,l_empemail
              ,l_seg1found
              ,l_supname
              ,l_supnum
              ,l_supemail
              ,l_total
          FROM applsys.fnd_attached_documents a
              ,ap.ap_expense_report_headers_all apexp
              ,(SELECT t.person_id
                      ,u.user_id
                      ,t.employee_number emp_num
                      ,t.email_address emp_email
                      ,t.first_name || ' ' || t.last_name emp_name
                  FROM xxcus.xxcushr_per_people_curr_tbl t
                      ,applsys.fnd_user                  u
                 WHERE t.person_id = u.employee_id) emp
              ,(SELECT t.person_id
                      ,u.user_id
                      ,t.employee_number emp_num
                      ,t.email_address emp_email
                      ,t.first_name || ' ' || t.last_name emp_name
                  FROM xxcus.xxcushr_per_people_curr_tbl t
                      ,applsys.fnd_user                  u
                 WHERE t.person_id = u.employee_id) emp_cre
              ,(SELECT person_id
                      ,employee_number sup_num
                      ,email_address sup_email
                      ,first_name || ' ' || last_name sup_name
                  FROM xxcus.xxcushr_per_people_curr_tbl t) sup
              ,(SELECT p.person_id
                      ,gcc.segment1
                  FROM xxcus.xxcushr_per_people_curr_tbl p
                      ,xxcus.xxcushr_per_assign_curr_tbl a
                      ,gl.gl_code_combinations           gcc
                 WHERE p.person_id = a.person_id
                   AND a.default_code_comb_id = gcc.code_combination_id(+)) gc
         WHERE apexp.employee_id = emp.person_id
           AND apexp.created_by = emp_cre.user_id
           AND apexp.override_approver_id = sup.person_id
           AND gc.person_id = emp.person_id
           AND a.document_id = :new.document_id
           AND entity_name = 'OIE_HEADER_ATTACHMENTS'
           AND a.pk1_value IN
               (SELECT report_header_id
                  FROM ap.ap_expense_report_headers_all)
           AND a.pk1_value = apexp.report_header_id;
      EXCEPTION
        WHEN OTHERS THEN
          RAISE program_error;
      END;
    
      IF nvl(l_seg1found, 'N') = 'Y' THEN
      
        --Database Sender
        pl_sender := 'Oracle.Applications_' || pl_instance ||
                     '@hdsupply.com';
      
        l_subject := 'FYI - Expense Report ' || l_invoicenum ||
                     ' Cover Page/Receipts viewable in iExpense';
      
        l_body := 'The cover page and receipts for ' || l_empname ||
                  ' have been received and are now viewable in iExpense for Expense Report ' ||
                  l_invoicenum || ', with the amount of ' || l_total || '.' ||
                  ' THIS IS A SEND ONLY EMAIL - DO NOT REPLY ';
      
        l_err_callpoint := 'Send user email.';
        --Setup Email structure
        IF l_empemail IS NOT NULL THEN
        
          l_cbcount := 0;
          SELECT COUNT(*)
            INTO l_cbcount
            FROM xxcus.xxcusie_cb_delegate_tbl t
           WHERE stg_employee_number = l_empnum;
        
        
        
          IF l_cbcount > 1 THEN
            l_cbcount := 0;
          ELSE
            xxcus_misc_pkg.html_email(p_to => l_empemail,
                                      p_from => pl_sender,
                                      p_subject => l_subject, p_text => ' ',
                                      p_html => l_body,
                                      p_smtp_hostname => p_host,
                                      p_smtp_portnum => l_port);
          
          
          END IF;
        END IF;
      
        l_err_callpoint := 'Send Supervisor email.';
        --Setup Email structure
        IF l_supemail IS NOT NULL THEN
          xxcus_misc_pkg.html_email(p_to => l_supemail, p_from => pl_sender,
                                    p_subject => l_subject, p_text => ' ',
                                    p_html => l_body,
                                    p_smtp_hostname => p_host,
                                    p_smtp_portnum => l_port);
        
        END IF;
      END IF;
    END IF;
  END IF;

EXCEPTION
  WHEN program_error THEN
    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                         p_calling => l_err_callpoint,
                                         p_request_id => l_req_id,
                                         p_ora_error_msg => SQLERRM,
                                         p_error_desc => 'Error running iExpense URL email trigger with program_error exception',
                                         p_distribution_list => l_distro_list,
                                         p_module => 'AP');
  WHEN OTHERS THEN
    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                         p_calling => l_err_callpoint,
                                         p_request_id => l_req_id,
                                         p_ora_error_msg => SQLERRM,
                                         p_error_desc => 'Error running iExpense URL email trigger with OTHERS Exception',
                                         p_distribution_list => l_distro_list,
                                         p_module => 'AP');
  
END xxcus_exp_url_trg;
/
show errors
/