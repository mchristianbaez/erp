
  CREATE OR REPLACE TRIGGER "APPS"."XXCUS_FND_LOOKUP_VALUES_TRG" 
  AFTER INSERT OR UPDATE OR DELETE ON applsys.fnd_lookup_values
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW

 WHEN (NEW.lookup_type = 'XXCUS_INTERFACE_ARCHIVE_DAYS'
      or OLD.lookup_type = 'XXCUS_INTERFACE_ARCHIVE_DAYS') DECLARE
  /**********************************************************************************************
   File Name: APPLSYS.FND_LOOKUP_VALUES

   PROGRAM TYPE: TRIGGER

   PURPOSE: Trigger to send notifications to Developers new entry for a directory
            to be deleted

   HISTORY
   =============================================================================
          Last Update Date : 04/11/2014
   =============================================================================
   =============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------
   1.0     11-APR-2014   Kathy Poling     Created
   1.1    04/08/2016   Balaguru Seshadri TMS 20160407-00172 / ESMS 322629, EBSPRD migration related fixes FOR GSC objects  
  ***********************************************************************************************/

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_FND_LOOKUP_VALUES_TRG';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'hdsoracledevelopers@hdsupply.com';

  --Email Variables
  --pl_email   fnd_user.email_address%TYPE := 'kathy.poling@hdsupply.com'; --'hdsoracledevelopers@hdsupply.com';
  pl_instance VARCHAR2(100);
  pl_sender   VARCHAR2(100);
  p_host      VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
  l_port      VARCHAR2(10) := '25';
  l_subject   VARCHAR2(150);
  l_body      VARCHAR2(2000);

  --Variable Store
  l_user_name VARCHAR2(100);
  l_req_id    NUMBER := 0;

BEGIN

  l_err_callpoint := 'Set database.';
  BEGIN
    SELECT upper(NAME) INTO pl_instance FROM v$database; --1.1
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

  l_err_callpoint := 'Get user name.';
  BEGIN
    SELECT user_name
      INTO l_user_name
      FROM fnd_user
     WHERE user_id = fnd_global.user_id();
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

  --Turn off if not PRD
    --IF pl_instance = 'EBIZPRD' THEN --1.1
 IF pl_instance = 'EBSPRD' THEN --1.1    

  IF inserting
  THEN
    l_err_callpoint := 'Set email subject and body for insert.';

    --Database Sender
    pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';

    l_subject := 'FYI - A New Record inserted to Lookup Type XXCUS_INTERFACE_ARCHIVE_DAYS';

    l_body := '<BR>RECORD INSERTED:' ||
              '<BR>Lookup Code = ' || :new.lookup_code ||
              '<BR>Directory Path = ' || :new.description ||
              '<BR>Retention Days = ' || :new.tag ||
              '<BR>File Name Prefix = ' || :new.attribute1 ||
              '<BR>Created by user ' || l_user_name;

  END IF;

  IF updating
  THEN

      l_err_callpoint := 'Set email subject and body for update.';

      --Database Sender
      pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';

      l_subject := 'FYI - Updated Record in Lookup Type XXCUS_INTERFACE_ARCHIVE_DAYS';

      l_body := '<BR>RECORD UPDATED:' ||
                '<BR>Lookup Code = ' || :new.lookup_code ||
                '<BR>Directory Path:  OLD = ' || :old.description || ' - and NEW = ' || :new.description ||
                '<BR>Retention Days OLD = ' || :old.tag || ' - and NEW = ' || :new.tag ||
                '<BR>File Name Prefix OLD = ' ||:old.attribute1 || ' - and NEW = ' || :new.attribute1 ||
                '<BR>Created by user ' || l_user_name;

  END IF;

  IF deleting
  THEN

    l_err_callpoint := 'Set email subject and body for delete.';

    --Database Sender
    pl_sender := 'Oracle.Applications_' || pl_instance || '@hdsupply.com';

    l_subject := 'FYI - Deleted Record in Lookup Type XXCUS_INTERFACE_ARCHIVE_DAYS';

    l_body := '<BR>RECORD DELETED:' ||
              '<BR>Lookup Code = ' || :old.lookup_code ||
              '<BR>Directory Path = ' || :old.description ||
              '<BR>Retention Days = ' || :old.tag ||
              '<BR>File Name Prefix = ' || :old.attribute1 ||
              '<BR>Created by user ' || l_user_name;

  END IF;

  l_err_callpoint := 'Send user email.';

  --Setup Email structure

  xxcus_misc_pkg.html_email(p_to            => l_distro_list
                           ,p_from          => pl_sender
                           ,p_subject       => l_subject
                           ,p_text          => ' '
                           ,p_html          => l_body
                           ,p_smtp_hostname => p_host
                           ,p_smtp_portnum  => l_port);

       END IF;

EXCEPTION
  WHEN OTHERS THEN
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(SQLERRM ||
                                                                       regexp_replace(l_err_callpoint
                                                                                     ,'[[:cntrl:]]'
                                                                                     ,NULL)
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => 'Error sending email trigger with OTHERS Exception'
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'APPLSYS');

END xxcus_fnd_lookup_values_trg;
/
show errors
/
ALTER TRIGGER "APPS"."XXCUS_FND_LOOKUP_VALUES_TRG" ENABLE;
