/*
 TMS: 20160407-00172 
 Date: 04/08/2016
 Notes: Script to  update table xxcus.xxcus_monitor_cp_instance, field db_name from EBIZPRD to EBSPRD. 
*/
set serveroutput on size 1000000
declare
 --
 n_loc number;
 --
begin --Main Processing...
   --
   n_loc :=101;
   --
   begin 
    --
    update xxcus.xxcus_monitor_cp_instance set db_name ='EBSPRD';
    --
    n_loc :=102;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=103;
     --
     dbms_output.put_line('@ Failed to update table xxcus.xxcus_monitor_cp_instance, @'||n_loc||', message ='||sqlerrm);
     --   
   end;
   --
exception
   when others then
      dbms_output.put_line ('TMS: 20160407-00172 , Outer block errors =' || sqlerrm);
end;
/