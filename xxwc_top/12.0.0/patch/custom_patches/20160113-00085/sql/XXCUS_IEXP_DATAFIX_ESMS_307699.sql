/*
 TMS: 20160113-00085
 ESMS: 307699
 Scope: Fix for i-Expense corrupted data.
 Date: 01/13/2016
*/
set serveroutput on size 1000000;
declare
 --

 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     print_log('Begin i-Expense data fix');
    -- 
      begin 
       update ap.ap_expense_report_lines_all aer
         set aer.org_id = 163
         where 1 =1
               and aer.credit_card_trx_id in (2446989,2496677,2502582,2516766,2518530); --Only specific line id's
        --
        print_log('');          
        print_log('Total rows updated: '||sql%rowcount);
        print_log('');          
        --
        if (sql%rowcount >0) then
         --
         commit;
         --              
        end if;
        --
      exception
       when others then
        rollback to start_here;
        print_log('Error in updating ap.ap_expense_report_lines_all, msg ='||sqlerrm);
      end;
    --
     print_log('End of i-Expense data fix');
     print_log('');
     --    
exception
 when others then
  rollback;
  print_log('Outer block, message ='||sqlerrm);
end;
/