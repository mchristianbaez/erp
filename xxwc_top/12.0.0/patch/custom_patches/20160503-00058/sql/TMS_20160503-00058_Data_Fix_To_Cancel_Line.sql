/*************************************************************************
  $Header TMS_20160503-00058_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160503-00058  Data Fix script for I672142

  PURPOSE: Data Fix script for I672142

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-MAY-2016  Raghav Velichetti         TMS#20160503-00058 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160503-00058    , Before Update');

/*update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED'
where line_id in (67658341,67915048)
and open_Flag='N'
and cancelled_flag='Y'; */

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
cancelled_flag='Y',
open_Flag='N'
where line_id =67658341;

   DBMS_OUTPUT.put_line (
         'TMS: 20160503-00058  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160503-00058    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160503-00058 , Errors : ' || SQLERRM);
END;
/