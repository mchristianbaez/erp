DROP MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__VNDR_QUOTE_H_MV;
CREATE MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__VNDR_QUOTE_H_MV (VENDOR_QUOTE_COST,ORDER_LINE_ID)
TABLESPACE APPS_TS_TX_DATA
PCTUSED    0
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT /* sub-MV used by APPS.XXWC_OBIEE_CUST_TRX_HIST_MV
       -- replacement for original OBI code calls to
                   FUNCTION apps.xxwc_mv_routines_pkg.get_vendor_quote_cost
       --
       -- NOTE: this sub-MV has date filter matching the main query
       --       KEEP in SYNC
       --
       -- This MV will be refreshed before the main query in
       -- XXWC_OBIEE_CUST_TRX_INCR_MV
       --
       -- 20141018 AGH Changed synonyms to direct owner.table references to avoid
             ORA-30372: fine grain access policy conflicts with materialized view
       --3/7/2016  Changed logic for vendor_quote_cost for TMS 20160224-00057
	   --6/14/2016 Changed for TMS-20150122-00027 Need to add a condition to the Price Type attribute
       */
      vendor_quote_cost, order_line_id
  FROM (SELECT xxcpl.special_cost AS vendor_quote_cost-- TO_NUMBER (qll.attribute4) --changed for TMS 20160224-00057
               ,ril.interface_line_attribute6 AS order_line_id,
               ROW_NUMBER ()
               OVER (PARTITION BY ril.interface_line_attribute6
                     ORDER BY TO_NUMBER (qll.attribute4) DESC NULLS LAST)
                  AS rn
          FROM qp.qp_qualifiers qq,
               apps.qp_list_headers qlh,
               qp.qp_list_lines qll,
               xxwc.xxwc_om_contract_pricing_lines xxcpl,
               xxwc.xxwc_om_contract_pricing_hdr xxcph,
               apps.mtl_system_items_b_kfv msi,
               ar.ra_customer_trx_lines_all ril,
               apps.po_vendors pov,
               apps.oe_price_adjustments opa, --Added for TMS-20150122-00027
               inv.mtl_parameters mp
         WHERE     qq.qualifier_context = 'CUSTOMER'
               AND qq.comparison_operator_code = '='
               AND qq.active_flag = 'Y'
               AND (   (    qlh.attribute10 = ('Contract Pricing')
                        AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE11'
                        AND (qq.qualifier_attr_value =
                                TO_CHAR (ril.ship_to_site_use_id)))
                    OR (    qlh.attribute10 = ('Contract Pricing')
                        AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE32'
                        AND (qq.qualifier_attr_value =
                                TO_CHAR (ril.ship_to_customer_id)))
                    OR (    qlh.attribute10 = ('CSP-NATIONAL')
                        AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE32'
                        AND (qq.qualifier_attr_value =
                                TO_CHAR (ril.ship_to_customer_id))))
               AND qlh.list_header_id = qq.list_header_id
               AND qlh.active_flag = 'Y'
               AND xxcph.agreement_id = TO_CHAR (qlh.attribute14)
               AND xxcpl.agreement_id = xxcph.agreement_id
               AND xxcpl.agreement_type = 'VQN'
               AND qlh.list_header_id = qll.list_header_id
               AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
               AND msi.inventory_item_id = ril.inventory_item_id
               AND xxcpl.product_value = msi.segment1
               AND msi.organization_id = ril.warehouse_id
               AND pov.vendor_id = xxcpl.vendor_id
               AND mp.organization_id = msi.organization_id
               AND ril.interface_line_attribute6 IS NOT NULL
               AND ril.interface_line_attribute6 = TO_CHAR(opa.line_id) --Added for TMS-20150122-00027
               AND opa.list_line_type_code       = 'DIS' --Added for TMS-20150122-00027
               AND opa.list_header_id            =  qlh.list_header_id --Added for TMS-20150122-00027
               AND TRUNC (ril.creation_date) BETWEEN NVL (
                                                        qll.start_date_active,
                                                        TRUNC (
                                                             ril.creation_date
                                                           - 1))
                                                 AND NVL (
                                                        qll.end_date_active,
                                                        TRUNC (
                                                             ril.creation_date
                                                           + 1)))
 WHERE rn = 1;


COMMENT ON MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__VNDR_QUOTE_H_MV IS 'snapshot table for snapshot APPS.XXWC_OBIEE_CT__VNDR_QUOTE_H_MV';
