/*
 TMS: 20150812-00160  
 Date: 08/12/2015
 Notes: To change CSP branch for agreement 12700
*/

SET SERVEROUTPUT ON SIZE 100000;

update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
,open_flag='N'
,flow_status_code='CLOSED'
--,INVOICED_QUANTITY=1
where line_id =33342206
and headeR_id=19908595;

commit;

/


