CREATE OR REPLACE PACKAGE BODY APPS.XXCUSAP_DCTM_INVOICE_INT_PKG IS

  /**************************************************************************
  File Name: XXCUSAP_DCTM_INVOICE_INT_PKG

  PROGRAM TYPE: SQL Script

  PURPOSE:      package Specification is used to import invoices loaded into
                the interface by Documentum

  HISTORY
  =============================================================================
         Last Update Date : 01/01/2012
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     27-Jul-2011   Kathy Poling       Creation this package
  1.2     17-Sep-2014   Kathy Poling       iProcurement changes RFC 42113 adding
                                           source and po for GSC
  1.3     22-Dec-2015   Balaguru Seshadri Fix iProcurement PO Line numbers on invoice import from default of "1" to actual line numbers
                                          TMS 20151111-00140 / ESMS 296372                                           
  =============================================================================
  *****************************************************************************/

  PROCEDURE import_inv(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) IS

    l_req_id            NUMBER := fnd_global.conc_request_id;
    l_set_req_status    BOOLEAN;
    l_req_phase         VARCHAR2(80);
    l_req_status        VARCHAR2(80);
    l_del_phase         VARCHAR2(30);
    l_del_status        VARCHAR2(30);
    l_message           VARCHAR2(255);
    l_interval          NUMBER := 10; -- In seconds
    l_max_time          NUMBER := 15000; -- In seconds
    l_completion_status VARCHAR2(30) := 'NORMAL';
    l_file_dir          VARCHAR2(100);
    l_instance          VARCHAR2(8);
    l_shortname         VARCHAR2(30);
    l_count             NUMBER;

    l_can_submit_request BOOLEAN := TRUE;
    l_globalset          VARCHAR2(100);
    l_err_msg            CLOB;
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_err_callfrom       VARCHAR2(75) DEFAULT 'XXCUSAP_DCTM_INVOICE_INT_PKG';
    --l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_procedure_name VARCHAR2(75) := 'IMPORT_INV';
    l_source         VARCHAR2(100);

  BEGIN
    l_sec := 'Starting the invoice import; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    SELECT COUNT(*)
      INTO l_count
      FROM xxcus.xxcusap_dctm_interface_tbl
     WHERE status = 'NEW';

    IF l_count > 0
    THEN

      FOR c_import IN (SELECT group_id, org_id
                         FROM xxcus.xxcusap_dctm_interface_tbl
                        WHERE status = 'NEW')
      LOOP

        l_source := NULL;

        UPDATE xxcus.xxcusap_dctm_interface_tbl
           SET request_id = g_request_id
         WHERE group_id = c_import.group_id
           AND org_id = c_import.org_id;

        BEGIN
          --Version 1.2
          SELECT DISTINCT SOURCE
            INTO l_source
            FROM xxcus.xxcusap_dctm_inv_header_tbl
           WHERE group_id = c_import.group_id
             AND org_id = c_import.org_id;

        EXCEPTION
          WHEN no_data_found THEN
            l_source := NULL;
        END;

        IF c_import.org_id = 163
        THEN

          load_inv(p_errbuf, p_retcode, c_import.group_id, l_source);
        ELSE
          wc_load_inv(p_errbuf, p_retcode, c_import.group_id);
        END IF;

        l_sec := 'call load_inv with group id:  ' || c_import.group_id;
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);

      END LOOP;
    END IF;
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;

      UPDATE xxcus.xxcusap_dctm_interface_tbl
         SET status      = 'ERROR'
            ,update_date = SYSDATE
            ,updated_by  = fnd_global.user_id
       WHERE request_id = g_request_id;
      COMMIT;

      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom || '.' ||
                                                                  l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');

    WHEN OTHERS THEN

      UPDATE xxcus.xxcusap_dctm_interface_tbl
         SET status      = 'ERROR'
            ,update_date = SYSDATE
            ,updated_by  = fnd_global.user_id
       WHERE request_id = l_req_id;
      COMMIT;

      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom || '.' ||
                                                                  l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');

  END import_inv;

  /**************************************************************************
  File Name: XXCUSAP_DCTM_INVOICE_INT_PKG

  PROGRAM TYPE: SQL Script

  PURPOSE:      package Specification is used to load invoices into
                the interface that Documentum stagged

  HISTORY
  =============================================================================
         Last Update Date : 04/13/2012
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     01-Jan-2012   Kathy Poling       Creation this package
  1.1     09-Dec-2013   Kathy Poling       DCTM moving to APEX. Adding gl_project
                                           for the code combination
                                           SR 232394
  1.2     17-Sep-2014   Kathy Poling       iProcurement changes RFC 42113 adding
                                           source and po for GSC
  1.3     22-Dec-2015   Balaguru Seshadri Fix iProcurement PO Line numbers on invoice import from default of "1" to actual line numbers
                                          TMS 20151111-00140 / ESMS 296372
  =============================================================================
  *****************************************************************************/

  PROCEDURE load_inv(p_errbuf   OUT VARCHAR2
                    ,p_retcode  OUT NUMBER
                    ,p_group_id IN VARCHAR2
                    ,p_source   IN VARCHAR2) IS

    l_req_id            NUMBER := fnd_global.conc_request_id;
    l_set_req_status    BOOLEAN;
    l_req_phase         VARCHAR2(80);
    l_req_status        VARCHAR2(80);
    l_del_phase         VARCHAR2(30);
    l_del_status        VARCHAR2(30);
    l_message           VARCHAR2(255);
    l_interval          NUMBER := 10; -- In seconds
    l_max_time          NUMBER := 15000; -- In seconds
    l_completion_status VARCHAR2(30) := 'NORMAL';
    l_file_dir          VARCHAR2(100);
    l_instance          VARCHAR2(8);
    l_shortname         VARCHAR2(30);
    l_count             NUMBER;

    l_can_submit_request BOOLEAN := TRUE;
    l_globalset          VARCHAR2(100);
    l_err_msg            CLOB;
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_err_callfrom       VARCHAR2(75) DEFAULT 'XXCUSAP_DCTM_INT_PKG.LOAD_INV';
    --l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_procedure_name VARCHAR2(75) := 'LOAD_INV';
    l_org            hr_all_organization_units.organization_id%TYPE;
    l_org_name CONSTANT hr_all_organization_units.name%TYPE := 'HD Supply Corp USD - Org'; --163 HD Supply US GSC

  BEGIN
    BEGIN
      --  Setup parameters for running FND JOBS!
      l_can_submit_request := xxcus_misc_pkg.set_responsibility('HDSAPINTERFACE'
                                                               ,'HDS Payables Manager (No Suppliers) - US GSC');
      IF l_can_submit_request
      THEN
        l_globalset := 'Global Variables are set.';

      ELSE

        l_globalset := 'Global Variables are not set.';
        l_sec       := 'Global Variables are not set for the Responsibility of XXCUS_CON and the User.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        RAISE program_error;
      END IF;
    END;

    SELECT organization_id
      INTO l_org
      FROM hr_all_organization_units
     WHERE NAME = l_org_name;
    BEGIN
      FOR c_inl_hdr IN (SELECT invoice_id
                              ,invoice_num
                              ,invoice_type_lookup_code
                              ,nvl(invoice_date, trunc(SYSDATE)) invoice_date
                              ,vendor_num
                              ,vendor_name
                              ,vendor_site_id
                              ,invoice_amount
                              ,nvl(invoice_currency_code, 'USD') invoice_currency_code
                              ,description
                              ,org_id
                              ,SOURCE
                              ,group_id
                              ,(CASE
                                 WHEN attribute1 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute2 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute3 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute4 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute5 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute6 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute7 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute8 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute9 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute10 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute11 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute12 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute13 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute14 IS NOT NULL THEN
                                  org_id
                                 WHEN attribute15 IS NOT NULL THEN
                                  org_id
                                 ELSE
                                  NULL
                               END) attribute_category
                              ,attribute1
                              ,attribute2
                              ,attribute3
                              ,attribute4
                              ,attribute5
                              ,attribute6
                              ,attribute7
                              ,attribute8
                              ,attribute9
                              ,attribute10
                              ,attribute11
                              ,attribute12
                              ,attribute13
                              ,attribute14
                              ,attribute15
                              ,po_number     --Version 1.2
                          FROM xxcus.xxcusap_dctm_inv_header_tbl
                         WHERE group_id = p_group_id)
      LOOP

        INSERT INTO ap_invoices_interface
          (invoice_id
          ,invoice_num
          ,invoice_type_lookup_code
          ,invoice_date
          ,vendor_num
          ,vendor_name
          ,vendor_site_id
          ,invoice_amount
          ,invoice_currency_code
          ,description
          ,org_id
          ,SOURCE
          ,group_id
          ,last_updated_by
          ,last_update_date
          ,last_update_login
          ,creation_date
          ,created_by
          ,attribute_category
          ,attribute1
          ,attribute2
          ,attribute3
          ,attribute4
          ,attribute5
          ,attribute6
          ,attribute7
          ,attribute8
          ,attribute9
          ,attribute10
          ,attribute11
          ,attribute12
          ,attribute13
          ,attribute14
          ,attribute15
          ,po_number     --Version 1.2
          )
        VALUES
          (c_inl_hdr.invoice_id
          ,c_inl_hdr.invoice_num
          ,c_inl_hdr.invoice_type_lookup_code
          ,c_inl_hdr.invoice_date
          ,c_inl_hdr.vendor_num
          ,c_inl_hdr.vendor_name
          ,c_inl_hdr.vendor_site_id
          ,c_inl_hdr.invoice_amount
          ,c_inl_hdr.invoice_currency_code
          ,c_inl_hdr.description
          ,c_inl_hdr.org_id
          ,c_inl_hdr.source
          ,c_inl_hdr.group_id
          ,g_user_id
          ,SYSDATE
          ,g_login_id
          ,SYSDATE
          ,g_user_id
          ,c_inl_hdr.attribute_category
          ,c_inl_hdr.attribute1
          ,c_inl_hdr.attribute2
          ,c_inl_hdr.attribute3
          ,c_inl_hdr.attribute4
          ,c_inl_hdr.attribute5
          ,c_inl_hdr.attribute6
          ,c_inl_hdr.attribute7
          ,c_inl_hdr.attribute8
          ,c_inl_hdr.attribute9
          ,c_inl_hdr.attribute10
          ,c_inl_hdr.attribute11
          ,c_inl_hdr.attribute12
          ,c_inl_hdr.attribute13
          ,c_inl_hdr.attribute14
          ,c_inl_hdr.attribute15
          ,c_inl_hdr.po_number   --Version 1.2
          );

      END LOOP;

      l_sec := 'Insert invoices into AP_INVOICES_INTERFACE';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    EXCEPTION
      WHEN OTHERS THEN
        p_retcode := SQLCODE;
        p_errbuf  := substr(SQLERRM, 1, 2000);
        fnd_file.put_line(fnd_file.log
                         ,'Error in ' || l_procedure_name || l_sec);
        fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
        fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
        -- Calling ERROR API
    END;

    BEGIN
      FOR c_inv_line IN (SELECT d.dctm_object_id
                               ,d.invoice_id
                               ,d.invoice_line_id
                               ,d.line_number
                               ,d.line_type_lookup_code
                               ,(CASE
                                  WHEN d.line_type_lookup_code = 'TAX' THEN
                                   'XXCUS_TAX_RATE'
                                  ELSE
                                   NULL
                                END) tax_code
                               ,(CASE
                                  WHEN d.line_type_lookup_code = 'TAX' THEN
                                   'N'
                                  ELSE
                                   NULL
                                END) tax_recoverable_flag
                               ,d.amount
                               ,d.description
                               ,d.fru
                               ,(CASE
                                  WHEN h.po_number
                                    IS NULL THEN
                               l.entrp_entity || '.' || l.entrp_loc || '.' ||
                                l.entrp_cc || '.' || d.gl_account || '.' ||
                                nvl(d.gl_project, '00000') || --Version 1.1
                                '.00000.00000'
                                else
                                  null
                                  end)  dist_code_concatenated   --Version 1.2 added case statement
                               ,(CASE
                                  WHEN d.attribute1 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute2 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute3 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute4 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute5 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute6 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute7 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute8 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute9 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute10 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute11 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute12 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute13 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute14 IS NOT NULL THEN
                                   org_id
                                  WHEN d.attribute15 IS NOT NULL THEN
                                   org_id
                                  ELSE
                                   NULL
                                END) attribute_category
                               ,d.attribute1
                               ,d.attribute2
                               ,d.attribute3
                               ,d.attribute4
                               ,d.attribute5
                               ,d.attribute6
                               ,d.attribute7
                               ,d.attribute8
                               ,d.attribute9
                               ,d.attribute10
                               ,d.attribute11
                               ,d.attribute12
                               ,d.attribute13
                               ,d.attribute14
                               ,d.attribute15
                               ,l.entrp_entity
                               ,l.entrp_loc
                               ,l.entrp_cc
                               ,h.org_id
                               ,h.po_number          --Version 1.2
                               /* --Ver 1.3
                               ,(CASE
                                  WHEN h.po_number
                                    IS NOT NULL THEN
                                    d.line_number
                                ELSE
                                   NULL
                                END) po_line_number   --Version 1.2
                             */ -- Ver 1.3
                               -- BEGIN Ver 1.3
                               ,(
                                 case when h.po_number is not null  
                                          then
                                                (
                                                     select min(a.line_num)
                                                     from po_lines_all a
                                                              ,po_headers_all b
                                                     where 1 =1
                                                           and  b.segment1 =h.po_number
                                                           and b.po_header_id =a.po_header_id       
                                                           and a.org_id =l_org 
                                                           and b.org_id =a.org_id
                                                ) 
                                           else null --when h.po_number is  null ...
                                 end
                               ) po_line_number 
                            --END Version 1.3                              
                           FROM xxcus.xxcusap_dctm_inv_line_tbl   d
                               ,xxcus.xxcusap_dctm_inv_header_tbl h
                               ,apps.xxcus_location_code_vw       l
                          WHERE d.dctm_object_id = h.dctm_object_id
                            AND h.group_id = p_group_id
                            AND d.invoice_id = h.invoice_id
                            AND d.fru = l.fru(+))
      LOOP

        INSERT INTO ap_invoice_lines_interface
          (invoice_id
          ,invoice_line_id
          ,line_number
          ,line_type_lookup_code
          ,amount
          ,description
          ,dist_code_concatenated
          ,last_updated_by
          ,last_update_date
          ,last_update_login
          ,creation_date
          ,created_by
          ,attribute_category
          ,attribute1
          ,attribute2
          ,attribute3
          ,attribute4
          ,attribute5
          ,attribute6
          ,attribute7
          ,attribute8
          ,attribute9
          ,attribute10
          ,attribute11
          ,attribute12
          ,attribute13
          ,attribute14
          ,attribute15
          ,org_id
          ,tax_code
          ,tax_recoverable_flag
          ,po_number     --Version 1.2
          ,PO_LINE_NUMBER  --Version 1.2
          )
        VALUES
          (c_inv_line.invoice_id
          ,c_inv_line.invoice_line_id
          ,c_inv_line.line_number
          ,c_inv_line.line_type_lookup_code
          ,c_inv_line.amount
          ,c_inv_line.description
          ,c_inv_line.dist_code_concatenated
          ,g_user_id
          ,SYSDATE
          ,g_login_id
          ,SYSDATE
          ,g_user_id
          ,c_inv_line.attribute_category
          ,c_inv_line.attribute1
          ,c_inv_line.attribute2
          ,c_inv_line.attribute3
          ,c_inv_line.attribute4
          ,c_inv_line.attribute5
          ,c_inv_line.attribute6
          ,c_inv_line.attribute7
          ,c_inv_line.attribute8
          ,c_inv_line.attribute9
          ,c_inv_line.attribute10
          ,c_inv_line.attribute11
          ,c_inv_line.attribute12
          ,c_inv_line.attribute13
          ,c_inv_line.attribute14
          ,c_inv_line.attribute15
          ,c_inv_line.org_id
          ,c_inv_line.tax_code
          ,c_inv_line.tax_recoverable_flag
          ,c_inv_line.po_number     --Version 1.2
          ,c_inv_line.po_line_number --Version 1.2
          );

      END LOOP;
      l_sec := 'Insert invoice lines into AP_INVOICE_LINES_INTERFACE';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    EXCEPTION
      WHEN OTHERS THEN
        p_retcode := SQLCODE;
        p_errbuf  := substr(SQLERRM, 1, 2000);
        fnd_file.put_line(fnd_file.log
                         ,'Error in ' || l_procedure_name || l_sec);
        fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
        fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);

    END;

    BEGIN
      --
      -- Submit the invoice import
      --
      l_sec := 'Submitting Payables Invoice Import';
      fnd_file.put_line(fnd_file.log, l_sec);
      l_req_id := fnd_request.submit_request('SQLAP'
                                            ,'APXIIMPT'
                                            ,NULL
                                            ,''
                                            ,FALSE
                                            ,l_org --Operating Unit HD Supply US GSC
                                            ,p_source --Source      --Version 1.2
                                            ,p_group_id --Group
                                            ,NULL --Batch Name
                                            ,NULL --NULL --Hold Name
                                            ,NULL --Hold Reason
                                            ,NULL --GL Date
                                            ,'Y' --Purge     --Version 1.2
                                            ,'N' --Trace Switch
                                            ,'N' --Debug Switch
                                            ,'N' --Summarize Report
                                            ,1000 --Commit batch size
                                             );
      COMMIT;

      l_sec := 'Updating xxcusap_dctm_interface_tbl to Processing';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      UPDATE xxcus.xxcusap_dctm_interface_tbl
         SET status      = 'PROCESSING'
            ,update_date = SYSDATE
            ,request_id  = l_req_id
            ,updated_by  = fnd_global.user_id
       WHERE group_id = p_group_id;

      UPDATE xxcus.xxcusap_dctm_inv_header_tbl
         SET interfaced_flag  = 'P'
            ,request_id       = l_req_id
            ,last_update_date = SYSDATE
            ,last_updated_by  = fnd_global.user_id
       WHERE group_id = p_group_id;

      UPDATE xxcus.xxcusap_dctm_inv_line_tbl
         SET interfaced_flag  = 'P'
            ,request_id       = l_req_id
            ,last_update_date = SYSDATE
            ,last_updated_by  = fnd_global.user_id
       WHERE group_id = p_group_id;

      -- get the status of the process.
      IF l_req_id != 0
      THEN
        --
        -- Wait for the previous concurrent request to complete
        --
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,l_interval
                                          ,l_max_time
                                          ,l_req_phase
                                          ,l_req_status
                                          ,l_del_phase
                                          ,l_del_status
                                          ,l_message)
        THEN
          l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             l_del_phase || ' DStatus ' || l_del_status ||
                             chr(10) || ' MSG - ' || l_message;
          -- Error Returned
          IF l_del_phase != 'COMPLETE' OR l_del_status != 'NORMAL'
          THEN
            l_sec := 'An error occured in the running the invoice import' ||
                           l_err_msg || '.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            RAISE program_error;

            UPDATE xxcus.xxcusap_dctm_interface_tbl
               SET status      = 'ERROR'
                  ,request_id  = l_req_id
                  ,update_date = SYSDATE
                  ,updated_by  = fnd_global.user_id
             WHERE group_id = p_group_id;

          END IF;

          l_sec := 'Payables Import Completed Request ID:  ' || l_req_id;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          -- Then Success!
          UPDATE xxcus.xxcusap_dctm_interface_tbl
             SET status      = 'COMPLETED'
                ,request_id  = l_req_id
                ,update_date = SYSDATE
                ,updated_by  = fnd_global.user_id
           WHERE group_id = p_group_id;

          UPDATE xxcus.xxcusap_dctm_inv_header_tbl
             SET interfaced_flag  = 'Y'
                ,request_id       = l_req_id
                ,last_update_date = SYSDATE
                ,last_updated_by  = fnd_global.user_id
           WHERE group_id = p_group_id;

          UPDATE xxcus.xxcusap_dctm_inv_line_tbl
             SET interfaced_flag  = 'Y'
                ,request_id       = l_req_id
                ,last_update_date = SYSDATE
                ,last_updated_by  = fnd_global.user_id
           WHERE group_id = p_group_id;
          COMMIT;

          l_sec := 'Updated xxcusap_dctm_interface_tbl to Completed';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

        END IF;
      END IF;
    END;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;

      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom || '.' ||
                                                                  l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
    WHEN OTHERS THEN

      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom || '.' ||
                                                                  l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');

  END load_inv;

  /**************************************************************************
  File Name: XXCUSAP_DCTM_IMAGE_TRG

  PROGRAM TYPE: SQL Script

  PURPOSE:      Attach Image url for TE - Expense Reports images from Documentum

  HISTORY
  =============================================================================
         Last Update Date : 01/Jan/2012
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     01-Jan-2012   Kathy Poling       Creation

  =============================================================================
  *****************************************************************************/
  PROCEDURE exp_dctm_image(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) IS

    l_err_msg          VARCHAR2(3000);
    l_sec              VARCHAR2(255);
    l_message          VARCHAR2(150);
    l_report_header_id ap_expense_report_headers_all.report_header_id%TYPE;
    l_doc_id           NUMBER(35);
    l_attach_doc_id    NUMBER(35);
    g_user_id          NUMBER := fnd_global.user_id;
    g_login_id         NUMBER := fnd_profile.value('LOGIN_ID');
    g_err_callfrom     VARCHAR2(75) DEFAULT 'XXCUSAP_DCTM_INVOICE_INT_PKG';
    g_err_callpoint    VARCHAR2(75) DEFAULT 'START';
    g_distro_list      VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    no_attachment_insert EXCEPTION;

  BEGIN
    FOR c1 IN (SELECT group_id
                     ,invoice_num
                     ,invoice_type
                     ,image_link
                     ,org_id
                     ,status
                 FROM xxcus.xxcusap_dctm_image_tbl
                WHERE status = 'NEW'
                  AND invoice_type = 'TE')
    LOOP

      BEGIN
        SELECT report_header_id
          INTO l_report_header_id
          FROM ap_expense_report_headers_all
         WHERE invoice_num = c1.invoice_num;
      EXCEPTION
        WHEN no_data_found THEN

          l_report_header_id := 0;
      END;

      IF l_report_header_id <> 0
      THEN

        SELECT fnd_documents_s.nextval INTO l_doc_id FROM dual;

        SELECT fnd_attached_documents_s.nextval
          INTO l_attach_doc_id
          FROM dual;

        INSERT INTO fnd_documents
          (document_id
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login
          ,datatype_id
          ,category_id
          ,security_type
          ,publish_flag
          ,usage_type
          ,url)
        VALUES
          (l_doc_id
          ,SYSDATE
          ,g_user_id
          ,SYSDATE
          ,g_user_id
          ,g_login_id
          ,'5'
          ,'1'
          ,'4'
          ,'Y'
          ,'O'
          ,c1.image_link);

        INSERT INTO fnd_attached_documents
          (attached_document_id
          ,document_id
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login
          ,seq_num
          ,entity_name
          ,automatically_added_flag
          ,pk1_value)
        VALUES
          (l_attach_doc_id
          ,l_doc_id
          ,SYSDATE
          ,g_user_id
          ,SYSDATE
          ,g_user_id
          ,g_login_id
          ,'1'
          ,'OIE_HEADER_ATTACHMENTS'
          ,'N'
          ,l_report_header_id);

        INSERT INTO fnd_documents_tl
          (document_id
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login
          ,LANGUAGE
          ,file_name
          ,source_lang)
        VALUES
          (l_doc_id
          ,SYSDATE
          ,g_user_id
          ,SYSDATE
          ,g_user_id
          ,g_login_id
          ,'US'
          ,c1.image_link
          ,'US');

        UPDATE xxcus.xxcusap_dctm_image_tbl
           SET status      = 'ATTACHED'
              ,update_date = SYSDATE
              ,updated_by  = fnd_global.user_id
         WHERE group_id = c1.group_id
           AND invoice_num = c1.invoice_num;

        l_sec := substr('Invoice_num: ' || c1.invoice_num || '-' || SQLERRM
                       ,1
                       ,500);
      END IF;
    END LOOP;

  EXCEPTION
    WHEN no_attachment_insert THEN
      ROLLBACK;
      l_err_msg := l_sec;
      l_err_msg := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      g_err_callpoint := l_message;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running EXP_DCTM_IMAGE with no attachment ERROR'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'AP');

    WHEN OTHERS THEN
      --dbms_output.put_line('Invoice_num: ' || :new.invoice_num || '-' || SQLERRM);

      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_msg := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                   substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      g_err_callpoint := l_message;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running EXP_DCTM_IMAGE with OTHERS Exception'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'AP');

  END exp_dctm_image;

  /**************************************************************************
  File Name: XXCUSAP_DCTM_INVOICE_INT_PKG

  PROGRAM TYPE: SQL Script

  PURPOSE:      package Specification is used to load invoices into
                the interface that Documentum stagged

  HISTORY
  =============================================================================
         Last Update Date : 04/13/2012
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     08-Mar-2012   Kathy Poling       Creation this package

  =============================================================================
  *****************************************************************************/

  PROCEDURE wc_load_inv(p_errbuf   OUT VARCHAR2
                       ,p_retcode  OUT NUMBER
                       ,p_group_id IN VARCHAR2) IS

    l_req_id            NUMBER := fnd_global.conc_request_id;
    l_set_req_status    BOOLEAN;
    l_req_phase         VARCHAR2(80);
    l_req_status        VARCHAR2(80);
    l_del_phase         VARCHAR2(30);
    l_del_status        VARCHAR2(30);
    l_message           VARCHAR2(255);
    l_interval          NUMBER := 10; -- In seconds
    l_max_time          NUMBER := 15000; -- In seconds
    l_completion_status VARCHAR2(30) := 'NORMAL';
    l_error_message     VARCHAR2(3000);
    l_file_dir          VARCHAR2(100);
    l_instance          VARCHAR2(8);
    l_shortname         VARCHAR2(30);
    l_count             NUMBER;

    l_can_submit_request BOOLEAN := TRUE;
    l_globalset          VARCHAR2(100);
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_err_callfrom       VARCHAR2(75) DEFAULT 'XXCUSAP_DCTM_INT_PKG';
    --l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_procedure_name VARCHAR2(75) := 'WC_LOAD_INV';
    l_org            hr_all_organization_units.organization_id%TYPE;
    l_org_name CONSTANT hr_all_organization_units.name%TYPE := 'HDS White Cap - Org'; --162 org_id
    l_batch_s NUMBER;
    l_verify  NUMBER;

  BEGIN
    BEGIN
      --  Setup parameters for running FND JOBS!
      l_can_submit_request := xxcus_misc_pkg.set_responsibility('XXWC_INT_FINANCE'
                                                               ,'HDS Payables Associate - WC');
      IF l_can_submit_request
      THEN
        l_globalset := 'Global Variables are set.';

      ELSE

        l_globalset := 'Global Variables are not set.';
        l_sec       := 'Global Variables are not set for the Responsibility of HDS Payables Associate - WC and the User.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        RAISE program_error;
      END IF;
    END;

    SELECT organization_id
      INTO l_org
      FROM hr_all_organization_units
     WHERE NAME = l_org_name;

    BEGIN
      --batch invoices importing to Oracle with max 50 invoices per batch
      FOR i IN 1 .. 10
      LOOP

        FOR c_batch IN (SELECT DISTINCT attribute1
                          FROM xxcus.xxcusap_dctm_inv_header_tbl
                         WHERE group_id = p_group_id
                           AND batch IS NULL)
        LOOP

          SELECT xxwc_ap_dctm_batch_s.nextval INTO l_batch_s FROM dual;

          FOR c_type IN (SELECT ROWID, group_id, attribute1
                           FROM xxcus.xxcusap_dctm_inv_header_tbl
                          WHERE group_id = p_group_id
                            AND interfaced_flag IS NULL
                            AND attribute1 = c_batch.attribute1
                            AND rownum <= 50)
          LOOP

            UPDATE xxcus.xxcusap_dctm_inv_header_tbl
               SET batch = l_batch_s
             WHERE group_id = p_group_id
               AND interfaced_flag IS NULL
               AND batch IS NULL
               AND attribute1 = c_batch.attribute1
               AND ROWID = c_type.rowid;
          END LOOP;
        END LOOP;
      END LOOP;

      BEGIN
        FOR c_inl_hdr IN (SELECT d.invoice_id
                                ,d.invoice_num
                                ,d.invoice_type_lookup_code
                                ,nvl(d.invoice_date, trunc(SYSDATE)) invoice_date
                                ,d.vendor_num
                                ,d.vendor_name
                                ,v.oracle_site_id vendor_site_id
                                ,d.invoice_amount
                                ,nvl(d.invoice_currency_code, 'USD') invoice_currency_code
                                ,d.description
                                ,d.org_id
                                ,d.source
                                ,d.group_id
                                ,(CASE
                                   WHEN d.po_number IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute1 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute2 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute3 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute4 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute5 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute6 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute7 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute8 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute9 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute10 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute11 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute12 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute13 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute14 IS NOT NULL THEN
                                    d.org_id
                                   WHEN d.attribute15 IS NOT NULL THEN
                                    d.org_id
                                   ELSE
                                    NULL
                                 END) attribute_category
                                ,d.attribute1
                                ,d.po_number attribute2 --,d.attribute2  po_number for White Cap
                                ,d.attribute3
                                ,d.attribute4
                                ,d.attribute5
                                ,d.attribute6
                                ,d.attribute7
                                ,d.attribute8
                                ,d.attribute9
                                ,d.attribute10
                                ,d.attribute11
                                ,d.attribute12
                                ,d.attribute13
                                ,d.attribute14
                                ,d.attribute15
                                ,(CASE
                                   WHEN d.invoice_amount < 0 THEN
                                    'DUE UPON RECEIPT'
                                   ELSE
                                    NULL
                                 END) term
                                ,d.batch
                            FROM xxcus.xxcusap_dctm_inv_header_tbl d
                                ,xxwcap_dctm_vendor_vw             v
                           WHERE group_id = p_group_id
                             AND d.org_id = l_org
                             AND d.vendor_num = v.oracle_vendor_num(+)
                             AND interfaced_flag IS NULL)
        LOOP

          INSERT INTO ap_invoices_interface
            (invoice_id
            ,invoice_num
            ,invoice_type_lookup_code
            ,invoice_date
            ,vendor_num
            ,vendor_name
            ,vendor_site_id
            ,invoice_amount
            ,invoice_currency_code
            ,description
            ,org_id
            ,SOURCE
            ,group_id
            ,last_updated_by
            ,last_update_date
            ,last_update_login
            ,creation_date
            ,created_by
            ,attribute_category
            ,attribute1
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7
            ,attribute8
            ,attribute9
            ,attribute10
            ,attribute11
            ,attribute12
            ,attribute13
            ,attribute14
            ,attribute15
            ,terms_name)
          VALUES
            (c_inl_hdr.invoice_id
            ,c_inl_hdr.invoice_num
            ,c_inl_hdr.invoice_type_lookup_code
            ,c_inl_hdr.invoice_date
            ,c_inl_hdr.vendor_num
            ,c_inl_hdr.vendor_name
            ,c_inl_hdr.vendor_site_id
            ,c_inl_hdr.invoice_amount
            ,c_inl_hdr.invoice_currency_code
            ,c_inl_hdr.description
            ,c_inl_hdr.org_id
            ,c_inl_hdr.source
            ,c_inl_hdr.group_id || '~' || c_inl_hdr.batch
            ,g_user_id
            ,SYSDATE
            ,g_login_id
            ,SYSDATE
            ,g_user_id
            ,c_inl_hdr.attribute_category
            ,c_inl_hdr.attribute1
            ,c_inl_hdr.attribute2
            ,c_inl_hdr.attribute3
            ,c_inl_hdr.attribute4
            ,c_inl_hdr.attribute5
            ,c_inl_hdr.attribute6
            ,c_inl_hdr.attribute7
            ,c_inl_hdr.attribute8
            ,c_inl_hdr.attribute9
            ,c_inl_hdr.attribute10
            ,c_inl_hdr.attribute11
            ,c_inl_hdr.attribute12
            ,c_inl_hdr.attribute13
            ,c_inl_hdr.attribute14
            ,c_inl_hdr.attribute15
            ,c_inl_hdr.term);

          FOR c_inv_line IN (SELECT d.rowid
                                   ,d.dctm_object_id
                                   ,d.invoice_id
                                   ,d.invoice_line_id
                                   ,d.line_number
                                   ,d.line_type_lookup_code
                                   ,d.amount
                                   ,d.description
                                   ,d.fru
                                   ,l.entrp_entity || '.' || l.entrp_loc || '.' ||
                                    l.entrp_cc || '.' || d.gl_account ||
                                    '.00000.00000.00000' dist_code_concatenated
                                   ,(CASE
                                      WHEN d.attribute1 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute2 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute3 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute4 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute5 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute6 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute7 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute8 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute9 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute10 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute11 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute12 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute13 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute14 IS NOT NULL THEN
                                       org_id
                                      WHEN d.attribute15 IS NOT NULL THEN
                                       org_id
                                      ELSE
                                       NULL
                                    END) attribute_category
                                   ,d.attribute1
                                   ,d.attribute2
                                   ,d.attribute3
                                   ,d.attribute4
                                   ,d.attribute5
                                   ,d.attribute6
                                   ,d.attribute7
                                   ,d.attribute8
                                   ,d.attribute9
                                   ,d.attribute10
                                   ,d.attribute11
                                   ,d.attribute12
                                   ,d.attribute13
                                   ,d.attribute14
                                   ,d.attribute15
                                   ,l.entrp_entity
                                   ,l.entrp_loc
                                   ,l.entrp_cc
                                   ,h.org_id
                               FROM xxcus.xxcusap_dctm_inv_line_tbl   d
                                   ,xxcus.xxcusap_dctm_inv_header_tbl h
                                   ,apps.xxcus_location_code_vw       l
                              WHERE d.dctm_object_id = h.dctm_object_id
                                AND h.group_id = p_group_id
                                AND d.invoice_id = h.invoice_id
                                AND h.invoice_id = c_inl_hdr.invoice_id
                                AND d.fru = l.fru(+))
          LOOP

            INSERT INTO ap_invoice_lines_interface
              (invoice_id
              ,invoice_line_id
              ,line_number
              ,line_type_lookup_code
              ,amount
              ,description
              ,dist_code_concatenated
              ,last_updated_by
              ,last_update_date
              ,last_update_login
              ,creation_date
              ,created_by
              ,attribute_category
              ,attribute1
              ,attribute2
              ,attribute3
              ,attribute4
              ,attribute5
              ,attribute6
              ,attribute7
              ,attribute8
              ,attribute9
              ,attribute10
              ,attribute11
              ,attribute12
              ,attribute13
              ,attribute14
              ,attribute15
              ,org_id)
            VALUES
              (c_inv_line.invoice_id
              ,c_inv_line.invoice_line_id
              ,c_inv_line.line_number
              ,c_inv_line.line_type_lookup_code
              ,c_inv_line.amount
              ,c_inv_line.description
              ,c_inv_line.dist_code_concatenated
              ,g_user_id
              ,SYSDATE
              ,g_login_id
              ,SYSDATE
              ,g_user_id
              ,c_inv_line.attribute_category
              ,c_inv_line.attribute1
              ,c_inv_line.attribute2
              ,c_inv_line.attribute3
              ,c_inv_line.attribute4
              ,c_inv_line.attribute5
              ,c_inv_line.attribute6
              ,c_inv_line.attribute7
              ,c_inv_line.attribute8
              ,c_inv_line.attribute9
              ,c_inv_line.attribute10
              ,c_inv_line.attribute11
              ,c_inv_line.attribute12
              ,c_inv_line.attribute13
              ,c_inv_line.attribute14
              ,c_inv_line.attribute15
              ,c_inv_line.org_id);

            l_sec := 'Insert invoice ID:  ' || c_inl_hdr.invoice_id ||
                     '  Insert Line ID:  ' || c_inv_line.invoice_line_id;

            l_sec := 'Updating xxcusap_dctm_inl_line_tbl to I - inserted interface';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);

            UPDATE xxcus.xxcusap_dctm_inv_line_tbl
               SET interfaced_flag  = 'I'
                  ,last_update_date = SYSDATE
                  ,last_updated_by  = fnd_global.user_id
             WHERE ROWID = c_inv_line.rowid;

          END LOOP;

          l_sec := 'Updating xxcusap_dctm_inl_header_tbl to I - inserted interface';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          UPDATE xxcus.xxcusap_dctm_inv_header_tbl
             SET interfaced_flag  = 'I'
                ,last_update_date = SYSDATE
                ,last_updated_by  = fnd_global.user_id
           WHERE group_id = p_group_id
             AND batch = c_inl_hdr.batch;

        END LOOP;

      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          p_retcode := SQLCODE;
          p_errbuf  := substr(SQLERRM, 1, 2000);
          fnd_file.put_line(fnd_file.log
                           ,'Error in ' || l_procedure_name || l_sec);
          fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
          fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
          RAISE program_error;
      END;

    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        p_retcode := SQLCODE;
        p_errbuf  := substr(SQLERRM, 1, 2000);
        fnd_file.put_line(fnd_file.log
                         ,'Error in ' || l_procedure_name || l_sec);
        fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
        fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
        RAISE program_error;
    END;

    BEGIN

      FOR submit IN (SELECT DISTINCT batch batch
                                    ,attribute1
                                    ,group_id || '~' || batch group_num
                       FROM xxcus.xxcusap_dctm_inv_header_tbl
                      WHERE group_id = p_group_id)
      LOOP
        --
        -- Submit the invoice import
        --
        l_sec := 'Submitting Payables Invoice Import';
        fnd_file.put_line(fnd_file.log, l_sec);
        l_req_id := fnd_request.submit_request('SQLAP'
                                              ,'APXIIMPT'
                                              ,'Payables Open Interface Import' -- Description of the request
                                              ,to_char(SYSDATE
                                                      ,'DD-MON-RR HH24:MI:SS')
                                              ,FALSE
                                              ,l_org --Operating Unit
                                              ,'DCTM' --Source
                                              ,submit.group_num --Group
                                              ,'DCTM_' || submit.attribute1 || '_' ||
                                               submit.batch || '_' ||
                                               to_char(SYSDATE, 'MMDDYYYY') --Batch Name
                                              ,NULL --NULL --Hold Name
                                              ,NULL --Hold Reason
                                              ,NULL --GL Date
                                              ,'N' --Purge
                                              ,NULL --Trace Switch
                                              ,NULL --Debug Switch
                                              ,NULL --Summarize Report
                                              ,NULL --Commit batch size
                                              ,NULL --User ID
                                              ,NULL); --Login ID
        COMMIT;
        --White Cap is batching invoices so we can't update the the interface table need to update the header
        --record with the request id
        l_sec := 'Updating xxcusap_dctm_interface_tbl to Processing';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);

        -- get the status of the process.
        IF l_req_id != 0
        THEN
          --
          -- Wait for the previous concurrent request to complete
          --
          IF fnd_concurrent.wait_for_request(l_req_id
                                            ,l_interval
                                            ,l_max_time
                                            ,l_req_phase
                                            ,l_req_status
                                            ,l_del_phase
                                            ,l_del_status
                                            ,l_message)
          THEN
            l_err_msg := chr(10) || 'ReqID=' || l_req_id ||
                               ' DPhase ' || l_del_phase || ' DStatus ' ||
                               l_del_status || chr(10) || ' MSG - ' ||
                               l_message;
            -- Error Returned
            IF l_del_phase != 'COMPLETE' OR l_del_status != 'NORMAL'
            THEN
              l_sec := 'An error occured in the running the invoice import' ||
                             l_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
              RAISE program_error;

              UPDATE xxcus.xxcusap_dctm_interface_tbl
                 SET status      = 'ERROR'
                    ,request_id  = l_req_id
                    ,update_date = SYSDATE
                    ,updated_by  = fnd_global.user_id
               WHERE group_id = p_group_id;

            END IF;

            l_sec := 'Payables Import Completed Request ID:  ' || l_req_id;
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);

            -- Then Success!
            UPDATE xxcus.xxcusap_dctm_inv_header_tbl
               SET interfaced_flag  = 'Y'
                  ,request_id       = l_req_id
                  ,last_update_date = SYSDATE
                  ,last_updated_by  = fnd_global.user_id
             WHERE group_id = p_group_id
               AND batch = submit.batch;

            COMMIT;

            l_sec := 'Updated xxcusap_dctm_interface_tbl to Completed';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);

          END IF;
        END IF;
      END LOOP;
      --
      BEGIN
        SELECT COUNT(*)
          INTO l_verify
          FROM xxcus.xxcusap_dctm_inv_header_tbl
         WHERE group_id = p_group_id
           AND interfaced_flag IS NULL;

        IF l_verify = 0
        THEN
          UPDATE xxcus.xxcusap_dctm_interface_tbl
             SET status      = 'COMPLETED'
                ,update_date = SYSDATE
                ,updated_by  = fnd_global.user_id
           WHERE group_id = p_group_id;
        ELSE
          UPDATE xxcus.xxcusap_dctm_interface_tbl
             SET status      = 'INCOMPLETED'
                ,update_date = SYSDATE
                ,updated_by  = fnd_global.user_id
           WHERE group_id = p_group_id;
        END IF;
      END;
    END;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;

      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom || '.' ||
                                                                  l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
    WHEN OTHERS THEN

      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom || '.' ||
                                                                  l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');

  END wc_load_inv;

END xxcusap_dctm_invoice_int_pkg;
/
