CREATE OR REPLACE PACKAGE XXWC_INV_PEND_TRNS_PROCESS_PKG AS
  /********************************************************************************
  FILE NAME: XXWC_INV_PEND_TRNS_PROCESS_PKG.pks
  
  PROGRAM TYPE: PL/SQL Package Specification
  
  PURPOSE: Automate processing for Inventory Transactions stuck in mtl transaction interface table
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/05/2015    Keerthi SM      TMS20150715-00148 Initial version.
  ********************************************************************************/
  PROCEDURE del_inv_int_err_prc(X_ERRORBUF OUT VARCHAR2,X_RETCODE OUT NUMBER);
  
  PROCEDURE upd_inv_int_sysd_prc(X_ERRORBUF OUT VARCHAR2,X_RETCODE OUT NUMBER);
  
  END XXWC_INV_PEND_TRNS_PROCESS_PKG;
/ 
 