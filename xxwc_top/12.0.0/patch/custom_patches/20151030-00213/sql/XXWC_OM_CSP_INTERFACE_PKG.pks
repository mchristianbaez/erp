CREATE OR REPLACE PACKAGE APPS.xxwc_om_csp_interface_pkg
AS

/********************************************************************************
FILE NAME: APPS.XXWC_OM_CSP_INTERFACE_PKG.pkg

PROGRAM TYPE: PL/SQL Package

PURPOSE: This contains contains the need procedures for the Contract Pricing 
         Maintenance Extension. This includes Customer Pricing Attribute for 
         Item Category, ability to import modifiers from the CSP Maintenance 
         Form to Advanced Pricing, aonvert a Quote to a CSP Agreement, and 
         process  a mass CSP upload into the custom form.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/09/2013    Lucidity Consulting    Initial Release
1.1     12/17/2015    Pattabhi Avula         TMS# 20151030-00213 -- Parameter Name changed
********************************************************************************/


  G_RETURN_SUCCESS      CONSTANT    VARCHAR2(1) := 'S' ;
  G_RETURN_ERROR        CONSTANT    VARCHAR2(1) := 'E' ;
  G_RETURN_UNEXP_ERROR  CONSTANT    VARCHAR2(1) := 'U' ;
  
  -- 10/09/2013 CG: TMS 20131009-00322: SQL*Loader Path Profile definition
  G_FILE_DIR_PATH       VARCHAR2(400) := Fnd_Profile.Value('XXWC_CSP_LOADER_PATH');
  
  PROCEDURE create_header (i_agreement_id IN NUMBER);
  
  PROCEDURE create_lines (i_agreement_id IN NUMBER);
  
  PROCEDURE create_exclusions (i_agreement_id IN NUMBER);
   
  PROCEDURE update_lines (i_agreement_id IN NUMBER);
  
  PROCEDURE import_modifiers (errbuf              OUT VARCHAR2
                             , retcode             OUT VARCHAR2
                            -- , i_agreement_id   IN     NUMBER);  -- Version# 1.1
							 , p_agreement_id   IN     NUMBER);  -- Version# 1.1
                             
  PROCEDURE csp_quote (errbuf          OUT VARCHAR2, 
                        retcode         OUT VARCHAR2,
                        i_quotenumber   IN NUMBER,
                        i_price_type    IN VARCHAR2,
                        i_incomp_group  IN varchar2,
                        i_vq_number     IN VARCHAR2,
                        i_vendor_number IN NUMBER,
                        i_start_date IN VARCHAR2);
                   
                          
  FUNCTION get_item_category (i_inv_item_id IN NUMBER)
  RETURN CHAR;
      
  PROCEDURE csp_conversion (errbuf OUT VARCHAR2, 
                              retcode OUT VARCHAR2);
                              
  PROCEDURE csp_headers ;
  
  PROCEDURE csp_lines ; 
  
  PROCEDURE csp_exclusions ; 
                      
  PROCEDURE csp_mass_conversion (errbuf   OUT VARCHAR2, 
                                retcode  OUT VARCHAR2);
                                
  PROCEDURE csp_cust_quote (errbuf          OUT VARCHAR2, 
    retcode         OUT VARCHAR2,
    i_quotenumber   IN NUMBER,
    i_price_type    IN VARCHAR2,
    i_incomp_group  IN varchar2,
    i_vq_number     IN VARCHAR2,
    i_vendor_number IN NUMBER,
    i_start_date IN VARCHAR2);
    
  PROCEDURE csp_end_date (errbuf OUT VARCHAR2, 
                              retcode OUT VARCHAR2);
  PROCEDURE csp_delta (errbuf OUT VARCHAR2, 
                          retcode OUT VARCHAR2) ; 
                          
                          
  PROCEDURE update_delta_lines ;

  PROCEDURE create_delta_lines ; 

  PROCEDURE mass_update (errbuf              OUT VARCHAR2
                             , retcode             OUT VARCHAR2);
                             
                               -- 04/09/13 CG: TMS# : Procedure to remove inactive CSPs
  PROCEDURE cleanup_csp_quotes (errbuf OUT VARCHAR2, 
                              retcode OUT VARCHAR2,
                              i_agreement_id IN NUMBER);

-- 08/28/2013 CG: TMS 20130828-00775: New procedure to be used from maintenance from for self service submission
  PROCEDURE xxwc_csp_maintenance (RETCODE         OUT NUMBER
                                   , ERRMSG         OUT VARCHAR2
                                   , P_FILE_NAME    IN VARCHAR2
                                   , P_FILE_ID      IN NUMBER);
                                   
PROCEDURE csp_maintenance_process
(errbuf OUT VARCHAR2, 
retcode OUT VARCHAR2);

PROCEDURE csp_maintenance_exclusions
(errbuf OUT VARCHAR2, 
retcode OUT VARCHAR2);



END;
/


