CREATE OR REPLACE PACKAGE BODY XXEIS.EIS_WC_ORDER_HOLD_DET_PKG
AS        
/*************************************************************************
     Procedure : EIS_WC_ORDER_HOLD_DET_PKG

     PURPOSE:   This will populate the staging table for the Eis report
                Bill Trust
     Parameter:
	 REVISIONS:
     Ver          Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        10/16/2015  Maharajan Shunmugam    TMS#20140516-00041 Credit - EIS Create Credit Hold Release Report
   ************************************************************************/
   PROCEDURE populate_stage 
   IS
   BEGIN 
 
   fnd_file.put_line (fnd_file.LOG, 'Truncating the staging table');
   --EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_CR_HOLD_CASE_FOLDER_STG';  
   DELETE FROM XXWC.XXWC_CR_HOLD_CASE_FOLDER_STG;
   COMMIT; 
 
   fnd_file.put_line (fnd_file.LOG, 'Inserting into staging table');

   INSERT INTO XXWC.XXWC_CR_HOLD_CASE_FOLDER_STG
  (SELECT accr.source_column1,
          accr.created_by ,
          accf.case_folder_number,
          DECODE(substr(accf.review_type,1,1),'O','O','C','C','S','B') REVIEW_TYPE,
          to_char(accf.creation_date,'DD-MON-YYYY HH24:MI:SS') FOLDER_CREATION_DATE ,
          to_char(accf.last_update_date,'DD-MON-YYYY HH24:MI:SS') CASE_FOLDER_RELEASED_DATE
   FROM   apps.ar_cmgt_case_folders accf, 
          apps.ar_cmgt_credit_requests accr
   WHERE accf.credit_request_id = accr.credit_request_id
   AND   accf.case_folder_number IS NOT NULL ) ;     
                      
COMMIT;

EXCEPTION
WHEN OTHERS THEN
    fnd_file.put_line (fnd_file.LOG, 'When others exception '||SQLERRM);

END populate_stage;
END EIS_WC_ORDER_HOLD_DET_PKG;
/