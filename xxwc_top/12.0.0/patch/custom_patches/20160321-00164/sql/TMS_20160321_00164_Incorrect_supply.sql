/*
 TMS: 20160321-00164  Incorrect supply
 */
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before delete');

   DELETE FROM mtl_supply
         WHERE     req_line_id = 25440495
               AND shipment_header_id = 3218731
               AND item_id = 2924792
               AND TO_ORGANIZATION_ID = 309;

   COMMIT;
   DBMS_OUTPUT.put_line ('Records deleted-' || SQL%ROWCOUNT);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to delete Supply record ' || SQLERRM);
END;
/