/*************************************************************************
  $Header TMS_20160928-00263_Order_issue.sql $
  Module Name: TMS#20160928-00263 - Order 21409241  -- Data fix Script 

  PURPOSE: Data fix to update the order status

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        10-OCT-2016 Pattabhi Avula         TMS#20160928-00263 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160928-00263    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
CANCELLED_FLAG='Y'
where line_id = 75884441
and header_id= 46381258;

   DBMS_OUTPUT.put_line (
         'TMS: 20160928-00263  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160928-00263    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160928-00263 , Errors : ' || SQLERRM);
END;
/