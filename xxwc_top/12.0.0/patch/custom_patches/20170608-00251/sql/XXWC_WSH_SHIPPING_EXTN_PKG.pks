CREATE OR REPLACE PACKAGE APPS.xxwc_wsh_shipping_extn_pkg
as
/*************************************************************************
 Copyright (c) 2013 Lucidity Consulting Group
 All rights reserved.
**************************************************************************
  $Header xxwc_wsh_shipping_extn_pkg $
  Module Name: xxwc_wsh_shipping_extn_pkg.pks

  PURPOSE:   This package is called by the concurrent programs
             XXWC WSH Backordered Transactions Processing

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        01/21/2013  Consuelo Gonzalez      Initial Version
  2.0        11/12/2013  Ram and Rasikha       TMS #20131009-00330
  3.0        11/19/2015  Manjula Chellappan    TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
  4.0        06/15/2017  Rakesh Patel          TMS#20170608-00251-Custom SOE Phase-2 Get cost API													 
**************************************************************************/

G_ONT_DEBUG_LEVEL       NUMBER := To_Number(Fnd_Profile.Value('ONT_DEBUG_LEVEL'));

g_dflt_email            fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com'; --Added for Ver 3.0

procedure subinventory_transfer (  p_header_id in number
                                   , p_line_id in number
                                   , p_delivery_id in number
                                   , p_from_subinv in varchar2
                                   , p_to_subinv in varchar2
                                   , p_cur_log_seq  in number
                                   , p_ret_status   out varchar
                                   , p_ret_msg  out varchar2
                                   , p_last_log_seq out number);

procedure order_subinv_transf (p_header_id in number
                               , p_organization_id in number
                               , p_user_id  in number
                               , p_resp_id  in number
                               , p_resp_appl_id in number
                               , p_update_login in number
                               , p_line_id IN NUMBER
                               , p_delivery_id in out number
                               , p_create_delivery in varchar2
                               , p_proc_subinv_transf in varchar2);
   

procedure delete_line(i_delivery_id in number, i_line_id in number);


PROCEDURE ship_confirm_order (p_header_id       IN     NUMBER
                           , p_from_org_id     IN     NUMBER
                           , p_delivery_id     IN     NUMBER
                           , p_pick_rule       IN     VARCHAR2
                           , p_return_status      OUT VARCHAR2
                           , p_return_msg         OUT VARCHAR2);

procedure sync_order_staging (p_header_id         IN NUMBER
                              , p_delivery_id         IN NUMBER
                              , p_ship_from_org_id  IN NUMBER
                              , p_return_status      OUT VARCHAR2
                              , p_return_msg        OUT VARCHAR2);  
                              
                              
PROCEDURE split_line(i_header_id in number,  
                      i_ship_from_org_id in number,
                      i_user_id in number,
                      i_resp_id in number,
                      i_appl_id in number,
                      o_return_status out varchar2, 
                      o_return_msg out varchar2);                              

procedure rebuild_lot_reservation (  p_header_id         IN NUMBER
                                      , p_delivery_id       IN NUMBER
                                      , p_ship_from_org_id  IN NUMBER
                                      , p_return_status      OUT VARCHAR2
                                      , p_return_msg        OUT VARCHAR2);                              
 
procedure cancelled_orders_upd (errbuf      OUT VARCHAR2,
                                retcode     OUT VARCHAR2,
                                p_header_id IN  NUMBER,
                                p_last_delta_date VARCHAR2);--11/13/2013 Ram Talluri and Rasikha Galimova TMS #20131009-00330

PROCEDURE check_lot_fs_reservation (
      p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER, 
      p_delivery_id   IN NUMBER,
      x_lot_count   OUT      NUMBER,
      x_check       OUT      VARCHAR2,
      x_message     OUT      VARCHAR2
   );
   
PROCEDURE check_lot_reservation (
      p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER, 
      p_delivery_id   IN NUMBER,
      x_lot_count   OUT      NUMBER,
      x_check       OUT      VARCHAR2,
      x_partial_check OUT     VARCHAR2,
      x_message     OUT      VARCHAR2
   );   

-- 04/17/2013 CG: TMS 20130422-01089 Added new procedure to clear out a delivery from the staging table
procedure clear_delivery (p_header_id         IN NUMBER
                          , p_delivery_id         IN NUMBER
                          , p_ship_from_org_id  IN NUMBER
                          , p_return_status      OUT VARCHAR2
                          , p_return_msg        OUT VARCHAR2);

-- 05/06/2013 CG: TMS 20130509-01444: Added new procedure to sync delivery statuses at the end of the day
procedure sync_del_line_status (errbuf      OUT VARCHAR2,
                                retcode     OUT VARCHAR2,
                                p_header_id IN NUMBER,
                                p_last_delta_date VARCHAR2);--11/13/2013 Ram Talluri and Rasikha Galimova TMS #20131009-00330

-- New Procedures added for Rev 3.0 Begin	
							
PROCEDURE Counter_subinv_xfer   (p_header_id IN NUMBER,
                                 p_return_status OUT VARCHAR2,
								 p_error_msg OUT VARCHAR2);	
								 
FUNCTION sync_Counter_line      (p_subscription_guid  IN  RAW
                               , p_event              IN  OUT wf_event_t)
	RETURN VARCHAR2;			
	  
PROCEDURE raise_sync_counter_line (p_line_id  IN NUMBER,
                                   p_line_qty IN NUMBER,
                                   p_error_msg  OUT VARCHAR2);
								   
PROCEDURE audit_line_qty  (
    itemtype  IN VARCHAR2 ,
    itemkey   IN VARCHAR2 ,
    actid     IN NUMBER ,
    funcmode  IN VARCHAR2 ,
    resultout IN OUT NOCOPY VARCHAR2);								   
	
PROCEDURE delete_audit_line  (
    itemtype  IN VARCHAR2 ,
    itemkey   IN VARCHAR2 ,
    actid     IN NUMBER ,
    funcmode  IN VARCHAR2 ,
    resultout IN OUT NOCOPY VARCHAR2);	

PROCEDURE update_audit_line_qty  (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER);		
	
PROCEDURE audit_split_int_line_qty  (
      p_line_id    IN   NUMBER,
	  p_error_msg  OUT  VARCHAR2          
      );	
	  
-- New Procedures added for Rev 3.0 End	

-- Rev# 4.0 < Start
/*************************************************************************
  Procedure : Update_SO_Lines_Cost
  PURPOSE: to update the unit_cost on the sales order lines.
************************************************************************/
   PROCEDURE Update_SO_Lines_Cost (
        p_header_id     IN  NUMBER
      , p_po_header_id  IN  NUMBER DEFAULT NULL
      , p_ret_status    OUT VARCHAR
   );
   
   PROCEDURE raise_Update_SO_Lines_Cost (p_po_header_id  IN NUMBER,
                                         p_error_msg  OUT VARCHAR2);
   
   FUNCTION Update_DS_SO_Lines_Cost ( p_subscription_guid  IN  RAW
                                    , p_event              IN  OUT wf_event_t)
   RETURN VARCHAR2;
			   
-- Rev# 4.0 > End
									
end xxwc_wsh_shipping_extn_pkg;
/