CREATE OR REPLACE TRIGGER APPS.XXWC_PO_HEADERS_ARCH_AIR_TRG
   /**************************************************************************
     $Header XXWC_PO_HEADERS_ARCH_AIR_TRG $
     Module Name:XXWC_PO_HEADERS_ARCH_AIR_TRG
     PURPOSE:   This Trigger is used to raise the business event to sync 
	            the cost on the so order line for the drop ship orders.
     REVISIONS:
     Ver        Date         Author                  Description
     ---------  -----------  ---------------         -------------------------
     1.0        19-Jun-2017  Rakesh Patel            TMS#20170608-00251-Custom SOE Phase-2 Get cost API
     **************************************************************************/
AFTER INSERT ON APPS.po_headers_archive_all 
FOR EACH ROW

DECLARE
      l_error_msg   VARCHAR2 (240);      
      l_sec         VARCHAR2 (100);
	  l_dflt_email  fnd_user.email_address%TYPE  := 'HDSOracleDevelopers@hdsupply.com';	  
BEGIN
	xxwc_wsh_shipping_extn_pkg.raise_Update_SO_Lines_Cost(:new.po_header_id, l_error_msg);

	IF l_error_msg IS NOT NULL THEN
       l_sec := 'po_header_id'||:new.po_header_id|| l_error_msg;
	   
       xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_HEADERS_ARCH_AIR_TRG',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => NULL,
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => l_dflt_email,
            p_module              => 'PO');
    END IF;
	
EXCEPTION
      WHEN OTHERS
      THEN	 		 
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_HEADERS_ARCH_AIR_TRG',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => l_dflt_email,
            p_module              => 'PO');
END;
/