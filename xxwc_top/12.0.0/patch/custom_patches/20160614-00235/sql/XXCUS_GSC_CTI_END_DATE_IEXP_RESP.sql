/*
 -- Author: Balaguru Seshadri
 -- Parameters: None
 -- Modification History
 -- ESMS        TMS              Date        Version  Comments
 -- =========== ===============  ========    ======   ========================
 -- 328499      20160614-00235   15-JUN-2016 1.0      Created.
 */ 
SET SERVEROUTPUT ON SIZE 1000000
declare
  --
  v_project_desc     varchar2(150) :=Null;
  v_nt_id                    fnd_user.user_name%type :=Null;
  --
  -- Pick all EBS users of former Creative Touch Interiors and end date their i-Expense US or CAD direct responsibility.
  --
 cursor disable_direct_resp is
   select c.user_name nt_id
          ,c.user_id user_id
          ,b.responsibility_key resp_key
          ,b.responsibility_name resp_name
          ,d.application_id resp_appl_id
          ,d.application_short_name resp_appl_short_name
          ,case when a.security_group_id =0 then 'STANDARD' else 'NA' end sec_grp_code
          ,a.start_date start_date
          ,trunc(sysdate) end_date
          ,c.employee_id
    from   fnd_user_resp_groups_direct a
          ,fnd_responsibility_vl       b
          ,fnd_user                    c
          ,fnd_application_vl          d
         ,(
                select c.person_id
                from   apps.per_all_assignments_f c
                           ,apps.gl_code_combinations d
                where  1 =1
                  and  d.code_combination_id =c.default_code_comb_id
                  and  d.segment1 IN ('42') --Employees owned by CTI only      
                group by c.person_id
          ) PS_users        
    where  1 =1
      and  d.application_id    =a.responsibility_application_id
      and c.employee_id =ps_users.person_id
      and  a.user_id           =c.user_id   --users of LOB CTI  only
      and  b.responsibility_id =a.responsibility_id
      and  a.end_date is null --only direct responsiblities that are not end dated for a user are pulled and deactivated
      and  exists
           (
            select lookup_code resp_key  --HDS Internet Expenses User
            from   fnd_lookup_values_vl
            where  1 =1
              and  lookup_type ='XXCUS_GRC_ROLE_RESP_EXCLUSION'
              and  lookup_code =b.responsibility_key
           );
 --
 procedure print_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_log;
 --
 --
 procedure print_output(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.output, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_output;
 --         
begin
 --
 print_log('Start: Main');
--
-- disable all active direct responsibilities
--
  begin 
      --
      for rec in disable_direct_resp loop 
        --
        -- even though we are calling addresp routine, it checks for active assignments
        -- and inactivates them when an end date is passed
        --
        v_nt_id :=rec.nt_id;
        --
        v_project_desc :='ESMS: 328499, end date i-Expense direct responsibility';
        --
        fnd_user_pkg.addresp
               (
                 username        =>rec.nt_id, --varchar2
                 resp_app        =>rec.resp_appl_short_name, --varchar2
                 resp_key        =>rec.resp_key, --varchar2,
                 security_group  =>rec.sec_grp_code, --varchar2,
                 description     =>v_project_desc, --varchar2,
                 start_date      =>rec.start_date, --date,
                 end_date        =>trunc(sysdate)--date --when passed and if an assignment exists for a user will be end dated
               );
        -- 
        print_log('i-Expense direct responsibility successfully end dated for CTI user: '||rec.nt_id);                    
        --  
      end loop;       
      --
  exception
   when others then
    print_log('@cursor disable_direct_resp for user, error in user  '||v_nt_id||', msg ='||sqlerrm);
  end;   
--
 commit;
 -- 
 print_log('End: Main');
 -- 
exception
 when others then
  print_log('Outer block of SQL script, message ='||sqlerrm);
end;
/