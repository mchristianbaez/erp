create or replace 
package       XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG AS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Bin Location With Sales History"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_BIN_LOCA_SALES_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      18-may-2016       Initial Build TMS#20160301-00067
--// 1.1        Siva  	      26-Oct-2016        TMS#20160824-00006 --Performance issue

/* --Commented start for version 1.1
function get_last_received_date (p_inventory_item_id in number,p_organization_id in number) return date;
--//============================================================================
--//
--// Object Name         :: get_last_received_date  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================

function GET_ITEM_COST(P_NUM in number,P_INVENTORY_ITEM_ID in number,P_ORGANIZATION_ID in number) return number;
--//============================================================================
--//
--// Object Name         :: GET_ITEM_COST  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
function GET_PRIMARY_BIN_LOC (P_INVENTORY_ITEM_ID in number,P_ORGANIZATION_ID in number) return varchar2;
--//============================================================================
--//
--// Object Name         :: GET_PRIMARY_BIN_LOC  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
function GET_ALTERNATE_BIN_LOC (P_INVENTORY_ITEM_ID in number,P_ORGANIZATION_ID in number) return varchar2;
--//============================================================================
--//
--// Object Name         :: GET_ALTERNATE_BIN_LOC  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
*/ --Commented end for version 1.1

function GET_START_BIN return varchar2;
--//============================================================================
--//
--// Object Name         :: GET_START_BIN  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
function GET_END_BIN return varchar2;
--//============================================================================
--//
--// Object Name         :: GET_END_BIN  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter .
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
FUNCTION GET_START_DATE  ( P_SET_OF_BOOKS_ID  IN NUMBER)
  return date;
--//============================================================================
--//
--// Object Name         :: GET_START_DATE  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter .
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================  
procedure get_bin_location_info (p_process_id         in number,
                                 p_category_from      in varchar2 default null,
                                 p_category_to        in varchar2 default null,
                                 p_organization       IN VARCHAR2  DEFAULT NULL,
                                 p_category_set       IN VARCHAR2  DEFAULT NULL--,
                                 --p_quantity_on_hand   in varchar2
								);
--//============================================================================
--//
--// Object Name         :: get_bin_location_info  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  			05-May-2016 		Initial Build   
--//============================================================================
type t_cashtype_vldn_tbl is table of varchar2(100) index by varchar2(100);
type t_number_tab is table of number index by binary_integer;


g_item_cost_vldn_tbl                 t_cashtype_vldn_tbl;
g_primary_bin_loc_vldn_tbl           t_cashtype_vldn_tbl;
g_alternate_bin_loc_vldn_tbl         t_cashtype_vldn_tbl;
g_last_received_date_vldn_tbl        t_cashtype_vldn_tbl;
g_period_name_vldn_tbl               T_CASHTYPE_VLDN_TBL;

g_item_cost          NUMBER;
G_PRIMARY_BIN_LOC    varchar2(200);
G_ALTERNATE_BIN_LOC  varchar2(200);
G_LAST_RECEIVED_DATE date;
G_START_BIN varchar2(100);
g_end_bin   varchar2(100);
type CURSOR_TYPE4 is ref cursor;


procedure CLEAR_TEMP_TABLES ( P_PROCESS_ID in number);
--//============================================================================
--//
--// Object Name         :: CLEAR_TEMP_TABLES  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in Afteer report and delete data from Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  				05-May-2016 	Initial Build   
--//============================================================================
function get_mtd_sales (p_inventory_item_id    number,
                           p_organization_id      NUMBER,
                           p_set_of_books_id NUMBER) RETURN NUMBER;
--//============================================================================
--//
--// Object Name         :: get_mtd_sales  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location With Sales History"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================						   
end EIS_XXWC_BIN_LOCA_SALES_PKG;
/
