/*
 TMS: (This is going to be applied frequently until the issue resolves permanently)
 Date: 08/17/2015
 */
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('Script 1 -Before Delete');

   DELETE FROM mtl_transactions_interface
         WHERE     source_code = 'Subinventory Transfer'
               AND process_flag = '3'
               AND ERROR_CODE = 'Invalid item';

   DBMS_OUTPUT.put_line (
      'Script 1 -After Delete, rows deleted: ' || SQL%ROWCOUNT);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('Script 1, Errors =' || SQLERRM);
END;
/