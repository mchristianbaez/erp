/*************************************************************************
  $Header TMS_20171003-00044_UPDATE_B2B_POS.sql $
  Module Name: TMS_20171003-00044 Data Fix for B2B PO's duplicates

  PURPOSE: Data Fix for B2B PO's duplicates

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-OCT-2017  Pattabhi Avula         TMS#20171003-00044

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20171003-00044   , Before Update');

UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl
   SET process_flag = 'S'
     , error_message = 'PO Created Manually, marked as processed -  -- Customer not setup as B2B'
WHERE customer_po_number IN ('39086',
'39081');

-- Expected to update TWO row

   DBMS_OUTPUT.put_line (
         'TMS: 20171003-00044 Sales order lines updated (Expected:2): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20171003-00044   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20171003-00044, Errors : ' || SQLERRM);
END;
/