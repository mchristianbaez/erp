CREATE OR REPLACE PACKAGE BODY xxwc_ahh_item_conv_pkg IS
  /***************************************************************************
  *    Script Name: xxwc_ahh_item_conv_pkg.pks
  *
  *    Interface / Conversion Name: Item conversion.
  *
  *    Functional Purpose: Convert Items using Interface for A.H Harris
  *
  *    History:
  *
  *    Version    Date             Author      Description
  *****************************************************************************
  *    1.0        06/04/2018       Naveen K    Initial Development.
  *****************************************************************************/

  g_debug    VARCHAR2(1) := 'Y';
  gid        ROWID;
  g_batch_id NUMBER;

  g_validation_only    BOOLEAN DEFAULT FALSE;
  g_include_mst_items  BOOLEAN DEFAULT TRUE;
  g_include_org_items  BOOLEAN DEFAULT TRUE;
  g_include_categories BOOLEAN DEFAULT TRUE;

  TYPE gt_xxwc_invitem_stg_t IS TABLE OF xxwc_invitem_stg%ROWTYPE;


  g_cmp_psts VARCHAR2(3) := 'CMP'; -- Complete
  g_inp_psts VARCHAR2(3) := 'INP'; -- Inprocess
  g_err_psts VARCHAR2(3) := 'ERR'; -- Error
  g_new_psts VARCHAR2(3) := 'NEW'; -- New/eligible for pickup.

  g_scs_sts VARCHAR2(1) := 'S'; -- Success
  g_fl_sts  VARCHAR2(1) := 'F'; -- Failed
  g_err_sts VARCHAR2(1) := 'E'; -- Errored
  g_prs_sts VARCHAR2(1) := 'P'; -- Processing
  g_new_sts VARCHAR2(1) := 'N'; -- Processing

  g_stg_log_msg     VARCHAR2(4000);
  g_org_stg_log_msg VARCHAR2(4000);

  ---
  -- PROCEDURE: Prints Debug/Log Message - Checks for Debug Flag.
  ---
  PROCEDURE print_debug(p_print_str IN VARCHAR2) IS
  BEGIN
    IF g_debug = 'Y' THEN
      fnd_file.put_line(fnd_file.log
                       ,p_print_str);
      --dbms_output.put_line(p_print_str);
    END IF;
  END print_debug;

  ---
  -- PROCEDURE: Print Log Message - Ignores Debug Flag.
  ---
  PROCEDURE print_log(p_print_str IN VARCHAR2) IS
  BEGIN
    fnd_file.put_line(fnd_file.log
                     ,p_print_str);
    dbms_output.put_line(p_print_str);
  END print_log;


  ---
  -- PROCEDURE: Update Staging Table 
  ---
  PROCEDURE update_stg_table(pid        ROWID
                            ,p_val_flag VARCHAR2
                            ,p_status   VARCHAR2
                            ,p_msg      VARCHAR2
                            ,p_upd_mod  VARCHAR2 DEFAULT 'A') IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
    UPDATE xxwc.xxwc_ahh_items_conv_stg
       SET validation_flag = (CASE
                               WHEN validation_flag = 'E' THEN
                                'E'
                               WHEN p_val_flag = 'NA' THEN
                                validation_flag
                               ELSE
                                p_val_flag
                             END)
          ,status = (CASE
                      WHEN status = 'ERR' THEN
                       'ERR'
                      WHEN p_status = 'NA' THEN
                       status
                      ELSE
                       p_status
                    END)
          ,error_message   = decode(p_upd_mod
                                   ,'A'
                                   ,error_message || '|' || p_msg
                                   ,'R'
                                   ,p_msg)
     WHERE ROWID = pid;
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      print_log('Error while updating staging table: ' || SQLERRM);
      -- RAISE;
  END;

  ---
  -- PROCEDURE: Update Staging Table 
  ---
  PROCEDURE update_org_stg_table(p_id             ROWID
                                ,p_process_status VARCHAR2
                                ,p_status_flag    VARCHAR2
                                ,p_msg            VARCHAR2
                                ,p_upd_mod        VARCHAR2 DEFAULT 'A') IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
  
    UPDATE xxwc.xxwc_ahh_items_org_conv_stg
       SET process_status = (CASE
                              WHEN p_process_status = 'ERR' THEN
                               'ERR'
                              WHEN p_process_status = 'NA' THEN
                               process_status
                              ELSE
                               p_process_status
                            END)
          ,status = (CASE
                      WHEN status = 'E' THEN
                       'E'
                      WHEN p_status_flag = 'NA' THEN
                       status
                      ELSE
                       p_status_flag
                    END)
          ,error_message  = decode(p_upd_mod
                                  ,'A'
                                  ,error_message || '|' || p_msg
                                  ,'R'
                                  ,p_msg)
     WHERE ROWID = p_id;
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      print_log('Error while updating org staging table: ' || SQLERRM);
      --RAISE;
  END;

  ---
  -- PROCEDURE: Update Locator Staging Table 
  ---
  PROCEDURE update_loc_stg_table(p_id             ROWID
                                ,p_process_status VARCHAR2
                                ,p_status_flag    VARCHAR2
                                ,p_msg            VARCHAR2
                                ,p_upd_mod        VARCHAR2 DEFAULT 'A') IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
  
    UPDATE xxwc.xxwc_ahh_items_loc_conv_stg
       SET process_status = decode(process_status
                                  ,'ERR'
                                  ,'ERR'
                                  ,p_process_status)
          ,status         = decode(status
                                  ,'E'
                                  ,'E'
                                  ,p_status_flag)
          ,error_message  = decode(p_upd_mod
                                  ,'A'
                                  ,error_message || '|' || p_msg
                                  ,'R'
                                  ,p_msg)
     WHERE ROWID = p_id;
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      print_log('Error while updating staging table: ' || SQLERRM);
      RAISE;
  END;

  ---
  -- PROCEDURE: Convert Varchar2 into Number function 
  ---
  FUNCTION convert_number(pid     IN ROWID
                         ,p_col   IN VARCHAR2
                         ,p_value IN VARCHAR2) RETURN NUMBER IS
    l_ret_value NUMBER;
  BEGIN
    SELECT to_number(p_value)
      INTO l_ret_value
      FROM dual;
    RETURN l_ret_value;
  EXCEPTION
    WHEN invalid_number THEN
      IF pid IS NOT NULL THEN
        update_stg_table(pid
                        ,g_prs_sts
                        ,g_inp_psts
                        ,'|numbererror-' || p_col || '-' || p_value || '|');
      END IF;
      RETURN NULL;
    WHEN OTHERS THEN
      IF pid IS NOT NULL THEN
        update_stg_table(pid
                        ,g_prs_sts
                        ,g_inp_psts
                        ,'|error-' || p_col || '-' || p_value || '-' || SQLERRM);
      END IF;
      RETURN NULL;
  END convert_number;

  PROCEDURE cleanup_tables IS
  
    l_cnt       NUMBER := NULL;
    sql_stmt    VARCHAR2(1000) := NULL; -- chr(13)
    usql_stmt   VARCHAR2(1000) := NULL; -- chr(13)
    tsql_stmt   VARCHAR2(1000) := NULL; -- chr(10)
    tusql_stmt  VARCHAR2(1000) := NULL; -- chr(10)
    l_tablename VARCHAR2(250) := 'XXX';
  
    CURSOR c_get_table_columns_list IS
      SELECT owner
            ,table_name
            ,column_name
        FROM all_tab_columns
       WHERE owner || '.' || table_name IN
             ('XXWC.XXWC_ITEM_MASTER_REF'
             ,'XXWC.XXWC_INVITEM_STG'
             ,'XXWC.XXWC_INVITEM_XREF_REF'
             ,'XXWC.XXWC_INVITEM_XREF_STG'
             ,'XXWC.XXWC_ONHAND_BAL_CNV'
             ,'XXWC.XXWC_ORG_ITEM_REF'
             ,'XXWC.XXWC_AP_BRANCH_LIST_STG_TBL'
             ,'XXWC.XXWC_AP_GL_MAP_STG_TBL')
       ORDER BY table_name
               ,column_name;
  BEGIN
    --
    print_log(' ****** Cleanup Tables *******');
    print_log(rpad('-'
                  ,100
                  ,'-'));
  
    FOR i IN c_get_table_columns_list LOOP
      --
      IF l_tablename <> i.owner || '.' || i.table_name THEN
        l_tablename := i.owner || '.' || i.table_name;
      
        print_log(' Scanning through..' || i.owner || '.' || i.table_name);
      END IF;
      --
      sql_stmt := 'SELECT COUNT(1) FROM ' || i.owner || '.' || i.table_name || ' WHERE ' ||
                  i.column_name || ' LIKE ''%'' || CHR(13) || ''%''';
      --
      EXECUTE IMMEDIATE sql_stmt
        INTO l_cnt;
      --
      IF l_cnt > 0 THEN
        --
        usql_stmt := 'UPDATE ' || i.owner || '.' || i.table_name || ' SET ' || i.column_name ||
                     ' = TRIM(REPLACE(' || i.column_name || ',CHR(13),''''))' || ' WHERE ' ||
                     i.column_name || ' LIKE ''%'' || CHR(13) || ''%''';
        --
        print_log(' ***    Updated chr(13) ' || l_cnt || ' records for ' || i.owner || '.' ||
                  i.table_name || ' - ' || i.column_name);
        --
        EXECUTE IMMEDIATE usql_stmt;
        --
        COMMIT;
        --
      END IF;
      --
    
      --
      tsql_stmt := 'SELECT COUNT(1) FROM ' || i.owner || '.' || i.table_name || ' WHERE ' ||
                   i.column_name || ' LIKE ''%'' || CHR(10) || ''%''';
      --
      EXECUTE IMMEDIATE tsql_stmt
        INTO l_cnt;
      --
      IF l_cnt > 0 THEN
        --
        tusql_stmt := 'UPDATE ' || i.owner || '.' || i.table_name || ' SET ' || i.column_name ||
                      ' = TRIM(REPLACE(' || i.column_name || ',CHR(10),''''))' || ' WHERE ' ||
                      i.column_name || ' LIKE ''%'' || CHR(10) || ''%''';
        --
        print_log(' ***    Updated chr(10)' || l_cnt || ' records for ' || i.owner || '.' ||
                  i.table_name || ' - ' || i.column_name);
        --
        EXECUTE IMMEDIATE tusql_stmt;
        --
        COMMIT;
        --
      END IF;
      --
      COMMIT;
    END LOOP;
    print_log(rpad('-'
                  ,100
                  ,'-'));
    print_log(' ****** Cleanup Tables *******');
    print_log(' ');
  END cleanup_tables;

  ---
  -- PROCEDURE: Validations 
  ---
  PROCEDURE validations IS
    l_invitemcnt NUMBER := 0;
    l_sec        VARCHAR2(10);
  BEGIN
  
    print_log('*** XXWC_INVITEM_STG ' || ': Stats >START ***');
  
    l_sec := '10';
    -- Update All records as Success first.
    UPDATE xxwc.xxwc_invitem_stg
       SET process_status = g_cmp_psts
          ,status         = g_scs_sts;
    print_log('*** Total INVITEM Records: ' || SQL%ROWCOUNT);
  
    l_sec := '20';
    -- Check Part Numbers in ITEM MASTER REF
    UPDATE xxwc.xxwc_invitem_stg
       SET process_status = g_err_psts
          ,status         = g_fl_sts
          ,error_message  = 'Item Not exists in Ref'
     WHERE NOT EXISTS (SELECT 1
              FROM xxwc.xxwc_item_master_ref
             WHERE icsp_prod = ahh_partnumber);
    print_log('*** Records (Item not in Master Ref Table): ' || SQL%ROWCOUNT);
  
  
    l_sec := '30';
    -- Check Organizations in Branch Ref Table.
    UPDATE xxwc.xxwc_invitem_stg stg
       SET process_status = g_err_psts
          ,status         = g_fl_sts
          ,error_message  = 'Branch Does Not exist in REF'
     WHERE NOT EXISTS (SELECT 1
              FROM xxwc.xxwc_ap_branch_list_stg_tbl br
                  ,mtl_parameters                   mp
             WHERE upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
               AND br.oracle_branch_number = mp.organization_code);
    print_log('*** Records (Branch/Orcl Org Does Not exist in REF): ' || SQL%ROWCOUNT);
  
    l_sec := '40';
    SELECT COUNT(*)
      INTO l_invitemcnt
      FROM xxwc.xxwc_invitem_stg
     WHERE status = g_scs_sts;
  
    print_log('*** Total Eligible Records: ' || l_invitemcnt);
    -- 
    print_log('*** XXWC_INVITEM_STG : Stats <END ***');
  
  
  
    print_log('*** XXWC_ITEM_MASTER_REF ' || ': Stats >START ***');
    -- Item Master Ref 
    -- Primary UOM - Made NULL for Org.
    -- Desc        - Use Get_Mst_Desc
    -- Long Desc   - Use Get_Mst_Longdesc
    -- Unit Weight - Made NULL for Org.
    l_sec := '50';
    -- AVP Code    - Update chr(13) *** Should be handled by cleanup_tables routine.
    /*UPDATE xxwc.xxwc_item_master_ref mref
       SET mref.wc_avp_code = TRIM(REPLACE(mref.wc_avp_code
                                          ,chr(13)
                                          ,''))
     WHERE mref.wc_avp_code LIKE '%' || chr(13) || '%';
    print_log('*** Total Master Ref, AVP clean up, Records: ' || SQL%ROWCOUNT);*/
    -- Hazard Class- Made NULL for Org.  
    -- UN Number   - Made NULL for Org.
  
  
    print_log('*** XXWC_ITEM_MASTER_REF : Stats <END ***');
    l_sec := '60';
    --  
    --
    --cleanup_tables();
    --
    --
  EXCEPTION
    WHEN OTHERS THEN
      print_log(' **** Unknown Error in <validations>: Section : ' || l_sec || ', Err: ' ||
                substr(SQLERRM
                      ,1
                      ,100));
      RAISE;
  END validations;



  FUNCTION derive_unnumber(p_item     IN VARCHAR2
                          ,p_unnumber IN VARCHAR2) RETURN po_un_numbers.un_number_id%TYPE IS
    --
    CURSOR c_unnumber(p_unnumber VARCHAR2) IS
      SELECT un_number_id
        FROM po_un_numbers
       WHERE upper(un_number) = upper(p_unnumber);
    --
    lun_number_id po_un_numbers.un_number_id%TYPE;
    CURSOR c_get_mst_unnumber(p_segment1 VARCHAR2) IS
      SELECT msib.un_number_id
        FROM mtl_system_items_b msib
       WHERE segment1 = p_segment1
         AND organization_id = 222;
  BEGIN
    -- Get Master Record if existing.
    OPEN c_get_mst_unnumber(p_item);
    FETCH c_get_mst_unnumber
      INTO lun_number_id;
  
    IF c_get_mst_unnumber%NOTFOUND THEN
      -- Get UN Number ID from Ref table. 
      OPEN c_unnumber(p_unnumber);
      FETCH c_unnumber
        INTO lun_number_id;
      CLOSE c_unnumber;
    END IF;
  
    IF c_get_mst_unnumber%ISOPEN THEN
      CLOSE c_get_mst_unnumber;
    END IF;
  
    RETURN lun_number_id;
  EXCEPTION
    WHEN OTHERS THEN
      IF c_get_mst_unnumber%ISOPEN THEN
        CLOSE c_get_mst_unnumber;
      END IF;
      --
      IF c_unnumber%ISOPEN THEN
        CLOSE c_unnumber;
      END IF;
      --
      RETURN NULL;
  END derive_unnumber;


  FUNCTION derive_hazard_class_id(p_item         IN VARCHAR2
                                 ,p_hazard_class IN VARCHAR2)
    RETURN po_hazard_classes.hazard_class_id%TYPE IS
  
    CURSOR c_hazard_class(p_hazardclass VARCHAR2) IS
      SELECT hazard_class_id -- added to remove default hazard class id
        FROM po_hazard_classes
       WHERE upper(hazard_class) = upper(p_hazardclass);
    l_hazard_class_id po_hazard_classes.hazard_class_id%TYPE;
  
    CURSOR c_get_mst_hzdclass(p_segment1 VARCHAR2) IS
      SELECT msib.hazard_class_id
        FROM mtl_system_items_b msib
       WHERE segment1 = p_segment1
         AND organization_id = 222;
  BEGIN
    -- Get Master Record if existing.
    OPEN c_get_mst_hzdclass(p_item);
    FETCH c_get_mst_hzdclass
      INTO l_hazard_class_id;
  
    IF c_get_mst_hzdclass%NOTFOUND THEN
      -- Derive when no Master Record  
      OPEN c_hazard_class(p_hazard_class);
      FETCH c_hazard_class
        INTO l_hazard_class_id;
      CLOSE c_hazard_class;
    END IF;
    --
    IF c_get_mst_hzdclass%ISOPEN THEN
      CLOSE c_get_mst_hzdclass;
    END IF;
    --
  
    RETURN l_hazard_class_id;
  EXCEPTION
    WHEN OTHERS THEN
      --
      IF c_get_mst_hzdclass%ISOPEN THEN
        CLOSE c_get_mst_hzdclass;
      END IF;
      --
      RETURN NULL;
    
  END derive_hazard_class_id;

  FUNCTION item_not_exists(p_mtl_system_items_int mtl_system_items_interface%ROWTYPE) RETURN BOOLEAN IS
    l_int_count   NUMBER := 0;
    l_item_exists NUMBER := 0;
  BEGIN
    -- Check if a pending record is available in Interface Table.
    SELECT COUNT(*)
      INTO l_int_count
      FROM mtl_system_items_interface
     WHERE segment1 = p_mtl_system_items_int.segment1
       AND organization_code = p_mtl_system_items_int.organization_code
       AND process_flag = 1;
  
    -- Skip this check if its already there in the Interface Table.
    IF l_int_count = 0 THEN
      SELECT COUNT(*)
        INTO l_item_exists
        FROM mtl_system_items_b
       WHERE segment1 = p_mtl_system_items_int.segment1
         AND organization_id =
             (SELECT organization_id
                FROM mtl_parameters
               WHERE organization_code = p_mtl_system_items_int.organization_code);
    END IF;
    --
    IF l_int_count > 0
       OR l_item_exists > 0 THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
    --
  EXCEPTION
    WHEN OTHERS THEN
      RETURN FALSE;
  END item_not_exists;

  FUNCTION item_cat_not_exists(p_mtl_item_categories_int mtl_item_categories_interface%ROWTYPE)
    RETURN BOOLEAN IS
    l_int_count   NUMBER := 0;
    l_item_exists NUMBER := 0;
  BEGIN
    -- Check if a pending record is available in Interface Table.
    SELECT COUNT(*)
      INTO l_int_count
      FROM mtl_item_categories_interface
     WHERE item_number = p_mtl_item_categories_int.item_number
       AND organization_code = p_mtl_item_categories_int.organization_code
       AND process_flag = 1
       AND category_set_name = p_mtl_item_categories_int.category_set_name;
  
    -- Skip this check if its already there in the Interface Table.
    IF l_int_count = 0 THEN
      SELECT COUNT(*)
        INTO l_item_exists
        FROM mtl_item_categories      mic
            ,mtl_system_items_b       msib
            ,mtl_parameters           mp
            ,inv.mtl_category_sets_tl csets
       WHERE mic.inventory_item_id = msib.inventory_item_id
         AND mic.organization_id = msib.organization_id
         AND msib.organization_id = mp.organization_id
         AND msib.segment1 = p_mtl_item_categories_int.item_number
         AND mp.organization_code = p_mtl_item_categories_int.organization_code
         AND mic.category_set_id = csets.category_set_id
         AND csets.language = userenv('LANG')
         AND csets.category_set_name = p_mtl_item_categories_int.category_set_name;
    END IF;
    --
    IF l_int_count > 0
       OR l_item_exists > 0 THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
    --
  EXCEPTION
    WHEN OTHERS THEN
      RETURN FALSE;
  END item_cat_not_exists;



  FUNCTION return_flm(p_xxwc_invitem_stg gt_xxwc_invitem_stg_t
                     ,p_org_code         VARCHAR2 DEFAULT NULL) RETURN NUMBER IS
    l_flm NUMBER;
  
  BEGIN
    FOR i IN p_xxwc_invitem_stg.first .. p_xxwc_invitem_stg.last LOOP
      BEGIN
        l_flm := to_number(p_xxwc_invitem_stg(i).fixed_lot);
        IF l_flm IS NOT NULL THEN
          EXIT;
        END IF;
      EXCEPTION
        WHEN invalid_number THEN
          NULL;
      END;
    END LOOP;
    RETURN l_flm;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END return_flm;

  FUNCTION get_mst_desc(p_item VARCHAR2
                       ,p_desc VARCHAR2) RETURN VARCHAR2 IS
  
    CURSOR c_get_desc(p_segment1 VARCHAR2) IS
      SELECT description
        FROM mtl_system_items_b
       WHERE segment1 = p_segment1
         AND organization_id = 222;
    --
    l_desc mtl_system_items_b.description%TYPE;
    --
  BEGIN
    --
    OPEN c_get_desc(p_item);
    FETCH c_get_desc
      INTO l_desc;
    --  
    IF c_get_desc%NOTFOUND THEN
      l_desc := nvl(p_desc
                   ,p_item);
    END IF;
    --  
    IF c_get_desc%ISOPEN THEN
      CLOSE c_get_desc;
    END IF;
    --  
    RETURN l_desc;
  EXCEPTION
    WHEN OTHERS THEN
      --
      IF c_get_desc%ISOPEN THEN
        CLOSE c_get_desc;
      END IF;
      --
      RETURN NULL;
    
  END get_mst_desc;

  FUNCTION get_mst_longdesc(p_item     VARCHAR2
                           ,p_longdesc VARCHAR2) RETURN VARCHAR2 IS
  
    CURSOR c_get_longdesc(p_segment1 VARCHAR2) IS
      SELECT msit.long_description
        FROM mtl_system_items_b  msib
            ,mtl_system_items_tl msit
       WHERE msib.organization_id = msit.organization_id
         AND msib.inventory_item_id = msit.inventory_item_id
         AND msit.language = userenv('LANG')
         AND msib.segment1 = p_segment1
         AND msib.organization_id = 222;
    --
    l_longdesc mtl_system_items_tl.long_description%TYPE;
    --
  BEGIN
    --
    OPEN c_get_longdesc(p_item);
    FETCH c_get_longdesc
      INTO l_longdesc;
    --  
    IF c_get_longdesc%NOTFOUND THEN
      l_longdesc := p_longdesc;
    END IF;
    --  
    IF c_get_longdesc%ISOPEN THEN
      CLOSE c_get_longdesc;
    END IF;
    --  
    RETURN l_longdesc;
  
  EXCEPTION
    WHEN OTHERS THEN
      --
      IF c_get_longdesc%ISOPEN THEN
        CLOSE c_get_longdesc;
      END IF;
      --
      RETURN NULL;
    
  END get_mst_longdesc;

  FUNCTION derive_avp_code(p_avp_code IN VARCHAR2) RETURN VARCHAR2 IS
    l_ret_avp  VARCHAR2(250);
    l_avp_code VARCHAR2(250);
  BEGIN
    l_avp_code := p_avp_code;
    --
    IF l_avp_code LIKE '%' || chr(13) || '%' THEN
      l_avp_code := REPLACE(l_avp_code
                           ,chr(13)
                           ,'');
    
    END IF;
    --
    l_ret_avp := CASE
                   WHEN upper(l_avp_code) = 'INTANGIBLE' THEN
                    NULL
                   WHEN upper(l_avp_code) = 'RENTAL' THEN
                    NULL
                   ELSE
                    upper(l_avp_code)
                 END;
  
    RETURN l_ret_avp;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END derive_avp_code;

  FUNCTION derive_item_type(p_item_type IN VARCHAR2) RETURN VARCHAR2 IS
    l_item_type fnd_lookup_values.lookup_code%TYPE;
  BEGIN
    SELECT lookup_code
      INTO l_item_type
      FROM fnd_lookup_values lkp
     WHERE lookup_type = 'ITEM_TYPE'
       AND upper(meaning) = TRIM(upper(p_item_type))
       AND lkp.language = userenv('LANG')
       AND lkp.enabled_flag = 'Y'
       AND SYSDATE BETWEEN nvl(lkp.start_date_active
                              ,SYSDATE - 1) AND
           nvl(lkp.end_date_active
              ,SYSDATE + 1);
  
    RETURN l_item_type;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END;
  FUNCTION derive_user_item_type(p_internal_item_type IN VARCHAR2) RETURN VARCHAR2 IS
    l_user_item_type fnd_lookup_values.meaning%TYPE;
  BEGIN
    SELECT meaning
      INTO l_user_item_type
      FROM fnd_lookup_values lkp
     WHERE lookup_type = 'ITEM_TYPE'
       AND upper(lookup_code) = TRIM(upper(p_internal_item_type))
       AND lkp.language = userenv('LANG')
       AND lkp.enabled_flag = 'Y'
       AND SYSDATE BETWEEN nvl(lkp.start_date_active
                              ,SYSDATE - 1) AND
           nvl(lkp.end_date_active
              ,SYSDATE + 1);
  
    RETURN l_user_item_type;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END;

  FUNCTION derive_template_id(p_item_type   IN VARCHAR2
                             ,p_item_status IN VARCHAR2) RETURN NUMBER IS
  
    FUNCTION ret_template_id(p_template_name IN VARCHAR2) RETURN NUMBER IS
      l_template_id mtl_item_templates_b.template_id%TYPE;
    BEGIN
      SELECT template_id
        INTO l_template_id
        FROM mtl_item_templates_vl tt
       WHERE upper(tt.template_name) = upper(p_template_name);
      RETURN l_template_id;
    
    EXCEPTION
      WHEN OTHERS THEN
        RETURN NULL;
    END;
  BEGIN
    RETURN(CASE WHEN p_item_status = 'ACTIVE' AND p_item_type = 'NON-STOCK ITEM' THEN
           ret_template_id('ACTIVE-NON-STOCK ITEM') WHEN
           p_item_status = 'ACTIVE' AND p_item_type = 'STOCK ITEM' THEN
           ret_template_id('ACTIVE-STOCK ITEM') WHEN
           p_item_status = 'ACTIVE' AND p_item_type = 'RENTAL' THEN
           ret_template_id('ACTIVE-RENTAL') WHEN
           p_item_status = 'DISCONTINU' AND p_item_type = 'NON-STOCK ITEM' THEN
           ret_template_id('DISCONTINU-NON-STOCK ITEM') WHEN
           p_item_status = 'DISCONTINU' AND p_item_type = 'STOCK ITEM' THEN
           ret_template_id('DISCONTINU-STOCK ITEM') WHEN
           p_item_status = 'INTANGIBLE' AND p_item_type = 'RENTAL' THEN
           ret_template_id('INTANGIBLE-RENTAL') WHEN
           p_item_status = 'INACTIVE' AND p_item_type = 'NON-STOCK ITEM' THEN
           ret_template_id('INACTIVE-NON-STOCK ITEM') WHEN
           p_item_status = 'ACTIVE' AND p_item_type = 'REPAIR' THEN
           ret_template_id('ACTIVE-REPAIR') WHEN
           p_item_status = 'INTANGIBLE' AND p_item_type = 'NON-STOCK ITEM' THEN
           ret_template_id('INTANGIBLE-NON-STOCK ITEM') WHEN
           p_item_status = 'ACTIVE' AND p_item_type = 'CONSIGNED' THEN
           ret_template_id('ACTIVE-CONSIGNED') WHEN
           p_item_status = 'INTANGIBLE' AND p_item_type = 'INTANGIBLE' THEN
           ret_template_id('INTANGIBLE-INTANGIBLE') ELSE NULL END);
  
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END;

  FUNCTION derive_mst_template_id(p_item_type   IN VARCHAR2
                                 ,p_item_status IN VARCHAR2) RETURN NUMBER IS
  
    FUNCTION ret_template_id(p_template_name IN VARCHAR2) RETURN NUMBER IS
      l_template_id mtl_item_templates_b.template_id%TYPE;
    BEGIN
      SELECT template_id
        INTO l_template_id
        FROM mtl_item_templates_vl tt
       WHERE upper(tt.template_name) = upper(p_template_name);
      RETURN l_template_id;
    
    EXCEPTION
      WHEN OTHERS THEN
        RETURN NULL;
    END;
  BEGIN
    RETURN(CASE WHEN p_item_status = 'ACTIVE' AND p_item_type = 'NON-STOCK ITEM' THEN
           ret_template_id('MST-ACTIVE-NON-STOCK ITEM') WHEN
           p_item_status = 'ACTIVE' AND p_item_type = 'STOCK ITEM' THEN
           ret_template_id('MST-ACTIVE-STOCK ITEM') WHEN
           p_item_status = 'ACTIVE' AND p_item_type = 'RENTAL' THEN
           ret_template_id('MST-ACTIVE-RENTAL') WHEN
           p_item_status = 'DISCONTINU' AND p_item_type = 'NON-STOCK ITEM' THEN
           ret_template_id('MST-DISCONTINU-NON-STOCK ITEM') WHEN
           p_item_status = 'DISCONTINU' AND p_item_type = 'STOCK ITEM' THEN
           ret_template_id('MST-DISCONTINU-STOCK ITEM') WHEN
           p_item_status = 'INTANGIBLE' AND p_item_type = 'RENTAL' THEN
           ret_template_id('MST-INTANGIBLE-RENTAL') WHEN
           p_item_status = 'INACTIVE' AND p_item_type = 'NON-STOCK ITEM' THEN
           ret_template_id('MST-INACTIVE-NON-STOCK ITEM') WHEN
           p_item_status = 'ACTIVE' AND p_item_type = 'REPAIR' THEN
           ret_template_id('MST-ACTIVE-REPAIR') WHEN
           p_item_status = 'INTANGIBLE' AND p_item_type = 'NON-STOCK ITEM' THEN
           ret_template_id('MST-INTANGIBLE-NON-STOCK ITEM') WHEN
           p_item_status = 'ACTIVE' AND p_item_type = 'CONSIGNED' THEN
           ret_template_id('MST-ACTIVE-CONSIGNED') WHEN
           p_item_status = 'INTANGIBLE' AND p_item_type = 'INTANGIBLE' THEN
           ret_template_id('MST-INTANGIBLE-INTANGIBLE') ELSE NULL END);
  
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END;

  FUNCTION derive_uom_code(p_uom IN VARCHAR2) RETURN VARCHAR2 IS
    l_uom_code mtl_units_of_measure.uom_code%TYPE;
  BEGIN
    SELECT uom_code
      INTO l_uom_code
      FROM mtl_units_of_measure
     WHERE upper(unit_of_measure) = upper(p_uom)
       AND rownum < 2;
    RETURN l_uom_code;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END;


  FUNCTION derive_category_id(p_category_concat IN VARCHAR2
                             ,p_category_set    IN VARCHAR2) RETURN VARCHAR2 IS
    l_category_id mtl_categories_b.category_id%TYPE;
  
    CURSOR c_get_cat(p_category_cat IN VARCHAR2) IS
      SELECT category_id
        FROM mtl_categories_b  cats
            ,mtl_category_sets cset
       WHERE segment1 || '.' || segment2 = TRIM(p_category_cat)
         AND cats.structure_id = cset.structure_id
         AND cset.category_set_name = p_category_set
         AND rownum < 2;
  
  BEGIN
    OPEN c_get_cat(p_category_concat);
    FETCH c_get_cat
      INTO l_category_id;
    CLOSE c_get_cat;
  
    IF l_category_id IS NULL THEN
      OPEN c_get_cat('N.');
      FETCH c_get_cat
        INTO l_category_id;
      CLOSE c_get_cat;
    END IF;
  
    RETURN l_category_id;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END;

  FUNCTION derive_orcl_org(ahh_org_code IN VARCHAR2) RETURN VARCHAR2 IS
    l_org_code mtl_parameters.organization_code%TYPE;
  BEGIN
  
    SELECT br.oracle_branch_number
      INTO l_org_code
      FROM xxwc.xxwc_ap_branch_list_stg_tbl br
          ,mtl_parameters                   mp
     WHERE upper(br.ahh_warehouse_number) = upper(TRIM(ahh_org_code))
       AND mp.organization_code = br.oracle_branch_number;
  
    IF l_org_code IS NULL THEN
      RETURN NULL;
    ELSE
      RETURN l_org_code;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
    
  END derive_orcl_org;

  FUNCTION derive_orcl_org_id(p_inv_org IN VARCHAR2) RETURN VARCHAR2 IS
    --
    l_org_code_id mtl_parameters.organization_id%TYPE;
    --
  BEGIN
  
    SELECT mp.organization_id
      INTO l_org_code_id
      FROM xxwc.xxwc_ap_branch_list_stg_tbl br
          ,mtl_parameters                   mp
     WHERE upper(br.ahh_warehouse_number) = upper(TRIM(p_inv_org))
       AND mp.organization_code = br.oracle_branch_number;
  
    IF l_org_code_id IS NULL THEN
      RETURN NULL;
    ELSE
      RETURN l_org_code_id;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
    
  END derive_orcl_org_id;

  FUNCTION derive_ccid(p_inv_org   IN VARCHAR2
                      ,p_ccid_name IN VARCHAR2) RETURN NUMBER IS
    --
    l_ret_cogs_ccid  mtl_parameters.cost_of_sales_account%TYPE;
    l_ret_sales_ccid mtl_parameters.sales_account%TYPE;
    --
  BEGIN
  
    SELECT mp.cost_of_sales_account
          ,mp.sales_account
      INTO l_ret_cogs_ccid
          ,l_ret_sales_ccid
      FROM mtl_parameters mp
     WHERE mp.organization_code = p_inv_org;
  
    -- Validation of CCID and create if needed
    -- postponed.
  
    IF p_ccid_name = 'COGS' THEN
      RETURN l_ret_cogs_ccid;
    ELSIF p_ccid_name = 'SALES' THEN
      RETURN l_ret_sales_ccid;
    ELSE
      RETURN NULL;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END derive_ccid;

  FUNCTION derive_ref_item_type(p_icsw_statusty IN VARCHAR2) RETURN VARCHAR2 IS
    l_item_type     VARCHAR2(100);
    l_int_item_type VARCHAR2(100);
  BEGIN
  
    SELECT itmorg.ebs_value
      INTO l_item_type
      FROM xxwc.xxwc_org_item_ref itmorg
     WHERE itmorg.ahh_attribute = 'ICSW.STATUSTY'
       AND itmorg.ahh_attr_value = upper(substr(p_icsw_statusty
                                               ,1
                                               ,1));
  
    IF l_item_type IS NOT NULL THEN
      l_int_item_type := derive_item_type(l_item_type);
    END IF;
  
    RETURN l_int_item_type;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'NON-STOCK';
  END derive_ref_item_type;

  /*FUNCTION derive_salesvelocity(pahh_organization_code VARCHAR2
                               ,p_icsp_prod            VARCHAR2
                               ,p_xxwc_invitem_stg     gt_xxwc_invitem_stg_t) RETURN VARCHAR2 IS
  BEGIN
    FOR i IN p_xxwc_invitem_stg.first .. p_xxwc_invitem_stg.last LOOP
      IF p_xxwc_invitem_stg(i).organization_code = pahh_organization_code
          AND p_xxwc_invitem_stg(i).ahh_partnumber  = p_icsp_prod THEN
          
          p_xxwc_invitem_stg(i).salesvelocitystorecount 
          
      
      END IF;
    END LOOP;
  
  END derive_salesvelocity; */
  FUNCTION derive_salesvelocity(p_sv        IN VARCHAR2
                               ,p_item_type IN VARCHAR2) RETURN VARCHAR2 IS
  
    CURSOR c_sales IS
      SELECT REPLACE(itmref.ebs_value
                    ,chr(13)
                    ,'') ebs_value
            ,convert_number(NULL
                           ,NULL
                           ,REPLACE(itmref.ahh_attr_value
                                   ,chr(13)
                                   ,'')) ahh_attr_value
        FROM xxwc.xxwc_org_item_ref itmref
       WHERE itmref.ahh_attribute = 'ICSWU.LINEHITS'
         AND convert_number(NULL
                           ,NULL
                           ,REPLACE(itmref.ahh_attr_value
                                   ,chr(13)
                                   ,'')) IS NOT NULL
       ORDER BY 2 ASC;
  
    l_sv_value xxwc.xxwc_org_item_ref.ebs_value%TYPE;
  BEGIN
    /*
    (VALUE/3) > 8 1
    (VALUE/3) > 4 2
    (VALUE/3) > 2 3
    (VALUE/3) > 1 4
    (VALUE/3) > 0.25  5
    (VALUE/3) > 0.125 6
    (VALUE/3) > 0 7
    (VALUE/3) = 0 C
    ICSW.STATUSTY = NONSTOCK  N
      */
    FOR r IN c_sales LOOP
      IF r.ahh_attr_value IS NOT NULL THEN
        BEGIN
          IF round(to_number(p_sv)
                  ,3) > r.ahh_attr_value THEN
            l_sv_value := r.ebs_value;
          END IF;
        EXCEPTION
          WHEN invalid_number THEN
            RETURN NULL;
        END;
      END IF;
    END LOOP;
    --  
    IF p_item_type = 'NON-STOCK' THEN
      l_sv_value := 'N';
    END IF;
    --
    RETURN l_sv_value;
    --
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END derive_salesvelocity;

  FUNCTION derive_source_type(p_src_from VARCHAR2) RETURN NUMBER IS
    l_source_type VARCHAR2(100);
  BEGIN
    --
    SELECT (CASE rr.ahh_attr_value
             WHEN 'V' THEN
              2
             WHEN 'W' THEN
              1
             ELSE
              2 -- default supplier
           END) src_type
      INTO l_source_type
      FROM xxwc.xxwc_org_item_ref rr
     WHERE rr.ahh_attribute = 'ICSW.ARPTYPE'
       AND rr.ahh_attr_value = upper(TRIM(p_src_from))
       AND rownum < 2;
    -- 
    RETURN l_source_type;
    --
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END derive_source_type;

  FUNCTION derive_source_org(p_src_type IN NUMBER
                            ,p_inv_org  IN VARCHAR2) RETURN NUMBER IS
    l_src_org_id NUMBER;
  BEGIN
  
    IF p_src_type = 1 THEN
      l_src_org_id := derive_orcl_org_id(p_inv_org);
    ELSE
      l_src_org_id := NULL;
    END IF;
  
    RETURN l_src_org_id;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END derive_source_org;

  --
  --
  --
  PROCEDURE post_run_org_interface IS
  
    v_count NUMBER := 0;
    --
    CURSOR c IS
      SELECT msii.organization_code
            ,msi.*
        FROM mtl_system_items_b         msi
            ,mtl_system_items_interface msii
       WHERE msi.segment1 = msii.segment1
         AND msi.organization_id = 222
         AND set_process_id <> 100
         AND process_flag = 1;
    --
  BEGIN
  
    print_log(' ********* Post_Run_Org_Interface -> Start ********* ');
    FOR msib IN c LOOP
      UPDATE mtl_system_items_interface
         SET primary_uom_code               = msib.primary_uom_code
            ,allowed_units_lookup_code      = msib.allowed_units_lookup_code
            ,description                    = msib.description
            ,tracking_quantity_ind          = msib.tracking_quantity_ind
            ,ont_pricing_qty_source         = msib.ont_pricing_qty_source
            ,secondary_default_ind          = msib.secondary_default_ind
            ,secondary_uom_code             = msib.secondary_uom_code
            ,dual_uom_deviation_high        = msib.dual_uom_deviation_high
            ,dual_uom_deviation_low         = msib.dual_uom_deviation_low
            ,inventory_item_flag            = msib.inventory_item_flag
            ,revision_qty_control_code      = msib.revision_qty_control_code
            ,lot_control_code               = msib.lot_control_code
            ,start_auto_lot_number          = msib.start_auto_lot_number
            ,auto_lot_alpha_prefix          = msib.auto_lot_alpha_prefix
            ,serial_number_control_code     = msib.serial_number_control_code
            ,start_auto_serial_number       = msib.start_auto_serial_number
            ,auto_serial_alpha_prefix       = msib.auto_serial_alpha_prefix
            ,shelf_life_code                = msib.shelf_life_code
            ,shelf_life_days                = msib.shelf_life_days
            ,restrict_subinventories_code   = msib.restrict_subinventories_code
            ,location_control_code          = msib.location_control_code
            ,restrict_locators_code         = msib.restrict_locators_code
            ,reservable_type                = msib.reservable_type
            ,default_material_status_id     = msib.default_material_status_id
            ,bom_item_type                  = msib.bom_item_type
            ,base_item_id                   = msib.base_item_id
            ,effectivity_control            = msib.effectivity_control
            ,auto_created_config_flag       = msib.auto_created_config_flag
            ,config_model_type              = msib.config_model_type
            ,config_orgs                    = msib.config_orgs
            ,config_match                   = msib.config_match
            ,default_include_in_rollup_flag = msib.default_include_in_rollup_flag
            ,std_lot_size                   = msib.std_lot_size
            ,purchasing_item_flag           = msib.purchasing_item_flag
            ,un_number_id                   = msib.un_number_id
            ,hazard_class_id                = msib.hazard_class_id
            ,rounding_factor                = msib.rounding_factor
            ,outside_operation_flag         = msib.outside_operation_flag
            ,outside_operation_uom_type     = msib.outside_operation_uom_type
            ,invoice_close_tolerance        = msib.invoice_close_tolerance
            ,weight_uom_code                = msib.weight_uom_code
            ,unit_weight                    = msib.unit_weight
            ,volume_uom_code                = msib.volume_uom_code
            ,unit_volume                    = msib.unit_volume
            ,container_item_flag            = msib.container_item_flag
            ,vehicle_item_flag              = msib.vehicle_item_flag
            ,maximum_load_weight            = msib.maximum_load_weight
            ,minimum_fill_percent           = msib.minimum_fill_percent
            ,internal_volume                = msib.internal_volume
            ,container_type_code            = msib.container_type_code
            ,collateral_flag                = msib.collateral_flag
            ,event_flag                     = msib.event_flag
            ,electronic_flag                = msib.electronic_flag
            ,downloadable_flag              = msib.downloadable_flag
            ,indivisible_flag               = msib.indivisible_flag
            ,dimension_uom_code             = msib.dimension_uom_code
            ,unit_length                    = msib.unit_length
            ,unit_width                     = msib.unit_width
            ,unit_height                    = msib.unit_height
            ,mrp_planning_code              = msib.mrp_planning_code
            ,customer_order_flag            = msib.customer_order_flag
            ,atp_flag                       = msib.atp_flag
            ,atp_components_flag            = msib.atp_components_flag
            ,ship_model_complete_flag       = msib.ship_model_complete_flag
            ,returnable_flag                = msib.returnable_flag
            ,contract_item_type_code        = msib.contract_item_type_code
            ,service_duration_period_code   = msib.service_duration_period_code
            ,service_duration               = msib.service_duration
            ,coverage_schedule_id           = msib.coverage_schedule_id
            ,serv_req_enabled_code          = msib.serv_req_enabled_code
            ,serviceable_product_flag       = msib.serviceable_product_flag
            ,material_billable_flag         = msib.material_billable_flag
            ,serv_billing_enabled_flag      = msib.serv_billing_enabled_flag
            ,service_starting_delay         = msib.service_starting_delay
       WHERE segment1 = msib.segment1
         AND organization_code = msib.organization_code
         AND set_process_id <> 100
         AND process_flag = 1;
    
      v_count := v_count + SQL%ROWCOUNT;
    END LOOP;
  
    print_log(' ****      Number of records updated-' || v_count);
    COMMIT;
    print_log(' ********* Post_Run_Org_Interface -> End ********* ');
  EXCEPTION
    WHEN OTHERS THEN
      print_log(' Error in Post_Run_Org_Interface - ' ||
                substr(SQLERRM
                      ,1
                      ,2000));
      print_log(' ********* Post_Run_Org_Interface -> Abort ********* ');
  END post_run_org_interface;

  PROCEDURE populate_item_categories(pid                       ROWID
                                    ,p_mtl_item_categories_int mtl_item_categories_interface%ROWTYPE
                                    ,x_out_flag                OUT VARCHAR2) IS
    lr_mtl_item_categories_int mtl_item_categories_interface%ROWTYPE;
  
    l_log_msg VARCHAR2(500);
    l_sec     VARCHAR2(10);
  
    --
    PROCEDURE update_stg(p_sts  VARCHAR2
                        ,p_psts VARCHAR2
                        ,p_msg  VARCHAR2) IS
    BEGIN
      IF p_mtl_item_categories_int.organization_code = 'MST' THEN
        g_stg_log_msg := g_stg_log_msg || l_log_msg;
        /*update_stg_table(pid
        ,p_sts
        ,p_psts
        ,p_msg);*/
      ELSE
        g_org_stg_log_msg := g_org_stg_log_msg || l_log_msg;
        /*update_org_stg_table(pid
        ,p_psts
        ,p_sts
        ,p_msg);*/
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    --
  BEGIN
    l_sec := '10';
  
    lr_mtl_item_categories_int := p_mtl_item_categories_int;
    --
    lr_mtl_item_categories_int.process_flag     := 1;
    lr_mtl_item_categories_int.transaction_type := 'CREATE';
    --lr_mtl_item_categories_int.set_process_id   := 100;
  
    l_sec := '20';
    IF NOT g_validation_only THEN
      l_sec := '30';
      INSERT INTO mtl_item_categories_interface
      VALUES lr_mtl_item_categories_int;
    
      l_log_msg := l_log_msg || '| ICat-Insrtd';
    
    ELSE
      l_sec     := '40';
      l_log_msg := l_log_msg || 'VOnly,' || p_mtl_item_categories_int.organization_code ||
                   '.ICat-Ins-Skp';
    END IF;
    l_sec := '50';
    --
    update_stg('S'
              ,pid
              ,l_log_msg);
    x_out_flag := 'S';
    --
  EXCEPTION
    WHEN OTHERS THEN
      l_log_msg := l_log_msg || '| ' || p_mtl_item_categories_int.organization_code || '-ICat-Err:' ||
                   l_sec || ',' || substr(SQLERRM
                                         ,1
                                         ,100);
      update_stg('E'
                ,pid
                ,l_log_msg);
      x_out_flag := 'E';
  END populate_item_categories;

  PROCEDURE insert_org_conv_stg(p_id               ROWID
                               ,p_orcl_item        VARCHAR2
                               ,p_xxwc_invitem_stg gt_xxwc_invitem_stg_t
                               ,x_out_flag         OUT VARCHAR2) IS
    lt_xxwc_invitem_stg      gt_xxwc_invitem_stg_t;
    l_orcl_organization_code mtl_parameters.organization_code%TYPE;
  BEGIN
    lt_xxwc_invitem_stg := p_xxwc_invitem_stg;
  
    -- Derive Oracle organization Code and Update org Conv Table.
    FOR i IN lt_xxwc_invitem_stg.first .. lt_xxwc_invitem_stg.last LOOP
      --  
      l_orcl_organization_code := derive_orcl_org(lt_xxwc_invitem_stg(i).organization_code);
      --    
      INSERT INTO xxwc.xxwc_ahh_items_org_conv_stg
        (batch_id
        ,orcl_organization_code
        ,mrid
        ,item
        ,partnumber
        ,ahh_partnumber
        ,description
        ,extendeddescription
        ,mst_item_status_code
        ,company_num
        ,org_item_status_code
        ,shelf_life_code
        ,vendor_owner_num
        ,vendor_name
        ,vendorpartnumber
        ,fixed_lot
        ,unitofmeasure
        ,product_line
        ,listprice
        ,blank1
        ,unitlength
        ,unitheight
        ,unitwidth
        ,unitweight
        ,product_category
        ,organization_code
        ,org_item_status_code1
        ,sourcing_from
        ,inv_org
        ,blank2
        ,org_fixed_lot
        ,velocityclassification
        ,inventory_planning_code
        ,threshold
        ,minmaxmin
        ,minmaxmax
        ,blank3
        ,source_org_vendor_num
        ,blank4
        ,blank5
        ,usage
        ,leadtime
        ,reviewtime
        ,blank6
        ,buyer
        ,blank7
        ,blank8
        ,blank9
        ,blank10
        ,blank11
        ,blank12
        ,blank13
        ,blank14
        ,blank15
        ,blank16
        ,blank17
        ,blank18
        ,blank19
        ,blank20
        ,blank21
        ,category
        ,blank22
        ,nontaxty
        ,taxablety
        ,blank23
        ,taxgroup
        ,blank24
        ,taxtype
        ,itemlocation1
        ,itemlocation2
        ,itemlocation3
        ,itemlocation4
        ,itemlocation5
        ,qty_onhand
        ,qty_unavailable
        ,avgcost
        ,val_from_icrtt
        ,salesvelocityyearlystorecount
        ,dotcode
        ,shippingnm
        ,freightclass
        ,hmcolumnentry
        ,unnumber
        ,ergno
        ,techemnm
        ,attribute17
        ,hazardyn
        ,hazardclass
        ,attribute9
        ,nmfccode
        ,enterdt
        ,icsw_minthresexdt
        ,process_status
        ,error_message
        ,status)
      VALUES
        (g_batch_id --batch_id
        ,l_orcl_organization_code --orcl_organization_code
        ,p_id --mrid
        ,p_orcl_item --item
        ,lt_xxwc_invitem_stg(i).partnumber
        ,lt_xxwc_invitem_stg(i).ahh_partnumber
        ,lt_xxwc_invitem_stg(i).description
        ,lt_xxwc_invitem_stg(i).extendeddescription
        ,lt_xxwc_invitem_stg(i).mst_item_status_code
        ,lt_xxwc_invitem_stg(i).company_num
        ,lt_xxwc_invitem_stg(i).org_item_status_code
        ,lt_xxwc_invitem_stg(i).shelf_life_code
        ,lt_xxwc_invitem_stg(i).vendor_owner_num
        ,lt_xxwc_invitem_stg(i).vendor_name
        ,lt_xxwc_invitem_stg(i).vendorpartnumber
        ,convert_number(p_id
                       ,'fixed_lot'
                       ,lt_xxwc_invitem_stg(i).fixed_lot)
        ,lt_xxwc_invitem_stg(i).unitofmeasure
        ,lt_xxwc_invitem_stg(i).product_line
        ,convert_number(p_id
                       ,'listprice'
                       ,lt_xxwc_invitem_stg(i).listprice)
        ,lt_xxwc_invitem_stg(i).blank1
        ,convert_number(p_id
                       ,'unitlength'
                       ,lt_xxwc_invitem_stg(i).unitlength)
        ,convert_number(p_id
                       ,'unitheight'
                       ,lt_xxwc_invitem_stg(i).unitheight)
        ,convert_number(p_id
                       ,'unitwidth'
                       ,lt_xxwc_invitem_stg(i).unitwidth)
        ,convert_number(p_id
                       ,'unitweight'
                       ,lt_xxwc_invitem_stg(i).unitweight)
        ,lt_xxwc_invitem_stg(i).product_category
        ,lt_xxwc_invitem_stg(i).organization_code
        ,lt_xxwc_invitem_stg(i).org_item_status_code1
        ,lt_xxwc_invitem_stg(i).sourcing_from
        ,lt_xxwc_invitem_stg(i).inv_org
        ,lt_xxwc_invitem_stg(i).blank2
        ,convert_number(p_id
                       ,'org_fixed_lot'
                       ,lt_xxwc_invitem_stg(i).org_fixed_lot)
        ,lt_xxwc_invitem_stg(i).velocityclassification
        ,lt_xxwc_invitem_stg(i).inventory_planning_code
        ,lt_xxwc_invitem_stg(i).threshold
        ,convert_number(p_id
                       ,'minmaxmin'
                       ,lt_xxwc_invitem_stg(i).minmaxmin)
        ,convert_number(p_id
                       ,'minmaxmax'
                       ,lt_xxwc_invitem_stg(i).minmaxmax)
        ,lt_xxwc_invitem_stg(i).blank3
        ,lt_xxwc_invitem_stg(i).source_org_vendor_num
        ,lt_xxwc_invitem_stg(i).blank4
        ,lt_xxwc_invitem_stg(i).blank5
        ,lt_xxwc_invitem_stg(i).usage
        ,convert_number(p_id
                       ,'leadtime'
                       ,lt_xxwc_invitem_stg(i).leadtime)
        ,convert_number(p_id
                       ,'reviewtime'
                       ,lt_xxwc_invitem_stg(i).reviewtime)
        ,lt_xxwc_invitem_stg(i).blank6
        ,lt_xxwc_invitem_stg(i).buyer
        ,lt_xxwc_invitem_stg(i).blank7
        ,lt_xxwc_invitem_stg(i).blank8
        ,lt_xxwc_invitem_stg(i).blank9
        ,lt_xxwc_invitem_stg(i).blank10
        ,lt_xxwc_invitem_stg(i).blank11
        ,lt_xxwc_invitem_stg(i).blank12
        ,lt_xxwc_invitem_stg(i).blank13
        ,lt_xxwc_invitem_stg(i).blank14
        ,lt_xxwc_invitem_stg(i).blank15
        ,lt_xxwc_invitem_stg(i).blank16
        ,lt_xxwc_invitem_stg(i).blank17
        ,lt_xxwc_invitem_stg(i).blank18
        ,lt_xxwc_invitem_stg(i).blank19
        ,lt_xxwc_invitem_stg(i).blank20
        ,lt_xxwc_invitem_stg(i).blank21
        ,lt_xxwc_invitem_stg(i).category
        ,lt_xxwc_invitem_stg(i).blank22
        ,lt_xxwc_invitem_stg(i).nontaxty
        ,lt_xxwc_invitem_stg(i).taxablety
        ,lt_xxwc_invitem_stg(i).blank23
        ,lt_xxwc_invitem_stg(i).taxgroup
        ,lt_xxwc_invitem_stg(i).blank24
        ,lt_xxwc_invitem_stg(i).taxtype
        ,lt_xxwc_invitem_stg(i).itemlocation1
        ,lt_xxwc_invitem_stg(i).itemlocation2
        ,lt_xxwc_invitem_stg(i).itemlocation3
        ,lt_xxwc_invitem_stg(i).itemlocation4
        ,lt_xxwc_invitem_stg(i).itemlocation5
        ,lt_xxwc_invitem_stg(i).qty_onhand
        ,lt_xxwc_invitem_stg(i).qty_unavailable
        ,to_char(round(convert_number(p_id
                                     ,'avgcost'
                                     ,lt_xxwc_invitem_stg(i).avgcost)
                      ,5))
        ,lt_xxwc_invitem_stg(i).val_from_icrtt
        ,lt_xxwc_invitem_stg(i).salesvelocityyearlystorecount
        ,lt_xxwc_invitem_stg(i).dotcode
        ,lt_xxwc_invitem_stg(i).shippingnm
        ,lt_xxwc_invitem_stg(i).freightclass
        ,lt_xxwc_invitem_stg(i).hmcolumnentry
        ,lt_xxwc_invitem_stg(i).unnumber
        ,lt_xxwc_invitem_stg(i).ergno
        ,lt_xxwc_invitem_stg(i).techemnm
        ,lt_xxwc_invitem_stg(i).attribute17
        ,lt_xxwc_invitem_stg(i).hazardyn
        ,lt_xxwc_invitem_stg(i).hazardclass
        ,lt_xxwc_invitem_stg(i).attribute9
        ,lt_xxwc_invitem_stg(i).nmfccode
        ,to_date(lt_xxwc_invitem_stg(i).enterdt
                ,'MM/DD/RR')
        ,lt_xxwc_invitem_stg(i).icsw_minthresexdt
        ,g_new_psts --process_status
        ,NULL --error_message
        ,g_new_sts --status
         );
      --    
    END LOOP;
    --
    x_out_flag := 'SUCCESS';
    COMMIT;
    --
  EXCEPTION
    WHEN OTHERS THEN
      --
      x_out_flag := 'Error: Insert Org Conv Stg: ' || substr(SQLERRM
                                                            ,1
                                                            ,100);
      --
  END insert_org_conv_stg;


  PROCEDURE insert_org_items(p_id                      ROWID
                            ,p_mtl_system_items_int    mtl_system_items_interface%ROWTYPE
                            ,p_mtl_item_categories_int mtl_item_categories_interface%ROWTYPE
                            ,p_xxwc_invitem_stg        gt_xxwc_invitem_stg_t
                            ,x_out_flag                OUT VARCHAR2) IS
    --
    l_org_mtl_system_items_int_rec mtl_system_items_interface%ROWTYPE;
    l_org_mtl_item_categories_int  mtl_item_categories_interface%ROWTYPE;
    l_mst_mtl_system_items_int_rec mtl_system_items_interface%ROWTYPE;
    l_mst_mtl_item_categories_int  mtl_item_categories_interface%ROWTYPE;
  
    x_out_org_conv_msg VARCHAR2(500);
    l_sec              VARCHAR2(10);
    l_salesvelocity    VARCHAR2(100);
    -- t_mst_item_rec xxwc.xxwc_ahh_items_org_conv_stg%ROWTYPE;
    --    -r_mst_item_rec     t_mst_item_rec;
  
    CURSOR c_sel_org_items(p_item IN VARCHAR2) IS
      SELECT orgs.rowid orid
            ,row_number() over(PARTITION BY orgs.item, orgs.orcl_organization_code ORDER BY orgs.organization_code, orgs.ahh_partnumber) rn
            ,orgs.*
        FROM xxwc.xxwc_ahh_items_org_conv_stg orgs
       WHERE batch_id = g_batch_id
         AND orgs.item = p_item
         AND process_status = g_new_psts
       ORDER BY batch_id
               ,item;
  
  BEGIN
  
    --Initializations
    l_sec                          := '10';
    l_mst_mtl_system_items_int_rec := p_mtl_system_items_int;
    l_mst_mtl_item_categories_int  := p_mtl_item_categories_int;
    g_org_stg_log_msg              := NULL;
  
    --
    --
    l_sec := '20';
    insert_org_conv_stg(p_id
                       ,l_mst_mtl_system_items_int_rec.segment1
                       ,p_xxwc_invitem_stg
                       ,x_out_org_conv_msg);
  
    --
    --
    IF substr(x_out_org_conv_msg
             ,1
             ,1) = 'S' THEN
      l_sec := '30';
      -- Copy Master Item Rec
      l_org_mtl_system_items_int_rec := l_mst_mtl_system_items_int_rec;
      l_org_mtl_item_categories_int  := l_mst_mtl_item_categories_int;
    
      FOR rec_orgs IN c_sel_org_items(l_org_mtl_system_items_int_rec.segment1) LOOP
      
        BEGIN
          g_org_stg_log_msg := NULL;
          --        
          update_org_stg_table(rec_orgs.orid
                              ,g_inp_psts
                              ,g_prs_sts
                              ,g_org_stg_log_msg);
          --    
          g_org_stg_log_msg := rpad('***'
                                   ,10
                                   ,' ') || 'OrclOrg,AhhOrg,AhhPart#>Rn ' ||
                               rec_orgs.orcl_organization_code || ',' || rec_orgs.organization_code || ',' ||
                               rec_orgs.ahh_partnumber || '>' || rec_orgs.rn;
        
          l_sec := '40';
          IF rec_orgs.rn = 1 THEN
          
            IF rec_orgs.orcl_organization_code IS NOT NULL THEN
              l_sec := '50';
              -- Assign Org level  to Rec
              l_org_mtl_system_items_int_rec.organization_code := rec_orgs.orcl_organization_code;
              -- user item type 
              l_org_mtl_system_items_int_rec.item_type := derive_ref_item_type(rec_orgs.org_item_status_code);
              -- status
            
              -- Rentals Default
              IF substr(l_org_mtl_system_items_int_rec.segment1
                       ,1
                       ,2) = 'RR' THEN
                --Re-Rental
                l_org_mtl_system_items_int_rec.item_type := 'RE_RENT';
              ELSIF substr(l_org_mtl_system_items_int_rec.segment1
                          ,1
                          ,1) = 'R' THEN
                --Rental
                l_org_mtl_system_items_int_rec.item_type := 'RENTAL';
              END IF;
            
              -- LogMsg
              g_org_stg_log_msg := g_org_stg_log_msg || '| ItmTyp-' ||
                                   l_org_mtl_system_items_int_rec.item_type;
            
              l_sec := '60';
              -- Process Master Items
              IF item_not_exists(l_org_mtl_system_items_int_rec) THEN
                BEGIN
                  l_sec := '70';
                  --
                  l_org_mtl_system_items_int_rec.buyer_id               := NULL; --MST - NULL and 44388
                  l_org_mtl_system_items_int_rec.attribute14            := round(convert_number(gid
                                                                                               ,'attribute14'
                                                                                               ,rec_orgs.salesvelocityyearlystorecount));
                  l_org_mtl_system_items_int_rec.list_price_per_unit    := to_number(rec_orgs.avgcost);
                  l_org_mtl_system_items_int_rec.source_type            := derive_source_type(rec_orgs.sourcing_from);
                  g_org_stg_log_msg                                     := g_org_stg_log_msg ||
                                                                           '| InitSrcTyp-' ||
                                                                           l_org_mtl_system_items_int_rec.source_type;
                  l_org_mtl_system_items_int_rec.source_organization_id := derive_source_org(l_org_mtl_system_items_int_rec.source_type
                                                                                            ,rec_orgs.inv_org);
                
                  -- Assign Source Type as NULL when Source Organization ID is null for Supplier Source Type.
                  l_org_mtl_system_items_int_rec.source_type := CASE
                                                                  WHEN l_org_mtl_system_items_int_rec.source_type = 1
                                                                       AND
                                                                       l_org_mtl_system_items_int_rec.source_organization_id IS NULL THEN
                                                                   derive_source_type('V')
                                                                  ELSE
                                                                   l_org_mtl_system_items_int_rec.source_type
                                                                END;
                  --l_org_mtl_system_items_int_rec.inventory_item_status_code := derive_org_item_status_code();
                  l_org_mtl_system_items_int_rec.inventory_planning_code := 2;
                  l_org_mtl_system_items_int_rec.fixed_lot_multiplier    := NULL; --lt_xxwc_invitem_stg(r).org_fixed_lot;
                  l_org_mtl_system_items_int_rec.preprocessing_lead_time := NULL; --rec_orgs.reviewtime; *Make this NULL
                  l_org_mtl_system_items_int_rec.full_lead_time          := rec_orgs.leadtime;
                  l_org_mtl_system_items_int_rec.min_minmax_quantity     := rec_orgs.minmaxmin;
                  l_org_mtl_system_items_int_rec.max_minmax_quantity     := rec_orgs.minmaxmax;
                  l_org_mtl_system_items_int_rec.attribute20             := NULL;
                  l_sec                                                  := '80';
                  l_org_mtl_system_items_int_rec.template_id             := derive_template_id(upper(derive_user_item_type(l_org_mtl_system_items_int_rec.item_type))
                                                                                              ,upper(l_org_mtl_system_items_int_rec.inventory_item_status_code));
                  -- Org diff
                  /* lr_mtl_item_categories_int.category_set_id   := derive_category_set_id('Inventory');
                  lr_mtl_item_categories_int.category_id       := derivevelocity.inv_cat_class;*/
                
                  l_sec := '90';
                  --Default other variables or Master controlled attributes
                  l_org_mtl_system_items_int_rec.attribute_category := 'WC';
                  l_org_mtl_system_items_int_rec.process_flag       := 1;
                  l_org_mtl_system_items_int_rec.set_process_id := CASE
                                                                     WHEN l_org_mtl_system_items_int_rec.source_organization_id IS NULL THEN
                                                                      101
                                                                     ELSE
                                                                      102
                                                                   END;
                  --
                  l_org_mtl_system_items_int_rec.unit_weight     := NULL;
                  l_org_mtl_system_items_int_rec.weight_uom_code := NULL;
                  /*IF l_org_mtl_system_items_int_rec.unit_weight IS NOT NULL THEN
                    l_org_mtl_system_items_int_rec.weight_uom_code := 'LBS';
                  END IF;*/
                  l_org_mtl_system_items_int_rec.primary_unit_of_measure := NULL;
                  l_org_mtl_system_items_int_rec.hazard_class_id         := NULL;
                  l_org_mtl_system_items_int_rec.un_number_id            := NULL;
                  l_org_mtl_system_items_int_rec.shelf_life_days         := NULL;
                
                  l_sec := '100';
                  -- COST_OF_SALES_ACCOUNT
                  l_org_mtl_system_items_int_rec.cost_of_sales_account := derive_ccid(l_org_mtl_system_items_int_rec.organization_code
                                                                                     ,'COGS');
                  -- SALES_ACCOUNT
                  l_org_mtl_system_items_int_rec.sales_account := derive_ccid(l_org_mtl_system_items_int_rec.organization_code
                                                                             ,'SALES');
                  -- Reserve Stock
                  l_org_mtl_system_items_int_rec.attribute21 := (CASE
                                                                  WHEN nvl(to_date(rec_orgs.icsw_minthresexdt
                                                                                  ,'MM/DD/RR')
                                                                          ,trunc(SYSDATE + 1)) >= trunc(SYSDATE) THEN
                                                                   rec_orgs.threshold
                                                                  ELSE
                                                                   NULL
                                                                END);
                
                  l_sec := '101';
                  -- Rentals Default
                  IF substr(l_org_mtl_system_items_int_rec.segment1
                           ,1
                           ,1) = 'R' THEN
                    l_org_mtl_system_items_int_rec.list_price_per_unit     := 0;
                    l_org_mtl_system_items_int_rec.inventory_planning_code := 6;
                    l_org_mtl_system_items_int_rec.min_minmax_quantity     := 0;
                    l_org_mtl_system_items_int_rec.max_minmax_quantity     := 0;
                  END IF;
                
                  l_sec := '102';
                  -- RecordLogMsg
                  g_org_stg_log_msg := g_org_stg_log_msg || '| Attr14-' ||
                                       l_org_mtl_system_items_int_rec.attribute14;
                  g_org_stg_log_msg := g_org_stg_log_msg || '| Srctyp-' ||
                                       l_org_mtl_system_items_int_rec.source_type;
                  g_org_stg_log_msg := g_org_stg_log_msg || '| SrcOrg-' ||
                                       l_org_mtl_system_items_int_rec.source_organization_id;
                  g_org_stg_log_msg := g_org_stg_log_msg || '| TmplID-' ||
                                       l_org_mtl_system_items_int_rec.template_id;
                  g_org_stg_log_msg := g_org_stg_log_msg || '| SPrcID-' ||
                                       l_org_mtl_system_items_int_rec.set_process_id;
                  g_org_stg_log_msg := g_org_stg_log_msg || '| COGS-' ||
                                       l_org_mtl_system_items_int_rec.cost_of_sales_account;
                  g_org_stg_log_msg := g_org_stg_log_msg || '| SLS-' ||
                                       l_org_mtl_system_items_int_rec.sales_account;
                  g_org_stg_log_msg := g_org_stg_log_msg || '| Thrld-' ||
                                       l_org_mtl_system_items_int_rec.attribute21;
                  --
                
                  IF NOT g_validation_only THEN
                    l_sec := '105';
                    INSERT INTO mtl_system_items_interface
                    VALUES l_org_mtl_system_items_int_rec;
                  
                    g_org_stg_log_msg := g_org_stg_log_msg || '| OrgItem-Insrtd';
                  
                  ELSE
                    l_sec             := '107';
                    g_org_stg_log_msg := g_org_stg_log_msg || '| VOnly,Org-Ins-Skp';
                  END IF;
                  l_sec := '110';
                
                EXCEPTION
                  WHEN OTHERS THEN
                    RAISE;
                END;
              ELSE
                l_sec             := '120';
                x_out_flag        := 'S';
                g_org_stg_log_msg := g_org_stg_log_msg || '| OrgItem-ExistsSkp';
              
              END IF;
              l_sec := '125';
              <<orgitemcat>>
              BEGIN
                l_sec := '126';
              
                -- Rentals Default
                IF substr(l_org_mtl_system_items_int_rec.segment1
                         ,1
                         ,1) = 'R' THEN
                  --Rental
                  l_salesvelocity := 'N';
                ELSE
                  l_salesvelocity := CASE l_org_mtl_system_items_int_rec.item_type
                                       WHEN NULL THEN
                                        NULL
                                       ELSE
                                        derive_salesvelocity(rec_orgs.velocityclassification
                                                            ,l_org_mtl_system_items_int_rec.item_type)
                                     END;
                END IF;
              
                --
                l_sec                                           := '130';
                l_org_mtl_item_categories_int.organization_code := rec_orgs.orcl_organization_code;
                l_org_mtl_item_categories_int.category_set_name := 'Sales Velocity';
                l_org_mtl_item_categories_int.category_id       := derive_category_id(l_salesvelocity || '.'
                                                                                     ,l_org_mtl_item_categories_int.category_set_name);
                l_org_mtl_item_categories_int.set_process_id := CASE
                                                                  WHEN l_org_mtl_system_items_int_rec.source_organization_id IS NULL THEN
                                                                   101
                                                                  ELSE
                                                                   102
                                                                END;
              
              
              
                l_sec             := '131'; -- RecordLogMsg
                g_org_stg_log_msg := g_org_stg_log_msg || '| SlsVel-' || l_salesvelocity;
                g_org_stg_log_msg := g_org_stg_log_msg || '| SLS-' ||
                                     l_org_mtl_item_categories_int.category_id;
                g_org_stg_log_msg := g_org_stg_log_msg || '| SPrcID-' ||
                                     l_org_mtl_item_categories_int.set_process_id;
              
                IF item_cat_not_exists(l_org_mtl_item_categories_int) THEN
                  --
                  l_sec := '140';
                  populate_item_categories(rec_orgs.orid
                                          ,l_org_mtl_item_categories_int
                                          ,x_out_flag);
                
                
                ELSE
                  l_sec             := '150';
                  x_out_flag        := 'S';
                  g_org_stg_log_msg := g_org_stg_log_msg || '| Org-ICatExistsSkp';
                
                END IF;
              END orgitemcat;
              update_org_stg_table(rec_orgs.orid
                                  ,'CMP'
                                  ,'S'
                                  ,g_org_stg_log_msg);
            
            ELSE
              l_sec             := '160';
              g_org_stg_log_msg := g_org_stg_log_msg || '| OrgIDNULL';
              update_org_stg_table(rec_orgs.orid
                                  ,'CMP'
                                  ,'F'
                                  ,g_org_stg_log_msg);
            END IF; -- OrclOrgID is not Null
            l_sec := '170';
          
          ELSE
            l_sec             := '170';
            g_org_stg_log_msg := g_org_stg_log_msg || '| OrgRecDup';
            update_org_stg_table(rec_orgs.orid
                                ,'CMP'
                                ,'F'
                                ,g_org_stg_log_msg);
          END IF; -- Rn <> 1
          --
          --
          l_sec := '180';
          --g_org_stg_log_msg := g_org_stg_log_msg || '| Complete';
          update_org_stg_table(rec_orgs.orid
                              ,'NA'
                              ,'NA'
                              ,'*Complete*');
          --
          --
        EXCEPTION
          WHEN OTHERS THEN
            x_out_flag        := 'E';
            g_org_stg_log_msg := g_org_stg_log_msg || '| Org-E,' || l_sec || ' - ' ||
                                 substr(SQLERRM
                                       ,1
                                       ,100);
            update_org_stg_table(rec_orgs.orid
                                ,'ERR'
                                ,'E'
                                ,g_org_stg_log_msg);
        END;
        --
        l_sec := '190';
        print_log(g_org_stg_log_msg);
      
      --
      END LOOP;
      l_sec := '200';
    
      --
      -- Post Run Org Interface
      post_run_org_interface();
      --
      --
    
      COMMIT;
      x_out_flag := 'S';
    ELSE
      l_sec         := '210';
      x_out_flag    := 'E';
      g_stg_log_msg := g_stg_log_msg || '| ' || x_out_org_conv_msg;
    END IF;
    --
    l_sec := '220';
    --
  EXCEPTION
    WHEN OTHERS THEN
      x_out_flag    := 'E';
      g_stg_log_msg := g_stg_log_msg || '| Err-InsOrgItems: ' || l_sec || ',' ||
                       substr(SQLERRM
                             ,1
                             ,100);
    
  END insert_org_items;


  ---
  -- PROCEDURE: Insert_Items
  ---
  PROCEDURE insert_items(p_mtl_system_items_int    mtl_system_items_interface%ROWTYPE
                        ,p_mtl_item_categories_int mtl_item_categories_interface%ROWTYPE
                        ,x_out_flag                OUT VARCHAR2) IS
    --
    l_mst_mtl_system_items_int_rec mtl_system_items_interface%ROWTYPE;
    l_mtl_item_categories_int      mtl_item_categories_interface%ROWTYPE;
    l_sec                          VARCHAR2(10);
  BEGIN
    l_sec                          := '10';
    l_mst_mtl_system_items_int_rec := p_mtl_system_items_int;
    l_mtl_item_categories_int      := p_mtl_item_categories_int;
  
    -- Process Master Items
    IF item_not_exists(l_mst_mtl_system_items_int_rec) THEN
    
      BEGIN
        l_sec := '10';
        --Default other variables
        l_mst_mtl_system_items_int_rec.attribute_category := 'WC';
        l_mst_mtl_system_items_int_rec.process_flag       := 1;
        l_mst_mtl_system_items_int_rec.set_process_id     := 100;
        --
        IF l_mst_mtl_system_items_int_rec.unit_weight IS NOT NULL THEN
          l_sec                                          := '20';
          l_mst_mtl_system_items_int_rec.weight_uom_code := 'LBS';
        END IF;
        --
        -- COST_OF_SALES_ACCOUNT - No Derivation needed
        -- SALES_ACCOUNT - No Derivation Needed
      
        l_sec := '30';
        IF NOT g_validation_only THEN
          l_sec := '40';
          INSERT INTO mtl_system_items_interface
          VALUES l_mst_mtl_system_items_int_rec;
          --
          g_stg_log_msg := g_stg_log_msg || '| MST-Insrtd';
        ELSE
          l_sec         := '50';
          g_stg_log_msg := g_stg_log_msg || '| VOnly,MST-Ins-Skp';
        END IF;
        l_sec := '60';
      
      EXCEPTION
        WHEN OTHERS THEN
          RAISE;
      END;
      l_sec := '70';
    
    ELSE
      l_sec         := '80';
      g_stg_log_msg := g_stg_log_msg || '| MST-ExistsSkp';
    
    END IF;
    l_sec := '90';
  
    -- Process Master Item Categories
    IF item_cat_not_exists(l_mtl_item_categories_int) THEN
      l_mtl_item_categories_int.set_process_id := 100;
    
      l_sec := '100';
      populate_item_categories(gid
                              ,l_mtl_item_categories_int
                              ,x_out_flag);
    
    ELSE
      l_sec         := '110';
      g_stg_log_msg := g_stg_log_msg || '| MST-ICatExistsSkp';
    
    END IF;
    l_sec := '120';
  
    x_out_flag := 'S';
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      g_stg_log_msg := g_stg_log_msg || '| Err-MSTInsrt,' || l_sec || ',' ||
                       substr(SQLERRM
                             ,1
                             ,100);
      x_out_flag    := 'E';
  END insert_items;

  ---
  -- PROCEDURE: Process_Item 
  ---
  PROCEDURE process_item(errbuf               OUT VARCHAR2
                        ,retcode              OUT NUMBER
                        ,p_validation_only    IN VARCHAR2
                        ,p_include_mst_items  IN VARCHAR2
                        ,p_include_org_items  IN VARCHAR2
                        ,p_include_categories IN VARCHAR2
                        ,p_item1              IN VARCHAR2
                        ,p_item2              IN VARCHAR2
                        ,p_item3              IN VARCHAR2) IS
    l_sec VARCHAR2(10);
  
    -- Load Master Items Cursor
    CURSOR c_load_mst_items(p_batch_id     IN VARCHAR2
                           ,p_processed_by IN VARCHAR2) IS
      SELECT p_batch_id batch_id
            ,'MST' organization_code
            ,icsp_prod
            ,item
            ,icsp_descrip1
            ,description
            ,icsp_descrip2
            ,long_description
            ,icsp_statusty
            ,item_status
            ,user_item_type
            ,icsw_statusty
            ,icsp_lifocat
            ,shelf_life_days
            ,icsw_arpvendno
            ,vendor_owner_number
            ,apsv_name
            ,vendor_name
            ,icsw_vendprod
            ,vendor_part_number
            ,icsp_unitstock
            ,oracle_primary_unit_of_measure
            ,icsp_length
            ,unit_length
            ,icsp_height
            ,unit_height
            ,icsp_width
            ,unit_width
            ,icsp_weight
            ,unit_weight
            ,part_type
            ,icsp_user1_dot_code
            ,inv_cat_class
            ,final_catmgt_grp
            ,final_catmgt_subcategory_desc
            ,final_catclass_description
            ,icsw_nontaxty
            ,icsw_taxablety
            ,icsw_taxgroup
            ,icsw_taxtype
            ,hazmat_dot_code
            ,hamat_description
            ,hazmat_flag
            ,hazard_class
            ,enterdt
            ,create_date
            ,wc_avp_code
            ,SYSDATE processed_date
            ,p_processed_by processed_by
            ,'N' validation_flag
            ,'NEW' status
            ,NULL error_message
        FROM xxwc.xxwc_item_master_ref
       WHERE item IS NOT NULL
         AND item IN
             (p_item1
             ,p_item2
             ,p_item3
             ,(CASE WHEN p_item1 IS NULL AND p_item2 IS NULL AND p_item3 IS NULL THEN item ELSE NULL END));
  
    TYPE rec_data_t IS TABLE OF c_load_mst_items%ROWTYPE;
    lrec_items rec_data_t;
  
    -- Select MST Items 
    CURSOR c_sel_items(p_batch_id IN NUMBER) IS
      SELECT stg.rowid id
            ,row_number() over(PARTITION BY item ORDER BY icsp_prod) rn
            ,stg.*
        FROM xxwc.xxwc_ahh_items_conv_stg stg
       WHERE status = 'NEW'
         AND batch_id = p_batch_id;
  
    -- Get AHH Part Number records
    CURSOR c_inv_stg(p_item_number IN VARCHAR2) IS
      SELECT /*+ ALL_ROWS */
       *
        FROM xxwc_invitem_stg
       WHERE EXISTS (SELECT 1
                FROM xxwc.xxwc_item_master_ref
               WHERE item = p_item_number
                 AND icsp_prod = ahh_partnumber)
         AND status = g_scs_sts;
  
  
    l_array_size     NUMBER := 50000;
    l_purge_old_recs VARCHAR2(1) := 'Y';
    l_reload         VARCHAR2(1) := 'Y';
    l_batch_id       NUMBER;
    l_processed_by   VARCHAR2(30);
    l_val_ret_flag   VARCHAR2(1);
    l_val_ret_msg    VARCHAR2(2000);
    l_log_msg        VARCHAR2(3000);
    l_mst_ret_flag   VARCHAR2(100);
    l_org_ret_flag   VARCHAR2(100);
  
    lr_mtl_system_items_int   mtl_system_items_interface%ROWTYPE;
    lorg_mtl_system_items_int mtl_system_items_interface%ROWTYPE;
  
    lr_mtl_item_categories_int   mtl_item_categories_interface%ROWTYPE;
    lorg_mtl_item_categories_int mtl_item_categories_interface%ROWTYPE;
  
  
    lt_xxwc_invitem_stg gt_xxwc_invitem_stg_t;
    l_cnt               NUMBER;
  
    --
    FUNCTION set_gbl_params(p_param VARCHAR2) RETURN BOOLEAN IS
    BEGIN
      RETURN(CASE nvl(p_param
                ,'N') WHEN 'Y' THEN TRUE ELSE FALSE END);
    EXCEPTION
      WHEN OTHERS THEN
        RETURN FALSE;
    END;
    --
  BEGIN
    l_sec := '10';
    --Initializations
    l_batch_id     := fnd_global.conc_request_id;
    g_batch_id     := l_batch_id;
    l_processed_by := fnd_global.user_name;
  
  
    g_validation_only    := set_gbl_params(p_validation_only);
    g_include_mst_items  := set_gbl_params(p_include_mst_items);
    g_include_org_items  := set_gbl_params(p_include_org_items);
    g_include_categories := set_gbl_params(p_include_categories);
  
    lt_xxwc_invitem_stg := gt_xxwc_invitem_stg_t();
    lrec_items          := rec_data_t();
    l_sec               := '20';
  
    -- Control Test Variables.
    /* l_reload         := 'N';
    l_purge_old_recs := 'N';
    l_batch_id       := 211064669;*/
    --
  
    -- Load Master Ref Staging table into Temp table for processing.
    BEGIN
    
      l_sec := '30';
      IF l_purge_old_recs = 'Y' THEN
        -- archive first
        INSERT INTO xxwc.xxwc_ahh_items_conv_stgarc
          SELECT *
            FROM xxwc.xxwc_ahh_items_conv_stg;
      
        INSERT INTO xxwc.xxwc_ahh_items_org_conv_stgarc
          SELECT *
            FROM xxwc.xxwc_ahh_items_org_conv_stg;
      
        COMMIT;
        -- clear table
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_AHH_ITEMS_CONV_STG';
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_AHH_ITEMS_ORG_CONV_STG';
      
        -- backup interface tables, this is Pre-request snapshot.
        EXECUTE IMMEDIATE 'INSERT INTO XXWC.XXWC_MSII_ARCHIVE_ALL SELECT :reqid ,MSII.* FROM MTL_SYSTEM_ITEMS_INTERFACE MSII'
          USING l_batch_id;
        EXECUTE IMMEDIATE 'INSERT INTO XXWC.XXWC_MICI_ARCHIVE_ALL SELECT :reqid REQUESTID ,MICI.* FROM MTL_ITEM_CATEGORIES_INTERFACE MICI'
          USING l_batch_id;
        EXECUTE IMMEDIATE 'INSERT INTO XXWC.XXWC_MIE_ARCHIVE_ALL SELECT :reqid ,MIE.* FROM MTL_INTERFACE_ERRORS MIE'
          USING l_batch_id;
        EXECUTE IMMEDIATE 'INSERT INTO XXWC.XXWC_MIRI_ARCHIVE_ALL SELECT :reqid ,MIRI.* FROM INV.MTL_ITEM_REVISIONS_INTERFACE MIRI'
          USING l_batch_id;
      
        COMMIT;
      
        EXECUTE IMMEDIATE 'TRUNCATE TABLE INV.MTL_SYSTEM_ITEMS_INTERFACE';
        EXECUTE IMMEDIATE 'TRUNCATE TABLE INV.MTL_INTERFACE_ERRORS';
        EXECUTE IMMEDIATE 'TRUNCATE TABLE INV.MTL_ITEM_REVISIONS_INTERFACE';
        EXECUTE IMMEDIATE 'TRUNCATE TABLE INV.MTL_ITEM_CATEGORIES_INTERFACE';
      
      END IF;
    
      IF l_reload = 'Y' THEN
        l_sec := '40';
        OPEN c_load_mst_items(l_batch_id
                             ,l_processed_by);
        LOOP
        
          l_sec := '50';
          FETCH c_load_mst_items BULK COLLECT
            INTO lrec_items LIMIT l_array_size;
        
          l_sec := '60';
          FORALL i IN 1 .. lrec_items.count
            INSERT INTO xxwc.xxwc_ahh_items_conv_stg
            VALUES lrec_items
              (i);
          --
          COMMIT;
          --
          EXIT WHEN c_load_mst_items%NOTFOUND;
          l_sec := '70';
        END LOOP;
        CLOSE c_load_mst_items;
        l_sec := '80';
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        print_log('Failed in loading temp staging table');
        RAISE;
      
    END;
  
    l_cnt := 0;
    --
  
    --    * Will check for duplicate Oracle Item Numbers and AHH Part Numbers.
    --    * Missing Items
    --    * Cleanup Columns
    validations();
    --
    --
  
    -- Parameter validations
    IF NOT g_include_mst_items
       AND g_include_org_items THEN
      print_log('**** Incorrect Parameter selection, Include Master Items should always be selected when Include Org Items is Selected');
      print_log('**** Its Ok if the Items already exist in Item Master, the code will just skip and go to - Org Items.');
    
    ELSIF NOT g_include_mst_items
          AND NOT g_include_org_items
          AND g_include_categories THEN
      print_log('**** Incorrect Parameter selection, Include Master Items and/or Include Org Items should always be selected when Include item Categories is Selected');
      print_log('**** If Include Org Items is selected as No, the categories for Org Items will not be created');
    
    ELSE
      print_log('**** XXWC AHH INV Item Conversion Program - Started');
    
      l_sec := '90';
      FOR rec_items IN c_sel_items(l_batch_id) LOOP
      
        --initializations
        lr_mtl_system_items_int      := NULL;
        lr_mtl_item_categories_int   := NULL;
        lorg_mtl_system_items_int    := NULL;
        lorg_mtl_item_categories_int := NULL;
        l_val_ret_flag               := NULL;
        l_val_ret_msg                := NULL;
        lt_xxwc_invitem_stg          := NULL;
        gid                          := rec_items.id; -- Need to fix this. Use G_Log_Msg
        g_stg_log_msg                := NULL;
      
        l_sec := '100';
        update_stg_table(gid
                        ,'P'
                        ,'INP'
                        ,g_stg_log_msg);
        --
        g_stg_log_msg := rec_items.organization_code || '.' || rec_items.item || '>';
        --
      
        -- Master Items
        IF g_include_mst_items THEN
        
          -- PickFirst record
          IF rec_items.rn = 1 THEN
            g_stg_log_msg := g_stg_log_msg || '1 |';
            BEGIN
              l_sec := '110';
              -- Get STG records
              OPEN c_inv_stg(rec_items.item);
              --
              FETCH c_inv_stg BULK COLLECT
                INTO lt_xxwc_invitem_stg;
              --
              IF c_inv_stg%ROWCOUNT = 0 THEN
                lt_xxwc_invitem_stg.extend;
              END IF;
              --
              CLOSE c_inv_stg;
            
              l_cnt         := l_cnt + 1;
              g_stg_log_msg := g_stg_log_msg || '| m-' ||
                               (l_cnt || ':c-' || lt_xxwc_invitem_stg.count);
            
              l_sec := '120';
              -- Common Set Mtl System Items variable.
              lr_mtl_system_items_int.organization_code := rec_items.organization_code;
              lr_mtl_system_items_int.segment1          := rec_items.item;
              lr_mtl_system_items_int.description       := get_mst_desc(rec_items.item
                                                                       ,rec_items.description);
            
              lr_mtl_system_items_int.hazard_class_id         := derive_hazard_class_id(rec_items.item
                                                                                       ,rec_items.hazard_class);
              lr_mtl_system_items_int.un_number_id            := derive_unnumber(rec_items.item
                                                                                ,lt_xxwc_invitem_stg(1)
                                                                                 .unnumber);
              lr_mtl_system_items_int.shelf_life_days         := rec_items.shelf_life_days;
              lr_mtl_system_items_int.unit_weight             := rec_items.unit_weight;
              lr_mtl_system_items_int.primary_unit_of_measure := rec_items.oracle_primary_unit_of_measure;
              lr_mtl_system_items_int.transaction_type        := 'CREATE';
              lr_mtl_system_items_int.long_description        := get_mst_longdesc(rec_items.item
                                                                                 ,rec_items.long_description);
              lr_mtl_system_items_int.unit_length             := CASE nvl(rec_items.unit_length
                                                                     ,0)
                                                                   WHEN 0 THEN
                                                                    NULL
                                                                   ELSE
                                                                    rec_items.unit_length
                                                                 END;
              lr_mtl_system_items_int.unit_width              := CASE nvl(rec_items.unit_width
                                                                     ,0)
                                                                   WHEN 0 THEN
                                                                    NULL
                                                                   ELSE
                                                                    rec_items.unit_width
                                                                 END;
              lr_mtl_system_items_int.unit_height             := CASE nvl(rec_items.unit_height
                                                                     ,0)
                                                                   WHEN 0 THEN
                                                                    NULL
                                                                   ELSE
                                                                    rec_items.unit_height
                                                                 END;
              lr_mtl_system_items_int.dimension_uom_code := (CASE
                                                              WHEN nvl(lr_mtl_system_items_int.unit_length
                                                                      ,0) = 0
                                                                   AND nvl(lr_mtl_system_items_int.unit_width
                                                                          ,0) = 0
                                                                   AND nvl(lr_mtl_system_items_int.unit_height
                                                                          ,0) = 0 THEN
                                                               NULL
                                                              ELSE
                                                               derive_uom_code(rec_items.oracle_primary_unit_of_measure)
                                                            END);
              lr_mtl_system_items_int.attribute16             := rec_items.icsp_prod;
              lr_mtl_system_items_int.attribute17             := rec_items.hamat_description;
              lr_mtl_system_items_int.attribute22             := derive_avp_code(rec_items.wc_avp_code);
              lr_mtl_system_items_int.hazardous_material_flag := rec_items.hazmat_flag;
            
              l_sec := '130';
              -- Same but Derived
              lr_mtl_system_items_int.attribute9 := lt_xxwc_invitem_stg(1).attribute9;
            
              l_sec := '140';
              --Different MST
              lr_mtl_system_items_int.item_type := derive_item_type(rec_items.user_item_type);
            
              -- Rentals Default
              IF substr(lr_mtl_system_items_int.segment1
                       ,1
                       ,2) = 'RR' THEN
                --Re-Rental
                lr_mtl_system_items_int.item_type := 'RE_RENT';
                rec_items.user_item_type          := 'Re-Rental';
                -- For Rentals
              ELSIF substr(lr_mtl_system_items_int.segment1
                          ,1
                          ,1) = 'R' THEN
                --Rental
                lr_mtl_system_items_int.item_type := 'RENTAL';
                rec_items.user_item_type          := 'Rental';
              END IF;
            
              lr_mtl_system_items_int.buyer_id                   := NULL; /*MST - NULL and 44388*/
              lr_mtl_system_items_int.attribute14                := NULL;
              lr_mtl_system_items_int.list_price_per_unit        := 0;
              lr_mtl_system_items_int.source_type                := NULL; -- derive_source_type
              lr_mtl_system_items_int.source_organization_id     := NULL;
              lr_mtl_system_items_int.inventory_item_status_code := rec_items.item_status;
              lr_mtl_system_items_int.inventory_planning_code    := NULL;
              lr_mtl_system_items_int.fixed_lot_multiplier       := NULL; -- return_flm(lt_xxwc_invitem_stg);
              lr_mtl_system_items_int.preprocessing_lead_time    := NULL;
              lr_mtl_system_items_int.full_lead_time             := NULL;
              lr_mtl_system_items_int.min_minmax_quantity        := NULL;
              lr_mtl_system_items_int.max_minmax_quantity        := NULL;
              lr_mtl_system_items_int.template_id                := derive_mst_template_id(upper(rec_items.user_item_type)
                                                                                          ,upper(rec_items.item_status));
              lr_mtl_system_items_int.attribute20                := NULL;
            
              l_sec := '150';
              -- master item category
              lr_mtl_item_categories_int.organization_code := rec_items.organization_code;
              --lr_mtl_item_categories_int.category_set_id   := derive_category_set_id('Inventory Category');
              lr_mtl_item_categories_int.category_set_name := 'Inventory Category';
              lr_mtl_item_categories_int.category_id       := derive_category_id(rec_items.inv_cat_class
                                                                                ,lr_mtl_item_categories_int.category_set_name);
              lr_mtl_item_categories_int.item_number       := rec_items.item;
            
            
              -- Record Log Message
              --
              g_stg_log_msg := g_stg_log_msg || '| desc-' || lr_mtl_system_items_int.description;
              g_stg_log_msg := g_stg_log_msg || '| hcls-' ||
                               lr_mtl_system_items_int.hazard_class_id;
              g_stg_log_msg := g_stg_log_msg || '| un#-' || lr_mtl_system_items_int.un_number_id;
              g_stg_log_msg := g_stg_log_msg || '| ldesc-' ||
                               lr_mtl_system_items_int.long_description;
              g_stg_log_msg := g_stg_log_msg || '| duom-' ||
                               lr_mtl_system_items_int.dimension_uom_code;
              g_stg_log_msg := g_stg_log_msg || '| avp-' || lr_mtl_system_items_int.attribute22;
              g_stg_log_msg := g_stg_log_msg || '| ityp-' || lr_mtl_system_items_int.item_type;
              g_stg_log_msg := g_stg_log_msg || '| tmpl-' || lr_mtl_system_items_int.template_id;
              g_stg_log_msg := g_stg_log_msg || '| icatid-' ||
                               lr_mtl_item_categories_int.category_id;
              --
              --
              l_mst_ret_flag := 'N';
              l_sec          := '160';
              insert_items(lr_mtl_system_items_int
                          ,lr_mtl_item_categories_int
                          ,l_mst_ret_flag);
              --
              --
              g_stg_log_msg := g_stg_log_msg || '| InsCall-' || l_mst_ret_flag;
              print_log(g_stg_log_msg); -- May be print after insert org recs
              --
              --
            
              IF g_include_org_items THEN
                l_sec := '180';
                --
                lorg_mtl_system_items_int    := lr_mtl_system_items_int;
                lorg_mtl_item_categories_int := lr_mtl_item_categories_int;
                --
              
                IF lt_xxwc_invitem_stg.count > 0
                   AND lt_xxwc_invitem_stg(1).ahh_partnumber IS NOT NULL THEN
                  l_sec := '190';
                  --
                  --
                  insert_org_items(rec_items.id
                                  ,lorg_mtl_system_items_int
                                  ,lorg_mtl_item_categories_int
                                  ,lt_xxwc_invitem_stg
                                  ,l_org_ret_flag);
                  --
                  --
                  IF l_org_ret_flag = 'S' THEN
                    l_sec         := '200';
                    g_stg_log_msg := g_stg_log_msg || '| Org-S';
                  ELSE
                    l_sec         := '210';
                    g_stg_log_msg := g_stg_log_msg || '| Org-Ins-E';
                  END IF;
                ELSE
                  l_sec         := '220';
                  g_stg_log_msg := g_stg_log_msg || '| Org-SkpAll-NA';
                
                END IF;
                l_sec := '230';
              ELSE
                g_stg_log_msg := g_stg_log_msg || '| Org-SkipFlag';
              END IF;
              l_sec := '240';
            
              --
            END;
            l_sec := '270';
            update_stg_table(gid
                            ,(CASE WHEN l_mst_ret_flag <> 'S' OR l_org_ret_flag <> 'S' THEN 'E' ELSE 'S' END)
                            ,'CMP'
                            ,g_stg_log_msg);
          ELSE
            g_stg_log_msg := g_stg_log_msg || '>' || rec_items.rn || '| Dup Record Skip *Complete*';
            print_log(g_stg_log_msg);
            update_stg_table(gid
                            ,'F'
                            ,'CMP'
                            ,g_stg_log_msg);
          
          END IF; -- << rec_items.rn = 1 >>
        
        ELSE
          g_stg_log_msg := g_stg_log_msg || '| Skipping Master Items Insert';
          print_log(g_stg_log_msg);
          update_stg_table(gid
                          ,'S'
                          ,'CMP'
                          ,g_stg_log_msg);
        END IF; -- << g_include_mst_items = FALSE >>
      
        -- Final Update
        update_stg_table(gid
                        ,'NA'
                        ,'NA'
                        ,'*Complete*');
      
        l_sec := '280';
        COMMIT;
        l_sec := '290';
      END LOOP;
      --
      l_sec := '300';
    END IF;
    l_sec := '310';
  EXCEPTION
    WHEN OTHERS THEN
      retcode := 2;
      errbuf  := SQLERRM;
      print_log('Error at section ' || l_sec || '-' || SQLERRM);
  END process_item;


  PROCEDURE process_item_locators(errbuf  OUT VARCHAR2
                                 ,retcode OUT VARCHAR2) IS
    l_return_status         VARCHAR2(1) := fnd_api.g_ret_sts_success;
    l_msg_count             NUMBER;
    l_msg_data              VARCHAR2(4000);
    l_subinventory_code     VARCHAR2(20);
    l_inventory_location_id NUMBER;
    l_locator_exists        VARCHAR2(1);
    l_itemreturn_status     VARCHAR2(1) := fnd_api.g_ret_sts_success;
    l_itemmsg_count         NUMBER DEFAULT 0;
    l_itemmsg_data          VARCHAR2(4000) DEFAULT NULL;
    l_msg_index_out         NUMBER;
    l_error_msg             VARCHAR2(2000) := NULL;
    l_err_flag              VARCHAR2(1) := 'N';
    l_err_cnt               NUMBER := 0;
    l_scs_cnt               NUMBER := 0;
    l_pcs_cnt               NUMBER := 0;
    l_sd_cnt                NUMBER := 0;
  
    CURSOR c_locator_stg(p_batch_id     IN NUMBER
                        ,p_processed_by IN VARCHAR2) IS
      SELECT p_batch_id
            ,mp.organization_code organization_code
            ,segment1 segment1
            ,stg.ahh_partnumber ahh_partnumber
            ,br.ahh_warehouse_number ahh_organization_code
            ,'1-' || TRIM(itemlocation1) || '.' itemlocation
            ,msib.inventory_item_id
            ,msib.organization_id
            ,SYSDATE
            ,p_processed_by
            ,g_new_psts
            ,g_new_sts
            ,NULL
        FROM xxwc.xxwc_invitem_stg            stg
            ,xxwc.xxwc_item_master_ref        refe
            ,xxwc.xxwc_ap_branch_list_stg_tbl br
            ,mtl_parameters                   mp
            ,mtl_system_items_b               msib
       WHERE ahh_partnumber = refe.icsp_prod
         AND upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
         AND br.oracle_branch_number = mp.organization_code
         AND refe.item = msib.segment1
         AND mp.organization_id = msib.organization_id
         AND TRIM(itemlocation1) IS NOT NULL
      UNION
      SELECT p_batch_id
            ,mp.organization_code organization_code
            ,segment1 segment1
            ,stg.ahh_partnumber ahh_partnumber
            ,br.ahh_warehouse_number ahh_organization_code
            ,'2-' || TRIM(itemlocation2) || '.' itemlocation
            ,msib.inventory_item_id
            ,msib.organization_id
            ,SYSDATE
            ,p_processed_by
            ,g_new_psts
            ,g_new_sts
            ,NULL
        FROM xxwc.xxwc_invitem_stg            stg
            ,xxwc.xxwc_item_master_ref        refe
            ,xxwc.xxwc_ap_branch_list_stg_tbl br
            ,mtl_parameters                   mp
            ,mtl_system_items_b               msib
       WHERE ahh_partnumber = refe.icsp_prod
         AND upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
         AND br.oracle_branch_number = mp.organization_code
         AND refe.item = msib.segment1
         AND mp.organization_id = msib.organization_id
         AND TRIM(itemlocation2) IS NOT NULL
      UNION
      SELECT p_batch_id
            ,mp.organization_code organization_code
            ,segment1 segment1
            ,stg.ahh_partnumber ahh_partnumber
            ,br.ahh_warehouse_number ahh_organization_code
            ,'3-' || TRIM(itemlocation3) || '.' itemlocation
            ,msib.inventory_item_id
            ,msib.organization_id
            ,SYSDATE
            ,p_processed_by
            ,g_new_psts
            ,g_new_sts
            ,NULL
        FROM xxwc.xxwc_invitem_stg            stg
            ,xxwc.xxwc_item_master_ref        refe
            ,xxwc.xxwc_ap_branch_list_stg_tbl br
            ,mtl_parameters                   mp
            ,mtl_system_items_b               msib
       WHERE ahh_partnumber = refe.icsp_prod
         AND upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
         AND br.oracle_branch_number = mp.organization_code
         AND refe.item = msib.segment1
         AND mp.organization_id = msib.organization_id
         AND TRIM(itemlocation3) IS NOT NULL
      UNION
      SELECT p_batch_id
            ,mp.organization_code organization_code
            ,segment1 segment1
            ,stg.ahh_partnumber ahh_partnumber
            ,br.ahh_warehouse_number ahh_organization_code
            ,'4-' || TRIM(itemlocation3) || '.' itemlocation
            ,msib.inventory_item_id
            ,msib.organization_id
            ,SYSDATE
            ,p_processed_by
            ,g_new_psts
            ,g_new_sts
            ,NULL
        FROM xxwc.xxwc_invitem_stg            stg
            ,xxwc.xxwc_item_master_ref        refe
            ,xxwc.xxwc_ap_branch_list_stg_tbl br
            ,mtl_parameters                   mp
            ,mtl_system_items_b               msib
       WHERE ahh_partnumber = refe.icsp_prod
         AND upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
         AND br.oracle_branch_number = mp.organization_code
         AND refe.item = msib.segment1
         AND mp.organization_id = msib.organization_id
         AND TRIM(itemlocation4) IS NOT NULL
      UNION
      SELECT p_batch_id
            ,mp.organization_code organization_code
            ,segment1 segment1
            ,stg.ahh_partnumber ahh_partnumber
            ,br.ahh_warehouse_number ahh_organization_code
            ,'5-' || TRIM(itemlocation3) || '.' itemlocation
            ,msib.inventory_item_id
            ,msib.organization_id
            ,SYSDATE
            ,p_processed_by
            ,g_new_psts
            ,g_new_sts
            ,NULL
        FROM xxwc.xxwc_invitem_stg            stg
            ,xxwc.xxwc_item_master_ref        refe
            ,xxwc.xxwc_ap_branch_list_stg_tbl br
            ,mtl_parameters                   mp
            ,mtl_system_items_b               msib
       WHERE ahh_partnumber = refe.icsp_prod
         AND upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
         AND br.oracle_branch_number = mp.organization_code
         AND refe.item = msib.segment1
         AND mp.organization_id = msib.organization_id
         AND TRIM(itemlocation5) IS NOT NULL;
  
  
    TYPE rec_locatordata_t IS TABLE OF c_locator_stg%ROWTYPE;
    lrec_locators rec_locatordata_t;
  
    -- Select Items Locators
    CURSOR c_sel_locs(p_batch_id IN NUMBER) IS
      SELECT stg.rowid id
            ,stg.*
        FROM xxwc.xxwc_ahh_items_loc_conv_stg stg
       WHERE status = g_new_sts
         AND batch_id = p_batch_id
      --AND stg.segment1 = '0001'
      ;
  
    l_batch_id       NUMBER;
    l_sec            VARCHAR2(10);
    l_purge_old_recs VARCHAR2(1) := 'Y';
    l_reload         VARCHAR2(1) := 'Y';
    l_processed_by   VARCHAR2(30);
    l_array_size     NUMBER := 10000;
    l_log_msg        VARCHAR2(2000);
  
    CURSOR c_sub_defaults(p_batch_id IN NUMBER) IS
      SELECT DISTINCT stg.segment1          partnumber
                     ,stg.organization_id
                     ,stg.inventory_item_id
                     ,NULL                  category
        FROM xxwc.xxwc_ahh_items_loc_conv_stg stg
       WHERE stg.status = 'S'
         AND stg.batch_id = p_batch_id
            --AND stg.segment1 = '0001'
         AND NOT EXISTS (SELECT 'x'
                FROM mtl_item_sub_defaults d
               WHERE d.inventory_item_id = stg.inventory_item_id
                 AND d.organization_id = stg.organization_id
                 AND d.default_type = 2);
  
  
  BEGIN
    mo_global.init('INV');
    l_batch_id     := fnd_global.conc_request_id;
    l_processed_by := fnd_global.user_name;
    lrec_locators  := rec_locatordata_t();
  
    BEGIN
      --l_purge_old_recs := 'N';
      l_sec := '30';
      -- Purge Staging tables
      IF l_purge_old_recs = 'Y' THEN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_AHH_ITEMS_LOC_CONV_STG';
      END IF;
    
      --  
      --
      cleanup_tables();
      --
    
      -- Load Staging Table
      IF l_reload = 'Y' THEN
        l_sec := '40';
        OPEN c_locator_stg(l_batch_id
                          ,l_processed_by);
        LOOP
        
          l_sec := '50';
          FETCH c_locator_stg BULK COLLECT
            INTO lrec_locators LIMIT l_array_size;
        
          l_sec := '60';
          FORALL i IN 1 .. lrec_locators.count
            INSERT INTO xxwc.xxwc_ahh_items_loc_conv_stg
            VALUES lrec_locators
              (i);
          --
          COMMIT;
          --
          EXIT WHEN c_locator_stg%NOTFOUND;
          l_sec := '70';
        END LOOP;
        CLOSE c_locator_stg;
        l_sec := '80';
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        print_log('Failed in loading temp staging table');
        RAISE;
      
    END;
  
    --mo_global.set_org_context('7186','0','INV');
    --      print_debug ('Item Locator Process Started......!');
    --    print_debug ('Number of processing records :' || l_processig_records);
  
    FOR r_locator_stg IN c_sel_locs(l_batch_id) LOOP
    
    
      BEGIN
        l_error_msg             := NULL;
        l_err_flag              := NULL;
        l_inventory_location_id := NULL;
        l_locator_exists        := NULL;
        l_msg_data              := NULL;
        l_msg_count             := NULL;
        l_return_status         := NULL;
        l_log_msg               := NULL;
        l_pcs_cnt               := l_pcs_cnt + 1;
      
        print_log(rpad('-'
                      ,100
                      ,'-'));
        print_debug('Item : ' || r_locator_stg.segment1 || ', Org: ' ||
                    r_locator_stg.organization_code || ', Locator: ' ||
                    r_locator_stg.item_location || '....');
      
        l_log_msg := 'Start - API';
        update_loc_stg_table(r_locator_stg.id
                            ,g_inp_psts
                            ,g_prs_sts
                            ,l_log_msg
                            ,'R');
        --
        --
        inv_loc_wms_pub.create_locator(x_return_status            => l_return_status
                                      ,x_msg_count                => l_msg_count
                                      ,x_msg_data                 => l_msg_data
                                      ,x_inventory_location_id    => l_inventory_location_id
                                      ,x_locator_exists           => l_locator_exists
                                      ,p_organization_id          => NULL
                                      ,p_organization_code        => r_locator_stg.organization_code
                                      ,p_concatenated_segments    => r_locator_stg.item_location
                                      ,p_description              => ''
                                      ,p_inventory_location_type  => NULL
                                      ,p_picking_order            => NULL
                                      ,p_location_maximum_units   => NULL
                                      ,p_subinventory_code        => 'General'
                                      ,p_location_weight_uom_code => NULL
                                      ,p_max_weight               => NULL
                                      ,p_volume_uom_code          => NULL
                                      ,p_max_cubic_area           => NULL
                                      ,p_x_coordinate             => NULL
                                      ,p_y_coordinate             => NULL
                                      ,p_z_coordinate             => NULL
                                      ,p_physical_location_id     => NULL
                                      ,p_pick_uom_code            => NULL
                                      ,p_dimension_uom_code       => NULL
                                      ,p_length                   => NULL
                                      ,p_width                    => NULL
                                      ,p_height                   => NULL
                                      ,p_status_id                => 1
                                      ,p_dropping_order           => NULL
                                      ,p_attribute_category       => ''
                                      ,p_attribute1               => ''
                                      ,p_attribute2               => ''
                                      ,p_attribute3               => ''
                                      ,p_attribute4               => ''
                                      ,p_attribute5               => ''
                                      ,p_attribute6               => ''
                                      ,p_attribute7               => ''
                                      ,p_attribute8               => ''
                                      ,p_attribute9               => ''
                                      ,p_attribute10              => ''
                                      ,p_attribute11              => ''
                                      ,p_attribute12              => ''
                                      ,p_attribute13              => ''
                                      ,p_attribute14              => ''
                                      ,p_attribute15              => ''
                                      ,p_alias                    => '');
      
        --       print_debug ('Return Status: ' || l_return_status);
      
        IF (l_return_status = 'S') THEN
          NULL;
          l_log_msg := l_log_msg || '| Locator Created';
          --Success. Process Locator Id and check if locator exists
          --commit;
          print_debug('Locator Id:' || l_inventory_location_id);
          print_debug('Locator exists:' || l_locator_exists);
        ELSE
          FOR l_index IN 1 .. l_msg_count LOOP
            l_msg_data := l_msg_data || fnd_msg_pub.get(l_index
                                                       ,'F');
          END LOOP;
          l_error_msg := l_error_msg || '-' || substr(l_msg_data
                                                     ,1
                                                     ,100);
          l_err_flag  := 'Y';
        
          l_log_msg := l_log_msg || '| Locator Failed - ' || l_error_msg;
          print_debug(' Locator Failed: ' || l_error_msg);
        END IF;
      
        l_itemreturn_status := NULL;
        l_itemmsg_count     := NULL;
        l_itemmsg_data      := NULL;
      
        inv_loc_wms_pub.create_loc_item_tie(x_return_status         => l_itemreturn_status
                                           ,x_msg_count             => l_itemmsg_count
                                           ,x_msg_data              => l_itemmsg_data
                                           ,p_inventory_item_id     => NULL
                                           ,p_item                  => r_locator_stg.segment1
                                           ,p_organization_id       => NULL
                                           ,p_organization_code     => r_locator_stg.organization_code
                                           ,p_subinventory_code     => 'General'
                                           ,p_inventory_location_id => l_inventory_location_id
                                           ,p_locator               => r_locator_stg.item_location
                                           ,p_status_id             => 1);
      
      
        IF l_itemreturn_status = 'S' THEN
        
          l_log_msg := l_log_msg || '| Locator Item Tied ';
          --Success. Process Locator Id and check if locator exists
          --commit;
          print_debug('Locator Item Tied ');
        
        
          --     COMMIT;
        ELSE
          FOR ln_index IN 1 .. l_itemmsg_count LOOP
            oe_msg_pub.get(p_msg_index     => ln_index
                          ,p_encoded       => fnd_api.g_false
                          ,p_data          => l_itemmsg_data
                          ,p_msg_index_out => l_msg_index_out);
            --l_error_msg :=                         l_error_msg || '-' || SUBSTR (l_itemmsg_data, 1, 100);
          END LOOP;
          l_error_msg := l_error_msg || '-' || substr(l_itemmsg_data
                                                     ,1
                                                     ,100);
        
        
          l_err_flag := 'Y';
          l_log_msg  := l_log_msg || '| Locator Tie Failed - ' || l_error_msg;
          print_debug(' Locator Tie Failed: ' || l_error_msg);
        
        END IF;
        --
        COMMIT;
        --    
        l_log_msg := l_log_msg || '| *Complete*';
        print_debug('*Complete* ');
      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          l_err_flag  := 'Y';
          l_error_msg := l_error_msg || '- Sec:' || l_sec || '-' ||
                         substr(SQLERRM
                               ,1
                               ,100);
          l_log_msg   := l_log_msg || '| Unknown Err - ' || l_error_msg;
          print_debug('Unknown Error ' || l_error_msg);
        
      END;
      --
      IF l_err_flag = 'Y' THEN
        l_err_cnt := l_err_cnt + 1;
      
        update_loc_stg_table(r_locator_stg.id
                            ,g_cmp_psts
                            ,g_err_sts
                            ,l_log_msg);
      
      ELSE
        l_scs_cnt := l_scs_cnt + 1;
        update_loc_stg_table(r_locator_stg.id
                            ,g_cmp_psts
                            ,g_scs_sts
                            ,l_log_msg);
      END IF;
      print_log(rpad('-'
                    ,100
                    ,'-'));
    
    END LOOP;
  
    print_debug('Locators creation Complete');
  
    print_log(rpad('*** Total Records Processed: '
                  ,40
                  ,' ') || ': ' || l_pcs_cnt);
    print_log(rpad('*** Total Records Errored: '
                  ,40
                  ,' ') || ': ' || l_err_cnt);
    print_log(rpad('*** Total Records Succesful: '
                  ,40
                  ,' ') || ': ' || l_scs_cnt);
  
    print_debug('Sub Defaults Insert : Started');
    FOR c1_rec IN c_sub_defaults(l_batch_id) LOOP
      --
      --
      BEGIN
        l_subinventory_code := 'General';
      
        INSERT INTO mtl_item_sub_defaults
          (inventory_item_id
          ,organization_id
          ,subinventory_code
          ,default_type
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login)
        VALUES
          (c1_rec.inventory_item_id
          ,c1_rec.organization_id
          ,l_subinventory_code
          ,2
          ,SYSDATE
          ,fnd_global.user_id
          ,SYSDATE
          ,fnd_global.user_id
          ,-1);
        l_sd_cnt := l_sd_cnt + 1;
      
      EXCEPTION
        WHEN dup_val_on_index THEN
        
          print_debug('                    : Sub Default Already Exists: InvItemID,Orgid' ||
                      c1_rec.inventory_item_id || ',' || c1_rec.organization_id);
        
        WHEN OTHERS THEN
          print_debug('                    : Sub Default unknown Err: InvItemID,Orgid' ||
                      c1_rec.inventory_item_id || ',' || c1_rec.organization_id || '-' || SQLERRM);
      END;
    END LOOP;
  
    --
    COMMIT;
    --
    print_debug('Sub Defaults Insert : Finished. Count: ' || l_sd_cnt);
  
  EXCEPTION
    WHEN OTHERS THEN
      retcode := 2;
      errbuf  := l_sec || '-' || SQLERRM;
      print_debug('Unknown Error - sec: ' || l_sec || '-' || SQLERRM);
  END process_item_locators;

  PROCEDURE process_itemxref(errbuf  OUT VARCHAR2
                            ,retcode OUT VARCHAR2) IS
    vn_index      NUMBER := 0;
    vt_xref_table mtl_cross_references_pub.xref_tbl_type;
  
    vv_crossreference_type  VARCHAR2(100) := 'VENDOR';
    vv_crossreference_type1 VARCHAR2(100) := 'WC_XREF';
    vv_crossreference_type2 VARCHAR2(100) := 'UPC';
  
    vn_inventory_itemid NUMBER;
  
    vv_return_status VARCHAR2(10);
    vn_msg_count     NUMBER := 0;
    vt_message_list  error_handler.error_tbl_type;
    vv_error_message VARCHAR2(1000);
  
    l_user           NUMBER := fnd_profile.value('USER_ID');
    l_creation_date  DATE := SYSDATE;
    l_reprocess_flag VARCHAR2(1) := 'Y';
  
    l_total_reset             NUMBER := 0;
    l_total_skipped_mst_items NUMBER := 0;
    l_skip_master_ref_items   NUMBER := 0;
    l_marked_cnt              NUMBER := 0;
    l_processed_cnt           NUMBER := 0;
    l_error_cnt               NUMBER := 0;
    l_success_cnt             NUMBER := 0;
  
    CURSOR c_xref_stg IS
      SELECT ROWID rid
            ,(SELECT item
                FROM xxwc.xxwc_item_master_ref xmref
               WHERE xmref.icsp_prod = xxwcixs.partnumber
                 AND rownum < 2) partnumber
            ,partnumber ahh_partnumber
            ,xref_part
            ,xref_type
        FROM xxwc.xxwc_invitem_xref_ref xxwcixs
      --,xxwc.xxwc_item_master_ref  xmref
       WHERE cross_ref_status = g_new_sts
      --AND xxwcixs.partnumber = xmref.icsp_prod
      ;
  
  
  BEGIN
    print_log('XXWC AHH INV Item XRefs Conversion Program: Started');
    print_log(rpad('***'
                  ,100
                  ,'*'));
    --  
    --
    cleanup_tables();
    --
    --
    print_log('');
  
    --xx_item_validation;
    vn_index := 1;
    --l_reprocess_flag := 'N';
  
    -- Reprocess/Validate
    IF l_reprocess_flag = 'Y' THEN
    
      -- Reset them all
      UPDATE xxwc.xxwc_invitem_xref_ref xxwcixs
         SET cross_ref_status  = NULL
            ,cross_ref_message = NULL
       WHERE nvl(status
                ,'Y') = 'Y';
      --    
      l_total_reset := SQL%ROWCOUNT;
      --  
      -- Mark records as eligible for XRefs load.
      UPDATE xxwc.xxwc_invitem_xref_ref xxwcixs
         SET cross_ref_status = g_new_sts
       WHERE EXISTS (SELECT 'x'
                FROM mtl_system_items          b
                    ,xxwc.xxwc_item_master_ref xmref
               WHERE xxwcixs.partnumber = xmref.icsp_prod
                 AND xmref.item = b.segment1
                 AND organization_id = fnd_profile.value('XXWC_ITEM_MASTER_ORG'))
         AND cross_ref_status IS NULL;
      --    
      l_marked_cnt := SQL%ROWCOUNT;
    
      -- Update Rows being skipped for not being in Master Ref Table.
      UPDATE xxwc.xxwc_invitem_xref_ref xxwcixs
         SET cross_ref_status  = g_err_sts
            ,cross_ref_message = 'AHH Part Number is not available in Master Ref Table'
       WHERE NOT EXISTS (SELECT 'x'
                FROM xxwc.xxwc_item_master_ref xmref
               WHERE xxwcixs.partnumber = xmref.icsp_prod);
      --
      l_skip_master_ref_items := SQL%ROWCOUNT;
      --    
    
      -- Update Invalids to skip processing.
      UPDATE xxwc.xxwc_invitem_xref_ref xxwcixs
         SET cross_ref_status  = g_err_sts
            ,cross_ref_message = 'Part Number is not available'
       WHERE NOT EXISTS (SELECT 'x'
                FROM mtl_system_items          b
                    ,xxwc.xxwc_item_master_ref xmref
               WHERE xxwcixs.partnumber = xmref.icsp_prod
                 AND xmref.item = b.segment1
                 AND organization_id = fnd_profile.value('XXWC_ITEM_MASTER_ORG'));
      --
      l_total_skipped_mst_items := SQL%ROWCOUNT;
      --
    END IF;
  
    COMMIT;
  
    -- call to create xref
    FOR rc_xref_stg IN c_xref_stg LOOP
      BEGIN
        print_log(rpad('-'
                      ,100
                      ,'-'));
        vt_xref_table.delete;
        vv_error_message := NULL;
        l_processed_cnt  := l_processed_cnt + 1;
        vv_return_status := NULL;
        --
        UPDATE xxwc.xxwc_invitem_xref_ref xxwcixs
           SET xxwcixs.cross_ref_status = g_prs_sts
         WHERE ROWID = rc_xref_stg.rid;
      
        COMMIT;
        --
        BEGIN
          SELECT msi.inventory_item_id
            INTO vn_inventory_itemid
            FROM mtl_system_items_b msi
           WHERE msi.segment1 = rc_xref_stg.partnumber
             AND organization_id = 222
             AND rownum < 2;
        EXCEPTION
          WHEN OTHERS THEN
            vv_error_message := SQLERRM;
            UPDATE xxwc.xxwc_invitem_xref_ref xxwcixs
               SET xxwcixs.cross_ref_status  = g_prs_sts
                  ,xxwcixs.cross_ref_message = cross_ref_message ||
                                               '|InventoryItem derivation error: ' ||
                                               substr(vv_error_message
                                                     ,1
                                                     ,100)
             WHERE ROWID = rc_xref_stg.rid;
            RAISE;
        END;
      
        vt_xref_table(vn_index).cross_reference_id := mtl_cross_references_b_s.nextval;
        vt_xref_table(vn_index).transaction_type := 'CREATE';
        vt_xref_table(vn_index).cross_reference_type := (CASE rc_xref_stg.xref_type
                                                          WHEN 'VENDOR' THEN
                                                           vv_crossreference_type
                                                          WHEN 'XREF' THEN
                                                           vv_crossreference_type1
                                                          WHEN 'WC_XREF' THEN
                                                           vv_crossreference_type1
                                                          WHEN 'UPC' THEN
                                                           vv_crossreference_type2
                                                          ELSE
                                                           NULL
                                                        END);
        vt_xref_table(vn_index).cross_reference := substr(rc_xref_stg.xref_part
                                                         ,1
                                                         ,25);
        vt_xref_table(vn_index).inventory_item_id := vn_inventory_itemid;
        vt_xref_table(vn_index).description := '';
        vt_xref_table(vn_index).org_independent_flag := 'Y';
        vt_xref_table(vn_index).creation_date := l_creation_date;
        vt_xref_table(vn_index).last_update_date := l_creation_date;
        vt_xref_table(vn_index).last_updated_by := l_user;
        vt_xref_table(vn_index).created_by := l_user;
      
        IF vt_xref_table(vn_index).cross_reference_type IS NOT NULL THEN
          mtl_cross_references_pub.process_xref(p_api_version   => 1.0
                                               ,p_init_msg_list => fnd_api.g_true
                                               ,p_commit        => fnd_api.g_false
                                               ,p_xref_tbl      => vt_xref_table
                                               ,x_return_status => vv_return_status
                                               ,x_msg_count     => vn_msg_count
                                               ,x_message_list  => vt_message_list);
        
          IF (vv_return_status = fnd_api.g_ret_sts_success) THEN
            UPDATE xxwc.xxwc_invitem_xref_ref xxwcixs
               SET xxwcixs.cross_ref_status = g_scs_sts
             WHERE ROWID = rc_xref_stg.rid;
          
            print_debug('Cross Reference Loaded Successfully for Item :' || rc_xref_stg.partnumber ||
                        ', AHH Part#' || rc_xref_stg.ahh_partnumber || ' with xref part number:' ||
                        rc_xref_stg.xref_part);
            --
            l_success_cnt := l_success_cnt + 1;
            --          
          ELSE
            --Error_Handler.GET_MESSAGE_LIST(x_message_list=>x_message_list);
            FOR i IN 1 .. vt_message_list.count LOOP
              vv_error_message := vv_error_message || ' - ' || vt_message_list(i).message_text;
            END LOOP;
          
            UPDATE xxwc.xxwc_invitem_xref_ref xxwcixs
               SET xxwcixs.cross_ref_status  = g_err_sts
                  ,xxwcixs.cross_ref_message = vv_error_message
             WHERE ROWID = rc_xref_stg.rid;
          
            print_debug('Error message:' || vv_error_message);
            print_debug('Load Failed for Item:' || rc_xref_stg.partnumber || ', AHH Part#' ||
                        rc_xref_stg.ahh_partnumber || ' with xref part number:' ||
                        rc_xref_stg.xref_part);
            --
            l_error_cnt := l_error_cnt + 1;
            --
          END IF;
        
        ELSE
          vv_error_message := 'Cross Reference mapping failed: ' || rc_xref_stg.xref_type;
          print_debug('Error message:' || vv_error_message);
          print_debug('Load Failed for Item:' || rc_xref_stg.partnumber || ', AHH Part#' ||
                      rc_xref_stg.ahh_partnumber || ' with xref part number:' ||
                      rc_xref_stg.xref_part);
          UPDATE xxwc.xxwc_invitem_xref_ref xxwcixs
             SET xxwcixs.cross_ref_status  = g_err_sts
                ,xxwcixs.cross_ref_message = vv_error_message
           WHERE ROWID = rc_xref_stg.rid;
          --
          l_error_cnt := l_error_cnt + 1;
          --
        END IF;
        COMMIT;
      
      EXCEPTION
        WHEN OTHERS THEN
          vv_error_message := SQLERRM;
          UPDATE xxwc.xxwc_invitem_xref_ref xxwcixs
             SET xxwcixs.cross_ref_status  = g_err_sts
                ,xxwcixs.cross_ref_message = cross_ref_message || '|Unknown error: ' ||
                                             substr(vv_error_message
                                                   ,1
                                                   ,100)
           WHERE ROWID = rc_xref_stg.rid;
          RAISE;
      END;
    
    END LOOP;
  
    COMMIT;
  
    print_log(rpad('-'
                  ,100
                  ,'-'));
  
    print_log(rpad('*'
                  ,100
                  ,'*'));
    print_log(rpad('*** Total Records Reset: '
                  ,40
                  ,' ') || ': ' || l_total_reset);
    print_log(rpad('*** Total Records Marked: '
                  ,40
                  ,' ') || ': ' || l_marked_cnt);
    print_log(rpad('*** Total Records(Part Not Available): '
                  ,40
                  ,' ') || ': ' || l_total_skipped_mst_items);
  
    print_log(rpad('*** Total Records(Part Not exists in Master Ref): '
                  ,40
                  ,' ') || ': ' || l_skip_master_ref_items);
    print_log(rpad('*** Total Records Processed: '
                  ,40
                  ,' ') || ': ' || l_processed_cnt);
    print_log(rpad('*** Total Records Errored: '
                  ,40
                  ,' ') || ': ' || l_error_cnt);
    print_log(rpad('*** Total Records Succesful: '
                  ,40
                  ,' ') || ': ' || l_success_cnt);
  
    print_log(rpad('*'
                  ,100
                  ,'*'));
  
    print_log('XXWC AHH INV Item XRefs Conversion Program: Completed');
  
  EXCEPTION
    WHEN OTHERS THEN
      retcode := 2;
      errbuf  := SQLERRM;
      print_log('XXWC AHH INV Item XRefs Conversion Program: Aborted');
    
  END process_itemxref;

  PROCEDURE process_mst_vendor_attrs(p_submit_import_flag VARCHAR2) IS
  
  
    -- Get Vendor Numbers List
    CURSOR c_get_vendor_num IS
      SELECT TRIM(nvl(vendor_owner_number
                     ,(SELECT segment1
                        FROM ap_suppliers
                       WHERE attribute1 = mref.icsw_arpvendno
                         AND rownum < 2))) vendor_owner_number
            ,vendor_name
            ,item
            ,msib.description
            ,msib.inventory_item_id
            ,msib.organization_id
            ,'MST' organization_code
            ,mref.icsw_arpvendno
        FROM xxwc.xxwc_item_master_ref mref
            ,mtl_system_items_b        msib
       WHERE segment1 = item
         AND organization_id = 222
         AND NOT EXISTS (SELECT c_ext_attr1
                FROM ego.ego_mtl_sy_items_ext_b t
               WHERE attr_group_id = 861
                 AND t.organization_id = msib.organization_id
                 AND t.inventory_item_id = msib.inventory_item_id
                 AND c_ext_attr1 IS NOT NULL)
      /*AND item IN ('FWSD69'
      ,'5165956'
      ,'DW49158')*/
      ;
  
    TYPE t_get_vendor_num IS TABLE OF c_get_vendor_num%ROWTYPE;
    l_get_vendor_num_tbl t_get_vendor_num;
    l_array_size         NUMBER := 2000;
  
    l_vendor_exists  VARCHAR2(1);
    x_ret_flag       VARCHAR2(1);
    x_ret_msg        VARCHAR2(1000);
    l_prs_cnt        NUMBER := 0;
    l_int_scs_cnt    NUMBER := 0;
    l_int_err_cnt    NUMBER := 0;
    l_ven_scs_cnt    NUMBER := 0;
    l_ven_fal_cnt    NUMBER := 0;
    l_row_identifier NUMBER := 0;
    ln_dataset_id    NUMBER;
    ln_request_id    NUMBER;
    l_mst_org_id     NUMBER := 222;
  
    l_sec VARCHAR2(10);
  
  BEGIN
  
    l_sec                := '10';
    l_get_vendor_num_tbl := t_get_vendor_num();
  
    print_log(rpad('***'
                  ,100
                  ,'*'));
    print_log(' *** Master Vendor Number Update  STARTED ***');
  
    l_sec := '20';
  
  
    -- Get the list of Vendors
    OPEN c_get_vendor_num;
  
    LOOP
      -- Initializations
      l_get_vendor_num_tbl := NULL;
      ln_dataset_id        := ego_iua_data_set_id_s.nextval;
    
    
      FETCH c_get_vendor_num BULK COLLECT
        INTO l_get_vendor_num_tbl LIMIT l_array_size;
    
      l_row_identifier := 0;
      print_log('***  BATCH START *** COUNT:: ' || l_get_vendor_num_tbl.count);
    
      -- Create Master Vendor Number
      --FOR rec_items IN c_get_vendor_num LOOP
      FOR i IN l_get_vendor_num_tbl.first .. l_get_vendor_num_tbl.last LOOP
        BEGIN
          l_sec := '30';
          print_log(rpad('-'
                        ,100
                        ,'-'));
        
          print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                        ,30
                        ,' ') || ': *** START ***');
          l_sec := '40';
          -- Initialize
          l_vendor_exists := 'N';
          x_ret_flag      := NULL;
          x_ret_msg       := NULL;
          l_prs_cnt       := l_prs_cnt + 1;
        
          l_row_identifier := l_row_identifier + 1;
        
          l_sec := '50';
          --check if the Vendor number exists
          BEGIN
            SELECT 'Y'
              INTO l_vendor_exists
              FROM xxwc_ego_suppliers_actv_dct_vw
             WHERE vendor_number = nvl(l_get_vendor_num_tbl(i).vendor_owner_number
                                      ,'xxxxx');
          
            l_ven_scs_cnt := l_ven_scs_cnt + 1;
            print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                          ,30
                          ,' ') || ': Vendor#: ' || l_get_vendor_num_tbl(i).vendor_owner_number);
          
          EXCEPTION
            WHEN no_data_found THEN
              l_sec := '60';
              -- Check ARPVendorNumber
              BEGIN
                l_sec := '51';
                SELECT 'Y'
                  INTO l_vendor_exists
                  FROM xxwc_ego_suppliers_actv_dct_vw
                      ,ap_suppliers aps
                 WHERE aps.attribute1 = nvl(l_get_vendor_num_tbl(i).icsw_arpvendno
                                           ,'xxxxx')
                   AND vendor_number = aps.segment1
                   AND rownum < 2;
              
                l_ven_scs_cnt := l_ven_scs_cnt + 1;
                print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                              ,30
                              ,' ') || ': Vendor(arpvendnum)#: ' || l_get_vendor_num_tbl(i)
                          .icsw_arpvendno);
              
              EXCEPTION
                WHEN no_data_found THEN
                  l_sec           := '61';
                  l_vendor_exists := 'N';
                  l_ven_fal_cnt   := l_ven_fal_cnt + 1;
                  print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                                ,30
                                ,' ') || ': Vendor number(arpvendnum) doesnt exist: ' ||
                            nvl(l_get_vendor_num_tbl(i).vendor_owner_number
                               ,'NULL'));
                WHEN OTHERS THEN
                  l_sec           := '71';
                  l_vendor_exists := 'N';
                  print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                                ,30
                                ,' ') || ': Vendor number(arpvendnum) Unknown Error: ' ||
                            substr(SQLERRM
                                  ,1
                                  ,100));
                
              END;
              --
          
            WHEN OTHERS THEN
              l_sec           := '70';
              l_vendor_exists := 'N';
              print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                            ,30
                            ,' ') || ': Vendor number Unknown Error: ' ||
                        substr(SQLERRM
                              ,1
                              ,100));
          END;
        
          l_sec := '80';
          -- Update ICC for Item
          INSERT INTO mtl_system_items_interface
            (item_number
            ,organization_code
            ,description
            ,item_catalog_group_name
            ,set_process_id
            ,process_flag
            ,transaction_type)
          VALUES
            (l_get_vendor_num_tbl(i).item /*ITEM_NUMBER*/
            ,'MST' /*ORGANIZATION_CODE*/
            ,l_get_vendor_num_tbl(i).description /*DESCRIPTION*/
            ,'WHITE CAP' /*ITEM_CATALOG_GROUP_NAME*/
            ,ln_dataset_id /*SET_PROCESS_ID*/
            ,1 /*PROCESS_FLAG*/
            ,'UPDATE' /*TRANSACTION_TYPE*/);
        
          print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                        ,30
                        ,' ') || ': MTL_SYSTEM_ITEMS_INTERFACE inserted: ' || SQL%ROWCOUNT ||
                    ', DataSet: ' || ln_dataset_id);
        
          l_sec := '90';
          -- Sync Master Vendor Number Attribute
          INSERT INTO ego_itm_usr_attr_intrfc
            (item_number
            ,organization_code
            ,attr_group_int_name
            ,attr_int_name
            ,attr_value_str
            ,item_catalog_group_id
            ,data_level_name
            ,data_set_id
            ,row_identifier
            ,process_status
            ,transaction_type)
          VALUES
            (l_get_vendor_num_tbl(i).item /*ITEM_NUMBER*/
            ,'MST' /*ORGANIZATION_CODE*/
            ,'XXWC_INTERNAL_ATTR_AG' /*ATTR_GROUP_INT_NAME*/
            ,'XXWC_VENDOR_NUMBER_ATTR' /*ATTR_INT_NAME*/
            ,l_get_vendor_num_tbl(i).vendor_owner_number /*ATTR_VALUE_STR*/
            ,2 /*ITEM_CATALOG_GROUP_ID*/
            ,'ITEM_LEVEL' /*DATA_LEVEL_NAME*/
            ,ln_dataset_id /*DATA_SET_ID*/
            ,l_row_identifier /*ROW_IDENTIFIER*/
            ,1 /*PROCESS_STATUS*/
            ,'SYNC' /*TRANSACTION_TYPE */);
        
          print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                        ,30
                        ,' ') || ': EGO_ITM_USR_ATTR_INTRFC inserted: ' || SQL%ROWCOUNT ||
                    ', DataSet: ' || ln_dataset_id || ',  Row# ' || l_row_identifier);
        
          --
          COMMIT;
          --
          l_sec         := '100';
          l_int_scs_cnt := l_int_scs_cnt + 1;
          print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                        ,30
                        ,' ') || ': *** END ***');
        
        EXCEPTION
          WHEN OTHERS THEN
            l_int_err_cnt := l_int_err_cnt + 1;
            ROLLBACK;
            print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                          ,30
                          ,' ') || 'Error at Sec: ' || l_sec || '-' ||
                      substr(SQLERRM
                            ,1
                            ,100));
            print_log(rpad('*** Item : ' || l_get_vendor_num_tbl(i).item
                          ,30
                          ,' ') || ': *** ABORT ***');
        END;
      END LOOP;
      --
      l_sec := '110';
      print_log(rpad('-'
                    ,100
                    ,'-'));
    
      IF p_submit_import_flag = 'Y' THEN
        l_sec := '120';
      
        IF l_int_scs_cnt > 0 THEN
        
          l_sec         := '130';
          ln_request_id := fnd_request.submit_request(application => 'EGO'
                                                     ,program     => 'EGOICI'
                                                     ,description => ''
                                                     ,start_time  => ''
                                                     ,sub_request => FALSE
                                                     ,argument1   => l_mst_org_id -- (Organization ID)
                                                     ,argument2   => '1' -- YES (All organizations)
                                                     ,argument3   => '1' -- YES (Validate Items)
                                                     ,argument4   => '1' -- YES (Process Items)
                                                     ,argument5   => '2' -- NO (Delete Processed Rows)
                                                     ,argument6   => ln_dataset_id -- DATA SET ID (Process Set (Null for All))
                                                     ,argument7   => '3' -- SYNC (Create or Update Items)
                                                      );
        
          --
          COMMIT;
          --
        
          l_sec := '140';
          fnd_file.put_line(fnd_file.log
                           ,'*** Request id:' || ln_request_id);
          fnd_file.put_line(fnd_file.log
                           ,'*** Import Catalog Items Submitted for Process Set' || ln_dataset_id ||
                            ' Request id:' || ln_request_id);
        
        ELSE
          l_sec := '150';
          print_log('*** IMPORT CATALOG ITEM Program SKIPPED-No Recs ***');
        END IF;
      ELSE
        l_sec := '160';
        print_log('*** IMPORT CATALOG ITEM Program SKIPPED-Option No ***');
      END IF;
    
      --
      print_log('***  BATCH COMPLETE ***');
      --
      --
      EXIT WHEN c_get_vendor_num%NOTFOUND;
      --    
      --   
    END LOOP;
  
    l_sec := '170';
  
    print_log(rpad('***'
                  ,100
                  ,'*'));
  
    print_log(rpad('-'
                  ,100
                  ,'-'));
    print_log(rpad('*** Total Records processed: '
                  ,40
                  ,' ') || ': ' || l_prs_cnt);
    print_log(rpad('*** Total Recs (Vendors Available): '
                  ,40
                  ,' ') || ': ' || l_ven_scs_cnt);
    print_log(rpad('*** Total Recs (Vendor# Error/NA): '
                  ,40
                  ,' ') || ': ' || l_ven_fal_cnt);
    print_log(rpad('*** Total Rec (Success): '
                  ,40
                  ,' ') || ': ' || l_int_scs_cnt);
    print_log(rpad('*** Total Rec (Errors): '
                  ,40
                  ,' ') || ': ' || l_int_err_cnt);
  
    print_log(rpad('-'
                  ,100
                  ,'-'));
    l_sec := '180';
    print_log(' *** Master Vendor Number Update  ENDED ***');
    print_log(rpad('***'
                  ,100
                  ,'*'));
  EXCEPTION
    WHEN OTHERS THEN
      print_log('*** Unknown Error in Process_MST_Vendor_Attrs at Sec: ' || l_sec || '-' ||
                substr(SQLERRM
                      ,1
                      ,100));
      print_log(' *** Master Vendor Number Update  ABORTED ***');
      print_log(rpad('***'
                    ,100
                    ,'*'));
      RAISE;
    
  END process_mst_vendor_attrs;

  PROCEDURE get_vendor_info(p_msib_organization_id     IN NUMBER
                           ,p_mref_icsp_prod           IN VARCHAR2
                           ,p_mref_vendor_owner_number IN VARCHAR2
                           ,x_vendor_id                OUT NUMBER
                           ,x_vendor_num               OUT VARCHAR2
                           ,x_vendor_name              OUT VARCHAR2
                           ,x_vendor_site_id           OUT NUMBER) IS
  
    CURSOR c_get_stg_vendor IS
      SELECT (SELECT segment1
                FROM ap.ap_suppliers supp
               WHERE attribute1 = TRIM(stg.vendor_owner_num)) vendor_owner_num
        FROM xxwc.xxwc_invitem_stg stg
       WHERE ahh_partnumber = p_mref_icsp_prod
         AND derive_orcl_org_id(organization_code) = p_msib_organization_id
         AND TRIM(stg.vendor_owner_num) IS NOT NULL;
  
    l_stg_vendor_num c_get_stg_vendor%ROWTYPE;
  
    CURSOR c_get_sup_num_info(p_vendor_number IN VARCHAR2) IS
      SELECT supp.vendor_id
            ,supp.segment1 vendor_num
            ,supp.vendor_name
            ,supsite.vendor_site_id
        FROM ap.ap_suppliers          supp
            ,ap.ap_supplier_sites_all supsite
       WHERE supp.vendor_id = supsite.vendor_id(+)
         AND supsite.vendor_site_code(+) = '0PURCHASING'
         AND supp.segment1 = p_vendor_number;
  
    lr_get_sup_num_info c_get_sup_num_info%ROWTYPE;
  
  BEGIN
    OPEN c_get_stg_vendor;
    FETCH c_get_stg_vendor
      INTO l_stg_vendor_num;
    CLOSE c_get_stg_vendor;
  
    IF l_stg_vendor_num.vendor_owner_num IS NOT NULL THEN
      OPEN c_get_sup_num_info(l_stg_vendor_num.vendor_owner_num);
    
      FETCH c_get_sup_num_info
        INTO lr_get_sup_num_info;
    
      CLOSE c_get_sup_num_info;
    
    ELSE
    
      OPEN c_get_sup_num_info(p_mref_vendor_owner_number);
    
      FETCH c_get_sup_num_info
        INTO lr_get_sup_num_info;
    
      CLOSE c_get_sup_num_info;
    
    END IF;
  
    --
    x_vendor_id      := lr_get_sup_num_info.vendor_id;
    x_vendor_num     := lr_get_sup_num_info.vendor_num;
    x_vendor_name    := lr_get_sup_num_info.vendor_name;
    x_vendor_site_id := lr_get_sup_num_info.vendor_site_id;
    --
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END get_vendor_info;

  -- *********************************************************************************
  -- PROCEDURE ASSIGN_SOURCING_RULE: API to create Sorucing Rules
  -- Public API MRP_SOURCING_RULE_PUB.Process_Sourcing_Rule is called to process sourcing rules.
  -- Upon successfully processing sourcing rules a record is created in Sourcing Rule Assignments table.
  -- *********************************************************************************

  PROCEDURE assign_sourcing_rule(x_return_status OUT NOCOPY VARCHAR2
                                ,x_msg_data      OUT NOCOPY VARCHAR2
                                ,x_msg_count     OUT NOCOPY NUMBER) IS
  
    CURSOR c_vendor_num_org_list IS
      SELECT TRIM(nvl((SELECT segment1
                        FROM ap_suppliers
                       WHERE attribute1 = mref.icsw_arpvendno
                         AND rownum < 2)
                     ,vendor_owner_number)) vendor_owner_number
            ,vendor_name
            ,item
            ,icsp_prod
            ,msib.description
            ,msib.inventory_item_id
            ,msib.organization_id
            ,mref.icsw_arpvendno
        FROM xxwc.xxwc_item_master_ref mref
            ,mtl_system_items_b        msib
       WHERE segment1 = item
         AND organization_id <> 222
         AND EXISTS (SELECT 1
                FROM mtl_parameters                   mp
                    ,xxwc.xxwc_ap_branch_list_stg_tbl br
               WHERE mp.organization_code = br.oracle_branch_number
                 AND mp.organization_id = msib.organization_id)
         AND mref.item IN ('6353104486'
                          ,'709RT-14'
                          ,'450401201');
  
    TYPE t_vendor_num_org_list IS TABLE OF c_vendor_num_org_list%ROWTYPE;
  
  
    l_vendor_num_org_list_tbl t_vendor_num_org_list;
  
    l_array_size NUMBER := 5000;
    l_sec        VARCHAR2(10);
  
    l_sourcing_rule_id NUMBER;
    l_vendor_id        ap.ap_suppliers.vendor_id%TYPE;
    l_vendor_num       ap.ap_suppliers.segment1%TYPE;
    l_vendor_name      ap.ap_suppliers.vendor_name%TYPE;
    l_vendor_site_id   ap.ap_supplier_sites_all.vendor_site_id%TYPE;
  
    l_src_scs_cnt      NUMBER := 0;
    l_src_err_cnt      NUMBER := 0;
    l_prs_cnt          NUMBER := 0;
    l_batch_cnt        NUMBER := 0;
    l_src_asgn_cnt     NUMBER := 0;
    l_src_fal_asgn_cnt NUMBER := 0;
  
    l_msg_data               VARCHAR2(2000);
    l_assignment_set_id      NUMBER;
    l_sourcing_rule_rec      mrp_sourcing_rule_pub.sourcing_rule_rec_type;
    l_sourcing_rule_val_rec  mrp_sourcing_rule_pub.sourcing_rule_val_rec_type;
    l_receiving_org_tbl      mrp_sourcing_rule_pub.receiving_org_tbl_type;
    l_receiving_org_val_tbl  mrp_sourcing_rule_pub.receiving_org_val_tbl_type;
    l_shipping_org_tbl       mrp_sourcing_rule_pub.shipping_org_tbl_type;
    l_shipping_org_val_tbl   mrp_sourcing_rule_pub.shipping_org_val_tbl_type;
    lx_sourcing_rule_rec     mrp_sourcing_rule_pub.sourcing_rule_rec_type;
    lx_sourcing_rule_val_rec mrp_sourcing_rule_pub.sourcing_rule_val_rec_type;
    lx_receiving_org_tbl     mrp_sourcing_rule_pub.receiving_org_tbl_type;
    lx_receiving_org_val_tbl mrp_sourcing_rule_pub.receiving_org_val_tbl_type;
    lx_shipping_org_tbl      mrp_sourcing_rule_pub.shipping_org_tbl_type;
    lx_shipping_org_val_tbl  mrp_sourcing_rule_pub.shipping_org_val_tbl_type;
  
  
  
  BEGIN
  
    l_sec                     := '10';
    l_vendor_num_org_list_tbl := t_vendor_num_org_list();
  
    print_log(rpad('***'
                  ,100
                  ,'*'));
    print_log(' *** ITEM ORG, Sourcing Rule Update  STARTED ***');
  
    l_sec := '20';
    -- Get the list of Vendors
    OPEN c_vendor_num_org_list;
  
    LOOP
    
      l_sec := '30';
      -- Initializations
      l_batch_cnt               := l_batch_cnt + 1;
      l_vendor_num_org_list_tbl := NULL;
    
      l_sec := '40';
      FETCH c_vendor_num_org_list BULK COLLECT
        INTO l_vendor_num_org_list_tbl LIMIT l_array_size;
    
      --l_row_identifier := 0;
      print_log('***  BATCH START *** COUNT:: ' || l_vendor_num_org_list_tbl.count);
    
      l_sec := '50'; -- Create Master Vendor Number
      --FOR rec_items IN c_get_vendor_num LOOP
      FOR r IN l_vendor_num_org_list_tbl.first .. l_vendor_num_org_list_tbl.last LOOP
      
        l_sec     := '60';
        l_prs_cnt := l_prs_cnt + 1;
        BEGIN
          -- Initialize Return Status Variable
          l_sec              := '70';
          x_return_status    := fnd_api.g_ret_sts_success;
          l_sourcing_rule_id := NULL;
          l_vendor_id        := NULL;
          l_vendor_num       := NULL;
          l_vendor_name      := NULL;
          l_vendor_site_id   := NULL;
        
          print_log(rpad('-'
                        ,100
                        ,'-'));
        
          print_log(rpad('*** OrgID.Item> : '
                        ,30
                        ,' ') || ': ' || l_vendor_num_org_list_tbl(r).organization_id || '.' || l_vendor_num_org_list_tbl(r).item || '>' ||
                    '*** START ***');
        
          --
          -- Get Vendor Owner Number                    
          --
          get_vendor_info(l_vendor_num_org_list_tbl(r).organization_id
                         ,l_vendor_num_org_list_tbl(r).icsp_prod
                         ,l_vendor_num_org_list_tbl(r).vendor_owner_number
                         ,l_vendor_id
                         ,l_vendor_num
                         ,l_vendor_name
                         ,l_vendor_site_id);
        
          print_log(rpad('*** Ven#,ID,Name,SiteID'
                        ,30
                        ,' ') || l_vendor_id || ',' || l_vendor_num || ',' || l_vendor_name || ',' ||
                    l_vendor_site_id);
          --
        
          IF l_vendor_id IS NOT NULL THEN
            --
            BEGIN
              l_sec := '80';
              -- Get Sourcing Rule against Vendor ID
              SELECT msr.sourcing_rule_id
                INTO l_sourcing_rule_id
                FROM mrp.mrp_sourcing_rules msr
                    ,mrp.mrp_sr_receipt_org msro
                    ,mrp.mrp_sr_source_org  msso
               WHERE msr.sourcing_rule_id = msro.sourcing_rule_id
                 AND msro.sr_receipt_id = msso.sr_receipt_id
                 AND msso.vendor_id = l_vendor_id
                    --AND     msr.Organization_ID = p_Organization_ID --* GLobal Sourcing Rule
                 AND rownum = 1;
            
              l_src_scs_cnt := l_src_scs_cnt + 1;
              print_log(rpad('***'
                            ,30
                            ,' ') || ': Sourcing Rule Id found, ID :' || l_sourcing_rule_id);
            
            EXCEPTION
              WHEN no_data_found THEN
                l_sourcing_rule_id := NULL;
                print_log(rpad('***'
                              ,30
                              ,' ') || ': Sourcing Rule Id Not found.');
              
              WHEN OTHERS THEN
                l_sourcing_rule_id := NULL;
                print_log(rpad('***'
                              ,30
                              ,' ') || ': Error. Sourcing RuleID. ' ||
                          substr(SQLERRM
                                ,1
                                ,100));
            END;
          
            IF l_sourcing_rule_id IS NULL THEN
              l_sec := '90';
              print_log(rpad('***'
                            ,30
                            ,' ') || ':750 : Start of API');
            
              -- calli Public API to create Sourcing RUle for Vendor/Vendor Site
            
              --l_Sourcing_RUle_ID := Fnd_Profile.Value('MRP_DEFAULT_ASSIGNMENT_SET');
              l_sourcing_rule_rec := mrp_sourcing_rule_pub.g_miss_sourcing_rule_rec;
              --l_sourcing_rule_rec.sourcing_rule_id := l_Sourcing_RUle_ID ;
            
            
              l_sourcing_rule_rec.sourcing_rule_name := l_vendor_num;
              -- Satish U: Creating Global Sourcing Rule :  So commenting Folling Line
              -- l_sourcing_rule_rec.Organization_Id    := p_Organization_ID;
              l_sourcing_rule_rec.planning_active    := 1; -- Active?
              l_sourcing_rule_rec.status             := 1; -- Update New record
              l_sourcing_rule_rec.sourcing_rule_type := 1; -- 1:Sourcing Rule
              l_sourcing_rule_rec.operation          := 'CREATE';
              l_sourcing_rule_rec.description        := l_vendor_num || '_' || l_vendor_name;
            
            
              l_receiving_org_tbl := mrp_sourcing_rule_pub.g_miss_receiving_org_tbl;
              --l_receiving_org_tbl(1).disable_date := SYSDATE + 90;
              l_receiving_org_tbl(1).effective_date := trunc(SYSDATE);
              l_receiving_org_tbl(1).operation := 'CREATE';
              --l_receiving_org_tbl(1).sr_receipt_id           := 3; -- sr_receipt_id;
              -- Satish U : 14-MAR-2012 : Create Global Sourcing Rule
            
              -- l_receiving_org_tbl(1).receipt_organization_id := p_Organization_ID;
            
              l_shipping_org_tbl := mrp_sourcing_rule_pub.g_miss_shipping_org_tbl;
              l_shipping_org_tbl(1).rank := 1;
              l_shipping_org_tbl(1).allocation_percent := 100; -- Allocation 100
              l_shipping_org_tbl(1).source_type := 3; -- BUY FROM
              l_shipping_org_tbl(1).vendor_id := l_vendor_id;
              l_shipping_org_tbl(1).vendor_site_id := l_vendor_site_id;
              l_shipping_org_tbl(1).receiving_org_index := 1;
              l_shipping_org_tbl(1).operation := 'CREATE';
            
            
              l_sec := '100';
              mrp_sourcing_rule_pub.process_sourcing_rule(p_api_version_number    => 1.0
                                                         ,p_init_msg_list         => fnd_api.g_false
                                                         ,p_return_values         => fnd_api.g_true
                                                         ,p_commit                => fnd_api.g_true
                                                         ,x_return_status         => x_return_status
                                                         ,x_msg_count             => x_msg_count
                                                         ,x_msg_data              => x_msg_data
                                                         ,p_sourcing_rule_rec     => l_sourcing_rule_rec
                                                         ,p_sourcing_rule_val_rec => l_sourcing_rule_val_rec
                                                         ,p_receiving_org_tbl     => l_receiving_org_tbl
                                                         ,p_receiving_org_val_tbl => l_receiving_org_val_tbl
                                                         ,p_shipping_org_tbl      => l_shipping_org_tbl
                                                         ,p_shipping_org_val_tbl  => l_shipping_org_val_tbl
                                                         ,x_sourcing_rule_rec     => lx_sourcing_rule_rec
                                                         ,x_sourcing_rule_val_rec => lx_sourcing_rule_val_rec
                                                         ,x_receiving_org_tbl     => lx_receiving_org_tbl
                                                         ,x_receiving_org_val_tbl => lx_receiving_org_val_tbl
                                                         ,x_shipping_org_tbl      => lx_shipping_org_tbl
                                                         ,x_shipping_org_val_tbl  => lx_shipping_org_val_tbl);
            
              l_sec := '120';
              IF x_return_status = fnd_api.g_ret_sts_success THEN
              
                l_sec              := '130';
                l_sourcing_rule_id := lx_sourcing_rule_rec.sourcing_rule_id;
                l_src_scs_cnt      := l_src_scs_cnt + 1;
                print_log(rpad('***'
                              ,30
                              ,' ') || ': 731:Sourcing Rule Successful, ID :' ||
                          l_sourcing_rule_id);
              
                COMMIT;
              
              ELSE
                l_sec := '140';
                IF x_msg_count > 1 THEN
                  l_sec      := '150';
                  x_msg_data := '732 : Assign_Sourcing_Rule API Returned Error...' || chr(13) ||
                                chr(10);
                  l_msg_data := ' ';
                
                  FOR i IN 1 .. x_msg_count LOOP
                    l_msg_data := fnd_msg_pub.get(p_msg_index => i
                                                 ,p_encoded   => 'F');
                    x_msg_data := x_msg_data || l_msg_data;
                  END LOOP;
                ELSE
                  l_sec      := '160';
                  x_msg_data := '733: Assign_Sourcing_Rule API Errored Out :' || x_msg_data;
                END IF;
              
                l_src_err_cnt := l_src_err_cnt + 1;
                --
                print_log(rpad('***'
                              ,30
                              ,' ') || ': ' || l_msg_data);
              END IF;
              print_log(rpad('***'
                            ,30
                            ,' ') || ':750 : End of API');
            END IF;
          
            IF l_sourcing_rule_id IS NOT NULL THEN
            
              l_sec               := '170';
              l_src_asgn_cnt      := l_src_asgn_cnt + 1;
              l_assignment_set_id := fnd_profile.value('MRP_DEFAULT_ASSIGNMENT_SET');
            
              l_sec := '180';
              INSERT INTO mrp.mrp_sr_assignments
                (assignment_id
                ,assignment_type
                ,sourcing_rule_id
                ,sourcing_rule_type
                ,assignment_set_id
                ,last_update_date
                ,last_updated_by
                ,creation_date
                ,created_by
                ,organization_id
                ,inventory_item_id)
              VALUES
                (mrp.mrp_sr_assignments_s.nextval
                ,6
                ,l_sourcing_rule_id
                ,1
                ,l_assignment_set_id
                ,SYSDATE
                ,fnd_global.user_id
                ,SYSDATE
                ,fnd_global.user_id
                ,l_vendor_num_org_list_tbl(r).organization_id
                ,l_vendor_num_org_list_tbl(r).inventory_item_id);
            
              l_sec := '190';
              COMMIT;
              l_msg_data := 'Record Successfully Inserted into Sourcing Rule Assignment table.';
              print_log(rpad('***'
                            ,30
                            ,' ') || ': ' || l_msg_data);
            ELSE
              l_sec              := '200';
              l_msg_data         := 'Sourcing Assignment Skipped,Sourcing Rule ID is NULL.';
              l_src_fal_asgn_cnt := l_src_fal_asgn_cnt + 1;
              print_log(rpad('***'
                            ,30
                            ,' ') || ': ' || l_msg_data);
            END IF;
          
          ELSE
            print_log(rpad('***'
                          ,30
                          ,' ') || ': ' || 'Vendor# is not available');
          END IF;
          x_return_status := 'S';
          l_sec           := '210';
        EXCEPTION
          WHEN OTHERS THEN
            x_return_status := fnd_api.g_ret_sts_error;
            l_msg_data      := 'When Others Error :' || SQLERRM;
            print_log(rpad('***'
                          ,30
                          ,' ') || ': ' || l_msg_data);
        END;
        l_sec := '220';
      
      END LOOP;
    
      l_sec := '225';
      print_log(rpad('-'
                    ,100
                    ,'-'));
      --
      print_log('***  BATCH COMPLETE ***');
    
      l_sec := '230';
      EXIT WHEN c_vendor_num_org_list%NOTFOUND;
    
    
    END LOOP;
  
    l_sec := '240';
    l_sec := '170';
  
    print_log(rpad('***'
                  ,100
                  ,'*'));
  
    print_log(rpad('-'
                  ,100
                  ,'-'));
    print_log(rpad('*** Total Batches processed: '
                  ,40
                  ,' ') || ': ' || l_batch_cnt);
    print_log(rpad('*** Total Recs processed: '
                  ,40
                  ,' ') || ': ' || l_prs_cnt);
    print_log(rpad('*** Total Recs Source Rules Created: '
                  ,40
                  ,' ') || ': ' || l_src_scs_cnt);
    print_log(rpad('*** Total Rec Source Rule API Errors: '
                  ,40
                  ,' ') || ': ' || l_src_err_cnt);
    print_log(rpad('*** Total Rec Source Rule Assignments: '
                  ,40
                  ,' ') || ': ' || l_src_asgn_cnt);
    print_log(rpad('*** Total Rec Failed Source Rule Assignments: '
                  ,40
                  ,' ') || ': ' || l_src_fal_asgn_cnt);
  
    print_log(rpad('-'
                  ,100
                  ,'-'));
    l_sec := '180';
    print_log(' *** ITEM ORG, Sourcing Rule Update  ENDED ***');
    print_log(rpad('***'
                  ,100
                  ,'*'));
    l_sec := '250';
  EXCEPTION
    WHEN OTHERS THEN
      print_log('*** Unknown Error in Assign_Sourcing_Rule at Sec: ' || l_sec || '-' ||
                substr(SQLERRM
                      ,1
                      ,100));
      print_log(' *** ITEM ORG, Sourcing Rule Update  ABORTED ***');
      print_log(rpad('***'
                    ,100
                    ,'*'));
      RAISE;
  END assign_sourcing_rule;
  ---
  -- PROCEDURE: Process_Item Post Processor
  ---
  PROCEDURE process_item_post_processor(errbuf                     OUT VARCHAR2
                                       ,retcode                    OUT NUMBER
                                       ,p_taxware_code_update_flag IN VARCHAR2
                                       ,p_master_vendor_num_attr   IN VARCHAR2
                                       ,p_assign_sourcing_rule     IN VARCHAR2) IS
  
  
    l_submit_item_cat_import_flag VARCHAR2(1);
  
    lasr_ret_flag VARCHAR2(1);
    lasr_msg_data VARCHAR2(1000);
    lasr_msg_cnt  NUMBER;
  
  
  BEGIN
    print_log('XXWC AHH Item Conversion - Post Processor: Started');
  
    -- Initializations
  
    -- Import Item Catalog program flag = Yes
    l_submit_item_cat_import_flag := 'Y';
  
    --  
    --
    cleanup_tables();
    --
    --
    print_log(rpad('***'
                  ,100
                  ,'*'));
    print_log(' *** Taxware Code Update ***');
  
  
    IF p_taxware_code_update_flag = 'Y' THEN
    
      -- Update Taxware DFF
      UPDATE mtl_system_items_b msib
         SET attribute22 = REPLACE(attribute22
                                  ,chr(13)
                                  ,'')
       WHERE attribute22 IS NOT NULL
         AND attribute22 LIKE '%' || chr(13) || '%'
         AND organization_id = 222
         AND EXISTS (SELECT 'x'
                FROM xxwc.xxwc_item_master_ref
               WHERE item = msib.segment1);
    
      print_log('Items updated: ' || SQL%ROWCOUNT);
      --
      COMMIT;
      print_log(' *** Taxware Code Update > COMPLETED ***');
    ELSE
      print_log(' *** Taxware Code Update > SKIPPED ***');
    END IF;
    --
    --
    print_log(rpad('***'
                  ,100
                  ,'*'));
    --
    --
    IF p_master_vendor_num_attr = 'Y' THEN
      print_log('|| *** Master Vendor Number Creation: CALLED *** ||');
      --
      --
      process_mst_vendor_attrs(l_submit_item_cat_import_flag);
      --
      --    
    ELSE
      print_log('|| *** Master Vendor Number Creation: SKIPPED *** ||');
    END IF;
  
    IF p_assign_sourcing_rule = 'Y' THEN
      print_log('|| *** ITEM ORG, Sourcing Rule Update: CALLED *** ||');
      --
      --
      assign_sourcing_rule(lasr_ret_flag
                          ,lasr_msg_data
                          ,lasr_msg_cnt);
      --
      --    
    ELSE
      print_log('|| *** ITEM ORG, Sourcing Rule Update: SKIPPED *** ||');
    END IF;
  
  
  
    print_log('XXWC AHH Item Conversion - Post Processor: Completed');
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      retcode := 2;
      errbuf  := SQLERRM;
      print_log('XXWC AHH Item Conversion - Post Processor: Aborted');
  END;

END xxwc_ahh_item_conv_pkg;
/
