CREATE OR REPLACE PACKAGE xxwc_ahh_item_conv_pkg IS
  /***************************************************************************
  *    Script Name: xxwc_ahh_item_conv_pkg.pks
  *
  *    Interface / Conversion Name: Item conversion.
  *
  *    Functional Purpose: Convert Items using Interface for A.H Harris
  *
  *    History:
  *
  *    Version    Date             Author      Description
  *****************************************************************************
  *    1.0        06/04/2018       Naveen K    Initial Development.
  *****************************************************************************/
  PROCEDURE print_debug(p_print_str IN VARCHAR2);

  FUNCTION convert_number(pid     IN ROWID
                         ,p_col   IN VARCHAR2
                         ,p_value IN VARCHAR2) RETURN NUMBER;

  PROCEDURE cleanup_tables;
  FUNCTION derive_orcl_org(ahh_org_code IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION derive_ref_item_type(p_icsw_statusty IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION derive_salesvelocity(p_sv        IN VARCHAR2
                               ,p_item_type IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION derive_orcl_org_id(p_inv_org IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION derive_source_type(p_src_from VARCHAR2) RETURN NUMBER;
  PROCEDURE process_item(errbuf               OUT VARCHAR2
                        ,retcode              OUT NUMBER
                        ,p_validation_only    IN VARCHAR2
                        ,p_include_mst_items  IN VARCHAR2
                        ,p_include_org_items  IN VARCHAR2
                        ,p_include_categories IN VARCHAR2
                        ,p_item1              IN VARCHAR2
                        ,p_item2              IN VARCHAR2
                        ,p_item3              IN VARCHAR2);

  PROCEDURE process_item_locators(errbuf  OUT VARCHAR2
                                 ,retcode OUT VARCHAR2);

  PROCEDURE process_itemxref(errbuf  OUT VARCHAR2
                            ,retcode OUT VARCHAR2);

  PROCEDURE process_item_post_processor(errbuf                     OUT VARCHAR2
                                       ,retcode                    OUT NUMBER
                                       ,p_taxware_code_update_flag IN VARCHAR2
                                       ,p_master_vendor_num_attr   IN VARCHAR2
                                       ,p_assign_sourcing_rule     IN VARCHAR2);
END xxwc_ahh_item_conv_pkg;
/
