  /********************************************************************************
  FILE NAME: XXWC_MICI_ARCHIVE_ALL.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Archive Table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version
  *******************************************************************************************/
-- CREATE TABLE
CREATE TABLE XXWC.XXWC_MIE_ARCHIVE_ALL
(
  REQUESTID              NUMBER,
  ORGANIZATION_ID        NUMBER,
  TRANSACTION_ID         NUMBER,
  UNIQUE_ID              NUMBER,
  LAST_UPDATE_DATE       DATE NOT NULL,
  LAST_UPDATED_BY        NUMBER NOT NULL,
  CREATION_DATE          DATE NOT NULL,
  CREATED_BY             NUMBER NOT NULL,
  LAST_UPDATE_LOGIN      NUMBER,
  TABLE_NAME             VARCHAR2(30),
  MESSAGE_NAME           VARCHAR2(30),
  COLUMN_NAME            VARCHAR2(32),
  REQUEST_ID             NUMBER,
  PROGRAM_APPLICATION_ID NUMBER,
  PROGRAM_ID             NUMBER,
  PROGRAM_UPDATE_DATE    DATE,
  ERROR_MESSAGE          VARCHAR2(2000),
  ENTITY_IDENTIFIER      VARCHAR2(30),
  BO_IDENTIFIER          VARCHAR2(30),
  MESSAGE_TYPE           VARCHAR2(1)
);