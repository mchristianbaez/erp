  /********************************************************************************
  FILE NAME: XXWC_MIRI_ARCHIVE_ALL.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Archive Table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version
  *******************************************************************************************/
-- CREATE TABLE
CREATE TABLE XXWC.XXWC_MIRI_ARCHIVE_ALL
(
  REQUESTID                 NUMBER,
  INVENTORY_ITEM_ID         NUMBER,
  ORGANIZATION_ID           NUMBER,
  REVISION                  VARCHAR2(3),
  LAST_UPDATE_DATE          DATE,
  LAST_UPDATED_BY           NUMBER,
  CREATION_DATE             DATE,
  CREATED_BY                NUMBER,
  LAST_UPDATE_LOGIN         NUMBER,
  CHANGE_NOTICE             VARCHAR2(10),
  ECN_INITIATION_DATE       DATE,
  IMPLEMENTATION_DATE       DATE,
  IMPLEMENTED_SERIAL_NUMBER VARCHAR2(30),
  EFFECTIVITY_DATE          DATE,
  ATTRIBUTE_CATEGORY        VARCHAR2(30),
  ATTRIBUTE1                VARCHAR2(150),
  ATTRIBUTE2                VARCHAR2(150),
  ATTRIBUTE3                VARCHAR2(150),
  ATTRIBUTE4                VARCHAR2(150),
  ATTRIBUTE5                VARCHAR2(150),
  ATTRIBUTE6                VARCHAR2(150),
  ATTRIBUTE7                VARCHAR2(150),
  ATTRIBUTE8                VARCHAR2(150),
  ATTRIBUTE9                VARCHAR2(150),
  ATTRIBUTE10               VARCHAR2(150),
  ATTRIBUTE11               VARCHAR2(150),
  ATTRIBUTE12               VARCHAR2(150),
  ATTRIBUTE13               VARCHAR2(150),
  ATTRIBUTE14               VARCHAR2(150),
  ATTRIBUTE15               VARCHAR2(150),
  REQUEST_ID                NUMBER,
  PROGRAM_APPLICATION_ID    NUMBER,
  PROGRAM_ID                NUMBER,
  PROGRAM_UPDATE_DATE       DATE,
  REVISED_ITEM_SEQUENCE_ID  NUMBER,
  DESCRIPTION               VARCHAR2(240),
  ITEM_NUMBER               VARCHAR2(700),
  ORGANIZATION_CODE         VARCHAR2(3),
  TRANSACTION_ID            NUMBER,
  PROCESS_FLAG              NUMBER,
  TRANSACTION_TYPE          VARCHAR2(10),
  SET_PROCESS_ID            NUMBER NOT NULL,
  REVISION_ID               NUMBER,
  REVISION_LABEL            VARCHAR2(80),
  REVISION_REASON           VARCHAR2(30),
  LIFECYCLE_ID              NUMBER,
  CURRENT_PHASE_ID          NUMBER,
  SOURCE_SYSTEM_ID          NUMBER,
  SOURCE_SYSTEM_REFERENCE   VARCHAR2(255),
  CHANGE_ID                 NUMBER,
  INTERFACE_TABLE_UNIQUE_ID NUMBER,
  TEMPLATE_ID               NUMBER,
  TEMPLATE_NAME             VARCHAR2(30),
  BUNDLE_ID                 NUMBER
);
