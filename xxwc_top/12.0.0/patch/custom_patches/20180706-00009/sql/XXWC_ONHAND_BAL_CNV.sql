  /********************************************************************************
  FILE NAME: XXWC_ONHAND_BAL_CNV.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Item On-Hand Conversion Staging table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version
  *******************************************************************************************/
-- DROP TABLE
DROP TABLE XXWC.XXWC_ONHAND_BAL_CNV;

-- CREATE TABLE
CREATE TABLE XXWC.XXWC_ONHAND_BAL_CNV
(
  SOURCE_CODE            VARCHAR2(30),
  ORGANIZATION_ID        NUMBER,
  STATUS                 VARCHAR2(1),
  BATCH_NAME             VARCHAR2(40),
  SUBINVENTORY_CODE      VARCHAR2(30),
  ATTRIBUTE1             VARCHAR2(30),
  ATTRIBUTE2             VARCHAR2(30),
  ATTRIBUTE3             VARCHAR2(30),
  ATTRIBUTE4             VARCHAR2(30),
  ATTRIBUTE5             VARCHAR2(30),
  ITEM_SEGMENT           VARCHAR2(40),
  ATTRIBUTE6             VARCHAR2(30),
  ORGANIZATION_CODE      VARCHAR2(10),
  TRANSACTION_QUANTITY   VARCHAR2(40),
  ATTRIBUTE7             VARCHAR2(30),
  TRANSACTION_UOM        VARCHAR2(3),
  TRANSACTION_TYPENAME   VARCHAR2(40),
  ACCTS_CODE_COMBINATION VARCHAR2(40),
  ERROR_MESSAGE          VARCHAR2(3000),
  TRANSACTION_SOURCE     VARCHAR2(30),
  TRANSACTION_TYPE       VARCHAR2(40),
  LOCATOR_NAME           VARCHAR2(40),
  TRANSACTION_REFERENCE  VARCHAR2(40),
  PRIMARY_QUANTITY       NUMBER,
  LOT_EXPIRATION_DATE    DATE,
  ORIGINATION_DATE       DATE,
  LOT_NUMBER             VARCHAR2(80),
  FM_SERIAL_NUMBER       VARCHAR2(30),
  TO_SERIAL_NUMBER       VARCHAR2(30),
  ATTRIBUTE8             VARCHAR2(30),
  TRANSACTION_COST       VARCHAR2(20),
  TRANSACTION_DATE       DATE,
  HOW_PRICED             VARCHAR2(1)
);
