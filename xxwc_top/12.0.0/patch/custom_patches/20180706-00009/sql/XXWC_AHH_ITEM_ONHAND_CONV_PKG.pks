CREATE OR REPLACE PACKAGE xxwc_ahh_item_onhand_conv_pkg AS
  /***************************************************************************
  *    script name: xxwc_inv_onhand_conv_pkg.pks
  *
  *    interface / conversion name: Item On-hand conversion.
  *
  *    functional purpose: convert On-hand Qty using Interface
  *
  *    history:
  *
  *    version    date              author             description
  ********************************************************************
  *    1.0        04-sep-2011   k.Tavva     initial development.
  *******************************************************************/
  PROCEDURE print_debug(p_print_str IN VARCHAR2);
  FUNCTION derive_orcl_org(ahh_org_code IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION convert_number(p_value IN VARCHAR2) RETURN NUMBER;
  PROCEDURE validations;

  PROCEDURE process_statements;

  PROCEDURE itemonhand_conv_proc(errbuf          OUT VARCHAR2
                                ,retcode         OUT VARCHAR2
                                ,p_validate_only IN VARCHAR2);

  PROCEDURE update_born_on_date(errbuf            OUT VARCHAR2
                               ,retcode           OUT VARCHAR2
                               ,i_trx_date        IN VARCHAR2
                               ,i_organization_id IN NUMBER);
END xxwc_ahh_item_onhand_conv_pkg;
/
