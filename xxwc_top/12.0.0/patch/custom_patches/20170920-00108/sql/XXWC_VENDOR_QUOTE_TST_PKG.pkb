CREATE OR REPLACE
PACKAGE BODY XXEIS.xxwc_vendor_quote_tst_pkg
  /*************************************************************************
  $Header XXWC_VENDOR_QUOTE_TST_PKG.pkb $
  Module Name: XXWC_VENDOR_QUOTE_TST_PKG
  PURPOSE: EIS Vendor Qoute Batch Summary Report
  REVISIONS:
  Ver        Date         Author                Description
  ---------- -----------  ------------------    ----------------
  1.0        NA     		EIS        			Initial Version
  1.1  07/05/15  			Mahender Reddy  	TMS#20150312-00153
  1.2  08/07/15  			Mahender Reddy  	TMS#20150805-00061
  1.3  10/07/2015  			Mahender Reddy  	TMS#20150925-00003
  1.4  08/10/2016			Siva   				TMS#20160804-00030 
  1.5  02/22/2017			Siva				TMS#20161111-00048 
  1.6  07/31/2017			Siva				TMS#20170626-00301
  1.7  08/28/2017			Siva				TMS#20170821-00178
  1.8  10/30/2017   	    Siva        	    TMS#20170920-00108
  **************************************************************************/
AS
  g_dflt_email VARCHAR2 (50) := 'HDSOracleDevelopers@hdsupply.com'; -- Added for Ver 1.3
  
 FUNCTION get_po_trans_cost(
    p_inventory_item_id IN NUMBER,
    p_organization_id   IN NUMBER,
    p_vendor_number     IN VARCHAR2)
  RETURN NUMBER
  /*************************************************************************
  Function Name: get_po_trans_cost
  PURPOSE: To get PO Transdaction Cost
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0      08/28/2017		Siva				TMS#20170821-00178
  **************************************************************************/ 
IS
  L_HASH_INDEX VARCHAR2(100);
  L_SQL        varchar2(32000);
BEGIN
  G_PO_TRANS_COST := null;
  
  l_sql           :=
  ' SELECT MAX (NVL (pol.unit_price, 0))  
FROM po_headers poh,    
po_lines pol,    
po_line_locations poll,    
po_vendors pv  
WHERE poh.po_header_id                                    = pol.po_header_id  
AND poh.enabled_flag                                      = ''Y''  
and nvl (poh.cancel_flag, ''N'')                            = ''N''  
and poh.authorization_status                              =''APPROVED''  
AND POL.PO_HEADER_ID                                      = POLL.PO_HEADER_ID(+)  
AND pol.po_line_id                                        = poll.po_line_id(+)  
AND poh.vendor_id                                         = pv.vendor_id  
AND pol.item_id                                           = :P_INVENTORY_ITEM_ID  
AND NVL (poll.ship_to_organization_id, :P_ORGANIZATION_ID) = :P_ORGANIZATION_ID    
and pv.segment1                                           = :p_vendor_number  
and poh.creation_date =(SELECT  max(poh.creation_date)  
FROM po_headers poh,    
po_lines pol,    
po_line_locations poll,    
po_vendors pv  
WHERE poh.po_header_id                                    = pol.po_header_id  
AND poh.enabled_flag                                      = ''Y''  
and nvl (poh.cancel_flag, ''N'')                            = ''N''  
and poh.authorization_status                              =''APPROVED''  
AND POL.PO_HEADER_ID                                      = POLL.PO_HEADER_ID(+)  
AND pol.po_line_id                                        = poll.po_line_id(+)  
AND poh.vendor_id                                         = pv.vendor_id  
AND pol.item_id                                           = :P_INVENTORY_ITEM_ID  
and nvl (poll.ship_to_organization_id, :p_organization_id) = :p_organization_id    
AND pv.segment1                                           = :P_VENDOR_NUMBER)'
  ;
  
  BEGIN
    l_hash_index    :=p_inventory_item_id||'-'||p_organization_id||'-'||p_vendor_number;
    G_PO_TRANS_COST := G_PO_TRANS_COST_VLDN_TBL(L_HASH_INDEX);
  EXCEPTION
  WHEN no_data_found THEN
    BEGIN
      EXECUTE immediate L_SQL INTO G_PO_TRANS_COST USING P_INVENTORY_ITEM_ID ,P_ORGANIZATION_ID,P_ORGANIZATION_ID,P_VENDOR_NUMBER,P_INVENTORY_ITEM_ID ,P_ORGANIZATION_ID,P_ORGANIZATION_ID,P_VENDOR_NUMBER;
    EXCEPTION
    WHEN no_data_found THEN
      g_po_trans_cost :=NULL;
    WHEN OTHERS THEN
      g_po_trans_cost :=NULL;
    END;
    L_HASH_INDEX                           :=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_VENDOR_NUMBER;
    G_PO_TRANS_COST_VLDN_TBL(L_HASH_INDEX) := g_po_trans_cost;
  end;
  
  RETURN g_po_trans_cost;
EXCEPTION
WHEN OTHERS THEN
  g_po_trans_cost:=NULL;
  RETURN g_po_trans_cost;
END get_po_trans_cost;


 FUNCTION get_po_int_trans_cost(
    p_inventory_item_id IN NUMBER,
    p_organization_id   IN NUMBER,
    p_vendor_number     IN VARCHAR2)
  RETURN NUMBER
  /*************************************************************************
  Function Name: GET_PO_INT_TRANS_COST
  PURPOSE: To get PO Transdaction Cost
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0      08/28/2017		Siva				TMS#20170821-00178
  **************************************************************************/ 
IS
  L_HASH_INDEX VARCHAR2(100);
  L_SQL        varchar2(32000);
BEGIN
  G_PO_INT_TRANS_COST := null;
  
  l_sql           :=
  ' SELECT MAX (NVL (pol.unit_price, 0))
FROM po_headers poh,
  po_lines pol,
  po_vendors pv
WHERE poh.po_header_id = pol.po_header_id
AND poh.enabled_flag           = ''Y''
AND NVL (poh.cancel_flag, ''N'') = ''N''
AND poh.authorization_status   =''APPROVED''
AND poh.vendor_id              = pv.vendor_id
AND poh.po_header_id          IN
  (SELECT transaction_source_id
  FROM mtl_material_transactions mmt
  WHERE inventory_item_id=:P_INVENTORY_ITEM_ID
  AND transaction_type_id=18
  AND organization_id    =
    (SELECT transfer_organization_id
    FROM mtl_material_transactions
    WHERE transaction_id=
      (SELECT MAX(transaction_id)
      FROM mtl_material_transactions
      WHERE transaction_type_id=61
      AND inventory_item_id    =:P_INVENTORY_ITEM_ID
      AND organization_id      =:P_ORGANIZATION_ID
      )
    )
  )
AND pol.item_id                                           = :P_INVENTORY_ITEM_ID
AND pv.segment1                                           = :P_VENDOR_NUMBER
AND poh.creation_date                                     =
  (SELECT MAX(poh.creation_date)
  FROM po_headers poh,
    po_lines pol,
    po_vendors pv
  WHERE poh.po_header_id = pol.po_header_id
  AND poh.enabled_flag           = ''Y''
  AND NVL (poh.cancel_flag, ''N'') = ''N''
  AND poh.authorization_status   =''APPROVED''
  AND poh.vendor_id              = pv.vendor_id
  AND poh.po_header_id          IN
    (SELECT transaction_source_id
    FROM mtl_material_transactions mmt
    WHERE inventory_item_id=:P_INVENTORY_ITEM_ID
    AND transaction_type_id=18
    AND organization_id    =
      (SELECT transfer_organization_id
      FROM mtl_material_transactions
      WHERE transaction_id=
        (SELECT MAX(transaction_id)
        FROM mtl_material_transactions
        WHERE transaction_type_id=61
        AND inventory_item_id    =:P_INVENTORY_ITEM_ID
        AND organization_id      =:P_ORGANIZATION_ID
        )
      )
    )
  AND pol.item_id                                           = :P_INVENTORY_ITEM_ID
  and pv.segment1                                           = :P_VENDOR_NUMBER
  )'
  ;
  
  BEGIN
    l_hash_index    :=p_inventory_item_id||'-'||p_organization_id||'-'||p_vendor_number;
    G_PO_INT_TRANS_COST := G_PO_INT_TRAN_COST_VLDN_TBL(L_HASH_INDEX);
  EXCEPTION
  WHEN no_data_found THEN
    BEGIN
      EXECUTE immediate L_SQL INTO G_PO_INT_TRANS_COST USING P_INVENTORY_ITEM_ID,P_INVENTORY_ITEM_ID,P_ORGANIZATION_ID,P_INVENTORY_ITEM_ID,P_VENDOR_NUMBER,P_INVENTORY_ITEM_ID,P_INVENTORY_ITEM_ID,P_ORGANIZATION_ID,P_INVENTORY_ITEM_ID,P_VENDOR_NUMBER;
    EXCEPTION
    WHEN no_data_found THEN
      G_PO_INT_TRANS_COST :=NULL;
    WHEN OTHERS THEN
      G_PO_INT_TRANS_COST :=NULL;
    END;
    L_HASH_INDEX                           :=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_VENDOR_NUMBER;
    G_PO_INT_TRAN_COST_VLDN_TBL(L_HASH_INDEX) := G_PO_INT_TRANS_COST;
  end;
  
  RETURN G_PO_INT_TRANS_COST;
EXCEPTION
when OTHERS then
  G_PO_INT_TRANS_COST:=NULL;
  return G_PO_INT_TRANS_COST;
END GET_PO_INT_TRANS_COST;


PROCEDURE get_header_id(
    p_process_id     IN NUMBER,
    p_inv_start_date IN DATE,
    p_inv_end_date   IN DATE)
  /*************************************************************************
  $Header XXWC_VENDOR_QUOTE_TST_PKG.get_header_id $
  Module Name: XXWC_VENDOR_QUOTE_TST_PKG
  PURPOSE: EIS Vendor Qoute Batch Summary Report
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0         NA     		EIS        			Initial Version
  1.1   	07/05/15  		Mahender Reddy  	TMS#20150312-00153
  1.2   	08/07/15  		Mahender Reddy  	TMS#20150805-00061
  1.3   	10/07/2015  	Mahender Reddy  	TMS#20150925-00003
  1.4      08/10/2016			Siva   			TMS#20160804-00030
  1.6  07/31/2017			Siva				TMS#20170626-00301
  1.7      08/28/2017		Siva				TMS#20170821-00178
  1.8      10/30/2017   	Siva        	    TMS#20170920-00108
  **************************************************************************/
AS
-- Added For Ver 1.3
  l_err_msg       VARCHAR2 (3000);
  l_err_callfrom  VARCHAR2 (50);
  l_err_callpoint VARCHAR2 (50);
  -- End of Ver 1.3
  
  /* Changed the enitire pl/sql logic for version 1.5*/
  --Logic Start for version 1.5
  L_VQN_MAIN_CSR SYS_REFCURSOR;

  type l_vendor_quote_type
is
  table of xxeis.xxwc_vendor_quote_temp1%rowtype index by binary_integer;
  l_vendor_quote_type_tbl l_vendor_quote_type;

  --Start Added for Version 1.6
  L_VQN_SUB_SQL_CSR SYS_REFCURSOR;  
  TYPE L_VQN_SUB_TYPE
 is
  TABLE OF XXEIS.XXWC_VQN_SUBQUERY1_DTL_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  L_VQN_SUB_TYPE_TBL L_VQN_SUB_TYPE; 
  
   
   L_VQN_SUB2_SQL_CSR SYS_REFCURSOR;  
  TYPE L_VQN_SUB_TYPE2
 is
  TABLE OF XXEIS.XXWC_VQN_SUBQUERY2_DTL_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  L_VQN_SUB_TYPE2_TBL L_VQN_SUB_TYPE2; 
  
  
  L_VQN_SUB3_SQL_CSR SYS_REFCURSOR;
  TYPE L_VQN_SUB_TYPE3
 is
  TABLE OF XXEIS.XXWC_VQN_SUBQUERY3_DTL_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  L_VQN_SUB_TYPE3_TBL L_VQN_SUB_TYPE3;
  --End Added for Version 1.6

  -- Commented below code for version 1.6 
--  l_vqn_main_qry varchar2(32000):= 'SELECT NAME,
--    SALESREP_NAME SALESPERSON_NAME, --added for version 1.5
--    SALESREP_NUMBER SALESPESON_NUMBER,  --added for version 1.5
--    ACCOUNT_NAME MASTER_ACCOUNT_NAME,  --added for version 1.5
--    ACCOUNT_NUMBER MASTER_ACCOUNT_NUMBER,  --added for version 1.5
--    SHIP_TO JOB_NAME, --added for version 1.5
--    ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
--    SOURCE_TYPE_CODE,  --Added by Mahender for 20150312-00153 on 07/05/15
--    PARTY_SITE_NUMBER JOB_NUMBER,  --added for version 1.5
--    VENDOR_NUMBER,
--    VENDOR_NAME,
--    ORACLE_QUOTE_NUMBER,
--    INVOICE_NUMBER,
--    INVOICE_DATE,
--    PART_NUMBER,
--    UOM,
--    DESCRIPTION,
--    BPA,
--    PO_COST,
--    AVERAGE_COST,
--    SPECIAL_COST,
--    (PO_COST - SPECIAL_COST) UNIT_CLAIM_VALUE,
--    (PO_COST - SPECIAL_COST) * QTY REBATE,
--    GL_CODING,
--    GL_STRING,
--    LOC,
--    CREATED_BY,
--    CUSTOMER_TRX_ID,
--    ORDER_NUMBER,
--    LINE_NUMBER,
--    CREATION_DATE,
--    LOCATION,
--    QTY,
--    PROCESS_ID, --added for version 1.5
--    1 COMMMON_OUTPUT_ID,
--	list_price_per_unit  --Added for Ver 1.4
--  FROM
--    (SELECT NAME,
--      SALESREP_NAME,
--      SALESREP_NUMBER,
--      ACCOUNT_NAME,
--      ACCOUNT_NUMBER,
--      SHIP_TO,
--      ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
--      SOURCE_TYPE_CODE,  --Added by Mahender for 20150312-00153 on 07/05/15
--      PARTY_SITE_NUMBER,
--      VENDOR_NUMBER,
--      VENDOR_NAME,
--      ORACLE_QUOTE_NUMBER,
--      INVOICE_NUMBER,
--      INVOICE_DATE,
--      PART_NUMBER,
--      UOM,
--      DESCRIPTION,
--      CASE
--        WHEN UPPER (VENDOR_NAME) LIKE ''SIMPSON%''
--        THEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_BEST_BUY ( INVENTORY_ITEM_ID, ORGANIZATION_ID, VENDOR_NUMBER) --changed for version 1.5
--        ELSE NULL
--      END BPA,
--      /*----Modified and added PO cost logic by Mahender for 20150312-00153 on 07/05/15*/
--      NVL(
--      (SELECT a.BPA_COST
--      FROM
        --XXEIS.EIS_PO_XXWC_ISR_BPA_COST##Z  a, ---- Commented For Ver 1.3
--        XXEIS.EIS_XXWC_PO_ISR_TAB a, -- Added For Ver 1.3
--        OE_ORDER_LINES OL1
--      WHERE OL1.ROWID         = OL_ROWID
--      AND a.INVENTORY_ITEM_ID = OL1.INVENTORY_ITEM_ID
--      AND a.ORGANIZATION_ID   = OL1.SHIP_FROM_ORG_ID
--      ), NVL (
--      (SELECT SUM ( DECODE (COST_ELEMENT_ID, 1, ACTUAL_COST, 0))
--      FROM MTL_CST_ACTUAL_COST_DETAILS MCD,
--        OE_ORDER_LINES OL1
--      WHERE OL1.ROWID    = OL_ROWID
--      AND TRANSACTION_ID =
--        (SELECT  /*+ no_unnest hash_sj */
--          MAX (TRANSACTION_ID)
--        FROM MTL_CST_ACTUAL_COST_DETAILS MCD1
--        WHERE MCD1.INVENTORY_ITEM_ID   = OL1.INVENTORY_ITEM_ID
--        AND MCD1.ORGANIZATION_ID       = OL1.SHIP_FROM_ORG_ID
--        AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
--          TRUNC (OL1.CREATION_DATE) + 0.9999
--        )
--      AND MCD.INVENTORY_ITEM_ID      = OL1.INVENTORY_ITEM_ID
--      AND MCD.ORGANIZATION_ID        = OL1.SHIP_FROM_ORG_ID
--      AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
--        TRUNC (OL1.CREATION_DATE) + 0.9999
--      ), LIST_PRICE_PER_UNIT)) PO_COST,
--      Commneted by Mahender for 20150312-00153 on 07/05/15
--      /*NVL (
--      (SELECT SUM (
--      DECODE (COST_ELEMENT_ID, 1, ACTUAL_COST, 0))
--      FROM MTL_CST_ACTUAL_COST_DETAILS MCD,
--      OE_ORDER_LINES OL1
--      WHERE     OL1.ROWID = OL_ROWID
--      AND TRANSACTION_ID =
--      (SELECT /*+ no_unnest hash_sj */
--      /*     MAX (TRANSACTION_ID)
--      FROM MTL_CST_ACTUAL_COST_DETAILS MCD1
--      WHERE     MCD1.INVENTORY_ITEM_ID =
--      OL1.INVENTORY_ITEM_ID
--      AND MCD1.ORGANIZATION_ID =
--      OL1.SHIP_FROM_ORG_ID
--      AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
--      TRUNC (OL1.CREATION_DATE) + 0.9999
--      )
--      AND MCD.INVENTORY_ITEM_ID =
--      OL1.INVENTORY_ITEM_ID
--      AND MCD.ORGANIZATION_ID = OL1.SHIP_FROM_ORG_ID
--      AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
--      TRUNC (OL1.CREATION_DATE) + 0.9999
--      ),
--      LIST_PRICE_PER_UNIT) PO_COST,*/
--      AVERAGE_COST,
--      SPECIAL_COST,
--      1 UNIT_CLAIM_VALUE,
--      1 REBATE,
--      GL_CODING,
--      GL_STRING,
--      LOC,
--      CREATED_BY,
--      CUSTOMER_TRX_ID,
--      ORDER_NUMBER,
--      LINE_NUMBER,
--      CREATION_DATE,
--      LOCATION,
--      QTY,
--      :P_PROCESS_ID PROCESS_ID, --changed for version 1.5
--      1 COMMMON_OUTPUT_ID,
--	  list_price_per_unit --Added for Ver 1.4
--    FROM
--      (SELECT NAME,
--        LINE_NUMBER,
--        OL_ROWID,
--        ORDER_NUMBER,
--        VENDOR_NUMBER,
--        VENDOR_NAME,
--        VENDOR_ID,
--        INVENTORY_ITEM_ID,
--        ORGANIZATION_ID,
--        ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
--        SOURCE_TYPE_CODE,  --Added by Mahender for 20150312-00153 on 07/05/15
--        ORACLE_QUOTE_NUMBER,
--        PART_NUMBER,
--        UOM,
--        DESCRIPTION,
--        AVERAGE_COST,
--        SPECIAL_COST,
--        CREATION_DATE,
--        LOCATION,
--        QTY,
--        CUSTOMER_TRX_ID,
--        INVOICE_NUMBER,
--        INVOICE_DATE,
--        LOC,
--        GL_CODING,
--        GL_STRING,
--        CREATED_BY,
--        SALESREP_ID,
--        CUSTOMER_ID,
--        CUSTOMER_SITE_ID,
--        HEADER_ID,
--        FULL_NAME,
--        SALESREP_NUMBER,
--        SALESREP_NAME,
--        ACCOUNT_NAME,
--        ACCOUNT_NUMBER,
--        SHIP_TO,
--        PARTY_SITE_NUMBER,
--        LIST_PRICE_PER_UNIT,
--        (ROW_NUMBER () OVER (PARTITION BY HEADER_ID, LINE_ID ORDER BY QUALIFIER_ATTRIBUTE)) LIST_NUM
--      FROM
--        (WITH OL AS
--        (SELECT  /*+ MATERIALIZE leading(rctx rctl rctlgl ol oh fu ppf1 ras.jrs hzcs_ship_to HCAS_SHIP_TO hcs hp HZPS_SHIP_TO) use_nl(rctx rctl rctlgl ol oh fu ppf1 ras.jrs) no_index_ss(rctlgl RA_CUST_TRX_LINE_GL_DIST_N2) */
--          OL.LINE_ID,
--          OL.ROWID OL_ROWID,
--          OL.INVENTORY_ITEM_ID,
--          OL.SHIP_FROM_ORG_ID,
--          OL.LINE_NUMBER,
--          OL.CREATION_DATE CREATION_DATE,
--          OL.SOURCE_TYPE_CODE SOURCE_TYPE_CODE, --Added by Mahender for 20150312-00153 on 07/05/15
--          RCTLGL.GL_DATE,
--          RCTLGL.CUSTOMER_TRX_LINE_ID,
--          RCTLGL.CODE_COMBINATION_ID,
--           RCTL.CUSTOMER_TRX_ID , --commented for version 1.5
--          RCTX.CUSTOMER_TRX_ID CUSTOMER_TRX_ID , --Added for version 1.5
--          RCTX.TRX_NUMBER INVOICE_NUMBER ,  --Added for version 1.5
--          TRUNC(RCTX.TRX_DATE) INVOICE_DATE ,  --Added for version 1.5
--          NVL (RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED) QTY,
--          OH.HEADER_ID,
--          OH.ORDER_NUMBER,
--          OH.CREATION_DATE OH_CREATION_DATE,
--          OH.CREATED_BY,
--          OH.SALESREP_ID,
--           PPF1.FULL_NAME , --Commented for version 1.5
--          (
--          SELECT PPF1.FULL_NAME
--          FROM APPS.FND_USER FU ,
--			    APPS.PER_ALL_PEOPLE_F PPF1
--          WHERE FU.USER_ID   = OH.CREATED_BY
--          AND FU.EMPLOYEE_ID = PPF1.PERSON_ID
--          AND TRUNC(SYSDATE) BETWEEN TRUNC(PPF1.EFFECTIVE_START_DATE) AND TRUNC(PPF1.EFFECTIVE_END_DATE)
--          ) FULL_NAME, --Added for version 1.5
--          RAS.SALESREP_NUMBER,
--          RAS.NAME SALESREP_NAME,
--          HCS.ACCOUNT_NAME,
--          HCS.ACCOUNT_NUMBER,
--          HCS.CUST_ACCOUNT_ID,
--          HZCS_SHIP_TO.LOCATION SHIP_TO,
--          HZPS_SHIP_TO.PARTY_SITE_NUMBER PARTY_SITE_NUMBER,
--          TO_CHAR (OH.SHIP_TO_ORG_ID) SHIP_TO_ORG_ID,
--          TO_CHAR (OH.SOLD_TO_ORG_ID) SOLD_TO_ORG_ID
--          leading(rctx rctl rctlgl ol oh fu ras.jrs  hzcs_ship_to hcs hp) push_pred(fu)  */
--        FROM 
--          APPS.RA_CUSTOMER_TRX rctx, -- added for version 1.4
--          APPS.RA_CUSTOMER_TRX_LINES RCTL,
--          APPS.RA_CUST_TRX_LINE_GL_DIST RCTLGL,
--          APPS.OE_ORDER_LINES OL,
--          OE_ORDER_HEADERS OH,
--           FND_USER FU, --Commented for version 1.5
--           PER_ALL_PEOPLE_F PPF1,  --Commented for version 1.5
--          RA_SALESREPS RAS,
--          HZ_CUST_ACCOUNTS HCS,
--          HZ_PARTIES HP,
--          HZ_CUST_SITE_USES HZCS_SHIP_TO,
--          HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
--          HZ_PARTY_SITES HZPS_SHIP_TO,
--          HZ_LOCATIONS HZL_SHIP_TO
--        WHERE RCTX.CUSTOMER_TRX_ID   = RCTL.CUSTOMER_TRX_ID -- added for version 1.4
--        and trunc(rctx.trx_date) between :p_inv_start_date and :p_inv_end_date  -- added for version 1.5
--        AND OL.FLOW_STATUS_CODE             = ''CLOSED''
--        AND RCTL.INTERFACE_LINE_CONTEXT    = ''ORDER ENTRY''
--        AND RCTL.INTERFACE_LINE_ATTRIBUTE6 = TO_CHAR (OL.LINE_ID)
--        AND RCTL.LINE_TYPE                 = ''LINE''
--        AND RCTLGL.CUSTOMER_TRX_LINE_ID    = RCTL.CUSTOMER_TRX_LINE_ID
--        AND RCTLGL.ACCOUNT_CLASS           = ''REV''
--        AND OH.HEADER_ID = OL.HEADER_ID
--        AND FU.USER_ID   = OH.CREATED_BY --Commented for version 1.5
--        AND FU.EMPLOYEE_ID  = PPF1.PERSON_ID --Commented for version 1.5
--        AND TRUNC(SYSDATE) BETWEEN trunc(PPF1.EFFECTIVE_START_DATE) AND trunc(PPF1.EFFECTIVE_END_DATE) --Commented for version 1.5
--        AND RAS.SALESREP_ID                = OH.SALESREP_ID
--        AND HCAS_SHIP_TO.CUST_ACCOUNT_ID   = HCS.CUST_ACCOUNT_ID
--        AND HCS.PARTY_ID                   = HP.PARTY_ID
--        AND OH.SHIP_TO_ORG_ID              = HZCS_SHIP_TO.SITE_USE_ID(+)
--        AND HZCS_SHIP_TO.CUST_ACCT_SITE_ID = HCAS_SHIP_TO.CUST_ACCT_SITE_ID(+)
--        AND HCAS_SHIP_TO.PARTY_SITE_ID     = HZPS_SHIP_TO.PARTY_SITE_ID(+)
--        AND HZL_SHIP_TO.LOCATION_ID(+)     = HZPS_SHIP_TO.LOCATION_ID
--        )
--      SELECT /*+  use_concat  leading(ol qq qlh xxcph xxcpl POV.pav pov.hp gcc.GL_CODE_COMBINATIONS  RCT QLL MSI @subq OOD) use_nl(ol qq qlh.b qlh.t)  */
--        QQ.QUALIFIER_ATTRIBUTE,
--        QLH.NAME,
--        OL.LINE_NUMBER,
--        OL_ROWID,
--        OL.ORDER_NUMBER,
--        POV.SEGMENT1 VENDOR_NUMBER,
--        POV.VENDOR_NAME VENDOR_NAME,
--        POV.VENDOR_ID,
--        XXCPL.VENDOR_QUOTE_NUMBER ORACLE_QUOTE_NUMBER,
--        MSI.CONCATENATED_SEGMENTS PART_NUMBER,
--        MSI.PRIMARY_UNIT_OF_MEASURE UOM,
--        MSI.DESCRIPTION DESCRIPTION,
--        MSI.LIST_PRICE_PER_UNIT,
--        MSI.INVENTORY_ITEM_ID,
--        MSI.ORGANIZATION_ID,
--        OOD.ORGANIZATION_NAME ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
--        XXCPL.AVERAGE_COST AVERAGE_COST,
--        XXCPL.SPECIAL_COST SPECIAL_COST,
--        OL.OH_CREATION_DATE CREATION_DATE,
--         MP.ORGANIZATION_CODE LOCATION,  --Commented by Mahender for 20150312-00153 on 07/05/15
--        OOD.ORGANIZATION_CODE LOCATION,       --Added by Mahender for 20150312-00153 on 07/05/15
--        OL.SOURCE_TYPE_CODE SOURCE_TYPE_CODE, --Added by Mahender for 20150312-00153 on 07/05/15
--        OL.QTY,
--         RCT.CUSTOMER_TRX_ID CUSTOMER_TRX_ID ,  --Commented for version 1.5
--         RCT.TRX_NUMBER INVOICE_NUMBER ,  --Commented for version 1.5
--         TRUNC(RCT.TRX_DATE) INVOICE_DATE ,  --Commented for version 1.5
--        OL.CUSTOMER_TRX_ID, --Added for version 1.5
--        OL.INVOICE_NUMBER,  --Added for version 1.5
--        OL.INVOICE_DATE,   --Added for version 1.5
--        GCC.SEGMENT2 LOC,
--        ''400-900''
--        || ''-''
--        || GCC.SEGMENT2 GL_CODING,
--        GCC.CONCATENATED_SEGMENTS GL_STRING,
--        OL.CREATED_BY,
--        OL.SALESREP_ID,
--        XXCPH.CUSTOMER_ID,
--        XXCPH.CUSTOMER_SITE_ID,
--        OL.HEADER_ID,
--        OL.FULL_NAME,
--        OL.SALESREP_NUMBER,
--        OL.SALESREP_NAME,
--        OL.ACCOUNT_NAME,
--        OL.ACCOUNT_NUMBER,
--        OL.SHIP_TO,
--        OL.PARTY_SITE_NUMBER PARTY_SITE_NUMBER,
--        TRUNC (XXCPL.START_DATE) START_DATE,
--         TRUNC (OL.GL_DATE) END_DATE, --commeneted by Mahender for TMS#20150805-00061 on 08/07/15 which was added by Mahender for 20150312-00153 on 07/05/15
--         NVL (TRUNC (XXCPL.END_DATE), TRUNC (RCT.TRX_DATE)) END_DATE, --Commented for version 1.5
--        NVL (TRUNC (XXCPL.END_DATE), OL.INVOICE_DATE) END_DATE, --added for version 1.5
--        TRUNC (OL.GL_DATE) GL_DATE,
--        OL.LINE_ID
--      FROM OL,
--        QP_QUALIFIERS QQ,
--        QP_LIST_HEADERS QLH,
--        APPS.XXWC_OM_CONTRACT_PRICING_HDR XXCPH,
--        APPS.XXWC_OM_CONTRACT_PRICING_LINES XXCPL,
--        CP_OM_CONTRACT_PRICING_LINES XXCPL,
--        QP_LIST_LINES QLL,
--        MTL_SYSTEM_ITEMS_B_KFV MSI,
--        PO_VENDORS POV,
--         MTL_PARAMETERS MP,  --Commented by Mahender for 20150312-00153 on 07/05/15
--         RA_CUSTOMER_TRX RCT, --Commented for version 1.5
--        GL_CODE_COMBINATIONS_KFV GCC,
--        ORG_ORGANIZATION_DEFINITIONS OOD --Added by Mahender for 20150312-00153 on 07/05/15
--      WHERE 1                         = 1
--      AND QQ.QUALIFIER_CONTEXT        = ''CUSTOMER'' --commented for version 1.5
--      AND QQ.COMPARISON_OPERATOR_CODE = ''=''
--      AND QQ.ACTIVE_FLAG              = ''Y''
--      AND ( ( QLH.ATTRIBUTE10         = (''Contract Pricing'')
--      AND QQ.QUALIFIER_CONTEXT        = ''CUSTOMER'' --added for version 1.5
--      AND QQ.QUALIFIER_ATTRIBUTE      = ''QUALIFIER_ATTRIBUTE11''
--      AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SHIP_TO_ORG_ID))
--      OR ( QLH.ATTRIBUTE10            = (''Contract Pricing'')
--	    AND QQ.QUALIFIER_CONTEXT        = ''CUSTOMER'' --added for version 1.5
--      AND QQ.QUALIFIER_ATTRIBUTE      = ''QUALIFIER_ATTRIBUTE32''
--      AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SOLD_TO_ORG_ID))
--      OR ( QLH.ATTRIBUTE10            = (''CSP-NATIONAL'')
--      AND QQ.QUALIFIER_CONTEXT        = ''CUSTOMER'' --added for version 1.5
--      AND QQ.QUALIFIER_ATTRIBUTE      = ''QUALIFIER_ATTRIBUTE32''
--      AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SOLD_TO_ORG_ID))
--      OR ( QQ.QUALIFIER_CONTEXT       = ''ORDER''
--      AND QLH.ATTRIBUTE10             = (''Vendor Quote'')
--      AND QQ.QUALIFIER_ATTRIBUTE      = ''QUALIFIER_ATTRIBUTE18''
--      AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SHIP_FROM_ORG_ID)) --added for version 1.5
--      )  
--      AND QLH.LIST_HEADER_ID          = QQ.LIST_HEADER_ID
--      AND QLH.ACTIVE_FLAG             = ''Y''
--                                          AND TO_CHAR (XXCPH.AGREEMENT_ID) =
--                                                 (QLH.ATTRIBUTE14)
--      AND (XXCPH.AGREEMENT_ID)              = to_number(QLH.ATTRIBUTE14)
--      AND XXCPL.AGREEMENT_ID                = XXCPH.AGREEMENT_ID
--      AND XXCPL.AGREEMENT_TYPE              = ''VQN''
--      AND QLH.LIST_HEADER_ID                = QLL.LIST_HEADER_ID
--      AND XXCPL.PRODUCT_VALUE               = MSI.SEGMENT1
--      AND TO_CHAR (XXCPL.AGREEMENT_LINE_ID) = (QLL.ATTRIBUTE2)
--      AND MSI.INVENTORY_ITEM_ID             = OL.INVENTORY_ITEM_ID
--      AND MSI.ORGANIZATION_ID               = OL.SHIP_FROM_ORG_ID
--      AND POV.VENDOR_ID                     = XXCPL.VENDOR_ID
--        AND MP.ORGANIZATION_ID = MSI.ORGANIZATION_ID
--      AND OOD.ORGANIZATION_ID     = MSI.ORGANIZATION_ID --Added by Mahender for 20150312-00153 on 07/05/15
--       AND RCT.CUSTOMER_TRX_ID     = OL.CUSTOMER_TRX_ID  --Commented for version 1.5
--      AND GCC.CODE_COMBINATION_ID = OL.CODE_COMBINATION_ID
--	    AND OL.INVOICE_DATE        >= TRUNC (XXCPL.START_DATE) --Added for version 1.5
--      AND OL.INVOICE_DATE        <= NVL (TRUNC (XXCPL.END_DATE), OL.INVOICE_DATE) --Added for version 1.5
--        /*  AND NOT EXISTS
--        (SELECT /*+ qb_name(subq) * ''Y''
--        FROM MTL_SYSTEM_ITEMS_B_KFV
--        WHERE     INVENTORY_ITEM_ID =
--        MSI.INVENTORY_ITEM_ID
--        AND ORGANIZATION_ID =
--        MSI.ORGANIZATION_ID
--        AND SEGMENT1 LIKE ''SP%'') */
--        Commented by Mahender for TMS#20150805-00061 on 08/07/15
--        ) A
--       WHERE INVOICE_DATE <= END_DATE  --commented for version 1.5
--       AND INVOICE_DATE   >= START_DATE --commented for version 1.5
--      )
--    WHERE LIST_NUM = 1
--    )';
-- Commented End for version 1.6 

--Start Added below code for version 1.6
 l_vqn_main_qry varchar2(32000):= 'SELECT NAME,
  SALESREP_NAME SALESPERSON_NAME,
  SALESREP_NUMBER SALESPESON_NUMBER,
  ACCOUNT_NAME MASTER_ACCOUNT_NAME,
  ACCOUNT_NUMBER MASTER_ACCOUNT_NUMBER,
  SHIP_TO JOB_NAME,
  ORGANIZATION_NAME,
  SOURCE_TYPE_CODE,
  PARTY_SITE_NUMBER JOB_NUMBER,
  VENDOR_NUMBER,
  VENDOR_NAME,
  ORACLE_QUOTE_NUMBER,
  INVOICE_NUMBER,
  INVOICE_DATE,
  PART_NUMBER,
  UOM,
  DESCRIPTION,
  BPA,
  PO_COST,
  AVERAGE_COST,
  SPECIAL_COST,
  (PO_COST - SPECIAL_COST) UNIT_CLAIM_VALUE,
  (PO_COST - SPECIAL_COST) * QTY REBATE,
  GL_CODING,
  GL_STRING,
  LOC,
  CREATED_BY,
  CUSTOMER_TRX_ID,
  ORDER_NUMBER,
  LINE_NUMBER,
  CREATION_DATE,
  LOCATION,
  QTY,
  PROCESS_ID,
  COMMMON_OUTPUT_ID,
  list_price_per_unit,
  po_trans_cost  --added for version 1.8
FROM
  (SELECT NAME,
    SALESREP_NAME,
    SALESREP_NUMBER,
    ACCOUNT_NAME,
    ACCOUNT_NUMBER,
    SHIP_TO,
    ORGANIZATION_NAME,
    SOURCE_TYPE_CODE,
    PARTY_SITE_NUMBER,
    VENDOR_NUMBER,
    VENDOR_NAME,
    ORACLE_QUOTE_NUMBER,
    INVOICE_NUMBER,
    INVOICE_DATE,
    PART_NUMBER,
    UOM,
    DESCRIPTION,
    CASE
      WHEN UPPER (VENDOR_NAME) LIKE ''SIMPSON%''
      THEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_BEST_BUY ( INVENTORY_ITEM_ID, ORGANIZATION_ID, VENDOR_NUMBER)
      ELSE NULL
    END BPA,
    NVL(
    (SELECT a.BPA_COST
    FROM XXEIS.EIS_XXWC_PO_ISR_TAB a,
      OE_ORDER_LINES OL1
    WHERE OL1.line_id       = xvsdt.line_id
    AND a.INVENTORY_ITEM_ID = OL1.INVENTORY_ITEM_ID
    AND a.ORGANIZATION_ID   = OL1.SHIP_FROM_ORG_ID
    ), NVL (
    (SELECT SUM ( DECODE (COST_ELEMENT_ID, 1, ACTUAL_COST, 0))
    FROM MTL_CST_ACTUAL_COST_DETAILS MCD,
      OE_ORDER_LINES OL1
    WHERE OL1.line_id  = xvsdt.line_id
    AND TRANSACTION_ID =
      (SELECT /*+ no_unnest hash_sj */
        MAX (TRANSACTION_ID)
      FROM MTL_CST_ACTUAL_COST_DETAILS MCD1
      WHERE MCD1.INVENTORY_ITEM_ID   = OL1.INVENTORY_ITEM_ID
      AND MCD1.ORGANIZATION_ID       = OL1.SHIP_FROM_ORG_ID
      AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
      )
    AND MCD.INVENTORY_ITEM_ID      = OL1.INVENTORY_ITEM_ID
    AND MCD.ORGANIZATION_ID        = OL1.SHIP_FROM_ORG_ID
    AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
    ), LIST_PRICE_PER_UNIT)) PO_COST,
    AVERAGE_COST,
    SPECIAL_COST,
    1 UNIT_CLAIM_VALUE,
    1 REBATE,
    GL_CODING,
    GL_STRING,
    LOC,
    CREATED_BY,
    CUSTOMER_TRX_ID,
    ORDER_NUMBER,
    LINE_NUMBER,
    CREATION_DATE,
    LOCATION,
    QTY,
    '''||P_PROCESS_ID||''' PROCESS_ID,
    1 COMMMON_OUTPUT_ID,
    list_price_per_unit,
    (case when nvl(xxeis.XXWC_VENDOR_QUOTE_TST_PKG.get_po_trans_cost( INVENTORY_ITEM_ID, ORGANIZATION_ID, VENDOR_NUMBER),0)>0
    then xxeis.XXWC_VENDOR_QUOTE_TST_PKG.get_po_trans_cost( INVENTORY_ITEM_ID, ORGANIZATION_ID, VENDOR_NUMBER) 
    else xxeis.XXWC_VENDOR_QUOTE_TST_PKG.get_po_int_trans_cost( INVENTORY_ITEM_ID, ORGANIZATION_ID, VENDOR_NUMBER) 
    end )po_trans_cost  --added for version 1.8
  FROM xxeis.XXWC_VQN_SUBQUERY3_DTL_TBL xvsdt
  WHERE LIST_NUM = 1)';   
    
 l_vqn_sub_qry varchar2(32000):='SELECT OL.LINE_ID,
  OL.INVENTORY_ITEM_ID,
  OL.SHIP_FROM_ORG_ID,
  --OL.LINE_NUMBER, --Commented for version 1.7
  RCTL.LINE_NUMBER,  --Added for version 1.7
  OL.CREATION_DATE CREATION_DATE,
  OL.SOURCE_TYPE_CODE SOURCE_TYPE_CODE,
  RCTLGL.GL_DATE,
  RCTLGL.CUSTOMER_TRX_LINE_ID,
  RCTLGL.CODE_COMBINATION_ID,
  RCTX.CUSTOMER_TRX_ID CUSTOMER_TRX_ID ,
  RCTX.TRX_NUMBER INVOICE_NUMBER ,
  TRUNC(RCTX.TRX_DATE) INVOICE_DATE ,
  NVL (RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED) QTY,
  OH.HEADER_ID,
  OH.ORDER_NUMBER,
  OH.CREATION_DATE OH_CREATION_DATE,
  OH.CREATED_BY,
  OH.SALESREP_ID,
  (SELECT PPF1.FULL_NAME
  FROM APPS.FND_USER FU ,
    APPS.PER_ALL_PEOPLE_F PPF1
  WHERE FU.USER_ID   = OH.CREATED_BY
  AND FU.EMPLOYEE_ID = PPF1.PERSON_ID
  AND TRUNC(SYSDATE) BETWEEN TRUNC(PPF1.EFFECTIVE_START_DATE) AND TRUNC(PPF1.EFFECTIVE_END_DATE)
  ) FULL_NAME,
  (SELECT RAS.SALESREP_NUMBER
  FROM APPS.RA_SALESREPS RAS
  WHERE RAS.SALESREP_ID = OH.SALESREP_ID
  )SALESREP_NUMBER,
  (SELECT RAS.NAME
  FROM APPS.RA_SALESREPS RAS
  WHERE RAS.SALESREP_ID = OH.SALESREP_ID
  )SALESREP_NAME,
  HCS.ACCOUNT_NAME,
  HCS.ACCOUNT_NUMBER,
  HCS.CUST_ACCOUNT_ID,
  HZCS_SHIP_TO.LOCATION SHIP_TO,
  HZPS_SHIP_TO.PARTY_SITE_NUMBER PARTY_SITE_NUMBER,
  TO_CHAR (OH.SHIP_TO_ORG_ID) SHIP_TO_ORG_ID,
  TO_CHAR (OH.SOLD_TO_ORG_ID) SOLD_TO_ORG_ID
FROM APPS.RA_CUSTOMER_TRX rctx,
  APPS.RA_CUSTOMER_TRX_LINES RCTL,
  APPS.RA_CUST_TRX_LINE_GL_DIST RCTLGL,
  APPS.OE_ORDER_LINES OL,
  APPS.OE_ORDER_HEADERS OH,
  APPS.HZ_CUST_SITE_USES HZCS_SHIP_TO,
  APPS.HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
  APPS.HZ_PARTY_SITES HZPS_SHIP_TO,
  APPS.HZ_LOCATIONS HZL_SHIP_TO,
  APPS.HZ_CUST_ACCOUNTS HCS,
  APPS.HZ_PARTIES HP
WHERE RCTX.CUSTOMER_TRX_ID = RCTL.CUSTOMER_TRX_ID
AND TRUNC(rctx.trx_date) BETWEEN '''||p_inv_start_date||''' AND '''||p_inv_end_date||'''
AND OL.FLOW_STATUS_CODE            = ''CLOSED''
AND RCTL.INTERFACE_LINE_CONTEXT    = ''ORDER ENTRY''
AND RCTL.INTERFACE_LINE_ATTRIBUTE6 = TO_CHAR (OL.LINE_ID)
AND RCTL.LINE_TYPE                 = ''LINE''
AND RCTLGL.CUSTOMER_TRX_LINE_ID    = RCTL.CUSTOMER_TRX_LINE_ID
AND RCTLGL.ACCOUNT_CLASS           = ''REV''
AND OL.HEADER_ID                   = OH.HEADER_ID
AND OH.SHIP_TO_ORG_ID              = HZCS_SHIP_TO.SITE_USE_ID(+)
AND HZCS_SHIP_TO.CUST_ACCT_SITE_ID = HCAS_SHIP_TO.CUST_ACCT_SITE_ID(+)
AND HCAS_SHIP_TO.PARTY_SITE_ID     = HZPS_SHIP_TO.PARTY_SITE_ID(+)
AND HZL_SHIP_TO.LOCATION_ID(+)     = HZPS_SHIP_TO.LOCATION_ID
AND HCAS_SHIP_TO.CUST_ACCOUNT_ID   = HCS.CUST_ACCOUNT_ID
AND HCS.PARTY_ID                   = HP.PARTY_ID';

l_vqn_sub_qry2 varchar2(32000):='
SELECT /*+ INDEX(ol XXWC_VQN_SUBQUERY1_DTL_N3)*/  --Added for version 1.7
  qq.qualifier_attribute,
  qlh.name,
  ol.line_number,
  ol.order_number,
  pov.segment1 vendor_number,
  pov.vendor_name vendor_name,
  pov.vendor_id,
  xxcpl.vendor_quote_number oracle_quote_number,
  msi.concatenated_segments part_number,
  msi.primary_unit_of_measure uom,
  msi.description description,
  msi.list_price_per_unit,
  msi.inventory_item_id,
  msi.organization_id,
  ood.name organization_name, 
  xxcpl.average_cost average_cost,
  xxcpl.special_cost special_cost,
  ol.oh_creation_date creation_date,
  mp.organization_code location,       
  ol.source_type_code source_type_code,
  ol.qty,
  ol.customer_trx_id, 
  ol.invoice_number,  
  ol.invoice_date,    
  gcc.segment2 loc,
  ''400-900''
  || ''-''
  || gcc.segment2 gl_coding,
  gcc.concatenated_segments gl_string,
  ol.created_by,
  ol.salesrep_id,
  xxcph.customer_id,
  xxcph.customer_site_id,
  ol.header_id,
  ol.full_name,
  ol.salesrep_number,
  ol.salesrep_name,
  ol.account_name,
  ol.account_number,
  ol.ship_to,
  ol.party_site_number party_site_number,
  ol.gl_date ,
  ol.line_id
FROM xxeis.XXWC_VQN_SUBQUERY1_DTL_TBL ol,
  qp_qualifiers qq,
  qp_list_headers qlh,
  apps.xxwc_om_contract_pricing_hdr xxcph,
  apps.xxwc_om_contract_pricing_lines xxcpl,
  qp_list_lines qll,
  mtl_system_items_b_kfv msi,
  po_vendors pov,
  gl_code_combinations_kfv gcc,
  mtl_parameters mp,
  hr_all_organization_units ood
WHERE  qq.comparison_operator_code    = ''=''
AND qq.active_flag                    = ''Y''
AND qlh.attribute10                   = ''Contract Pricing''
AND qq.qualifier_context              = ''CUSTOMER'' 
AND qq.qualifier_attribute            = ''QUALIFIER_ATTRIBUTE11''
AND qq.qualifier_attr_value           = ol.ship_to_org_id
AND qlh.list_header_id                = qq.list_header_id
AND qlh.active_flag                   = ''Y''
AND xxcph.agreement_id                = to_number(qlh.attribute14)
AND xxcpl.agreement_id                = xxcph.agreement_id
AND xxcpl.agreement_type              = ''VQN''
AND qlh.list_header_id                = qll.list_header_id
AND xxcpl.product_value               = msi.segment1
AND TO_CHAR (xxcpl.agreement_line_id) = qll.attribute2
AND msi.inventory_item_id             = ol.inventory_item_id
AND msi.organization_id               = ol.ship_from_org_id
AND pov.vendor_id                     = xxcpl.vendor_id
AND ol.ship_from_org_id               = mp.organization_id
AND mp.organization_id                = ood.organization_id
AND gcc.code_combination_id           = ol.code_combination_id
AND ol.invoice_date                  >= TRUNC (xxcpl.start_date)
AND ol.invoice_date                  <= NVL (TRUNC (xxcpl.end_date), ol.invoice_date)
UNION ALL
SELECT  /*+ INDEX(ol XXWC_VQN_SUBQUERY1_DTL_N2)*/ --Added for version 1.7
  qq.qualifier_attribute,
  qlh.name,
  ol.line_number,
  ol.order_number,
  pov.segment1 vendor_number,
  pov.vendor_name vendor_name,
  pov.vendor_id,
  xxcpl.vendor_quote_number oracle_quote_number,
  msi.concatenated_segments part_number,
  msi.primary_unit_of_measure uom,
  msi.description description,
  msi.list_price_per_unit,
  msi.inventory_item_id,
  msi.organization_id,
  ood.name organization_name, 
  xxcpl.average_cost average_cost,
  xxcpl.special_cost special_cost,
  ol.oh_creation_date creation_date,
  mp.organization_code location,       
  ol.source_type_code source_type_code,
  ol.qty,
  ol.customer_trx_id, 
  ol.invoice_number,  
  ol.invoice_date,    
  gcc.segment2 loc,
  ''400-900''
  || ''-''
  || gcc.segment2 gl_coding,
  gcc.concatenated_segments gl_string,
  ol.created_by,
  ol.salesrep_id,
  xxcph.customer_id,
  xxcph.customer_site_id,
  ol.header_id,
  ol.full_name,
  ol.salesrep_number,
  ol.salesrep_name,
  ol.account_name,
  ol.account_number,
  ol.ship_to,
  ol.party_site_number party_site_number,
  ol.gl_date ,
  ol.line_id
FROM xxeis.XXWC_VQN_SUBQUERY1_DTL_TBL ol,
  qp_qualifiers qq,
  qp_list_headers qlh,
  apps.xxwc_om_contract_pricing_hdr xxcph,
  apps.xxwc_om_contract_pricing_lines xxcpl,
  qp_list_lines qll,
  mtl_system_items_b_kfv msi,
  po_vendors pov,
  gl_code_combinations_kfv gcc,
  mtl_parameters mp,
  hr_all_organization_units ood
WHERE qq.comparison_operator_code     = ''=''
AND qq.active_flag                    = ''Y''
AND qlh.attribute10                   = ''Contract Pricing''
AND qq.qualifier_context              = ''CUSTOMER'' 
AND qq.qualifier_attribute            = ''QUALIFIER_ATTRIBUTE32''
AND qq.qualifier_attr_value           = ol.sold_to_org_id
AND qlh.list_header_id                = qq.list_header_id
AND qlh.active_flag                   = ''Y''
AND xxcph.agreement_id                = to_number(qlh.attribute14)
AND xxcpl.agreement_id                = xxcph.agreement_id
AND xxcpl.agreement_type              = ''VQN''
AND qlh.list_header_id                = qll.list_header_id
AND xxcpl.product_value               = msi.segment1
AND TO_CHAR (xxcpl.agreement_line_id) = qll.attribute2
AND msi.inventory_item_id             = ol.inventory_item_id
AND msi.organization_id               = ol.ship_from_org_id
AND pov.vendor_id                     = xxcpl.vendor_id
AND ol.ship_from_org_id               = mp.organization_id
AND mp.organization_id                = ood.organization_id
AND gcc.code_combination_id           = ol.code_combination_id
AND ol.invoice_date                  >= TRUNC (xxcpl.start_date)
AND ol.invoice_date                  <= NVL (TRUNC (xxcpl.end_date), ol.invoice_date)
UNION ALL
SELECT /*+ INDEX(ol XXWC_VQN_SUBQUERY1_DTL_N2)*/ --Added for version 1.7
  qq.qualifier_attribute,
  qlh.name,
  ol.line_number,
  ol.order_number,
  pov.segment1 vendor_number,
  pov.vendor_name vendor_name,
  pov.vendor_id,
  xxcpl.vendor_quote_number oracle_quote_number,
  msi.concatenated_segments part_number,
  msi.primary_unit_of_measure uom,
  msi.description description,
  msi.list_price_per_unit,
  msi.inventory_item_id,
  msi.organization_id,
  ood.name organization_name, 
  xxcpl.average_cost average_cost,
  xxcpl.special_cost special_cost,
  ol.oh_creation_date creation_date,
  mp.organization_code location,        
  ol.source_type_code source_type_code, 
  ol.qty,
  ol.customer_trx_id, 
  ol.invoice_number,  
  ol.invoice_date,   
  gcc.segment2 loc,
  ''400-900''
  || ''-''
  || gcc.segment2 gl_coding,
  gcc.concatenated_segments gl_string,
  ol.created_by,
  ol.salesrep_id,
  xxcph.customer_id,
  xxcph.customer_site_id,
  ol.header_id,
  ol.full_name,
  ol.salesrep_number,
  ol.salesrep_name,
  ol.account_name,
  ol.account_number,
  ol.ship_to,
  ol.party_site_number party_site_number,
  ol.gl_date ,
  ol.line_id
FROM xxeis.XXWC_VQN_SUBQUERY1_DTL_TBL ol,
  qp_qualifiers qq,
  qp_list_headers qlh,
  apps.xxwc_om_contract_pricing_hdr xxcph,
  apps.xxwc_om_contract_pricing_lines xxcpl,
  qp_list_lines qll,
  mtl_system_items_b_kfv msi,
  po_vendors pov,
  gl_code_combinations_kfv gcc,
  mtl_parameters mp,
  hr_all_organization_units ood
WHERE qq.comparison_operator_code     = ''=''
AND qq.active_flag                    = ''Y''
AND qlh.attribute10                   = (''CSP-NATIONAL'')
AND qq.qualifier_context              = ''CUSTOMER''
AND qq.qualifier_attribute            = ''QUALIFIER_ATTRIBUTE32''
AND qq.qualifier_attr_value           = ol.sold_to_org_id
AND qlh.list_header_id                = qq.list_header_id
AND qlh.active_flag                   = ''Y''
AND xxcph.agreement_id                = to_number(qlh.attribute14)
AND xxcpl.agreement_id                = xxcph.agreement_id
AND xxcpl.agreement_type              = ''VQN''
AND qlh.list_header_id                = qll.list_header_id
AND xxcpl.product_value               = msi.segment1
AND TO_CHAR (xxcpl.agreement_line_id) = qll.attribute2
AND msi.inventory_item_id             = ol.inventory_item_id
AND msi.organization_id               = ol.ship_from_org_id
AND pov.vendor_id                     = xxcpl.vendor_id
AND ol.ship_from_org_id               = mp.organization_id
AND mp.organization_id                = ood.organization_id
AND gcc.code_combination_id           = ol.code_combination_id
AND ol.invoice_date                  >= TRUNC (xxcpl.start_date)
AND ol.invoice_date                  <= NVL (TRUNC (xxcpl.end_date), ol.invoice_date)
UNION ALL
SELECT /*+ INDEX(ol XXWC_VQN_SUBQUERY1_DTL_N1)*/ --Added for version 1.7
  qq.qualifier_attribute,
  qlh.name,
  ol.line_number,
  ol.order_number,
  pov.segment1 vendor_number,
  pov.vendor_name vendor_name,
  pov.vendor_id,
  xxcpl.vendor_quote_number oracle_quote_number,
  msi.concatenated_segments part_number,
  msi.primary_unit_of_measure uom,
  msi.description description,
  msi.list_price_per_unit,
  msi.inventory_item_id,
  msi.organization_id,
  ood.name organization_name, 
  xxcpl.average_cost average_cost,
  xxcpl.special_cost special_cost,
  ol.oh_creation_date creation_date,
  mp.organization_code location,       
  ol.source_type_code source_type_code, 
  ol.qty,
  ol.customer_trx_id, 
  ol.invoice_number,  
  ol.invoice_date,    
  gcc.segment2 loc,
  ''400-900''
  || ''-''
  || gcc.segment2 gl_coding,
  gcc.concatenated_segments gl_string,
  ol.created_by,
  ol.salesrep_id,
  xxcph.customer_id,
  xxcph.customer_site_id,
  ol.header_id,
  ol.full_name,
  ol.salesrep_number,
  ol.salesrep_name,
  ol.account_name,
  ol.account_number,
  ol.ship_to,
  ol.party_site_number party_site_number,
  ol.gl_date ,
  ol.line_id
FROM xxeis.XXWC_VQN_SUBQUERY1_DTL_TBL ol,
  qp_qualifiers qq,
  qp_list_headers qlh,
  apps.xxwc_om_contract_pricing_hdr xxcph,
  apps.xxwc_om_contract_pricing_lines xxcpl,
  qp_list_lines qll,
  mtl_system_items_b_kfv msi,
  po_vendors pov,
  gl_code_combinations_kfv gcc,
  mtl_parameters mp,
  hr_all_organization_units ood
WHERE qq.comparison_operator_code     = ''=''
AND qq.active_flag                    = ''Y''
AND qq.qualifier_context              = ''ORDER''
AND qlh.attribute10                   = ''Vendor Quote''
AND qq.qualifier_attribute            = ''QUALIFIER_ATTRIBUTE18''
AND qq.qualifier_attr_value           = ol.ship_from_org_id
AND qlh.list_header_id                = qq.list_header_id
AND qlh.active_flag                   = ''Y''
AND xxcph.agreement_id                = to_number(qlh.attribute14)
AND xxcpl.agreement_id                = xxcph.agreement_id
AND xxcpl.agreement_type              = ''VQN''
AND qlh.list_header_id                = qll.list_header_id
AND xxcpl.product_value               = msi.segment1
AND TO_CHAR (xxcpl.agreement_line_id) = qll.attribute2
AND msi.inventory_item_id             = ol.inventory_item_id
AND msi.organization_id               = ol.ship_from_org_id
AND pov.vendor_id                     = xxcpl.vendor_id
AND ol.ship_from_org_id               = mp.organization_id
AND mp.organization_id                = ood.organization_id
AND gcc.code_combination_id           = ol.code_combination_id
AND ol.invoice_date                  >= TRUNC (xxcpl.start_date)
AND ol.invoice_date                  <= NVL (TRUNC (xxcpl.end_date), ol.invoice_date)
';


l_vqn_sub_qry3 varchar2(32000):='SELECT   NAME,
        LINE_NUMBER,
        LINE_ID,
        ORDER_NUMBER,
        VENDOR_NUMBER,
        VENDOR_NAME,
        VENDOR_ID,
        INVENTORY_ITEM_ID,
        ORGANIZATION_ID,
        ORGANIZATION_NAME, 
        SOURCE_TYPE_CODE,  
        ORACLE_QUOTE_NUMBER,
        PART_NUMBER,
        UOM,
        DESCRIPTION,        
        AVERAGE_COST,
        SPECIAL_COST,
        CREATION_DATE,
        LOCATION,
        QTY,
        CUSTOMER_TRX_ID,
        INVOICE_NUMBER,
        INVOICE_DATE,
        LOC,
        GL_CODING,
        GL_STRING,
        CREATED_BY,
        SALESREP_ID,
        CUSTOMER_ID,
        CUSTOMER_SITE_ID,
        HEADER_ID,
        FULL_NAME,
        SALESREP_NUMBER,
        SALESREP_NAME,
        ACCOUNT_NAME,
        ACCOUNT_NUMBER,
        SHIP_TO,
        PARTY_SITE_NUMBER,
        LIST_PRICE_PER_UNIT,
        (ROW_NUMBER () OVER (PARTITION BY HEADER_ID, LINE_ID ORDER BY QUALIFIER_ATTRIBUTE)) LIST_NUM
      FROM xxeis.XXWC_VQN_SUBQUERY2_DTL_TBL';
--End code for version 1.6

BEGIN
-- Added For Ver 1.3
  l_err_callfrom  := 'xxwc_vendor_quote_tst_pkg.get_header_id';
  l_err_callpoint := 'get header id';
  -- End of Ver 1.3
   l_vendor_quote_type_tbl.DELETE;
   --Added for Ver 1.6
   l_vqn_sub_type_tbl.DELETE;
   L_VQN_SUB_TYPE2_TBL.DELETE;
   L_VQN_SUB_TYPE3_TBL.delete;
   --End for Ver 1.6

/*   --Commented code for version 1.6
  FND_FILE.PUT_LINE(FND_FILE.LOG,'l_vqn_main_qry '||l_vqn_main_qry);
  
  open l_vqn_main_csr for l_vqn_main_qry using  p_process_id , p_inv_start_date, p_inv_end_date ;
  loop
    fetch l_vqn_main_csr bulk collect into l_vendor_quote_type_tbl limit 10000;
     FND_FILE.PUT_LINE(FND_FILE.LOG,'Insert Completed '||l_vendor_quote_type_tbl.COUNT);
     if l_vendor_quote_type_tbl.count>0
  then   
   forall j in 1..l_vendor_quote_type_tbl.count 
  insert into xxeis.xxwc_vendor_quote_temp1 values l_vendor_quote_type_tbl(j);
  END IF;
  IF l_vqn_main_csr%notfound THEN
    CLOSE l_vqn_main_csr;
    EXIT;
  end if;
  END LOOP;  
	-- Logic End for Version 1.5
*/
--Added below code for Ver 1.6
fnd_file.put_line(fnd_file.LOG,'l_vqn_sub_qry '||l_vqn_sub_qry);
fnd_file.put_line(fnd_file.LOG,'l_vqn_sub_qry2 '||l_vqn_sub_qry2);
fnd_file.put_line(fnd_file.LOG,'l_vqn_sub_qry3 '||l_vqn_sub_qry3);
fnd_file.put_line(fnd_file.log,'l_vqn_main_qry '||l_vqn_main_qry);

  open l_vqn_sub_sql_csr for l_vqn_sub_qry ;
  loop
    fetch l_vqn_sub_sql_csr bulk collect into L_VQN_SUB_TYPE_TBL limit 10000;
     if L_VQN_SUB_TYPE_TBL.count>0
  then   
   forall j in 1..L_VQN_SUB_TYPE_TBL.count 
  insert into xxeis.XXWC_VQN_SUBQUERY1_DTL_TBL values l_vqn_sub_type_tbl(j);
  END IF;
  IF l_vqn_sub_sql_csr%notfound THEN
    CLOSE l_vqn_sub_sql_csr;
    EXIT;
  END IF;
  END LOOP;

  open L_VQN_SUB2_SQL_CSR for l_vqn_sub_qry2 ;
  loop
    fetch L_VQN_SUB2_SQL_CSR bulk collect into L_VQN_SUB_TYPE2_TBL limit 10000;
     if L_VQN_SUB_TYPE2_TBL.count>0
  then   
   forall j in 1..L_VQN_SUB_TYPE2_TBL.count 
  insert into xxeis.XXWC_VQN_SUBQUERY2_DTL_TBL values L_VQN_SUB_TYPE2_TBL(j);
  END IF;
  IF L_VQN_SUB2_SQL_CSR%notfound THEN
    CLOSE L_VQN_SUB2_SQL_CSR;
    EXIT;
  END IF;
  END LOOP;

  open L_VQN_SUB3_SQL_CSR for l_vqn_sub_qry3 ;
  loop
    fetch L_VQN_SUB3_SQL_CSR bulk collect into L_VQN_SUB_TYPE3_TBL limit 10000;
     if L_VQN_SUB_TYPE3_TBL.count>0
  then   
   forall j in 1..L_VQN_SUB_TYPE3_TBL.count 
  insert into xxeis.XXWC_VQN_SUBQUERY3_DTL_TBL values L_VQN_SUB_TYPE3_TBL(j);
  END IF;
  IF L_VQN_SUB3_SQL_CSR%notfound THEN
    CLOSE L_VQN_SUB3_SQL_CSR;
    EXIT;
  END IF;
  END LOOP;  

 open l_vqn_main_csr for l_vqn_main_qry ;
  loop
    fetch l_vqn_main_csr bulk collect into l_vendor_quote_type_tbl limit 10000;
     if l_vendor_quote_type_tbl.count>0
  then   
   forall j in 1..l_vendor_quote_type_tbl.count 
  insert into xxeis.xxwc_vendor_quote_temp1 values l_vendor_quote_type_tbl(j);
  END IF;
  IF l_vqn_main_csr%notfound THEN
    CLOSE l_vqn_main_csr;
    EXIT;
  END IF;
  END LOOP;  
--End for Ver 1.6  

	-- Commented below Logic for Version 1.5  
--  fnd_file.put_line(fnd_file.log,'in insert' ||l_name);
--  INSERT /*+ APPEND */
--  INTO xxeis.eis_xxwc_om_vendor_quote_v --cp_EIS_XXWC_OM_VENDOR_QUOTE_V
--  SELECT NAME,
--    SALESREP_NAME,
--    SALESREP_NUMBER,
--    ACCOUNT_NAME,
--    ACCOUNT_NUMBER,
--    SHIP_TO,
--    ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
--    SOURCE_TYPE_CODE,  --Added by Mahender for 20150312-00153 on 07/05/15
--    PARTY_SITE_NUMBER,
--    VENDOR_NUMBER,
--    VENDOR_NAME,
--    ORACLE_QUOTE_NUMBER,
--    INVOICE_NUMBER,
--    INVOICE_DATE,
--    PART_NUMBER,
--    UOM,
--    DESCRIPTION,
--    BPA,
--    PO_COST,
--    AVERAGE_COST,
--    SPECIAL_COST,
--    (PO_COST - SPECIAL_COST) UNIT_CLAIM_VALUE,
--    (PO_COST - SPECIAL_COST) * QTY REBATE,
--    GL_CODING,
--    GL_STRING,
--    LOC,
--    CREATED_BY,
--    CUSTOMER_TRX_ID,
--    ORDER_NUMBER,
--    LINE_NUMBER,
--    CREATION_DATE,
--    LOCATION,
--    QTY,
--    P_PROCESS_ID,
--    1 COMMMON_OUTPUT_ID,
--	list_price_per_unit  --Added for Ver 1.4
--  FROM
--    (SELECT NAME,
--      SALESREP_NAME,
--      SALESREP_NUMBER,
--      ACCOUNT_NAME,
--      ACCOUNT_NUMBER,
--      SHIP_TO,
--      ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
--      SOURCE_TYPE_CODE,  --Added by Mahender for 20150312-00153 on 07/05/15
--      PARTY_SITE_NUMBER,
--      VENDOR_NUMBER,
--      VENDOR_NAME,
--      ORACLE_QUOTE_NUMBER,
--      INVOICE_NUMBER,
--      INVOICE_DATE,
--      PART_NUMBER,
--      UOM,
--      DESCRIPTION,
--      CASE
--        WHEN UPPER (VENDOR_NAME) LIKE 'SIMPSON%'
--        THEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_BEST_BUY ( INVENTORY_ITEM_ID, ORGANIZATION_ID, VENDOR_NUMBER)
--        ELSE NULL
--      END BPA,
--      /*----Modified and added PO cost logic by Mahender for 20150312-00153 on 07/05/15*/
--      NVL(
--      (SELECT a.BPA_COST
--      FROM
--        ----XXEIS.EIS_PO_XXWC_ISR_BPA_COST##Z  a, ---- Commented For Ver 1.3
--        XXEIS.EIS_XXWC_PO_ISR_TAB a, -- Added For Ver 1.3
--        OE_ORDER_LINES OL1
--      WHERE OL1.ROWID         = OL_ROWID
--      AND a.INVENTORY_ITEM_ID = OL1.INVENTORY_ITEM_ID
--      AND a.ORGANIZATION_ID   = OL1.SHIP_FROM_ORG_ID
--      ), NVL (
--      (SELECT SUM ( DECODE (COST_ELEMENT_ID, 1, ACTUAL_COST, 0))
--      FROM MTL_CST_ACTUAL_COST_DETAILS MCD,
--        OE_ORDER_LINES OL1
--      WHERE OL1.ROWID    = OL_ROWID
--      AND TRANSACTION_ID =
--        (SELECT  /*+ no_unnest hash_sj */
--          MAX (TRANSACTION_ID)
--        FROM MTL_CST_ACTUAL_COST_DETAILS MCD1
--        WHERE MCD1.INVENTORY_ITEM_ID   = OL1.INVENTORY_ITEM_ID
--        AND MCD1.ORGANIZATION_ID       = OL1.SHIP_FROM_ORG_ID
--        AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
--          --TRUNC (OL1.CREATION_DATE) + 0.9999
--        )
--      AND MCD.INVENTORY_ITEM_ID      = OL1.INVENTORY_ITEM_ID
--      AND MCD.ORGANIZATION_ID        = OL1.SHIP_FROM_ORG_ID
--      AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
--        --TRUNC (OL1.CREATION_DATE) + 0.9999
--      ), LIST_PRICE_PER_UNIT)) PO_COST,
--      --Commneted by Mahender for 20150312-00153 on 07/05/15
--      /*NVL (
--      (SELECT SUM (
--      DECODE (COST_ELEMENT_ID, 1, ACTUAL_COST, 0))
--      FROM MTL_CST_ACTUAL_COST_DETAILS MCD,
--      OE_ORDER_LINES OL1
--      WHERE     OL1.ROWID = OL_ROWID
--      AND TRANSACTION_ID =
--      (SELECT /*+ no_unnest hash_sj */
--      /*     MAX (TRANSACTION_ID)
--      FROM MTL_CST_ACTUAL_COST_DETAILS MCD1
--      WHERE     MCD1.INVENTORY_ITEM_ID =
--      OL1.INVENTORY_ITEM_ID
--      AND MCD1.ORGANIZATION_ID =
--      OL1.SHIP_FROM_ORG_ID
--      AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
--      --TRUNC (OL1.CREATION_DATE) + 0.9999
--      )
--      AND MCD.INVENTORY_ITEM_ID =
--      OL1.INVENTORY_ITEM_ID
--      AND MCD.ORGANIZATION_ID = OL1.SHIP_FROM_ORG_ID
--      AND (TRANSACTION_COSTED_DATE) <= INVOICE_DATE
--      --TRUNC (OL1.CREATION_DATE) + 0.9999
--      ),
--      LIST_PRICE_PER_UNIT) PO_COST,*/
--      AVERAGE_COST,
--      SPECIAL_COST,
--      1 UNIT_CLAIM_VALUE,
--      1 REBATE,
--      GL_CODING,
--      GL_STRING,
--      LOC,
--      CREATED_BY,
--      CUSTOMER_TRX_ID,
--      ORDER_NUMBER,
--      LINE_NUMBER,
--      CREATION_DATE,
--      LOCATION,
--      QTY,
--      P_PROCESS_ID,
--      1 COMMMON_OUTPUT_ID,
--	  list_price_per_unit --Added for Ver 1.4
--    FROM
--      (SELECT NAME,
--        LINE_NUMBER,
--        OL_ROWID,
--        ORDER_NUMBER,
--        VENDOR_NUMBER,
--        VENDOR_NAME,
--        VENDOR_ID,
--        INVENTORY_ITEM_ID,
--        ORGANIZATION_ID,
--        ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
--        SOURCE_TYPE_CODE,  --Added by Mahender for 20150312-00153 on 07/05/15
--        ORACLE_QUOTE_NUMBER,
--        PART_NUMBER,
--        UOM,
--        DESCRIPTION,
--        AVERAGE_COST,
--        SPECIAL_COST,
--        CREATION_DATE,
--        LOCATION,
--        QTY,
--        CUSTOMER_TRX_ID,
--        INVOICE_NUMBER,
--        INVOICE_DATE,
--        LOC,
--        GL_CODING,
--        GL_STRING,
--        CREATED_BY,
--        SALESREP_ID,
--        CUSTOMER_ID,
--        CUSTOMER_SITE_ID,
--        HEADER_ID,
--        FULL_NAME,
--        SALESREP_NUMBER,
--        SALESREP_NAME,
--        ACCOUNT_NAME,
--        ACCOUNT_NUMBER,
--        SHIP_TO,
--        PARTY_SITE_NUMBER,
--        LIST_PRICE_PER_UNIT,
--        (ROW_NUMBER () OVER (PARTITION BY HEADER_ID, LINE_ID ORDER BY QUALIFIER_ATTRIBUTE)) LIST_NUM
--      FROM
--        (WITH OL AS
--        (SELECT  /*+ MATERIALIZE leading(rctx rctl rctlgl ol oh fu ppf1 ras.jrs hzcs_ship_to HCAS_SHIP_TO hcs hp HZPS_SHIP_TO) use_nl(rctx rctl rctlgl ol oh fu ppf1 ras.jrs) no_index_ss(rctlgl RA_CUST_TRX_LINE_GL_DIST_N2) */
--          OL.LINE_ID,
--          OL.ROWID OL_ROWID,
--          OL.INVENTORY_ITEM_ID,
--          OL.SHIP_FROM_ORG_ID,
--          OL.LINE_NUMBER,
--          OL.CREATION_DATE CREATION_DATE,
--          OL.SOURCE_TYPE_CODE SOURCE_TYPE_CODE, --Added by Mahender for 20150312-00153 on 07/05/15
--          RCTLGL.GL_DATE,
--          RCTLGL.CUSTOMER_TRX_LINE_ID,
--          RCTLGL.CODE_COMBINATION_ID,
--          -- RCTL.CUSTOMER_TRX_ID ,
--          RCTX.CUSTOMER_TRX_ID CUSTOMER_TRX_ID ,
--          RCTX.TRX_NUMBER INVOICE_NUMBER ,
--          TRUNC(RCTX.TRX_DATE) INVOICE_DATE ,
--          NVL (RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED) QTY,
--          OH.HEADER_ID,
--          OH.ORDER_NUMBER,
--          OH.CREATION_DATE OH_CREATION_DATE,
--          OH.CREATED_BY,
--          OH.SALESREP_ID,
--          -- PPF1.FULL_NAME ,
--          (
--          SELECT PPF1.FULL_NAME
--          FROM APPS.FND_USER FU ,
--			   APPS.PER_ALL_PEOPLE_F PPF1
--          WHERE FU.USER_ID   = OH.CREATED_BY
--          AND FU.EMPLOYEE_ID = PPF1.PERSON_ID
--          AND TRUNC(SYSDATE) BETWEEN TRUNC(PPF1.EFFECTIVE_START_DATE) AND TRUNC(PPF1.EFFECTIVE_END_DATE)
--          ) FULL_NAME,
--          RAS.SALESREP_NUMBER,
--          RAS.NAME SALESREP_NAME,
--          HCS.ACCOUNT_NAME,
--          HCS.ACCOUNT_NUMBER,
--          HCS.CUST_ACCOUNT_ID,
--          HZCS_SHIP_TO.LOCATION SHIP_TO,
--          HZPS_SHIP_TO.PARTY_SITE_NUMBER PARTY_SITE_NUMBER,
--          TO_CHAR (OH.SHIP_TO_ORG_ID) SHIP_TO_ORG_ID,
--          TO_CHAR (OH.SOLD_TO_ORG_ID) SOLD_TO_ORG_ID
--          --leading(rctx rctl rctlgl ol oh fu ras.jrs  hzcs_ship_to hcs hp) push_pred(fu)  */
--        FROM 
--          APPS.RA_CUSTOMER_TRX rctx, -- added for version 1.4
--          APPS.RA_CUSTOMER_TRX_LINES RCTL,
--          APPS.RA_CUST_TRX_LINE_GL_DIST RCTLGL,
--          APPS.OE_ORDER_LINES OL,
--          OE_ORDER_HEADERS OH,
--          -- FND_USER FU,
--          -- PER_ALL_PEOPLE_F PPF1,
--          RA_SALESREPS RAS,
--          HZ_CUST_ACCOUNTS HCS,
--          HZ_PARTIES HP,
--          HZ_CUST_SITE_USES HZCS_SHIP_TO,
--          HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
--          HZ_PARTY_SITES HZPS_SHIP_TO,
--          HZ_LOCATIONS HZL_SHIP_TO
--        WHERE RCTX.CUSTOMER_TRX_ID   = RCTL.CUSTOMER_TRX_ID -- added for version 1.4
--        and trunc(rctx.trx_date) between p_inv_start_date and p_inv_end_date  -- added for version 1.4
--        AND OL.FLOW_STATUS_CODE             = 'CLOSED'
--        AND RCTL.INTERFACE_LINE_CONTEXT    = 'ORDER ENTRY'
--        AND RCTL.INTERFACE_LINE_ATTRIBUTE6 = TO_CHAR (OL.LINE_ID)
--        AND RCTL.LINE_TYPE                 = 'LINE'
--        AND RCTLGL.CUSTOMER_TRX_LINE_ID    = RCTL.CUSTOMER_TRX_LINE_ID
--        AND RCTLGL.ACCOUNT_CLASS           = 'REV'
--		--AND RCTLGL.GL_DATE BETWEEN p_inv_start_date --'01-may-14' -- Commented for version 1.4
--        --AND p_inv_end_date                          --'31-may-14' -- Commented for version 1.4
--        AND OH.HEADER_ID = OL.HEADER_ID
--        --AND FU.USER_ID   = OH.CREATED_BY
--        --AND FU.EMPLOYEE_ID  = PPF1.PERSON_ID --Changed for Ver 1.4
--        --AND TRUNC(SYSDATE) BETWEEN trunc(PPF1.EFFECTIVE_START_DATE) AND trunc(PPF1.EFFECTIVE_END_DATE) --Changed for Ver 1.4
--        AND RAS.SALESREP_ID                = OH.SALESREP_ID
--        AND HCAS_SHIP_TO.CUST_ACCOUNT_ID   = HCS.CUST_ACCOUNT_ID
--        AND HCS.PARTY_ID                   = HP.PARTY_ID
--        AND OH.SHIP_TO_ORG_ID              = HZCS_SHIP_TO.SITE_USE_ID(+)
--        AND HZCS_SHIP_TO.CUST_ACCT_SITE_ID = HCAS_SHIP_TO.CUST_ACCT_SITE_ID(+)
--        AND HCAS_SHIP_TO.PARTY_SITE_ID     = HZPS_SHIP_TO.PARTY_SITE_ID(+)
--        AND HZL_SHIP_TO.LOCATION_ID(+)     = HZPS_SHIP_TO.LOCATION_ID
--        )
--      SELECT /*+  use_concat  leading(ol qq qlh xxcph xxcpl POV.pav pov.hp gcc.GL_CODE_COMBINATIONS  RCT QLL MSI @subq OOD) use_nl(ol qq qlh.b qlh.t)  */
--        QQ.QUALIFIER_ATTRIBUTE,
--        QLH.NAME,
--        OL.LINE_NUMBER,
--        OL_ROWID,
--        OL.ORDER_NUMBER,
--        POV.SEGMENT1 VENDOR_NUMBER,
--        POV.VENDOR_NAME VENDOR_NAME,
--        POV.VENDOR_ID,
--        XXCPL.VENDOR_QUOTE_NUMBER ORACLE_QUOTE_NUMBER,
--        MSI.CONCATENATED_SEGMENTS PART_NUMBER,
--        MSI.PRIMARY_UNIT_OF_MEASURE UOM,
--        MSI.DESCRIPTION DESCRIPTION,
--        MSI.LIST_PRICE_PER_UNIT,
--        MSI.INVENTORY_ITEM_ID,
--        MSI.ORGANIZATION_ID,
--        OOD.ORGANIZATION_NAME ORGANIZATION_NAME, --Added by Mahender for 20150312-00153 on 07/05/15
--        XXCPL.AVERAGE_COST AVERAGE_COST,
--        XXCPL.SPECIAL_COST SPECIAL_COST,
--        OL.OH_CREATION_DATE CREATION_DATE,
--        -- MP.ORGANIZATION_CODE LOCATION,  --Commented by Mahender for 20150312-00153 on 07/05/15
--        OOD.ORGANIZATION_CODE LOCATION,       --Added by Mahender for 20150312-00153 on 07/05/15
--        OL.SOURCE_TYPE_CODE SOURCE_TYPE_CODE, --Added by Mahender for 20150312-00153 on 07/05/15
--        OL.QTY,
--        -- RCT.CUSTOMER_TRX_ID CUSTOMER_TRX_ID ,
--        -- RCT.TRX_NUMBER INVOICE_NUMBER ,
--        -- TRUNC(RCT.TRX_DATE) INVOICE_DATE ,
--		OL.CUSTOMER_TRX_ID,
--		OL.INVOICE_NUMBER,
--		OL.INVOICE_DATE, 
--        GCC.SEGMENT2 LOC,
--        '400-900'
--        || '-'
--        || GCC.SEGMENT2 GL_CODING,
--        GCC.CONCATENATED_SEGMENTS GL_STRING,
--        OL.CREATED_BY,
--        OL.SALESREP_ID,
--        XXCPH.CUSTOMER_ID,
--        XXCPH.CUSTOMER_SITE_ID,
--        OL.HEADER_ID,
--        OL.FULL_NAME,
--        OL.SALESREP_NUMBER,
--        OL.SALESREP_NAME,
--        OL.ACCOUNT_NAME,
--        OL.ACCOUNT_NUMBER,
--        OL.SHIP_TO,
--        OL.PARTY_SITE_NUMBER PARTY_SITE_NUMBER,
--        TRUNC (XXCPL.START_DATE) START_DATE,
--        -- TRUNC (OL.GL_DATE) END_DATE, --commeneted by Mahender for TMS#20150805-00061 on 08/07/15 which was added by Mahender for 20150312-00153 on 07/05/15
--        -- NVL (TRUNC (XXCPL.END_DATE), TRUNC (RCT.TRX_DATE)) END_DATE, --Added TRX_DATE for version 1.4 --ADD by Mahender for for TMS#20150805-00061 on 08/07/15 which was Commented by Mahender for 20150312-00153 on 07/05/15
--		NVL (TRUNC (XXCPL.END_DATE), OL.INVOICE_DATE) END_DATE,
--        TRUNC (OL.GL_DATE) GL_DATE,
--        OL.LINE_ID
--      FROM OL,
--        QP_QUALIFIERS QQ,
--        QP_LIST_HEADERS QLH,
--        APPS.XXWC_OM_CONTRACT_PRICING_HDR XXCPH,
--        APPS.XXWC_OM_CONTRACT_PRICING_LINES XXCPL,
--        --CP_OM_CONTRACT_PRICING_LINES XXCPL,
--        QP_LIST_LINES QLL,
--        MTL_SYSTEM_ITEMS_B_KFV MSI,
--        PO_VENDORS POV,
--        -- MTL_PARAMETERS MP,  --Commented by Mahender for 20150312-00153 on 07/05/15
--        -- RA_CUSTOMER_TRX RCT,
--        GL_CODE_COMBINATIONS_KFV GCC,
--        ORG_ORGANIZATION_DEFINITIONS OOD --Added by Mahender for 20150312-00153 on 07/05/15
--      WHERE 1                         = 1
--      --AND QQ.QUALIFIER_CONTEXT        = 'CUSTOMER' --commented for version 1.5
--      AND QQ.COMPARISON_OPERATOR_CODE = '='
--      AND QQ.ACTIVE_FLAG              = 'Y'
--      AND ( ( QLH.ATTRIBUTE10         = ('Contract Pricing')
--      AND QQ.QUALIFIER_CONTEXT        = 'CUSTOMER' --added for version 1.5
--      AND QQ.QUALIFIER_ATTRIBUTE      = 'QUALIFIER_ATTRIBUTE11'
--      AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SHIP_TO_ORG_ID))
--      OR ( QLH.ATTRIBUTE10            = ('Contract Pricing')
--	  AND QQ.QUALIFIER_CONTEXT        = 'CUSTOMER' --added for version 1.5
--      AND QQ.QUALIFIER_ATTRIBUTE      = 'QUALIFIER_ATTRIBUTE32'
--      AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SOLD_TO_ORG_ID))
--      OR ( QLH.ATTRIBUTE10            = ('CSP-NATIONAL')
--      AND QQ.QUALIFIER_CONTEXT        = 'CUSTOMER' --added for version 1.5
--      AND QQ.QUALIFIER_ATTRIBUTE      = 'QUALIFIER_ATTRIBUTE32'
--      AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SOLD_TO_ORG_ID))
--      OR ( QQ.QUALIFIER_CONTEXT       = 'ORDER'
--	  AND QLH.ATTRIBUTE10             = ('Vendor Quote')
--	  AND QQ.QUALIFIER_ATTRIBUTE      = 'QUALIFIER_ATTRIBUTE18'
--	  AND (QQ.QUALIFIER_ATTR_VALUE    = OL.SHIP_FROM_ORG_ID)) --added for version 1.5
--	  )  
--      AND QLH.LIST_HEADER_ID          = QQ.LIST_HEADER_ID
--      AND QLH.ACTIVE_FLAG             = 'Y'
--        --                                  AND TO_CHAR (XXCPH.AGREEMENT_ID) =
--        --                                         (QLH.ATTRIBUTE14)
--      AND (XXCPH.AGREEMENT_ID)              = to_number(QLH.ATTRIBUTE14)
--      AND XXCPL.AGREEMENT_ID                = XXCPH.AGREEMENT_ID
--      AND XXCPL.AGREEMENT_TYPE              = 'VQN'
--      AND QLH.LIST_HEADER_ID                = QLL.LIST_HEADER_ID
--      AND XXCPL.PRODUCT_VALUE               = MSI.SEGMENT1
--      AND TO_CHAR (XXCPL.AGREEMENT_LINE_ID) = (QLL.ATTRIBUTE2)
--      AND MSI.INVENTORY_ITEM_ID             = OL.INVENTORY_ITEM_ID
--      AND MSI.ORGANIZATION_ID               = OL.SHIP_FROM_ORG_ID
--      AND POV.VENDOR_ID                     = XXCPL.VENDOR_ID
--        --AND MP.ORGANIZATION_ID = MSI.ORGANIZATION_ID
--      AND OOD.ORGANIZATION_ID     = MSI.ORGANIZATION_ID --Added by Mahender for 20150312-00153 on 07/05/15
--      -- AND RCT.CUSTOMER_TRX_ID     = OL.CUSTOMER_TRX_ID
--      AND GCC.CODE_COMBINATION_ID = OL.CODE_COMBINATION_ID
--	  AND OL.INVOICE_DATE        >= TRUNC (XXCPL.START_DATE)
--      AND OL.INVOICE_DATE        <= NVL (TRUNC (XXCPL.END_DATE), OL.INVOICE_DATE)
--        /*  AND NOT EXISTS
--        (SELECT /*+ qb_name(subq) * 'Y'
--        FROM MTL_SYSTEM_ITEMS_B_KFV
--        WHERE     INVENTORY_ITEM_ID =
--        MSI.INVENTORY_ITEM_ID
--        AND ORGANIZATION_ID =
--        MSI.ORGANIZATION_ID
--        AND SEGMENT1 LIKE 'SP%') */
--        --Commented by Mahender for TMS#20150805-00061 on 08/07/15
--        ) A
--      -- WHERE INVOICE_DATE <= END_DATE  --added INVOICE_DATE Instead of GL_DATE for version 1.4
--      -- AND INVOICE_DATE   >= START_DATE --added INVOICE_DATE Instead of GL_DATE for version 1.4
--      )
--    WHERE LIST_NUM = 1
--    );
--  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  FND_FILE.PUT_LINE (FND_FILE.LOG, 'in insert exception' || SQLERRM);
  -- Added For Ver 1.3
  APPS.XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API ( P_CALLED_FROM => L_ERR_CALLFROM , P_CALLING => L_ERR_CALLPOINT , P_REQUEST_ID => fnd_global.conc_request_id , P_ORA_ERROR_MSG => SQLERRM , P_ERROR_DESC => L_ERR_MSG , P_DISTRIBUTION_LIST => G_DFLT_EMAIL , p_module => 'OM');
  -- End of Ver 1.3
END;
END XXWC_VENDOR_QUOTE_TST_PKG;
/
