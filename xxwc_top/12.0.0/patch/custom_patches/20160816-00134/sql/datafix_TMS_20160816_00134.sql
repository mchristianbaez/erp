/*************************************************************************
  $Header datafix_TMS_20160816_00134.sql $
  Module Name: TMS_20160816-00134


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0       30-AUG-2016  Niraj K Ranjan        TMS#20160816-00134   Order 20778618 line 2.1 pending pre-bill
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160816-00134    , Before Update');

   UPDATE apps.oe_order_lines_all
            SET 
            shipping_quantity = ordered_quantity,
                shipped_quantity = ordered_quantity,
                actual_shipment_date = TO_DATE('01/07/2016 11:18:48','MM/DD/YYYY HH24:MI:SS'),
                shipping_quantity_uom = order_quantity_uom,
                fulfilled_flag = 'Y',
                fulfillment_date = TO_DATE('01/07/2016 11:18:48','MM/DD/YYYY HH24:MI:SS'),
                fulfilled_quantity = ordered_quantity,
                last_updated_by = -1,
                last_update_date = SYSDATE
--                ACCEPTED_BY = 4716,
--                REVREC_SIGNATURE = 'TODD MYERS',
--                REVREC_SIGNATURE_DATE = '07-FEB-2013'
          WHERE line_id = 71888577
          and headeR_id=43898386;

   DBMS_OUTPUT.put_line (
         'TMS: 20160816-00134  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160816-00134    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160816-00134 , Errors : ' || SQLERRM);
END;
/
