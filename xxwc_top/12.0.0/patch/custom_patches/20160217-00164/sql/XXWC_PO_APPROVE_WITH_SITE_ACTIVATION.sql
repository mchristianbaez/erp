   /**************************************************************************
     PURPOSE:   This script does the following 
	            1. Activate the supplier site
				2. Approves the PO
				3. Inactivates the Supplier Site
     REVISIONS:
     Ver        Date         Author                  Description
     ---------  -----------  ---------------         -------------------------
     1.0        8-Mar-2016  Manjula Chellappan      TMS# 20160217-00164  - Cleanup POs in requires reapproval status 
	                                                     but supplier sites are inactive
     **************************************************************************/
SET SERVEROUTPUT ON SIZE 200000;
SET VERIFY OFF;
DECLARE
--
lv_return_status varchar2(1);
lv_msg_count number;
lv_msg_data varchar2(2000);
lv_po_return_status varchar2(1);
lv_po_msg_data varchar2(2000);
--
lv_vendor_site_id ap_supplier_sites.vendor_site_id%type;
lv_vendor_site_rec AP_VENDOR_PUB_PKG.r_vendor_site_rec_type;
lv_approval_path_id NUMBER;
--

CURSOR cur_vendor_sites IS
SELECT DISTINCT vendor_site_id, vendor_site_code
FROM ap_supplier_sites_all aps
WHERE org_id                             = 162
AND NVL (INACTIVE_DATE, TRUNC (SYSDATE)) < TRUNC (SYSDATE)
AND EXISTS
  (SELECT 'x'
  FROM po_headers_v
  WHERE vendor_site_id     = aps.vendor_site_id
  AND authorization_status = 'REQUIRES REAPPROVAL'
  )  
ORDER BY 1;

CURSOR cur_po(p_vendor_site_id NUMBER) IS
SELECT po_header_id,
	  'PO' doc_type,
	  type_lookup_code doc_subtype,
	  segment1 po_number,
	  vendor_name,
	  vendor_site_code,
	  ship_to_location
FROM apps.po_headers_v
WHERE authorization_status = 'REQUIRES REAPPROVAL'
AND type_lookup_code = 'STANDARD'
AND vendor_site_id         = p_vendor_site_id;

BEGIN
--
fnd_global.apps_initialize(user_id=>2559,resp_id=>50893,resp_appl_id=>201);
apps.mo_global.init ('PO');
mo_global.set_policy_context('S','162');
--

FOR rec_vendor_sites IN cur_vendor_sites
LOOP

lv_vendor_site_id:= rec_vendor_sites.vendor_site_id;
lv_vendor_site_rec.inactive_date:= trunc(sysdate);
--
AP_VENDOR_PUB_PKG.Update_Vendor_Site
( p_api_version => 1.0
, p_init_msg_list => fnd_api.g_true
, p_commit => fnd_api.g_false
, p_validation_level => fnd_api.g_valid_level_full
, x_return_status => lv_return_status
, x_msg_count => lv_msg_count
, x_msg_data => lv_msg_data
, p_vendor_site_rec => lv_vendor_site_rec
, p_vendor_site_id => lv_vendor_site_id
);
--
IF lv_msg_count >0 THEN
	dbms_output.put_line(SUBSTR('Failed to Update Activate the Site '|| rec_vendor_sites.vendor_site_id||':'||
	rec_vendor_sites.vendor_site_code||lv_return_status||'-'||lv_msg_count||'-'||lv_msg_data,1,255));
	rollback;
ELSE

--Approve PO
FOR rec_po in cur_po(rec_vendor_sites.vendor_site_id)
LOOP

SELECT podt.default_approval_path_id
    INTO   lv_approval_path_id
    FROM   apps.po_document_types podt
    WHERE  podt.document_type_code   = rec_po.doc_type 
           AND podt.document_subtype = rec_po.doc_subtype;

apps.po_document_action_pvt.do_approve(
        p_document_id => rec_po.po_header_id,
        p_document_type => rec_po.doc_type,
        p_document_subtype => rec_po.doc_subtype,
        p_note => NULL,
        p_approval_path_id => lv_approval_path_id,
        x_return_status => lv_po_return_status,
        x_exception_msg => lv_po_msg_data );
		
		IF lv_po_return_status = 'S' THEN
			dbms_output.put_line(SUBSTR('Success - PO Number: '|| rec_po.po_number||', '||'Vendor Name: '||rec_po.vendor_name||', '||
			'Vendor Site Code: '||rec_po.vendor_site_code||', '||'ShipToLocation: '||rec_po.ship_to_location,1,255));
		ELSE
			dbms_output.put_line(SUBSTR('Error - PO Number: '|| rec_po.po_number||', '||'Vendor Name: '||rec_po.vendor_name||', '||
			'Vendor Site Code: '|| rec_po.vendor_site_code||', '|| 'ShipToLocation: '||rec_po.ship_to_location ||' - '||lv_po_msg_data,1,255));
		END IF;
	
END LOOP;

lv_msg_count:= NULL;
lv_msg_data := NULL;
lv_return_status := NULL;

lv_vendor_site_id:= rec_vendor_sites.vendor_site_id;
lv_vendor_site_rec.inactive_date:= trunc(sysdate)-1;
--
AP_VENDOR_PUB_PKG.Update_Vendor_Site
( p_api_version => 1.0
, p_init_msg_list => fnd_api.g_true
, p_commit => fnd_api.g_false
, p_validation_level => fnd_api.g_valid_level_full
, x_return_status => lv_return_status
, x_msg_count => lv_msg_count
, x_msg_data => lv_msg_data
, p_vendor_site_rec => lv_vendor_site_rec
, p_vendor_site_id => lv_vendor_site_id
);

	IF lv_msg_count = 0 THEN
		commit;
	ELSE 
		rollback;
	END IF;

END IF;

END LOOP;
EXCEPTION WHEN OTHERS THEN
dbms_output.put_line('Error : ' ||SUBSTR(SQLERRM,1,200));
END;
/
