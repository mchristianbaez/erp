/*************************************************************************
  $Header TMS_20170116-00276_CSP_BPW_Modifiers_to_EXCLUSIVE.sql $
  Module Name: TMS_20170116-00276  Data Fix script for Direct 20242952

  PURPOSE: Data fix script update all CSP BPW Modifiers to EXCLUSIVE

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        17-JAN-2017  Pattabhi Avula        TMS#20170116-00276 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170116-00276    , Before Update');

update apps.xxwc_om_contract_pricing_hdr 
set INCOMPATABILITY_GROUP='Exclusive'
where INCOMPATABILITY_GROUP='Best Price Wins';

   DBMS_OUTPUT.put_line (
         'TMS: 20170116-00276  CSP_BPW_Modifier lines updated (Expected:233): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170116-00276    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170116-00276 , Errors : ' || SQLERRM);
END;
/