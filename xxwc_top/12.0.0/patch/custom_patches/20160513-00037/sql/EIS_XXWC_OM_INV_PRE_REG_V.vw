DROP VIEW XXEIS.EIS_XXWC_OM_INV_PRE_REG_V;

/* Formatted on 5/13/2016 2:11:49 PM (QP5 v5.269.14213.34769) */
CREATE OR REPLACE FORCE VIEW XXEIS.EIS_XXWC_OM_INV_PRE_REG_V
(
   ORDER_NUMBER,
   ORDER_SOURCE,
   JOB_NUMBER,
   ORDERED_DATE,
   CREATED_BY,
   ORDER_TYPE,
   ORDER_STATUS,
   CUSTOMER_NUMBER,
   CUSTOMER_NAME,
   GROSS_AMOUNT,
   DISCOUNT_AMT,
   TAX_VALUE,
   FREIGHT_AMT,
   AVERAGE_COST,
   PROFIT,
   SALESREP_NAME,
   TRX_NUMBER,
   SALESREP_NUMBER,
   WAREHOUSE,
   INV_AMOUNT,
   SALES,
   SALE_COST,
   PAYMENT_TERM,
   PAYMENT_DESC,
   ITEM,
   ITEM_DESC,
   QTY,
   UOM,
   UNIT_SELLING_PRICE,
   ORDER_LINE,
   INVOICE_DATE,
   MODIFIER_NAME,
   LINE_FLOW_STATUS_CODE,
   CAT,
   SPECIAL_COST,
   VQN_MODIFIER_NAME,
   HEADER_ID,
   PARTY_ID,
   ORGANIZATION_ID,
   LINE_TYPE
)
AS
   SELECT                                                          --'A' dbug,
         oh.order_number,
          oos.name order_source,                                      -- ?????
          hps.party_site_number job_number,                           -- ?????
          TRUNC (oh.ordered_date) ordered_date,    --ppf.full_name created_by,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status,
          cust_acct.account_number customer_number,
          NVL (cust_acct.account_name, party.party_name) customer_name,
            NVL (ol.unit_selling_price, 0)
          * DECODE (ol.line_category_code,
                    'RETURN', (ol.ordered_quantity * -1),
                    ol.ordered_quantity)
             gross_amount,
          NVL (
             xxeis.eis_rs_xxwc_com_util_pkg.get_discount_amt (ol.header_id,
                                                              ol.line_id),
             0)
             discount_amt,
          NVL (
             DECODE (ol.line_category_code,
                     'RETURN', (ol.tax_value * -1),
                     ol.tax_value),
             0)
             tax_value,
          NVL (
             xxeis.eis_rs_xxwc_com_util_pkg.get_freight_amt (ol.header_id,
                                                             ol.line_id),
             0)
             freight_amt, /*decode(mcvc.segment2,
                          'PRMO',
                          (-1 * ol.unit_selling_price),
                          apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,
                          */
          -- Changed
          CASE
             WHEN mcvc.segment2 = 'PRMO' THEN (-1 * ol.unit_selling_price)
             ELSE apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
          --      ELSE NVL(apps.cst_cost_api.get_item_cost(1, msi.inventory_item_id, msi.organization_id), 0)
          END
             average_cost, --((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY)- (DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * OL.ORDERED_QUANTITY)) PROFIT,
          --((NVL(ol.unit_selling_price,0) * NVL (rctl.quantity_invoiced, rctl.quantity_credited) )- (NVL (rctl.quantity_invoiced, rctl.quantity_credited)* apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id),0 )) profit,
          0 profit,
          rep.name salesrep_name,
          rct.trx_number trx_number,
          rep.salesrep_number,
          ship_from_org.organization_code warehouse,
            NVL ( (rctl.extended_amount + NVL (rctl.tax_recoverable, 0)), 0)
          + xxeis.eis_rs_xxwc_com_util_pkg.get_charger_amt (oh.header_id)
             inv_amount, --((DECODE(xxeis.eis_rs_xxwc_com_util_pkg.get_promo_item(ol.inventory_item_id,ol.ship_from_org_id), 'N', ( NVL (rctl.quantity_invoiced, rctl.quantity_credited) * NVL (ol.unit_selling_price, 0)), 'Y', 0 ))+ NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_delivery_charge_amt(rct.customer_trx_id),0)) sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales, --(NVL(DECODE(mcvc.segment2, 'PRMO', 0, (NVL(rctl.quantity_invoiced, rctl.quantity_credited) * NVL(ol.unit_selling_price, 0))), 0)) sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))
                 * NVL (
                      apps.xxwc_mv_routines_pkg.get_order_line_cost (
                         ol.line_id),
                      0)),
              0))
             sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          rct.creation_date invoice_date,
          xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,
          ol.flow_status_code line_flow_status_code, -- 'Previous Day Invoices' TYPE,
          mcvc.segment2 cat,
          (SELECT xxcpl.special_cost
             FROM apps.oe_price_adjustments adj,
                  apps.qp_list_lines qll,
                  apps.qp_list_headers qlh,
                  apps.xxwc_om_contract_pricing_hdr xxcph,
                  apps.xxwc_om_contract_pricing_lines xxcpl
            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                  AND xxcpl.agreement_id = xxcph.agreement_id
                  AND xxcpl.agreement_type = 'VQN'
                  AND qlh.list_header_id = qll.list_header_id
                  AND qlh.list_header_id = adj.list_header_id
                  AND qll.list_line_id = adj.list_line_id
                  AND adj.list_line_type_code = 'DIS'
                  AND adj.applied_flag = 'Y'
                  AND adj.header_id = ol.header_id
                  AND adj.line_id = ol.line_id
                  AND xxcpl.product_value = msi.segment1
                  AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                  AND ROWNUM = 1)
             special_cost,
          (SELECT qlh.name
             FROM apps.oe_price_adjustments adj,
                  apps.qp_list_lines qll,
                  apps.qp_list_headers qlh,
                  apps.xxwc_om_contract_pricing_hdr xxcph,
                  apps.xxwc_om_contract_pricing_lines xxcpl
            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                  AND xxcpl.agreement_id = xxcph.agreement_id
                  AND xxcpl.agreement_type = 'VQN'
                  AND qlh.list_header_id = qll.list_header_id
                  AND qlh.list_header_id = adj.list_header_id
                  AND qll.list_line_id = adj.list_line_id
                  AND adj.list_line_type_code = 'DIS'
                  AND adj.applied_flag = 'Y'
                  AND adj.header_id = ol.header_id
                  AND adj.line_id = ol.line_id
                  AND xxcpl.product_value = msi.segment1
                  AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                  AND ROWNUM = 1)
             vqn_modifier_name,                                ---Primary Keys
          oh.header_id,
          party.party_id,
          ship_from_org.organization_id,
          otl.name line_type
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org,
          hz_parties party,
          hz_cust_accounts cust_acct,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
          ra_salesreps rep,
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          fnd_lookup_values_vl flv,                        --per_people_f ppf,
          --fnd_user fu ,
          ra_customer_trx rct,
          ra_customer_trx_lines rctl,           --oe_charge_lines_v      oclv,
          ra_terms rt,
          hz_cust_site_uses_all hcsu                          -- ????? > Start
                                    ,
          hz_cust_acct_sites_all hcas,
          hz_party_sites hps,
          oe_order_sources oos                                  -- ????? < End
    WHERE     ol.sold_to_org_id = cust_acct.cust_account_id(+)
          AND hcsu.site_use_id = oh.invoice_to_org_id         -- ????? > Start
          AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
          AND hps.party_site_id = hcas.party_site_id
          AND oos.order_source_id = oh.order_source_id          -- ????? < End
          AND cust_acct.party_id = party.party_id(+)
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.org_id = otl.org_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND flv.lookup_code(+) = oh.flow_status_code
          AND rctl.creation_date >=
                   TO_DATE (
                      TO_CHAR (
                         xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                            xxeis.eis_rs_utility.get_date_format
                         || ' HH24:MI:SS'),
                      xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 0.25
          AND rctl.creation_date <=
                   TO_DATE (
                      TO_CHAR (
                         xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                            xxeis.eis_rs_utility.get_date_format
                         || ' HH24:MI:SS'),
                      xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 1.25
          AND TRUNC (oh.ordered_date) < TRUNC (SYSDATE)
          AND ol.flow_status_code IN ('CLOSED')
          AND ol.invoice_interface_status_code = 'YES'
          AND oth.name != 'INTERNAL ORDER'
          --AND fu.user_id                       =ol.created_by
          --AND fu.employee_id                   =ppf.person_id(+)
          AND rct.customer_trx_id = rctl.customer_trx_id
          AND TO_CHAR (ol.line_id) = rctl.interface_line_attribute6
          AND TO_CHAR (oh.order_number) = rctl.interface_line_attribute1
          AND rctl.interface_line_context = 'ORDER ENTRY'
          --AND oh.header_id = oclv.header_id(+)
          --AND TRUNC (ol.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (ol.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (ol.creation_date) )
          --added
          ---AND ol.fulfilled_flag    ='Y'
          AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
          -- and OH.ORDER_NUMBER = 12916546
          AND NOT EXISTS
                 (SELECT 'Y'
                    FROM gl_code_combinations_kfv gcc
                   WHERE     1 = 1
                         AND gcc.code_combination_id =
                                msi.cost_of_sales_account
                         AND gcc.segment4 = '646080')
   UNION
   /* Same Days Counter Orders */
   SELECT                                                          --'B' dbug,
         oh.order_number,
          oos.name order_source,                                      -- ?????
          hps.party_site_number job_number,                           -- ?????
          TRUNC (oh.ordered_date) ordered_date,    --ppf.full_name created_by,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status,
          cust_acct.account_number customer_number,
          NVL (cust_acct.account_name, party.party_name) customer_name,
          (  NVL (ol.unit_selling_price, 0)
           * DECODE (ol.line_category_code,
                     'RETURN', (ol.ordered_quantity * -1),
                     ol.ordered_quantity))
             gross_amount,
          (NVL (
              xxeis.eis_rs_xxwc_com_util_pkg.get_discount_amt (ol.header_id,
                                                               ol.line_id),
              0))
             discount_amt,
          (NVL (
              DECODE (ol.line_category_code,
                      'RETURN', (ol.tax_value * -1),
                      ol.tax_value),
              0))
             tax_value,
          (NVL (
              xxeis.eis_rs_xxwc_com_util_pkg.get_freight_amt (ol.header_id,
                                                              ol.line_id),
              0))
             freight_amt, --decode(decode(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost),0,xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id,msi.organization_id),nvl(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost,
          /*decode(mcvc.segment2,
          'PRMO',
          (-1 * ol.unit_selling_price),
          apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,*/
          -- Changed
          CASE
             WHEN mcvc.segment2 = 'PRMO'
             THEN
                (-1 * ol.unit_selling_price)
             WHEN ol.flow_status_code IN ('CLOSED')
             THEN
                apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
             WHEN otl.name = 'BILL ONLY'
             THEN
                0
             ELSE
                DECODE (
                   NVL (
                      apps.cst_cost_api.get_item_cost (1,
                                                       msi.inventory_item_id,
                                                       msi.organization_id),
                      0),
                   0, ol.unit_cost,
                   apps.cst_cost_api.get_item_cost (1,
                                                    msi.inventory_item_id,
                                                    msi.organization_id))
          END
             average_cost, --(((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY)- (DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * OL.ORDERED_QUANTITY))) PROFIT,
          --((NVL(ol.unit_selling_price,0) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))- (DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity) * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id),0))) profit,
          0 profit,
          rep.name,
          NULL trx_number,
          rep.salesrep_number,
          ship_from_org.organization_code warehouse,
          NULL inv_amount,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (CASE
                       WHEN ol.flow_status_code IN ('CLOSED')
                       THEN
                          apps.xxwc_mv_routines_pkg.get_order_line_cost (
                             ol.line_id)
                       WHEN otl.name = 'BILL ONLY'
                       THEN
                          0
                       ELSE
                          DECODE (
                             NVL (
                                apps.cst_cost_api.get_item_cost (
                                   1,
                                   msi.inventory_item_id,
                                   msi.organization_id),
                                0),
                             0, ol.unit_cost,
                             apps.cst_cost_api.get_item_cost (
                                1,
                                msi.inventory_item_id,
                                msi.organization_id))
                    END)
                 * (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))),
              0))
             sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          NULL invoice_date,
          xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,
          ol.flow_status_code line_flow_status_code, -- 'Same Day Counter Orders' TYPE,
          mcvc.segment2 cat,
          (SELECT xxcpl.special_cost
             FROM apps.oe_price_adjustments adj,
                  apps.qp_list_lines qll,
                  apps.qp_list_headers qlh,
                  apps.xxwc_om_contract_pricing_hdr xxcph,
                  apps.xxwc_om_contract_pricing_lines xxcpl
            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                  AND xxcpl.agreement_id = xxcph.agreement_id
                  AND xxcpl.agreement_type = 'VQN'
                  AND qlh.list_header_id = qll.list_header_id
                  AND qlh.list_header_id = adj.list_header_id
                  AND qll.list_line_id = adj.list_line_id
                  AND adj.list_line_type_code = 'DIS'
                  AND adj.applied_flag = 'Y'
                  AND adj.header_id = ol.header_id
                  AND adj.line_id = ol.line_id
                  AND xxcpl.product_value = msi.segment1
                  AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                  AND ROWNUM = 1)
             special_cost,
          (SELECT qlh.name
             FROM apps.oe_price_adjustments adj,
                  apps.qp_list_lines qll,
                  apps.qp_list_headers qlh,
                  apps.xxwc_om_contract_pricing_hdr xxcph,
                  apps.xxwc_om_contract_pricing_lines xxcpl
            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                  AND xxcpl.agreement_id = xxcph.agreement_id
                  AND xxcpl.agreement_type = 'VQN'
                  AND qlh.list_header_id = qll.list_header_id
                  AND qlh.list_header_id = adj.list_header_id
                  AND qll.list_line_id = adj.list_line_id
                  AND adj.list_line_type_code = 'DIS'
                  AND adj.applied_flag = 'Y'
                  AND adj.header_id = ol.header_id
                  AND adj.line_id = ol.line_id
                  AND xxcpl.product_value = msi.segment1
                  AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                  AND ROWNUM = 1)
             vqn_modifier_name,                                ---Primary Keys
          oh.header_id,
          party.party_id,
          ship_from_org.organization_id,
          otl.name line_type
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org,
          hz_parties party,
          hz_cust_accounts cust_acct,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
          ra_salesreps rep,
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          fnd_lookup_values_vl flv,                        --per_people_f ppf,
          --fnd_user fu ,
          ra_terms rt,
          hz_cust_site_uses_all hcsu                          -- ????? > Start
                                    ,
          hz_cust_acct_sites_all hcas,
          hz_party_sites hps,
          oe_order_sources oos                                  -- ????? < End
    --added
    ---ra_customer_trx rct,
    ---ra_customer_trx_lines rctl
    WHERE     ol.sold_to_org_id = cust_acct.cust_account_id(+)
          AND hcsu.site_use_id = oh.invoice_to_org_id         -- ????? > Start
          AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
          AND hps.party_site_id = hcas.party_site_id
          AND oos.order_source_id = oh.order_source_id          -- ????? < End
          AND cust_acct.party_id = party.party_id(+)
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.flow_status_code <> 'CANCELLED'
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND ol.org_id = otl.org_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND oth.name != 'INTERNAL ORDER'
          AND flv.lookup_code(+) = oh.flow_status_code
          AND TRUNC (SYSDATE) =
                 TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
          AND oth.name = 'COUNTER ORDER'
          AND oh.flow_status_code = 'BOOKED'
          AND TRUNC (oh.ordered_date) = TRUNC (SYSDATE)
          --AND fu.user_id               = ol.created_by
          --AND fu.employee_id           = ppf.person_id(+)
          --AND TRUNC (ol.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (ol.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (ol.creation_date) )
          --added
          --AND ol.fulfilled_flag           ='Y'
          --AND rct.customer_trx_id         = rctl.customer_trx_id
          --AND TO_CHAR(ol.line_id)         = rctl.interface_line_attribute6
          --AND TO_CHAR (oh.order_number)   = rctl.interface_line_attribute1
          --AND rctl.interface_line_context = 'ORDER ENTRY'
          AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
          AND NOT EXISTS
                 (SELECT 'Y'
                    FROM gl_code_combinations_kfv gcc
                   WHERE     1 = 1
                         AND gcc.code_combination_id =
                                msi.cost_of_sales_account
                         AND gcc.segment4 = '646080')
   UNION
   /* Invoiced Lines after 6 AM on the same day */
   SELECT                                                          --'C' dbug,
         oh.order_number,
          oos.name order_source,                                      -- ?????
          hps.party_site_number job_number,                           -- ?????
          TRUNC (oh.ordered_date) ordered_date,    --ppf.full_name created_by,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status,
          cust_acct.account_number customer_number,
          NVL (cust_acct.account_name, party.party_name) customer_name,
          (  NVL (ol.unit_selling_price, 0)
           * DECODE (ol.line_category_code,
                     'RETURN', (ol.ordered_quantity * -1),
                     ol.ordered_quantity))
             gross_amount,
          (NVL (
              xxeis.eis_rs_xxwc_com_util_pkg.get_discount_amt (ol.header_id,
                                                               ol.line_id),
              0))
             discount_amt,
          (NVL (
              DECODE (ol.line_category_code,
                      'RETURN', (ol.tax_value * -1),
                      ol.tax_value),
              0))
             tax_value,
          (NVL (
              xxeis.eis_rs_xxwc_com_util_pkg.get_freight_amt (ol.header_id,
                                                              ol.line_id),
              0))
             freight_amt, /*decode(mcvc.segment2,
                          'PRMO',
                          (-1 * ol.unit_selling_price),
                          apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,*/
          -- Changed
          CASE
             WHEN mcvc.segment2 = 'PRMO' THEN (-1 * ol.unit_selling_price) /*      WHEN ol.flow_status_code IN ('CLOSED','INVOICED', 'INVOICED_PARTIAL', 'INVOICE_DELIVERY', 'INVOICE_HOLD', 'INVOICE_INCOMPLETE', 'INVOICE_NOT_APPLICABLE', 'INVOICE_RFR', 'INVOICE_UNEXPECTED_ERROR')
                                                                           THEN apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id) */
             ELSE apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) /*NVL(apps.cst_cost_api.get_item_cost(1, msi.inventory_item_id, msi.organization_id), 0)*/
          END
             average_cost, --(((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY)- (DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * OL.ORDERED_QUANTITY))) PROFIT,
          --    ((NVL(ol.unit_selling_price,0) * NVL (rctl.quantity_invoiced, rctl.quantity_credited)) -(NVL (rctl.quantity_invoiced, rctl.quantity_credited)* NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0))) profit,
          0 profit,
          rep.name,
          rct.trx_number trx_number,
          rep.salesrep_number,
          ship_from_org.organization_code warehouse,
            NVL ( (rctl.extended_amount + NVL (rctl.tax_recoverable, 0)), 0)
          + xxeis.eis_rs_xxwc_com_util_pkg.get_charger_amt (oh.header_id)
             inv_amount, --(NVL(DECODE(mcvc.segment2, 'PRMO', 0, (NVL(rctl.quantity_invoiced, rctl.quantity_credited) * NVL(ol.unit_selling_price, 0))), 0)) sales,
          -- (NVL(DECODE(mcvc.segment2, 'PRMO',                                                         -1 * (NVL(rctl.quantity_invoiced, rctl.quantity_credited) * NVL(ol.unit_selling_price, 0)), NVL(rctl.quantity_invoiced, rctl.quantity_credited) * NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id), 0)), 0)) sale_cost,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales, --(NVL(DECODE(mcvc.segment2, 'PRMO', 0, (NVL(rctl.quantity_invoiced, rctl.quantity_credited) * NVL(ol.unit_selling_price, 0))), 0)) sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))
                 * NVL (
                      apps.xxwc_mv_routines_pkg.get_order_line_cost (
                         ol.line_id),
                      0)),
              0))
             sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          rct.creation_date invoice_date,
          xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,
          ol.flow_status_code line_flow_status_code, -- 'Same Day Invoices' TYPE,
          mcvc.segment2 cat,
          (SELECT xxcpl.special_cost
             FROM apps.oe_price_adjustments adj,
                  apps.qp_list_lines qll,
                  apps.qp_list_headers qlh,
                  apps.xxwc_om_contract_pricing_hdr xxcph,
                  apps.xxwc_om_contract_pricing_lines xxcpl
            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                  AND xxcpl.agreement_id = xxcph.agreement_id
                  AND xxcpl.agreement_type = 'VQN'
                  AND qlh.list_header_id = qll.list_header_id
                  AND qlh.list_header_id = adj.list_header_id
                  AND qll.list_line_id = adj.list_line_id
                  AND adj.list_line_type_code = 'DIS'
                  AND adj.applied_flag = 'Y'
                  AND adj.header_id = ol.header_id
                  AND adj.line_id = ol.line_id
                  AND xxcpl.product_value = msi.segment1
                  AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                  AND ROWNUM = 1)
             special_cost,
          (SELECT qlh.name
             FROM apps.oe_price_adjustments adj,
                  apps.qp_list_lines qll,
                  apps.qp_list_headers qlh,
                  apps.xxwc_om_contract_pricing_hdr xxcph,
                  apps.xxwc_om_contract_pricing_lines xxcpl
            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                  AND xxcpl.agreement_id = xxcph.agreement_id
                  AND xxcpl.agreement_type = 'VQN'
                  AND qlh.list_header_id = qll.list_header_id
                  AND qlh.list_header_id = adj.list_header_id
                  AND qll.list_line_id = adj.list_line_id
                  AND adj.list_line_type_code = 'DIS'
                  AND adj.applied_flag = 'Y'
                  AND adj.header_id = ol.header_id
                  AND adj.line_id = ol.line_id
                  AND xxcpl.product_value = msi.segment1
                  AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                  AND ROWNUM = 1)
             vqn_modifier_name,                                ---Primary Keys
          oh.header_id,
          party.party_id,
          ship_from_org.organization_id,
          otl.name line_type
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org,
          hz_parties party,
          hz_cust_accounts cust_acct,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
          ra_salesreps rep,
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          fnd_lookup_values_vl flv,                        --per_people_f ppf,
          --fnd_user fu ,
          ra_customer_trx rct,
          ra_customer_trx_lines rctl,
          ra_terms rt,
          hz_cust_site_uses_all hcsu                          -- ????? > Start
                                    ,
          hz_cust_acct_sites_all hcas,
          hz_party_sites hps,
          oe_order_sources oos                                  -- ????? < End
    WHERE     ol.sold_to_org_id = cust_acct.cust_account_id(+)
          AND hcsu.site_use_id = oh.invoice_to_org_id         -- ????? > Start
          AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
          AND hps.party_site_id = hcas.party_site_id
          AND oos.order_source_id = oh.order_source_id          -- ????? < End
          AND cust_acct.party_id = party.party_id(+)
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.flow_status_code <> 'CANCELLED'
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND ol.org_id = otl.org_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND oth.name != 'INTERNAL ORDER'
          AND flv.lookup_code(+) = oh.flow_status_code
          AND TRUNC (SYSDATE) =
                 TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
          AND rctl.creation_date >=
                   TO_DATE (
                      TO_CHAR (
                         xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                            xxeis.eis_rs_utility.get_date_format
                         || ' HH24:MI:SS'),
                      xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 0.25
          --AND fu.user_id          = ol.created_by
          --AND fu.employee_id      = ppf.person_id(+)
          AND rct.customer_trx_id = rctl.customer_trx_id
          AND TO_CHAR (ol.line_id) = rctl.interface_line_attribute6
          -- AND TO_CHAR (OH.ORDER_NUMBER)      = RCTL.INTERFACE_LINE_ATTRIBUTE1
          -- AND RCTL.INTERFACE_LINE_CONTEXT    = 'ORDER ENTRY'
          --AND TRUNC (ol.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (ol.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (ol.creation_date) )
          --added
          --  AND ol.fulfilled_flag    ='Y'
          AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
          -- and OH.ORDER_NUMBER = 12916546
          AND NOT EXISTS
                 (SELECT 'Y'
                    FROM gl_code_combinations_kfv gcc
                   WHERE     1 = 1
                         AND gcc.code_combination_id =
                                msi.cost_of_sales_account
                         AND gcc.segment4 = '646080')
   UNION
   SELECT                                                          --'D' dbug,
         oh.order_number,
          oos.name order_source,                                      -- ?????
          hps.party_site_number job_number,                           -- ?????
          TRUNC (oh.ordered_date) ordered_date,    --ppf.full_name created_by,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status,
          cust_acct.account_number customer_number,
          NVL (cust_acct.account_name, party.party_name) customer_name,
          (  NVL (ol.unit_selling_price, 0)
           * DECODE (ol.line_category_code,
                     'RETURN', (ol.ordered_quantity * -1),
                     ol.ordered_quantity))
             gross_amount,
          (NVL (
              xxeis.eis_rs_xxwc_com_util_pkg.get_discount_amt (ol.header_id,
                                                               ol.line_id),
              0))
             discount_amt,
          (NVL (
              DECODE (ol.line_category_code,
                      'RETURN', (ol.tax_value * -1),
                      ol.tax_value),
              0))
             tax_value,
          (NVL (
              xxeis.eis_rs_xxwc_com_util_pkg.get_freight_amt (ol.header_id,
                                                              ol.line_id),
              0))
             freight_amt, --DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost, cm
          --DECODE(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost),0,xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id,msi.organization_id),NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost,
          /*decode(mcvc.segment2,
          'PRMO',
          (-1 * ol.unit_selling_price),
          apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,*/
          -- Changed
          CASE
             WHEN mcvc.segment2 = 'PRMO'
             THEN
                (-1 * ol.unit_selling_price)
             WHEN ol.flow_status_code IN ('CLOSED',
                                          'INVOICED',
                                          'INVOICED_PARTIAL',
                                          'INVOICE_DELIVERY',
                                          'INVOICE_HOLD',
                                          'INVOICE_INCOMPLETE')
             THEN
                apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
             WHEN otl.name = 'BILL ONLY'
             THEN
                0
             ELSE
                NVL (
                   apps.cst_cost_api.get_item_cost (1,
                                                    msi.inventory_item_id,
                                                    msi.organization_id),
                   0)
          END
             average_cost, --(((nvl(ol.unit_selling_price,0) * ol.ordered_quantity)-
          --(NVL(DECODE(APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID),0,
          --OL.UNIT_COST,APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID) ),0) * OL.ORDERED_QUANTITY))) PROFIT,
          --((NVL(ol.unit_selling_price,0) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))- (DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity) * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) , 0))) profit,
          0 profit,
          rep.name,
          NULL trx_number,
          rep.salesrep_number,
          ship_from_org.organization_code warehouse,
          NULL inv_amount, -- (NVL(OL.UNIT_SELLING_PRICE,0)                                                                                                                                 * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY))SALES,
          --(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))sale_cost,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (CASE
                       WHEN ol.flow_status_code IN ('CLOSED',
                                                    'INVOICED',
                                                    'INVOICED_PARTIAL',
                                                    'INVOICE_DELIVERY',
                                                    'INVOICE_HOLD',
                                                    'INVOICE_INCOMPLETE')
                       THEN
                          apps.xxwc_mv_routines_pkg.get_order_line_cost (
                             ol.line_id)
                       WHEN otl.name = 'BILL ONLY'
                       THEN
                          0
                       ELSE
                          NVL (
                             apps.cst_cost_api.get_item_cost (
                                1,
                                msi.inventory_item_id,
                                msi.organization_id),
                             0)
                    END)
                 * (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))),
              0))
             sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          NULL invoice_date,
          xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,
          ol.flow_status_code line_flow_status_code, -- 'Waiting for Invoices' TYPE,
          mcvc.segment2 cat,
          (SELECT xxcpl.special_cost
             FROM apps.oe_price_adjustments adj,
                  apps.qp_list_lines qll,
                  apps.qp_list_headers qlh,
                  apps.xxwc_om_contract_pricing_hdr xxcph,
                  apps.xxwc_om_contract_pricing_lines xxcpl
            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                  AND xxcpl.agreement_id = xxcph.agreement_id
                  AND xxcpl.agreement_type = 'VQN'
                  AND qlh.list_header_id = qll.list_header_id
                  AND qlh.list_header_id = adj.list_header_id
                  AND qll.list_line_id = adj.list_line_id
                  AND adj.list_line_type_code = 'DIS'
                  AND adj.applied_flag = 'Y'
                  AND adj.header_id = ol.header_id
                  AND adj.line_id = ol.line_id
                  AND xxcpl.product_value = msi.segment1
                  AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                  AND ROWNUM = 1)
             special_cost,
          (SELECT qlh.name
             FROM apps.oe_price_adjustments adj,
                  apps.qp_list_lines qll,
                  apps.qp_list_headers qlh,
                  apps.xxwc_om_contract_pricing_hdr xxcph,
                  apps.xxwc_om_contract_pricing_lines xxcpl
            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                  AND xxcpl.agreement_id = xxcph.agreement_id
                  AND xxcpl.agreement_type = 'VQN'
                  AND qlh.list_header_id = qll.list_header_id
                  AND qlh.list_header_id = adj.list_header_id
                  AND qll.list_line_id = adj.list_line_id
                  AND adj.list_line_type_code = 'DIS'
                  AND adj.applied_flag = 'Y'
                  AND adj.header_id = ol.header_id
                  AND adj.line_id = ol.line_id
                  AND xxcpl.product_value = msi.segment1
                  AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                  AND ROWNUM = 1)
             vqn_modifier_name,                                ---Primary Keys
          oh.header_id,
          party.party_id,                                   -- Rct.trx_number,
          ship_from_org.organization_id,
          otl.name line_type
     --ol.ship_from_org_id organization_id
     --added
     --NULL extended_sale,
     --NULL extended_cost
     --    msi.inventory_item_id ,
     --    msi.organization_id msi_organization_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org,
          hz_parties party,
          hz_cust_accounts cust_acct,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
          ra_salesreps rep,
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          fnd_lookup_values_vl flv,
          ra_terms rt,
          hz_cust_site_uses_all hcsu                          -- ????? > Start
                                    ,
          hz_cust_acct_sites_all hcas,
          hz_party_sites hps,
          oe_order_sources oos                                  -- ????? < End
    WHERE     ol.sold_to_org_id = cust_acct.cust_account_id(+)
          AND hcsu.site_use_id = oh.invoice_to_org_id         -- ????? > Start
          AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
          AND hps.party_site_id = hcas.party_site_id
          AND oos.order_source_id = oh.order_source_id          -- ????? < End
          AND cust_acct.party_id = party.party_id(+)
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND ol.org_id = otl.org_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND oth.name != 'INTERNAL ORDER'
          AND flv.lookup_code(+) = oh.flow_status_code
          AND (   (    oth.name IN ('STANDARD ORDER')
                   AND (   ol.user_item_description = 'DELIVERED'
                        OR (    ol.source_type_code = 'EXTERNAL'
                            AND ol.flow_status_code = 'INVOICE_HOLD')))
               OR (    oth.name IN ('REPAIR ORDER', 'RETURN ORDER')
                   AND ol.flow_status_code = 'INVOICE_HOLD')
               --  OR (OTH.NAME                 IN ('WC SHORT TERM RENTAL','WC LONG TERM RENTAL')
               --  AND ol.flow_status_code      IN ('INVOICE_HOLD','CLOSED'))
               OR (EXISTS
                      (SELECT 1
                         FROM ra_interface_lines_all ril
                        WHERE     ril.interface_line_context = 'ORDER ENTRY'
                              AND TO_CHAR (ol.line_id) =
                                     ril.interface_line_attribute6
                              AND TO_CHAR (oh.order_number) =
                                     ril.interface_line_attribute1)))
          AND TRUNC (SYSDATE) =
                 TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
          AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
          --and OH.ORDER_NUMBER IN( 12916583,12916584)
          AND NOT EXISTS
                 (SELECT 'Y'
                    FROM gl_code_combinations_kfv gcc
                   WHERE     1 = 1
                         AND gcc.code_combination_id =
                                msi.cost_of_sales_account
                         AND gcc.segment4 = '646080')
   UNION
   -- for rental  orders before invoice
   SELECT                                                          --'D' dbug,
         oh.order_number,
          oos.name order_source,                                      -- ?????
          hps.party_site_number job_number,                           -- ?????
          TRUNC (oh.ordered_date) ordered_date,    --ppf.full_name created_by,
          oh.attribute7 created_by,
          oth.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status,
          cust_acct.account_number customer_number,
          NVL (cust_acct.account_name, party.party_name) customer_name,
          (  NVL (ol.unit_selling_price, 0)
           * DECODE (ol.line_category_code,
                     'RETURN', (ol.ordered_quantity * -1),
                     ol.ordered_quantity))
             gross_amount,
          (NVL (
              xxeis.eis_rs_xxwc_com_util_pkg.get_discount_amt (ol.header_id,
                                                               ol.line_id),
              0))
             discount_amt,
          (NVL (
              DECODE (ol.line_category_code,
                      'RETURN', (ol.tax_value * -1),
                      ol.tax_value),
              0))
             tax_value,
          (NVL (
              xxeis.eis_rs_xxwc_com_util_pkg.get_freight_amt (ol.header_id,
                                                              ol.line_id),
              0))
             freight_amt, --DECODE(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost, cm
          --DECODE(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost),0,xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id,msi.organization_id),NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) average_cost,
          /*decode(mcvc.segment2,
          'PRMO',
          (-1 * ol.unit_selling_price),
          apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id)) average_cost,*/
          -- Changed
          CASE
             WHEN mcvc.segment2 = 'PRMO'
             THEN
                (-1 * ol.unit_selling_price)
             WHEN ol.flow_status_code IN ('CLOSED',
                                          'INVOICED',
                                          'INVOICED_PARTIAL',
                                          'INVOICE_DELIVERY',
                                          'INVOICE_HOLD',
                                          'INVOICE_INCOMPLETE')
             THEN
                apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id)
             WHEN otl.name = 'BILL ONLY'
             THEN
                0
             ELSE
                NVL (
                   apps.cst_cost_api.get_item_cost (1,
                                                    msi.inventory_item_id,
                                                    msi.organization_id),
                   0)
          END
             average_cost, --(((nvl(ol.unit_selling_price,0) * ol.ordered_quantity)-
          --(NVL(DECODE(APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID),0,
          --OL.UNIT_COST,APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST(OL.LINE_ID) ),0) * OL.ORDERED_QUANTITY))) PROFIT,
          --((NVL(ol.unit_selling_price,0) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))- (DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity) * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) , 0))) profit,
          0 profit,
          rep.name,
          NULL trx_number,
          rep.salesrep_number,
          ship_from_org.organization_code warehouse,
          NULL inv_amount, -- (NVL(OL.UNIT_SELLING_PRICE,0)                                                                                                                                 * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY))SALES,
          --(DECODE(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0,ol.unit_cost,NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),ol.unit_cost)) * DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))sale_cost,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO', 0,
                   NVL (ol.unit_selling_price, 0)
                 * DECODE (ol.line_category_code,
                           'RETURN', (ol.ordered_quantity * -1),
                           ol.ordered_quantity)),
              0))
             sales,
          (NVL (
              DECODE (
                 mcvc.segment2,
                 'PRMO',   -1
                         * (  NVL (ol.unit_selling_price, 0)
                            * DECODE (ol.line_category_code,
                                      'RETURN', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity)),
                   (CASE
                       WHEN ol.flow_status_code IN ('CLOSED',
                                                    'INVOICED',
                                                    'INVOICED_PARTIAL',
                                                    'INVOICE_DELIVERY',
                                                    'INVOICE_HOLD',
                                                    'INVOICE_INCOMPLETE')
                       THEN
                          apps.xxwc_mv_routines_pkg.get_order_line_cost (
                             ol.line_id)
                       WHEN otl.name = 'BILL ONLY'
                       THEN
                          0
                       ELSE
                          NVL (
                             apps.cst_cost_api.get_item_cost (
                                1,
                                msi.inventory_item_id,
                                msi.organization_id),
                             0)
                    END)
                 * (DECODE (ol.line_category_code,
                            'RETURN', (ol.ordered_quantity * -1),
                            ol.ordered_quantity))),
              0))
             sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          CASE
             WHEN msi.segment1 = 'Rental Charge'
             THEN
                ol.user_item_description
             ELSE
                msi.description
          END
             item_desc,
          DECODE (ol.line_category_code,
                  'RETURN', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             qty,
          ol.order_quantity_uom uom,
          DECODE (mcvc.segment2, 'PRMO', 0, ol.unit_selling_price)
             unit_selling_price,
          DECODE (
             ol.option_number,
             '', (ol.line_number || '.' || ol.shipment_number),
             (   ol.line_number
              || '.'
              || ol.shipment_number
              || '.'
              || ol.option_number))
             order_line,
          NULL invoice_date,
          xxeis.eis_rs_xxwc_com_util_pkg.get_line_modifier_name (
             oh.header_id,
             ol.line_id)
             modifier_name,
          ol.flow_status_code line_flow_status_code, -- 'Waiting for Invoices' TYPE,
          mcvc.segment2 cat,
          (SELECT xxcpl.special_cost
             FROM apps.oe_price_adjustments adj,
                  apps.qp_list_lines qll,
                  apps.qp_list_headers qlh,
                  apps.xxwc_om_contract_pricing_hdr xxcph,
                  apps.xxwc_om_contract_pricing_lines xxcpl
            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                  AND xxcpl.agreement_id = xxcph.agreement_id
                  AND xxcpl.agreement_type = 'VQN'
                  AND qlh.list_header_id = qll.list_header_id
                  AND qlh.list_header_id = adj.list_header_id
                  AND qll.list_line_id = adj.list_line_id
                  AND adj.list_line_type_code = 'DIS'
                  AND adj.applied_flag = 'Y'
                  AND adj.header_id = ol.header_id
                  AND adj.line_id = ol.line_id
                  AND xxcpl.product_value = msi.segment1
                  AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                  AND ROWNUM = 1)
             special_cost,
          (SELECT qlh.name
             FROM apps.oe_price_adjustments adj,
                  apps.qp_list_lines qll,
                  apps.qp_list_headers qlh,
                  apps.xxwc_om_contract_pricing_hdr xxcph,
                  apps.xxwc_om_contract_pricing_lines xxcpl
            WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                  AND xxcpl.agreement_id = xxcph.agreement_id
                  AND xxcpl.agreement_type = 'VQN'
                  AND qlh.list_header_id = qll.list_header_id
                  AND qlh.list_header_id = adj.list_header_id
                  AND qll.list_line_id = adj.list_line_id
                  AND adj.list_line_type_code = 'DIS'
                  AND adj.applied_flag = 'Y'
                  AND adj.header_id = ol.header_id
                  AND adj.line_id = ol.line_id
                  AND xxcpl.product_value = msi.segment1
                  AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                  AND ROWNUM = 1)
             vqn_modifier_name,                                ---Primary Keys
          oh.header_id,
          party.party_id,                                   -- Rct.trx_number,
          ship_from_org.organization_id,
          otl.name line_type
     --ol.ship_from_org_id organization_id
     --added
     --NULL extended_sale,
     --NULL extended_cost
     --    msi.inventory_item_id ,
     --    msi.organization_id msi_organization_id
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters ship_from_org,
          hz_parties party,
          hz_cust_accounts cust_acct,
          oe_order_headers oh,
          oe_order_lines ol,
          hr_all_organization_units hou,
          ra_salesreps rep,
          oe_transaction_types_vl oth,
          oe_transaction_types_vl otl,
          mtl_system_items_kfv msi,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          fnd_lookup_values_vl flv,
          ra_terms rt,
          hz_cust_site_uses_all hcsu                          -- ????? > Start
                                    ,
          hz_cust_acct_sites_all hcas,
          hz_party_sites hps,
          oe_order_sources oos                                  -- ????? < End
    WHERE     ol.sold_to_org_id = cust_acct.cust_account_id(+)
          AND hcsu.site_use_id = oh.invoice_to_org_id         -- ????? > Start
          AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
          AND hps.party_site_id = hcas.party_site_id
          AND oos.order_source_id = oh.order_source_id          -- ????? < End
          AND cust_acct.party_id = party.party_id(+)
          AND ol.ship_from_org_id = ship_from_org.organization_id(+)
          AND ol.header_id = oh.header_id
          AND msi.organization_id = hou.organization_id
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          AND oh.payment_term_id = rt.term_id
          AND msi.inventory_item_id(+) = ol.inventory_item_id
          AND msi.organization_id(+) = ol.ship_from_org_id
          AND msi.inventory_item_id = micc.inventory_item_id(+)
          AND msi.organization_id = micc.organization_id(+)
          AND micc.category_id = mcvc.category_id(+)
          AND mcvc.structure_id(+) = 101
          AND micc.category_set_id(+) = 1100000062
          AND oh.order_type_id = oth.transaction_type_id
          AND oh.org_id = oth.org_id
          AND ol.flow_status_code <> 'CANCELLED'
          -- AND ol.flow_status_code NOT  IN ('CLOSED', 'CANCELLED')
          AND ol.line_type_id = otl.transaction_type_id
          AND ol.ordered_item NOT IN ('CONTOFFSET', 'CONTBILL')
          AND ol.org_id = otl.org_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND oth.name != 'INTERNAL ORDER'
          AND flv.lookup_code(+) = oh.flow_status_code
          AND ol.line_type_id NOT IN (1015, 1007)
          AND (   (    oth.name IN ('WC SHORT TERM RENTAL',
                                    'WC LONG TERM RENTAL')
                   AND ol.flow_status_code IN ('INVOICE_HOLD', 'CLOSED')
                   AND TRUNC (oh.creation_date) = TRUNC (SYSDATE) --  AND TRUNC(OL.CREATION_DATE)       >= to_date(TO_CHAR(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
                                                                 --    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
                                                                 --    || ' HH24:MI:SS') + 0.25
                                                                 --  AND TRUNC(OL.CREATION_DATE) <= to_date(TO_CHAR(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
                                                                 --    || ' HH24:MI:SS'), XXEIS.EIS_RS_UTILITY.GET_DATE_FORMAT
                                                                 --    || ' HH24:MI:SS') + 1.25
                  )
               --  OR (EXISTS
               --    (SELECT 1
               --    FROM ra_interface_lines_all ril
               --    WHERE ril.interface_line_context = 'ORDER ENTRY'
               --    AND TO_CHAR(OL.LINE_ID)          = RIL.INTERFACE_LINE_ATTRIBUTE6
               --      AND TO_CHAR (OH.ORDER_NUMBER)      = RIL.INTERFACE_LINE_ATTRIBUTE1
               --    ))
               OR (    oth.name IN ('WC SHORT TERM RENTAL',
                                    'WC LONG TERM RENTAL')
                   AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
                   AND ol.flow_status_code IN ('INVOICE_HOLD')
                   AND TRUNC (SYSDATE) =
                          TRUNC (
                             xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)))
          -- AND TRUNC(SYSDATE)       = TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
          AND ol.source_type_code IN ('INTERNAL', 'EXTERNAL')
          AND NOT EXISTS
                 (SELECT 'Y'
                    FROM gl_code_combinations_kfv gcc
                   WHERE     1 = 1
                         AND gcc.code_combination_id =
                                msi.cost_of_sales_account
                         AND gcc.segment4 = '646080')
   UNION
   SELECT oh.order_number,
          oos.name order_source,                                      -- ?????
          hps.party_site_number job_number,                           -- ?????
          TRUNC (oh.ordered_date) ordered_date,
          oh.attribute7 created_by,
          otlh.name order_type,
          NVL (flv.meaning,
               INITCAP (REPLACE (oh.flow_status_code, '_', ' ')))
             order_status,
          hca.account_number customer_number,
          NVL (hca.account_name, hp.party_name) customer_name,
          (  NVL (rctl.unit_selling_price, 0)
           * DECODE (
                otlh.name,
                'RETURN', (  NVL (rctl.quantity_invoiced,
                                  rctl.quantity_credited)
                           * -1),
                NVL (rctl.quantity_invoiced, rctl.quantity_credited)))
             gross_amount,
          NULL discount_amt,                                 --NULL tax_value,
          (NVL (
              DECODE (otlh.name,
                      'RETURN', (rctl.taxable_amount * -1),
                      rctl.taxable_amount),
              0))
             tax_value,
          NULL freight_amt,
          NVL (
             xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost (
                msi.inventory_item_id,
                msi.organization_id),
             0)
             average_cost,
          0 profit,
          rep.name,
          rct.trx_number trx_number,
          rep.salesrep_number salesrep_number,
          mp.organization_code warehouse,
          rctl.extended_amount inv_amount,                       --null sales,
          --null sale_cost,
          --nvl(DECODE(ol.line_category_code, 'RETURN', (ol.ordered_quantity * -1), ol.ordered_quantity)* NVL(rctl.unit_selling_price, 0), 0) sales,
          NVL (
               NVL (rctl.quantity_invoiced, rctl.quantity_credited)
             * NVL (rctl.unit_selling_price, 0),
             0)
             sales,
          0 sale_cost,
          rt.name payment_term,
          rt.description payment_desc,
          msi.segment1 item,
          msi.description item_desc,                               --null qty,
          NVL (rctl.quantity_invoiced, rctl.quantity_credited) qty,
          rctl.uom_code uom,
          rctl.unit_selling_price unit_selling_price,
          NULL order_line,
          rct.trx_date invoice_date,
          NULL modifier_name,
          NULL line_flow_status_code,              -- 'Delivery Charges' TYPE,
          NULL cat,
          NULL special_cost,
          NULL vqn_modifier_name,                              ---Primary Keys
          oh.header_id,
          hp.party_id,
          mp.organization_id,
          NULL line_type
     --descr#flexfield#start
     --descr#flexfield#end
     --gl#accountff#start
     --gl#accountff#end
     FROM ra_customer_trx rct,
          ra_customer_trx_lines rctl,
          oe_order_headers oh,             --ORG_ORGANIZATION_DEFINITIONS OOD,
          oe_transaction_types_vl otlh,
          mtl_parameters mp,
          mtl_system_items_kfv msi,
          hz_cust_accounts hca,
          hz_parties hp,
          fnd_lookup_values_vl flv,
          ra_salesreps rep,
          ra_terms rt,
          hz_cust_site_uses_all hcsu                          -- ????? > Start
                                    ,
          hz_cust_acct_sites_all hcas,
          hz_party_sites hps,
          oe_order_sources oos                                  -- ????? < End
    WHERE     1 = 1
          AND hcsu.site_use_id = oh.invoice_to_org_id         -- ????? > Start
          AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
          AND hps.party_site_id = hcas.party_site_id
          AND oos.order_source_id = oh.order_source_id          -- ????? < End
          AND rctl.creation_date >=
                   TO_DATE (
                      TO_CHAR (
                         xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                            xxeis.eis_rs_utility.get_date_format
                         || ' HH24:MI:SS'),
                      xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 0.25
          AND rctl.creation_date <=
                   TO_DATE (
                      TO_CHAR (
                         xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                            xxeis.eis_rs_utility.get_date_format
                         || ' HH24:MI:SS'),
                      xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 1.25
          AND rctl.customer_trx_id = rct.customer_trx_id
          --AND RCT.INTERFACE_HEADER_ATTRIBUTE1  ='11435332'
          AND mp.organization_id = oh.ship_from_org_id
          AND rct.interface_header_context = 'ORDER ENTRY'
          --AND mp.organization_code = '017'
          AND TO_CHAR (oh.order_number) = rct.interface_header_attribute1
          AND rctl.interface_line_context = 'ORDER ENTRY'
          AND TO_CHAR (oh.order_number) = rctl.interface_line_attribute1
          AND otlh.name = rctl.interface_line_attribute2
          AND rctl.description = 'Delivery Charge'
          AND rctl.line_type = 'LINE'
          AND otlh.transaction_type_id = oh.order_type_id
          AND mp.organization_id = msi.organization_id
          AND rctl.inventory_item_id = msi.inventory_item_id
          AND rct.bill_to_customer_id = hca.cust_account_id
          AND hp.party_id = hca.party_id
          AND flv.lookup_type(+) = 'FLOW_STATUS'
          AND flv.lookup_code(+) = oh.flow_status_code
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          AND oh.payment_term_id = rt.term_id;
