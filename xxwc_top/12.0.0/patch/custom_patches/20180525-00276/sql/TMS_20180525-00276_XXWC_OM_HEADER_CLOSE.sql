/*************************************************************************
  $Header TMS_20180525-00276_XXWC_OM_HEADER_CLOSE.sql $
  Module Name: TMS_20180525-00276_XXWC_OM_HEADER_CLOSE.sql

  PURPOSE:   Created to process the order headers stucked in booked status
             and line  is in cancelled and closed status.

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        07/27/2018  Pattabhi Avula       TMS#20180525-00276
  **************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
 DBMS_OUTPUT.put_line ('TMS: Datafix script    , Before Update');
 -- Updating the headers table
 
UPDATE apps.oe_order_headers_all
   SET FLOW_STATUS_CODE='CLOSED'
      ,open_Flag='N'
 WHERE header_id =46927380;

DBMS_OUTPUT.put_line ('Records updated - ' || SQL%ROWCOUNT);
		   
COMMIT;

	  DBMS_OUTPUT.put_line ('TMS: 20180525-00276  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180525-00276 , Errors : ' || SQLERRM);
END;
/