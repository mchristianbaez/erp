
/*************************************************************************
  $Header TMS_20180329-00007_PICKING_QC_CLEAN.sql $
  Module Name: TMS_20180329-00007  Non pilot branch records cleaning

  PURPOSE: Data fix for Non pilot branch records cleaning

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        30-MAR-2018  Pattabhi Avula        TMS#20180329-00007 

**************************************************************************/ 

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
BEGIN

DBMS_OUTPUT.put_line ('TMS: 20180329-00007    , Before Delete');

DELETE FROM XXWC.XXWC_OM_PQC_PICKING_TBL WHERE SHIP_FROM_ORG_CODE<>'020';

DBMS_OUTPUT.put_line ('TMS: 20180329-00007    , Deleted XXWC_OM_PQC_PICKING_TBL records : '||SQL%ROWCOUNT);

DELETE FROM XXWC.XXWC_OM_PQC_LOAD_CHECK_TBL  WHERE SHIP_FROM_ORG_CODE<>'020';

DBMS_OUTPUT.put_line ('TMS: 20180329-00007    , Deleted XXWC_OM_PQC_LOAD_CHECK_TBL records : '||SQL%ROWCOUNT);

COMMIT;
EXCEPTION
WHEN OTHERS THEN
   DBMS_OUTPUT.put_line ('TMS:20180329-00007   , When others Error '||SQLERRM);   
END; 
/


