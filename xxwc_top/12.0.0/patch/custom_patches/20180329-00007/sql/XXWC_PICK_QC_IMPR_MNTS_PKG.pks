CREATE OR REPLACE 
PACKAGE      XXWC_PICK_QC_IMPR_MNTS_PKG
AS
   /*************************************************************************
     $Header xxwc_pick_qc_impr_mnts_pkg$
     Module Name: xxwc_pick_qc_impr_mnts_pkg.pkb

     PURPOSE:   This package will call in Apex application to submit the PICk
                slip.

     REVISIONS:
     Ver     Date        Author            Description
     ------  ----------  ---------------   -------------------------
     1.0     1/3/2018    Pattabhi Avula    TMS#20171011-00108 - Initial Version	 
     *************************************************************************/

   PROCEDURE submit_pick_ticket (p_ntid           IN     VARCHAR2,
                                 p_order_number   IN     NUMBER,
                                 p_printer        IN     VARCHAR2,
                                 p_copies         IN     NUMBER,
                                 retcode             OUT VARCHAR2,
                                 errbuf              OUT VARCHAR2);

   PROCEDURE submit_badge_label (p_ntid          IN     VARCHAR2,
                                 p_action_type   IN     VARCHAR2,
                                 p_printer       IN     VARCHAR2,
                                 p_copies        IN     NUMBER,
                                 retcode            OUT VARCHAR2,
                                 errbuf             OUT VARCHAR2);

   PROCEDURE submit_load_check_label (p_request_date    IN     DATE,
                                      p_prepared_by     IN     VARCHAR2,
                                      p_checked_by      IN     VARCHAR2,
                                      p_customer_name   IN     VARCHAR2,
                                      p_customer_city   IN     VARCHAR2,
                                      p_order_number    IN     NUMBER,
                                      p_pallets         IN     NUMBER,
                                      p_bundles         IN     NUMBER,
                                      p_tools           IN     NUMBER,
                                      p_units           IN     NUMBER,
                                      p_num_of_labels   IN     NUMBER,
                                      p_status_flag     IN     VARCHAR2,
                                      p_print_date      IN     DATE,
                                      p_printer         IN     VARCHAR2,
                                      retcode              OUT VARCHAR2,
                                      errbuf               OUT VARCHAR2);

   PROCEDURE save_updated_pick_ticket (l_number_of_lines   IN     NUMBER, --Example: l_number_of_lines
                                       l_ordernum          IN     NUMBER, --Example: l_ordernum
                                       l_version           IN     NUMBER,
                                       l_ntid              IN     VARCHAR2, --Example: l_ntid
                                       l_action_type       IN     VARCHAR2, --Example: l_action_type
                                       l_action            IN     VARCHAR2,
                                       l_creation_date     IN     VARCHAR2,
                                       l_pallets           IN     NUMBER,
                                       l_bundles           IN     NUMBER,
                                       l_tools             IN     NUMBER,
                                       l_units             IN     NUMBER,
                                       l_line_errors       IN     NUMBER,
                                       l_notes             IN     VARCHAR2,									  
                                       retcode                OUT VARCHAR2,
                                       errbuf                 OUT VARCHAR2);
END XXWC_PICK_QC_IMPR_MNTS_PKG;
/