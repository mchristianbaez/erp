/*
TMS#20170828-00236
Created by : Vamshi.
*/
declare
   CURSOR CUR_ITEM
   IS
      SELECT '33695000' customerAcct#,
             inventory_item_id ProductID,
             item_number ProductSKU
        FROM APPS.XXWC_QP_ECOMMERCE_ITEMS_MV
       WHERE web_flag = 'Y';

   p_cust_account_id          NUMBER := 57460;
   p_cust_acct_use_id         NUMBER := 1852803;
   p_organization_id          NUMBER := 222;
   p_line_tbl                 qp_preq_grp.line_tbl_type;
   p_qual_tbl                 qp_preq_grp.qual_tbl_type;
   p_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
   p_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
   p_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
   p_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
   p_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
   p_control_rec              qp_preq_grp.control_record_type;
   x_line_tbl                 qp_preq_grp.line_tbl_type;
   x_line_qual                qp_preq_grp.qual_tbl_type;
   x_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
   x_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
   x_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
   x_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
   x_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
   x_return_status            VARCHAR2 (240);
   x_return_status_text       VARCHAR2 (240);
   qual_rec                   qp_preq_grp.qual_rec_type;
   line_attr_rec              qp_preq_grp.line_attr_rec_type;
   line_rec                   qp_preq_grp.line_rec_type;
   detail_rec                 qp_preq_grp.line_detail_rec_type;
   ldet_rec                   qp_preq_grp.line_detail_rec_type;
   rltd_rec                   qp_preq_grp.related_lines_rec_type;
   l_pricing_contexts_tbl     qp_attr_mapping_pub.contexts_result_tbl_type;
   l_qualifier_contexts_tbl   qp_attr_mapping_pub.contexts_result_tbl_type;
   v_line_tbl_cnt             INTEGER;

   i                          BINARY_INTEGER;
   j                          BINARY_INTEGER;
   k                          BINARY_INTEGER;

   l_version                  VARCHAR2 (240);
   l_file_val                 VARCHAR2 (60);
   l_modifier_name            VARCHAR2 (240);
   l_list_header_id           NUMBER;
   l_list_line_id             NUMBER;
   l_incomp_code              VARCHAR2 (30);
   l_modifier_type            VARCHAR2 (30);
   l_line_modifier_type       VARCHAR2 (30);
   l_item_category            NUMBER;
   l_gm_selling_price         NUMBER;
   l_message_level            NUMBER;
   l_currency_code            VARCHAR2 (240);
   l_org_id                   NUMBER := fnd_profile.VALUE ('ORG_ID');
   l_list_line_type_code      VARCHAR2 (30);              -- Shankar 27-Mar-15

   CURSOR cur
   IS
        SELECT inventory_item_id, NVL (primary_uom_code, 'EA') primary_uom_code
          FROM xxwc.xxwc_md_search_product_gtt_tbl
      ORDER BY inventory_item_id;

   l_rec_cntr                 NUMBER;
   l_line_attr_tbl_cntr       NUMBER;
   l_qual_tbl_cntr            NUMBER;
   l_inventory_item_id        NUMBER;
   lvc_table_create           VARCHAR2 (1000);
   ln_num_rows                NUMBER := 0;
BEGIN
   MO_GLOBAL.SET_POLICY_CONTEXT ('S', 162);

   APPS.Fnd_global.APPS_INITIALIZE (user_id        => 15985,
                                    resp_id        => 51229,
                                    resp_appl_id   => 431);


   DELETE FROM XXWC_PA_WAREHOUSE_GT;

   BEGIN
      INSERT INTO xxwc.xxwc_pa_warehouse_gt (organization_id)
           VALUES (711);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;

   -------------------------------------------------------------
   -- Delete non-InvOrg items
   -------------------------------------------------------------
   DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl;

   INSERT INTO xxwc.xxwc_md_search_product_gtt_tbl (inventory_item_id,
                                                    primary_uom_code)
      SELECT A.inventory_item_id, B.PRIMARY_UOM_CODE
        FROM APPS.XXWC_QP_ECOMMERCE_ITEMS_MV A, APPS.MTL_SYSTEM_ITEMS_B B
       WHERE     A.web_flag = 'Y'
             AND A.INVENTORY_ITEM_ID = B.INVENTORY_ITEM_ID
             AND B.ORGANIZATION_ID = 222;
            -- and b.inventory_item_id in (2966628,2964722);


   ln_num_rows := SQL%ROWCOUNT;

   UPDATE xxwc.xxwc_md_search_product_gtt_tbl stg
      SET stg.primary_uom_code =
             (SELECT msib.primary_uom_code
                FROM INV.MTL_SYSTEM_ITEMS_B msib
               WHERE     msib.inventory_item_id = stg.inventory_item_id
                     AND msib.organization_id = p_organization_id),
          QUANTITYONHAND =
             xxwc_ascp_scwb_pkg.get_on_hand (stg.inventory_item_id,
                                             p_organization_id,
                                             'G'),
          OPEN_SALES_ORDERS =
             NVL (
                xxwc_ascp_scwb_pkg.get_open_orders_qty (
                   stg.inventory_item_id,
                   p_organization_id),
                0),
          LIST_PRICE =
             xxwc_inv_ais_pkg.shipto_last_price_paid (p_cust_acct_use_id,
                                                      stg.inventory_item_id), --TMS 20161128-00028
          LIST_PRICE_2 =
             xxwc_qp_market_price_util_pkg.get_market_list_price (
                stg.inventory_item_id,
                p_organization_id);


   dbms_output.put_line('step1');
  
   -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
   SELECT gsob.currency_code
     INTO l_currency_code
     FROM hr_operating_units hou, gl_sets_of_books gsob
    WHERE     hou.organization_id = l_org_id
          AND gsob.set_of_books_id = hou.set_of_books_id;


   SELECT gsob.currency_code
     INTO l_currency_code
     FROM hr_operating_units hou, gl_sets_of_books gsob
    WHERE     hou.organization_id = l_org_id
          AND gsob.set_of_books_id = hou.set_of_books_id;

   dbms_output.put_line('step2');

   l_rec_cntr := 0;
   l_line_attr_tbl_cntr := 0;
   l_qual_tbl_cntr := 0;

   FOR rec IN cur
   LOOP
      l_rec_cntr := l_rec_cntr + 1;
      l_line_attr_tbl_cntr := l_line_attr_tbl_cntr + 1;
      l_qual_tbl_cntr := l_qual_tbl_cntr + 1;

      l_gm_selling_price := NULL;
      l_item_category := NULL;

      BEGIN
         SELECT mic.category_id
           INTO l_item_category
           FROM mtl_item_categories_v mic
          WHERE     mic.category_set_name = 'Inventory Category'
                AND mic.inventory_item_id = rec.inventory_item_id
                AND mic.organization_id = p_organization_id
                AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_item_category := NULL;
      END;

   dbms_output.put_line('step3');

      qp_attr_mapping_pub.build_contexts (
         p_request_type_code           => 'ONT',
         p_pricing_type                => 'L',
         x_price_contexts_result_tbl   => l_pricing_contexts_tbl,
         x_qual_contexts_result_tbl    => l_qualifier_contexts_tbl);

      ---- Control Record
      p_control_rec.pricing_event := 'LINE';                       -- 'BATCH';
      p_control_rec.calculate_flag := 'Y'; --QP_PREQ_GRP.G_SEARCH_N_CALCULATE;
      p_control_rec.simulation_flag := 'Y';
      p_control_rec.rounding_flag := 'Q';
      p_control_rec.manual_discount_flag := 'Y';
      p_control_rec.request_type_code := 'ONT';
      p_control_rec.temp_table_insert_flag := 'Y';

      ---- Line Records ---------
      line_rec.request_type_code := 'ONT';
      line_rec.line_id := -1; -- Order Line Id. This can be any thing for this script
      line_rec.line_index := l_rec_cntr;                 -- Request Line Index
      line_rec.line_type_code := 'LINE';        -- LINE or ORDER(Summary Line)
      line_rec.pricing_effective_date := SYSDATE; -- Pricing as of what date ?
      line_rec.active_date_first := SYSDATE; -- Can be Ordered Date or Ship Date
      line_rec.active_date_second := SYSDATE; -- Can be Ordered Date or Ship Date
      line_rec.active_date_first_type := 'NO TYPE';                -- ORD/SHIP
      line_rec.active_date_second_type := 'NO TYPE';               -- ORD/SHIP
      line_rec.line_quantity := 1;                         -- Ordered Quantity

      line_rec.line_uom_code := rec.primary_uom_code;

      line_rec.currency_code := l_currency_code;

      line_rec.price_flag := 'Y'; -- Price Flag can have 'Y' , 'N'(No pricing) , 'P'(Phase)
      p_line_tbl (l_rec_cntr) := line_rec;

      ---- Line Attribute Record
      line_attr_rec.line_index := l_rec_cntr;
      line_attr_rec.pricing_context := 'ITEM';
      line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE1';
      line_attr_rec.pricing_attr_value_from := TO_CHAR (rec.inventory_item_id); -- INVENTORY ITEM ID
      line_attr_rec.validated_flag := 'N';
      p_line_attr_tbl (l_line_attr_tbl_cntr) := line_attr_rec;

      IF l_item_category IS NOT NULL
      THEN
         l_line_attr_tbl_cntr := l_line_attr_tbl_cntr + 1;
         line_attr_rec.line_index := l_rec_cntr;
         line_attr_rec.pricing_context := 'ITEM';                           --
         line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE2';
         line_attr_rec.pricing_attr_value_from := l_item_category; -- Category ID
         line_attr_rec.validated_flag := 'N';
         p_line_attr_tbl (l_line_attr_tbl_cntr) := line_attr_rec;
      END IF;

      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'ORDER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE18';
      qual_rec.qualifier_attr_value_from := p_organization_id; -- SHIP_FROM_ORG_ID;
      qual_rec.comparison_operator_code := '=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;

      IF p_cust_account_id IS NOT NULL
      THEN
         l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
         qual_rec.line_index := l_rec_cntr;
         qual_rec.qualifier_context := 'CUSTOMER';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE32';
         qual_rec.qualifier_attr_value_from := p_cust_account_id; -- CUSTOMER ID;
         qual_rec.comparison_operator_code := '=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
      END IF;

      IF p_cust_acct_use_id IS NOT NULL
      THEN
         l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
         qual_rec.line_index := l_rec_cntr;
         qual_rec.qualifier_context := 'CUSTOMER';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE11';
         qual_rec.qualifier_attr_value_from := p_cust_acct_use_id; -- Ship To Site Use ID;
         qual_rec.comparison_operator_code := '=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
      END IF;

      l_qual_tbl_cntr := l_qual_tbl_cntr + 1;

      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'ITEM_NUMBER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE31';
      qual_rec.qualifier_attr_value_from := TO_CHAR (rec.inventory_item_id); -- ItemId
      qual_rec.comparison_operator_code := 'NOT=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;

      IF l_item_category IS NOT NULL
      THEN
         l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
         qual_rec.line_index := l_rec_cntr;
         qual_rec.qualifier_context := 'ITEM_CAT';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE31';
         qual_rec.qualifier_attr_value_from := TO_CHAR (l_item_category); -- CatergoryId
         qual_rec.comparison_operator_code := 'NOT=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
      END IF;

      UPDATE xxwc.xxwc_md_search_product_gtt_tbl
         SET sequence = l_rec_cntr
       WHERE inventory_item_id = rec.inventory_item_id;
   END LOOP;

   qp_preq_pub.price_request (p_line_tbl,
                              p_qual_tbl,
                              p_line_attr_tbl,
                              p_line_detail_tbl,
                              p_line_detail_qual_tbl,
                              p_line_detail_attr_tbl,
                              p_related_lines_tbl,
                              p_control_rec,
                              x_line_tbl,
                              x_line_qual,
                              x_line_attr_tbl,
                              x_line_detail_tbl,
                              x_line_detail_qual_tbl,
                              x_line_detail_attr_tbl,
                              x_related_lines_tbl,
                              x_return_status,
                              x_return_status_text);
                              
   dbms_output.put_line('step4');                              

   i := x_line_detail_tbl.FIRST;

   IF i IS NOT NULL
   THEN
      LOOP
         IF x_line_detail_tbl (i).automatic_flag = 'Y'
         THEN
            l_modifier_name := NULL;
            l_list_header_id := NULL;
            l_list_line_id := NULL;
            l_incomp_code := NULL;
            l_modifier_type := NULL;
            l_list_line_type_code := NULL;
            l_line_modifier_type := NULL;

            BEGIN
               SELECT name, attribute10
                 INTO l_modifier_name, l_modifier_type
                 FROM qp_list_headers_vl
                WHERE list_header_id = x_line_detail_tbl (i).list_header_id;

               l_list_header_id := x_line_detail_tbl (i).list_header_id;
               l_list_line_id := x_line_detail_tbl (i).list_line_id;

               IF l_list_line_id IS NOT NULL
               THEN
                  SELECT incompatibility_grp_code,
                         list_line_type_code,
                         attribute5
                    INTO l_incomp_code,
                         l_list_line_type_code,
                         l_line_modifier_type
                    FROM qp_list_lines
                   WHERE list_line_id = l_list_line_id;

                  IF l_modifier_type = 'Contract Pricing'
                  THEN
                     l_modifier_type :=
                        NVL (l_line_modifier_type, l_modifier_type);
                  END IF;

                  IF l_modifier_type IS NOT NULL
                  THEN
                     BEGIN
                        SELECT description
                          INTO l_modifier_Type
                          FROM fnd_flex_values_vl
                         WHERE     flex_value_set_id = 1015252
                               AND flex_value = l_modifier_Type;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           NULL;
                     END;
                  END IF;

                  IF l_list_line_type_code = 'PLL'
                  THEN
                     l_incomp_code := 'PLL';
                  END IF;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_modifier_name := NULL;
                  l_list_header_id := NULL;
                  l_list_line_id := NULL;
                  l_incomp_code := NULL;
                  l_modifier_type := NULL;
                  l_list_line_type_code := NULL;
                  l_line_modifier_type := NULL;
            END;

            -- Getting new selling price

            l_gm_selling_price := NULL;
            j := x_line_tbl.FIRST;

            IF j IS NOT NULL
            THEN
               LOOP
                  IF x_line_tbl (j).LINE_INDEX =
                        x_line_detail_tbl (i).LINE_INDEX
                  THEN
                     l_gm_selling_price := x_line_tbl (j).adjusted_unit_price;
                  END IF;

                  EXIT WHEN l_gm_selling_price IS NOT NULL;
                  j := x_line_tbl.NEXT (j);
               END LOOP;
            END IF;

            IF l_gm_selling_price = 0
            THEN
               l_gm_selling_price := NULL;
            END IF;

            UPDATE xxwc.xxwc_md_search_product_gtt_tbl
               SET selling_price = l_gm_selling_price,
                   modifier = l_modifier_name,
                   modifier_type = l_modifier_type
             WHERE sequence = x_line_detail_tbl (i).line_index;
         END IF;              -- IF x_line_detail_tbl (i).automatic_flag = 'Y'

         EXIT WHEN i = x_line_detail_tbl.LAST;
         i := x_line_detail_tbl.NEXT (i);
      END LOOP;
   END IF;



   BEGIN
      lvc_table_create :=
            'CREATE TABLE XXWC.XXWC_TEST_ECOMM_PRICE_TEMP '
         || ' AS SELECT GTT.INVENTORY_ITEM_ID,GTT.ORGANIZATION_ID, GTT.SELLING_PRICE,GTT.modifier FROM xxwc.xxwc_md_search_product_gtt_tbl GTT';

      EXECUTE IMMEDIATE lvc_table_create;

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
            'XXWC.XXWC_ORG_ITEM_PRICE_'
         || P_ORGANIZATION_ID
         || '##'
         || ' Table successfully created');
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               'XXWC.XXWC_ORG_ITEM_PRICE_'
            || P_ORGANIZATION_ID
            || '##'
            || '  - table creation failed');
   END;
END;
/