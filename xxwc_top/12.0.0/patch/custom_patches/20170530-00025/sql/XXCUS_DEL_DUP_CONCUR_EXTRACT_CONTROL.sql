/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170530-00025     05/30/2017   Balaguru Seshadri Delete duplicate rows
*/
set serveroutput on size 100000;
declare
begin
	delete from xxcus.xxcus_concur_extract_controls a
	where  rowid > (
	select min(rowid)
	from   xxcus.xxcus_concur_extract_controls b
	where b.employee_group =a.employee_group 
	and b.type =a.type
	);
 --	
 dbms_output.put_line('Total rows deleted :'||sql%rowcount);
 --
 commit;
 --
 dbms_output.put_line('Commit Complete...');
 --
exception
 when others then
  rollback;       
end;
/