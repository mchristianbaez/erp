 /*
   Ticket#                            Date         Author            Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS:20170530-00025 / ESMS 561767   05/28/2017   Balaguru Seshadri Concur accrual process related routines  
 */
CREATE TABLE XXCUS.XXCUS_CONCUR_ACCRUALS_ELIG_JE
(
  RPT_ID                VARCHAR2(32 BYTE),
  EMP_ID                VARCHAR2(48 BYTE),
  EMP_FULL_NAME         VARCHAR2(32 BYTE),
  ACCRUAL_ERP           VARCHAR2(48 BYTE),
  ACCRUAL_COMPANY       VARCHAR2(48 BYTE),
  ACCRUAL_BRANCH        VARCHAR2(48 BYTE),
  ACCRUAL_DEPT          VARCHAR2(48 BYTE),
  ACCRUAL_ACCOUNT       VARCHAR2(48 BYTE),
  ACCRUAL_PROJECT       VARCHAR2(48 BYTE),
  RUN_ID                NUMBER,
  ACCRUAL_PERIOD        VARCHAR2(10 BYTE),
  REVERSAL_PERIOD       VARCHAR2(10 BYTE),
  ACCRUAL_FISCAL_START  DATE,
  ACCRUAL_FISCAL_END    DATE,
  ACCRUAL_STATUS        VARCHAR2(30 BYTE),
  CHAR_150_1            VARCHAR2(150 BYTE),
  CHAR_150_2            VARCHAR2(150 BYTE),
  CHAR_150_3            VARCHAR2(150 BYTE),
  CHAR_150_4            VARCHAR2(150 BYTE),
  CHAR_150_5            VARCHAR2(150 BYTE),
  CHAR_150_6            VARCHAR2(150 BYTE),
  CHAR_150_7            VARCHAR2(150 BYTE),
  CHAR_150_8            VARCHAR2(150 BYTE),
  CHAR_150_9            VARCHAR2(150 BYTE),
  CHAR_150_10           VARCHAR2(150 BYTE),
  CHAR_150_11           VARCHAR2(150 BYTE),
  CHAR_150_12           VARCHAR2(150 BYTE),
  CHAR_150_13           VARCHAR2(150 BYTE),
  CHAR_150_14           VARCHAR2(150 BYTE),
  CHAR_150_15           VARCHAR2(150 BYTE),
  CHAR_150_16           VARCHAR2(150 BYTE),
  CHAR_150_17           VARCHAR2(150 BYTE),
  CHAR_150_18           VARCHAR2(150 BYTE),
  CHAR_150_19           VARCHAR2(150 BYTE),
  CHAR_150_20           VARCHAR2(150 BYTE),
  NUM_1                 NUMBER,
  NUM_2                 NUMBER,
  NUM_3                 NUMBER,
  NUM_4                 NUMBER,
  NUM_5                 NUMBER,
  DATE_1                DATE,
  DATE_2                DATE,
  DATE_3                DATE,
  DATE_4                DATE,
  DATE_5                DATE,
  OOP_CARD              VARCHAR2(30 BYTE),
  EXPENSE_TYPE          VARCHAR2(64 BYTE),
  EXPENSE_CODE          VARCHAR2(64 BYTE)
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_ACCRUALS_ELIG_JE IS 'TMS:20170524-00141 / ESMS 561767';
--