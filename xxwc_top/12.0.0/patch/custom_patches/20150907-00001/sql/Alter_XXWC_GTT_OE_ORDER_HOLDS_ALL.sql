/*************************************************************************
$Header Alter_XXWC_GTT_OE_ORDER_HOLDS_ALL.sql $
Module Name: XXWC_GTT_OE_ORDER_HOLDS_ALL

PURPOSE:   This script is used to:
           1. Add column - CREDIT_PROFILE_LEVEL to XXEIS.XXWC_GTT_OE_ORDER_HOLDS_ALL table.
           2. Re-compile the package XXEIS.EIS_RS_XXWC_COM_UTIL_PKG

REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        09/07/2015  Gopi Damuluri           Initial Version
                                               TMS#  20150907-00001
****************************************************************************/
ALTER TABLE XXEIS.XXWC_GTT_OE_ORDER_HOLDS_ALL ADD CREDIT_PROFILE_LEVEL VARCHAR2(30);
/

ALTER PACKAGE XXEIS.EIS_RS_XXWC_COM_UTIL_PKG COMPILE;
/