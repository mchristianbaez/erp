/*************************************************************************
  $Header TMS_20160520-00024_Data_Fix_discarded_POs_still_getting_paid.sql $
  Module Name: TMS_20160520-00024  Data Fix script 

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        28-NOV-2016  Pattabhi Avula        TMS#20160520-00024 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
l_sql1   VARCHAR2 (32000);
l_sql2   VARCHAR2 (32000);
l_sql3   VARCHAR2 (32000);
l_cnt   NUMBER;
l_cnt1  NUMBER;
l_tab_cnt  NUMBER:=0;
BEGIN
  BEGIN	 
	  SELECT  COUNT(1) 
      INTO    l_tab_cnt
	  FROM    all_tables  
	  WHERE   table_name='XXWC_SR_3_13412654701_AILA'
	  AND     owner='XXWC';
	IF l_tab_cnt > 0 THEN  
	  
      EXECUTE IMMEDIATE 'DROP TABLE XXWC.XXWC_SR_3_13412654701_AILA';
      DBMS_OUTPUT.put_line ('TMS: 20160520-00024    , Table Dropped');
    ELSE
      BEGIN
         DBMS_OUTPUT.put_line ('TMS: 20160520-00024    , Before Create table');
        l_sql1 :=
            'CREATE TABLE XXWC.XXWC_SR_3_13412654701_AILA AS '
         || 'SELECT INV.invoice_id, LINE.line_number, LINE.amount, LINE.discarded_flag, '
         || ' LINE.cancelled_flag,inv.invoice_num FROM ap_invoices_all INV, ap_invoice_lines_all LINE '
         || ' WHERE     INV.invoice_id = LINE.invoice_id AND LINE.amount = 0 AND inv.org_id = 162 '
         || ' AND NVL (LINE.discarded_flag,'||'''N'''||') <>'|| '''Y'''  
         || 'AND NVL (LINE.cancelled_flag, '||'''N'''||') <>'|| '''Y'''  
         || ' AND NOT EXISTS (SELECT '||'''Line has at least one distribution'''
         ||'FROM ap_invoice_distributions_all D5 WHERE     D5.invoice_id = LINE.invoice_id  AND D5.invoice_line_number = LINE.line_number)';
      
         EXECUTE IMMEDIATE l_sql1;
	     COMMIT;
      END;
      
	DBMS_OUTPUT.put_line ('TMS: 20160520-00024    , After Created table');
	BEGIN	 
	  SELECT  COUNT(1) 
      INTO    l_cnt
	  FROM    all_tables  
	  WHERE   table_name='XXWC_SR_3_13412654701_AILA'
	  AND     owner='XXWC';
   
      IF l_cnt > 0 THEN
    
        DBMS_OUTPUT.put_line ('TMS: 20160520-00024    , Table exists');
   
        l_sql2:=   'SELECT COUNT(1) 
	                FROM XXWC.XXWC_SR_3_13412654701_AILA';
			EXECUTE IMMEDIATE l_sql2 INTO l_cnt1;	
			
				  DBMS_OUTPUT.put_line (
                   'TMS: 20160520-00024  Invoice lines Count :'||l_cnt1);

		  IF l_cnt1>0 THEN
   
            l_sql3:='UPDATE ap_invoice_lines_all LINES
                     SET LINES.discarded_flag = ''Y''
                     WHERE (LINES.invoice_id, LINES.line_number) IN (SELECT  TEMP.invoice_id,
                                                                             TEMP.line_number
                                                                      FROM   XXWC.XXWC_SR_3_13412654701_AILA TEMP)';
	                EXECUTE IMMEDIATE l_sql3;
	                COMMIT;
	      END IF;
	 
	  END IF;
        DBMS_OUTPUT.put_line (
           'TMS: 20160520-00024  After Invoice lines updated Count : '
         || l_cnt1);
	END;
	
	END IF;
	END;

EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160520-00024 , Errors : ' || SQLERRM);
END;
/