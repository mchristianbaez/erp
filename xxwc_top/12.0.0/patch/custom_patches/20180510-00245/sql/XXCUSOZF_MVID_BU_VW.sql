  -- ------------------------------------------------------------------------------
  -- Copyright 2010 HD Supply  Inc (Orlando, FL) - All rights reserved
  --
  --
  --    NAME:       XXCUS_TM_MISC_PKG.pck
  --
  --    REVISIONS:
  --    Ver        Date        Author                 Description
  --    ---------  ----------  ---------------        -------------------------------
  --    1.0        04/12/2014  Balguru Sheshadri      Initial Build           
  --    1.1        10/29/2018  Ashwin Sridhar         TMS#20180510-00245 Collection Report update collector by BU 
  --================================================================================
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_MVID_BU_VW" ("MVID", "MASTER_VENDOR", "BU_ID", "BU_NAME", "BU_NUMBER", "LOB_NAME", "LOB_NUMBER", "US_BU_ID", "US_BU_NAME") AS 
  SELECT ca.account_number MVID,
            hp.party_name MASTER_VENDOR,
            XXCUSOZF_UTIL_PKG.XXCUSOZF_GET_MAX_LOB (ca.cust_account_id) BU_ID,
            hp1.party_name BU_NAME,
            hp1.party_number BU_NUMBER,
            HP2.PARTY_NAME LOB_NAME,
            HP2.PARTY_NUMBER LOB_NUMBER,
            XXCUSOZF_UTIL_PKG.XXCUSOZF_GET_MAX_US_LOB (CA.CUST_ACCOUNT_ID) US_BU_ID,
            hp3.party_name US_BU_NAME
       FROM hz_cust_accounts ca,
            hz_parties hp,
            hz_parties hp1,
            HZ_PARTIES HP2,
            hz_parties hp3,
            hz_relationships rel
      WHERE 1 =1
        AND ca.party_id = hp.party_id
        AND ca.attribute1 = 'HDS_MVID'
      --  AND ca.status ='A' --Only Active Customers v1.1 Commented by Ashwin.S on 29-Oct-2018 for TMS#20180510-00245
        AND XXCUSOZF_UTIL_PKG.XXCUSOZF_GET_MAX_LOB (CA.CUST_ACCOUNT_ID) =HP1.PARTY_ID(+)
        AND XXCUSOZF_UTIL_PKG.XXCUSOZF_GET_MAX_US_LOB (ca.cust_account_id) =hp3.party_id(+)
        AND HP1.ATTRIBUTE1(+) = 'HDS_BU'
        AND hp3.attribute1(+) = 'HDS_BU'
        AND hp1.party_id = rel.subject_id(+)
        AND hp2.party_id(+) = rel.object_id
        AND hp2.attribute1(+) = 'HDS_LOB'
        AND rel.subject_table_name(+) = 'HZ_PARTIES'
        AND rel.object_table_name(+) = 'HZ_PARTIES'
        AND rel.relationship_code(+) = 'DIVISION_OF'
        AND REL.DIRECTIONAL_FLAG(+) IN ('F', 'B')
ORDER BY 2;
