/*************************************************************************
    $Header XXWC_B2B_CONFIG_TBL_Update_Print_SOA_Flag.sql $
     Module Name: XXWC_B2B_CONFIG_TBL_Update_Print_SOA_Flag
   
     PURPOSE:   This script is used to update XXWC_B2B_CONFIG_TBL table with SOA_PRINT_PRICE = Y for current customer
     REVISIONS:
     Ver        Date        Author                             Description
     ---------  ----------  -------------------------------    -------------------------
     1.0        12/14/2016  Rakesh Patel                       TMS#20161213-00178 - B2B Update SAO Print Price for Customer to Y
**************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
    mo_global.set_policy_context('S',162);
   	DBMS_OUTPUT.put_line ('TMS# 20161213-00178  , Script 1 -Before Update-sql 1');

   UPDATE XXWC.XXWC_B2B_CONFIG_TBL B2B_CONFIG
      SET SOA_PRINT_PRICE = 'Y'
    WHERE EXISTS	  (SELECT '1'
                                 FROM apps.hz_cust_acct_sites hcas,
                                      apps.hz_party_sites hps,
                                      apps.hz_cust_site_uses hcsu
                                WHERE cust_account_id        = B2B_CONFIG.cust_account_id
                                  AND hcsu.site_use_id       = B2B_CONFIG.site_use_id
								  AND hcas.status            = 'A'
                                  AND hps.STATUS             = 'A'
                                  AND hcas.PARTY_SITE_ID     = hps.PARTY_SITE_ID
                                  AND hcsu.CUST_ACCT_SITE_ID = hcas.CUST_ACCT_SITE_ID
                                  AND hcsu.status            = 'A'
                                  AND hcsu.SITE_USE_CODE     = 'SHIP_TO'
                                  AND hcsu.status            = 'A'
                                  AND PRIMARY_FLAG           = 'Y'
								  AND hcas.attribute1 = '0'
                            );
    
	DBMS_OUTPUT.put_line ('After Update SOA_PRINT_PRICE to Y , no of records updated in XXWC_B2B_CONFIG_TBL table sql 1: '||sql%rowcount);							
							
	DBMS_OUTPUT.put_line ('TMS# 20161213-00178  , Script 1 -Before Update-sql 2');
	
	UPDATE XXWC.XXWC_B2B_CONFIG_TBL B2B_CONFIG
      SET SOA_PRINT_PRICE = 'Y'
    WHERE EXISTS	  (SELECT '1'
                                 FROM apps.hz_cust_acct_sites hcas,
                                      apps.hz_party_sites hps,
                                      apps.hz_cust_site_uses hcsu
                                WHERE cust_account_id        = B2B_CONFIG.cust_account_id
                                  AND B2B_CONFIG.site_use_id IS NULL
								  AND hcas.status            = 'A'
                                  AND hps.STATUS             = 'A'
                                  AND hcas.PARTY_SITE_ID     = hps.PARTY_SITE_ID
                                  AND hcsu.CUST_ACCT_SITE_ID = hcas.CUST_ACCT_SITE_ID
                                  AND hcsu.status            = 'A'
                                  AND hcsu.SITE_USE_CODE     = 'SHIP_TO'
                                  AND hcsu.status            = 'A'
                                  AND PRIMARY_FLAG           = 'Y'
								  AND hcas.attribute1 = '0'
                            )
	  AND B2B_CONFIG.site_use_id IS NULL;

	DBMS_OUTPUT.put_line ('After Update SOA_PRINT_PRICE to Y , no of records updated in XXWC_B2B_CONFIG_TBL table sql 2: '||sql%rowcount);

    COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS# 20161213-00178, Errors ='||SQLERRM);
END;
/