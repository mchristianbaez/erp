 /**********************************************************************************
  -- Synonym Script: XXEIS_EIS_RS_XXWC_EOM_COMM_REP_PKG_SYN.sql
  -- *******************************************************************************
  --
  -- PURPOSE: Synonym for Package XXEIS.EIS_RS_XXWC_EOM_COMM_REP_PKG
  -- HISTORY
  -- ===============================================================================
  -- ===============================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -----------------------------------------
  -- 1.0     08-Nov-2016   P.Vamshidhar   Initial Version - TMS#20161102-00266 -  Automation of "White Cap EOM Commissions Report"
************************************************************************************/
CREATE OR REPLACE SYNONYM APPS.EIS_RS_XXWC_EOM_COMM_REP_PKG FOR XXEIS.EIS_RS_XXWC_EOM_COMM_REP_PKG;
