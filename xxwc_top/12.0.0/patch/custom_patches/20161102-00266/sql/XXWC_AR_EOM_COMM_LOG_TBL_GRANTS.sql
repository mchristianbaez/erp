--------------------------------------------------------------------------------------
/*******************************************************************************
  Grant : XXWC_AR_EOM_COMM_LOG_TBL_GRANTS.sql
  Description: To grant permissions on XXWC_AR_EOM_COMM_LOG_TBL table
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11/08/2016          P.vamshidhar   TMS#20161102-00266 -  Automation of "White Cap EOM Commissions Report"
********************************************************************************/
GRANT SELECT, UPDATE ON XXEIS.XXWC_AR_EOM_COMM_LOG_TBL TO INTERFACE_XXCUS;
/
--Run this file in XXEIS user