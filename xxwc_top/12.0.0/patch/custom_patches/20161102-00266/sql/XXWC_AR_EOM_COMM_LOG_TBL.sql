--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: XXWC_AR_EOM_COMM_LOG_TBL
  Description: To maintain process status of WC End of the Month Commissions Extract from UC4 
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11/08/2016          P.vamshidhar   TMS#20161102-00266 -  Automation of "White Cap EOM Commissions Report"
********************************************************************************/
 CREATE TABLE XXEIS.XXWC_AR_EOM_COMM_LOG_TBL
(
   PROCESS_ID       NUMBER,
   REQUEST_ID       NUMBER,
   CREATED_BY       VARCHAR2 (40),
   READY_TO_PROCESS VARCHAR2 (10),
   PROCESSED_DATE   DATE,
   PROCESSED_BY     VARCHAR2 (100)
)
/
