-------------------------------------------------------------------------------------------------------------------  
/* *************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_EOM_COMM_TBL $
  Module Name: Receivables
  PURPOSE: This Table is used in a view  XXEIS.EIS_XXWC_AR_EOM_COMM_V for White Cap End of Month Commissions Report
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        11/14/2016  P.Vamshidhar          20161024-00080  
************************************************************************************************************* */
ALTER TABLE XXEIS.EIS_XXWC_AR_EOM_COMM_TBL ADD
(EXT_AVG_COST NUMBER,
EXT_SPECIAL_COST  NUMBER,
GM_AVG_COST  NUMBER,
GMS_SPECIAL_COST  NUMBER,
GP_AVG_COST  NUMBER,
GPS_SPECIAL_COST  NUMBER)
/