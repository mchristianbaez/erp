  set serveroutput on;
 /*************************************************************************
      PURPOSE:  Rental Lines Wont Generate - Data Script Fix Needed? # 25608652 

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        14-Nov-2017  Sundaramoorthy     Initial Version - TMS #20171027-00287 
	************************************************************************/ 
 BEGIN
 dbms_output.put_line ('Start Update ');

 UPDATE oe_order_lines_all
  SET  flow_status_code ='CLOSED'
  , open_flag ='N'
  WHERE line_id = 110113943
  AND header_id =62845175;  
  COMMIT;
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/	