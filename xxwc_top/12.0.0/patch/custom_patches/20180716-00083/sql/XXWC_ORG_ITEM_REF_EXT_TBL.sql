 /********************************************************************************
  FILE NAME: XXWC.XXWC_ORG_ITEM_REF_EXT_TBL

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Nancy Pahwa  TMS#20180308-00301  -- Initial Version
  1.1     07/16/2018    Naveen K     TMS# 20180716-00083
  ********************************************************************************/
CREATE TABLE "XXWC"."XXWC_ORG_ITEM_REF_EXT_TBL" 
   (	"AHH_ATTRIBUTE" VARCHAR2(100), 
	"AHH_ATTR_VALUE" VARCHAR2(100), 
	"EBS_VALUE" VARCHAR2(100)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_ORG_ITEM_REF.bad'
    DISCARDFILE 'XXWC_ORG_ITEM_REF.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                                    )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_ORG_ITEM_REF.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;