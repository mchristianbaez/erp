 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_GL_CONV_REF_GRANTS_TO_APEX_USER.sql

  PROGRAM TYPE: Grant scripts

  PURPOSE: INV Conversion purpose in APEX application

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/09/2018    Naveen K  		TMS#20180713-00072 |20180716-00083 -- Initial Version
  ********************************************************************************/
  GRANT ALTER,SELECT,INSERT,UPDATE,DELETE ON "XXWC"."XXWC_AP_BRANCH_LIST_STG_TBL" TO EA_APEX
/
  GRANT SELECT ON "XXWC"."XXWC_AP_BRANCH_LIST_EXT_TBL" TO EA_APEX
/
  GRANT ALTER,SELECT,INSERT,UPDATE,DELETE ON "XXWC"."XXWC_INVITEM_XREF_REF" TO EA_APEX
/
  GRANT SELECT ON "XXWC"."XXWC_INVITEM_XREF_EXT_TBL" TO EA_APEX
/
  GRANT ALTER,SELECT,INSERT,UPDATE,DELETE ON "XXWC"."XXWC_INVITEM_XREF_STG" TO EA_APEX
/
  GRANT SELECT ON "XXWC"."XXWC_INVITEM_XREF_STG_EXT_TBL" TO EA_APEX
/
  GRANT ALTER,SELECT,INSERT,UPDATE,DELETE ON "XXWC"."XXWC_ITEM_MASTER_REF" TO EA_APEX
/
  GRANT SELECT ON "XXWC"."XXWC_ITEM_MASTER_REF_EXT_TBL" TO EA_APEX
/
  GRANT ALTER,SELECT,INSERT,UPDATE,DELETE ON "XXWC"."XXWC_ORG_ITEM_REF" TO EA_APEX
/
  GRANT SELECT ON "XXWC"."XXWC_ORG_ITEM_REF_EXT_TBL" TO EA_APEX
/
