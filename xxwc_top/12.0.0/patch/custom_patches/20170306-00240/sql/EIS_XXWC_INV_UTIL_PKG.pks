CREATE OR REPLACE 
PACKAGE  XXEIS.EIS_XXWC_INV_UTIL_PKG IS
 --//============================================================================
  --// Object Name          :: EIS_XXWC_INV_UTIL_PKG
  --//
  --// Object Type          :: Package Specification
  --//
  --// Object Description   :: This Package will trigger in view and populate the values into View.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016         Initial Build  -- Added for TMS#20160601-00025.
  --// 1.1        Siva        10/17/2016         TMS#20160930-00078 
  --// 1.2        Siva        10-Jan-2017        TMS#20161025-00027 
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --// 1.4		  Siva		  18-May-2017		 TMS#20170306-00240
  --//============================================================================
type T_VARCHAR_TYPE_VLDN_TBL is table of varchar2(200) index by varchar2(200);

G_ONHAND_QTY_VLDN_TBL   T_VARCHAR_TYPE_VLDN_TBL;

g_onhand_qty number;
--start for version 1.1
g_url varchar2(2000);
g_invoice_num varchar2(2000);
g_url_vldn_tbl   t_varchar_type_vldn_tbl;
G_invoice_num_VLDN_TBL   T_VARCHAR_TYPE_VLDN_TBL;
--End for version 1.1  

--start for version 1.2
g_vendor_num_vldn_tbl     t_varchar_type_vldn_tbl;
g_vendor_name_vldn_tbl    t_varchar_type_vldn_tbl;  
g_vendor_id_vldn_tbl      t_varchar_type_vldn_tbl; 
g_vendor_number varchar2(100);
g_vendor_name varchar2(240);
g_vendor_id number;
--End for version 1.2

--start for version 1.3
G_DATE_FROM DATE;
G_DATE_TO date;
G_SO_ADOPTION NUMBER;
G_SO_ADOPTION_VLDN_TBL T_VARCHAR_TYPE_VLDN_TBL;
G_dist_ADOPTION NUMBER;
G_dist_ADOPTION_VLDN_TBL T_VARCHAR_TYPE_VLDN_TBL;
G_region_ADOPTION NUMBER;
G_region_ADOPTION_VLDN_TBL T_VARCHAR_TYPE_VLDN_TBL;
--end for version 1.3

--start for version 1.4
g_catmgt_vldn_tbl     t_varchar_type_vldn_tbl;
g_catclass_vldn_tbl    t_varchar_type_vldn_tbl;  
G_CAT_CLASS_DESC_VLDN_TBL      T_VARCHAR_TYPE_VLDN_TBL; 
g_catmgt_category_desc varchar2(500);
g_catclass varchar2(240);
g_category_class_desc varchar2(240);
--End for version 1.4

FUNCTION GET_ONHAND_QTY(
    P_INVENTORY_ITEM_ID NUMBER,
    P_ORGANIZATION_ID   NUMBER,
    P_SUBINVENTORY_CODE VARCHAR2)
  RETURN number;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Inventory - Onhand Quantity - WC"
  --//
  --// Object Name          :: GET_ONHAND_QTY
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016      Initial Build  -- Added for TMS#20160601-00025.
  --//============================================================================

FUNCTION get_invoice_url(
    p_po_distribution_id NUMBER,
    p_period_name        VARCHAR2,
    p_request_type       VARCHAR2)
  RETURN VARCHAR2;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "HDS Account Analysis Subledger Detail Report"
  --//
  --// Object Name          :: get_invoice_url
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        10/07/2016      Initial Build  -- Added for TMS#20160930-00078
  --//============================================================================  
  
FUNCTION GET_VENDOR_OWNER_NUM(
    p_inventory_item_id NUMBER,
    p_request_type       varchar2)
  RETURN VARCHAR2;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "PO Cost Change Analysis Report - WC"
  --//
  --// Object Name          :: GET_VENDOR_OWNER_NUM
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.2        Siva          10-Jan-2017        TMS#20161025-00027 
  --//============================================================================  

FUNCTION GET_CUST_SO_ADOPTION(
    P_DATE_FROM DATE,
    P_DATE_TO DATE,
    P_BRANCH VARCHAR2,
    P_REGION   VARCHAR2,
    P_DISTRICT VARCHAR2
    )
  return number;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: GET_CUST_SO_ADOPTION
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  
  
function EIS_GET_DATE_FROM
return date;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: EIS_GET_DATE_FROM
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  

function EIS_GET_DATE_TO
RETURN DATE;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: EIS_GET_DATE_TO
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  
  
FUNCTION GET_DISTRICT_ADOPTION(
    P_DATE_FROM DATE,
    P_DATE_TO DATE,
    P_REGION   VARCHAR2,
    P_DISTRICT VARCHAR2
    )
  RETURN NUMBER;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: GET_DISTRICT_ADOPTION
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  
  
FUNCTION GET_REGION_ADOPTION(
    P_DATE_FROM DATE,
    P_DATE_TO DATE,
    P_REGION   VARCHAR2
    )
  RETURN NUMBER; 
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: GET_REGION_ADOPTION
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  

FUNCTION GET_ITEM_CATEGORIES(
    p_item_catalog_group_id NUMBER,
    P_REQUEST_TYPE       varchar2)
  RETURN VARCHAR2;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Extended Attributes Extract (ICC) - WC"
  --//
  --// Object Name          :: GET_ITEM_CATEGORIES
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.4		  Siva		  18-May-2017		 TMS#20170306-00240
  --//============================================================================ 
  
Function Get_Attr_Specifications(
    P_Desc_Flex_Context_Code In Varchar2,
    p_num NUMBER)
  RETURN VARCHAR2;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Extended Attributes Extract (ICC) - WC"
  --//
  --// Object Name          :: Get_Attr_Specifications
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.4		  Siva		  18-May-2017		 TMS#20170306-00240
  --//============================================================================ 
  
END;
/
