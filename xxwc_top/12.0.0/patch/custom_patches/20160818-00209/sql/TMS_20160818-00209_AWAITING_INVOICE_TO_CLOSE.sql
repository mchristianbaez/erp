/*************************************************************************
  $Header TMS_20160818-00209_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160818-00209  Data Fix script for I675908

  PURPOSE: Data fix script for I675908--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        02-AUG-2016  Raghav Velichetti         TMS#20160818-00209 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160818-00209    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED'
where line_id in (66157862,66154977);

   DBMS_OUTPUT.put_line (
         'TMS: 20160818-00209  Sales order lines updated (Expected:2): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160818-00209    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160818-00209 , Errors : ' || SQLERRM);
END;
/