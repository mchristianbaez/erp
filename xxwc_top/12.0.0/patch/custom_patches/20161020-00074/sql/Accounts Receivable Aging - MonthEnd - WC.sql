--Report Name            : Accounts Receivable Aging - MonthEnd - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Accounts Receivable Aging - MonthEnd - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_AGING_MONTH_END_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_AGING_MONTH_END_V',222,'','','','','SA059956','XXEIS','Eis Xxwc Ar Aging Month End V','EXAAMEV','','');
--Delete View Columns for EIS_XXWC_AR_AGING_MONTH_END_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_AGING_MONTH_END_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_AGING_MONTH_END_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','ACCOUNT_BALANCE',222,'Account Balance','ACCOUNT_BALANCE','','','','SA059956','NUMBER','','','Account Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','ACCOUNT_MANAGER',222,'Account Manager','ACCOUNT_MANAGER','','','','SA059956','VARCHAR2','','','Account Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CREDIT_ANALYST',222,'Credit Analyst','CREDIT_ANALYST','','','','SA059956','VARCHAR2','','','Credit Analyst','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','COLLECTOR_NAME',222,'Collector Name','COLLECTOR_NAME','','','','SA059956','VARCHAR2','','','Collector Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CUSTOMER_PROFILE_CLASS',222,'Customer Profile Class','CUSTOMER_PROFILE_CLASS','','','','SA059956','VARCHAR2','','','Customer Profile Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','SITE_CREDIT_HOLD',222,'Site Credit Hold','SITE_CREDIT_HOLD','','','','SA059956','VARCHAR2','','','Site Credit Hold','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CUSTOMER_ACCOUNT_STATUS',222,'Customer Account Status','CUSTOMER_ACCOUNT_STATUS','','','','SA059956','VARCHAR2','','','Customer Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','LAST_PAYMENT_DATE',222,'Last Payment Date','LAST_PAYMENT_DATE','','','','SA059956','DATE','','','Last Payment Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','OVER_THREE_SIXTY_DAYS_BAL',222,'Over Three Sixty Days Bal','OVER_THREE_SIXTY_DAYS_BAL','','~~2','','SA059956','NUMBER','','','Over Three Sixty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','THREE_SIXTY_DAYS_BAL',222,'Three Sixty Days Bal','THREE_SIXTY_DAYS_BAL','','~~2','','SA059956','NUMBER','','','Three Sixty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','ONE_EIGHTY_DAYS_BAL',222,'One Eighty Days Bal','ONE_EIGHTY_DAYS_BAL','','~~2','','SA059956','NUMBER','','','One Eighty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','NINETY_DAYS_BAL',222,'Ninety Days Bal','NINETY_DAYS_BAL','','~~2','','SA059956','NUMBER','','','Ninety Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','SIXTY_DAYS_BAL',222,'Sixty Days Bal','SIXTY_DAYS_BAL','','~~2','','SA059956','NUMBER','','','Sixty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','THIRTY_DAYS_BAL',222,'Thirty Days Bal','THIRTY_DAYS_BAL','','~~2','','SA059956','NUMBER','','','Thirty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','AGE',222,'Age','AGE','','','','SA059956','NUMBER','','','Age','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRANSACTION_REMAINING_BALANCE',222,'Transaction Remaining Balance','TRANSACTION_REMAINING_BALANCE','','','','SA059956','NUMBER','','','Transaction Remaining Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','DUE_DATE',222,'Due Date','DUE_DATE','','','','SA059956','DATE','','','Due Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','RECEIPT_NUMBER',222,'Receipt Number','RECEIPT_NUMBER','','','','SA059956','VARCHAR2','','','Receipt Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','SA059956','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BRANCH_LOCATION',222,'Branch Location','BRANCH_LOCATION','','','','SA059956','VARCHAR2','','','Branch Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CURRENT_BALANCE',222,'Current Balance','CURRENT_BALANCE','','','','SA059956','NUMBER','','','Current Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CUSTOMER_ACCOUNT_NUMBER',222,'Customer Account Number','CUSTOMER_ACCOUNT_NUMBER','','','','SA059956','VARCHAR2','','','Customer Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRANSATION_BALANCE',222,'Transation Balance','TRANSATION_BALANCE','','~~2','','SA059956','NUMBER','','','Transation Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_BILL_TO_SITE_NAME',222,'Trx Bill To Site Name','TRX_BILL_TO_SITE_NAME','','','','SA059956','VARCHAR2','','','Trx Bill To Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','SA059956','DATE','','','Trx Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_PARTY_SITE_NUMBER',222,'Trx Party Site Number','TRX_PARTY_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Trx Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_TYPE',222,'Trx Type','TRX_TYPE','','','','SA059956','VARCHAR2','','','Trx Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','SALESREP_NUMBER',222,'Salesrep Number','SALESREP_NUMBER','','','','SA059956','VARCHAR2','','','Salesrep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','PRISM_CUSTOMER_NUMBER',222,'Prism Customer Number','PRISM_CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Prism Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CUST_PAYMENT_TERM',222,'Cust Payment Term','CUST_PAYMENT_TERM','','','','SA059956','VARCHAR2','','','Cust Payment Term','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_ADDRESS1',222,'Bill To Address1','BILL_TO_ADDRESS1','','','','SA059956','VARCHAR2','','','Bill To Address1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_ADDRESS2',222,'Bill To Address2','BILL_TO_ADDRESS2','','','','SA059956','VARCHAR2','','','Bill To Address2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_ADDRESS3',222,'Bill To Address3','BILL_TO_ADDRESS3','','','','SA059956','VARCHAR2','','','Bill To Address3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_ADDRESS4',222,'Bill To Address4','BILL_TO_ADDRESS4','','','','SA059956','VARCHAR2','','','Bill To Address4','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_CITY',222,'Bill To City','BILL_TO_CITY','','','','SA059956','VARCHAR2','','','Bill To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_CITY_PROVINCE',222,'Bill To City Province','BILL_TO_CITY_PROVINCE','','','','SA059956','VARCHAR2','','','Bill To City Province','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_COUNTRY',222,'Bill To Country','BILL_TO_COUNTRY','','','','SA059956','VARCHAR2','','','Bill To Country','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_ZIP_CODE',222,'Bill To Zip Code','BILL_TO_ZIP_CODE','','','','SA059956','VARCHAR2','','','Bill To Zip Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CUSTOMER_PO_NUMBER',222,'Customer Po Number','CUSTOMER_PO_NUMBER','','','','SA059956','VARCHAR2','','','Customer Po Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','PMT_STATUS',222,'Pmt Status','PMT_STATUS','','','','SA059956','VARCHAR2','','','Pmt Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','AMOUNT_PAID',222,'Amount Paid','AMOUNT_PAID','','','','SA059956','NUMBER','','','Amount Paid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CREDIT_LIMIT',222,'Credit Limit','CREDIT_LIMIT','','','','SA059956','NUMBER','','','Credit Limit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','IN_PROCESS',222,'In Process','IN_PROCESS','','','','SA059956','VARCHAR2','','','In Process','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','LARGEST_BALANCE',222,'Largest Balance','LARGEST_BALANCE','','','','SA059956','NUMBER','','','Largest Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','PHONE_NUMBER',222,'Phone Number','PHONE_NUMBER','','','','SA059956','VARCHAR2','','','Phone Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TERMS',222,'Terms','TERMS','','','','SA059956','VARCHAR2','','','Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TERRITORY',222,'Territory','TERRITORY','','','','SA059956','VARCHAR2','','','Territory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TWELVE_MONTHS_SALES',222,'Twelve Months Sales','TWELVE_MONTHS_SALES','','','','SA059956','NUMBER','','','Twelve Months Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','ACCOUNT_STATUS',222,'Account Status','ACCOUNT_STATUS','','','','SA059956','VARCHAR2','','','Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_BILL_TO_ADDRESS1',222,'Trx Bill To Address1','TRX_BILL_TO_ADDRESS1','','','','SA059956','VARCHAR2','','','Trx Bill To Address1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_BILL_TO_ADDRESS2',222,'Trx Bill To Address2','TRX_BILL_TO_ADDRESS2','','','','SA059956','VARCHAR2','','','Trx Bill To Address2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_BILL_TO_ADDRESS3',222,'Trx Bill To Address3','TRX_BILL_TO_ADDRESS3','','','','SA059956','VARCHAR2','','','Trx Bill To Address3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_BILL_TO_ADDRESS4',222,'Trx Bill To Address4','TRX_BILL_TO_ADDRESS4','','','','SA059956','VARCHAR2','','','Trx Bill To Address4','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_BILL_TO_CITY',222,'Trx Bill To City','TRX_BILL_TO_CITY','','','','SA059956','VARCHAR2','','','Trx Bill To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_BILL_TO_CITY_PROV',222,'Trx Bill To City Prov','TRX_BILL_TO_CITY_PROV','','','','SA059956','VARCHAR2','','','Trx Bill To City Prov','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_BILL_TO_COUNTRY',222,'Trx Bill To Country','TRX_BILL_TO_COUNTRY','','','','SA059956','VARCHAR2','','','Trx Bill To Country','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_ADDRESS',222,'Trx Address','TRX_ADDRESS','','','','SA059956','VARCHAR2','','','Trx Address','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','ACRA_CCID',222,'Acra Ccid','ACRA_CCID','','','','SA059956','NUMBER','','','Acra Ccid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_PARTY_SITE_NAME',222,'Bill To Party Site Name','BILL_TO_PARTY_SITE_NAME','','','','SA059956','VARCHAR2','','','Bill To Party Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_PARTY_SITE_NUMBER',222,'Bill To Party Site Number','BILL_TO_PARTY_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Bill To Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_PRISM_SITE_NUMBER',222,'Bill To Prism Site Number','BILL_TO_PRISM_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Bill To Prism Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_SITE_NAME',222,'Bill To Site Name','BILL_TO_SITE_NAME','','','','SA059956','VARCHAR2','','','Bill To Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','BILL_TO_SITE_USE_ID',222,'Bill To Site Use Id','BILL_TO_SITE_USE_ID','','','','SA059956','NUMBER','','','Bill To Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CASH_RECEIPT_ID',222,'Cash Receipt Id','CASH_RECEIPT_ID','','','','SA059956','NUMBER','','','Cash Receipt Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','SA059956','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','SA059956','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','PAYMENT_SCHEDULE_ID',222,'Payment Schedule Id','PAYMENT_SCHEDULE_ID','','','','SA059956','NUMBER','','','Payment Schedule Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','RCTA_CCID',222,'Rcta Ccid','RCTA_CCID','','','','SA059956','NUMBER','','','Rcta Ccid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','REMIT_TO_ADDRESS_CODE',222,'Remit To Address Code','REMIT_TO_ADDRESS_CODE','','','','SA059956','VARCHAR2','','','Remit To Address Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','SEND_CREDIT_BAL_FLAG',222,'Send Credit Bal Flag','SEND_CREDIT_BAL_FLAG','','','','SA059956','VARCHAR2','','','Send Credit Bal Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','SEND_STATEMENT_FLAG',222,'Send Statement Flag','SEND_STATEMENT_FLAG','','','','SA059956','VARCHAR2','','','Send Statement Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','STMT_BY_JOB',222,'Stmt By Job','STMT_BY_JOB','','','','SA059956','VARCHAR2','','','Stmt By Job','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_BILL_SITE_USE_ID',222,'Trx Bill Site Use Id','TRX_BILL_SITE_USE_ID','','','','SA059956','NUMBER','','','Trx Bill Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_CUSTOMER_ID',222,'Trx Customer Id','TRX_CUSTOMER_ID','','','','SA059956','NUMBER','','','Trx Customer Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_NUMBER',222,'Trx Number','TRX_NUMBER','','','','SA059956','VARCHAR2','','','Trx Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_PARTY_SITE_NAME',222,'Trx Party Site Name','TRX_PARTY_SITE_NAME','','','','SA059956','VARCHAR2','','','Trx Party Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','AVERAGE_DAYS',222,'Average Days','AVERAGE_DAYS','','','','SA059956','NUMBER','','','Average Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_MONTH_END_V','TRX_BILL_TO_ZIP_CODE',222,'Trx Bill To Zip Code','TRX_BILL_TO_ZIP_CODE','','','','SA059956','VARCHAR2','','','Trx Bill To Zip Code','','','');
--Inserting View Components for EIS_XXWC_AR_AGING_MONTH_END_V
--Inserting View Component Joins for EIS_XXWC_AR_AGING_MONTH_END_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Accounts Receivable Aging - MonthEnd - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Accounts Receivable Aging - MonthEnd - WC
xxeis.eis_rs_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select name from ar_collectors','null','Collector','Displays list of values for Collector','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct name from hz_cust_profile_classes','null','PROFILE CLASS','This LOV lists all the profile classes of the customers','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'SELECT  resource_name resource_name
 FROM jtf_rs_role_relations a,
  jtf_rs_roles_vl b,
  jtf_rs_resource_extns_vl c
WHERE a.role_resource_type  = ''RS_INDIVIDUAL''
AND a.role_resource_id      = c.resource_id
AND a.role_id               = b.role_id
AND b.role_code             = ''CREDIT_ANALYST''
AND c.category              = ''EMPLOYEE''
AND NVL(a.delete_flag,''N'') <> ''Y''
Order BY resource_name','','Credit Analyst','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct credit_hold from hz_customer_profiles','','Credit Holds','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT SEGMENT2 Location,
  FV.description
FROM GL_CODE_COMBINATIONS_KFV GCC,
  FND_FLEX_VALUES_vL FV
WHERE 1                   =1
AND FV.FLEX_VALUE(+)      =GCC.SEGMENT2
AND FV.FLEX_VALUE_SET_ID IN
  (SELECT FS.FLEX_VALUE_SET_ID
  FROM FND_FLEX_VALUE_SETS FS
  WHERE FS.FLEX_VALUE_SET_NAME =''XXCUS_GL_LOCATION''
  )
order by SEGMENT2','','AR Branch Location LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select account_name,account_number from hz_cust_accounts','','AR Customer Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT  DISTINCT PARTY_SITE_NUMBER SITE_NUMBER,Site.location SITE_NAME
FROM  HZ_PARTIES HP,
      HZ_CUST_ACCOUNTS HCS,
      HZ_PARTY_SITES PARTY_SITES,
      Hz_Cust_Site_Uses Site,
      HZ_CUST_ACCT_SITES CUST_SITES
WHERE HP.PARTY_ID =HCS.PARTY_ID
AND HCS.CUST_ACCOUNT_ID =CUST_SITES.CUST_ACCOUNT_ID 
AND SITE.CUST_ACCT_SITE_ID= CUST_SITES.CUST_ACCT_SITE_ID(+)
AND CUST_SITES.PARTY_SITE_ID = PARTY_SITES.PARTY_SITE_ID(+)
order by PARTY_SITE_NUMBER','','AR Site Number LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select  customer_status.meaning Status
from  fnd_lookup_values_vl customer_status
where customer_status.lookup_type= ''ACCOUNT_STATUS''
 and customer_status.view_application_id=222','','XXWC Customer Account Status','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct account_manager
from XXEIS.EIS_XXWC_AR_AGING_MONTH_END_V
order by account_manager','','XXWC AR Aging SalesRep Name','Sales Rep Name from AR Aging View','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct salesrep_number
from XXEIS.EIS_XXWC_AR_AGING_MONTH_END_V
order by salesrep_number','','XXWC AR Aging SalesRep Num','Sales Rep number from Aging View','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Accounts Receivable Aging - MonthEnd - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Accounts Receivable Aging - MonthEnd - WC
xxeis.eis_rs_utility.delete_report_rows( 'Accounts Receivable Aging - MonthEnd - WC' );
--Inserting Report - Accounts Receivable Aging - MonthEnd - WC
xxeis.eis_rs_ins.r( 222,'Accounts Receivable Aging - MonthEnd - WC','','Accounts Receivable Aging - MonthEnd - WC','','','','SA059956','EIS_XXWC_AR_AGING_MONTH_END_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Accounts Receivable Aging - MonthEnd - WC
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'ACCOUNT_BALANCE','Account Total Balance','Account Balance','','~T~D~2','default','','30','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'ACCOUNT_MANAGER','Salesrep Name','Account Manager','','','default','','29','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'AGE','Age','Age','','~~~','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'BRANCH_LOCATION','Branch Location','Branch Location','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'COLLECTOR_NAME','Collector','Collector Name','','','default','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'CREDIT_ANALYST','Credit Analyst','Credit Analyst','','','default','','27','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'CURRENT_BALANCE',' Current','Current Balance','','~T~D~2','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'CUSTOMER_ACCOUNT_NUMBER','Customer Number','Customer Account Number','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'CUSTOMER_ACCOUNT_STATUS','Customer Account Status','Customer Account Status','','','default','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'CUSTOMER_PROFILE_CLASS','Profile Class','Customer Profile Class','','','default','','25','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'DUE_DATE','Due Date','Due Date','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'LAST_PAYMENT_DATE','Last Payment Date','Last Payment Date','','','default','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'NINETY_DAYS_BAL','61 To 90 Days','Ninety Days Bal','','~T~D~2','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'ONE_EIGHTY_DAYS_BAL','91 To 180 Days','One Eighty Days Bal','','~T~D~2','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'OVER_THREE_SIXTY_DAYS_BAL','361+Days Past Due','Over Three Sixty Days Bal','','~T~D~2','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'RECEIPT_NUMBER','Receipt Number','Receipt Number','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'SITE_CREDIT_HOLD','Site Credit Hold Status','Site Credit Hold','','','default','','26','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'THIRTY_DAYS_BAL','1 To 30 Days','Thirty Days Bal','','~T~D~2','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'THREE_SIXTY_DAYS_BAL','181 To 360 Days','Three Sixty Days Bal','','~T~D~2','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'TRANSACTION_REMAINING_BALANCE','Transaction Remaining Balance','Transaction Remaining Balance','','~T~D~2','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'TRANSATION_BALANCE','Transaction Balance','Transation Balance','','~T~D~2','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'TRX_BILL_TO_SITE_NAME','Site Name','Trx Bill To Site Name','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'TRX_DATE','Invoice Date','Trx Date','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'SIXTY_DAYS_BAL','31 To 60 Days','Sixty Days Bal','','~T~D~2','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'TRX_PARTY_SITE_NUMBER','Site Number','Trx Party Site Number','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'TRX_TYPE','Transaction Type','Trx Type','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'OUTSTANDING_AMOUNT','Outstanding Balance','Trx Type','NUMBER','~T~D~2','default','','14','Y','','','','','','','EXAAMEV.transaction_remaining_balance','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - MonthEnd - WC',222,'SALESREP_NUMBER','Salesrep Number','Salesrep Number','','','default','','28','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_MONTH_END_V','','');
--Inserting Report Parameters - Accounts Receivable Aging - MonthEnd - WC
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Customer Name','Customer Name','CUSTOMER_NAME','IN','AR Customer Name LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Customer Number','Customer Number','CUSTOMER_ACCOUNT_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','2','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Customer Account Status','Customer Account Status','CUSTOMER_ACCOUNT_STATUS','IN','XXWC Customer Account Status','','VARCHAR2','N','Y','3','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Collector','Collector','COLLECTOR_NAME','IN','Collector','','VARCHAR2','N','Y','4','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Credit Analyst','Credit Analyst','CREDIT_ANALYST','IN','Credit Analyst','','VARCHAR2','N','Y','5','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Credit Hold','Credit Hold','SITE_CREDIT_HOLD','IN','Credit Holds','','VARCHAR2','N','Y','6','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Profile Class','Profile Class','CUSTOMER_PROFILE_CLASS','IN','PROFILE CLASS','','VARCHAR2','N','Y','7','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Sales Rep Name','Sales Rep Name','ACCOUNT_MANAGER','IN','XXWC AR Aging SalesRep Name','','VARCHAR2','N','Y','8','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Location','Location','BRANCH_LOCATION','IN','AR Branch Location LOV','','VARCHAR2','N','Y','10','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Site Number','Site Number','TRX_PARTY_SITE_NUMBER','IN','AR Site Number LOV','','VARCHAR2','N','Y','11','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - MonthEnd - WC',222,'Sales Rep Number','Sales Rep Number','SALESREP_NUMBER','IN','XXWC AR Aging SalesRep Num','','VARCHAR2','N','Y','9','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Accounts Receivable Aging - MonthEnd - WC
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'CUSTOMER_NAME','IN',':Customer Name','','','Y','1','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'CUSTOMER_ACCOUNT_NUMBER','IN',':Customer Number','','','Y','2','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'CUSTOMER_ACCOUNT_STATUS','IN',':Customer Account Status','','','Y','3','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'COLLECTOR_NAME','IN',':Collector','','','Y','4','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'CREDIT_ANALYST','IN',':Credit Analyst','','','Y','5','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'SITE_CREDIT_HOLD','IN',':Credit Hold','','','Y','6','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'CUSTOMER_PROFILE_CLASS','IN',':Profile Class','','','Y','7','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'BRANCH_LOCATION','IN',':Location','','','Y','10','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'TRX_PARTY_SITE_NUMBER','IN',':Site Number','','','Y','11','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'ACCOUNT_MANAGER','IN',':Sales Rep Name','','','Y','8','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - MonthEnd - WC',222,'SALESREP_NUMBER','IN',':Sales Rep Number','','','Y','9','Y','SA059956');
--Inserting Report Sorts - Accounts Receivable Aging - MonthEnd - WC
xxeis.eis_rs_ins.rs( 'Accounts Receivable Aging - MonthEnd - WC',222,'CUSTOMER_NAME','ASC','SA059956','','');
xxeis.eis_rs_ins.rs( 'Accounts Receivable Aging - MonthEnd - WC',222,'TRX_PARTY_SITE_NUMBER','ASC','SA059956','','');
xxeis.eis_rs_ins.rs( 'Accounts Receivable Aging - MonthEnd - WC',222,'TRX_DATE','ASC','SA059956','','');
--Inserting Report Triggers - Accounts Receivable Aging - MonthEnd - WC
--Inserting Report Templates - Accounts Receivable Aging - MonthEnd - WC
--Inserting Report Portals - Accounts Receivable Aging - MonthEnd - WC
--Inserting Report Dashboards - Accounts Receivable Aging - MonthEnd - WC
xxeis.eis_rs_ins.r_dash( 'Accounts Receivable Aging - MonthEnd - WC','Dynamic 668','Dynamic 668','absolute line','large','Account Total Balance','Account Total Balance','Account Total Balance','Account Total Balance','Avg','SA059956');
xxeis.eis_rs_ins.r_dash( 'Accounts Receivable Aging - MonthEnd - WC','Dynamic 665','Dynamic 665','absolute line','large','Last Payment Date','Last Payment Date','Account Total Balance','Account Total Balance','Avg','SA059956');
--Inserting Report Security - Accounts Receivable Aging - MonthEnd - WC
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','','10010432','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','','LC053655','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','','RB054040','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','','RV003897','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','','SO004816','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','','SS084202','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50847',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','101','','50720',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','101','','50723',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','101','','50722',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50638',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50622',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','20778',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','21404',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','20678',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50844',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50845',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50846',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50865',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50862',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50863',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50864',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50866',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50848',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50868',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50849',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50867',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50870',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50871',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50869',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','201','','50850',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','201','','50872',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50851',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','702','','50875',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','260','','50758',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','260','','50770',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','702','','50852',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50876',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50878',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50877',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50879',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50853',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50880',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50854',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50873',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','512','','50881',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','175','','50922',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','175','','50941',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50942',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','140','','50728',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','140','','50789',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','140','','50874',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','101','','50982',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50882',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50883',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50981',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50855',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50884',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','20005','','50861',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','20005','','50843',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','704','','50885',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50857',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50858',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50859',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50886',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50860',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50901',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','200','','50904',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','200','','50905',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','200','','50902',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','200','','50890',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','200','','50903',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','200','','50887',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','200','','50888',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','200','','50889',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','661','','50891',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','201','','50921',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','201','','50892',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','201','','50910',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','201','','50893',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50894',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','1','','50962',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50895',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','20005','','50897',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','20005','','50900',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','1','','50961',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','706','','50909',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50856',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','50861',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50871',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50872',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50835',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','401','','50840',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50846',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50845',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50848',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50849',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','222','','50944',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','20005','','50880',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - MonthEnd - WC','660','','51044',222,'SA059956','','');
--Inserting Report Pivots - Accounts Receivable Aging - MonthEnd - WC
xxeis.eis_rs_ins.rpivot( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','TRANSACTION_REMAINING_BALANCE','DATA_FIELD','SUM','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','OUTSTANDING_AMOUNT','DATA_FIELD','SUM','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','CURRENT_BALANCE','DATA_FIELD','SUM','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','THIRTY_DAYS_BAL','DATA_FIELD','SUM','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','SIXTY_DAYS_BAL','DATA_FIELD','SUM','','5','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','NINETY_DAYS_BAL','DATA_FIELD','SUM','','6','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','ONE_EIGHTY_DAYS_BAL','DATA_FIELD','SUM','','7','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','THREE_SIXTY_DAYS_BAL','DATA_FIELD','SUM','','8','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','OVER_THREE_SIXTY_DAYS_BAL','DATA_FIELD','SUM','','9','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','CUSTOMER_NAME','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','CUSTOMER_ACCOUNT_NUMBER','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','TRX_PARTY_SITE_NUMBER','ROW_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Accounts Receivable Aging - MonthEnd - WC',222,'Pivot','TRX_BILL_TO_SITE_NAME','ROW_FIELD','','','4','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
