---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_CASH_DRWR_RECON_V $
  Module Name : OM
  REVISIONS   :
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     17-Aug-2016           Siva   		 TMS#20160816-00088
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_CASH_DRWR_RECON_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_CASH_DRWR_RECON_V (ORDER_NUMBER, ORG_ID, PAYMENT_TYPE_CODE, PAYMENT_TYPE_CODE_NEW, TAKEN_BY, CASH_TYPE, CHK_CARDNO, CARD_NUMBER, CARD_ISSUER_CODE, CARD_ISSUER_NAME, CASH_AMOUNT, CUSTOMER_NUMBER, CUSTOMER_NAME, BRANCH_NUMBER, CHECK_NUMBER, ORDER_DATE, CASH_DATE, PAYMENT_CHANNEL_NAME, NAME, PAYMENT_NUMBER, PAYMENT_AMOUNT, ORDER_AMOUNT, TOTAL_ORDER_AMOUNT, PARTY_ID, CUST_ACCOUNT_ID, INVOICE_NUMBER, INVOICE_DATE, SEGMENT_NUMBER, HEADER_ID, CASH_RECEIPT_ID, ORDER_TYPE, LAST_UPDATE_DATE, LINE_LAST_UPDATE_DATE, ON_ACCOUNT)
AS
  SELECT DISTINCT oh.order_number ,
    oh.org_id ,
    CASE
      WHEN ( ( oe.payment_type_code IN ('CHECK', 'CREDIT_CARD')
      AND ( upper (arc.name) LIKE upper ('%Card%')
      OR upper (arc.name) LIKE upper ('%AMX%')
      OR upper (arc.name) LIKE upper ('%VMD%')))
      OR oh.attribute8 = 'CREDIT_CARD')
      THEN 'Credit Card'
      WHEN ( (oe.payment_type_code IN ('CHECK')
      AND upper (arc.name) NOT LIKE upper ('%Card%'))
      OR oh.attribute8 = 'CHECK')
      THEN 'Check'
      WHEN (oe.payment_type_code IN ('CASH')
      OR oh.attribute8            = 'CASH')
      THEN 'Cash'
      ELSE NULL
    END payment_type_code ,
    CASE
      WHEN oe.payment_type_code IN ('CHECK', 'CREDIT_CARD')
      AND upper (arc.name) LIKE upper ('%Card%')
      THEN 'Credit Card'
      WHEN oe.payment_type_code IN ('CHECK')
      AND upper (arc.name) NOT LIKE upper ('%Card%')
      THEN 'Check'
      ELSE olkp.meaning
    END payment_type_code_new ,
    ppf.last_name taken_by ,
    CASE
      WHEN otl.name = 'RETURN ORDER'
      THEN 'CASH RETURN'
      WHEN oe.payment_type_code IN ('CASH', 'CHECK', 'CREDIT_CARD')
      AND NOT EXISTS
        (SELECT ordered_item
        FROM oe_order_lines ol
        WHERE ol.header_id = oh.header_id
        AND ordered_item LIKE '%DEPOSIT%'
        )
        /*AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )*/
      THEN 'CASH SALES'
      WHEN oe.payment_type_code IN ('CASH', 'CHECK', 'CREDIT_CARD')
      AND EXISTS
        (SELECT ordered_item
        FROM oe_order_lines ol
        WHERE ol.header_id = oh.header_id
        AND ordered_item LIKE '%DEPOSIT%'
        )
        /*AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )*/
      THEN 'DEPOSIT'
    END cash_type ,
    DECODE (oe.payment_type_code ,'CHECK', oe.check_number ,'CREDIT_CARD', SUBSTR (ite.card_number, -4)) chk_cardno ,
    SUBSTR (ite.card_number,                                                                        -4) card_number ,
    ite.card_issuer_code ,
    ite.card_issuer_name ,
    CASE
      WHEN otl.name     = 'RETURN ORDER'
      AND oe.header_id IS NOT NULL
      THEN
        (SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0) order_amount
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
      WHEN TRUNC (rap.creation_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date
        -- WHEN TRUNC(rap.creation_date) <= NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE, NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE,SYSDATE))
      THEN --NVL(cr.amount,0)
        (SELECT SUM (ar.amount_applied)
        FROM ar_receivable_applications_all ar,
          ar_cash_receipts_all ac
        WHERE payment_set_id         = oe.payment_set_id
        AND ar.status               <> 'UNAPP'
        AND TRUNC (ar.creation_date) = TRUNC (rap.creation_date)
        AND ac.cash_receipt_id       = ar.cash_receipt_id
        AND ac.receipt_method_id     = oe.receipt_method_id --   GROUP BY CASH_RECEIPT_ID
          -- AND TRUNC(CREATION_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        )
      ELSE 0
    END cash_amount ,
    hca.account_number customer_number ,
    hzp.party_name customer_name ,
    ood.organization_code branch_number ,
    oe.check_number ,
    TRUNC (oh.ordered_date) order_date
    --         ,TRUNC (oe.last_update_date) cash_date                                               --Added By Mahender
    -- ,TRUNC (rap.creation_date) cash_date  --Commented by Mahender
    ,
    TRUNC (rap.gl_posted_date) cash_date --Added By Mahender
    ,
    ite.payment_channel_name ,
    arc.name ,
    payment_number ,
    (
    CASE
      WHEN otl.name     = 'RETURN ORDER'
      AND oe.header_id IS NOT NULL
      THEN
        (SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0)
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
      WHEN TRUNC (rap.creation_date) <= NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date ,NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date, sysdate))
        -- WHEN TRUNC(rap.creation_date) <= NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE, NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE,SYSDATE))
      THEN --NVL(cr.amount,0)
        (SELECT NVL (ROUND (SUM (amount_applied), 2), 0)
        FROM ar_receivable_applications_all
        WHERE payment_set_id       = oe.payment_set_id
        AND status                <> 'UNAPP'
        AND TRUNC (creation_date) <= NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date ,NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date, sysdate))
        )
        --       (SELECT SUM(AMOUNT_APPLIED)
        --          FROM AR_RECEIVABLE_APPLICATIONS_ALL
        --          WHERE PAYMENT_SET_ID = OE.PAYMENT_SET_ID
        --          AND STATUS <>'UNAPP'
        --          AND TRUNC(CREATION_DATE) = trunc(rap.creation_date)
        --         -- AND TRUNC(CREATION_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        --          )
      ELSE 0
    END) payment_amount ,
    CASE
      WHEN payment_number = 1
      AND ( otl.name     <> 'STANDARD ORDER'
      OR EXISTS
        (SELECT 1
        FROM oe_order_lines ol1
        WHERE ol1.header_id = oh.header_id
        AND ol1.ordered_item LIKE '%DEPOSIT%'
        ))
      THEN
        (SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0) order_amount
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
      WHEN payment_number = 1
      AND otl.name        = 'STANDARD ORDER'
      THEN
        (SELECT NVL ( ROUND ( SUM ( ROUND (NVL (ol.shipped_quantity, 0) * ol.unit_selling_price, 2) + ol.tax_value) ,2) ,0) order_amount
        FROM oe_order_lines ol
        WHERE ol.header_id             = oh.header_id
        AND NVL (shipped_quantity, 0) <> 0 --   AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        AND ol.ordered_item NOT LIKE '%DEPOSIT%'
        )
      WHEN payment_number IS NULL
      AND ( otl.name      <> 'STANDARD ORDER'
      OR EXISTS
        (SELECT 1
        FROM oe_order_lines ol1
        WHERE ol1.header_id = oh.header_id
        AND ol1.ordered_item LIKE '%DEPOSIT%'
        ))
      THEN
        (SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0) order_amount
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
      WHEN payment_number IS NULL
      AND otl.name         = 'STANDARD ORDER'
      THEN
        (SELECT NVL ( ROUND (SUM (ROUND (ol.shipped_quantity * ol.unit_selling_price, 2) + ol.tax_value) ,2) ,0) order_amount
        FROM oe_order_lines ol
        WHERE ol.header_id = oh.header_id --  AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        AND ol.ordered_item NOT LIKE '%DEPOSIT%'
        )
      ELSE 0
    END order_amount ,
    CASE
      WHEN otl.name = 'STANDARD ORDER'
      THEN
        (SELECT NVL ( ROUND ( SUM ( ROUND (NVL (ol.shipped_quantity, 0) * ol.unit_selling_price, 2) + ol.tax_value) ,2) ,0) order_amount
        FROM oe_order_lines ol
        WHERE ol.header_id                     = oh.header_id
        AND NVL (shipped_quantity, 0)         <> 0
        AND ( TRUNC (ol.actual_shipment_date) <= xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date
        OR TRUNC (ol.creation_date)           <= xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date)
        AND ol.ordered_item NOT LIKE '%DEPOSIT%'
        )
      ELSE
        --       ( SELECT NVL (ROUND(SUM(ROUND(NVL(OL.SHIPPED_QUANTITY,0)*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE),2),0) ORDER_AMOUNT
        --        FROM OE_ORDER_LINES OL
        --        WHERE OL.HEADER_ID                  =12347382
        --        AND NVL(SHIPPED_QUANTITY,0)        <> 0
        --        AND (TRUNC(OL.ACTUAL_SHIPMENT_DATE) <= XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        --          OR TRUNC(OL.CREATION_DATE) <= XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)
        --        )
        (
        SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0) order_amount
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
    END total_order_amount ,
    hzp.party_id ,
    hca.cust_account_id ,
    (SELECT MAX (rct.trx_number)
    FROM ra_customer_trx rct
    WHERE TO_CHAR (oh.order_number) = rct.interface_header_attribute1
    ) invoice_number , --   NULL CREDIT_NUMBER,
    (SELECT MAX (rct.trx_date)
    FROM ra_customer_trx rct
    WHERE TO_CHAR (oh.order_number) = rct.interface_header_attribute1
    ) invoice_date ,
    (SELECT MAX (gcc.segment2)
    FROM oe_order_lines l ,
      ra_customer_trx_lines rctl ,
      ra_customer_trx rct ,
      ra_cust_trx_line_gl_dist rctg ,
      gl_code_combinations_kfv gcc
    WHERE oh.header_id            = l.header_id
    AND TO_CHAR (oh.order_number) = rctl.interface_line_attribute1
    AND TO_CHAR (l.line_id)       = rctl.interface_line_attribute6
    AND rct.customer_trx_id       = rctl.customer_trx_id
    AND rctl.customer_trx_id      = rctg.customer_trx_id
    AND rctl.customer_trx_line_id = rctg.customer_trx_line_id
    AND rctg.code_combination_id  = gcc.code_combination_id
    ) segment_number ,
    oh.header_id header_id ,
    0 cash_receipt_id ,
    otl.name order_type ,
    TRUNC (oh.last_update_date) last_update_date ,
    TRUNC (ol.last_update_date) line_last_update_date ,
    tmp.on_account on_account
  FROM oe_order_headers oh ,
    oe_order_lines ol ,
    oe_payments oe ,
    xxeis.eis_xxwc_cash_exce_tmp_tbl tmp ,
    ar_receipt_methods arc ,
    oe_transaction_types_vl otl ,
    ar_receivable_applications_all rap ,
    iby_trxn_extensions_v ite ,
    oe_lookups olkp ,
    per_all_people_f ppf ,
    fnd_user fu ,
    hz_parties hzp ,
    hz_cust_accounts hca ,
    org_organization_definitions ood ,
    ra_terms ra
  WHERE 1                  = 1
  AND oh.header_id         = ol.header_id
  AND oe.header_id(+)      = oh.header_id
  AND oe.receipt_method_id = arc.receipt_method_id(+)
  AND oh.order_type_id     = otl.transaction_type_id
  AND oh.payment_term_id   = ra.term_id
  AND ( arc.name          IS NULL
  OR (upper (arc.name) NOT LIKE upper ('%GIFT%')
  OR upper (arc.name) NOT LIKE upper ('%REBATE%')))
  AND rap.payment_set_id(+) = oe.payment_set_id
  AND rap.status           <> 'UNAPP' --Mahi
    --          AND TRUNC(rap.creation_date) = TRUNC(ol.last_update_date)
  AND oe.payment_set_id    = tmp.payment_set_id(+)
  AND oe.payment_type_code = olkp.lookup_code(+)
  AND oe.trxn_extension_id = ite.trxn_extension_id(+)
  AND fu.user_id           = oh.created_by
  AND fu.employee_id       = ppf.person_id(+)
  AND TRUNC (oh.ordered_date) BETWEEN TRUNC (NVL (ppf.effective_start_date, oh.ordered_date)) AND TRUNC (NVL (ppf.effective_end_date, oh.ordered_date))
  AND oh.sold_to_org_id              = hca.cust_account_id
  AND hzp.party_id                   = hca.party_id
  AND oh.ship_from_org_id            = ood.organization_id
  AND oe.payment_collection_event(+) = 'PREPAY'
  AND oh.flow_status_code           IN ('BOOKED', 'CLOSED', 'CANCELLED')
  AND olkp.lookup_type(+)            = 'PAYMENT TYPE'
  AND otl.name                      <> 'INTERNAL ORDER'
  AND (payment_number               IS NOT NULL
  OR ra.name                        IN ('COD', 'PRFRDCASH', 'IMMEDIATE'))
  AND NOT EXISTS
    (SELECT 1
    FROM hz_customer_profiles hcp,
      hz_cust_profile_classes hcpc
    WHERE hca.party_id       = hcp.party_id
    AND hca.cust_account_id  = hcp.cust_account_id
    AND hcp.site_use_id     IS NULL
    AND hcp.profile_class_id = hcpc.profile_class_id(+)
    AND hcpc.name LIKE 'WC%Branches%'
    )
  AND ( otl.name IN ('COUNTER ORDER', 'STANDARD ORDER')
  OR NOT EXISTS
    (SELECT 1
    FROM xxwc_om_cash_refund_tbl xoc
    WHERE xoc.return_header_id = oh.header_id
    ))
  AND ( ( otl.name = 'STANDARD ORDER'
  AND EXISTS
    (SELECT 1
    FROM oe_order_lines l
    WHERE oh.header_id = l.header_id
    AND ( ( (TRUNC (l.actual_shipment_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date)
    OR ( TRUNC (rap.creation_date) <= xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date
    AND (TRUNC (rap.creation_date) >= xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date) --                                                  OR TRUNC (rap.creation_date) <= xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date --Mahi
      ))
    OR (TRUNC (rap.creation_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date) -- OR (TRUNC(L.LAST_UPDATE_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)
      )
    OR ( oh.header_id = l.header_id
    AND l.ordered_item LIKE '%DEPOSIT%'
    AND (TRUNC (oh.ordered_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date))
    ))
  OR ( otl.name <> 'STANDARD ORDER'
  AND ( (TRUNC (oh.ordered_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date)
  AND (TRUNC (rap.creation_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date)
  OR (TRUNC (rap.creation_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date)
  OR (TRUNC (ol.last_update_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date))))
  /* AND EXISTS
  (SELECT 1
  FROM OE_ORDER_LINES L
  WHERE OH.HEADER_ID =L.HEADER_ID
  AND (( otl.name    ='STANDARD ORDER'
  AND (TRUNC(L.ACTUAL_SHIPMENT_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE ))
  OR (L.ORDERED_ITEM LIKE '%DEPOSIT%')
  OR (oe.Payment_Number IS NOT NULL)
  OR (otl.name <> 'STANDARD ORDER' )
  )*/
  /* AND EXISTS
  (SELECT 1
  FROM RA_CUSTOMER_TRX RCT
  WHERE TO_CHAR(Oh.Order_Number) =Rct.Interface_Header_Attribute1
  )*/
  ----Union query added to display retun orders.
  UNION
  SELECT oh.order_number ,
    oh.org_id ,
    CASE
      WHEN ( ( oe.payment_type_code IN ('CHECK', 'CREDIT_CARD')
      AND ( upper (arc.name) LIKE upper ('%Card%')
      OR upper (arc.name) LIKE upper ('%AMX%')
      OR upper (arc.name) LIKE upper ('%VMD%')))
      OR oh.attribute8 = 'CREDIT_CARD')
      THEN 'Credit Card'
      WHEN ( (oe.payment_type_code IN ('CHECK')
      AND upper (arc.name) NOT LIKE upper ('%Card%'))
      OR oh.attribute8 = 'CHECK')
      THEN 'Check'
      WHEN (oe.payment_type_code IN ('CASH')
      OR oh.attribute8            = 'CASH')
      THEN 'Cash'
      ELSE NULL
    END payment_type_code ,
    CASE
      WHEN oe.payment_type_code IN ('CHECK', 'CREDIT_CARD')
      AND upper (arc.name) LIKE upper ('%Card%')
      THEN 'Credit Card'
      WHEN oe.payment_type_code IN ('CHECK')
      AND upper (arc.name) NOT LIKE upper ('%Card%')
      THEN 'Check'
      ELSE olkp.meaning
    END payment_type_code_new ,
    ppf.last_name taken_by ,
    CASE
      WHEN otl.name = 'RETURN ORDER'
      THEN 'CASH RETURN'
      WHEN oe.payment_type_code IN ('CASH', 'CHECK', 'CREDIT_CARD')
      AND NOT EXISTS
        (SELECT ordered_item
        FROM oe_order_lines ol
        WHERE ol.header_id = oh.header_id
        AND ordered_item LIKE '%DEPOSIT%'
        )
      THEN 'CASH SALES'
      WHEN oe.payment_type_code IN ('CASH', 'CHECK', 'CREDIT_CARD')
      AND EXISTS
        (SELECT ordered_item
        FROM oe_order_lines ol
        WHERE ol.header_id = oh.header_id
        AND ordered_item LIKE '%DEPOSIT%'
        )
      THEN 'DEPOSIT'
    END cash_type ,
    DECODE (oe.payment_type_code ,'CHECK', oe.check_number ,'CREDIT_CARD', SUBSTR (ite.card_number, -4)) chk_cardno ,
    SUBSTR (ite.card_number,                                                                        -4) card_number ,
    ite.card_issuer_code ,
    ite.card_issuer_name ,
    CASE
      WHEN otl.name     = 'RETURN ORDER'
      AND oe.header_id IS NOT NULL
      THEN
        (SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0) order_amount
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
      WHEN TRUNC (rap.creation_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date
      THEN --NVL(cr.amount,0)
        (SELECT SUM (ar.amount_applied)
        FROM ar_receivable_applications_all ar,
          ar_cash_receipts_all ac
        WHERE payment_set_id         = oe.payment_set_id
        AND ar.status               <> 'UNAPP'
        AND TRUNC (ar.creation_date) = TRUNC (rap.creation_date)
        AND ac.cash_receipt_id       = ar.cash_receipt_id
        AND ac.receipt_method_id     = oe.receipt_method_id --   GROUP BY CASH_RECEIPT_ID
          -- AND TRUNC(CREATION_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        )
      ELSE 0
    END cash_amount ,
    hca.account_number customer_number ,
    hzp.party_name customer_name ,
    ood.organization_code branch_number ,
    oe.check_number ,
    TRUNC (oh.ordered_date) order_date
    --         ,TRUNC (oe.last_update_date) cash_date                                               --added by Mahender
    --         ,TRUNC (rap.creation_date) cash_date   --Commented by Mahender
    ,
    TRUNC (rap.gl_posted_date) cash_date --Added By Mahender
    ,
    ite.payment_channel_name ,
    arc.name ,
    payment_number ,
    (
    CASE
      WHEN otl.name     = 'RETURN ORDER'
      AND oe.header_id IS NOT NULL
      THEN
        (SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0)
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
      WHEN TRUNC (rap.creation_date) <= NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date ,NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date, sysdate))
        -- WHEN TRUNC(rap.creation_date) <= NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE, NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE,SYSDATE))
      THEN --NVL(cr.amount,0)
        (SELECT NVL (ROUND (SUM (amount_applied), 2), 0)
        FROM ar_receivable_applications_all
        WHERE payment_set_id       = oe.payment_set_id
        AND status                <> 'UNAPP'
        AND TRUNC (creation_date) <= NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date ,NVL (xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date, sysdate))
        )
      ELSE 0
    END) payment_amount ,
    CASE
      WHEN payment_number = 1
      AND ( otl.name     <> 'STANDARD ORDER'
      OR EXISTS
        (SELECT 1
        FROM oe_order_lines ol1
        WHERE ol1.header_id = oh.header_id
        AND ol1.ordered_item LIKE '%DEPOSIT%'
        ))
      THEN
        (SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0) order_amount
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
      WHEN payment_number = 1
      AND otl.name        = 'STANDARD ORDER'
      THEN
        (SELECT NVL ( ROUND ( SUM ( ROUND (NVL (ol.shipped_quantity, 0) * ol.unit_selling_price, 2) + ol.tax_value) ,2) ,0) order_amount
        FROM oe_order_lines ol
        WHERE ol.header_id             = oh.header_id
        AND NVL (shipped_quantity, 0) <> 0 --   AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        AND ol.ordered_item NOT LIKE '%DEPOSIT%'
        )
      WHEN payment_number IS NULL
      AND ( otl.name      <> 'STANDARD ORDER'
      OR EXISTS
        (SELECT 1
        FROM oe_order_lines ol1
        WHERE ol1.header_id = oh.header_id
        AND ol1.ordered_item LIKE '%DEPOSIT%'
        ))
      THEN
        (SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0) order_amount
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
      WHEN payment_number IS NULL
      AND otl.name         = 'STANDARD ORDER'
      THEN
        (SELECT NVL ( ROUND (SUM (ROUND (ol.shipped_quantity * ol.unit_selling_price, 2) + ol.tax_value) ,2) ,0) order_amount
        FROM oe_order_lines ol
        WHERE ol.header_id = oh.header_id --  AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        AND ol.ordered_item NOT LIKE '%DEPOSIT%'
        )
      ELSE 0
    END order_amount ,
    CASE
      WHEN otl.name = 'STANDARD ORDER'
      THEN
        (SELECT NVL ( ROUND ( SUM ( ROUND (NVL (ol.shipped_quantity, 0) * ol.unit_selling_price, 2) + ol.tax_value) ,2) ,0) order_amount
        FROM oe_order_lines ol
        WHERE ol.header_id                     = oh.header_id
        AND NVL (shipped_quantity, 0)         <> 0
        AND ( TRUNC (ol.actual_shipment_date) <= xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date
        OR TRUNC (ol.creation_date)           <= xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date)
        AND ol.ordered_item NOT LIKE '%DEPOSIT%'
        )
      ELSE
        (SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0) order_amount
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
    END total_order_amount ,
    hzp.party_id ,
    hca.cust_account_id ,
    (SELECT MAX (rct.trx_number)
    FROM ra_customer_trx rct
    WHERE TO_CHAR (oh.order_number) = rct.interface_header_attribute1
    ) invoice_number , --   NULL CREDIT_NUMBER,
    (SELECT MAX (rct.trx_date)
    FROM ra_customer_trx rct
    WHERE TO_CHAR (oh.order_number) = rct.interface_header_attribute1
    ) invoice_date ,
    (SELECT MAX (gcc.segment2)
    FROM oe_order_lines l ,
      ra_customer_trx_lines rctl ,
      ra_customer_trx rct ,
      ra_cust_trx_line_gl_dist rctg ,
      gl_code_combinations_kfv gcc
    WHERE oh.header_id            = l.header_id
    AND TO_CHAR (oh.order_number) = rctl.interface_line_attribute1
    AND TO_CHAR (l.line_id)       = rctl.interface_line_attribute6
    AND rct.customer_trx_id       = rctl.customer_trx_id
    AND rctl.customer_trx_id      = rctg.customer_trx_id
    AND rctl.customer_trx_line_id = rctg.customer_trx_line_id
    AND rctg.code_combination_id  = gcc.code_combination_id
    ) segment_number ,
    oh.header_id header_id ,
    0 cash_receipt_id ,
    otl.name order_type ,
    TRUNC (oh.last_update_date) last_update_date ,
    TRUNC (ol.last_update_date) line_last_update_date ,
    tmp.on_account on_account
  FROM oe_order_headers oh ,
    oe_order_lines ol ,
    oe_payments oe ,
    xxeis.eis_xxwc_cash_exce_tmp_tbl tmp ,
    ar_receipt_methods arc ,
    oe_transaction_types_vl otl ,
    ar_receivable_applications_all rap ,
    iby_trxn_extensions_v ite ,
    oe_lookups olkp ,
    per_all_people_f ppf ,
    fnd_user fu ,
    hz_parties hzp ,
    hz_cust_accounts hca ,
    org_organization_definitions ood ,
    ra_terms ra
  WHERE 1                  = 1
  AND oh.header_id         = ol.header_id
  AND oe.header_id(+)      = oh.header_id
  AND oe.receipt_method_id = arc.receipt_method_id(+)
  AND oh.order_type_id     = otl.transaction_type_id
  AND oh.payment_term_id   = ra.term_id
  AND ( arc.name          IS NULL
  OR (upper (arc.name) NOT LIKE upper ('%GIFT%')
  OR upper (arc.name) NOT LIKE upper ('%REBATE%')))
  AND rap.payment_set_id(+) = oe.payment_set_id
  AND rap.status           <> 'UNAPP' --Mahi
  AND oe.payment_set_id     = tmp.payment_set_id(+)
  AND oe.payment_type_code  = olkp.lookup_code(+)
  AND oe.trxn_extension_id  = ite.trxn_extension_id(+)
  AND fu.user_id            = oh.created_by
  AND fu.employee_id        = ppf.person_id(+)
  AND TRUNC (oh.ordered_date) BETWEEN TRUNC (NVL (ppf.effective_start_date, oh.ordered_date)) AND TRUNC (NVL (ppf.effective_end_date, oh.ordered_date))
  AND oh.sold_to_org_id              = hca.cust_account_id
  AND hzp.party_id                   = hca.party_id
  AND oh.ship_from_org_id            = ood.organization_id
  AND oe.payment_collection_event(+) = 'PREPAY'
  AND oh.flow_status_code           IN ('BOOKED', 'CLOSED', 'CANCELLED')
  AND olkp.lookup_type(+)            = 'PAYMENT TYPE'
  AND otl.name                      <> 'INTERNAL ORDER'
  AND (payment_number               IS NOT NULL
  OR ra.name                        IN ('COD', 'PRFRDCASH', 'IMMEDIATE'))
  AND NOT EXISTS
    (SELECT 1
    FROM hz_customer_profiles hcp,
      hz_cust_profile_classes hcpc
    WHERE hca.party_id       = hcp.party_id
    AND hca.cust_account_id  = hcp.cust_account_id
    AND hcp.site_use_id     IS NULL
    AND hcp.profile_class_id = hcpc.profile_class_id(+)
    AND hcpc.name LIKE 'WC%Branches%'
    )
  AND ( otl.name IN ('RETURN ORDER')
  AND NOT EXISTS
    (SELECT 1
    FROM xxwc_om_cash_refund_tbl xoc
    WHERE xoc.return_header_id = oh.header_id
    ))
  --End of return order union query
  UNION
  SELECT oh.order_number ,
    oh.org_id ,
    CASE
      WHEN (upper (arc.name) LIKE upper ('%AMX%')
      OR upper (arc.name) LIKE upper ('%VMD%'))
      THEN 'Credit Card'
      ELSE DECODE (olkp.meaning, 'Check', 'Cash', olkp.meaning)
    END payment_type_code ,
    olkp.meaning payment_type_code_new ,
    ppf.last_name taken_by ,
    CASE
      WHEN otl.name = 'RETURN ORDER'
      THEN 'CASH RETURN'
      WHEN xmcr.payment_type_code IN ('CASH', 'CHECK', 'CREDIT_CARD')
      AND NOT EXISTS
        (SELECT ordered_item
        FROM oe_order_lines ol
        WHERE ol.header_id = oh.header_id
        AND ordered_item LIKE '%DEPOSIT%'
        )
        /*AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )*/
      THEN 'CASH SALES'
      WHEN xmcr.payment_type_code IN ('CASH', 'CHECK', 'CREDIT_CARD')
      AND EXISTS
        (SELECT ordered_item
        FROM oe_order_lines ol
        WHERE ol.header_id = oh.header_id
        AND ordered_item LIKE '%DEPOSIT%'
        )
        /*AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )*/
      THEN 'DEPOSIT'
    END cash_type , -- 'CASH RETURN' CASH_TYPE,
    -- DECODE(XMCR.PAYMENT_TYPE_CODE,'CHECK',OE.CHECK_NUMBER,'CREDIT_CARD',SUBSTR(ITE.CARD_NUMBER,-4)) CHK_CARDNO,
    CASE
        /*WHEN XMCR.PAYMENT_TYPE_CODE ='CREDIT_CARD'
        THEN
        (SELECT MAX(SUBSTR(ITE.CARD_NUMBER,-4))
        FROM IBY_TRXN_EXTENSIONS_V ITE,
        oe_payments oe
        WHERE oe.header_id       = xmcr.header_id
        AND OE.TRXN_EXTENSION_ID =ITE.TRXN_EXTENSION_ID
        )*/
      WHEN (upper (arc.name) LIKE upper ('%AMX%')
      OR upper (arc.name) LIKE upper ('%VMD%'))
      THEN
        (SELECT (oe.check_number)
        FROM oe_payments oe
        WHERE oe.header_id       = op.header_id
        AND receipt_method_id    = op.receipt_method_id
        AND oe.payment_type_code = 'CHECK'
        )
      ELSE NULL
    END chk_cardno , --SUBSTR(ITE.CARD_NUMBER,                                                                    -4) CARD_NUMBER,
    NULL card_number ,
    NULL card_issuer_code , --ITE.CARD_ISSUER_CODE,
    CASE
      WHEN xmcr.payment_type_code = 'CREDIT_CARD'
      THEN
        (SELECT MAX (ite.card_issuer_name)
        FROM iby_trxn_extensions_v ite,
          oe_payments oe
        WHERE oe.header_id       = xmcr.header_id
        AND oe.trxn_extension_id = ite.trxn_extension_id
        )
      ELSE NULL
    END card_issuer_name , --ITE.CARD_ISSUER_NAME,
    -- NVL(xmcr.refund_amount*-1,0) cash_amount,
    --    CASE
    --      WHEN TRUNC(XMCR.CREATION_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
    --      THEN NVL(XMCR.REFUND_AMOUNT*-1,0)
    --      ELSE 0
    --    END cash_amount,
    CASE
      WHEN ( (NVL (TRUNC (xmcr.refund_date), TRUNC (xmcr.creation_date)) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date)
      OR (TRUNC (oh.ordered_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date)
      OR (TRUNC (ol.last_update_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date))
        --TRUNC(XMCR.CREATION_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
      THEN
        --NVL(XMCR.REFUND_AMOUNT,0)
        (
        SELECT NVL (ROUND (SUM (ol.prepaid_amount), 2), 0)
        FROM oe_payments ol
        WHERE ol.header_id       = oh.header_id
        AND ol.receipt_method_id = op.receipt_method_id
        )
      ELSE
        (SELECT NVL ( ROUND ( SUM ( (ROUND (ol.ordered_quantity * ol.unit_selling_price, 2) + ol.tax_value) * DECODE (otl.order_category_code, 'RETURN', -1, 1)) ,2) ,0) order_amount
        FROM oe_order_lines ol,
          oe_transaction_types_vl otl
        WHERE ol.header_id  = oh.header_id
        AND ol.line_type_id = otl.transaction_type_id
        )
    END cash_amount ,
    hca.account_number customer_number ,
    hzp.party_name customer_name ,
    ood.organization_code branch_number ,
    NULL check_number , --OE.CHECK_NUMBER,
    -- TRUNC(OH.ORDERED_DATE) CASH_DATE,
    oh.ordered_date order_date , --  NVL(XMCR.refund_date,XMCR.creation_date) cash_date,
    acr.last_update_date cash_date ,
    NULL payment_channel_name , --ITE.PAYMENT_CHANNEL_NAME,
    (SELECT name
    FROM ar_receipt_methods
    WHERE receipt_method_id = op.receipt_method_id
    ) name , --  ARC.NAME NAME,             --Arc.Name,
    -- NULL PAYMENT_NUMBER,
    1 payment_number ,
    (SELECT NVL (ROUND (SUM (ol.prepaid_amount), 2), 0)
    FROM oe_payments ol
    WHERE ol.header_id = oh.header_id
    ) payment_amount ,
    (
    CASE
      WHEN op.payment_number = 1
      THEN
        (SELECT ROUND ( SUM ( ( (ol.ordered_quantity * ol.unit_selling_price) + ol.tax_value) * DECODE (ol.line_category_code, 'RETURN', -1, 1)) ,2) order_amount
          -- INTO l_order_amount
        FROM oe_order_lines ol
        WHERE ol.header_id = oh.header_id
        )
      ELSE 0
    END) order_amount ,
    (SELECT ROUND ( SUM ( ( (ol.ordered_quantity * ol.unit_selling_price) + ol.tax_value) * DECODE (ol.line_category_code, 'RETURN', -1, 1)) ,2) order_amount
      -- INTO l_order_amount
    FROM oe_order_lines ol
    WHERE ol.header_id = oh.header_id
    ) total_order_amount , --  xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_cash_drawer_amt(oh.header_id) ORDER_AMOUNT,
    --  xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_cash_drawer_amt(oh.header_id) TOTAL_ORDER_AMOUNT,
    hzp.party_id ,
    hca.cust_account_id ,
    (SELECT MAX (rct.trx_number)
    FROM ra_customer_trx rct,
      oe_order_headers h
    WHERE h.header_id            = xmcr.header_id
    AND TO_CHAR (h.order_number) = rct.interface_header_attribute1
    ) invoice_number ,
    /*   (SELECT MAX(RCT.TRX_NUMBER)
    FROM RA_CUSTOMER_TRX RCT,
    Oe_Order_Headers H
    WHERE H.HEADER_ID           =XMCR.RETURN_HEADER_ID
    And To_Char(H.Order_Number) =Rct.Interface_Header_Attribute1
    )CREDIT_NUMBER,*/
    (
    SELECT MAX (rct.trx_date)
    FROM ra_customer_trx rct,
      oe_order_headers h1
    WHERE TO_CHAR (h1.order_number) = rct.interface_header_attribute1
    AND h1.header_id                = xmcr.header_id
    ) invoice_date ,
    (SELECT MAX (gcc.segment2)
    FROM oe_order_lines l ,
      oe_order_headers h2 ,
      ra_customer_trx_lines rctl ,
      ra_customer_trx rct ,
      ra_cust_trx_line_gl_dist rctg ,
      gl_code_combinations_kfv gcc
    WHERE h2.header_id            = xmcr.header_id
    AND h2.header_id              = l.header_id
    AND TO_CHAR (h2.order_number) = rct.interface_header_attribute1
    AND TO_CHAR (l.line_id)       = rctl.interface_line_attribute6
    AND rct.customer_trx_id       = rctl.customer_trx_id
    AND rctl.customer_trx_id      = rctg.customer_trx_id
    AND rctl.customer_trx_line_id = rctg.customer_trx_line_id
    AND rctg.code_combination_id  = gcc.code_combination_id
    ) segment_number ,
    oh.header_id header_id ,
    xmcr.cash_receipt_id cash_receipt_id ,
    otl.name order_type ,
    TRUNC (oh.last_update_date) last_update_date ,
    TRUNC (ol.last_update_date) line_last_update_date ,
    tmp.on_account on_account
  FROM oe_order_headers oh
    --         ,oe_order_headers ol
    ,
    oe_order_lines ol ,
    oe_lookups olkp ,
    hz_parties hzp ,
    hz_cust_accounts hca ,
    org_organization_definitions ood ,
    xxwc_om_cash_refund_tbl xmcr ,
    ar_receipt_methods arc ,
    ar_cash_receipts acr ,
    per_all_people_f ppf ,
    fnd_user fu ,
    oe_transaction_types_vl otl ,
    oe_payments op ,
    xxeis.eis_xxwc_cash_exce_tmp_tbl tmp
  WHERE oh.header_id        = ol.header_id
  AND oh.sold_to_org_id     = hca.cust_account_id
  AND hzp.party_id          = hca.party_id
  AND oh.ship_from_org_id   = ood.organization_id
  AND xmcr.return_header_id = oh.header_id
  AND xmcr.cash_receipt_id  = acr.cash_receipt_id(+)
  AND acr.receipt_method_id = arc.receipt_method_id(+)
  AND fu.user_id            = oh.created_by
  AND op.header_id(+)       = xmcr.return_header_id
  AND op.payment_set_id     = tmp.payment_set_id(+)
    --AND OP.receipt_method_id = arc.receipt_method_id
  AND otl.name NOT  IN ('COUNTER ORDER', 'STANDARD ORDER')
  AND fu.employee_id = ppf.person_id(+)
  AND TRUNC (oh.ordered_date) BETWEEN NVL (TRUNC (ppf.effective_start_date), TRUNC (oh.ordered_date)) AND NVL (TRUNC (ppf.effective_end_date), TRUNC (oh.ordered_date))
  AND oh.order_type_id = otl.transaction_type_id
  AND ( (NVL (TRUNC (xmcr.refund_date), TRUNC (xmcr.creation_date)) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date)
  OR (TRUNC (oh.ordered_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date)
  OR (TRUNC (ol.last_update_date) BETWEEN xxeis.eis_rs_xxwc_com_util_pkg.get_cash_from_date AND xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date))
    --AND(NVL(TRUNC(XMCR.refund_date),TRUNC(XMCR.creation_date)) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)
    -- AND XMCR.PAYMENT_TYPE_CODE <> 'CHECK'
  AND NVL (xmcr.refund_amount, 0) <> 0
  AND xmcr.payment_type_code       = olkp.lookup_code
  AND olkp.lookup_type             = 'PAYMENT TYPE'
  AND otl.name                    <> 'INTERNAL ORDER'
/
	