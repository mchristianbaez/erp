CREATE OR REPLACE PACKAGE BODY APPS."XXWCAR_DCTM_PKG"
IS
   /**************************************************************************
   File Name: XXWCAR_DCTM_PKG

   PROGRAM TYPE: SQL Script

   PURPOSE:      package Specification is used to create a file for open ar
                 transactions that will be loaded by Documentum

   HISTORY
   =============================================================================
          Last Update Date : 11/23/2011
   =============================================================================
   =============================================================================
   VERSION DATE          AUTHOR(S)          DESCRIPTION
   ------- -----------   -----------------  ------------------------------------
   1.0     23-Nov-2011   Kathy Poling       Initial creation of the procedure
   1.1     28-May-2012   Kathy Poling       Added record count and dms_output
                                            for being displayed in UC4
   1.2     06-Jun-2012   Kathy Poling       Changed query for conversion
                                            to use getpaid data dump to get the
                                            tax and freight.
   1.3     23-Aug-2012   Kathy Poling       Changed the transaction to not parse
                                            the subnumber and subnumber will be null.
                                            Changed the date format to mm/dd/yyyy
                                            requested by Madhu.  SR 168677
   1.4     19-Sep-2012   Kathy Poling       RFC# 35126  Service# 177461 change to
                                            format for Payment Enchancement Project
   1.5     10-Mar-2014   Maharajan          TMS# 20130908-00094
                 Shunmugam          Replaced AR_RECEIVABLE_APPLICATIONS with
                        AR_PAYMENT_SCHEDULES_ALL for short code
   2.0     23-Jul-2014   Maharajan
             Shunmugam          TMS# 20140723-00171 New parameter'Apply unearn discounts' handling in process lockbox
   3.0     30-Jul-2014   Maharajan
             Shunmugam          TMS# 20140731-00049 New parameter'No of instances' handling in process lockbox
   3.1     05-Jan-2014    Kathy Poling      TMS 20141212-00024 fix org_id for procedure dctm_lockbox 
                                            and added file name to notification in uc4_load_dctm
   3.2     03/Jul/2015    M Hari Prasad       TMS# 20150702-00029  increase variable l_sec size
                                             to 3000 within the package XXWCAR_DCTM_PKG.uc4_submit_inv_short_code										
   =============================================================================
   *****************************************************************************/
   l_distro_list   VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_err_callfrom  VARCHAR2(75) DEFAULT 'XXWCAR_DCTM_PKG';

   PROCEDURE open_trns (errbuf    OUT VARCHAR2,
                        retcode   OUT NUMBER,
                        p_count   OUT NUMBER)
   IS
      l_file_name            VARCHAR2 (100);
      l_file_dir             VARCHAR2 (100);
      l_file_handle          UTL_FILE.file_type;
      l_file_name_temp       VARCHAR2 (100);
      l_file_name_archive    VARCHAR2 (100);
      l_file_log             VARCHAR2 (100);
      l_sid                  VARCHAR2 (8);
      l_req_id               NUMBER := fnd_global.conc_request_id;

      v_set_req_status       BOOLEAN;
      v_req_phase            VARCHAR2 (80);
      v_req_status           VARCHAR2 (80);
      v_dev_phase            VARCHAR2 (30);
      v_dev_status           VARCHAR2 (30);
      v_message              VARCHAR2 (255);
      v_interval             NUMBER := 10;                       -- In seconds
      v_max_time             NUMBER := 15000;                    -- In seconds
      v_completion_status    VARCHAR2 (30) := 'NORMAL';
      v_error_message        VARCHAR2 (3000);
      v_file_dir             VARCHAR2 (100);
      v_instance             VARCHAR2 (8);
      v_shortname            VARCHAR2 (30);
      v_count                NUMBER := 0;

      l_can_submit_request   BOOLEAN := TRUE;
      l_globalset            VARCHAR2 (100);
      l_err_msg              VARCHAR2 (3000);
      l_err_code             NUMBER;
      l_sec                  VARCHAR2 (255);
      l_message              VARCHAR2 (150);
      l_statement            VARCHAR2 (9000);
      l_org         CONSTANT hr_all_organization_units.organization_id%TYPE
                                := 162 ;                 --HDS White Cap - Org
      l_err_callfrom         VARCHAR2 (75) DEFAULT 'XXWCAR_DCTM_PKG';
      l_err_callpoint        VARCHAR2 (75) DEFAULT 'START';
      --l_distro_list        VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_procedure_name       VARCHAR2 (75) := 'XXWCAR_DCTM_PKG.OPEN_TRNS';
   BEGIN
      l_sec := 'Starting the AR Transactions file creation; ';
      fnd_file.put_line (fnd_file.LOG, l_sec);
      fnd_file.put_line (fnd_file.output, l_sec);
      DBMS_OUTPUT.put_line (l_sec);
      --Get Database
      -- SELECT lower(NAME) INTO l_sid FROM v$database;

      l_file_dir := 'XXWCAR_UC4_ECM_OB_DIR';
      --Set the file name and open the file
      l_file_name_temp := 'TEMP_' || 'WCC_OPENAR_DATA';

      l_file_name := 'WCC_OPENAR_DATA';

      l_file_name_archive :=
            'WCC_OPENAR_DATA_'
         || TO_CHAR (SYSDATE, 'YYYYMMDD_HH24MISS')
         || '.txt';

      BEGIN
         UTL_FILE.frename (l_file_dir,
                           l_file_name,
                           l_file_dir,
                           l_file_name_archive);
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Exception...'
               || l_file_name
               || ' file does not exist to rename/archive...');
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'Filename generated : ' || l_file_name);
      fnd_file.put_line (fnd_file.output,
                         'Filename generated : ' || l_file_name);
      DBMS_OUTPUT.put_line ('Filename generated : ' || l_file_name);
      l_file_handle := UTL_FILE.fopen (l_file_dir, l_file_name_temp, 'w');
      DBMS_OUTPUT.put_line (l_file_name_temp);

      FOR c_trns
         IN /*(SELECT ca.account_number
                                                                                                                             ,t.customer_trx_id
                                                                                                                             ,regexp_substr(t.trx_number, '[^-]+', 1, 1) trx_number
                                                                                                                             ,CASE
                                                                                                                                WHEN t.trx_number LIKE '%-%' THEN
                                                                                                                                 substr(t.trx_number
                                                                                                                                       ,instr(t.trx_number, '-') + 1
                                                                                                                                       ,11)   --changed for testing extending the subnumber from 4 to 11
                                                                                                                                ELSE
                                                                                                                                 NULL
                                                                                                                              END seq_nbr
                                                                                                                             ,ps.amount_due_original
                                                                                                                             ,ps.freight_original
                                                                                                                             ,ps.tax_original
                                                                                                                             ,ps.term_id
                                                                                                                             ,ps.amount_due_remaining
                                                                                                                             ,t.trx_date
                                                                                                                             ,ps.due_date
                                                                                                                             ,xxwcar_dctm_pkg.get_disc_date(ps.term_id
                                                                                                                                                           ,t.trx_date
                                                                                                                                                           ,ps.due_date) disc_date
                                                                                                                             ,xxwcar_dctm_pkg.get_disc_amt(t.term_id
                                                                                                                                                          ,t.customer_trx_id) disc_amt
                                                                                                                         FROM ar.ra_customer_trx_all      t
                                                                                                                             ,ar.ar_payment_schedules_all ps
                                                                                                                             ,ar.hz_cust_accounts         ca
                                                                                                                        WHERE t.bill_to_customer_id = ca.cust_account_id
                                                                                                                          AND t.customer_trx_id = ps.customer_trx_id
                                                                                                                          AND t.complete_flag = 'Y'
                                                                                                                          AND t.status_trx = 'OP'
                                                                                                                          AND t.org_id = l_org
                                                                                                                          AND ps.amount_due_remaining <> 0
                                                                                                                          AND ca.account_number NOT IN
                                                                                                                              ('1000-DIS'
                                                                                                                              ,'NEW'
                                                                                                                              ,'XXWC1'
                                                                                                                              ,'XX3156'
                                                                                                                              ,'1000-TEST'
                                                                                                                              ,'WC0719'
                                                                                                                              ,'WC0711'
                                                                                                                              ,'WC0777'
                                                                                                                              ,'WC0721'
                                                                                                                              ,'WC0712'
                                                                                                                              ,'WC0710'))
                                                                                                                              Version 1.2 */

            (                          --Non Conversion Invoices   Version 1.2
             SELECT ca.account_number,
                    t.customer_trx_id,
                    t.trx_number                                 --Version 1.3
                                --,regexp_substr(t.trx_number, '[^-]+', 1, 1) trx_number
                    ,
                    CASE
                       WHEN t.trx_number LIKE '%-%'
                       THEN
                          SUBSTR (t.trx_number,
                                  INSTR (t.trx_number, '-') + 1,
                                  14)
                       ELSE
                          NULL
                    END
                       seq_nbr_new,
                    NULL seq_nbr                                 --Version 1.3
                                ,
                    ps.amount_due_original,
                    ps.freight_original,
                    ps.tax_original,
                    ps.term_id,
                    ps.amount_due_remaining,
                    t.trx_date,
                    ps.due_date,
                    xxwcar_dctm_pkg.get_disc_date (ps.term_id,
                                                   t.trx_date,
                                                   ps.due_date)
                       disc_date,
                    xxwcar_dctm_pkg.get_disc_amt (t.term_id,
                                                  t.customer_trx_id)
                       disc_amt
               FROM ar.ra_customer_trx_all t,
                    ar.ar_payment_schedules_all ps,
                    ar.ra_batch_sources_all bs,
                    ar.hz_cust_accounts ca
              WHERE     t.batch_source_id = bs.batch_source_id
                    AND t.org_id = bs.org_id
                    AND NVL (bs.name, '#####') <> 'CONVERSION'
                    AND t.complete_flag = 'Y'
                    AND t.status_trx = 'OP'
                    AND t.org_id = l_org
                    AND t.customer_trx_id = ps.customer_trx_id
                    AND ps.amount_due_remaining <> 0
                    AND t.bill_to_customer_id = ca.cust_account_id
             UNION
             --Conversion invoices to use getpaid data dump to get tax and freight
             SELECT ca.account_number,
                    t.customer_trx_id,
                    t.trx_number                                 --Version 1.3
                                --,regexp_substr(t.trx_number, '[^-]+', 1, 1) trx_number
                    ,
                    CASE
                       WHEN t.trx_number LIKE '%-%'
                       THEN
                          SUBSTR (t.trx_number,
                                  INSTR (t.trx_number, '-') + 1,
                                  14)
                       ELSE
                          NULL
                    END
                       seq_nbr_new,
                    NULL seq_nbr                                 --Version 1.3
                                ,
                    gp.invamt amount_due_original     --ps.amount_due_original
                                                 ,
                    gp.freight,
                    gp.tax,
                    ps.term_id,
                    ps.amount_due_remaining,
                    t.trx_date,
                    ps.due_date,
                    xxwcar_dctm_pkg.get_disc_date (ps.term_id,
                                                   t.trx_date,
                                                   ps.due_date)
                       disc_date,
                    xxwcar_dctm_pkg.get_disc_amt (t.term_id,
                                                  t.customer_trx_id)
                       disc_amt
               FROM ar.ra_customer_trx_all t,
                    ar.ar_payment_schedules_all ps,
                    ra_batch_sources_all bs,
                    ar.hz_cust_accounts ca,
                    xxwc.xxwc_armast_getpaid_dump_tbl gp
              WHERE     t.batch_source_id = bs.batch_source_id
                    AND t.org_id = bs.org_id
                    AND NVL (bs.name, '#####') = 'CONVERSION'
                    AND t.complete_flag = 'Y'
                    AND t.status_trx = 'OP'
                    AND t.org_id = l_org
                    AND t.customer_trx_id = ps.customer_trx_id
                    AND ps.amount_due_remaining <> 0
                    AND t.bill_to_customer_id = ca.cust_account_id
                    AND t.trx_number = gp.invno
                    AND ca.account_number = gp.custno)
      LOOP
         v_count := v_count + 1;

         --Write rows
         UTL_FILE.put_line (
            l_file_handle,
               c_trns.account_number
            || '|'
            || c_trns.trx_number
            || '|'
            || --c_trns.trx_number
               c_trns.seq_nbr_new
            || '|'
            || c_trns.seq_nbr
            || '|'
            || TRIM (TO_CHAR (c_trns.amount_due_original, 9999999990.99))
            || '|'
            || TRIM (TO_CHAR (c_trns.freight_original, 9999999990.99))
            || '|'
            || TRIM (TO_CHAR (c_trns.tax_original, 9999999990.99))
            || '|'
            || TRIM (TO_CHAR (c_trns.disc_amt, 9999999990.9999))
            || '|'
            || TRIM (TO_CHAR (c_trns.amount_due_remaining, 9999999990.99))
            || '|'
            || TO_CHAR (c_trns.trx_date, 'MM/DD/YYYY')
            || '|'
            || TO_CHAR (c_trns.disc_date, 'MM/DD/YYYY'));
      END LOOP;

      --Close the file
      l_sec := 'Closing the File; ';
      fnd_file.put_line (fnd_file.LOG, l_sec);
      fnd_file.put_line (fnd_file.output, l_sec);
      fnd_file.put_line (fnd_file.LOG, v_count);

      UTL_FILE.fclose (l_file_handle);

      UTL_FILE.frename (l_file_dir,
                        l_file_name_temp,
                        l_file_dir,
                        l_file_name);

      retcode := 0;
      p_count := v_count;

      --COMMIT;
      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('OpenAR Record Count:  ' || v_count);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_sec);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         l_err_callpoint := l_sec;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWCAR_DCTM_PKG with PROGRAM ERROR',
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (l_sec);

         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         l_err_callpoint := l_sec;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWCAR_DCTM_PKG with OTHERS Exception',
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
   END open_trns;

   /**************************************************************************
   File Name: XXWCAR_DCTM_PKG

   PROGRAM TYPE: SQL Script

   PURPOSE:      package Specification is used to create a file for ar
                 customers that will be loaded by Documentum

   HISTORY
   =============================================================================
          Last Update Date : 11/23/2011
   =============================================================================
   =============================================================================
   VERSION DATE          AUTHOR(S)          DESCRIPTION
   ------- -----------   -----------------  ------------------------------------
   1.0     29-Nov-2011   Kathy Poling       Initial creation of the procedure
   1.1     28-May-2012   Kathy Poling       Added record count and dms_output
                                            for being displayed in UC4
   =============================================================================
   *****************************************************************************/
   PROCEDURE customer (errbuf    OUT VARCHAR2,
                       retcode   OUT NUMBER,
                       p_count   OUT NUMBER)
   IS
      l_file_name            VARCHAR2 (100);
      l_file_dir             VARCHAR2 (100);
      l_file_handle          UTL_FILE.file_type;
      l_file_name_temp       VARCHAR2 (100);
      l_file_name_archive    VARCHAR2 (100);
      l_file_log             VARCHAR2 (100);
      l_sid                  VARCHAR2 (8);
      l_req_id               NUMBER := fnd_global.conc_request_id;

      v_set_req_status       BOOLEAN;
      v_req_phase            VARCHAR2 (80);
      v_req_status           VARCHAR2 (80);
      v_dev_phase            VARCHAR2 (30);
      v_dev_status           VARCHAR2 (30);
      v_message              VARCHAR2 (255);
      v_interval             NUMBER := 10;                       -- In seconds
      v_max_time             NUMBER := 15000;                    -- In seconds
      v_completion_status    VARCHAR2 (30) := 'NORMAL';
      v_error_message        VARCHAR2 (3000);
      v_file_dir             VARCHAR2 (100);
      v_instance             VARCHAR2 (8);
      v_shortname            VARCHAR2 (30);
      v_count                NUMBER := 0;

      l_can_submit_request   BOOLEAN := TRUE;
      l_globalset            VARCHAR2 (100);
      l_err_msg              VARCHAR2 (3000);
      l_err_code             NUMBER;
      l_sec                  VARCHAR2 (255);
      l_message              VARCHAR2 (150);
      l_statement            VARCHAR2 (9000);
      l_err_callfrom         VARCHAR2 (75) DEFAULT 'XXWCAR_DCTM_PKG';
      l_err_callpoint        VARCHAR2 (75) DEFAULT 'START';
      --l_distro_list        VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_procedure_name       VARCHAR2 (75) := 'XXWCAR_DCTM_PKG.CUSTOMER';
   BEGIN
      l_sec := 'Starting the AR Customer file creation; ';
      fnd_file.put_line (fnd_file.LOG, l_sec);
      fnd_file.put_line (fnd_file.output, l_sec);
      DBMS_OUTPUT.put_line (l_sec);
      --Get Database
      -- SELECT lower(NAME) INTO l_sid FROM v$database;

      l_file_dir := 'XXWCAR_UC4_ECM_OB_DIR';
      --Set the file name and open the file
      l_file_name_temp := 'TEMP_' || 'WCC_MASTER_DATA';

      l_file_name := 'WCC_MASTER_DATA';

      l_file_name_archive :=
            'WCC_MASTER_DATA_'
         || TO_CHAR (SYSDATE, 'YYYYMMDD_HH24MISS')
         || '.txt';

      BEGIN
         UTL_FILE.frename (l_file_dir,
                           l_file_name,
                           l_file_dir,
                           l_file_name_archive);
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Exception...'
               || l_file_name
               || ' file does not exist to rename/archive...');
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'Filename generated : ' || l_file_name);
      fnd_file.put_line (fnd_file.output,
                         'Filename generated : ' || l_file_name);
      DBMS_OUTPUT.put_line ('Filename generated : ' || l_file_name);
      l_file_handle := UTL_FILE.fopen (l_file_dir, l_file_name_temp, 'w');
      DBMS_OUTPUT.put_line (l_file_name_temp);

      FOR c_cust
         IN (SELECT ca.account_number, party_name
               FROM ar.hz_cust_accounts ca, ar.hz_parties p
              WHERE     p.attribute1 IS NULL
                    AND p.party_type = 'ORGANIZATION'
                    AND p.created_by_module NOT IN ('CE',
                                                    'EBTAX_SERVICE_PROVIDER',
                                                    'XLE_CREATE_LE',
                                                    'AP_SUPPLIERS_API')
                    AND ca.party_id = p.party_id
                    AND ca.account_number NOT IN ('1000-DIS',
                                                  'NEW',
                                                  'XXWC1',
                                                  'XX3156',
                                                  '1000-TEST',
                                                  'WC0719',
                                                  'WC0711',
                                                  'WC0777',
                                                  'WC0721',
                                                  'WC0712',
                                                  'WC0710',
                                                  'DISCARD-1000') --used for testing
                                                                 )
      LOOP
         v_count := v_count + 1;

         --Write rows
         UTL_FILE.put_line (
            l_file_handle,
               c_cust.account_number
            || '|'
            || SUBSTR (c_cust.party_name, 1, 40)
            || '|||||N');
      END LOOP;

      --Close the file
      l_sec := 'Closing the File; ';
      fnd_file.put_line (fnd_file.LOG, l_sec);
      fnd_file.put_line (fnd_file.output, l_sec);
      fnd_file.put_line (fnd_file.LOG, v_count);
      UTL_FILE.fclose (l_file_handle);

      UTL_FILE.frename (l_file_dir,
                        l_file_name_temp,
                        l_file_dir,
                        l_file_name);

      retcode := 0;
      p_count := v_count;

      --COMMIT;
      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Customer Record Count:  ' || v_count);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_sec);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         l_err_callpoint := l_sec;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWCAR_DCTM_PKG with PROGRAM ERROR',
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (l_sec);

         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         l_err_callpoint := l_sec;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWCAR_DCTM_PKG with OTHERS Exception',
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
   END customer;

   FUNCTION get_disc_date (p_term       IN NUMBER,
                           p_trx_date   IN DATE,
                           p_due_date   IN DATE)
      RETURN DATE
   IS
      l_disc_days         NUMBER;
      l_disc_dayofmnth    NUMBER;
      l_month             VARCHAR2 (3);
      l_disc_mnth_frwrd   NUMBER;
      l_percent           NUMBER;
      l_disc_dom          NUMBER;
      l_date              DATE;
      l_err_msg           VARCHAR2 (3000);
      l_err_code          NUMBER;
      l_sec               VARCHAR2 (255);
      l_procedure_name    VARCHAR2 (75) := 'XXWCAR_DCTM_PKG.GET_DISC_DATE';
      l_req_id            NUMBER := fnd_global.conc_request_id;
   BEGIN
      BEGIN
         SELECT NVL (discount_percent, -1),
                NVL (discount_days, -1),
                NVL (discount_day_of_month, -1),
                NVL (discount_months_forward, -1)
           INTO l_percent,
                l_disc_days,
                l_disc_dayofmnth,
                l_disc_mnth_frwrd
           FROM ar.ra_terms_tl t,
                ar.ra_terms_lines tl,
                ar.ra_terms_lines_discounts ld
          WHERE     t.term_id = tl.term_id
                AND t.term_id = ld.term_id(+)
                AND t.term_id = p_term;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_date := p_trx_date;
            RETURN l_date;
      END;

      l_sec := 'Working trx date:  ' || p_trx_date || '  Term ID:  ' || p_term;

      IF l_percent <> -1 AND l_disc_days <> -1
      THEN
         SELECT p_trx_date + l_disc_days INTO l_date FROM DUAL;

         RETURN l_date;
      ELSIF l_percent <> -1 AND l_disc_dayofmnth <> -1
      THEN
         IF     TO_CHAR (ADD_MONTHS (p_trx_date, l_disc_mnth_frwrd), 'MON') =
                   'FEB'
            AND l_disc_dayofmnth IN (29, 30, 31)
         THEN
            l_disc_dom := 28;

            SELECT TO_DATE (
                         l_disc_dom
                      || '-'
                      || TO_CHAR (ADD_MONTHS (p_trx_date, l_disc_mnth_frwrd),
                                  'MON-RR'),
                      'DD-MON-RRRR')
              INTO l_date
              FROM DUAL;

            RETURN l_date;
         END IF;

         IF     TO_CHAR (ADD_MONTHS (p_trx_date, l_disc_mnth_frwrd), 'MON') =
                   'FEB'
            AND l_disc_dayofmnth NOT IN (29, 30, 31)
         THEN
            SELECT TO_DATE (
                         l_disc_dayofmnth
                      || '-'
                      || TO_CHAR (ADD_MONTHS (p_trx_date, l_disc_mnth_frwrd),
                                  'MON-RR'),
                      'DD-MON-RRRR')
              INTO l_date
              FROM DUAL;

            RETURN l_date;
         END IF;

         IF TO_CHAR (ADD_MONTHS (p_trx_date, l_disc_mnth_frwrd), 'MON') <>
               'FEB'
         THEN
            SELECT TO_DATE (
                         l_disc_dayofmnth
                      || '-'
                      || TO_CHAR (ADD_MONTHS (p_trx_date, l_disc_mnth_frwrd),
                                  'MON-RR'),
                      'DD-MON-RRRR')
              INTO l_date
              FROM DUAL;

            RETURN l_date;
         END IF;
      ELSE
         SELECT p_due_date INTO l_date FROM DUAL;

         RETURN l_date;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (
            fnd_file.output,
               'Can not get discount date for:  '
            || p_trx_date
            || '  Term ID:  '
            || p_term);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_procedure_name,
            p_calling             => 'Exception for xxwcar_dctm_pkg',
            p_request_id          => l_req_id,
            p_ora_error_msg       => 900045,
            p_error_desc          => SQLERRM,
            p_distribution_list   => l_distro_list,
            p_module              => 'XXCUS');
   END get_disc_date;

   /**************************************************************************
   VERSION DATE          AUTHOR(S)          DESCRIPTION
   ------- -----------   -----------------  ------------------------------------
   1.0     10-Mar-2012   Kathy Poling       Initial creation of the function
   1.1     5-Feb-2013   Kathy Poling       SR 190095 removing freight
   =============================================================================
   *****************************************************************************/

   FUNCTION get_disc_amt (p_term IN NUMBER, p_trx_id IN NUMBER)
      RETURN NUMBER
   IS
      l_disc             NUMBER;
      l_disc_amt         NUMBER;
      l_err_msg          VARCHAR2 (3000);
      l_err_code         NUMBER;
      l_sec              VARCHAR2 (255);
      l_procedure_name   VARCHAR2 (75) := 'XXWCAR_DCTM_PKG.GET_DISC_AMT';
      l_req_id           NUMBER := fnd_global.conc_request_id;
   BEGIN
      BEGIN
         SELECT discount_percent
           INTO l_disc
           FROM ar.ra_terms_lines_discounts
          WHERE term_id = p_term;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_disc := 0;
      END;

      IF l_disc <> 0
      THEN
         SELECT SUM (amount_due_original   -- - freight_original   Version 1.1
                                        - tax_original) * l_disc / 100
           INTO l_disc_amt
           FROM ar.ar_payment_schedules_all
          WHERE customer_trx_id = p_trx_id;

         RETURN l_disc_amt;
      ELSE
         RETURN 0;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (
            fnd_file.output,
               'Can not get discount amount for:  '
            || p_trx_id
            || '  Term ID:  '
            || p_term);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_procedure_name,
            p_calling             => 'Exception for xxwcar_dctm_pkg',
            p_request_id          => l_req_id,
            p_ora_error_msg       => 900045,
            p_error_desc          => SQLERRM,
            p_distribution_list   => l_distro_list,
            p_module              => 'XXCUS');
   END get_disc_amt;

   /*******************************************************************************
   * Procedure:   UC4_RECEIPTS
   * Description: This is for UC4 to start the concurrent request to load Receipts
   *
   *
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     01/05/2012    Kathy Poling    Initial creation of the procedure
   1.1     06/13/2012    Kathy Poling    Commented out dms_output for returning file
                                         names.
   1.2     09/14/2012    Kathy Poling    Changed the archive nameing to help with the
                                         purging of files.
   1.3     03/13/2013    Kathy Poling    SR 190095 adding instance name to notification
   3.1     01/13/2015    Kathy Poling    TMS 20141212-00024 added file name to notification
   ********************************************************************************/

   PROCEDURE uc4_load_dctm (errbuf                OUT VARCHAR2,
                            retcode               OUT NUMBER,
                            p_directory        IN     VARCHAR2,
                            p_user             IN     VARCHAR2,
                            p_responsibility   IN     VARCHAR2)
   IS
      --
      -- Package Variables
      --
      -- Email Defaults
      l_dflt_email           fnd_user.email_address%TYPE
                                := 'HDSOracleDevelopers@hdsupply.com';
      l_sender               VARCHAR2 (100);
      l_host                 VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
      l_hostport             VARCHAR2 (20) := '25';
      l_sid                  VARCHAR2 (8);
      l_subject              VARCHAR2 (32767) DEFAULT NULL;
      l_body                 VARCHAR2 (32767) DEFAULT NULL;
      l_body_header          VARCHAR2 (32767) DEFAULT NULL;
      l_body_detail          VARCHAR2 (32767) DEFAULT NULL;
      l_body_footer          VARCHAR2 (32767) DEFAULT NULL;
      l_package              VARCHAR2 (50) := 'XXCUSAR_DCTM_PKG.UC4_LOCKBOX';
      l_email                fnd_user.email_address%TYPE;

      l_req_id               NUMBER NULL;
      v_phase                VARCHAR2 (50);
      v_status               VARCHAR2 (50);
      v_dev_status           VARCHAR2 (50);
      v_dev_phase            VARCHAR2 (50);
      v_message              VARCHAR2 (250);
      v_error_message        VARCHAR2 (3000);
      v_supplier_id          NUMBER;
      v_rec_cnt              NUMBER := 0;
      l_message              VARCHAR2 (150);
      l_errormessage         VARCHAR2 (3000);
      pl_errorstatus         NUMBER;
      l_can_submit_request   BOOLEAN := TRUE;
      l_globalset            VARCHAR2 (100);
      l_err_msg              VARCHAR2 (3000);
      l_err_code             NUMBER;
      l_sec                  VARCHAR2 (255);
      l_statement            VARCHAR2 (9000);
      l_user                 fnd_user.user_id%TYPE;
      l_directory            VARCHAR2 (100);
      l_file_name            VARCHAR2 (100);
      l_file_dir             VARCHAR2 (100);
      l_file_handle          UTL_FILE.file_type;
      l_file_name_archive    VARCHAR2 (100);

      -- Error DEBUG
      l_err_callfrom         VARCHAR2 (75) DEFAULT 'XXCUSAR_DCTM_PKG';
      l_err_callpoint        VARCHAR2 (75) DEFAULT 'START';
      l_distro_list          VARCHAR2 (75)
                                DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      SELECT LOWER (NAME) INTO l_sid FROM v$database;           -- Version 1.3

      --l_directory := '/xx_iface/'||l_sid||'/inbound/uc4/ar/dctm/';
      
      --Database Sender
      l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';  --Version 3.1 

      SELECT user_id
        INTO l_user
        FROM fnd_user
       WHERE user_name = UPPER (p_user);

      l_file_dir := 'XXWCAR_DCTM_RECPTS_IB_DIR';

      --  Setup parameters for running FND JOBS!
      l_can_submit_request :=
         xxcus_misc_pkg.set_responsibility (UPPER (p_user), p_responsibility);

      IF l_can_submit_request
      THEN
         l_globalset := 'Global Variables are set.';
      ELSE
         l_globalset := 'Global Variables are not set.';
         l_sec := 'Global Variables are not set for the XXCUS INTERFACE.';
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         RAISE PROGRAM_ERROR;
      END IF;

      BEGIN
         SELECT COUNT (*)
           INTO v_rec_cnt
           FROM xxwc.xxwcar_dctm_ib_files_tbl
          WHERE file_name LIKE 'DCTM_%';

         IF v_rec_cnt > 0
         THEN
            BEGIN
               FOR c_file IN (SELECT file_name
                                FROM xxwc.xxwcar_dctm_ib_files_tbl
                               WHERE file_name LIKE 'DCTM_%')
               LOOP
                  l_sec :=
                     'UC4 call to run concurrent request WC SQL*Loader-WC Lockbox file from DCTM for file ' || c_file.file_name;

                  l_req_id :=
                     fnd_request.submit_request (
                        'XXWC',
                        'XXWC_AR_LOCKBX_DCTM' --WC SQL*Loader-WC Lockbox file from DCTM
                                             ,
                        NULL,
                        SYSDATE,
                        FALSE,
                        p_directory || c_file.file_name);
                  COMMIT;

                  IF (l_req_id != 0)
                  THEN
                     IF fnd_concurrent.wait_for_request (l_req_id,
                                                         6,
                                                         0,
                                                         v_phase,
                                                         v_status,
                                                         v_dev_phase,
                                                         v_dev_status,
                                                         v_message)
                     THEN
                        v_error_message :=
                              CHR (10)
                           || 'ReqID='
                           || l_req_id
                           || ' DPhase '
                           || v_dev_phase
                           || ' DStatus '
                           || v_dev_status
                           || CHR (10)
                           || ' MSG - '
                           || v_message;

                        --IF v_dev_phase = 'COMPLETE' AND v_dev_status = 'NORMAL'  --Version 1.3
                        IF    v_dev_phase != 'COMPLETE'
                           OR v_dev_status != 'NORMAL'
                        THEN
                           /*  l_file_name := c_file.file_name;

                             l_file_name_archive := 'LOADED_' || c_file.file_name;

                             fnd_file.put_line(fnd_file.log, l_file_name_archive);
                             --Version 1.1
                             --dbms_output.put_line('Archive file name:  ' ||
                             --                     l_file_name_archive);

                             BEGIN
                               utl_file.frename(l_file_dir
                                               ,l_file_name
                                               ,l_file_dir
                                               ,l_file_name_archive);
                             EXCEPTION
                               WHEN OTHERS THEN
                                 fnd_file.put_line(fnd_file.log
                                                  ,'Exception...' || l_file_name ||
                                                   ' file does not exist to rename/archive...');
                             END;
                             l_err_code := 0;

                           ELSE
                             l_err_code := 2;  */

                           l_sec :=
                                 'An error/warning occured in the SQL LOADER for DCTM Receipts, please review the Log for concurrent request '
                              || l_req_id
                              || ' - '
                              || v_error_message
                              || '.';
                           fnd_file.put_line (fnd_file.LOG, l_sec);
                           fnd_file.put_line (fnd_file.output, l_sec);
                           --RAISE program_error;

                           l_subject :=
                                 l_sid
                              || ' *** DCTM SQLLDR for receipts Error/Warning for lockbox processing ***'; -- Version 1.3
                           l_body_header :=
                              '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';

                           l_body_detail :=
                                 l_body_detail
                              || '<BR>DCTM SQLLDR for receipts - concurrent request:  '
                              || l_req_id || ' Processing file ' || c_file.file_name;
                           l_body_footer := l_body_footer;

                           l_body :=
                              l_body_header || l_body_detail || l_body_footer;
                           xxcus_misc_pkg.html_email (
                              p_to              => l_dflt_email,
                              p_from            => l_sender,
                              p_text            => 'test',
                              p_subject         => l_subject,
                              p_html            => l_body,
                              p_smtp_hostname   => l_host,
                              p_smtp_portnum    => l_hostport);
                        END IF;
                     --Version 1.3
                     ELSE
                        l_sec :=
                              'Concurrent program wait timed out on file "'
                           || c_file.file_name;
                        DBMS_OUTPUT.put_line (l_sec);
                     END IF;
                  ELSE
                     --Version 1.3
                     /* l_sec := 'An error/warning occured in the SQL LOADER for DCTM Receipts, please review the Log for concurrent request ' ||
                              l_req_id || ' - ' || v_error_message || '.';
                     fnd_file.put_line(fnd_file.log, l_sec);
                     fnd_file.put_line(fnd_file.output, l_sec); */
                     --RAISE program_error;
                     l_sec :=
                           'Concurrent Request not initated for DCTM file "'
                        || c_file.file_name
                        || ' - '
                        || v_error_message
                        || '.';

                     l_subject :=
                        '*** DCTM SQLLDR for receipts Error/Warning for lockbox processing ***';
                     l_body_header :=
                        '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';

                     l_body_detail :=
                           l_body_detail
                        || '<BR>DCTM SQLLDR for receipts - file:  '
                        || c_file.file_name;
                     l_body_footer := l_body_footer;

                     l_body := l_body_header || l_body_detail || l_body_footer;
                     xxcus_misc_pkg.html_email (
                        p_to              => l_dflt_email,
                        p_from            => l_sender,
                        p_text            => 'test',
                        p_subject         => l_subject,
                        p_html            => l_body,
                        p_smtp_hostname   => l_host,
                        p_smtp_portnum    => l_hostport);
                  END IF;

                  --Version 1.3
                  l_file_name := c_file.file_name;

                  l_file_name_archive := 'LOADED_' || c_file.file_name;
                  fnd_file.put_line (fnd_file.LOG, l_file_name_archive);

                  BEGIN
                     UTL_FILE.frename (l_file_dir,
                                       l_file_name,
                                       l_file_dir,
                                       l_file_name_archive);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG,
                              'Exception...'
                           || l_file_name
                           || ' file does not exist to rename/archive...');
                  END;
               END LOOP;
            END;
         END IF;
      END;

      retcode := 0;
      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := l_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXCUSAR_DCTM_PKG.DCTM_LOCKBOX package with PROGRAM ERROR',
            p_distribution_list   => l_distro_list,
            p_module              => 'TM');

         fnd_file.put_line (fnd_file.output, 'Fix the error!');
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := l_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXCUSAR_DCTM_PKG.DCTM_LOCKBOX package with OTHERS Exception',
            p_distribution_list   => l_distro_list,
            p_module              => 'TM');
   END uc4_load_dctm;

   /**************************************************************************
   File Name: XXWCAR_DCTM_PKG

   PROGRAM TYPE: SQL Script

   PURPOSE:      package is used to create a file for lockbox being created
                 from the file being received from DCTM.  Once file is generated
                 the LockBox Interface will be called to process in Oracle.  Last
                 step will be to load the short code from Documentum
   HISTORY
   =============================================================================
          Last Update Date : 12/17/2011
   =============================================================================
   =============================================================================
   VERSION DATE          AUTHOR(S)          DESCRIPTION
   ------- -----------   -----------------  ------------------------------------
   1.0     07-Dec-2011   Kathy Poling       Initial creation of the procedure
   1.2     12-Mar-2012   Kathy Poling       DCTM files need to be batched
                                            created procedure dctm_lockbox to handle
   1.3     01-Jun-2012   Kathy Poling       Change made for creating the file for
                                            lockbox to process to handle multiple
                                            deposit dates.
   1.4     04-Jun-2012   Kathy Poling       Change made for creating the file for
                                            lockbox to handle checks with the same
                                            check number so it won't lose the
                                            payment method.
   1.5     14-Aug-2012   Kathy Poling       SR 160402 check number with same deposit
                                            date within the same contol with different
                                            check amount needs to be handled as a
                                            different receipt
   2.0     23-Jul-2014   Maharajan
            Shunmugam          TMS# 20140723-00171 New parameter'Apply unearn discounts' handling in process lockbox
   3.0     30-Jul-2014   Maharajan
            Shunmugam          TMS# 20140731-00049 New parameter'No of instances' handling in process lockbox
   3.1     05-Jan-2014    Kathy Poling      TMS 20141212-00024 fix org_id for procedure dctm_lockbox
 =============================================================================
   *****************************************************************************/
   PROCEDURE dctm_lockbox (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      l_file_name              VARCHAR2 (100);
      l_file_dir               VARCHAR2 (100);
      l_file_handle            UTL_FILE.file_type;
      l_file_name_temp         VARCHAR2 (100);
      l_file_name_archive      VARCHAR2 (100);
      l_file_log               VARCHAR2 (100);
      l_sid                    VARCHAR2 (8);
      l_trans_string           VARCHAR2 (1000);
      l_header_string          VARCHAR2 (5000);
      l_pmt_string             VARCHAR2 (10000);
      l_ovrflw_string          VARCHAR2 (10000);
      l_batch_trailer_string   VARCHAR2 (1000);
      l_lckbx_trailer_string   VARCHAR2 (1000);
      l_trans_trailer_string   VARCHAR2 (1000);
      l_control                VARCHAR2 (15);
      l_count                  NUMBER := 0;
      l_ck_cnt                 NUMBER := 0;
      l_cnt_inv                NUMBER;
      l_cnt_ovrflw             NUMBER;
      l_chk_count              NUMBER;
      l_chk_amt                NUMBER;
      l_batch                  NUMBER;
      --l_org_id                 NUMBER;    --Version 3.1
      l_req_id                 NUMBER := fnd_global.conc_request_id;
      l_resp                   VARCHAR2 (100)
                                  := 'HDS Credit Assoc Cash App Mgr - WC';
      l_org_name      CONSTANT hr_all_organization_units.name%TYPE
                                  := 'HDS White Cap - Org' ;
      l_org                    hr_all_organization_units.organization_id%TYPE;

      --submit lockbox
      v_new_trans_flag         VARCHAR2 (16) := 'Y';
      v_run_import             VARCHAR2 (16) := 'Y';
      v_run_validation         VARCHAR2 (16) := 'Y';
      v_post_partial           VARCHAR2 (16) := 'Y'; -- post partial amout as unapplied
      v_quick_cash             VARCHAR2 (16) := 'Y';  --submit post quick cash
      v_submiss_type           VARCHAR2 (16) := 'L';         --submission type
      v_pay_inv                VARCHAR2 (16) := 'N'; -- Pay Unrelated Invoices
      v_c_batch_only           VARCHAR2 (16) := 'N';  -- Complete Batches Only
      v_alt_serach             VARCHAR2 (16) := 'N'; -- Alternate name search option
      v_trans_format_id        NUMBER;
      v_lockbox_id             NUMBER;
      v_org_id                 NUMBER;
      v_req_id                 NUMBER;
      v_report_format          VARCHAR2 (16) := 'A'; --R 'Rejects Only', 'A' -all
      v_user_name              VARCHAR2 (164) := 'xxwc_int_finance';
      v_user_id                NUMBER;
      v_lockbox_directory      VARCHAR2 (512);
      v_data_file              VARCHAR2 (25) := 'WC_LOCKBOX_FILE';
      v_control_file           VARCHAR2 (25) := 'XXWCAR_DCTM_LOCKBOX';
      v_trans_format           VARCHAR2 (25) := 'XXWC_LOCKBOX_DCTM';
      v_trans_name             VARCHAR2 (25) := 'DCTM_';

      v_resp_id                NUMBER;
      v_resp_appl_id           NUMBER;

      v_set_req_status         BOOLEAN;
      v_req_phase              VARCHAR2 (80);
      v_req_status             VARCHAR2 (80);
      v_dev_phase              VARCHAR2 (30);
      v_dev_status             VARCHAR2 (30);
      v_message                VARCHAR2 (255);
      v_interval               NUMBER := 10;                     -- In seconds
      v_max_time               NUMBER := 15000;                  -- In seconds
      v_completion_status      VARCHAR2 (30) := 'NORMAL';
      v_error_message          VARCHAR2 (3000);
      v_file_dir               VARCHAR2 (100);
      v_instance               VARCHAR2 (8);
      v_shortname              VARCHAR2 (30);
      v_count                  NUMBER;

      l_can_submit_request     BOOLEAN := TRUE;
      l_globalset              VARCHAR2 (100);
      l_err_msg                VARCHAR2 (3000);
      l_err_code               NUMBER;
      l_sec                    VARCHAR2 (255);
      l_message                VARCHAR2 (150);
      l_statement              VARCHAR2 (9000);
      l_err_callfrom           VARCHAR2 (75) DEFAULT 'XXWCAR_DCTM_PKG';
      l_err_callpoint          VARCHAR2 (75) DEFAULT 'START';
      --l_distro_list        VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_procedure_name         VARCHAR2 (75)
                                  := 'XXWCAR_DCTM_PKG.DCTM_LOCKBOX';

      CURSOR c_header (
         p_date        DATE,
         p_bank_nbr    VARCHAR2)
      IS
         SELECT DISTINCT l.lockbox_number,
                         r.deposit_date,
                         '4100067610' dest_acct,
                         l.bank_origination_number
           FROM xxwc.xxwcar_cash_rcpts_tbl r,
                ar.ar_lockboxes_all l,
                apps.fnd_lookup_values h
          WHERE     UPPER (r.operator_code) = h.lookup_code
                AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                AND h.enabled_flag = 'Y'
                AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                AND h.meaning = l.lockbox_number
                AND lockbox_number NOT LIKE 'PRISM%'
                AND l.status = 'A'
                AND l.org_id = l_org
                AND r.status = 'NEW'
                AND r.deposit_date = p_date
                AND l.bank_origination_number = p_bank_nbr;

      CURSOR c_control_trans (
         p_lockbox    VARCHAR2,
         p_date       DATE)
      IS
         SELECT DISTINCT l.lockbox_number, r.deposit_date, control_nbr
           FROM xxwc.xxwcar_cash_rcpts_tbl r,
                ar.ar_lockboxes_all l,
                apps.fnd_lookup_values h
          WHERE     UPPER (r.operator_code) = h.lookup_code
                AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                AND h.enabled_flag = 'Y'
                AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                AND h.meaning = l.lockbox_number
                AND lockbox_number NOT LIKE 'PRISM%'
                AND l.status = 'A'
                AND l.org_id = l_org
                AND r.status = 'NEW'
                AND l.lockbox_number = p_lockbox
                AND r.deposit_date = p_date;

      CURSOR c_check (
         p_lockbox    VARCHAR2,
         p_date       DATE,
         p_control    VARCHAR2)
      IS
           SELECT DISTINCT r.check_nbr,
                           r.check_amt,
                           r.deposit_date,
                           l.lockbox_number,
                           r.url_link,
                           r.comments
             FROM xxwc.xxwcar_cash_rcpts_tbl r,
                  ar.ar_lockboxes_all l,
                  apps.fnd_lookup_values h
            WHERE     UPPER (r.operator_code) = h.lookup_code
                  AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                  AND h.enabled_flag = 'Y'
                  AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                  AND h.meaning = l.lockbox_number
                  AND lockbox_number NOT LIKE 'PRISM%'
                  AND l.status = 'A'
                  AND l.org_id = l_org
                  AND r.status = 'NEW'
                  AND l.lockbox_number = p_lockbox
                  AND r.deposit_date = p_date
                  AND r.control_nbr = p_control
         ORDER BY r.check_nbr;
   BEGIN
      SELECT LOWER (NAME) INTO l_sid FROM v$database;

      v_lockbox_directory := '/xx_iface/' || l_sid || '/inbound/ar/lockbox/';

      SELECT organization_id
        INTO l_org
        FROM hr_all_organization_units
       WHERE NAME = l_org_name;

      SELECT COUNT (*)
        INTO l_count
        FROM xxwc.xxwcar_cash_rcpts_tbl r,
             ar.ar_lockboxes_all l,
             apps.fnd_lookup_values h
       WHERE     UPPER (r.operator_code) = h.lookup_code
             AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
             AND h.enabled_flag = 'Y'
             AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
             AND h.meaning = l.lockbox_number
             AND lockbox_number NOT LIKE 'PRISM%'
             AND l.status = 'A'
             AND l.org_id = l_org
             AND r.status = 'NEW';

      IF l_count > 0
      THEN
         DBMS_OUTPUT.put_line (
            'Number of records to process from Documentum:  ' || l_count);

         BEGIN
            SELECT user_id
              INTO v_user_id
              FROM fnd_user
             WHERE user_name = UPPER (v_user_name);
         EXCEPTION
            WHEN OTHERS
            THEN
               v_user_id := 0;
         END;

         l_file_dir := 'XXWCAR_LOCKBOX_DIR';

         l_sec := 'Starting the AR Lockbox file creation from DCTM; ';
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);

         BEGIN
            FOR c_trans
               IN (SELECT DISTINCT
                          r.deposit_date,
                          '4100067610' dest_acct,
                          l.bank_origination_number
                     FROM xxwc.xxwcar_cash_rcpts_tbl r,
                          ar.ar_lockboxes_all l,
                          apps.fnd_lookup_values h
                    WHERE     UPPER (r.operator_code) = h.lookup_code
                          AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                          AND h.enabled_flag = 'Y'
                          AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                          AND h.meaning = l.lockbox_number
                          AND lockbox_number NOT LIKE 'PRISM%'
                          AND l.status = 'A'
                          AND l.org_id = l_org
                          AND r.status = 'NEW')
            LOOP
               BEGIN
                  l_sec := 'Truncate the summary table before loading.';

                  EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWCAR_LOCKBOX_TBL';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_msg :=
                           'Failed to Truncate XXWC.XXWCAR_LOCKBOX_GTT: '
                        || SQLERRM;

                     DBMS_OUTPUT.put_line (l_err_msg);
                     RAISE PROGRAM_ERROR;
               END;

               INSERT INTO xxwc.xxwcar_lockbox_tbl
                  SELECT DISTINCT check_nbr,
                                  l.lockbox_number,
                                  r.deposit_date,
                                  check_amt,
                                  control_nbr,
                                  l.bank_origination_number
                    FROM xxwc.xxwcar_cash_rcpts_tbl r,
                         ar.ar_lockboxes_all l,
                         apps.fnd_lookup_values h
                   WHERE     UPPER (r.operator_code) = h.lookup_code
                         AND h.lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                         AND h.enabled_flag = 'Y'
                         AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                         AND h.meaning = l.lockbox_number
                         AND lockbox_number NOT LIKE 'PRISM%'
                         AND l.status = 'A'
                         AND l.org_id = l_org
                         AND r.status = 'NEW'
                         AND r.deposit_date = c_trans.deposit_date;

               COMMIT;                                           --Version 1.5
               --Set the file name and open the file
               l_file_name_temp := 'TEMP_' || 'WC_LOCKBOX_FILE.txt';

               l_file_name :=
                     'WC_LOCKBOX_FILE_'
                  || TO_CHAR (SYSDATE, 'YYYYMMDD_HH24MISS')
                  || '.txt';

               fnd_file.put_line (fnd_file.LOG,
                                  'Filename generated : ' || l_file_name);
               fnd_file.put_line (fnd_file.output,
                                  'Filename generated : ' || l_file_name);
               DBMS_OUTPUT.put_line ('Filename generated : ' || l_file_name);

               l_file_handle :=
                  UTL_FILE.fopen (l_file_dir, l_file_name_temp, 'w');

               BEGIN
                  l_trans_string :=
                        '1'
                     || RPAD (c_trans.dest_acct, 25, ' ')
                     || RPAD (c_trans.bank_origination_number, 12, ' ')
                     || RPAD (c_trans.deposit_date, 9, ' ');

                  UTL_FILE.put_line (l_file_handle, l_trans_string);

                  FOR c_lckbx
                     IN c_header (c_trans.deposit_date,
                                  c_trans.bank_origination_number)
                  LOOP
                     -- Added to not pickup PRISM data
                     IF c_lckbx.lockbox_number NOT LIKE 'PRISM%'
                     THEN
                        l_header_string :=
                              '2'
                           || RPAD (c_lckbx.lockbox_number, 30, ' ')
                           || RPAD (c_trans.deposit_date, 9, ' ')
                           || RPAD (c_trans.dest_acct, 24, ' ')
                           || RPAD (c_trans.bank_origination_number, 11, ' ');
                     END IF;

                     UTL_FILE.put_line (l_file_handle, l_header_string);

                     FOR c_batch
                        IN c_control_trans (c_lckbx.lockbox_number,
                                            c_lckbx.deposit_date)
                     LOOP
                        FOR c_chk
                           IN c_check (c_lckbx.lockbox_number,
                                       c_lckbx.deposit_date,
                                       c_batch.control_nbr)
                        LOOP
                           FOR c_chk_cust
                              IN (SELECT check_amt,
                                         deposit_date,
                                         lockbox_number,
                                         url_link,
                                         invoice_total,
                                         account_number,
                                         check_nbr,
                                         control_nbr
                                    FROM (  SELECT r.check_amt,
                                                   r.deposit_date,
                                                   l.lockbox_number,
                                                   r.url_link,
                                                   SUM (r.invoice_amt)
                                                      invoice_total,
                                                   ca.account_number,
                                                   r.check_nbr,
                                                   control_nbr
                                              FROM xxwc.xxwcar_cash_rcpts_tbl r,
                                                   ar.ar_lockboxes_all l,
                                                   apps.fnd_lookup_values h,
                                                   ar.hz_cust_accounts ca
                                             WHERE     UPPER (r.operator_code) =
                                                          h.lookup_code
                                                   AND h.lookup_type =
                                                          'XXWC_AR_LOCKBOX_DCTM'
                                                   AND h.enabled_flag = 'Y'
                                                   AND NVL (h.end_date_active,
                                                            SYSDATE) >= SYSDATE
                                                   AND h.meaning =
                                                          l.lockbox_number
                                                   AND lockbox_number NOT LIKE
                                                          'PRISM%'
                                                   AND l.status = 'A'
                                                   AND l.org_id = l_org
                                                   AND r.status = 'NEW'
                                                   AND r.customer_nbr =
                                                          ca.account_number(+)
                                                   AND l.lockbox_number =
                                                          c_chk.lockbox_number
                                                   AND r.deposit_date =
                                                          c_chk.deposit_date
                                                   AND r.control_nbr =
                                                          c_batch.control_nbr --Version 1.5
                                                   AND check_nbr =
                                                          c_chk.check_nbr
                                                   AND r.check_amt =
                                                          c_chk.check_amt --Version 1.5
                                          GROUP BY r.check_nbr,
                                                   r.check_amt,
                                                   r.deposit_date,
                                                   l.lockbox_number,
                                                   r.url_link,
                                                   r.comments,
                                                   ca.account_number,
                                                   control_nbr
                                          ORDER BY r.check_nbr,
                                                   SUM (r.invoice_amt) DESC)
                                   WHERE ROWNUM < 2              --Version 1.5
                                                   )
                           LOOP
                              l_ck_cnt := l_ck_cnt + 1;

                              -- Added to not pickup PRISM data
                              IF c_lckbx.lockbox_number NOT LIKE 'PRISM%'
                              THEN
                                 l_pmt_string :=
                                       '3'
                                    || RPAD (c_chk_cust.control_nbr, 20, ' ')
                                    || LPAD (l_ck_cnt, 4, '0')
                                    || LPAD (c_chk_cust.check_amt, 16, ' ')
                                    || LPAD (c_chk_cust.check_nbr, 30, ' ')
                                    || 'USD'
                                    || RPAD (
                                          NVL (c_chk_cust.account_number,
                                               ' '),
                                          30,
                                          ' ')
                                    || RPAD (c_chk_cust.deposit_date, 9, ' ')
                                    || RPAD (c_chk_cust.lockbox_number,
                                             30,
                                             ' ')
                                    || c_chk.url_link;
                              END IF;

                              UTL_FILE.put_line (l_file_handle, l_pmt_string);

                              l_cnt_inv := NULL;

                              SELECT COUNT (*)
                                INTO l_cnt_inv
                                FROM xxwc.xxwcar_cash_rcpts_tbl r,
                                     apps.fnd_lookup_values h
                               WHERE     r.status = 'NEW'
                                     AND r.deposit_date =
                                            c_lckbx.deposit_date
                                     AND r.check_nbr = c_chk.check_nbr
                                     AND r.control_nbr =
                                            c_chk_cust.control_nbr --Version 1.5
                                     AND r.check_amt = c_chk_cust.check_amt --Version 1.5
                                     AND UPPER (r.operator_code) =
                                            h.lookup_code
                                     AND h.lookup_type =
                                            'XXWC_AR_LOCKBOX_DCTM'
                                     AND h.enabled_flag = 'Y'
                                     AND NVL (h.end_date_active, SYSDATE) >=
                                            SYSDATE
                                     AND h.meaning = c_lckbx.lockbox_number;

                              FOR c_pay
                                 IN (  SELECT r.ROWID,
                                              r.check_nbr,
                                              r.check_amt,
                                              r.invoice_nbr,
                                              r.invoice_amt,
                                              r.deposit_date,
                                              l.lockbox_number,
                                              r.control_nbr,
                                              ROWNUM nbr_count
                                         FROM xxwc.xxwcar_cash_rcpts_tbl r,
                                              ar.ar_lockboxes_all l,
                                              apps.fnd_lookup_values h
                                        WHERE     UPPER (r.operator_code) =
                                                     h.lookup_code
                                              AND h.lookup_type =
                                                     'XXWC_AR_LOCKBOX_DCTM'
                                              AND h.enabled_flag = 'Y'
                                              AND NVL (h.end_date_active,
                                                       SYSDATE) >= SYSDATE
                                              AND h.meaning = l.lockbox_number
                                              AND lockbox_number NOT LIKE
                                                     'PRISM%'
                                              AND l.status = 'A'
                                              AND l.org_id = l_org
                                              AND r.status = 'NEW'
                                              AND l.lockbox_number =
                                                     c_lckbx.lockbox_number
                                              AND r.deposit_date =
                                                     c_lckbx.deposit_date
                                              AND r.check_nbr = c_chk.check_nbr
                                              AND r.control_nbr =
                                                     c_chk_cust.control_nbr --Version 1.5
                                              AND r.check_amt =
                                                     c_chk_cust.check_amt --Version 1.5
                                     ORDER BY nbr_count)
                              LOOP
                                 IF l_cnt_inv = c_pay.nbr_count
                                 THEN
                                    l_cnt_ovrflw := 9;
                                 ELSE
                                    l_cnt_ovrflw := 0;
                                 END IF;

                                 -- Added to not pickup PRISM data
                                 IF c_lckbx.lockbox_number NOT LIKE 'PRISM%'
                                 THEN
                                    l_ovrflw_string :=
                                          '4'
                                       || RPAD (c_pay.control_nbr, 20, ' ')
                                       || LPAD (l_ck_cnt, 4, '0')
                                       || LPAD (c_pay.nbr_count, 3, '0')
                                       || l_cnt_ovrflw
                                       || RPAD (
                                             NVL (c_pay.invoice_nbr, 'ZZZZZ'),
                                             20,
                                             ' ')
                                       || RPAD (
                                             NVL (c_pay.invoice_amt,
                                                  c_pay.check_amt),
                                             16,
                                             ' ');
                                 END IF;

                                 UTL_FILE.put_line (l_file_handle,
                                                    l_ovrflw_string);

                                 UPDATE xxwc.xxwcar_cash_rcpts_tbl
                                    SET status = 'DCTM_PROCESSED'
                                  WHERE     check_nbr = c_pay.check_nbr
                                        AND NVL (invoice_nbr, 'ZZZZZ') =
                                               NVL (c_pay.invoice_nbr,
                                                    'ZZZZZ')
                                        AND deposit_date = c_pay.deposit_date
                                        AND ROWID = c_pay.ROWID;
                              END LOOP;           --invoice details by receipt
                           END LOOP;                         --receipt details
                        END LOOP;                            --receipt summary

                        SELECT COUNT (*), SUM (check_amt)
                          INTO l_chk_count, l_chk_amt
                          FROM xxwc.xxwcar_lockbox_tbl
                         WHERE     lockbox_number = c_lckbx.lockbox_number
                               AND control_nbr = c_batch.control_nbr;

                        l_batch_trailer_string :=
                              '7'
                           || RPAD (c_batch.control_nbr, 20, ' ')
                           || RPAD (c_lckbx.lockbox_number, 30, ' ')
                           || RPAD (c_lckbx.deposit_date, 9, ' ')
                           || RPAD (l_chk_count, 4, ' ')
                           || RPAD (l_chk_amt, 16, ' ');

                        UTL_FILE.put_line (l_file_handle,
                                           l_batch_trailer_string);
                     END LOOP;     --c_batch totals by lockbox and control nbr

                     SELECT COUNT (*), SUM (check_amt)
                       INTO l_chk_count, l_chk_amt
                       FROM xxwc.xxwcar_lockbox_tbl
                      WHERE lockbox_number = c_lckbx.lockbox_number;

                     SELECT COUNT (DISTINCT control_nbr)
                       INTO l_batch
                       FROM xxwc.xxwcar_lockbox_tbl
                      WHERE lockbox_number = c_lckbx.lockbox_number;

                     l_lckbx_trailer_string :=
                           '8'
                        || RPAD (c_lckbx.lockbox_number, 30, ' ')
                        || c_lckbx.deposit_date
                        || RPAD (l_chk_count, 4, ' ')
                        || RPAD (l_chk_amt, 16, ' ')
                        || RPAD (l_batch, 4, ' ');

                     UTL_FILE.put_line (l_file_handle,
                                        l_lckbx_trailer_string);
                  END LOOP;                        --c_lckbx  total by lockbox

                  --Close the file
                  l_sec := 'Closing the File; ';
                  fnd_file.put_line (fnd_file.LOG, l_sec);
                  fnd_file.put_line (fnd_file.output, l_sec);
                  UTL_FILE.fclose (l_file_handle);
                  UTL_FILE.frename (l_file_dir,
                                    l_file_name_temp,
                                    l_file_dir,
                                    l_file_name);
               END;

               BEGIN
                  SELECT organization_id
                    INTO v_org_id
                    FROM hr_all_organization_units
                   WHERE NAME = 'HDS White Cap - Org';

                  SELECT transmission_format_id
                    INTO v_trans_format_id
                    FROM ar_transmission_formats
                   WHERE format_name = 'XXWC_LOCKBOX_DCTM';

                  BEGIN
                     SELECT responsibility_id, application_id
                       INTO v_resp_id, v_resp_appl_id
                       FROM fnd_responsibility_vl
                      WHERE     responsibility_name =
                                   'HDS Credit Assoc Cash App Mgr - WC'
                            AND SYSDATE BETWEEN start_date
                                            AND NVL (end_date,
                                                     TRUNC (SYSDATE) + 1);
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        l_err_msg :=
                              'Responsibility - '
                           || 'HDS Credit Assoc Cash App Mgr - WC'
                           || ' not defined in Oracle';

                        DBMS_OUTPUT.put_line (l_err_msg);
                        RAISE PROGRAM_ERROR;
                     WHEN OTHERS
                     THEN
                        l_err_msg :=
                              'Error deriving Responsibility_id for ResponsibilityName - '
                           || 'HDS Credit Assoc Cash App Mgr - WC';

                        DBMS_OUTPUT.put_line (l_err_msg);
                        RAISE PROGRAM_ERROR;
                  END;

                  fnd_global.apps_initialize (
                     user_id        => v_user_id,
                     resp_id        => v_resp_id,
                     resp_appl_id   => v_resp_appl_id);

                  mo_global.init ('AR');
                  mo_global.set_policy_context ('S', l_org);  --Version 3.1 
                  fnd_request.set_org_id (l_org); --Version 3.1

                  v_req_id :=
                     fnd_request.submit_request (
                        'AR',
                        'ARLPLB',
                        NULL,
                        NULL,
                        FALSE,
                        v_new_trans_flag,
                        NULL                                -- Transmission Id
                            ,
                        NULL                            -- Original Request Id
                            ,
                           v_trans_name
                        || TO_CHAR (SYSDATE, 'mmddyyyy HH24:MI:SS') -- Transmission Name
                                                                   ,
                        v_run_import                          -- Submit Import
                                    ,
                        v_lockbox_directory || l_file_name        -- Data File
                                                          ,
                        v_control_file                         -- Control File
                                      ,
                        v_trans_format_id                --Transmission Format
                                         ,
                        v_run_validation                  -- Submit Validation
                                        ,
                        v_pay_inv                    -- Pay Unrelated Invoices
                                 ,
                        v_lockbox_id                             -- Lockbox Id
                                    ,
                        NULL                                        -- GL Date
                            ,
                        v_report_format                       -- Report Format
                                       ,
                        v_c_batch_only                -- Complete Batches Only
                                      ,
                        v_quick_cash                      -- Submit Post batch
                                    ,
                        v_alt_serach           -- Alternate name search option
                                    ,
                        v_post_partial --Post Partial Amount or Reject Entire Receipt
                                      ,
                        NULL                         -- USSGL transaction code
                            ,
                        v_org_id,
                        'Y',                            --Apply unearn discount         --Added by Maha for ver 2.0 on 7/23/14
                        1,                                   --No of instances                  --Added by Maha for ver 3.0 on 7/30/14
                        v_submiss_type,                      -- submission type
                        NULL,
                        CHR (0),
                        CHR (0));
                  COMMIT;

                  fnd_file.put_line (fnd_file.LOG, v_req_id);
                  fnd_file.put_line (fnd_file.output, v_req_id);

                  --Write detail Line Log rows
                  l_sec := 'Waiting for Lockbox to finish ' || v_req_id;
                  fnd_file.put_line (fnd_file.LOG, l_sec);
                  fnd_file.put_line (fnd_file.output, l_sec);

                  DBMS_OUTPUT.put_line (
                     'Submitting Lockbox for file:  ' || l_file_name);
                  DBMS_OUTPUT.put_line (
                        'Concurrent Program Process Lockbox Submitted. Request ID: '
                     || v_req_id);

                  --Wait for Lockbox
                  IF (v_req_id != 0)
                  THEN
                     IF fnd_concurrent.wait_for_request (v_req_id,
                                                         v_interval,
                                                         v_max_time,
                                                         v_req_phase,
                                                         v_req_status,
                                                         v_dev_phase,
                                                         v_dev_status,
                                                         v_message)
                     THEN
                        v_error_message :=
                              'ReqID = '
                           || v_req_id
                           || '   DPhase = '
                           || v_dev_phase
                           || '   DStatus = '
                           || v_dev_status
                           || '   MSG = '
                           || v_message;

                        IF    v_dev_phase != 'COMPLETE'
                           OR v_dev_status != 'NORMAL'
                        THEN
                           l_statement :=
                                 'An error occured in the running of the XXCUSAR_DCTM_PKG HDS AR DCTM Lockbox Process'
                              || v_error_message
                              || '.';
                           fnd_file.put_line (fnd_file.LOG, l_statement);
                           fnd_file.put_line (fnd_file.output, l_statement);

                           DBMS_OUTPUT.put_line (l_statement);

                           retcode := 2;
                           RAISE PROGRAM_ERROR;
                        END IF;
                     -- Then Success!

                     ELSE
                        l_statement :=
                              'Wait Timed Out-running the XXCUSAR_DCTM_PKG.DCTM_LOCKBOX Process Lockbox'
                           || v_error_message
                           || '.';

                        l_err_msg := l_statement;
                        fnd_file.put_line (fnd_file.LOG, l_err_msg);
                        fnd_file.put_line (fnd_file.output, l_err_msg);
                        DBMS_OUTPUT.put_line (l_statement);
                        RAISE PROGRAM_ERROR;
                     END IF;
                  ELSE
                     l_statement :=
                        'An error occured XXCUSAR_DCTM_PKG.DCTM_LOCKBOX - Process Lockbox not initated';

                     l_err_msg := l_statement;
                     fnd_file.put_line (fnd_file.LOG, l_err_msg);
                     fnd_file.put_line (fnd_file.output, l_err_msg);
                     DBMS_OUTPUT.put_line (l_statement);
                     RAISE PROGRAM_ERROR;
                  END IF;

                  --dbms_output.put_line(l_sec);
                  --dbms_output.put_line('Request ID:  ' || v_req_id);
                  retcode := 0;
               END;                                           --submit lockbox
            END LOOP;
         END;
      ELSE
         DBMS_OUTPUT.put_line (
            'Number of records to process from Documentum:  0');
         retcode := 0;
      END IF;

--      inv_short_code (l_err_msg, l_err_code);        --COMMENTED BY MAHA FOR VER 1.5

   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         --dbms_output.put_line(l_sec);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         l_err_callpoint := l_sec;

         DBMS_OUTPUT.put_line (l_err_msg);

         UTL_FILE.fclose (l_file_handle);
         --utl_file.fremove(l_file_dir, l_file_name);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWCAR_DCTM_PKG with PROGRAM ERROR',
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
      WHEN OTHERS
      THEN
         ROLLBACK;
         --dbms_output.put_line(l_sec);

         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         retcode := l_err_code;
         errbuf := l_err_msg;
         l_err_callpoint := l_sec;

         DBMS_OUTPUT.put_line (l_err_msg);

         UTL_FILE.fclose (l_file_handle);
         --utl_file.fremove(l_file_dir, l_file_name);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWCAR_DCTM_PKG with OTHERS Exception',
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
   END dctm_lockbox;


/**************************************************************************
  File Name: XXWCAR_DCTM_PKG
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      Procedure to submit concurrent program �WC AR Invoice Short Pay Code DCTM�  
  HISTORY
  =============================================================================
  VERSION DATE          AUTHOR(S)            DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     24-Feb-2014   Maharajan Shunmugam  TMS 20130908-00094 
  3.2     03/Jul/2015    M Hari Prasad       TMS# 20150702-00029  increase variable l_sec size
                                             to 3000 within the package XXWCAR_DCTM_PKG.uc4_submit_inv_short_code
                                
  =============================================================================
  *****************************************************************************/
  PROCEDURE uc4_submit_inv_short_code(errbuf                 OUT VARCHAR2
                                       ,retcode                OUT NUMBER
                                       ,p_user_name            IN VARCHAR2
                                       ,p_responsibility_name  IN VARCHAR2)
  IS

    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 7200; -- In seconds
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
    --l_sec                VARCHAR2(255); Commented By Hari for ver 3.2
    l_sec                VARCHAR2(3000);  ---Added by M Hari Prasad for ver 3.2
    l_user_id            fnd_user.user_id%TYPE;
    l_procedure          VARCHAR2(50) := 'uc4_submit_inv_short_code';
  
    l_application_name   VARCHAR2(30) := 'XXCUS';
    l_program_short_name VARCHAR2(240) := 'XXWC_AR_DCTM_SHORTPAY';

  
  BEGIN
    retcode := 0;
  
    SELECT user_id
      INTO l_user_id
      FROM fnd_user
     WHERE user_name = p_user_name;
  
    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(p_user_name
                                                             ,p_responsibility_name);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';
    
    ELSE
    
      l_sec := 'Global Variables are not set for the ' || p_user_name || '.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
    l_sec := 'UC4 call to run concurrent request WC AR Invoice Short Pay Code DCTM.';
  
    l_req_id := fnd_request.submit_request(application => l_application_name
                                          ,program     => l_program_short_name
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE);
    COMMIT;
  
    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,v_interval
                                        ,v_max_time
                                        ,v_phase
                                        ,v_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
        THEN
          l_sec := 'An error occured in the running of the WC AR Invoice Short Pay Code DCTM Program' ||
                   v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE program_error;
        
        END IF;
        -- Then Success!
      
      ELSE
        l_sec := 'EBS timed out waiting on WC AR Invoice Short Pay Code DCTM  Program' ||
                 v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        RAISE program_error;
      END IF;
    
    ELSE
      l_sec := 'An error occured when trying to submit WC AR Invoice Short Pay Code DCTM  Program';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);
  
  EXCEPTION
    WHEN program_error THEN
      l_err_msg := l_sec || ' Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('retcode:  ' || retcode);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM ||
                                                                         regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running WC AR Invoice Short Pay Code DCTM'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
    
      retcode := 2;
      errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_sec || ' Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('retcode:  ' || retcode);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM ||
                                                                         regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running WC AR Invoice Short Pay Code DCTM with Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
    
      retcode := 2;
      errbuf  := l_err_msg;
    
  END uc4_submit_inv_short_code;  


/**************************************************************************
  File Name: XXWCAR_DCTM_PKG
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      Procedure to update "attribute1" of ar_payment_schedules_all 
  HISTORY
  =============================================================================
  VERSION DATE          AUTHOR(S)            DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     24-Feb-2014   Maharajan Shunmugam  TMS 20130908-00094 
  =============================================================================
  *****************************************************************************/

  PROCEDURE update_short_code(p_payment_schedule_id IN NUMBER, p_short_code IN VARCHAR2) IS
  l_category         VARCHAR2(30) := '162';
  l_meaning          VARCHAR2(30);

    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_message            VARCHAR2(150);
    l_statement          VARCHAR2(9000);
    l_req_id           NUMBER := fnd_global.conc_request_id;
    l_err_callfrom       VARCHAR2(75) DEFAULT 'XXWCAR_DCTM_PKG';
    l_err_callpoint      VARCHAR2(75) DEFAULT 'update_short_code';
    l_distro_list        VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_procedure_name VARCHAR2(75) := 'XXWCAR_DCTM_PKG.INV_SHORT_CODE';



  BEGIN

     ----------------------------------------------------------------
      -- Query to get meaning of the short code
      ----------------------------------------------------------------
  BEGIN
      SELECT meaning
           INTO l_meaning 
       FROM apps.fnd_lookup_values 
       WHERE  lookup_type = 'XXWC_DCMT_REASON_CODES'
          AND enabled_flag = 'Y'
          AND nvl(end_date_active, SYSDATE) >= SYSDATE             
          AND lookup_code = p_short_code;
  EXCEPTION
  WHEN OTHERS THEN
     l_meaning := NULL;
  END;

   ----------------------------------------------------------------
      -- Update ar_payment_schedules_all with short code
   ----------------------------------------------------------------

     fnd_file.put_line (fnd_file.LOG, 'Updating short code for payment schedule id :'||p_payment_schedule_id);
     UPDATE ar_payment_schedules_all
             SET attribute_category = l_category  
                ,attribute1         = l_meaning
           WHERE payment_schedule_id = p_payment_schedule_id;
 COMMIT;
    fnd_file.put_line (fnd_file.LOG, 'Updated short code '||p_short_code|| ' for payment schedule id :'||p_payment_schedule_id);
  EXCEPTION
  WHEN OTHERS THEN
   l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
  fnd_file.put_line (fnd_file.LOG, 'Exception while updating for payment schedule id :'||p_payment_schedule_id);
  ROLLBACK;
    xxcus_error_pkg.xxcus_error_main_api(p_called_from         => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running XXWCAR_DCTM_PKG.update_short_code with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
  END update_short_code;
  

 /**************************************************************************
  File Name: XXWCAR_DCTM_PKG
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      DCTM delivers receipts and some have a short pay code that
                needs to be recorded after the lockbox processing.  Lockbox
                can only apply attributes on the receipt.
                DFF is setup on ar_payment_schedules_all "attribute1"
  HISTORY
  =============================================================================
         Last Update Date : 12/17/2011
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     10-Mar-2012   Kathy Poling       Initial creation of the procedure
  1.1     26-Jul-2012   Kathy Poling       SR 158356 added attribute category
                                           for the DFF being changed to context  
  2.0     19-Sep-2012   Kathy Poling       Short Code for Payment enhancement 
                                           project for in house lockbox
                                           also a fix to the invoice amount =
                                           to the application to update the correct
                                           record   RFC# 35126  Service# 177461 
  2.1     15-Feb-2013   Kathy Poling       SR 190095 adding more reason codes 
                                           and includes all lockboxes for coding 
                                           TMS ID 20130206-00750             
  2.2     24-Feb-2014   Maharajan          Added short codes 8, 9 and V.
                        Shunmugam          Short code is appended to ar_payment_schedules_all
                                           TMS 20130908-00094 
  =============================================================================
  *****************************************************************************/

  PROCEDURE inv_short_code(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    l_file_name        VARCHAR2(100);
    l_file_dir         VARCHAR2(100);
    l_file_handle      utl_file.file_type;
    l_file_name_temp   VARCHAR2(100);
    l_file_log         VARCHAR2(100);
    l_sid              VARCHAR2(8);
    l_req_id           NUMBER := fnd_global.conc_request_id;
    l_count            NUMBER;
    l_category         VARCHAR2(30) := '162'; --Version 1.1
    l_org_id           NUMBER := 162; --HDS White Cap - Org
    l_short_int        VARCHAR2(1) := 'N';
    l_short_int_date   DATE := SYSDATE;
    l_short_code       VARCHAR2(30) := '1'; --Short pay
    l_unearned_code    VARCHAR2(30) := 'U'; --Unearned discount
    l_freight_code     VARCHAR2(30) := 'G'; --Freight discount
    l_salestax_code    VARCHAR2(30) := 'I'; --Sales Tax discount
    l_discount_code    VARCHAR2(30) := 'X'; --Discount not taken
    l_overpayment_code VARCHAR2(30) := 'OP'; --Overpayment     Version 2.1
    l_duplicate_code   VARCHAR2(30) := '2'; --Duplicate Payment   Version 2.1
    l_dup_deduct_code  VARCHAR2(30) := 'DD'; --Duplicate Deduction   Version 2.1
 
    --Added below for ver 2.2 <start
    l_tax_adj_code     VARCHAR2(30) := '8';   --tax adjustment
    l_receipt_rev_code VARCHAR2(30) := '9';   --receipt reversal
    l_credit_bal_code  VARCHAR2(30) := 'V';   --credit applied leaving balance
    l_amount_due_original  NUMBER;
    l_amount_due_remaining NUMBER;
    l_tax_original       NUMBER;
    l_amount_credited       NUMBER;
    l_append_code VARCHAR2(50);
      --End >
    
  
    v_set_req_status    BOOLEAN;
    v_req_phase         VARCHAR2(80);
    v_req_status        VARCHAR2(80);
    v_dev_phase         VARCHAR2(30);
    v_dev_status        VARCHAR2(30);
    v_message           VARCHAR2(255);
    v_interval          NUMBER := 10; -- In seconds
    v_max_time          NUMBER := 15000; -- In seconds
    v_completion_status VARCHAR2(30) := 'NORMAL';
    v_error_message     VARCHAR2(3000);
    v_file_dir          VARCHAR2(100);
    v_instance          VARCHAR2(8);
    v_shortname         VARCHAR2(30);
    v_count             NUMBER;
    v_reverse           VARCHAR2(30);
  
    l_can_submit_request BOOLEAN := TRUE;
    l_globalset          VARCHAR2(100);
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_message            VARCHAR2(150);
    l_statement          VARCHAR2(9000);
    l_err_callfrom       VARCHAR2(75) DEFAULT 'XXWCAR_DCTM_PKG';
    l_err_callpoint      VARCHAR2(75) DEFAULT 'START';
    --l_distro_list        VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_procedure_name VARCHAR2(75) := 'XXWCAR_DCTM_PKG.INV_SHORT_CODE';


  BEGIN
    l_sec := 'Checking for short pay code to process; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    BEGIN
      FOR c1 IN (SELECT ps.payment_schedule_id 
               ,ca.account_number
                       ,rcta.trx_number
                       ,ps.amount_due_original
                       ,ps.freight_original
                       ,ps.tax_original
                       ,ps.amount_due_remaining
                       ,ps.discount_remaining --Version 2.1
                       ,ps.discount_taken_earned
                       ,ps.discount_taken_unearned
                       ,ps.amount_applied
                       ,ps.amount_credited
                       ,ps.status
                       ,ps.due_date
                       ,xxwcar_dctm_pkg.get_disc_date(ps.term_id
                                                     ,rcta.trx_date
                                                     ,ps.due_date) disc_date
                       ,xxwcar_dctm_pkg.get_disc_amt(rcta.term_id
                                                    ,rcta.customer_trx_id) disc_amt
                   FROM xxwc.xxwc_invoice_shortpay_tbl  r
                       ,ar.ra_customer_trx_all      rcta
                       ,ar.ar_payment_schedules_all ps
                       ,ar.ra_batch_sources_all     bs
                       ,ar.hz_cust_accounts         ca
                  WHERE ps.payment_schedule_id = r.payment_schedule_id
                    AND rcta.batch_source_id = bs.batch_source_id
                    AND rcta.org_id = bs.org_id
                    AND nvl(bs.name, '#####') <> 'CONVERSION'
                    AND rcta.org_id = l_org_id
                    AND rcta.customer_trx_id = ps.customer_trx_id
                    AND rcta.bill_to_customer_id = ca.cust_account_id
                    AND ps.status = 'OP'
                 UNION
                 --Conversion invoices to use getpaid data dump to get tax and freight
                 SELECT ps.payment_schedule_id
               ,ca.account_number
                       ,rcta.trx_number
                       ,gp.invamt amount_due_original --ps.amount_due_original
                       ,gp.freight freight_original
                       ,gp.tax tax_original
                       ,ps.amount_due_remaining
                       ,ps.discount_remaining --Version 2.1
                       ,ps.discount_taken_earned
                       ,ps.discount_taken_unearned
                       ,ps.amount_applied
                       ,ps.amount_credited
                       ,ps.status
                       ,ps.due_date
                       ,xxwcar_dctm_pkg.get_disc_date(ps.term_id
                                                     ,rcta.trx_date
                                                     ,ps.due_date) disc_date
                       ,xxwcar_dctm_pkg.get_disc_amt(rcta.term_id
                                                    ,rcta.customer_trx_id) disc_amt
                   FROM xxwc.xxwc_invoice_shortpay_tbl        r
                       ,ar.ra_customer_trx_all            rcta
                       ,ar.ar_payment_schedules_all       ps
                       ,ra_batch_sources_all              bs
                       ,ar.hz_cust_accounts               ca
                       ,xxwc.xxwc_armast_getpaid_dump_tbl gp
                  WHERE ps.payment_schedule_id = r.payment_schedule_id
                    AND rcta.batch_source_id = bs.batch_source_id
                    AND rcta.org_id = bs.org_id
                    AND nvl(bs.name, '#####') = 'CONVERSION'
                    AND rcta.org_id = l_org_id
                    AND rcta.customer_trx_id = ps.customer_trx_id
                    AND rcta.bill_to_customer_id = ca.cust_account_id
                    AND rcta.trx_number = gp.invno
                    AND ca.account_number = gp.custno
                    AND ps.status = 'OP')
      LOOP

   ----------------------------------------------------------------
      -- Check if reversal happened in the last 24 hours
   ----------------------------------------------------------------

        BEGIN
            SELECT 'Y'
                INTO  v_reverse
                FROM APPS.AR_APP_ADJ_V
             WHERE payment_schedule_id = c1.payment_schedule_id
               -- AND  amount = c1.amount_due_remaining
                AND (sysdate -creation_date)*24 < 24
                AND  receipt_state = 'Reversed'
                AND ROWNUM = 1;
            EXCEPTION
            WHEN OTHERS THEN
               v_reverse := 'N';
            END;


   ----------------------------------------------------------------
      -- Short code: 'X' - didn't take allowed discount
   ----------------------------------------------------------------

        IF nvl(c1.disc_amt, 0) > 0 AND
           c1.amount_due_remaining = nvl(c1.discount_remaining, 0) 
        THEN
            fnd_file.put_line (fnd_file.LOG,'Calling discount code proc');
            update_short_code(c1.payment_schedule_id,l_discount_code);         
       
      --******As per Lenora's email on Tameka's confirmation, Commented unearned discount logic by Maha on 04-Mar-14 *******
      /*  ELSIF nvl(c1.disc_amt, 0) > 0 AND                      
              nvl(c1.discount_taken_earned, 0) = 0 AND
              c1.amount_due_original - c1.discount_taken_unearned - c1.disc_amt BETWEEN - .01 AND .01
        THEN
              fnd_file.put_line (fnd_file.LOG,'Calling unearned discount code proc');
             update_short_code(c1.payment_schedule_id,l_unearned_code);          --Unearned discount 
       */
   ----------------------------------------------------------------
      -- Short code: 'G'- freight discount
   ----------------------------------------------------------------

        ELSIF c1.freight_original <> 0 AND
              c1.amount_due_remaining  = c1.freight_original
        THEN
          fnd_file.put_line (fnd_file.LOG,'Calling freight discount proc');
          update_short_code(c1.payment_schedule_id,l_freight_code);   
       
   ----------------------------------------------------------------
      -- Short code: 'I' - Sales tax discount
   ----------------------------------------------------------------        

        ELSIF c1.amount_due_remaining  = c1.tax_original AND
              c1.tax_original <> 0
        THEN 
              fnd_file.put_line (fnd_file.LOG,'Calling sales tax discount proc');
              update_short_code(c1.payment_schedule_id,l_salestax_code);         

   ----------------------------------------------------------------
      -- Short code: 'OP' - Overpaid
   ---------------------------------------------------------------- 
     
        ELSIF c1.amount_due_remaining < 0 AND c1.amount_due_original > 0 AND
              c1.amount_due_remaining * -1 <> c1.amount_due_original
        THEN
                fnd_file.put_line (fnd_file.LOG,'Calling overpaid proc');
                update_short_code(c1.payment_schedule_id,l_overpayment_code);     

   ----------------------------------------------------------------
      -- Short code: 'DD' -  Duplicate Deduction
   ---------------------------------------------------------------- 
        
        ELSIF c1.amount_due_remaining > 0 AND c1.amount_due_original < 0
        THEN
            fnd_file.put_line (fnd_file.LOG,'Calling duplicate deduction proc');
            update_short_code(c1.payment_schedule_id,l_dup_deduct_code);  
 
   ----------------------------------------------------------------
      -- Short code: '2' -  Duplicate payment
   ----------------------------------------------------------------        
        
        ELSIF c1.amount_due_original = c1.amount_due_remaining * -1
        THEN
             fnd_file.put_line (fnd_file.LOG,'Calling duplicate payment proc');
             update_short_code(c1.payment_schedule_id,l_duplicate_code);          
 
   ----------------------------------------------------------------
      -- Short code: - 'V'  credit applied balance
   ---------------------------------------------------------------- 

        ELSIF  c1.amount_due_original       <> c1.amount_due_remaining AND       
        SIGN(c1.amount_due_remaining) = 1 AND
           c1.amount_due_original - c1.amount_due_remaining = c1.amount_credited*(-1)
        THEN
             fnd_file.put_line (fnd_file.LOG,'Credit applied balance proc');
             update_short_code(c1.payment_schedule_id,l_credit_bal_code); 

   ----------------------------------------------------------------
      -- Short code: '8'-  Tax adjustment code
   ---------------------------------------------------------------- 
        
        ELSIF ((c1.amount_due_original - c1.amount_due_remaining) = c1.tax_original)
        THEN             
             fnd_file.put_line (fnd_file.LOG,'Calling tax adjustment proc');
             update_short_code(c1.payment_schedule_id,l_tax_adj_code);    
          
     ----------------------------------------------------------------
      -- Short code: '1' -  Short paid
     ----------------------------------------------------------------     
    
        ELSIF c1.amount_due_original <> c1.amount_due_remaining
              AND v_reverse = 'N'
        THEN
             fnd_file.put_line (fnd_file.LOG,'Calling short paid proc');
             update_short_code(c1.payment_schedule_id,l_short_code );              

     ----------------------------------------------------------------
      -- Short code: '9' -  Receipt reversal code 
     ---------------------------------------------------------------- 

       ELSIF v_reverse = 'Y' -- AND c1.amount_due_original = c1.amount_due_remaining            
        THEN         
            fnd_file.put_line (fnd_file.LOG,'Receipt reversal proc');
            update_short_code(c1.payment_schedule_id,l_receipt_rev_code);           
        END IF;
      END LOOP;
    END;  
  

    ----------------------------------------------------------------
      -- Truncate staging table
     ---------------------------------------------------------------- 

     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_invoice_shortpay_tbl';       

   retcode := 0;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running XXWCAR_DCTM_PKG with PROGRAM ERROR'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
    WHEN OTHERS THEN
      ROLLBACK;
      --dbms_output.put_line(l_sec);
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running XXWCAR_DCTM_PKG with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
--  END;
  END inv_short_code;
END xxwcar_dctm_pkg;
/
