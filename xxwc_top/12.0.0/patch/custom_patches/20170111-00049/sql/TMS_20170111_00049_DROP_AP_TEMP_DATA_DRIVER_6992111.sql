/*************************************************************************
  $Header TMS_20170111_00049_DROP_AP_TEMP_DATA_DRIVER_6992111.sql $

  PURPOSE: drop temp table AP_TEMP_DATA_DRIVER_6992111

  REVISIONS:  TMS_20170111_00049_DROP_AP_TEMP_DATA_DRIVER_6992111
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        11-JAN-2017  Niraj K Ranjan         TMS#20170111-00049

**************************************************************************/ 
DROP TABLE AP_TEMP_DATA_DRIVER_6992111;
