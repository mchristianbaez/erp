/*
 TMS: 20150803-00207 
 Date: 08/03/2015
 Notes: ESMS 608442 
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20150803-00207 , Script 1 -Before Delete');
   DELETE 
     FROM mtl_supply
    WHERE item_id = 2939369
       AND TO_ORGANIZATION_ID = 890
       AND req_header_id = 2059518
       AND req_line_id = 6736703
       AND quantity = -23;
   DBMS_OUTPUT.put_line ('TMS: 20150803-00207, Script 1 -After Delete, rows deleted: '||sql%rowcount);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20150803-00207, Script 1, Errors ='||SQLERRM);
END;
/