/*************************************************************************
  $Header TMS_20160907-00107_CANCEL_LINE.sql $
  Module Name: TMS_20160907-00107  Data Fix script for I675908

  PURPOSE: Data fix script for 20160907-00107 -- Order stuck on hold

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-OCT-2016  Niraj Kumar Ranjan         TMS#20160907-00107 -- order stuck on hold

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160907-00107    , Before Update');

   update apps.oe_order_lines_all
   set FLOW_STATUS_CODE='CANCELLED',
   CANCELLED_FLAG='Y'
   where line_id = 77679638
   and header_id= 47486064;
   
   DBMS_OUTPUT.put_line (
         'TMS: 20160907-00107  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160907-00107    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160907-00107 , Errors : ' || SQLERRM);
END;
/
