/***********************************************************************************************************************************************
   NAME:       TMS_20170309-00055_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        3/02/2017   P.Vamshidhar     TMS#20170309-00055 - Costing Error
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE MTL_MATERIAL_TRANSACTIONS
      SET TRANSACTION_SOURCE_ID = 7856,
          costed_flag = 'N',
          transaction_group_id = NULL,
          ERROR_CODE = NULL,
          error_explanation = NULL
    WHERE transaction_id = 539683201 AND organization_id = 291;

   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/