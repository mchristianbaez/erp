/*
   Ticket#              Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20180730-00087
*/
declare
v_result varchar2(2000);
v_command varchar2(2000);
v_db varchar2(10):=Null;
l_folder varchar2(200);
begin
--
select lower(name) into v_db from v$database;
l_folder := '/xx_iface/'||v_db||'/inbound/bala';
 v_command :='mkdir '
           ||'/xx_iface/'||v_db||'/inbound/bala';
  --
  dbms_output.put_line('OS command =>'||v_command);
  --
  select xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
  into   v_result
  from   dual;
  --
 v_command :='chmod '
           ||'777 '||l_folder;
  --
  dbms_output.put_line('OS command =>'||v_command);
  --
  select xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
  into   v_result
  from   dual;
  --  
exception
  when others then
    dbms_output.put_line('Failed to create folder, message =>'||v_result);
end;
/
