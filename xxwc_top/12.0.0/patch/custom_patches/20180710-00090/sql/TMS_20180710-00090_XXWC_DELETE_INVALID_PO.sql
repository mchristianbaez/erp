/*************************************************************************
  $Header TMS_20180710-00090_XXWC_DELETE_INVALID_PO.sql $
  Module Name: TMS_20180710-00090_XXWC_DELETE_INVALID_PO.sql

  PURPOSE:   Created to Delete invalid records from RCV interfaces

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        07/27/2018  Pattabhi Avula       TMS#20180710-00090
  **************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;

BEGIN
   DBMS_OUTPUT.put_line ('Before delete');

   DELETE FROM apps.rcv_transactions_interface
         WHERE po_header_id = 4418738;

   DBMS_OUTPUT.put_line ('Records deleted-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to delete ' || SQLERRM);
END;
/     
