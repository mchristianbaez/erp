CREATE OR REPLACE PROCEDURE XXWC_PO_EXPECT_REC_PRINT_PRC (
   P_SQL_PART1   IN VARCHAR2,
   P_SQL_PART2   IN VARCHAR2)
IS
   /*******************************************************************************
     * Procedure:   XXWC_PO_EXPECT_REC_PRINT_PRC
     =============================================================================================================
     VERSION DATE               AUTHOR(S)       DESCRIPTION
     ------- -----------------  --------------- ------------------------------------------------------------------
     1.0     06-Dec-2017        P.Vamshidhar    TMS#20171204-00028 - expect receipt reports not printing
                                                Initial creation of the procedure
     *************************************************************************************************************/
   g_dflt_email   VARCHAR2 (75) DEFAULT 'HDSORACLEDEVELOPERS@hdsupply.com';
BEGIN
   EXECUTE IMMEDIATE (P_SQL_PART1 || P_SQL_PART2);
EXCEPTION
   WHEN OTHERS
   THEN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_PO_EXPECT_REC_PRINT_PRC',
         p_calling             => ' ',
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
         p_error_desc          => 'Error occured while executing sql command',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');
END;
/