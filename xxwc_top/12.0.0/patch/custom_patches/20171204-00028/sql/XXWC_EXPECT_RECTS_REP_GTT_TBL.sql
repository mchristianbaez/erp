/************************************************************************************************************
* Table: XXWC.XXWC_EXPECT_RECTS_REP_GTT_TBL
=============================================================================================================
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- ------------------------------------------------------------------
  1.0     06-Dec-2017        P.Vamshidhar    TMS#20171204-00028 - expect receipt reports not printing
                                             Initial creation of the procedure
*************************************************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_EXPECT_RECTS_REP_GTT_TBL
(
   P_DATE                VARCHAR2 (100 BYTE),
   SOURCE                VARCHAR2 (1000 BYTE),
   C_FLEX_ITEM           VARCHAR2 (4000 BYTE),
   C_FLEX_CAT            VARCHAR2 (4000 BYTE),
   PO_NUM                VARCHAR2 (100 BYTE),
   PO_REL_NUM            VARCHAR2 (100 BYTE),
   PO_LINE_NUM           VARCHAR2 (100 BYTE),
   NUMBER_RELEASE_LINE   VARCHAR2 (100 BYTE),
   DOC_TYPE              VARCHAR2 (100 BYTE),
   SHIPMENT_TYPE         VARCHAR2 (100 BYTE),
   LOCATION              VARCHAR2 (100 BYTE),
   LOCATION_ID           VARCHAR2 (100 BYTE),
   REV                   VARCHAR2 (100 BYTE),
   UNIT                  VARCHAR2 (100 BYTE),
   PRODUCT_NUMBER        VARCHAR2 (100 BYTE),
   LINE_TYPE             VARCHAR2 (100 BYTE),
   ITEM_DESCRIPTION      VARCHAR2 (1000 BYTE),
   ORDERED               VARCHAR2 (100 BYTE),
   DUE                   VARCHAR2 (100 BYTE),
   NOTE_RECEIVER         VARCHAR2 (4000 BYTE),
   HAZARD                VARCHAR2 (1000 BYTE),
   UN_NUMBER             VARCHAR2 (100 BYTE),
   UN_DESCRIPTION        VARCHAR2 (1000 BYTE),
   SOURCE_TYPE           VARCHAR2 (100 BYTE),
   DOCUMENT_NUMBERING1   VARCHAR2 (100 BYTE),
   DOCUMENT_NUMBERING2   VARCHAR2 (100 BYTE),
   HEAD_ID               VARCHAR2 (100 BYTE),
   LOC_ID                VARCHAR2 (100 BYTE),
   ORG_CODE              VARCHAR2 (100 BYTE),
   PO_RELEASE_ID         VARCHAR2 (100 BYTE),
   REVISION_NUM          VARCHAR2 (100 BYTE),
   EXPECTED_DATE         VARCHAR2 (100 BYTE),
   LOT_CONTROL_CODE      VARCHAR2 (100 BYTE),
   SHELF_LIFE_DAYS       VARCHAR2 (100 BYTE),
   PO_LINE_ID            VARCHAR2 (100 BYTE),
   NOTE_TO_VENDOR        VARCHAR2 (1000 BYTE),
   PO_LINE_LOC_ID        VARCHAR2 (100 BYTE),
   HDR_EXPECTED_DATE     VARCHAR2 (100 BYTE),
   VENDOR_NAME           VARCHAR2 (1000 BYTE),
   BUYER_NAME            VARCHAR2 (1000 BYTE),
   REVISION_NUM1         VARCHAR2 (100 BYTE),
   SORT1                 VARCHAR2 (100 BYTE),
   SORT2                 VARCHAR2 (100 BYTE),
   SORT3                 VARCHAR2 (100 BYTE),
   SORT4                 VARCHAR2 (100 BYTE),
   SORT5                 VARCHAR2 (100 BYTE),
   SORT6                 VARCHAR2 (100 BYTE),
   ITEM_ID               VARCHAR2 (100 BYTE)
) ON COMMIT PRESERVE ROWS;
/
