/*************************************************************************
  $Header ALTER_XXWC_OM_CSP_NOTIFICATIONS_TBL.sql $
  Module Name: ALTER_XXWC_OM_CSP_NOTIFICATIONS_TBL.sql

  PURPOSE:   To Add the column XXWC.XXWC_OM_CSP_NOTIFICATIONS_TBL

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        08/03/2017  Ashwin Sridhar             TMS#20170711-00251 - CSP approval workflow dashboard Feedback  
**************************************************************************/
ALTER TABLE XXWC.XXWC_OM_CSP_NOTIFICATIONS_TBL ADD (Waiting_On_User VARCHAR2(3));
/