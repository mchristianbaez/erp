   /*
      ************************************************************************************************************************
    HD Supply - Whitecap
    All rights reserved.
   ***************************************************************************************************************************
     PURPOSE:   

     REVISIONS:
     Ver        Date                Author      Description
     ---------  ---------- ---------------  ----------------------------------------------------------------------------------
     1.0       01/13/2017  P.Vamshidhar     TMS:20161104-00046 Wells Fargo Check Printing Files archiving without transmitting.
   **************************************************************************************************************************** 
  */
DECLARE
   l_db_name    VARCHAR2 (20) := NULL;
   l_path_inb   VARCHAR2 (240) := NULL;
   l_path_oub   VARCHAR2 (240) := NULL;
   l_sql        VARCHAR2 (240) := NULL;
   g_command    VARCHAR2 (2000) := NULL; 
   l_result     VARCHAR2 (2000) := NULL;    
--
BEGIN
   --
   SELECT LOWER (name) INTO l_db_name FROM v$database;
   --
	begin   
     --	
      l_path_oub := '/xx_iface/' || l_db_name || '/outbound/check_print/wc_wellsfg/stage';	
	 --
				 g_command := 'mkdir '
							 ||' '
							 ||l_path_oub                      
							 ; 
				 --
				 dbms_output.put_line('MKDIR command:');
				 dbms_output.put_line('============');
				 dbms_output.put_line(g_command);
				 --
				 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
				 into   l_result
				 from   dual;
				 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
				 --
                 if (l_result is null) then --folder created just make sure it has 777 permission				    
					 --
				     g_command := 'chmod 777 '
							 ||l_path_oub
							 ; 
					 --
					 dbms_output.put_line('CHMOD command:');
					 dbms_output.put_line('============');
					 dbms_output.put_line(g_command);
					 --
					 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
					 into   l_result
					 from   dual;
					 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
					 --				 
				 end if;
     --
	exception
		when others then
		 dbms_output.put_line('Error in step1..gsc_wellsfargo folder setup, message =>'||sqlerrm);
		 raise program_error;
	end;
	-- 
EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('OUTER BLOCK : '||SQLERRM);
END;
/