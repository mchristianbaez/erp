/*
 TMS: 20160525-00037  After header is deleted, orphan data left in sales order line interface tables
 Date: 11/16/2015
 */
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   v_count   NUMBER;
BEGIN
   DBMS_OUTPUT.put_line ('TMS:20160525-00037   , Script 1 -Before delete');

   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM apps.oe_lines_iface_all oli
       WHERE     order_source_id = 10
             AND NOT EXISTS
                    (SELECT 1
                       FROM oe_headers_iface_all ohi
                      WHERE     oli.order_source_id = ohi.order_source_id
                            AND oli.orig_sys_document_ref =
                                   ohi.orig_sys_document_ref);

      DELETE FROM apps.oe_lines_iface_all oli
            WHERE     order_source_id = 10
                  AND NOT EXISTS
                         (SELECT 1
                            FROM oe_headers_iface_all ohi
                           WHERE     oli.order_source_id =
                                        ohi.order_source_id
                                 AND oli.orig_sys_document_ref =
                                        ohi.orig_sys_document_ref);

   DBMS_OUTPUT.put_line (
         'TMS: 20160525-00037, Script 1 -After delete, rows deleted: '||SQL%ROWCOUNT);

   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         DBMS_OUTPUT.PUT_LINE (
            'Error in deleting records from interface table-' || SQLERRM);
   END;

   COMMIT;

EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error is' || SQLERRM);
END;
/