/*************************************************************************
  $Header TMS_20160314-00035_ITEM_STUCK_IN_ORG_244.sql $
  Module Name: TMS_20160314-00035 Data Fix for item stuck in org 244 

  PURPOSE: Data Fix for item stuck in org 244

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        14-MAR-2016  Pattabhi Avula         TMS#20160314-00035

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160314-00035   , Before Update');

UPDATE MTL_CYCLE_COUNT_ENTRIES 
SET EXPORT_FLAG = NULL 
WHERE ORGANIZATION_ID = 332 
AND ENTRY_STATUS_CODE IN (1,3) 
AND EXPORT_FLAG IS NOT NULL;

-- Expected to update on row

   DBMS_OUTPUT.put_line (
         'TMS: 20160314-00035 Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160314-00035   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160314-00035, Errors : ' || SQLERRM);
END;
/