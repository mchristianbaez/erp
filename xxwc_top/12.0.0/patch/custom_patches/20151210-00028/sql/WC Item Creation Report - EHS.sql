--Report Name            : WC Item Creation Report - EHS
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for WC Item Creation Report - EHS
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_1041488_KZKAHO_V
xxeis.eis_rs_ins.v( 'XXEIS_1041488_KZKAHO_V',201,'Paste SQL View for WC Item Creation Report - EHS','1.0','','','MR020532','APPS','WC Item Creation Report - EHS View','X1KV','','');
--Delete View Columns for XXEIS_1041488_KZKAHO_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_1041488_KZKAHO_V',201,FALSE);
--Inserting View Columns for XXEIS_1041488_KZKAHO_V
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','ORG',201,'','','','','','MR020532','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','ITEM_NUMBER',201,'','','','','','MR020532','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','DESCRIPTION',201,'','','','','','MR020532','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','ITEM_STATUS',201,'','','','','','MR020532','VARCHAR2','','','Item Status','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','ITEM_TYPE',201,'','','','','','MR020532','VARCHAR2','','','Item Type','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','CREATION_DATE',201,'','','','','','MR020532','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','HAZARDOUS_MATERIAL_FLAG',201,'','','','','','MR020532','VARCHAR2','','','Hazardous Material Flag','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','HAZARD_CLASS_ID',201,'','','','','','MR020532','NUMBER','','','Hazard Class Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','SHELF_LIFE_DAYS',201,'','','','','','MR020532','NUMBER','','','Shelf Life Days','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','MSDS',201,'','','','','','MR020532','VARCHAR2','','','Msds','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','ORMD_FLAG',201,'','','','','','MR020532','VARCHAR2','','','Ormd Flag','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','PESTICIDE_FLAG',201,'','','','','','MR020532','VARCHAR2','','','Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','PESTICIDE_STATE',201,'','','','','','MR020532','VARCHAR2','','','Pesticide State','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','CA_PROP_65',201,'','','','','','MR020532','VARCHAR2','','','Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','VOC_CATEGORY',201,'','','','','','MR020532','VARCHAR2','','','Voc Category','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','VOC_SUB_CATEGORY',201,'','','','','','MR020532','VARCHAR2','','','Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','CONTAINER_TYPE',201,'Container Type','CONTAINER_TYPE','','','','MR020532','VARCHAR2','','','Container Type','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','DOT_PROPER_SHIPPING_NAME',201,'Dot Proper Shipping Name','DOT_PROPER_SHIPPING_NAME','','','','MR020532','VARCHAR2','','','Dot Proper Shipping Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','PACKAGING_GROUP',201,'Packaging Group','PACKAGING_GROUP','','','','MR020532','VARCHAR2','','','Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','VOC_CONTENT',201,'Voc Content','VOC_CONTENT','','','','MR020532','VARCHAR2','','','Voc Content','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','LIMITED_QTY_FLAG',201,'Limited Qty Flag','LIMITED_QTY_FLAG','','','','MR020532','VARCHAR2','','','Limited Qty Flag','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','PESTICIDE_FLAG_STATE',201,'Pesticide Flag State','PESTICIDE_FLAG_STATE','','','','MR020532','VARCHAR2','','','Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','HAZMAT_CONTAINER',201,'','','','','','MR020532','VARCHAR2','','','Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','HAZMAT_PACKAGING_GROUP',201,'','','','','','MR020532','VARCHAR2','','','Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','HAZMAT_DESCRIPTION',201,'','','','','','MR020532','VARCHAR2','','','Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1041488_KZKAHO_V','VOC_GL',201,'','','','','','MR020532','VARCHAR2','','','Voc Gl','','','');
--Inserting View Components for XXEIS_1041488_KZKAHO_V
--Inserting View Component Joins for XXEIS_1041488_KZKAHO_V
END;
/
set scan on define on
prompt Creating Report Data for WC Item Creation Report - EHS
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC Item Creation Report - EHS
xxeis.eis_rs_utility.delete_report_rows( 'WC Item Creation Report - EHS' );
--Inserting Report - WC Item Creation Report - EHS
xxeis.eis_rs_ins.r( 201,'WC Item Creation Report - EHS','','','','','','MR020532','XXEIS_1041488_KZKAHO_V','Y','','select decode(mib.organization_id, orgs.organization_id, orgs.organization_code) ORG,
       mib.segment1 ITEM_NUMBER,
       mib.description,
       mib.inventory_item_status_code ITEM_STATUS,
       mib.item_type,
       mib.creation_date,
       mib.hazardous_material_flag,
       mib.hazard_class_id,
       mib.shelf_life_days,
       mib.ATTRIBUTE18 HAZMAT_CONTAINER,
       mib.ATTRIBUTE8  MSDS,
       mib.ATTRIBUTE11 ORMD_FLAG,
       mib.ATTRIBUTE9 Hazmat_Packaging_Group,
       mib.ATTRIBUTE17 hazmat_description,
       mib.ATTRIBUTE3 Pesticide_Flag,
       mib.ATTRIBUTE5 Pesticide_State,
       mib.ATTRIBUTE1 CA_Prop_65,
       mib.ATTRIBUTE4 VOC_GL,
       mib.ATTRIBUTE6 VOC_Category,
       mib.ATTRIBUTE7 VOC_Sub_category

from apps.MTL_SYSTEM_ITEMS_B MIB,
     apps.mtl_parameters orgs

WHERE orgs.organization_id = mib.organization_id
   and orgs.organization_code in (''MST'')
-- and  mib.inventory_item_status_code in (''Active'')
   and mib.item_type not in (''SPECIAL'')
   and mib.hazardous_material_flag = ''Y''
   and mib.creation_date > sysdate-365
order by mib.SEGMENT1,orgs.organization_code
','MR020532','','N','Audit and Setup','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - WC Item Creation Report - EHS
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'ORG','Org','','','','default','','1','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'DESCRIPTION','Description','','','','default','','3','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'ITEM_STATUS','Item Status','','','','default','','4','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'SHELF_LIFE_DAYS','Shelf Life Days','','','~~~','default','','7','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'HAZARD_CLASS_ID','Hazard Class Id','','','~~~','default','','8','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'ITEM_TYPE','Item Type','','','','default','','5','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'VOC_SUB_CATEGORY','Voc Sub Category','','','','default','','9','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'CA_PROP_65','Ca Prop 65','','','','default','','10','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'HAZARDOUS_MATERIAL_FLAG','Hazardous Material Flag','','','','default','','11','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'CREATION_DATE','Creation Date','','','','default','','6','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'PESTICIDE_FLAG','Pesticide Flag','','','','default','','13','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'MSDS','Msds','','','','default','','15','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'VOC_CATEGORY','Voc Category','','','','default','','18','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'ITEM_NUMBER','Item Number','','','','default','','2','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'CONTAINER_TYPE','Container Type','Container Type','','','','','20','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'DOT_PROPER_SHIPPING_NAME','Dot Proper Shipping Name','Dot Proper Shipping Name','','','','','17','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'PACKAGING_GROUP','Packaging Group','Packaging Group','','','','','12','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'VOC_CONTENT','Voc Content','Voc Content','','','','','19','N','','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'LIMITED_QTY_FLAG','Limited Qty Flag','Limited Qty Flag','','','','','21','','Y','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
xxeis.eis_rs_ins.rc( 'WC Item Creation Report - EHS',201,'PESTICIDE_FLAG_STATE','Pesticide Flag State','Pesticide Flag State','','','','','22','','Y','','','','','','','MR020532','N','N','','XXEIS_1041488_KZKAHO_V','','');
--Inserting Report Parameters - WC Item Creation Report - EHS
--Inserting Report Conditions - WC Item Creation Report - EHS
--Inserting Report Sorts - WC Item Creation Report - EHS
--Inserting Report Triggers - WC Item Creation Report - EHS
--Inserting Report Templates - WC Item Creation Report - EHS
--Inserting Report Portals - WC Item Creation Report - EHS
--Inserting Report Dashboards - WC Item Creation Report - EHS
--Inserting Report Security - WC Item Creation Report - EHS
xxeis.eis_rs_ins.rsec( 'WC Item Creation Report - EHS','20005','','50900',201,'MR020532','','');
--Inserting Report Pivots - WC Item Creation Report - EHS
END;
/
set scan on define on
