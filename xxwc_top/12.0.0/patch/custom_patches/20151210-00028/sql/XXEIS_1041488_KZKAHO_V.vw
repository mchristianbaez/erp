CREATE OR REPLACE FORCE VIEW APPS.XXEIS_1041488_KZKAHO_V (ORG, ITEM_NUMBER, DESCRIPTION, ITEM_STATUS, ITEM_TYPE, CREATION_DATE, HAZARDOUS_MATERIAL_FLAG, HAZARD_CLASS_ID, SHELF_LIFE_DAYS, HAZMAT_CONTAINER, Container_Type, MSDS, ORMD_FLAG, HAZMAT_PACKAGING_GROUP, Packaging_Group, HAZMAT_DESCRIPTION, DOT_Proper_Shipping_Name, PESTICIDE_FLAG, PESTICIDE_STATE, CA_PROP_65, VOC_GL, VOC_Content, VOC_CATEGORY, VOC_SUB_CATEGORY, Limited_Qty_Flag, Pesticide_Flag_State)
/**************************************************************************************************************
$Header XXEIS.XXEIS_1041488_KZKAHO_V $
Module Name: Purchasing
PURPOSE: View for EIS Report WC Item Creation Report - EHS
TMS Task Id : NA
REVISIONS:
Ver        Date        Author                Description
---------  ----------  ------------------    ----------------
1.0        NA           NA   Initial Version
1.1        12/30/2015  Mahender Reddy         20151210-00028
**************************************************************************************************************/
AS
  SELECT DECODE(mib.organization_id, orgs.organization_id, orgs.organization_code) ORG,
    mib.segment1 ITEM_NUMBER,
    mib.description,
    mib.inventory_item_status_code ITEM_STATUS,
    mib.item_type,
    mib.creation_date,
    mib.hazardous_material_flag,
    mib.hazard_class_id,
    mib.shelf_life_days,
    mib.ATTRIBUTE18 HAZMAT_CONTAINER,
	emib.Container_Type Container_Type, --Added for Ver 1.1
    --mib.ATTRIBUTE8 MSDS,  --Commeneted for Ver 1.1
	emib.MSDS, --Added for Ver 1.1
    mib.ATTRIBUTE11 ORMD_FLAG,
    mib.ATTRIBUTE9 Hazmat_Packaging_Group,
	emib.Packaging_Group Packaging_Group, --Added for Ver 1.1
    mib.ATTRIBUTE17 hazmat_description,
	emib.DOT_Proper_Shipping_Name DOT_Proper_Shipping_Name,--Added for Ver 1.1
  --  mib.ATTRIBUTE3 Pesticide_Flag, --Commeneted for Ver 1.1
	emib.Pesticide_Flag Pesticide_Flag, --Added for Ver 1.1
    mib.ATTRIBUTE5 Pesticide_State,
   -- mib.ATTRIBUTE1 CA_Prop_65, --Commeneted for Ver 1.1
	emib.CA_PROP_65 CA_PROP_65, --Added for Ver 1.1
    mib.ATTRIBUTE4 VOC_GL,
	emib.VOC_Content VOC_Content, --Added for Ver 1.1
   -- mib.ATTRIBUTE6 VOC_Category, --Commeneted for Ver 1.1
	emib.VOC_CATEGORY VOC_CATEGORY, --Added for Ver 1.1
    --mib.ATTRIBUTE7 VOC_Sub_category, --Commeneted for Ver 1.1
	emib.VOC_Sub_Category VOC_Sub_Category,--Added for Ver 1.1
	emib.Limited_Qty_Flag Limited_Qty_Flag, --Added for Ver 1.1
	emib.Pesticide_Flag_State Pesticide_Flag_State  --Added for Ver 1.1
FROM apps.MTL_SYSTEM_ITEMS_B MIB,
    apps.mtl_parameters orgs,
	(select inventory_item_id,
                organization_id,
		C_EXT_ATTR2 Packaging_Group,
		C_EXT_ATTR3 DOT_Proper_Shipping_Name,
		C_EXT_ATTR4 Limited_Qty_Flag,
		C_EXT_ATTR5 Container_Type,
		C_EXT_ATTR6 CA_PROP_65,
		C_EXT_ATTR7 VOC_Content,
		C_EXT_ATTR8 VOC_CATEGORY,
		C_EXT_ATTR9 VOC_Sub_Category,
		C_EXT_ATTR10 Pesticide_Flag,
		C_EXT_ATTR11 Pesticide_Flag_State,
		C_EXT_ATTR12 MSDS
      from apps.EGO_MTL_SY_ITEMS_EXT_B
      where attr_group_id = 1021
        and organization_id = 222) emib -- Added for Ver 1.1
  WHERE orgs.organization_id  = mib.organization_id
  and mib.inventory_item_id = emib.inventory_item_id(+) -- Added for Ver 1.1
  AND orgs.organization_code IN ('MST')
    -- and  mib.inventory_item_status_code in ('Active')
  AND mib.item_type NOT          IN ('SPECIAL')
  AND mib.hazardous_material_flag = 'Y'
  AND mib.creation_date           > sysdate-365
  ORDER BY mib.SEGMENT1,
    orgs.organization_code
/