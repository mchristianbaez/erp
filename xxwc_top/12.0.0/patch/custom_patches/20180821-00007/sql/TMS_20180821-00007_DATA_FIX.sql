SET SERVEROUT ON

BEGIN
  DBMS_OUTPUT.PUT_LINE('Insert start - Debit memos');
  
  INSERT
  INTO XXWC.XXWC_AR_AHH_DM_STG_TBL
    (SELECT *
      FROM XXWC.xxwcar_cash_rcpts_tbl
      WHERE RECEIPT_TYPE LIKE 'DEBIT%'
      AND CREATION_DATE>=to_date('12-JUL-2018','DD-MON-YYYY')
      AND RECEIPT_ID NOT IN
        (SELECT RECEIPT_ID FROM XXWC.XXWC_AR_AHH_DM_STG_TBL
        )	  
    );
	
  DBMS_OUTPUT.PUT_LINE('Debit memos inserted:'||SQL%ROWCOUNT);
  
  DBMS_OUTPUT.PUT_LINE('Insert Completed - Debit memos');
  
  DBMS_OUTPUT.PUT_LINE('Insert start - Cash Receipts');
  
  INSERT
  INTO XXWC.XXWC_AR_AHH_CASH_RCPTS_STG_TBL
    (SELECT *
      FROM XXWC.xxwcar_cash_rcpts_tbl
      WHERE RECEIPT_TYPE IS NULL
      AND STATUS          = 'AHH_PROCESSED'
      AND CREATION_DATE  >=to_date('12-JUL-2018','DD-MON-YYYY')
      AND RECEIPT_ID NOT IN
        (SELECT RECEIPT_ID FROM XXWC.XXWC_AR_AHH_CASH_RCPTS_STG_TBL
        )
    );
  DBMS_OUTPUT.PUT_LINE('Cash Receipts inserted:'||SQL%ROWCOUNT);
  
  DBMS_OUTPUT.PUT_LINE('Insert Completed - Debit memos');
  
  COMMIT;
  
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('ERROR OCCURED:'||SQLERRM);
  ROLLBACK;
END;
/  
  


