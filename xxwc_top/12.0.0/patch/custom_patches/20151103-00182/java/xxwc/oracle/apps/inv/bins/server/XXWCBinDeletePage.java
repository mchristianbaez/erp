/*************************************************************************
   *   $Header XXWCBinDeletePage.java $
   *   Module Name: XXWCBinDeletePage
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinDeletePage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
        1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
**************************************************************************/

package xxwc.oracle.apps.inv.bins.server;

import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.inv.lov.server.ItemLOV;
import oracle.apps.inv.lov.server.LocatorKFF;
import oracle.apps.inv.lov.server.SubinventoryLOV;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCBinDeletePage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCBinDeletePage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
 **************************************************************************/


public class XXWCBinDeletePage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCItemLOV mItemFld;
    TextFieldBean mItemDescFld;
    protected SubinventoryLOV mSubFld;
    protected XXWCBinsLOV mLocFld;
    TextFieldBean mLocFldPreFix;
    protected ButtonFieldBean mDelete;
    protected ButtonFieldBean mMenu;
    protected String m_userid;


    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinDeletePage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    private static String gCallFrom = "XXWCBinDeletePage";



    /*************************************************************************
     *   NAME: public XXWCBinDeletePage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public XXWCBinDeletePage(Session session) {
        initLayout(session);
    }

    
    /*************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
          1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void initLayout(Session session) {
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        /*Item Field*/
        // mItemFld = new LOVFieldBean();
        mItemFld = new XXWCItemLOV("ALL");
        mItemFld.setName("XXWC.ITEM");
        mItemFld.setRequired(true);
        /*Item Description*/
        mItemDescFld = new TextFieldBean();
        mItemDescFld.setName("XXWC.DESC");
        mItemDescFld.setPrompt("Item Desc");
        mItemDescFld.setEditable(false);
        /*Subinventory*/
        mSubFld = new SubinventoryLOV("INVINQ");
        mSubFld.setName("XXWC.SUB");
        mSubFld.setHidden(true);
        mSubFld.setRequired(false);
        /*Bin*/
        try {
            mLocFld = new XXWCBinsLOV();
            mLocFld.setName("XXWC.LOC");
            mLocFld.setRequired(false);
            mLocFld.setHidden(true);
            
            mLocFldPreFix = new TextFieldBean();
            mLocFldPreFix.setName("XXWC.WC_PREFIX");
            mLocFldPreFix.setRequired(false);
            mLocFldPreFix.setHidden(true);
            mLocFldPreFix.setEditable(false);
            
        } catch (Exception e) {
            UtilFns.trace("Error instantiating xxwc locator");
        }
        /*No Print and Assign*/
        mDelete = new ButtonFieldBean();
        mDelete.setName("XXWC.DELETE");
        mDelete.setPrompt("Delete");
        mDelete.setEnableAcceleratorKey(true);
        mDelete.setHidden(true);
        /*Cancel Properties*/
        mMenu = new ButtonFieldBean();
        mMenu.setName("XXWC.CANCEL");
        //mMenu.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
        mMenu.setPrompt("Cancel"); //Added 03/29/2016 TMS Ticket 20151103-00182
        mMenu.setEnableAcceleratorKey(true);
        mMenu.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mItemFld);
        addFieldBean(mItemDescFld);
        addFieldBean(mSubFld);
        addFieldBean(mLocFld);
        addFieldBean(mLocFldPreFix);
        addFieldBean(mDelete);
        addFieldBean(mMenu);
        /*Point Page to the XXWCPrimaryBinAssignmentFListener Listener*/
        XXWCBinDeleteFListener fieldListener = new XXWCBinDeleteFListener();
        /*Add Listener to each property*/
        mItemFld.addListener(fieldListener);
        mItemDescFld.addListener(fieldListener);
        mSubFld.addListener(fieldListener);
        mLocFld.addListener(fieldListener);
        mLocFldPreFix.addListener(fieldListener);
        mDelete.addListener(fieldListener);
        mMenu.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }
    }
    
    /*************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        try {
            String gMethod = "initPrompts";
            String gCallPoint = "Start";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Bin Delete (").append(s).append(")").toString());

            mItemFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                  "INV_ITEM_PROMPT"));
            mItemFld.retrieveAttributes("INV_ITEM_PROMPT");
            mItemDescFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                      "INV_DESCRIPTION_PROMPT"));
            mItemDescFld.retrieveAttributes("INV_DESCRIPTION_PROMPT");
            mSubFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                 "INV_SUB_PROMPT"));
            mSubFld.retrieveAttributes("INV_SUB_PROMPT");
            mLocFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                 "INV_LOCATOR_PROMPT"));
            mLocFld.retrieveAttributes("INV_LOCATOR_PROMPT");
            mLocFldPreFix.setPrompt("Loc PreFix");
            savenextPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            areThePromptsSet = true;
            gCallPoint = "End";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
          1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void setPrompts(Session session) {
            String gMethod = "setPrompts";
            String gCallPoint = "Start";
            if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                }
            }
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Bin Delete (").append(s).append(")").toString());

            mDelete.setPrompt(donePrompt);
            mMenu.setPrompt(cancelPrompt);
            mDelete.retrieveAttributes("INV_DONE_PROMPT");
            mMenu.retrieveAttributes("INV_CANCEL_PROMPT");
            mDelete.setPrompt("Delete");
            //mMenu.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
    }


    /*************************************************************************
     *   NAME: public ItemLOV  getmItemFld()
     *
     *   PURPOSE:   return the value of mItemFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
      **************************************************************************/

    public XXWCItemLOV getmItemFld()
        //public LOVFieldBean getmItemFld()
    {
        return mItemFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmItemDescFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public TextFieldBean getmItemDescFld() {
        return mItemDescFld;
    }

    /*************************************************************************
     *   NAME: public SubinventoryLOV getmSubFld()
     *
     *   PURPOSE:   return the value of mSubFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public SubinventoryLOV getmSubFld() {
        return mSubFld;
    }

    /*************************************************************************
     *   NAME: public XXWCBinsLOV getmLocFld()
     *
     *   PURPOSE:   return the value of mLocFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public XXWCBinsLOV getmLocFld() {
        return mLocFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmLocFldPreFix
     *
     *   PURPOSE:   return the value of mLocFldPreFix
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public TextFieldBean getmLocFldPreFix() {
         return mLocFldPreFix;
     }

     /*************************************************************************
      *   NAME: public ButtonFieldBean getmMenu
      *
      *   PURPOSE:   return the value of mMenu
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
      **************************************************************************/

    public ButtonFieldBean getmMenu() {
        return mMenu;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDelete()
     *
     *   PURPOSE:   return the value of mDelete
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public ButtonFieldBean getmDelete() {
        return mDelete;
    }

    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();
    }

}
