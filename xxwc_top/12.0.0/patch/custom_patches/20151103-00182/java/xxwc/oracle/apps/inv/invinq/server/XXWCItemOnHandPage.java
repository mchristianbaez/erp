/*************************************************************************
   *   $Header XXWCItemOnHandPage.java $
   *   Module Name: XXWCItemOnHandPage
   *   
   *   Package: package xxwc.oracle.apps.inv.invinq.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCItemOnHandPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
        1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
**************************************************************************/

package xxwc.oracle.apps.inv.invinq.server;

import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.inv.lov.server.ItemLOV;
import oracle.apps.inv.lov.server.LocatorKFF;
import oracle.apps.inv.lov.server.SubinventoryLOV;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCItemOnHandPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCItemOnHandPage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
 **************************************************************************/


public class XXWCItemOnHandPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCItemLOV mItemFld;
    protected XXWCItemLOV mUPCFld;
    TextFieldBean mItemDescFld;
    protected SubinventoryLOV mSubFld;
    protected LocatorKFF mLocFld;
    TextFieldBean mUOMFld;
    TextFieldBean mLotFld;
    TextFieldBean mOnHandFld;
    TextFieldBean mAvailableFld;
    TextFieldBean mOnOrderFld;
    protected ButtonFieldBean mPrevious;
    protected ButtonFieldBean mNext;
    protected ButtonFieldBean mNextItem;
    protected ButtonFieldBean mBins;
    protected ButtonFieldBean mCancel;
    protected String m_userid;
    public XXWCItemOnHandIterator mItemOnHandSeq;
    public XXWCItemOnHand mCurrentItemOnHand;

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.invinq.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandPage";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCItemOnHandPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    private static String gCallFrom = "XXWCItemOnHandPage";



    /*************************************************************************
     *   NAME: public XXWCItemOnHandPage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public XXWCItemOnHandPage(Session session) {
        initLayout(session);
    }

    
    /*************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
          1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void initLayout(Session session) {
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        /*Item Field*/
        // mItemFld = new LOVFieldBean();
        mItemFld = new XXWCItemLOV("ALL");
        mItemFld.setName("INV.ITEM");
        mItemFld.setRequired(false); //updated 5/11/2015
        //UPC//
        // mItemFld = new LOVFieldBean();
        mUPCFld = new XXWCItemLOV("UPC");
        mUPCFld.setName("XXWC.UPC");
        mUPCFld.setRequired(false);
        mUPCFld.setEditable(false);
        mUPCFld.setHidden(true); //added 5/11/2015
        /*Item Description*/
        mItemDescFld = new TextFieldBean();
        mItemDescFld.setName("INV.DESC");
        mItemDescFld.setPrompt("Item Desc");
        mItemDescFld.setEditable(false);
        mItemDescFld.setHidden(false);
        /*Subinventory*/
        mSubFld = new SubinventoryLOV("INVINQ");
        mSubFld.setName("INV.SUB");
        mSubFld.setRequired(false);
        mSubFld.setEditable(false);
        mSubFld.setHidden(false);
        /*Bin*/
        try {
            mLocFld = new LocatorKFF("INQLOC", session);
            mLocFld.setName("INV.LOC");
            mLocFld.setRequired(false);
            mLocFld.setEditable(false);
            mLocFld.setHidden(true);
        } catch (Exception e) {
            UtilFns.trace("Error instantiating rcvtxn locator kff");
        }
        /*UOM*/
        mUOMFld = new TextFieldBean();
        mUOMFld.setName("XXWC.UOM");
        mUOMFld.setEditable(false);
        mUOMFld.setHidden(false);
        /*Lot*/
        mLotFld = new TextFieldBean();
        mLotFld.setName("XXWC.LOT");
        mLotFld.setEditable(false);
        mLotFld.setHidden(true);
        /*On-Hand*/
        mOnHandFld = new TextFieldBean();
        mOnHandFld.setName("XXWC.ON_HAND");
        mOnHandFld.setEditable(false);
        mOnHandFld.setHidden(false);
        /*Available*/
        mAvailableFld = new TextFieldBean();
        mAvailableFld.setName("XXWC.AVAILABLE");
        mAvailableFld.setEditable(false);
        mAvailableFld.setHidden(false);
        /*On-Order*/
        mOnOrderFld = new TextFieldBean();
        mOnOrderFld.setName("XXWC.ON_ORDER");
        mOnOrderFld.setEditable(false);
        mOnOrderFld.setHidden(false);
        /*Next Properties*/
        mNext = new ButtonFieldBean();
        mNext.setName("XXWC.NEXT");
        mNext.setPrompt("Next");
        mNext.setEnableAcceleratorKey(true);
        mNext.setHidden(true);
        /*Previous Properties*/
        mPrevious = new ButtonFieldBean();
        mPrevious.setName("XXWC.PREVIOUS");
        mPrevious.setPrompt("Previous");
        mPrevious.setEnableAcceleratorKey(true);
        mPrevious.setHidden(true);
        /*Save Properties*/
        mBins = new ButtonFieldBean();
        mBins.setName("XXWC.BINS");
        mBins.setPrompt("Bins");
        mBins.setEnableAcceleratorKey(true);
        mBins.setHidden(false);
        /*Save Properties*/
        mCancel = new ButtonFieldBean();
        mCancel.setName("INV.CANCEL");
        //mCancel.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
        mCancel.setPrompt("Cancel"); //Added 03/29/2016 TMS Ticket 20151103-00182
        mCancel.setEnableAcceleratorKey(true);
        mCancel.setHidden(false);
        /*Cancel Properties*/
        mNextItem = new ButtonFieldBean();
        mNextItem.setName("XXWC.NEXT_ITEM");
        mNextItem.setEnableAcceleratorKey(true);
        mNextItem.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mItemFld);
        addFieldBean(mUPCFld);
        addFieldBean(mItemDescFld);
        addFieldBean(mSubFld);
        addFieldBean(mLocFld);
        addFieldBean(mUOMFld);
        addFieldBean(mLotFld);
        addFieldBean(mOnHandFld);
        addFieldBean(mAvailableFld);
        addFieldBean(mOnOrderFld);
        addFieldBean(mNext);
        addFieldBean(mPrevious);
        addFieldBean(mBins);
        addFieldBean(mNextItem);
        addFieldBean(mCancel);
        /*Point Page to the XXWCPrimaryBinAssignmentFListener Listener*/
        XXWCItemOnHandFListener fieldListener = new XXWCItemOnHandFListener();
        /*Add Listener to each property*/
        mItemFld.addListener(fieldListener);
        mUPCFld.addListener(fieldListener);
        mItemDescFld.addListener(fieldListener);
        mSubFld.addListener(fieldListener);
        mLocFld.addListener(fieldListener);
        mLotFld.addListener(fieldListener);
        mUOMFld.addListener(fieldListener);
        mOnHandFld.addListener(fieldListener);
        mAvailableFld.addListener(fieldListener);
        mOnOrderFld.addListener(fieldListener);
        mPrevious.addListener(fieldListener);
        mNext.addListener(fieldListener);
        mBins.addListener(fieldListener);
        mNextItem.addListener(fieldListener);
        mCancel.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }
    }
    
    /*************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        try {
            String gMethod = "initPrompts";
            String gCallPoint = "Start";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Bin Putaway (").append(s).append(")").toString());

            mItemFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                  "INV_ITEM_PROMPT"));
            mItemFld.retrieveAttributes("INV_ITEM_PROMPT");
            mUPCFld.setPrompt("UPC");
            mItemDescFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                      "INV_DESCRIPTION_PROMPT"));
            mItemDescFld.retrieveAttributes("INV_DESCRIPTION_PROMPT");
            mSubFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                 "INV_SUB_PROMPT"));
            mSubFld.retrieveAttributes("INV_SUB_PROMPT");
            mLocFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                 "INV_LOCATOR_PROMPT"));
            mLocFld.retrieveAttributes("INV_LOCATOR_PROMPT");
            
            mUOMFld.setPrompt("UOM");
            mLotFld.setPrompt("Lot");
            savenextPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            areThePromptsSet = true;
            gCallPoint = "End";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
          1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void setPrompts(Session session) {
            String gMethod = "setPrompts";
            String gCallPoint = "Start";
            if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                    
                }
            }
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Item Inquiry (").append(s).append(")").toString());
            mOnHandFld.setPrompt("On-Hand");
            mAvailableFld.setPrompt("Avail");
            mOnOrderFld.setPrompt("On-Order");
            mNext.setPrompt("Next");
            mPrevious.setPrompt("Previous");
            mBins.setPrompt("Bins");
            mNextItem.setPrompt("Next Item");
            mCancel.setPrompt(cancelPrompt);
            mNextItem.retrieveAttributes("INV_SAVE_NEXT_PROMPT");
            mCancel.retrieveAttributes("INV_CANCEL_PROMPT");
            //mCancel.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
    }


    /*************************************************************************
     *   NAME: public XXWCItemLOV  getmItemFld()
     *
     *   PURPOSE:   return the value of mItemFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
      **************************************************************************/

    public XXWCItemLOV getmItemFld()
        //public LOVFieldBean getmItemFld()
    {
        return mItemFld;
    }



    /*************************************************************************
     *   NAME: public XXWCItemLOV  getmItemFld()
     *
     *   PURPOSE:   return the value of mItemFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
      **************************************************************************/

    public XXWCItemLOV getmUPCFld()
        //public LOVFieldBean getmItemFld()
    {
        return mUPCFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmItemDescFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public TextFieldBean getmItemDescFld() {
        return mItemDescFld;
    }

    /*************************************************************************
     *   NAME: public SubinventoryLOV getmSubFld()
     *
     *   PURPOSE:   return the value of mSubFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public SubinventoryLOV getmSubFld() {
        return mSubFld;
    }
    
    /*************************************************************************
     *   NAME: public LocatorKFF getmLocFld()
     *
     *   PURPOSE:   return the value of mLocFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public LocatorKFF getmLocFld() {
        return mLocFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmUOMFld()
     *
     *   PURPOSE:   return the value of mOnHandFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public TextFieldBean getmUOMFld() {
        return mUOMFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmLotFld()
     *
     *   PURPOSE:   return the value of mLotFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public TextFieldBean getmLotFld() {
        return mLotFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmOnHandFld()
     *
     *   PURPOSE:   return the value of mOnHandFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public TextFieldBean getmOnHandFld() {
        return mOnHandFld;
    }


    /*************************************************************************
     *   NAME: public TextFieldBean getmAvailableFld()
     *
     *   PURPOSE:   return the value of mAvailableFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public TextFieldBean getmAvailableFld() {
        return mAvailableFld;
    }


    /*************************************************************************
     *   NAME: public TextFieldBean getmItemDescFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public TextFieldBean getmOnOrderFld() {
        return mOnOrderFld;
    }

     /*************************************************************************
      *   NAME: public ButtonFieldBean getmNextItem
      *
      *   PURPOSE:   return the value of mNextItem
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
      **************************************************************************/

    public ButtonFieldBean getmNextItem() {
        return mNextItem;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmPrevious
     *
     *   PURPOSE:   return the value of mNextItem
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public ButtonFieldBean getmPrevious() {
       return mPrevious;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmBins()
     *
     *   PURPOSE:   return the value of mNextItem
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public ButtonFieldBean getmBins() {
       return mBins;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmNext()
     *
     *   PURPOSE:   return the value of mNext
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public ButtonFieldBean getmNext() {
        return mNext;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmCancel()
     *
     *   PURPOSE:   return the value of mCancel
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public ButtonFieldBean getmCancel() {
        return mCancel;
    }

    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       02-MAR-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();
    }

    /*************************************************************************
     *   NAME: setXXWCBinPutAwaySeq(XXWCBinPutAwayIterator XXWCBinPutAwayIterator)
     *
     *   PURPOSE:   sets the vector sequence number on the XXWCBinPutAwayIterator class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public void setXXWCItemOnHandSeq(XXWCItemOnHandIterator XXWCItemOnHandIterator) {
        mItemOnHandSeq = XXWCItemOnHandIterator;
    }

    /*************************************************************************
     *   NAME: XXWCItemOnHandIterator getmXXWCItemOnHandSeq()
     *
     *   PURPOSE:   gets the vector sequence number on the XXWCPrimaryBinAssignmentIterator class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public XXWCItemOnHandIterator getmXXWCItemOnHandSeq() {
        return mItemOnHandSeq;
    }

    /*************************************************************************
     *   NAME: XXWCBinPutAway getmCurrentXXWCItemOnHand()
     *
     *   PURPOSE:   gets the current sequence number on the XXWCItemOnHandIterator class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public XXWCItemOnHand getmCurrentXXWCItemOnHand() {
        return mCurrentItemOnHand;
    }

    /*************************************************************************
     *   NAME: setCurrentXXWCItemOnHand(XXWCItemOnHand XXWCItemOnHand)
     *
     *   PURPOSE:   sets the current sequence number on the XXWCItemOnHandIterator class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       21-OCT-2014   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00175 - RF - Inventory Inquiry   
     **************************************************************************/

    public void setCurrentXXWCItemOnHand(XXWCItemOnHand XXWCItemOnHand) {
        mCurrentItemOnHand = XXWCItemOnHand;
    }

}

